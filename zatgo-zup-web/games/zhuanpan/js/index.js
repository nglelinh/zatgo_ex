$(function() {

    var token = GetQueryString("zatgoToken");
    var webSign = GetQueryString("webSign");
    var skin = GetQueryString('skin');
    var gameId = "fd8a73044f72497daa4899775cea4a2f";
    var pageNo = 1;
    $.session.set("token", token);
    if(skin){
        $("html").removeClass().addClass("skin"+skin);
	}


	var load = function() {
        $("#my_record").on("click",function () {
            initRecordList();
            $("#recordList").show();
        });

        $(".close-list").on("click",function () {
            $("#recordList").hide();
        });
		$.doAjax({
			url: base_url + "/game/zhuanpan/load/fd8a73044f72497daa4899775cea4a2f",
			type: 'post',
			beforeSend: function(request) {
				request.setRequestHeader("token", $.session.get("token"));
				request.setRequestHeader("webSign", webSign);
			},
			success: function(ret) {
				var retData = null;
				if(typeof(ret)=="object"){
                    retData = ret;
				}else {
                    retData = JSON.parse(ret);
				}
                var errCode = "对不起，游戏维护中，请稍后再来!("+retData.code+")";

				if(retData.code == "0000"&&retData.data) {

					var data = ret.data;

					initGameFee(data.gameFee);
					initGameFreeTimes(data.todayFreeTimes);
					initGamePrize(data.gamePrize,data.gameFee,data.todayFreeTimes);
				}else {
                    //运行失败
                    $.alert({
                        title:"提醒",
                        content:errCode,
                    });
				}

			},
			error: function(err) {
                //运行失败
                $.alert({
                    title:"提醒",
                    content:err.message,
                });
			}
		});
	}

	var initGameFee = function(gameFees) {

		var feeHtml = "";
		for(var i = 0; i < gameFees.length; i++) {
			feeHtml += " / " + gameFees[i].fee + gameFees[i].coin.toUpperCase()
		}
		$("#gameFee").html(feeHtml.substring(3));
	}
	
	var parseText = function(value, prize) {
		if(prize == "1") {
			return value + "<p class='prize-name'>ZAT</p>";
		} else if(prize == "2") {
			return value + "<p class='prize-name'>算力</p>";
		}else {
            return value + "<p class='prize-name'>"+prize+"</p>";
        }
	}

	var initGameFreeTimes = function(todayFreeTimes) {
        /* 切换提示语 */
        $(".free_times").show();
        $("#no_free").hide();
		if(todayFreeTimes==-1){
			//免费游戏
			$("#tip .free_times").hide();
			$("#tip .game_fee").hide();
		}else if(todayFreeTimes == 0) {
			//次数用完需要收费
			$("#tip .free_times").hide();
			$("#tip .game_fee").show();
			/* 切换提示语 */
			$(".free_times").hide();
			$("#no_free").show();
		} else {
			$("#tip .free_times").show();
			$("#tip .game_fee").hide();
		}
		$("#freeTimes").html(todayFreeTimes);
		$("#freeTimesBtn").html(todayFreeTimes);
	}

	var initGamePrize = function(gamePrizes,gameFees,todayFreeTimes) {
		var prizes = [];
		for(var i = 0; i < gamePrizes.length; i++) {
			var p = {};
            if(gamePrizes[i].aliasName!==null){
                p.text = parseText(gamePrizes[i].value, gamePrizes[i].aliasName);
            }else {
                p.text = parseText(gamePrizes[i].value, gamePrizes[i].prize);
            }
			//p.text = parseText(gamePrizes[i].value, gamePrizes[i].prize);
			prizes.push(p);
		}

		prizes.push({
			text: "谢谢参与"
		});

		gbTurntable.init({
			id: 'turntable',
			config: function(callback) {
				// 获取奖品信息
				callback && callback(prizes)
			},
			gameFees:gameFees,
			freeTimes:todayFreeTimes,
			getPrize: function(callback,errorCallback) {
				// 获取中奖信息
				$.doAjax({
					url: base_url + "/game/zhuanpan/run/fd8a73044f72497daa4899775cea4a2f/zat",
					type: 'post',
					beforeSend: function(request) {
						request.setRequestHeader("token", $.session.get("token"));
						request.setRequestHeader("webSign", webSign);
					},
					success: function(ret) {
						if(ret.code == "0000") {
							var data = ret.data;
							
							var num = prizes.length-1; //奖品ID
							var chances = data.todayFreeTimes; // 剩余可抽奖次数
							
							if(data.result==1){
								//中奖
								num = data.prize.level-1;
							}
							
							initGameFreeTimes(chances);
							
							callback && callback([num, chances]);
						}else if(ret.code == "1010"){
							//余额不足
							errorCallback && errorCallback("您的余额不足，请充值后继续。");
						}else{
							//其他错误
							errorCallback && errorCallback("对不起，游戏维护中，请稍后再来。");
						}
					},
					error: function(err) {
						//其他
						errorCallback && errorCallback("对不起，游戏维护中，请稍后再来。");
					}
				});

				

			},
			gotBack: function(data) {
				if(data=="谢谢参与"){
					$.alert({
						title:'',
						content:'谢谢参与，再接再厉~',
					});
				}else{
					$.alert({
						title:'',
						content:'恭喜您抽中' + data + '，奖品将自动发放至您的账户内~',
					});
				}
			}
		});
	}

	var initRecordList = function () {
        var dataObj={
            gameId: gameId,
            pageNo: pageNo,
            pageSize: 10
        };
        $.doAjax({
            url:base_url+"/game/zhuanpan/recordList",
            type:"POST",
            data:JSON.stringify(dataObj),
            beforeSend: function(request) {
                request.setRequestHeader("token", $.session.get("token"));
                request.setRequestHeader("webSign", webSign);
            },
            success:function (data) {
            	var dataList = data.data.list;
            	if(dataList.length===0){
                    $("#record_list").css({
						'color':'#f3f3f3',
                        'line-height': '9.5rem',
						"font-size":"0.6rem"
					}).html("暂无记录");
				}else {
                    recordListHtml(data);
				}

                $('#pagination').pagination({
                    currentPage: 1,
                    isShow: false,
                    count: 4,
                    prevPageText: "<",
                    nextPageText: ">",
                    totalPage: data.data.pages,
                    callback: function(current) {
                        nextRecordList(current)
                    }
                });
            }
        })
    }

    var nextRecordList = function (page) {
        var dataObj={
            gameId: gameId,
            pageNo: page,
            pageSize: 10
        };
        $.doAjax({
            url:base_url+"/game/zhuanpan/recordList",
            type:"POST",
            data:JSON.stringify(dataObj),
            beforeSend: function(request) {
                request.setRequestHeader("token", $.session.get("token"));
                request.setRequestHeader("webSign", webSign);
            },
            success:function (data) {
                recordListHtml(data);
            }
        })
    }

    var recordListHtml = function (data) {
        var dataList = data.data.list;
        var html="";
        var prize = "";//奖品
        var free = "";//费用
        var time = "";//时间
        for(var i=0;i<dataList.length;i++){
            time = fmtDate(dataList[i].recordTime);
            if(dataList[i].feeType==2){//1-免费参与   2-付费参与
                free = dataList[i].feeAmount+dataList[i].feeCoin;
            }else {
                free = "免费参与";
            }
            if(dataList[i].isWin==1){//是否获胜 0-否  1-是
                // prize = dataList[i].prizeInfo;
				switch (dataList[i].prizeType){//中奖类型 1-ZAT，2-算力，3-红包，4-实物
					case 1:
                        prize = dataList[i].prizeInfo+dataList[i].prizeCoinType;
                        break;
                    case 2:
                        prize = dataList[i].prizeInfo+"算力";
                        break;
					default:
                        prize = "未中奖";
						break;
				}

            }else {
                prize = "未中奖";
            }

            html += "<li><span>"+time+"</span><span>"+free+"</span><span>"+prize+"</span></li>"
        }
        $("#record_list").html(html);
    }

    var fmtDate = function (obj){

        var date =  new Date(obj);
        var y = 1900+date.getYear();
        var m = "0"+(date.getMonth()+1);
        var d = "0"+date.getDate();
        return y+"."+m.substring(m.length-2,m.length)+"."+d.substring(d.length-2,d.length);
    }
	
	load();

});