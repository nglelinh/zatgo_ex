$.extend({
	doAjax:function(params){
		var async=true;
		if(params.async == false)
		{
			async=params.async;
		}
		
		//按钮控制相关：按钮禁止重复点击、按钮上显示的内容
		var button = null;
		var buttonName = null;
		if(params.buttonId!=null){
			button = $("#"+params.buttonId);
			button.prop("disabled","disabled");
			buttonName = button.html();
			if(params.message!=null){
				button.html(params.message);
			}
		}
		
		$.ajax({
	    	url:params.url,
	    	type:params.type,
	    	data:params.data,
	    	async: async,
	    	contentType:"application/json",
			beforeSend: function(request) {
				params.beforeSend && params.beforeSend(request);
			},
	    	error : function(XMLHttpRequest, textStatus, errorThrown) {
	    		
	    		if(button!=null){
	        		button.prop("disabled","");
	        		if(buttonName!=null){
	        			button.html(buttonName);
	        		}
	        	}
	    		
	    		var ret = XMLHttpRequest.responseText;
	    		var err = $.parseJSON(ret);
	    		if(params.error!=undefined){
	    			params.error(err);
	    		}else{
	    			alert(err.message);
	    			if(err.code == 8004){
						layer.alert(err.message,{btn:['确定']},function(){
							window.location.href="/auth/login";
						});
					}
	    		}
	        },
	        success : function(data) {
	        	
	        	if(button!=null){
	        		button.prop("disabled","");
	        		if(buttonName!=null){
	        			button.html(buttonName);
	        		}
	        	}
	        	
	        	if(params.success!=undefined){
	        		params.success(data);
	        	}
	        }
	    });
	}
});

$.extend({
	doAjaxUpload:function(params){
		var async=true;
		if(params.async!=null&&params.async!=""&&params.async!=undefined)
		{
			async=params.async;
		}
		
		//按钮控制相关：按钮禁止重复点击、按钮上显示的内容
		var button = null;
		var buttonName = null;
		if(params.buttonId!=null){
			button = $("#"+params.buttonId);
			button.prop("disabled","disabled");
			buttonName = button.html();
			if(params.message!=null){
				button.html(params.message);
			}
		}
		var form = new FormData(document.getElementById(params.formId));
		$.ajax({
			url:params.url,
			type:'post',
			data:form,
			processData:false,
            contentType:false,
            error : function(XMLHttpRequest, textStatus, errorThrown) {
	    		
	    		if(button!=null){
	        		button.prop("disabled","");
	        		if(buttonName!=null){
	        			button.html(buttonName);
	        		}
	        	}
	    		
	    		var ret = XMLHttpRequest.responseText;
	    		var err = $.parseJSON(ret);
	    		if(params.error!=undefined){
	    			params.error(err);
	    		}else{
	    			alert(err.message);
	    			if(err.code == 8004){
						layer.alert(err.message,{btn:['确定']},function(){
							window.location.href="/auth/login";
						});
					}
	    		}
	        },
	        success : function(data) {
	        	
	        	if(button!=null){
	        		button.prop("disabled","");
	        		if(buttonName!=null){
	        			button.html(buttonName);
	        		}
	        	}
	        	
	        	if(params.success!=undefined){
	        		params.success(data);
	        	}
	        }
		});
		
	}
});


