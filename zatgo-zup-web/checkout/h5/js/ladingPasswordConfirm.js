$(function(){
    var check_pass_word='';
    var passwords = $('#password').get(0);
    var inputLI = $('.pass_input .pass');
    var pass_Arr = [];

    /* 获取当前url */
    var nowUrl = window.location.href;
    var domain = window.location.origin;

    var token = window.localStorage.getItem("token");
    var urlData = JSON.parse(window.localStorage.getItem("urlData"));
    // var payStatusObj = JSON.parse(window.localStorage.getItem("payStatusObj"));
    console.log(urlData)
    var coinArr = JSON.parse(window.localStorage.getItem("coinArr"));
    var allData = JSON.parse(window.localStorage.getItem("allData"));

    //获取returnUrl
    // var return_url= JSON.parse(window.localStorage.getItem("returnUrl"));

    //获取prepayId
    var prepayId = window.localStorage.getItem("prepayId");

    //获取是否从第三方跳入
    var thirdReturn = window.sessionStorage.getItem("thirdReturn");

    //判断是否Safari浏览器
    // if (navigator.userAgent.indexOf("Safari") > -1 &&(!!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) ) {
    //     window.location.href = "zatgo://";
    //     return ;
    // }


    /* 如果是第三方支付返回的则直接跳转到支付结果页面 */
    if (thirdReturn==1||thirdReturn=="1"){
        window.location.href = "pay_success.html";
        // WebSocketFun();
        // $("#load").show().find(".load_text").html("支付状态获取中...");
        // $("#load_bg").show();
    }

    /* 金额显示 */
    $("#zat_amount").html(urlData.amount);
    if(urlData.coinType=="CNY"){
        $(".pay_num").html("<span>"+urlData.amount+"</span>"+urlData.blendCoinType);
    }else {
        $(".pay_num").html("<span>"+urlData.amount+"</span>"+urlData.coinType);
    }
    $(".info_address").html(urlData.receiptName);


    /* 对应图片显示 */
    for(var i=0;i<coinArr.length;i++){
        if(urlData.coinType=="CNY"){
            if(urlData.blendCoinType==coinArr[i].coinType){
                $(".pay_amount_left  img").attr("src",urlData.blendCoinUrl)
            }
        }else if(urlData.coinType==coinArr[i].coinType){
            $(".pay_amount_left  img").attr("src",urlData.blendCoinUrl)
        }
    }

    /* 显示密码支付界面 */
    $("#pay_btn").on("touchend",function (e) {

        $(".pay_pass").show();
        e.stopPropagation();
    });

    /* 关闭密码支付界面 */
    $(".close_btn").on("click",function (e) {
        $(".pay_pass").hide();
        e.stopPropagation();
    });

    /* 数字键盘 */
    var character,index=0;
    //#keyboard li
    $('#keyboard li').on("touchstart",function(){
        if ($(this).hasClass('delete')) {
            --index;
            inputLI.eq(index%6).find(".pass_text").hide();
            pass_Arr.pop();
            if(index<=0){
                index = 0;
            }
            return false;
        }

        if ($(this).hasClass('symbol') || $(this).hasClass('tab')){
            character = $(this).text();
            pass_Arr.push(character);
            inputLI.eq(index).find(".pass_text").show();
            index++;
            if(inputLI.eq(5).find(".pass_text").is(":visible")) {
                $("#load").show().find(".load_text").html("提交中...");
                $("#load_bg").show();
                inputLI.find(".pass_text").hide();
                index=0;
                check_pass_word = pass_Arr.join("");
                pass_Arr=[];

                thirdPayFun();

            }

        }
        return false;
    });



    function thirdPayFun() {
        /* 支付接口 */
        var payData = {};
        payData.amount = urlData.amount;
        payData.cashAmount = urlData.cashAmount;
        payData.originalPrice =Number(urlData.totalMoney);

        if(urlData.coinType == "CNY"){
            payData.coinType = urlData.blendCoinType;
            payData.networkType = urlData.blendNetworkType;
        }else {
            payData.coinType = urlData.coinType;
            payData.networkType = urlData.networkType;
        }

        payData.isMultiPay = urlData.isMultiPay;

        payData.orderId = urlData.flowCode;
        payData.checkoutOrderId = urlData.orderId;


        if(prepayId!=null){
            payData.prepayId = prepayId;
        }

        payData.payPassword = check_pass_word;
        payData.receiptUserId = urlData.receiptUserId;//收款方ID
        payData.receiptUserName = urlData.receiptName;//收款方名称
        //payData.payUserName = urlData.userId;
        payData.clientType = 0;
        if (urlData.thirdPayType==0||urlData.thirdPayType=="0"){
            payData.thirdPayType = 0;
        }else if(urlData.thirdPayType==1||urlData.thirdPayType=="1"){
            payData.thirdPayType = 1;
        }else {
            payData.thirdPayType = "";
        }
        payData.notifyUrl =urlData.notifyUrl;
        /* 优惠券参数 */
        /* 优惠券参数 */
        if(urlData.userCouponsId){
            payData.userCouponsIds = [];
            payData.userCouponsIds.push(urlData.userCouponsId)
        }
        $.ajax({
            url:Interface+'pay/orderpayv2',
            type:'post',
            headers: {
                Accept: "application/json; charset=utf-8",
                token: "" + token
            },
            data:JSON.stringify(payData),
            contentType:"application/json",
            dataType:'json',
            success:function(data){
                $("#load").hide();
                $("#load_bg").hide();
                if (data.code == "0000" && data.data.resultCode=="SUCCESS"){
                    if (urlData.cashAmount>0 && data.data.wxCodeUrl){//混合支付
                        /* 标识只成功了zatgo支付 */
                        window.sessionStorage.setItem("payStatusObj","1");
                        /* 标识是由第三方支付跳转回来 */
                        window.sessionStorage.setItem("thirdReturn", "1");
                        var url = data.data.wxCodeUrl;
                        url = url+"&redirect_url="+encodeURI(domain+"/checkout/h5/pay_success.html");
                        // if(checkIos()){ //IOS
                        //     url = url+"&redirect_url="+encodeURI(nowUrl);
                        // }
                        window.location.href = url;
                        setTimeout(function () {
                            window.location.reload();
                        },2000)
                    }else {
                        window.location.href = "pay_success.html";
                    }
                }else {
                    var resultInfo;
                    if(data.data&&data.data.resultInfo){
                        if (data.data.resultInfo==="pay password error"){
                            resultInfo = "支付密码错误"
                        }else if(data.data.resultInfo==="System error，Please try again later"){
                            resultInfo = "系统错误，请再次尝试";
                        }else if(data.data.resultInfo==="balance not enough"){
                            resultInfo = "账户余额不足";
                        }else {
                            //resultInfo = data.data.resultInfo;
                            resultInfo = "支付失败，请再次尝试";
                        }
                    }
                    else {
                        if (data.code==='1009'){
                            resultInfo = "支付密码错误"
                        }else if(data.code==="1010"){
                            resultInfo = "账户余额不足";
                        }else {
                            //resultInfo = data.data.resultInfo;
                            resultInfo = "支付失败，请再次尝试";
                        }
                    }
                    $(".pass_error").show().find(".err_text").html(resultInfo);
                }
            },
            error:function(err){
                $("#load").hide();
                $("#load_bg").hide();
                //$(".pass_error").show().find(".err_text").html("pay failed");
                var errData = "支付失败，请再次尝试";
                $(".pass_error").show().find(".err_text").html(errData);
            }
        });
    }



    //检查设备
    function checkIos() {
        var u = navigator.userAgent;
        var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
        var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
        if (isiOS){
            return true;
        }else {
            return false;
        }
    }


    /* zatGo再次支付 */
    $("#err_again").on("click",function (e) {
        $(".pass_error").hide();
        $("#password input.pass").val("");
        $("#keyBg").hide();
        $("#keyboardDIV").animate({
            opacity:"1"
        },500);
        e.stopPropagation();
    });
    /* 放弃支付 */
    $("#err_forget").on("click",function () {
        if(urlData.returnUrl){

            if(urlData.returnUrl.indexOf("?")!=-1){
                window.location.href = urlData.returnUrl+"&resultCode=FAIL";
            }else {
                window.location.href = urlData.returnUrl+"?resultCode=FAIL";
            }
        }else {
            if(checkIos()){
                window.postMessage("closeWeb");
            }else {
                window.zatgo.closePayPage();
            }
        }
    })



});