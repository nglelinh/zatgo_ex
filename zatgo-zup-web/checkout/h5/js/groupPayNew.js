$(function () {
    //清除
    window.sessionStorage.removeItem("thirdReturn");

    /* 数字键盘 */
    var passwords = $('#password').get(0);
    var pass_Arr = [];

    /* 获取当前url */
    var nowUrl = window.location.href;
    var domain = window.location.origin;
    /* 获取当前时间*/
    var nowTime = new Date().getTime();

    //记录当前页面
    window.localStorage.setItem("currentRouter",JSON.stringify({name:'groupPay',time:nowTime}));

    /* 获取数据 */
    var urlData = JSON.parse(window.localStorage.getItem("urlData"))?JSON.parse(window.localStorage.getItem("urlData")):{};
    //获取token
    var token = window.localStorage.getItem("token");

    //获取第三方接口数据
    var allData = null;

    // //获取returnUrl
    // var return_url= JSON.parse(window.localStorage.getItem("returnUrl"));

    //获取prepayId
    var prepayId = window.localStorage.getItem("prepayId");

    //获取thirdReturn
    var thirdReturn = window.sessionStorage.getItem("thirdReturn");


    var payWayNum = 0;//默认是支付宝 0微信 1支付宝

    var couponStatus = null;//优惠券状态
    var orderStatus = null;//币支付状态
    var thirdPayStatus = null;//第三方支付状态

    var zatGetAmount = null;//接口获取到的最大的zatGo的数量
    var rmbGetAmount = null;//接口获取到的人名币的数量

    var alreadyPayZatAmount = null;
    var alreadyPayRmbAmount = null;

    var coinArr;
    var payCoinObj = {};
    var popupId = '';//弹窗id
    var couponList = [];/* 优惠券列表 */
    var couponObj={};//选中的优惠券参数

    /* 支付方式 */
    var payListData = [
        {
            imgUrl:"images/weixin.png",
            name:"微信",
            instructions:"",
            payway:0
        }
        // {
        //     imgUrl:"images/ali_logo.png",
        //     name:"支付宝",
        //     instructions:"",
        //     payway:1
        // }
    ];


    /* 支持支付币的数组 */
    var payCoinListData = [];

    //判断是否由第三方支付返回
    if(thirdReturn=="1"||thirdReturn==1){
        window.location.href = "pay_success.html";
    }

    /* 获取所有支持支付的币种 */
    // getSupportPayCoin();
    //获取所有数据
    getAllData();


    /* 添加输入框光标 */
    function addCursor() {
        var border = false;
        var setTime = setInterval(function () {
            border = !border;
            if(border){
                $(" .coin_pop .coin_input .span_num").css("border-right","1px solid #fff")
            }else {
                $(" .coin_pop .coin_input .span_num").css("border-right","1px solid #4DCC7B")
            }
        },1000);
        return setTime;
    }
    addCursor();

    /* 关闭弹窗点击事件 */
    $("#popup_bg").on("click",function () {
        var _this = this;
        $("#"+popupId).animate({
            bottom:"-400px"
        },500,function () {
            $(this).hide();
            $(_this).hide();
            $("#coin_pop .input_err").html("");
        });
        popupId = '';
    });
    /* 唤起优惠券弹窗 */
    $(".coupon_div").on("click",function () {
        if(!(couponStatus==0||couponStatus==null||couponStatus==2)||!(orderStatus==0||orderStatus==2)||couponList.length==0){
            /* 已经支付的情况下 不能选择 或者 在没有优惠券的情况下也不让弹窗 */
            return;
        }
        popupId = 'coupon_pop';
        $("#popup_bg").show();
        $("#coupon_pop").show().animate({
            bottom:"0px"
        },500);

    })
    /* 优惠券列表选择 点击事件 */
    $(document).on("touchend","#coupon_pop .list_cont ",function (e) {

        if($(this).hasClass('active')){
            $(this).removeClass('active').siblings('.list_cont').removeClass('active');
            zatGetAmount = payCoinObj.token;
            rmbGetAmount = payCoinObj.cashMoney;
            urlData.blendCoinType = payCoinObj.secondaryCoinType;
            urlData.userCouponsId = null;
            couponObj = {};
            $(".couponD_right span").html(couponList.length+'张可用').attr("data-cloudid",'');
            CalculationFun(zatGetAmount);
        }else {
            var couponsid = $(this).attr("data-couponsid");
            $(this).addClass('active').siblings('.list_cont').removeClass('active');
            zatGetAmount = payCoinObj.token;
            rmbGetAmount = payCoinObj.cashMoney;
            urlData.blendCoinType = payCoinObj.secondaryCoinType;
            urlData.userCouponsId = $(this).attr("data-couponsid");
            for(var i=0;i<couponList.length;i++){
                if(couponsid===couponList[i].userCouponsId){
                    couponObj = couponList[i];

                    break;
                }
            }
            if(couponObj.discountType==1){//折扣优惠券
                $(".couponD_right span").html(""+couponObj.couponsPrice+" 折").attr("data-cloudid",couponsid);
                couponObj.couponsDiscountPrice = bigNumberFun(urlData.totalMoney)*(1-bigNumberFun(couponObj.couponsPrice)/10);
            }else {
                $(".couponD_right span").html("- ￥"+couponObj.couponsPrice).attr("data-cloudid",couponsid);
            }
            recalculationFun();
        }

        $("#coupon_pop").animate({
            bottom:"-400px"
        },500,function () {
            $("#coupon_pop,#popup_bg").hide();
        });
        e.stopPropagation();

    });

    /* 第三方支付选择界面 */
    // $(".change_pay").on("click",function () {
    //     var html = "";
    //     for (var i=0;i<payListData.length;i++){
    //         html += '<div class="list payList">\n' +
    //             '          <div class="logo"><img src="'+payListData[i].imgUrl+'" width="100%" alt=""></div>\n' +
    //             '          <div class="name">'+payListData[i].name+'</div>\n' +
    //             '          <div class="selected"><img src="images/icon1.png" width="30%" alt=""></div>\n' +
    //             '   </div>';
    //     }
    //
    //     $(".change_list").show().find(".change_cont").animate({
    //         bottom:"0px"
    //     },400).find(".coin_list").html(html);
    // });

    /* 唤起币支付选择弹框 */
    $(".zat_pay").on("click",function () {
        if(!(couponStatus==0||couponStatus==null||couponStatus==2)||!(orderStatus==0||orderStatus==2)){
            /* 已经支付的情况下 不能选择 */
            return;
        }
        popupId = 'coin_pop';
        $("#popup_bg").show();
        $("#coin_pop").show().animate({
            bottom:"0px"
        },400);
        // listShowFun();
    })

    /*支付方式选择 */
    $(document).on("click",".payList",function (e) {
        var index = $(this).index();
        payWayNum = payListData[index].payway;
        $(this).addClass("active").siblings(".list").removeClass("active");
        setTimeout(function () {
            /* 显示选中的支付方式 */
            $("#third_pay").find(".logo img").attr("src",payListData[index].imgUrl);
            $("#third_pay").find(".pay_name").html(payListData[index].name);

            showPayStatus(orderStatus,thirdPayStatus);
        },100);
        e.stopPropagation();
    });
    /*币种支付方式选择 */
    $(document).on("touchend","#coin_pop .list_cont",function (e) {
        var index = $(this).index();
        $(this).addClass("active").siblings(".list_cont").removeClass("active");
        setTimeout(function () {
            // $("#maxCoinNum").html(payCoinListData[index].token);
            $("#coin_pop  .coin_logo img").attr("src",payCoinListData[index].imgUrl)
            /* 显示选中的支付方式 */
            $(".zat_pay").find(".pay_name_span").html(payCoinListData[index].secondaryCoinType);
        },100);
        payCoinObj = payCoinListData[index];
        zatGetAmount = payCoinListData[index].token;
        rmbGetAmount = payCoinListData[index].cashMoney;
        urlData.blendCoinType = payCoinListData[index].secondaryCoinType;
        urlData.blendCoinUrl = payCoinListData[index].imgUrl;
        $('#maxCoinNum').html(zatGetAmount);
        if(couponObj.couponsPrice){ //如果选择了优惠券就先计算优惠券
            recalculationFun();//重新计算优惠券
        }else {
            CalculationFun(zatGetAmount);
        }
        $("#coin_pop .coin_tab .tab").eq(1).trigger("click")
        /* 从新渲染人名币和币种的数量 */
        // $(".zat_pay .pay_number").html(zatGetAmount);
        // $("#third_pay .pay_number").html("￥"+rmbGetAmount);
        e.stopPropagation();
    });

    $(".change_list").on("click",function () {
        var _this = this;
        $(".change_list .change_cont").animate({
            bottom:"-200px"
        },400,function () {
            $(_this).hide();
        });
    });

    /*  coin选择 tab切换 */
    $("#coin_pop .coin_tab .tab").on("click",function (e) {
        e.stopPropagation();
        var _this = this;
        var index = $(this).index();
        $(this).addClass("active").siblings(".tab").removeClass('active');
        $("#coin_pop .input_err").html("");
        $("#coin_pop .coin_main").hide().eq(index).show();
    });
    /* 完成按钮 */
    $("#coin_pop .input_btn").on("click",function () {
        /*判断是否超过最大值 */
        var coinInputNumber = Number(pass_Arr.join(''));
        if(coinInputNumber>zatGetAmount){
            $("#coin_pop .input_err").html("(输入的值不能大于最大额度)").css("font-size","0.4rem")
            return;
        }
        $("#coin_pop").animate({
            bottom:"-400px"
        },500,function () {
            $(this).hide();
            $("#popup_bg").hide();
            $("#coin_pop .number").val('');
            $("#coin_pop .input_err").html("");
            $(".zat_pay .pay_number").attr('data-coinNumber',Number(pass_Arr.join(''))).html('-'+Number(pass_Arr.join('')));
            pass_Arr=[];//清空数组
        });
        CalculationFun(Number(pass_Arr.join('')));
    });
    /* 数字键盘 */
    var character,keyIndex=0;
    //#keyboard li
    $('#keyboard li').on("touchstart",function(e){
        e.stopPropagation();
        if ($(this).hasClass('delete')) {
            --keyIndex;
            pass_Arr.pop();
            // $("#coin_pop input.number").val(pass_Arr.join(''))
            $("#coin_pop span.span_num").html(pass_Arr.join(''))
            if(keyIndex<=0){
                keyIndex = 0;
            }
            return false;
        }

        if ($(this).hasClass('symbol') || $(this).hasClass('tab')){
            character = $(this).text();
            if(character==='.'&&pass_Arr.length===0){
                return false;
            }
            if(character==='.'&&pass_Arr.indexOf('.')!==-1){
                return false;
            }
            if(pass_Arr.indexOf('.')!=-1&&(pass_Arr.length-pass_Arr.indexOf('.'))>6){ //保留小数点6位
                return false;
            }
            pass_Arr.push(character);
            keyIndex++;
            $("#coin_pop span.span_num").html(pass_Arr.join(''))
        }
        return false;
    });

    /* 支付按钮 */
    $("#payment_btn").on("click",function () {
        urlData.thirdPayType = payWayNum;
        /* 还得加上金额  接口获取 */
        window.localStorage.setItem("urlData",JSON.stringify(urlData));
        if(orderStatus==1 && thirdPayStatus!=1){  //币已经支付，但是第三方支付未成功
            if (urlData.cashAmount>0){//金额大于0
                //直接调支付接口
                thirdPayFun();
            }else {
                if(urlData.returnUrl){
                    if(prepayId==null||prepayId=="null"){
                        window.location.href = urlData.returnUrl;
                    }else {
                        if(urlData.returnUrl.indexOf("?")!=-1){
                            window.location.href = urlData.returnUrl+"&resultCode=FAIL";
                        }else {
                            window.location.href = urlData.returnUrl+"?resultCode=FAIL";
                        }
                    }
                }else {
                    if(checkIos()){
                        window.postMessage("closeWeb");
                    }else {
                        window.zatgo.closePayPage();
                    }
                }

            }
        }else if(orderStatus!=1 && thirdPayStatus!=1){ //币和第三方都没支付
            if(urlData.amount===0){ //支付的zatgo数量为0的情况下
                //直接调支付接口
                thirdPayFun();
            }else {
                // /* 记录是由其他页面进入 */
                window.sessionStorage.setItem("thirdReturn","0");
                window.location.href = "pay.html";
            }
        }else {
            if(urlData.returnUrl){
                if(prepayId==null||prepayId=="null"){
                    window.location.href = urlData.returnUrl;
                }else {
                    if(urlData.returnUrl.indexOf("?")!=-1){
                        window.location.href = urlData.returnUrl+"&resultCode=SUCCESS";
                    }else {
                        window.location.href = urlData.returnUrl+"?resultCode=SUCCESS";
                    }
                }
            }else {
                if(checkIos()){
                    window.postMessage("closeWeb");
                }else {
                    window.zatgo.closePayPage();
                }
            }
        }
    });

    /* 失败尝试按钮 */
    $("#err_again1").on("click",function () {
        window.location.reload();
    });
    /* 失败后返回按钮 */
    $(".err_forget").on("click",function () {
        if(urlData.returnUrl){
            if(prepayId==null||prepayId=="null"){
                window.location.href = urlData.returnUrl;
            }else {
                if(urlData.returnUrl.indexOf("?")!=-1){
                    window.location.href = urlData.returnUrl+"&resultCode=FAIL";
                }else {
                    window.location.href = urlData.returnUrl+"?resultCode=FAIL";
                }

            }
        }else {
            if(checkIos()){
                window.postMessage("closeWeb");
            }else {
                window.zatgo.closePayPage();
            }
        }
    });

    /* 返回上一级按钮 */
    $("#back").on("click",function () {
        if(urlData.returnUrl){
            if(prepayId==null||prepayId=="null"){
                window.location.href = urlData.returnUrl;
            }else {
                if(urlData.returnUrl.indexOf("?")!=-1){
                    window.location.href = urlData.returnUrl+"&resultCode=FAIL";
                }else {
                    window.location.href = urlData.returnUrl+"?resultCode=FAIL";
                }
            }
        }else {
            if(checkIos()){
                window.postMessage("closeWeb");
            }else {
                window.zatgo.closePayPage();
            }
        }
    });


    //检查设备
    function checkIos() {
        var u = navigator.userAgent;
        var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
        var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
        if (isiOS){
            return true;
        }else {
            return false;
        }
    }

    /* 支付接口 */
    function thirdPayFun() {
        /* 支付接口 */
        var payData = {};
        payData.amount = Number(urlData.amount);

        payData.cashAmount = urlData.cashAmount?Number(urlData.cashAmount):"";
        payData.originalPrice =Number(urlData.totalMoney);

        if(urlData.coinType == "CNY"){
            payData.coinType = urlData.blendCoinType;
            payData.networkType = urlData.blendNetworkType;
        }else {
            payData.coinType = urlData.coinType;
            payData.networkType = urlData.networkType;
        }

        payData.isMultiPay = urlData.isMultiPay;
        payData.orderId = urlData.flowCode;
        payData.checkoutOrderId = urlData.orderId;
        payData.payPassword = "";
        payData.receiptUserId = urlData.receiptUserId;
        // payData.payUserName = urlData.userId;
        payData.clientType = 0;
        /* 优惠券参数 */
        if(urlData.userCouponsId){
            payData.userCouponsIds = [];
            payData.userCouponsIds.push(urlData.userCouponsId)
        }

        if(prepayId!=null){
            payData.prepayId = prepayId;
        }

        if (urlData.thirdPayType==0||urlData.thirdPayType=="0"){
            payData.thirdPayType = 0;//微信支付
        }else if(urlData.thirdPayType==1||urlData.thirdPayType=="1"){
            payData.thirdPayType = 1;//支付宝支付
        }else {
            payData.thirdPayType = "";
        }
        payData.notifyUrl =urlData.notifyUrl;
        $.ajax({
            url:Interface+'pay/orderpayv2',
            type:'post',
            headers: {
                Accept: "application/json; charset=utf-8",
                token: "" + token
            },
            data:JSON.stringify(payData),
            contentType:"application/json",
            dataType:'json',
            success:function(data){
                //window.location.href = "pay_success.html";
                $("#load").hide();
                if (data.code == "0000" && data.data.resultCode=="SUCCESS"){
                    if (urlData.cashAmount>0 && data.data.wxCodeUrl) {//混合支付
                        window.sessionStorage.setItem("payStatusObj","1");
                        /* 标识是由第三方支付跳转回来 */
                        window.sessionStorage.setItem("thirdReturn", "1");
                        var url = data.data.wxCodeUrl;
                        url = url+"&redirect_url="+encodeURI(domain+"/checkout/h5/pay_success.html");
                        //  if(checkIos()){ //IOS
                        //      url = url+"&redirect_url="+encodeURI(nowUrl);
                        //  }
                        window.location.href = url;
                        setTimeout(function () {
                            window.location.reload();
                        },2000)
                    }else {
                        window.location.href = "pay_success.html";
                    }
                }else {
                    //var resultInfo = data.data.resultInfo?data.data.resultInfo:"支付失败，请再次尝试";
                    var resultInfo = "支付失败，请再次尝试";
                    if(data.data&&data.data.resultInfo){
                        if (data.data.resultInfo==="pay password error"){
                            resultInfo = "支付密码错误"
                        }else if(data.data.resultInfo==="System error，Please try again later"){
                            resultInfo = "系统错误，请再次尝试";
                        }else if(data.data.resultInfo==="balance not enough"){
                            resultInfo = "账户余额不足";
                        }else {
                            //resultInfo = data.data.resultInfo;
                            resultInfo = "支付失败，请再次尝试";
                        }
                    }
                    else {
                        resultInfo = "支付失败，请再次尝试";
                    }
                    $(".pass_error").show().find(".err_text").html(resultInfo);
                }
            },
            error:function(err){
                $("#load").hide();
                $(".pass_error").show().find(".err_text").html("pay failed");
            }
        });
    }

    /* 获取优惠券接口 */
    function getCouponListFun() {
        var data = {
            'prePayId':prepayId
        }
        $.ajax({
            url:Interface+"coupons/user/getUserCouponsByOrder",
            headers: {
                Accept: "application/json; charset=utf-8",
                token: "" + token
            },
            data:JSON.stringify(data),
            type:'POST',
            contentType: 'application/json',
            success:function (data) {
                /* 渲染优惠券列表 */
                if(data.code==='0000'){
                    var list = data.data,
                        html='';
                    for(var i=0;i<list.length;i++){
                        html+= '<div class="list_cont" data-couponsid="'+list[i].userCouponsId+'">'+
                            '<div class="list_left">';
                            if(list[i].discountType==0){
                                html+= '<div class="coupon_number">￥<span style="font-size: 1.4rem">'+list[i].couponsPrice+'</span></div>';
                            }else if(list[i].discountType==1) {
                                html+= '<div class="coupon_number"><span style="font-size: 1.4rem">'+list[i].couponsPrice+'</span> 折</div>';
                            }

                            if(list[i].howManyFull&&list[i].howManyFull>0){
                                html+='<div class="coupon_man">满'+list[i].howManyFull+'可用</div>';
                            }else {
                                html+='<div class="coupon_man"></div>';
                            }
                        html+= '</div>'+
                            '<div class="list_center">'+
                            '<div class="coupon_name">'+list[i].couponsName+'</div>'+
                            '<div class="coupon_time">'+new Date(list[i].validStartDate).Format("yy-MM-dd")+' - '+new Date(list[i].validEndDate).Format("yy-MM-dd")+'</div>'+
                            '<div class="coupon_limit"></div>'+
                            ' </div>'+
                            ' <div class="list_right">✔</div>'+
                            ' </div>';
                    }
                    couponList = list;
                    if(!(couponStatus==0||couponStatus==null)){

                    }else {
                        $(".couponD_right span").html(list.length+'张可用');
                        if(list.length===0){
                            $(".couponD_right em").remove();
                        }
                    }
                    $("#coupon_pop .coupon_list").html(html);
                }
            }
        })
    }

    /* 获取所有接口数据 */
    function getAllData() {
        //少一点的数据
        $.ajax({
            url:Interface+"pay/orders/prepay/"+prepayId,
            type:"GET",
            headers:{
                token:''+token,
                Accept: "application/json; charset=utf-8"
            },
            contentType:"application/json",
            dataType:'json',
            success:function (data) {
                $("#load").hide();
                $("#load_bg").hide();

                if(data.code == "0000" && data.data ){
                    var allDataObj = data.data;
                    allData = data.data;

                    couponStatus = allDataObj.order.usageCouponsStatus;//优惠券支付状态
                    orderStatus = allDataObj.order.orderStatus;//币支付状态
                    thirdPayStatus = allDataObj.order.thirdPayStatus;//第三方支付状态
                    /* 已经支付的金额 */
                    alreadyPayZatAmount = allDataObj.order.orderMoney;
                    alreadyPayRmbAmount =  allDataObj.order.cashAmount;


                    zatGetAmount = allDataObj.payMoney.token;//接口获取到的最大的zatGo的数量
                    rmbGetAmount = allDataObj.payMoney.cashMoney;//接口获取到的人名币的数量


                    urlData.amount = allDataObj.payMoney.token;//接口获取到的最大的zatGo的数量
                    urlData.cashAmount = allDataObj.payMoney.cashMoney;//接口获取到的人名币的数量
                    urlData.orderId = allDataObj.order.orderId;// zatGo订单号
                    urlData.flowCode = allDataObj.order.orderFlowCode;// 第三方的订单号
                    urlData.receiptUserId = allDataObj.order.receiptUserId;//收款方Id
                    urlData.receiptName = allDataObj.order.receiptUserName;//收款人姓名

                    urlData.coinType = allDataObj.order.coinType;//币种 人名币
                    urlData.networkType = allDataObj.order.coinNetworkType;//网络类型 人民币

                    urlData.blendCoinType = allDataObj.order.mixCoinType;//混合支付的另一个币种类型
                    urlData.blendNetworkType = allDataObj.order.mixCoinNetworkType;//混合支付的另一个网络类型

                    // urlData.returnUrl = getQueryString("returnUrl");//支付成功后返回的地址
                    urlData.notifyUrl = allDataObj.order.notifyUrl;//回调地址.

                    urlData.totalMoney = allDataObj.order.originalPrice;//总金额

                    urlData.couponsList = allDataObj.order.couponsList;//优惠券信息

                    window.localStorage.setItem("allData",JSON.stringify(data.data));

                    $('.order_info .order_amount').html("￥ "+allDataObj.order.originalPrice);
                    // $('.order_info .order_name').html(allDataObj.order.orderName);

                    //获取优惠券列表
                    getCouponListFun();
                    /* 获取所有币种列表 */
                    getSupportCoin();
                    //显示支付状态
                    showPayStatus(orderStatus,thirdPayStatus);

                    /*  页面渲染 */
                    listShowFun();
                }else {
                    $(".group_list").show();
                    $("#pass_error1").show().find(".err_text").html("获取数据失败！");
                }
            },
            error:function () {
                $(".group_list").show();
            }
        })
    }

    /* 获取所有的币种 */
    function getSupportCoin() {
        $.ajax({
            url:Interface+"wallet/system/cointypes",
            headers: {
                Accept: "application/json; charset=utf-8",
                token: "" + token
            },
            type:"GET",
            contentType: 'application/json',
            success:function (data) {
                coinArr = data.data;
                window.localStorage.setItem("coinArr",JSON.stringify(data.data));
                // for(var i=0;i<coinArr.length;i++){
                //     if(urlData.coinType=="CNY"  ){
                //         if(urlData.blendCoinType==coinArr[i].coinType){
                //             $(".zat_pay .logo img").attr("src","images/coinImg/"+urlData.blendCoinType+".png")
                //         }
                //     }else if(urlData.coinType==coinArr[i].coinType){
                //         $(".zat_pay .logo img").attr("src","images/coinImg/"+urlData.coinType+".png")
                //     }
                // }
                getSupportPayCoin();

            },
            error:function (err) {
                console.log(err);
            }

        })
    }

    /* 获取支持支付的币种 */
    function getSupportPayCoin() {
        $.ajax({
            url:Interface+"pay/coin/list?prepayId="+prepayId,
            headers: {
                Accept: "application/json; charset=utf-8",
                token: "" + token
            },
            type:"GET",
            contentType: 'application/json',
            success:function (data) {
                payCoinListData = data.data;
                // for(var i=0;i<payCoinListData.length;i++){
                //     payCoinListData[i].imgUrl = 'images/coinImg/'+payCoinListData[i].secondaryCoinType+'.png';
                // }
                var html='';
                for(var i=0;i<payCoinListData.length;i++){
                    for(var j=0;j<coinArr.length;j++){
                        if(payCoinListData[i].secondaryCoinType===coinArr[j].coinShortName.toUpperCase()){
                            payCoinListData[i].imgUrl = coinArr[j].coinImage;
                            break;
                        }
                    }
                    if(payCoinListData[i].secondaryCoinType===urlData.blendCoinType){
                        payCoinObj = payCoinListData[i]; //保存默认支付币的信息
                        html += '<div class="list_cont active">';
                    }else {
                        html += '<div class="list_cont">';
                    }
                    html +=  '<div class="list_left">'+
                        '<div class="logo"><img src="'+payCoinListData[i].imgUrl+'" width="100%" alt=""></div>'+
                        '<div class="coin_name">'+payCoinListData[i].secondaryCoinType+'</div>'+
                        '</div>'+
                        '<div class="list_right">✔</div>'+
                        '</div>';
                }
                $("#coin_pop").find(".coin_list").html(html)
            },
            error:function (err) {
                console.log(err);
            }
        })
    }


    function listShowFun() {
        if(urlData.coinType=="CNY"){

            urlData.isMultiPay = 1;

            $(".group_list").show();
            $("#payment_btn").show();
            /* 显示币种名称 */
            $(".pay_name_span").html(urlData.blendCoinType);
            // $(".drag_title").html("设置"+urlData.blendCoinType+"支付数量");

            /* 显示已经使用的优惠券 */
            if(!(couponStatus==0||couponStatus==null||couponStatus==2)&&urlData.couponsList){
                var couponPayList = allData.order.userCouponsList;
                console.log(couponPayList)
                var listItem = couponPayList[0];//
                urlData.userCouponsId = listItem.userCouponsId;
                if(listItem.discountType==1){//折扣券
                    $(".couponD_right span").html(""+listItem.couponsPrice+" 折").attr("data-cloudid",urlData.userCouponsId);
                }else {//满减券
                    $(".couponD_right span").html("- ￥"+listItem.couponsPrice).attr("data-cloudid",urlData.userCouponsId);
                }
                $(".couponD_right em").remove();
                $(".zat_pay .pay_cont em").remove();
            }


            /* 显示金额 */
            $('#maxCoinNum').html(urlData.amount);
            $(".zat_pay .pay_number").html(urlData.amount);
            // if(couponObj.couponsPrice){
            //     if(couponObj.discountType==1){
            //         $("#third_pay .pay_number").html("￥"+(bigNumberFun(rmbGetAmount)-bigNumberFun(couponObj.couponsDiscountPrice)));
            //     }else {
            //         $("#third_pay .pay_number").html("￥"+(bigNumberFun(rmbGetAmount)-bigNumberFun(couponObj.couponsPrice)));
            //     }
            //
            // }else {
            //
            // }
            $("#third_pay .pay_number").html("￥"+urlData.cashAmount);


            if(zatGetAmount.toString().length>10 && zatGetAmount.toString().length<=15){
                $(".zat_pay .pay_number").css("font-size","0.8rem")
            }else if(zatGetAmount.toString().length>15){
                $(".zat_pay .pay_number").css("font-size","0.6rem")
            }

            /* 显示问题 */
            if(zatGetAmount>0&&rmbGetAmount>0){ //两者都大于0的情况下才显示全部
                $("#pay_CNY,.zat_pay").show();
                // if(orderStatus==0 && thirdPayStatus==0){ //如果两者都未支付 显示拖动比例条
                //     $("#drag_set").show();
                // }
            }
            else if (zatGetAmount>0 && rmbGetAmount==0){ //全部用币支付  可以有拖动条
                // $(".zat_pay").show();
                $("#pay_CNY,.zat_pay").show();
                // if(orderStatus==0 && thirdPayStatus==0){ // 显示拖动比例条
                //     $("#drag_set").show();
                // }
            }
            else if(zatGetAmount==0 && rmbGetAmount>0){ //全部用人名币支付 可以有拖动条
                // $(".pay_other").show();
                $("#pay_CNY,.zat_pay").show();
                // if(orderStatus==0 && thirdPayStatus==0){ // 显示拖动比例条
                //     $("#drag_set").show();
                // }
            }else if(zatGetAmount==-1 && rmbGetAmount>0){//全部用人名币支付 没有拖动条
                $("#pay_CNY").show();
            }
            /* 如果返回的金额不为空 则显示他返回的金额 */
            if(alreadyPayZatAmount!=null&&!(orderStatus==0||orderStatus==2)){
                $(".zat_pay .pay_number").html(alreadyPayZatAmount);
            }
            if(alreadyPayRmbAmount!=null&&!(thirdPayStatus==0||thirdPayStatus==2)){
                $("#pay_CNY .pay_number").html("￥"+alreadyPayRmbAmount);
            }

            /* 金额问题 */
            if((orderStatus==0||orderStatus==2) && (thirdPayStatus==0||thirdPayStatus==2) ){ //未支付的情况下

            }else {
                urlData.amount = alreadyPayZatAmount;
                urlData.cashAmount = alreadyPayRmbAmount;
            }
        }
        else { //全部用币支付的时候
            urlData.isMultiPay = 0; //全部用币支付的时候 不为混合支付

            $(".group_list").show();
            $("#payment_btn").show();
            //这是全部用币支付
            $(".zat_pay").show();
            /* 显示金额 */
            $(".zat_pay .pay_number").html(urlData.totalMoney);

            /* 显示币种名称 */
            $(".pay_name_span").html(urlData.coinType);


            /* 金额 */
            urlData.amount = urlData.totalMoney;
            urlData.cashAmount = 0;
        }
        $(".group_list").show();
    }


    // 支付状态显示
    function showPayStatus(orderStatus,thirdPayStatus) {

        var payHtml = "<span style='font-size: 0.5rem;color: #12A0D2 '>（已支付）</span>";
        var nopayHtml = "<span style='font-size: 0.5rem;color: #E26A6A '>（未支付）</span>";
        var pay = "（已支付）";
        var nopay = "（未支付）";
        if(orderStatus==1||orderStatus=="1"){
            $(".zat_pay .pay_status").html(pay).css("color","#12A0D2");
            $(".zat_pay .pay_cont em").remove();
        }else {
            $(".zat_pay .pay_status").html(nopay).css("color","#E26A6A");
        }
        if(thirdPayStatus==1||thirdPayStatus=="1"){
            $("#third_pay .pay_name").append(payHtml);
        }else{
            $("#third_pay .pay_name").append(nopayHtml);
        }
        /* 优惠券 */
        if(couponStatus==null||couponStatus==0){//未使用
            if(couponList.length>0){
                $(".couponD_name span").html('（未抵扣）').css("color","#E26A6A");
            }
        }else {
            $(".couponD_name span").html('（已抵扣）').css("color","#12A0D2");
        }
        if(orderStatus==1&&thirdPayStatus==1){
            $("#payment_btn").html("订单已支付");
        }

    }

    /* 选择优惠券后 计算币的最大支付比例 */
    function recalculationFun() {
        var coupPrice;
        if(couponObj.discountType===1){
            coupPrice = couponObj.couponsDiscountPrice;
        }else {
            coupPrice = couponObj.couponsPrice;
        }
        /* 当选择优惠券后，如果要支付的金额小于要优惠的金额时，减少她要支付的币的数量*/
        if(rmbGetAmount>=coupPrice){  //要支付的金额大于优惠券的金额 无需修改
            //修改最小的人名币的支付数量
            rmbGetAmount = Math.ceil((bigNumberFun(rmbGetAmount)-bigNumberFun(coupPrice))*100)/100;//选择比例后 需要支付的人名币的数量 （被优惠券抵消）;
            urlData.amount = Number(zatGetAmount.toFixed(7));//选择优惠券后 需要支付的币的数量
            urlData.cashAmount = rmbGetAmount;//选择比例后 需要支付的人名币的数量 （被优惠券抵消）
            listShowFun();
            return;
        }else {
            //未输入币的情况下  币支付所对应的人民币数量
            var zat_RMB_Num = bigNumberFun(urlData.totalMoney)-bigNumberFun(rmbGetAmount);

            //计算 人名币和币的支付比例  （币的价格）
            //var coin_rmb_Proportion = bigNumberFun(zatGetAmount)/bigNumberFun(zat_RMB_Num);
            //计算 优惠券的差额（最小支付人名币的数量 和 优惠券的 差额）
            //var needRmbNum =  bigNumberFun(couponObj.couponsPrice)-bigNumberFun(rmbGetAmount);

            //差额换算成币的数量
            var needCoinNum = (bigNumberFun(coupPrice)-bigNumberFun(rmbGetAmount))/(bigNumberFun(zat_RMB_Num)/bigNumberFun(zatGetAmount));
            //修改最大的币的支付数量
            zatGetAmount =  zatGetAmount-needCoinNum>=0?zatGetAmount-needCoinNum:0;
            //修改最小的人名币的支付数量
            rmbGetAmount = Math.ceil((bigNumberFun(rmbGetAmount)-bigNumberFun(coupPrice))*100)/100>=0?Math.ceil((bigNumberFun(rmbGetAmount)-bigNumberFun(coupPrice))*100)/100:0;
            urlData.amount = Number(zatGetAmount.toFixed(7));//选择优惠券后 需要支付的币的数量
            urlData.cashAmount = Number(rmbGetAmount);//选择比例后 需要支付的人名币的数量 （被优惠券抵消）
            // $("#maxCoinNum").html(zatGetAmount);

            listShowFun();

        }

    }

    /*  计算支付比例   */
    function CalculationFun(num) { //num 是输入的币的金额
        // var zat_RMB_Num = bigNumberFun(urlData.totalMoney)-bigNumberFun(rmbGetAmount);//未输入币的情况下  币支付所对应的人民币数量
        var zat_RMB_Num = 0;//未输入币的情况下  币支付所对应的人民币数量
        //选择比例后 需要支付的人名币的数量
        if(couponObj.couponsPrice){//有优惠券的情况下
            if(couponObj.discountType===1){ //折扣券
                //zat_RMB_Num=bigNumberFun(Math.ceil((bigNumberFun(rmbGetAmount)+bigNumberFun(zat_RMB_Num)*(bigNumberFun(1)-bigNumberFun(coinPayAmount)))*100)/100)-bigNumberFun(couponObj.couponsDiscountPrice);
                zat_RMB_Num=bigNumberFun(urlData.totalMoney)-bigNumberFun(couponObj.couponsDiscountPrice)-bigNumberFun(rmbGetAmount);
            }else {
                zat_RMB_Num=bigNumberFun(urlData.totalMoney)-bigNumberFun(couponObj.couponsPrice)-bigNumberFun(rmbGetAmount);
               //urlData.cashAmount=bigNumberFun(Math.ceil((bigNumberFun(rmbGetAmount)+bigNumberFun(zat_RMB_Num)*(bigNumberFun(1)-bigNumberFun(coinPayAmount)))*100)/100)-bigNumberFun(couponObj.couponsPrice);
            }
        }else {//无优惠券
            zat_RMB_Num = bigNumberFun(urlData.totalMoney)-bigNumberFun(rmbGetAmount);
        }
        var coinPayAmount =bigNumberFun(num)/bigNumberFun(zatGetAmount); //输入的需要支付的币的数量百分比
        // urlData.amount = zatGetAmount*100000000*num/10000000000;//选择比例后 需要支付的币的数量
        urlData.amount = Number(num);//选择比例后 需要支付的币的数量


        console.log(rmbGetAmount,zat_RMB_Num*(1-coinPayAmount),zat_RMB_Num,1-coinPayAmount)
        urlData.cashAmount=Math.ceil((bigNumberFun(rmbGetAmount)+bigNumberFun(zat_RMB_Num*(1-coinPayAmount)))*100)/100;
        console.log(bigNumberFun(rmbGetAmount),bigNumberFun(zat_RMB_Num)*(bigNumberFun(1)-bigNumberFun(coinPayAmount)))
        if(urlData.cashAmount>0){
            urlData.isMultiPay=1;
        }
        urlData.cashAmount = urlData.cashAmount.toFixed(2);//计算精度问题  保留两位小数点
        $(".zat_pay .pay_number").html(urlData.amount);
        $("#third_pay .pay_number").html("￥"+urlData.cashAmount);
    }


    /* js 处理高精度小数运算函数 */
    function bigNumberFun(num) {
        return new BigNumber(num).toNumber();
    }



})
