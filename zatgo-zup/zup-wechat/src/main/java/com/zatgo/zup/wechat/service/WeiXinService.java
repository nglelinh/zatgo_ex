package com.zatgo.zup.wechat.service;

import com.zatgo.zup.common.model.wx.WxUserInfoData;
import com.zatgo.zup.wechat.model.*;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author wangyucong
 * @Date 2019/2/19 13:44
 */
public interface WeiXinService {
    /**
     * 获取微信openId
     * @param wxCode
     * @return
     */
    WxAccessTokenData getOpenId(String wxCode);

    /**
     * 获取微信用户信息
     * @param openId
     */
    WxUserInfoData getUserInfo(String openId, String cloudUserId);

    /**
     * 获取微信公共access_token
     * @param appId
     * @param appSecret
     * @return
     */
//    WxCommonAccessTokenData getAccessToken(String appId, String appSecret);

    /**
     * 获取微信 jsapi_ticket
     * @param accessToken
     * @return
     */
    WxJsapiTicketData getJsApiTicket(String accessToken);

    /**
     * 注册绑定微信用户
     * @param info
     */
//    @Transactional
//    void registerWeixinAccount(WxUserInfoData info);

    /**
     * 获取微信js接口配置
     * @param url
     * @return
     */
    WxJsapiConfigData selectJsapiConfig(String url);

    WxUserInfoData getRegisterWxUserInfo(String openId, String accessToken);

    /**
     * 
     * @return
     */
//    String getWechatAccessToken();
}
