package com.zatgo.zup.wechat.controller;

import com.zatgo.zup.wechat.model.CommonResult;
import com.zatgo.zup.wechat.service.WeiXinPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "WXPayController", description = "微信支付")
@RestController
@RequestMapping(value = "/wechat/wx/pay")
public class WXPayController extends BaseController{

	@Autowired
	private WeiXinPayService weiXinPayService;

	@ApiOperation("获取订单微信支付参数")
	@GetMapping(value = "/params/{orderId}/{wxPayType}/{wxOpenId}/{userId}")
	public CommonResult getOrderPayWeixinParams(@PathVariable("orderId") Long orderId,
												@PathVariable("wxPayType") Integer wxPayType,
												@PathVariable("wxOpenId") String wxOpenId,
												@PathVariable("userId") String userId) {
		return weiXinPayService.getOrderPayWeixinParams(orderId, wxPayType, wxOpenId, userId);
	}

}
