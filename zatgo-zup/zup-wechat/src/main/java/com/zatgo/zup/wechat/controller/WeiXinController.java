package com.zatgo.zup.wechat.controller;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.wx.WechatRefundRequest;
import com.zatgo.zup.common.model.wx.WxUserInfoData;
import com.zatgo.zup.wechat.entity.WechatConstant;
import com.zatgo.zup.wechat.model.CommonResult;
import com.zatgo.zup.wechat.model.WxAccessTokenData;
import com.zatgo.zup.wechat.model.WxJsapiConfigData;
import com.zatgo.zup.wechat.service.IWechatAccessTokenService;
import com.zatgo.zup.wechat.service.WeiXinPayService;
import com.zatgo.zup.wechat.service.WeiXinService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @Author wangyucong
 * @Date 2019/4/8 13:54
 */
@RestController
@RequestMapping("/wechat")
@Api(tags = "WeiXinController", description = "微信管理")
public class WeiXinController {

    private static final Logger logger = LoggerFactory.getLogger(WeiXinController.class);

    @Resource
    private WeiXinService weiXinService;
    @Autowired
    private WeiXinPayService weiXinPayService;
    @Autowired
    private IWechatAccessTokenService iWechatAccessTokenService;

    @GetMapping("/jsapiConfig")
    @ApiOperation("获取微信接口配置")
    public Object selectJsapiConfig(@RequestParam("url") String url){
        WxJsapiConfigData data = new WxJsapiConfigData();
        try {
            data = weiXinService.selectJsapiConfig(url);
        } catch (BusinessException e) {
            return new CommonResult().failed("获取微信接口配置失败");
        }
        return new CommonResult().success(data);
    }

    @GetMapping("/getWechatOpenId")
    @ApiOperation("获取微信接口配置")
    public Object getWechatOpenId(String wxCode){
        WxAccessTokenData data = null;
        try {
            data = weiXinService.getOpenId(wxCode);
        } catch (BusinessException e) {
            return new CommonResult().failed("获取微信openId失败");
        }
        return new CommonResult().success(data);
    }

    @GetMapping("/getWechatAccessToken")
    @ApiOperation("获取微信AccessToken")
    public Object getWechatAccessToken(@RequestParam("weixinOfficialAcctAppId") String weixinOfficialAcctAppId,
                                       @RequestParam("weixinOfficialAcctKey") String weixinOfficialAcctKey){
        String wechatAccessToken =iWechatAccessTokenService.getAccessToken(weixinOfficialAcctAppId, weixinOfficialAcctKey);
        return new CommonResult().success(wechatAccessToken);
    }

    @GetMapping("/getWxUserInfo")
    @ApiOperation("获取微信用户信息")
    public Object getWxUserInfo(@RequestParam("openId") String openId, @RequestParam("cloudUserId") String cloudUserId){
        WxUserInfoData userInfo = weiXinService.getUserInfo(openId, cloudUserId);
        return new CommonResult().success(userInfo);
    }

    @GetMapping("/register/getWxUserInfo")
    @ApiOperation("获取微信用户信息")
    public Object getRegisterWxUserInfo(@RequestParam("openId") String openId, @RequestParam("accessToken") String accessToken){
        try {
            WxUserInfoData userInfo = weiXinService.getRegisterWxUserInfo(openId, accessToken);
            return new CommonResult().success(userInfo);
        } catch (BusinessException e){
            if ("48001".equals(e.getCode())){
                return new CommonResult().unauthorized(null);
            }
            return new CommonResult().failed(BusinessExceptionCode.WEIXIN_HTTP_ERROR);
        }
    }

    @PostMapping ("/weixinRefund")
    @ApiOperation("微信退款")
    public Object weixinRefund(@RequestBody WechatRefundRequest request){
        try {
            weiXinPayService.weixinRefund(request.getOrderNumber(), request.getRefundAmount());
        } catch (Exception e) {
            logger.error("", e);
            return new CommonResult().failed("微信退款失败");
        }
        return new CommonResult().success();
    }

    @GetMapping ("/getQRCode")
    @ApiOperation("获取二维码")
    public Object getQRCode(@RequestParam("cloudUserId") String cloudUserId,
                            @RequestParam("sceneStr") String sceneStr,
                            @RequestParam("userId") String userId){
        String res = weiXinPayService.getQRCode(cloudUserId, sceneStr, userId);
        return new CommonResult().success(res);
    }
}