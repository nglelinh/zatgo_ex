package com.zatgo.zup.wechat.sdk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * 微信支付配置
 * @author Administrator
 *
 */
@Component
public class WeiXinPayConfig extends WXPayConfig {

	private static final Logger logger = LoggerFactory.getLogger(WeiXinPayConfig.class);
	
	@Value("${wx.pay.appId}")
	private String wxPayAppId;
	
	@Value("${wx.pay.mchId}")
	private String wxPayMchId;
	
	@Value("${wx.pay.key}")
	private String wxPayKey;

	@Value("${wx.pay.keyFilePath}")
	private String keyFilePath;

	@Override
	String getAppID() {
		return wxPayAppId;
	}

	@Override
	String getMchID() {
		return wxPayMchId;
	}

	@Override
	String getKey() {
		return wxPayKey;
	}

	@Override
	InputStream getCertStream() {
		ByteArrayInputStream certBis = null;
		try{
			File file = new File(keyFilePath);
			InputStream inputStream = new FileInputStream(file);
			byte[] certData;
			certData = new byte[(int) file.length()];
			inputStream.read(certData);
			inputStream.close();
			certBis = new ByteArrayInputStream(certData);
		} catch (Exception e){
			logger.error("", e);
		}
		return certBis;
	}

	@Override
	IWXPayDomain getWXPayDomain() {
		IWXPayDomain iwxPayDomain = new IWXPayDomain() {
            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {

            }
            @Override
            public DomainInfo getDomain(WXPayConfig config) {
                return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API, true);
            }
        };
        return iwxPayDomain;
	}

}
