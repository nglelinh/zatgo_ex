package com.zatgo.zup.wechat.service.impl;

import com.ykb.mall.common.enumType.BusinessEnum.WechatMsgStatus;
import com.ykb.mall.common.model.WechatMessageRecord;
import com.ykb.mall.common.model.WechatMessageRecordExample;
import com.zatgo.zup.wechat.entity.MsgToken;
import com.zatgo.zup.wechat.entity.WechatMsg;
import com.zatgo.zup.wechat.mapper.WechatMessageRecordMapper;
import com.zatgo.zup.wechat.service.IWechatMsgService;
import com.zatgo.zup.wechat.service.WechatMsgReplyService;
import com.zatgo.zup.wechat.utils.HashUtil;
import com.zatgo.zup.wechat.utils.Xml2BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@SuppressWarnings("ALL")
@Service
public class WechatMsgServiceImpl implements IWechatMsgService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Value("${system.thirdAuthenticationToken}")
	private String thirdAuthenticationToken;

	@Autowired
	private WechatMsgReplyService wechatMsgReplyService;

	@Autowired
	private WechatMessageRecordMapper wechatMessageRecordMapper;

	@Override
	public MsgToken validateWxToken(MsgToken token, String cloudUserId) {

		String signature = token.getSignature();
		String timestamp = token.getTimestamp();
		String nonce = token.getNonce();
		List<String> str = Arrays.asList(thirdAuthenticationToken, timestamp, nonce);
		Collections.sort(str);
		StringBuffer data = new StringBuffer();
		for (String string : str) {
			data.append(string);
		}

		String hashcode = HashUtil.hash(data.toString(), "SHA-1");
		if (!hashcode.equals(signature)) {
			token.setEchostr("");
		}

		return token;
	}

	@Override
	public String wxMsg(WechatMsg msg) {
		try {
			String msgType = msg.getMsgType();
			if (WechatMsg.MsgType.TEXT.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.IMAGE.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.VOICE.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.VIDEO.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.SHORTVIDEO.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.LOCATION.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.LINK.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.EVENT.getType().equals(msgType)) {
				return wechatEventMsg(msg);
			}

		} catch (Exception e) {
			log.error("微信消息处理失败", e);
		}
		return "success";
	}

	private String wechatEventMsg(WechatMsg msg) {
		WechatMsg res = null;
		String event = msg.getEvent();
		WechatMsg.MsgType msgType = WechatMsg.MsgType.EVENT;
		if (WechatMsg.Event.SUBSCRIBE.getEvent().equals(event)) {

			String reply = wechatMsgReplyService.selectWechatMsgReply(msgType, WechatMsg.Event.SUBSCRIBE, event);
			if (!StringUtils.isEmpty(reply)) {
				res = new WechatMsg();
				res.setToUserName(msg.getFromUserName());
				res.setFromUserName(msg.getToUserName());
				res.setCreateTime(String.valueOf(new Date().getTime()));
				res.setMsgType(WechatMsg.MsgType.TEXT.getType());
				res.setContent(reply);
			}

		} else if (WechatMsg.Event.UNSUBSCRIBE.getEvent().equals(event)) {
			return "success";
		} else if (WechatMsg.Event.SCAN.getEvent().equals(event)) {
			return "success";
		} else if (WechatMsg.Event.LOCATION.getEvent().equals(event)) {
			return "success";
		} else if (WechatMsg.Event.CLICK.getEvent().equals(event)) {
			res = wechatClickMsg(msg);
		} else if (WechatMsg.Event.VIEW.getEvent().equals(event)) {
			return "success";
		} else if (WechatMsg.Event.TEMPLATESENDJOBFINISH.getEvent().equals(event)) {
			wechatMsgSend(msg);
		}

		String str = null;
		if (res == null) {
			str = "success";
		} else {
			str = Xml2BeanUtils.convertToXml(res, "UTF-8");
		}
		return str;
	}

	private WechatMsg wechatClickMsg(WechatMsg msg) {

		WechatMsg res = null;

		String reply = wechatMsgReplyService.selectWechatMsgReply(WechatMsg.MsgType.EVENT, WechatMsg.Event.CLICK,
				msg.getEventKey());

		if (!StringUtils.isEmpty(reply)) {
			res = new WechatMsg();
			res.setToUserName(msg.getFromUserName());
			res.setFromUserName(msg.getToUserName());
			res.setCreateTime(String.valueOf(new Date().getTime()));
			res.setMsgType(WechatMsg.MsgType.TEXT.getType());
			res.setContent(reply);
		}

		return res;
	}

	private void wechatMsgSend(WechatMsg msg) {
		String msgId = msg.getMsgId();
		if (StringUtils.isEmpty(msgId)) {
			log.error("微信消息发送事件通知msgId丢失：{}", msg);
			return;
		}

		WechatMessageRecord record = new WechatMessageRecord();
		if (WechatMsg.Status.SUCCESS.getStatus().equals(msg.getStatus())) {
			record.setStatus(WechatMsgStatus.SUCCESS.getCode());
		} else {
			record.setStatus(WechatMsgStatus.FAILED.getCode());
		}
		record.setStatusMsg(msg.getStatus());

		WechatMessageRecordExample example = new WechatMessageRecordExample();
		example.createCriteria().andMsgIdEqualTo(msgId);

		wechatMessageRecordMapper.updateByExampleSelective(record, example);
	}

}
