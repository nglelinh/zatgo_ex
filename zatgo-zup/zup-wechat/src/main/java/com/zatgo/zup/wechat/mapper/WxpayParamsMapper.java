package com.zatgo.zup.wechat.mapper;

import com.ykb.mall.common.model.WxpayParams;
import com.ykb.mall.common.model.WxpayParamsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WxpayParamsMapper {
    int countByExample(WxpayParamsExample example);

    int deleteByExample(WxpayParamsExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WxpayParams record);

    int insertSelective(WxpayParams record);

    List<WxpayParams> selectByExample(WxpayParamsExample example);

    WxpayParams selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WxpayParams record, @Param("example") WxpayParamsExample example);

    int updateByExample(@Param("record") WxpayParams record, @Param("example") WxpayParamsExample example);

    int updateByPrimaryKeySelective(WxpayParams record);

    int updateByPrimaryKey(WxpayParams record);
}