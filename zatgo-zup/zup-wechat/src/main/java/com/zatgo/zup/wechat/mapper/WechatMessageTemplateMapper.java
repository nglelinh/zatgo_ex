package com.zatgo.zup.wechat.mapper;

import com.ykb.mall.common.model.WechatMessageTemplate;
import com.ykb.mall.common.model.WechatMessageTemplateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WechatMessageTemplateMapper {
    int countByExample(WechatMessageTemplateExample example);

    int deleteByExample(WechatMessageTemplateExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WechatMessageTemplate record);

    int insertSelective(WechatMessageTemplate record);

    List<WechatMessageTemplate> selectByExample(WechatMessageTemplateExample example);

    WechatMessageTemplate selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WechatMessageTemplate record, @Param("example") WechatMessageTemplateExample example);

    int updateByExample(@Param("record") WechatMessageTemplate record, @Param("example") WechatMessageTemplateExample example);

    int updateByPrimaryKeySelective(WechatMessageTemplate record);

    int updateByPrimaryKey(WechatMessageTemplate record);
}