package com.zatgo.zup.wechat.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.wechat.entity.WechatConstant;
import com.zatgo.zup.wechat.service.IWechatAccessTokenService;
import com.zatgo.zup.wechat.service.WechatMaterialService;
import com.zatgo.zup.wechat.service.WeiXinService;
import com.zatgo.zup.wechat.utils.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WechatMaterialServiceImpl implements WechatMaterialService {

	private static final String URL = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=%s";
	
	private static final Logger LOG = LoggerFactory.getLogger(WechatMaterialServiceImpl.class);


	@Autowired
	private IWechatAccessTokenService wechatAccessTokenService;
	@Autowired
	private WechatConstant wechatConstant;

	@Override
	public String getWechatMaterialList() {
		
		try {
			int pageNum = 0;
	    	int pageSize = 20;
	    	boolean flag = true;
	    	
	    	JSONArray array = new JSONArray();
	    	while (flag) {
	    		pageNum++;
	    		Map<String, String> params = new HashMap<String, String>();
	    		params.put("type", "image");
	    		params.put("offset", String.valueOf((pageNum - 1) * pageSize));
	    		params.put("count", String.valueOf(pageSize));
	    		JSONObject res = JSON.parseObject(HttpUtil.doPost(
	    				String.format(URL,
								wechatAccessTokenService.getAccessToken(wechatConstant.weixinOfficialAcctAppId, wechatConstant.weixinOfficialAcctKey)),
	    				JSON.toJSONString(params)));
	    		LOG.info("素材列表：{}", res.toJSONString());
	    		array.add(res);
	    		int total = res.getIntValue("total_count");
	    		if(total <= pageNum * pageSize) {
	    			flag = false;
	    		}
			}
	    	
	    	return array.toJSONString();
		} catch (Exception e) {
			LOG.error("", e);
		}
		
		return null;
	}

}
