package com.zatgo.zup.wechat.service;


import com.zatgo.zup.wechat.model.CommonResult;

import java.util.Map;

public interface WechatMessageService {
	
	/**
	 * 发送微信消息
	 * @param params
	 * @param userId
	 * @param templateId
	 */
	void sendWechatMessage(Map<String, String> params, String openId, Integer templateId);
	
	CommonResult sendWechatSupportMsg(String openId);
	
	/**
	 * 发送微信助力活动消息
	 * @param userId
	 */
	CommonResult sendWechatSupportSuccessMsg(String openId, String id);

}
