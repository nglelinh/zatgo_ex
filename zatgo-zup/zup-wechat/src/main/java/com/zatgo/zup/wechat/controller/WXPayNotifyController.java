package com.zatgo.zup.wechat.controller;

import com.ykb.mall.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.wechat.remoteservice.TradeApiRemoteservice;
import com.zatgo.zup.wechat.sdk.WXPayConstants;
import com.zatgo.zup.wechat.sdk.WXPayUtil;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(tags = "WXPayNotifyController", description = "微信支付通知", hidden = true)
//@RestController(value = "/wx/pay/notify")
@Controller
@RequestMapping(value = "/wechat/weixinpay/callback")
public class WXPayNotifyController {

	private static final Logger logger = LoggerFactory.getLogger(WXPayNotifyController.class);

	private static final String RETURN_TEXT = "<xml><return_code><![CDATA[%s]]></return_code><return_msg><![CDATA[%s]]></return_msg></xml>";

	@Autowired
	private RedisLockUtils redisLockUtils;

//	@Autowired
//	private WeiXinPayNotifyService weiXinPayNotifyService;
	@Autowired
	private TradeApiRemoteservice tradeApiRemoteservice;


	@Value("${wx.pay.key}")
	private String wxPayKey;

	@RequestMapping(value = "/pay", method = RequestMethod.POST)
	@ResponseBody
	public String wxPayNotify(@RequestBody String strXML) {
		logger.info("微信支付通知参数：{}", strXML);

		Map<String, String> xmlMap = null;

		try {
			xmlMap = WXPayUtil.xmlToMap(strXML);
		} catch (Exception e) {
			logger.error("微信支付通知参数解析失败：{}", strXML);
		}

		if (!CollectionUtils.isEmpty(xmlMap)) {
			String orderNumber = xmlMap.get(WXPayConstants.FIELD_OUT_TRADE_NO);
			if (!StringUtils.isEmpty(orderNumber)) {
				String lockKey = String.format(RedisKeyConstants.WECHAT_PAY_NOTIFY_LOCK, orderNumber);
				if (redisLockUtils.lock(lockKey)) {
					try {
						tradeApiRemoteservice.wxPayNotify(xmlMap, wxPayKey);
						return String.format(RETURN_TEXT, WXPayConstants.SUCCESS, WXPayConstants.OK);
					} catch (Exception e) {
						logger.error("", e);
						logger.error("处理微信支付通知失败：{}", xmlMap);
					} finally {
						redisLockUtils.releaseLock(lockKey);
					}
				}

			} else {
				logger.error("微信支付通知订单号丢失：{}", xmlMap);
			}

		}

		return String.format(RETURN_TEXT, WXPayConstants.FAIL, WXPayConstants.FAIL);
	}

	@PostMapping(value = "/refund")
	public String wxRefundNotify(@RequestBody String strXML) {
		logger.info("微信退款通知参数：{}", strXML);
		return String.format(RETURN_TEXT, WXPayConstants.SUCCESS, WXPayConstants.OK);
	}

}
