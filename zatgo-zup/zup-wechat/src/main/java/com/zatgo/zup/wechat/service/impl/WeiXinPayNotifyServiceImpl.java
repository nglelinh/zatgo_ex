package com.zatgo.zup.wechat.service.impl;

import com.ykb.mall.common.enumType.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.OmsOrder;
import com.zatgo.zup.common.model.ResponseData;
import com.ykb.mall.common.model.PortalOrderBaseInfo;
import com.zatgo.zup.wechat.remoteservice.ActivityRemoteService;
import com.zatgo.zup.wechat.remoteservice.TradeApiRemoteservice;
import com.zatgo.zup.wechat.sdk.WXPayConstants;
import com.zatgo.zup.wechat.sdk.WXPayUtil;
import com.zatgo.zup.wechat.service.WeiXinPayNotifyService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;

@Service
public class WeiXinPayNotifyServiceImpl implements WeiXinPayNotifyService {

	private static final Logger logger = LoggerFactory.getLogger(WeiXinPayNotifyServiceImpl.class);

	@Value("${wx.pay.key}")
	private String wxPayKey;

	@Value("${system.isDev:false}")
	private boolean isDev;

	@Autowired
	private TradeApiRemoteservice tradeApiRemoteservice;
	@Autowired
	private ActivityRemoteService activityRemoteService;

//	@Value("${third.api.url}")
//	private String thirdApiUrl;


	@Transactional
	@Override
	public void wxPayNotify(Map<String, String> xmlMap) throws Exception {

		WXPayConstants.SignType signType = WXPayConstants.SignType.HMACSHA256;

		if (!WXPayUtil.isSignatureValid(xmlMap, wxPayKey, signType)) {
			logger.error("微信支付通知签名验证失败：{}", xmlMap);
			throw new BusinessException(BusinessExceptionCode.GO_TO_SIGN_ERROR);
		}

		String orderNumber = xmlMap.get(WXPayConstants.FIELD_OUT_TRADE_NO);
		if (StringUtils.isEmpty(orderNumber)) {
			logger.error("微信支付通知订单号丢失：{}", xmlMap);
			throw new BusinessException(BusinessExceptionCode.LACK_MUST_PARAMETER);
		}
		ResponseData<PortalOrderBaseInfo> portalOrderBaseInfoResponseData = tradeApiRemoteservice.selectOrderByOrderSn(orderNumber);
		if (portalOrderBaseInfoResponseData == null || !portalOrderBaseInfoResponseData.isSuccessful()) {
			logger.error("微信支付通知未查询到订单：{}", xmlMap);
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_NOT_EXIST);
		}
		PortalOrderBaseInfo order = portalOrderBaseInfoResponseData.getData();
		if (order == null) {
			logger.error("微信支付通知未查询到订单：{}", xmlMap);
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_NOT_EXIST);
		}
		if (BusinessEnum.OrderStatus.wait_pay.getCode() != order.getStatus()) {
			logger.error("微信支付重复通知：{}", xmlMap);
			return;
		}
		String totalFee = xmlMap.get(WXPayConstants.FIELD_TOTAL_FEE);
		if (StringUtils.isEmpty(totalFee)) {
			logger.error("微信支付通知支付金额丢失：{}", xmlMap);
			throw new BusinessException(BusinessExceptionCode.LACK_MUST_PARAMETER);
		}

		if (new BigDecimal(totalFee)
				.compareTo(isDev ? BigDecimal.ONE : order.getPayPrice().multiply(new BigDecimal("100"))) != 0) {
			logger.error("微信支付通知支付金额不匹配：{}", xmlMap);
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		ResponseData<OmsOrder> omsOrderResponseData = tradeApiRemoteservice.selectById(order.getOrderId());
		if (omsOrderResponseData == null || !omsOrderResponseData.isSuccessful()){
			logger.error("查詢订单失败：{}", xmlMap);
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		OmsOrder omsOrder = omsOrderResponseData.getData();
		// 更改状态，扣除库存
		tradeApiRemoteservice.paySuccess(order.getOrderId(), 2);

		// 保存用户订单支付操作记录
		tradeApiRemoteservice.insertOrderPayHistory(order.getOrderId(), order.getPayPrice());

		// 保存支付记录
		tradeApiRemoteservice.insertPaymentRecord(orderNumber, BusinessEnum.PayType.weixinPay, order.getPayPrice(),
				xmlMap.get(WXPayConstants.FIELD_TRANSACTION_ID));

		String note = omsOrder.getNote();
		if (!StringUtils.isEmpty(note)){
			String[] split = note.split("_");
			String activityId = split[0];
			String userId = split[1];
			ResponseData<Boolean> res = activityRemoteService.orderCallBack(activityId, userId, order.getOrderId() + "", true);
			if (res == null || !res.isSuccessful()){
				logger.info("返回信息 ============== activityId: " + activityId + "_userId:" + userId +  "_orderId: " + order.getOrderId() + "_isSuccess :" + true);
				throw new BusinessException("", "支付回调通知失败");
			}
		}

	}
}
