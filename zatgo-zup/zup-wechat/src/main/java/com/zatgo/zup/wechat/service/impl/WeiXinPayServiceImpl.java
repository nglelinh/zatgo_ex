package com.zatgo.zup.wechat.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ykb.mall.common.enumType.BusinessEnum.BusinessType;
import com.ykb.mall.common.enumType.BusinessEnum.OrderStatus;
import com.ykb.mall.common.enumType.BusinessEnum.PayType;
import com.ykb.mall.common.enumType.BusinessEnum.WeiXinPayType;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.*;
import com.ykb.mall.common.utils.DateTimeUtils;
import com.ykb.mall.common.utils.SignUtils;
import com.ykb.mall.common.utils.UUIDUtil;
import com.zatgo.zup.common.model.CloudUser;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.WechatRefundRecordRequest;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.wechat.mapper.WxpayParamsMapper;
import com.zatgo.zup.wechat.model.CommonResult;
import com.zatgo.zup.wechat.remoteservice.ActivityRemoteService;
import com.zatgo.zup.wechat.remoteservice.TradeApiRemoteservice;
import com.zatgo.zup.wechat.remoteservice.UserRemoteService;
import com.zatgo.zup.wechat.sdk.WXPay;
import com.zatgo.zup.wechat.sdk.WXPayConstants;
import com.zatgo.zup.wechat.sdk.WeiXinPayConfig;
import com.zatgo.zup.wechat.service.IWechatAccessTokenService;
import com.zatgo.zup.wechat.service.WeiXinPayService;
import com.zatgo.zup.wechat.utils.HttpUtil;
import com.zatgo.zup.wechat.utils.NetworkUtil;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
public class WeiXinPayServiceImpl implements WeiXinPayService {

	private static final Logger logger = LoggerFactory.getLogger(WeiXinPayServiceImpl.class);

	private static final String WxCreateQRCodeUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=";

	private static final String SCENE_INFO = "{\"h5_info\":{\"type\":\"Wap\",\"wap_url\":\"%s\",\"wap_name\":\"%s\"}}";

	@Value("${wx.pay.notifyUrl}")
	private String notifyUrl;

	@Value("${wx.pay.localIp}")
	private String localIp;

	@Value("${wx.pay.wapUrl}")
	private String wapUrl;

	@Value("${wx.pay.wapName}")
	private String wapName;

	@Value("${wx.pay.key}")
	private String wxPayKey;

	@Value("${system.isDev:false}")
	private boolean isDev;

	@Autowired
	private NetworkUtil networkUtil;

	@Autowired
	private WeiXinPayConfig weiXinPayConfig;

	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	private IWechatAccessTokenService iWechatAccessTokenService;
	//
	// @Autowired
	// private WeiXinPublicPayConfig wxPayPublicConfig;

	@Autowired
	private TradeApiRemoteservice tradeApiRemoteservice;
	@Autowired
	private WxpayParamsMapper wxpayParamsMapper;
	@Autowired
	private UserRemoteService userRemoteService;
	@Autowired
	private ActivityRemoteService activityRemoteService;


	@Override
	public CommonResult getOrderPayWeixinParams(Long orderId, Integer wxPayType, String wxOpenId, String userId) {
		ResponseData<PortalOrderDetailInfo> orderDetail = tradeApiRemoteservice.selectOrderDetail(orderId, userId);
		if (orderDetail == null || !orderDetail.isSuccessful()) {
			return new CommonResult().failed("订单信息不存在");
		}
		PortalOrderDetailInfo order = orderDetail.getData();
		if (OrderStatus.wait_pay.getCode() != order.getStatus()) {
			return new CommonResult().failed("订单已支付，请勿重复支付");
		}

		// TODO 校验订单

		return getWXPayParams(order.getOrderSn(), wxPayType, isDev ? new BigDecimal("0.01") : order.getPayPrice(), order.getBusinessType(),
				wxOpenId, order.getCreateTime());
	}

	/**
	 * 获取微信支付参数分发
	 *
	 * @param orderNumber
	 * @param wxPayType
	 * @param payAmount
	 * @param businessType
	 * @param wxOpenId
	 * @return
	 */
	private CommonResult getWXPayParams(String orderNumber, Integer wxPayType, BigDecimal payAmount, Byte businessType,
			String wxOpenId, String createTime) {
		WxpayParamsExample example = new WxpayParamsExample();
		WxpayParamsExample.Criteria criteria = example.createCriteria();
		criteria.andOrderNumberEqualTo(orderNumber).andPayTypeEqualTo(wxPayType.byteValue());
		List<WxpayParams> params = wxpayParamsMapper.selectByExample(example);
		if (!CollectionUtils.isEmpty(params)) {

			WXPayData data = new WXPayData();
			WxpayParams wxpayParams = params.get(0);
			if (WeiXinPayType.PUBLIC.getCode().intValue() == wxpayParams.getPayType()) {
				data = getWXPayJSSDKParams(wxpayParams.getAppId(), wxpayParams.getPrepayId());
			} else if (WeiXinPayType.H5.getCode().intValue() == wxpayParams.getPayType()) {
				data = getWXPayH5Params(wxpayParams.getCodeUrl());
			}

			return new CommonResult().success(data);
		}

		try {
			if (WeiXinPayType.H5.getCode() == wxPayType) {
				return getWXH5PayParams(orderNumber, payAmount, businessType, createTime);
			} else if (WeiXinPayType.PUBLIC.getCode() == wxPayType) {
				return getPublicPayParams(orderNumber, payAmount, businessType, wxOpenId, createTime);
			} else {
				return new CommonResult().failed("微信支付类型不匹配");
			}
		} catch (Exception e) {
			logger.error("获取微信支付参数失败：", e);
		}

		return new CommonResult().failed().failed("获取微信支付参数失败");
	}

	/**
	 * 生成微信公众号支付返回参数
	 *
	 * @param orderNumber
	 * @param payAmount
	 * @param businessType
	 * @param wxOpenId
	 * @return
	 * @throws Exception
	 */
	private CommonResult getPublicPayParams(String orderNumber, BigDecimal payAmount, Byte businessType, String wxOpenId,
			String createTime) throws Exception {


		WXPay wxPay = new WXPay(weiXinPayConfig, notifyUrl);

		Map<String, String> req = getWXPublicPayReqParams(orderNumber, payAmount, businessType,
				wxOpenId, createTime);
		logger.info("请求微信公众号支付参数：{}", req);
		Map<String, String> resp = wxPay.unifiedOrder(req);
		logger.info("请求微信公众号支付参数响应：{}", resp);

		return getWXPayResult(resp, orderNumber, businessType);
	}

	/**
	 * 生成微信H5支付返回参数
	 *
	 * @param orderNumber
	 * @param money
	 * @param businessType
	 * @return
	 * @throws Exception
	 */
	private CommonResult getWXH5PayParams(String orderNumber, BigDecimal money, Byte businessType, String createTime)
			throws Exception {

		WXPay wxPay = new WXPay(weiXinPayConfig, notifyUrl);
		Map<String, String> req = getWXH5PayReqParams(orderNumber, money, businessType, createTime);

		logger.info("请求微信h5支付参数：{}", req);
		Map<String, String> resp = wxPay.unifiedOrder(req);
		logger.info("请求微信h5支付参数响应：{}", resp);

		return getWXPayResult(resp, orderNumber, businessType);
	}

	/**
	 * 封装微信支付参数返回对象
	 *
	 * @param resp
	 * @param orderNumber
	 * @param businessType
	 * @return
	 */
	private CommonResult getWXPayResult(Map<String, String> resp, String orderNumber, Byte businessType) {
		WxpayParams wxpayParams = insertWxpayParams(resp, orderNumber, businessType);
		if (wxpayParams == null) {
			return new CommonResult().failed("调用微信支付失败");
		}
		WXPayData data = new WXPayData();

		if (WeiXinPayType.PUBLIC.getCode().intValue() == wxpayParams.getPayType()) {
			data = getWXPayJSSDKParams(wxpayParams.getAppId(), wxpayParams.getPrepayId());
		} else if (WeiXinPayType.H5.getCode().intValue() == wxpayParams.getPayType()) {
			data = getWXPayH5Params(wxpayParams.getCodeUrl());
		}

		return new CommonResult().success(data);
	}

	private WXPayData getWXPayJSSDKParams(String wxAppId, String prepayId) {

		String nonceStr = String.valueOf(new Random().nextLong());
		String packageStr = new StringBuffer("prepay_id=").append(prepayId).toString();
		String timestamp = String.valueOf(System.currentTimeMillis() / 1000);

		StringBuffer sign = new StringBuffer();
		sign.append("appId=").append(wxAppId).append("&nonceStr=").append(nonceStr).append("&package=")
				.append(packageStr).append("&signType=").append(WXPayConstants.HMACSHA256).append("&timeStamp=")
				.append(timestamp);
		sign.append("&key=").append(wxPayKey);
		String signStr = SignUtils.sha256_HMAC(sign.toString(), wxPayKey).toUpperCase();

		WXPayData payData = new WXPayData();
		payData.setNonceStr(nonceStr);
		payData.setPackageStr(packageStr);
		payData.setSignType(WXPayConstants.HMACSHA256);
		payData.setPaySign(signStr);
		payData.setTimestamp(timestamp);
		payData.setPayType(WeiXinPayType.PUBLIC.getCode().byteValue());
		return payData;
	}

	private WXPayData getWXPayH5Params(String codeUrl) {
		WXPayData payData = new WXPayData();
		payData.setPayType(WeiXinPayType.H5.getCode().byteValue());
		payData.setCodeUrl(codeUrl);
		return payData;
	}

	/**
	 * 保存微信支付参数
	 *
	 * @param resp
	 * @param orderNumber
	 * @param businessType
	 * @return
	 */
	@Transactional
	private WxpayParams insertWxpayParams(Map<String, String> resp, String orderNumber, Byte businessType) {
		if (!WXPayConstants.SUCCESS.equalsIgnoreCase(resp.get(WXPayConstants.FIELD_RETURN_CODE))) {
			logger.error("调用微信支付接口失败：{}", resp);
			return null;
		}

		if (!WXPayConstants.SUCCESS.equalsIgnoreCase(resp.get(WXPayConstants.FIELD_RESULT_CODE))) {
			logger.error("微信支付接口下单失败：{}", resp);
			return null;
		}

		WxpayParams wxpayParams = new WxpayParams();
		wxpayParams.setAppId(resp.get(WXPayConstants.FIELD_APPID));
		wxpayParams.setBody(resp.get(WXPayConstants.FIELD_BODY));
		wxpayParams.setCodeUrl(resp.get(WXPayConstants.FIELD_MWEB_URL));
		wxpayParams.setBusinessType(businessType);
		wxpayParams.setNonceStr(resp.get(WXPayConstants.FIELD_NONCE_STR));
		wxpayParams.setOrderNumber(orderNumber);

		if (resp.get(WXPayConstants.FIELD_TRADE_TYPE).equals(WXPayConstants.TRADE_TYPE.JSAPI.name())) {
			wxpayParams.setPayType(WeiXinPayType.PUBLIC.getCode().byteValue());
		} else if (resp.get(WXPayConstants.FIELD_TRADE_TYPE).equals(WXPayConstants.TRADE_TYPE.MWEB.name())) {
			wxpayParams.setPayType(WeiXinPayType.H5.getCode().byteValue());
		}

		wxpayParams.setPrepayId(resp.get(WXPayConstants.FIELD_PREPAY_ID));
		wxpayParams.setSign(resp.get(WXPayConstants.FIELD_SIGN));

		wxpayParamsMapper.insertSelective(wxpayParams);

		return wxpayParams;
	}

	/**
	 * 生成微信公众号支付请求参数
	 *
	 * @param orderNumber
	 * @param money
	 * @param businessType
	 * @param openId
	 * @return
	 */
	private Map<String, String> getWXPublicPayReqParams(String orderNumber, BigDecimal money, Byte businessType,
			String openId, String createTime) {
		Map<String, String> params = getWXPayParams(orderNumber, money, businessType,
				WXPayConstants.TRADE_TYPE.JSAPI.name(), createTime);

		params.put(WXPayConstants.FIELD_OPENID, openId);
		params.put(WXPayConstants.FIELD_SPBILL_CREATE_IP, localIp);
		return params;
	}

	/**
	 * 生成微信H5支付请求参数
	 *
	 * @param orderNumber
	 * @param money
	 * @param businessType
	 * @return
	 */
	private Map<String, String> getWXH5PayReqParams(String orderNumber, BigDecimal money, Byte businessType,
			String createTime) {
		Map<String, String> params = getWXPayParams(orderNumber, money, businessType,
				WXPayConstants.TRADE_TYPE.MWEB.name(), createTime);

		params.put(WXPayConstants.FIELD_SPBILL_CREATE_IP, networkUtil.getRemoteIP());
		params.put(WXPayConstants.FIELD_SCENE_INFO, String.format(SCENE_INFO, wapUrl, wapName));
		return params;
	}

	/**
	 * 获取微信支付公共参数
	 *
	 * @param orderNumber
	 * @param money
	 * @param businessType
	 * @param tradeType
	 * @return
	 */
	private Map<String, String> getWXPayParams(String orderNumber, BigDecimal money, Byte businessType,
			String tradeType, String createTime) {
		Map<String, String> map = new HashMap<>();

		if (BusinessType.TRAVEL.getCode().equals(businessType)) {
			map.put(WXPayConstants.FIELD_BODY, WXPayConstants.FIELD_BODY_TRAVEL);
		} else if (BusinessType.FLIGHT.getCode().equals(businessType)) {
			map.put(WXPayConstants.FIELD_BODY, WXPayConstants.FIELD_BODY_FLIGHT);
		} else if (BusinessType.HOTEL.getCode().equals(businessType)) {
			map.put(WXPayConstants.FIELD_BODY, WXPayConstants.FIELD_BODY_HOTEL);
		} else if (BusinessType.ARTICLE.getCode().equals(businessType)) {
			map.put(WXPayConstants.FIELD_BODY, WXPayConstants.FIELD_BODY_ARTICLE);
		}

		map.put(WXPayConstants.FIELD_OUT_TRADE_NO, orderNumber);
		map.put(WXPayConstants.FIELD_TOTAL_FEE, String.valueOf(money.multiply(new BigDecimal(100)).intValue()));
		map.put(WXPayConstants.FIELD_TRADE_TYPE, tradeType);

		// 设置订单有效期为下单内1小时，目前先交由微信控制，后期加过期订单逻辑
		DateTime dateTime = new DateTime(
				DateTimeUtils.parseStringToDate(createTime, DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM_SS).getTime());
		dateTime = dateTime.plusHours(1);
		map.put(WXPayConstants.FIELD_TIME_EXPIRE, dateTime.toString(DateTimeUtils.PATTEN_YYYYMMDDHHMMSS));

		return map;
	}

	@Override
	public void weixinRefund(String orderNumber, BigDecimal refundAmount) throws Exception {
		logger.info("订单退款信息: orderNumber :" + orderNumber + "  refundAmount" + refundAmount);
		ResponseData<PortalOrderBaseInfo> portalOrderBaseInfoResponseData = tradeApiRemoteservice.selectOrderByOrderSn(orderNumber);
		if (portalOrderBaseInfoResponseData == null || !portalOrderBaseInfoResponseData.isSuccessful()){
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "订单查询失败");
		}
		PortalOrderBaseInfo order = portalOrderBaseInfoResponseData.getData();
		if (null == order) {
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_NOT_EXIST);
		}

		if (OrderStatus.wait_pay.getCode() == order.getStatus()) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "订单未付款");
		}

		if (order.getPayPrice().compareTo(refundAmount) == -1) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "退款金额大于订单金额");
		}

		Map<String, String> reqData = new HashMap<>();
		reqData.put(WXPayConstants.FIELD_OUT_TRADE_NO, order.getOrderSn());
		reqData.put(WXPayConstants.FIELD_OUT_REFUND_NO, order.getOrderSn() + "-" + UUIDUtil.getRandomUserName());
		reqData.put(WXPayConstants.FIELD_TOTAL_FEE,
				order.getPayPrice().multiply(new BigDecimal("100")).toPlainString().replaceFirst("\\.00", ""));
		reqData.put(WXPayConstants.FIELD_REFUND_FEE, refundAmount.multiply(new BigDecimal("100")).toPlainString().replaceFirst("\\.00", ""));
		WXPay wxPay = new WXPay(weiXinPayConfig, "");

		Map<String, String> respData = wxPay.refund(reqData);
		if (!WXPayConstants.SUCCESS.equalsIgnoreCase(respData.get(WXPayConstants.FIELD_RETURN_CODE))) {
			logger.error("调用微信退款接口失败：{}", respData);
			throw new BusinessException(BusinessExceptionCode.HTTP_FAIL_CODE, "调用微信退款接口失败");
		}

		if (!WXPayConstants.OK.equalsIgnoreCase(respData.get(WXPayConstants.FIELD_RETURN_MSG))) {
			logger.error("微信退款接口退款失败：{}", respData);
			throw new BusinessException(BusinessExceptionCode.HTTP_FAIL_CODE, "微信退款接口退款失败");
		}

		WechatRefundRecordRequest request = new WechatRefundRecordRequest();
		request.setOrderNumber(orderNumber);
		request.setMoney(refundAmount);
		request.setPayType(PayType.weixinPay);
		request.setRefundNumber(reqData.get(WXPayConstants.FIELD_OUT_REFUND_NO));
		request.setThirdRefundNumber(reqData.get(WXPayConstants.FIELD_REFUND_ID));
		request.setThirdBusinessNumber(reqData.get(WXPayConstants.FIELD_TRANSACTION_ID));
		// 保存退款记录
		tradeApiRemoteservice.insertRefundRecord(request);

		String activityType = order.getActivityType();
		String activityId = order.getActivityId();
		if (!StringUtils.isEmpty(activityType) && "2".equals(activityType)){
			try{
				activityRemoteService.updateGroupStatus(activityId, order.getOrderId());
			} catch (Exception e){
				logger.error("", e);
			}
		}
	}

	@Override
	public String getQRCode(String cloudUserId, String sceneStr, String userId) {
		ResponseData<CloudUser> cloudUserResponseData = userRemoteService.selectCloudUserDetail(cloudUserId);
		if (cloudUserResponseData == null || !cloudUserResponseData.isSuccessful())
			throw new BusinessException();
		CloudUser data = cloudUserResponseData.getData();
		String accessToken = iWechatAccessTokenService.getAccessToken(data.getWeixinOfficialAcctAppId(), data.getWeixinOfficialAcctKey());
		JSONObject body = new JSONObject();
		body.put("expire_seconds", 2592000);
		body.put("action_name", "QR_STR_SCENE");
		JSONObject actionInfo = new JSONObject();
		JSONObject scene = new JSONObject();
		scene.put("scene_str", sceneStr);
		actionInfo.put("scene", scene);
		body.put("action_info", actionInfo);
		String[] split = sceneStr.split("_");
		try {
			String res = HttpUtil.doPost(WxCreateQRCodeUrl + accessToken, body.toJSONString());
			logger.info(res);
			if (!StringUtils.isEmpty(res)){
				JSONObject wxRes = JSONObject.parseObject(res);
				wxRes.put("userId", userId);
				redisUtils.put(getKey(split[0], cloudUserId, split[2]), wxRes, 30l, TimeUnit.DAYS);
				return wxRes.toJSONString();
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}

	private String getKey(String userId, String cloudUserId, String activityJoinId){
		return RedisKeyConstants.WECHAT_QR + cloudUserId + ":" + userId + ":" + activityJoinId;
	}

}
