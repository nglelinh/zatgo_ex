package com.zatgo.zup.wechat.service;

public interface IWechatAccessTokenService {

	/**
	 * 获取微信AccessToken
	 *
	 * @return
	 */
	String getAccessToken(String appId, String appSecret);

	/**
	 * 强制更新并获取微信AccessToken
	 *
	 * @param timeOut
	 *            等待时间 0不等待，-1同步，直到拿到值
	 * @return
	 */
	String cacheAccessToken(Long timeOut, String useAppId, String userAppSecret);

}
