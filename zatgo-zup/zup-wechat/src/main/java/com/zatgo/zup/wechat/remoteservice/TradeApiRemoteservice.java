package com.zatgo.zup.wechat.remoteservice;

import com.ykb.mall.common.enumType.BusinessEnum;
import com.ykb.mall.common.model.OmsOrder;
import com.ykb.mall.common.model.PortalOrderBaseInfo;
import com.ykb.mall.common.model.PortalOrderDetailInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.WechatRefundRecordRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by 46041 on 2019/6/5.
 */

@FeignClient("zup-trade-api")
public interface TradeApiRemoteservice {

    @GetMapping(value = "/trade/api/order/detail/{id}/{userId}")
    @ResponseBody
    ResponseData<PortalOrderDetailInfo> selectOrderDetail(@PathVariable("id") Long id, @PathVariable("userId") String userId);

    @PostMapping(value = "/trade/api/order/selectOrderByOrderSn/{orderNumber}")
    @ResponseBody
    ResponseData<PortalOrderBaseInfo> selectOrderByOrderSn(@PathVariable("orderNumber") String orderNumber);

    @PostMapping(value = "/trade/api/order/save/refundRecord")
    @ResponseBody
    void insertRefundRecord(@RequestBody WechatRefundRecordRequest request);

    @PostMapping(value = "/trade/api/order/save/insertPaymentRecord")
    @ResponseBody
    void insertPaymentRecord(@RequestParam("orderNumber") String orderNumber,
                             @RequestParam("payType") BusinessEnum.PayType payType,
                             @RequestParam("money")  BigDecimal money,
                             @RequestParam("thirdBusinessNumber") String thirdBusinessNumber);

    @GetMapping(value = "/trade/api/order/paySuccess")
    @ResponseBody
    ResponseData<Boolean> paySuccess(@RequestParam("orderId") Long orderId, @RequestParam("payType") Integer payType);

    @GetMapping(value = "/trade/api/order/insertOrderPayHistory")
    @ResponseBody
    void insertOrderPayHistory(@RequestParam("orderId") Long orderId, @RequestParam("fee") BigDecimal fee);

    @GetMapping(value = "/trade/api/order/selectById")
    @ResponseBody
    ResponseData<OmsOrder> selectById(@RequestParam("orderId") Long orderId);

    @PostMapping(value = "/trade/api/wx/remote/wxPayNotify")
    @ResponseBody
    void wxPayNotify(@RequestBody Map<String, String> xmlMap, @RequestParam("wxPayKey") String wxPayKey) throws Exception ;

}
