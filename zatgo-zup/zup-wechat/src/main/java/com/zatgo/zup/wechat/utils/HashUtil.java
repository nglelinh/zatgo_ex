package com.zatgo.zup.wechat.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.util.zip.CRC32;

/**
 * @Author hejiang
 * @Description: 工具类
 * @Date: Created in 17:35 2017/9/22
 * @Modified By:
 */
public class HashUtil {
	
	private static Logger log = LoggerFactory.getLogger("HashUtil");
	
    /**
     * crc32
     * @param bytes
     * @return
     */
    public static long crc32Code(byte[] bytes) {
        CRC32 crc32 = new CRC32();
        crc32.update(bytes);
        return crc32.getValue();
    }
    
    /**
     * 计算字符串的hash值(hash算法)
     * @param string    明文
     * @param algorithm 算法名
     * @return          字符串的hash值
     */
    public static String hash(String string, String algorithm) {
        if (string.isEmpty()) {
            return "";
        }
        MessageDigest hash = null;
        try {
            hash = MessageDigest.getInstance(algorithm);
            byte[] bytes = hash.digest(string.getBytes("UTF-8"));
            String result = "";
            for (byte b : bytes) {
                String temp = Integer.toHexString(b & 0xff);
                if (temp.length() == 1) {
                    temp = "0" + temp;
                }
                result += temp;
            }
            return result;
        } catch (Exception e) {
        	log.error("获取hash失败", e);
        }
        return "";
    }
}
