package com.zatgo.zup.wechat.remoteservice;

import com.zatgo.zup.common.model.CloudUser;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by 46041 on 2019/6/5.
 */

@FeignClient("zup-merchant")
public interface UserRemoteService {

    @GetMapping(value = "/user/cloud/account/detail/{cloudUserId}")
    ResponseData<CloudUser> selectCloudUserDetail(@PathVariable("cloudUserId") String cloudUserId);


    @RequestMapping(value = "/user/userInfo/wechatOpenId/{wechatOpenId}/{cloudUserId}", method = RequestMethod.GET)
    ResponseData<String> getUserInfoByWechatOpenId(@PathVariable("wechatOpenId") String wechatOpenId,
                                                          @PathVariable("cloudUserId") String cloudUserId);
}
