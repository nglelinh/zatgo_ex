package com.zatgo.zup.wechat.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * @Author wangyucong
 * @Date 2019/4/8 10:33
 */
public class WxCommonAccessTokenData extends WechatErrData implements Serializable {

    private static final long serialVersionUID = -1L;

    /**
     * 获取到的凭证
     */
    @JSONField(name = "access_token")
    private String accessToken;

    /**
     * 凭证有效时间，单位：秒
     */
    @JSONField(name = "expires_in")
    private Integer expiresIn;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }
}