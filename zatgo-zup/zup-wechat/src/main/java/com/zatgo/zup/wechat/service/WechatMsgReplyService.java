package com.zatgo.zup.wechat.service;


import com.zatgo.zup.wechat.entity.WechatMsg;

public interface WechatMsgReplyService {
	
	String selectWechatMsgReply(WechatMsg.MsgType msgType, WechatMsg.Event event, String key);

}
