package com.zatgo.zup.wechat.service.impl;

import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.redis.RedisKeyConstants;
import com.ykb.mall.common.utils.SignUtils;
import com.ykb.mall.common.utils.UUIDUtil;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.model.CloudUser;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.WxUserInfoData;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.wechat.entity.WechatConstant;
import com.zatgo.zup.wechat.model.WxAccessTokenData;
import com.zatgo.zup.wechat.model.WxJsapiConfigData;
import com.zatgo.zup.wechat.model.WxJsapiTicketData;
import com.zatgo.zup.wechat.remoteservice.UserRemoteService;
import com.zatgo.zup.wechat.service.IWechatAccessTokenService;
import com.zatgo.zup.wechat.service.WeiXinService;
import com.zatgo.zup.wechat.utils.RedisService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Author wangyucong
 * @Date 2019/2/19 13:48
 */
@Service
public class WeiXinServiceImpl implements WeiXinService {

    private static final Logger logger = LoggerFactory.getLogger(WeiXinServiceImpl.class);

    @Value("${wx.pay.appId}")
    private String APP_ID;

    @Value("${wx.appSecret}")
    private String APP_SECRET;

//    @Value("${wx.account.default.password}")
//    private String DEFAULT_PASSWORD;


    /**
     * openid 获取
     */
    private String GET_OPENID_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";

    /**
     * 用户信息获取
     */
    private String GET_REGIST_USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo";

    /**
     * 公共access_token 获取
     */
    private String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token";

    private String GET_USER_INFO_URL_ACCESS = "https://api.weixin.qq.com/cgi-bin/user/info";

    /**
     * jsapi_ticket 获取
     */
    private String GET_JSAPI_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";

    private final static String authorization_code = "authorization_code";

    private final static String language = "zh_CN";

    private final static String grant_type = "client_credential";

    private final static String jsapi_type = "jsapi";

    @Resource
    private OkHttpService httpService;

    @Resource
    private RedisService redisService;

    @Resource
    private RedisLockUtils redisLockUtils;
    @Autowired
    private IWechatAccessTokenService iWechatAccessTokenService;

    @Autowired
    private WechatConstant wechatConstant;
    @Autowired
    private UserRemoteService userRemoteService;


    /**
     * 获取微信用户信息
     *
     * @param openId
     * @param accessToken
     */
    @Override
    public WxUserInfoData getUserInfo(String openId, String cloudUserId) {
        ResponseData<CloudUser> cloudUserResponseData = userRemoteService.selectCloudUserDetail(cloudUserId);
        if (cloudUserResponseData == null || !cloudUserResponseData.isSuccessful())
            throw new BusinessException();
        CloudUser data = cloudUserResponseData.getData();
        String accessToken = iWechatAccessTokenService.getAccessToken(data.getWeixinOfficialAcctAppId(), data.getWeixinOfficialAcctKey());
        LinkedHashMap<String, String> param = new LinkedHashMap<>();
        param.put("access_token", accessToken);
        param.put("openid", openId);
        param.put("lang", language);
        WxUserInfoData info = httpService.wxGet(GET_USER_INFO_URL_ACCESS, param, WxUserInfoData.class);
        if (StringUtils.isNotEmpty(info.getErrCode())) {
            logger.error("获取微信用户信息失败，{}", info.getErrMsg());
            throw new BusinessException(BusinessExceptionCode.WEIXIN_HTTP_ERROR, "{}");
        }
        return info;
    }

    /**
     * 获取微信公共access_token
     *
     * @param appId
     * @param appSecret
     * @return
     */
//    @Override
//    public WxCommonAccessTokenData getAccessToken(String appId, String appSecret) {
//        LinkedHashMap<String, String> param = new LinkedHashMap<>();
//        param.put("grant_type", grant_type);
//        param.put("appid", appId);
//        param.put("secret", appSecret);
//        WxCommonAccessTokenData info = httpService.wxGet(GET_ACCESS_TOKEN_URL, param, WxCommonAccessTokenData.class);
//        if (StringUtils.isNotEmpty(info.getErrCode())) {
//            logger.error("获取微信公共access_token 失败，{}", info.getErrMsg());
//            throw new BusinessException(BusinessExceptionCode.WEIXIN_HTTP_ERROR, "获取微信公共access_token失败");
//        }
//        return info;
//    }

    /**
     * 获取微信jsapi_ticket
     *
     * @param accessToken
     * @return
     */
    @Override
    public WxJsapiTicketData getJsApiTicket(String accessToken) {
        LinkedHashMap<String, String> param = new LinkedHashMap<>();
        param.put("access_token", accessToken);
        param.put("type", jsapi_type);
        WxJsapiTicketData info = httpService.wxGet(GET_JSAPI_TICKET_URL, param, WxJsapiTicketData.class);
        if (StringUtils.isNotEmpty(info.getErrCode()) && !"0".equals(info.getErrCode())) {
            logger.error("获取微信jsapi_ticket 失败，{}", info.getErrMsg());
            throw new BusinessException(BusinessExceptionCode.WEIXIN_HTTP_ERROR, "{}");
        }
        return info;
    }


    /**
     * 注册绑定微信用户
     *
     * @param info
     */
//    @Override
//    public void registerWeixinAccount(WxUserInfoData info) {

//        UmsMemberWeixin weixin = new UmsMemberWeixin();
//        weixin.setOpenId(info.getOpenId());
//        weixin.setNickName(info.getNickName());
//        weixin.setSex(Byte.valueOf(info.getSex()));
//        weixin.setProvince(info.getProvince());
//        weixin.setCity(info.getCity());
//        weixin.setCountry(info.getCountry());
//        weixin.setHeadimgUrl(info.getHeadimgUrl());
//        weixin.setPrivilege(info.getPrivilege());
//        weixin.setUnionId(info.getUnionId());
//        weixin.setCreateTime(new Date());
//        umsMemberWeixinMapper.insertSelective(weixin);
//
//        UmsMember umsMember = new UmsMember();
//        umsMember.setUsername(UUIDUtil.getRandomUserName());
//        umsMember.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
//        umsMember.setWeixinId(weixin.getId());
//        umsMember.setNickname(weixin.getNickName());
//        umsMember.setStatus(1);
//        umsMember.setCreateTime(new Date());
//        umsMember.setIcon(weixin.getHeadimgUrl());
//        umsMember.setGender(Integer.valueOf(weixin.getSex()));
//        umsMember.setCity(weixin.getCity());
//        //获取默认会员等级并设置
//        UmsMemberLevelExample levelExample = new UmsMemberLevelExample();
//        levelExample.createCriteria().andDefaultStatusEqualTo(1);
//        List<UmsMemberLevel> memberLevelList = memberLevelMapper.selectByExample(levelExample);
//        if (!CollectionUtils.isEmpty(memberLevelList)) {
//            umsMember.setMemberLevelId(memberLevelList.get(0).getId());
//        }
//        memberMapper.insert(umsMember);
//    }

    /**
     * 获取微信js接口配置
     *
     * @param url
     * @return
     */
    @Override
    public WxJsapiConfigData selectJsapiConfig(String url) {
        if (StringUtils.isEmpty(url)) {
            throw new BusinessException(BusinessExceptionCode.WEIXIN_INFO_ERROR, "请输入分享的链接地址");
        }
        String accessToken = iWechatAccessTokenService.getAccessToken(wechatConstant.weixinOfficialAcctAppId, wechatConstant.weixinOfficialAcctKey);
        if (StringUtils.isEmpty(accessToken)) {
            throw new BusinessException(BusinessExceptionCode.WEIXIN_INFO_ERROR, "获取微信凭证失败");
        }
        String jsapiTicket = getWechatJsapiTicket(accessToken);
        if (StringUtils.isEmpty(accessToken)) {
            throw new BusinessException(BusinessExceptionCode.WEIXIN_INFO_ERROR, "获取微信临时票据失败");
        }

        WxJsapiConfigData data = new WxJsapiConfigData();
        data.setAppId(APP_ID);
        data.setTimestamp(String.valueOf(System.currentTimeMillis() / 1000));
        data.setNonceStr(UUIDUtil.getRandomUserName());

        Map<String, String> map = new HashMap<String, String>();
        map.put("noncestr", data.getNonceStr());
        map.put("jsapi_ticket", jsapiTicket);
        map.put("timestamp", data.getTimestamp());
        map.put("url", url);

        List<String> keys = new ArrayList<>(map.keySet());
        keys.sort((fir, sed) -> fir.compareTo(sed));
        StringBuffer sign = new StringBuffer();
        keys.forEach(value -> sign.append("&").append(value).append("=").append(map.get(value)));
        String signStr = SignUtils.sha1(sign.substring(1));
        data.setSignature(signStr);
        return data;
    }

    /**
     * 获取微信openId
     *
     * @param wxCode
     * @return
     */
    @Override
    public WxAccessTokenData getOpenId(String wxCode) {
        LinkedHashMap<String, String> param = new LinkedHashMap<>();
        param.put("appid", APP_ID);
        param.put("secret", APP_SECRET);
        param.put("code", wxCode);
        param.put("grant_type", authorization_code);
        WxAccessTokenData result = httpService.wxGet(GET_OPENID_URL, param, WxAccessTokenData.class);
        if (StringUtils.isNotEmpty(result.getErrCode())) {
            logger.error("获取微信openId失败，{}", result.getErrMsg());
            throw new BusinessException(BusinessExceptionCode.WEIXIN_HTTP_ERROR, "{}");
        }
        return result;
    }

    @Override
    public WxUserInfoData getRegisterWxUserInfo(String openId, String accessToken) {
        LinkedHashMap<String, String> param = new LinkedHashMap<>();
        param.put("access_token", accessToken);
        param.put("openid", openId);
        param.put("lang", language);
        WxUserInfoData info = httpService.wxGet(GET_REGIST_USER_INFO_URL, param, WxUserInfoData.class);
        if (StringUtils.isNotEmpty(info.getErrCode())) {
            logger.error("获取微信用户信息失败，{}", info.getErrMsg());
            throw new BusinessException(info.getErrCode(), "{}");
        }
        return info;
    }

//    @Override
//    public String getWechatAccessToken() {
//        String accessToken = "";
//        String lockKey = redisLockUtils.getRedisKey(WeiXinServiceImpl.class, RedisKeyConstants.WECHAT_ACCESS_TOKEN_LOCK);
//        try {
//            if (redisLockUtils.lock(lockKey)) {
//                accessToken = redisService.get(RedisKeyConstants.WECHAT_ACCESS_TOKEN);
//                if (StringUtils.isEmpty(accessToken)) {
//                    accessToken = getLatestAccessToken();
//                }
//            }
//        } catch (Exception e) {
//            logger.error("获取微信access_token失败", e);
//            throw new BusinessException(BusinessExceptionCode.WEIXIN_INFO_ERROR, "获取微信access_token失败");
//        } finally {
//            redisLockUtils.releaseLock(lockKey);
//        }
//        return accessToken;
//    }

    private String getWechatJsapiTicket(String accessToken) {
        String jsapiTicket = "";
        String lockKey = redisLockUtils.getRedisKey(WeiXinServiceImpl.class, RedisKeyConstants.WECHAT_JSAPI_TICKET_LOCK);
        try {
            if (redisLockUtils.lock(lockKey)) {
                jsapiTicket = redisService.get(RedisKeyConstants.WECHAT_JSAPI_TICKET);
                if (StringUtils.isEmpty(jsapiTicket)) {
                    jsapiTicket = getLatestJsapiTicket(accessToken);
                }
            }
        } catch (Exception e) {
            logger.error("获取微信最新jsapi_ticket失败", e);
            throw new BusinessException(BusinessExceptionCode.WEIXIN_INFO_ERROR, "{}");
        } finally {
            redisLockUtils.releaseLock(lockKey);
        }
        return jsapiTicket;
    }

//    private String getLatestAccessToken() {
//        WxCommonAccessTokenData data = getAccessToken(APP_ID, APP_SECRET);
//        redisService.set(RedisKeyConstants.WECHAT_ACCESS_TOKEN, data.getAccessToken());
//        // 提前5分钟刷新获取新的凭证
//        redisService.expire(RedisKeyConstants.WECHAT_ACCESS_TOKEN, data.getExpiresIn() - 5 * 60);
//        return data.getAccessToken();
//    }

    private String getLatestJsapiTicket(String accessToken) {
        WxJsapiTicketData data = getJsApiTicket(accessToken);
        redisService.set(RedisKeyConstants.WECHAT_JSAPI_TICKET, data.getTicket());
        // 提前5分钟刷新获取新的临时票据
        redisService.expire(RedisKeyConstants.WECHAT_JSAPI_TICKET, data.getExpiresIn() - 5 * 60);
        return data.getTicket();
    }
}