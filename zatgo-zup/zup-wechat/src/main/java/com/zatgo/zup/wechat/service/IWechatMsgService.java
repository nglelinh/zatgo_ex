package com.zatgo.zup.wechat.service;


import com.zatgo.zup.wechat.entity.MsgToken;
import com.zatgo.zup.wechat.entity.WechatMsg;

public interface IWechatMsgService {

	MsgToken validateWxToken(MsgToken token, String cloudUserId);
	
	String wxMsg(WechatMsg msg);
	
}
