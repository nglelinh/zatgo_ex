package com.zatgo.zup.wechat.model;

import io.swagger.annotations.ApiModel;

/**
 * @Author wangyucong
 * @Date 2019/4/8 13:36
 */
@ApiModel("微信js接口配置数据")
public class WxJsapiConfigData {

    private String appId;

    private String timestamp;

    private String nonceStr;

    private String signature;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}