package com.zatgo.zup.wechat.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * @Author wangyucong
 * @Date 2019/2/25 15:34
 */
@ApiModel("个人订单列表")
public class PortalOrderBaseInfo {
    @ApiModelProperty("订单编号")
    private Long orderId;

    @ApiModelProperty("订单号")
    private String orderSn;
    
    @ApiModelProperty("业务类型（1-旅游；2-机票；3-酒店；4-文章）")
    private Byte businessType;

    @ApiModelProperty("产品名称")
    private String productName;

    @ApiModelProperty("产品图片")
    private String productPic;

    @ApiModelProperty("订单状态")
    private Integer status;

    @ApiModelProperty("订单实际支付价")
    private BigDecimal payPrice;

    @ApiModelProperty("创建时间")
    private String createTime;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Byte getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Byte businessType) {
        this.businessType = businessType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPic() {
        return productPic;
    }

    public void setProductPic(String productPic) {
        this.productPic = productPic;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}