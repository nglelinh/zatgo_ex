package com.zatgo.zup.wechat.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class NetworkUtil {

	@Autowired
	protected HttpServletRequest request;

	/**
	 * 获取访问ip
	 * 
	 * @return
	 */
	public String getRemoteIP() {
		String ip = request.getHeader("x-forwarded-for"); // XFF头，它代表客户端，也就是HTTP的请求端真实的IP
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP"); // 代理客户端IP
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (ip != null && ip.length() > 0) {
			String[] ipArray = ip.split(",");
			if (ipArray != null && ipArray.length > 1) {
				return ipArray[0];
			}
			return ip;
		}

		return null;
	}

	/**
	 * 获取请求来源
	 * 
	 * @return
	 */
	public String getReferer() {
		return request.getHeader("Referer");
	}

}
