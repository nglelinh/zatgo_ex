package com.zatgo.zup.wechat.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * @Author wangyucong
 * @Date 2019/4/8 10:46
 */
public class WxJsapiTicketData implements Serializable {

    private static final long serialVersionUID = -1L;

    private String ticket;

    @JSONField(name = "expires_in")
    private Integer expiresIn;

    @JSONField(name = "errcode")
    private String errCode;

    @JSONField(name = "errmsg")
    private String errMsg;

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}