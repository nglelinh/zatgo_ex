package com.zatgo.zup.wechat.service.impl;

import com.alibaba.fastjson.JSON;
import com.ykb.mall.common.model.WechatMessageRecord;
import com.ykb.mall.common.model.WechatMessageTemplate;
import com.ykb.mall.common.model.WechatSendMessageData;
import com.ykb.mall.common.utils.WechatMsgTemplateConstants;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.wechat.entity.WechatConstant;
import com.zatgo.zup.wechat.mapper.WechatMessageRecordMapper;
import com.zatgo.zup.wechat.mapper.WechatMessageTemplateMapper;
import com.zatgo.zup.wechat.model.CommonResult;
import com.zatgo.zup.wechat.service.IWechatAccessTokenService;
import com.zatgo.zup.wechat.service.WechatMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@Service
public class WechatMessageServiceImpl implements WechatMessageService {

	private static final Logger LOG = LoggerFactory.getLogger(WechatMessageServiceImpl.class);

	private static final String URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s";
	
	@Value("${mall.web.url}")
	private String mallWebUrl;

	@Autowired
	private WechatMessageRecordMapper wechatMessageRecordMapper;

	@Autowired
	private WechatMessageTemplateMapper wechatMessageTemplateMapper;

	@Autowired
	private IWechatAccessTokenService iWechatAccessTokenService;

	@Autowired
	private OkHttpService okHttpService;

	@Autowired
	private WechatConstant wechatConstant;

	@Override
	public void sendWechatMessage(Map<String, String> params, String openId, Integer templateId) {
		WechatMessageTemplate template = wechatMessageTemplateMapper.selectByPrimaryKey(templateId);
		if (template == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "微信模板不存在");
		}

		String content = template.getContent();
		if(params == null) {
			params = new HashMap<>();
		}
		params.put("#OPENID#", openId);
		params.put("#TEMPLATE_ID#", template.getTemplateId());
		Set<Entry<String, String>> entries = params.entrySet();
		for (Entry<String, String> entry : entries) {
			content = content.replaceAll(entry.getKey(), entry.getValue());
		}

		WechatSendMessageData messageData = okHttpService.post(String.format(URL, iWechatAccessTokenService.getAccessToken(wechatConstant.weixinOfficialAcctAppId, wechatConstant.weixinOfficialAcctKey)),
				JSON.parseObject(content), WechatSendMessageData.class);
		if (!"0".equals(messageData.getErrCode())) {
			LOG.error("下发微信消息失败：{}", messageData);
			throw new BusinessException(BusinessExceptionCode.WEIXIN_HTTP_ERROR, messageData.getErrMsg());
		}

		WechatMessageRecord record = new WechatMessageRecord();
		record.setMsg(content);
		record.setMsgId(messageData.getMsgId());
		record.setOpenId(openId);
		record.setTemplateId(template.getId());
		wechatMessageRecordMapper.insertSelective(record);
	}

	@Override
	public CommonResult sendWechatSupportMsg(String openId) {
		try {
			Map<String, String> params = new HashMap<>();
			params.put("#FIRST#", "恭喜你，3位好友已经帮你完成助力啦~");
			params.put("#KEYWORD1#", "重返18岁，18元畅游乌镇");
			params.put("#KEYWORD2#", "6月16日");
			params.put("#KEYWORD3#", "乌镇");
			params.put("#REMARK#", "请耐心等待系统筛选，期待我们能在乌镇面基~");
			sendWechatMessage(params, openId, WechatMsgTemplateConstants.WECHAT_MSG_SUPPORT_ID);
			return new CommonResult().success("");
		} catch (Exception e) {
			LOG.error("发送助力通知失败：", e);
			return new CommonResult().failed(e.getMessage());
		}
	}

	@Override
	public CommonResult sendWechatSupportSuccessMsg(String openId, String id) {
		
		try {
			Map<String, String> params = new HashMap<>();
			params.put("#FIRST#", "恭喜你已成功入选！");
			params.put("#KEYWORD1#", "重返18岁，18元畅游乌镇");
			params.put("#KEYWORD2#", "6月16日");
			params.put("#KEYWORD3#", "一块伴旅行");
			params.put("#REMARK#", "我在乌镇等你，快来进群集合吧~\n" + 
					"（付款有效期为24小时，请点击支付）");
			params.put("#URL#", mallWebUrl + "/huodong/detail?id=" + id);
			sendWechatMessage(params, openId, WechatMsgTemplateConstants.WECHAT_MSG_SUPPORT_SUCCESS_ID);
			return new CommonResult().success("");
		} catch (Exception e) {
			LOG.error("发送助力通知失败：", e);
			return new CommonResult().failed(e.getMessage());
		}
		
	}

}
