package com.zatgo.zup.wechat.sdk;

import org.apache.http.client.HttpClient;

/**
 * 常量
 */
public class WXPayConstants {

    public enum SignType {
        MD5, HMACSHA256
    }
    
    public enum TRADE_TYPE {
    	JSAPI, NATIVE, APP, MWEB
    }

    public static final String DOMAIN_API = "api.mch.weixin.qq.com";
    public static final String DOMAIN_API2 = "api2.mch.weixin.qq.com";
    public static final String DOMAIN_APIHK = "apihk.mch.weixin.qq.com";
    public static final String DOMAIN_APIUS = "apius.mch.weixin.qq.com";


    public static final String FAIL     = "FAIL";
    public static final String SUCCESS  = "SUCCESS";
    public static final String HMACSHA256 = "HMAC-SHA256";
    public static final String MD5 = "MD5";

    public static final String FIELD_SIGN = "sign";
    public static final String FIELD_SIGN_TYPE = "sign_type";

    public static final String WXPAYSDK_VERSION = "WXPaySDK/3.0.9";
    public static final String USER_AGENT = WXPAYSDK_VERSION +
            " (" + System.getProperty("os.arch") + " " + System.getProperty("os.name") + " " + System.getProperty("os.version") +
            ") Java/" + System.getProperty("java.version") + " HttpClient/" + HttpClient.class.getPackage().getImplementationVersion();

    public static final String MICROPAY_URL_SUFFIX     = "/pay/micropay";
    public static final String UNIFIEDORDER_URL_SUFFIX = "/pay/unifiedorder";
    public static final String ORDERQUERY_URL_SUFFIX   = "/pay/orderquery";
    public static final String REVERSE_URL_SUFFIX      = "/secapi/pay/reverse";
    public static final String CLOSEORDER_URL_SUFFIX   = "/pay/closeorder";
    public static final String REFUND_URL_SUFFIX       = "/secapi/pay/refund";
    public static final String REFUNDQUERY_URL_SUFFIX  = "/pay/refundquery";
    public static final String DOWNLOADBILL_URL_SUFFIX = "/pay/downloadbill";
    public static final String REPORT_URL_SUFFIX       = "/payitil/report";
    public static final String SHORTURL_URL_SUFFIX     = "/tools/shorturl";
    public static final String AUTHCODETOOPENID_URL_SUFFIX = "/tools/authcodetoopenid";

    // sandbox
    public static final String SANDBOX_MICROPAY_URL_SUFFIX     = "/sandboxnew/pay/micropay";
    public static final String SANDBOX_UNIFIEDORDER_URL_SUFFIX = "/sandboxnew/pay/unifiedorder";
    public static final String SANDBOX_ORDERQUERY_URL_SUFFIX   = "/sandboxnew/pay/orderquery";
    public static final String SANDBOX_REVERSE_URL_SUFFIX      = "/sandboxnew/secapi/pay/reverse";
    public static final String SANDBOX_CLOSEORDER_URL_SUFFIX   = "/sandboxnew/pay/closeorder";
    public static final String SANDBOX_REFUND_URL_SUFFIX       = "/sandboxnew/secapi/pay/refund";
    public static final String SANDBOX_REFUNDQUERY_URL_SUFFIX  = "/sandboxnew/pay/refundquery";
    public static final String SANDBOX_DOWNLOADBILL_URL_SUFFIX = "/sandboxnew/pay/downloadbill";
    public static final String SANDBOX_REPORT_URL_SUFFIX       = "/sandboxnew/payitil/report";
    public static final String SANDBOX_SHORTURL_URL_SUFFIX     = "/sandboxnew/tools/shorturl";
    public static final String SANDBOX_AUTHCODETOOPENID_URL_SUFFIX = "/sandboxnew/tools/authcodetoopenid";
    
   /* public final static String device_info  = "device_info";
	public final static String body  = "body";
	public final static String detail  = "detail";
	public final static String attach  = "attach";
	public final static String out_trade_no  = "out_trade_no";
	public final static String fee_type  = "fee_type";
	public final static String total_fee  = "total_fee";
	public final static String spbill_create_ip  = "spbill_create_ip";
	public final static String time_start  = "time_start";
	public final static String time_expire  = "time_expire";
	public final static String goods_tag  = "goods_tag";
	public final static String notify_url  = "notify_url";
	public final static String trade_type  = "trade_type";
	public final static String product_id  = "product_id";
	public final static String limit_pay  = "limit_pay";
	public final static String openid  = "openid";
	public final static String scene_info  = "scene_info";
	public final static String nonce_str  = "nonce_str";
	public final static String sign_type  = "sign_type";*/
	
	public static final String FIELD_NONCE_STR = "nonce_str";
	public final static String FIELD_BODY = "body";
	public final static String FIELD_OUT_TRADE_NO = "out_trade_no";
	public final static String FIELD_TOTAL_FEE = "total_fee";
	public final static String FIELD_REFUND_FEE = "refund_fee";
	public final static String FIELD_SPBILL_CREATE_IP = "spbill_create_ip";
	public final static String FIELD_NOTIFY_URL = "notify_url";
	public final static String FIELD_TRADE_TYPE = "trade_type";
	public final static String FIELD_OPENID = "openid";
	public final static String FIELD_APPID = "appid";
	public final static String FIELD_SCENE_INFO = "scene_info";
	public final static String FIELD_TIME_EXPIRE = "time_expire";
	
	public final static String FIELD_PREPAY_ID = "prepay_id";
	public final static String FIELD_MWEB_URL = "mweb_url";
	
	public final static String FIELD_RETURN_CODE = "return_code";
	public final static String FIELD_RESULT_CODE = "result_code";
	public final static String FIELD_RETURN_MSG = "return_msg";
	public final static String FIELD_TRANSACTION_ID = "transaction_id";
	public final static String FIELD_REFUND_ID = "refund_id";
	public final static String FIELD_OUT_REFUND_NO = "out_refund_no";
	
	
	public final static String FIELD_BODY_TRAVEL = "一块伴-旅游产品支付";
	public final static String FIELD_BODY_FLIGHT = "一块伴-机票产品支付";
	public final static String FIELD_BODY_HOTEL = "一块伴-酒店产品支付";
	public final static String FIELD_BODY_ARTICLE = "一块伴-文章产品支付";
	
	public static final String OK  = "OK";

}

