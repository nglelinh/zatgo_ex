package com.zatgo.zup.wechat.service.impl;

import com.ykb.mall.common.model.WechatMsgReply;
import com.ykb.mall.common.model.WechatMsgReplyExample;
import com.ykb.mall.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.wechat.entity.WechatMsg;
import com.zatgo.zup.wechat.mapper.WechatMsgReplyMapper;
import com.zatgo.zup.wechat.service.WechatMsgReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class WechatMsgReplyServiceImpl implements WechatMsgReplyService {

	@Autowired
	private WechatMsgReplyMapper wechatMsgReplyMapper;

	@Autowired
	private RedisUtils redisUtils;

	@Override
	public String selectWechatMsgReply(WechatMsg.MsgType msgType, WechatMsg.Event event, String key) {

		String msgKey = String.format(RedisKeyConstants.WECHAT_MSG_REPLY, msgType.getType(),
				event == null ? "" : event.getEvent(), key);

		boolean hasKey = redisUtils.hasKey(msgKey);
		if (hasKey) {
			return redisUtils.get(msgKey, String.class);
		}

		WechatMsgReplyExample example = new WechatMsgReplyExample();
		WechatMsgReplyExample.Criteria criteria = example.createCriteria().andMsgTypeEqualTo(msgType.getType()).andKeyEqualTo(key);
		if (event != null) {
			criteria.andEventEqualTo(event.getEvent());
		}

		String reply = null;
		List<WechatMsgReply> replies = wechatMsgReplyMapper.selectByExample(example);
		if (!CollectionUtils.isEmpty(replies)) {
			reply = replies.get(0).getReply();
			redisUtils.put(msgKey, reply);
		}

		return reply;
	}

}
