package com.zatgo.zup.wechat.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "xml")
public class WechatMsg {

	public enum MsgType {
		TEXT("text"), IMAGE("image"), VOICE("voice"), VIDEO("video"), SHORTVIDEO("shortvideo"), LOCATION(
				"location"), LINK("link"), EVENT("event");

		private String type;

		private MsgType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}

	public enum Event {
		SUBSCRIBE("subscribe"), UNSUBSCRIBE("unsubscribe"), SCAN("SCAN"), LOCATION("LOCATION"), CLICK("CLICK"), VIEW(
				"VIEW"),TEMPLATESENDJOBFINISH("TEMPLATESENDJOBFINISH");

		private String event;

		private Event(String event) {
			this.event = event;
		}

		public String getEvent() {
			return event;
		}
	}

	public enum Status {
		SUCCESS("success"), USERBOLOCK("failed:user block"),SYSTEMFAILED("failed: system failed");

		private String Status;

		private Status(String Status) {
			this.Status = Status;
		}

		public String getStatus() {
			return Status;
		}
	}
	@XmlElement(name = "ToUserName")
	private String toUserName;

	@XmlElement(name = "FromUserName")
	private String fromUserName;

	@XmlElement(name = "CreateTime")
	private String createTime;

	@XmlElement(name = "MsgType")
	private String msgType;

	@XmlElement(name = "Content")
	private String content;

	@XmlElement(name = "PicUrl")
	private String picUrl;

	@XmlElement(name = "MediaId")
	private String mediaId;

	@XmlElement(name = "Format")
	private String format;

	@XmlElement(name = "Recognition")
	private String recognition;

	@XmlElement(name = "ThumbMediaId")
	private String thumbMediaId;

	@XmlElement(name = "Location_X")
	private String locationX;

	@XmlElement(name = "Location_Y")
	private String locationY;

	@XmlElement(name = "Scale")
	private String scale;

	@XmlElement(name = "Label")
	private String label;

	@XmlElement(name = "Title")
	private String title;

	@XmlElement(name = "Description")
	private String description;

	@XmlElement(name = "Url")
	private String url;

	@XmlElement(name = "Event")
	private String event;

	@XmlElement(name = "EventKey")
	private String eventKey;

	@XmlElement(name = "Ticket")
	private String ticket;

	@XmlElement(name = "Latitude")
	private String latitude;

	@XmlElement(name = "Longitude")
	private String longitude;

	@XmlElement(name = "Precision")
	private String precision;

	@XmlElement(name = "MsgID")
	private String msgId;

	@XmlElement(name = "Status")
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getRecognition() {
		return recognition;
	}

	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public String getLocationX() {
		return locationX;
	}

	public void setLocationX(String locationX) {
		this.locationX = locationX;
	}

	public String getLocationY() {
		return locationY;
	}

	public void setLocationY(String locationY) {
		this.locationY = locationY;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

}
