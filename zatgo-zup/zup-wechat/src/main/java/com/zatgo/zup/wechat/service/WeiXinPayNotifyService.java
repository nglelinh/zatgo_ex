package com.zatgo.zup.wechat.service;

import java.util.Map;

public interface WeiXinPayNotifyService {

	/**
	 * 微信支付通知处理
	 * 
	 * @param xmlMap
	 * @return
	 */
	public void wxPayNotify(Map<String, String> xmlMap) throws Exception;

}
