package com.zatgo.zup.wechat.entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2019/6/13.
 */

@Component
public class WechatConstant {

    @Value("${wx.pay.appId}")
    public String weixinOfficialAcctAppId;
    @Value("${wx.appSecret}")
    public String weixinOfficialAcctKey;
}
