package com.zatgo.zup.wechat.controller;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.wechat.entity.MsgToken;
import com.zatgo.zup.wechat.entity.WechatMsg;
import com.zatgo.zup.wechat.model.CommonResult;
import com.zatgo.zup.wechat.remoteservice.ActivityRemoteService;
import com.zatgo.zup.wechat.remoteservice.UserRemoteService;
import com.zatgo.zup.wechat.service.IWechatMsgService;
import com.zatgo.zup.wechat.service.WechatMaterialService;
import com.zatgo.zup.wechat.service.WechatMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/wechat/wx/msg")
public class WechatMsgController {
	private static final Logger logger = LoggerFactory.getLogger(WechatMsgController.class);

	private static final String WECHAT_MSG_KEY = "WECHAT_MSG_KEY_:%s";

	@Resource
	private IWechatMsgService wechatMsgService;

	@Autowired
	private ActivityRemoteService activityRemoteService;

	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	private WechatMaterialService wechatMaterialService;

	@Autowired
	private WechatMessageService wechatMessageService;

	@Autowired
	private UserRemoteService userRemoteService;

	@GetMapping("/support/{openId}")
	public CommonResult sendWechatSupportMsg(@PathVariable("openId") String openId) {
		return wechatMessageService.sendWechatSupportMsg(openId);
	}

	@GetMapping("/support/success/{openId}/{id}")
	public CommonResult sendWechatSupportSuccessMsg(@PathVariable("openId") String openId, @PathVariable("id") String id) {
		return wechatMessageService.sendWechatSupportSuccessMsg(openId, id);
	}

	@RequestMapping(value = "/token/{cloudUserId}", method = RequestMethod.GET)
	@ResponseBody
	public String msgToken(MsgToken token, @PathVariable("cloudUserId") String cloudUserId) {

		if (StringUtils.isEmpty(token.getSignature()) || StringUtils.isEmpty(token.getTimestamp())
				|| StringUtils.isEmpty(token.getNonce()) || StringUtils.isEmpty(token.getEchostr())) {
			token.setEchostr("");
			return token.getEchostr();
		}

		return wechatMsgService.validateWxToken(token, cloudUserId).getEchostr();
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public String msgToken(@RequestBody WechatMsg msg) {
		return wechatMsgService.wxMsg(msg);
	}

	@PostMapping(value = "/token/{cloudUserId}")
	@ResponseBody
	public String attention(@RequestBody WechatMsg msg) {
		try {
			logger.info("微信公众号关注通知参数：{}", JSONObject.toJSONString(msg));
			String msgKey;
			String msgId = msg.getMsgId();
			if(!StringUtils.isEmpty(msgId)) {
				msgKey = String.format(WECHAT_MSG_KEY, msgId);
			} else {
				msgKey = String.format(WECHAT_MSG_KEY, msg.getFromUserName() + "_" + msg.getCreateTime());
			}

			boolean hasKey = redisUtils.hasKey(msgKey);
			logger.info("haskeu：{}",hasKey );
			if(hasKey) {
				return "success";
			}



			redisUtils.put(msgKey, msgKey, 1, TimeUnit.DAYS);

			String event = msg.getEvent();
			String fromWXOpenId = msg.getFromUserName();
			String eventKey = msg.getEventKey();
			if (StringUtils.isEmpty(eventKey))
				eventKey = "";
			String[] split = eventKey.split("_");
			if (WechatMsg.Event.SUBSCRIBE.getEvent().equals(event) && split.length > 1){
				return activityRemoteService.attention(eventKey, fromWXOpenId, msg.getToUserName());
			} else {
				return wechatMsgService.wxMsg(msg);
			}

//            if (WechatMsg.Event.SCAN.getEvent().equals(event)){
//                loginInfo = getUserInfo(fromWXOpenId);
//                attentionNotifyService.cancel(loginInfo, eventKey);
//            }

		} catch (Exception e){
			logger.error("", e);
		}

		return "success";
	}

	@RequestMapping(value = "/material", method = RequestMethod.GET)
	@ResponseBody
	public String material() {
		return wechatMaterialService.getWechatMaterialList();
	}

}
