package com.zatgo.zup.wechat.service;


import com.zatgo.zup.wechat.model.CommonResult;

import java.math.BigDecimal;

public interface WeiXinPayService {
	
	/**
	 * 获取订单微信支付参数
	 * @param orderId
	 * @param wxPayType
	 * @return
	 */
	CommonResult getOrderPayWeixinParams(Long orderId, Integer wxPayType, String wxOpenId, String userId);
	
	/**
	 * 微信退款
	 * @param orderNumber
	 * @param refundAmount
	 */
	void weixinRefund(String orderNumber, BigDecimal refundAmount) throws Exception;

	/**
	 * 获取二维码
	 * @param cloudUserId
	 * @return
	 */
    String getQRCode(String cloudUserId, String sceneStr, String userId);
}
