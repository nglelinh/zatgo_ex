package com.zatgo.zup.wechat.sdk;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;

/**
 * 微信公众号支付配置
 * @author Administrator
 *
 */
@Component
public class WeiXinPublicPayConfig extends WXPayConfig {
	
	/**
	 * 公众号appid
	 */
	@Value("${wx.pay.appId}")
	private String wxPayAppId;
	
	@Value("${wx.pay.mchId}")
	private String wxPayMchId;
	
	@Value("${wx.pay.key}")
	private String wxPayKey;

	@Override
	String getAppID() {
		return wxPayAppId;
	}

	@Override
	String getMchID() {
		return wxPayMchId;
	}

	@Override
	String getKey() {
		return wxPayKey;
	}

	@Override
	InputStream getCertStream() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	IWXPayDomain getWXPayDomain() {
		// TODO Auto-generated method stub
		return null;
	}

}
