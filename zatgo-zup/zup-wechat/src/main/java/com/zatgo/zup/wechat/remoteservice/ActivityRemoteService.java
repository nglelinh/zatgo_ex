package com.zatgo.zup.wechat.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2019/6/11.
 */

@FeignClient("zup-activity")
public interface ActivityRemoteService {

    @PostMapping(value = "/activity/internal/orderCallBack")
    @ResponseBody
    ResponseData<Boolean> orderCallBack(@RequestParam("activityId") String activityId,
                                        @RequestParam("userId") String userId,
                                        @RequestParam("orderId") String orderId,
                                        @RequestParam("isSuccess") Boolean isSuccess);


    @PostMapping(value = "/activity/wx/remote/attention")
    String attention(@RequestParam("eventKey") String eventKey,
                            @RequestParam("fromWXOpenId") String fromWXOpenId,
                            @RequestParam("toWXOpenId") String toWXOpenId);

    @PostMapping(value = "/activity/groupBooking/internal/updateGroupStatus")
    void updateGroupStatus(@RequestParam("activityId") String activityId, @RequestParam("orderId") Long orderId);
}
