package com.zatgo.zup.wechat.utils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Xml2BeanUtils {
	
	private static Logger logger = LoggerFactory.getLogger(Xml2BeanUtils.class.getName());
	
	/**
	 * xml转java Bean
	 * @param str
	 * @param clazz
	 * @return
	 */
	public static <T> T xmlToObject(String str,Class<T> clazz) {
		JAXBContext context = null;		   
		try {
			context = JAXBContext.newInstance(clazz);
			StringReader reader = new StringReader(str);
	        Unmarshaller unmar = context.createUnmarshaller();
			return (T)unmar.unmarshal(reader);
		} catch (JAXBException e) {
			logger.error("xml转bean异常",e);
		}
		return null;
	}

	public static String convertToXml(Object obj, String encoding) {  
        String result = null;  
        StringWriter writer = new StringWriter();  
        try {  
            JAXBContext context = JAXBContext.newInstance(obj.getClass());  
            Marshaller marshaller = context.createMarshaller();  
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);  
            marshaller.setProperty(Marshaller.JAXB_ENCODING, encoding);  
            marshaller.marshal(obj, writer);  
            
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);//生成
            result = writer.toString();  
        } catch (Exception e) {  
        	logger.error("bean转xml异常",e);
        } finally {
        	if(writer != null){
        		try {
					writer.close();
				} catch (IOException e) {
					logger.error("bean转xml异常",e);
				}
        	}
        }
        result = result.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "").trim();
        logger.info("dida请求参数：\n" + result);
        return result;  
    }

	/**
	 * java bean 转xml 包含头部信息
	 * @param obj
	 * @param encoding
	 * @return
	 */
	public static String objectToXml(Object obj, String encoding) {
		String result = null;
		StringWriter writer = new StringWriter();
		try {
			JAXBContext context = JAXBContext.newInstance(obj.getClass());
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, encoding);
			marshaller.marshal(obj, writer);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);//生成
			result = writer.toString();
		} catch (Exception e) {
			logger.error("bean转xml异常",e);
		} finally {
			if(writer != null){
				try {
					writer.close();
				} catch (IOException e) {
					logger.error("bean转xml异常",e);
				}
			}
		}
		return result;
	}

	public static Map<String, String> xmlToMap(String xml) {
		Map<String, String> map = new HashMap<String, String>();
		Document doc;
		try {
			doc = DocumentHelper.parseText(xml);
			Element el = doc.getRootElement();
			map = recGetXmlElementValue(el, map);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return map;
	}

	private static Map<String, String> recGetXmlElementValue(Element ele, Map<String, String> map) {
		List<Element> eleList = ele.elements();
		if (eleList.size() == 0) {
			map.put(ele.getName(), ele.getTextTrim());
			return map;
		} else {
			for (Iterator<Element> iter = eleList.iterator(); iter.hasNext();) {
				Element innerEle = iter.next();
				recGetXmlElementValue(innerEle, map);
			}
			return map;
		}
	}

}
