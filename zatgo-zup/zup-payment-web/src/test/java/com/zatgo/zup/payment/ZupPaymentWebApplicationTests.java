package com.zatgo.zup.payment;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.model.payment.PayMoney;
import com.zatgo.zup.payment.service.OrderPayService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZupPaymentWebApplicationTests {
	@Autowired
	private OrderPayService service;

	@Autowired
	private RedisTemplate redisTemplate;
	public final static String HASH_KEY_CALLBACKTIMES  = "callbackTimes";
	public final static String HASH_KEY_EXPIRETIME  = "expireTime";
	public final static long EXPIRETIME  = 60000;
	public final static String KEY  = "423432";
	@Test
	public void contextLoads() {
		String userId = "cae4d08b-2123-4826-ae68-374c37e5b7bf";
		String type = "ZAT";
		PayMoney payMoney = service.computeMoney(userId, new BigDecimal(800), type);
		System.out.println(JSON.toJSONString(payMoney));
	}

	@Test
	public void redisTest(){
		String s = (String) redisTemplate.opsForHash().get(KEY, HASH_KEY_CALLBACKTIMES);
		System.out.println(s);
	}

}
