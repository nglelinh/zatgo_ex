package com.zatgo.zup.payment.service.impl;

import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.payment.service.AlipayService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class AlipayServiceImplTest {
	@Autowired
	private AlipayService service;
	@Test
	public void mwebPay() throws Exception {
		PayParams payParams = new PayParams();
		payParams.setCheckoutOrderId("965cac75332643c681a8c890f8e3bc54");
		payParams.setCashAmount(new BigDecimal(1));
		String s = service.mwebPay(payParams);
	}

}