package com.zatgo.zup.payment.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.Page;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.CheckoutOrderData;
import com.zatgo.zup.common.model.OrderStatus;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.payment.PrepayInfo;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.payment.entity.AppList;
import com.zatgo.zup.payment.entity.CheckoutOrder;
import com.zatgo.zup.payment.entity.Order;
import com.zatgo.zup.payment.entity.PageData;
import com.zatgo.zup.payment.entity.SignOrderQueryPageParams;
import com.zatgo.zup.payment.entity.SignOrderQueryParams;
import com.zatgo.zup.payment.service.CheckoutOrderService;
import com.zatgo.zup.payment.service.MerchantAppService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/pay/orders",description = "收银台订单相关",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(value = "/pay/orders")
public class OrderController extends BaseController{

	@Autowired
	private CheckoutOrderService checkoutOrderService;

	@Autowired
	private MerchantAppService merchantAppService;
	@Value("${api-rsa.privateKey}")
	private String privateKey;
	@ApiOperation(value = "获取订单状态信息")
	@RequestMapping(value = "/status/{orderId}",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<OrderStatus> selectOrderByUserAndOrderId(@PathVariable("orderId") String orderId){
		AuthUserInfo userInfo = getUserInfo();
		CheckoutOrder checkoutOrder = checkoutOrderService.selectOrderByOrderIdAndPayUserId(userInfo.getUserId(), orderId);
		OrderStatus orderStatus = new OrderStatus();
		if(checkoutOrder == null){
			orderStatus.setOrderStatus(BusinessEnum.OrderStatus.PAYING.getCode());
			orderStatus.setThirdPayStatus(BusinessEnum.OrderStatus.PAYING.getCode());
		} else{
			orderStatus.setOrderStatus(checkoutOrder.getOrderStatus());
			orderStatus.setThirdPayStatus(checkoutOrder.getThirdPayStatus());
			orderStatus.setCash(checkoutOrder.getCashAmount());
			orderStatus.setToken(checkoutOrder.getOrderMoney());
		}
		return BusinessResponseFactory.createSuccess(orderStatus);
	}

	@ApiOperation(value = "获取订单信息")
	@RequestMapping(value = "/pageinfo/{pageNo}/{pageSize}",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<PageInfo<CheckoutOrderData>> selectOrderByPage(@PathVariable("pageNo") String pageNo, 
			@PathVariable("pageSize") String pageSize){
		AuthUserInfo userInfo = getUserInfo();
		return BusinessResponseFactory.createSuccess(
				checkoutOrderService.selectOrderByPage(userInfo.getUserId(), pageNo, pageSize));
	}
	@RequestMapping(value = "/payerror",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<List<CheckoutOrder>> selectPayErrorOrder(){
		return BusinessResponseFactory.createSuccess(
				checkoutOrderService.selectPayErrorOrder());
	}
	@RequestMapping(value = "/refunderror",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<List<CheckoutOrder>> selectRefundErrorOrder(){
		return BusinessResponseFactory.createSuccess(
				checkoutOrderService.selectRefundErrorOrder());
	}
	@ApiOperation(value = "根据prepayid获取订单信息")
	@RequestMapping(value = "/prepay/{id}",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<PrepayInfo> selectOrderByPrePayId(@PathVariable("id") String prePayId){
		return BusinessResponseFactory.createSuccess(
				checkoutOrderService.selectOrderByPrePayId(prePayId));
	}

	@ApiOperation(value = "验证prepayid")
	@RequestMapping(value = "/check/prepay/{id}",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Boolean> checkPrepayId(@PathVariable("id") String prePayId){
		return BusinessResponseFactory.createSuccess(
				checkoutOrderService.checkPrepayId(prePayId));
	}

	@ApiOperation(value = "商户订单查询")
	@RequestMapping(value = "/sign/orderquery/tradeno",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Order> signOrderQuery(@RequestBody SignOrderQueryParams params){
		String id = StringUtils.isEmpty(params.getOutTradeNo())?params.getTradeNo():params.getOutTradeNo();
		String appId = params.getAppId();
		AppList appList = merchantAppService.selectByAppId(appId);
		if(appList == null){
		    throw new BusinessException(BusinessExceptionCode.USER_CONFIG_ERROR);
		}
		CheckoutOrder checkoutOrder = checkoutOrderService.signOrderQuery(appList.getUserId(), id);
		if(checkoutOrder == null){
		    throw new BusinessException(BusinessExceptionCode.RECORD_NOT_EXIST);
		}
		Order order = new Order();
		order.setBody(checkoutOrder.getOrderMemo());
		order.setCoinAmount(checkoutOrder.getCoinType());
		order.setCoinNetworkType(checkoutOrder.getCoinNetworkType());
		order.setMixedCoinNetworkType(checkoutOrder.getMixCoinNetworkType());
		order.setCoinType(checkoutOrder.getCoinType());
		order.setTradeNo(checkoutOrder.getOrderId());
		order.setOutTradeNo(checkoutOrder.getOrderFlowCode());
		order.setNotifyUrl(checkoutOrder.getNotifyUrl());
		order.setSubject(checkoutOrder.getOrderName());
		order.setMixedCoinType(checkoutOrder.getMixCoinType());
		order.setOrder_time(DateTimeUtils.parseDateToString(checkoutOrder.getCreateDate(),DateTimeUtils.PATTEN_YYYYMMDDHHMMSS));
		Date paymentDate = checkoutOrder.getPaymentDate();
		if(paymentDate != null){
			order.setPay_time(DateTimeUtils.parseDateToString(paymentDate,DateTimeUtils.PATTEN_YYYYMMDDHHMMSS));
		}
		
		//返回状态处理
		Byte coinPayStatus = checkoutOrder.getOrderStatus();
		Byte cnyPayStatus = checkoutOrder.getThirdPayStatus();
		String resultCode;
		if(coinPayStatus.equals(BusinessEnum.OrderStatus.PAID.getCode()) 
				&& cnyPayStatus.equals(BusinessEnum.OrderStatus.PAID.getCode())) {
			resultCode = BusinessEnum.ResultCode.SUCCESS.getCode();
		}else if(coinPayStatus.equals(BusinessEnum.OrderStatus.PAYING.getCode()) 
				&& cnyPayStatus.equals(BusinessEnum.OrderStatus.PAYING.getCode())) {
			resultCode = BusinessEnum.ResultCode.PROCESSING.getCode();
		}else {
			resultCode = BusinessEnum.ResultCode.FAIL.getCode();
		}
		
		order.setResultCode(resultCode);
		return BusinessResponseFactory.createSuccess(order);
	}

	@ApiOperation(value = "商户订单查询")
	@RequestMapping(value = "/sign/orderquery/page",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<PageData<Order>> signOrderQueryPage(@RequestBody SignOrderQueryPageParams params){
		String appId = params.getAppId();
		AppList appList = merchantAppService.selectByAppId(appId);
		if(appList == null){
			throw new BusinessException(BusinessExceptionCode.USER_CONFIG_ERROR);
		}
		Page<CheckoutOrder> checkoutOrders = checkoutOrderService.selectOrderByUserIdAndTime(appList.getUserId(), params.getPageNo()
				, params.getPageSize(), params.getBusiStartDate(), params.getBusiEndDate());
		List<Order> orders = new ArrayList<>();
		if(!CollectionUtils.isEmpty(checkoutOrders)){
			for (CheckoutOrder checkoutOrder : checkoutOrders) {
				Order order = new Order();
				order.setBody(checkoutOrder.getOrderMemo());
				order.setCoinAmount(checkoutOrder.getCoinType());
				order.setCoinNetworkType(checkoutOrder.getCoinNetworkType());
				order.setMixedCoinNetworkType(checkoutOrder.getMixCoinNetworkType());
				order.setCoinType(checkoutOrder.getCoinType());
				order.setTradeNo(checkoutOrder.getOrderId());
				order.setOutTradeNo(checkoutOrder.getOrderFlowCode());
				order.setNotifyUrl(checkoutOrder.getNotifyUrl());
				order.setSubject(checkoutOrder.getOrderName());
				order.setMixedCoinType(checkoutOrder.getMixCoinType());
				order.setOrder_time(DateTimeUtils.parseDateToString(checkoutOrder.getCreateDate(),DateTimeUtils.PATTEN_YYYYMMDDHHMMSS));
				Date paymentDate = checkoutOrder.getPaymentDate();
				if(paymentDate != null){
					order.setPay_time(DateTimeUtils.parseDateToString(paymentDate,DateTimeUtils.PATTEN_YYYYMMDDHHMMSS));
				}

				//返回状态处理
				Byte coinPayStatus = checkoutOrder.getOrderStatus();
				Byte cnyPayStatus = checkoutOrder.getThirdPayStatus();
				String resultCode;
				if(coinPayStatus.equals(BusinessEnum.OrderStatus.PAID.getCode()) 
						&& cnyPayStatus.equals(BusinessEnum.OrderStatus.PAID.getCode())) {
					resultCode = BusinessEnum.ResultCode.SUCCESS.getCode();
				}else if(coinPayStatus.equals(BusinessEnum.OrderStatus.PAYING.getCode()) 
						&& cnyPayStatus.equals(BusinessEnum.OrderStatus.PAYING.getCode())) {
					resultCode = BusinessEnum.ResultCode.PROCESSING.getCode();
				}else {
					resultCode = BusinessEnum.ResultCode.FAIL.getCode();
				}
				
				order.setResultCode(resultCode);
				orders.add(order);
			}
		}
		PageData<Order> orderPageData = new PageData<Order>(String.valueOf(checkoutOrders.getPageNum()),
				String.valueOf(checkoutOrders.getPageSize()),String.valueOf(checkoutOrders.getTotal()),orders);
		return BusinessResponseFactory.createSuccess(orderPageData);
	}


}
