package com.zatgo.zup.payment.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CheckoutOrderData;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;
import com.zatgo.zup.common.model.payment.PayMoney;
import com.zatgo.zup.common.model.payment.PrepayInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.payment.entity.CheckoutOrder;
import com.zatgo.zup.payment.mapper.CheckoutOrderMapper;
import com.zatgo.zup.payment.remoteservice.CouponsRemoteService;
import com.zatgo.zup.payment.remoteservice.UserRemoteService;
import com.zatgo.zup.payment.service.CheckoutOrderService;
import com.zatgo.zup.payment.service.OrderPayService;

@Component
public class CheckoutOrderServiceImpl implements CheckoutOrderService {

	private static final Logger logger = LoggerFactory.getLogger(CheckoutOrderServiceImpl.class);

	@Autowired
	private CheckoutOrderMapper checkoutOrderMapper;
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private OrderPayService orderPayService;
	@Autowired
	private CouponsRemoteService couponsRemoteService;
	@Autowired
	private UserRemoteService userRemoteService;
	@Override
	public CheckoutOrder selectOrderByUserAndOrderId(String userId, String orderId) {
		return checkoutOrderMapper.selectOrderByOrderIdAndUserId(userId,orderId);
	}
	@Override
	public PageInfo<CheckoutOrderData> selectOrderByPage(String userId,String pageNo, String pageSize) {
		PageHelper.startPage(Integer.valueOf(pageNo), Integer.valueOf(pageSize));
		Page<CheckoutOrder> pageData = checkoutOrderMapper.selectOrderByUserId(userId);
		
		List<CheckoutOrderData> list = new ArrayList<>();
		for(CheckoutOrder order:pageData) {
			CheckoutOrderData orderData = new CheckoutOrderData();
			try {
				BeanUtils.copyProperties(orderData, order);

				orderData.setCoinNetworkTypeEnum(order.getCoinNetworkType().toUpperCase());
				orderData.setMixCoinNetworkTypeEnum(order.getMixCoinNetworkType().toUpperCase());
				orderData.setCoinTypeEnum(order.getCoinType().toUpperCase());
				orderData.setMixCoinTypeEnum(order.getMixCoinType().toUpperCase());
				orderData.setOrderStatusEnum(BusinessEnum.OrderStatus.getEnumByType(order.getOrderStatus()));
				orderData.setThirdPayStatusEnum(BusinessEnum.OrderStatus.getEnumByType(order.getThirdPayStatus()));
				orderData.setThirdPayTypeEnum(BusinessEnum.ThirdPayType.getEnumByType(order.getThirdPayType()));
			} catch (Exception e) {
				logger.error("",e);
			}

			list.add(orderData);
		}
		
		
		PageInfo<CheckoutOrderData> pageInfoOrder =
				new PageInfo<CheckoutOrderData>(pageData.getPageNum(),pageData.getPageSize(),pageData.getTotal(),list);

		PageHelper.clearPage();
		return pageInfoOrder;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void newTransactionalUpdateCheckoutOrder(CheckoutOrder checkoutOrder) {
		checkoutOrderMapper.updateByPrimaryKeySelective(checkoutOrder);
	}

	@Override
	public List<CheckoutOrder> selectPayErrorOrder() {
		return null;
	}

	@Override
	public List<CheckoutOrder> selectRefundErrorOrder() {
		return null;
	}

	@Override
	public CheckoutOrder selectOrderByOrderIdAndPayUserId(String userId, String orderId) {
		return checkoutOrderMapper.selectOrderByOrderIdAndPayUserId(userId,orderId);
	}

	@Override
	public PrepayInfo selectOrderByPrePayId(String prepayId) {
		PrepayInfo prepayInfo = new PrepayInfo();
		String orderId = (String)redisTemplate.opsForValue().get(RedisKeyConstants.PREPAY_ + prepayId);
		if(StringUtils.isEmpty(orderId)){
		    throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		CheckoutOrder checkoutOrder = checkoutOrderMapper.selectByPrimaryKey(orderId);
		if(checkoutOrder == null){
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		ResponseData<UserData> receiptUserData = userRemoteService.getUserById(checkoutOrder.getReceiptUserId());
		if(!receiptUserData.isSuccessful() || receiptUserData.getData() == null) {
			throw new BusinessException(receiptUserData.getCode(),receiptUserData.getMessage());
		}
		
		//查询当前订单已使用过的优惠券
		List<UserCouponsModel> usedUserCouponsList = new ArrayList<>();
		if(checkoutOrder.getUsageCouponsStatus() != null 
				&& !checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.PAYING) 
				&& !checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.FAIL)
				&& checkoutOrder.getCouponsList() != null
				&& !checkoutOrder.getCouponsList().trim().equals("")){
			ResponseData<List<UserCouponsModel>>  userCouponsListResp = couponsRemoteService.getUsedUserCouponsByOrder(receiptUserData.getData().getCloudUserId(), checkoutOrder.getOrderId());
			if(!userCouponsListResp.isSuccessful()) {
				throw new BusinessException(userCouponsListResp.getCode(),userCouponsListResp.getMessage());
			}
			usedUserCouponsList = userCouponsListResp.getData();
		}
		
		
		PayMoney payMoney = orderPayService.computeMoney(checkoutOrder.getOrderId());
		CheckoutOrderData orderData = new CheckoutOrderData();
		
		try {
			BeanUtils.copyProperties(orderData, checkoutOrder);
		} catch (Exception e) {
			throw new BusinessException();
		}
		orderData.setUserCouponsList(usedUserCouponsList);
		prepayInfo.setOrder(orderData);
		prepayInfo.setPayMoney(payMoney);
		return prepayInfo;
	}

	@Override
	public Boolean checkPrepayId(String prepayId) {
		Boolean flag = false;
		String orderId = (String)redisTemplate.opsForValue().get(RedisKeyConstants.PREPAY_ + prepayId);
		if(!StringUtils.isEmpty(orderId)){
			flag = true;
		}
		return flag;
	}

	@Override
	public CheckoutOrder signOrderQuery(String userId, String id) {
		return checkoutOrderMapper.signOrderQuery(userId,id);
	}

	@Override
	public Page<CheckoutOrder> selectOrderByUserIdAndTime(String userId, String page, String size, String start, String end) {
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(size));
		Page<CheckoutOrder> checkoutOrders = checkoutOrderMapper.signOrderQueryPage(userId, start, end);

		PageHelper.clearPage();
		return checkoutOrders;
	}
}
