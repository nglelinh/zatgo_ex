package com.zatgo.zup.payment.entity;

import java.math.BigDecimal;
import java.util.Date;

public class AppList {
    private String appId;

    private String userId;

    private String appName;

    private String appImage;

    private String gatewayUrl;

    private String merchantPublicKey;

    private String merchantGatewayUrl;

    private String merchantCallbackUrl;

    private Byte status;

    private Date createTime;

    private Date updateTime;

    private BigDecimal serviceChargePercent;

    private String appCategoryName;

    private Byte appPayType;

    private Byte appGotoType;

    private String appGotoAddress;

    private String cloudUserId;

    private String appAuth;

    private Byte language;

    private Integer sort;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName == null ? null : appName.trim();
    }

    public String getAppImage() {
        return appImage;
    }

    public void setAppImage(String appImage) {
        this.appImage = appImage == null ? null : appImage.trim();
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl == null ? null : gatewayUrl.trim();
    }

    public String getMerchantPublicKey() {
        return merchantPublicKey;
    }

    public void setMerchantPublicKey(String merchantPublicKey) {
        this.merchantPublicKey = merchantPublicKey == null ? null : merchantPublicKey.trim();
    }

    public String getMerchantGatewayUrl() {
        return merchantGatewayUrl;
    }

    public void setMerchantGatewayUrl(String merchantGatewayUrl) {
        this.merchantGatewayUrl = merchantGatewayUrl == null ? null : merchantGatewayUrl.trim();
    }

    public String getMerchantCallbackUrl() {
        return merchantCallbackUrl;
    }

    public void setMerchantCallbackUrl(String merchantCallbackUrl) {
        this.merchantCallbackUrl = merchantCallbackUrl == null ? null : merchantCallbackUrl.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public BigDecimal getServiceChargePercent() {
        return serviceChargePercent;
    }

    public void setServiceChargePercent(BigDecimal serviceChargePercent) {
        this.serviceChargePercent = serviceChargePercent;
    }

    public String getAppCategoryName() {
        return appCategoryName;
    }

    public void setAppCategoryName(String appCategoryName) {
        this.appCategoryName = appCategoryName == null ? null : appCategoryName.trim();
    }

    public Byte getAppPayType() {
        return appPayType;
    }

    public void setAppPayType(Byte appPayType) {
        this.appPayType = appPayType;
    }

    public Byte getAppGotoType() {
        return appGotoType;
    }

    public void setAppGotoType(Byte appGotoType) {
        this.appGotoType = appGotoType;
    }

    public String getAppGotoAddress() {
        return appGotoAddress;
    }

    public void setAppGotoAddress(String appGotoAddress) {
        this.appGotoAddress = appGotoAddress == null ? null : appGotoAddress.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getAppAuth() {
        return appAuth;
    }

    public void setAppAuth(String appAuth) {
        this.appAuth = appAuth == null ? null : appAuth.trim();
    }

    public Byte getLanguage() {
        return language;
    }

    public void setLanguage(Byte language) {
        this.language = language;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}