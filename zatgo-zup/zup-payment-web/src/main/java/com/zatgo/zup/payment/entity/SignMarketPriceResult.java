package com.zatgo.zup.payment.entity;

import com.zatgo.zup.common.model.PayCommonCreateReqParams;

public class SignMarketPriceResult extends PayCommonCreateReqParams {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private String coinType;
	
	private String cnyPrice;
	
	private String dollarPrice;
	
	private String btcPrice;

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getCnyPrice() {
		return cnyPrice;
	}

	public void setCnyPrice(String cnyPrice) {
		this.cnyPrice = cnyPrice;
	}

	public String getDollarPrice() {
		return dollarPrice;
	}

	public void setDollarPrice(String dollarPrice) {
		this.dollarPrice = dollarPrice;
	}

	public String getBtcPrice() {
		return btcPrice;
	}

	public void setBtcPrice(String btcPrice) {
		this.btcPrice = btcPrice;
	}
	
	
}
