package com.zatgo.zup.payment.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.model.AsyncPayResult;
import com.zatgo.zup.payment.entity.SignInterfaceRecord;
import com.zatgo.zup.payment.service.SignInterfaceRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.mq.MQProducer;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.payment.service.CallbackService;

@Component
public class CallbackServiceImpl implements CallbackService {
	private static final Logger logger = LoggerFactory.getLogger(CallbackServiceImpl.class);
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private OkHttpService okHttpService;
	@Autowired
	private MQProducer producer;
	private static final Map<Integer,Integer> delayMap = new HashMap<>();
	@Autowired
	private SignInterfaceRecordService signInterfaceRecordService;
	/**
	 * value表示消息延时级别 1延时3s,2延时30s,3延时3m，4表示延时30m，5表示延时1h
	 */
	static {
		delayMap.put(1,3*1000);
		delayMap.put(2,30*1000);
		delayMap.put(3,30*1000*6);
		delayMap.put(4,30*1000*60);
		delayMap.put(5,1000*60*60);
	}
	public final static String HASH_KEY_CALLBACKTIMES  = "callbackTimes";
	public final static String HASH_KEY_EXPIRETIME  = "expireTime";
	public final static long EXPIRETIME  = 60000*60*24;
	

	@Override
	public String callback(String notifyUrl, String userId, String orderId, String topic,Object messageBody,String fullMessage) {
		if(StringUtils.isEmpty(notifyUrl)||!notifyUrl.startsWith("h")){
			logger.info("notifyurl empty");
		    return "FAIL";
		}
		String key = RedisKeyConstants.NOTICE_PAY_CALLBACK_PRE + userId+"@"+orderId;
		String cts = (String) redisTemplate.opsForHash().get(key, HASH_KEY_CALLBACKTIMES);
		Integer callbackTimes = null;
		Integer delayLevel = null;
		Long expTime=  null;
		if(cts == null){
			expTime = System.currentTimeMillis() + EXPIRETIME;
			redisTemplate.opsForHash().put(key,HASH_KEY_EXPIRETIME,expTime.toString());
			delayLevel = new Integer(1);
			callbackTimes  = new Integer(1);
		} else {
			callbackTimes = Integer.valueOf(cts);
		}
		delayLevel = delayMap.get(callbackTimes);
		String ets = (String) redisTemplate.opsForHash().get(key, HASH_KEY_EXPIRETIME);
		if(ets != null){
			expTime = Long.valueOf(ets);
			long currentTimeMillis = System.currentTimeMillis();
			if(expTime != null && expTime < currentTimeMillis){
				logger.info("支付回调信息过期：" + JSON.toJSONString(messageBody));
				return "FAIL";
			}
		}
		if(callbackTimes!=null&&callbackTimes >= 5){
			//如果回调超过5次，对方没有返回SUCCESS，回调频率设置为LEVEL5
			delayLevel = delayMap.get(5);
		}
		String result = null;
		try {
			String contact = JSON.toJSONString(messageBody);
			result = okHttpService.doPost(notifyUrl, contact);
			logger.info("url:"+notifyUrl + " contact:" + contact);
			if(StringUtils.isEmpty(result) || !result.contains(BusinessEnum.ResultCode.SUCCESS.getCode())){
				logger.error("回调结果："+result);
				producer.sendDelay(topic,null, fullMessage,key,delayLevel.longValue());
				redisTemplate.opsForHash().increment(key,HASH_KEY_CALLBACKTIMES,1);
			} else {
				SignInterfaceRecord record = new SignInterfaceRecord();
				String appId = "";
				if(messageBody instanceof AsyncPayResult){
				    appId  = ((AsyncPayResult) messageBody).getAppId();
				}
				record.setAppId(appId);
				record.setInterfaceName(notifyUrl);
				record.setRequestDatetime(new Date());
				record.setRequestContent(contact);
				record.setRequestType((byte)1);
				Boolean flag = signInterfaceRecordService.insertRecord(record);
				redisTemplate.delete(key);
			}
		} catch (Exception e) {
			logger.error("回调失败:"+fullMessage,e);
			producer.sendDelay(topic,null,fullMessage,userId+"-"+orderId,delayLevel.longValue());
			redisTemplate.opsForHash().increment(key,HASH_KEY_CALLBACKTIMES,1);
		}
		return result;
	}

}
