package com.zatgo.zup.payment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class CheckoutConfig implements Serializable {
    private String configId;

    private String appId;

    private String userId;

    /**
     * 0???̶???????
            1???ٷֱ
     */
    private Byte payCoinScaleType;

    /**
     * ?ٷֱȸ?ʽ??10 ---10%ʹ????????֧??
            ?̶?ֵ??ʽ??10 --- 10??????ʹ????????֧??
     */
    private BigDecimal payCoinScale;

    /**
     * ??ʽ?? 1:10   -----??????:???
     */
    private BigDecimal coinToCnyPrice;

    private Date createDate;

    private Date updateDate;

    private String coinType;

    private String networkType;
    
    private Byte sort;
    
    private BigDecimal coinPayBalance;

    public BigDecimal getCoinPayBalance() {
		return coinPayBalance;
	}

	public void setCoinPayBalance(BigDecimal coinPayBalance) {
		this.coinPayBalance = coinPayBalance;
	}

	private static final long serialVersionUID = 1L;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Byte getPayCoinScaleType() {
        return payCoinScaleType;
    }

    public void setPayCoinScaleType(Byte payCoinScaleType) {
        this.payCoinScaleType = payCoinScaleType;
    }

    public BigDecimal getPayCoinScale() {
        return payCoinScale;
    }

    public void setPayCoinScale(BigDecimal payCoinScale) {
        this.payCoinScale = payCoinScale;
    }

    public BigDecimal getCoinToCnyPrice() {
        return coinToCnyPrice;
    }

    public void setCoinToCnyPrice(BigDecimal coinToCnyPrice) {
        this.coinToCnyPrice = coinToCnyPrice;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public Byte getSort() {
		return sort;
	}

	public void setSort(Byte sort) {
		this.sort = sort;
	}

	@Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CheckoutConfig other = (CheckoutConfig) that;
        return (this.getConfigId() == null ? other.getConfigId() == null : this.getConfigId().equals(other.getConfigId()))
            && (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getPayCoinScaleType() == null ? other.getPayCoinScaleType() == null : this.getPayCoinScaleType().equals(other.getPayCoinScaleType()))
            && (this.getPayCoinScale() == null ? other.getPayCoinScale() == null : this.getPayCoinScale().equals(other.getPayCoinScale()))
            && (this.getCoinToCnyPrice() == null ? other.getCoinToCnyPrice() == null : this.getCoinToCnyPrice().equals(other.getCoinToCnyPrice()))
            && (this.getCreateDate() == null ? other.getCreateDate() == null : this.getCreateDate().equals(other.getCreateDate()))
            && (this.getUpdateDate() == null ? other.getUpdateDate() == null : this.getUpdateDate().equals(other.getUpdateDate()))
            && (this.getCoinType() == null ? other.getCoinType() == null : this.getCoinType().equals(other.getCoinType()))
            && (this.getNetworkType() == null ? other.getNetworkType() == null : this.getNetworkType().equals(other.getNetworkType()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getConfigId() == null) ? 0 : getConfigId().hashCode());
        result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getPayCoinScaleType() == null) ? 0 : getPayCoinScaleType().hashCode());
        result = prime * result + ((getPayCoinScale() == null) ? 0 : getPayCoinScale().hashCode());
        result = prime * result + ((getCoinToCnyPrice() == null) ? 0 : getCoinToCnyPrice().hashCode());
        result = prime * result + ((getCreateDate() == null) ? 0 : getCreateDate().hashCode());
        result = prime * result + ((getUpdateDate() == null) ? 0 : getUpdateDate().hashCode());
        result = prime * result + ((getCoinType() == null) ? 0 : getCoinType().hashCode());
        result = prime * result + ((getNetworkType() == null) ? 0 : getNetworkType().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", configId=").append(configId);
        sb.append(", appId=").append(appId);
        sb.append(", userId=").append(userId);
        sb.append(", payCoinScaleType=").append(payCoinScaleType);
        sb.append(", payCoinScale=").append(payCoinScale);
        sb.append(", coinToCnyPrice=").append(coinToCnyPrice);
        sb.append(", createDate=").append(createDate);
        sb.append(", updateDate=").append(updateDate);
        sb.append(", coinType=").append(coinType);
        sb.append(", networkType=").append(networkType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}