package com.zatgo.zup.payment.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.wxpay.sdk.WXPay;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.WXAppReqParam;
import com.zatgo.zup.common.model.WeixinConstants;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.payment.config.WXConfig;
import com.zatgo.zup.payment.config.WeixinPayConfig;
import com.zatgo.zup.payment.entity.ThirdPayRecord;
import com.zatgo.zup.payment.entity.WxH5Info;
import com.zatgo.zup.payment.entity.WxpayParams;
import com.zatgo.zup.payment.service.ThirdPayRecordService;
import com.zatgo.zup.payment.service.WxPayService;
import com.zatgo.zup.payment.service.WxpayParamsService;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


import java.math.BigDecimal;
import java.util.*;

@Component
public class WxPayServiceImpl implements WxPayService {
	private static final Logger logger = LoggerFactory.getLogger(WxPayServiceImpl.class);
	@Autowired
	private WeixinPayConfig config;
	@Autowired
	private WxpayParamsService wxpayParamsService;
	@Autowired
	private ThirdPayRecordService thirdPayRecordService;
	@Autowired
	private OkHttpService httpService;
	@Override
	public WXAppReqParam AppWxPay(PayParams params) {
		WXConfig wxConfig = new WXConfig(config);
		WXPay wxPay = new WXPay(wxConfig);
		String receiptUserId = params.getReceiptUserId();
		String checkoutOrderId = params.getCheckoutOrderId();
		WxpayParams data = wxpayParamsService.selectByUserIdAndOrderId(receiptUserId, checkoutOrderId);
		String prePayId = null;
		if(data == null){
			Map<String, String> wxPayParams = getWXPayParams(checkoutOrderId, params.getCashAmount(), receiptUserId);
			wxPayParams.put(WeixinConstants.trade_type,"APP");
			wxPayParams.put(WeixinConstants.body,wxConfig.getBody());
			try {
				Map<String,String> returnMap = wxPay.unifiedOrder(wxPayParams);
				if(returnMap.get(WeixinConstants.result_code).equals("FAIL")){
					throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR,"微信支付错误");
				}
				prePayId = returnMap.get(WeixinConstants.prepay_id);
				wxpayParamsService.insertParams(null,prePayId,receiptUserId,checkoutOrderId);
			} catch (Exception e) {
				throw new BusinessException(BusinessExceptionCode.WXPAY_ERROR);
			}
		} else{
			prePayId = data.getPrepayId();
		}
		return getAppWXpayParams(prePayId,wxConfig);
	}

	@Override
	public String h5WxPay(PayParams params) {
		String receiptUserId = params.getReceiptUserId();
		String checkoutOrderId = params.getCheckoutOrderId();
		WxpayParams data = wxpayParamsService.selectByUserIdAndOrderId(receiptUserId, checkoutOrderId);
		Date createTime = null;
		Long l = null;
		if(data != null){
			createTime = data.getCreateTime();
			l = (System.currentTimeMillis() - createTime.getTime())/1000/60;
		}
		String codeUrl = null;
		if(data == null || l > 5){
			Map<String, String> wxPayParams = getWXPayParams(checkoutOrderId, params.getCashAmount(), receiptUserId);
			wxPayParams.put(WeixinConstants.trade_type,"MWEB");
			wxPayParams.put(WeixinConstants.appid,config.getAppId());
			wxPayParams.put(WeixinConstants.mch_id,config.getMchId());
			wxPayParams.put(WeixinConstants.nonce_str,generateNonceStr());
			wxPayParams.put(WeixinConstants.spbill_create_ip,params.getClientIp());
			wxPayParams.put(WeixinConstants.notify_url,config.getNotifyUrl());
			wxPayParams.put(WeixinConstants.body,config.getBody());
			WxH5Info info = new WxH5Info();
			info.setType(BusinessEnum.WxInfoType.WAP.getType());
			info.setWapUrl(config.getWapUrl());
			info.setWapName("zat");
			Map<String, Object> map = new HashMap<>();
			map.put(WxH5Info.h5_info, info);
			wxPayParams.put(WeixinConstants.scene_info, JSON.toJSONString(map));
			wxPayParams.put(WeixinConstants.sign, getWxSign(new TreeMap<>(wxPayParams)));
			String xmlParam = xmlToString(wxPayParams);
			String res = httpService.postXml("https://api.mch.weixin.qq.com/pay/unifiedorder", xmlParam);
			Map<String, String> resultMap = xmlToMap(res);
			if(resultMap.get(WeixinConstants.result_code).equals("FAIL")){
				throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR,"微信支付错误");
			}
			codeUrl = resultMap.get("mweb_url");
			String prePayId = resultMap.get(WeixinConstants.prepay_id);
			wxpayParamsService.insertParams(codeUrl,prePayId,receiptUserId,checkoutOrderId);
		} else{
		    codeUrl = data.getCodeUrl();
		}
		return codeUrl;
	}

	@Override
	public String wxRefund(String orderId,BigDecimal refundMoney) {
		ThirdPayRecord record = thirdPayRecordService.selectByCheckoutOrderId(orderId);
		WXConfig wxConfig = new WXConfig(config);
		WXPay wxPay = new WXPay(wxConfig);
		Map<String,String> returnMap = null;
		String recordId = null;
		try {
			returnMap = wxPay.refund(getWxPayRefundParams(record.getThirdPayNumber(), UUIDUtils.getUuid(),refundMoney,record.getMoney()));
			String returnCode = returnMap.get(WeixinConstants.return_code);
			String resultCode = returnMap.get(WeixinConstants.result_code);
			if(returnCode.equals(BusinessEnum.ResultCode.FAIL.getCode())){
				logger.error("wxpay error:" + JSON.toJSONString(returnMap));
				throw new BusinessException(BusinessExceptionCode.WXPAY_ERROR);
			}
			
			if(resultCode.equals(BusinessEnum.ResultCode.FAIL.getCode())){
				logger.error("wxpay error:" + JSON.toJSONString(returnMap));
				throw new BusinessException(BusinessExceptionCode.WXPAY_ERROR);
			}
			
			recordId = thirdPayRecordService.insertRecord(record.getThirdPayNumber(), record.getOrderId(), record.getPayUserId(),
					record.getPayType(), refundMoney, record.getReceiptUserId(), BusinessEnum.MoneyFlow.REFUND.getCode()
					, BusinessEnum.ThirdPayStatus.SUCCESS.getCode());
			logger.info("wxpay refund data:"+ JSON.toJSONString(returnMap));
		} catch (Exception e) {
			logger.error("微信退款失败："+record.getOrderId(),e);
			throw new BusinessException(BusinessExceptionCode.WXPAY_ERROR);
		}
		return recordId;
	}



	private Map<String,String> getWXPayParams(String orderNumber, BigDecimal money, String attach) {
		Map<String,String> map = new HashMap<>();
		map.put(WeixinConstants.out_trade_no,orderNumber);
		map.put(WeixinConstants.total_fee,String.valueOf(money.multiply(new BigDecimal(100)).intValue()));
		map.put(WeixinConstants.spbill_create_ip,config.getLocalIp());
		map.put(WeixinConstants.attach,attach);
		map.put(WeixinConstants.notify_url,config.getNotifyUrl());
		return map;
	}

	public WXAppReqParam getAppWXpayParams(String prePayId,WXConfig wxConfig) {
		TreeMap<String,String> map = new TreeMap<>();
		map.put(WeixinConstants.appid,wxConfig.getAppID());
		map.put(WeixinConstants.partnerid,wxConfig.getMchID());
		map.put(WeixinConstants.prepayid,prePayId);
		map.put("package","Sign=WXPay");
		map.put(WeixinConstants.noncestr,generateNonceStr());
		map.put(WeixinConstants.timestamp, String.valueOf(System.currentTimeMillis()/1000));
		String sign = getWxSign(map);
		WXAppReqParam param = new WXAppReqParam();
		param.set_package("Sign=WXPay");
		param.setAppId(wxConfig.getAppID());
		param.setPartnerId(wxConfig.getMchID());
		param.setPrepayId(prePayId);
		param.setNonceStr(map.get(WeixinConstants.noncestr));
		param.setTimeStamp(map.get(WeixinConstants.timestamp));
		param.setSign(sign);
		return param;
	}
	public static String generateNonceStr() {
		return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 32);
	}
	public String getWxSign(TreeMap<String, String> map) {
		StringBuilder signTemp = new StringBuilder();
		map.forEach((k,v) -> {
			signTemp.append(k).append("=").append(v).append("&");
		});
		signTemp.append("key=").append(config.getKey());
		return MD5.MD5(signTemp.toString());
	}

	public Map<String, String> getWxPayRefundParams(String orderNumber, String refundNumber,BigDecimal money,BigDecimal totalMoney) {
		Map<String,String> map = new HashMap<>();
		map.put(WeixinConstants.transaction_id,orderNumber);
		map.put(WeixinConstants.total_fee,String.valueOf(totalMoney.multiply(new BigDecimal(100)).intValue()));
		map.put(WeixinConstants.refund_fee,String.valueOf(money.multiply(new BigDecimal(100)).intValue()));
		map.put(WeixinConstants.out_refund_no,refundNumber);
		return map;
	}

	public String xmlToString(Map<String, String> params) {
		if(params.isEmpty()) {
			return "";
		}

		StringBuffer str = new StringBuffer();
		str.append("<xml>");
		params.forEach((key, value) -> {
			str.append("<").append(key).append(">").append(value).append("</").append(key).append(">");
		});
		str.append("</xml>");
		return str.toString();
	}

	public static Map<String, String> xmlToMap(String xml) {
		Map<String, String> map = new HashMap<String, String>();
		Document doc;
		try {
			doc = DocumentHelper.parseText(xml);
			Element el = doc.getRootElement();
			map = recGetXmlElementValue(el, map);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return map;
	}

	private static Map<String, String> recGetXmlElementValue(Element ele, Map<String, String> map) {
		List<Element> eleList = ele.elements();
		if (eleList.size() == 0) {
			map.put(ele.getName(), ele.getTextTrim());
			return map;
		} else {
			for (Iterator<Element> iter = eleList.iterator(); iter.hasNext();) {
				Element innerEle = iter.next();
				recGetXmlElementValue(innerEle, map);
			}
			return map;
		}
	}

}
