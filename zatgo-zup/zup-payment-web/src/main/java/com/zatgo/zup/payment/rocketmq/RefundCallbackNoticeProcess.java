package com.zatgo.zup.payment.rocketmq;

import com.zatgo.zup.payment.service.impl.CallbackServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.mq.MQConsumerCallBack;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.payment.entity.RefundReturnData;
import com.zatgo.zup.payment.service.CallbackService;
import org.springframework.stereotype.Component;

@Component
public class RefundCallbackNoticeProcess implements MQConsumerCallBack {

	private static final Logger logger = LoggerFactory.getLogger(RefundCallbackNoticeProcess.class);
	
	@Autowired
	private CallbackService callbackService;
	
	@Override
	public boolean callBack(String json) {
		try {
			RefundReturnData refundReturnData = JSON.parseObject(json, RefundReturnData.class);
			callbackService.callback(refundReturnData.getNotifyUrl(),refundReturnData.getUserId(),
					refundReturnData.getOrderId(),MQContants.TOPIC_REFUND_CALLBACK,refundReturnData,json);
		}catch(Exception e) {
			logger.error(json,e);
		}
		
		return true;
	}

}
