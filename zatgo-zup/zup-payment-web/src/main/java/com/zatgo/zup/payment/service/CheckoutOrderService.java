package com.zatgo.zup.payment.service;


import java.util.List;

import com.github.pagehelper.Page;
import com.zatgo.zup.common.model.CheckoutOrderData;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.payment.PrepayInfo;
import com.zatgo.zup.payment.entity.CheckoutOrder;

public interface CheckoutOrderService {

	CheckoutOrder selectOrderByUserAndOrderId(String userId,String orderId);

	/**
	 * 分页查询订单数据
	 * @param userId
	 * @param pageNo 第几页
	 * @param pageSize 每一页数据
	 * @return
	 */
	PageInfo<CheckoutOrderData> selectOrderByPage(String userId, String pageNo, String pageSize);


	List<CheckoutOrder> selectPayErrorOrder();

	List<CheckoutOrder> selectRefundErrorOrder();

	CheckoutOrder selectOrderByOrderIdAndPayUserId(String userId,String orderId);

	PrepayInfo selectOrderByPrePayId(String prepayId);
	Boolean checkPrepayId(String prepayId);

	CheckoutOrder signOrderQuery(String userId,String id);

	Page<CheckoutOrder> selectOrderByUserIdAndTime(String userId,String page,String size,String start,String end);
	
	/**
	 * 新启动事务更新订单信息
	 * @param checkoutOrder
	 * @return
	 */
	public void newTransactionalUpdateCheckoutOrder (CheckoutOrder checkoutOrder);
}
