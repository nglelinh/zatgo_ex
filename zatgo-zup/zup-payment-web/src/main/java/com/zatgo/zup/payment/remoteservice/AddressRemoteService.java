package com.zatgo.zup.payment.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("zup-wallet")
public interface AddressRemoteService {
	@RequestMapping(value = "/wallet/address/pay/{userId}/{type}",method = RequestMethod.GET)
	String createPayAddress(@PathVariable("userId") String userId,@PathVariable("type") String type);

}
