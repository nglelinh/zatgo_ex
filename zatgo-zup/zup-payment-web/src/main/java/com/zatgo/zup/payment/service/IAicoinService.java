package com.zatgo.zup.payment.service;

import com.zatgo.zup.common.model.AicoinMarketModel;

public interface IAicoinService {

	/**
	 * 查询交易所币对价格
	 * @param marketName
	 * @param pairs
	 * @return
	 */
	public AicoinMarketModel getMarket(String marketName, String pairs);
}
