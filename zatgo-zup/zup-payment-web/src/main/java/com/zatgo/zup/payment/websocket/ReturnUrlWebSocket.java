package com.zatgo.zup.payment.websocket;//package com.zatgo.zup.gateway.websocket;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/ws/pay/return/{prepayId}")
@Component
public class ReturnUrlWebSocket {
	private static final Logger logger = LoggerFactory.getLogger(ReturnUrlWebSocket.class);

	private Session session;
	public volatile static ConcurrentHashMap<String,Session> cacheMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String,String> userCache = new ConcurrentHashMap<>();

	@OnOpen
	public void onOpen(@PathParam("prepayId")String prepayId,EndpointConfig config,Session session){
		this.session =session;
		cacheMap.put(prepayId,session);
	}
	@OnClose
	public void onClose(@PathParam("prepayId")String prepayId){
		cacheMap.remove(prepayId);
		logger.info("websocket  disconnect");
	}
	public void sendMessage(String msg) throws IOException {
		this.session.getBasicRemote().sendText(msg);
	}
	@OnMessage
	public void onMessage(String message,Session session) throws IOException {

		sendMessage("websocket connect");
	}









}
