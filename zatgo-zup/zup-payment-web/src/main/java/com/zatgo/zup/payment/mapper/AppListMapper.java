package com.zatgo.zup.payment.mapper;

import com.zatgo.zup.payment.entity.AppList;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface AppListMapper {
    int deleteByPrimaryKey(String appId);

    int insert(AppList record);

    int insertSelective(AppList record);

    AppList selectByPrimaryKey(String appId);

    int updateByPrimaryKeySelective(AppList record);

    int updateByPrimaryKey(AppList record);
	AppList selectByUserIdAndAppName(@Param("userId") String userId,@Param("name") String name);
	
	List<AppList> selectAppListByCloudUserId(@Param("cloudUserId") String cloudUserId, @Param("language") Byte language);
}