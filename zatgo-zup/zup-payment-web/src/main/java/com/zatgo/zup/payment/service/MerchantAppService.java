package com.zatgo.zup.payment.service;

import java.util.List;

import com.zatgo.zup.payment.entity.AppCategoryData;
import com.zatgo.zup.payment.entity.AppList;
import com.zatgo.zup.payment.entity.RegistAppParams;

public interface MerchantAppService {

	String regist(RegistAppParams params);

	AppList selectByAppId(String appId);
	
	List<AppCategoryData> getAppCategoryList(String cloudUserId, Byte language);
	
	/**
	 * 更新收银台币种汇率
	 */
	void updateCheckoutCoinRate();
}
