package com.zatgo.zup.payment.service;

import com.zatgo.zup.payment.entity.ThirdPayRecord;

import java.math.BigDecimal;

public interface ThirdPayRecordService {

	String insertRecord(String thirdPayNumber, String orderId, String payUserId, Byte payType, BigDecimal money,
		String receiptUserId,Byte moneyFlow,Byte status);

	ThirdPayRecord selectByCheckoutOrderId(String id);

	BigDecimal refundAmountByOrderId(String orderId);
}
