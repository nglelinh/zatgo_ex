package com.zatgo.zup.payment.rocketmq;


import javax.annotation.PostConstruct;

import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.mq.MQConsumer;
import com.zatgo.zup.common.mq.MQContants;
@Component
public class RefundCallbackComsumer {
	private static final Logger logger = LoggerFactory.getLogger(PayCallbackComsumer.class);
	
	@Autowired
	MQConsumer consumer;
	@Autowired
	private RefundCallbackNoticeProcess process;
	@PostConstruct
	public void consume(){
		try {
			consumer.startConsumer(MQContants.CONSUMER_GROUP_REFUND_CALLBACK,
					MQContants.TOPIC_REFUND_CALLBACK,null, process, MessageModel.CLUSTERING);
		} catch (Exception e) {
			logger.error("RefundCallbackComsumer error",e);
		}
	}
}
