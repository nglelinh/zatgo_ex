package com.zatgo.zup.payment.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
@FeignClient("zup-wallet")
public interface SystemParamsRemoteService {

	@RequestMapping(value = "/coin/rate/{coinType}",method = RequestMethod.GET)
	@ResponseBody
	ResponseData<BigDecimal> queryCoinRate(@PathVariable("coinType")String coinType);

}
