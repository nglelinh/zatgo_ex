package com.zatgo.zup.payment.entity;

import com.zatgo.zup.common.model.PayCommonCreateReqParams;

public class SignOrderQueryPageParams extends PayCommonCreateReqParams {

	private String pageNo;
	
	private String pageSize;
	
	private String busiStartDate;
	
	private String busiEndDate;

	public String getPageNo() {
		return pageNo;
	}

	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getBusiStartDate() {
		return busiStartDate;
	}

	public void setBusiStartDate(String busiStartDate) {
		this.busiStartDate = busiStartDate;
	}

	public String getBusiEndDate() {
		return busiEndDate;
	}

	public void setBusiEndDate(String busiEndDate) {
		this.busiEndDate = busiEndDate;
	}
}
