package com.zatgo.zup.payment.service;

public interface CallbackService  {

	String callback(String notifyUrl, String userId, String orderId,String topic,Object messageBody,String fullMessage);


}
