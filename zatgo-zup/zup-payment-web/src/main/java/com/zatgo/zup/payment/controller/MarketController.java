package com.zatgo.zup.payment.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AicoinMarketModel;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.common.utils.SignUtils;
import com.zatgo.zup.payment.entity.AppList;
import com.zatgo.zup.payment.entity.SignMarketPriceReqParams;
import com.zatgo.zup.payment.entity.SignMarketPriceResult;
import com.zatgo.zup.payment.service.IAicoinService;
import com.zatgo.zup.payment.service.MerchantAppService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/market",description = "交易所相关",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/pay/market")
public class MarketController extends BaseController{
	
	@Autowired
	private MerchantAppService merchantAppService;
	
	@Autowired
	private IAicoinService aicoinService;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@Value("${api-rsa.privateKey}")
	private String privateKey;
	
	private static HashMap<String, String[]> COIN_TYPE_TO_PAIRS = new HashMap<>();
	static {
//		COIN_TYPE_TO_PAIRS.put(BusinessEnum.CurrencyEnum.ZAT.getCode(), new String[] {"coinegg","zat_btc"});
//		COIN_TYPE_TO_PAIRS.put(BusinessEnum.CurrencyEnum.QTUM.getCode(), new String[] {"coinegg","qtum_btc"});
	}

	@ApiOperation(value = "获取币种市场价格")
	@RequestMapping(value = "/sign/price",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<SignMarketPriceResult> queryMarketPrice(@RequestBody SignMarketPriceReqParams params){
		AppList appList = merchantAppService.selectByAppId(params.getAppId());
		if(appList == null){
			throw new BusinessException(BusinessExceptionCode.MERCHANT_SETTING_ERROR);
		}
		String sign = params.getSign();
		if(!SignUtils.signCheck(params,sign,appList.getMerchantPublicKey())){
			throw new BusinessException(BusinessExceptionCode.SIGN_ERROR);
		}
		
		if(!COIN_TYPE_TO_PAIRS.containsKey(params.getCoinType())) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR,"不支持币种" + params.getCoinType() + "查询");
		}
		
		String[] pairs = COIN_TYPE_TO_PAIRS.get(params.getCoinType());
		
		SignMarketPriceResult result = (SignMarketPriceResult)redisTemplate.opsForValue().get(RedisKeyConstants.BUSI_MARKET_PRICE_QUERY_DATA + params.getCoinType());
		if(result == null) {
			String lockKey = RedisKeyConstants.BUSI_MARKET_PRICE_QUERY_LOCK + params.getCoinType();
			redisLockUtils.lock(lockKey);
			try {
				result = (SignMarketPriceResult)redisTemplate.opsForValue().get(RedisKeyConstants.BUSI_MARKET_PRICE_QUERY_DATA + params.getCoinType());
				if(result == null) {
					AicoinMarketModel model = aicoinService.getMarket(pairs[0], pairs[1]);
					
					result = new SignMarketPriceResult();
					result.setAppId(params.getAppId());
					result.setSignCharset("utf-8");
					result.setSignType("RSA2");
					result.setTimestamp(DateTimeUtils.parseDateToString(new Date(),DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM_SS));
					result.setVersion("1.0");
					result.setBtcPrice(model.getPrice().toString());
					result.setCnyPrice(model.getPriceCny().toString());
					result.setDollarPrice(model.getPriceUsd().toString());
					result.setCoinType(params.getCoinType());
					result.setSign(SignUtils.sign256(MD5.getMD5Sign(result),privateKey));
					
					redisTemplate.opsForValue().set(RedisKeyConstants.BUSI_MARKET_PRICE_QUERY_DATA + params.getCoinType(), result);
					redisTemplate.expire(RedisKeyConstants.BUSI_MARKET_PRICE_QUERY_DATA + params.getCoinType(), 30,TimeUnit.SECONDS);
				}
			}finally {
				redisLockUtils.releaseLock(lockKey);
			}
		}
		
		
		return BusinessResponseFactory.createSuccess(result);
	}
}
