package com.zatgo.zup.payment.entity;

import java.io.Serializable;
import java.util.List;

public class PageData<T> implements Serializable {

	private String pageNo;

	private String pageSize;

	private String size;
	
	private String total;

	private List<T> list;

	public PageData(String pageNo, String pageSize, String total, List<T> list) {
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.size = String.valueOf(list.size());
		this.total = total;
		this.list = list;
	}

	public String getPageNo() {
		return pageNo;
	}

	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
}
