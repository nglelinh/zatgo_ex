package com.zatgo.zup.payment.entity;

import com.zatgo.zup.common.model.PayCommonCreateReqParams;

public class SignMarketPriceReqParams extends PayCommonCreateReqParams {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String coinType;

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}
	
	
}
