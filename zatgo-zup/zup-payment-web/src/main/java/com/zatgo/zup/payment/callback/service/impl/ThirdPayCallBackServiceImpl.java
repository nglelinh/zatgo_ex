package com.zatgo.zup.payment.callback.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.model.AsyncPayResult;
import com.zatgo.zup.common.model.PayCallbackMsg;
import com.zatgo.zup.common.model.WxPayCallbackData;
import com.zatgo.zup.common.model.WxPayReturnBack;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.common.mq.MQProducer;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.common.utils.SignUtils;
import com.zatgo.zup.payment.callback.service.ThirdPayCallBackService;
import com.zatgo.zup.payment.entity.CheckoutOrder;
import com.zatgo.zup.payment.entity.PayCallbackData;
import com.zatgo.zup.payment.entity.ThirdPayRecord;
import com.zatgo.zup.payment.mapper.CheckoutOrderMapper;
import com.zatgo.zup.payment.service.ThirdPayRecordService;

//import com.zatgo.zup.common.redis.RedissonLockUtils;

@Component
public class ThirdPayCallBackServiceImpl implements ThirdPayCallBackService {
	private static final Logger logger = LoggerFactory.getLogger(ThirdPayCallBackServiceImpl.class);
	@Autowired
	private CheckoutOrderMapper checkoutOrderMapper;
	@Autowired
	private ThirdPayRecordService thirdPayRecordService;
	@Autowired
	private MQProducer producer;
	@Autowired
	private RedisTemplate redisTemplate;
	public final static String PAY_CALLBACK = "payCallback";
	public final static String ORDER_RECEIVE  = "orderReceive";
//	@Autowired
//	private RedissonLockUtils redissonLockUtils;
	@Autowired
	private RedisLockUtils redisLockUtils;
	@Value("${api-rsa.privateKey}")
	private String privateKey;
	@Override
	@Transactional
	public String payCallback(PayCallbackData data) {
		PayCallbackMsg<AsyncPayResult> payCallbackMsg = null;
		String checkoutOrderId = null;
		if(!redisLockUtils.lock(data.getOutTradeNum()))
			return "FAIL";
		try {
			checkoutOrderId = data.getLocalParams();
			CheckoutOrder checkoutOrder = checkoutOrderMapper.selectByPrimaryKey(checkoutOrderId);
			ThirdPayRecord thirdPayRecord = thirdPayRecordService.selectByCheckoutOrderId(checkoutOrder.getOrderId());
			if(thirdPayRecord != null){
				return BusinessEnum.ResultCode.SUCCESS.getCode();
			}
			String json = (String) redisTemplate.opsForValue().get(checkoutOrderId);
			logger.info("wxPayCheckoutOrderId=" + json);
			payCallbackMsg = JSON.parseObject(json, new TypeReference<PayCallbackMsg<AsyncPayResult>>() {
			});
			AsyncPayResult result = payCallbackMsg.getResult();
			if(data.getResultCode().equals("FAIL")){
				checkoutOrderMapper.updateThirdPayStatus(checkoutOrderId,BusinessEnum.OrderStatus.FAIL.getCode());
				result.setResultCode(BusinessEnum.ResultCode.FAIL.getCode());
				return BusinessEnum.ResultCode.SUCCESS.getCode();
			}
			checkoutOrderMapper.updateThirdPayStatus(checkoutOrderId,BusinessEnum.OrderStatus.PAID.getCode());
			thirdPayRecordService.insertRecord(data.getOutTradeNum(),checkoutOrderId,checkoutOrder.getPayUserId(),
					BusinessEnum.CashPayType.WX.getCode(),checkoutOrder.getCashAmount(),checkoutOrder.getReceiptUserId()
					,BusinessEnum.MoneyFlow.PAY.getCode(),BusinessEnum.ThirdPayStatus.SUCCESS.getCode());
			String sign = SignUtils.sign256(MD5.getMD5Sign(result),privateKey);
			result.setSign(sign);
			payCallbackMsg.setResultCode(BusinessEnum.ResultCode.SUCCESS.getCode());
			String payCallbackMsgStr = JSON.toJSONString(payCallbackMsg);
			logger.info("payCallbackMsg=" + payCallbackMsgStr);
			producer.send(MQContants.TOPIC_ORDER_RECEIVE,null,payCallbackMsgStr,result.getReceiptUserId()+"-"+result.getOutTradeNo());
			producer.send(MQContants.TOPIC_PAY_CALLBACK,null, payCallbackMsgStr,result.getReceiptUserId()+"-"+result.getOutTradeNo());
			redisTemplate.delete(checkoutOrderId);
			return BusinessEnum.ResultCode.SUCCESS.getCode();
		} catch (Exception e) {
			logger.error("微信回调错误,订单信息={}",JSON.toJSONString(data));
			throw e;
		} finally {
			redisLockUtils.releaseLock(data.getOutTradeNum());
		}
	}

	@Override
	public WxPayReturnBack wxRefundCallback(WxPayCallbackData data) {
		return null;
	}
}
