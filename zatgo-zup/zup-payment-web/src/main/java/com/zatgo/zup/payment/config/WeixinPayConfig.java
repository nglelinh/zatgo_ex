package com.zatgo.zup.payment.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WeixinPayConfig {
	@Value("${wxpay.wxAppId}")
	private String appId;
	@Value("${wxpay.wxMchId}")
	private String mchId;
	@Value("${wxpay.wxKey}")
	private String key;
	@Value("${wxpay.keyFilePath}")
	private String path;
	@Value("${wxpay.connectTimeoutMs}")
	private int connectTimeoutMs;
	@Value("${wxpay.readTimeoutMs}")
	private int readTimeoutMs;
	@Value("${wxpay.wxpayCallbackUrl}")
	private String notifyUrl;
	@Value("${wxpay.localIp}")
	private String localIp;
	@Value("${wxpay.body}")
	private String body;
	@Value("${wxpay.wapUrl}")
	private String wapUrl;

	public String getWapUrl() {
		return wapUrl;
	}

	public void setWapUrl(String wapUrl) {
		this.wapUrl = wapUrl;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getConnectTimeoutMs() {
		return connectTimeoutMs;
	}

	public void setConnectTimeoutMs(int connectTimeoutMs) {
		this.connectTimeoutMs = connectTimeoutMs;
	}

	public int getReadTimeoutMs() {
		return readTimeoutMs;
	}

	public void setReadTimeoutMs(int readTimeoutMs) {
		this.readTimeoutMs = readTimeoutMs;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

}
