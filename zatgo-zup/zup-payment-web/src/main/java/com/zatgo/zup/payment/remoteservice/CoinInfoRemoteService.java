package com.zatgo.zup.payment.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.payment.entity.IssuedCoinInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("zup-wallet")
public interface CoinInfoRemoteService {

	@RequestMapping(value = "/wallet/coin/{type}",method = RequestMethod.GET )
	@ResponseBody
	ResponseData<IssuedCoinInfo> selectByCoinType(@PathVariable("type") String type);

}
