package com.zatgo.zup.payment.entity;

import java.io.Serializable;


public class PayCallbackData implements Serializable {
	/**
	 *  本系统标识
	 */
	private String localParams;
	/**
	 * 第三方支付系统回调结果
	 */
	private String resultCode;
	/**
	 * 第三方支付系统标识
	 */
	private String outTradeNum;

	public String getLocalParams() {
		return localParams;
	}

	public void setLocalParams(String localParams) {
		this.localParams = localParams;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getOutTradeNum() {
		return outTradeNum;
	}

	public void setOutTradeNum(String outTradeNum) {
		this.outTradeNum = outTradeNum;
	}
}
