package com.zatgo.zup.payment.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

@FeignClient("zup-wallet")
public interface RefundRecordRemoteService {

	@RequestMapping(value = "/wallet/refund/record/{orderId}",method = RequestMethod.GET)
	@ResponseBody
	ResponseData<BigDecimal> sumRefund(@PathVariable("orderId") String orderId);

}
