package com.zatgo.zup.payment.config;


import com.github.wxpay.sdk.WXPayConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class WXConfig implements WXPayConfig {
	private static final Logger logger = LoggerFactory.getLogger(WXConfig.class);
	private  WeixinPayConfig config;
	private String appId;
	private String mchId;
	private String key;
	private String path;
	private int connectTimeoutMs;
	private int readTimeoutMs;
	private String notifyUrl;
	private String localIp;
	private String body;
	private byte[] certData;
	@Autowired
	public WXConfig(WeixinPayConfig config) {
		this.config = config;
		appId = config.getAppId();
		mchId = config.getMchId();
		key = config.getKey();
		path = config.getPath();
		connectTimeoutMs = config.getConnectTimeoutMs();
		readTimeoutMs = config.getReadTimeoutMs();
		notifyUrl = config.getNotifyUrl();
		localIp = config.getLocalIp();
		body = config.getBody();
		File file = new File(path);
		if(file.exists()){
			try {
				InputStream certStream = new FileInputStream(file);
				this.certData = new byte[(int) file.length()];
				certStream.read(this.certData);
				certStream.close();
			} catch (IOException e) {
				logger.error("读取微信证书错误",e);
			}
		}
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String getAppID() {
		return this.appId;
	}

	@Override
	public String getMchID() {
		return this.mchId;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public InputStream getCertStream() {
		ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
		return certBis;
	}

	@Override
	public int getHttpConnectTimeoutMs() {
		return this.connectTimeoutMs;
	}

	@Override
	public int getHttpReadTimeoutMs() {
		return this.readTimeoutMs;
	}

	public WeixinPayConfig getConfig() {
		return config;
	}

	public void setConfig(WeixinPayConfig config) {
		this.config = config;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getConnectTimeoutMs() {
		return connectTimeoutMs;
	}

	public void setConnectTimeoutMs(int connectTimeoutMs) {
		this.connectTimeoutMs = connectTimeoutMs;
	}

	public int getReadTimeoutMs() {
		return readTimeoutMs;
	}

	public void setReadTimeoutMs(int readTimeoutMs) {
		this.readTimeoutMs = readTimeoutMs;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

	public byte[] getCertData() {
		return certData;
	}

	public void setCertData(byte[] certData) {
		this.certData = certData;
	}
}
