package com.zatgo.zup.payment.service;

import java.math.BigDecimal;
import java.util.List;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.RefundRequestParams;
import com.zatgo.zup.common.model.payment.PayMoney;
import com.zatgo.zup.payment.entity.CreateOrderBizData;
import com.zatgo.zup.payment.entity.CreateOrderResult;
import com.zatgo.zup.payment.entity.OrderPayResult;
import com.zatgo.zup.payment.entity.RefundReturnData;

public interface OrderPayService {

	RefundReturnData refund(RefundRequestParams params,String cloudUserId);

	PayMoney computeMoney(String userId, BigDecimal total,String coinType);

	CreateOrderResult createOrder(CreateOrderBizData params,String userId);

	OrderPayResult orderPayV2(PayParams params, AuthUserInfo userInfo);

	PayMoney computeMoney(String orderId);

//	SignOrderPayResult	signOrderPay(SignOrderPayReqParams params);
	
	List<PayMoney> getOrderPayCoinList(String prepayId);

}
