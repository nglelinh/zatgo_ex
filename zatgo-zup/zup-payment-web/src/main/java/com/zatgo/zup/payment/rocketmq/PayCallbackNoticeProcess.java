package com.zatgo.zup.payment.rocketmq;

import com.alibaba.fastjson.TypeReference;
import com.zatgo.zup.common.model.AsyncPayResult;
import com.zatgo.zup.common.model.PayCallbackMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.mq.MQConsumerCallBack;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.payment.service.CallbackService;
import org.springframework.stereotype.Component;

@Component
public class PayCallbackNoticeProcess implements MQConsumerCallBack{

	private static final Logger logger = LoggerFactory.getLogger(PayCallbackNoticeProcess.class);

	@Autowired
	private CallbackService callbackService;

	@Override
	public boolean callBack(String json) {
		try {
			PayCallbackMsg<AsyncPayResult> payCallbackMsg = JSON.parseObject(json, new TypeReference<PayCallbackMsg<AsyncPayResult>>() {
			});
			AsyncPayResult asyncPayResult = payCallbackMsg.getResult();
			callbackService.callback(payCallbackMsg.getNotifyUrl(),payCallbackMsg.getReceiptUserId(),
					payCallbackMsg.getOutTradeNo(),MQContants.TOPIC_PAY_CALLBACK,asyncPayResult,json);
		}catch(Exception e) {
			logger.error(json,e);
		}
		
		return true;
	}

}
