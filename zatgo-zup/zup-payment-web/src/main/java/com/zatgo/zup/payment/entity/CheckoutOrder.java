package com.zatgo.zup.payment.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
@ApiModel(value = "订单")
public class CheckoutOrder implements Serializable {
	@ApiModelProperty(value = "订单编号",required = true)
    private String orderId;
	@ApiModelProperty(value = "付款人ID",required = true)
    private String payUserId;
	@ApiModelProperty(value = "收款人ID",required = true)
    private String receiptUserId;
	@ApiModelProperty(value = "第三方订单号",required = true)
    private String orderFlowCode;
	@ApiModelProperty(value = "主要币种支付状态",required = true)
    private Byte orderStatus;
	
    private String orderType;
	@ApiModelProperty(value = "订单描述",required = true)
    private String orderMemo;
	@ApiModelProperty(value = "订单标题",required = true)
    private String orderName;
	@ApiModelProperty(value = "主币种金额",required = true)
    private BigDecimal orderMoney;

    /**
     * BTC
            ETH
            QTUM
            ZAT
     */
    @ApiModelProperty(value = "主币类型",required = true)
    private String coinType;

    /**
     * BTC
            ETH
            QTUM
     */
    private String coinNetworkType;

    private String appId;

    private String checkoutMetadata;

    private Date createDate;

    private Date updateDate;

    private Date paymentDate;
	@ApiModelProperty(value = "次币金额",required = true)
    private BigDecimal cashAmount;
	@ApiModelProperty(value = "是否混合支付",required = true)
    private Byte isMultiPay;
	@ApiModelProperty(value = "第三方支付类型",required = true)
    private Byte thirdPayType;
	@ApiModelProperty(value = "次币支付状态",required = true)
    private Byte thirdPayStatus;
	@ApiModelProperty(value = "收款方名称",required = true)
    private String receiptUserName;

    /**
     * 代币在支付中所占比例，如1 表示代币在支付比例中为百分之1
     */
    private BigDecimal payCoinScale;

    /**
     * 币种人民币价格
     */
    private BigDecimal coinToCnyPrice;

    private Byte payCoinScaleType;

    private String payUserName;

    private String notifyUrl;

    private BigDecimal originalPrice;

    private String mixCoinType;

    private String mixCoinNetworkType;
    
    private BigDecimal serviceCharge;
    
    private Byte usageCouponsStatus;
    
    private String couponsList;

    private static final long serialVersionUID = 1L;

    public Byte getUsageCouponsStatus() {
		return usageCouponsStatus;
	}

	public void setUsageCouponsStatus(Byte usageCouponsStatus) {
		this.usageCouponsStatus = usageCouponsStatus;
	}

	public String getCouponsList() {
		return couponsList;
	}

	public void setCouponsList(String couponsList) {
		this.couponsList = couponsList;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPayUserId() {
        return payUserId;
    }

    public void setPayUserId(String payUserId) {
        this.payUserId = payUserId;
    }

    public String getReceiptUserId() {
        return receiptUserId;
    }

    public void setReceiptUserId(String receiptUserId) {
        this.receiptUserId = receiptUserId;
    }

    public String getOrderFlowCode() {
        return orderFlowCode;
    }

    public void setOrderFlowCode(String orderFlowCode) {
        this.orderFlowCode = orderFlowCode;
    }

    public Byte getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Byte orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderMemo() {
        return orderMemo;
    }

    public void setOrderMemo(String orderMemo) {
        this.orderMemo = orderMemo;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public BigDecimal getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(BigDecimal orderMoney) {
        this.orderMoney = orderMoney;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType;
    }

    public String getCheckoutMetadata() {
        return checkoutMetadata;
    }

    public void setCheckoutMetadata(String checkoutMetadata) {
        this.checkoutMetadata = checkoutMetadata;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public BigDecimal getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(BigDecimal cashAmount) {
        this.cashAmount = cashAmount;
    }

    public Byte getIsMultiPay() {
        return isMultiPay;
    }

    public void setIsMultiPay(Byte isMultiPay) {
        this.isMultiPay = isMultiPay;
    }

    public Byte getThirdPayType() {
        return thirdPayType;
    }

    public void setThirdPayType(Byte thirdPayType) {
        this.thirdPayType = thirdPayType;
    }

    public Byte getThirdPayStatus() {
        return thirdPayStatus;
    }

    public void setThirdPayStatus(Byte thirdPayStatus) {
        this.thirdPayStatus = thirdPayStatus;
    }

    public String getReceiptUserName() {
        return receiptUserName;
    }

    public void setReceiptUserName(String receiptUserName) {
        this.receiptUserName = receiptUserName;
    }

    public BigDecimal getPayCoinScale() {
        return payCoinScale;
    }

    public void setPayCoinScale(BigDecimal payCoinScale) {
        this.payCoinScale = payCoinScale;
    }

    public BigDecimal getCoinToCnyPrice() {
        return coinToCnyPrice;
    }

    public void setCoinToCnyPrice(BigDecimal coinToCnyPrice) {
        this.coinToCnyPrice = coinToCnyPrice;
    }

    public Byte getPayCoinScaleType() {
        return payCoinScaleType;
    }

    public void setPayCoinScaleType(Byte payCoinScaleType) {
        this.payCoinScaleType = payCoinScaleType;
    }

    public String getPayUserName() {
        return payUserName;
    }

    public void setPayUserName(String payUserName) {
        this.payUserName = payUserName;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getMixCoinType() {
        return mixCoinType;
    }

    public void setMixCoinType(String mixCoinType) {
        this.mixCoinType = mixCoinType;
    }

    public String getMixCoinNetworkType() {
        return mixCoinNetworkType;
    }

    public void setMixCoinNetworkType(String mixCoinNetworkType) {
        this.mixCoinNetworkType = mixCoinNetworkType;
    }
}