package com.zatgo.zup.payment.entity;

import com.zatgo.zup.common.model.PayCommonCreateReqParams;

import java.io.Serializable;

public class CreateOrderBizData  extends PayCommonCreateReqParams implements Serializable{

	private String notifyUrl;
	private String body;
	private String subject;
	private String outTradeNo;
	private String timeoutExpress;
	private String coinAmount;
	private String coinNetworkType;
	private String coinType;
	private String mixedCoinNetworkType;
	private String mixedCoinType;

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getTimeoutExpress() {
		return timeoutExpress;
	}

	public void setTimeoutExpress(String timeoutExpress) {
		this.timeoutExpress = timeoutExpress;
	}

	public String getCoinAmount() {
		return coinAmount;
	}

	public void setCoinAmount(String coinAmount) {
		this.coinAmount = coinAmount;
	}

	public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getMixedCoinNetworkType() {
		return mixedCoinNetworkType;
	}

	public void setMixedCoinNetworkType(String mixedCoinNetworkType) {
		this.mixedCoinNetworkType = mixedCoinNetworkType;
	}

	public String getMixedCoinType() {
		return mixedCoinType;
	}

	public void setMixedCoinType(String mixedCoinType) {
		this.mixedCoinType = mixedCoinType;
	}
}
