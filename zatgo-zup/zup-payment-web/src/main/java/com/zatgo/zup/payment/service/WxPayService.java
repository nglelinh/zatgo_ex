package com.zatgo.zup.payment.service;

import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.WXAppReqParam;

import java.math.BigDecimal;

public interface WxPayService {

	WXAppReqParam AppWxPay(PayParams params);

	String wxRefund(String checkoutOrderId,BigDecimal refundMoney);

	String h5WxPay(PayParams params);


}
