package com.zatgo.zup.payment.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.payment.config.AlipayConfig;
import com.zatgo.zup.payment.service.AlipayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AlipayServiceImpl implements AlipayService {
	private static final Logger logger = LoggerFactory.getLogger(AlipayServiceImpl.class);
	@Autowired
	private AlipayConfig config;
	public final static String PRODUCT_CODE  = "QUICK_WAP_WAY";
	@Override
	public String mwebPay(PayParams params) {
		AlipayClient client = new DefaultAlipayClient(config.getURL(), config.getAPPID(),
				config.getRSA_PRIVATE_KEY(), config.getFORMAT(), config.getCHARSET(), config.getALIPAY_PUBLIC_KEY(),config.getSIGNTYPE());
		AlipayTradeWapPayRequest aliPayReq=new AlipayTradeWapPayRequest();
		AlipayTradeWapPayModel model=new AlipayTradeWapPayModel();
		model.setOutTradeNo(params.getCheckoutOrderId());
		model.setSubject("支付宝测试");
		model.setTotalAmount(params.getCashAmount().toString());
		model.setProductCode(PRODUCT_CODE);
		aliPayReq.setBizModel(model);
		aliPayReq.setNotifyUrl(config.notify_url);
		aliPayReq.setReturnUrl(config.getReturn_url());
		String form = "";
		try {
			form = client.pageExecute(aliPayReq).getBody();
			String body = client.sdkExecute(aliPayReq).getBody();
			System.err.println(body);
		} catch (AlipayApiException e) {
			logger.error("调用支付宝接口错误",e);
		}
		return form;
	}
}
