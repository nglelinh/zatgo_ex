package com.zatgo.zup.payment.entity;



import com.zatgo.zup.common.model.PayCommonCreateReqParams;

import java.io.Serializable;

public class CreateOrderResult extends PayCommonCreateReqParams implements Serializable {


	private String outTradeNo;

	private String tradeNo;

	private String prepayId;

	private String returnCode;

	private String returnMsg;

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getPrepayId() {
		return prepayId;
	}

	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}
}
