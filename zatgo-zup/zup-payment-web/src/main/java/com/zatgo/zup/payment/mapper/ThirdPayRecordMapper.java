package com.zatgo.zup.payment.mapper;

import com.zatgo.zup.payment.entity.ThirdPayRecord;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

public interface ThirdPayRecordMapper {
    int deleteByPrimaryKey(String recordId);

    int insert(ThirdPayRecord record);

    int insertSelective(ThirdPayRecord record);

    ThirdPayRecord selectByPrimaryKey(String recordId);

    int updateByPrimaryKeySelective(ThirdPayRecord record);

    int updateByPrimaryKey(ThirdPayRecord record);

	ThirdPayRecord selectByCheckoutOrderId(@Param("orderId") String orderId);

	BigDecimal sumRefundAmountByOrderId(@Param("orderId")String orderId);
}