package com.zatgo.zup.payment.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.RefundRequestParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.payment.PayMoney;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.SignUtils;
import com.zatgo.zup.payment.entity.AppList;
import com.zatgo.zup.payment.entity.CreateOrderBizData;
import com.zatgo.zup.payment.entity.CreateOrderResult;
import com.zatgo.zup.payment.entity.OrderPayResult;
import com.zatgo.zup.payment.entity.RefundReturnData;
import com.zatgo.zup.payment.service.MerchantAppService;
import com.zatgo.zup.payment.service.OrderPayService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "pay",description = "收银台支付",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(value = "/pay")
public class OrderPayController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(OrderPayController.class);

	@Autowired
	private RedisLockUtils redisLockUtils;
	@Autowired
	private OrderPayService service;
	@Autowired
	private MerchantAppService merchantAppService;

	@RequestMapping(value = "/orderRefund",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<RefundReturnData> orderRefund(@RequestBody RefundRequestParams params){
		AppList appList = merchantAppService.selectByAppId(params.getAppId());
		if(appList == null){
			logger.error("appId=" + params.getAppId() + " is not exist!");
			throw new BusinessException(BusinessExceptionCode.MERCHANT_SETTING_ERROR);
		}
//		String sign = params.getSign();
//		if(!SignUtils.signCheck(params,sign,appList.getMerchantPublicKey())){
//			throw new BusinessException(BusinessExceptionCode.SIGN_ERROR);
//		}
		
		String lockKey = RedisKeyConstants.PAYMENT_ORDER_PRO + params.getAppId() + params.getOrderId();
		redisLockUtils.lock(lockKey);
		try {
			RefundReturnData returnData = service.refund(params,appList.getCloudUserId());
			return BusinessResponseFactory.createSuccess(returnData);
		}finally {
			redisLockUtils.releaseLock(lockKey);
		}
		
		
	}

	@RequestMapping(value = "/coin/list",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<List<PayMoney>> computeMoney(@RequestParam("prepayId") String prepayId){
		
		return BusinessResponseFactory.createSuccess(service.getOrderPayCoinList(prepayId));
	}

	@RequestMapping(value = "/sign/createOrder",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<CreateOrderResult> createOrder(@RequestBody CreateOrderBizData param){
		AppList appList = merchantAppService.selectByAppId(param.getAppId());
		if(appList == null){
			logger.error("appId=" + param.getAppId() + " is not exist!");
			throw new BusinessException(BusinessExceptionCode.MERCHANT_SETTING_ERROR);
		}
		String sign = param.getSign();
		if(!SignUtils.signCheck(param,sign,appList.getMerchantPublicKey())){
			throw new BusinessException(BusinessExceptionCode.SIGN_ERROR);
		}
		
		String lockKey = RedisKeyConstants.PAYMENT_ORDER_PRO + param.getAppId() + param.getOutTradeNo();
		redisLockUtils.lock(lockKey);
		try {
			return BusinessResponseFactory.createSuccess(service.createOrder(param,appList.getUserId()));
		}finally {
			redisLockUtils.releaseLock(lockKey);
		}
		
	}

	@RequestMapping(value = "/orderpayv2",method = RequestMethod.POST)
	@ApiOperation(value = "订单支付", notes = "订单支付", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<OrderPayResult> orderPayV2(@RequestBody PayParams params){
		if(params.getClientType().equals(BusinessEnum.ClientType.H5.getCode())){
			String clientIp = getClientIp();
			if(StringUtils.isEmpty(clientIp)){
				throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
			}
			params.setClientIp(clientIp);
		}
		AuthUserInfo userInfo = getUserInfo();
		params.setPayUserName(userInfo.getUserName());
		
		String lockKey = RedisKeyConstants.PAYMENT_ORDER_PRO + params.getAppId() + params.getOrderId();
		redisLockUtils.lock(lockKey);
		try {
			return BusinessResponseFactory.createSuccess(service.orderPayV2(params,userInfo));
		}finally {
			redisLockUtils.releaseLock(lockKey);
		}
		
	}

//	@RequestMapping(value = "/sign/orderpay",method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseData<SignOrderPayResult> signOrderPay(@RequestBody SignOrderPayReqParams params){
//		AppList appList = merchantAppService.selectByAppId(params.getAppId());
//		if(appList == null){
//			throw new BusinessException(BusinessExceptionCode.MERCHANT_SETTING_ERROR);
//		}
//		String sign = params.getSign();
//		if(!SignUtils.signCheck(params,sign,appList.getMerchantPublicKey())){
//			throw new BusinessException(BusinessExceptionCode.SIGN_ERROR);
//		}
//		
//		String lockKey = RedisKeyConstants.PAYMENT_ORDER_PRO + params.getAppId() + params.getOutTradeNo();
//		redisLockUtils.lock(lockKey);
//		try {
//			return BusinessResponseFactory.createSuccess(service.signOrderPay(params));
//		}finally {
//			redisLockUtils.releaseLock(lockKey);
//		}
//		
//	}




}
