package com.zatgo.zup.payment.remoteservice;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zatgo.zup.common.model.ResponseData;

import io.swagger.annotations.ApiOperation;

@FeignClient("zup-exchange-appapi")
public interface ExchangeRemoteService {

	@ApiOperation(value = "查询指定币种的法币汇率")
	@RequestMapping(value = "/exchange/appapi/rateByVirtualCoin/{virtualCoin}/{legalCoin}", name = "指定币对汇率查询", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Map<String, Map<String, BigDecimal>>> getRateByCoinPair(
			@PathVariable("virtualCoin") String virtualCoin,@PathVariable("legalCoin") String legalCoin);
}
