package com.zatgo.zup.payment.entity;

import com.zatgo.zup.common.model.PayCommonCreateReqParams;

import java.io.Serializable;

public class SignOrderPayResult extends PayCommonCreateReqParams implements Serializable {

	private String outTradeno;
	
	private String tradeNo;

	private String receiptUserId;
	
	private String payUserId;

	private String resultCode;

	public String getOutTradeno() {
		return outTradeno;
	}

	public void setOutTradeno(String outTradeno) {
		this.outTradeno = outTradeno;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getReceiptUserId() {
		return receiptUserId;
	}

	public void setReceiptUserId(String receiptUserId) {
		this.receiptUserId = receiptUserId;
	}

	public String getPayUserId() {
		return payUserId;
	}

	public void setPayUserId(String payUserId) {
		this.payUserId = payUserId;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
}
