package com.zatgo.zup.payment.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.payment.entity.AppCategory;

public interface AppCategoryMapper {
    int deleteByPrimaryKey(String appCategoryName);

    int insert(AppCategory record);

    int insertSelective(AppCategory record);

    AppCategory selectByPrimaryKey(String appCategoryName);

    int updateByPrimaryKeySelective(AppCategory record);

    int updateByPrimaryKey(AppCategory record);
    
    List<AppCategory> selectAppCategoryListByCloudUserId(@Param("cloudUserId") String cloudUserId, @Param("language") Byte language);
}