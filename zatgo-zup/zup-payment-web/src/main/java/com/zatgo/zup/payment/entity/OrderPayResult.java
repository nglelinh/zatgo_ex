package com.zatgo.zup.payment.entity;

import com.zatgo.zup.common.model.WXAppReqParam;

import java.io.Serializable;

public class OrderPayResult implements Serializable {

	private String resultCode;
	private String resultInfo;
	private String paymentId;

	private WXAppReqParam wxAppReqParam;
	private String wxCodeUrl;
	private Byte thirdPayType;
	private String thirdPayResultCode;

	public String getWxCodeUrl() {
		return wxCodeUrl;
	}

	public void setWxCodeUrl(String wxCodeUrl) {
		this.wxCodeUrl = wxCodeUrl;
	}

	public String getResultInfo() {
		return resultInfo;
	}

	public void setResultInfo(String resultInfo) {
		this.resultInfo = resultInfo;
	}

	public String getThirdPayResultCode() {
		return thirdPayResultCode;
	}

	public void setThirdPayResultCode(String thirdPayResultCode) {
		this.thirdPayResultCode = thirdPayResultCode;
	}

	public Byte getThirdPayType() {
		return thirdPayType;
	}

	public void setThirdPayType(Byte thirdPayType) {
		this.thirdPayType = thirdPayType;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public WXAppReqParam getWxAppReqParam() {
		return wxAppReqParam;
	}

	public void setWxAppReqParam(WXAppReqParam wxAppReqParam) {
		this.wxAppReqParam = wxAppReqParam;
	}
}
