package com.zatgo.zup.payment.service.impl;

import static java.math.BigDecimal.ROUND_HALF_UP;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.BusinessEnum.CashPayType;
import com.zatgo.zup.common.enumtype.BusinessEnum.ClientType;
import com.zatgo.zup.common.enumtype.BusinessEnum.CoinScaleType;
import com.zatgo.zup.common.enumtype.BusinessEnum.MultiPay;
import com.zatgo.zup.common.enumtype.BusinessEnum.OrderType;
import com.zatgo.zup.common.enumtype.BusinessEnum.ResultCode;
import com.zatgo.zup.common.enumtype.CouponsEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AsyncPayResult;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.CheckoutOrderData;
import com.zatgo.zup.common.model.PayCallbackMsg;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.ReceiveMsg;
import com.zatgo.zup.common.model.RefundParams;
import com.zatgo.zup.common.model.RefundRequestParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.model.WXAppReqParam;
import com.zatgo.zup.common.model.coupons.InternalUsageCouponsOrderParams;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;
import com.zatgo.zup.common.model.payment.PayMoney;
import com.zatgo.zup.common.model.wallet.PaymentRecordData;
import com.zatgo.zup.common.model.wallet.WalletAccountData;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.common.mq.MQProducer;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.common.utils.SignUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.payment.entity.CheckoutConfig;
import com.zatgo.zup.payment.entity.CheckoutOrder;
import com.zatgo.zup.payment.entity.CreateOrderBizData;
import com.zatgo.zup.payment.entity.CreateOrderResult;
import com.zatgo.zup.payment.entity.OrderPayResult;
import com.zatgo.zup.payment.entity.RefundReturnData;
import com.zatgo.zup.payment.mapper.CheckoutConfigMapper;
import com.zatgo.zup.payment.mapper.CheckoutOrderMapper;
import com.zatgo.zup.payment.remoteservice.AccountRemoteService;
import com.zatgo.zup.payment.remoteservice.CouponsRemoteService;
import com.zatgo.zup.payment.remoteservice.PayRemoteService;
import com.zatgo.zup.payment.remoteservice.RefundRecordRemoteService;
import com.zatgo.zup.payment.remoteservice.UserRemoteService;
import com.zatgo.zup.payment.service.CheckoutConfigService;
import com.zatgo.zup.payment.service.CheckoutOrderService;
import com.zatgo.zup.payment.service.OrderPayService;
import com.zatgo.zup.payment.service.ThirdPayRecordService;
import com.zatgo.zup.payment.service.WxPayService;

@Service
public class OrderPayServiceImpl implements OrderPayService {
	private static final Logger logger = LoggerFactory.getLogger(OrderPayServiceImpl.class);
	
	@Autowired
	private MQProducer producer;

	@Autowired
	private CheckoutOrderMapper checkoutOrderMapper;
	
	@Autowired
	private CheckoutConfigMapper checkoutConfigMapper;

	@Autowired
	private UserRemoteService userService;
	@Autowired
	private PayRemoteService payService;
	@Autowired
	private WxPayService wxPayService;
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private RedisLockUtils redisLockUtils;
	@Autowired
	private RefundRecordRemoteService refundRecordRemoteService;
	@Autowired
	private ThirdPayRecordService thirdPayRecordService;
	@Autowired
	private CheckoutOrderService checkoutOrderService;
	@Autowired
	private CheckoutConfigService checkoutConfigService;
	@Autowired
	private AccountRemoteService accountRemoteService;
	@Value("${api-rsa.privateKey}")
	private String privateKey;
	@Autowired
	private CouponsRemoteService couponsRemoteService;

	@Override
	@Transactional
	public RefundReturnData refund(RefundRequestParams params,String cloudUserId) {
		CheckoutOrder checkoutOrder = checkoutOrderMapper.selectOrderByOrderIdAndUserId(params.getUserId(), params.getOrderId());
		if(checkoutOrder == null){
			throw new BusinessException(BusinessExceptionCode.CHECKOUT_ORDER_ID_ERROR);
		}
		
		UserData user = userService.getUserById(params.getUserId()).getData();
		if(user == null){
			throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		
		Byte orderStatus = checkoutOrder.getOrderStatus();
		Byte thirdPayStatus = checkoutOrder.getThirdPayStatus();
		if(!orderStatus.equals(BusinessEnum.OrderStatus.PAID.getCode())) {
			throw new BusinessException(BusinessExceptionCode.REFUND_STATUS_ERROR);
		}
		
		RefundReturnData returnData = new RefundReturnData();
		returnData.setNotifyUrl(params.getNotifyUrl());
		returnData.setUserId(params.getUserId());
		returnData.setRefundMoney(params.getAmount());
		returnData.setAttach(params.getAttach());
		returnData.setSign("");
		
		//代币已经退款的总额
		BigDecimal refund  = refundRecordRemoteService.sumRefund(checkoutOrder.getOrderId()).getData();
		BigDecimal totalZat = checkoutOrder.getOrderMoney();
		
		//法币已经退款的总额
		BigDecimal cashAmountPaid = checkoutOrder.getCashAmount();
		BigDecimal refundCash = thirdPayRecordService.refundAmountByOrderId(checkoutOrder.getOrderId());
		
		//计算优惠券金额
		BigDecimal couponsDeductionNum = computeCouponsSumMoney(
				CheckoutOrderData.parseCouponsListToIdList(checkoutOrder.getCouponsList()), 
				checkoutOrder.getOriginalPrice(), cloudUserId);
		
		
		BigDecimal amount = params.getAmount();
		BigDecimal payCoinScale = checkoutOrder.getPayCoinScale();
		BigDecimal cnyPrice = checkoutOrder.getCoinToCnyPrice();
		
		//计算需要退款的数量
		PayMoney compute = this.calMoney(amount,payCoinScale, cnyPrice);		
		BigDecimal cashMoney = compute.getCashMoney();
		BigDecimal token = compute.getToken();
		
		//判断是否超出可退金额
		if((totalZat.compareTo(refund.add(token)) < 0) 
				|| (cashAmountPaid.compareTo(refundCash.add(cashMoney)) < 0)){
			
			//如果存在优惠券，则扣减优惠券金额，再计算退款数据
			if(couponsDeductionNum.compareTo(BigDecimal.ZERO) > 0) {
				amount = amount.compareTo(couponsDeductionNum) < 0?BigDecimal.ZERO:amount.subtract(couponsDeductionNum);
				compute = this.calMoney(amount,payCoinScale, cnyPrice);		
				cashMoney = compute.getCashMoney();
				token = compute.getToken();
				if(totalZat.compareTo(refund.add(token)) < 0){
					logger.error("totalZat="+totalZat + ", refund=" + refund + ",token=" + token 
							+ ",cashAmountPaid=" + cashAmountPaid + ",refundCash=" 
							+ refundCash + ",cashMoney=" + cashMoney + ",couponsDeductionNum=" + couponsDeductionNum);
					throw new BusinessException(BusinessExceptionCode.REFUND_AMOUNT_MORETHAN_ORDER_AMOUNT);
				}
			}else {
				logger.error("totalZat="+totalZat + ", refund=" + refund + ",token=" + token 
						+ ",cashAmountPaid=" + cashAmountPaid + ",refundCash=" 
						+ refundCash + ",cashMoney=" + cashMoney + ",couponsDeductionNum=" + couponsDeductionNum);
				throw new BusinessException(BusinessExceptionCode.REFUND_AMOUNT_MORETHAN_ORDER_AMOUNT);
			}
		}

		ResponseData<String> refundRsp = null;
		String refundId = null;
		String coinType = checkoutOrder.getCoinType();
		Byte isMultiPay = checkoutOrder.getIsMultiPay();
		if(MultiPay.TRUE.getCode().equals(isMultiPay)){
			coinType = checkoutOrder.getMixCoinType();
		}
		if(token.compareTo(BigDecimal.ZERO) ==1 ){
			RefundParams refundParams = new RefundParams();
			refundParams.setCoinType(coinType);
			refundParams.setOderId(checkoutOrder.getOrderId());
			refundParams.setReceiveUserId(checkoutOrder.getPayUserId());
			refundParams.setRefundUserId(checkoutOrder.getReceiptUserId());
			refundParams.setOrderMoney(checkoutOrder.getOrderMoney());
			refundParams.setAmount(token);
			refundParams.setReceiveUserName(checkoutOrder.getPayUserName());
			refundParams.setRefundUserName(checkoutOrder.getReceiptUserName());
			try {
				refundRsp = payService.refund(refundParams);
				if(refundRsp != null){
				    refundId = refundRsp.getData();
				}
			} catch (Exception e) {
				if(refundRsp != null){
					returnData.setResultInfo(refundRsp.getMessage());
				}
				logger.error("代币退款失败",e);
			}
		}

		Byte thirdPayType = checkoutOrder.getThirdPayType();
		if(refundRsp !=null && refundRsp.isSuccessful() || token.compareTo(BigDecimal.ZERO) == 0){
			if(totalZat.compareTo(refund.add(token)) < 1){
				orderStatus = BusinessEnum.OrderStatus.REFUND.getCode();
			}
			if(cashMoney.compareTo(BigDecimal.ZERO) ==1 && thirdPayStatus.equals(BusinessEnum.OrderStatus.PAID.getCode())){
				if(cashAmountPaid.compareTo(refundCash.add(cashMoney)) < 0){
					logger.error("totalZat="+totalZat + ", refund=" + refund + ",token=" + token 
							+ ",cashAmountPaid=" + cashAmountPaid + ",refundCash=" 
							+ refundCash + ",cashMoney=" + cashMoney + ",couponsDeductionNum=" + couponsDeductionNum);
					throw new BusinessException(BusinessExceptionCode.REFUND_AMOUNT_MORETHAN_ORDER_AMOUNT);
					
				}

				if(thirdPayType.equals(CashPayType.WX.getCode())){
					try {
						if(cashMoney.compareTo(BigDecimal.ZERO) > 0) {
							refundId = wxPayService.wxRefund(checkoutOrder.getOrderId(), cashMoney);
						}
						
						returnData.setResultCode(ResultCode.SUCCESS.getCode());
						if(cashAmountPaid != null && cashAmountPaid.compareTo(refundCash.add(cashMoney)) < 1){
							thirdPayStatus = BusinessEnum.OrderStatus.REFUND.getCode();
						}
					} catch (Exception e) {
						logger.error("wxpay refund error",e);
						returnData.setResultCode(ResultCode.FAIL.getCode());
					}
				} else if(thirdPayType.equals(CashPayType.ALIPAY.getCode())){
				    
				}
			} else {
				returnData.setResultCode(ResultCode.SUCCESS.getCode());
				producer.send(MQContants.TOPIC_REFUND_CALLBACK,null, JSON.toJSONString(returnData),params.getUserId()+"-"+params.getOrderId());
			}
		} else if(refundRsp == null){
			orderStatus = BusinessEnum.OrderStatus.REFUND_UNKOWN_ERROR.getCode();
			thirdPayStatus = BusinessEnum.OrderStatus.REFUND_UNKOWN_ERROR.getCode();
		} else {
			returnData.setResultCode(ResultCode.FAIL.getCode());
			producer.send(MQContants.TOPIC_REFUND_CALLBACK,null, JSON.toJSONString(returnData),params.getUserId()+"-"+params.getOrderId());
		}
		checkoutOrderMapper.updateStatus(checkoutOrder.getOrderId(), orderStatus, thirdPayStatus);
		return returnData;
	}
	
	private BigDecimal mixedPaymentCoinToOtherCoin(String userId,BigDecimal total,String coinType) {
		CheckoutConfig config = checkoutConfigService.selectConfigByUserId(userId,coinType);
		if(config == null) {
			return total;
		}
		Byte type = config.getPayCoinScaleType();
		BigDecimal coinToCnyPrice = config.getCoinToCnyPrice();
		
		return total.multiply(coinToCnyPrice);
	}


	@Override
	public PayMoney computeMoney(String userId,BigDecimal total,String coinType) {
		CheckoutConfig config = checkoutConfigService.selectConfigByUserId(userId,coinType);
		if(config == null) {
			return null;
		}
		Byte type = config.getPayCoinScaleType();
		BigDecimal payCoinScale = config.getPayCoinScale();
		BigDecimal coinToCnyPrice = config.getCoinToCnyPrice();
		PayMoney payMoney = compute(total, type, payCoinScale, coinToCnyPrice);
		return payMoney;
	}

	private PayMoney compute(BigDecimal total, Byte type, BigDecimal payCoinScale, BigDecimal coinToCnyPrice) {
		PayMoney payMoney = new PayMoney();
		if(type.equals(CoinScaleType.FIXED.getCode())){
			BigDecimal fixed = payCoinScale;
			if(total.compareTo(fixed) <= 0){
			    fixed = total;
			}
			BigDecimal k = total.subtract(fixed);
			BigDecimal c = fixed.multiply(BigDecimal.ONE.divide(coinToCnyPrice));
			payMoney.setCashMoney(k);
			payMoney.setToken(c);
		} else {
			BigDecimal c = total.multiply(BigDecimal.ONE.subtract(payCoinScale.divide(new BigDecimal(100)))).setScale(2, ROUND_HALF_UP);
			BigDecimal t = total.subtract(c).multiply(BigDecimal.ONE.divide(coinToCnyPrice,8,ROUND_HALF_UP));
			payMoney.setToken(t);
			payMoney.setCashMoney(c);
		}
		return payMoney;
	}
	private PayMoney calMoney(BigDecimal total,BigDecimal payCoinScale, BigDecimal coinToCnyPrice){
		PayMoney payMoney = new PayMoney();
		BigDecimal c = total.multiply(BigDecimal.ONE.subtract(payCoinScale.divide(new BigDecimal(100)))).setScale(2, ROUND_HALF_UP);
		BigDecimal t = total.subtract(c).multiply(BigDecimal.ONE.divide(coinToCnyPrice,8,ROUND_HALF_UP));
		payMoney.setToken(t);
		payMoney.setCashMoney(c);
		return payMoney;
	}

	@Override
	public CreateOrderResult createOrder(CreateOrderBizData params,String userId) {
		CreateOrderResult result = new CreateOrderResult();
		UserData userData = userService.getUserById(userId).getData();
		String mixedCoinType = params.getMixedCoinType();
		Byte mixPay = MultiPay.FALSE.getCode();
		BigDecimal coinPrice = BigDecimal.ONE;
		
		if(!StringUtils.isEmpty(mixedCoinType)){
			CheckoutConfig checkoutConfig = checkoutConfigService.selectConfigByUserId(userData.getUserId(), params.getMixedCoinType());
			if(checkoutConfig == null){
				throw new BusinessException(BusinessExceptionCode.MERCHANT_SETTING_ERROR);
			}
			coinPrice = checkoutConfig.getCoinToCnyPrice();
			mixPay = MultiPay.TRUE.getCode();
		}
		CheckoutOrder order = checkoutOrderService.selectOrderByUserAndOrderId(userData.getUserId(), params.getOutTradeNo());
		String prepayId = null;
		String key = null;
		String timeoutExpress = params.getTimeoutExpress();
		Long expireTime = getExpireTime(timeoutExpress);
		if(order != null){
			key = RedisKeyConstants.PREPAY_ + order.getOrderId();
			prepayId = (String) redisTemplate.opsForValue().get(key);
			if(StringUtils.isEmpty(prepayId)){
				prepayId = UUIDUtils.getUuid();
			}
		} else {
			order = new CheckoutOrder();
			order.setOrderStatus(BusinessEnum.OrderStatus.PAYING.getCode());
			order.setCoinNetworkType(params.getCoinNetworkType());
			order.setCoinType(params.getCoinType());
			order.setOrderMoney(null);//虚拟币数量
			order.setReceiptUserId(userData.getUserId());
			String orderId = UUIDUtils.getUuid();
			key = RedisKeyConstants.PREPAY_ + orderId;
			prepayId = UUIDUtils.getUuid();
			order.setOrderId(orderId);
			order.setCreateDate(new Date());
			order.setUpdateDate(new Date());
			order.setOrderFlowCode(params.getOutTradeNo());
			order.setOrderType(OrderType.order.getCode());
			order.setCashAmount(null);//法币数量
			order.setIsMultiPay(mixPay);
			order.setReceiptUserName(userData.getUserName());
			order.setAppId(params.getAppId());
			order.setOrderName(params.getSubject());
			order.setOrderMemo(params.getBody());
			order.setThirdPayStatus(BusinessEnum.OrderStatus.PAYING.getCode());
			order.setMixCoinNetworkType(params.getMixedCoinNetworkType());
			order.setMixCoinType(params.getMixedCoinType());
			order.setCoinToCnyPrice(coinPrice);
			String notifyUrl = params.getNotifyUrl();
			order.setNotifyUrl(notifyUrl);
			order.setOriginalPrice(new BigDecimal(params.getCoinAmount()));
			checkoutOrderMapper.insertSelective(order);
		}
		redisTemplate.opsForValue().set(key,prepayId);
		redisTemplate.opsForValue().set(RedisKeyConstants.PREPAY_+prepayId,order.getOrderId());
		redisTemplate.expire(key,expireTime,TimeUnit.SECONDS);
		redisTemplate.expire(RedisKeyConstants.PREPAY_+prepayId,expireTime,TimeUnit.SECONDS);
		result.setOutTradeNo(order.getOrderFlowCode());
		result.setPrepayId(prepayId);
		result.setTradeNo(order.getOrderId());
		result.setAppId(params.getAppId());
		result.setSignCharset(params.getSignCharset());
		result.setSignType(params.getSignType());
		result.setTimestamp(DateTimeUtils.parseDateToString(new Date(),DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM_SS));
		result.setVersion("1.0");
		String sign = SignUtils.sign(MD5.getMD5Sign(result), privateKey);
		result.setSign(sign);
		return result;
	}
	public Long getExpireTime(String str){
		Long ext = null;
		if(str.contains("m")){
			String m = str.replace("m", "");
			ext = new BigDecimal(m).multiply(new BigDecimal(60)).longValue();
		} else if(str.contains("d")){
			String m = str.replace("d", "");
			ext = new BigDecimal(m).multiply(new BigDecimal(24)).multiply(new BigDecimal(3600)).longValue();
		} else if(str.contains("h")){
			String m = str.replace("h", "");
			ext = new BigDecimal(m).multiply(new BigDecimal(3600)).longValue();
		} else if(str.contains("c")){
			Date date = DateTimeUtils.endOfTodDay(new Date());
			ext = (System.currentTimeMillis()-date.getTime())/1000;
		}
		return ext;
	}
	@Override
	public PayMoney computeMoney(String orderId) {
		PayMoney payMoney = new PayMoney();
		CheckoutOrder checkoutOrder = checkoutOrderMapper.selectByPrimaryKey(orderId);
		Byte isMultiPay = checkoutOrder.getIsMultiPay();
		if(BusinessEnum.OrderStatus.PAID.getCode().equals(checkoutOrder.getOrderStatus())){
		    payMoney.setToken(checkoutOrder.getOrderMoney());
		    payMoney.setCashMoney(checkoutOrder.getCashAmount());
		    payMoney.setMainCoinType(checkoutOrder.getCoinType());
		    payMoney.setSecondaryCoinType(checkoutOrder.getMixCoinType());
		} else {
			if(MultiPay.TRUE.getCode().equals(isMultiPay)){
				CheckoutConfig config = checkoutConfigService.selectConfigByUserId(checkoutOrder.getReceiptUserId(),checkoutOrder.getMixCoinType());
				payMoney = compute(checkoutOrder.getOriginalPrice(), config.getPayCoinScaleType(), config.getPayCoinScale(), config.getCoinToCnyPrice());
				payMoney.setMainCoinType(checkoutOrder.getCoinType());
				payMoney.setSecondaryCoinType(checkoutOrder.getMixCoinType());
			} else {
				//todo 后期需要修改
				if("CNY".equals(checkoutOrder.getCoinType())){
					payMoney.setCashMoney(checkoutOrder.getOriginalPrice());
					payMoney.setSecondaryCoinType(checkoutOrder.getCoinType());
				} else {
					payMoney.setMainCoinType(checkoutOrder.getCoinType());
					payMoney.setToken(checkoutOrder.getOriginalPrice());
				}
			}
		}
		return payMoney;
	}
	
	/**
	 * 验证用户的使用优惠券
	 * @param params
	 * @param userId
	 * @param cloudUserId
	 * @return
	 */
	private ResponseData<List<UserCouponsModel>> checkUsageCoupons(CheckoutOrder checkoutOrder,PayParams params, String userId,String cloudUserId) {
		if(params.getUserCouponsIds() == null || params.getUserCouponsIds().size() == 0) {
			return BusinessResponseFactory.createSuccess(new ArrayList<>());
		}
		InternalUsageCouponsOrderParams usageCouponsOrder = new InternalUsageCouponsOrderParams();
		usageCouponsOrder.setCloudUserId(cloudUserId);
		usageCouponsOrder.setUserId(userId);
		usageCouponsOrder.setBusinessType(null);
		usageCouponsOrder.setOrderId(params.getCheckoutOrderId());
		usageCouponsOrder.setOrderMoney(params.getOriginalPrice());
		usageCouponsOrder.setSubBusinessCode(checkoutOrder.getOrderName());
		usageCouponsOrder.setUserLevel(null);
		usageCouponsOrder.setUserType(Byte.valueOf("2"));
		usageCouponsOrder.setUserCouponsIds(params.getUserCouponsIds());
		ResponseData<List<UserCouponsModel>> resp = couponsRemoteService.checkUsageCoupons(usageCouponsOrder);

		return resp;
	}
	
	/**
	 * 使用优惠券
	 * @param params
	 * @param userId
	 * @param cloudUserId
	 */
	private ResponseData<List<UserCouponsModel>> usageCoupons(CheckoutOrder checkoutOrder,PayParams params, String userId,String cloudUserId) {
		if(params.getUserCouponsIds() == null || params.getUserCouponsIds().size() == 0) {
			return BusinessResponseFactory.createSuccess(null);
		}
		InternalUsageCouponsOrderParams usageCouponsOrder = new InternalUsageCouponsOrderParams();
		usageCouponsOrder.setCloudUserId(cloudUserId);
		usageCouponsOrder.setUserId(userId);
		usageCouponsOrder.setBusinessType(null);
		usageCouponsOrder.setOrderId(params.getCheckoutOrderId());
		usageCouponsOrder.setOrderMoney(params.getOriginalPrice());
		usageCouponsOrder.setSubBusinessCode(checkoutOrder.getOrderName());
		usageCouponsOrder.setUserLevel(null);
		usageCouponsOrder.setUserType(Byte.valueOf("2"));
		usageCouponsOrder.setUserCouponsIds(params.getUserCouponsIds());
		ResponseData<List<UserCouponsModel>> resp = couponsRemoteService.usageCoupons(usageCouponsOrder);
		
		return resp;
		
	}
	
	/**
	 * 计算订单上的优惠券总价值
	 * @param userCouponsIds
	 * @param orderOriginalPrice
	 * @param cloudUserId
	 * @return
	 */
	private BigDecimal computeCouponsSumMoney(List<String> userCouponsIds,BigDecimal orderOriginalPrice,String cloudUserId) {
		BigDecimal couponsDeductionNum = BigDecimal.ZERO;
		if(userCouponsIds != null) {
			for(String userCouponsId:userCouponsIds) {
				ResponseData<UserCouponsModel> model = couponsRemoteService.getCouponsById(cloudUserId, userCouponsId);
				if(!model.isSuccessful()) {
					throw new BusinessException(model.getCode(),model.getMessage());
				}
				if(model.getData().getDiscountType().equals(CouponsEnum.CouponsDiscountType.discount.getCode())) {
					BigDecimal orderOriginalPriceByCoupons = orderOriginalPrice;
					orderOriginalPriceByCoupons = orderOriginalPriceByCoupons.subtract(couponsDeductionNum);
					couponsDeductionNum = couponsDeductionNum.add(
							orderOriginalPriceByCoupons.subtract(
									orderOriginalPriceByCoupons.multiply(model.getData().getCouponsPrice().divide(BigDecimal.TEN,3,BigDecimal.ROUND_UP))));
				}else {
					couponsDeductionNum = couponsDeductionNum.add(model.getData().getCouponsPrice());
				}
			}
			//当优惠券的金额大于订单总金额时，最大优惠为订单总金额
			if(couponsDeductionNum.compareTo(orderOriginalPrice) > 0) {
				couponsDeductionNum = orderOriginalPrice;
			}
		}
		
		return couponsDeductionNum;
	}

	@Override
	public OrderPayResult orderPayV2(PayParams params, AuthUserInfo userInfo) {
		
		//------------------------------------验证-----------------------------------------
		//收款人与支付人不能是同一个人
		if(userInfo.getUserId().equals(params.getReceiptUserId())){
			logger.error("支付人与被支付人不能是同一个人");
		    throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
		}
		
		//验证订单
		CheckoutOrder checkoutOrder = checkoutOrderMapper.selectByPrimaryKey(params.getCheckoutOrderId());
		if (checkoutOrder == null) {
			throw new BusinessException(BusinessExceptionCode.PAY_INFO_ERROR);
		} else {
			//订单不在支付中，支付失败，支付未知错误状态，不能重新支付
			Byte orderStatus = checkoutOrder.getOrderStatus() == null ? BusinessEnum.OrderStatus.PAYING.getCode():checkoutOrder.getOrderStatus();
			Byte thirdPayStatus = checkoutOrder.getThirdPayStatus() == null ? BusinessEnum.OrderStatus.PAYING.getCode():checkoutOrder.getThirdPayStatus();
			Byte usageCouponsStatus = checkoutOrder.getUsageCouponsStatus() == null ? BusinessEnum.OrderStatus.PAYING.getCode():checkoutOrder.getUsageCouponsStatus();
			if (!thirdPayStatus.equals(BusinessEnum.OrderStatus.PAYING.getCode()) 
					&& !thirdPayStatus.equals(BusinessEnum.OrderStatus.FAIL.getCode()) 
					&& !thirdPayStatus.equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode()) 
					&& !orderStatus.equals(BusinessEnum.OrderStatus.PAYING.getCode())
					&& !orderStatus.equals(BusinessEnum.OrderStatus.FAIL.getCode())
					&& !orderStatus.equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())
					&& !usageCouponsStatus.equals(BusinessEnum.OrderStatus.PAYING.getCode())
					&& !usageCouponsStatus.equals(BusinessEnum.OrderStatus.FAIL.getCode())
					&& !usageCouponsStatus.equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())) {
				throw new BusinessException(BusinessExceptionCode.CHECKOUT_ORDER_ID_DUPLICATE);
			}
			
			//只要有一个支付状态不在支付中，则不能修改这个订单的支付内容
			if(!orderStatus.equals(BusinessEnum.OrderStatus.PAYING.getCode())
					&& !orderStatus.equals(BusinessEnum.OrderStatus.FAIL.getCode())) {
				
				//虚拟币数量不能修改
				BigDecimal tokenAmount = params.getAmount() == null?BigDecimal.ZERO:params.getAmount();
				if(tokenAmount.compareTo(checkoutOrder.getOrderMoney()) != 0) {
					throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
				}
				
				//法币数量不能修改
				BigDecimal cashAmount = params.getCashAmount()==null?BigDecimal.ZERO:params.getCashAmount();
				if(cashAmount.compareTo(checkoutOrder.getCashAmount()) != 0) {
					throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
				}
				
				//优惠券不能修改
				Map<String,String> couponsMap = CheckoutOrderData.parseCouponsListToMap(checkoutOrder.getCouponsList());
				List<String> userCouponsIds = params.getUserCouponsIds() == null?new ArrayList<>():params.getUserCouponsIds();
				if( couponsMap.size() != userCouponsIds.size()) {
					throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
				}
				for(String userCouponsId:userCouponsIds) {
					if(!couponsMap.containsKey(userCouponsId)) {
						throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
					}
				}
			}
		}
		

		//验证支付的币数量或法定币金额不能少于0
		BigDecimal cashAmount = params.getCashAmount()==null?BigDecimal.ZERO:params.getCashAmount();
		if (params.getAmount().compareTo(BigDecimal.ZERO) == -1 || cashAmount != null && cashAmount.compareTo(BigDecimal.ZERO) == -1) {
			throw new BusinessException(BusinessExceptionCode.ERROR_AMOUNT_OF_MONEY);
		}
		
		//商家的APP ID不能为空
		String appId = checkoutOrder.getAppId();
		if(org.apache.commons.lang3.StringUtils.isEmpty(appId)) {
			throw new BusinessException();
		}
		
		//商家配置信息
		CheckoutConfig config = checkoutConfigService.selectConfigByAppid(checkoutOrder.getAppId(), params.getReceiptUserId(), params.getCoinType());
		if(config == null) {
			throw new BusinessException();
		}
		

		//计算优惠券金额
		BigDecimal couponsDeductionNum = computeCouponsSumMoney(params.getUserCouponsIds(), checkoutOrder.getOriginalPrice(), userInfo.getCloudUserId());
		
		
		//人民币不能小于指定比例
		PayMoney payMoney = compute(checkoutOrder.getOriginalPrice(), config.getPayCoinScaleType(), config.getPayCoinScale(), config.getCoinToCnyPrice());
		if (payMoney != null && cashAmount != null && cashAmount.add(couponsDeductionNum).compareTo(payMoney.getCashMoney()) == -1) {
			throw new BusinessException(BusinessExceptionCode.ERROR_AMOUNT_OF_MONEY);
		}

		//验证订单总金额与混合支付金额是否相等
		BigDecimal otherCoinNum = params.getAmount().multiply(config.getCoinToCnyPrice());
		if (checkoutOrder.getOriginalPrice().subtract(
				params.getCashAmount().add(otherCoinNum).add(couponsDeductionNum)).compareTo(new BigDecimal(0.2)) == 1) {
			throw new BusinessException(BusinessExceptionCode.ERROR_AMOUNT_OF_MONEY);
		}
		
		//流通量限制
		if(isLimitCheckoutCoinBalance(params.getCoinType(), params.getAmount(), appId)) {
			throw new BusinessException(BusinessExceptionCode.CHECKOUT_COIN_BALANCE_INSUFFICIENT);
		}
		
		//验证用户余额 
		if (checkoutOrder.getOrderStatus() == null
				|| checkoutOrder.getOrderStatus().equals(BusinessEnum.OrderStatus.PAYING.getCode()) 
				|| checkoutOrder.getOrderStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode()) 
				|| checkoutOrder.getOrderStatus().equals(BusinessEnum.OrderStatus.FAIL.getCode())) {
			if(params.getAmount() != null && params.getAmount().compareTo(BigDecimal.ZERO) > 0) {
				//如果为未知的状态，则先查询是否已支付过
				ResponseData<PaymentRecordData> payRecordResp = null;
				if(checkoutOrder.getOrderStatus() != null 
						&& checkoutOrder.getOrderStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())) {
					payRecordResp = payService.getPaymentRecordByTradeNo(checkoutOrder.getOrderId());
					if(!payRecordResp.isSuccessful()) {
						throw new BusinessException(payRecordResp.getCode(),payRecordResp.getMessage());
					}
				}
				//如果没有支付过，就验证用户余额，否则更新订单支付状态
				if(payRecordResp == null || payRecordResp.getData() == null) {
					ResponseData<WalletAccountData> accountDataResp = accountRemoteService.selectByUserIdAndType(params.getCoinType());
					if(!accountDataResp.isSuccessful()) {
						throw new BusinessException(accountDataResp.getCode(),accountDataResp.getMessage());
					}	
					WalletAccountData account = accountDataResp.getData();
					if(account == null || account.getBalance() == null || account.getBalance().compareTo(params.getAmount()) < 0) {
						throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
					}
				}else {
					//更新订单虚拟币支付状态为支付成功状态（独立事务）
					checkoutOrder.setOrderStatus(BusinessEnum.OrderStatus.PAID.getCode());
					checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
				}
			}
		}
		
		
		//验证用户使用的优惠券
		if(checkoutOrder.getUsageCouponsStatus() == null
				|| checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.PAYING.getCode()) 
				|| checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())
				|| (checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.FAIL.getCode()))) {
			if(params.getUserCouponsIds() != null && params.getUserCouponsIds().size() > 0) {
				
				//如果为未知状态，则先查询是否已支付
				ResponseData<List<UserCouponsModel>>  usedUserCouponsResp = null;
				if(checkoutOrder.getUsageCouponsStatus() != null &&
						checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())){
					usedUserCouponsResp = couponsRemoteService.getUsedUserCouponsByOrder(userInfo.getCloudUserId(), checkoutOrder.getOrderId());
					if(!usedUserCouponsResp.isSuccessful()) {
						throw new BusinessException(usedUserCouponsResp.getCode(),usedUserCouponsResp.getMessage());
					}
				}
				
				//如果已支付，则修改状态，如果未支付，则验证优惠券有效性
				if(usedUserCouponsResp == null || usedUserCouponsResp.getData() == null || usedUserCouponsResp.getData().size() == 0) {
					ResponseData<List<UserCouponsModel>> couponsCheckResult = checkUsageCoupons(checkoutOrder,params, userInfo.getUserId(), userInfo.getCloudUserId());
					if(!couponsCheckResult.isSuccessful()) {
						logger.error("优惠券使用验证失败" + couponsCheckResult.getCode() + " " + couponsCheckResult.getMessage() + JSON.toJSONString(params));
						throw new BusinessException(couponsCheckResult.getCode(),couponsCheckResult.getMessage());
					}
					List<UserCouponsModel> userCouponsModelList = couponsCheckResult.getData();
					checkoutOrder.setCouponsList(CheckoutOrderData.toCouponsList(userCouponsModelList));
				}else {
					//更新订单优惠券支付状态为支付成功状态（独立事务）
					List<UserCouponsModel> userCouponsModelList = usedUserCouponsResp.getData();
					checkoutOrder.setCouponsList(CheckoutOrderData.toCouponsList(userCouponsModelList));
					checkoutOrder.setUsageCouponsStatus(BusinessEnum.OrderStatus.PAID.getCode());
					checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
				}
				
			}
		}
		
		


		//------------------------------------开始支付-----------------------------------------
		
		//组装订单其它信息
		checkoutOrder.setMixCoinType(params.getCoinType());
		checkoutOrder.setMixCoinNetworkType(params.getNetworkType());
		checkoutOrder.setCoinToCnyPrice(config.getCoinToCnyPrice());
		checkoutOrder.setPayUserId(userInfo.getUserId());
		checkoutOrder.setPayUserName(params.getPayUserName());
		checkoutOrder.setReceiptUserName(params.getReceiptUserName());
		checkoutOrder.setUpdateDate(new Date());
		BigDecimal originalPrice = params.getOriginalPrice();
		BigDecimal cashPercent = cashAmount. divide(originalPrice, 8, ROUND_HALF_UP);
		BigDecimal tokenPercent = BigDecimal.ONE.subtract(cashPercent).multiply(new BigDecimal(100));
		checkoutOrder.setOrderMoney(params.getAmount());
		checkoutOrder.setCashAmount(cashAmount);
		checkoutOrder.setPayCoinScale(tokenPercent);
		checkoutOrder.setThirdPayType(params.getThirdPayType());
		checkoutOrder.setPaymentDate(new Date());


		//虚拟币支付
		ResponseData<String> coinPayReturnData = null;
		if (checkoutOrder.getOrderStatus() == null
				|| checkoutOrder.getOrderStatus().equals(BusinessEnum.OrderStatus.PAYING.getCode()) 
				|| checkoutOrder.getOrderStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode()) 
				|| checkoutOrder.getOrderStatus().equals(BusinessEnum.OrderStatus.FAIL.getCode())) {
			
			if(params.getAmount().compareTo(BigDecimal.ZERO) == 1) {
				
				//更新订单虚拟币支付状态为未知支付状态（独立事务）
				if(checkoutOrder.getOrderStatus() != null && !checkoutOrder.getOrderStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())) {
					checkoutOrder.setOrderStatus(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode());
					checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
				}

				//支付
				coinPayReturnData = payService.pay(params);
				if(!coinPayReturnData.isSuccessful()) {
					//更新虚拟币支付状态为支付失败（独立事务）
					checkoutOrder.setOrderStatus(BusinessEnum.OrderStatus.FAIL.getCode());
					checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
					logger.error("虚拟币支付失败" + coinPayReturnData.getCode() + " " + coinPayReturnData.getMessage() + JSON.toJSONString(params));
					throw new BusinessException(coinPayReturnData.getCode(),coinPayReturnData.getMessage());
				}
			}
			
			//更新虚拟币支付状态为已支付（独立事务）
			checkoutOrder.setOrderStatus(BusinessEnum.OrderStatus.PAID.getCode());
			checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
		}
		
		
		//优惠券支付
		if(checkoutOrder.getUsageCouponsStatus() == null
						|| checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.PAYING.getCode()) 
						|| checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())
						|| (checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.FAIL.getCode()))) {
			
			
			if(params.getUserCouponsIds() != null 
					&& params.getUserCouponsIds().size() > 0) {
				
				//更新订单优惠券支付状态为未知支付状态（独立事务）
				if(checkoutOrder.getUsageCouponsStatus() != null 
						&& !checkoutOrder.getUsageCouponsStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())) {
					checkoutOrder.setUsageCouponsStatus(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode());
					checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
				}
				
				
				//支付
				ResponseData<List<UserCouponsModel>> usageCouponsResp = usageCoupons(checkoutOrder,params,userInfo.getUserId(),userInfo.getCloudUserId());
				if(!usageCouponsResp.isSuccessful()) {
					//更新订单优惠券支付状态为支付失败状态（独立事务）
					checkoutOrder.setUsageCouponsStatus(BusinessEnum.OrderStatus.FAIL.getCode());
					checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
					
					logger.error("优惠券使用失败" + usageCouponsResp.getCode() + " " + usageCouponsResp.getMessage() + JSON.toJSONString(params));
					throw new BusinessException(usageCouponsResp.getCode() + " " + usageCouponsResp.getMessage());
				}
			}
			
			
			//更新优惠券支付状态为已支付（独立事务）
			checkoutOrder.setUsageCouponsStatus(BusinessEnum.OrderStatus.PAID.getCode());
			checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
		}

		
		//组装支付回调的信息
		AsyncPayResult asyncPayResult = new AsyncPayResult();
		asyncPayResult.setOutTradeNo(checkoutOrder.getOrderFlowCode());
		asyncPayResult.setPayUserId(userInfo.getUserId());
		asyncPayResult.setReceiptUserId(checkoutOrder.getReceiptUserId());
		asyncPayResult.setResultCode(ResultCode.SUCCESS.getCode());
		asyncPayResult.setAppId(checkoutOrder.getAppId());
		asyncPayResult.setSignCharset("utf-8");
		asyncPayResult.setSignType("RSA2");
		asyncPayResult.setTimestamp(DateTimeUtils.parseDateToString(new Date(),DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM_SS));
		asyncPayResult.setVersion("1.0");
		PayCallbackMsg<AsyncPayResult> payCallbackMsg = new PayCallbackMsg<AsyncPayResult>();
		payCallbackMsg.setResult(asyncPayResult);
		payCallbackMsg.setNotifyUrl(params.getNotifyUrl());
		payCallbackMsg.setOutTradeNo(checkoutOrder.getOrderFlowCode());
		payCallbackMsg.setPayUserId(checkoutOrder.getPayUserId());
		payCallbackMsg.setReceiptUserId(checkoutOrder.getReceiptUserId());
		payCallbackMsg.setPrepayId(params.getPrepayId());
		asyncPayResult.setTradeNo(checkoutOrder.getOrderId());
		
		
		//法币支付
		WXAppReqParam wxAppReqParam_app = null;
		String wxCodeUrl_h5 = null;
		Byte thirdPayStatus = null;
		if(checkoutOrder.getThirdPayStatus() == null 
				|| checkoutOrder.getThirdPayStatus().equals(BusinessEnum.OrderStatus.PAYING.getCode())
				|| checkoutOrder.getThirdPayStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())
				|| (checkoutOrder.getThirdPayStatus().equals(BusinessEnum.OrderStatus.FAIL.getCode()))) {
			
			if(params.getCashAmount().compareTo(BigDecimal.ZERO) == 1) {
				
				//先更新订单法币支付状态为未知支付状态（单独事务）
				if(checkoutOrder.getThirdPayStatus() != null && !checkoutOrder.getThirdPayStatus().equals(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode())) {
					checkoutOrder.setThirdPayStatus(BusinessEnum.OrderStatus.PAY_UNKOWN_ERROR.getCode());
					checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
				}
				
				
				Byte thirdPayType = params.getThirdPayType();
				if (thirdPayType.equals(CashPayType.WX.getCode())) {
					try {
						Byte clientType = params.getClientType();
						if (clientType.equals(ClientType.APP.getCode())) {
							wxAppReqParam_app = wxPayService.AppWxPay(params);
						} else if (clientType.equals(ClientType.H5.getCode())) {
							wxCodeUrl_h5 = wxPayService.h5WxPay(params);
						}
						
						asyncPayResult.setResultCode(ResultCode.SUCCESS.getCode());
						thirdPayStatus = BusinessEnum.OrderStatus.PAYING.getCode();
					} catch (Exception e) {
						logger.error("微信支付出错", e);
						throw new BusinessException();
					}	
				}
				//第三方支付回调时使用当前数据
				redisTemplate.opsForValue().set(checkoutOrder.getOrderId(), JSON.toJSONString(payCallbackMsg));
			}else {
				String sign = SignUtils.sign256(MD5.getMD5Sign(asyncPayResult), privateKey);
				asyncPayResult.setSign(sign);
				payCallbackMsg.setResultCode(ResultCode.SUCCESS.getCode());
				producer.send(MQContants.TOPIC_ORDER_RECEIVE, null, JSON.toJSONString(payCallbackMsg), userInfo.getUserId() + "-" + params.getOrderId());
				producer.send(MQContants.TOPIC_PAY_CALLBACK, null, JSON.toJSONString(payCallbackMsg), userInfo.getUserId() + "-" + params.getOrderId());
				thirdPayStatus = BusinessEnum.OrderStatus.PAID.getCode();
			}
			
			
			//更新法币支付状态为对应状态（独立事务）
			checkoutOrder.setThirdPayStatus(thirdPayStatus);
			checkoutOrderService.newTransactionalUpdateCheckoutOrder(checkoutOrder);
		}
		

		//组装返回结果
		OrderPayResult orderPayResult = new OrderPayResult();
		orderPayResult.setThirdPayType(params.getThirdPayType());
		orderPayResult.setWxAppReqParam(wxAppReqParam_app);
		orderPayResult.setWxCodeUrl(wxCodeUrl_h5);
		orderPayResult.setResultCode(ResultCode.SUCCESS.getCode());
		
		return orderPayResult;
	}
	
	/**
	 * 当前币种流通量限制(控制山寨币风险)
	 * @param coinType
	 * @param payTokenAmount
	 * @param appId
	 * @return
	 */
	private Boolean isLimitCheckoutCoinBalance(String coinType,BigDecimal payTokenAmount,String appId) {
		Boolean isLimit = false;
		Boolean isUpdateDB = false;
		//流通量限制
		String redisKey = RedisKeyConstants.PAYMENT_WEB_CHECKOUT_BALANCE + appId;
		redisLockUtils.lock(redisKey);
		try {
			Object balanceObj = redisTemplate.opsForValue().get(redisKey);
			HashMap<String,BigDecimal> balanceMap = null;
			if(balanceObj != null) {
				balanceMap = (HashMap<String,BigDecimal>)balanceObj;
				logger.info("读取到缓存信息-checkout-balance");
			}else {
				List<CheckoutConfig> configList = checkoutConfigService.selectConfigByAppid(appId);
				//logger.debug("测试信息" + JSON.toJSONString(configList));
				HashMap<String,BigDecimal> coinBalanceMap = new HashMap<>();
				for(CheckoutConfig conf:configList) {
					coinBalanceMap.put(conf.getCoinType().toLowerCase(), conf.getCoinPayBalance());
				}
				redisTemplate.opsForValue().set(redisKey, coinBalanceMap);
				balanceMap = coinBalanceMap;
				logger.info("更新缓存信息-checkout-balance");
			}
			
			BigDecimal balanceNum = (BigDecimal)balanceMap.get(coinType.toLowerCase());
			if(balanceNum != null) {
				if(payTokenAmount.compareTo(balanceNum) > 0) {
					isLimit = true;
					logger.error("商家余额不足，appId=" + appId + " coinType=" +coinType + " payTokenAmount=" + payTokenAmount + " balanceNum=" + balanceNum);
				}else {
					BigDecimal updateBalance = balanceNum.subtract(payTokenAmount);
					balanceMap.put(coinType.toLowerCase(), updateBalance);
					redisTemplate.opsForValue().set(redisKey, balanceMap);
					isUpdateDB = true;
					logger.info("商家余额redis检查通过，扣减商家余额成功");
				}
			}else {
				logger.info("当前币种不受余额限制");
			}

		}finally {
			redisLockUtils.releaseLock(redisKey);
		}
		
		if(isUpdateDB) {
			checkoutConfigMapper.subtractCoinPayBalance(coinType, appId, payTokenAmount);
		}
		
		
		return isLimit;
	}

//	@Override
//	public SignOrderPayResult signOrderPay(SignOrderPayReqParams params) {
//		SignOrderPayResult result = new SignOrderPayResult();
//		String payUserId = params.getPayUserId();
//		String receiptUserId = params.getReceiptUserId();
//		if(payUserId.equals(receiptUserId)){
//			throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
//		}
//		String outTradeNo = params.getOutTradeNo();
//		UserData payUser = userService.getUserById(payUserId).getData();
//		UserData rUser = userService.getUserById(receiptUserId).getData();
//		BigDecimal coinAmount = new BigDecimal(params.getCoinAmount());
//		//金额不能少于0
//		if(coinAmount.compareTo(BigDecimal.ZERO) ==-1){
//			throw new BusinessException(BusinessExceptionCode.ERROR_AMOUNT_OF_MONEY);
//		}
//		CheckoutOrder checkoutOrder = checkoutOrderMapper.selectOrderByOrderIdAndUserId(receiptUserId,outTradeNo);
//		if(checkoutOrder != null) {
//			Byte orderStatus = checkoutOrder.getOrderStatus();
//			if(BusinessEnum.OrderStatus.PAID.getCode().equals(orderStatus)){
//				throw new BusinessException(BusinessExceptionCode.CHECKOUT_ORDER_ID_DUPLICATE);
//			}
//		}
//		String orderId = UUIDUtils.getUuid();
//		if(checkoutOrder == null) {
//			checkoutOrder = new CheckoutOrder();
//			checkoutOrder.setOrderStatus(BusinessEnum.OrderStatus.PAYING.getCode());
//			checkoutOrder.setPayUserId(payUserId);
//			checkoutOrder.setCoinNetworkType(params.getCoinNetworkType());
//			checkoutOrder.setOrderMoney(coinAmount);
//			checkoutOrder.setCoinType(params.getCoinType());
//			checkoutOrder.setReceiptUserId(params.getReceiptUserId());
//			checkoutOrder.setOrderId(orderId);
//			checkoutOrder.setCreateDate(new Date());
//			checkoutOrder.setUpdateDate(new Date());
//			checkoutOrder.setOrderFlowCode(outTradeNo);
//			checkoutOrder.setOrderType(OrderType.order.getCode());
//			checkoutOrder.setReceiptUserName(rUser.getUserName());
//			checkoutOrder.setPayUserName(payUser.getUserName());
//			checkoutOrder.setThirdPayStatus(BusinessEnum.OrderStatus.PAYING.getCode());
//			checkoutOrder.setAppId(params.getAppId());
//			checkoutOrderMapper.insertSelective(checkoutOrder);
//		}
//		PayParams payParams = new PayParams();
//		payParams.setCheckoutOrderId(checkoutOrder.getOrderId());
//		payParams.setAmount(checkoutOrder.getOrderMoney());
//		payParams.setCoinType(checkoutOrder.getCoinType());
//		payParams.setNetworkType(checkoutOrder.getCoinNetworkType());
//		payParams.setReceiptUserId(checkoutOrder.getReceiptUserId());
//		payParams.setReceiptUserName(checkoutOrder.getReceiptUserName());
//		payParams.setPayUserName(checkoutOrder.getPayUserName());
//		payParams.setPayPassword(params.getPayPassword());
//		//异步处理出错不影响当前事务
//		Byte orderStatus = checkoutOrder.getOrderStatus();
//		ResponseData<String> payReturnData = null;
//		if(!orderStatus.equals(BusinessEnum.OrderStatus.PAID.getCode())){
//			try {
//				payReturnData = payService.signPay(payParams,payUserId);
//			}catch(Exception e) {
//				logger.error("调用支付接口失败",e);
//			}
//		}
//		result.setOutTradeno(outTradeNo);
//		result.setPayUserId(payUserId);
//		result.setReceiptUserId(receiptUserId);
//		if (payReturnData.isSuccessful()){
//			orderStatus = BusinessEnum.OrderStatus.PAID.getCode();
//			result.setResultCode(ResultCode.SUCCESS.getCode());
//			result.setTradeNo(payReturnData.getData());
//			AsyncPayResult asyncPayResult = new AsyncPayResult();
//			asyncPayResult.setOutTradeNo(outTradeNo);
//			asyncPayResult.setPayUserId(payUserId);
//			asyncPayResult.setReceiptUserId(receiptUserId);
//			asyncPayResult.setTradeNo(payReturnData.getData());
//			asyncPayResult.setResultCode(ResultCode.SUCCESS.getCode());
//			asyncPayResult.setAppId(params.getAppId());
//			asyncPayResult.setSignCharset("utf-8");
//			asyncPayResult.setSignType("RSA2");
//			asyncPayResult.setTimestamp(DateTimeUtils.parseDateToString(new Date(),DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM_SS));
//			asyncPayResult.setVersion("1.0");
//			asyncPayResult.setSign(SignUtils.sign256(MD5.getMD5Sign(asyncPayResult),privateKey));
//			PayCallbackMsg payCallbackMsg = new PayCallbackMsg();
//			payCallbackMsg.setResult(asyncPayResult);
//			payCallbackMsg.setNotifyUrl(params.getNotifyUrl());
//			payCallbackMsg.setOutTradeNo(checkoutOrder.getOrderFlowCode());
//			payCallbackMsg.setPayUserId(checkoutOrder.getPayUserId());
//			payCallbackMsg.setReceiptUserId(checkoutOrder.getReceiptUserId());
//			producer.send(MQContants.TOPIC_PAY_CALLBACK, null, JSON.toJSONString(payCallbackMsg), payUserId + "-" + outTradeNo);
//		} else {
//			result.setResultCode(ResultCode.FAIL.getCode());
//			orderStatus = BusinessEnum.OrderStatus.FAIL.getCode();
//		}
//		result.setAppId(params.getAppId());
//		result.setSignCharset(params.getSignCharset());
//		result.setSignType(params.getSignType());
//		result.setTimestamp(DateTimeUtils.parseDateToString(new Date(),DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM_SS));
//		result.setVersion("1.0");
//		String sign = SignUtils.sign256(MD5.getMD5Sign(result), privateKey);
//		result.setSign(sign);
//		checkoutOrder.setOrderStatus(orderStatus);
//		checkoutOrderMapper.updateByPrimaryKeySelective(checkoutOrder);
//		return result;
//	}

	@Override
	public List<PayMoney> getOrderPayCoinList(String prepayId) {
		String orderId = (String)redisTemplate.opsForValue().get(RedisKeyConstants.PREPAY_ + prepayId);
		if(StringUtils.isEmpty(orderId)){
		    throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		
		CheckoutOrder checkoutOrder = checkoutOrderMapper.selectByPrimaryKey(orderId);
		if(checkoutOrder == null){
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		
		String appid = checkoutOrder.getAppId();
		if(StringUtils.isEmpty(appid)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		
		List<CheckoutConfig> configs = checkoutConfigService.selectConfigByAppid(appid);
		if(CollectionUtils.isEmpty(configs)) {
			return null;
		}
		
		String mainCoinType = checkoutOrder.getCoinType();
		
		BigDecimal total = checkoutOrder.getOriginalPrice();
		List<PayMoney> payMoneys = new ArrayList<PayMoney>();
		for (CheckoutConfig config : configs) {
			PayMoney payMoney = compute(total, config.getPayCoinScaleType(), config.getPayCoinScale(), config.getCoinToCnyPrice());
			payMoney.setSort(config.getSort());
			payMoney.setMainCoinType(mainCoinType);
			payMoney.setSecondaryCoinType(config.getCoinType());
			payMoneys.add(payMoney);
		}
		
		return payMoneys;
	}
}
