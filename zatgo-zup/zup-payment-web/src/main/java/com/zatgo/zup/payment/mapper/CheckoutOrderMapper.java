package com.zatgo.zup.payment.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;
import com.zatgo.zup.payment.entity.CheckoutOrder;

public interface CheckoutOrderMapper {
    int deleteByPrimaryKey(String orderId);

    int insert(CheckoutOrder record);

    int insertSelective(CheckoutOrder record);

    CheckoutOrder selectByPrimaryKey(String orderId);

    int updateByPrimaryKeySelective(CheckoutOrder record);

    int updateByPrimaryKey(CheckoutOrder record);

	@Update("update checkout_order set order_status = #{orderStatus},order_memo= #{orderMemo} where order_id = #{orderId} and receipt_user_id = #{userId}")
	int updateOrderStatus(@Param("userId")String userId, @Param("orderId")String orderId, @Param("orderStatus") Byte orderStatus, @Param("orderMemo") String orderMemo);
	@Update("update checkout_order set third_pay_status = #{status} where order_id = #{orderId}")
	int updateThirdPayStatus( @Param("orderId")String orderId, @Param("status") Byte status);
	@Update("update checkout_order set order_status = #{orderStatus},third_pay_status =#{thirdPayStatus} where order_id = #{orderId} ")
	int updateStatus(@Param("orderId")String orderId, @Param("orderStatus") Byte orderStatus,@Param("thirdPayStatus") Byte thirdPayStatus);

	CheckoutOrder selectOrderByOrderIdAndUserId(@Param("userId")String userId, @Param("orderFlowCode")String orderFlowCode);

	@Select("select * from checkout_order where pay_user_id = #{userId} order by create_date desc")
	Page<CheckoutOrder> selectOrderByUserId(@Param("userId") String userId);

	List<CheckoutOrder> selectPayErrorOrder();
	List<CheckoutOrder> selectRefundErrorOrder();

	CheckoutOrder selectOrderByOrderIdAndPayUserId(@Param("userId")String userId, @Param("orderFlowCode")String orderFlowCode);

	CheckoutOrder signOrderQuery(@Param("userId")String userId, @Param("id")String id);

	Page<CheckoutOrder> signOrderQueryPage(@Param("userId")String userId, @Param("start")String start,@Param("endTime")String end);
}