package com.zatgo.zup.payment;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ServletComponentScan
@EnableEurekaClient
@EnableFeignClients
@EnableSwagger2
@MapperScan("com.zatgo.zup.payment.mapper")
@ComponentScan(basePackages="com.zatgo.zup")
@EnableTransactionManagement
@EnableDiscoveryClient
public class ZupPaymentWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZupPaymentWebApplication.class, args);

	}
}
