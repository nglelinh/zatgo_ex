package com.zatgo.zup.payment.mapper;

import com.zatgo.zup.payment.entity.CheckoutConfig;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface CheckoutConfigMapper {
    int deleteByPrimaryKey(String configId);

    int insert(CheckoutConfig record);

    int insertSelective(CheckoutConfig record);

    CheckoutConfig selectByPrimaryKey(String configId);

    int updateByPrimaryKeySelective(CheckoutConfig record);

    int updateByPrimaryKey(CheckoutConfig record);

	CheckoutConfig selectConfigByUserId(@Param("userId")String userId, @Param("type") String coinType);
	
	List<CheckoutConfig> selectConfigByAppid(String appId);
	
	CheckoutConfig selectConfigByUserIdAndAppid(@Param("appId") String appId, @Param("userId")String userId, @Param("type") String coinType);
	
	@Select("select * from checkout_config where is_auto_sync_rate = true")
	List<CheckoutConfig> selectByAutoSyncRate();
	
	@Update("update checkout_config set coin_pay_balance = coin_pay_balance - #{subtractNum} where coin_type = #{coinType} and app_id = #{appId}")
	void subtractCoinPayBalance(@Param("coinType")String coinType,@Param("appId")String appId,@Param("subtractNum")BigDecimal subtractNum);
}