package com.zatgo.zup.payment.entity;

import com.zatgo.zup.common.model.PayCommonCreateReqParams;

public class SignOrderQueryParams extends PayCommonCreateReqParams {
	
	private String outTradeNo;
	
	private String tradeNo;

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
}
