package com.zatgo.zup.payment.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "应用信息")
public class AppData {
	
	@ApiModelProperty(value = "应用名称", required = true)
	private String appName;
	
	@ApiModelProperty(value = "应用图标", required = true)
	private String appImage;
	
	@ApiModelProperty(value = "应用访问类型 0：APP内跳转 1：h5地址跳转", required = true)
	private Byte appGotoType;
	
	@ApiModelProperty(value = "应用访问地址", required = true)
	private String appGotoAddress;
	
	@ApiModelProperty(value = "应用鉴权地址", required = false)
	private String appAuth;
	
	@ApiModelProperty(value = "应用状态：1-未开放；2-已开放", required = true)
	private Byte status;
	
	@ApiModelProperty(value = "升序排序字段", required = false)
	private Integer sort;

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppImage() {
		return appImage;
	}

	public void setAppImage(String appImage) {
		this.appImage = appImage;
	}

	public Byte getAppGotoType() {
		return appGotoType;
	}

	public void setAppGotoType(Byte appGotoType) {
		this.appGotoType = appGotoType;
	}

	public String getAppGotoAddress() {
		return appGotoAddress;
	}

	public void setAppGotoAddress(String appGotoAddress) {
		this.appGotoAddress = appGotoAddress;
	}

	public String getAppAuth() {
		return appAuth;
	}

	public void setAppAuth(String appAuth) {
		this.appAuth = appAuth;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

}
