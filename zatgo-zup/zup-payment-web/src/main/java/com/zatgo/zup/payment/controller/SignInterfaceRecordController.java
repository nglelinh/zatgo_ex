package com.zatgo.zup.payment.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.payment.entity.SignInterfaceRecord;
import com.zatgo.zup.payment.service.SignInterfaceRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/pay/sign/interface")
public class SignInterfaceRecordController {

	@Autowired
	private SignInterfaceRecordService service;

	@RequestMapping(value = "/record",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Boolean> insertRecord(@RequestBody SignInterfaceRecord record){
		return BusinessResponseFactory.createSuccess(service.insertRecord(record));
	}


}
