package com.zatgo.zup.payment.service.impl;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AicoinMarketModel;
import com.zatgo.zup.payment.service.IAicoinService;


/**
 * Created by chen on 2018/6/25.
 */
@Service
public class AicoinServiceImpl implements IAicoinService {

    protected Logger logger = LoggerFactory.getLogger(getClass().getName());

    private static int AICOIN_TIME_OUT = 60000;

    private static String AICOIN_URL = "https://www.aicoin.net.cn/api/second/tickers?fq=b6eed45b30ae87ab6ec40c061416fa9c&symbol=";
    //private static String AICOIN_URL = "https://www.aicoin.net.cn/symbols/";
    private static String AICOIN_REFERER = "https://www.aicoin.net.cn/";

    @Override
    public AicoinMarketModel getMarket(String marketName, String pairs) {

        logger.info("getMarket "+marketName+","+pairs);

        String[] pairsArray = pairs.toLowerCase().split("_");
        String currCoin = pairsArray[0];   //当前币种
        String baseCoin = pairsArray[1];   //汇率币种

        String url = AICOIN_URL+marketName.toLowerCase()+currCoin+baseCoin;


        String html = doRemote(url);

        JSONObject obj = JSONObject.parseObject(html);
        JSONObject ticker = obj.getJSONObject("ticker");
        String price = ticker.getString("last");

        String usdToCyn = "";
        String baseCoinPrice = "";
        JSONArray rateList = obj.getJSONObject("index_rate").getJSONArray("index");
        for(Object rate:rateList){
            JSONObject jr = (JSONObject) rate;
            String name = jr.getString("name");
            if(baseCoin.equalsIgnoreCase(name)
                    ||("usdt_huobipro".equalsIgnoreCase(name)&&"usdt".equalsIgnoreCase(baseCoin))){
                //汇率币种价格，CNY
                baseCoinPrice = jr.getString("price");
            }
            if("usd_sina".equalsIgnoreCase(name)){
                //美元汇率
                usdToCyn = jr.getString("price");
            }
        }

        if(StringUtils.isEmpty(baseCoinPrice)){
            throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR,"未找到基准币种汇率");
        }

        //币对汇率
        BigDecimal priceCoin = new BigDecimal(price).setScale(8,BigDecimal.ROUND_DOWN);;
        //币种人民币汇率
        BigDecimal priceCny = new BigDecimal(baseCoinPrice).multiply(priceCoin).setScale(4,BigDecimal.ROUND_DOWN);
        //币种美元汇率
        BigDecimal priceUsd = priceCny.divide(new BigDecimal(usdToCyn),4,BigDecimal.ROUND_DOWN);


        AicoinMarketModel model = new AicoinMarketModel();
        model.setMarketName(marketName);
        model.setPairs(pairs);
        model.setPrice(priceCoin);
        model.setPriceCny(priceCny);
        model.setPriceUsd(priceUsd);


        return model;
    }

    private String doRemote(String url){
//        String ipport = Data5uService.getIp();
        return webParseHtml(url,null);
    }

    public String webParseHtml(String url,String ipport) {
        String html = "";
        BrowserVersion[] versions = {BrowserVersion.BEST_SUPPORTED, BrowserVersion.CHROME, BrowserVersion.EDGE, BrowserVersion.FIREFOX_52};
        WebClient client = new WebClient(versions[(int)(versions.length * Math.random())]);
        try {
            client.getOptions().setThrowExceptionOnFailingStatusCode(false);
            client.getOptions().setJavaScriptEnabled(false);
            client.getOptions().setCssEnabled(false);
            client.getOptions().setThrowExceptionOnScriptError(false);
            client.getOptions().setTimeout(AICOIN_TIME_OUT);
            client.getOptions().setAppletEnabled(true);
            client.getOptions().setGeolocationEnabled(true);
            client.getOptions().setRedirectEnabled(true);

            client.addRequestHeader("Referer", AICOIN_REFERER);

            if (ipport != null) {
                ProxyConfig proxyConfig = new ProxyConfig((ipport.split(",")[0]).split(":")[0], Integer.parseInt((ipport.split(",")[0]).split(":")[1]));
                client.getOptions().setProxyConfig(proxyConfig);
            }

            Page page = client.getPage(url);
            WebResponse response = page.getWebResponse();

            if (response.getContentType().equals("application/json")) {

                html = response.getContentAsString();

            }else if(page.isHtmlPage()){
                html = ((HtmlPage)page).asXml();
                System.out.println(html);

                Document doc = Jsoup.parse(html);
                Elements els = doc.select(".current-price");
                Elements mainPrice = els.get(0).select(".main-price");
                String price = mainPrice.get(0).val();

                Elements subPrice = els.get(0).select(".sub-price");
                Elements subPriceTr = subPrice.get(0).select("tr");

                String priceCny = subPriceTr.get(0).child(0).val();
                String priceUsd = subPriceTr.get(0).child(1).val();

                logger.info(price+","+priceCny+","+priceUsd);
            }



        } catch (Exception e) {
           logger.error("",e);
        } finally {
            client.close();
        }
        return html;
    }


}
