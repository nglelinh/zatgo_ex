package com.zatgo.zup.payment.service.impl;

import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.payment.entity.WxpayParams;
import com.zatgo.zup.payment.mapper.WxpayParamsMapper;
import com.zatgo.zup.payment.service.WxpayParamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class WxpayParamsServiceImpl implements WxpayParamsService {
	@Autowired
	private WxpayParamsMapper wxpayParamsMapper;
	@Override
	public void insertParams(String codeUrl,String prepayId,String userId,String orderId) {
		WxpayParams wxpayParams = new WxpayParams();
		wxpayParams.setCodeUrl(codeUrl);
		wxpayParams.setCreateTime(new Date());
		wxpayParams.setOrderId(orderId);
		wxpayParams.setPrepayId(prepayId);
		wxpayParams.setRecordId(UUIDUtils.getUuid());
		wxpayParams.setUserId(userId);
		wxpayParamsMapper.insertSelective(wxpayParams);
	}

	@Override
	public WxpayParams selectByUserIdAndOrderId(String userId, String orderId) {
		return wxpayParamsMapper.selectByUserIdAndOrderId(userId,orderId);
	}
}
