package com.zatgo.zup.payment.remoteservice;

import com.zatgo.zup.common.model.RefundParams;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wallet.PaymentRecordData;

import io.swagger.annotations.ApiOperation;

@FeignClient("zup-wallet")
public interface PayRemoteService {

	@RequestMapping(value = "/wallet/pay",method = RequestMethod.POST)
	@ResponseBody
	ResponseData<String> pay(@RequestBody PayParams params);

	@RequestMapping(value = "/wallet/refund",method = RequestMethod.POST)
	@ResponseBody
	ResponseData<String> refund(@RequestBody RefundParams params);

//	@RequestMapping(value = "/wallet/sign/pay/{payUserId}",method = RequestMethod.POST)
//	@ResponseBody
//	ResponseData<String> signPay(@RequestBody PayParams params, @PathVariable("payUserId") String payUserId);

	@RequestMapping(value = "/wallet/payment/paymentRecord/{tradeNo}",method = RequestMethod.GET)
	public ResponseData<PaymentRecordData> getPaymentRecordByTradeNo(@PathVariable("tradeNo") String tradeNo);
}
