package com.zatgo.zup.payment.notice;

import javax.annotation.PostConstruct;

import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.mq.MQConsumer;
import com.zatgo.zup.common.mq.MQContants;

/**
 * 支付通知
 * @Title
 * @author lincm
 * @date 2018年5月8日 上午11:47:42
 */
@Component
public class PayNotice {
	private static final Logger logger = LoggerFactory.getLogger(PayNotice.class);

	@Autowired
	MQConsumer consumer;

	@PostConstruct
	public void consume(){
		try {
			PaymentMsgNoticeCallBack callBack = new PaymentMsgNoticeCallBack();
			consumer.startConsumer(MQContants.CONSUMER_GROUP_ORDER_RECEIVE,
					MQContants.TOPIC_ORDER_RECEIVE,null, callBack, MessageModel.BROADCASTING);
		} catch (Exception e) {
			logger.error("consumer error",e);
		}
	}
}
