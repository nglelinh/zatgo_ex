package com.zatgo.zup.payment.service.impl;

import com.zatgo.zup.payment.entity.SignInterfaceRecord;
import com.zatgo.zup.payment.mapper.SignInterfaceRecordMapper;
import com.zatgo.zup.payment.service.SignInterfaceRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SignInterfaceRecordServiceImpl implements SignInterfaceRecordService {
	@Autowired
	private SignInterfaceRecordMapper signInterfaceRecordMapper;
	@Override
	public Boolean insertRecord(SignInterfaceRecord record) {
		int i = signInterfaceRecordMapper.insertSelective(record);
		return i==1?true:false;
	}
}
