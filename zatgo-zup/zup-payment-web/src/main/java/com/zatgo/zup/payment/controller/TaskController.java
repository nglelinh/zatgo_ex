package com.zatgo.zup.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.payment.service.MerchantAppService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/pay/task",description = "定时器接口",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/pay/task")
public class TaskController extends BaseController{
	
	@Autowired
	private MerchantAppService merchantAppService;

	@ApiOperation(value = "获取币种市场价格")
	@RequestMapping(value = "/syncCheckoutCoinRate",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Boolean> syncCheckoutCoinRate(){
		merchantAppService.updateCheckoutCoinRate();
		
		return BusinessResponseFactory.createSuccess(true);
	}
}
