package com.zatgo.zup.payment.service.impl;

import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.payment.entity.ThirdPayRecord;
import com.zatgo.zup.payment.mapper.ThirdPayRecordMapper;
import com.zatgo.zup.payment.service.ThirdPayRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

@Component
public class ThirdPayRecordServiceImpl implements ThirdPayRecordService {
	@Autowired
	private ThirdPayRecordMapper thirdPayRecordMapper;
	@Override
	public String insertRecord(String thirdPayNumber, String orderId, String payUserId, Byte payType,
	                         BigDecimal money, String receiptUserId, Byte moneyFlow,Byte status) {
		ThirdPayRecord record = new ThirdPayRecord();
		record.setMoney(money);
		record.setMoneyFlow(moneyFlow);
		record.setThirdPayNumber(thirdPayNumber);
		record.setOrderId(orderId);
		record.setPayUserId(payUserId);
		record.setPayType(payType);
		record.setReceiptUserId(receiptUserId);
		record.setRecordId(UUIDUtils.getUuid());
		record.setRecordTime(new Date());
		record.setPaySuccess(status);
		thirdPayRecordMapper.insertSelective(record);
		return record.getRecordId();
	}

	@Override
	public ThirdPayRecord selectByCheckoutOrderId(String id) {
		return thirdPayRecordMapper.selectByCheckoutOrderId(id);
	}

	@Override
	public BigDecimal refundAmountByOrderId(String orderId) {
		return thirdPayRecordMapper.sumRefundAmountByOrderId(orderId);
	}
}
