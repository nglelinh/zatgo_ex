package com.zatgo.zup.payment.entity;

import java.io.Serializable;
import java.util.Date;

public class RegistAppParams implements Serializable {


	private String userId;

	private String appName;

	private String appImage;

	private Byte appType;

	private String gatewayUrl;
	
	private String merchantPublicKey;

	private String merchantGatewayUrl;

	private String merchantCallbackUrl;

	private Byte status;

	private Date createTime;

	private Date updateTime;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppImage() {
		return appImage;
	}

	public void setAppImage(String appImage) {
		this.appImage = appImage;
	}

	public Byte getAppType() {
		return appType;
	}

	public void setAppType(Byte appType) {
		this.appType = appType;
	}

	public String getGatewayUrl() {
		return gatewayUrl;
	}

	public void setGatewayUrl(String gatewayUrl) {
		this.gatewayUrl = gatewayUrl;
	}

	public String getMerchantPublicKey() {
		return merchantPublicKey;
	}

	public void setMerchantPublicKey(String merchantPublicKey) {
		this.merchantPublicKey = merchantPublicKey;
	}

	public String getMerchantGatewayUrl() {
		return merchantGatewayUrl;
	}

	public void setMerchantGatewayUrl(String merchantGatewayUrl) {
		this.merchantGatewayUrl = merchantGatewayUrl;
	}

	public String getMerchantCallbackUrl() {
		return merchantCallbackUrl;
	}

	public void setMerchantCallbackUrl(String merchantCallbackUrl) {
		this.merchantCallbackUrl = merchantCallbackUrl;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
