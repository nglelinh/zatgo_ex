package com.zatgo.zup.payment.mapper;

import com.zatgo.zup.payment.entity.SignInterfaceRecord;

public interface SignInterfaceRecordMapper {
    int insert(SignInterfaceRecord record);

    int insertSelective(SignInterfaceRecord record);
}