package com.zatgo.zup.payment.callback.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.model.WxPayCallbackData;
import com.zatgo.zup.common.model.WxPayReturnBack;
import com.zatgo.zup.payment.callback.service.ThirdPayCallBackService;
import com.zatgo.zup.payment.controller.BaseController;
import com.zatgo.zup.payment.entity.PayCallbackData;

import io.swagger.annotations.Api;

@Api(value = "/pay/callback",description = "微信回调",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/pay/callback")
public class ThirdPayCallBackController extends BaseController {
	@Autowired
	private ThirdPayCallBackService callBackService;
	
	private static Logger logger = LoggerFactory.getLogger(ThirdPayCallBackController.class);

	@RequestMapping(value = "/wxpay",method = RequestMethod.POST)
	@ResponseBody
	public WxPayReturnBack weixinPayCallback(@RequestBody WxPayCallbackData data) throws Exception {
//		if(!weixinPayService.getWeixinSign(BeanToMapUtilsK.beanToMap(data))){
//
//		}
		logger.info("微信回调参数：" + JSON.toJSONString(data));
		PayCallbackData payCallbackData = new PayCallbackData();
		payCallbackData.setLocalParams(data.getOut_trade_no());
		payCallbackData.setOutTradeNum(data.getTransaction_id());
		payCallbackData.setResultCode(data.getReturn_code());
		String resultCode = callBackService.payCallback(payCallbackData);
		WxPayReturnBack back = new WxPayReturnBack();
		back.setReturn_code(resultCode);
		return back;
	}
}
