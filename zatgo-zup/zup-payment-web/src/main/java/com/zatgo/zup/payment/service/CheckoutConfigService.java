package com.zatgo.zup.payment.service;

import java.util.List;

import com.zatgo.zup.payment.entity.CheckoutConfig;

public interface CheckoutConfigService {

	CheckoutConfig selectConfigByUserId(String userId,String coinType);

	List<CheckoutConfig> selectConfigByAppid(String appid);
	
	CheckoutConfig selectConfigByAppid(String appid, String userId, String coinType);

}
