package com.zatgo.zup.payment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class ThirdPayRecord implements Serializable {
    private String recordId;

    private String thirdPayNumber;

    private String orderId;

    private String payUserId;

    private Byte payType;

    private BigDecimal money;

    private String receiptUserId;

    private Byte moneyFlow;

    private Date recordTime;

    private Byte paySuccess;

    private static final long serialVersionUID = 1L;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getThirdPayNumber() {
        return thirdPayNumber;
    }

    public void setThirdPayNumber(String thirdPayNumber) {
        this.thirdPayNumber = thirdPayNumber;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPayUserId() {
        return payUserId;
    }

    public void setPayUserId(String payUserId) {
        this.payUserId = payUserId;
    }

    public Byte getPayType() {
        return payType;
    }

    public void setPayType(Byte payType) {
        this.payType = payType;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getReceiptUserId() {
        return receiptUserId;
    }

    public void setReceiptUserId(String receiptUserId) {
        this.receiptUserId = receiptUserId;
    }

    public Byte getMoneyFlow() {
        return moneyFlow;
    }

    public void setMoneyFlow(Byte moneyFlow) {
        this.moneyFlow = moneyFlow;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public Byte getPaySuccess() {
        return paySuccess;
    }

    public void setPaySuccess(Byte paySuccess) {
        this.paySuccess = paySuccess;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ThirdPayRecord other = (ThirdPayRecord) that;
        return (this.getRecordId() == null ? other.getRecordId() == null : this.getRecordId().equals(other.getRecordId()))
            && (this.getThirdPayNumber() == null ? other.getThirdPayNumber() == null : this.getThirdPayNumber().equals(other.getThirdPayNumber()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getPayUserId() == null ? other.getPayUserId() == null : this.getPayUserId().equals(other.getPayUserId()))
            && (this.getPayType() == null ? other.getPayType() == null : this.getPayType().equals(other.getPayType()))
            && (this.getMoney() == null ? other.getMoney() == null : this.getMoney().equals(other.getMoney()))
            && (this.getReceiptUserId() == null ? other.getReceiptUserId() == null : this.getReceiptUserId().equals(other.getReceiptUserId()))
            && (this.getMoneyFlow() == null ? other.getMoneyFlow() == null : this.getMoneyFlow().equals(other.getMoneyFlow()))
            && (this.getRecordTime() == null ? other.getRecordTime() == null : this.getRecordTime().equals(other.getRecordTime()))
            && (this.getPaySuccess() == null ? other.getPaySuccess() == null : this.getPaySuccess().equals(other.getPaySuccess()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRecordId() == null) ? 0 : getRecordId().hashCode());
        result = prime * result + ((getThirdPayNumber() == null) ? 0 : getThirdPayNumber().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getPayUserId() == null) ? 0 : getPayUserId().hashCode());
        result = prime * result + ((getPayType() == null) ? 0 : getPayType().hashCode());
        result = prime * result + ((getMoney() == null) ? 0 : getMoney().hashCode());
        result = prime * result + ((getReceiptUserId() == null) ? 0 : getReceiptUserId().hashCode());
        result = prime * result + ((getMoneyFlow() == null) ? 0 : getMoneyFlow().hashCode());
        result = prime * result + ((getRecordTime() == null) ? 0 : getRecordTime().hashCode());
        result = prime * result + ((getPaySuccess() == null) ? 0 : getPaySuccess().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", recordId=").append(recordId);
        sb.append(", thirdPayNumber=").append(thirdPayNumber);
        sb.append(", orderId=").append(orderId);
        sb.append(", payUserId=").append(payUserId);
        sb.append(", payType=").append(payType);
        sb.append(", money=").append(money);
        sb.append(", receiptUserId=").append(receiptUserId);
        sb.append(", moneyFlow=").append(moneyFlow);
        sb.append(", recordTime=").append(recordTime);
        sb.append(", paySuccess=").append(paySuccess);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}