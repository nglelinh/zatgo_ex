package com.zatgo.zup.payment.remoteservice;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.coupons.InternalUsageCouponsOrderParams;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;

import io.swagger.annotations.ApiOperation;


@FeignClient("zup-coupons")
public interface CouponsRemoteService {
	@RequestMapping(value = "/coupons/user/internal/usageCoupons",method = RequestMethod.POST)
	public ResponseData<List<UserCouponsModel>> usageCoupons(@RequestBody InternalUsageCouponsOrderParams params);

	
	@RequestMapping(value = "/coupons/user/internal/getUserCoupons/{cloudUserId}/{userCouponsId}",method = RequestMethod.GET)
	public ResponseData<UserCouponsModel> getCouponsById(@PathVariable("cloudUserId") String cloudUserId, 
			@PathVariable("userCouponsId") String userCouponsId);
	
	@RequestMapping(value = "/coupons/user/internal/checkUsageCoupons",method = RequestMethod.POST)
	public ResponseData<List<UserCouponsModel>> checkUsageCoupons(@RequestBody InternalUsageCouponsOrderParams params);
	
	@RequestMapping(value = "/coupons/user/internal/getUsedUserCouponsByOrder/{cloudUserId}/{orderId}",method = RequestMethod.GET)
	public ResponseData<List<UserCouponsModel>> getUsedUserCouponsByOrder(@PathVariable("cloudUserId") String cloudUserId,@PathVariable("orderId") String orderId);
}
