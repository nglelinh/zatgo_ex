package com.zatgo.zup.payment.notice;

import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zatgo.zup.common.model.AsyncPayResult;
import com.zatgo.zup.common.model.PayCallbackMsg;
import com.zatgo.zup.common.mq.MQConsumerCallBack;
import com.zatgo.zup.payment.websocket.PayWebSocket;
import com.zatgo.zup.payment.websocket.ReturnUrlWebSocket;

/**
 * 支付通知回调类
 * @Title
 * @author lincm
 * @date 2018年5月8日 上午11:48:10
 */
public class PaymentMsgNoticeCallBack implements MQConsumerCallBack{

	private static final Logger logger = LoggerFactory.getLogger(PaymentMsgNoticeCallBack.class);

	@Override
	public boolean callBack(String json) {
		try {
			PayCallbackMsg<AsyncPayResult> msg = JSON.parseObject(json, new TypeReference<PayCallbackMsg<AsyncPayResult>>() {
			});
			String key = msg.getReceiptUserId()+"_"+msg.getOutTradeNo();
			ConcurrentHashMap<String, Session> payment = PayWebSocket.cacheMap;
			Session session = payment.get(key);
			if(session != null){
				session.getBasicRemote().sendText(json);
			}
			ConcurrentHashMap<String, Session> redirect = ReturnUrlWebSocket.cacheMap;
			Session redirectSession = redirect.get(msg.getPrepayId());
			if(redirectSession != null){
				redirectSession.getBasicRemote().sendText(json);
			}
		}catch(Exception e) {
			logger.error(json,e);
		}

		return true;
	}

}
