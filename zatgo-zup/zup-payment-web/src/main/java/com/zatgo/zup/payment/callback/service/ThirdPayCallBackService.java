package com.zatgo.zup.payment.callback.service;

import com.zatgo.zup.common.model.WxPayCallbackData;
import com.zatgo.zup.common.model.WxPayReturnBack;
import com.zatgo.zup.payment.entity.PayCallbackData;

public interface ThirdPayCallBackService {

	String payCallback(PayCallbackData data);
	WxPayReturnBack wxRefundCallback(WxPayCallbackData data);


}
