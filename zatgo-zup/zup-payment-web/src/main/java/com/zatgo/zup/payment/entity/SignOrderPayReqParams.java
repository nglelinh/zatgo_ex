package com.zatgo.zup.payment.entity;

import com.zatgo.zup.common.model.PayCommonCreateReqParams;

public class SignOrderPayReqParams extends PayCommonCreateReqParams {
	
	private String payUserId;
	
	private String receiptUserId;

	private String notifyUrl;

	private String body;
	
	private String subject;
	
	private String outTradeNo;
	
	private String coinAmount;

	private String coinNetworkType;
	
	private String coinType;
	private String payPassword;

	public String getPayPassword() {
		return payPassword;
	}

	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}

	public String getPayUserId() {
		return payUserId;
	}

	public void setPayUserId(String payUserId) {
		this.payUserId = payUserId;
	}

	public String getReceiptUserId() {
		return receiptUserId;
	}

	public void setReceiptUserId(String receiptUserId) {
		this.receiptUserId = receiptUserId;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getCoinAmount() {
		return coinAmount;
	}

	public void setCoinAmount(String coinAmount) {
		this.coinAmount = coinAmount;
	}

	public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}
}
