package com.zatgo.zup.payment.entity;

import com.alibaba.fastjson.annotation.JSONField;

public class WxH5Info {
	
	public final static String h5_info = "h5_info";
	
	public final static String wap_url  = "wap_url";
	
	public final static String wap_name = "wap_name";
	
	public final static String mweb_url = "mweb_url";
	
	private String type;
	
	@JSONField(name = "app_name")
	private String appName;
	
	@JSONField(name = "package_name")
	private String packageName;
	
	@JSONField(name = "wap_url")
	private String wapUrl;
	
	@JSONField(name = "wap_name")
	private String wapName;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getWapUrl() {
		return wapUrl;
	}

	public void setWapUrl(String wapUrl) {
		this.wapUrl = wapUrl;
	}

	public String getWapName() {
		return wapName;
	}

	public void setWapName(String wapName) {
		this.wapName = wapName;
	}
	
	public enum Type {
		IOS("IOS"), ANDROID("Android"), WAP("Wap");
		
		private String type;
		
		private Type(String type) {
			this.type = type;
		}
		
		public String getType() {
			return type;
		}
		
	}

}
