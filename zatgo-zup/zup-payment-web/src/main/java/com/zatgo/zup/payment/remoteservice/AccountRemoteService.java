package com.zatgo.zup.payment.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wallet.WalletAccountData;

@FeignClient("zup-wallet")
public interface AccountRemoteService {

	@RequestMapping(value = "/wallet/account/primary/{type}",method = RequestMethod.GET )
	public ResponseData<WalletAccountData> selectByUserIdAndType(@PathVariable("type")String type);
}
