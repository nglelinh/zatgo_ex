package com.zatgo.zup.payment.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.payment.entity.AppCategoryData;
import com.zatgo.zup.payment.entity.RegistAppParams;
import com.zatgo.zup.payment.service.MerchantAppService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/pay/merchant",description = "商户相关",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(value = "/pay/merchant")
public class MerchantAppController extends BaseController {

	@Autowired
	private MerchantAppService merchantAppService;

	@RequestMapping(value = "/regist",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<String> regist(RegistAppParams params){
		AuthUserInfo userInfo = getUserInfo();
		params.setUserId(userInfo.getUserId());
		return BusinessResponseFactory.createSuccess(merchantAppService.regist(params));
	}

	@ApiOperation(value = "获取app应用列表")
	@RequestMapping(value = "/app/list", name = "获取app应用列表", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<List<AppCategoryData>> getAppCategoryList() {
		String cloudUserId = getUserInfo().getCloudUserId();
		
		List<AppCategoryData> datas = merchantAppService.getAppCategoryList(cloudUserId, getLanguage());
		return BusinessResponseFactory.createSuccess(datas);
	}

}
