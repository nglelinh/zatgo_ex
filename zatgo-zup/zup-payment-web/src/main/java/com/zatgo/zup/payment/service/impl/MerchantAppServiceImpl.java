package com.zatgo.zup.payment.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.payment.entity.AppCategory;
import com.zatgo.zup.payment.entity.AppCategoryData;
import com.zatgo.zup.payment.entity.AppData;
import com.zatgo.zup.payment.entity.AppList;
import com.zatgo.zup.payment.entity.CheckoutConfig;
import com.zatgo.zup.payment.entity.RegistAppParams;
import com.zatgo.zup.payment.mapper.AppCategoryMapper;
import com.zatgo.zup.payment.mapper.AppListMapper;
import com.zatgo.zup.payment.mapper.CheckoutConfigMapper;
import com.zatgo.zup.payment.remoteservice.ExchangeRemoteService;
import com.zatgo.zup.payment.service.MerchantAppService;
@Component
public class MerchantAppServiceImpl implements MerchantAppService {
	@Autowired
	private AppListMapper appListMapper;
	
	@Autowired
	private AppCategoryMapper appCategoryMapper;
	
	@Autowired
	private CheckoutConfigMapper checkoutConfigMapper;
	
	@Autowired
	ExchangeRemoteService exchangeRemoteService;
	
	@Override
	public String regist(RegistAppParams params) {
		AppList result = appListMapper.selectByUserIdAndAppName(params.getUserId(), params.getAppName());
		if(result != null){
		    throw new BusinessException(BusinessExceptionCode.APP_NAME_DUPLICATE);
		}
		AppList appList = new AppList();
		String appId = UUIDUtils.getUuid();
		appList.setAppId(appId);
		appList.setAppName(params.getAppName());
		appList.setAppPayType(params.getAppType());
		appList.setCreateTime(new Date());
		appList.setMerchantCallbackUrl(params.getMerchantCallbackUrl());
		appList.setMerchantGatewayUrl(params.getMerchantGatewayUrl());
		appList.setMerchantPublicKey(params.getMerchantPublicKey());
		appList.setStatus(BusinessEnum.MerchantAppStatus.FALSE.getCode());
		appListMapper.insertSelective(appList);
		return appId;
	}

	@Override
	public AppList selectByAppId(String appId) {
		return appListMapper.selectByPrimaryKey(appId);
	}
	
	@Override
	public List<AppCategoryData> getAppCategoryList(String cloudUserId, Byte language) {

		List<AppCategory> categories = appCategoryMapper.selectAppCategoryListByCloudUserId(cloudUserId, language);
		if (CollectionUtils.isEmpty(categories)) {
			return null;
		}

		// categories.sort((fir, sec) -> fir.getSort().compareTo(sec.getSort()));

		List<AppList> appLists = appListMapper.selectAppListByCloudUserId(cloudUserId, language);
		if (CollectionUtils.isEmpty(appLists)) {
			appLists = new ArrayList<AppList>();
		}

		List<AppCategoryData> datas = new ArrayList<>();
		for (AppCategory category : categories) {
			AppCategoryData categoryData = new AppCategoryData();
			categoryData.setAppCategoryName(category.getAppCategoryName());
			categoryData.setSort(category.getSort());

			List<AppData> appDatas = new ArrayList<>();

			Iterator<AppList> iterator = appLists.iterator();
			while (iterator.hasNext()) {
				AppList app = iterator.next();

				if (!app.getAppCategoryName().equals(category.getAppCategoryName())) {
					continue;
				}

				AppData appData = new AppData();
				appData.setAppGotoAddress(app.getAppGotoAddress());
				appData.setAppGotoType(app.getAppGotoType());
				appData.setAppImage(app.getAppImage());
				appData.setAppName(app.getAppName());
				appData.setAppAuth(app.getAppAuth());
				appData.setStatus(app.getStatus());
				appData.setSort(app.getSort());
				appDatas.add(appData);

				iterator.remove();
			}
			appDatas.sort((fir, sec) -> fir.getSort().compareTo(sec.getSort()));
			categoryData.setAppDatas(appDatas);
			datas.add(categoryData);
		}

		return datas;
	}

	@Override
	public void updateCheckoutCoinRate() {
		List<CheckoutConfig> list = checkoutConfigMapper.selectByAutoSyncRate();
		String legalCoin = "cny";
		for(CheckoutConfig config:list) {
			ResponseData<Map<String, Map<String, BigDecimal>>> rates = 
					exchangeRemoteService.getRateByCoinPair(config.getCoinType().toLowerCase(), legalCoin);
			
			if(rates.getData() != null) {
				Map<String, BigDecimal> rate =  rates.getData().get(legalCoin);
				if(rate != null) {
					BigDecimal coinRate = rate.get(config.getCoinType().toLowerCase());
					if(coinRate != null && coinRate.compareTo(BigDecimal.ZERO) > 0) {
						CheckoutConfig updateCheckoutConfig = new CheckoutConfig();
						updateCheckoutConfig.setConfigId(config.getConfigId());
						updateCheckoutConfig.setCoinToCnyPrice(coinRate);
						checkoutConfigMapper.updateByPrimaryKeySelective(updateCheckoutConfig);
					}
				}
			}
		}
		
		
	}
}
