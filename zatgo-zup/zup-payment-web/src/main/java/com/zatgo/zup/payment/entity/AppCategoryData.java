package com.zatgo.zup.payment.entity;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("app分类")
public class AppCategoryData {
	
	@ApiModelProperty(value = "分类名称", required = true)
	private String appCategoryName;
	
	@ApiModelProperty(value = "分类排序", required = true)
	private Integer sort;
	
	@ApiModelProperty(value = "APP应用", required = true)
	private List<AppData> appDatas;

	public String getAppCategoryName() {
		return appCategoryName;
	}

	public void setAppCategoryName(String appCategoryName) {
		this.appCategoryName = appCategoryName;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public List<AppData> getAppDatas() {
		return appDatas;
	}

	public void setAppDatas(List<AppData> appDatas) {
		this.appDatas = appDatas;
	}

}
