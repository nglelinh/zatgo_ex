package com.zatgo.zup.payment.websocket;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/ws/pay/paywebsocket/{userId}/{orderId}")
@Component
public class PayWebSocket {
	private static final Logger logger = LoggerFactory.getLogger(PayWebSocket.class);

	private Session session;
	public volatile static ConcurrentHashMap<String,Session> cacheMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String,String> userCache = new ConcurrentHashMap<>();

	@OnOpen
	public void onOpen(@PathParam("userId")String userId,@PathParam("orderId")String orderId,EndpointConfig config,Session session){
		this.session =session;
		cacheMap.put(userId+"_"+orderId,session);
	}
	@OnClose
	public void onClose(@PathParam("userId")String userId,@PathParam("orderId")String orderId){
		cacheMap.remove(userId+"_"+orderId);
		logger.info("websocket  disconnect");
	}
	public void sendMessage(String msg) throws IOException {
		this.session.getBasicRemote().sendText(msg);
	}
	@OnMessage
	public void onMessage(String message,Session session) throws IOException {

		sendMessage("websocket connect");
	}









}
