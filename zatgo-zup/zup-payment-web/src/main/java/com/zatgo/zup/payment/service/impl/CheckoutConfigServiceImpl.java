package com.zatgo.zup.payment.service.impl;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.payment.entity.CheckoutConfig;
import com.zatgo.zup.payment.mapper.CheckoutConfigMapper;
import com.zatgo.zup.payment.service.CheckoutConfigService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CheckoutConfigServiceImpl implements CheckoutConfigService {
	@Autowired
	private CheckoutConfigMapper checkoutConfigMapper;
	@Override
	public CheckoutConfig selectConfigByUserId(String userId,String coinType) {
		CheckoutConfig data = checkoutConfigMapper.selectConfigByUserId(userId,coinType);
		if(data == null){
		    throw new BusinessException(BusinessExceptionCode.USER_CONFIG_ERROR);
		}
		return data;
	}
	@Override
	public List<CheckoutConfig> selectConfigByAppid(String appid) {
		List<CheckoutConfig> configs = checkoutConfigMapper.selectConfigByAppid(appid);
		return configs;
	}
	@Override
	public CheckoutConfig selectConfigByAppid(String appid, String userId, String coinType) {
		return checkoutConfigMapper.selectConfigByUserIdAndAppid(appid, userId, coinType);
	}
}
