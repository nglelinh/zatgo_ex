package com.zatgo.zup.payment.mapper;

import com.zatgo.zup.payment.entity.WxpayParams;
import org.apache.ibatis.annotations.Param;

public interface WxpayParamsMapper {
    int deleteByPrimaryKey(String recordId);

    int insert(WxpayParams record);

    int insertSelective(WxpayParams record);

    WxpayParams selectByPrimaryKey(String recordId);

    int updateByPrimaryKeySelective(WxpayParams record);

    int updateByPrimaryKey(WxpayParams record);

    WxpayParams selectByUserIdAndOrderId(@Param("userId") String userId,@Param("orderId") String orderId);
}