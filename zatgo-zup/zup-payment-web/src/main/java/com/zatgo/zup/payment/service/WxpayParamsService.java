package com.zatgo.zup.payment.service;

import com.zatgo.zup.payment.entity.WxpayParams;

public interface WxpayParamsService {

	void insertParams(String codeUrl,String prepayId,String userId,String orderId);

	WxpayParams selectByUserIdAndOrderId(String userId,String orderId);
}
