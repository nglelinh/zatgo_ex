package com.zatgo.zup.activity.controller.bargain;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.controller.BaseController;
import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.activity.entity.request.BargainListRequest;
import com.zatgo.zup.activity.entity.response.ExamineListRespones;
import com.zatgo.zup.activity.model.AddActivityBargainParams;
import com.zatgo.zup.activity.model.UpdateActivityBargainParams;
import com.zatgo.zup.activity.service.AdminService;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Api(value = "/activity/admin",description = "管理员砍价功能接口")
@RestController
@RequestMapping(value = "/activity/admin")
public class AdminController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);


	@Autowired
	private AdminService adminService;

	@RequestMapping(value = "/addActivity",method = RequestMethod.POST)
	@ApiOperation(value = "新增砍价活动", notes = "新增砍价活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> addActivityBargain(@RequestBody AddActivityBargainParams params) {
		ThirdLoginInfo info = getAdminInfo();
		adminService.addActivityBargain(params, info);
		return BusinessResponseFactory.createSuccess(null);
	}

	@RequestMapping(value = "/updateActivity",method = RequestMethod.POST)
	@ApiOperation(value = "修改砍价活动", notes = "修改砍价活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> updateActivityBargain(@RequestBody UpdateActivityBargainParams params) {
		ThirdLoginInfo info = getAdminInfo();
		adminService.updateActivityBargain(params, info);
		return BusinessResponseFactory.createSuccess(null);
	}

	@RequestMapping(value = "/offlineActivity/{activityBargainId}",method = RequestMethod.POST)
	@ApiOperation(value = "下线砍价活动", notes = "下线砍价活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> offlineActivityBargain(@PathVariable("activityBargainId") String activityBargainId) {
		ThirdLoginInfo info = getAdminInfo();
		adminService.offlineActivityBargain(activityBargainId, info);
		return BusinessResponseFactory.createSuccess(null);
	}

	@RequestMapping(value = "/onlineActivity/{activityBargainId}",method = RequestMethod.POST)
	@ApiOperation(value = "上线砍价活动", notes = "上线砍价活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> onlineActivityBargain(@PathVariable("activityBargainId") String activityBargainId) {
		ThirdLoginInfo info = getAdminInfo();
		adminService.onlineActivityBargain(activityBargainId, info);
		return BusinessResponseFactory.createSuccess(null);
	}

	@RequestMapping(value = "/getActivityList",method = RequestMethod.POST)
	@ApiOperation(value = "查询砍价活动列表", notes = "查询砍价活动列表", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData getActivityBargainList(@RequestBody BargainListRequest request){
		ThirdLoginInfo info = getAdminInfo();
		PageInfo<ActivityBargain> activityBargainList = adminService.getActivityBargainList(request, info);
		return BusinessResponseFactory.createSuccess(activityBargainList);
	}

	@RequestMapping(value = "/getActivity/{activityBargainId}",method = RequestMethod.POST)
	@ApiOperation(value = "根据ID查询砍价活动", notes = "根据ID查询砍价活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData getActivityBargain(@PathVariable("activityBargainId") String activityBargainId){
		ThirdLoginInfo info = getAdminInfo();
		ActivityBargain activityBargain = adminService.getActivityBargain(activityBargainId, info);
		return BusinessResponseFactory.createSuccess(activityBargain);
	}


	@RequestMapping(value = "/examine/list",method = RequestMethod.GET)
	@ApiOperation(value = "审核列表", notes = "根据ID查询砍价活动", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<PageInfo<ExamineListRespones>> examineList(@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
																   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
																   @RequestParam("status") Byte status,
																   @RequestParam("activityBargainId") String activityBargainId,
																   @RequestParam(value = "keyword", required = false) String keyword){
		ThirdLoginInfo info = getAdminInfo();
		PageInfo<ExamineListRespones> pageInfo = adminService.examineList(pageNo, pageSize, status, info, activityBargainId, keyword);
		return BusinessResponseFactory.createSuccess(pageInfo);
	}

	@RequestMapping(value = "/examine/execute",method = RequestMethod.GET)
	@ApiOperation(value = "审核", notes = "审核", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> execute(@RequestParam(value = "id") String id,
										 @RequestParam(value = "status", defaultValue = "-1") Byte status){
		ThirdLoginInfo info = getAdminInfo();
		adminService.execute(id, status, info);
		return BusinessResponseFactory.createSuccess(true);
	}

}
