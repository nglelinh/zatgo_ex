package com.zatgo.zup.activity.model;

import io.swagger.annotations.ApiModelProperty;

public class ActivityBargainSearchParams {

	@ApiModelProperty(value = "状态：0=录入\r\n" + 
			"1=上线\r\n" + 
			"2=下线",required = false)
	private Byte status;

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
}
