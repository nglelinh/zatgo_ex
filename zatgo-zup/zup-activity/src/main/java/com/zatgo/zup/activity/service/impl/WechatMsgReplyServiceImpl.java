package com.zatgo.zup.activity.service.impl;

import com.ykb.mall.common.redis.RedisKeyConstants;
import com.zatgo.zup.activity.entity.WechatMsgReply;
import com.zatgo.zup.activity.entity.WechatMsgReplyExample;
import com.zatgo.zup.activity.entity.WechatMsgReplyExample.Criteria;
import com.zatgo.zup.activity.entity.wx.WechatMsg.Event;
import com.zatgo.zup.activity.entity.wx.WechatMsg.MsgType;
import com.zatgo.zup.activity.mapper.WechatMsgReplyMapper;
import com.zatgo.zup.activity.service.WechatMsgReplyService;
import com.zatgo.zup.common.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class WechatMsgReplyServiceImpl implements WechatMsgReplyService {

	@Autowired
	private WechatMsgReplyMapper wechatMsgReplyMapper;

	@Autowired
	private RedisUtils redisUtils;

	@Override
	public String selectWechatMsgReply(MsgType msgType, Event event, String key) {

		String msgKey = String.format(RedisKeyConstants.WECHAT_MSG_REPLY, msgType.getType(),
				event == null ? "" : event.getEvent(), key);

		boolean hasKey = redisUtils.hasKey(msgKey);
		if (hasKey) {
			return redisUtils.get(msgKey, String.class);
		}

		WechatMsgReplyExample example = new WechatMsgReplyExample();
		Criteria criteria = example.createCriteria().andMsgTypeEqualTo(msgType.getType()).andKeyEqualTo(key);
		if (event != null) {
			criteria.andEventEqualTo(event.getEvent());
		}

		String reply = null;
		List<WechatMsgReply> replies = wechatMsgReplyMapper.selectByExample(example);
		if (!CollectionUtils.isEmpty(replies)) {
			reply = replies.get(0).getReply();
			redisUtils.put(msgKey, reply);
		}

		return reply;
	}

}
