package com.zatgo.zup.activity.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class AddBargainOrderParams {

	@ApiModelProperty(value = "三方用户ID",required = true)
	private String userId;
	
	@ApiModelProperty(value = "订单ID",required = true)
	private String orderId;
	
	@ApiModelProperty(value = "参加的砍价活动实例ID",required = true)
	private String activityBargainJoinId;
	
	@ApiModelProperty(value = "下单金额",required = true)
	private BigDecimal orderMoney;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(BigDecimal orderMoney) {
		this.orderMoney = orderMoney;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getActivityBargainJoinId() {
		return activityBargainJoinId;
	}

	public void setActivityBargainJoinId(String activityBargainJoinId) {
		this.activityBargainJoinId = activityBargainJoinId;
	}
	
	
}
