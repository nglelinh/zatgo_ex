package com.zatgo.zup.activity.model;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/3/15.
 */
public class AddGroupBookingActivityParams {


    @ApiModelProperty(value = "活动名称",required = true)
    private String productName;

    @ApiModelProperty(value = "砍价商品ID",required = true)
    private String productId;

    @ApiModelProperty(value = "拼团价",required = true)
    private BigDecimal activityPrice;

    @ApiModelProperty(value = "活动图片",required = true)
    private String activityImg;

    @ApiModelProperty(value = "活动开始时间",required = true)
    private Date activityStartDate;

    @ApiModelProperty(value = "活动结束时间",required = true)
    private Date activityEndDate;

    @ApiModelProperty(value = "成团有效时间(小时)",required = true)
    private Long buildGroupLimitHour;

    @ApiModelProperty(value = "支付时间(分)",required = true)
    private Long payTime;

    @ApiModelProperty(value = "成团人数",required = true)
    private Long buildGroupPeopleNum;

    @ApiModelProperty(value = "每人限购数量",required = true)
    private Long perLimitBookingNum;

    @ApiModelProperty(value = "是否可以超过人数限制",required = true)
    private Boolean isOutOfLimit;

    @ApiModelProperty(value = "分享的标题",required = true)
    private String shareTitle;

    @ApiModelProperty(value = "分享的描述",required = true)
    private String shareDesc;

    @ApiModelProperty(value = "分享的图片链接",required = true)
    private String shareImgUrl;

    @ApiModelProperty(value = "分享的类型",required = true)
    private String shareType;

    @ApiModelProperty(value = "分享的跳转链接",required = true)
    private String shareLink;
    @ApiModelProperty(value = "订单price_ids",required = true)
    private List<Long> priceIds;


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getActivityPrice() {
        return activityPrice;
    }

    public void setActivityPrice(BigDecimal activityPrice) {
        this.activityPrice = activityPrice;
    }

    public Date getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(Date activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public Date getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(Date activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public Long getBuildGroupLimitHour() {
        return buildGroupLimitHour;
    }

    public void setBuildGroupLimitHour(Long buildGroupLimitHour) {
        this.buildGroupLimitHour = buildGroupLimitHour;
    }

    public Long getBuildGroupPeopleNum() {
        return buildGroupPeopleNum;
    }

    public void setBuildGroupPeopleNum(Long buildGroupPeopleNum) {
        this.buildGroupPeopleNum = buildGroupPeopleNum;
    }

    public Long getPerLimitBookingNum() {
        return perLimitBookingNum;
    }

    public void setPerLimitBookingNum(Long perLimitBookingNum) {
        this.perLimitBookingNum = perLimitBookingNum;
    }

    public Boolean getOutOfLimit() {
        return isOutOfLimit;
    }

    public void setOutOfLimit(Boolean outOfLimit) {
        isOutOfLimit = outOfLimit;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareDesc() {
        return shareDesc;
    }

    public void setShareDesc(String shareDesc) {
        this.shareDesc = shareDesc;
    }

    public String getShareImgUrl() {
        return shareImgUrl;
    }

    public void setShareImgUrl(String shareImgUrl) {
        this.shareImgUrl = shareImgUrl;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public String getActivityImg() {
        return activityImg;
    }

    public void setActivityImg(String activityImg) {
        this.activityImg = activityImg;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public List<Long> getPriceIds() {
        return priceIds;
    }

    public void setPriceIds(List<Long> priceIds) {
        this.priceIds = priceIds;
    }
}
