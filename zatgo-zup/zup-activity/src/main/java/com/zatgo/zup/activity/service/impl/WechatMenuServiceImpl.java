package com.zatgo.zup.activity.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.activity.entity.WechatMenu;
import com.zatgo.zup.activity.entity.WechatMenuExample;
import com.zatgo.zup.activity.mapper.WechatMenuMapper;
import com.zatgo.zup.activity.model.WechatMenuButton;
import com.zatgo.zup.activity.model.WechatMenuData;
import com.zatgo.zup.activity.remoteservice.WechatRemoteService;
import com.zatgo.zup.activity.service.IWechatMenuService;
import com.zatgo.zup.common.http.OkHttpService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class WechatMenuServiceImpl implements IWechatMenuService {

	private Logger log = LoggerFactory.getLogger(getClass());

	private static String URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";
	
	@Value("${system.isDefault}")
    private Boolean isDefault;
    @Value("${system.cloudUserId}")
    private String defaultCloudUserId;
    @Value("${system.thirdAuthenticationUrl}")
    private String thirdAuthenticationUrl;
    @Value("${system.thirdAuthenticationAdminUrl}")
    private String thirdAuthenticationAdminUrl;
    @Value("${system.thirdAuthenticationToken}")
    private String thirdAuthenticationToken;
    @Value("${system.weixinOfficialAcctAppId}")
    private String weixinOfficialAcctAppId;
    @Value("${system.weixinOfficialAcctKey}")
    private String weixinOfficialAcctKey;

//	@Resource
//	private IWechatAccessTokenService wechatAccessTokenService;

	@Resource
	private OkHttpService okHttpService;

	@Resource
	private WechatMenuMapper wechatMenuMapper;

	@Autowired
	private WechatRemoteService wechatRemoteService;

	private enum MenuLevel {
		FIRST_MENU(1), SECOND_MENU(2);

		private Byte level;

		MenuLevel(Integer level) {
			this.level = level.byteValue();
		}

		public Byte getLevel() {
			return level;
		}
	}

	@Override
	public void generateWechatMenu(String cloudUserId) {
		
		WechatMenuExample example = new WechatMenuExample();
		example.createCriteria().andAppIdEqualTo(weixinOfficialAcctAppId);
		List<WechatMenu> menus = wechatMenuMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(menus)) {
			log.info("微信菜单未配置");
			return;
		}

		Iterator<WechatMenu> all = menus.iterator();

		List<WechatMenu> firMenu = new ArrayList<WechatMenu>();
		while (all.hasNext()) {
			WechatMenu menu = all.next();
			if (menu.getMenuLevel().equals(MenuLevel.FIRST_MENU.getLevel())) {
				firMenu.add(menu);
				all.remove();
			}
		}

		if (CollectionUtils.isEmpty(firMenu)) {
			log.error("微信一级菜单未配置");
			return;
		}

		Map<Long, List<WechatMenu>> secMenu = new HashMap<Long, List<WechatMenu>>();
		for (WechatMenu _firMenu : firMenu) {
			List<WechatMenu> _secMenu = new ArrayList<WechatMenu>();
			Iterator<WechatMenu> part = menus.iterator();
			while (part.hasNext()) {
				WechatMenu menu = part.next();
				if (_firMenu.getMenuId().equals(menu.getParentId())) {
					_secMenu.add(menu);
					part.remove();
				}
			}

			secMenu.put(_firMenu.getMenuId(), _secMenu);
		}

		firMenu.sort((min, max) -> min.getSort().compareTo(max.getSort()));

		List<WechatMenuButton> menuButtons = new ArrayList<WechatMenuButton>();
		for (WechatMenu menu : firMenu) {
			WechatMenuButton button = new WechatMenuButton();
			button.setAppid(menu.getAppid());
			button.setKey(menu.getKey());
			button.setMediaId(menu.getMediaId());
			button.setName(menu.getName());
			button.setPagepath(menu.getPagepath());
			button.setType(menu.getType());
			button.setUrl(menu.getUrl());
			List<WechatMenu> _secMenu = secMenu.get(menu.getMenuId());
			if (CollectionUtils.isNotEmpty(_secMenu)) {
				List<WechatMenuButton> buttons = new ArrayList<WechatMenuButton>();
				for (WechatMenu _menu : _secMenu) {
					WechatMenuButton _button = new WechatMenuButton();
					_button.setAppid(_menu.getAppid());
					_button.setKey(_menu.getKey());
					_button.setMediaId(_menu.getMediaId());
					_button.setName(_menu.getName());
					_button.setPagepath(_menu.getPagepath());
					_button.setType(_menu.getType());
					_button.setUrl(_menu.getUrl());
					buttons.add(_button);
				}

				button.setSub_button(buttons);
			}
			menuButtons.add(button);
		}

		WechatMenuData menu = new WechatMenuData();
		menu.setButton(menuButtons);
		String url = new StringBuffer(URL)
				.append(wechatRemoteService.getWechatAccessToken(weixinOfficialAcctAppId, weixinOfficialAcctKey).getData())
				.toString();
		String res = okHttpService.postStr(url, menu);
		log.info("微信创建菜单返回结果：" + res);
	}

	public static void main(String[] args) {
		String menu = "{\"button\":[{\"name\":\"去旅行\",\"sub_button\":[{\"type\":\"view\",\"name\":\"一块伴\",\"url\":\"http://yiqiban.wxtest.flybytrip.com\"}]}]}";
		JSONObject object = JSONObject.parseObject(menu);
		JSONArray button = object.getJSONArray("button");
		JSONObject jipiao = new JSONObject();
		jipiao.put("机票", "http://work.flybycloud.com:1018/flight/");
		jipiao.put("type","view");
		button.add(jipiao);

		System.out.println(object.toJSONString());
	}

}
