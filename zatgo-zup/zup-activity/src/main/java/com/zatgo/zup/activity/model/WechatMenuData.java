package com.zatgo.zup.activity.model;

import java.util.List;

public class WechatMenuData {
	
	private List<WechatMenuButton> button;

	public List<WechatMenuButton> getButton() {
		return button;
	}

	public void setButton(List<WechatMenuButton> button) {
		this.button = button;
	}

}
