package com.zatgo.zup.activity.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.entity.response.ActivityGroupBookingInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupDetailInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupInfoResponse;

import java.util.List;

/**
 * Created by 46041 on 2019/3/15.
 */
public interface GroupBookingUserService {


    ActivityGroupBookingInfoResponse getActivityInfo(String activityId, String userId, Integer pageSize);

    GroupDetailInfoResponse getActivityDetailInfo(String groupOrderId);

    List<GroupInfoResponse> groupOrdeInfo(String activityId, Integer pageSize);

    PageInfo<ActivityGroupBookingInfoResponse> groupOrdeList(Integer pageSize, Integer pageNo, Boolean start);
}
