package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityBargainListExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ActivityBargainListExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andActivityBargainListIdIsNull() {
            addCriterion("activity_bargain_list_id is null");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdIsNotNull() {
            addCriterion("activity_bargain_list_id is not null");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdEqualTo(String value) {
            addCriterion("activity_bargain_list_id =", value, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdNotEqualTo(String value) {
            addCriterion("activity_bargain_list_id <>", value, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdGreaterThan(String value) {
            addCriterion("activity_bargain_list_id >", value, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdGreaterThanOrEqualTo(String value) {
            addCriterion("activity_bargain_list_id >=", value, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdLessThan(String value) {
            addCriterion("activity_bargain_list_id <", value, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdLessThanOrEqualTo(String value) {
            addCriterion("activity_bargain_list_id <=", value, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdLike(String value) {
            addCriterion("activity_bargain_list_id like", value, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdNotLike(String value) {
            addCriterion("activity_bargain_list_id not like", value, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdIn(List<String> values) {
            addCriterion("activity_bargain_list_id in", values, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdNotIn(List<String> values) {
            addCriterion("activity_bargain_list_id not in", values, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdBetween(String value1, String value2) {
            addCriterion("activity_bargain_list_id between", value1, value2, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainListIdNotBetween(String value1, String value2) {
            addCriterion("activity_bargain_list_id not between", value1, value2, "activityBargainListId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdIsNull() {
            addCriterion("activity_bargain_join_id is null");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdIsNotNull() {
            addCriterion("activity_bargain_join_id is not null");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdEqualTo(String value) {
            addCriterion("activity_bargain_join_id =", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdNotEqualTo(String value) {
            addCriterion("activity_bargain_join_id <>", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdGreaterThan(String value) {
            addCriterion("activity_bargain_join_id >", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdGreaterThanOrEqualTo(String value) {
            addCriterion("activity_bargain_join_id >=", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdLessThan(String value) {
            addCriterion("activity_bargain_join_id <", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdLessThanOrEqualTo(String value) {
            addCriterion("activity_bargain_join_id <=", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdLike(String value) {
            addCriterion("activity_bargain_join_id like", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdNotLike(String value) {
            addCriterion("activity_bargain_join_id not like", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdIn(List<String> values) {
            addCriterion("activity_bargain_join_id in", values, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdNotIn(List<String> values) {
            addCriterion("activity_bargain_join_id not in", values, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdBetween(String value1, String value2) {
            addCriterion("activity_bargain_join_id between", value1, value2, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdNotBetween(String value1, String value2) {
            addCriterion("activity_bargain_join_id not between", value1, value2, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyIsNull() {
            addCriterion("bargain_money is null");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyIsNotNull() {
            addCriterion("bargain_money is not null");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyEqualTo(BigDecimal value) {
            addCriterion("bargain_money =", value, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyNotEqualTo(BigDecimal value) {
            addCriterion("bargain_money <>", value, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyGreaterThan(BigDecimal value) {
            addCriterion("bargain_money >", value, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("bargain_money >=", value, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyLessThan(BigDecimal value) {
            addCriterion("bargain_money <", value, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("bargain_money <=", value, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyIn(List<BigDecimal> values) {
            addCriterion("bargain_money in", values, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyNotIn(List<BigDecimal> values) {
            addCriterion("bargain_money not in", values, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bargain_money between", value1, value2, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andBargainMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bargain_money not between", value1, value2, "bargainMoney");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdIsNull() {
            addCriterion("weixin_open_id is null");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdIsNotNull() {
            addCriterion("weixin_open_id is not null");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdEqualTo(String value) {
            addCriterion("weixin_open_id =", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdNotEqualTo(String value) {
            addCriterion("weixin_open_id <>", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdGreaterThan(String value) {
            addCriterion("weixin_open_id >", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdGreaterThanOrEqualTo(String value) {
            addCriterion("weixin_open_id >=", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdLessThan(String value) {
            addCriterion("weixin_open_id <", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdLessThanOrEqualTo(String value) {
            addCriterion("weixin_open_id <=", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdLike(String value) {
            addCriterion("weixin_open_id like", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdNotLike(String value) {
            addCriterion("weixin_open_id not like", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdIn(List<String> values) {
            addCriterion("weixin_open_id in", values, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdNotIn(List<String> values) {
            addCriterion("weixin_open_id not in", values, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdBetween(String value1, String value2) {
            addCriterion("weixin_open_id between", value1, value2, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdNotBetween(String value1, String value2) {
            addCriterion("weixin_open_id not between", value1, value2, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andWechatNickIsNull() {
            addCriterion("wechat_nick is null");
            return (Criteria) this;
        }

        public Criteria andWechatNickIsNotNull() {
            addCriterion("wechat_nick is not null");
            return (Criteria) this;
        }

        public Criteria andWechatNickEqualTo(String value) {
            addCriterion("wechat_nick =", value, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickNotEqualTo(String value) {
            addCriterion("wechat_nick <>", value, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickGreaterThan(String value) {
            addCriterion("wechat_nick >", value, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickGreaterThanOrEqualTo(String value) {
            addCriterion("wechat_nick >=", value, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickLessThan(String value) {
            addCriterion("wechat_nick <", value, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickLessThanOrEqualTo(String value) {
            addCriterion("wechat_nick <=", value, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickLike(String value) {
            addCriterion("wechat_nick like", value, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickNotLike(String value) {
            addCriterion("wechat_nick not like", value, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickIn(List<String> values) {
            addCriterion("wechat_nick in", values, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickNotIn(List<String> values) {
            addCriterion("wechat_nick not in", values, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickBetween(String value1, String value2) {
            addCriterion("wechat_nick between", value1, value2, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatNickNotBetween(String value1, String value2) {
            addCriterion("wechat_nick not between", value1, value2, "wechatNick");
            return (Criteria) this;
        }

        public Criteria andWechatImgIsNull() {
            addCriterion("wechat_img is null");
            return (Criteria) this;
        }

        public Criteria andWechatImgIsNotNull() {
            addCriterion("wechat_img is not null");
            return (Criteria) this;
        }

        public Criteria andWechatImgEqualTo(String value) {
            addCriterion("wechat_img =", value, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgNotEqualTo(String value) {
            addCriterion("wechat_img <>", value, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgGreaterThan(String value) {
            addCriterion("wechat_img >", value, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgGreaterThanOrEqualTo(String value) {
            addCriterion("wechat_img >=", value, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgLessThan(String value) {
            addCriterion("wechat_img <", value, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgLessThanOrEqualTo(String value) {
            addCriterion("wechat_img <=", value, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgLike(String value) {
            addCriterion("wechat_img like", value, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgNotLike(String value) {
            addCriterion("wechat_img not like", value, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgIn(List<String> values) {
            addCriterion("wechat_img in", values, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgNotIn(List<String> values) {
            addCriterion("wechat_img not in", values, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgBetween(String value1, String value2) {
            addCriterion("wechat_img between", value1, value2, "wechatImg");
            return (Criteria) this;
        }

        public Criteria andWechatImgNotBetween(String value1, String value2) {
            addCriterion("wechat_img not between", value1, value2, "wechatImg");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}