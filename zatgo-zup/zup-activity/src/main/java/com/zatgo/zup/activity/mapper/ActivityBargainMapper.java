package com.zatgo.zup.activity.mapper;


import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.common.model.ActivityListRespones;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityBargainMapper extends BaseMapper<ActivityBargain>{


    List<ActivityListRespones> myActivity(@Param("cloudUserId") String cloudUserId, @Param("userId") String userId);

    void addHelpPeople(@Param("num") int num, @Param("bargainActivityId") String bargainActivityId);
}