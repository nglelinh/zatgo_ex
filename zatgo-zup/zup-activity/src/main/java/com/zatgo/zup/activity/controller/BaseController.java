package com.zatgo.zup.activity.controller;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.model.wx.ThirdWxInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by 46041 on 2018/9/4.
 */
public class BaseController {

    @Resource
    protected HttpServletRequest request;

    @Resource
    protected HttpServletResponse response;

    @Resource
    protected HttpSession session;

    @Autowired
    protected RedisTemplate redisTemplate;

//    @Autowired
//    private ThirdService thirdService;



    @Value("${system.weixinOfficialAcctAppId}")
    private String weixinOfficialAcctAppId;
    @Value("${system.weixinOfficialAcctKey}")
    private String weixinOfficialAcctKey;

    protected ThirdLoginInfo getUserInfo(){
        String token = request.getHeader("token");
        AuthUserInfo userInfo = getUserInfo(token);
        String userId = userInfo.getUserId();
        ThirdWxInfo info = new ThirdWxInfo();
        info.setOpenId(userInfo.getWechatOpenId());
        info.setHeadUrl(userInfo.getIcon());
        info.setName(userInfo.getNickname());
        ThirdLoginInfo authorization = new ThirdLoginInfo();
        authorization.setUserName(userInfo.getUserName());
        authorization.setUserId(userId);
        authorization.setCloudUserId(userInfo.getCloudUserId());
        authorization.setCloudUserWXAppId(weixinOfficialAcctAppId);
        authorization.setCloudUserWXSecret(weixinOfficialAcctKey);
        authorization.setWxInfo(info);
        return authorization;
    }
    
    protected ThirdLoginInfo getAdminInfo(){
        return getUserInfo();
    }


    protected AuthUserInfo getUserInfo(String token){
        AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
        if(authUserInfo == null) {
            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
        }
        
        return authUserInfo;
    }
    
    protected AuthUserInfo getAdminInfo(String token){
        AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
        if(authUserInfo == null) {
            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
        }
        
        if(!authUserInfo.getIsAdmin()) {
        	throw new BusinessException(BusinessExceptionCode.USER_AUTH_ERROR);
        }
        
        return authUserInfo;
    }

    /**
     * 校验操作用户数据的权限
     * @param userId 被操作的用户ID
     * @return
     */
//    protected void verifyUserInterface(String userId) {
//
//        //是否内部接口调用
//        String innerAccessId = request.getHeader("innerAccessId");
//        if(innerAccessId != null && !innerAccessId.trim().equals("")) {
//            String isInnerAccessTrue = (String)redisTemplate.opsForValue().get(innerAccessId);
//            if(isInnerAccessTrue != null) {
//                return;
//            }
//        }
//
//        //如果不是内部接口调用，则只能操作自己的数据
//        AuthUserInfo userInfo = getUserInfo();
//        if(userInfo.getUserId().equals(userId)) {
//            return;
//        }else {
//            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
//        }
//    }

    protected String getClientIp(){
        return request.getHeader("client-ip");
    }

    protected String getAppType() {
        return request.getHeader("appType");
    }

    protected String getVersion() {
        return request.getHeader("appVersion");
    }
}
