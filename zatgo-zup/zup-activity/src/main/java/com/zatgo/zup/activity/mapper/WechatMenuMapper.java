package com.zatgo.zup.activity.mapper;

import com.zatgo.zup.activity.entity.WechatMenu;
import com.zatgo.zup.activity.entity.WechatMenuExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WechatMenuMapper {
    int countByExample(WechatMenuExample example);

    int deleteByExample(WechatMenuExample example);

    int deleteByPrimaryKey(Long menuId);

    int insert(WechatMenu record);

    int insertSelective(WechatMenu record);

    List<WechatMenu> selectByExample(WechatMenuExample example);

    WechatMenu selectByPrimaryKey(Long menuId);

    int updateByExampleSelective(@Param("record") WechatMenu record, @Param("example") WechatMenuExample example);

    int updateByExample(@Param("record") WechatMenu record, @Param("example") WechatMenuExample example);

    int updateByPrimaryKeySelective(WechatMenu record);

    int updateByPrimaryKey(WechatMenu record);
}