package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityBargainJoinExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ActivityBargainJoinExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andActivityBargainJoinIdIsNull() {
            addCriterion("activity_bargain_join_id is null");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdIsNotNull() {
            addCriterion("activity_bargain_join_id is not null");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdEqualTo(String value) {
            addCriterion("activity_bargain_join_id =", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdNotEqualTo(String value) {
            addCriterion("activity_bargain_join_id <>", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdGreaterThan(String value) {
            addCriterion("activity_bargain_join_id >", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdGreaterThanOrEqualTo(String value) {
            addCriterion("activity_bargain_join_id >=", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdLessThan(String value) {
            addCriterion("activity_bargain_join_id <", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdLessThanOrEqualTo(String value) {
            addCriterion("activity_bargain_join_id <=", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdLike(String value) {
            addCriterion("activity_bargain_join_id like", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdNotLike(String value) {
            addCriterion("activity_bargain_join_id not like", value, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdIn(List<String> values) {
            addCriterion("activity_bargain_join_id in", values, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdNotIn(List<String> values) {
            addCriterion("activity_bargain_join_id not in", values, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdBetween(String value1, String value2) {
            addCriterion("activity_bargain_join_id between", value1, value2, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andActivityBargainJoinIdNotBetween(String value1, String value2) {
            addCriterion("activity_bargain_join_id not between", value1, value2, "activityBargainJoinId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdIsNull() {
            addCriterion("bargain_activity_id is null");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdIsNotNull() {
            addCriterion("bargain_activity_id is not null");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdEqualTo(String value) {
            addCriterion("bargain_activity_id =", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdNotEqualTo(String value) {
            addCriterion("bargain_activity_id <>", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdGreaterThan(String value) {
            addCriterion("bargain_activity_id >", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdGreaterThanOrEqualTo(String value) {
            addCriterion("bargain_activity_id >=", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdLessThan(String value) {
            addCriterion("bargain_activity_id <", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdLessThanOrEqualTo(String value) {
            addCriterion("bargain_activity_id <=", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdLike(String value) {
            addCriterion("bargain_activity_id like", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdNotLike(String value) {
            addCriterion("bargain_activity_id not like", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdIn(List<String> values) {
            addCriterion("bargain_activity_id in", values, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdNotIn(List<String> values) {
            addCriterion("bargain_activity_id not in", values, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdBetween(String value1, String value2) {
            addCriterion("bargain_activity_id between", value1, value2, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdNotBetween(String value1, String value2) {
            addCriterion("bargain_activity_id not between", value1, value2, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(String value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(String value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(String value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(String value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(String value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLike(String value) {
            addCriterion("product_id like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotLike(String value) {
            addCriterion("product_id not like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<String> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<String> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(String value1, String value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(String value1, String value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNull() {
            addCriterion("product_name is null");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNotNull() {
            addCriterion("product_name is not null");
            return (Criteria) this;
        }

        public Criteria andProductNameEqualTo(String value) {
            addCriterion("product_name =", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotEqualTo(String value) {
            addCriterion("product_name <>", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThan(String value) {
            addCriterion("product_name >", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThanOrEqualTo(String value) {
            addCriterion("product_name >=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThan(String value) {
            addCriterion("product_name <", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThanOrEqualTo(String value) {
            addCriterion("product_name <=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLike(String value) {
            addCriterion("product_name like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotLike(String value) {
            addCriterion("product_name not like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameIn(List<String> values) {
            addCriterion("product_name in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotIn(List<String> values) {
            addCriterion("product_name not in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameBetween(String value1, String value2) {
            addCriterion("product_name between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotBetween(String value1, String value2) {
            addCriterion("product_name not between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIsNull() {
            addCriterion("product_img_abbr_url is null");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIsNotNull() {
            addCriterion("product_img_abbr_url is not null");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlEqualTo(String value) {
            addCriterion("product_img_abbr_url =", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotEqualTo(String value) {
            addCriterion("product_img_abbr_url <>", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlGreaterThan(String value) {
            addCriterion("product_img_abbr_url >", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlGreaterThanOrEqualTo(String value) {
            addCriterion("product_img_abbr_url >=", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLessThan(String value) {
            addCriterion("product_img_abbr_url <", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLessThanOrEqualTo(String value) {
            addCriterion("product_img_abbr_url <=", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLike(String value) {
            addCriterion("product_img_abbr_url like", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotLike(String value) {
            addCriterion("product_img_abbr_url not like", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIn(List<String> values) {
            addCriterion("product_img_abbr_url in", values, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotIn(List<String> values) {
            addCriterion("product_img_abbr_url not in", values, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlBetween(String value1, String value2) {
            addCriterion("product_img_abbr_url between", value1, value2, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotBetween(String value1, String value2) {
            addCriterion("product_img_abbr_url not between", value1, value2, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIsNull() {
            addCriterion("add_order_url is null");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIsNotNull() {
            addCriterion("add_order_url is not null");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlEqualTo(String value) {
            addCriterion("add_order_url =", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotEqualTo(String value) {
            addCriterion("add_order_url <>", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlGreaterThan(String value) {
            addCriterion("add_order_url >", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlGreaterThanOrEqualTo(String value) {
            addCriterion("add_order_url >=", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLessThan(String value) {
            addCriterion("add_order_url <", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLessThanOrEqualTo(String value) {
            addCriterion("add_order_url <=", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLike(String value) {
            addCriterion("add_order_url like", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotLike(String value) {
            addCriterion("add_order_url not like", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIn(List<String> values) {
            addCriterion("add_order_url in", values, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotIn(List<String> values) {
            addCriterion("add_order_url not in", values, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlBetween(String value1, String value2) {
            addCriterion("add_order_url between", value1, value2, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotBetween(String value1, String value2) {
            addCriterion("add_order_url not between", value1, value2, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIsNull() {
            addCriterion("product_money is null");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIsNotNull() {
            addCriterion("product_money is not null");
            return (Criteria) this;
        }

        public Criteria andProductMoneyEqualTo(BigDecimal value) {
            addCriterion("product_money =", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotEqualTo(BigDecimal value) {
            addCriterion("product_money <>", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyGreaterThan(BigDecimal value) {
            addCriterion("product_money >", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("product_money >=", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLessThan(BigDecimal value) {
            addCriterion("product_money <", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("product_money <=", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIn(List<BigDecimal> values) {
            addCriterion("product_money in", values, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotIn(List<BigDecimal> values) {
            addCriterion("product_money not in", values, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("product_money between", value1, value2, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("product_money not between", value1, value2, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductIntroIsNull() {
            addCriterion("product_intro is null");
            return (Criteria) this;
        }

        public Criteria andProductIntroIsNotNull() {
            addCriterion("product_intro is not null");
            return (Criteria) this;
        }

        public Criteria andProductIntroEqualTo(String value) {
            addCriterion("product_intro =", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotEqualTo(String value) {
            addCriterion("product_intro <>", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroGreaterThan(String value) {
            addCriterion("product_intro >", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroGreaterThanOrEqualTo(String value) {
            addCriterion("product_intro >=", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLessThan(String value) {
            addCriterion("product_intro <", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLessThanOrEqualTo(String value) {
            addCriterion("product_intro <=", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLike(String value) {
            addCriterion("product_intro like", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotLike(String value) {
            addCriterion("product_intro not like", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroIn(List<String> values) {
            addCriterion("product_intro in", values, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotIn(List<String> values) {
            addCriterion("product_intro not in", values, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroBetween(String value1, String value2) {
            addCriterion("product_intro between", value1, value2, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotBetween(String value1, String value2) {
            addCriterion("product_intro not between", value1, value2, "productIntro");
            return (Criteria) this;
        }

        public Criteria andBargainCountIsNull() {
            addCriterion("bargain_count is null");
            return (Criteria) this;
        }

        public Criteria andBargainCountIsNotNull() {
            addCriterion("bargain_count is not null");
            return (Criteria) this;
        }

        public Criteria andBargainCountEqualTo(Integer value) {
            addCriterion("bargain_count =", value, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountNotEqualTo(Integer value) {
            addCriterion("bargain_count <>", value, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountGreaterThan(Integer value) {
            addCriterion("bargain_count >", value, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("bargain_count >=", value, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountLessThan(Integer value) {
            addCriterion("bargain_count <", value, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountLessThanOrEqualTo(Integer value) {
            addCriterion("bargain_count <=", value, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountIn(List<Integer> values) {
            addCriterion("bargain_count in", values, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountNotIn(List<Integer> values) {
            addCriterion("bargain_count not in", values, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountBetween(Integer value1, Integer value2) {
            addCriterion("bargain_count between", value1, value2, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andBargainCountNotBetween(Integer value1, Integer value2) {
            addCriterion("bargain_count not between", value1, value2, "bargainCount");
            return (Criteria) this;
        }

        public Criteria andDealMoneyIsNull() {
            addCriterion("deal_money is null");
            return (Criteria) this;
        }

        public Criteria andDealMoneyIsNotNull() {
            addCriterion("deal_money is not null");
            return (Criteria) this;
        }

        public Criteria andDealMoneyEqualTo(BigDecimal value) {
            addCriterion("deal_money =", value, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyNotEqualTo(BigDecimal value) {
            addCriterion("deal_money <>", value, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyGreaterThan(BigDecimal value) {
            addCriterion("deal_money >", value, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("deal_money >=", value, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyLessThan(BigDecimal value) {
            addCriterion("deal_money <", value, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("deal_money <=", value, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyIn(List<BigDecimal> values) {
            addCriterion("deal_money in", values, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyNotIn(List<BigDecimal> values) {
            addCriterion("deal_money not in", values, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("deal_money between", value1, value2, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andDealMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("deal_money not between", value1, value2, "dealMoney");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderIsNull() {
            addCriterion("is_add_order is null");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderIsNotNull() {
            addCriterion("is_add_order is not null");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderEqualTo(Boolean value) {
            addCriterion("is_add_order =", value, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderNotEqualTo(Boolean value) {
            addCriterion("is_add_order <>", value, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderGreaterThan(Boolean value) {
            addCriterion("is_add_order >", value, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_add_order >=", value, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderLessThan(Boolean value) {
            addCriterion("is_add_order <", value, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderLessThanOrEqualTo(Boolean value) {
            addCriterion("is_add_order <=", value, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderIn(List<Boolean> values) {
            addCriterion("is_add_order in", values, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderNotIn(List<Boolean> values) {
            addCriterion("is_add_order not in", values, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderBetween(Boolean value1, Boolean value2) {
            addCriterion("is_add_order between", value1, value2, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andIsAddOrderNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_add_order not between", value1, value2, "isAddOrder");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNull() {
            addCriterion("pay_time is null");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNotNull() {
            addCriterion("pay_time is not null");
            return (Criteria) this;
        }

        public Criteria andPayTimeEqualTo(Date value) {
            addCriterion("pay_time =", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotEqualTo(Date value) {
            addCriterion("pay_time <>", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThan(Date value) {
            addCriterion("pay_time >", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("pay_time >=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThan(Date value) {
            addCriterion("pay_time <", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThanOrEqualTo(Date value) {
            addCriterion("pay_time <=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIn(List<Date> values) {
            addCriterion("pay_time in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotIn(List<Date> values) {
            addCriterion("pay_time not in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeBetween(Date value1, Date value2) {
            addCriterion("pay_time between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotBetween(Date value1, Date value2) {
            addCriterion("pay_time not between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayStatusIsNull() {
            addCriterion("pay_status is null");
            return (Criteria) this;
        }

        public Criteria andPayStatusIsNotNull() {
            addCriterion("pay_status is not null");
            return (Criteria) this;
        }

        public Criteria andPayStatusEqualTo(Boolean value) {
            addCriterion("pay_status =", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusNotEqualTo(Boolean value) {
            addCriterion("pay_status <>", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusGreaterThan(Boolean value) {
            addCriterion("pay_status >", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusGreaterThanOrEqualTo(Boolean value) {
            addCriterion("pay_status >=", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusLessThan(Boolean value) {
            addCriterion("pay_status <", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusLessThanOrEqualTo(Boolean value) {
            addCriterion("pay_status <=", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusIn(List<Boolean> values) {
            addCriterion("pay_status in", values, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusNotIn(List<Boolean> values) {
            addCriterion("pay_status not in", values, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusBetween(Boolean value1, Boolean value2) {
            addCriterion("pay_status between", value1, value2, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusNotBetween(Boolean value1, Boolean value2) {
            addCriterion("pay_status not between", value1, value2, "payStatus");
            return (Criteria) this;
        }

        public Criteria andExtInfoIsNull() {
            addCriterion("ext_info is null");
            return (Criteria) this;
        }

        public Criteria andExtInfoIsNotNull() {
            addCriterion("ext_info is not null");
            return (Criteria) this;
        }

        public Criteria andExtInfoEqualTo(String value) {
            addCriterion("ext_info =", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoNotEqualTo(String value) {
            addCriterion("ext_info <>", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoGreaterThan(String value) {
            addCriterion("ext_info >", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoGreaterThanOrEqualTo(String value) {
            addCriterion("ext_info >=", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoLessThan(String value) {
            addCriterion("ext_info <", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoLessThanOrEqualTo(String value) {
            addCriterion("ext_info <=", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoLike(String value) {
            addCriterion("ext_info like", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoNotLike(String value) {
            addCriterion("ext_info not like", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoIn(List<String> values) {
            addCriterion("ext_info in", values, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoNotIn(List<String> values) {
            addCriterion("ext_info not in", values, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoBetween(String value1, String value2) {
            addCriterion("ext_info between", value1, value2, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoNotBetween(String value1, String value2) {
            addCriterion("ext_info not between", value1, value2, "extInfo");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUserImgIsNull() {
            addCriterion("user_img is null");
            return (Criteria) this;
        }

        public Criteria andUserImgIsNotNull() {
            addCriterion("user_img is not null");
            return (Criteria) this;
        }

        public Criteria andUserImgEqualTo(String value) {
            addCriterion("user_img =", value, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgNotEqualTo(String value) {
            addCriterion("user_img <>", value, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgGreaterThan(String value) {
            addCriterion("user_img >", value, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgGreaterThanOrEqualTo(String value) {
            addCriterion("user_img >=", value, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgLessThan(String value) {
            addCriterion("user_img <", value, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgLessThanOrEqualTo(String value) {
            addCriterion("user_img <=", value, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgLike(String value) {
            addCriterion("user_img like", value, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgNotLike(String value) {
            addCriterion("user_img not like", value, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgIn(List<String> values) {
            addCriterion("user_img in", values, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgNotIn(List<String> values) {
            addCriterion("user_img not in", values, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgBetween(String value1, String value2) {
            addCriterion("user_img between", value1, value2, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserImgNotBetween(String value1, String value2) {
            addCriterion("user_img not between", value1, value2, "userImg");
            return (Criteria) this;
        }

        public Criteria andUserNickIsNull() {
            addCriterion("user_nick is null");
            return (Criteria) this;
        }

        public Criteria andUserNickIsNotNull() {
            addCriterion("user_nick is not null");
            return (Criteria) this;
        }

        public Criteria andUserNickEqualTo(String value) {
            addCriterion("user_nick =", value, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickNotEqualTo(String value) {
            addCriterion("user_nick <>", value, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickGreaterThan(String value) {
            addCriterion("user_nick >", value, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickGreaterThanOrEqualTo(String value) {
            addCriterion("user_nick >=", value, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickLessThan(String value) {
            addCriterion("user_nick <", value, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickLessThanOrEqualTo(String value) {
            addCriterion("user_nick <=", value, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickLike(String value) {
            addCriterion("user_nick like", value, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickNotLike(String value) {
            addCriterion("user_nick not like", value, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickIn(List<String> values) {
            addCriterion("user_nick in", values, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickNotIn(List<String> values) {
            addCriterion("user_nick not in", values, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickBetween(String value1, String value2) {
            addCriterion("user_nick between", value1, value2, "userNick");
            return (Criteria) this;
        }

        public Criteria andUserNickNotBetween(String value1, String value2) {
            addCriterion("user_nick not between", value1, value2, "userNick");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}