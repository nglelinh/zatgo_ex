package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GroupBookingActivityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GroupBookingActivityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGroupActivityIdIsNull() {
            addCriterion("group_activity_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdIsNotNull() {
            addCriterion("group_activity_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdEqualTo(String value) {
            addCriterion("group_activity_id =", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdNotEqualTo(String value) {
            addCriterion("group_activity_id <>", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdGreaterThan(String value) {
            addCriterion("group_activity_id >", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdGreaterThanOrEqualTo(String value) {
            addCriterion("group_activity_id >=", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdLessThan(String value) {
            addCriterion("group_activity_id <", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdLessThanOrEqualTo(String value) {
            addCriterion("group_activity_id <=", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdLike(String value) {
            addCriterion("group_activity_id like", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdNotLike(String value) {
            addCriterion("group_activity_id not like", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdIn(List<String> values) {
            addCriterion("group_activity_id in", values, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdNotIn(List<String> values) {
            addCriterion("group_activity_id not in", values, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdBetween(String value1, String value2) {
            addCriterion("group_activity_id between", value1, value2, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdNotBetween(String value1, String value2) {
            addCriterion("group_activity_id not between", value1, value2, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(String value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(String value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(String value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(String value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(String value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLike(String value) {
            addCriterion("product_id like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotLike(String value) {
            addCriterion("product_id not like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<String> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<String> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(String value1, String value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(String value1, String value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNull() {
            addCriterion("product_name is null");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNotNull() {
            addCriterion("product_name is not null");
            return (Criteria) this;
        }

        public Criteria andProductNameEqualTo(String value) {
            addCriterion("product_name =", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotEqualTo(String value) {
            addCriterion("product_name <>", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThan(String value) {
            addCriterion("product_name >", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThanOrEqualTo(String value) {
            addCriterion("product_name >=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThan(String value) {
            addCriterion("product_name <", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThanOrEqualTo(String value) {
            addCriterion("product_name <=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLike(String value) {
            addCriterion("product_name like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotLike(String value) {
            addCriterion("product_name not like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameIn(List<String> values) {
            addCriterion("product_name in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotIn(List<String> values) {
            addCriterion("product_name not in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameBetween(String value1, String value2) {
            addCriterion("product_name between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotBetween(String value1, String value2) {
            addCriterion("product_name not between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andActivityPriceIsNull() {
            addCriterion("activity_price is null");
            return (Criteria) this;
        }

        public Criteria andActivityPriceIsNotNull() {
            addCriterion("activity_price is not null");
            return (Criteria) this;
        }

        public Criteria andActivityPriceEqualTo(BigDecimal value) {
            addCriterion("activity_price =", value, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceNotEqualTo(BigDecimal value) {
            addCriterion("activity_price <>", value, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceGreaterThan(BigDecimal value) {
            addCriterion("activity_price >", value, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_price >=", value, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceLessThan(BigDecimal value) {
            addCriterion("activity_price <", value, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_price <=", value, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceIn(List<BigDecimal> values) {
            addCriterion("activity_price in", values, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceNotIn(List<BigDecimal> values) {
            addCriterion("activity_price not in", values, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_price between", value1, value2, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andActivityPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_price not between", value1, value2, "activityPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIsNull() {
            addCriterion("original_price is null");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIsNotNull() {
            addCriterion("original_price is not null");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceEqualTo(BigDecimal value) {
            addCriterion("original_price =", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotEqualTo(BigDecimal value) {
            addCriterion("original_price <>", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceGreaterThan(BigDecimal value) {
            addCriterion("original_price >", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("original_price >=", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceLessThan(BigDecimal value) {
            addCriterion("original_price <", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("original_price <=", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIn(List<BigDecimal> values) {
            addCriterion("original_price in", values, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotIn(List<BigDecimal> values) {
            addCriterion("original_price not in", values, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("original_price between", value1, value2, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("original_price not between", value1, value2, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIsNull() {
            addCriterion("promotion_price is null");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIsNotNull() {
            addCriterion("promotion_price is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceEqualTo(BigDecimal value) {
            addCriterion("promotion_price =", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotEqualTo(BigDecimal value) {
            addCriterion("promotion_price <>", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceGreaterThan(BigDecimal value) {
            addCriterion("promotion_price >", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_price >=", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceLessThan(BigDecimal value) {
            addCriterion("promotion_price <", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_price <=", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIn(List<BigDecimal> values) {
            addCriterion("promotion_price in", values, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotIn(List<BigDecimal> values) {
            addCriterion("promotion_price not in", values, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_price between", value1, value2, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_price not between", value1, value2, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlIsNull() {
            addCriterion("product_img_url is null");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlIsNotNull() {
            addCriterion("product_img_url is not null");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlEqualTo(String value) {
            addCriterion("product_img_url =", value, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlNotEqualTo(String value) {
            addCriterion("product_img_url <>", value, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlGreaterThan(String value) {
            addCriterion("product_img_url >", value, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("product_img_url >=", value, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlLessThan(String value) {
            addCriterion("product_img_url <", value, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlLessThanOrEqualTo(String value) {
            addCriterion("product_img_url <=", value, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlLike(String value) {
            addCriterion("product_img_url like", value, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlNotLike(String value) {
            addCriterion("product_img_url not like", value, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlIn(List<String> values) {
            addCriterion("product_img_url in", values, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlNotIn(List<String> values) {
            addCriterion("product_img_url not in", values, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlBetween(String value1, String value2) {
            addCriterion("product_img_url between", value1, value2, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgUrlNotBetween(String value1, String value2) {
            addCriterion("product_img_url not between", value1, value2, "productImgUrl");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIsNull() {
            addCriterion("activity_start_date is null");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIsNotNull() {
            addCriterion("activity_start_date is not null");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateEqualTo(Date value) {
            addCriterion("activity_start_date =", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotEqualTo(Date value) {
            addCriterion("activity_start_date <>", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateGreaterThan(Date value) {
            addCriterion("activity_start_date >", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("activity_start_date >=", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateLessThan(Date value) {
            addCriterion("activity_start_date <", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateLessThanOrEqualTo(Date value) {
            addCriterion("activity_start_date <=", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIn(List<Date> values) {
            addCriterion("activity_start_date in", values, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotIn(List<Date> values) {
            addCriterion("activity_start_date not in", values, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateBetween(Date value1, Date value2) {
            addCriterion("activity_start_date between", value1, value2, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotBetween(Date value1, Date value2) {
            addCriterion("activity_start_date not between", value1, value2, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIsNull() {
            addCriterion("activity_end_date is null");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIsNotNull() {
            addCriterion("activity_end_date is not null");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateEqualTo(Date value) {
            addCriterion("activity_end_date =", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotEqualTo(Date value) {
            addCriterion("activity_end_date <>", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateGreaterThan(Date value) {
            addCriterion("activity_end_date >", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("activity_end_date >=", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateLessThan(Date value) {
            addCriterion("activity_end_date <", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateLessThanOrEqualTo(Date value) {
            addCriterion("activity_end_date <=", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIn(List<Date> values) {
            addCriterion("activity_end_date in", values, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotIn(List<Date> values) {
            addCriterion("activity_end_date not in", values, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateBetween(Date value1, Date value2) {
            addCriterion("activity_end_date between", value1, value2, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotBetween(Date value1, Date value2) {
            addCriterion("activity_end_date not between", value1, value2, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourIsNull() {
            addCriterion("build_group_limit_hour is null");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourIsNotNull() {
            addCriterion("build_group_limit_hour is not null");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourEqualTo(Long value) {
            addCriterion("build_group_limit_hour =", value, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourNotEqualTo(Long value) {
            addCriterion("build_group_limit_hour <>", value, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourGreaterThan(Long value) {
            addCriterion("build_group_limit_hour >", value, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourGreaterThanOrEqualTo(Long value) {
            addCriterion("build_group_limit_hour >=", value, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourLessThan(Long value) {
            addCriterion("build_group_limit_hour <", value, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourLessThanOrEqualTo(Long value) {
            addCriterion("build_group_limit_hour <=", value, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourIn(List<Long> values) {
            addCriterion("build_group_limit_hour in", values, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourNotIn(List<Long> values) {
            addCriterion("build_group_limit_hour not in", values, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourBetween(Long value1, Long value2) {
            addCriterion("build_group_limit_hour between", value1, value2, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupLimitHourNotBetween(Long value1, Long value2) {
            addCriterion("build_group_limit_hour not between", value1, value2, "buildGroupLimitHour");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumIsNull() {
            addCriterion("build_group_people_num is null");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumIsNotNull() {
            addCriterion("build_group_people_num is not null");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumEqualTo(Long value) {
            addCriterion("build_group_people_num =", value, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumNotEqualTo(Long value) {
            addCriterion("build_group_people_num <>", value, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumGreaterThan(Long value) {
            addCriterion("build_group_people_num >", value, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumGreaterThanOrEqualTo(Long value) {
            addCriterion("build_group_people_num >=", value, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumLessThan(Long value) {
            addCriterion("build_group_people_num <", value, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumLessThanOrEqualTo(Long value) {
            addCriterion("build_group_people_num <=", value, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumIn(List<Long> values) {
            addCriterion("build_group_people_num in", values, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumNotIn(List<Long> values) {
            addCriterion("build_group_people_num not in", values, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumBetween(Long value1, Long value2) {
            addCriterion("build_group_people_num between", value1, value2, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBuildGroupPeopleNumNotBetween(Long value1, Long value2) {
            addCriterion("build_group_people_num not between", value1, value2, "buildGroupPeopleNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumIsNull() {
            addCriterion("per_limit_booking_num is null");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumIsNotNull() {
            addCriterion("per_limit_booking_num is not null");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumEqualTo(Long value) {
            addCriterion("per_limit_booking_num =", value, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumNotEqualTo(Long value) {
            addCriterion("per_limit_booking_num <>", value, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumGreaterThan(Long value) {
            addCriterion("per_limit_booking_num >", value, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumGreaterThanOrEqualTo(Long value) {
            addCriterion("per_limit_booking_num >=", value, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumLessThan(Long value) {
            addCriterion("per_limit_booking_num <", value, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumLessThanOrEqualTo(Long value) {
            addCriterion("per_limit_booking_num <=", value, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumIn(List<Long> values) {
            addCriterion("per_limit_booking_num in", values, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumNotIn(List<Long> values) {
            addCriterion("per_limit_booking_num not in", values, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumBetween(Long value1, Long value2) {
            addCriterion("per_limit_booking_num between", value1, value2, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andPerLimitBookingNumNotBetween(Long value1, Long value2) {
            addCriterion("per_limit_booking_num not between", value1, value2, "perLimitBookingNum");
            return (Criteria) this;
        }

        public Criteria andActivityStatusIsNull() {
            addCriterion("activity_status is null");
            return (Criteria) this;
        }

        public Criteria andActivityStatusIsNotNull() {
            addCriterion("activity_status is not null");
            return (Criteria) this;
        }

        public Criteria andActivityStatusEqualTo(Byte value) {
            addCriterion("activity_status =", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusNotEqualTo(Byte value) {
            addCriterion("activity_status <>", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusGreaterThan(Byte value) {
            addCriterion("activity_status >", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("activity_status >=", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusLessThan(Byte value) {
            addCriterion("activity_status <", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusLessThanOrEqualTo(Byte value) {
            addCriterion("activity_status <=", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusIn(List<Byte> values) {
            addCriterion("activity_status in", values, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusNotIn(List<Byte> values) {
            addCriterion("activity_status not in", values, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusBetween(Byte value1, Byte value2) {
            addCriterion("activity_status between", value1, value2, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("activity_status not between", value1, value2, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNull() {
            addCriterion("operator_name is null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNotNull() {
            addCriterion("operator_name is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameEqualTo(String value) {
            addCriterion("operator_name =", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotEqualTo(String value) {
            addCriterion("operator_name <>", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThan(String value) {
            addCriterion("operator_name >", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThanOrEqualTo(String value) {
            addCriterion("operator_name >=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThan(String value) {
            addCriterion("operator_name <", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThanOrEqualTo(String value) {
            addCriterion("operator_name <=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLike(String value) {
            addCriterion("operator_name like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotLike(String value) {
            addCriterion("operator_name not like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIn(List<String> values) {
            addCriterion("operator_name in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotIn(List<String> values) {
            addCriterion("operator_name not in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameBetween(String value1, String value2) {
            addCriterion("operator_name between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotBetween(String value1, String value2) {
            addCriterion("operator_name not between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperateDateIsNull() {
            addCriterion("operate_date is null");
            return (Criteria) this;
        }

        public Criteria andOperateDateIsNotNull() {
            addCriterion("operate_date is not null");
            return (Criteria) this;
        }

        public Criteria andOperateDateEqualTo(Date value) {
            addCriterion("operate_date =", value, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateNotEqualTo(Date value) {
            addCriterion("operate_date <>", value, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateGreaterThan(Date value) {
            addCriterion("operate_date >", value, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("operate_date >=", value, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateLessThan(Date value) {
            addCriterion("operate_date <", value, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateLessThanOrEqualTo(Date value) {
            addCriterion("operate_date <=", value, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateIn(List<Date> values) {
            addCriterion("operate_date in", values, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateNotIn(List<Date> values) {
            addCriterion("operate_date not in", values, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateBetween(Date value1, Date value2) {
            addCriterion("operate_date between", value1, value2, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperateDateNotBetween(Date value1, Date value2) {
            addCriterion("operate_date not between", value1, value2, "operateDate");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNull() {
            addCriterion("operator_id is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNotNull() {
            addCriterion("operator_id is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdEqualTo(String value) {
            addCriterion("operator_id =", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotEqualTo(String value) {
            addCriterion("operator_id <>", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThan(String value) {
            addCriterion("operator_id >", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("operator_id >=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThan(String value) {
            addCriterion("operator_id <", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThanOrEqualTo(String value) {
            addCriterion("operator_id <=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIn(List<String> values) {
            addCriterion("operator_id in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotIn(List<String> values) {
            addCriterion("operator_id not in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdBetween(String value1, String value2) {
            addCriterion("operator_id between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotBetween(String value1, String value2) {
            addCriterion("operator_id not between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitIsNull() {
            addCriterion("is_out_of_limit is null");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitIsNotNull() {
            addCriterion("is_out_of_limit is not null");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitEqualTo(Boolean value) {
            addCriterion("is_out_of_limit =", value, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitNotEqualTo(Boolean value) {
            addCriterion("is_out_of_limit <>", value, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitGreaterThan(Boolean value) {
            addCriterion("is_out_of_limit >", value, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_out_of_limit >=", value, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitLessThan(Boolean value) {
            addCriterion("is_out_of_limit <", value, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitLessThanOrEqualTo(Boolean value) {
            addCriterion("is_out_of_limit <=", value, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitIn(List<Boolean> values) {
            addCriterion("is_out_of_limit in", values, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitNotIn(List<Boolean> values) {
            addCriterion("is_out_of_limit not in", values, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitBetween(Boolean value1, Boolean value2) {
            addCriterion("is_out_of_limit between", value1, value2, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andIsOutOfLimitNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_out_of_limit not between", value1, value2, "isOutOfLimit");
            return (Criteria) this;
        }

        public Criteria andShareTitleIsNull() {
            addCriterion("share_title is null");
            return (Criteria) this;
        }

        public Criteria andShareTitleIsNotNull() {
            addCriterion("share_title is not null");
            return (Criteria) this;
        }

        public Criteria andShareTitleEqualTo(String value) {
            addCriterion("share_title =", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotEqualTo(String value) {
            addCriterion("share_title <>", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleGreaterThan(String value) {
            addCriterion("share_title >", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleGreaterThanOrEqualTo(String value) {
            addCriterion("share_title >=", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLessThan(String value) {
            addCriterion("share_title <", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLessThanOrEqualTo(String value) {
            addCriterion("share_title <=", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLike(String value) {
            addCriterion("share_title like", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotLike(String value) {
            addCriterion("share_title not like", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleIn(List<String> values) {
            addCriterion("share_title in", values, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotIn(List<String> values) {
            addCriterion("share_title not in", values, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleBetween(String value1, String value2) {
            addCriterion("share_title between", value1, value2, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotBetween(String value1, String value2) {
            addCriterion("share_title not between", value1, value2, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareDescIsNull() {
            addCriterion("share_desc is null");
            return (Criteria) this;
        }

        public Criteria andShareDescIsNotNull() {
            addCriterion("share_desc is not null");
            return (Criteria) this;
        }

        public Criteria andShareDescEqualTo(String value) {
            addCriterion("share_desc =", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescNotEqualTo(String value) {
            addCriterion("share_desc <>", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescGreaterThan(String value) {
            addCriterion("share_desc >", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescGreaterThanOrEqualTo(String value) {
            addCriterion("share_desc >=", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescLessThan(String value) {
            addCriterion("share_desc <", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescLessThanOrEqualTo(String value) {
            addCriterion("share_desc <=", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescLike(String value) {
            addCriterion("share_desc like", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescNotLike(String value) {
            addCriterion("share_desc not like", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescIn(List<String> values) {
            addCriterion("share_desc in", values, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescNotIn(List<String> values) {
            addCriterion("share_desc not in", values, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescBetween(String value1, String value2) {
            addCriterion("share_desc between", value1, value2, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescNotBetween(String value1, String value2) {
            addCriterion("share_desc not between", value1, value2, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlIsNull() {
            addCriterion("share_img_url is null");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlIsNotNull() {
            addCriterion("share_img_url is not null");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlEqualTo(String value) {
            addCriterion("share_img_url =", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlNotEqualTo(String value) {
            addCriterion("share_img_url <>", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlGreaterThan(String value) {
            addCriterion("share_img_url >", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("share_img_url >=", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlLessThan(String value) {
            addCriterion("share_img_url <", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlLessThanOrEqualTo(String value) {
            addCriterion("share_img_url <=", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlLike(String value) {
            addCriterion("share_img_url like", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlNotLike(String value) {
            addCriterion("share_img_url not like", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlIn(List<String> values) {
            addCriterion("share_img_url in", values, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlNotIn(List<String> values) {
            addCriterion("share_img_url not in", values, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlBetween(String value1, String value2) {
            addCriterion("share_img_url between", value1, value2, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlNotBetween(String value1, String value2) {
            addCriterion("share_img_url not between", value1, value2, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareTypeIsNull() {
            addCriterion("share_type is null");
            return (Criteria) this;
        }

        public Criteria andShareTypeIsNotNull() {
            addCriterion("share_type is not null");
            return (Criteria) this;
        }

        public Criteria andShareTypeEqualTo(String value) {
            addCriterion("share_type =", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotEqualTo(String value) {
            addCriterion("share_type <>", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeGreaterThan(String value) {
            addCriterion("share_type >", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeGreaterThanOrEqualTo(String value) {
            addCriterion("share_type >=", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLessThan(String value) {
            addCriterion("share_type <", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLessThanOrEqualTo(String value) {
            addCriterion("share_type <=", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLike(String value) {
            addCriterion("share_type like", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotLike(String value) {
            addCriterion("share_type not like", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeIn(List<String> values) {
            addCriterion("share_type in", values, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotIn(List<String> values) {
            addCriterion("share_type not in", values, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeBetween(String value1, String value2) {
            addCriterion("share_type between", value1, value2, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotBetween(String value1, String value2) {
            addCriterion("share_type not between", value1, value2, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareLinkIsNull() {
            addCriterion("share_link is null");
            return (Criteria) this;
        }

        public Criteria andShareLinkIsNotNull() {
            addCriterion("share_link is not null");
            return (Criteria) this;
        }

        public Criteria andShareLinkEqualTo(String value) {
            addCriterion("share_link =", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkNotEqualTo(String value) {
            addCriterion("share_link <>", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkGreaterThan(String value) {
            addCriterion("share_link >", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkGreaterThanOrEqualTo(String value) {
            addCriterion("share_link >=", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkLessThan(String value) {
            addCriterion("share_link <", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkLessThanOrEqualTo(String value) {
            addCriterion("share_link <=", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkLike(String value) {
            addCriterion("share_link like", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkNotLike(String value) {
            addCriterion("share_link not like", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkIn(List<String> values) {
            addCriterion("share_link in", values, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkNotIn(List<String> values) {
            addCriterion("share_link not in", values, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkBetween(String value1, String value2) {
            addCriterion("share_link between", value1, value2, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkNotBetween(String value1, String value2) {
            addCriterion("share_link not between", value1, value2, "shareLink");
            return (Criteria) this;
        }

        public Criteria andActivityImgIsNull() {
            addCriterion("activity_img is null");
            return (Criteria) this;
        }

        public Criteria andActivityImgIsNotNull() {
            addCriterion("activity_img is not null");
            return (Criteria) this;
        }

        public Criteria andActivityImgEqualTo(String value) {
            addCriterion("activity_img =", value, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgNotEqualTo(String value) {
            addCriterion("activity_img <>", value, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgGreaterThan(String value) {
            addCriterion("activity_img >", value, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgGreaterThanOrEqualTo(String value) {
            addCriterion("activity_img >=", value, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgLessThan(String value) {
            addCriterion("activity_img <", value, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgLessThanOrEqualTo(String value) {
            addCriterion("activity_img <=", value, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgLike(String value) {
            addCriterion("activity_img like", value, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgNotLike(String value) {
            addCriterion("activity_img not like", value, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgIn(List<String> values) {
            addCriterion("activity_img in", values, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgNotIn(List<String> values) {
            addCriterion("activity_img not in", values, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgBetween(String value1, String value2) {
            addCriterion("activity_img between", value1, value2, "activityImg");
            return (Criteria) this;
        }

        public Criteria andActivityImgNotBetween(String value1, String value2) {
            addCriterion("activity_img not between", value1, value2, "activityImg");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNull() {
            addCriterion("pay_time is null");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNotNull() {
            addCriterion("pay_time is not null");
            return (Criteria) this;
        }

        public Criteria andPayTimeEqualTo(Long value) {
            addCriterion("pay_time =", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotEqualTo(Long value) {
            addCriterion("pay_time <>", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThan(Long value) {
            addCriterion("pay_time >", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("pay_time >=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThan(Long value) {
            addCriterion("pay_time <", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThanOrEqualTo(Long value) {
            addCriterion("pay_time <=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIn(List<Long> values) {
            addCriterion("pay_time in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotIn(List<Long> values) {
            addCriterion("pay_time not in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeBetween(Long value1, Long value2) {
            addCriterion("pay_time between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotBetween(Long value1, Long value2) {
            addCriterion("pay_time not between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIsNull() {
            addCriterion("price_ids is null");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIsNotNull() {
            addCriterion("price_ids is not null");
            return (Criteria) this;
        }

        public Criteria andPriceIdsEqualTo(String value) {
            addCriterion("price_ids =", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotEqualTo(String value) {
            addCriterion("price_ids <>", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsGreaterThan(String value) {
            addCriterion("price_ids >", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsGreaterThanOrEqualTo(String value) {
            addCriterion("price_ids >=", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLessThan(String value) {
            addCriterion("price_ids <", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLessThanOrEqualTo(String value) {
            addCriterion("price_ids <=", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLike(String value) {
            addCriterion("price_ids like", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotLike(String value) {
            addCriterion("price_ids not like", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIn(List<String> values) {
            addCriterion("price_ids in", values, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotIn(List<String> values) {
            addCriterion("price_ids not in", values, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsBetween(String value1, String value2) {
            addCriterion("price_ids between", value1, value2, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotBetween(String value1, String value2) {
            addCriterion("price_ids not between", value1, value2, "priceIds");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}