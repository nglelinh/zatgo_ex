package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ActivityProduct {
    private String productId;

    private String cloudUserId;

    private String cloudUserProductId;

    private String productName;

    private String productImgAbbrUrl;

    private String productIntro;

    private String addOrderUrl;

    private BigDecimal productMoney;

    private Date createDate;

    private Date updateDate;

    private BigDecimal promotionPrice;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public String getProductImgAbbrUrl() {
        return productImgAbbrUrl;
    }

    public void setProductImgAbbrUrl(String productImgAbbrUrl) {
        this.productImgAbbrUrl = productImgAbbrUrl == null ? null : productImgAbbrUrl.trim();
    }

    public String getProductIntro() {
        return productIntro;
    }

    public void setProductIntro(String productIntro) {
        this.productIntro = productIntro == null ? null : productIntro.trim();
    }

    public String getAddOrderUrl() {
        return addOrderUrl;
    }

    public void setAddOrderUrl(String addOrderUrl) {
        this.addOrderUrl = addOrderUrl == null ? null : addOrderUrl.trim();
    }

    public BigDecimal getProductMoney() {
        return productMoney;
    }

    public void setProductMoney(BigDecimal productMoney) {
        this.productMoney = productMoney;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCloudUserProductId() {
        return cloudUserProductId;
    }

    public void setCloudUserProductId(String cloudUserProductId) {
        this.cloudUserProductId = cloudUserProductId;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }
}