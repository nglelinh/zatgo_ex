package com.zatgo.zup.activity.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GroupUserOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GroupUserOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGroupUserOrderIdIsNull() {
            addCriterion("group_user_order_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdIsNotNull() {
            addCriterion("group_user_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdEqualTo(String value) {
            addCriterion("group_user_order_id =", value, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdNotEqualTo(String value) {
            addCriterion("group_user_order_id <>", value, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdGreaterThan(String value) {
            addCriterion("group_user_order_id >", value, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("group_user_order_id >=", value, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdLessThan(String value) {
            addCriterion("group_user_order_id <", value, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdLessThanOrEqualTo(String value) {
            addCriterion("group_user_order_id <=", value, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdLike(String value) {
            addCriterion("group_user_order_id like", value, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdNotLike(String value) {
            addCriterion("group_user_order_id not like", value, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdIn(List<String> values) {
            addCriterion("group_user_order_id in", values, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdNotIn(List<String> values) {
            addCriterion("group_user_order_id not in", values, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdBetween(String value1, String value2) {
            addCriterion("group_user_order_id between", value1, value2, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupUserOrderIdNotBetween(String value1, String value2) {
            addCriterion("group_user_order_id not between", value1, value2, "groupUserOrderId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdIsNull() {
            addCriterion("group_order_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdIsNotNull() {
            addCriterion("group_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdEqualTo(String value) {
            addCriterion("group_order_id =", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdNotEqualTo(String value) {
            addCriterion("group_order_id <>", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdGreaterThan(String value) {
            addCriterion("group_order_id >", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("group_order_id >=", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdLessThan(String value) {
            addCriterion("group_order_id <", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdLessThanOrEqualTo(String value) {
            addCriterion("group_order_id <=", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdLike(String value) {
            addCriterion("group_order_id like", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdNotLike(String value) {
            addCriterion("group_order_id not like", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdIn(List<String> values) {
            addCriterion("group_order_id in", values, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdNotIn(List<String> values) {
            addCriterion("group_order_id not in", values, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdBetween(String value1, String value2) {
            addCriterion("group_order_id between", value1, value2, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdNotBetween(String value1, String value2) {
            addCriterion("group_order_id not between", value1, value2, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateIsNull() {
            addCriterion("create_order_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateIsNotNull() {
            addCriterion("create_order_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateEqualTo(Date value) {
            addCriterion("create_order_date =", value, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateNotEqualTo(Date value) {
            addCriterion("create_order_date <>", value, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateGreaterThan(Date value) {
            addCriterion("create_order_date >", value, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_order_date >=", value, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateLessThan(Date value) {
            addCriterion("create_order_date <", value, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateLessThanOrEqualTo(Date value) {
            addCriterion("create_order_date <=", value, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateIn(List<Date> values) {
            addCriterion("create_order_date in", values, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateNotIn(List<Date> values) {
            addCriterion("create_order_date not in", values, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateBetween(Date value1, Date value2) {
            addCriterion("create_order_date between", value1, value2, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andCreateOrderDateNotBetween(Date value1, Date value2) {
            addCriterion("create_order_date not between", value1, value2, "createOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateIsNull() {
            addCriterion("pay_order_date is null");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateIsNotNull() {
            addCriterion("pay_order_date is not null");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateEqualTo(Date value) {
            addCriterion("pay_order_date =", value, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateNotEqualTo(Date value) {
            addCriterion("pay_order_date <>", value, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateGreaterThan(Date value) {
            addCriterion("pay_order_date >", value, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateGreaterThanOrEqualTo(Date value) {
            addCriterion("pay_order_date >=", value, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateLessThan(Date value) {
            addCriterion("pay_order_date <", value, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateLessThanOrEqualTo(Date value) {
            addCriterion("pay_order_date <=", value, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateIn(List<Date> values) {
            addCriterion("pay_order_date in", values, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateNotIn(List<Date> values) {
            addCriterion("pay_order_date not in", values, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateBetween(Date value1, Date value2) {
            addCriterion("pay_order_date between", value1, value2, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andPayOrderDateNotBetween(Date value1, Date value2) {
            addCriterion("pay_order_date not between", value1, value2, "payOrderDate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineIsNull() {
            addCriterion("pay_deadline is null");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineIsNotNull() {
            addCriterion("pay_deadline is not null");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineEqualTo(Date value) {
            addCriterion("pay_deadline =", value, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineNotEqualTo(Date value) {
            addCriterion("pay_deadline <>", value, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineGreaterThan(Date value) {
            addCriterion("pay_deadline >", value, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineGreaterThanOrEqualTo(Date value) {
            addCriterion("pay_deadline >=", value, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineLessThan(Date value) {
            addCriterion("pay_deadline <", value, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineLessThanOrEqualTo(Date value) {
            addCriterion("pay_deadline <=", value, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineIn(List<Date> values) {
            addCriterion("pay_deadline in", values, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineNotIn(List<Date> values) {
            addCriterion("pay_deadline not in", values, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineBetween(Date value1, Date value2) {
            addCriterion("pay_deadline between", value1, value2, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andPayDeadlineNotBetween(Date value1, Date value2) {
            addCriterion("pay_deadline not between", value1, value2, "payDeadline");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNull() {
            addCriterion("nickname is null");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNotNull() {
            addCriterion("nickname is not null");
            return (Criteria) this;
        }

        public Criteria andNicknameEqualTo(String value) {
            addCriterion("nickname =", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotEqualTo(String value) {
            addCriterion("nickname <>", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThan(String value) {
            addCriterion("nickname >", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThanOrEqualTo(String value) {
            addCriterion("nickname >=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThan(String value) {
            addCriterion("nickname <", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThanOrEqualTo(String value) {
            addCriterion("nickname <=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLike(String value) {
            addCriterion("nickname like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotLike(String value) {
            addCriterion("nickname not like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameIn(List<String> values) {
            addCriterion("nickname in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotIn(List<String> values) {
            addCriterion("nickname not in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameBetween(String value1, String value2) {
            addCriterion("nickname between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotBetween(String value1, String value2) {
            addCriterion("nickname not between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andIconIsNull() {
            addCriterion("icon is null");
            return (Criteria) this;
        }

        public Criteria andIconIsNotNull() {
            addCriterion("icon is not null");
            return (Criteria) this;
        }

        public Criteria andIconEqualTo(String value) {
            addCriterion("icon =", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotEqualTo(String value) {
            addCriterion("icon <>", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconGreaterThan(String value) {
            addCriterion("icon >", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconGreaterThanOrEqualTo(String value) {
            addCriterion("icon >=", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLessThan(String value) {
            addCriterion("icon <", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLessThanOrEqualTo(String value) {
            addCriterion("icon <=", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLike(String value) {
            addCriterion("icon like", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotLike(String value) {
            addCriterion("icon not like", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconIn(List<String> values) {
            addCriterion("icon in", values, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotIn(List<String> values) {
            addCriterion("icon not in", values, "icon");
            return (Criteria) this;
        }

        public Criteria andIconBetween(String value1, String value2) {
            addCriterion("icon between", value1, value2, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotBetween(String value1, String value2) {
            addCriterion("icon not between", value1, value2, "icon");
            return (Criteria) this;
        }

        public Criteria andExtInfoIsNull() {
            addCriterion("ext_info is null");
            return (Criteria) this;
        }

        public Criteria andExtInfoIsNotNull() {
            addCriterion("ext_info is not null");
            return (Criteria) this;
        }

        public Criteria andExtInfoEqualTo(String value) {
            addCriterion("ext_info =", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoNotEqualTo(String value) {
            addCriterion("ext_info <>", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoGreaterThan(String value) {
            addCriterion("ext_info >", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoGreaterThanOrEqualTo(String value) {
            addCriterion("ext_info >=", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoLessThan(String value) {
            addCriterion("ext_info <", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoLessThanOrEqualTo(String value) {
            addCriterion("ext_info <=", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoLike(String value) {
            addCriterion("ext_info like", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoNotLike(String value) {
            addCriterion("ext_info not like", value, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoIn(List<String> values) {
            addCriterion("ext_info in", values, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoNotIn(List<String> values) {
            addCriterion("ext_info not in", values, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoBetween(String value1, String value2) {
            addCriterion("ext_info between", value1, value2, "extInfo");
            return (Criteria) this;
        }

        public Criteria andExtInfoNotBetween(String value1, String value2) {
            addCriterion("ext_info not between", value1, value2, "extInfo");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}