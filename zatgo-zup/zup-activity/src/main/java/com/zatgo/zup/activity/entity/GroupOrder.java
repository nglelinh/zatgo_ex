package com.zatgo.zup.activity.entity;

import java.util.Date;

public class GroupOrder {
    private String groupOrderId;

    private String groupActivityId;

    private Byte groupOrderStatus;

    private Date groupStartDate;

    private Date groupEndDate;

    private Boolean isValid;

    private String activityUrl;

    private Boolean isPrivate;

    private Date updateDate;

    public String getGroupOrderId() {
        return groupOrderId;
    }

    public void setGroupOrderId(String groupOrderId) {
        this.groupOrderId = groupOrderId == null ? null : groupOrderId.trim();
    }

    public String getGroupActivityId() {
        return groupActivityId;
    }

    public void setGroupActivityId(String groupActivityId) {
        this.groupActivityId = groupActivityId == null ? null : groupActivityId.trim();
    }

    public Byte getGroupOrderStatus() {
        return groupOrderStatus;
    }

    public void setGroupOrderStatus(Byte groupOrderStatus) {
        this.groupOrderStatus = groupOrderStatus;
    }

    public Date getGroupStartDate() {
        return groupStartDate;
    }

    public void setGroupStartDate(Date groupStartDate) {
        this.groupStartDate = groupStartDate;
    }

    public Date getGroupEndDate() {
        return groupEndDate;
    }

    public void setGroupEndDate(Date groupEndDate) {
        this.groupEndDate = groupEndDate;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public String getActivityUrl() {
        return activityUrl;
    }

    public void setActivityUrl(String activityUrl) {
        this.activityUrl = activityUrl;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}