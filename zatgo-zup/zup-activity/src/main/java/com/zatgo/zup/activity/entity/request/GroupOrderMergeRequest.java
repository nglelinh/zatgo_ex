package com.zatgo.zup.activity.entity.request;

import java.util.List;

/**
 * Created by 46041 on 2019/6/27.
 */
public class GroupOrderMergeRequest {

    private String groupSourceLeftId;

    private String groupSOurceRightId;

    private List<String> targetList;

    public String getGroupSourceLeftId() {
        return groupSourceLeftId;
    }

    public void setGroupSourceLeftId(String groupSourceLeftId) {
        this.groupSourceLeftId = groupSourceLeftId;
    }

    public String getGroupSOurceRightId() {
        return groupSOurceRightId;
    }

    public void setGroupSOurceRightId(String groupSOurceRightId) {
        this.groupSOurceRightId = groupSOurceRightId;
    }

    public List<String> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<String> targetList) {
        this.targetList = targetList;
    }
}
