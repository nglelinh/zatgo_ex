package com.zatgo.zup.activity.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.activity.entity.ActivityBargainJoin;
import com.zatgo.zup.activity.entity.response.ActivityBargainInfoResponse;
import com.zatgo.zup.activity.model.JoinActivityBargainParams;
import com.zatgo.zup.common.model.HelpInfoRespone;
import com.zatgo.zup.common.model.NewActivityListRespones;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/3/11.
 */
public interface UserService {

    ActivityBargainInfoResponse getActivityBargainJoin(ThirdLoginInfo authorization, String joinId);

    BigDecimal bargain(ThirdLoginInfo authorization, String activityBargainJoinId);

    ActivityBargainJoin joinActivityBargain(JoinActivityBargainParams params, ThirdLoginInfo userInfo);

    ActivityBargainInfoResponse activityInfo(ThirdLoginInfo authorization, String activityId);

    PageInfo<NewActivityListRespones> activityList(ThirdLoginInfo userInfo, Integer pageNo, Integer pageSize);

    HelpInfoRespone helpInfo(String bargainActivityJoinId, ThirdLoginInfo userInfo);

    PageInfo<ActivityBargain> bargainList(ThirdLoginInfo userInfo, Integer pageNo, Integer pageSize);
}
