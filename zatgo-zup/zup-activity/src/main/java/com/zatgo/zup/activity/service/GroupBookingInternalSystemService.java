package com.zatgo.zup.activity.service;

import com.zatgo.zup.activity.entity.GroupBookingActivity;
import com.zatgo.zup.activity.entity.GroupOrder;
import com.zatgo.zup.activity.entity.request.CreateGroupBookingRequest;
import com.zatgo.zup.activity.entity.request.JoinGroupBookingRequest;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/3/15.
 */
public interface GroupBookingInternalSystemService {



    GroupOrder createGroupBooking(CreateGroupBookingRequest request, String url, ThirdLoginInfo userInfo);

    void cancelOrder(String groupUserOrderId);

    void joinGroupBooking(JoinGroupBookingRequest request, ThirdLoginInfo userInfo);

    void getOrderInfo(String groupOrderId);

    BigDecimal getActivityOrderPrice(String activityId, String groupOrderId, String userId);

    String saveOrder(String groupOrderId, String userId, String orderId, String type, String activityId);

    void saveOrderRecord(String activityId, String userId, String orderId, Boolean isSuccess);

    void updateRefundStatus(String activityId);

    void updateGroupStatus(String activityId, Long orderId);

    GroupBookingActivity selectById(String activityId);
}
