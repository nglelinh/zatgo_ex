package com.zatgo.zup.activity.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by 46041 on 2019/6/19.
 */

@FeignClient("zup-merchant")
public interface UserRemoteService {

    @RequestMapping(value = "/user/byid/{userId}",method = RequestMethod.GET)
    ResponseData<UserData> getUserById(@PathVariable("userId") String userId);
}

