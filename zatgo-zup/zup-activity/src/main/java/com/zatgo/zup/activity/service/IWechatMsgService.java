package com.zatgo.zup.activity.service;

import com.zatgo.zup.activity.entity.wx.MsgToken;
import com.zatgo.zup.activity.entity.wx.WechatMsg;

public interface IWechatMsgService {

	MsgToken validateWxToken(MsgToken token, String cloudUserId);
	
	String wxMsg(WechatMsg msg);
	
}
