package com.zatgo.zup.activity.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ykb.mall.common.model.OmsOrder;
import com.ykb.mall.common.model.PortalOrderDetailInfo;
import com.zatgo.zup.activity.entity.*;
import com.zatgo.zup.activity.entity.request.CreateGroupBookingRequest;
import com.zatgo.zup.activity.entity.request.JoinGroupBookingRequest;
import com.zatgo.zup.activity.mapper.ActivityOrderRecordMapper;
import com.zatgo.zup.activity.mapper.GroupBookingActivityMapper;
import com.zatgo.zup.activity.mapper.GroupOrderMapper;
import com.zatgo.zup.activity.mapper.GroupUserOrderMapper;
import com.zatgo.zup.activity.remoteservice.TradeApiRemoteService;
import com.zatgo.zup.activity.service.GroupBookingInternalSystemService;
import com.zatgo.zup.common.encrypt.MD5Util;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.model.wx.ThirdWxInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by 46041 on 2019/3/15.
 */

@Service("groupBookingInternalSystemService")
public class GroupBookingInternalSystemServiceImpl implements GroupBookingInternalSystemService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private GroupUserOrderMapper groupUserOrderMapper;
    @Autowired
    private GroupOrderMapper groupOrderMapper;
    @Autowired
    private GroupBookingActivityMapper groupBookingActivityMapper;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private ActivityOrderRecordMapper activityOrderRecordMapper;
    @Autowired
    private TradeApiRemoteService tradeApiRemoteService;




    @Override
    @Transactional
    public GroupOrder createGroupBooking(CreateGroupBookingRequest request, String url, ThirdLoginInfo userInfo) {
        log.info("创建拼团入参：" + JSONObject.toJSONString(request));
        String groupActivityId = request.getGroupActivityId();
        Date createTime = request.getCreateTime();
        String userId = userInfo.getUserId();
        String cloudUserId = userInfo.getCloudUserId();
        String orderId = request.getOrderId();
        String extInfo = request.getExtInfo();
        ThirdWxInfo wxInfo = userInfo.getWxInfo();
        String activityUrl = url + "?cloudUserId=" + cloudUserId;
        ResponseData<PortalOrderDetailInfo> portalOrderDetailInfoResponseData = tradeApiRemoteService.selectOrderDetail(Long.valueOf(orderId), userId);
        //校验订单是否支付
        if (portalOrderDetailInfoResponseData == null || !portalOrderDetailInfoResponseData.isSuccessful()){
            throw new BusinessException(BusinessExceptionCode.ORDER_NOT_EXIST);
        }
        PortalOrderDetailInfo data = portalOrderDetailInfoResponseData.getData();
        if (data.getStatus().intValue() != 1){
            throw new BusinessException(BusinessExceptionCode.ORDER_STATUS_ERROR);
        }
        GroupBookingActivity groupBookingActivity = groupBookingActivityMapper.selectByPrimaryKey(groupActivityId);
        Date date = new Date();
        GroupBookingActivityExample example = new GroupBookingActivityExample();
        example.createCriteria()
                .andCloudUserIdEqualTo(cloudUserId)
                .andGroupActivityIdEqualTo(groupActivityId)
                .andActivityStartDateLessThanOrEqualTo(date)
                .andActivityEndDateGreaterThan(date);
        List<GroupBookingActivity> groupBookingActivities = groupBookingActivityMapper.selectByExample(example);
        GroupBookingActivity activity = groupBookingActivities.get(0);
        Long buildGroupLimitHour = groupBookingActivity.getBuildGroupLimitHour();

        //创建订单
        GroupOrder groupOrder = new GroupOrder();
        groupOrder.setIsValid(true);
        groupOrder.setGroupOrderId(MD5Util.MD5(userId + groupActivityId + createTime.getTime()));
        groupOrder.setGroupActivityId(groupActivityId);
        groupOrder.setGroupOrderStatus(BusinessEnum.GroupBookingOrderStatus.GroupBooking.getCode());
        groupOrder.setGroupStartDate(date);
        groupOrder.setGroupEndDate(getHour(date, buildGroupLimitHour.intValue()));
        groupOrder.setActivityUrl(activityUrl);
        groupOrder.setIsPrivate(request.getPrivateGroup());
        groupOrder.setUpdateDate(date);
        groupOrderMapper.insertSelective(groupOrder);

        //创建用户拼单记录
        GroupUserOrder groupUserOrder = new GroupUserOrder();
        groupUserOrder.setGroupOrderId(groupOrder.getGroupOrderId());
        groupUserOrder.setUserId(userId);
        groupUserOrder.setExtInfo(extInfo);
        groupUserOrder.setStatus(BusinessEnum.GroupBookingOrderPayStatus.Done.getCode());
        groupUserOrder.setCreateOrderDate(date);
        groupUserOrder.setGroupUserOrderId(UUIDUtils.getUuid());
        groupUserOrder.setNickname(wxInfo.getName());
        groupUserOrder.setIcon(wxInfo.getHeadUrl());
        groupUserOrder.setUpdateDate(date);
        groupUserOrder.setPayDeadline(new Date(date.getTime() + activity.getPayTime() * 60 * 1000l));
        groupUserOrderMapper.insertSelective(groupUserOrder);


        //填上groupOrderId
        ActivityOrderRecordExample recordExample = new ActivityOrderRecordExample();
        recordExample.createCriteria().andCloudUserIdEqualTo(cloudUserId).andOrderIdEqualTo(orderId);
        ActivityOrderRecord aor = new ActivityOrderRecord();
        aor.setActivityBargainJoinId(groupOrder.getGroupOrderId());
        activityOrderRecordMapper.updateByExampleSelective(aor, recordExample);

        OmsOrder order = new OmsOrder();
        order.setId(Long.valueOf(orderId));
        order.setActivityId(groupUserOrder.getGroupUserOrderId());
        tradeApiRemoteService.updateOrderInfo(order);
        return groupOrder;
    }

    @Override
    public void cancelOrder(String groupUserOrderId) {
        GroupUserOrder groupUserOrder = groupUserOrderMapper.selectByPrimaryKey(groupUserOrderId);
        if (groupUserOrder == null)
            throw new BusinessException(BusinessExceptionCode.ORDER_NOT_EXIST);
        groupUserOrder.setStatus(BusinessEnum.GroupBookingOrderPayStatus.Cancel.getCode());
        groupUserOrder.setUpdateDate(new Date());
        groupUserOrderMapper.updateByPrimaryKeySelective(groupUserOrder);
    }

    @Override
    @Transactional
    public void joinGroupBooking(JoinGroupBookingRequest request, ThirdLoginInfo userInfo) {
        String cloudUserId = request.getCloudUserId();
        String userId = request.getUserId();
        String groupOrderId = request.getGroupOrderId();
        String orderId = request.getOrderId();
        String extInfo = request.getExtInfo();
        ThirdWxInfo wxInfo = userInfo.getWxInfo();
        Date payTime = request.getPayTime();
        ResponseData<PortalOrderDetailInfo> portalOrderDetailInfoResponseData = tradeApiRemoteService.selectOrderDetail(Long.valueOf(orderId), userId);
        //校验订单是否支付
        if (portalOrderDetailInfoResponseData == null || !portalOrderDetailInfoResponseData.isSuccessful()){
            throw new BusinessException(BusinessExceptionCode.ORDER_NOT_EXIST);
        }
        if (portalOrderDetailInfoResponseData.getData().getStatus() != 1){
            throw new BusinessException(BusinessExceptionCode.ORDER_STATUS_ERROR);
        }
        GroupOrder groupOrder = groupOrderMapper.selectByPrimaryKey(groupOrderId);
        //校验活动是否存在
        String groupActivityId = groupOrder.getGroupActivityId();
        Date date = new Date();

        GroupBookingActivity groupBookingActivity = groupBookingActivityMapper.selectByPrimaryKey(groupActivityId);
        Long buildGroupPeopleNum = groupBookingActivity.getBuildGroupPeopleNum();

        int size = 0;
        GroupUserOrderExample groupUserOrderExample = new GroupUserOrderExample();
        groupUserOrderExample.createCriteria().andGroupOrderIdEqualTo(groupOrderId);
        List<GroupUserOrder> groupUserOrders = groupUserOrderMapper.selectByExample(groupUserOrderExample);
        size = groupUserOrders.size();

        //可以拼团
        //创建用户拼单记录
        GroupUserOrder groupUserOrder = new GroupUserOrder();
        groupUserOrder.setGroupOrderId(groupOrder.getGroupOrderId());
        groupUserOrder.setUserId(userId);
        groupUserOrder.setStatus(BusinessEnum.GroupBookingOrderPayStatus.Done.getCode());
        groupUserOrder.setPayOrderDate(payTime);
        groupUserOrder.setCreateOrderDate(date);
        groupUserOrder.setGroupUserOrderId(UUIDUtils.getUuid());
        groupUserOrder.setNickname(wxInfo.getName());
        groupUserOrder.setIcon(wxInfo.getHeadUrl());
        groupUserOrder.setExtInfo(extInfo);
        groupUserOrder.setUpdateDate(date);
        groupUserOrderMapper.insertSelective(groupUserOrder);

        OmsOrder order = new OmsOrder();
        order.setId(Long.valueOf(orderId));
        order.setActivityId(groupUserOrder.getGroupUserOrderId());
        tradeApiRemoteService.updateOrderInfo(order);

        //修改订单状态
        if (size + 1 == buildGroupPeopleNum.intValue()){
            GroupOrder go = new GroupOrder();
            go.setGroupOrderId(groupOrderId);
            go.setGroupOrderStatus(BusinessEnum.GroupBookingOrderStatus.GroupBookingSuccess.getCode());
            go.setUpdateDate(date);
            groupOrderMapper.updateByPrimaryKeySelective(go);
        }
    }

    @Override
    public void getOrderInfo(String groupOrderId) {

    }

    @Override
    public BigDecimal getActivityOrderPrice(String activityId, String groupOrderId, String userId) {
        log.info("获取活动价入参：" + activityId + "======" + groupOrderId + "userId");
        GroupBookingActivity activity = groupBookingActivityMapper.selectByPrimaryKey(activityId);
        //拼团价
        BigDecimal activityPrice = activity.getActivityPrice();
        if (activity == null){
            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "活动不存在");
        }
        Date date = new Date();
        if (date.getTime() >= activity.getActivityEndDate().getTime())
            throw new BusinessException(BusinessExceptionCode.GROUP_BOOKING_IS_OVER);
        if (!StringUtils.isEmpty(groupOrderId)){
            String createOrderLock = RedisKeyConstants.BARGAIN_CREATE_ORDER_LOCK + ":" + groupOrderId;
            redisLockUtils.lock(createOrderLock);
            try {
                //是否参团
                GroupOrder groupOrder = groupOrderMapper.selectByPrimaryKey(groupOrderId);
                Long buildGroupPeopleNum = activity.getBuildGroupPeopleNum();
                GroupUserOrderExample groupUserOrderExample = new GroupUserOrderExample();
                groupUserOrderExample.createCriteria().andGroupOrderIdEqualTo(groupOrderId);
                //查拼团人数
                List<GroupUserOrder> userOrders = groupUserOrderMapper.selectByExample(groupUserOrderExample);
                int size = userOrders.size();
                Boolean isOutOfLimit = activity.getIsOutOfLimit();
                Date groupEndDate = groupOrder.getGroupEndDate();
                //有没有超过拼团的时间
                if (date.getTime() >= groupEndDate.getTime())
                    throw new BusinessException(BusinessExceptionCode.GROUP_BOOKING_IS_OVER);
                //校验是否可以继续拼团
//                if (!isOutOfLimit){
//
//                }
                if (size >= buildGroupPeopleNum.intValue())
                    throw new BusinessException(BusinessExceptionCode.OUT_OF_GROUP_BOOKING_PEOPLE_LIMIT);
                //有没有拼过
                for (GroupUserOrder guo : userOrders) {
                    if (userId.equals(guo.getUserId()))
                        throw new BusinessException(BusinessExceptionCode.YOU_CAN_NOT_JOIN_AGAIN);
                }

//            //保存订单
//            ActivityOrderRecord record = new ActivityOrderRecord();
//            record.setCloudUserId(cloudUserId);
//            record.setUserId(userId);
//            record.setActivityBargainJoinId(groupOrderId);
//            record.setOrderMoney(activityPrice);
//            record.setOrderId(orderId);
//            record.setCreateDate(date);
//            record.setId(UUIDUtils.getUuid());
//            activityOrderRecordMapper.insertSelective(record);
            }finally {
                redisLockUtils.releaseLock(createOrderLock);
            }
        }
        return activityPrice;
    }

    @Override
    public String saveOrder(String groupOrderId, String userId, String orderId, String type, String activityId) {
        Date date = new Date();
        GroupBookingActivity activity = groupBookingActivityMapper.selectByPrimaryKey(activityId);
        //检查活动是否存在
        if (activity == null){
            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "活动不存在");
        }
        ActivityOrderRecord record = new ActivityOrderRecord();
        String cloudUserId = activity.getCloudUserId();
        if (!StringUtils.isEmpty(groupOrderId)){
            //校验订单是否存在避免重复下单
            GroupUserOrderExample example = new GroupUserOrderExample();
            example.createCriteria().andGroupOrderIdEqualTo(groupOrderId).andUserIdEqualTo(userId);
            List<GroupUserOrder> groupUserOrders = groupUserOrderMapper.selectByExample(example);
            //校验是否参与了活动
            if (!CollectionUtils.isEmpty(groupUserOrders)){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "已参与活动");
            }
            record.setActivityBargainJoinId(groupOrderId);
        }
        //保存订单
        record.setCloudUserId(cloudUserId);
        record.setUserId(userId);
        record.setOrderMoney(activity.getActivityPrice());
        record.setOrderId(orderId);
        record.setCreateDate(date);
        record.setOrderType(type);
        record.setActivityBargainId(activityId);
        record.setId(UUIDUtils.getUuid());
        activityOrderRecordMapper.insertSelective(record);

        return activityId;
    }

    @Override
    public void saveOrderRecord(String activityId, String userId, String orderId, Boolean isSuccess) {
        //看订单是不是存在了
        log.info("返回信息 ============== activityId: " + activityId + "_userId:" + userId +  "_orderId: " + orderId + "_isSuccess :" + isSuccess);
        ActivityOrderRecordExample example = new ActivityOrderRecordExample();
        example.createCriteria().andActivityBargainIdEqualTo(activityId).andOrderIdEqualTo(orderId).andUserIdEqualTo(userId);
        ActivityOrderRecord record = new ActivityOrderRecord();
        record.setStatus(isSuccess ? new Byte("1") : new Byte("2"));
        record.setOrderId(orderId);
        record.setActivityBargainId(activityId);
        activityOrderRecordMapper.updateByExampleSelective(record, example);
    }

    @Override
    public void updateRefundStatus(String activityId) {
        GroupUserOrder guo = new GroupUserOrder();
        guo.setGroupUserOrderId(activityId);
        guo.setStatus("5");
        guo.setUpdateDate(new Date());
        groupUserOrderMapper.updateByPrimaryKeySelective(guo);
    }

    @Override
    public void updateGroupStatus(String activityId, Long orderId) {
        GroupUserOrder guo = groupUserOrderMapper.selectByPrimaryKey(activityId);
        if (guo != null){
            String groupOrderId = guo.getGroupOrderId();
            ActivityOrderRecordExample example = new ActivityOrderRecordExample();
            example.createCriteria().andActivityBargainJoinIdEqualTo(groupOrderId).andOrderIdEqualTo(orderId+ "");
            List<ActivityOrderRecord> activityOrderRecords = activityOrderRecordMapper.selectByExample(example);
            if (!CollectionUtils.isEmpty(activityOrderRecords)){
                ActivityOrderRecord record = activityOrderRecords.get(0);
                GroupOrder groupOrder = groupOrderMapper.selectByPrimaryKey(groupOrderId);
                if (groupOrder != null){
                    GroupUserOrderExample groupUserOrderExample = new GroupUserOrderExample();
                    groupUserOrderExample.createCriteria().andGroupUserOrderIdEqualTo(groupOrderId).andStatusIn(Arrays.asList("1", "2", "3"));
                    List<GroupUserOrder> groupUserOrders = groupUserOrderMapper.selectByExample(groupUserOrderExample);
                    if (CollectionUtils.isEmpty(groupUserOrders)){
                        groupOrder.setUpdateDate(new Date());
                        groupOrder.setGroupOrderStatus(new Byte("4"));
                        groupOrderMapper.updateByPrimaryKeySelective(groupOrder);
                    }
                }
            }
        }
    }

    @Override
    public GroupBookingActivity selectById(String activityId) {
        GroupBookingActivity activity = groupBookingActivityMapper.selectByPrimaryKey(activityId);
        return activity;
    }


    public static Date getHour(Date date, int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, i);
        return calendar.getTime();
    }
}
