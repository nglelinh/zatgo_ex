package com.zatgo.zup.activity.entity.response;

/**
 * Created by 46041 on 2019/5/16.
 */
public class CountResult {

    private String id;

    private Integer helpPeople;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getHelpPeople() {
        return helpPeople;
    }

    public void setHelpPeople(Integer helpPeople) {
        this.helpPeople = helpPeople;
    }
}
