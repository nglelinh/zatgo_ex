package com.zatgo.zup.activity.mapper;

import com.zatgo.zup.activity.entity.WechatMsgReply;
import com.zatgo.zup.activity.entity.WechatMsgReplyExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WechatMsgReplyMapper {
    int countByExample(WechatMsgReplyExample example);

    int deleteByExample(WechatMsgReplyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WechatMsgReply record);

    int insertSelective(WechatMsgReply record);

    List<WechatMsgReply> selectByExample(WechatMsgReplyExample example);

    WechatMsgReply selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WechatMsgReply record, @Param("example") WechatMsgReplyExample example);

    int updateByExample(@Param("record") WechatMsgReply record, @Param("example") WechatMsgReplyExample example);

    int updateByPrimaryKeySelective(WechatMsgReply record);

    int updateByPrimaryKey(WechatMsgReply record);
}