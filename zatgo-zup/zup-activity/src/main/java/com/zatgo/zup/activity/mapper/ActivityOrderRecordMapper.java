package com.zatgo.zup.activity.mapper;


import com.zatgo.zup.activity.entity.ActivityOrderRecord;
import com.zatgo.zup.activity.entity.ActivityProduct;

import java.util.List;

public interface ActivityOrderRecordMapper extends BaseMapper<ActivityOrderRecord>{

    void syncProduct(List<ActivityProduct> list);

}