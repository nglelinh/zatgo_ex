package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ActivityBargainList {
    private String activityBargainListId;

    private String userId;

    private String activityBargainJoinId;

    private Date createDate;

    private BigDecimal bargainMoney;

    private String weixinOpenId;

    private String cloudUserId;

    private String wechatNick;

    private String wechatImg;

    public String getActivityBargainListId() {
        return activityBargainListId;
    }

    public void setActivityBargainListId(String activityBargainListId) {
        this.activityBargainListId = activityBargainListId == null ? null : activityBargainListId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getActivityBargainJoinId() {
        return activityBargainJoinId;
    }

    public void setActivityBargainJoinId(String activityBargainJoinId) {
        this.activityBargainJoinId = activityBargainJoinId == null ? null : activityBargainJoinId.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getBargainMoney() {
        return bargainMoney;
    }

    public void setBargainMoney(BigDecimal bargainMoney) {
        this.bargainMoney = bargainMoney;
    }

    public String getWeixinOpenId() {
        return weixinOpenId;
    }

    public void setWeixinOpenId(String weixinOpenId) {
        this.weixinOpenId = weixinOpenId == null ? null : weixinOpenId.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }

    public String getWechatNick() {
        return wechatNick;
    }

    public void setWechatNick(String wechatNick) {
        this.wechatNick = wechatNick;
    }

    public String getWechatImg() {
        return wechatImg;
    }

    public void setWechatImg(String wechatImg) {
        this.wechatImg = wechatImg;
    }
}