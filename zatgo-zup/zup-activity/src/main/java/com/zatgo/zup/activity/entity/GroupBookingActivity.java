package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.Date;

public class GroupBookingActivity {
    private String groupActivityId;

    private String cloudUserId;

    private String productId;

    private String productName;

    private BigDecimal activityPrice;

    private BigDecimal originalPrice;

    private BigDecimal promotionPrice;

    private String productImgUrl;

    private Date activityStartDate;

    private Date activityEndDate;

    private Long buildGroupLimitHour;

    private Long buildGroupPeopleNum;

    private Long perLimitBookingNum;

    private Byte activityStatus;

    private String operatorName;

    private Date operateDate;

    private String operatorId;

    private Date createTime;

    private Boolean isOutOfLimit;

    private String shareTitle;

    private String shareDesc;

    private String shareImgUrl;

    private String shareType;

    private String shareLink;

    private String activityImg;

    private Long payTime;

    private String priceIds;

    public String getGroupActivityId() {
        return groupActivityId;
    }

    public void setGroupActivityId(String groupActivityId) {
        this.groupActivityId = groupActivityId == null ? null : groupActivityId.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public BigDecimal getActivityPrice() {
        return activityPrice;
    }

    public void setActivityPrice(BigDecimal activityPrice) {
        this.activityPrice = activityPrice;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public String getProductImgUrl() {
        return productImgUrl;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl == null ? null : productImgUrl.trim();
    }

    public Date getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(Date activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public Date getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(Date activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public Long getBuildGroupLimitHour() {
        return buildGroupLimitHour;
    }

    public void setBuildGroupLimitHour(Long buildGroupLimitHour) {
        this.buildGroupLimitHour = buildGroupLimitHour;
    }

    public Long getBuildGroupPeopleNum() {
        return buildGroupPeopleNum;
    }

    public void setBuildGroupPeopleNum(Long buildGroupPeopleNum) {
        this.buildGroupPeopleNum = buildGroupPeopleNum;
    }

    public Long getPerLimitBookingNum() {
        return perLimitBookingNum;
    }

    public void setPerLimitBookingNum(Long perLimitBookingNum) {
        this.perLimitBookingNum = perLimitBookingNum;
    }

    public Byte getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Byte activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    public Date getOperateDate() {
        return operateDate;
    }

    public void setOperateDate(Date operateDate) {
        this.operateDate = operateDate;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId == null ? null : operatorId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getIsOutOfLimit() {
        return isOutOfLimit;
    }

    public void setIsOutOfLimit(Boolean isOutOfLimit) {
        this.isOutOfLimit = isOutOfLimit;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareDesc() {
        return shareDesc;
    }

    public void setShareDesc(String shareDesc) {
        this.shareDesc = shareDesc;
    }

    public String getShareImgUrl() {
        return shareImgUrl;
    }

    public void setShareImgUrl(String shareImgUrl) {
        this.shareImgUrl = shareImgUrl;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public String getActivityImg() {
        return activityImg;
    }

    public void setActivityImg(String activityImg) {
        this.activityImg = activityImg;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public String getPriceIds() {
        return priceIds;
    }

    public void setPriceIds(String priceIds) {
        this.priceIds = priceIds;
    }
}