package com.zatgo.zup.activity.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.activity.service.ThirdService;
import com.zatgo.zup.activity.utils.HttpUtil;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by 46041 on 2019/5/14.
 */

@Service("thirdService")
public class ThirdServiceImpl implements ThirdService {

    private static final Logger logger = LoggerFactory.getLogger(ThirdServiceImpl.class);


//    @Value("${system.isDefault}")
//    private Boolean isDefault;
//    @Value("${system.cloudUserId}")
//    private String defaultCloudUserId;
//    @Value("${system.thirdAuthenticationUrl}")
//    private String thirdAuthenticationUrl;
//    @Value("${system.thirdAuthenticationAdminUrl}")
//    private String thirdAuthenticationAdminUrl;
//    @Value("${system.thirdAuthenticationToken}")
//    private String thirdAuthenticationToken;
//    @Value("${wx.pay.appId}")
//    private String weixinOfficialAcctAppId;
//    @Value("${wx.appSecret}")
//    private String weixinOfficialAcctKey;



//    @Override
//    public ThirdLoginInfo authorization(String cloudUserId, String token, HttpServletRequest request) {
//        return getInfo(cloudUserId, request, thirdAuthenticationUrl);
//    }
//
//    @Override
//    public ThirdLoginInfo adminAuthorization(String cloudUserId, String token, HttpServletRequest request) {
//        return getInfo(cloudUserId, request, thirdAuthenticationAdminUrl);
//    }

//    private ThirdLoginInfo getInfo(String cloudUserId, HttpServletRequest request, String url){
//        cloudUserId = isDefault ? defaultCloudUserId : cloudUserId;
//        Map<String, String> header = new HashMap<>();
//        try {
//            header.putAll(getHeaders(request));
//            String res = HttpUtil.doPost(url, "", header);
//            ResponseData responseData = JSONObject.parseObject(res, ResponseData.class);
//            String code = responseData.getCode();
//            if (code != null && ("200".equals(code) || "0000".equals(code))){
//                ThirdLoginInfo loginInfo = JSONObject.parseObject(JSONObject.toJSONString(responseData.getData()), ThirdLoginInfo.class);
//                loginInfo.setCloudUserWXAppId(weixinOfficialAcctAppId);
//                loginInfo.setCloudUserWXSecret(weixinOfficialAcctKey);
//                loginInfo.setCloudUserId(cloudUserId);
//                return loginInfo;
//            } else {
//                logger.error("", JSONObject.toJSONString(responseData));
//            }
//        } catch (Exception e) {
//            logger.error("", e);
//        }
//        return null;
//    }


    private Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> map = new LinkedHashMap<>();
        Enumeration<String> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }

}
