package com.zatgo.zup.activity.mapper;

import com.zatgo.zup.activity.entity.ActivityBargainJoin;
import com.zatgo.zup.activity.entity.response.CountResult;
import com.zatgo.zup.activity.entity.response.ExamineListRespones;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ActivityBargainJoinMapper extends BaseMapper<ActivityBargainJoin>{

    void updateBargainCount(@Param("joinId") String joinId, @Param("amount") BigDecimal amount);

    List<ExamineListRespones> selectExamineList(@Param("status") Byte status,
                                                @Param("activityBargainId") String activityBargainId,
                                                @Param("keyword") String keyword);

    List<CountResult> getCount(@Param("ids") String ids);

    String getHelpUserNick(@Param("bargainActivityId") String bargainActivityId, @Param("openId") String openId);
}