package com.zatgo.zup.activity.controller.wx;

import com.zatgo.zup.activity.service.WXAttentionNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by 46041 on 2019/6/13.
 */



@Controller
@RequestMapping(value = "/activity/wx/remote")
public class WechatRemoteController {

    @Autowired
    private WXAttentionNotifyService attentionNotifyService;

    @PostMapping(value = "/attention")
    @ResponseBody
    public String attention(@RequestParam("eventKey") String eventKey,
                            @RequestParam("fromWXOpenId") String fromWXOpenId,
                            @RequestParam("toWXOpenId") String toWXOpenId){
        return attentionNotifyService.attention(eventKey, fromWXOpenId, toWXOpenId);
    }
}
