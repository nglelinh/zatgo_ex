package com.zatgo.zup.activity.controller.wx;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.activity.entity.wx.MsgToken;
import com.zatgo.zup.activity.entity.wx.WechatMsg;
import com.zatgo.zup.activity.service.IWechatMenuService;
import com.zatgo.zup.activity.service.IWechatMsgService;
import com.zatgo.zup.activity.service.WXAttentionNotifyService;
import com.zatgo.zup.activity.service.WechatMaterialService;
import com.zatgo.zup.activity.utils.HttpUtil;
import com.zatgo.zup.activity.utils.Xml2BeanUtils;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.redis.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 暂不使用
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/activity/wx/msg")
public class WechatMsgController{

	private static final Logger logger = LoggerFactory.getLogger(WechatMsgController.class);

	private static final String WECHAT_MSG_KEY = "WECHAT_MSG_KEY_:%s";

	@Resource
	private IWechatMsgService wechatMsgService;

	@Resource
	private IWechatMenuService wechatMenuService;

	@Autowired
	private WXAttentionNotifyService attentionNotifyService;


	@Autowired
	private WXAttentionNotifyService weiXinService;

	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	private WechatMaterialService wechatMaterialService;

	@RequestMapping(value = "/token/{cloudUserId}", method = RequestMethod.GET)
	@ResponseBody
	public String msgToken(MsgToken token, @PathVariable("cloudUserId") String cloudUserId) {

		if (StringUtils.isEmpty(token.getSignature()) || StringUtils.isEmpty(token.getTimestamp())
				|| StringUtils.isEmpty(token.getNonce()) || StringUtils.isEmpty(token.getEchostr())) {
			token.setEchostr("");
			return token.getEchostr();
		}

		return wechatMsgService.validateWxToken(token, cloudUserId).getEchostr();
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public String msgToken(@RequestBody WechatMsg msg) {
		return wechatMsgService.wxMsg(msg);
	}

	@RequestMapping(value = "/menu/{cloudUserId}", method = RequestMethod.GET)
	@ResponseBody
	public void menu(@PathVariable String cloudUserId) {
		wechatMenuService.generateWechatMenu(cloudUserId);
	}

	@PostMapping(value = "/token/{cloudUserId}")
	@ResponseBody
	public String attention(@RequestBody WechatMsg msg) {
		try {
			logger.info("微信公众号关注通知参数：{}", JSONObject.toJSONString(msg));
			String msgKey;
			String msgId = msg.getMsgId();
			if(!StringUtils.isEmpty(msgId)) {
				msgKey = String.format(WECHAT_MSG_KEY, msgId);
			} else {
				msgKey = String.format(WECHAT_MSG_KEY, msg.getFromUserName() + "_" + msg.getCreateTime());
			}

			boolean hasKey = redisUtils.hasKey(msgKey);
			logger.info("haskeu：{}",hasKey );
			if(hasKey) {
				return "success";
			}



			redisUtils.put(msgKey, msgKey, 1, TimeUnit.DAYS);

			String event = msg.getEvent();
			String fromWXOpenId = msg.getFromUserName();
			String eventKey = msg.getEventKey();
			if (StringUtils.isEmpty(eventKey))
				eventKey = "";
			String[] split = eventKey.split("_");
			if (WechatMsg.Event.SUBSCRIBE.getEvent().equals(event) && split.length > 1){
//				loginInfo = getUserInfo(fromWXOpenId);
				return attentionNotifyService.attention(eventKey, fromWXOpenId, msg.getToUserName());
			} else {
				return wechatMsgService.wxMsg(msg);
			}

//            if (WechatMsg.Event.SCAN.getEvent().equals(event)){
//                loginInfo = getUserInfo(fromWXOpenId);
//                attentionNotifyService.cancel(loginInfo, eventKey);
//            }

		} catch (Exception e){
			logger.error("", e);
		}

		return "success";
	}

	@RequestMapping(value = "/material", method = RequestMethod.GET)
	@ResponseBody
	public String material() {
		return wechatMaterialService.getWechatMaterialList();
	}


	public static void main(String[] args) {
		String url = "{\"createTime\":\"1558419634\",\"event\":\"subscribe\",\"eventKey\":\"qrscene_109_8ce72a140c57480296cd48412677081f_835F60BB232CC9429C1C893398255E90\",\"fromUserName\":\"ogKJm1CiJYENfOmbtqckvmdbh908\",\"msgType\":\"event\",\"ticket\":\"gQF78DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyOFd4d0VYMHlkQ2sxMWRCYnh0Y0cAAgRNmONcAwQAjScA\",\"toUserName\":\"gh_398483f31047\"}";
		System.out.println(Xml2BeanUtils.objectToXml(JSONObject.parseObject(url, WechatMsg.class), "utf-8"));
	}

}
