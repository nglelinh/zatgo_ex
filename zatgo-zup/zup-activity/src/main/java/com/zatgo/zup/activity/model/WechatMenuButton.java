package com.zatgo.zup.activity.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;
import java.util.List;

public class WechatMenuButton {

	public enum ButtonType {
		CLICK("click"), VIEW("view"), SCANCODE_PUSH("scancode_push"), SCANCODE_WAITMSG(
				"scancode_waitmsg"), PIC_SYSPHOTO("pic_sysphoto"), PIC_PHOTO_OR_ALBUM("pic_photo_or_album"), PIC_WEIXIN(
						"pic_weixin"), LOCATION_SELECT(
								"location_select"), MEDIA_ID("media_id"), VIEW_LIMITED("view_limited");

		private String type;

		ButtonType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}

	private List<WechatMenuButton> sub_button;

	private String type;

	private String name;

	private String key;

	private String url;

	@JSONField(name = "media_id")
	private String mediaId;

	private String appid;

	private String pagepath;

	public List<WechatMenuButton> getSub_button() {
		if (sub_button == null && (ButtonType.SCANCODE_WAITMSG.getType().equals(type)
				|| ButtonType.SCANCODE_PUSH.getType().equals(type) || ButtonType.PIC_SYSPHOTO.getType().equals(type)
				|| ButtonType.PIC_PHOTO_OR_ALBUM.getType().equals(type)
				|| ButtonType.PIC_WEIXIN.getType().equals(type))) {
			sub_button = new ArrayList<WechatMenuButton>();
		}
		return sub_button;
	}

	public void setSub_button(List<WechatMenuButton> sub_button) {
		this.sub_button = sub_button;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getPagepath() {
		return pagepath;
	}

	public void setPagepath(String pagepath) {
		this.pagepath = pagepath;
	}

}
