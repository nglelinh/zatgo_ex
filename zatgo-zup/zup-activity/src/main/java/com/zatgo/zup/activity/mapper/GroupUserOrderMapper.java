package com.zatgo.zup.activity.mapper;


import com.zatgo.zup.activity.entity.GroupUserOrder;
import com.zatgo.zup.common.model.NewActivityListRespones;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GroupUserOrderMapper extends BaseMapper<GroupUserOrder>{

    List<GroupUserOrder> selectActivityOrderList(@Param("activityId") String activityId, @Param("userId") String userId);

    List<NewActivityListRespones> getActivityList(@Param("userId") String userId);
}