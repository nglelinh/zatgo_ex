package com.zatgo.zup.activity.mapper;


import com.zatgo.zup.activity.entity.GroupBookingActivity;
import com.zatgo.zup.activity.entity.response.GroupInfoResponse;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface GroupBookingActivityMapper extends BaseMapper<GroupBookingActivity>{

    List<GroupInfoResponse> getGroupInfo(@Param("activityId") String activityId,
                                         @Param("pageSize") Integer pageSize,
                                         @Param("nowDate")Date nowDate);

    List<GroupInfoResponse> getAdminGroupInfo(@Param("activityId") String activityId,@Param("status") Integer status,@Param("time") Date time);

    List<String> needAutoRefund(@Param("groupOrderId") String groupOrderId, @Param("groupOrderUserId") String groupOrderUserId);

}