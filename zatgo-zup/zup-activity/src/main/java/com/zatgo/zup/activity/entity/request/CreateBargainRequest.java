package com.zatgo.zup.activity.entity.request;

import java.util.Date;

/**
 * Created by 46041 on 2019/3/12.
 */
public class CreateBargainRequest {

    private String bargainActivityId;

    private Date date;


    public String getBargainActivityId() {
        return bargainActivityId;
    }

    public void setBargainActivityId(String bargainActivityId) {
        this.bargainActivityId = bargainActivityId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
