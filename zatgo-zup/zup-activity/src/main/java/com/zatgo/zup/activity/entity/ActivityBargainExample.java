package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityBargainExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ActivityBargainExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBargainActivityIdIsNull() {
            addCriterion("bargain_activity_id is null");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdIsNotNull() {
            addCriterion("bargain_activity_id is not null");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdEqualTo(String value) {
            addCriterion("bargain_activity_id =", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdNotEqualTo(String value) {
            addCriterion("bargain_activity_id <>", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdGreaterThan(String value) {
            addCriterion("bargain_activity_id >", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdGreaterThanOrEqualTo(String value) {
            addCriterion("bargain_activity_id >=", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdLessThan(String value) {
            addCriterion("bargain_activity_id <", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdLessThanOrEqualTo(String value) {
            addCriterion("bargain_activity_id <=", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdLike(String value) {
            addCriterion("bargain_activity_id like", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdNotLike(String value) {
            addCriterion("bargain_activity_id not like", value, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdIn(List<String> values) {
            addCriterion("bargain_activity_id in", values, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdNotIn(List<String> values) {
            addCriterion("bargain_activity_id not in", values, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdBetween(String value1, String value2) {
            addCriterion("bargain_activity_id between", value1, value2, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andBargainActivityIdNotBetween(String value1, String value2) {
            addCriterion("bargain_activity_id not between", value1, value2, "bargainActivityId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(String value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(String value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(String value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(String value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(String value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLike(String value) {
            addCriterion("product_id like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotLike(String value) {
            addCriterion("product_id not like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<String> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<String> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(String value1, String value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(String value1, String value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNull() {
            addCriterion("product_name is null");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNotNull() {
            addCriterion("product_name is not null");
            return (Criteria) this;
        }

        public Criteria andProductNameEqualTo(String value) {
            addCriterion("product_name =", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotEqualTo(String value) {
            addCriterion("product_name <>", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThan(String value) {
            addCriterion("product_name >", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThanOrEqualTo(String value) {
            addCriterion("product_name >=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThan(String value) {
            addCriterion("product_name <", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThanOrEqualTo(String value) {
            addCriterion("product_name <=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLike(String value) {
            addCriterion("product_name like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotLike(String value) {
            addCriterion("product_name not like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameIn(List<String> values) {
            addCriterion("product_name in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotIn(List<String> values) {
            addCriterion("product_name not in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameBetween(String value1, String value2) {
            addCriterion("product_name between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotBetween(String value1, String value2) {
            addCriterion("product_name not between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIsNull() {
            addCriterion("product_img_abbr_url is null");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIsNotNull() {
            addCriterion("product_img_abbr_url is not null");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlEqualTo(String value) {
            addCriterion("product_img_abbr_url =", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotEqualTo(String value) {
            addCriterion("product_img_abbr_url <>", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlGreaterThan(String value) {
            addCriterion("product_img_abbr_url >", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlGreaterThanOrEqualTo(String value) {
            addCriterion("product_img_abbr_url >=", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLessThan(String value) {
            addCriterion("product_img_abbr_url <", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLessThanOrEqualTo(String value) {
            addCriterion("product_img_abbr_url <=", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLike(String value) {
            addCriterion("product_img_abbr_url like", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotLike(String value) {
            addCriterion("product_img_abbr_url not like", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIn(List<String> values) {
            addCriterion("product_img_abbr_url in", values, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotIn(List<String> values) {
            addCriterion("product_img_abbr_url not in", values, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlBetween(String value1, String value2) {
            addCriterion("product_img_abbr_url between", value1, value2, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotBetween(String value1, String value2) {
            addCriterion("product_img_abbr_url not between", value1, value2, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIsNull() {
            addCriterion("add_order_url is null");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIsNotNull() {
            addCriterion("add_order_url is not null");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlEqualTo(String value) {
            addCriterion("add_order_url =", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotEqualTo(String value) {
            addCriterion("add_order_url <>", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlGreaterThan(String value) {
            addCriterion("add_order_url >", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlGreaterThanOrEqualTo(String value) {
            addCriterion("add_order_url >=", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLessThan(String value) {
            addCriterion("add_order_url <", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLessThanOrEqualTo(String value) {
            addCriterion("add_order_url <=", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLike(String value) {
            addCriterion("add_order_url like", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotLike(String value) {
            addCriterion("add_order_url not like", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIn(List<String> values) {
            addCriterion("add_order_url in", values, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotIn(List<String> values) {
            addCriterion("add_order_url not in", values, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlBetween(String value1, String value2) {
            addCriterion("add_order_url between", value1, value2, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotBetween(String value1, String value2) {
            addCriterion("add_order_url not between", value1, value2, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIsNull() {
            addCriterion("product_money is null");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIsNotNull() {
            addCriterion("product_money is not null");
            return (Criteria) this;
        }

        public Criteria andProductMoneyEqualTo(BigDecimal value) {
            addCriterion("product_money =", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotEqualTo(BigDecimal value) {
            addCriterion("product_money <>", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyGreaterThan(BigDecimal value) {
            addCriterion("product_money >", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("product_money >=", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLessThan(BigDecimal value) {
            addCriterion("product_money <", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("product_money <=", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIn(List<BigDecimal> values) {
            addCriterion("product_money in", values, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotIn(List<BigDecimal> values) {
            addCriterion("product_money not in", values, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("product_money between", value1, value2, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("product_money not between", value1, value2, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductIntroIsNull() {
            addCriterion("product_intro is null");
            return (Criteria) this;
        }

        public Criteria andProductIntroIsNotNull() {
            addCriterion("product_intro is not null");
            return (Criteria) this;
        }

        public Criteria andProductIntroEqualTo(String value) {
            addCriterion("product_intro =", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotEqualTo(String value) {
            addCriterion("product_intro <>", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroGreaterThan(String value) {
            addCriterion("product_intro >", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroGreaterThanOrEqualTo(String value) {
            addCriterion("product_intro >=", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLessThan(String value) {
            addCriterion("product_intro <", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLessThanOrEqualTo(String value) {
            addCriterion("product_intro <=", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLike(String value) {
            addCriterion("product_intro like", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotLike(String value) {
            addCriterion("product_intro not like", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroIn(List<String> values) {
            addCriterion("product_intro in", values, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotIn(List<String> values) {
            addCriterion("product_intro not in", values, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroBetween(String value1, String value2) {
            addCriterion("product_intro between", value1, value2, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotBetween(String value1, String value2) {
            addCriterion("product_intro not between", value1, value2, "productIntro");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyIsNull() {
            addCriterion("activity_money is null");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyIsNotNull() {
            addCriterion("activity_money is not null");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyEqualTo(BigDecimal value) {
            addCriterion("activity_money =", value, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyNotEqualTo(BigDecimal value) {
            addCriterion("activity_money <>", value, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyGreaterThan(BigDecimal value) {
            addCriterion("activity_money >", value, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_money >=", value, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyLessThan(BigDecimal value) {
            addCriterion("activity_money <", value, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_money <=", value, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyIn(List<BigDecimal> values) {
            addCriterion("activity_money in", values, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyNotIn(List<BigDecimal> values) {
            addCriterion("activity_money not in", values, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_money between", value1, value2, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_money not between", value1, value2, "activityMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyIsNull() {
            addCriterion("activity_start_money is null");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyIsNotNull() {
            addCriterion("activity_start_money is not null");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyEqualTo(BigDecimal value) {
            addCriterion("activity_start_money =", value, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyNotEqualTo(BigDecimal value) {
            addCriterion("activity_start_money <>", value, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyGreaterThan(BigDecimal value) {
            addCriterion("activity_start_money >", value, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_start_money >=", value, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyLessThan(BigDecimal value) {
            addCriterion("activity_start_money <", value, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_start_money <=", value, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyIn(List<BigDecimal> values) {
            addCriterion("activity_start_money in", values, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyNotIn(List<BigDecimal> values) {
            addCriterion("activity_start_money not in", values, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_start_money between", value1, value2, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andActivityStartMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_start_money not between", value1, value2, "activityStartMoney");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitIsNull() {
            addCriterion("is_remove_limit is null");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitIsNotNull() {
            addCriterion("is_remove_limit is not null");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitEqualTo(Boolean value) {
            addCriterion("is_remove_limit =", value, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitNotEqualTo(Boolean value) {
            addCriterion("is_remove_limit <>", value, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitGreaterThan(Boolean value) {
            addCriterion("is_remove_limit >", value, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_remove_limit >=", value, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitLessThan(Boolean value) {
            addCriterion("is_remove_limit <", value, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitLessThanOrEqualTo(Boolean value) {
            addCriterion("is_remove_limit <=", value, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitIn(List<Boolean> values) {
            addCriterion("is_remove_limit in", values, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitNotIn(List<Boolean> values) {
            addCriterion("is_remove_limit not in", values, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitBetween(Boolean value1, Boolean value2) {
            addCriterion("is_remove_limit between", value1, value2, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andIsRemoveLimitNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_remove_limit not between", value1, value2, "isRemoveLimit");
            return (Criteria) this;
        }

        public Criteria andBargainRateIsNull() {
            addCriterion("bargain_rate is null");
            return (Criteria) this;
        }

        public Criteria andBargainRateIsNotNull() {
            addCriterion("bargain_rate is not null");
            return (Criteria) this;
        }

        public Criteria andBargainRateEqualTo(BigDecimal value) {
            addCriterion("bargain_rate =", value, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateNotEqualTo(BigDecimal value) {
            addCriterion("bargain_rate <>", value, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateGreaterThan(BigDecimal value) {
            addCriterion("bargain_rate >", value, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("bargain_rate >=", value, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateLessThan(BigDecimal value) {
            addCriterion("bargain_rate <", value, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("bargain_rate <=", value, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateIn(List<BigDecimal> values) {
            addCriterion("bargain_rate in", values, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateNotIn(List<BigDecimal> values) {
            addCriterion("bargain_rate not in", values, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bargain_rate between", value1, value2, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andBargainRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bargain_rate not between", value1, value2, "bargainRate");
            return (Criteria) this;
        }

        public Criteria andJoinCountIsNull() {
            addCriterion("join_count is null");
            return (Criteria) this;
        }

        public Criteria andJoinCountIsNotNull() {
            addCriterion("join_count is not null");
            return (Criteria) this;
        }

        public Criteria andJoinCountEqualTo(Integer value) {
            addCriterion("join_count =", value, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountNotEqualTo(Integer value) {
            addCriterion("join_count <>", value, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountGreaterThan(Integer value) {
            addCriterion("join_count >", value, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("join_count >=", value, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountLessThan(Integer value) {
            addCriterion("join_count <", value, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountLessThanOrEqualTo(Integer value) {
            addCriterion("join_count <=", value, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountIn(List<Integer> values) {
            addCriterion("join_count in", values, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountNotIn(List<Integer> values) {
            addCriterion("join_count not in", values, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountBetween(Integer value1, Integer value2) {
            addCriterion("join_count between", value1, value2, "joinCount");
            return (Criteria) this;
        }

        public Criteria andJoinCountNotBetween(Integer value1, Integer value2) {
            addCriterion("join_count not between", value1, value2, "joinCount");
            return (Criteria) this;
        }

        public Criteria andProductDescIsNull() {
            addCriterion("product_desc is null");
            return (Criteria) this;
        }

        public Criteria andProductDescIsNotNull() {
            addCriterion("product_desc is not null");
            return (Criteria) this;
        }

        public Criteria andProductDescEqualTo(String value) {
            addCriterion("product_desc =", value, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescNotEqualTo(String value) {
            addCriterion("product_desc <>", value, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescGreaterThan(String value) {
            addCriterion("product_desc >", value, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescGreaterThanOrEqualTo(String value) {
            addCriterion("product_desc >=", value, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescLessThan(String value) {
            addCriterion("product_desc <", value, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescLessThanOrEqualTo(String value) {
            addCriterion("product_desc <=", value, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescLike(String value) {
            addCriterion("product_desc like", value, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescNotLike(String value) {
            addCriterion("product_desc not like", value, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescIn(List<String> values) {
            addCriterion("product_desc in", values, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescNotIn(List<String> values) {
            addCriterion("product_desc not in", values, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescBetween(String value1, String value2) {
            addCriterion("product_desc between", value1, value2, "productDesc");
            return (Criteria) this;
        }

        public Criteria andProductDescNotBetween(String value1, String value2) {
            addCriterion("product_desc not between", value1, value2, "productDesc");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIsNull() {
            addCriterion("activity_start_date is null");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIsNotNull() {
            addCriterion("activity_start_date is not null");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateEqualTo(Date value) {
            addCriterion("activity_start_date =", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotEqualTo(Date value) {
            addCriterion("activity_start_date <>", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateGreaterThan(Date value) {
            addCriterion("activity_start_date >", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("activity_start_date >=", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateLessThan(Date value) {
            addCriterion("activity_start_date <", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateLessThanOrEqualTo(Date value) {
            addCriterion("activity_start_date <=", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIn(List<Date> values) {
            addCriterion("activity_start_date in", values, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotIn(List<Date> values) {
            addCriterion("activity_start_date not in", values, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateBetween(Date value1, Date value2) {
            addCriterion("activity_start_date between", value1, value2, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotBetween(Date value1, Date value2) {
            addCriterion("activity_start_date not between", value1, value2, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIsNull() {
            addCriterion("activity_end_date is null");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIsNotNull() {
            addCriterion("activity_end_date is not null");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateEqualTo(Date value) {
            addCriterion("activity_end_date =", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotEqualTo(Date value) {
            addCriterion("activity_end_date <>", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateGreaterThan(Date value) {
            addCriterion("activity_end_date >", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("activity_end_date >=", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateLessThan(Date value) {
            addCriterion("activity_end_date <", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateLessThanOrEqualTo(Date value) {
            addCriterion("activity_end_date <=", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIn(List<Date> values) {
            addCriterion("activity_end_date in", values, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotIn(List<Date> values) {
            addCriterion("activity_end_date not in", values, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateBetween(Date value1, Date value2) {
            addCriterion("activity_end_date between", value1, value2, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotBetween(Date value1, Date value2) {
            addCriterion("activity_end_date not between", value1, value2, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIsNull() {
            addCriterion("online_date is null");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIsNotNull() {
            addCriterion("online_date is not null");
            return (Criteria) this;
        }

        public Criteria andOnlineDateEqualTo(Date value) {
            addCriterion("online_date =", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotEqualTo(Date value) {
            addCriterion("online_date <>", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateGreaterThan(Date value) {
            addCriterion("online_date >", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateGreaterThanOrEqualTo(Date value) {
            addCriterion("online_date >=", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateLessThan(Date value) {
            addCriterion("online_date <", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateLessThanOrEqualTo(Date value) {
            addCriterion("online_date <=", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIn(List<Date> values) {
            addCriterion("online_date in", values, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotIn(List<Date> values) {
            addCriterion("online_date not in", values, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateBetween(Date value1, Date value2) {
            addCriterion("online_date between", value1, value2, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotBetween(Date value1, Date value2) {
            addCriterion("online_date not between", value1, value2, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIsNull() {
            addCriterion("offline_date is null");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIsNotNull() {
            addCriterion("offline_date is not null");
            return (Criteria) this;
        }

        public Criteria andOfflineDateEqualTo(Date value) {
            addCriterion("offline_date =", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotEqualTo(Date value) {
            addCriterion("offline_date <>", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateGreaterThan(Date value) {
            addCriterion("offline_date >", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateGreaterThanOrEqualTo(Date value) {
            addCriterion("offline_date >=", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateLessThan(Date value) {
            addCriterion("offline_date <", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateLessThanOrEqualTo(Date value) {
            addCriterion("offline_date <=", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIn(List<Date> values) {
            addCriterion("offline_date in", values, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotIn(List<Date> values) {
            addCriterion("offline_date not in", values, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateBetween(Date value1, Date value2) {
            addCriterion("offline_date between", value1, value2, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotBetween(Date value1, Date value2) {
            addCriterion("offline_date not between", value1, value2, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNull() {
            addCriterion("operator_name is null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNotNull() {
            addCriterion("operator_name is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameEqualTo(String value) {
            addCriterion("operator_name =", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotEqualTo(String value) {
            addCriterion("operator_name <>", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThan(String value) {
            addCriterion("operator_name >", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThanOrEqualTo(String value) {
            addCriterion("operator_name >=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThan(String value) {
            addCriterion("operator_name <", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThanOrEqualTo(String value) {
            addCriterion("operator_name <=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLike(String value) {
            addCriterion("operator_name like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotLike(String value) {
            addCriterion("operator_name not like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIn(List<String> values) {
            addCriterion("operator_name in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotIn(List<String> values) {
            addCriterion("operator_name not in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameBetween(String value1, String value2) {
            addCriterion("operator_name between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotBetween(String value1, String value2) {
            addCriterion("operator_name not between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNull() {
            addCriterion("operator_id is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNotNull() {
            addCriterion("operator_id is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdEqualTo(String value) {
            addCriterion("operator_id =", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotEqualTo(String value) {
            addCriterion("operator_id <>", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThan(String value) {
            addCriterion("operator_id >", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("operator_id >=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThan(String value) {
            addCriterion("operator_id <", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThanOrEqualTo(String value) {
            addCriterion("operator_id <=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIn(List<String> values) {
            addCriterion("operator_id in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotIn(List<String> values) {
            addCriterion("operator_id not in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdBetween(String value1, String value2) {
            addCriterion("operator_id between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotBetween(String value1, String value2) {
            addCriterion("operator_id not between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNull() {
            addCriterion("pay_type is null");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNotNull() {
            addCriterion("pay_type is not null");
            return (Criteria) this;
        }

        public Criteria andPayTypeEqualTo(Byte value) {
            addCriterion("pay_type =", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotEqualTo(Byte value) {
            addCriterion("pay_type <>", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThan(Byte value) {
            addCriterion("pay_type >", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("pay_type >=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThan(Byte value) {
            addCriterion("pay_type <", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThanOrEqualTo(Byte value) {
            addCriterion("pay_type <=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeIn(List<Byte> values) {
            addCriterion("pay_type in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotIn(List<Byte> values) {
            addCriterion("pay_type not in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeBetween(Byte value1, Byte value2) {
            addCriterion("pay_type between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("pay_type not between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andShareTitleIsNull() {
            addCriterion("share_title is null");
            return (Criteria) this;
        }

        public Criteria andShareTitleIsNotNull() {
            addCriterion("share_title is not null");
            return (Criteria) this;
        }

        public Criteria andShareTitleEqualTo(String value) {
            addCriterion("share_title =", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotEqualTo(String value) {
            addCriterion("share_title <>", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleGreaterThan(String value) {
            addCriterion("share_title >", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleGreaterThanOrEqualTo(String value) {
            addCriterion("share_title >=", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLessThan(String value) {
            addCriterion("share_title <", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLessThanOrEqualTo(String value) {
            addCriterion("share_title <=", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLike(String value) {
            addCriterion("share_title like", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotLike(String value) {
            addCriterion("share_title not like", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleIn(List<String> values) {
            addCriterion("share_title in", values, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotIn(List<String> values) {
            addCriterion("share_title not in", values, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleBetween(String value1, String value2) {
            addCriterion("share_title between", value1, value2, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotBetween(String value1, String value2) {
            addCriterion("share_title not between", value1, value2, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareDescIsNull() {
            addCriterion("share_desc is null");
            return (Criteria) this;
        }

        public Criteria andShareDescIsNotNull() {
            addCriterion("share_desc is not null");
            return (Criteria) this;
        }

        public Criteria andShareDescEqualTo(String value) {
            addCriterion("share_desc =", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescNotEqualTo(String value) {
            addCriterion("share_desc <>", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescGreaterThan(String value) {
            addCriterion("share_desc >", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescGreaterThanOrEqualTo(String value) {
            addCriterion("share_desc >=", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescLessThan(String value) {
            addCriterion("share_desc <", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescLessThanOrEqualTo(String value) {
            addCriterion("share_desc <=", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescLike(String value) {
            addCriterion("share_desc like", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescNotLike(String value) {
            addCriterion("share_desc not like", value, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescIn(List<String> values) {
            addCriterion("share_desc in", values, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescNotIn(List<String> values) {
            addCriterion("share_desc not in", values, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescBetween(String value1, String value2) {
            addCriterion("share_desc between", value1, value2, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareDescNotBetween(String value1, String value2) {
            addCriterion("share_desc not between", value1, value2, "shareDesc");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlIsNull() {
            addCriterion("share_img_url is null");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlIsNotNull() {
            addCriterion("share_img_url is not null");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlEqualTo(String value) {
            addCriterion("share_img_url =", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlNotEqualTo(String value) {
            addCriterion("share_img_url <>", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlGreaterThan(String value) {
            addCriterion("share_img_url >", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("share_img_url >=", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlLessThan(String value) {
            addCriterion("share_img_url <", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlLessThanOrEqualTo(String value) {
            addCriterion("share_img_url <=", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlLike(String value) {
            addCriterion("share_img_url like", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlNotLike(String value) {
            addCriterion("share_img_url not like", value, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlIn(List<String> values) {
            addCriterion("share_img_url in", values, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlNotIn(List<String> values) {
            addCriterion("share_img_url not in", values, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlBetween(String value1, String value2) {
            addCriterion("share_img_url between", value1, value2, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareImgUrlNotBetween(String value1, String value2) {
            addCriterion("share_img_url not between", value1, value2, "shareImgUrl");
            return (Criteria) this;
        }

        public Criteria andShareTypeIsNull() {
            addCriterion("share_type is null");
            return (Criteria) this;
        }

        public Criteria andShareTypeIsNotNull() {
            addCriterion("share_type is not null");
            return (Criteria) this;
        }

        public Criteria andShareTypeEqualTo(String value) {
            addCriterion("share_type =", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotEqualTo(String value) {
            addCriterion("share_type <>", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeGreaterThan(String value) {
            addCriterion("share_type >", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeGreaterThanOrEqualTo(String value) {
            addCriterion("share_type >=", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLessThan(String value) {
            addCriterion("share_type <", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLessThanOrEqualTo(String value) {
            addCriterion("share_type <=", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLike(String value) {
            addCriterion("share_type like", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotLike(String value) {
            addCriterion("share_type not like", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeIn(List<String> values) {
            addCriterion("share_type in", values, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotIn(List<String> values) {
            addCriterion("share_type not in", values, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeBetween(String value1, String value2) {
            addCriterion("share_type between", value1, value2, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotBetween(String value1, String value2) {
            addCriterion("share_type not between", value1, value2, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareLinkIsNull() {
            addCriterion("share_link is null");
            return (Criteria) this;
        }

        public Criteria andShareLinkIsNotNull() {
            addCriterion("share_link is not null");
            return (Criteria) this;
        }

        public Criteria andShareLinkEqualTo(String value) {
            addCriterion("share_link =", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkNotEqualTo(String value) {
            addCriterion("share_link <>", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkGreaterThan(String value) {
            addCriterion("share_link >", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkGreaterThanOrEqualTo(String value) {
            addCriterion("share_link >=", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkLessThan(String value) {
            addCriterion("share_link <", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkLessThanOrEqualTo(String value) {
            addCriterion("share_link <=", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkLike(String value) {
            addCriterion("share_link like", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkNotLike(String value) {
            addCriterion("share_link not like", value, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkIn(List<String> values) {
            addCriterion("share_link in", values, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkNotIn(List<String> values) {
            addCriterion("share_link not in", values, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkBetween(String value1, String value2) {
            addCriterion("share_link between", value1, value2, "shareLink");
            return (Criteria) this;
        }

        public Criteria andShareLinkNotBetween(String value1, String value2) {
            addCriterion("share_link not between", value1, value2, "shareLink");
            return (Criteria) this;
        }
        public Criteria andProductStockTotalIsNull() {
            addCriterion("product_stock_total is null");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalIsNotNull() {
            addCriterion("product_stock_total is not null");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalEqualTo(Long value) {
            addCriterion("product_stock_total =", value, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalNotEqualTo(Long value) {
            addCriterion("product_stock_total <>", value, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalGreaterThan(Long value) {
            addCriterion("product_stock_total >", value, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalGreaterThanOrEqualTo(Long value) {
            addCriterion("product_stock_total >=", value, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalLessThan(Long value) {
            addCriterion("product_stock_total <", value, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalLessThanOrEqualTo(Long value) {
            addCriterion("product_stock_total <=", value, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalIn(List<Long> values) {
            addCriterion("product_stock_total in", values, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalNotIn(List<Long> values) {
            addCriterion("product_stock_total not in", values, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalBetween(Long value1, Long value2) {
            addCriterion("product_stock_total between", value1, value2, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockTotalNotBetween(Long value1, Long value2) {
            addCriterion("product_stock_total not between", value1, value2, "productStockTotal");
            return (Criteria) this;
        }

        public Criteria andProductStockIsNull() {
            addCriterion("product_stock is null");
            return (Criteria) this;
        }

        public Criteria andProductStockIsNotNull() {
            addCriterion("product_stock is not null");
            return (Criteria) this;
        }

        public Criteria andProductStockEqualTo(Long value) {
            addCriterion("product_stock =", value, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockNotEqualTo(Long value) {
            addCriterion("product_stock <>", value, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockGreaterThan(Long value) {
            addCriterion("product_stock >", value, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockGreaterThanOrEqualTo(Long value) {
            addCriterion("product_stock >=", value, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockLessThan(Long value) {
            addCriterion("product_stock <", value, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockLessThanOrEqualTo(Long value) {
            addCriterion("product_stock <=", value, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockIn(List<Long> values) {
            addCriterion("product_stock in", values, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockNotIn(List<Long> values) {
            addCriterion("product_stock not in", values, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockBetween(Long value1, Long value2) {
            addCriterion("product_stock between", value1, value2, "productStock");
            return (Criteria) this;
        }

        public Criteria andProductStockNotBetween(Long value1, Long value2) {
            addCriterion("product_stock not between", value1, value2, "productStock");
            return (Criteria) this;
        }

        public Criteria andModelIsNull() {
            addCriterion("model is null");
            return (Criteria) this;
        }

        public Criteria andModelIsNotNull() {
            addCriterion("model is not null");
            return (Criteria) this;
        }

        public Criteria andModelEqualTo(Byte value) {
            addCriterion("model =", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotEqualTo(Byte value) {
            addCriterion("model <>", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelGreaterThan(Byte value) {
            addCriterion("model >", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelGreaterThanOrEqualTo(Byte value) {
            addCriterion("model >=", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelLessThan(Byte value) {
            addCriterion("model <", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelLessThanOrEqualTo(Byte value) {
            addCriterion("model <=", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelIn(List<Byte> values) {
            addCriterion("model in", values, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotIn(List<Byte> values) {
            addCriterion("model not in", values, "model");
            return (Criteria) this;
        }

        public Criteria andModelBetween(Byte value1, Byte value2) {
            addCriterion("model between", value1, value2, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotBetween(Byte value1, Byte value2) {
            addCriterion("model not between", value1, value2, "model");
            return (Criteria) this;
        }

        public Criteria andTimeOutIsNull() {
            addCriterion("time_out is null");
            return (Criteria) this;
        }

        public Criteria andTimeOutIsNotNull() {
            addCriterion("time_out is not null");
            return (Criteria) this;
        }

        public Criteria andTimeOutEqualTo(Long value) {
            addCriterion("time_out =", value, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutNotEqualTo(Long value) {
            addCriterion("time_out <>", value, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutGreaterThan(Long value) {
            addCriterion("time_out >", value, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutGreaterThanOrEqualTo(Long value) {
            addCriterion("time_out >=", value, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutLessThan(Long value) {
            addCriterion("time_out <", value, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutLessThanOrEqualTo(Long value) {
            addCriterion("time_out <=", value, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutIn(List<Long> values) {
            addCriterion("time_out in", values, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutNotIn(List<Long> values) {
            addCriterion("time_out not in", values, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutBetween(Long value1, Long value2) {
            addCriterion("time_out between", value1, value2, "timeOut");
            return (Criteria) this;
        }

        public Criteria andTimeOutNotBetween(Long value1, Long value2) {
            addCriterion("time_out not between", value1, value2, "timeOut");
            return (Criteria) this;
        }

        public Criteria andImgsIsNull() {
            addCriterion("imgs is null");
            return (Criteria) this;
        }

        public Criteria andImgsIsNotNull() {
            addCriterion("imgs is not null");
            return (Criteria) this;
        }

        public Criteria andImgsEqualTo(String value) {
            addCriterion("imgs =", value, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsNotEqualTo(String value) {
            addCriterion("imgs <>", value, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsGreaterThan(String value) {
            addCriterion("imgs >", value, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsGreaterThanOrEqualTo(String value) {
            addCriterion("imgs >=", value, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsLessThan(String value) {
            addCriterion("imgs <", value, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsLessThanOrEqualTo(String value) {
            addCriterion("imgs <=", value, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsLike(String value) {
            addCriterion("imgs like", value, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsNotLike(String value) {
            addCriterion("imgs not like", value, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsIn(List<String> values) {
            addCriterion("imgs in", values, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsNotIn(List<String> values) {
            addCriterion("imgs not in", values, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsBetween(String value1, String value2) {
            addCriterion("imgs between", value1, value2, "imgs");
            return (Criteria) this;
        }

        public Criteria andImgsNotBetween(String value1, String value2) {
            addCriterion("imgs not between", value1, value2, "imgs");
            return (Criteria) this;
        }

        public Criteria andActivityNameIsNull() {
            addCriterion("activity_name is null");
            return (Criteria) this;
        }

        public Criteria andActivityNameIsNotNull() {
            addCriterion("activity_name is not null");
            return (Criteria) this;
        }

        public Criteria andActivityNameEqualTo(String value) {
            addCriterion("activity_name =", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotEqualTo(String value) {
            addCriterion("activity_name <>", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameGreaterThan(String value) {
            addCriterion("activity_name >", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameGreaterThanOrEqualTo(String value) {
            addCriterion("activity_name >=", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLessThan(String value) {
            addCriterion("activity_name <", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLessThanOrEqualTo(String value) {
            addCriterion("activity_name <=", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLike(String value) {
            addCriterion("activity_name like", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotLike(String value) {
            addCriterion("activity_name not like", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameIn(List<String> values) {
            addCriterion("activity_name in", values, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotIn(List<String> values) {
            addCriterion("activity_name not in", values, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameBetween(String value1, String value2) {
            addCriterion("activity_name between", value1, value2, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotBetween(String value1, String value2) {
            addCriterion("activity_name not between", value1, value2, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityInfoIsNull() {
            addCriterion("activity_info is null");
            return (Criteria) this;
        }

        public Criteria andActivityInfoIsNotNull() {
            addCriterion("activity_info is not null");
            return (Criteria) this;
        }

        public Criteria andActivityInfoEqualTo(String value) {
            addCriterion("activity_info =", value, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoNotEqualTo(String value) {
            addCriterion("activity_info <>", value, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoGreaterThan(String value) {
            addCriterion("activity_info >", value, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoGreaterThanOrEqualTo(String value) {
            addCriterion("activity_info >=", value, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoLessThan(String value) {
            addCriterion("activity_info <", value, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoLessThanOrEqualTo(String value) {
            addCriterion("activity_info <=", value, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoLike(String value) {
            addCriterion("activity_info like", value, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoNotLike(String value) {
            addCriterion("activity_info not like", value, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoIn(List<String> values) {
            addCriterion("activity_info in", values, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoNotIn(List<String> values) {
            addCriterion("activity_info not in", values, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoBetween(String value1, String value2) {
            addCriterion("activity_info between", value1, value2, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityInfoNotBetween(String value1, String value2) {
            addCriterion("activity_info not between", value1, value2, "activityInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoIsNull() {
            addCriterion("activity_ext_info is null");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoIsNotNull() {
            addCriterion("activity_ext_info is not null");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoEqualTo(String value) {
            addCriterion("activity_ext_info =", value, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoNotEqualTo(String value) {
            addCriterion("activity_ext_info <>", value, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoGreaterThan(String value) {
            addCriterion("activity_ext_info >", value, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoGreaterThanOrEqualTo(String value) {
            addCriterion("activity_ext_info >=", value, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoLessThan(String value) {
            addCriterion("activity_ext_info <", value, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoLessThanOrEqualTo(String value) {
            addCriterion("activity_ext_info <=", value, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoLike(String value) {
            addCriterion("activity_ext_info like", value, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoNotLike(String value) {
            addCriterion("activity_ext_info not like", value, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoIn(List<String> values) {
            addCriterion("activity_ext_info in", values, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoNotIn(List<String> values) {
            addCriterion("activity_ext_info not in", values, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoBetween(String value1, String value2) {
            addCriterion("activity_ext_info between", value1, value2, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityExtInfoNotBetween(String value1, String value2) {
            addCriterion("activity_ext_info not between", value1, value2, "activityExtInfo");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionIsNull() {
            addCriterion("activity_criterion is null");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionIsNotNull() {
            addCriterion("activity_criterion is not null");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionEqualTo(Long value) {
            addCriterion("activity_criterion =", value, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionNotEqualTo(Long value) {
            addCriterion("activity_criterion <>", value, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionGreaterThan(Long value) {
            addCriterion("activity_criterion >", value, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionGreaterThanOrEqualTo(Long value) {
            addCriterion("activity_criterion >=", value, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionLessThan(Long value) {
            addCriterion("activity_criterion <", value, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionLessThanOrEqualTo(Long value) {
            addCriterion("activity_criterion <=", value, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionIn(List<Long> values) {
            addCriterion("activity_criterion in", values, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionNotIn(List<Long> values) {
            addCriterion("activity_criterion not in", values, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionBetween(Long value1, Long value2) {
            addCriterion("activity_criterion between", value1, value2, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andActivityCriterionNotBetween(Long value1, Long value2) {
            addCriterion("activity_criterion not between", value1, value2, "activityCriterion");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIsNull() {
            addCriterion("price_ids is null");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIsNotNull() {
            addCriterion("price_ids is not null");
            return (Criteria) this;
        }

        public Criteria andPriceIdsEqualTo(String value) {
            addCriterion("price_ids =", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotEqualTo(String value) {
            addCriterion("price_ids <>", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsGreaterThan(String value) {
            addCriterion("price_ids >", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsGreaterThanOrEqualTo(String value) {
            addCriterion("price_ids >=", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLessThan(String value) {
            addCriterion("price_ids <", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLessThanOrEqualTo(String value) {
            addCriterion("price_ids <=", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLike(String value) {
            addCriterion("price_ids like", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotLike(String value) {
            addCriterion("price_ids not like", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIn(List<String> values) {
            addCriterion("price_ids in", values, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotIn(List<String> values) {
            addCriterion("price_ids not in", values, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsBetween(String value1, String value2) {
            addCriterion("price_ids between", value1, value2, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotBetween(String value1, String value2) {
            addCriterion("price_ids not between", value1, value2, "priceIds");
            return (Criteria) this;
        }

    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}