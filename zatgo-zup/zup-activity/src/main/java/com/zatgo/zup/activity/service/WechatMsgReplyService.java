package com.zatgo.zup.activity.service;

import com.zatgo.zup.activity.entity.wx.WechatMsg.Event;
import com.zatgo.zup.activity.entity.wx.WechatMsg.MsgType;

public interface WechatMsgReplyService {

	String selectWechatMsgReply(MsgType msgType, Event event, String key);

}
