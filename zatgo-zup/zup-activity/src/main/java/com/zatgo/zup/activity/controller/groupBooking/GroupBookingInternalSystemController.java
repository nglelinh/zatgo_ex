package com.zatgo.zup.activity.controller.groupBooking;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.activity.controller.BaseController;
import com.zatgo.zup.activity.entity.GroupBookingActivity;
import com.zatgo.zup.activity.entity.GroupOrder;
import com.zatgo.zup.activity.entity.request.CreateGroupBookingRequest;
import com.zatgo.zup.activity.entity.request.JoinGroupBookingRequest;
import com.zatgo.zup.activity.entity.response.GroupDetailInfoResponse;
import com.zatgo.zup.activity.service.CommonService;
import com.zatgo.zup.activity.service.GroupBookingInternalSystemService;
import com.zatgo.zup.activity.service.GroupBookingUserService;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/3/15.
 */


@Api(value = "/activity/groupBooking/internal",description = "拼团内部接口")
@RestController
@RequestMapping(value = "/activity/groupBooking/internal")
public class GroupBookingInternalSystemController extends BaseController {


    @Autowired
    private GroupBookingInternalSystemService groupBookingInternalSystemService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private CommonService commonService;
    @Autowired
    private GroupBookingUserService groupBookingUserService;

    @Value("${groupBooking.url}")
    private String url;


    @RequestMapping(value = "/orderCallBack",method = RequestMethod.POST)
    @ApiOperation(value = "下单号回调", notes = "下单号回调", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData<Boolean> addOrder(@RequestParam("activityId") String activityId,
                                          @RequestParam("userId") String userId,
                                          @RequestParam("orderId") String orderId,
                                          @RequestParam("isSuccess") Boolean isSuccess) {
        groupBookingInternalSystemService.saveOrderRecord(activityId, userId, orderId, isSuccess);
        return BusinessResponseFactory.createSuccess(null);
    }

    @PostMapping(value = "/createGroupBooking")
    @ApiOperation(value = "创建拼团订单", notes = "创建拼团订单")
    @ResponseBody
    public ResponseData createGroupBooking(@RequestBody CreateGroupBookingRequest request) {
        ThirdLoginInfo userInfo = getUserInfo();
        String key = RedisKeyConstants.CREATE_GROUP_BOOKING_ORDER_LOCK + userInfo.getUserId();
        GroupOrder groupBooking = null;
        redisLockUtils.lock(key);
        try {
            groupBooking = groupBookingInternalSystemService.createGroupBooking(request, url, userInfo);
        }finally {
            redisLockUtils.releaseLock(key);
        }
        return BusinessResponseFactory.createSuccess(groupBooking);
    }

    @PostMapping(value = "/joinGroupBooking")
    @ApiOperation(value = "用户参加拼团", notes = "用户参加拼团")
    @ResponseBody
    public ResponseData joinGroupBooking(@RequestBody JoinGroupBookingRequest request) {
        ThirdLoginInfo userInfo = getUserInfo();
        request.setUserId(userInfo.getUserId());
        String groupOrderId = request.getGroupOrderId();
        String key = RedisKeyConstants.JOIN_GROUP_BOOKING_LOCK + groupOrderId;
        redisLockUtils.lock(key);
        try {
            groupBookingInternalSystemService.joinGroupBooking(request, userInfo);
        }finally {
            redisLockUtils.releaseLock(key);
        }
        return BusinessResponseFactory.createSuccess(null);
    }

    @PostMapping(value = "/updateGroupStatus")
    @ApiOperation(value = "退款完成之后修改状态", notes = "退款完成之后修改状态")
    @ResponseBody
    public void updateGroupStatus(@RequestParam("activityId") String activityId, @RequestParam("orderId") Long orderId) {
        String key = RedisKeyConstants.BARGAIN_CANCEL_ORDER_LOCK + orderId;
        redisLockUtils.lock(key);
        try {
            groupBookingInternalSystemService.updateGroupStatus(activityId, orderId);
        }finally {
            redisLockUtils.releaseLock(key);
        }
    }

    @RequestMapping(value = "/saveOrder",method = RequestMethod.POST)
    @ApiOperation(value = "保存订单", notes = "保存订单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData<String> saveOrder(@RequestParam(value = "groupOrderId", required = false) String groupOrderId,
                                           @RequestParam("userId") String userId,
                                           @RequestParam("orderId") String orderId,
                                           @RequestParam("type") String type,
                                           @RequestParam("activityId") String activityId) {
        try {
            String id = groupBookingInternalSystemService.saveOrder(groupOrderId, userId, orderId, type, activityId);
            return BusinessResponseFactory.createSuccess(id);
        } catch (BusinessException e){
            return BusinessResponseFactory.createBusinessError(e.getCode(), e.getMessage());
        }
    }


    @RequestMapping(value = "/cancelOrder",method = RequestMethod.POST)
    @ApiOperation(value = "取消订单接口", notes = "取消订单接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData<Boolean> cancelOrder(@RequestParam("GroupUserOrderId") String GroupUserOrderId) {
        groupBookingInternalSystemService.cancelOrder(GroupUserOrderId);
        return BusinessResponseFactory.createSuccess(null);
    }

    @RequestMapping(value = "/orderInfo",method = RequestMethod.POST)
    @ApiOperation(value = "查询订单信息", notes = "查询订单信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData getOrderInfo(@RequestParam("GroupOrderId") String GroupOrderId) {
        GroupDetailInfoResponse activityDetailInfo = groupBookingUserService.getActivityDetailInfo(GroupOrderId);
        return BusinessResponseFactory.createSuccess(activityDetailInfo);
    }

    @RequestMapping(value = "/getActivityOrderPrice",method = RequestMethod.POST)
    @ApiOperation(value = "获取活动价格", notes = "获取活动价格", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData<BigDecimal> getActivityOrderPrice(@RequestParam(value = "groupOrderId", required = false) String groupOrderId,
                                                          @RequestParam("userId") String userId,
                                                          @RequestParam("activityId") String activityId) {
        BigDecimal price;
        try {
            price = groupBookingInternalSystemService.getActivityOrderPrice(activityId, groupOrderId, userId);
            return BusinessResponseFactory.createSuccess(price);
        } catch (BusinessException e){
            return BusinessResponseFactory.createBusinessError(e.getCode(), e.getMessage());
        }
    }


    @GetMapping(value = "/updateRefundStatus")
    @ResponseBody
    public void updateRefundStatus(@RequestParam("activityId") String activityId){
        groupBookingInternalSystemService.updateRefundStatus(activityId);
    }

    @GetMapping(value = "/selectById/{activityId}")
    @ResponseBody
    public ResponseData<GroupBookingActivity> selectById(@PathVariable("activityId") String activityId){
        GroupBookingActivity gba = groupBookingInternalSystemService.selectById(activityId);
        return BusinessResponseFactory.createSuccess(gba);
    }
}
