package com.zatgo.zup.activity.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.model.PmsProduct;
import com.zatgo.zup.activity.entity.*;
import com.zatgo.zup.activity.entity.request.AdminActivityListRequest;
import com.zatgo.zup.activity.entity.request.GroupOrderDetailListInfoRequest;
import com.zatgo.zup.activity.entity.request.GroupOrderMergeRequest;
import com.zatgo.zup.activity.entity.response.ActivityGroupBookingInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupInfoResponse;
import com.zatgo.zup.activity.mapper.GroupBookingActivityMapper;
import com.zatgo.zup.activity.mapper.GroupOrderMapper;
import com.zatgo.zup.activity.mapper.GroupUserOrderMapper;
import com.zatgo.zup.activity.model.AddGroupBookingActivityParams;
import com.zatgo.zup.activity.model.UpdateGroupBookingActivityParams;
import com.zatgo.zup.activity.remoteservice.TradeApiRemoteService;
import com.zatgo.zup.activity.service.GroupBookingAdminService;
import com.zatgo.zup.common.encrypt.MD5Util;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2019/3/15.
 */

@Service("groupBookingAdminService")
public class GroupBookingAdminServiceImpl implements GroupBookingAdminService {

    @Autowired
    private GroupBookingActivityMapper groupBookingActivityMapper;
    @Autowired
    private GroupOrderMapper groupOrderMapper;
    @Autowired
    private GroupUserOrderMapper groupUserOrderMapper;
    @Autowired
    private TradeApiRemoteService tradeApiRemoteService;

    @Override
    public void addGroupBooking(AddGroupBookingActivityParams params, ThirdLoginInfo info) {
        String cloudUserId = info.getCloudUserId();
        Date activityEndDate = params.getActivityEndDate();
        Date activityStartDate = params.getActivityStartDate();
        BigDecimal activityPrice = params.getActivityPrice();
        Long buildGroupLimitHour = params.getBuildGroupLimitHour();
        Long buildGroupPeopleNum = params.getBuildGroupPeopleNum();
        Boolean outOfLimit = params.getOutOfLimit();
        String productId = params.getProductId();
        Long perLimitBookingNum = params.getPerLimitBookingNum();
        String shareDesc = params.getShareDesc();
        String shareImgUrl = params.getShareImgUrl();
        String shareType = params.getShareType();
        String shareTitle = params.getShareTitle();
        String shareLink = params.getShareLink();
        String activityImg = params.getActivityImg();
        String productName = params.getProductName();
        List<Long> priceIds = params.getPriceIds();
        Long payTime = params.getPayTime();

        ResponseData<PmsProduct> productInfo = tradeApiRemoteService.getProductInfo(Long.valueOf(productId));
        if (productInfo == null || !productInfo.isSuccessful()){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_PRODUCT_NOT_EXIST);
        }
        PmsProduct data = productInfo.getData();
        Date date = new Date();

        GroupBookingActivity groupBookingActivity = new GroupBookingActivity();
        groupBookingActivity.setGroupActivityId(UUIDUtils.getUuid());
        groupBookingActivity.setCloudUserId(cloudUserId);
        groupBookingActivity.setProductId(productId);
        groupBookingActivity.setActivityPrice(activityPrice);
        groupBookingActivity.setOriginalPrice(data.getOriginalPrice());
        groupBookingActivity.setPromotionPrice(data.getPromotionPrice());
        groupBookingActivity.setProductImgUrl(data.getPic());
        groupBookingActivity.setIsOutOfLimit(outOfLimit);
        groupBookingActivity.setActivityStartDate(activityStartDate);
        groupBookingActivity.setActivityEndDate(activityEndDate);
        groupBookingActivity.setBuildGroupLimitHour(buildGroupLimitHour);
        groupBookingActivity.setBuildGroupPeopleNum(buildGroupPeopleNum);
        groupBookingActivity.setPerLimitBookingNum(perLimitBookingNum);
        groupBookingActivity.setActivityStatus(BusinessEnum.GroupBookingStatus.Input.getCode());
        groupBookingActivity.setOperatorName(info.getUserName());
        groupBookingActivity.setOperateDate(date);
        groupBookingActivity.setOperatorId(info.getUserId());
        groupBookingActivity.setCreateTime(date);
        groupBookingActivity.setShareDesc(shareDesc);
        groupBookingActivity.setShareImgUrl(shareImgUrl);
        groupBookingActivity.setShareLink(shareLink);
        groupBookingActivity.setShareTitle(shareTitle);
        groupBookingActivity.setShareType(shareType);
        groupBookingActivity.setActivityImg(activityImg);
        groupBookingActivity.setProductName(productName);
        groupBookingActivity.setPayTime(payTime);
        if (!CollectionUtils.isEmpty(priceIds)){
            groupBookingActivity.setPriceIds(JSONArray.toJSONString(priceIds));
        }
        groupBookingActivityMapper.insertSelective(groupBookingActivity);

    }

    @Override
    public void updateGroupBooking(UpdateGroupBookingActivityParams params, ThirdLoginInfo info) {
        Date activityEndDate = params.getActivityEndDate();
        Date activityStartDate = params.getActivityStartDate();
        BigDecimal activityPrice = params.getActivityPrice();
        Long buildGroupLimitHour = params.getBuildGroupLimitHour();
        Long buildGroupPeopleNum = params.getBuildGroupPeopleNum();
        Boolean outOfLimit = params.getOutOfLimit();
        String productId = params.getProductId();
        Long perLimitBookingNum = params.getPerLimitBookingNum();
        String shareDesc = params.getShareDesc();
        String shareImgUrl = params.getShareImgUrl();
        String shareType = params.getShareType();
        String shareTitle = params.getShareTitle();
        String shareLink = params.getShareLink();
        String activityImg = params.getActivityImg();
        Long payTime = params.getPayTime();
        List<Long> priceIds = params.getPriceIds();

        ResponseData<PmsProduct> productInfo = tradeApiRemoteService.getProductInfo(Long.valueOf(productId));
        if (productInfo == null || !productInfo.isSuccessful()){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_PRODUCT_NOT_EXIST);
        }
        PmsProduct data = productInfo.getData();
        Date date = new Date();

        GroupBookingActivity groupBookingActivity = new GroupBookingActivity();
        groupBookingActivity.setGroupActivityId(params.getGroupActivityId());
        groupBookingActivity.setProductId(productId);
        groupBookingActivity.setProductName(data.getName());
        groupBookingActivity.setActivityPrice(activityPrice);
        groupBookingActivity.setOriginalPrice(data.getOriginalPrice());
        groupBookingActivity.setPromotionPrice(data.getPromotionPrice());
        groupBookingActivity.setProductImgUrl(data.getPic());
        groupBookingActivity.setIsOutOfLimit(outOfLimit);
        groupBookingActivity.setActivityStartDate(activityStartDate);
        groupBookingActivity.setActivityEndDate(activityEndDate);
        groupBookingActivity.setBuildGroupLimitHour(buildGroupLimitHour);
        groupBookingActivity.setBuildGroupPeopleNum(buildGroupPeopleNum);
        groupBookingActivity.setPerLimitBookingNum(perLimitBookingNum);
        groupBookingActivity.setOperatorName(info.getUserName());
        groupBookingActivity.setOperateDate(date);
        groupBookingActivity.setOperatorId(info.getUserId());
        groupBookingActivity.setShareDesc(shareDesc);
        groupBookingActivity.setShareImgUrl(shareImgUrl);
        groupBookingActivity.setShareLink(shareLink);
        groupBookingActivity.setShareTitle(shareTitle);
        groupBookingActivity.setShareType(shareType);
        groupBookingActivity.setActivityImg(activityImg);
        groupBookingActivity.setPayTime(payTime);
        if (!CollectionUtils.isEmpty(priceIds)){
            groupBookingActivity.setPriceIds(JSONArray.toJSONString(priceIds));
        }
        groupBookingActivityMapper.updateByPrimaryKeySelective(groupBookingActivity);
    }

    @Override
    public void onlineActivity(String groupActivityId, ThirdLoginInfo info) {
        GroupBookingActivity groupBookingActivity = new GroupBookingActivity();
        groupBookingActivity.setGroupActivityId(groupActivityId);
        groupBookingActivity.setOperatorId(info.getUserId());
        groupBookingActivity.setOperatorName(info.getUserName());
        groupBookingActivity.setOperateDate(new Date());
        groupBookingActivity.setActivityStatus(BusinessEnum.GroupBookingStatus.Online.getCode());
        groupBookingActivityMapper.updateByPrimaryKeySelective(groupBookingActivity);
    }

    @Override
    public void offlineActivity(String groupActivityId, ThirdLoginInfo info) {
        GroupBookingActivity groupBookingActivity = new GroupBookingActivity();
        groupBookingActivity.setGroupActivityId(groupActivityId);
        groupBookingActivity.setOperatorId(info.getUserId());
        groupBookingActivity.setOperatorName(info.getUserName());
        groupBookingActivity.setOperateDate(new Date());
        groupBookingActivity.setActivityStatus(BusinessEnum.GroupBookingStatus.Offline.getCode());
        groupBookingActivityMapper.updateByPrimaryKeySelective(groupBookingActivity);
    }

    @Override
    public PageInfo<GroupBookingActivity> activityList(ThirdLoginInfo user, AdminActivityListRequest request) {
        String activityName = request.getActivityName();
        Date endDate = request.getEndDate();
        String status = request.getStatus();
        Date startDate = request.getStartDate();
        String cloudUserId = user.getCloudUserId();
        GroupBookingActivityExample example = new GroupBookingActivityExample();
        GroupBookingActivityExample.Criteria criteria = example.createCriteria().andCloudUserIdEqualTo(cloudUserId);
        if (!StringUtils.isEmpty(activityName)){
            criteria.andProductNameLike("%" + activityName + "%");
        }
        if (!StringUtils.isEmpty(status)){
            criteria.andActivityStatusEqualTo(new Byte(status));
        }
        if (startDate != null){
            criteria.andActivityStartDateGreaterThanOrEqualTo(startDate);
        }
        if (endDate != null){
            criteria.andActivityEndDateLessThan(endDate);
        }
        example.setOrderByClause("activity_start_date asc");
        PageHelper.startPage(request.getPageNo(), request.getPageSize());
        List<GroupBookingActivity> groupBookingActivities = groupBookingActivityMapper.selectByExample(example);
        return new PageInfo<>(groupBookingActivities);
    }

    @Override
    public void updateActivityOrder(GroupOrder groupOrder) {
        groupOrder.setGroupActivityId(null);
        groupOrderMapper.updateByPrimaryKeySelective(groupOrder);
    }

    @Override
    public PageInfo<GroupInfoResponse> activityDetailList(String activityId, Integer pageNo, Integer pageSize, Integer status) {
        PageHelper.startPage(pageNo, pageSize);
        List<GroupInfoResponse> adminGroupInfo = groupBookingActivityMapper.getAdminGroupInfo(activityId, status, new Date());
        return new PageInfo<>(adminGroupInfo);
    }

    @Override
    public PageInfo<GroupUserOrder> activityDetailListInfo(GroupOrderDetailListInfoRequest request) {
        Integer pageNo = request.getPageNo();
        Integer pageSize = request.getPageSize();
        String orderId = request.getOrderId();
        String status = request.getStatus();
        String keyword = request.getKeyword();
        PageHelper.startPage(pageNo, pageSize);
        GroupUserOrderExample example = new GroupUserOrderExample();
        GroupUserOrderExample.Criteria criteria = example.createCriteria().andGroupOrderIdEqualTo(orderId);
        if (!"-1".equals(status)){
            criteria.andStatusEqualTo(status);
        }
        if (!StringUtils.isEmpty(keyword)){
            keyword = "%" + keyword + "%";
            criteria.andExtInfoLike(keyword);
        }
        List<GroupUserOrder> groupUserOrders = groupUserOrderMapper.selectByExample(example);
        return new PageInfo<>(groupUserOrders);
    }

    @Override
    public ActivityGroupBookingInfoResponse activityDetailInfo(String activityId) {
        ActivityGroupBookingInfoResponse response = new ActivityGroupBookingInfoResponse();
        GroupBookingActivity activity = groupBookingActivityMapper.selectByPrimaryKey(activityId);
        response.setActivity(activity);
        ResponseData<PmsProduct> productInfo = tradeApiRemoteService.getProductInfo(Long.valueOf(activity.getProductId()));
        if (productInfo != null && productInfo.isSuccessful()){
            response.setPmsProduct(productInfo.getData());
        }
        return response;
    }

    @Override
    public void autoRefund(String groupOrderId, String groupOrderUserId) {
        List<String> groupUserOrderList = groupBookingActivityMapper.needAutoRefund(groupOrderId, groupOrderUserId);
        if (!CollectionUtils.isEmpty(groupUserOrderList)){
            tradeApiRemoteService.autoRefund(JSONArray.toJSONString(groupUserOrderList));
        }
    }

    @Override
    @Transactional
    public void merge(GroupOrderMergeRequest request) {
        String groupSourceLeftId = request.getGroupSourceLeftId();
        String groupSOurceRightId = request.getGroupSOurceRightId();
        List<String> targetList = request.getTargetList();
        if (targetList != null){
            GroupUserOrderExample leftExample = new GroupUserOrderExample();
            leftExample.createCriteria().andGroupOrderIdEqualTo(groupSourceLeftId);
            List<GroupUserOrder> left = groupUserOrderMapper.selectByExample(leftExample);
            GroupUserOrderExample rightExample = new GroupUserOrderExample();
            rightExample.createCriteria().andGroupOrderIdEqualTo(groupSOurceRightId);
            List<GroupUserOrder> right = groupUserOrderMapper.selectByExample(rightExample);
            Map<String, GroupUserOrder> map = new HashMap<>();
            for (GroupUserOrder groupUserOrder : left) {
                map.put(groupUserOrder.getGroupUserOrderId(), groupUserOrder);
            }
            for (GroupUserOrder groupUserOrder : right) {
                map.put(groupUserOrder.getGroupUserOrderId(), groupUserOrder);
            }
            GroupUserOrder groupUserOrder = right.get(0);
            GroupOrder go = groupOrderMapper.selectByPrimaryKey(groupUserOrder.getGroupOrderId());
            //创建订单
            Date date = new Date();
            GroupOrder groupOrder = new GroupOrder();
            groupOrder.setIsValid(true);
            groupOrder.setGroupOrderId(UUIDUtils.getUuid());
            groupOrder.setGroupActivityId(go.getGroupActivityId());
            groupOrder.setGroupOrderStatus(BusinessEnum.GroupBookingOrderStatus.GroupBookingSuccess.getCode());
            groupOrder.setGroupStartDate(date);
            groupOrder.setGroupEndDate(date);
            groupOrder.setActivityUrl(null);
            groupOrder.setIsPrivate(true);
            groupOrder.setUpdateDate(date);
            groupOrderMapper.insertSelective(groupOrder);


            for (String str : targetList) {
                GroupUserOrder guo = map.get(str);
                remove(guo, left);
                remove(guo, right);
                guo.setGroupOrderId(groupOrder.getGroupOrderId());
                groupUserOrderMapper.updateByPrimaryKeySelective(guo);
            }

            if (CollectionUtils.isEmpty(left)){
                GroupOrder gro = new GroupOrder();
                gro.setGroupOrderId(groupSourceLeftId);
                gro.setUpdateDate(date);
                gro.setGroupOrderStatus(BusinessEnum.GroupBookingOrderStatus.Cancel.getCode());
                groupOrderMapper.updateByPrimaryKeySelective(gro);
            }

            if (CollectionUtils.isEmpty(right)){
                GroupOrder gro = new GroupOrder();
                gro.setGroupOrderId(groupSOurceRightId);
                gro.setGroupOrderStatus(BusinessEnum.GroupBookingOrderStatus.Cancel.getCode());
                gro.setUpdateDate(date);
                groupOrderMapper.updateByPrimaryKeySelective(gro);
            }
        }
    }

    private void remove(GroupUserOrder guo, List<GroupUserOrder> list){
        if (!CollectionUtils.isEmpty(list)){
            for (int i = 0; i < list.size(); i++){
                GroupUserOrder groupUserOrder = list.get(i);
                String groupUserOrderId = groupUserOrder.getGroupUserOrderId();
                if (groupUserOrderId.equals(guo.getGroupUserOrderId())){
                    list.remove(i);
                    return;
                }
            }
        }
    }


}
