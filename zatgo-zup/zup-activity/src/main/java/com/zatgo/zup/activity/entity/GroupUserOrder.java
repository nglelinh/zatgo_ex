package com.zatgo.zup.activity.entity;

import java.util.Date;

public class GroupUserOrder {
    private String groupUserOrderId;

    private String userId;

    private String groupOrderId;

    private Date createOrderDate;

    private Date payOrderDate;

    private String status;

    private Date payDeadline;

    private String nickname;

    private String icon;

    private String extInfo;

    private Date updateDate;

    public String getGroupUserOrderId() {
        return groupUserOrderId;
    }

    public void setGroupUserOrderId(String groupUserOrderId) {
        this.groupUserOrderId = groupUserOrderId == null ? null : groupUserOrderId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getGroupOrderId() {
        return groupOrderId;
    }

    public void setGroupOrderId(String groupOrderId) {
        this.groupOrderId = groupOrderId == null ? null : groupOrderId.trim();
    }

    public Date getCreateOrderDate() {
        return createOrderDate;
    }

    public void setCreateOrderDate(Date createOrderDate) {
        this.createOrderDate = createOrderDate;
    }

    public Date getPayOrderDate() {
        return payOrderDate;
    }

    public void setPayOrderDate(Date payOrderDate) {
        this.payOrderDate = payOrderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public Date getPayDeadline() {
        return payDeadline;
    }

    public void setPayDeadline(Date payDeadline) {
        this.payDeadline = payDeadline;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}