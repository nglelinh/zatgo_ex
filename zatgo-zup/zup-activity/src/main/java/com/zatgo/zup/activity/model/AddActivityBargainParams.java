package com.zatgo.zup.activity.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class AddActivityBargainParams{

	@ApiModelProperty(value = "商品ID",required = true)
	private String productId;
	@ApiModelProperty(value = "下单url",required = true)
	private String addOrderUrl;
	@ApiModelProperty(value = "活动价",required = true)
	private BigDecimal activityMoney;
	@ApiModelProperty(value = "起砍价",required = true)
	private BigDecimal activityStartMoney;
	@ApiModelProperty(value = "是否解除人数限制",required = true)
	private Boolean isRemoveLimit;
	@ApiModelProperty(value = "超过参与人数之后，每次砍价的百分比（解除人数限制后使用）",required = true)
	private BigDecimal bargainRate;
	@ApiModelProperty(value = "参与人数",required = true)
	private Integer joinCount;
	@ApiModelProperty(value = "商品描述",required = true)
	private String productDesc;
	@ApiModelProperty(value = "活动开始时间",required = true)
	private Date activityStartDate;
	@ApiModelProperty(value = "活动结束时间",required = true)
	private Date activityEndDate;
	@ApiModelProperty(value = "支付方式  0 先砍后下单  1先下单再砍",required = true)
	private Byte payType;
	@ApiModelProperty(value = "分享的标题",required = true)
	private String shareTitle;
	@ApiModelProperty(value = "分享的描述",required = true)
	private String shareDesc;
	@ApiModelProperty(value = "分享的图片链接",required = true)
	private String shareImgUrl;
	@ApiModelProperty(value = "分享的类型",required = true)
	private String shareType;
	@ApiModelProperty(value = "分享的跳转链接",required = true)
	private String shareLink;
	@ApiModelProperty(value = "商品总库存",required = true)
	private Long productStockTotal;
	@ApiModelProperty(value = "商品当前库存",required = true)
	private Long productStock;
	@ApiModelProperty(value = "模式 0自动 1手动",required = true)
	private Byte model;
	@ApiModelProperty(value = "超时时间（分）",required = true)
	private Long timeOut;
	@ApiModelProperty(value = "活动图片",required = true)
	private String imgs;
	@ApiModelProperty(value = "活动名称",required = true)
	private String activityName;
	@ApiModelProperty(value = "活动介绍",required = true)
	private String activityInfo;
	@ApiModelProperty(value = "扩展属性",required = true)
	private String activityExtInfo;
	@ApiModelProperty(value = "活动标准",required = true)
	private Long activityCriterion;
	@ApiModelProperty(value = "微信群二维码",required = true)
	private String wxGroupQrUrl;
	@ApiModelProperty(value = "订单price_ids",required = true)
	private List<Long> priceIds;



	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public BigDecimal getActivityMoney() {
		return activityMoney;
	}

	public void setActivityMoney(BigDecimal activityMoney) {
		this.activityMoney = activityMoney;
	}

	public BigDecimal getActivityStartMoney() {
		return activityStartMoney;
	}

	public void setActivityStartMoney(BigDecimal activityStartMoney) {
		this.activityStartMoney = activityStartMoney;
	}

	public Integer getJoinCount() {
		return joinCount;
	}

	public void setJoinCount(Integer joinCount) {
		this.joinCount = joinCount;
	}

	public Date getActivityStartDate() {
		return activityStartDate;
	}

	public void setActivityStartDate(Date activityStartDate) {
		this.activityStartDate = activityStartDate;
	}

	public Date getActivityEndDate() {
		return activityEndDate;
	}

	public void setActivityEndDate(Date activityEndDate) {
		this.activityEndDate = activityEndDate;
	}

	public String getAddOrderUrl() {
		return addOrderUrl;
	}

	public void setAddOrderUrl(String addOrderUrl) {
		this.addOrderUrl = addOrderUrl;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public Boolean getRemoveLimit() {
		return isRemoveLimit;
	}

	public void setRemoveLimit(Boolean removeLimit) {
		isRemoveLimit = removeLimit;
	}

	public BigDecimal getBargainRate() {
		return bargainRate;
	}

	public void setBargainRate(BigDecimal bargainRate) {
		this.bargainRate = bargainRate;
	}

	public Byte getPayType() {
		return payType;
	}

	public void setPayType(Byte payType) {
		this.payType = payType;
	}

	public String getShareTitle() {
		return shareTitle;
	}

	public void setShareTitle(String shareTitle) {
		this.shareTitle = shareTitle;
	}

	public String getShareDesc() {
		return shareDesc;
	}

	public void setShareDesc(String shareDesc) {
		this.shareDesc = shareDesc;
	}

	public String getShareImgUrl() {
		return shareImgUrl;
	}

	public void setShareImgUrl(String shareImgUrl) {
		this.shareImgUrl = shareImgUrl;
	}

	public String getShareType() {
		return shareType;
	}

	public void setShareType(String shareType) {
		this.shareType = shareType;
	}

	public String getShareLink() {
		return shareLink;
	}

	public void setShareLink(String shareLink) {
		this.shareLink = shareLink;
	}

	public Long getProductStockTotal() {
		return productStockTotal;
	}

	public void setProductStockTotal(Long productStockTotal) {
		this.productStockTotal = productStockTotal;
	}

	public Long getProductStock() {
		return productStock;
	}

	public void setProductStock(Long productStock) {
		this.productStock = productStock;
	}

	public Byte getModel() {
		return model;
	}

	public void setModel(Byte model) {
		this.model = model;
	}

	public Long getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(Long timeOut) {
		this.timeOut = timeOut;
	}

	public String getImgs() {
		return imgs;
	}

	public void setImgs(String imgs) {
		this.imgs = imgs;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getActivityInfo() {
		return activityInfo;
	}

	public void setActivityInfo(String activityInfo) {
		this.activityInfo = activityInfo;
	}

	public String getActivityExtInfo() {
		return activityExtInfo;
	}

	public void setActivityExtInfo(String activityExtInfo) {
		this.activityExtInfo = activityExtInfo;
	}

	public Long getActivityCriterion() {
		return activityCriterion;
	}

	public void setActivityCriterion(Long activityCriterion) {
		this.activityCriterion = activityCriterion;
	}

	public String getWxGroupQrUrl() {
		return wxGroupQrUrl;
	}

	public void setWxGroupQrUrl(String wxGroupQrUrl) {
		this.wxGroupQrUrl = wxGroupQrUrl;
	}

	public List<Long> getPriceIds() {
		return priceIds;
	}

	public void setPriceIds(List<Long> priceIds) {
		this.priceIds = priceIds;
	}
}
