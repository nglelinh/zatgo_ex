package com.zatgo.zup.activity.service;

import com.zatgo.zup.activity.entity.wx.WxUserInfoData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;

/**
 * Created by 46041 on 2019/5/13.
 */
public interface WXAttentionNotifyService {


    String attention(String eventKey, String fromWXOpenId, String toWXOpenId);

    void cancel(ThirdLoginInfo loginInfo, String eventKey);

    WxUserInfoData getWechatInfo(String openId);

}
