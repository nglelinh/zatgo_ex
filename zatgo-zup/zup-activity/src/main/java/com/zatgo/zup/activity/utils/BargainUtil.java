package com.zatgo.zup.activity.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by 46041 on 2019/3/12.
 */
public class BargainUtil {


    /**
     * 获取砍一刀的金额
     * @param startAmount
     * @param activityAmount
     * @return
     */
    public static BigDecimal getAmount(BigDecimal startAmount, BigDecimal activityAmount, int count){
        BigDecimal amount = startAmount.add(activityAmount.negate());
        BigDecimal up = amount.divide(new BigDecimal(count - 1 + ""), 8, RoundingMode.DOWN);
        BigDecimal low = up.divide(new BigDecimal("2"), 8, RoundingMode.DOWN);
        if (low.compareTo(up) == 1)
            return low;
        return new BigDecimal(Math.random()).multiply(up.add(low.negate())).add(low).setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }
}
