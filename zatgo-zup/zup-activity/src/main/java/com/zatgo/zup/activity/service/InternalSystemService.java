package com.zatgo.zup.activity.service;

import com.zatgo.zup.activity.entity.ActivityBargain;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/3/12.
 */
public interface InternalSystemService {


    void saveOrderRecord(String activityId, String userId, String orderId, Boolean isSuccess);

    void cancelOrder(String orderId);

    BigDecimal getActivityOrderPrice(String activityId, String userId);

    String saveOrder(String activityId, String userId, String orderId, String type);

//    String getWechatAccessToken(String appId, String appSecret);

    void sendWechatSupportSuccessMsg(String openId, String id);

    void sendWechatSupportMsg(String openId);

    void weixinRefundCallBack(String orderId, String cloudUserId);

    String selectActivityIdByOrderId(String orderId, String cloudUserId);


    ActivityBargain selectActivityId(String activityId);
}
