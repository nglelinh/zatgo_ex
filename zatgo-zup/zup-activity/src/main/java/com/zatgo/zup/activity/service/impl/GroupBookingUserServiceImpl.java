package com.zatgo.zup.activity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.model.PmsProduct;
import com.zatgo.zup.activity.entity.*;
import com.zatgo.zup.activity.entity.request.CreateGroupBookingRequest;
import com.zatgo.zup.activity.entity.request.JoinGroupBookingRequest;
import com.zatgo.zup.activity.entity.response.ActivityGroupBookingInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupDetailInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupInfoResponse;
import com.zatgo.zup.activity.mapper.GroupBookingActivityMapper;
import com.zatgo.zup.activity.mapper.GroupOrderMapper;
import com.zatgo.zup.activity.mapper.GroupUserOrderMapper;
import com.zatgo.zup.activity.remoteservice.TradeApiRemoteService;
import com.zatgo.zup.activity.service.GroupBookingUserService;
import com.zatgo.zup.common.encrypt.MD5Util;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * Created by 46041 on 2019/3/15.
 */

@Service("groupBookingUserService")
public class GroupBookingUserServiceImpl implements GroupBookingUserService {

    @Autowired
    private GroupUserOrderMapper groupUserOrderMapper;
    @Autowired
    private GroupOrderMapper groupOrderMapper;
    @Autowired
    private GroupBookingActivityMapper groupBookingActivityMapper;
    @Autowired
    private TradeApiRemoteService tradeApiRemoteService;


    @Override
    public ActivityGroupBookingInfoResponse getActivityInfo(String activityId, String userId, Integer pageSize) {
        ActivityGroupBookingInfoResponse response = new ActivityGroupBookingInfoResponse();
        GroupBookingActivity activity = groupBookingActivityMapper.selectByPrimaryKey(activityId);
        if (activity != null){
            response.setActivity(activity);
            List<GroupInfoResponse> list = groupBookingActivityMapper.getGroupInfo(activityId, pageSize, new Date());
            for (GroupInfoResponse groupInfoResponse : list) {
                groupInfoResponse.setSelf(userId.equals(groupInfoResponse.getUserId()));
            }
            response.setList(list);
        }
        ResponseData<PmsProduct> productInfo = tradeApiRemoteService.getProductInfo(Long.valueOf(activity.getProductId()));
        if (productInfo != null && productInfo.isSuccessful()){
            response.setPmsProduct(productInfo.getData());
        }
        return response;
    }

    @Override
    public GroupDetailInfoResponse getActivityDetailInfo(String groupOrderId) {
        GroupOrder groupOrder = groupOrderMapper.selectByPrimaryKey(groupOrderId);
        GroupDetailInfoResponse response = new GroupDetailInfoResponse();
        if (groupOrder != null){
            GroupOrderExample example = new GroupOrderExample();
            example.createCriteria().andGroupOrderIdEqualTo(groupOrderId);
            List<GroupUserOrder> list = groupUserOrderMapper.selectByExample(example);
            response.setList(list);
        }
        response.setGroupOrder(groupOrder);
        GroupBookingActivity activity = groupBookingActivityMapper.selectByPrimaryKey(groupOrder.getGroupActivityId());
        response.setActivity(activity);
        ResponseData<PmsProduct> productInfo = tradeApiRemoteService.getProductInfo(Long.valueOf(activity.getProductId()));
        if (productInfo != null && productInfo.isSuccessful()){
            response.setPmsProduct(productInfo.getData());
        }
        return response;
    }

    @Override
    public List<GroupInfoResponse> groupOrdeInfo(String activityId, Integer pageSize) {
        List<GroupInfoResponse> list = groupBookingActivityMapper.getGroupInfo(activityId, pageSize.intValue(), new Date());
        return list;
    }

    @Override
    public PageInfo<ActivityGroupBookingInfoResponse> groupOrdeList(Integer pageSize, Integer pageNo, Boolean start) {
        GroupBookingActivityExample example = new GroupBookingActivityExample();
        GroupBookingActivityExample.Criteria criteria = example.createCriteria();
        Date date = new Date();
        if (start){
            criteria.andActivityEndDateGreaterThan(date).andActivityStartDateLessThanOrEqualTo(date);
        } else {
            criteria.andActivityStartDateGreaterThan(date);
        }
        criteria.andActivityStatusEqualTo(new Byte("1"));
        PageHelper.startPage(pageNo, pageSize);
        List<GroupBookingActivity> groupBookingActivities = groupBookingActivityMapper.selectByExample(example);
        List<ActivityGroupBookingInfoResponse> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(groupBookingActivities)){
            List<Long> ids = new ArrayList<>();
            groupBookingActivities.forEach(gba -> {ids.add(Long.valueOf(gba.getProductId()));});
            ResponseData<List<PmsProduct>> productInfoList = tradeApiRemoteService.getProductInfoList(ids);
            if (productInfoList != null && productInfoList.isSuccessful()){
                List<PmsProduct> data = productInfoList.getData();
                Map<String, PmsProduct> map = new HashMap<>();
                data.forEach(o->{map.put(o.getId().toString(), o);});
                groupBookingActivities.forEach(o -> {
                    ActivityGroupBookingInfoResponse res = new ActivityGroupBookingInfoResponse();
                    res.setActivity(o);
                    res.setPmsProduct(map.get(o.getProductId().toString()));
                    list.add(res);
                });
            }
        }
        return new PageInfo<>(list);
    }
}
