package com.zatgo.zup.activity.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.enumType.BusinessEnum;
import com.ykb.mall.common.enumType.BusinessEnum.ActivityBargainStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.PmsProduct;
import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.activity.entity.ActivityBargainExample;
import com.zatgo.zup.activity.entity.ActivityBargainJoin;
import com.zatgo.zup.activity.entity.ActivityProduct;
import com.zatgo.zup.activity.entity.request.BargainListRequest;
import com.zatgo.zup.activity.entity.response.CountResult;
import com.zatgo.zup.activity.entity.response.ExamineListRespones;
import com.zatgo.zup.activity.mapper.ActivityBargainJoinMapper;
import com.zatgo.zup.activity.mapper.ActivityBargainMapper;
import com.zatgo.zup.activity.model.AddActivityBargainParams;
import com.zatgo.zup.activity.model.UpdateActivityBargainParams;
import com.zatgo.zup.activity.remoteservice.TradeApiRemoteService;
import com.zatgo.zup.activity.remoteservice.UserRemoteService;
import com.zatgo.zup.activity.service.AdminService;
import com.zatgo.zup.activity.service.InternalSystemService;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2019/3/12.
 */
@Service("adminService")
public class AdminServiceImpl implements AdminService {

    @Autowired
    private ActivityBargainMapper activityBargainMapper;
    @Autowired
    private ActivityBargainJoinMapper activityBargainJoinMapper;
    @Autowired
    private InternalSystemService internalSystemService;
    @Autowired
    private UserRemoteService userRemoteService;
    @Autowired
    private TradeApiRemoteService tradeApiRemoteService;


    @Override
    public PageInfo<ActivityBargain> getActivityBargainList(BargainListRequest request, ThirdLoginInfo userInfo) {
        Date activityEndDate = request.getActivityEndDate();
        Date activityStartDate = request.getActivityStartDate();
        Byte payType = request.getPayType();
        String productId = request.getProductId();
        Byte status = request.getStatus();
        String cloudUserId = userInfo.getCloudUserId();
        Integer pageNo = request.getPageNo();
        Integer pageSize = request.getPageSize();
        String keyword = request.getKeyword();

        ActivityBargainExample example = new ActivityBargainExample();
        example.setOrderByClause("activity_start_date asc");
        ActivityBargainExample.Criteria criteria = example.createCriteria();
        if (activityStartDate != null){
            criteria.andActivityStartDateGreaterThanOrEqualTo(activityStartDate);
        }
        if (activityEndDate != null){
            criteria.andActivityEndDateLessThan(activityEndDate);
        }
        if (payType != null){
            criteria.andPayTypeEqualTo(payType);
        }
        if (!StringUtils.isEmpty(productId)){
            criteria.andProductIdEqualTo(productId);
        }
        if (status != null){
            if(ActivityBargainStatus.Input.getCode().equals(status)) {
                criteria.andStatusNotEqualTo(ActivityBargainStatus.Online.getCode());
            } else {
                criteria.andStatusEqualTo(status);
            }
        }
        if (!StringUtils.isEmpty(keyword)){
            keyword = "%" + keyword + "%";
            criteria.andActivityNameLike(keyword);
        }

        criteria.andCloudUserIdEqualTo(cloudUserId);
        PageHelper.startPage(pageNo, pageSize);
        List<ActivityBargain> activityBargains = activityBargainMapper.selectByExample(example);
        return new PageInfo<ActivityBargain>(activityBargains);
    }

    @Override
    public void addActivityBargain(AddActivityBargainParams params, ThirdLoginInfo info) {
        String cloudUserId = info.getCloudUserId();
        String userId = info.getUserId();
        String userName = info.getUserName();
        String productId = params.getProductId();
        String shareDesc = params.getShareDesc();
        String shareImgUrl = params.getShareImgUrl();
        String shareType = params.getShareType();
        String shareTitle = params.getShareTitle();
        String shareLink = params.getShareLink();
        List<Long> priceIds = params.getPriceIds();
        Long productStock = params.getProductStock();
        Long productStockTotal = params.getProductStockTotal();
        Byte model = params.getModel();
        Long timeOut = params.getTimeOut();
        String imgs = params.getImgs();
        String activityName = params.getActivityName();
        String activityInfo = params.getActivityInfo();
        String activityExtInfo = params.getActivityExtInfo();
        Long activityCriterion = params.getActivityCriterion();
        String wxGroupQrUrl = params.getWxGroupQrUrl();
        //校验商品是否存在
        ResponseData<PmsProduct> productInfo = tradeApiRemoteService.getProductInfo(Long.valueOf(productId));
        if(productInfo == null || !productInfo.isSuccessful()){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_PRODUCT_NOT_EXIST);
        }
        PmsProduct activityProduct = productInfo.getData();
        Date date = new Date();

        ActivityBargain activityBargain = new ActivityBargain();
        activityBargain.setBargainActivityId(UUIDUtils.getUuid());
        activityBargain.setCloudUserId(cloudUserId);
        activityBargain.setProductId(productId);
        activityBargain.setProductName(activityProduct.getName());
        activityBargain.setProductImgAbbrUrl(activityProduct.getPic());
        activityBargain.setAddOrderUrl(params.getAddOrderUrl());
        activityBargain.setProductMoney(activityProduct.getPrice());
        activityBargain.setProductIntro(activityProduct.getDescription());
        activityBargain.setActivityMoney(params.getActivityMoney());
        activityBargain.setActivityStartMoney(params.getActivityStartMoney());
        activityBargain.setRemoveLimit(params.getRemoveLimit());
        activityBargain.setBargainRate(params.getBargainRate());
        activityBargain.setJoinCount(params.getJoinCount());
        activityBargain.setProductDesc(params.getProductDesc());
        activityBargain.setActivityStartDate(params.getActivityStartDate());
        activityBargain.setActivityEndDate(params.getActivityEndDate());
        activityBargain.setStatus(BusinessEnum.ActivityBargainStatus.Input.getCode());
        activityBargain.setCreateDate(date);
        activityBargain.setOperatorName(userName);
        activityBargain.setOperatorId(userId);
        activityBargain.setPayType(params.getPayType());
        activityBargain.setShareDesc(shareDesc);
        activityBargain.setShareImgUrl(shareImgUrl);
        activityBargain.setShareLink(shareLink);
        activityBargain.setShareTitle(shareTitle);
        activityBargain.setShareType(shareType);
        activityBargain.setProductStock(productStock);
        activityBargain.setProductStockTotal(productStockTotal);
        activityBargain.setModel(model);
        activityBargain.setTimeOut(timeOut);
        activityBargain.setImgs(imgs);
        activityBargain.setActivityName(activityName);
        activityBargain.setActivityInfo(activityInfo);
        activityBargain.setActivityExtInfo(activityExtInfo);
        activityBargain.setActivityCriterion(activityCriterion);
        activityBargain.setWxGroupQrUrl(wxGroupQrUrl);
        if (!CollectionUtils.isEmpty(priceIds)){
            activityBargain.setPriceIds(JSONArray.toJSONString(priceIds));
        }
        activityBargainMapper.insertSelective(activityBargain);
    }

    @Override
    public void updateActivityBargain(UpdateActivityBargainParams params, ThirdLoginInfo info) {
        String userId = info.getUserId();
        String userName = info.getUserName();
        String productId = params.getProductId();
        String bargainActivityId = params.getBargainActivityId();
        String shareDesc = params.getShareDesc();
        String shareImgUrl = params.getShareImgUrl();
        String shareType = params.getShareType();
        String shareTitle = params.getShareTitle();
        String shareLink = params.getShareLink();
        Long productStock = params.getProductStock();
        Long productStockTotal = params.getProductStockTotal();
        Byte model = params.getModel();
        Long timeOut = params.getTimeOut();
        String imgs = params.getImgs();
        String activityName = params.getActivityName();
        String activityInfo = params.getActivityInfo();
        String activityExtInfo = params.getActivityExtInfo();
        Long activityCriterion = params.getActivityCriterion();
        String wxGroupQrUrl = params.getWxGroupQrUrl();
        List<Long> priceIds = params.getPriceIds();


        //校验商品是否存在
        ResponseData<PmsProduct> productInfo = tradeApiRemoteService.getProductInfo(Long.valueOf(productId));
        if(productInfo == null || !productInfo.isSuccessful()){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_PRODUCT_NOT_EXIST);
        }
        PmsProduct activityProduct = productInfo.getData();
        //修改
        ActivityBargain activityBargain = new ActivityBargain();
        activityBargain.setBargainActivityId(bargainActivityId);
        activityBargain.setProductId(productId);
        activityBargain.setProductName(activityProduct.getName());
        activityBargain.setProductImgAbbrUrl(activityProduct.getPic());
        activityBargain.setAddOrderUrl(params.getAddOrderUrl());
        activityBargain.setProductMoney(activityProduct.getPrice());
        activityBargain.setProductIntro(activityProduct.getDescription());
        activityBargain.setActivityMoney(params.getActivityMoney());
        activityBargain.setActivityStartMoney(params.getActivityStartMoney());
        activityBargain.setRemoveLimit(params.getRemoveLimit());
        activityBargain.setBargainRate(params.getBargainRate());
        activityBargain.setJoinCount(params.getJoinCount());
        activityBargain.setProductDesc(params.getProductDesc());
        activityBargain.setActivityStartDate(params.getActivityStartDate());
        activityBargain.setActivityEndDate(params.getActivityEndDate());
        activityBargain.setOperatorName(userName);
        activityBargain.setOperatorId(userId);
        activityBargain.setPayType(params.getPayType());
        activityBargain.setShareDesc(shareDesc);
        activityBargain.setShareImgUrl(shareImgUrl);
        activityBargain.setShareLink(shareLink);
        activityBargain.setShareTitle(shareTitle);
        activityBargain.setShareType(shareType);
        activityBargain.setProductStock(productStock);
        activityBargain.setProductStockTotal(productStockTotal);
        activityBargain.setModel(model);
        activityBargain.setTimeOut(timeOut);
        activityBargain.setImgs(imgs);
        activityBargain.setActivityName(activityName);
        activityBargain.setActivityInfo(activityInfo);
        activityBargain.setActivityExtInfo(activityExtInfo);
        activityBargain.setActivityCriterion(activityCriterion);
        activityBargain.setWxGroupQrUrl(wxGroupQrUrl);
        if (!CollectionUtils.isEmpty(priceIds)){
            activityBargain.setPriceIds(JSONArray.toJSONString(priceIds));
        }

        activityBargainMapper.updateByPrimaryKeySelective(activityBargain);
    }

    @Override
    public void offlineActivityBargain(String activityBargainId, ThirdLoginInfo info) {
        ActivityBargain activityBargain = activityBargainMapper.selectByPrimaryKey(activityBargainId);
        if (activityBargain == null)
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_NOT_EXIST);
        ActivityBargain activity = new ActivityBargain();
        activity.setBargainActivityId(activityBargainId);
        activity.setOperatorId(info.getUserId());
        activity.setOperatorName(info.getUserName());
        activity.setStatus(BusinessEnum.ActivityBargainStatus.Offline.getCode());
        activityBargainMapper.updateByPrimaryKeySelective(activity);
    }

    @Override
    public void onlineActivityBargain(String activityBargainId, ThirdLoginInfo info) {
        ActivityBargain activityBargain = activityBargainMapper.selectByPrimaryKey(activityBargainId);
        if (activityBargain == null)
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_NOT_EXIST);
        ActivityBargain activity = new ActivityBargain();
        activity.setBargainActivityId(activityBargainId);
        activity.setOperatorId(info.getUserId());
        activity.setOperatorName(info.getUserName());
        activity.setStatus(BusinessEnum.ActivityBargainStatus.Online.getCode());
        activityBargainMapper.updateByPrimaryKeySelective(activity);
    }

    @Override
    public ActivityBargain getActivityBargain(String activityBargainId, ThirdLoginInfo info) {
        ActivityBargainExample example = new ActivityBargainExample();
        example.createCriteria().andBargainActivityIdEqualTo(activityBargainId).andCloudUserIdEqualTo(info.getCloudUserId());
        List<ActivityBargain> activityBargains = activityBargainMapper.selectByExample(example);
        return CollectionUtils.isEmpty(activityBargains) ? null : activityBargains.get(0);
    }

    @Override
    public PageInfo<ExamineListRespones> examineList(Integer pageNo, Integer pageSize, Byte status, ThirdLoginInfo info,
                                                     String activityBargainId, String keyword) {
        if (!StringUtils.isEmpty(keyword))
            keyword = "%" + keyword + "%";
        PageHelper.startPage(pageNo, pageSize);
        List<ExamineListRespones> list = activityBargainJoinMapper.selectExamineList(status, activityBargainId, keyword);
        StringBuffer sb = new StringBuffer();
        list.forEach(examine -> {
            sb.append(",");
            sb.append("'" + examine.getId() + "'");
        });
        if (!CollectionUtils.isEmpty(list)){
            List<CountResult> count = activityBargainJoinMapper.getCount(sb.toString().replaceFirst(",", ""));
            Map<String, Integer> map = new HashMap<>();
            count.forEach(m -> {
                String id = m.getId();
                Integer num = m.getHelpPeople();
                map.put(id, num);
            });
            for (ExamineListRespones examine : list) {
                examine.setHelpPeople(map.get(examine.getId()));
            }
        }
        return new PageInfo<>(list);
    }

    @Override
    @Transactional
    public void execute(String id, Byte status, ThirdLoginInfo info) {
        ActivityBargainJoin abj = new ActivityBargainJoin();
        abj.setActivityBargainJoinId(id);
        abj.setStatus(status);
        abj.setUpdateTime(new Date());
        activityBargainJoinMapper.updateByPrimaryKeySelective(abj);

        if(status.intValue() == 2) {
            ActivityBargainJoin bargainJoin = activityBargainJoinMapper.selectByPrimaryKey(id);
            ResponseData<UserData> userById = userRemoteService.getUserById(bargainJoin.getUserId() + "");
            if(userById == null || !userById.isSuccessful())
                throw new BusinessException();
            internalSystemService.sendWechatSupportSuccessMsg(userById.getData().getWechatOpenid(), bargainJoin.getActivityBargainJoinId());
        }
    }
}
