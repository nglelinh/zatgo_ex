package com.zatgo.zup.activity.entity.response;

import java.util.Date;

/**
 * Created by 46041 on 2019/5/16.
 */
public class ExamineListRespones {

    private String userName;

    private String userImg;

    private Integer helpPeople;

    private String questionnaire;

    private String id;

    private String activityName;

    private Byte status;

    private String orderId;

    private Date updateTime;
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public Integer getHelpPeople() {
        return helpPeople;
    }

    public void setHelpPeople(Integer helpPeople) {
        this.helpPeople = helpPeople;
    }

    public String getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(String questionnaire) {
        this.questionnaire = questionnaire;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
