package com.zatgo.zup.activity.entity.response;

import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.activity.entity.ActivityBargainJoin;
import com.zatgo.zup.activity.entity.ActivityBargainList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/3/12.
 */
@ApiModel
public class ActivityBargainInfoResponse {

    @ApiModelProperty(value = "活动信息",required = true)
    private ActivityBargain activityBargain;
    @ApiModelProperty(value = "活动参加信息",required = true)
    private ActivityBargainJoin activityBargainJoin;
    @ApiModelProperty(value = "助力列表",required = true)
    private List<ActivityBargainList> list;
    @ApiModelProperty(value = "云用户商品Id",required = true)
    private String cloudUserProductId;
    @ApiModelProperty(value = "出发时间",required = true)
    private Date departureTime;


    public ActivityBargainJoin getActivityBargainJoin() {
        return activityBargainJoin;
    }

    public void setActivityBargainJoin(ActivityBargainJoin activityBargainJoin) {
        this.activityBargainJoin = activityBargainJoin;
    }

    public List<ActivityBargainList> getList() {
        return list;
    }

    public void setList(List<ActivityBargainList> list) {
        this.list = list;
    }

    public ActivityBargain getActivityBargain() {
        return activityBargain;
    }

    public void setActivityBargain(ActivityBargain activityBargain) {
        this.activityBargain = activityBargain;
    }

    public String getCloudUserProductId() {
        return cloudUserProductId;
    }

    public void setCloudUserProductId(String cloudUserProductId) {
        this.cloudUserProductId = cloudUserProductId;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }
}
