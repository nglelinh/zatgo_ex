package com.zatgo.zup.activity.entity.response;

import com.ykb.mall.common.model.PmsProduct;
import com.zatgo.zup.activity.entity.GroupBookingActivity;
import com.zatgo.zup.activity.entity.GroupUserOrder;

import java.util.List;

/**
 * Created by 46041 on 2019/3/18.
 */
public class ActivityGroupBookingInfoResponse {


    private GroupBookingActivity activity;

    private List<GroupInfoResponse> list;

    private PmsProduct pmsProduct;


    public GroupBookingActivity getActivity() {
        return activity;
    }

    public void setActivity(GroupBookingActivity activity) {
        this.activity = activity;
    }

    public List<GroupInfoResponse> getList() {
        return list;
    }

    public void setList(List<GroupInfoResponse> list) {
        this.list = list;
    }

    public PmsProduct getPmsProduct() {
        return pmsProduct;
    }

    public void setPmsProduct(PmsProduct pmsProduct) {
        this.pmsProduct = pmsProduct;
    }
}
