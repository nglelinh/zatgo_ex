package com.zatgo.zup.activity.entity.response;

import com.ykb.mall.common.model.PmsProduct;
import com.zatgo.zup.activity.entity.GroupBookingActivity;
import com.zatgo.zup.activity.entity.GroupOrder;
import com.zatgo.zup.activity.entity.GroupUserOrder;

import java.util.List;

/**
 * Created by 46041 on 2019/6/20.
 */
public class GroupDetailInfoResponse {


    private GroupBookingActivity activity;

    private List<GroupUserOrder> list;

    private PmsProduct pmsProduct;

    private GroupOrder groupOrder;

    public GroupBookingActivity getActivity() {
        return activity;
    }

    public void setActivity(GroupBookingActivity activity) {
        this.activity = activity;
    }

    public List<GroupUserOrder> getList() {
        return list;
    }

    public void setList(List<GroupUserOrder> list) {
        this.list = list;
    }

    public PmsProduct getPmsProduct() {
        return pmsProduct;
    }

    public void setPmsProduct(PmsProduct pmsProduct) {
        this.pmsProduct = pmsProduct;
    }

    public GroupOrder getGroupOrder() {
        return groupOrder;
    }

    public void setGroupOrder(GroupOrder groupOrder) {
        this.groupOrder = groupOrder;
    }
}
