package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ActivityBargainJoin {
    private String activityBargainJoinId;

    private String bargainActivityId;

    private String userId;

    private String productId;

    private String productName;

    private String productImgAbbrUrl;

    private String addOrderUrl;

    private BigDecimal productMoney;

    private String productIntro;

    private Integer bargainCount;

    private BigDecimal dealMoney;

    private Date createTime;

    private Boolean isAddOrder;

    private Date payTime;

    private Boolean payStatus;

    private String extInfo;

    private String cloudUserId;

    private Byte status;

    private String userImg;

    private String userNick;

    private Date updateTime;

    public String getActivityBargainJoinId() {
        return activityBargainJoinId;
    }

    public void setActivityBargainJoinId(String activityBargainJoinId) {
        this.activityBargainJoinId = activityBargainJoinId == null ? null : activityBargainJoinId.trim();
    }

    public String getBargainActivityId() {
        return bargainActivityId;
    }

    public void setBargainActivityId(String bargainActivityId) {
        this.bargainActivityId = bargainActivityId == null ? null : bargainActivityId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public String getProductImgAbbrUrl() {
        return productImgAbbrUrl;
    }

    public void setProductImgAbbrUrl(String productImgAbbrUrl) {
        this.productImgAbbrUrl = productImgAbbrUrl == null ? null : productImgAbbrUrl.trim();
    }

    public String getAddOrderUrl() {
        return addOrderUrl;
    }

    public void setAddOrderUrl(String addOrderUrl) {
        this.addOrderUrl = addOrderUrl == null ? null : addOrderUrl.trim();
    }

    public BigDecimal getProductMoney() {
        return productMoney;
    }

    public void setProductMoney(BigDecimal productMoney) {
        this.productMoney = productMoney;
    }

    public String getProductIntro() {
        return productIntro;
    }

    public void setProductIntro(String productIntro) {
        this.productIntro = productIntro == null ? null : productIntro.trim();
    }

    public Integer getBargainCount() {
        return bargainCount;
    }

    public void setBargainCount(Integer bargainCount) {
        this.bargainCount = bargainCount;
    }

    public BigDecimal getDealMoney() {
        return dealMoney;
    }

    public void setDealMoney(BigDecimal dealMoney) {
        this.dealMoney = dealMoney;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getIsAddOrder() {
        return isAddOrder;
    }

    public void setIsAddOrder(Boolean isAddOrder) {
        this.isAddOrder = isAddOrder;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Boolean getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Boolean payStatus) {
        this.payStatus = payStatus;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}