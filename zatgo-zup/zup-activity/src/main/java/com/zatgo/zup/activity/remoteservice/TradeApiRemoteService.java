package com.zatgo.zup.activity.remoteservice;

import com.alibaba.fastjson.JSONArray;
import com.ykb.mall.common.model.OmsOrder;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductGroup;
import com.ykb.mall.common.model.PortalOrderDetailInfo;
import com.zatgo.zup.common.model.ResponseData;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 46041 on 2019/6/19.
 */

@FeignClient("zup-trade-api")
public interface TradeApiRemoteService {


    @GetMapping(value = "/trade/api/pmsProduct/get/{id}")
    ResponseData<PmsProduct> getProductInfo(@PathVariable("id") Long id);

    @RequestMapping(value = "/trade/api/pmsProduct/get/list")
    ResponseData<List<PmsProduct>> getProductInfoList(@RequestParam("ids") List<Long> ids);

    @GetMapping(value = "/trade/api/order/detail/{id}/{userId}")
    ResponseData<PortalOrderDetailInfo> selectOrderDetail(@PathVariable("id") Long id, @PathVariable("userId") String userId);


    @PostMapping(value = "/trade/api/order/update")
    void updateOrderInfo(@RequestBody OmsOrder omsOrder);

    @PostMapping(value = "/trade/api/order/auto/refund")
    void autoRefund(@RequestParam("groupUserOrderList") String groupUserOrderList);

    @GetMapping(value = "/trade/api/pmsProduct/travel")
    ResponseData<PmsProductGroup> getTravelGroupInfo(@RequestParam("id") Long id);
}
