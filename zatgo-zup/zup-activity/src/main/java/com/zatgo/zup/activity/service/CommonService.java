package com.zatgo.zup.activity.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.entity.ActivityProduct;
import com.zatgo.zup.activity.entity.UserAbbr;
import com.zatgo.zup.activity.entity.request.SyncProductRequest;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;

import java.util.Map;

/**
 * Created by 46041 on 2019/3/19.
 */
public interface CommonService {


    void syncProduct(SyncProductRequest params);

//    UserAbbr getUser(UserAbbr info);

    PageInfo<ActivityProduct> productList(String productName, Integer pageNo, Integer pageSize, ThirdLoginInfo userInfo);

    Map<String,Long> getPayTime(String ids);

    String getActivityId(String joinId);

    Long getActiviTypayTime(String activityId, String orderType);
}
