package com.zatgo.zup.activity.controller.groupBooking;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.controller.BaseController;
import com.zatgo.zup.activity.entity.UserAbbr;
import com.zatgo.zup.activity.entity.response.ActivityGroupBookingInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupDetailInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupInfoResponse;
import com.zatgo.zup.activity.service.CommonService;
import com.zatgo.zup.activity.service.GroupBookingUserService;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.redis.RedisLockUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Created by 46041 on 2019/3/15.
 */

@Api(value = "/activity/groupBooking/user",description = "拼团用户接口")
@RestController
@RequestMapping(value = "/activity/groupBooking/user")
public class GroupBookingUserController extends BaseController {


    @Autowired
    private GroupBookingUserService groupBookingUserService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private CommonService commonService;

    @Value("${groupBooking.url}")
    private String url;

    @RequestMapping(value = "/activityInfo/{activityId}",method = RequestMethod.POST)
    @ApiOperation(value = "根据拼团活动ID查活动", notes = "根据拼团ID查活动", httpMethod = "POST")
    @ResponseBody
    public ResponseData getActivityInfo(@PathVariable("activityId") String activityId, @RequestParam("pageSize") Integer pageSize) {
        ThirdLoginInfo userInfo = getUserInfo();
        ActivityGroupBookingInfoResponse activityInfo = groupBookingUserService.getActivityInfo(activityId, userInfo.getUserId(), pageSize);
        return BusinessResponseFactory.createSuccess(activityInfo);
    }

    @RequestMapping(value = "/activityOrderInfo/{groupOrderId}",method = RequestMethod.POST)
    @ApiOperation(value = "根据参团ID查活动", notes = "根据参团ID查活动", httpMethod = "POST")
    @ResponseBody
    public ResponseData getActivityDetailInfo(@PathVariable("groupOrderId") String groupOrderId) {
        ThirdLoginInfo userInfo = getUserInfo();
        GroupDetailInfoResponse activityInfo = groupBookingUserService.getActivityDetailInfo(groupOrderId);
        return BusinessResponseFactory.createSuccess(activityInfo);
    }


    @RequestMapping(value = "/group/order/{activityId}",method = RequestMethod.POST)
    @ApiOperation(value = "根据根据活动id查询待成团信息", notes = "根据根据活动id查询待成团信息", httpMethod = "POST")
    @ResponseBody
    public ResponseData groupOrdeInfo(@PathVariable("activityId") String activityId,
                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        ThirdLoginInfo userInfo = getUserInfo();
        List<GroupInfoResponse> activityInfo = groupBookingUserService.groupOrdeInfo(activityId, pageSize);
        return BusinessResponseFactory.createSuccess(activityInfo);
    }

    @RequestMapping(value = "/group/list",method = RequestMethod.GET)
    @ApiOperation(value = "拼团列表", notes = "拼团列表", httpMethod = "GET")
    @ResponseBody
    public ResponseData groupOrdeList(@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                      @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                      @RequestParam(value = "start", defaultValue = "true") Boolean start) {
        ThirdLoginInfo userInfo = getUserInfo();
        PageInfo<ActivityGroupBookingInfoResponse> activityInfo = groupBookingUserService.groupOrdeList(pageSize, pageNo, start);
        return BusinessResponseFactory.createSuccess(activityInfo);
    }

}
