package com.zatgo.zup.activity.controller.wx;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.activity.controller.BaseController;
import com.zatgo.zup.activity.remoteservice.WechatRemoteService;
import com.zatgo.zup.activity.service.WXAttentionNotifyService;
import com.zatgo.zup.activity.utils.HttpUtil;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.TimeUnit;

/**
 * Created by 46041 on 2019/5/13.
 */

@Controller
@Api(value = "/activity/productActivity",description = "商品活动管理",produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping("/activity/productActivity")
public class PmsProductActivityController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(PmsProductActivityController.class);


    private static final String WxCreateQRCodeUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=";


    @Autowired
    private WXAttentionNotifyService weiXinService;
    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private WechatRemoteService wechatRemoteService;
//    @Autowired
//    private IWechatAccessTokenService iWechatAccessTokenService;


    @ApiOperation("QRCode")
    @RequestMapping(value = "/QRCode", method = RequestMethod.POST)
    @ResponseBody
    public Object getQRCode(@RequestParam("activityJoinId") String activityJoinId) {
        ThirdLoginInfo loginInfo = getUserInfo();
        String cloudUserId = loginInfo.getCloudUserId();
        String userId = loginInfo.getUserId();
        JSONObject wxRes = redisUtils.get(getKey(userId, cloudUserId, activityJoinId), JSONObject.class);
        if (wxRes == null){
            wxRes = initQR(userId, cloudUserId, activityJoinId);
        }
        return BusinessResponseFactory.createSuccess(wxRes);
    }



    private JSONObject initQR(String userId, String cloudUserId, String activityJoinId){
        String scene_str = userId + "_" + cloudUserId + "_" + activityJoinId;
        ResponseData<String> qrCode = wechatRemoteService.getQRCode(cloudUserId, scene_str, userId);
        if (qrCode == null || !qrCode.isSuccessful()){
            throw new BusinessException(BusinessExceptionCode.GET_WECHAT_QECODE_FAIL);
        }
        return JSONObject.parseObject(qrCode.getData());
    }



    private String getKey(String userId, String cloudUserId, String activityJoinId){
        return RedisKeyConstants.WECHAT_QR + cloudUserId + ":" + userId + ":" + activityJoinId;
    }
}
