package com.zatgo.zup.activity.entity.request;

import java.util.Date;

/**
 * Created by 46041 on 2019/3/12.
 */
public class BargainListRequest {

    private String productId;

    private Date activityStartDate;

    private Date activityEndDate;

    /**
     * 状态 0=录入
             1=上线
             2=下线
     */
    private Byte status;
    /**
     *  0 下单后再付款  1下单前付款
     */
    private Byte payType;

    private Integer pageNo;

    private Integer pageSize;

    private String keyword;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Date getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(Date activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public Date getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(Date activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getPayType() {
        return payType;
    }

    public void setPayType(Byte payType) {
        this.payType = payType;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
