//package com.zatgo.zup.activity.service.impl;
//
//import javax.annotation.Resource;
//
//import com.alibaba.fastjson.JSON;
//import com.zatgo.zup.activity.service.IWechatAccessTokenService;
//import com.zatgo.zup.common.http.OkHttpService;
//import com.zatgo.zup.common.model.wx.WechatAccessToken;
//import com.zatgo.zup.common.redis.RedisKeyConstants;
//import com.zatgo.zup.common.redis.RedisLockUtils;
//import com.zatgo.zup.common.redis.RedisUtils;
//import com.zatgo.zup.common.utils.ThreadPoolUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Service;
//
//import java.util.concurrent.TimeUnit;
//
//
//@Service
//public class WechatAccessTokenServiceImpl implements IWechatAccessTokenService {
//
//	private Logger log = LoggerFactory.getLogger(getClass());
//
//	/**
//	 * 微信ACCESS_TOKEN获取链接固定部分
//	 */
//	private static final String URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
//
////	@Value("${wx.appid}")
////	private String appid;
////
////	@Value("${wx.appsecret}")
////	private String appsecret;
//
//	@Resource
//	private OkHttpService okHttpService;
//
//	@Resource
//	private RedisUtils redisUtils;
//
//	@Resource
//	private RedisLockUtils redisLockUtils;
//
//	@Autowired
//	private RedisTemplate redisTemplate;
//
//	@Override
//	public String getAccessToken(String appId,String appSecret) {
//
//		// 获取微信AccessToken缓存
////		String useAppId;
////		String userAppSecret;
////		if(null != appIdParam){
////			useAppId=appIdParam;
////		}else{
////			useAppId=appId;
////		}
////		if(null != appSecret){
////			userAppSecret=appSecret;
////		}else{
////			userAppSecret=appsecret;
////		}
//
//		String accessToken = redisUtils.get(RedisKeyConstants.WECHAT_ACCESS_TOKEN, String.class);
//		if (StringUtils.isNotEmpty(accessToken)) {
//			return accessToken;
//		}
//
//		// 微信AccessToken缓存过期后，获取备份AccessToken缓存
//		accessToken = redisUtils.get(RedisKeyConstants.WECHAT_ACCESS_TOKEN_BACKUP, String.class);
//		if (StringUtils.isNotEmpty(accessToken)) {
//			// 微信AccessToken缓存已失效且备份缓存未失效的情况下线程更新AccessToken
//			ThreadPoolUtils.getThreadPool().execute(new Runnable() {
//
//				@Override
//				public void run() {
//					// 等待时间传入0，无法在cacheAccessToken内获得锁认为已经有获取TOKEN的线程再执行，放弃更新
//					cacheAccessToken(0L,appId,appSecret);
//				}
//			});
//			return accessToken;
//		}
//
//		// 在AccessToken缓存和备份缓存都已经失效的情况，传入负数，强制同步获取Token
//		// 在有更新TOKEN的线程执行时会等待线程执行完成后获取缓存
//		accessToken = cacheAccessToken(-1L,appId,appSecret);
//
//		return accessToken;
//	}
//
//	@Override
//	public String cacheAccessToken(Long timeOut,String useAppId,String userAppSecret) {
//
//		String accessToken = "";
//
//		String redisKey = RedisKeyConstants.WECHAT_ACCESS_TOKEN + "_zup";
//
//		try {
//			if (!redisLockUtils.lock(redisKey)) {
//				return accessToken;
//			}
//
//			accessToken = redisUtils.get(RedisKeyConstants.WECHAT_ACCESS_TOKEN, String.class);
////			accessToken = "21_dNbSsM1ohpcr9pXH_fh_tiVa6Nhfv8TJDcpUh4HY1Foox-7LLay8Tt-iAyUHMrxkTk3FJITZ3eKMb6L1IUJTDxIJ3vhBE_kIzeBBRIDx0pgNs5IXI1lB0jigkl2HuDFBKXhmfvxD-BSqEE_MDXXgADAZTT";
//			if (StringUtils.isNotEmpty(accessToken)) {
//				return accessToken;
//			}
//
//			WechatAccessToken token = getWechatAccessToken(useAppId,userAppSecret);
//			accessToken = token.getAccessToken();
//			Integer expiresIn = token.getExpiresIn();
//
//			// 微信ACCESS_TOKEN缓存过期时间为返回过期秒数减去10分钟
//			redisTemplate.opsForValue().set(RedisKeyConstants.WECHAT_ACCESS_TOKEN, accessToken);
//			redisTemplate.expire(RedisKeyConstants.WECHAT_ACCESS_TOKEN, expiresIn - 10 * 60, TimeUnit.SECONDS);
//
//			// 微信ACCESS_TOKEN备份缓存过期时间为返回过期秒速减去5分钟，存储两份是为了在获取新ACCESS_TOKEN时能用备份TOKEN正常进行业务进行
//			// 新旧微信ACCESS_TOKEN在新ACCESS_TOKEN获取后5分钟内同时有效
//			redisTemplate.opsForValue().set(RedisKeyConstants.WECHAT_ACCESS_TOKEN_BACKUP, accessToken);
//			redisTemplate.expire(RedisKeyConstants.WECHAT_ACCESS_TOKEN_BACKUP, expiresIn - 5 * 60, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			log.error("获取微信ACCESS_TOKEN失败", e);
//		} finally {
//			redisLockUtils.releaseLock(redisKey);
//		}
//
//		return accessToken;
//	}
//
//	/**
//	 * 请求微信获取AccessToken
//	 *
//	 * @return
//	 */
//	private WechatAccessToken getWechatAccessToken(String useAppId,String userAppSecret) {
//		String url = new StringBuffer(URL).append("&appid=").append(useAppId).append("&secret=").append(userAppSecret)
//				.toString();
//		WechatAccessToken token = okHttpService.get(url, WechatAccessToken.class);
//		log.info(JSON.toJSONString(token));
//		return token;
//	}
//}
