package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityProductExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ActivityProductExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(String value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(String value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(String value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(String value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(String value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLike(String value) {
            addCriterion("product_id like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotLike(String value) {
            addCriterion("product_id not like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<String> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<String> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(String value1, String value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(String value1, String value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdIsNull() {
            addCriterion("cloud_user_product_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdIsNotNull() {
            addCriterion("cloud_user_product_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdEqualTo(String value) {
            addCriterion("cloud_user_product_id =", value, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdNotEqualTo(String value) {
            addCriterion("cloud_user_product_id <>", value, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdGreaterThan(String value) {
            addCriterion("cloud_user_product_id >", value, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_product_id >=", value, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdLessThan(String value) {
            addCriterion("cloud_user_product_id <", value, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_product_id <=", value, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdLike(String value) {
            addCriterion("cloud_user_product_id like", value, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdNotLike(String value) {
            addCriterion("cloud_user_product_id not like", value, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdIn(List<String> values) {
            addCriterion("cloud_user_product_id in", values, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdNotIn(List<String> values) {
            addCriterion("cloud_user_product_id not in", values, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdBetween(String value1, String value2) {
            addCriterion("cloud_user_product_id between", value1, value2, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andCloudUserProductIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_product_id not between", value1, value2, "cloudUserProductId");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNull() {
            addCriterion("product_name is null");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNotNull() {
            addCriterion("product_name is not null");
            return (Criteria) this;
        }

        public Criteria andProductNameEqualTo(String value) {
            addCriterion("product_name =", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotEqualTo(String value) {
            addCriterion("product_name <>", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThan(String value) {
            addCriterion("product_name >", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThanOrEqualTo(String value) {
            addCriterion("product_name >=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThan(String value) {
            addCriterion("product_name <", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThanOrEqualTo(String value) {
            addCriterion("product_name <=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLike(String value) {
            addCriterion("product_name like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotLike(String value) {
            addCriterion("product_name not like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameIn(List<String> values) {
            addCriterion("product_name in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotIn(List<String> values) {
            addCriterion("product_name not in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameBetween(String value1, String value2) {
            addCriterion("product_name between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotBetween(String value1, String value2) {
            addCriterion("product_name not between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIsNull() {
            addCriterion("product_img_abbr_url is null");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIsNotNull() {
            addCriterion("product_img_abbr_url is not null");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlEqualTo(String value) {
            addCriterion("product_img_abbr_url =", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotEqualTo(String value) {
            addCriterion("product_img_abbr_url <>", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlGreaterThan(String value) {
            addCriterion("product_img_abbr_url >", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlGreaterThanOrEqualTo(String value) {
            addCriterion("product_img_abbr_url >=", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLessThan(String value) {
            addCriterion("product_img_abbr_url <", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLessThanOrEqualTo(String value) {
            addCriterion("product_img_abbr_url <=", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlLike(String value) {
            addCriterion("product_img_abbr_url like", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotLike(String value) {
            addCriterion("product_img_abbr_url not like", value, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlIn(List<String> values) {
            addCriterion("product_img_abbr_url in", values, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotIn(List<String> values) {
            addCriterion("product_img_abbr_url not in", values, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlBetween(String value1, String value2) {
            addCriterion("product_img_abbr_url between", value1, value2, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductImgAbbrUrlNotBetween(String value1, String value2) {
            addCriterion("product_img_abbr_url not between", value1, value2, "productImgAbbrUrl");
            return (Criteria) this;
        }

        public Criteria andProductIntroIsNull() {
            addCriterion("product_intro is null");
            return (Criteria) this;
        }

        public Criteria andProductIntroIsNotNull() {
            addCriterion("product_intro is not null");
            return (Criteria) this;
        }

        public Criteria andProductIntroEqualTo(String value) {
            addCriterion("product_intro =", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotEqualTo(String value) {
            addCriterion("product_intro <>", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroGreaterThan(String value) {
            addCriterion("product_intro >", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroGreaterThanOrEqualTo(String value) {
            addCriterion("product_intro >=", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLessThan(String value) {
            addCriterion("product_intro <", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLessThanOrEqualTo(String value) {
            addCriterion("product_intro <=", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroLike(String value) {
            addCriterion("product_intro like", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotLike(String value) {
            addCriterion("product_intro not like", value, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroIn(List<String> values) {
            addCriterion("product_intro in", values, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotIn(List<String> values) {
            addCriterion("product_intro not in", values, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroBetween(String value1, String value2) {
            addCriterion("product_intro between", value1, value2, "productIntro");
            return (Criteria) this;
        }

        public Criteria andProductIntroNotBetween(String value1, String value2) {
            addCriterion("product_intro not between", value1, value2, "productIntro");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIsNull() {
            addCriterion("add_order_url is null");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIsNotNull() {
            addCriterion("add_order_url is not null");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlEqualTo(String value) {
            addCriterion("add_order_url =", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotEqualTo(String value) {
            addCriterion("add_order_url <>", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlGreaterThan(String value) {
            addCriterion("add_order_url >", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlGreaterThanOrEqualTo(String value) {
            addCriterion("add_order_url >=", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLessThan(String value) {
            addCriterion("add_order_url <", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLessThanOrEqualTo(String value) {
            addCriterion("add_order_url <=", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlLike(String value) {
            addCriterion("add_order_url like", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotLike(String value) {
            addCriterion("add_order_url not like", value, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlIn(List<String> values) {
            addCriterion("add_order_url in", values, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotIn(List<String> values) {
            addCriterion("add_order_url not in", values, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlBetween(String value1, String value2) {
            addCriterion("add_order_url between", value1, value2, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andAddOrderUrlNotBetween(String value1, String value2) {
            addCriterion("add_order_url not between", value1, value2, "addOrderUrl");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIsNull() {
            addCriterion("product_money is null");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIsNotNull() {
            addCriterion("product_money is not null");
            return (Criteria) this;
        }

        public Criteria andProductMoneyEqualTo(BigDecimal value) {
            addCriterion("product_money =", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotEqualTo(BigDecimal value) {
            addCriterion("product_money <>", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyGreaterThan(BigDecimal value) {
            addCriterion("product_money >", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("product_money >=", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLessThan(BigDecimal value) {
            addCriterion("product_money <", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("product_money <=", value, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyIn(List<BigDecimal> values) {
            addCriterion("product_money in", values, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotIn(List<BigDecimal> values) {
            addCriterion("product_money not in", values, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("product_money between", value1, value2, "productMoney");
            return (Criteria) this;
        }

        public Criteria andProductMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("product_money not between", value1, value2, "productMoney");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIsNull() {
            addCriterion("promotion_price is null");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIsNotNull() {
            addCriterion("promotion_price is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceEqualTo(BigDecimal value) {
            addCriterion("promotion_price =", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotEqualTo(BigDecimal value) {
            addCriterion("promotion_price <>", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceGreaterThan(BigDecimal value) {
            addCriterion("promotion_price >", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_price >=", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceLessThan(BigDecimal value) {
            addCriterion("promotion_price <", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_price <=", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIn(List<BigDecimal> values) {
            addCriterion("promotion_price in", values, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotIn(List<BigDecimal> values) {
            addCriterion("promotion_price not in", values, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_price between", value1, value2, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_price not between", value1, value2, "promotionPrice");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}