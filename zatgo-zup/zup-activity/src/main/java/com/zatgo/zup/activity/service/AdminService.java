package com.zatgo.zup.activity.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.activity.entity.request.BargainListRequest;
import com.zatgo.zup.activity.entity.response.ExamineListRespones;
import com.zatgo.zup.activity.model.AddActivityBargainParams;
import com.zatgo.zup.activity.model.UpdateActivityBargainParams;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;

/**
 * Created by 46041 on 2019/3/12.
 */
public interface AdminService {

    PageInfo<ActivityBargain> getActivityBargainList(BargainListRequest request, ThirdLoginInfo info);

    void addActivityBargain(AddActivityBargainParams params, ThirdLoginInfo info);

    void updateActivityBargain(UpdateActivityBargainParams params, ThirdLoginInfo info);

    void offlineActivityBargain(String activityBargainId, ThirdLoginInfo info);

    void onlineActivityBargain(String activityBargainId, ThirdLoginInfo info);

    ActivityBargain getActivityBargain(String activityBargainId, ThirdLoginInfo info);

    PageInfo<ExamineListRespones> examineList(Integer pageNo, Integer pageSize, Byte status,
                                              ThirdLoginInfo info, String activityBargainId, String keyword);

    void execute(String id, Byte status, ThirdLoginInfo info);
}
