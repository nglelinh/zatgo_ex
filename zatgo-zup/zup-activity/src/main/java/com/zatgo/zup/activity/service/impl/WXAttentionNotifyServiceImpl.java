package com.zatgo.zup.activity.service.impl;

import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.activity.entity.ActivityBargainJoin;
import com.zatgo.zup.activity.entity.ActivityBargainList;
import com.zatgo.zup.activity.entity.ActivityBargainListExample;
import com.zatgo.zup.activity.entity.wx.WechatMsg;
import com.zatgo.zup.activity.entity.wx.WxUserInfoData;
import com.zatgo.zup.activity.mapper.ActivityBargainJoinMapper;
import com.zatgo.zup.activity.mapper.ActivityBargainListMapper;
import com.zatgo.zup.activity.mapper.ActivityBargainMapper;
import com.zatgo.zup.activity.remoteservice.WechatRemoteService;
import com.zatgo.zup.activity.service.InternalSystemService;
import com.zatgo.zup.activity.service.WXAttentionNotifyService;
import com.zatgo.zup.activity.service.WechatMsgReplyService;
import com.zatgo.zup.activity.utils.Xml2BeanUtils;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by 46041 on 2019/5/13.
 */

@Service
public class WXAttentionNotifyServiceImpl implements WXAttentionNotifyService {


    private static final Logger logger = LoggerFactory.getLogger(WXAttentionNotifyServiceImpl.class);

    private final static String grant_type = "client_credential";

    /**
     * 公共access_token 获取
     */
    private String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token";

    @Autowired
    private OkHttpService httpService;
    @Autowired
    private ActivityBargainListMapper activityBargainListMapper;
    @Autowired
    private ActivityBargainJoinMapper activityBargainJoinMapper;
    @Autowired
    private ActivityBargainMapper activityBargainMapper;
    @Autowired
    private InternalSystemService internalSystemService;

    @Value("${system.weixinOfficialAcctAppId}")
    private String weixinOfficialAcctAppId;
    @Value("${system.weixinOfficialAcctKey}")
    private String weixinOfficialAcctKey;
    @Value("${system.cloudUserId}")
    private String defaultCloudUserId;
    @Autowired
    private WechatMsgReplyService wechatMsgReplyService;

    @Autowired
    private WechatRemoteService wechatRemoteService;


    /**
     * 用户信息获取
     */
    private String GET_USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info";

    @Override
    @Transactional
    public String attention(String eventKey, String fromWXOpenId, String toWXOpenId) {
//        String userId = loginInfo.getUserId();
        if (!StringUtils.isEmpty(eventKey)){
            String[] split = eventKey.split("_");
            String cloudUserId = split[2];
            String activityJoinId = split[3];
            WxUserInfoData wxInfo = getWechatInfo(fromWXOpenId);
            ActivityBargainList activityBargainList = new ActivityBargainList();
            //先获取活动id
            ActivityBargainJoin join = activityBargainJoinMapper.selectByPrimaryKey(activityJoinId);
            String bargainActivityId = join.getBargainActivityId();
            //获取是否关注过
            String nick = activityBargainJoinMapper.getHelpUserNick(bargainActivityId, fromWXOpenId);
            if (StringUtils.isEmpty(nick)){
                //没助力过
                activityBargainList.setActivityBargainJoinId(activityJoinId);
                activityBargainList.setActivityBargainListId(UUIDUtils.getUuid());
                activityBargainList.setBargainMoney(BigDecimal.ZERO);
                activityBargainList.setCloudUserId(cloudUserId);
                activityBargainList.setCreateDate(new Date());
                activityBargainList.setWeixinOpenId(wxInfo.getOpenId());
                activityBargainList.setWechatNick(wxInfo.getNickName());
                activityBargainList.setWechatImg(wxInfo.getHeadimgUrl());
                activityBargainListMapper.insertSelective(activityBargainList);

                ActivityBargain activityBargain = activityBargainMapper.selectByPrimaryKey(bargainActivityId);
                Long activityCriterion = activityBargain.getActivityCriterion();
                ActivityBargainListExample activityBargainListExample = new ActivityBargainListExample();
                activityBargainListExample.createCriteria().andActivityBargainJoinIdEqualTo(activityJoinId);
                int i = activityBargainListMapper.countByExample(activityBargainListExample);
                if (i >= activityCriterion.intValue() && join.getStatus().intValue() == 0){
                    join.setStatus(new Byte("1"));
                    join.setUpdateTime(new Date());
                    activityBargainJoinMapper.updateByPrimaryKeySelective(join);
                    activityBargainMapper.addHelpPeople(1, bargainActivityId);
                    if (i == activityCriterion.intValue()){
                        internalSystemService.sendWechatSupportMsg(join.getUserId());
                    }

                }
            }
        }

        WechatMsg res = null;

        String reply = wechatMsgReplyService.selectWechatMsgReply(WechatMsg.MsgType.EVENT, WechatMsg.Event.SCAN,
                "qr_subscribe");

        if(!StringUtils.isEmpty(reply)) {
            res = new WechatMsg();
            res.setToUserName(fromWXOpenId);
            res.setFromUserName(toWXOpenId);
            res.setCreateTime(String.valueOf(new Date().getTime()));
            res.setMsgType(WechatMsg.MsgType.TEXT.getType());
            res.setContent(reply);
        }

        if(res != null) {
            return Xml2BeanUtils.convertToXml(res, "UTF-8");
        }

        return "success";
    }

    @Override
    public void cancel(ThirdLoginInfo loginInfo, String eventKey) {

    }

    @Override
    public WxUserInfoData getWechatInfo(String openId){
        ResponseData<WxUserInfoData> wxUserInfo = wechatRemoteService.getWxUserInfo(openId, defaultCloudUserId);
        if (wxUserInfo == null || !wxUserInfo.isSuccessful())
            throw new BusinessException(BusinessExceptionCode.GET_USER_INFO_FAIL);
        return wxUserInfo.getData();
    }

}
