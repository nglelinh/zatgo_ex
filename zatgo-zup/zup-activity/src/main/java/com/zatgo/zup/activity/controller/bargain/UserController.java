package com.zatgo.zup.activity.controller.bargain;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.controller.BaseController;
import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.activity.entity.ActivityBargainJoin;
import com.zatgo.zup.activity.entity.response.ActivityBargainInfoResponse;
import com.zatgo.zup.activity.model.BargainParams;
import com.zatgo.zup.activity.model.JoinActivityBargainParams;
import com.zatgo.zup.activity.service.UserService;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.HelpInfoRespone;
import com.zatgo.zup.common.model.NewActivityListRespones;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@Api(value = "/activity/user",description = "用户砍价功能接口")
@RestController
@RequestMapping(value = "/activity/user")
public class UserController extends BaseController {


	@Autowired
	private UserService userService;
	@Autowired
	private RedisLockUtils redisLockUtils;

	@RequestMapping(value = "/userJoinActivity/{joinId}",method = RequestMethod.POST)
	@ApiOperation(value = "根据参与ID查询参与的砍价活动", notes = "根据参与ID查询参与的砍价活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<ActivityBargainInfoResponse> getActivityBargainJoin(@PathVariable("joinId") String joinId) {
		ThirdLoginInfo authorization = getUserInfo();
		ActivityBargainInfoResponse activityBargainJoin = userService.getActivityBargainJoin(authorization, joinId);
		return BusinessResponseFactory.createSuccess(activityBargainJoin);
	}


	@RequestMapping(value = "/activity/info/{activityId}",method = RequestMethod.GET)
	@ApiOperation(value = "根据活动id查活动详情", notes = "根据活动id查活动详情", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData activityInfo(@PathVariable("activityId") String activityId) {
		ThirdLoginInfo authorization = getUserInfo();
		ActivityBargainInfoResponse activityBargainJoin = userService.activityInfo(authorization, activityId);
		return BusinessResponseFactory.createSuccess(activityBargainJoin);
	}


	@RequestMapping(value = "/bargain",method = RequestMethod.POST)
	@ApiOperation(value = "用户砍价", notes = "用户砍价", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<BigDecimal> bargain(@RequestBody BargainParams params) {
		ThirdLoginInfo authorization = getUserInfo();
		String bargainLock = RedisKeyConstants.BARGAIN_LOCK + params.getActivityBargainJoinId();
		BigDecimal bargain = BigDecimal.ZERO;
		redisLockUtils.lock(bargainLock);
		try{
			bargain = userService.bargain(authorization, params.getActivityBargainJoinId());
		} finally {
			redisLockUtils.releaseLock(bargainLock);
		}
		return BusinessResponseFactory.createSuccess(bargain);
	}

	@RequestMapping(value = "/join",method = RequestMethod.POST)
	@ApiOperation(value = "参与砍价活动", notes = "参与砍价活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData joinActivityBargain(@RequestBody JoinActivityBargainParams params) {
		ThirdLoginInfo userInfo = getUserInfo();
		ActivityBargainJoin join = userService.joinActivityBargain(params, userInfo);
		return BusinessResponseFactory.createSuccess(join);
	}

	@RequestMapping(value = "/helpInfo",method = RequestMethod.POST)
	@ApiOperation(value = "助力信息", notes = "助力信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData helpInfo(@RequestParam("bargainActivityJoinId") String bargainActivityJoinId) {
		ThirdLoginInfo userInfo = getUserInfo();
		HelpInfoRespone respone = userService.helpInfo(bargainActivityJoinId, userInfo);
		return BusinessResponseFactory.createSuccess(respone);
	}


	@RequestMapping(value = "/list",method = RequestMethod.GET)
	@ApiOperation(value = "活动列表", notes = "活动列表", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData activityList(@RequestParam (value = "pageNo", defaultValue = "1") Integer pageNo,
									 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
		ThirdLoginInfo userInfo = getUserInfo();
		PageInfo<NewActivityListRespones> pageInfo = userService.activityList(userInfo, pageNo, pageSize);
		return BusinessResponseFactory.createSuccess(pageInfo);
	}

	@RequestMapping(value = "/bargain/list",method = RequestMethod.GET)
	@ApiOperation(value = "砍价列表", notes = "砍价列表", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData bargainList(@RequestParam (value = "pageNo", defaultValue = "1") Integer pageNo,
									 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
		ThirdLoginInfo userInfo = getUserInfo();
		PageInfo<ActivityBargain> pageInfo = userService.bargainList(userInfo, pageNo, pageSize);
		return BusinessResponseFactory.createSuccess(pageInfo);
	}
}
