package com.zatgo.zup.activity.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class JoinActivityBargainParams {

	@ApiModelProperty(value = "活动ID",required = true)
	private String bargainActivityId;

	@ApiModelProperty(value = "时间戳",required = true)
	private Date date;

	@ApiModelProperty(value = "扩展信息",required = true)
	private String extInfo;
	
	public String getBargainActivityId() {
		return bargainActivityId;
	}

	public void setBargainActivityId(String bargainActivityId) {
		this.bargainActivityId = bargainActivityId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getExtInfo() {
		return extInfo;
	}

	public void setExtInfo(String extInfo) {
		this.extInfo = extInfo;
	}
}
