package com.zatgo.zup.activity.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.entity.GroupBookingActivity;
import com.zatgo.zup.activity.entity.GroupOrder;
import com.zatgo.zup.activity.entity.GroupUserOrder;
import com.zatgo.zup.activity.entity.request.AdminActivityListRequest;
import com.zatgo.zup.activity.entity.request.GroupOrderDetailListInfoRequest;
import com.zatgo.zup.activity.entity.request.GroupOrderMergeRequest;
import com.zatgo.zup.activity.entity.response.ActivityGroupBookingInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupInfoResponse;
import com.zatgo.zup.activity.model.AddGroupBookingActivityParams;
import com.zatgo.zup.activity.model.UpdateGroupBookingActivityParams;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;

/**
 * Created by 46041 on 2019/3/15.
 */
public interface GroupBookingAdminService {


    void addGroupBooking(AddGroupBookingActivityParams params, ThirdLoginInfo info);

    void updateGroupBooking(UpdateGroupBookingActivityParams params, ThirdLoginInfo info);

    void onlineActivity(String groupActivityId, ThirdLoginInfo info);

    void offlineActivity(String groupActivityId, ThirdLoginInfo info);

    PageInfo<GroupBookingActivity> activityList(ThirdLoginInfo info, AdminActivityListRequest request);

    void updateActivityOrder(GroupOrder groupOrder);

    PageInfo<GroupInfoResponse> activityDetailList(String activityId, Integer pageNo, Integer pageSize, Integer status);

    PageInfo<GroupUserOrder> activityDetailListInfo(GroupOrderDetailListInfoRequest request);

    ActivityGroupBookingInfoResponse activityDetailInfo(String activityId);

    void autoRefund(String groupOrderId, String groupOrderUserId);

    void merge(GroupOrderMergeRequest request);
}
