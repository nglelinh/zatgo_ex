package com.zatgo.zup.activity.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GroupOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GroupOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGroupOrderIdIsNull() {
            addCriterion("group_order_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdIsNotNull() {
            addCriterion("group_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdEqualTo(String value) {
            addCriterion("group_order_id =", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdNotEqualTo(String value) {
            addCriterion("group_order_id <>", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdGreaterThan(String value) {
            addCriterion("group_order_id >", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("group_order_id >=", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdLessThan(String value) {
            addCriterion("group_order_id <", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdLessThanOrEqualTo(String value) {
            addCriterion("group_order_id <=", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdLike(String value) {
            addCriterion("group_order_id like", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdNotLike(String value) {
            addCriterion("group_order_id not like", value, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdIn(List<String> values) {
            addCriterion("group_order_id in", values, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdNotIn(List<String> values) {
            addCriterion("group_order_id not in", values, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdBetween(String value1, String value2) {
            addCriterion("group_order_id between", value1, value2, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderIdNotBetween(String value1, String value2) {
            addCriterion("group_order_id not between", value1, value2, "groupOrderId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdIsNull() {
            addCriterion("group_activity_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdIsNotNull() {
            addCriterion("group_activity_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdEqualTo(String value) {
            addCriterion("group_activity_id =", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdNotEqualTo(String value) {
            addCriterion("group_activity_id <>", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdGreaterThan(String value) {
            addCriterion("group_activity_id >", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdGreaterThanOrEqualTo(String value) {
            addCriterion("group_activity_id >=", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdLessThan(String value) {
            addCriterion("group_activity_id <", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdLessThanOrEqualTo(String value) {
            addCriterion("group_activity_id <=", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdLike(String value) {
            addCriterion("group_activity_id like", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdNotLike(String value) {
            addCriterion("group_activity_id not like", value, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdIn(List<String> values) {
            addCriterion("group_activity_id in", values, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdNotIn(List<String> values) {
            addCriterion("group_activity_id not in", values, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdBetween(String value1, String value2) {
            addCriterion("group_activity_id between", value1, value2, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupActivityIdNotBetween(String value1, String value2) {
            addCriterion("group_activity_id not between", value1, value2, "groupActivityId");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusIsNull() {
            addCriterion("group_order_status is null");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusIsNotNull() {
            addCriterion("group_order_status is not null");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusEqualTo(Byte value) {
            addCriterion("group_order_status =", value, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusNotEqualTo(Byte value) {
            addCriterion("group_order_status <>", value, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusGreaterThan(Byte value) {
            addCriterion("group_order_status >", value, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("group_order_status >=", value, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusLessThan(Byte value) {
            addCriterion("group_order_status <", value, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusLessThanOrEqualTo(Byte value) {
            addCriterion("group_order_status <=", value, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusIn(List<Byte> values) {
            addCriterion("group_order_status in", values, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusNotIn(List<Byte> values) {
            addCriterion("group_order_status not in", values, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusBetween(Byte value1, Byte value2) {
            addCriterion("group_order_status between", value1, value2, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupOrderStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("group_order_status not between", value1, value2, "groupOrderStatus");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateIsNull() {
            addCriterion("group_start_date is null");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateIsNotNull() {
            addCriterion("group_start_date is not null");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateEqualTo(Date value) {
            addCriterion("group_start_date =", value, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateNotEqualTo(Date value) {
            addCriterion("group_start_date <>", value, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateGreaterThan(Date value) {
            addCriterion("group_start_date >", value, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("group_start_date >=", value, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateLessThan(Date value) {
            addCriterion("group_start_date <", value, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateLessThanOrEqualTo(Date value) {
            addCriterion("group_start_date <=", value, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateIn(List<Date> values) {
            addCriterion("group_start_date in", values, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateNotIn(List<Date> values) {
            addCriterion("group_start_date not in", values, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateBetween(Date value1, Date value2) {
            addCriterion("group_start_date between", value1, value2, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupStartDateNotBetween(Date value1, Date value2) {
            addCriterion("group_start_date not between", value1, value2, "groupStartDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateIsNull() {
            addCriterion("group_end_date is null");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateIsNotNull() {
            addCriterion("group_end_date is not null");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateEqualTo(Date value) {
            addCriterion("group_end_date =", value, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateNotEqualTo(Date value) {
            addCriterion("group_end_date <>", value, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateGreaterThan(Date value) {
            addCriterion("group_end_date >", value, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("group_end_date >=", value, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateLessThan(Date value) {
            addCriterion("group_end_date <", value, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateLessThanOrEqualTo(Date value) {
            addCriterion("group_end_date <=", value, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateIn(List<Date> values) {
            addCriterion("group_end_date in", values, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateNotIn(List<Date> values) {
            addCriterion("group_end_date not in", values, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateBetween(Date value1, Date value2) {
            addCriterion("group_end_date between", value1, value2, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andGroupEndDateNotBetween(Date value1, Date value2) {
            addCriterion("group_end_date not between", value1, value2, "groupEndDate");
            return (Criteria) this;
        }

        public Criteria andIsValidIsNull() {
            addCriterion("is_valid is null");
            return (Criteria) this;
        }

        public Criteria andIsValidIsNotNull() {
            addCriterion("is_valid is not null");
            return (Criteria) this;
        }

        public Criteria andIsValidEqualTo(Boolean value) {
            addCriterion("is_valid =", value, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidNotEqualTo(Boolean value) {
            addCriterion("is_valid <>", value, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidGreaterThan(Boolean value) {
            addCriterion("is_valid >", value, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_valid >=", value, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidLessThan(Boolean value) {
            addCriterion("is_valid <", value, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidLessThanOrEqualTo(Boolean value) {
            addCriterion("is_valid <=", value, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidIn(List<Boolean> values) {
            addCriterion("is_valid in", values, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidNotIn(List<Boolean> values) {
            addCriterion("is_valid not in", values, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidBetween(Boolean value1, Boolean value2) {
            addCriterion("is_valid between", value1, value2, "isValid");
            return (Criteria) this;
        }

        public Criteria andIsValidNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_valid not between", value1, value2, "isValid");
            return (Criteria) this;
        }

        public Criteria andActivityUrlIsNull() {
            addCriterion("activity_url is null");
            return (Criteria) this;
        }

        public Criteria andActivityUrlIsNotNull() {
            addCriterion("activity_url is not null");
            return (Criteria) this;
        }

        public Criteria andActivityUrlEqualTo(String value) {
            addCriterion("activity_url =", value, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlNotEqualTo(String value) {
            addCriterion("activity_url <>", value, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlGreaterThan(String value) {
            addCriterion("activity_url >", value, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlGreaterThanOrEqualTo(String value) {
            addCriterion("activity_url >=", value, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlLessThan(String value) {
            addCriterion("activity_url <", value, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlLessThanOrEqualTo(String value) {
            addCriterion("activity_url <=", value, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlLike(String value) {
            addCriterion("activity_url like", value, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlNotLike(String value) {
            addCriterion("activity_url not like", value, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlIn(List<String> values) {
            addCriterion("activity_url in", values, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlNotIn(List<String> values) {
            addCriterion("activity_url not in", values, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlBetween(String value1, String value2) {
            addCriterion("activity_url between", value1, value2, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andActivityUrlNotBetween(String value1, String value2) {
            addCriterion("activity_url not between", value1, value2, "activityUrl");
            return (Criteria) this;
        }

        public Criteria andIsPrivateIsNull() {
            addCriterion("is_private is null");
            return (Criteria) this;
        }

        public Criteria andIsPrivateIsNotNull() {
            addCriterion("is_private is not null");
            return (Criteria) this;
        }

        public Criteria andIsPrivateEqualTo(Boolean value) {
            addCriterion("is_private =", value, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateNotEqualTo(Boolean value) {
            addCriterion("is_private <>", value, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateGreaterThan(Boolean value) {
            addCriterion("is_private >", value, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_private >=", value, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateLessThan(Boolean value) {
            addCriterion("is_private <", value, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateLessThanOrEqualTo(Boolean value) {
            addCriterion("is_private <=", value, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateIn(List<Boolean> values) {
            addCriterion("is_private in", values, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateNotIn(List<Boolean> values) {
            addCriterion("is_private not in", values, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateBetween(Boolean value1, Boolean value2) {
            addCriterion("is_private between", value1, value2, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andIsPrivateNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_private not between", value1, value2, "isPrivate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}