package com.zatgo.zup.activity.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.enumType.BusinessEnum.ActivityBargainStatus;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductGroup;
import com.zatgo.zup.activity.entity.*;
import com.zatgo.zup.activity.entity.response.ActivityBargainInfoResponse;
import com.zatgo.zup.activity.entity.wx.WxUserInfoData;
import com.zatgo.zup.activity.mapper.*;
import com.zatgo.zup.activity.model.JoinActivityBargainParams;
import com.zatgo.zup.activity.remoteservice.TradeApiRemoteService;
import com.zatgo.zup.activity.service.CommonService;
import com.zatgo.zup.activity.service.ThirdService;
import com.zatgo.zup.activity.service.UserService;
import com.zatgo.zup.activity.service.WXAttentionNotifyService;
import com.zatgo.zup.activity.utils.BargainUtil;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.ActivityListRespones;
import com.zatgo.zup.common.model.HelpInfoRespone;
import com.zatgo.zup.common.model.NewActivityListRespones;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.model.wx.ThirdWxInfo;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/3/11.
 */

@Service("userService")
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);


    @Autowired
    private ActivityBargainJoinMapper activityBargainJoinMapper;
    @Autowired
    private ActivityBargainMapper activityBargainMapper;
    @Autowired
    private ActivityBargainListMapper activityBargainListMapper;
    @Autowired
    private WXAttentionNotifyService wxAttentionNotifyService;
    @Autowired
    private GroupUserOrderMapper groupUserOrderMapper;
    @Autowired
    private TradeApiRemoteService tradeApiRemoteService;



    @Override
    public ActivityBargainInfoResponse getActivityBargainJoin(ThirdLoginInfo authorization, String joinId) {
        ActivityBargainInfoResponse response = new ActivityBargainInfoResponse();
        ActivityBargainJoinExample example = new ActivityBargainJoinExample();
        example.createCriteria().andActivityBargainJoinIdEqualTo(joinId);
        List<ActivityBargainJoin> activityBargainJoins = activityBargainJoinMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(activityBargainJoins)){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_NOT_EXIST);
        }
        ActivityBargainJoin join = activityBargainJoins.get(0);
        ActivityBargain activityBargain = activityBargainMapper.selectByPrimaryKey(join.getBargainActivityId());
        String priceIds = activityBargain.getPriceIds();
        if (!StringUtils.isEmpty(priceIds)){
            List<Long> list = JSONArray.parseArray(priceIds, Long.class);
            if (!CollectionUtils.isEmpty(list)){
                Long id = list.get(0);
                ResponseData<PmsProductGroup> travelGroupInfo = tradeApiRemoteService.getTravelGroupInfo(id);
                if (travelGroupInfo != null && travelGroupInfo.isSuccessful())
                    response.setDepartureTime(travelGroupInfo.getData().getCollectionTime());
            }
        }
        if (activityBargain == null){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_NOT_EXIST);
        }
        response.setActivityBargain(activityBargain);
        ActivityBargainListExample activityBargainListExample = new ActivityBargainListExample();
        activityBargainListExample.createCriteria().andActivityBargainJoinIdEqualTo(joinId);
        List<ActivityBargainList> activityBargainLists = activityBargainListMapper.selectByExample(activityBargainListExample);
        response.setCloudUserProductId(activityBargain.getProductId());
        response.setActivityBargainJoin(join);
        response.setList(activityBargainLists);
        return response;
    }

    @Override
    @Transactional
    public BigDecimal bargain(ThirdLoginInfo authorization, String joinId) {
        String userId = authorization.getUserId();
        String wechatOpenId = authorization.getWxInfo().getOpenId();
        //先验joinId
        ActivityBargainJoinExample example = new ActivityBargainJoinExample();
        example.createCriteria().andActivityBargainJoinIdEqualTo(joinId);
        List<ActivityBargainJoin> activityBargainJoins = activityBargainJoinMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(activityBargainJoins)){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_NOT_EXIST);
        }
        ActivityBargainJoin join = activityBargainJoins.get(0);
        String bargainActivityId = join.getBargainActivityId();
        ActivityBargain activityBargain = activityBargainMapper.selectByPrimaryKey(bargainActivityId);
        //验这个单子有没有付过钱
        if (join.getPayStatus() && activityBargain.getPayType().equals(new Byte("1")))
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_NOT_EXIST);
        //再验这个用户是否砍过了
        ActivityBargainListExample activityBargainListExample = new ActivityBargainListExample();
        activityBargainListExample.createCriteria().andUserIdEqualTo(userId).andActivityBargainJoinIdEqualTo(joinId);
        List<ActivityBargainList> activityBargainLists = activityBargainListMapper.selectByExample(activityBargainListExample);
        if (!CollectionUtils.isEmpty(activityBargainLists)){
            throw new BusinessException(BusinessExceptionCode.YOU_CAN_NOT_CUT_AGAIN);
        }
        int size = join.getBargainCount();
        int joinCount = activityBargain.getJoinCount();
        Boolean removeLimit = activityBargain.getRemoveLimit();
        //看看能不能砍
        if (joinCount < size && !removeLimit)
            throw new BusinessException(BusinessExceptionCode.OVER_ACTIVITY_NUMBER_LIMIT);
        //可以砍，就保存记录
        int i = activityBargain.getJoinCount().intValue() - 1 - size;
        BigDecimal amount = null;
        if (i > 0){
            amount = BargainUtil.getAmount(activityBargain.getActivityStartMoney(), activityBargain.getActivityMoney(), activityBargain.getJoinCount());
        }
        if (i == 0){
            amount = activityBargain.getActivityStartMoney().add(activityBargain.getActivityMoney().negate()).add(join.getDealMoney().negate());
        }
        if (i < 0){
            amount = activityBargain.getActivityStartMoney().add(join.getDealMoney().negate()).multiply(activityBargain.getBargainRate()).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        }

        ActivityBargainList list = new ActivityBargainList();
        list.setActivityBargainListId(UUIDUtils.getUuid());
        list.setActivityBargainJoinId(joinId);
        list.setBargainMoney(amount);
        list.setCreateDate(new Date());
        list.setUserId(userId);
        list.setWeixinOpenId(wechatOpenId);
        activityBargainListMapper.insertSelective(list);
        activityBargainJoinMapper.updateBargainCount(joinId, amount.negate());
        return amount;
    }


    @Override
    public ActivityBargainJoin joinActivityBargain(JoinActivityBargainParams params, ThirdLoginInfo userInfo) {
        String bargainActivityId = params.getBargainActivityId();
        Date date = params.getDate();
        String cloudUserId = userInfo.getCloudUserId();
        String extInfo = params.getExtInfo();
        String userId = userInfo.getUserId();
        ThirdWxInfo wxInfo = userInfo.getWxInfo();
        //先判断活动是否存在
        ActivityBargainExample example = new ActivityBargainExample();
        example.createCriteria().andBargainActivityIdEqualTo(bargainActivityId)
                .andCloudUserIdEqualTo(cloudUserId)
                .andActivityStartDateLessThanOrEqualTo(date)
                .andActivityEndDateGreaterThan(date);
        List<ActivityBargain> activityBargains = activityBargainMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(activityBargains))
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_NOT_EXIST);
        //判断是否正在参加此活动
        ActivityBargain activityBargain = activityBargains.get(0);
        ActivityBargainJoinExample activityBargainJoinExample = new ActivityBargainJoinExample();
        activityBargainJoinExample.createCriteria()
                .andCloudUserIdEqualTo(cloudUserId)
                .andBargainActivityIdEqualTo(bargainActivityId)
                .andUserIdEqualTo(userId)
                .andCreateTimeGreaterThan(DateTimeUtils.addDateSomeMinute(new Date(),-activityBargain.getTimeOut().intValue()))
                .andStatusIn(Arrays.asList(new Byte("0"), new Byte("1"), new Byte("2")));
        int i = activityBargainJoinMapper.countByExample(activityBargainJoinExample);
        if (i > 0){
            throw new BusinessException(BusinessExceptionCode.THIS_ACTIVITY_IS_UNDER_WAY);
        }
        if (activityBargain.getProductStockTotal().intValue() - activityBargain.getJoinCount().intValue() < 1){
            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "库存不足");
        }

        ResponseData<PmsProduct> productInfo = tradeApiRemoteService.getProductInfo(Long.valueOf(activityBargain.getProductId()));
        if (productInfo == null || !productInfo.isSuccessful()){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_PRODUCT_NOT_EXIST);
        }
        PmsProduct activityProduct = productInfo.getData();

        ActivityBargainJoin join = new ActivityBargainJoin();
        String id = MD5.MD5(bargainActivityId + userId + date.getTime());
        Date createDate = new Date();
        //加入砍价活动
        BigDecimal productMoney = activityProduct.getPrice();
//        BigDecimal amount = BargainUtil.getAmount(activityBargain.getActivityStartMoney(), activityBargain.getActivityMoney(), activityBargain.getJoinCount());
        join.setUserId(userId);
        join.setActivityBargainJoinId(id);
        join.setProductName(activityBargain.getProductName());
        join.setProductIntro(activityBargain.getProductIntro());
        join.setProductId(activityBargain.getProductId());
        join.setProductMoney(productMoney);
        join.setBargainCount(1);
        join.setStatus(new Byte("2"));
        join.setCreateTime(createDate);
        join.setDealMoney(productMoney);
        join.setBargainActivityId(activityBargain.getBargainActivityId());
        join.setExtInfo(extInfo);
        join.setCloudUserId(cloudUserId);
        join.setUserNick(wxInfo.getName());
        join.setUserImg(wxInfo.getHeadUrl());
        join.setUpdateTime(date);
        join.setPayTime(new Date(date.getTime() + activityBargain.getTimeOut().longValue() * 3600 * 1000l));
        activityBargainJoinMapper.insertSelective(join);
//        //自动砍一刀
//        ActivityBargainList list = new ActivityBargainList();
//        list.setActivityBargainListId(UUIDUtils.getUuid());
//        list.setActivityBargainJoinId(join.getActivityBargainJoinId());
//        list.setBargainMoney(amount);
//        list.setCreateDate(createDate);
//        list.setUserId(userId);
//        list.setWeixinOpenId(wechatOpenId);
//        activityBargainListMapper.insertSelective(list);
        return join;
    }

    @Override
    public ActivityBargainInfoResponse activityInfo(ThirdLoginInfo authorization, String activityId) {
        ActivityBargainInfoResponse response = new ActivityBargainInfoResponse();
        ActivityBargain activityBargain = activityBargainMapper.selectByPrimaryKey(activityId);
        if (activityBargain == null){
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_NOT_EXIST);
        }

        if(!ActivityBargainStatus.Online.getCode().equals(activityBargain.getStatus())) {
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_OFFLINE);
        }

        response.setActivityBargain(activityBargain);
        String cloudUserId = authorization.getCloudUserId();
        ActivityBargainJoinExample example = new ActivityBargainJoinExample();
        example.setOrderByClause("create_time desc");
        example.createCriteria().andCloudUserIdEqualTo(cloudUserId).andBargainActivityIdEqualTo(activityId).andUserIdEqualTo(authorization.getUserId());
        List<ActivityBargainJoin> activityBargainJoins = activityBargainJoinMapper.selectByExample(example);
        if (activityBargainJoins != null && !CollectionUtils.isEmpty(activityBargainJoins)){
            ActivityBargainJoin join = activityBargainJoins.get(0);

            //暂时修改一下支付超时的状态
            if(join.getCreateTime().compareTo(DateTimeUtils.addDateSomeMinute(new Date(),-activityBargain.getTimeOut().intValue())) < 0){
                join.setStatus(Byte.valueOf("3"));
            }
            response.setActivityBargainJoin(join);
            ActivityBargainListExample activityBargainListExample = new ActivityBargainListExample();
            activityBargainListExample.createCriteria().andActivityBargainJoinIdEqualTo(join.getActivityBargainJoinId());
            List<ActivityBargainList> activityBargainLists = activityBargainListMapper.selectByExample(activityBargainListExample);
            response.setList(activityBargainLists);
        }
        String priceIds = activityBargain.getPriceIds();
        if (!StringUtils.isEmpty(priceIds)){
            List<Long> list = JSONArray.parseArray(priceIds, Long.class);
            if (!CollectionUtils.isEmpty(list)){
                Long id = list.get(0);
                ResponseData<PmsProductGroup> travelGroupInfo = tradeApiRemoteService.getTravelGroupInfo(id);
                if (travelGroupInfo != null && travelGroupInfo.isSuccessful())
                    response.setDepartureTime(travelGroupInfo.getData().getCollectionTime());
            }
        }
        response.setCloudUserProductId(activityBargain.getProductId());
        return response;
    }

    @Override
    public PageInfo<NewActivityListRespones> activityList(ThirdLoginInfo userInfo, Integer pageNo, Integer pageSize) {
        String cloudUserId = userInfo.getCloudUserId();
        String userId = userInfo.getUserId();
        PageHelper.startPage(pageNo, pageSize);
        List<NewActivityListRespones> groupList = groupUserOrderMapper.getActivityList(userId);
        for (NewActivityListRespones newActivityListRespones : groupList) {
            newActivityListRespones.setActivityType("2");
        }
        PageInfo<NewActivityListRespones> newActivityListResponesPageInfo = new PageInfo<>(groupList);
        PageHelper.startPage(pageNo, pageSize);
        List<ActivityListRespones> list = activityBargainMapper.myActivity(cloudUserId, userId);
        if (!CollectionUtils.isEmpty(list)){
            for (ActivityListRespones respones : list) {
                NewActivityListRespones res = new NewActivityListRespones();
                res.setActivityId(respones.getBargainActivityId());
                res.setActivityCreateTime(respones.getActivityJoinTime());
                res.setActivityImage(respones.getImgs());
                res.setActivityJoinId(respones.getActivityBargainJoinId());
                res.setActivityName(respones.getActivityName());
                res.setActivityType("1");
                res.setActivityDesc(respones.getProductDesc());
                res.setActivityStatus(respones.getActivityStatus().toString());
                groupList.add(res);
            }
            groupList.sort((a,b) -> b.getActivityCreateTime().compareTo(a.getActivityCreateTime()) );
            newActivityListResponesPageInfo.setList(groupList);
        }
        return newActivityListResponesPageInfo;
    }

    @Override
    public HelpInfoRespone helpInfo(String bargainActivityJoinId, ThirdLoginInfo userInfo) {
        String openId = userInfo.getWxInfo().getOpenId();
        ActivityBargainJoin join = activityBargainJoinMapper.selectByPrimaryKey(bargainActivityJoinId);
        String bargainActivityId = join.getBargainActivityId();
        String nick = activityBargainJoinMapper.getHelpUserNick(bargainActivityId, openId);
        HelpInfoRespone res = new HelpInfoRespone();
        if (StringUtils.isEmpty(nick)){
            res.setHelp(false);
        } else {
            res.setHelp(true);
            res.setUserNick(nick);
        }
        res.setSubscribe(false);
        try {
            WxUserInfoData wechatInfo = wxAttentionNotifyService.getWechatInfo(openId);
            if (wechatInfo != null){
                Integer subscribe = wechatInfo.getSubscribe();
                if (subscribe != null && subscribe.intValue() == 1){
                    res.setSubscribe(true);
                }
            }
        } catch (Exception e){
            logger.error("", e);
        }

        return res;
    }

    @Override
    public PageInfo<ActivityBargain> bargainList(ThirdLoginInfo userInfo, Integer pageNo, Integer pageSize) {
        ActivityBargainExample example = new ActivityBargainExample();
        example.setOrderByClause("activity_end_date asc");
        example.createCriteria().andCloudUserIdEqualTo(userInfo.getCloudUserId()).andActivityEndDateGreaterThan(new Date()).andStatusEqualTo(new Byte("1"));
        PageHelper.startPage(pageNo, pageSize);
        List<ActivityBargain> activityBargains = activityBargainMapper.selectByExample(example);
        return new PageInfo<>(activityBargains);
    }
}
