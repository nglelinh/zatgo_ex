package com.zatgo.zup.activity.entity.response;

import java.util.Date;

/**
 * Created by 46041 on 2019/6/20.
 */
public class GroupInfoResponse {
    /**
     * 头像
     */
    private String icon;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 总数
     */
    private Integer count;
    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 开始时间
     */
    private Date endTime;
    /**
     * 拼团id
     */
    private String groupOrderId;

    /**
     * 订单状态
     */
    private String orderStatus;

    private String userId;

    private Boolean isSelf;


    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getGroupOrderId() {
        return groupOrderId;
    }

    public void setGroupOrderId(String groupOrderId) {
        this.groupOrderId = groupOrderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getSelf() {
        return isSelf;
    }

    public void setSelf(Boolean self) {
        isSelf = self;
    }
}
