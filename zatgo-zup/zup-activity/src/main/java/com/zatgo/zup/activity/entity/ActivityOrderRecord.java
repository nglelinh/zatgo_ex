package com.zatgo.zup.activity.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ActivityOrderRecord {
    private String orderId;

    private String cloudUserId;

    private String activityBargainJoinId;

    private String userId;

    private BigDecimal orderMoney;

    private Date createDate;

    private String id;

    private Byte status;

    private String activityBargainId;

    private String orderType;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getActivityBargainJoinId() {
        return activityBargainJoinId;
    }

    public void setActivityBargainJoinId(String activityBargainJoinId) {
        this.activityBargainJoinId = activityBargainJoinId == null ? null : activityBargainJoinId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public BigDecimal getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(BigDecimal orderMoney) {
        this.orderMoney = orderMoney;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getActivityBargainId() {
        return activityBargainId;
    }

    public void setActivityBargainId(String activityBargainId) {
        this.activityBargainId = activityBargainId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
}