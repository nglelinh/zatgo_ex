package com.zatgo.zup.activity.mapper;

import com.zatgo.zup.activity.entity.CloudUserAbbr;

public interface CloudUserAbbrMapper extends BaseMapper<CloudUserAbbr>{
}