package com.zatgo.zup.activity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.enumType.BusinessEnum;
import com.zatgo.zup.activity.entity.*;
import com.zatgo.zup.activity.entity.request.SyncProductRequest;
import com.zatgo.zup.activity.mapper.*;
import com.zatgo.zup.activity.model.SyncProductParams;
import com.zatgo.zup.activity.service.CommonService;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by 46041 on 2019/3/19.
 */


@Service("commonService")
public class CommonServicImpl implements CommonService {


    @Autowired
    private ActivityOrderRecordMapper activityOrderRecordMapper;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private UserAbbrMapper userAbbrMapper;
    @Autowired
    private ActivityProductMapper activityProductMapper;
    @Autowired
    private ActivityBargainMapper activityBargainMapper;
    @Autowired
    private GroupBookingActivityMapper groupBookingActivityMapper;
    @Autowired
    private GroupUserOrderMapper groupUserOrderMapper;
    @Autowired
    private GroupOrderMapper groupOrderMapper;


    @Override
    public void syncProduct(SyncProductRequest params) {
        List<SyncProductParams> products = params.getProducts();
        String cloudUserId = params.getCloudUserId();
        if (!CollectionUtils.isEmpty(products)){
            List<ActivityProduct> list = new ArrayList<>(products.size());
            Date date = new Date();
            for (SyncProductParams product : products) {
                ActivityProduct activityProduct = new ActivityProduct();
                activityProduct.setProductId(UUIDUtils.getUuid());
                activityProduct.setCreateDate(date);
                activityProduct.setAddOrderUrl(product.getAddOrderUrl());
                activityProduct.setCloudUserId(cloudUserId);
                activityProduct.setUpdateDate(date);
                activityProduct.setProductName(product.getProductName());
                activityProduct.setCloudUserProductId(product.getProductId());
                activityProduct.setProductImgAbbrUrl(product.getProductImgAbbrUrl());
                activityProduct.setProductMoney(product.getProductMoney());
                activityProduct.setPromotionPrice(product.getPromotionPrice());
                list.add(activityProduct);
            }
            activityOrderRecordMapper.syncProduct(list);
        }
    }

//    @Override
//    public UserAbbr getUser(UserAbbr info) {
//        String cloudUserId = info.getCloudUserId();
//        String weixinOfficialAcctAppId = info.getWeixinOpenId();
//        String userId = info.getUserId();
//        String busiUserId = info.getBusiUserId();
//        UserAbbrExample example = new UserAbbrExample();
//        UserAbbrExample.Criteria criteria = example.createCriteria();
//        if (!StringUtils.isEmpty(cloudUserId)){
//            criteria.andCloudUserIdEqualTo(cloudUserId);
//        }
//        if (!StringUtils.isEmpty(weixinOfficialAcctAppId)){
//            criteria.andWeixinOpenIdEqualTo(weixinOfficialAcctAppId);
//        }
//        if (!StringUtils.isEmpty(userId)){
//            criteria.andUserIdEqualTo(userId);
//        }
//        if (!StringUtils.isEmpty(busiUserId)){
//            criteria.andBusiUserIdEqualTo(busiUserId);
//        }
//        List<UserAbbr> userAbbrs = null;
//        String createUserLock = RedisKeyConstants.BARGAIN_CREATE_USER_LOCK + userId + weixinOfficialAcctAppId;
//        redisLockUtils.lock(createUserLock);
//        try {
//            userAbbrs = userAbbrMapper.selectByExample(example);
//            //用户不存在，则创建一个
//            if (CollectionUtils.isEmpty(userAbbrs)) {
//                userAbbrs = new ArrayList<>();
//                info.setUserId(UUIDUtils.getUuid());
//                userAbbrMapper.insertSelective(info);
//                userAbbrs.add(info);
//            } else {
//                //存在，则更新
//                UserAbbr userAbbr = userAbbrs.get(0);
//                userAbbr.setWeixinOpenId(weixinOfficialAcctAppId);
//                userAbbr.setBusiUserId(busiUserId);
//                userAbbr.setCloudUserId(cloudUserId);
//                userAbbrMapper.updateByPrimaryKeySelective(userAbbr);
//            }
//        } finally {
//            redisLockUtils.releaseLock(createUserLock);
//        }
//        return userAbbrs.get(0);
//    }

    @Override
    public PageInfo<ActivityProduct> productList(String productName, Integer pageNo, Integer pageSize, ThirdLoginInfo userInfo) {
        String cloudUserId = userInfo.getCloudUserId();
        ActivityProductExample example = new ActivityProductExample();
        ActivityProductExample.Criteria criteria = example.createCriteria().andCloudUserIdEqualTo(cloudUserId);
        if (!StringUtils.isEmpty(productName)){
            criteria.andProductNameLike("%" + productName + "%");
        }
        PageHelper.startPage(pageNo, pageSize);
        return new PageInfo<>(activityProductMapper.selectByExample(example));
    }

    @Override
    public Map<String, Long> getPayTime(String ids) {
        Map<String, Long> map = new HashMap<>();
        String[] split = ids.split(",");
        List<String> idList = Arrays.asList(split);
        ActivityBargainExample activityBargainExample = new ActivityBargainExample();
        activityBargainExample.createCriteria().andBargainActivityIdIn(idList);
        List<ActivityBargain> activityBargains = activityBargainMapper.selectByExample(activityBargainExample);
        GroupBookingActivityExample groupBookingActivityExample = new GroupBookingActivityExample();
        groupBookingActivityExample.createCriteria().andGroupActivityIdIn(idList);
        List<GroupBookingActivity> groupBookingActivities = groupBookingActivityMapper.selectByExample(groupBookingActivityExample);
        if (!CollectionUtils.isEmpty(activityBargains)){
            for (ActivityBargain activityBargain : activityBargains) {
                map.put(activityBargain.getBargainActivityId(), activityBargain.getTimeOut());
            }
        }
        if (!CollectionUtils.isEmpty(groupBookingActivities)){
            for (GroupBookingActivity groupBookingActivity : groupBookingActivities) {
                map.put(groupBookingActivity.getGroupActivityId(), groupBookingActivity.getPayTime());
            }
        }
        return map;
    }

    @Override
    public String getActivityId(String joinId) {
        String activityId = null;
        GroupUserOrder groupUserOrder = groupUserOrderMapper.selectByPrimaryKey(joinId);
        if (groupUserOrder != null){
            String groupOrderId = groupUserOrder.getGroupOrderId();
            GroupOrder groupOrder = groupOrderMapper.selectByPrimaryKey(groupOrderId);
            if (groupOrder != null){
                activityId = groupOrder.getGroupActivityId();
            }
        }
        return activityId;
    }

    @Override
    public Long getActiviTypayTime(String activityId, String orderType) {
        if (BusinessEnum.CreateOrderType.BARGAIN.getCode().equals(orderType)){
            ActivityBargain activityBargain = activityBargainMapper.selectByPrimaryKey(activityId);
            return activityBargain.getTimeOut();
        }
        if (BusinessEnum.CreateOrderType.GROUPBOOKING.getCode().equals(orderType)){
            GroupBookingActivity activity = groupBookingActivityMapper.selectByPrimaryKey(activityId);
            return activity.getPayTime();
        }
        return null;
    }
}
