package com.zatgo.zup.activity.model;

import io.swagger.annotations.ApiModelProperty;

public class BargainParams {

	@ApiModelProperty(value = "被砍的实例ID",required = true)
	private String activityBargainJoinId;

	public String getActivityBargainJoinId() {
		return activityBargainJoinId;
	}

	public void setActivityBargainJoinId(String activityBargainJoinId) {
		this.activityBargainJoinId = activityBargainJoinId;
	}
	
	
}
