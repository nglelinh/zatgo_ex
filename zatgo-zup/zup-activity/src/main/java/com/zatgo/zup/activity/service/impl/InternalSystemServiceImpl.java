package com.zatgo.zup.activity.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.OmsOrder;
import com.zatgo.zup.activity.entity.*;
import com.zatgo.zup.activity.mapper.*;
import com.zatgo.zup.activity.remoteservice.TradeApiRemoteService;
import com.zatgo.zup.activity.remoteservice.WechatRemoteService;
import com.zatgo.zup.activity.service.CommonService;
import com.zatgo.zup.activity.service.InternalSystemService;
import com.zatgo.zup.activity.service.UserService;
import com.zatgo.zup.activity.utils.HttpUtil;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/3/12.
 */

@Service("internalSystemService")
public class InternalSystemServiceImpl implements InternalSystemService {

    private Logger log = LoggerFactory.getLogger(getClass());


    private static final String getAccessTokenUrl = "/third/get/accessToken";

    private static final String SEND_WECHAT_SUPPORT_SUCCESS_MSG = "/wx/msg/support/success/%s/%s";

    private static final String SEND_WECHAT_SUPPORT_MSG = "/wx/msg/support/%s";

    @Autowired
    private ActivityBargainMapper activityBargainMapper;
    @Autowired
    private ActivityBargainJoinMapper activityBargainJoinMapper;
    @Autowired
    private ActivityOrderRecordMapper activityOrderRecordMapper;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private WechatRemoteService wechatRemoteService;
    @Autowired
    private TradeApiRemoteService tradeApiRemoteService;

//    @Value("${wx.pay.appId}")
//    private String weixinOfficialAcctAppId;
//    @Value("${wx.appSecret}")
//    private String weixinOfficialAcctKey;


    @Override
    public void saveOrderRecord(String activityId, String userId, String orderId, Boolean isSuccess) {
        //看订单是不是存在了
        log.info("返回信息 ============== activityId: " + activityId + "_userId:" + userId +  "_orderId: " + orderId + "_isSuccess :" + isSuccess);
        ActivityBargainJoinExample activityBargainJoinExample = new ActivityBargainJoinExample();
        activityBargainJoinExample.setOrderByClause(" create_time desc ");
        activityBargainJoinExample.createCriteria().andBargainActivityIdEqualTo(activityId).andUserIdEqualTo(userId);
        List<ActivityBargainJoin> activityBargainJoins = activityBargainJoinMapper.selectByExample(activityBargainJoinExample);
        if (!CollectionUtils.isEmpty(activityBargainJoins)){
            ActivityBargainJoin join = activityBargainJoins.get(0);
            ActivityOrderRecordExample example = new ActivityOrderRecordExample();
            example.createCriteria().andOrderIdEqualTo(orderId).andUserIdEqualTo(userId).andActivityBargainJoinIdEqualTo(join.getActivityBargainJoinId());
            List<ActivityOrderRecord> activityOrderRecords = activityOrderRecordMapper.selectByExample(example);
            if (!CollectionUtils.isEmpty(activityOrderRecords)){
                ActivityOrderRecord activityOrderRecord = activityOrderRecords.get(0);
                activityOrderRecord.setStatus(isSuccess ? new Byte("1") : new Byte("2"));
                activityOrderRecordMapper.updateByPrimaryKeySelective(activityOrderRecord);
                if (isSuccess){
                    join.setStatus(new Byte("4"));
                    join.setUpdateTime(new Date());
                    activityBargainJoinMapper.updateByPrimaryKeySelective(join);
                    activityBargainMapper.addHelpPeople(1, join.getBargainActivityId());
                }
            }
        }
    }

    @Override
    @Transactional
    public void cancelOrder(String orderId) {
        ActivityOrderRecordExample example = new ActivityOrderRecordExample();
        example.createCriteria().andOrderIdEqualTo(orderId);
        List<ActivityOrderRecord> activityOrderRecords = activityOrderRecordMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(activityOrderRecords))
            throw new BusinessException(BusinessExceptionCode.ACTIVITY_ORDER_NOT_EXIST);
        ActivityOrderRecord record = activityOrderRecords.get(0);
        ActivityOrderRecord newRecord = new ActivityOrderRecord();
        newRecord.setId(record.getId());
        newRecord.setStatus(new Byte("2"));
        activityOrderRecordMapper.updateByPrimaryKeySelective(newRecord);
    }

    @Override
    public BigDecimal getActivityOrderPrice(String activityId, String userId) {
        Date date = new Date();
        String createOrderLock = RedisKeyConstants.BARGAIN_CREATE_ORDER_LOCK + ":" + activityId;
        redisLockUtils.lock(createOrderLock);
        try {
            ActivityBargainExample activityBargainExample = new ActivityBargainExample();
            activityBargainExample.createCriteria().andBargainActivityIdEqualTo(activityId)
                    .andActivityStartDateLessThanOrEqualTo(date)
                    .andActivityEndDateGreaterThan(date);
            List<ActivityBargain> list = activityBargainMapper.selectByExample(activityBargainExample);
            //检查活动是否存在
            if (CollectionUtils.isEmpty(list)){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "活动不存在");
            }
            ActivityBargain activityBargain = list.get(0);
            String cloudUserId = activityBargain.getCloudUserId();
            //校验订单是否存在避免重复下单
            ActivityOrderRecordExample activityOrderRecordExample = new ActivityOrderRecordExample();
//            activityOrderRecordExample.createCriteria()
//                    .andActivityBargainIdEqualTo(activityId).andOrderIdEqualTo(orderId)
//                    .andCloudUserIdEqualTo(cloudUserId);
//            int i = activityOrderRecordMapper.countByExample(activityOrderRecordExample);
//            if (i > 0){
//                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "重复下单");
//            }
            ActivityBargainJoinExample example = new ActivityBargainJoinExample();
            example.setOrderByClause("create_time desc");
            example.createCriteria().andBargainActivityIdEqualTo(activityId).andUserIdEqualTo(userId);
            List<ActivityBargainJoin> activityBargainJoins = activityBargainJoinMapper.selectByExample(example);
            //校验是否参与了活动
            if (CollectionUtils.isEmpty(activityBargainJoins)){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "未参与活动");
            }
            ActivityBargainJoin join = activityBargainJoins.get(0);
            String activityBargainJoinId = join.getActivityBargainJoinId();
//            ActivityBargainListExample activityBargainListExample = new ActivityBargainListExample();
//            activityBargainListExample.createCriteria().andActivityBargainJoinIdEqualTo(join.getActivityBargainJoinId());
//            int count = activityBargainListMapper.countByExample(activityBargainListExample);
//            //校验订单是否达到要求
//            if (activityBargain.getActivityCriterion().intValue() > count){
//                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR);
//            }
            //校验报名状态
            if (join.getStatus().byteValue() != new Byte("2").byteValue()){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "未获取下单资格");
            }
            //校验是否超时
            if (((date.getTime() - join.getUpdateTime().getTime()) / 1000l /60l) > activityBargain.getTimeOut()){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "未在限定时间内付款");
            }
            //查看库存是否足够
//            activityOrderRecordExample = new ActivityOrderRecordExample();
//            activityOrderRecordExample.createCriteria().andActivityBargainIdEqualTo(activityId)
//                    .andStatusIn(Arrays.asList(new Byte("2"), new Byte("4")));
//            int productCount = activityOrderRecordMapper.countByExample(activityOrderRecordExample);
//
            if (activityBargain.getProductStockTotal().intValue() - activityBargain.getJoinCount().intValue() < 1){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "库存不足");
            }
//            //保存订单
//            ActivityOrderRecord record = new ActivityOrderRecord();
//            record.setCloudUserId(cloudUserId);
//            record.setUserId(userId);
//            record.setActivityBargainJoinId(activityBargainJoinId);
//            record.setOrderMoney(join.getDealMoney());
//            record.setOrderId(orderId);
//            record.setCreateDate(date);
//            record.setId(UUIDUtils.getUuid());
//            activityOrderRecordMapper.insertSelective(record);
            return activityBargain.getActivityMoney();
        }finally {
            redisLockUtils.releaseLock(createOrderLock);
        }
    }

    @Override
    public String saveOrder(String activityId, String userId, String orderId, String type) {
        Date date = new Date();
        ActivityBargainExample activityBargainExample = new ActivityBargainExample();
        activityBargainExample.createCriteria().andBargainActivityIdEqualTo(activityId);
//                .andActivityStartDateLessThanOrEqualTo(date)
//                .andActivityEndDateGreaterThan(date);
        List<ActivityBargain> list = activityBargainMapper.selectByExample(activityBargainExample);
        //检查活动是否存在
        if (CollectionUtils.isEmpty(list)){
            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "活动不存在");
        }
        ActivityBargain activityBargain = list.get(0);
        String cloudUserId = activityBargain.getCloudUserId();
        //校验订单是否存在避免重复下单
        ActivityBargainJoinExample example = new ActivityBargainJoinExample();
        example.setOrderByClause("create_time desc");
        example.createCriteria().andBargainActivityIdEqualTo(activityId).andUserIdEqualTo(userId);
        List<ActivityBargainJoin> activityBargainJoins = activityBargainJoinMapper.selectByExample(example);
        //校验是否参与了活动
        if (CollectionUtils.isEmpty(activityBargainJoins)){
            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "未参与活动");
        }
        ActivityBargainJoin join = activityBargainJoins.get(0);
        //保存订单
        ActivityOrderRecord record = new ActivityOrderRecord();
        record.setCloudUserId(cloudUserId);
        record.setUserId(userId);
        record.setActivityBargainJoinId(join.getActivityBargainJoinId());
        record.setOrderMoney(join.getDealMoney());
        record.setOrderId(orderId);
        record.setCreateDate(date);
        record.setOrderType(type);
        record.setActivityBargainId(activityId);
        record.setId(UUIDUtils.getUuid());
        activityOrderRecordMapper.insertSelective(record);

        return join.getBargainActivityId();
    }

//    @Override
//    public String getWechatAccessToken(String appId, String appSecret) {
//        String res = HttpUtil.doGet(url + getAccessTokenUrl + "?appId=" + weixinOfficialAcctAppId + "&appSecret=" + weixinOfficialAcctKey);
//        if (!StringUtils.isEmpty(res)){
//            ResponseData responseData = JSONObject.parseObject(res, ResponseData.class);
//            if ("200".equals(responseData.getCode())){
//                return (String) responseData.getData();
//            }else {
//                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,res);
//            }
//        }else {
//            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"res is null");
//        }
//    }

    @Override
    public void sendWechatSupportSuccessMsg(String openId, String id) {
        ResponseData<String> resData = wechatRemoteService.sendWechatSupportSuccessMsg(openId, id);
        if (resData != null && !resData.isSuccessful()){
            log.error("发送微信消息失败：{}", JSONObject.toJSONString(resData));
//            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, JSONObject.toJSONString(resData));
        }
//        String res = resData.getData();
//        if (!StringUtils.isEmpty(res)){
//            ResponseData responseData = JSONObject.parseObject(res, ResponseData.class);
//            if (!"200".equals(responseData.getCode())){
//                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,res);
//            }
//        }else {
//            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"res is null");
//        }
    }

    @Override
    public void sendWechatSupportMsg(String openId) {
        try {
            ResponseData<String> resData = wechatRemoteService.sendWechatSupportMsg(openId);
            if (resData != null && !resData.isSuccessful()){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, JSONObject.toJSONString(resData));
            }
            String res = resData.getData();
            if (!StringUtils.isEmpty(res)){
                ResponseData responseData = JSONObject.parseObject(res, ResponseData.class);
                if (!"200".equals(responseData.getCode())){
                    log.error("发送微信消息失败：{}", res);
//	            	throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,res);
                }
            }else {
                log.error("发送微信消息失败：{}", res);
//	        	throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"res is null");
            }
        } catch (Exception e) {
            log.error("发送微信消息失败：{}", e);
        }
    }

    @Override
    public void weixinRefundCallBack(String orderId, String cloudUserId) {
        try {
            ActivityOrderRecordExample example = new ActivityOrderRecordExample();
            example.createCriteria().andCloudUserIdEqualTo(cloudUserId).andOrderIdEqualTo(orderId);
            List<ActivityOrderRecord> activityOrderRecords = activityOrderRecordMapper.selectByExample(example);
            ActivityOrderRecord activityOrderRecord = activityOrderRecords.get(0);
            String activityBargainJoinId = activityOrderRecord.getActivityBargainJoinId();
            ActivityBargainJoin join = new ActivityBargainJoin();
            join.setActivityBargainJoinId(activityBargainJoinId);
            join.setStatus(new Byte("6"));
            activityBargainJoinMapper.updateByPrimaryKeySelective(join);
            activityBargainMapper.addHelpPeople(-1, activityOrderRecord.getActivityBargainId());
        } catch (Exception e) {
            log.error("微信退款回调，订单状态修改失败：{}", e);
        }
    }

    @Override
    public String selectActivityIdByOrderId(String orderId, String cloudUserId) {
        ActivityOrderRecordExample example = new ActivityOrderRecordExample();
        example.createCriteria().andCloudUserIdEqualTo(cloudUserId).andOrderIdEqualTo(orderId);
        List<ActivityOrderRecord> activityOrderRecords = activityOrderRecordMapper.selectByExample(example);
        return activityOrderRecords.isEmpty() ? null : activityOrderRecords.get(0).getActivityBargainId();
    }

    @Override
    public ActivityBargain selectActivityId(String activityId) {
        return activityBargainMapper.selectByPrimaryKey(activityId);
    }

    public static void main(String[] args) throws InterruptedException {
        Date date = new Date();
        Thread.sleep(3000l);
        Date date1 = new Date();
        long l = (date1.getTime() - date.getTime()) / 1000;
        System.out.println(l);
    }

}
