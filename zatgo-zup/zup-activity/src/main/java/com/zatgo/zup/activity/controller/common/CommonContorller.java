package com.zatgo.zup.activity.controller.common;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.controller.BaseController;
import com.zatgo.zup.activity.entity.ActivityProduct;
import com.zatgo.zup.activity.entity.request.SyncProductRequest;
import com.zatgo.zup.activity.service.CommonService;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by 46041 on 2019/3/19.
 */


@Api(value = "/activity",description = "公共接口")
@RestController
@RequestMapping(value = "/activity")
public class CommonContorller extends BaseController{


    @Autowired
    private CommonService commonService;




    @RequestMapping(value = "/syncProduct",method = RequestMethod.POST)
    @ApiOperation(value = "同步商品接口", notes = "同步商品接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData syncProduct(@RequestBody SyncProductRequest params) {
        commonService.syncProduct(params);
        return BusinessResponseFactory.createSuccess(null);
    }


    @RequestMapping(value = "/productList",method = RequestMethod.GET)
    @ApiOperation(value = "商品列表", notes = "商品列表", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData productList(@RequestParam(name = "productName", required = false) String productName,
                                             @RequestParam("pageNo") Integer pageNo,
                                             @RequestParam("pageSize") Integer pageSize) {

        ThirdLoginInfo userInfo = getAdminInfo();
        PageInfo<ActivityProduct> pageInfo = commonService.productList(productName, pageNo, pageSize, userInfo);
        return BusinessResponseFactory.createSuccess(pageInfo);
    }

    @RequestMapping(value = "/get/userId",method = RequestMethod.GET)
    @ApiOperation(value = "获取用户Id", notes = "获取用户Id", httpMethod = "GET")
    @ResponseBody
    public ResponseData getUserId() {
        ThirdLoginInfo userInfo = getUserInfo();
        return BusinessResponseFactory.createSuccess(userInfo.getUserId());
    }

    @RequestMapping(value = "/getPayTime",method = RequestMethod.GET)
    @ApiOperation(value = "根据活动id获取活动的支付时限", notes = "根据活动id获取活动的支付时限", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData<Map<String, Long>> getPayTime(@RequestParam("ids") String ids){
        Map<String, Long> res = commonService.getPayTime(ids);
        return BusinessResponseFactory.createSuccess(res);
    }

    @RequestMapping(value = "/getActivityId/{joinId}",method = RequestMethod.GET)
    @ApiOperation(value = "根据joinId获取活动id", notes = "根据joinId获取活动id", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData<String> getActivityId(@PathVariable("joinId") String joinId){
        String activityId = commonService.getActivityId(joinId);
        return BusinessResponseFactory.createSuccess(activityId);
    }

    @RequestMapping(value = "/get/activiTypayTime",method = RequestMethod.GET)
    @ResponseBody
    public ResponseData getActivityPayTime(@RequestParam("activityId") String activityId, @RequestParam("orderType") String orderType) {
        Long time = commonService.getActiviTypayTime(activityId, orderType);
        return BusinessResponseFactory.createSuccess(time);
    }
}
