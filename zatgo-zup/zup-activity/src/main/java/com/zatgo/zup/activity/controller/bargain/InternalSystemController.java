package com.zatgo.zup.activity.controller.bargain;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.activity.controller.BaseController;
import com.zatgo.zup.activity.entity.ActivityBargain;
import com.zatgo.zup.activity.service.InternalSystemService;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisLockUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@Api(value = "/activity/internal",description = "内部系统砍价功能接口")
@RestController
@RequestMapping(value = "/activity/internal")
public class InternalSystemController extends BaseController{

	@Value("${activity.url}")
	private String url;


	@Autowired
	private InternalSystemService internalSystemService;
	@Autowired
	private RedisLockUtils redisLockUtils;

	@RequestMapping(value = "/orderCallBack",method = RequestMethod.POST)
	@ApiOperation(value = "下单号回调", notes = "下单号回调", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> addOrder(@RequestParam("activityId") String activityId,
										  @RequestParam("userId") String userId,
										  @RequestParam("orderId") String orderId,
										  @RequestParam("isSuccess") Boolean isSuccess) {
		internalSystemService.saveOrderRecord(activityId, userId, orderId, isSuccess);
		return BusinessResponseFactory.createSuccess(null);
	}

	@RequestMapping(value = "/weixinRefundCallBack",method = RequestMethod.POST)
	@ApiOperation(value = "微信退款回调", notes = "微信退款回调", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> weixinRefundCallBack(@RequestParam("orderId") String orderId, @RequestParam("cloudUserId") String cloudUserId) {
		internalSystemService.weixinRefundCallBack(orderId, cloudUserId);
		return BusinessResponseFactory.createSuccess(null);
	}

	@RequestMapping(value = "/getActivityOrderPrice",method = RequestMethod.POST)
	@ApiOperation(value = "获取活动价格", notes = "获取活动价格", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<BigDecimal> getActivityOrderPrice(@RequestParam("activityId") String activityId,
														  @RequestParam("userId") String userId) {
		BigDecimal price;
		try {
			price = internalSystemService.getActivityOrderPrice(activityId, userId);
			return BusinessResponseFactory.createSuccess(price);
		} catch (BusinessException e){
			return BusinessResponseFactory.createBusinessError(e.getCode(), e.getMessage());
		}
	}

	@RequestMapping(value = "/saveOrder",method = RequestMethod.POST)
	@ApiOperation(value = "保存订单", notes = "保存订单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<String> saveOrder(@RequestParam("activityId") String activityId,
										   @RequestParam("userId") String userId,
										   @RequestParam("orderId") String orderId,
										   @RequestParam("type") String type) {
		String activityJoinId;
		try {
			activityJoinId = internalSystemService.saveOrder(activityId, userId, orderId, type);
			return BusinessResponseFactory.createSuccess(activityJoinId);
		} catch (BusinessException e){
			return BusinessResponseFactory.createBusinessError(e.getCode(), e.getMessage());
		}
	}

	@RequestMapping(value = "/cancelOrder",method = RequestMethod.POST)
	@ApiOperation(value = "取消订单接口", notes = "取消订单接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> cancelOrder(@RequestParam("orderId") String orderId) {
		internalSystemService.cancelOrder(orderId);
		return BusinessResponseFactory.createSuccess(null);
	}


	@ApiOperation(value = "根据订单id查活动id", notes = "根据订单id查活动id", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping(value = "/selectActivityIdByOrderId")
	@ResponseBody
	public ResponseData<String> selectActivityIdByOrderId(@RequestParam("orderId") String orderId,
														  @RequestParam("cloudUserId") String cloudUserId){
		String activityId = internalSystemService.selectActivityIdByOrderId(orderId, cloudUserId);
		return BusinessResponseFactory.createSuccess(activityId);
	}

	@ApiOperation(value = "根据活动id查活动", notes = "根据活动id查活动", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping(value = "/selectActivityId/{activityId}")
	@ResponseBody
	public ResponseData<ActivityBargain> selectActivityIdByOrderId(@PathVariable("activityId") String activityId){
		ActivityBargain ab = internalSystemService.selectActivityId(activityId);
		return BusinessResponseFactory.createSuccess(ab);
	}

}
