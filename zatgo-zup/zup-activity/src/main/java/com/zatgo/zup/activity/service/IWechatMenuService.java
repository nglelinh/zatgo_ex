package com.zatgo.zup.activity.service;

public interface IWechatMenuService {
	
	void generateWechatMenu(String cloudUserId);

}
