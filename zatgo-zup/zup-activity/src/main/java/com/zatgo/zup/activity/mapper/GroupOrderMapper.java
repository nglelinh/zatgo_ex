package com.zatgo.zup.activity.mapper;

import com.zatgo.zup.activity.entity.GroupBookingActivity;
import com.zatgo.zup.activity.entity.GroupOrder;
import org.apache.ibatis.annotations.Param;

public interface GroupOrderMapper extends BaseMapper<GroupOrder>{




    GroupBookingActivity getActivityInfoByActivityIdAndUserId(@Param("activityId") String activityId, @Param("userId") String userId);
}