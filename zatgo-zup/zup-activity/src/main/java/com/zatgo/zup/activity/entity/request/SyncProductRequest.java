package com.zatgo.zup.activity.entity.request;

import com.zatgo.zup.activity.model.SyncProductParams;

import java.util.List;

/**
 * Created by 46041 on 2019/3/13.
 */
public class SyncProductRequest {

    private List<SyncProductParams> products;

    private String cloudUserId;

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }

    public List<SyncProductParams> getProducts() {
        return products;
    }

    public void setProducts(List<SyncProductParams> products) {
        this.products = products;
    }
}
