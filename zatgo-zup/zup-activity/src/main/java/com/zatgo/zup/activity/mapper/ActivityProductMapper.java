package com.zatgo.zup.activity.mapper;

import com.zatgo.zup.activity.entity.ActivityProduct;

public interface ActivityProductMapper extends BaseMapper<ActivityProduct>{

}