package com.zatgo.zup.activity.controller.task;

import com.zatgo.zup.activity.entity.*;
import com.zatgo.zup.activity.mapper.ActivityBargainJoinMapper;
import com.zatgo.zup.activity.mapper.ActivityBargainMapper;
import com.zatgo.zup.activity.mapper.GroupOrderMapper;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2019/7/25.
 */

@Component
public class UpdateActivityStatusTask {

    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private GroupOrderMapper groupOrderMapper;
    @Autowired
    private ActivityBargainJoinMapper activityBargainJoinMapper;
    @Autowired
    private ActivityBargainMapper activityBargainMapper;


    @Scheduled(cron = "0 */1 * * * ?")
    public void groupExecute(){
        boolean flag = redisLockUtils.taskLock(RedisKeyConstants.GROUP_TASK_PRE);
        if (flag){
            try{
                GroupOrderExample example = new GroupOrderExample();
                example.createCriteria().andGroupEndDateLessThan(new Date()).andGroupOrderStatusEqualTo(BusinessEnum.GroupBookingOrderStatus.GroupBooking.getCode());
                int i = groupOrderMapper.countByExample(example);
                if (i > 0){
                    GroupOrder groupOrder = new GroupOrder();
                    groupOrder.setGroupOrderStatus(BusinessEnum.GroupBookingOrderStatus.Refunding.getCode());
                    groupOrderMapper.updateByExampleSelective(groupOrder, example);
                }
            } finally {
                redisLockUtils.releaseLock(RedisKeyConstants.GROUP_TASK_PRE);
            }
        }
    }

    @Scheduled(cron = "0 */1 * * * ?")
    public void bargainExecute(){
        boolean flag = redisLockUtils.taskLock(RedisKeyConstants.BARGAIN_TASK_PRE);
        if (flag){
            try{
                ActivityBargainJoinExample example = new ActivityBargainJoinExample();
                example.createCriteria().andPayTimeLessThan(new Date()).andStatusEqualTo(new Byte("2"));
                List<ActivityBargainJoin> activityBargainJoins = activityBargainJoinMapper.selectByExample(example);
                if (!CollectionUtils.isEmpty(activityBargainJoins)){
                    ActivityBargainJoin abj = new ActivityBargainJoin();
                    abj.setStatus(new Byte("5"));
                    activityBargainJoinMapper.updateByExampleSelective(abj, example);
                    Map<String, Integer> map = new HashMap<>();
                    for (ActivityBargainJoin activityBargainJoin : activityBargainJoins) {
                        String bargainActivityId = activityBargainJoin.getBargainActivityId();
                        if (map.containsKey(bargainActivityId)){
                            Integer count = map.get(bargainActivityId);
                            map.put(bargainActivityId, count + 1);
                        } else {
                            map.put(bargainActivityId, 1);
                        }
                    }
                    map.forEach((k,v) -> {
                        activityBargainMapper.addHelpPeople(-v,k);
                    });
                }
            } finally {
                redisLockUtils.releaseLock(RedisKeyConstants.BARGAIN_TASK_PRE);
            }
        }
    }
}
