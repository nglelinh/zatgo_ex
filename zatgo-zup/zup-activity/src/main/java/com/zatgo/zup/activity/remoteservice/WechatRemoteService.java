package com.zatgo.zup.activity.remoteservice;

import com.zatgo.zup.activity.entity.wx.WxUserInfoData;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by 46041 on 2019/6/13.
 */
@FeignClient("zup-wechat")
public interface WechatRemoteService {

    @GetMapping(value = "/wechat/getWechatAccessToken")
    @ResponseBody
    ResponseData getWechatAccessToken(@RequestParam("weixinOfficialAcctAppId") String weixinOfficialAcctAppId,
                                @RequestParam("weixinOfficialAcctKey") String weixinOfficialAcctKey);


    @GetMapping("/wechat/wx/msg/support/success/{openId}/{id}")
    ResponseData sendWechatSupportSuccessMsg(@PathVariable("openId") String openId, @PathVariable("id") String id);

    @GetMapping("/wechat/wx/msg/support/{userId}")
    ResponseData sendWechatSupportMsg(@PathVariable("openId") String openId);

    @GetMapping ("/wechat/getQRCode")
    ResponseData<String> getQRCode(@RequestParam("cloudUserId") String cloudUserId,
                                   @RequestParam("sceneStr") String sceneStr,
                                   @RequestParam("userId") String userId);

    @GetMapping("/wechat/getWxUserInfo")
    ResponseData<WxUserInfoData> getWxUserInfo(@RequestParam("openId") String openId, @RequestParam("cloudUserId") String cloudUserId);
}
