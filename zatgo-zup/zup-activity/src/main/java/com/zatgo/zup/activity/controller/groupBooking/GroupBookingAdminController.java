package com.zatgo.zup.activity.controller.groupBooking;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.activity.controller.BaseController;
import com.zatgo.zup.activity.entity.GroupBookingActivity;
import com.zatgo.zup.activity.entity.GroupOrder;
import com.zatgo.zup.activity.entity.GroupUserOrder;
import com.zatgo.zup.activity.entity.request.AdminActivityListRequest;
import com.zatgo.zup.activity.entity.request.GroupOrderDetailListInfoRequest;
import com.zatgo.zup.activity.entity.request.GroupOrderMergeRequest;
import com.zatgo.zup.activity.entity.response.ActivityGroupBookingInfoResponse;
import com.zatgo.zup.activity.entity.response.GroupInfoResponse;
import com.zatgo.zup.activity.model.AddGroupBookingActivityParams;
import com.zatgo.zup.activity.model.UpdateGroupBookingActivityParams;
import com.zatgo.zup.activity.service.GroupBookingAdminService;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2019/3/15.
 */


@Api(value = "/activity/groupBooking/admin",description = "拼团管理员接口")
@RestController
@RequestMapping(value = "/activity/groupBooking/admin")
public class GroupBookingAdminController extends BaseController {


    @Autowired
    private GroupBookingAdminService groupBookingAdminService;


    @PostMapping(value = "/addGroupBookingActivity")
    @ApiOperation(value = "新增拼团活动", notes = "新增拼团活动")
    @ResponseBody
    public ResponseData<Boolean> addGroupBooking(@RequestBody AddGroupBookingActivityParams params) {
        ThirdLoginInfo info = getAdminInfo();
        groupBookingAdminService.addGroupBooking(params, info);
        return BusinessResponseFactory.createSuccess(null);
    }

    @PostMapping(value = "/updateGroupBookingActivity")
    @ApiOperation(value = "修改拼团活动", notes = "修改拼团活动")
    @ResponseBody
    public ResponseData<Boolean> updateGroupBooking(@RequestBody UpdateGroupBookingActivityParams params) {
        ThirdLoginInfo info = getAdminInfo();
        groupBookingAdminService.updateGroupBooking(params, info);
        return BusinessResponseFactory.createSuccess(null);
    }

    @PostMapping(value = "/onlineActivity/{groupActivityId}")
    @ApiOperation(value = "活动上线", notes = "活动上线")
    @ResponseBody
    public ResponseData<Boolean> onlineActivity(@PathVariable("groupActivityId") String groupActivityId) {
        ThirdLoginInfo info = getAdminInfo();
        groupBookingAdminService.onlineActivity(groupActivityId, info);
        return BusinessResponseFactory.createSuccess(null);
    }


    @PostMapping(value = "/offlineActivity/{groupActivityId}")
    @ApiOperation(value = "活动下线", notes = "活动下线")
    @ResponseBody
    public ResponseData<Boolean> offlineActivity(@PathVariable("groupActivityId") String groupActivityId) {
        ThirdLoginInfo info = getAdminInfo();
        groupBookingAdminService.offlineActivity(groupActivityId, info);
        return BusinessResponseFactory.createSuccess(null);
    }

    @PostMapping(value = "/activity/list")
    @ApiOperation(value = "活动列表", notes = "活动列表")
    @ResponseBody
    public ResponseData activityList(@RequestBody AdminActivityListRequest request) {
        ThirdLoginInfo userInfo = getAdminInfo();
        PageInfo<GroupBookingActivity> info = groupBookingAdminService.activityList(userInfo, request);
        return BusinessResponseFactory.createSuccess(info);
    }

//    @PostMapping(value = "/activity/order/update")
//    @ApiOperation(value = "修改活动订单信息", notes = "修改活动订单信息")
//    @ResponseBody
//    public ResponseData updateActivityOrder(@RequestBody GroupOrder groupOrder) {
//        groupBookingAdminService.updateActivityOrder(groupOrder);
//        return BusinessResponseFactory.createSuccess(null);
//    }

    @PostMapping(value = "/activity/detail/list/{activityId}")
    @ApiOperation(value = "活动拼团详情", notes = "活动拼团详情")
    @ResponseBody
    public ResponseData activityDetailList(@PathVariable ("activityId") String activityId,
                                       @RequestParam("pageNo") Integer pageNo,
                                       @RequestParam("pageSize") Integer pageSize,
                                       @RequestParam(value = "status", required = false) Integer status) {
        PageInfo<GroupInfoResponse> info = groupBookingAdminService.activityDetailList(activityId, pageNo, pageSize, status);
        return BusinessResponseFactory.createSuccess(info);
    }

    @PostMapping(value = "/activity/detail/{activityId}")
    @ApiOperation(value = "活动详情信息", notes = "活动详情信息")
    @ResponseBody
    public ResponseData activityDetailInfo(@PathVariable ("activityId") String activityId) {
        ActivityGroupBookingInfoResponse response = groupBookingAdminService.activityDetailInfo(activityId);
        return BusinessResponseFactory.createSuccess(response);
    }

    @PostMapping(value = "/activity/detail/list/info/{orderId}")
    @ApiOperation(value = "拼团详情", notes = "拼团详情")
    @ResponseBody
    public ResponseData activityDetailListInfo(@RequestBody GroupOrderDetailListInfoRequest request) {
        PageInfo<GroupUserOrder> info = groupBookingAdminService.activityDetailListInfo(request);
        return BusinessResponseFactory.createSuccess(info);
    }

    @RequestMapping(value = "/auto/refund",method = RequestMethod.GET)
    @ApiOperation(value = "拼团失败自动退款", notes = "拼团失败自动退款", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData autoRefund(@RequestParam("groupOrderId") String groupOrderId, @RequestParam(value = "groupOrderUserId", required = false) String groupOrderUserId) {
        ThirdLoginInfo adminInfo = getAdminInfo();
        groupBookingAdminService.autoRefund(groupOrderId, groupOrderUserId);
        return BusinessResponseFactory.createSuccess(null);
    }


    @RequestMapping(value = "/merge",method = RequestMethod.POST)
    @ApiOperation(value = "拼团合并", notes = "拼团合并", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseData merge(@RequestBody GroupOrderMergeRequest request) {
        ThirdLoginInfo adminInfo = getAdminInfo();
        groupBookingAdminService.merge(request);
        return BusinessResponseFactory.createSuccess(null);
    }

}
