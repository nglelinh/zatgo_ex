package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.ComputePowerPojo;
import com.zatgo.zup.mining.entity.UserMiningProperty;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface UserMiningPropertyMapper extends BaseMapper<UserMiningProperty>{


    List<UserMiningProperty> selectAllUserCompute(Date nowDate);

    ComputePowerPojo getUserComputePower(@Param("userId") String userId,@Param("nowDate") Date date);

    void updateUserCompute(@Param("num") Long num,@Param("userId") String userId);

    void insertList(List<UserMiningProperty> list);
}