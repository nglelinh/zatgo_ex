package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.SubTaskRewardManage;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

public interface SubTaskRewardManageMapper extends BaseMapper<SubTaskRewardManage>{


    int updateCoinRemainingTotal(@Param("rewardId") String rewardId, @Param("amount") BigDecimal amount);
}