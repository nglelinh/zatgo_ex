package com.zatgo.zup.mining.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MineralPoolExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MineralPoolExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerIsNull() {
            addCriterion("all_computing_power is null");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerIsNotNull() {
            addCriterion("all_computing_power is not null");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerEqualTo(String value) {
            addCriterion("all_computing_power =", value, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerNotEqualTo(String value) {
            addCriterion("all_computing_power <>", value, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerGreaterThan(String value) {
            addCriterion("all_computing_power >", value, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerGreaterThanOrEqualTo(String value) {
            addCriterion("all_computing_power >=", value, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerLessThan(String value) {
            addCriterion("all_computing_power <", value, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerLessThanOrEqualTo(String value) {
            addCriterion("all_computing_power <=", value, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerLike(String value) {
            addCriterion("all_computing_power like", value, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerNotLike(String value) {
            addCriterion("all_computing_power not like", value, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerIn(List<String> values) {
            addCriterion("all_computing_power in", values, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerNotIn(List<String> values) {
            addCriterion("all_computing_power not in", values, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerBetween(String value1, String value2) {
            addCriterion("all_computing_power between", value1, value2, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andAllComputingPowerNotBetween(String value1, String value2) {
            addCriterion("all_computing_power not between", value1, value2, "allComputingPower");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNull() {
            addCriterion("coin_network_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNotNull() {
            addCriterion("coin_network_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeEqualTo(String value) {
            addCriterion("coin_network_type =", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotEqualTo(String value) {
            addCriterion("coin_network_type <>", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThan(String value) {
            addCriterion("coin_network_type >", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_network_type >=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThan(String value) {
            addCriterion("coin_network_type <", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_network_type <=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLike(String value) {
            addCriterion("coin_network_type like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotLike(String value) {
            addCriterion("coin_network_type not like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIn(List<String> values) {
            addCriterion("coin_network_type in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotIn(List<String> values) {
            addCriterion("coin_network_type not in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeBetween(String value1, String value2) {
            addCriterion("coin_network_type between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotBetween(String value1, String value2) {
            addCriterion("coin_network_type not between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNull() {
            addCriterion("coin_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNotNull() {
            addCriterion("coin_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeEqualTo(String value) {
            addCriterion("coin_type =", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotEqualTo(String value) {
            addCriterion("coin_type <>", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThan(String value) {
            addCriterion("coin_type >", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_type >=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThan(String value) {
            addCriterion("coin_type <", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_type <=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLike(String value) {
            addCriterion("coin_type like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotLike(String value) {
            addCriterion("coin_type not like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIn(List<String> values) {
            addCriterion("coin_type in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotIn(List<String> values) {
            addCriterion("coin_type not in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeBetween(String value1, String value2) {
            addCriterion("coin_type between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotBetween(String value1, String value2) {
            addCriterion("coin_type not between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinSumIsNull() {
            addCriterion("coin_sum is null");
            return (Criteria) this;
        }

        public Criteria andCoinSumIsNotNull() {
            addCriterion("coin_sum is not null");
            return (Criteria) this;
        }

        public Criteria andCoinSumEqualTo(BigDecimal value) {
            addCriterion("coin_sum =", value, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumNotEqualTo(BigDecimal value) {
            addCriterion("coin_sum <>", value, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumGreaterThan(BigDecimal value) {
            addCriterion("coin_sum >", value, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coin_sum >=", value, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumLessThan(BigDecimal value) {
            addCriterion("coin_sum <", value, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coin_sum <=", value, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumIn(List<BigDecimal> values) {
            addCriterion("coin_sum in", values, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumNotIn(List<BigDecimal> values) {
            addCriterion("coin_sum not in", values, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coin_sum between", value1, value2, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCoinSumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coin_sum not between", value1, value2, "coinSum");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsIsNull() {
            addCriterion("coin_decimals is null");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsIsNotNull() {
            addCriterion("coin_decimals is not null");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsEqualTo(Integer value) {
            addCriterion("coin_decimals =", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsNotEqualTo(Integer value) {
            addCriterion("coin_decimals <>", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsGreaterThan(Integer value) {
            addCriterion("coin_decimals >", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsGreaterThanOrEqualTo(Integer value) {
            addCriterion("coin_decimals >=", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsLessThan(Integer value) {
            addCriterion("coin_decimals <", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsLessThanOrEqualTo(Integer value) {
            addCriterion("coin_decimals <=", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsIn(List<Integer> values) {
            addCriterion("coin_decimals in", values, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsNotIn(List<Integer> values) {
            addCriterion("coin_decimals not in", values, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsBetween(Integer value1, Integer value2) {
            addCriterion("coin_decimals between", value1, value2, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsNotBetween(Integer value1, Integer value2) {
            addCriterion("coin_decimals not between", value1, value2, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andNumIsNull() {
            addCriterion("num is null");
            return (Criteria) this;
        }

        public Criteria andNumIsNotNull() {
            addCriterion("num is not null");
            return (Criteria) this;
        }

        public Criteria andNumEqualTo(Integer value) {
            addCriterion("num =", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotEqualTo(Integer value) {
            addCriterion("num <>", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThan(Integer value) {
            addCriterion("num >", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("num >=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThan(Integer value) {
            addCriterion("num <", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThanOrEqualTo(Integer value) {
            addCriterion("num <=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumIn(List<Integer> values) {
            addCriterion("num in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotIn(List<Integer> values) {
            addCriterion("num not in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumBetween(Integer value1, Integer value2) {
            addCriterion("num between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotBetween(Integer value1, Integer value2) {
            addCriterion("num not between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}