package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.ComputeGetRecord;
import com.zatgo.zup.mining.entity.ComputePowerPojo;
import com.zatgo.zup.mining.entity.UserComputePowerPojo;
import com.zatgo.zup.mining.entity.UserMiningProperty;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ComputeGetRecordMapper extends BaseMapper<ComputeGetRecord>{

    Long getComputePowerByUserId(@Param("userId") String userId,
                                 @Param("date") Date date);

    List<UserComputePowerPojo> getComputePowerByUserIdAndDate(@Param("userId") String userId,
                                                              @Param("startDate") Date startDate,
                                                              @Param("endDate") Date endDate);

    Long getAllUserComputePower(Date date);

    List<UserMiningProperty> getUserCompute();

}