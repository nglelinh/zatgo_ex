package com.zatgo.zup.mining.entity;

/**
 * Created by 46041 on 2018/7/25.
 */
public class UserComputePowerPojo {

    private String date;
    private Long computePower;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getComputePower() {
        return computePower;
    }

    public void setComputePower(Long computePower) {
        this.computePower = computePower;
    }
}
