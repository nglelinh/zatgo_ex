package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.MineralPool;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MineralPoolMapper extends BaseMapper<MineralPool>{


    List<MineralPool> getMiningRecordListByTime(@Param("startDate") Date startDate,
                                                @Param("endDate") Date endDate,
                                                @Param("cloudUserId") String cloudUserId);

    int insertMineralPoolListSelective(List<MineralPool> list);
}