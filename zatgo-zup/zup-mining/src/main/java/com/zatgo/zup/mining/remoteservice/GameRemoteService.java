package com.zatgo.zup.mining.remoteservice;

import com.zatgo.zup.common.model.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/1/14.
 */

@FeignClient("zup-games")
public interface GameRemoteService {

    @PostMapping(value = "/game/reward/getReward")
    @ResponseBody
    ResponseData<ZhuanpanRunModel> getRewar(@RequestBody GetRewarRequest request);


    @PostMapping(value = "/game/reward/addGameFreeNum")
    @ResponseBody
    ResponseData<ZhuanpanRunModel> addGameFreeNum(@RequestParam("num") Integer num,
                                                  @RequestParam("gameId") String gameId,
                                                  @RequestParam("userId") String userId);


    @PostMapping(value = "/game/qa/getAnswerRecord")
    @ResponseBody
    ResponseData<List<AnswerQuestionResultModel>> getAnswerRecord(@RequestBody AnswerRecordRequest request);

//    @PostMapping(value = "/game/qa/countByDoneTask")
//    @ResponseBody
//    ResponseData<Integer> countByDoneTask(@RequestBody AnswerRecordRequest request);

    @PostMapping(value = "/game/qa/countByDoneTaskFen")
    @ResponseBody
    ResponseData<Integer> countByDoneTaskFen(@RequestBody AnswerDoneFenRequest request);
}
