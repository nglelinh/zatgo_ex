package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.UserTaskInfoRewardRecord;

public interface UserTaskInfoRewardRecordMapper extends BaseMapper<UserTaskInfoRewardRecord>{
}