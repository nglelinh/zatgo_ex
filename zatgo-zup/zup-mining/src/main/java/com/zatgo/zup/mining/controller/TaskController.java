package com.zatgo.zup.mining.controller;

import com.github.pagehelper.PageInfo;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.TaskRecordListRequest;
import com.zatgo.zup.common.model.TaskRecordListResponse;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.mining.entity.TaskManage;
import com.zatgo.zup.mining.entity.TaskManageList;
import com.zatgo.zup.mining.entity.UserTaskInfoRecord;
import com.zatgo.zup.mining.entity.UserTaskInfoRecordModel;
import com.zatgo.zup.mining.entity.response.GetRewardRecordResponse;
import com.zatgo.zup.mining.entity.response.GetRewardResponse;
import com.zatgo.zup.mining.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2018/8/7.
 */
@Api(value = "/task",description = "用户任务相关",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/mining/task")
public class TaskController extends BaseController{


    private static final Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;
    @Autowired
    private RedisLockUtils redisLockUtils;



//    @ApiOperation(value = "创建任务")
//    @PostMapping("/create/task")
//    public ResponseData crateTask(@RequestBody TaskManage taskManage){
//        AuthUserInfo userInfo = getUserInfo();
//        return taskService.crateTask(taskManage, userInfo.getUserId());
//    }
//
//    @ApiOperation(value = "任务修改")
//    @PostMapping("/edit/task")
//    public ResponseData editTask(@RequestBody TaskManage taskManage){
//        AuthUserInfo userInfo = getUserInfo();
//        return taskService.editTask(taskManage, userInfo.getUserId());
//    }
//
//    @ApiOperation(value = "删除任务")
//    @GetMapping("/delete/task/{id}")
//    public ResponseData deleteTask(@PathVariable(value = "id") String id){
//        AuthUserInfo userInfo = getUserInfo();
//        return taskService.deleteTask(id, userInfo.getUserId());
//    }

//    @ApiOperation(value = "任务列表(管理后台)")
//    @GetMapping("/list/task")
//    public ResponseData<PageInfo<TaskManage>> taskList(@RequestParam("title") String title,
//                                                       @RequestParam("type") String type,
//                                                       @RequestParam("pageSize") Integer pageSize,
//                                                       @RequestParam("pageNo") Integer pageNo,
//                                                       @RequestParam("cloudUserId") String cloudUserId){
//        return taskService.taskList(title, type, pageSize, pageNo, cloudUserId);
//    }

    @ApiOperation(value = "用户登录时获取算力")
    @GetMapping("/loginTaskDone")
    public ResponseData loginTaskDone(){
        AuthUserInfo userInfo = getUserInfo();
        String userId = userInfo.getUserId();
        return taskService.loginTaskDone(userId, userInfo.getUserName(), userInfo.getCloudUserId(), UUIDUtils.getUuid());
    }

    @ApiOperation(value = "任务列表(app调用)")
    @GetMapping("/list/taskData")
    public ResponseData<Map<String, List<TaskManageList>>> taskListDate(){
        AuthUserInfo userInfo = getUserInfo();
        ResponseData<Map<String, List<TaskManageList>>> mapResponseData = null;
        try {
            mapResponseData = taskService.taskListDate(userInfo.getUserId(), userInfo.getCloudUserId());
        } catch (ParseException e) {
            logger.error("", e);
            mapResponseData = BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
        return mapResponseData;
    }


    @ApiOperation(value = "其他任务完成")
    @GetMapping("/done/{taskId}")
    public ResponseData taskDone(@PathVariable("taskId") String taskId, @RequestParam(value = "source", required=false) String source){
        AuthUserInfo userInfo = getUserInfo();

        //解决同一用户并发问题
        String accountLockKey = RedisKeyConstants.TASK_LOCK + userInfo.getUserId();
        redisLockUtils.lock(accountLockKey);
        ResponseData responseData = null;
        try {
        	if (StringUtils.isEmpty(source))
                source = "1";
            String businessId = UUIDUtils.getUuid();
            responseData = taskService.taskDone(userInfo.getUserId(), userInfo.getUserName(), taskId, BusinessEnum.ComputePowerSourceType.getEnumByType(source), businessId);
        } finally {
            redisLockUtils.releaseLock(accountLockKey);
        }

        return responseData;
    }

    @ApiOperation(value = "完成注册任务")
    @GetMapping("/done/registerTaskDone")
    public ResponseData registerTaskDone(@RequestParam("userId") String userId, @RequestParam("cloudUserId") String cloudUserId, @RequestParam("businessId") String businessId){
        ResponseData responseData = taskService.registerTaskDone(userId, cloudUserId, businessId);
        return responseData;
    }

    @ApiOperation(value = "任务记录列表")
    @GetMapping("/record/list")
    public ResponseData recordList(@RequestBody TaskRecordListRequest request){
        PageInfo<UserTaskInfoRecordModel> pageInfo = taskService.recordList(request);
        return BusinessResponseFactory.createSuccess(pageInfo);
    }

    @ApiOperation(value = "邀请码任务完成(内部调用)")
    @GetMapping("/done/invitationTask")
    public void invitationTaskDone(@RequestParam("count") Long count,
                                    @RequestParam("invitedUserId") String invitedUserId,
                                   @RequestParam("cloudUserId") String cloudUserId,
                                    @RequestParam("businessId") String businessId){
        taskService.invitationTaskDone(count, invitedUserId, cloudUserId, businessId);
    }

    @ApiOperation(value = "获取用户邀请码(APP调用)")
    @GetMapping("/invitationCode/{userId}")
    public ResponseData<String> getUserInvitationCode(){
        ResponseData responseData = null;
        try {
            AuthUserInfo userInfo = getUserInfo();
            responseData = BusinessResponseFactory.createSuccess(userInfo.getInviteCode());
        } catch (Exception e) {
            logger.error("", e);
            responseData = BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
        return responseData;
    }


    @ApiOperation(value = "领奖")
    @PostMapping("/get/reward")
    public ResponseData getReward(@RequestParam("taskId") String taskId){
        AuthUserInfo user = getUserInfo();
        //解决同一用户并发问题
        String accountLockKey = RedisKeyConstants.TASK_LOCK + user.getUserId();
        redisLockUtils.lock(accountLockKey);
        GetRewardRecordResponse res = null;
        try {
            res = taskService.getReward(taskId, user);
        }finally {
            redisLockUtils.releaseLock(accountLockKey);
        }
        return BusinessResponseFactory.createSuccess(res);
    }

    @ApiOperation(value = "领奖记录")
    @PostMapping("/get/rewardRecord")
    public ResponseData getRewardRecord(@RequestParam("taskId") String taskId){
        AuthUserInfo user = getUserInfo();
        GetRewardRecordResponse res = taskService.getRewardRecord(taskId, user);
        return BusinessResponseFactory.createSuccess(res);
    }
}
