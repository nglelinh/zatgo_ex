package com.zatgo.zup.mining.entity.response;

/**
 * Created by 46041 on 2019/2/14.
 */
public class AnswerResponse {

    private Boolean isTrue;

    private String answer;


    public Boolean getTrue() {
        return isTrue;
    }

    public void setTrue(Boolean aTrue) {
        isTrue = aTrue;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
