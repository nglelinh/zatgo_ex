package com.zatgo.zup.mining.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.TaskRecordListRequest;
import com.zatgo.zup.common.model.TaskRecordListResponse;
import com.zatgo.zup.mining.entity.TaskManage;
import com.zatgo.zup.mining.entity.TaskManageList;
import com.zatgo.zup.mining.entity.UserTaskInfoRecord;
import com.zatgo.zup.mining.entity.UserTaskInfoRecordModel;
import com.zatgo.zup.mining.entity.response.GetRewardRecordResponse;
import com.zatgo.zup.mining.entity.response.GetRewardResponse;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2018/8/7.
 */
public interface TaskService {


    /**
     * 任务列表(app调用)
     * @return
     */
    ResponseData<Map<String, List<TaskManageList>>> taskListDate(String userId, String cloudUserId) throws ParseException;

    /**
     * 阅读任务完成
     * @param userId
     * @param taskId
     * @return
     */
    ResponseData taskDone(String userId, String userName, String taskId, BusinessEnum.ComputePowerSourceType source, String businessId);

    /**
     * 用户登陆任务完成
     * @param userId
     * @return
     */
    ResponseData loginTaskDone(String userId, String userName, String cloudUserId, String businessId);

    /**
     * 邀请码任务
     * @param invitedUserId
     * @throws ParseException
     */
    void invitationTaskDone(Long count, String invitedUserId, String cloudUserId, String businessId);

    /**
     * 注册任务
     * @param userId
     * @return
     */
    ResponseData registerTaskDone(String userId, String cloudUserId, String businessId);

    /**
     * 任务记录列表
     * @param request
     * @return
     */
    PageInfo<UserTaskInfoRecordModel> recordList(TaskRecordListRequest request);

    /**
     * 更具任务Id获取任务
     * @param cloudUserId
     * @param taskId
     * @return
     */
    List<TaskManageList> getTask(String cloudUserId, String taskId, Date date, Byte special);

    /**
     * 判断任务是否完成
     * @param userTaskInfoRecords
     * @param taskManage
     * @param userId
     * @return
     */
//    Boolean isDone(List<UserTaskInfoRecord> userTaskInfoRecords, TaskManage taskManage, String userId);

    /**
     * 查询任务记录
     * @param taskId
     * @param userId
     * @param startDate
     * @param endDate
     * @return
     */
    List<UserTaskInfoRecord> getTaskRecord(String taskId, String userId, Date startDate, Date endDate);

    /**
     * 领奖
     * @param taskId
     * @param user
     * @return
     */
    GetRewardRecordResponse getReward(String taskId, AuthUserInfo user);

    /**
     * 领奖记录
     * @param taskId
     * @param user
     * @return
     */
    GetRewardRecordResponse getRewardRecord(String taskId, AuthUserInfo user);
}
