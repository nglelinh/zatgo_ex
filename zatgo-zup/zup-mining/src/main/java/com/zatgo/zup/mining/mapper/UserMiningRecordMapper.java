package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.UserMiningRecord;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMiningRecordMapper extends BaseMapper<UserMiningRecord>{

    List<UserMiningRecord> getUserMiningRecords(String userId);
}