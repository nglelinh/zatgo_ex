package com.zatgo.zup.mining.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.GameEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.*;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.mining.entity.*;
import com.zatgo.zup.mining.entity.response.GetRewardRecordResponse;
import com.zatgo.zup.mining.entity.response.GetRewardResponse;
import com.zatgo.zup.mining.mapper.*;
import com.zatgo.zup.mining.remoteservice.CouponsRemoveService;
import com.zatgo.zup.mining.remoteservice.DepositService;
import com.zatgo.zup.mining.remoteservice.GameRemoteService;
import com.zatgo.zup.mining.remoteservice.UserService;
import com.zatgo.zup.mining.service.MiningService;
import com.zatgo.zup.mining.service.TaskService;
import com.zatgo.zup.mining.util.DateUtil;
import com.zatgo.zup.mining.util.GameRewardCarveUp;
import com.zatgo.zup.mining.util.TaskInfoManage;
import com.zatgo.zup.mining.util.TaskUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.*;

/**
 * Created by 46041 on 2018/8/7.
 */

@Service("taskService")
public class TaskServiceImpl implements TaskService{

    private static final Logger logger = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Autowired
    private TaskManageMapper taskManageMapper;
    @Autowired
    private UserTaskInfoRecordMapper userTaskInfoRecordMapper;
    @Autowired
    private MiningService miningService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private DepositService depositService;
    @Autowired
    private TaskUtil taskUtil;
    @Autowired
    private GameRemoteService gameRemoteService;
    @Autowired
    private GameRewardCarveUp gameRewardCarveUp;
    @Autowired
    private SubTaskRewardManageMapper subTaskRewardManageMapper;
    @Autowired
    private UserTaskInfoRewardRecordMapper userTaskInfoRewardRecordMapper;
    @Autowired
    private SubTaskManageMapper subTaskManageMapper;
    @Autowired
    private CouponsRemoveService couponsRemoveService;

    @Override
    public ResponseData<Map<String, List<TaskManageList>>> taskListDate(String userId, String cloudUserId) throws ParseException {
        //获取所有的任务
        List<TaskManageList> taskManages = taskManageMapper.taskList(cloudUserId, null, null, null, null);
        //任务分个类
        Map<String, TaskManageList> map = new HashMap<>();
        Map<String, List<SubTaskManageList>> ruleMap = new HashMap<>();
        Map<String, List<TaskManageList>> resultMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(taskManages)){
            TaskInfoManage taskInfoManage = new TaskInfoManage(taskManages);
            for (TaskManageList taskManage : taskManages){
                List<SubTaskManageList> subTaskManageLists = taskManage.getList();
                subTaskManageLists.forEach(subTask -> {
                    String ruleType = subTask.getRuleType();
                    if (ruleMap.containsKey(ruleType)){
                        List<SubTaskManageList> list = ruleMap.get(ruleType);
                        list.add(subTask);
                    } else {
                        List<SubTaskManageList> list = new ArrayList<>();
                        list.add(subTask);
                        ruleMap.put(ruleType, list);
                    }
                });
                map.put(taskManage.getId(), taskManage);
            }

            //获取规则为0的任务的完成情况
            //从不重置
            if (ruleMap.containsKey("0")){
                taskRuleType0(ruleMap, userId, taskInfoManage);
            }
            //获取规则为1的任务的完成情况
            //每隔 execute_interval 分钟记一次, 每次奖励 once_reward,每天重置一次
            if (ruleMap.containsKey("1")){
                taskRuleType1(ruleMap, userId, taskInfoManage);
            }
            //获取规则为2的任务的完成情况
            //购物 每月重置一次, 每次奖励 once_reward.
            if (ruleMap.containsKey("2")){
                taskRuleType2(ruleMap, userId, taskInfoManage);
            }
            //获取规则为3的任务的完成情况
            //总共 interval 次, 每次奖励 once_reward.
            if (ruleMap.containsKey("3")) {
                taskRuleType3(ruleMap, userId, taskInfoManage);
            }
            //获取规则为4的任务的完成情况
            //每天重置一次,每次奖励 once_reward
            if (ruleMap.containsKey("4")) {
                taskRuleType4(ruleMap, userId, taskInfoManage);
            }
            //如果是周期性的任务
            if (ruleMap.containsKey("9")){
                taskRuleType5(ruleMap, userId, taskInfoManage);
            }
            //把数据做成前端要的样子
            for (Map.Entry<String, TaskManageList> taskManage : map.entrySet()){
                TaskManageList value = taskManage.getValue();
                Integer[] taskInfo = taskInfoManage.getTaskInfo(value.getId());
                String type = value.getType();
                SubTaskManageList subTaskManageList = value.getList().get(0);
                Byte special = value.getSpecial();
                if (resultMap.containsKey(type)){
                    List<TaskManageList> list = resultMap.get(type);
                    value.setCount(taskInfo[1]);
                    value.setIsDone(taskInfo[0]);
                    value.setExecuteInterval(subTaskManageList.getExecuteInterval());
                    value.setCount(subTaskManageList.getCount());
                    value.setRewardUp(subTaskManageList.getRewardUp());
                    value.setRuleType(new Byte("1").equals(special) ? "6" : "");
                    list.add(value);
                } else {
                    List<TaskManageList> list = new ArrayList<>();
                    value.setCount(taskInfo[1]);
                    value.setIsDone(taskInfo[0]);
                    value.setExecuteInterval(subTaskManageList.getExecuteInterval());
                    value.setCount(subTaskManageList.getCount());
                    value.setRewardUp(subTaskManageList.getRewardUp());
                    value.setRuleType(new Byte("1").equals(special) ? "6" : "");
                    list.add(value);
                    resultMap.put(type, list);
                }
            }
        }
        //查他任务完成历史
        return BusinessResponseFactory.createSuccess(resultMap);
    }

    @Override
    @Transactional
    public ResponseData taskDone(String userId, String userName, String taskId, BusinessEnum.ComputePowerSourceType source, String businessId) {
        ZhuanpanRunModel data = new ZhuanpanRunModel();
        try {
            UserTaskInfoRecordExample userTaskInfoRecordExample = new UserTaskInfoRecordExample();
            userTaskInfoRecordExample.createCriteria().andBusinessIdEqualTo(businessId);
            int i = userTaskInfoRecordMapper.countByExample(userTaskInfoRecordExample);
            if (i > 0){
                logger.error("重复数据处理" + businessId);
                throw new BusinessException(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING);
            }
            Date date = new Date();
            List<TaskManageList> taskManages = taskManageMapper.taskList(null, taskId, date, "1", null);
            if (CollectionUtils.isEmpty(taskManages)){
                logger.error("没有查到对应的任务" + taskId);
                throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
            }
            TaskInfoManage taskInfoManage = new TaskInfoManage(taskManages);
            TaskManageList taskManage = taskManages.get(0);
            List<SubTaskManageList> list = taskManage.getList();
            for (SubTaskManageList subTask : list) {
                long time = date.getTime();
                if (subTask.getStartDate().getTime() <= time && subTask.getEndDate().getTime() > time){
                    String ruleType = subTask.getRuleType();
                    UserTaskInfoRecordExample example = new UserTaskInfoRecordExample();
                    UserTaskInfoRecordExample.Criteria criteria = example.createCriteria();
                    List<UserTaskInfoRecord> userTaskInfoRecords = null;
                    //获取规则为0的任务的完成情况
                    //从不重置
                    if ("0".equals(ruleType)) {
                        criteria.andCreateDateGreaterThanOrEqualTo(taskManage.getStartDate());
                        criteria.andCreateDateLessThan(taskManage.getEndDate());
                    }
                    //获取规则为1的任务的完成情况
                    //每隔 execute_interval 分钟记一次, 每次奖励 once_reward,每天重置一次
                    if ("1".equals(ruleType)) {
                        criteria.andCreateDateGreaterThanOrEqualTo(DateUtil.getDate(0));
                    }
                    //获取规则为2的任务的完成情况
                    //购物 每月重置一次, 每次奖励 once_reward.
                    if ("2".equals(ruleType)) {
                        criteria.andCreateDateGreaterThanOrEqualTo(DateUtil.getMonth(0));
                    }
                    //获取规则为3,6的任务的完成情况
                    //总共 interval 次, 每次奖励 once_reward.
                    if ("3".equals(ruleType)) {
                        criteria.andCreateDateGreaterThanOrEqualTo(taskManage.getStartDate());
                        criteria.andCreateDateLessThan(taskManage.getEndDate());
                    }
                    //获取规则为4的任务的完成情况
                    //每天重置一次,每次奖励 once_reward
                    if ("4".equals(ruleType)) {
                        criteria.andCreateDateGreaterThanOrEqualTo(DateUtil.getDate(0));
                    }
                    //9为周期性任务，任务时间为N天，N天里面有1-X个周期，一个周期内满足任务要求，才能获得这个周期的奖励
                    if ("9".equals(ruleType)){
                        criteria.andCreateDateGreaterThanOrEqualTo(taskManage.getStartDate());
                        criteria.andCreateDateLessThan(taskManage.getEndDate());
                    }
                    criteria.andTaskIdEqualTo(taskId);
                    criteria.andUserIdEqualTo(userId);
                    userTaskInfoRecords = userTaskInfoRecordMapper.selectByExample(example);
                    taskInfoManage.judgeSubTask(userTaskInfoRecords);


                    Boolean flag = taskInfoManage.subTaskIsDone(taskId, subTask.getId());
                    //创建任务记录
                    UserTaskInfoRecord record = new UserTaskInfoRecord();
                    record.setId(UUIDUtils.getUuid());
                    record.setUserId(userId);
                    record.setTaskId(taskId);
                    record.setSubTaskId(subTask.getId());
                    record.setCreateDate(date);
                    record.setBusinessId(businessId);

                    //先判断是不是周期性任务
                    if ("9".equals(ruleType)){
                        //如果是周期性任务，而且做过了，直接跳过
                        List<UserTaskInfoRecord> records = todayTaskRecords(userId, taskId, subTask.getId());
                        if (!CollectionUtils.isEmpty(records)){
                            data.setResult("0");
                            continue;
                        }
                    }
                    //任务完成，保存记录，发奖
                    if (!flag || (flag && "9".equals(ruleType))){
                        grantReward(record, subTask, data, source, userName, userId, taskManage.getCloudUserId());
                    }
                    //如果是周期性任务没完成，依旧添加任务记录
                    if (!flag && "9".equals(ruleType)){
                        userTaskInfoRecordMapper.insertSelective(record);
                    }
                }
            }
        } catch (ParseException e){
            return BusinessResponseFactory.createSystemError();
        }
        return BusinessResponseFactory.createSuccess(data);
    }

    @Override
    public ResponseData loginTaskDone(String userId, String userName, String cloudUserId, String businessId) {
        List<TaskManageList> taskManages = getTask(cloudUserId, null, null, new Byte("0"));
        //解决同一用户并发问题
        String accountLockKey = RedisKeyConstants.TASK_LOCK + userId;
        redisLockUtils.lock(accountLockKey);
        try {
            //添加任务记录
            if (!CollectionUtils.isEmpty(taskManages)){
                for (int i = 0; i < taskManages.size(); i++){
                    TaskManageList taskManage = taskManages.get(i);
                    taskDone(userId, userName, taskManage.getId(), BusinessEnum.ComputePowerSourceType.LOGIN, businessId + i);
                }
            }
        } finally {
            redisLockUtils.releaseLock(accountLockKey);
        }
        return BusinessResponseFactory.createSuccess(null);
    }

    @Override
    public void invitationTaskDone(Long count, String invitedUserId, String cloudUserId, String businessId){
        //获取邀请码任务
        List<TaskManageList> taskManages = getTask(cloudUserId, null, null, new Byte("1"));
        //解决同一用户并发问题
        String accountLockKey = RedisKeyConstants.TASK_LOCK + invitedUserId;
        redisLockUtils.lock(accountLockKey);
        try {
            if (!CollectionUtils.isEmpty(taskManages)){
                for (int i = 0; i < taskManages.size(); i++){
                    TaskManageList taskManage = taskManages.get(i);
                    ResponseData responseData = taskDone(invitedUserId, null, taskManage.getId(), BusinessEnum.ComputePowerSourceType.INVITE, businessId + i);
                    logger.error(JSONObject.toJSONString(responseData));
                }
            }
        } finally {
            redisLockUtils.releaseLock(accountLockKey);
        }
    }

    @Override
    public ResponseData registerTaskDone(String userId, String cloudUserId, String businessId) {
        List<TaskManageList> taskManages = getTask(cloudUserId, null, null, new Byte("3"));

        if (!CollectionUtils.isEmpty(taskManages)){
            for (int i = 0; i < taskManages.size(); i++) {
                TaskManageList taskManage = taskManages.get(i);
                ResponseData responseData = taskDone(userId, null, taskManage.getId(), BusinessEnum.ComputePowerSourceType.REGISTER, businessId + i);
                logger.error(JSONObject.toJSONString(responseData));
            }
        }
        return BusinessResponseFactory.createSuccess(null);
    }

    @Override
    public PageInfo<UserTaskInfoRecordModel> recordList(TaskRecordListRequest request) {
        PageHelper.startPage(request.getPageNo(), request.getPageSize());
        return new PageInfo<>(userTaskInfoRecordMapper.selectTaskRecordList(request.getUserId()));
    }

    @Override
    public List<TaskManageList> getTask(String cloudUserId, String taskId, Date date, Byte special) {
        List<TaskManageList> taskManageLists = taskManageMapper.taskList(cloudUserId, taskId, date, "1", special);
        return taskManageLists;
    }

    private void grantReward(UserTaskInfoRecord record, SubTaskManageList subTask, ZhuanpanRunModel data,
                             BusinessEnum.ComputePowerSourceType source, String userName, String userId, String cloudUserId){
        List<SubTaskRewardManage> rewardList = subTask.getList();
        for (SubTaskRewardManage reward : rewardList) {
            try {
                Boolean isInstantAward = reward.getIsInstantAward();
                String rewardId = reward.getId();
                byte type = reward.getType();
                BigDecimal num = reward.getNum();
                String aliasName = null;
                //只发即时领奖的
                if (isInstantAward){
                    UserTaskInfoRewardRecord rewardRecord = new UserTaskInfoRewardRecord();
                    //算力
                    if (type == BusinessEnum.TaskRewardType.compute.getCode().byteValue()){
                        addComputerPower(new Date(), num.longValue(), source, record.getUserId(), rewardRecord, BusinessEnum.ComputePowerType.PERMANENT);
                        GamePrizeModel prize = new GamePrizeModel();
                        data.setResult("1");
                        prize.setPrize("2");
                        prize.setValue(num.toPlainString());
                        data.setPrize(prize);
                    }

                    //币
                    if (type == BusinessEnum.TaskRewardType.coin.getCode().byteValue()){
                        //如果是限额的任务
                        if (!taskUtil.isInfinity(rewardId)){
                            //如果扣款成功了
                            if (taskUtil.cutBalance(rewardId, num)){
                                subTaskRewardManageMapper.updateCoinRemainingTotal(rewardId, num.negate());
                            } else {
                                //余额不够，直接return
                                return;
                            }
                        }
                        addCoin(rewardRecord, reward.getCoinNetworkType(), reward.getCoinType(), reward.getNum(), record.getUserId(), userName, BusinessEnum.PaymentRecordType.task);
                    }

                    //奖励  券
                    if (type == BusinessEnum.TaskRewardType.coupon.getCode().byteValue()){
                        try {
                            couponsRemoveService.triggerGetCoupons(cloudUserId, userId, reward.getTriggerBusinessType(), reward.getTriggerBusinessCode());
                        } catch (Exception e){
                            logger.error("", e);
                        }
                    }

                    //加游戏次数
                    if (type == BusinessEnum.TaskRewardType.gameNumber.getCode().byteValue()){
                        gameRemoteService.addGameFreeNum(reward.getNum().intValue(), reward.getGameId(), userId);
                    }

                    //游戏奖励
                    if (type == BusinessEnum.TaskRewardType.game.getCode().byteValue()){
                        GetRewarRequest request = new GetRewarRequest();
                        request.setCoinType("zat");
                        request.setGameId(reward.getGameId());
                        request.setFree(true);
                        request.setUserId(record.getUserId());
                        ResponseData<ZhuanpanRunModel> res = gameRemoteService.getRewar(request);
                        if (!res.isSuccessful())
                            throw new BusinessException();
                        ZhuanpanRunModel resData = res.getData();
                        String result = resData.getResult();
                        GamePrizeModel gamePrize = resData.getPrize();
                        data.setPrize(gamePrize);
                        data.setResult(result);
                        //中奖
                        if (GameEnum.GameResult.WIN.getCode().equals(result)){
                            GamePrizeModel prize = resData.getPrize();
                            if (GameEnum.GamePrizeType.COIN.getCode().equals(prize.getPrize())){
                                BigDecimal amount = new BigDecimal(prize.getValue());
                                //是不是不限额的
                                if (BigDecimal.ZERO.compareTo(reward.getCoinTotal()) == -1){
                                    //限额的先扣费
                                    if (taskUtil.cutBalance(rewardId, amount)){
                                        addCoin(rewardRecord, prize.getCoinNetworkType(), prize.getCoinType(), amount, record.getUserId(), userName, BusinessEnum.PaymentRecordType.gamePay);
                                        rewardRecord.setCoinType(prize.getCoinType());
                                        rewardRecord.setCoinNetworkType(prize.getCoinNetworkType());
                                        rewardRecord.setNum(new BigDecimal(prize.getValue()));
                                        subTaskRewardManageMapper.updateCoinRemainingTotal(rewardId, amount.negate());
                                    } else {
                                        data.setResult(GameEnum.GameResult.LOSE.getCode());
                                    }
                                } else {
                                    //不限额度的  直接法
                                    addCoin(rewardRecord, prize.getCoinNetworkType(), prize.getCoinType(), amount, record.getUserId(), userName, BusinessEnum.PaymentRecordType.gamePay);
                                    rewardRecord.setCoinType(prize.getCoinType());
                                    rewardRecord.setCoinNetworkType(prize.getCoinNetworkType());
                                    rewardRecord.setNum(new BigDecimal(prize.getValue()));
                                }
                            }
                            if (GameEnum.GamePrizeType.SUANLI.getCode().equals(prize.getPrize())){
                                num = new BigDecimal(prize.getValue());
                                rewardRecord.setNum(num);
                                addComputerPower(new Date(), rewardRecord.getNum().longValue(), source, record.getUserId(), rewardRecord, BusinessEnum.ComputePowerType.PERMANENT);
                            }

                            if (GameEnum.GamePrizeType.COUPON.getCode().equals(prize.getPrize())){
                                aliasName = gamePrize.getValue() + gamePrize.getAliasName();
                                try{
                                    couponsRemoveService.triggerGetCoupons(cloudUserId, userId, prize.getTriggerBusinessType(), prize.getTriggerBusinessCode());
                                } catch (Exception e){
                                    logger.error("", e);
                                }
                            }
                        }
                    }

                    insertRecord(record.getId(), reward.getCoinNetworkType(), reward.getCoinType(), num, getRandomBlessings(reward.getBlessings()), type, aliasName);
                }
            } catch (Exception e) {
                logger.error("", e);
            }
        }
        data.setRecordId(record.getId());
        userTaskInfoRecordMapper.insertSelective(record);
    }

    private void insertRecord(String taskRecordId, String coinNetworkTyoe, String coinType,
                              BigDecimal num, String reward, Byte type, String aliasName){
        UserTaskInfoRewardRecord record = new UserTaskInfoRewardRecord();
        record.setId(UUIDUtils.getUuid());
        record.setCoinNetworkType(coinNetworkTyoe);
        record.setCoinType(coinType);
        record.setNum(num);
        record.setReward(reward);
        record.setTaskRecordId(taskRecordId);
        record.setType(type);
        record.setAliasName(aliasName);
        userTaskInfoRewardRecordMapper.insertSelective(record);
    }


    /**
     * 检查必要参数
     * @return
     */
//    private boolean check(TaskManage taskManage){
//        String title = taskManage.getTitle();
//        String type = taskManage.getType();
//        String ruleType = taskManage.getRuleType();
//        Date startDate = taskManage.getStartDate();
//        Date endDate = taskManage.getEndDate();
//        Long rewardUp = taskManage.getRewardUp();
//        Long onceReward = taskManage.getOnceReward();
//        if (startDate == null && endDate == null){
//            startDate = new Date();
//            try {
//                endDate = DateUtil.getYear(100);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            taskManage.setStartDate(startDate);
//            taskManage.setEndDate(endDate);
//        }
//        if (StringUtils.isEmpty(type) || StringUtils.isEmpty(ruleType) ||
//                StringUtils.isEmpty(rewardUp) || StringUtils.isEmpty(onceReward) ||
//                StringUtils.isEmpty(title))
//            return false;
//        if (endDate.getTime() <= startDate.getTime())
//            return false;
//        return true;
//    }

    private void processingData(UserTaskInfoRecordExample userTaskInfoRecordExample, TaskInfoManage taskInfoManage){
        //查出所有已完成的任务,改变状态
        List<UserTaskInfoRecord> userTaskInfoRecords = userTaskInfoRecordMapper.selectByExample(userTaskInfoRecordExample);
        taskInfoManage.judgeSubTask(userTaskInfoRecords);
    }

    /**
     * 获取规则为0的任务的完成情况
     * 从不重置
     * @param ruleMap
     * @param userId
     */
    private void taskRuleType0(Map<String, List<SubTaskManageList>> ruleMap, String userId, TaskInfoManage taskInfoManage){
        List<String> idList = new ArrayList<>();
        List<SubTaskManageList> list = ruleMap.get("0");
        for (int i = 0; i < list.size(); i++){
            idList.add(list.get(i).getId());
        }
        UserTaskInfoRecordExample userTaskInfoRecordExample = new UserTaskInfoRecordExample();
        UserTaskInfoRecordExample.Criteria criteria = userTaskInfoRecordExample.createCriteria();
        criteria.andSubTaskIdIn(idList);
        criteria.andUserIdEqualTo(userId);
        processingData(userTaskInfoRecordExample, taskInfoManage);
    }

    /**
     * 获取规则为1的任务的完成情况
     * 每隔 execute_interval 分钟记一次, 每次奖励 once_reward,每天重置一次
     * @param ruleMap
     * @param userId
     */
    private void taskRuleType1(Map<String, List<SubTaskManageList>> ruleMap, String userId, TaskInfoManage taskInfoManage) throws ParseException {
        List<String> idList = new ArrayList<>();
        List<SubTaskManageList> list = ruleMap.get("1");
        for (int i = 0; i < list.size(); i++){
            idList.add(list.get(i).getId());
        }
        UserTaskInfoRecordExample userTaskInfoRecordExample = new UserTaskInfoRecordExample();
        UserTaskInfoRecordExample.Criteria criteria = userTaskInfoRecordExample.createCriteria();
        criteria.andSubTaskIdIn(idList);
        criteria.andUserIdEqualTo(userId);
        criteria.andCreateDateGreaterThanOrEqualTo(DateUtil.getDate(0));
        processingData(userTaskInfoRecordExample, taskInfoManage);
    }

    /**
     * 获取规则为2的任务的完成情况
     * 购物 每月重置一次, 每次奖励 once_reward
     * @param ruleMap
     * @param userId
     */
    private void taskRuleType2(Map<String, List<SubTaskManageList>> ruleMap, String userId, TaskInfoManage taskInfoManage) throws ParseException {
        List<String> idList = new ArrayList<>();
        List<SubTaskManageList> list = ruleMap.get("2");
        for (int i = 0; i < list.size(); i++){
            idList.add(list.get(i).getId());
        }
        UserTaskInfoRecordExample userTaskInfoRecordExample = new UserTaskInfoRecordExample();
        UserTaskInfoRecordExample.Criteria criteria = userTaskInfoRecordExample.createCriteria();
        criteria.andSubTaskIdIn(idList);
        criteria.andUserIdEqualTo(userId);
        criteria.andCreateDateGreaterThanOrEqualTo(DateUtil.getMonth(0));
        processingData(userTaskInfoRecordExample, taskInfoManage);
    }

    /**
     * 获取规则为3的任务的完成情况
     * 总共 interval 次, 每次奖励 once_reward
     * @param ruleMap
     * @param userId
     */
    private void taskRuleType3(Map<String, List<SubTaskManageList>> ruleMap, String userId, TaskInfoManage taskInfoManage){
        List<SubTaskManageList> list = ruleMap.get("3");
        List<SubTaskManageList> inviteList = ruleMap.get("6");
        if (CollectionUtils.isEmpty(list)){
            list = inviteList;
        } else if (!CollectionUtils.isEmpty(inviteList)){
            list.addAll(inviteList);
        }
        List<String> idList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            idList.add(list.get(i).getId());
        }
        UserTaskInfoRecordExample userTaskInfoRecordExample = new UserTaskInfoRecordExample();
        UserTaskInfoRecordExample.Criteria criteria = userTaskInfoRecordExample.createCriteria();
        criteria.andSubTaskIdIn(idList);
        criteria.andUserIdEqualTo(userId);
        processingData(userTaskInfoRecordExample, taskInfoManage);
    }

    /**
     * 获取规则为4的任务的完成情况
     * 每天重置一次,每次奖励 once_reward
     * @param ruleMap
     * @param userId
     */
    private void taskRuleType4(Map<String, List<SubTaskManageList>> ruleMap, String userId, TaskInfoManage taskInfoManage) throws ParseException {
        List<String> idList = new ArrayList<>();
        List<SubTaskManageList> list = ruleMap.get("4");
        List<SubTaskManageList> loginList = ruleMap.get("5");
        if (CollectionUtils.isEmpty(list)){
            list = loginList;
        } else if (!CollectionUtils.isEmpty(loginList)){
            list.addAll(loginList);
        }

        for (int i = 0; i < list.size(); i++) {
            idList.add(list.get(i).getId());
        }
        UserTaskInfoRecordExample userTaskInfoRecordExample = new UserTaskInfoRecordExample();
        UserTaskInfoRecordExample.Criteria criteria = userTaskInfoRecordExample.createCriteria();
        criteria.andSubTaskIdIn(idList);
        criteria.andUserIdEqualTo(userId);
        criteria.andCreateDateGreaterThanOrEqualTo(DateUtil.getDate(0));
        processingData(userTaskInfoRecordExample, taskInfoManage);
    }

    /**
     * 周期性任务
     * @param ruleMap
     * @param userId
     */
    private void taskRuleType5(Map<String, List<SubTaskManageList>> ruleMap, String userId, TaskInfoManage taskInfoManage) {
        List<String> idList = new ArrayList<>();
        List<SubTaskManageList> list = ruleMap.get("9");
        for (int i = 0; i < list.size(); i++){
            idList.add(list.get(i).getId());
        }
        UserTaskInfoRecordExample userTaskInfoRecordExample = new UserTaskInfoRecordExample();
        UserTaskInfoRecordExample.Criteria criteria = userTaskInfoRecordExample.createCriteria();
        criteria.andSubTaskIdIn(idList);
        criteria.andUserIdEqualTo(userId);
        processingData(userTaskInfoRecordExample, taskInfoManage);
    }

    /**
     * 判断任务是否完成
     * @return
     */
//    public Boolean isDone(List<UserTaskInfoRecord> userTaskInfoRecords, TaskManage taskManage, String userId){
//        if (CollectionUtils.isEmpty(userTaskInfoRecords))
//            return false;
//        int count = userTaskInfoRecords.size();
//        //是否为完成一个周期后才能完成任务
//        if (taskManage.getCycleNumber().equals(-1)){
//            if (count < taskManage.getRewardUp().longValue())
//                return false;
//        } else {
//            int size = userTaskInfoRecords.size();
//            int cycleNumber = taskManage.getCycleNumber().intValue();
//            int limit = taskManage.getRewardUp().intValue();
//            //如果不是周期的倍数 且 总次数没到
//            if (size < cycleNumber * limit){
//                if (CollectionUtils.isEmpty(todayTaskRecords(userId, taskManage.getId()))){
//                    return false;
//                }
//            }
//        }
//
//        return true;
//    }

    @Override
    public List<UserTaskInfoRecord> getTaskRecord(String taskId, String userId, Date startDate, Date endDate) {
        UserTaskInfoRecordExample example = new UserTaskInfoRecordExample();
        UserTaskInfoRecordExample.Criteria criteria = example.createCriteria();
        criteria.andTaskIdEqualTo(taskId);
        criteria.andUserIdEqualTo(userId);
        if (startDate != null){
            criteria.andCreateDateGreaterThanOrEqualTo(startDate);
        }
        if (endDate != null){
            criteria.andCreateDateLessThan(endDate);
        }
        return userTaskInfoRecordMapper.selectByExample(example);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 50; i ++){
            System.out.println(UUIDUtils.getUuid());
        }
    }

    @Override
    @Transactional
    public GetRewardRecordResponse getReward(String taskId, AuthUserInfo user) {
        String cloudUserId = user.getCloudUserId();
        List<TaskManageList> tasks = getTask(cloudUserId, taskId, null, null);
        if (CollectionUtils.isEmpty(tasks))
            return null;
        TaskManageList task = tasks.get(0);
        TaskInfoManage taskInfoManage = new TaskInfoManage(Arrays.asList(task));
        List<SubTaskManageList> list = task.getList();
        GetRewardRecordResponse rewardRecordResponse = new GetRewardRecordResponse();
        list.forEach(subTask -> {
            List<SubTaskRewardManage> subTaskRewardList = subTask.getList();
            subTaskRewardList.forEach(subTaskReward -> {
                String gameId = subTaskReward.getGameId();
                //任务是需要领奖的
                if (task != null && !subTaskReward.getIsInstantAward()){
                    Date startDate = subTaskReward.getAwardStartDate();
                    Date endDate = subTaskReward.getAwardEndDate();
                    Date date = new Date();
                    long time = date.getTime();
                    //时间在领奖范围内
                    if (time < startDate.getTime() || time > endDate.getTime()){
                        throw new BusinessException(BusinessExceptionCode.GAME_NOT_IN_AWARD_TIME);
                    }
                    //捞答题记录
                    AnswerRecordRequest request = new AnswerRecordRequest();
                    request.setStartDate(task.getStartDate());
                    request.setEndDate(task.getEndDate());
                    request.setGameId(gameId);
                    request.setUserId(user.getUserId());
                    request.setTrue(true);
                    ResponseData<List<AnswerQuestionResultModel>> responseData = gameRemoteService.getAnswerRecord(request);
                    if (!responseData.isSuccessful())
                        throw new BusinessException();
                    List<AnswerQuestionResultModel> results = responseData.getData();
                    List<UserTaskInfoRecord> taskRecord = getTaskRecord(taskId, user.getUserId(), null, null);
                    taskInfoManage.judgeSubTask(taskRecord);
                    //看是否领过奖了
                    if (taskInfoManage.getTaskInfo(taskId)[0] == 1){
                        throw new BusinessException(BusinessExceptionCode.GAME_AWARD_HAS_BEEN_RECEIVED);
                    }
                    UserTaskInfoRecord record = new UserTaskInfoRecord();
                    record.setId(UUIDUtils.getUuid());
                    record.setCreateDate(new Date());
                    record.setUserId(user.getUserId());
                    record.setTaskId(taskId);
                    record.setSubTaskId(subTask.getId());
                    List<GetRewardResponse> response = new ArrayList<>();
                    int size = results.size();
                    userTaskInfoRecordMapper.insertSelective(record);
                    getGameReward(size, subTaskReward, user, record, response, rewardRecordResponse, gameId, task);
                    rewardRecordResponse.setFen(size);
                }
            });
        });
        return rewardRecordResponse;
    }

    @Override
    public GetRewardRecordResponse getRewardRecord(String taskId, AuthUserInfo user) {
        GetRewardRecordResponse rewardRecordResponse = new GetRewardRecordResponse();
        rewardRecordResponse.setGet(false);
        List<TaskManageList> tasks = getTask(user.getCloudUserId(), taskId, null, null);
        if (CollectionUtils.isEmpty(tasks))
            return null;
        TaskManageList task = tasks.get(0);
        String gameId = task.getList().get(0).getList().get(0).getGameId();
        AnswerRecordRequest request = new AnswerRecordRequest();
        request.setStartDate(task.getStartDate());
        request.setEndDate(task.getEndDate());
        request.setGameId(gameId);
        request.setUserId(user.getUserId());
        request.setTrue(true);
        ResponseData<List<AnswerQuestionResultModel>> answerRecord = gameRemoteService.getAnswerRecord(request);
        if (answerRecord == null || !answerRecord.isSuccessful()){
            throw new BusinessException();
        }
        List<AnswerQuestionResultModel> data = answerRecord.getData();
        int count = data.size();
        rewardRecordResponse.setFen(count);
        UserTaskInfoRecordExample example = new UserTaskInfoRecordExample();
        example.createCriteria().andTaskIdEqualTo(taskId).andUserIdEqualTo(user.getUserId());
        List<UserTaskInfoRecord> records = userTaskInfoRecordMapper.selectByExample(example);
        List<GetRewardResponse> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(records)){
            rewardRecordResponse.setGet(true);
            UserTaskInfoRecord r = records.get(0);
            UserTaskInfoRewardRecordExample rewardRecordExample = new UserTaskInfoRewardRecordExample();
            rewardRecordExample.createCriteria().andTaskRecordIdEqualTo(r.getId());
            List<UserTaskInfoRewardRecord> userTaskInfoRewardRecords = userTaskInfoRewardRecordMapper.selectByExample(rewardRecordExample);
            if (!CollectionUtils.isEmpty(userTaskInfoRewardRecords)){
                for (UserTaskInfoRewardRecord record : userTaskInfoRewardRecords){
                    BigDecimal computePower = record.getNum();
                    Byte type = record.getType();
                    if (new Byte("0").byteValue() == type.byteValue()){
                        GetRewardResponse response = new GetRewardResponse();
                        response.setType(1);
                        response.setNum(computePower);
                        response.setFen(count);
                        list.add(response);
                    }
                    if (new Byte("1").byteValue() == type.byteValue()){
                        GetRewardResponse response = new GetRewardResponse();
                        response.setType(3);
                        response.setNum(computePower);
                        response.setCoinNetwork(record.getCoinNetworkType());
                        response.setCoinType(record.getCoinType());
                        response.setFen(count);
                        list.add(response);
                    }
                    if (new Byte("3").byteValue() == type.byteValue()){
                        GetRewardResponse response = new GetRewardResponse();
                        response.setType(4);
                        response.setAliasName(record.getAliasName());
                        list.add(response);
                    }
                    String reward = record.getReward();
                    if (!StringUtils.isEmpty(reward)){
                        GetRewardResponse response = new GetRewardResponse();
                        response.setType(2);
                        response.setNum(new BigDecimal(reward));
                        response.setFen(count);
                        list.add(response);
                    }
                }
            }
        }
        rewardRecordResponse.setList(list);
        return rewardRecordResponse;
    }

    private void getGameReward(int size, SubTaskRewardManage reward, AuthUserInfo user,
                               UserTaskInfoRecord record, List<GetRewardResponse> response,
                               GetRewardRecordResponse rewardRecordResponse, String gameId, TaskManageList task){
        UserTaskInfoRewardRecord rewardRecord = new UserTaskInfoRewardRecord();
        rewardRecord.setId(UUIDUtils.getUuid());
        rewardRecord.setTaskRecordId(record.getId());
        ZhuanpanRunModel gameRandomReward = null;
        size = size * 2;
        //积分大于20
        if (size >= 60){
            gameRandomReward = getGameRandomReward("0ddbf989d5d34428b7afe61d6af87159", reward, user.getUserId(), user.getCloudUserId(), user.getUserName(),
                    BusinessEnum.ComputePowerSourceType.GAME, record);
            GamePrizeModel prize = gameRandomReward.getPrize();
            GetRewardResponse res = new GetRewardResponse();
            res.setNum(new BigDecimal(prize.getValue()));
            res.setType(new Integer(prize.getType()));
            res.setFen(size);
            res.setPrize(prize);
            response.add(res);
            rewardRecordResponse.setGet(true);
            rewardRecordResponse.setList(response);
            return;
        }
        //大于15
        if (size >= 40){
            gameRandomReward = getGameRandomReward("44fca667a96b4a9eaae5dbfd7b42ff4a", reward, user.getUserId(), user.getCloudUserId(), user.getUserName(),
                    BusinessEnum.ComputePowerSourceType.GAME, record);
            GetRewardResponse res = new GetRewardResponse();
            GamePrizeModel prize = gameRandomReward.getPrize();
            res.setNum(new BigDecimal(prize.getValue()));
            res.setType(new Integer(prize.getType()));
            res.setFen(size);
            res.setPrize(prize);
            response.add(res);
            rewardRecordResponse.setGet(true);
            rewardRecordResponse.setList(response);
            return;
        }
        //大于10
        if (size >= 25){
            gameRandomReward = getGameRandomReward("a1a8552d07974ff99b1547b1292f8b02", reward, user.getUserId(), user.getCloudUserId(), user.getUserName(),
                    BusinessEnum.ComputePowerSourceType.GAME, record);
            GetRewardResponse res = new GetRewardResponse();
            GamePrizeModel prize = gameRandomReward.getPrize();
            res.setNum(new BigDecimal(prize.getValue()));
            res.setType(new Integer(prize.getType()));
            res.setFen(size);
            res.setPrize(prize);
            response.add(res);
            rewardRecordResponse.setGet(true);
            rewardRecordResponse.setList(response);
            return;
        }
    }


    private ZhuanpanRunModel getGameRandomReward(String gameId, SubTaskRewardManage reward, String userId, String cloudUserId,
                                           String userName, BusinessEnum.ComputePowerSourceType source, UserTaskInfoRecord record){
        ZhuanpanRunModel data = new ZhuanpanRunModel();
        GetRewarRequest request = new GetRewarRequest();
        UserTaskInfoRewardRecord rewardRecord = new UserTaskInfoRewardRecord();
        String rewardId = reward.getId();
        request.setCoinType("zat");
        request.setGameId(gameId);
        request.setFree(true);
        request.setUserId(gameId);
        ResponseData<ZhuanpanRunModel> res = gameRemoteService.getRewar(request);
        if (!res.isSuccessful())
            throw new BusinessException();
        ZhuanpanRunModel resData = res.getData();
        String result = resData.getResult();
        GamePrizeModel gamePrize = resData.getPrize();
        String aliasName = null;
        data.setPrize(gamePrize);
        data.setResult(result);
        if (GameEnum.GameResult.WIN.getCode().equals(result)){
            GamePrizeModel prize = resData.getPrize();
            if (GameEnum.GamePrizeType.COIN.getCode().equals(prize.getPrize())){
                reward.setType(BusinessEnum.TaskRewardType.coin.getCode());
                BigDecimal amount = new BigDecimal(prize.getValue());
                //是不是不限额的
                if (BigDecimal.ZERO.compareTo(reward.getCoinTotal()) == -1){
                    //限额的先扣费
                    if (taskUtil.cutBalance(rewardId, amount)){
                        addCoin(rewardRecord, prize.getCoinNetworkType(), prize.getCoinType(), amount, record.getUserId(), userName, BusinessEnum.PaymentRecordType.gamePay);
                        rewardRecord.setCoinType(prize.getCoinType());
                        rewardRecord.setCoinNetworkType(prize.getCoinNetworkType());
                        rewardRecord.setNum(new BigDecimal(prize.getValue()));
                        subTaskRewardManageMapper.updateCoinRemainingTotal(rewardId, amount.negate());
                    } else {
                        data.setResult(GameEnum.GameResult.LOSE.getCode());
                    }
                } else {
                    //不限额度的  直接发
                    addCoin(rewardRecord, prize.getCoinNetworkType(), prize.getCoinType(), amount, record.getUserId(), userName, BusinessEnum.PaymentRecordType.gamePay);
                    rewardRecord.setCoinType(prize.getCoinType());
                    rewardRecord.setCoinNetworkType(prize.getCoinNetworkType());
                    rewardRecord.setNum(new BigDecimal(prize.getValue()));
                }
            }
            if (GameEnum.GamePrizeType.SUANLI.getCode().equals(prize.getPrize())){
                reward.setType(BusinessEnum.TaskRewardType.compute.getCode());
                rewardRecord.setNum(new BigDecimal(prize.getValue()));
                addComputerPower(new Date(), rewardRecord.getNum().longValue(), source, record.getUserId(), rewardRecord, BusinessEnum.ComputePowerType.PERMANENT);
            }

            if (GameEnum.GamePrizeType.COUPON.getCode().equals(prize.getPrize())){
                try{
                    reward.setType(BusinessEnum.TaskRewardType.coupon.getCode());
                    aliasName = gamePrize.getValue() + gamePrize.getAliasName();
                    couponsRemoveService.triggerGetCoupons(cloudUserId, userId, prize.getTriggerBusinessType(), prize.getTriggerBusinessCode());
                } catch (Exception e){
                    logger.error("", e);
                }
            }
        }
        insertRecord(record.getId(), reward.getCoinNetworkType(), reward.getCoinType(), new BigDecimal(gamePrize.getValue()), getRandomBlessings(reward.getBlessings()), reward.getType(), aliasName);
        return data;
    }

    private List<UserTaskInfoRecord> todayTaskRecords(String userId, String taskId, String subTaskId) {
        List<UserTaskInfoRecord> userTaskInfoRecords = null;
        try {
            //查询今天的任务记录
            UserTaskInfoRecordExample example = new UserTaskInfoRecordExample();
            example.createCriteria().andTaskIdEqualTo(taskId)
                    .andUserIdEqualTo(userId)
                    .andSubTaskIdEqualTo(subTaskId)
                    .andCreateDateLessThan(DateUtil.getDate(1))
                    .andCreateDateGreaterThan(DateUtil.getDate(0));
            userTaskInfoRecords = userTaskInfoRecordMapper.selectByExample(example);
        } catch (ParseException e) {
            logger.error("", e);
        }
        return userTaskInfoRecords;
    }


    private String getRandomBlessings(String blessings){
        if (!StringUtils.isEmpty(blessings)){
            String[] split = blessings.split("_");
            int length = split.length;
            if (split != null && length >= 1){
                return split[new Integer(System.currentTimeMillis() % length + "")];
            }
        }
        return null;
    }

    private void addComputerPower(Date startDate, Long onceReward, BusinessEnum.ComputePowerSourceType source,
                                  String userId, UserTaskInfoRewardRecord record, BusinessEnum.ComputePowerType computePowerType){
        AddComputePowerParams params = new AddComputePowerParams();
        params.setValidStartDate(startDate);
        try {
            params.setValidEndDate(DateUtil.getYear(100));
        } catch (ParseException e) {
            logger.error("", e);
        }
        params.setComputeNum(onceReward);
        params.setComputePowerType(computePowerType);
        params.setComputePowerSourceType(source);
        params.setUserId(userId);
        miningService.addComputePower(params);
        record.setNum(new BigDecimal(onceReward));
        record.setType(new Byte("0"));
    }

    public void addCoin(UserTaskInfoRewardRecord record, String coinNetworkType, String coinType, BigDecimal coinNum,
                        String userId, String userName, BusinessEnum.PaymentRecordType paymentRecordType){
        MiningDepositInsertParams params = new MiningDepositInsertParams();
        params.setRecordId(record.getId());
        params.setCoinType(coinType);
        params.setAmount(coinNum.toPlainString());
        params.setUserId(userId);
        params.setNetWorkType(coinNetworkType);
        params.setUserName(userName);
        params.setType(paymentRecordType);
        ResponseData<Object> objectResponseData = depositService.insertMiningDepositRecored(params);
        if (!objectResponseData.isSuccessful()){
            throw new BusinessException(objectResponseData.getCode());
        }
        record.setNum(coinNum);
        record.setCoinNetworkType(coinNetworkType);
        record.setCoinType(coinType);
        record.setType(new Byte("1"));
    }
}
