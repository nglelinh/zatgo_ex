package com.zatgo.zup.mining.entity;

import java.util.Date;

public class SubTaskManage {
    private String id;

    private Integer cycleNumber;

    private String ruleType;

    private Long rewardUp;

    private Long executeInterval;

    private String taskId;

    private Date startDate;

    private Date endDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Integer getCycleNumber() {
        return cycleNumber;
    }

    public void setCycleNumber(Integer cycleNumber) {
        this.cycleNumber = cycleNumber;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType == null ? null : ruleType.trim();
    }

    public Long getRewardUp() {
        return rewardUp;
    }

    public void setRewardUp(Long rewardUp) {
        this.rewardUp = rewardUp;
    }

    public Long getExecuteInterval() {
        return executeInterval;
    }

    public void setExecuteInterval(Long executeInterval) {
        this.executeInterval = executeInterval;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId == null ? null : taskId.trim();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}