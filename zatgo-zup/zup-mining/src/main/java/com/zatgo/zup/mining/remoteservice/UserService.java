package com.zatgo.zup.mining.remoteservice;

import com.zatgo.zup.common.model.UserData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by 46041 on 2018/9/10.
 */

@FeignClient("zup-merchant")
public interface UserService {

    @GetMapping(value = "/user/userInfo/userId/{userId}")
    @ResponseBody
    UserData getUserInfoForTask(@PathVariable("userId") String userId);

    @GetMapping(value = "/user/userInfo/invitationCode/{invitationCode}")
    @ResponseBody
    List<UserData> getUserInfoByInvitationCode(@PathVariable("invitationCode") String invitationCode);

    @GetMapping(value = "/user/userId/invitationCode/{invitationCode}")
    @ResponseBody
    String getUserIdByInvitationCode(@PathVariable("invitationCode") String invitationCode);
}
