package com.zatgo.zup.mining.remoteservice;

import com.zatgo.zup.common.model.MiningDepositInsertParams;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * Created by cy on 2018/8/5.
 */
@FeignClient("zup-wallet")
public interface DepositService {

    @PostMapping(value = "/wallet/deposit/miningRecord")
    @ResponseBody
    ResponseData<Object> insertMiningDepositRecored(@RequestBody MiningDepositInsertParams params);
}
