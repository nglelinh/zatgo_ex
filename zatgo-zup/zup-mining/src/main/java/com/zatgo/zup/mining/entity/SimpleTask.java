package com.zatgo.zup.mining.entity;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2018/12/20.
 */
public class SimpleTask {

    private String rewardId;

    private BigDecimal total;

    private BigDecimal remainingTotal;

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getRemainingTotal() {
        return remainingTotal;
    }

    public void setRemainingTotal(BigDecimal remainingTotal) {
        this.remainingTotal = remainingTotal;
    }
}
