package com.zatgo.zup.mining.entity;

import java.math.BigDecimal;
import java.util.Date;

public class MineralPool {
    private String id;

    private String allComputingPower;

    private String coinNetworkType;

    private String coinType;

    private BigDecimal coinSum;

    private Date createDate;

    private Integer coinDecimals;

    private String coinImage;

    private Integer num;

    private String cloudUserId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAllComputingPower() {
        return allComputingPower;
    }

    public void setAllComputingPower(String allComputingPower) {
        this.allComputingPower = allComputingPower == null ? null : allComputingPower.trim();
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType == null ? null : coinNetworkType.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public BigDecimal getCoinSum() {
        return coinSum;
    }

    public void setCoinSum(BigDecimal coinSum) {
        this.coinSum = coinSum;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getCoinDecimals() {
        return coinDecimals;
    }

    public void setCoinDecimals(Integer coinDecimals) {
        this.coinDecimals = coinDecimals;
    }

    public String getCoinImage() {
        return coinImage;
    }

    public void setCoinImage(String coinImage) {
        this.coinImage = coinImage;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }
}