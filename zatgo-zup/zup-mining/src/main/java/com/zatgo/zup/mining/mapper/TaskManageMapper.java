package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.TaskManage;
import com.zatgo.zup.mining.entity.TaskManageList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
public interface TaskManageMapper extends BaseMapper<TaskManage> {


    /**
     *  查任务列表
     * @param cloudUserId
     * @return
     */
    List<TaskManageList> taskList(@Param("cloudUserId") String cloudUserId,
                                  @Param("taskId") String taskId,
                                  @Param("today") Date today,
                                  @Param("isOpen") String isOpen,
                                  @Param("special") Byte special);
}