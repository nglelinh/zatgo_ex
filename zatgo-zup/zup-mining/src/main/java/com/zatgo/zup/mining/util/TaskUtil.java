package com.zatgo.zup.mining.util;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.mining.entity.*;
import com.zatgo.zup.mining.mapper.TaskManageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2018/12/20.
 */

@Component
public class TaskUtil {

    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private TaskManageMapper taskManageMapper;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 判断这个任务是不是不限金额
     * @param rewardId
     * @return
     */
    public boolean isInfinity(String rewardId){
        Map<String, SimpleTask> cache = getCache();
        return cache.containsKey(rewardId);
    }

    /**
     * 返回操作是否成功
     * @param rewardId
     * @param amount
     * @return
     */
    public boolean cutBalance(String rewardId, BigDecimal amount){
        //解决同一用户并发问题
        String accountLockKey = RedisKeyConstants.UPDATE_CACHE_BALANCE_LOCK;
        redisLockUtils.lock(accountLockKey);
        try {
            Map<String, SimpleTask> cache = getCache();
            if (cache.containsKey(rewardId)){
                SimpleTask simpleTask = cache.get(rewardId);
                BigDecimal remainingTotal = simpleTask.getRemainingTotal();
                BigDecimal balance = remainingTotal.subtract(amount);
                if (balance.compareTo(BigDecimal.ZERO) == -1)
                    return false;
                simpleTask.setRemainingTotal(balance);
                redisUtils.put(RedisKeyConstants.TASK_CACHE_KEY, cache);
            }
            return true;
        } finally {
            redisLockUtils.releaseLock(accountLockKey);
        }
    }

    private Map<String, SimpleTask> getCache(){
        //解决同一用户并发问题
        String accountLockKey = RedisKeyConstants.GET_CACHE_BALANCE_LOCK;
        redisLockUtils.lock(accountLockKey);
        try {
            Map<String, SimpleTask> cache = null;
            Object o = redisUtils.get(RedisKeyConstants.TASK_CACHE_KEY);
            if (o != null){
                cache = JSONObject.parseObject((String) o, new TypeReference<HashMap<String, SimpleTask>>() {});
            }
            if (cache == null){
                cache = init();
            }

            return cache;
        } finally {
            redisLockUtils.releaseLock(accountLockKey);
        }
    }

    /**
     * 初始化
     */
    private Map<String, SimpleTask> init(){
        Map<String, SimpleTask> taskCache = new HashMap<>();
        List<TaskManageList> taskManages = taskManageMapper.taskList(null, null, null, null, null);
        if (CollectionUtils.isEmpty(taskManages)){
            return taskCache;
        }
        taskManages.forEach(task -> {
            List<SubTaskManageList> list = task.getList();
            list.forEach(subTask -> {
                List<SubTaskRewardManage> rewardList = subTask.getList();
                rewardList.forEach(reward -> {
                    BigDecimal coinRemainingTotal = reward.getCoinRemainingTotal();
                    BigDecimal coinTotal = reward.getCoinTotal();
                    if (BigDecimal.ZERO.compareTo(coinTotal) == 1){
                        SimpleTask simpleTask = new SimpleTask();
                        simpleTask.setRewardId(reward.getId());
                        simpleTask.setRemainingTotal(coinRemainingTotal);
                        simpleTask.setTotal(coinTotal);
                        taskCache.put(simpleTask.getRewardId(), simpleTask);
                    }
                });
            });
        });
        redisUtils.put(RedisKeyConstants.TASK_CACHE_KEY, taskCache);
        return taskCache;
    }
}
