package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.MiningConfig;
import org.springframework.stereotype.Repository;

@Repository
public interface MiningConfigMapper extends BaseMapper<MiningConfig>{
}