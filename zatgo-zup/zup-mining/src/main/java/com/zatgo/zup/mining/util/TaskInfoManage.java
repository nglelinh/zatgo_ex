package com.zatgo.zup.mining.util;

import com.zatgo.zup.mining.entity.SubTaskManageList;
import com.zatgo.zup.mining.entity.TaskManageList;
import com.zatgo.zup.mining.entity.UserTaskInfoRecord;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2019/2/20.
 */
public class TaskInfoManage {

    private static final Logger logger = LoggerFactory.getLogger(TaskInfoManage.class);


    // 第一个key  taskId  第二个key subTaskId
    private HashMap<String, HashMap<String, SubTaskManageList>> map = new HashMap<>();

    public TaskInfoManage(List<TaskManageList> list) {
        if (!CollectionUtils.isEmpty(list)){
            list.forEach(masterTask -> {
                String taskId = masterTask.getId();
                List<SubTaskManageList> subTaskList = masterTask.getList();
                subTaskList.forEach(subTask -> {
                    String subTaskId = subTask.getId();
                    HashMap<String, SubTaskManageList> subTaskMap = map.get(taskId);
                    if (subTaskMap == null){
                        subTaskMap = new HashMap<String, SubTaskManageList>();
                        subTaskMap.put(subTaskId, subTask);
                        map.put(taskId, subTaskMap);
                    } else {
                        subTaskMap.put(subTaskId, subTask);
                    }
                });
            });
        }
    }


    /**
     * 判定子任务是否完成
     * @param userTaskInfoRecords
     */
    public void judgeSubTask(List<UserTaskInfoRecord> userTaskInfoRecords){
        if (!CollectionUtils.isEmpty(userTaskInfoRecords)){
            userTaskInfoRecords.forEach(record -> {
                String subTaskId = record.getSubTaskId();
                String taskId = record.getTaskId();
                HashMap<String, SubTaskManageList> subTaskList = map.get(taskId);
                if (subTaskList != null){
                    SubTaskManageList subTask = subTaskList.get(subTaskId);
                    if (subTask != null){
                        int count = subTask.getCount() + 1;
                        subTask.setCount(count);
                        int cycleNumber = subTask.getCycleNumber();
                        if (cycleNumber < 1)
                            cycleNumber = 1;
                        int rewardUp = subTask.getRewardUp().intValue();
                        if (rewardUp * cycleNumber <= count){
                            subTask.setIsDone(1);
                        }
                    }
                }
            });
        }
    }

    /**
     *  判断任务是否完成
     *  第一个是是否完成，第二个是数量
     * @param taskId
     * @return
     */
    public Integer[] getTaskInfo(String taskId){
        boolean flag = true;
        Integer[] res = {0, 0};
        HashMap<String, SubTaskManageList> subTaskMap = map.get(taskId);
        if (subTaskMap == null){
                flag = false;
        }
        for (Map.Entry<String, SubTaskManageList> m : subTaskMap.entrySet()){
            SubTaskManageList v = m.getValue();
            int count = (Integer) res[1];
            res[1] = Integer.valueOf(count + v.getCount());
            flag = flag && (v.getIsDone() == 1);
            res[0] = flag ? 1 : 0;
        }
        return res;
    }

    public Boolean subTaskIsDone(String taskId, String subTaskId){
        HashMap<String, SubTaskManageList> subTaskMap = map.get(taskId);
        SubTaskManageList subTask = subTaskMap.get(subTaskId);
        int cycleNumber = subTask.getCycleNumber();
        if (cycleNumber < 1)
            cycleNumber = 1;
        int rewardUp = subTask.getRewardUp().intValue();
        return cycleNumber * rewardUp - subTask.getCount().intValue() <= 0;
    }
}
