package com.zatgo.zup.mining.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SubTaskRewardManageExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SubTaskRewardManageExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdIsNull() {
            addCriterion("sub_task_id is null");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdIsNotNull() {
            addCriterion("sub_task_id is not null");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdEqualTo(String value) {
            addCriterion("sub_task_id =", value, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdNotEqualTo(String value) {
            addCriterion("sub_task_id <>", value, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdGreaterThan(String value) {
            addCriterion("sub_task_id >", value, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdGreaterThanOrEqualTo(String value) {
            addCriterion("sub_task_id >=", value, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdLessThan(String value) {
            addCriterion("sub_task_id <", value, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdLessThanOrEqualTo(String value) {
            addCriterion("sub_task_id <=", value, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdLike(String value) {
            addCriterion("sub_task_id like", value, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdNotLike(String value) {
            addCriterion("sub_task_id not like", value, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdIn(List<String> values) {
            addCriterion("sub_task_id in", values, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdNotIn(List<String> values) {
            addCriterion("sub_task_id not in", values, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdBetween(String value1, String value2) {
            addCriterion("sub_task_id between", value1, value2, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andSubTaskIdNotBetween(String value1, String value2) {
            addCriterion("sub_task_id not between", value1, value2, "subTaskId");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Byte value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Byte value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Byte value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Byte value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Byte value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Byte> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Byte> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Byte value1, Byte value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andGameIdIsNull() {
            addCriterion("game_id is null");
            return (Criteria) this;
        }

        public Criteria andGameIdIsNotNull() {
            addCriterion("game_id is not null");
            return (Criteria) this;
        }

        public Criteria andGameIdEqualTo(String value) {
            addCriterion("game_id =", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdNotEqualTo(String value) {
            addCriterion("game_id <>", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdGreaterThan(String value) {
            addCriterion("game_id >", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdGreaterThanOrEqualTo(String value) {
            addCriterion("game_id >=", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdLessThan(String value) {
            addCriterion("game_id <", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdLessThanOrEqualTo(String value) {
            addCriterion("game_id <=", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdLike(String value) {
            addCriterion("game_id like", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdNotLike(String value) {
            addCriterion("game_id not like", value, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdIn(List<String> values) {
            addCriterion("game_id in", values, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdNotIn(List<String> values) {
            addCriterion("game_id not in", values, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdBetween(String value1, String value2) {
            addCriterion("game_id between", value1, value2, "gameId");
            return (Criteria) this;
        }

        public Criteria andGameIdNotBetween(String value1, String value2) {
            addCriterion("game_id not between", value1, value2, "gameId");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNull() {
            addCriterion("coin_network_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNotNull() {
            addCriterion("coin_network_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeEqualTo(String value) {
            addCriterion("coin_network_type =", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotEqualTo(String value) {
            addCriterion("coin_network_type <>", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThan(String value) {
            addCriterion("coin_network_type >", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_network_type >=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThan(String value) {
            addCriterion("coin_network_type <", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_network_type <=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLike(String value) {
            addCriterion("coin_network_type like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotLike(String value) {
            addCriterion("coin_network_type not like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIn(List<String> values) {
            addCriterion("coin_network_type in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotIn(List<String> values) {
            addCriterion("coin_network_type not in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeBetween(String value1, String value2) {
            addCriterion("coin_network_type between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotBetween(String value1, String value2) {
            addCriterion("coin_network_type not between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNull() {
            addCriterion("coin_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNotNull() {
            addCriterion("coin_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeEqualTo(String value) {
            addCriterion("coin_type =", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotEqualTo(String value) {
            addCriterion("coin_type <>", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThan(String value) {
            addCriterion("coin_type >", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_type >=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThan(String value) {
            addCriterion("coin_type <", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_type <=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLike(String value) {
            addCriterion("coin_type like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotLike(String value) {
            addCriterion("coin_type not like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIn(List<String> values) {
            addCriterion("coin_type in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotIn(List<String> values) {
            addCriterion("coin_type not in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeBetween(String value1, String value2) {
            addCriterion("coin_type between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotBetween(String value1, String value2) {
            addCriterion("coin_type not between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andNumIsNull() {
            addCriterion("num is null");
            return (Criteria) this;
        }

        public Criteria andNumIsNotNull() {
            addCriterion("num is not null");
            return (Criteria) this;
        }

        public Criteria andNumEqualTo(BigDecimal value) {
            addCriterion("num =", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotEqualTo(BigDecimal value) {
            addCriterion("num <>", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThan(BigDecimal value) {
            addCriterion("num >", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("num >=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThan(BigDecimal value) {
            addCriterion("num <", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("num <=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumIn(List<BigDecimal> values) {
            addCriterion("num in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotIn(List<BigDecimal> values) {
            addCriterion("num not in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("num between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("num not between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateIsNull() {
            addCriterion("award_start_date is null");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateIsNotNull() {
            addCriterion("award_start_date is not null");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateEqualTo(Date value) {
            addCriterion("award_start_date =", value, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateNotEqualTo(Date value) {
            addCriterion("award_start_date <>", value, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateGreaterThan(Date value) {
            addCriterion("award_start_date >", value, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("award_start_date >=", value, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateLessThan(Date value) {
            addCriterion("award_start_date <", value, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateLessThanOrEqualTo(Date value) {
            addCriterion("award_start_date <=", value, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateIn(List<Date> values) {
            addCriterion("award_start_date in", values, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateNotIn(List<Date> values) {
            addCriterion("award_start_date not in", values, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateBetween(Date value1, Date value2) {
            addCriterion("award_start_date between", value1, value2, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardStartDateNotBetween(Date value1, Date value2) {
            addCriterion("award_start_date not between", value1, value2, "awardStartDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateIsNull() {
            addCriterion("award_end_date is null");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateIsNotNull() {
            addCriterion("award_end_date is not null");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateEqualTo(Date value) {
            addCriterion("award_end_date =", value, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateNotEqualTo(Date value) {
            addCriterion("award_end_date <>", value, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateGreaterThan(Date value) {
            addCriterion("award_end_date >", value, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("award_end_date >=", value, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateLessThan(Date value) {
            addCriterion("award_end_date <", value, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateLessThanOrEqualTo(Date value) {
            addCriterion("award_end_date <=", value, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateIn(List<Date> values) {
            addCriterion("award_end_date in", values, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateNotIn(List<Date> values) {
            addCriterion("award_end_date not in", values, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateBetween(Date value1, Date value2) {
            addCriterion("award_end_date between", value1, value2, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andAwardEndDateNotBetween(Date value1, Date value2) {
            addCriterion("award_end_date not between", value1, value2, "awardEndDate");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardIsNull() {
            addCriterion("is_instant_award is null");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardIsNotNull() {
            addCriterion("is_instant_award is not null");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardEqualTo(Boolean value) {
            addCriterion("is_instant_award =", value, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardNotEqualTo(Boolean value) {
            addCriterion("is_instant_award <>", value, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardGreaterThan(Boolean value) {
            addCriterion("is_instant_award >", value, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_instant_award >=", value, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardLessThan(Boolean value) {
            addCriterion("is_instant_award <", value, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardLessThanOrEqualTo(Boolean value) {
            addCriterion("is_instant_award <=", value, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardIn(List<Boolean> values) {
            addCriterion("is_instant_award in", values, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardNotIn(List<Boolean> values) {
            addCriterion("is_instant_award not in", values, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardBetween(Boolean value1, Boolean value2) {
            addCriterion("is_instant_award between", value1, value2, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andIsInstantAwardNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_instant_award not between", value1, value2, "isInstantAward");
            return (Criteria) this;
        }

        public Criteria andBlessingsIsNull() {
            addCriterion("blessings is null");
            return (Criteria) this;
        }

        public Criteria andBlessingsIsNotNull() {
            addCriterion("blessings is not null");
            return (Criteria) this;
        }

        public Criteria andBlessingsEqualTo(String value) {
            addCriterion("blessings =", value, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsNotEqualTo(String value) {
            addCriterion("blessings <>", value, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsGreaterThan(String value) {
            addCriterion("blessings >", value, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsGreaterThanOrEqualTo(String value) {
            addCriterion("blessings >=", value, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsLessThan(String value) {
            addCriterion("blessings <", value, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsLessThanOrEqualTo(String value) {
            addCriterion("blessings <=", value, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsLike(String value) {
            addCriterion("blessings like", value, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsNotLike(String value) {
            addCriterion("blessings not like", value, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsIn(List<String> values) {
            addCriterion("blessings in", values, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsNotIn(List<String> values) {
            addCriterion("blessings not in", values, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsBetween(String value1, String value2) {
            addCriterion("blessings between", value1, value2, "blessings");
            return (Criteria) this;
        }

        public Criteria andBlessingsNotBetween(String value1, String value2) {
            addCriterion("blessings not between", value1, value2, "blessings");
            return (Criteria) this;
        }

        public Criteria andCoinTotalIsNull() {
            addCriterion("coin_total is null");
            return (Criteria) this;
        }

        public Criteria andCoinTotalIsNotNull() {
            addCriterion("coin_total is not null");
            return (Criteria) this;
        }

        public Criteria andCoinTotalEqualTo(BigDecimal value) {
            addCriterion("coin_total =", value, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalNotEqualTo(BigDecimal value) {
            addCriterion("coin_total <>", value, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalGreaterThan(BigDecimal value) {
            addCriterion("coin_total >", value, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coin_total >=", value, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalLessThan(BigDecimal value) {
            addCriterion("coin_total <", value, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coin_total <=", value, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalIn(List<BigDecimal> values) {
            addCriterion("coin_total in", values, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalNotIn(List<BigDecimal> values) {
            addCriterion("coin_total not in", values, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coin_total between", value1, value2, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinTotalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coin_total not between", value1, value2, "coinTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalIsNull() {
            addCriterion("coin_remaining_total is null");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalIsNotNull() {
            addCriterion("coin_remaining_total is not null");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalEqualTo(BigDecimal value) {
            addCriterion("coin_remaining_total =", value, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalNotEqualTo(BigDecimal value) {
            addCriterion("coin_remaining_total <>", value, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalGreaterThan(BigDecimal value) {
            addCriterion("coin_remaining_total >", value, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coin_remaining_total >=", value, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalLessThan(BigDecimal value) {
            addCriterion("coin_remaining_total <", value, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coin_remaining_total <=", value, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalIn(List<BigDecimal> values) {
            addCriterion("coin_remaining_total in", values, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalNotIn(List<BigDecimal> values) {
            addCriterion("coin_remaining_total not in", values, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coin_remaining_total between", value1, value2, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andCoinRemainingTotalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coin_remaining_total not between", value1, value2, "coinRemainingTotal");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeIsNull() {
            addCriterion("trigger_business_type is null");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeIsNotNull() {
            addCriterion("trigger_business_type is not null");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeEqualTo(String value) {
            addCriterion("trigger_business_type =", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeNotEqualTo(String value) {
            addCriterion("trigger_business_type <>", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeGreaterThan(String value) {
            addCriterion("trigger_business_type >", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeGreaterThanOrEqualTo(String value) {
            addCriterion("trigger_business_type >=", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeLessThan(String value) {
            addCriterion("trigger_business_type <", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeLessThanOrEqualTo(String value) {
            addCriterion("trigger_business_type <=", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeLike(String value) {
            addCriterion("trigger_business_type like", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeNotLike(String value) {
            addCriterion("trigger_business_type not like", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeIn(List<String> values) {
            addCriterion("trigger_business_type in", values, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeNotIn(List<String> values) {
            addCriterion("trigger_business_type not in", values, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeBetween(String value1, String value2) {
            addCriterion("trigger_business_type between", value1, value2, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeNotBetween(String value1, String value2) {
            addCriterion("trigger_business_type not between", value1, value2, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeIsNull() {
            addCriterion("trigger_business_code is null");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeIsNotNull() {
            addCriterion("trigger_business_code is not null");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeEqualTo(String value) {
            addCriterion("trigger_business_code =", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeNotEqualTo(String value) {
            addCriterion("trigger_business_code <>", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeGreaterThan(String value) {
            addCriterion("trigger_business_code >", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeGreaterThanOrEqualTo(String value) {
            addCriterion("trigger_business_code >=", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeLessThan(String value) {
            addCriterion("trigger_business_code <", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeLessThanOrEqualTo(String value) {
            addCriterion("trigger_business_code <=", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeLike(String value) {
            addCriterion("trigger_business_code like", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeNotLike(String value) {
            addCriterion("trigger_business_code not like", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeIn(List<String> values) {
            addCriterion("trigger_business_code in", values, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeNotIn(List<String> values) {
            addCriterion("trigger_business_code not in", values, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeBetween(String value1, String value2) {
            addCriterion("trigger_business_code between", value1, value2, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeNotBetween(String value1, String value2) {
            addCriterion("trigger_business_code not between", value1, value2, "triggerBusinessCode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}