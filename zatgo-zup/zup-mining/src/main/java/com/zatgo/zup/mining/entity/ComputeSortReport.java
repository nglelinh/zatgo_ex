package com.zatgo.zup.mining.entity;

public class ComputeSortReport {

    private String userId;

    private Long computeNum;

    private Long sort;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Long getComputeNum() {
        return computeNum;
    }

    public void setComputeNum(Long computeNum) {
        this.computeNum = computeNum;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }
}