package com.zatgo.zup.mining.entity;

import java.util.Date;

public class UserMiningProperty extends BaseEntity{
    private String userId;

    private Long computingPower;

    private Date timeStamp;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Long getComputingPower() {
        return computingPower;
    }

    public void setComputingPower(Long computingPower) {
        this.computingPower = computingPower;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}