package com.zatgo.zup.mining.entity.response;

import java.util.List;

/**
 * Created by 46041 on 2019/2/16.
 */
public class GetRewardRecordResponse {

    private Boolean isGet;

    private List<GetRewardResponse> list;

    private Integer fen;

    public Boolean getGet() {
        return isGet;
    }

    public void setGet(Boolean get) {
        isGet = get;
    }

    public List<GetRewardResponse> getList() {
        return list;
    }

    public void setList(List<GetRewardResponse> list) {
        this.list = list;
    }

    public Integer getFen() {
        return fen;
    }

    public void setFen(Integer fen) {
        this.fen = fen;
    }
}
