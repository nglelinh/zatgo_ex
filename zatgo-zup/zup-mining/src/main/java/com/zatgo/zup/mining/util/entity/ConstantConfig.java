package com.zatgo.zup.mining.util.entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by 46041 on 2018/7/24.
 */
@Configuration
public class ConstantConfig {

    /**
     * 规定一天内的挖矿次数
     */
//    @Value("${mining.miningNumOfDay}")
//    public Integer MINING_NUM_DAY;

    /**
     * 设定一天内发币数量
     */
//    @Value("${mining.coinNumMap}")
//    public String COIN_NUM_MAP;

    /**
     * 需要挖矿的币种
     */
//    @Value("${mining.coinType}")
//    public String COIN_TYPE;

    /**
     * 用户挖矿签名时候用到的key
     */
    @Value("${mining.signatureKey}")
    public String SIGNATURE_KEY;

    /**
     * 挖矿的天数
     */
    public Integer MINING_DAY = 3;

    /**
     * 答题错误展示时间（秒）
     */
    public long RESULT_SHOW_TIME = 3l;

    /**
     * 每天登录获得的算力
     */
//    @Value("${mining.loginComputePower}")
//    public Long LOGIN_COMPUTE_POWER;
}
