package com.zatgo.zup.mining.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@ApiModel(value = "矿池记录")
public class MiningRecord extends BaseEntity{
    @ApiModelProperty(value = "矿可获取时间",required = true)
    private Date startTime;
    @ApiModelProperty(value = "货币类型",required = true)
    private String coinType;
    @ApiModelProperty(value = "货币网络类型",required = true)
    private String coinNetworkType;
    @ApiModelProperty(value = "货币数量",required = true)
    private String coinNum;
    @ApiModelProperty(value = "签名",required = true)
    private String signature;
    @ApiModelProperty(value = "币种图片",required = false)
    private String coinImage;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getCoinNum() {
        return coinNum;
    }

    public void setCoinNum(String coinNum) {
        this.coinNum = coinNum;
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getCoinImage() {
        return coinImage;
    }

    public void setCoinImage(String coinImage) {
        this.coinImage = coinImage;
    }
}