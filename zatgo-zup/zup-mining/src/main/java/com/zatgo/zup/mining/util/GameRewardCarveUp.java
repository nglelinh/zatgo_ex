package com.zatgo.zup.mining.util;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by 46041 on 2019/2/14.
 */


@Component
public class GameRewardCarveUp {

    @Autowired
    private RedisUtils redisUtils;

    private static final int level = 5;

    private static final double rate = 0.05;

    private static final int reward = 10000;

    public BigDecimal getReward(String taskId, Integer count){
        Map<Integer, RewardModel> data = getData(taskId);
        if (data == null){
            data = init(level, rate, count);
        }
        Integer key = getKey(data);
        RewardModel model = data.get(key);
        model.setSurplus(model.getSurplus() - 1);
        data.put(key, model);
        return new BigDecimal(reward * model.getRate() / model.getCount() + "");
    }

    private Integer getKey(Map<Integer, RewardModel> data){
        //统计还有那个档位有空位
        int i = 0;
        List<Integer> list = new ArrayList<>();
        Iterator<Map.Entry<Integer, RewardModel>> iterator = data.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<Integer, RewardModel> next = iterator.next();
            Integer key = next.getKey();
            RewardModel value = next.getValue();
            if (value.getSurplus() > 0){
                i++;
                list.add(key);
            }
        }
        //随机取一个
        return QuestionUtil.getRandomBetweenXAndY(0l, new Integer(list.size() - 1).longValue()).intValue();
    }

    /**
     *
     * @param level  奖励分级数
     * @param rate  递增/减 比率
     * @param count 总人数
     * @return
     *
     *  校验参数是否正确的公式为：  level/2 * rate < 1/level 这个结果是true，则可以参数为正确的
     *  最低档位计算公式： 1/level-rate*(level/2)
     */
    private static Map<Integer, RewardModel> init(int level, double rate, int count){
        if (level/2 * rate >= 1d/level){
            throw new BusinessException();
        }
        Map<Integer, RewardModel> map = new HashMap<>();
        double lastLevel = 1d/level - rate * (level / 2);
        for (int i = 0; i < level; i++){
            RewardModel model = new GameRewardCarveUp().new RewardModel();
            model.setRate(lastLevel + i * rate);
            model.setCount((int) (count * model.getRate()));
            model.setSurplus(model.getCount());
            map.put(i + 1, model);
        }
        return map;
    }

    /**
     * key 为档数， value为人数
     * @return
     */
    private Map<Integer, RewardModel> getData(String taskId){
        String key = getKey(taskId);
        if (redisUtils.hasKey(key)){
            String value = (String) redisUtils.get(key);
            return JSONObject.parseObject(value, new TypeReference<HashMap<Integer, RewardModel>>() {});
        }
        return null;
    }


    private String getKey(String taskId){
        return RedisKeyConstants.GAME_REWARD_CARVE_UP + "_" + taskId;
    }

    private class RewardModel {

        private double rate;

        private int count;

        private int surplus;


        public double getRate() {
            return rate;
        }

        public void setRate(double rate) {
            this.rate = rate;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getSurplus() {
            return surplus;
        }

        public void setSurplus(int surplus) {
            this.surplus = surplus;
        }
    }
}
