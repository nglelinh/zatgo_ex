package com.zatgo.zup.mining.entity;

import java.util.List;

/**
 * Created by 46041 on 2019/2/19.
 */
public class SubTaskManageList extends SubTaskManage{

    private List<SubTaskRewardManage> list;

    private Integer isDone = 0;

    private Integer count = 0;

    public List<SubTaskRewardManage> getList() {
        return list;
    }

    public void setList(List<SubTaskRewardManage> list) {
        this.list = list;
    }

    public Integer getIsDone() {
        return isDone;
    }

    public void setIsDone(Integer isDone) {
        this.isDone = isDone;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
