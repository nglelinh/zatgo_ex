package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.UserTaskInfoRecord;
import com.zatgo.zup.mining.entity.UserTaskInfoRecordModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface UserTaskInfoRecordMapper extends BaseMapper<UserTaskInfoRecord>{



    List<UserTaskInfoRecordModel> selectTaskRecordList(String userId);

    List<Map<String, Object>> selectOldRecord();
}