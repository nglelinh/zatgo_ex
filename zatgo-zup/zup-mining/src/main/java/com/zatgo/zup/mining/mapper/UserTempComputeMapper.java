package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.UserTempCompute;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTempComputeMapper extends BaseMapper<UserTempCompute>{

}