package com.zatgo.zup.mining.entity;

import java.util.List;

/**
 * Created by 46041 on 2019/2/21.
 */
public class UserTaskInfoRecordModel extends UserTaskInfoRecord {

    private List<UserTaskInfoRewardRecord> list;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<UserTaskInfoRewardRecord> getList() {
        return list;
    }

    public void setList(List<UserTaskInfoRewardRecord> list) {
        this.list = list;
    }
}
