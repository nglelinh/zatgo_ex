package com.zatgo.zup.mining.entity;

import java.util.List;

/**
 * Created by 46041 on 2019/2/19.
 */
public class TaskManageList extends TaskManage {

    private Integer isDone = 0;

    private Long executeInterval;

    private List<SubTaskManageList> list;

    private Integer count;

    private Long rewardUp;

    private String ruleType;

    public List<SubTaskManageList> getList() {
        return list;
    }

    public void setList(List<SubTaskManageList> list) {
        this.list = list;
    }

    public Integer getIsDone() {
        return isDone;
    }

    public void setIsDone(Integer isDone) {
        this.isDone = isDone;
    }

    public Long getExecuteInterval() {
        return executeInterval;
    }

    public void setExecuteInterval(Long executeInterval) {
        this.executeInterval = executeInterval;
    }

    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getRewardUp() {
        return rewardUp;
    }

    public void setRewardUp(Long rewardUp) {
        this.rewardUp = rewardUp;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }
}
