package com.zatgo.zup.mining.entity;

import java.util.Date;

public class ComputeGetRecord {
    private String recordId;

    private String userId;

    private Long computeNum;

    private Byte source;

    private String sourceName;

    private Byte computeType;

    private Date createDate;

    private Date validStartDate;

    private Date validEndDate;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId == null ? null : recordId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Long getComputeNum() {
        return computeNum;
    }

    public void setComputeNum(Long computeNum) {
        this.computeNum = computeNum;
    }

    public Byte getSource() {
        return source;
    }

    public void setSource(Byte source) {
        this.source = source;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName == null ? null : sourceName.trim();
    }

    public Byte getComputeType() {
        return computeType;
    }

    public void setComputeType(Byte computeType) {
        this.computeType = computeType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getValidStartDate() {
        return validStartDate;
    }

    public void setValidStartDate(Date validStartDate) {
        this.validStartDate = validStartDate;
    }

    public Date getValidEndDate() {
        return validEndDate;
    }

    public void setValidEndDate(Date validEndDate) {
        this.validEndDate = validEndDate;
    }
}