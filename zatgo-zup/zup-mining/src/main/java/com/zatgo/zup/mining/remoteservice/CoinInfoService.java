package com.zatgo.zup.mining.remoteservice;

import com.zatgo.zup.common.model.IssuedCoinInfoData;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created by 46041 on 2018/7/25.
 */
@FeignClient("zup-wallet")
public interface CoinInfoService {

    @GetMapping(value = "/wallet/coin/rpc/{type}")
    @ResponseBody
    ResponseData<Map<String, IssuedCoinInfoData>> selectByCoinTypeRPC(@PathVariable("type") String type);
}
