package com.zatgo.zup.mining.entity;

import java.util.Date;

public class TaskManage {

    private String id;

    private String title;

    private String taskAbstract;

    private String description;

    private String rewardDescription;

    private String img;

    private String firstAbstract;

    private Date createDate;

    private Date updateDate;

    private String isDel;

    private String createBy;

    private String updateBy;

    private Integer sort;

    private Date startDate;

    private Date endDate;

    private String localtionUrl;

    private String isOpen;

    private String cloudUserId;

    private Byte clickEventType;

    private String type;

    private Byte special;

    private Integer count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTaskAbstract() {
        return taskAbstract;
    }

    public void setTaskAbstract(String taskAbstract) {
        this.taskAbstract = taskAbstract;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRewardDescription() {
        return rewardDescription;
    }

    public void setRewardDescription(String rewardDescription) {
        this.rewardDescription = rewardDescription;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getFirstAbstract() {
        return firstAbstract;
    }

    public void setFirstAbstract(String firstAbstract) {
        this.firstAbstract = firstAbstract;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLocaltionUrl() {
        return localtionUrl;
    }

    public void setLocaltionUrl(String localtionUrl) {
        this.localtionUrl = localtionUrl;
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }

    public Byte getClickEventType() {
        return clickEventType;
    }

    public void setClickEventType(Byte clickEventType) {
        this.clickEventType = clickEventType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Byte getSpecial() {
        return special;
    }

    public void setSpecial(Byte special) {
        this.special = special;
    }
}