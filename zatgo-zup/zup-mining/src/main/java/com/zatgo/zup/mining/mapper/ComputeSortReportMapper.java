package com.zatgo.zup.mining.mapper;

import com.zatgo.zup.mining.entity.ComputePowerPojo;
import com.zatgo.zup.mining.entity.ComputeSortReport;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComputeSortReportMapper extends BaseMapper<ComputeSortReport>{

    /**
     * 更新排行信息
     * @param list
     */
    void insertList(List<ComputeSortReport> list);

    /**
     * 获取排名前n
     * @return
     */
    List<ComputePowerPojo> getTopTen(@Param("n") Integer n);

    void updateUserCompute(@Param("num") Long num,@Param("userId") String userId);
}