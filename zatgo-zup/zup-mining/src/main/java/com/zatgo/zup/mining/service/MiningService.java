package com.zatgo.zup.mining.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.model.AddComputePowerParams;
import com.zatgo.zup.common.model.IssuedCoinInfoData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.mining.entity.*;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2018/7/24.
 */
public interface MiningService {

    /**
     * 获取用户近N天的挖抗信息
     * N在配置文件中配置
     * @param userId
     * @return
     */
    ResponseData<List<MiningRecord>> getUserMiningInfo(String userId, String cloudUserId) throws IllegalAccessException, ParseException;

    /**
     * 获取用户当前的算力
     * @param userId
     * @return
     */
    ComputePowerPojo getUserComputePower(String userId);

    /**
     * 挖矿
     * @param record
     * @return
     */
    ResponseData<Boolean> mining(MiningRecord record, String userId, String userName);

    /**
     * 用户挖矿记录
     * @param page
     * @param size
     * @param userId
     * @return
     */
    ResponseData<PageInfo<UserMiningRecord>> miningRecord(Integer page, Integer size, String userId);

    /**
     * 用户算力排行
     * @return
     */
    List<ComputePowerPojo> userComputePowerTop();

    /**
     * 用户获取算力
     * @param params
     * @return
     */
    ResponseData addComputePower(AddComputePowerParams params);

    /**
     * 用户算力获取记录
     * @param userId
     * @param page
     * @param size
     * @return
     */
    ResponseData<PageInfo<ComputeGetRecord>> getComputePowerRecord(String userId, Integer page, Integer size);

    /**
     * 为矿池生成记录,由定时器调用
     */
    void createMineralPoolRecord(Map<String, IssuedCoinInfoData> map) throws ParseException;

    /**
     * 用户登录的时候获取算力调用
     * @param userId
     * @return
     */
//    ResponseData userLoginGetComputePower(String userId, Long computePower);

    /**
     * 获取挖矿配置
     * @return
     */
    List<MiningConfig> getMiningConfig();


    /**
     * 生成当前用户算力排行
     */
    void updateComputeSortTask();
}
