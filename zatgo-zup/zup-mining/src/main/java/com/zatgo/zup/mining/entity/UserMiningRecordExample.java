package com.zatgo.zup.mining.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserMiningRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserMiningRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRecordIdIsNull() {
            addCriterion("record_id is null");
            return (Criteria) this;
        }

        public Criteria andRecordIdIsNotNull() {
            addCriterion("record_id is not null");
            return (Criteria) this;
        }

        public Criteria andRecordIdEqualTo(String value) {
            addCriterion("record_id =", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotEqualTo(String value) {
            addCriterion("record_id <>", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThan(String value) {
            addCriterion("record_id >", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThanOrEqualTo(String value) {
            addCriterion("record_id >=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThan(String value) {
            addCriterion("record_id <", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThanOrEqualTo(String value) {
            addCriterion("record_id <=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLike(String value) {
            addCriterion("record_id like", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotLike(String value) {
            addCriterion("record_id not like", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdIn(List<String> values) {
            addCriterion("record_id in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotIn(List<String> values) {
            addCriterion("record_id not in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdBetween(String value1, String value2) {
            addCriterion("record_id between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotBetween(String value1, String value2) {
            addCriterion("record_id not between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNull() {
            addCriterion("coin_network_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNotNull() {
            addCriterion("coin_network_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeEqualTo(String value) {
            addCriterion("coin_network_type =", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotEqualTo(String value) {
            addCriterion("coin_network_type <>", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThan(String value) {
            addCriterion("coin_network_type >", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_network_type >=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThan(String value) {
            addCriterion("coin_network_type <", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_network_type <=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLike(String value) {
            addCriterion("coin_network_type like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotLike(String value) {
            addCriterion("coin_network_type not like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIn(List<String> values) {
            addCriterion("coin_network_type in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotIn(List<String> values) {
            addCriterion("coin_network_type not in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeBetween(String value1, String value2) {
            addCriterion("coin_network_type between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotBetween(String value1, String value2) {
            addCriterion("coin_network_type not between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNull() {
            addCriterion("coin_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNotNull() {
            addCriterion("coin_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeEqualTo(String value) {
            addCriterion("coin_type =", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotEqualTo(String value) {
            addCriterion("coin_type <>", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThan(String value) {
            addCriterion("coin_type >", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_type >=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThan(String value) {
            addCriterion("coin_type <", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_type <=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLike(String value) {
            addCriterion("coin_type like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotLike(String value) {
            addCriterion("coin_type not like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIn(List<String> values) {
            addCriterion("coin_type in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotIn(List<String> values) {
            addCriterion("coin_type not in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeBetween(String value1, String value2) {
            addCriterion("coin_type between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotBetween(String value1, String value2) {
            addCriterion("coin_type not between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinNumIsNull() {
            addCriterion("coin_num is null");
            return (Criteria) this;
        }

        public Criteria andCoinNumIsNotNull() {
            addCriterion("coin_num is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNumEqualTo(BigDecimal value) {
            addCriterion("coin_num =", value, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumNotEqualTo(BigDecimal value) {
            addCriterion("coin_num <>", value, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumGreaterThan(BigDecimal value) {
            addCriterion("coin_num >", value, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coin_num >=", value, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumLessThan(BigDecimal value) {
            addCriterion("coin_num <", value, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coin_num <=", value, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumIn(List<BigDecimal> values) {
            addCriterion("coin_num in", values, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumNotIn(List<BigDecimal> values) {
            addCriterion("coin_num not in", values, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coin_num between", value1, value2, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCoinNumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coin_num not between", value1, value2, "coinNum");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphIsNull() {
            addCriterion("time_paragraph is null");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphIsNotNull() {
            addCriterion("time_paragraph is not null");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphEqualTo(Date value) {
            addCriterion("time_paragraph =", value, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphNotEqualTo(Date value) {
            addCriterion("time_paragraph <>", value, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphGreaterThan(Date value) {
            addCriterion("time_paragraph >", value, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphGreaterThanOrEqualTo(Date value) {
            addCriterion("time_paragraph >=", value, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphLessThan(Date value) {
            addCriterion("time_paragraph <", value, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphLessThanOrEqualTo(Date value) {
            addCriterion("time_paragraph <=", value, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphIn(List<Date> values) {
            addCriterion("time_paragraph in", values, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphNotIn(List<Date> values) {
            addCriterion("time_paragraph not in", values, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphBetween(Date value1, Date value2) {
            addCriterion("time_paragraph between", value1, value2, "timeParagraph");
            return (Criteria) this;
        }

        public Criteria andTimeParagraphNotBetween(Date value1, Date value2) {
            addCriterion("time_paragraph not between", value1, value2, "timeParagraph");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}