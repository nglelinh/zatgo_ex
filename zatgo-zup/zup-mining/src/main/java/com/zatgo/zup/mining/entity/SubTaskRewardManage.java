package com.zatgo.zup.mining.entity;

import java.math.BigDecimal;
import java.util.Date;

public class SubTaskRewardManage {
    private String id;

    private String subTaskId;

    private Byte type;

    private String gameId;

    private String coinNetworkType;

    private String coinType;

    private BigDecimal num;

    private Date awardStartDate;

    private Date awardEndDate;

    private Boolean isInstantAward;

    private String blessings;

    private BigDecimal coinTotal;

    private BigDecimal coinRemainingTotal;

    private String triggerBusinessType;

    private String triggerBusinessCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getSubTaskId() {
        return subTaskId;
    }

    public void setSubTaskId(String subTaskId) {
        this.subTaskId = subTaskId == null ? null : subTaskId.trim();
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId == null ? null : gameId.trim();
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType == null ? null : coinNetworkType.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public Date getAwardStartDate() {
        return awardStartDate;
    }

    public void setAwardStartDate(Date awardStartDate) {
        this.awardStartDate = awardStartDate;
    }

    public Date getAwardEndDate() {
        return awardEndDate;
    }

    public void setAwardEndDate(Date awardEndDate) {
        this.awardEndDate = awardEndDate;
    }

    public Boolean getIsInstantAward() {
        return isInstantAward;
    }

    public void setIsInstantAward(Boolean isInstantAward) {
        this.isInstantAward = isInstantAward;
    }

    public String getBlessings() {
        return blessings;
    }

    public void setBlessings(String blessings) {
        this.blessings = blessings == null ? null : blessings.trim();
    }

    public BigDecimal getCoinTotal() {
        return coinTotal;
    }

    public void setCoinTotal(BigDecimal coinTotal) {
        this.coinTotal = coinTotal;
    }

    public BigDecimal getCoinRemainingTotal() {
        return coinRemainingTotal;
    }

    public void setCoinRemainingTotal(BigDecimal coinRemainingTotal) {
        this.coinRemainingTotal = coinRemainingTotal;
    }

    public String getTriggerBusinessType() {
        return triggerBusinessType;
    }

    public void setTriggerBusinessType(String triggerBusinessType) {
        this.triggerBusinessType = triggerBusinessType;
    }

    public String getTriggerBusinessCode() {
        return triggerBusinessCode;
    }

    public void setTriggerBusinessCode(String triggerBusinessCode) {
        this.triggerBusinessCode = triggerBusinessCode;
    }
}