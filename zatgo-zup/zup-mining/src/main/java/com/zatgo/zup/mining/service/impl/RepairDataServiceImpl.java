package com.zatgo.zup.mining.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.mining.entity.*;
import com.zatgo.zup.mining.mapper.*;
import com.zatgo.zup.mining.service.RepairDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2018/11/28.
 */

@Service("repairDataService")
public class RepairDataServiceImpl implements RepairDataService {

    private static final Logger logger = LoggerFactory.getLogger(RepairDataServiceImpl.class);

    @Autowired
    private ComputeGetRecordMapper computeGetRecordMapper;
    @Autowired
    private UserMiningPropertyMapper userMiningPropertyMapper;
    @Autowired
    private UserTaskInfoRecordMapper userTaskInfoRecordMapper;
    @Autowired
    private SubTaskManageMapper subTaskManageMapper;
    @Autowired
    private UserTaskInfoRewardRecordMapper userTaskInfoRewardRecordMapper;

    @Override
    public void computeData() {
        List<UserMiningProperty> list = computeGetRecordMapper.getUserCompute();
        userMiningPropertyMapper.insertList(list);
        logger.error("==================================数据修复完成======================================");
    }

    @Override
    @Transactional
    public void taskListRecord() {
        List<SubTaskManage> subTaskManages = subTaskManageMapper.selectByExample(new SubTaskManageExample());
        Map<String, SubTaskManage> subTaskMap = new HashMap<>();
        subTaskManages.forEach(subTask -> {subTaskMap.put(subTask.getTaskId(), subTask);});
        int count = 10;
        int pageSize = 100;
        int n = 0;
        for (int i = 1; i <= count; i++){
            logger.error("count：" + count + "pageSize:" + pageSize);
            PageInfo<Map<String, Object>> pageInfo = getrecord(i, pageSize);
            count = new Long(pageInfo.getPages()).intValue();
            List<Map<String, Object>> list = pageInfo.getList();
            for (Map<String, Object> record : list) {
                UserTaskInfoRecord newRecord = new UserTaskInfoRecord();
                String taskId =(String) record.get("task_id");
                String id =(String) record.get("id");
                String reward = (String) record.get("reward");
                String coinType = (String) record.get("coin_type");
                String coinNetworkType = (String) record.get("coin_network_type");
                Long computePower = (Long) record.get("compute_power");
                BigDecimal coinNum = (BigDecimal) record.get("coin_num");
                newRecord.setId(id);
                SubTaskManage subTaskManage = subTaskMap.get(taskId);
                if (subTaskManage == null)
                    continue;
                newRecord.setSubTaskId(subTaskManage.getId());
                userTaskInfoRecordMapper.updateByPrimaryKeySelective(newRecord);
                //币
                if (!StringUtils.isEmpty(coinType)){
                    insertRewardRecord(taskId, null, new Byte("1"), coinNum, coinType, coinNetworkType);
                    n++;
                }
                //算力
                if (computePower != null && computePower.longValue() > 0){
                    insertRewardRecord(taskId, null, new Byte("0"), new BigDecimal(computePower + ""), null, null);
                    n++;
                }
                //奖励
                if (!StringUtils.isEmpty(reward)){
                    insertRewardRecord(taskId, reward, new Byte("5"), null, null, null);
                    n++;
                }
            }
        }
        logger.error("数据" + n + "条数据");
        logger.error("数据修复完成");
    }


    private PageInfo<Map<String, Object>> getrecord(int pageNo, int pageSize){
        PageHelper.startPage(pageNo, pageSize);
        List<Map<String, Object>> maps = userTaskInfoRecordMapper.selectOldRecord();
        return new PageInfo<>(maps);
    }

    private void insertRewardRecord(String taskId, String reward, Byte type, BigDecimal num, String coinType, String CoinNetworkType){
        UserTaskInfoRewardRecord rewardRecord = new UserTaskInfoRewardRecord();
        rewardRecord.setId(UUIDUtils.getUuid());
        rewardRecord.setTaskRecordId(taskId);
        rewardRecord.setReward(reward);
        rewardRecord.setType(type);
        rewardRecord.setNum(num);
        rewardRecord.setCoinType(coinType);
        rewardRecord.setCoinNetworkType(CoinNetworkType);
        userTaskInfoRewardRecordMapper.insertSelective(rewardRecord);
    }
}
