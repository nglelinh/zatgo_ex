package com.zatgo.zup.mining.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.encrypt.MD5Util;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AddComputePowerParams;
import com.zatgo.zup.common.model.IssuedCoinInfoData;
import com.zatgo.zup.common.model.MiningDepositInsertParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.mining.entity.*;
import com.zatgo.zup.mining.mapper.*;
import com.zatgo.zup.mining.remoteservice.DepositService;
import com.zatgo.zup.mining.service.MiningService;
import com.zatgo.zup.mining.util.BigDecimalUtil;
import com.zatgo.zup.mining.util.DateUtil;
import com.zatgo.zup.mining.util.MiningComparator;
import com.zatgo.zup.mining.util.entity.ConstantConfig;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by 46041 on 2018/7/24.
 */
@Service("miningService")
public class MiningServiceImpl implements MiningService{

    private static final Logger logger = LoggerFactory.getLogger(MiningServiceImpl.class);

    @Autowired
    private UserMiningPropertyMapper userMiningPropertyMapper;
    @Autowired
    private ComputeGetRecordMapper computeGetRecordMapper;
    @Autowired
    private MineralPoolMapper mineralPoolMapper;
    @Autowired
    private UserMiningRecordMapper userMiningRecordMapper;
    @Autowired
    private ConstantConfig constantConfig;
    @Autowired
    private DepositService depositService;
    @Autowired
    private MiningConfigMapper miningConfigMapper;
    @Autowired
    private UserTaskInfoRecordMapper userTaskInfoRecordMapper;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private UserTempComputeMapper userTempComputeMapper;
    @Autowired
    private ComputeSortReportMapper computeSortReportMapper;


    @Override
    public ResponseData<List<MiningRecord>> getUserMiningInfo(String userId, String cloudUserId) throws IllegalAccessException, ParseException {
        //获取挖矿配置
//        List<MiningConfig> configs = miningConfigMapper.selectByExample(new MiningConfigExample());
//        if (CollectionUtils.isEmpty(configs))
//            throw new BusinessException();
        //将配置做成一个以币种类型为key的map
//        Map<String, MiningConfig> configMap = new HashMap<>();
//        for(MiningConfig config : configs){
//            configMap.put(config.getCoinType(), config);
//        }
        List<MiningRecord> list = new ArrayList<>();
        //先看看3天内的挖矿情况
        Date today = null;
        Date beforeYesterday = null;
        beforeYesterday = DateUtil.getDate(- constantConfig.MINING_DAY + 1);
        today = DateUtil.getDate(1);
        UserMiningRecordExample userMiningRecordExample = new UserMiningRecordExample();
        UserMiningRecordExample.Criteria criteria = userMiningRecordExample.createCriteria();
        criteria.andTimeParagraphBetween(beforeYesterday, today);
        criteria.andUserIdEqualTo(userId);
        List<UserMiningRecord> userMiningRecords = userMiningRecordMapper.selectByExample(userMiningRecordExample);
        int size = userMiningRecords.size();
        //获取矿池记录
        List<MineralPool> mineralPools = mineralPoolMapper.getMiningRecordListByTime(beforeYesterday, today, cloudUserId);
        //如果矿池记录不是空的
        if (!CollectionUtils.isEmpty(mineralPools)){
            //3天内挖矿总次数
            SimpleDateFormat min = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
            Map<String, MiningRecord> miningRecordMap = new HashMap<>();
            Map<String, List<MineralPool>> mineralPoolmap = new HashMap<>();
            Map<String, Long> computePower = new HashMap<>();
            //获取用户算力
            Date date = DateUtil.getDate(0);
            Long sum = computeGetRecordMapper.getComputePowerByUserId(userId, date);
            if (sum == null)
                sum = 0l;
            //查出用户最近几天的算力获取情况
            List<UserComputePowerPojo> computePowerByUserIdAndDate = computeGetRecordMapper.getComputePowerByUserIdAndDate
                    (userId, DateUtil.getDate(-constantConfig.MINING_DAY), date);
            for (int i = 1; i <= constantConfig.MINING_DAY; i++){
                String afterTime = day.format(DateUtil.getDate(-i + 1));
                if (!CollectionUtils.isEmpty(computePowerByUserIdAndDate)) {
                    for (UserComputePowerPojo computePowerPojo : computePowerByUserIdAndDate) {
                        String date1 = day.format(DateUtil.getDate(-i));
                        if (computePowerPojo.getDate().equals(date1)) {
                            computePower.put(afterTime, sum);
                            sum -= computePowerPojo.getComputePower();
                            break;
                        }
                        computePower.put(afterTime, sum);
                    }
                } else {
                    computePower.put(afterTime, sum);
                }
            }
            //初始化所有挖矿数据
            for (MineralPool mp : mineralPools){
                String format = day.format(mp.getCreateDate());
                if (mineralPoolmap.containsKey(format)) {
                    List<MineralPool> arr = mineralPoolmap.get(format);
                    arr.add(mp);
                } else {
                    List<MineralPool> arr = new ArrayList<>();
                    arr.add(mp);
                    mineralPoolmap.put(format, arr);
                }
            }


            //天数
            for (int i = 0; i < constantConfig.MINING_DAY; i++){
                //获取这一天的算力值
                String format = day.format(DateUtil.getDate(-constantConfig.MINING_DAY + 1 + i));
                Long userComputePower = computePower.get(format);
                if (userComputePower == null || userComputePower <= 0)
                    continue;
                //一天24小时
                if (mineralPoolmap.containsKey(format)) {
                    //获取这一天所有需要挖矿的币种
                    List<MineralPool> poolList = mineralPoolmap.get(format);
                    for (MineralPool mineralPool : poolList){
                        //获取此币的挖矿配置
                        String coinType = mineralPool.getCoinType();
//                        MiningConfig config = configMap.get(coinType);
                        Integer configNum = mineralPool.getNum();
                        String coinImage = mineralPool.getCoinImage();
                        int num = 24 / configNum;
                        for (int j = 0, count = 0; j < 24 && count < configNum; j += num, count++) {
                            Date hour = DateUtil.getHour(DateUtil.getDate(-constantConfig.MINING_DAY + 1), i * 24 + j);
                            MiningRecord miningRecord = new MiningRecord();
                            miningRecord.setCoinNetworkType(mineralPool.getCoinNetworkType());
                            miningRecord.setStartTime(hour);
                            miningRecord.setCoinType(coinType);
                            miningRecord.setCoinImage(coinImage);
                            String s = BigDecimalUtil.div(mineralPool.getCoinSum().toString(),
                                    mineralPool.getAllComputingPower(), mineralPool.getCoinDecimals(), BigDecimal.ROUND_DOWN);
                            String mul = BigDecimalUtil.mul(s, userComputePower.toString());
                            mul = BigDecimalUtil.div(mul, configNum + "", mineralPool.getCoinDecimals(), BigDecimal.ROUND_DOWN);
                            miningRecord.setCoinNum(mul);
                            String key = userId + coinType + mul + hour.getTime() + constantConfig.SIGNATURE_KEY;
                            miningRecordMap.put(MD5Util.MD5(key), miningRecord);
                        }

                    }

                }
            }

            //有领过的,去除掉领过的
            if (size != 0){
                for (UserMiningRecord userMiningRecord : userMiningRecords){
                    miningRecordMap.remove(userMiningRecord.getRecordId());
                }
            }
            //把剩下的放到list中返回
            for (Map.Entry<String, MiningRecord> map : miningRecordMap.entrySet()){
                MiningRecord v = map.getValue();
                String key = userId + v.getCoinType() + v.getCoinNum() + v.getStartTime().getTime() + constantConfig.SIGNATURE_KEY;
                v.setSignature(MD5Util.MD5(key));
                list.add(v);
            }
        }
        if (!CollectionUtils.isEmpty(list)){
            Collections.sort(list, new MiningComparator());
        }
        return BusinessResponseFactory.createSuccess(list);
    }

    public static void main(String[] args) throws IllegalAccessException {
        String a = "100";
        String b = "36";
        System.out.println(BigDecimalUtil.div(a, b, 8, BigDecimal.ROUND_DOWN));
    }

    @Override
    public ComputePowerPojo getUserComputePower(String userId) {
        ComputePowerPojo userComputePower = userMiningPropertyMapper.getUserComputePower(userId, new Date());
        ComputeSortReport computeSortReport = computeSortReportMapper.selectByPrimaryKey(userId);
        Long sort = null;
        if (computeSortReport == null){
            int i = computeSortReportMapper.countByExample(new ComputeSortReportExample());
            sort = i + 0l;
        } else {
            sort = computeSortReport.getSort();
            if (sort == null){
                int i = computeSortReportMapper.countByExample(new ComputeSortReportExample());
                sort = i + 0l;
            }
        }
        userComputePower.setSort(sort);
        return userComputePower;
    }

    @Override
    public List<ComputePowerPojo> userComputePowerTop() {
        List<ComputePowerPojo> list = computeSortReportMapper.getTopTen(10);
        for (int i = 0; i < list.size(); i++){
            ComputePowerPojo computePowerPojo = list.get(i);
            computePowerPojo.setSort(i + 1l);
        }
        return list;
    }

    @Override
    public ResponseData addComputePower(AddComputePowerParams params) {
        Byte source = params.getComputePowerSourceType().getType();
        Byte type = params.getComputePowerType().getType();
        Date validStartDate = params.getValidStartDate();
        Date validEndDate = params.getValidEndDate();
        if (BusinessEnum.ComputePowerType.PERMANENT.getType().equals(type)){
            validStartDate = new Date();
            try {
                validEndDate = DateUtil.getYear(100);
            } catch (ParseException e) {
                logger.error("", e);
            }
        }
        ComputeGetRecord record = new ComputeGetRecord();
        record.setRecordId(UUIDUtils.getUuid());
        record.setUserId(params.getUserId());
        record.setComputeNum(params.getComputeNum());
        record.setComputeType(type);
        record.setSource(source);
        record.setSourceName(BusinessEnum.ComputePowerSourceTypeName.getName(source));
        record.setValidStartDate(validStartDate);
        record.setValidEndDate(validEndDate);
        record.setCreateDate(new Date());
        computeGetRecordMapper.insertSelective(record);
        userMiningPropertyMapper.updateUserCompute(record.getComputeNum(), record.getUserId());
        computeSortReportMapper.updateUserCompute(record.getComputeNum(), record.getUserId());
        return BusinessResponseFactory.createSuccess(null);
    }

    @Override
    public ResponseData<PageInfo<ComputeGetRecord>> getComputePowerRecord(String userId, Integer page, Integer size) {
        ComputeGetRecordExample example = new ComputeGetRecordExample();
        example.createCriteria().andUserIdEqualTo(userId);
        example.setOrderByClause("create_date desc");
        PageHelper.startPage(page, size);
        return BusinessResponseFactory.createSuccess(new PageInfo<ComputeGetRecord>(computeGetRecordMapper.selectByExample(example)));
    }

    @Override
    public void createMineralPoolRecord(Map<String, IssuedCoinInfoData> map) throws ParseException {
        //获取前一天所有用户的总算力
        Long allUserComputePower = computeGetRecordMapper.getAllUserComputePower(DateUtil.getDate(0));
        //获取挖矿配置
        List<MiningConfig> configs = miningConfigMapper.selectByExample(new MiningConfigExample());
        List<MineralPool> list = new ArrayList<>();
        for (MiningConfig config : configs){
            //币种
            String coinType = config.getCoinType();
            //数量
            BigDecimal count = config.getCount();
            IssuedCoinInfoData issuedCoinInfoData = map == null ? null : map.get(coinType);
            MineralPool mineralPool = new MineralPool();
            mineralPool.setCreateDate(new Date());
            mineralPool.setCoinType(coinType);
            mineralPool.setCoinSum(count);
            mineralPool.setCoinNetworkType(issuedCoinInfoData == null ? null : issuedCoinInfoData.getCoinNetworkType());
            mineralPool.setId(UUIDUtils.getUuid());
            mineralPool.setCoinDecimals(issuedCoinInfoData == null ? null : issuedCoinInfoData.getCoinDecimals());
            mineralPool.setAllComputingPower(allUserComputePower.toString());
            mineralPool.setNum(config.getNum());
            mineralPool.setCloudUserId(config.getCloudUserId());
            list.add(mineralPool);
        }
        MineralPoolExample example = new MineralPoolExample();
        Date date = DateUtil.getDate(-1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        example.createCriteria().andCreateDateGreaterThan(date);
        List<MineralPool> exist = mineralPoolMapper.selectByExample(example);
        int size = list.size();
        for (int i = size - 1; i >= 0; i--){
            MineralPool mp = list.get(i);
            String cloudUserId = mp.getCloudUserId();
            String coinType = mp.getCoinType();
            Date createDate = mp.getCreateDate();
            for (MineralPool mineralPool: exist){
                if (cloudUserId.equals(mineralPool.getCloudUserId())
                        && coinType.equals(mineralPool.getCoinType())
                        && sdf.format(createDate).equals(sdf.format(mineralPool.getCreateDate()))){
                    list.remove(i);
                    break;
                }
            }
        }
        if (!CollectionUtils.isEmpty(list)){
            mineralPoolMapper.insertMineralPoolListSelective(list);
        }
    }

//    @Override
//    @Transactional
//    public ResponseData userLoginGetComputePower(String userId, Long computePower) {
//        ComputeGetRecordExample example = new ComputeGetRecordExample();
//        ComputeGetRecordExample.Criteria criteria = example.createCriteria();
//        criteria.andUserIdEqualTo(userId);
//        criteria.andSourceEqualTo((byte) 0);
//        try {
//            criteria.andCreateDateGreaterThan(DateUtil.getDate(0));
//        } catch (ParseException e) {
//            logger.error("获取时间错误");
//            e.printStackTrace();
//        }
//        int i = computeGetRecordMapper.countByExample(example);
//        //如果没有加过,加上指定的算力
//        if (i <= 0){
//            Date date = new Date();
//            ComputeGetRecord record = new ComputeGetRecord();
//            record.setComputeNum(computePower);
//            record.setComputeType((byte) 0);
//            record.setSource((byte) 0);
//            record.setUserId(userId);
//            record.setValidStartDate(date);
//            try {
//                record.setValidEndDate(DateUtil.getYear(100));
//            } catch (ParseException e) {
//                logger.error("获取时间错误");
//                e.printStackTrace();
//            }
//            return getComputePower(record);
//        }
//        return BusinessResponseFactory.createSuccess(null);
//    }

    @Override
    public List<MiningConfig> getMiningConfig() {
        return miningConfigMapper.selectByExample(new MiningConfigExample());
    }

    @Override
    @Transactional
    public void updateComputeSortTask() {
        //获取所有用户的永久算力
        List<UserMiningProperty> userMiningProperties = userMiningPropertyMapper.selectAllUserCompute(new Date());
        List<ComputeSortReport> list = new ArrayList<>();
        Collections.sort(userMiningProperties, new Comparator<UserMiningProperty>() {
            @Override
            public int compare(UserMiningProperty o1, UserMiningProperty o2) {
            	return o2.getComputingPower().compareTo(o1.getComputingPower());
            }
        });
        for (int i = 0; i < userMiningProperties.size() ; i++){
            UserMiningProperty userMiningProperty = userMiningProperties.get(i);
            ComputeSortReport csr = new ComputeSortReport();
            csr.setComputeNum(userMiningProperty.getComputingPower());
            csr.setSort(i + 1l);
            csr.setUserId(userMiningProperty.getUserId());
            list.add(csr);
        }
        computeSortReportMapper.deleteByExample(new ComputeSortReportExample());
        computeSortReportMapper.insertList(list);
    }

    @Override
    @Transactional
    public ResponseData<Boolean> mining(MiningRecord record, String userId, String userName) {
        if (!miningCheck(record, userId)){
            logger.error("用户挖矿数据与签名不一致<<<" + JSONObject.toJSONString(record) + ">>>userId:" + userId);
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REQUEST_PARAM_ERROR);
        }
        if (record.getStartTime().getTime() > new Date().getTime()){
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.RECORD_NOT_EXIST);
        }
        UserMiningRecordExample example = new UserMiningRecordExample();
        example.createCriteria()
                .andTimeParagraphEqualTo(record.getStartTime())
                .andUserIdEqualTo(userId)
                .andCoinTypeEqualTo(record.getCoinType());
        List<UserMiningRecord> userMiningRecords = userMiningRecordMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(userMiningRecords)) {
            UserMiningRecord userMiningRecord = new UserMiningRecord();
            userMiningRecord.setCoinNum(new BigDecimal(record.getCoinNum()));
            userMiningRecord.setUserId(userId);
            userMiningRecord.setCreateDate(new Date());
            userMiningRecord.setTimeParagraph(record.getStartTime());
            userMiningRecord.setCoinType(record.getCoinType());
            userMiningRecord.setCoinNetworkType(record.getCoinNetworkType());
            userMiningRecord.setRecordId(record.getSignature());
            userMiningRecordMapper.insertSelective(userMiningRecord);
            MiningDepositInsertParams params = new MiningDepositInsertParams();
            params.setRecordId(userMiningRecord.getRecordId());
            params.setCoinType(userMiningRecord.getCoinType());
            params.setAmount(userMiningRecord.getCoinNum().toString());
            params.setUserId(userId);
            params.setNetWorkType(userMiningRecord.getCoinNetworkType());
            params.setUserName(userName);
            params.setType(BusinessEnum.PaymentRecordType.mining);
            ResponseData<Object> objectResponseData = depositService.insertMiningDepositRecored(params);
            if (!objectResponseData.isSuccessful()){
                throw new BusinessException(objectResponseData.getCode());
            }
        }
        return BusinessResponseFactory.createSuccess(true);
    }

    @Override
    public ResponseData<PageInfo<UserMiningRecord>> miningRecord(Integer page, Integer size, String userId) {
        PageHelper.startPage(page, size);
        List<UserMiningRecord> userMiningRecords = userMiningRecordMapper.getUserMiningRecords(userId);
        return BusinessResponseFactory.createSuccess(new PageInfo<UserMiningRecord>(userMiningRecords));
    }



    private boolean miningCheck(MiningRecord record, String userId){
        String key = userId + record.getCoinType() + record.getCoinNum() + record.getStartTime().getTime()
                + constantConfig.SIGNATURE_KEY;
        String signature = MD5Util.MD5(key);
        return signature.equals(record.getSignature());
    }

}
