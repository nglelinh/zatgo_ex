package com.zatgo.zup.mining.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.mining.entity.TaskManage;
import com.zatgo.zup.mining.service.RepairDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2018/11/28.
 */


@Api(value = "/repairData",description = "修数据接口",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/repairData")
public class RepairDataController {

    @Autowired
    private RepairDataService repairDataService;

    @ApiOperation(value = "修复算力数据")
    @GetMapping("/computeData")
    public ResponseData computeData(){
        repairDataService.computeData();
        return BusinessResponseFactory.createSuccess(null);
    }


    @ApiOperation(value = "任务改造，修任务历史接口")
    @GetMapping("/taskListRecord")
    public ResponseData taskListRecord(){
        repairDataService.taskListRecord();
        return BusinessResponseFactory.createSuccess(null);
    }
}
