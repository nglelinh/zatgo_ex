package com.zatgo.zup.mining.entity;

import com.zatgo.zup.common.enumtype.BusinessEnum;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/1/14.
 */
public class Reward {

    protected Byte type;

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 算力奖励
     */
    public static class ComputerPower extends Reward{

        private Long computePower;

        public Long getComputePower() {
            return computePower;
        }

        public void setComputePower(Long computePower) {
            this.computePower = computePower;
        }
    }

    /**
     * 币种奖励
     */
    public static class Coin extends Reward{

        private BigDecimal coinNum;

        private String coinType;

        private String coinNetworkType;

        public BigDecimal getCoinNum() {
            return coinNum;
        }

        public void setCoinNum(BigDecimal coinNum) {
            this.coinNum = coinNum;
        }

        public String getCoinType() {
            return coinType;
        }

        public void setCoinType(String coinType) {
            this.coinType = coinType;
        }

        public String getCoinNetworkType() {
            return coinNetworkType;
        }

        public void setCoinNetworkType(String coinNetworkType) {
            this.coinNetworkType = coinNetworkType;
        }
    }
}
