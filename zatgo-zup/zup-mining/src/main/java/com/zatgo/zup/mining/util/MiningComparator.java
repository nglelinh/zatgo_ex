package com.zatgo.zup.mining.util;

import com.zatgo.zup.mining.entity.MiningRecord;

import java.util.Comparator;

/**
 * Created by 46041 on 2018/7/27.
 */
public class MiningComparator implements Comparator {

        public int compare(Object o1, Object o2) {
            MiningRecord q1 = (MiningRecord)o1;
            MiningRecord q2 = (MiningRecord)o2;
            return q1.getStartTime().compareTo(q2.getStartTime());
        }
}
