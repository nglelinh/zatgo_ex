package com.zatgo.zup.mining.entity.response;

import com.zatgo.zup.common.model.GamePrizeModel;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/2/15.
 */
public class GetRewardResponse {

    /**
     * 类型
     */
    private Integer type;
    /**
     * 数量
     */
    private BigDecimal num;

    private Integer fen;

    private String coinNetwork;

    private String coinType;

    private String aliasName;

    private GamePrizeModel prize;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public String getCoinNetwork() {
        return coinNetwork;
    }

    public void setCoinNetwork(String coinNetwork) {
        this.coinNetwork = coinNetwork;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public Integer getFen() {
        return fen;
    }

    public void setFen(Integer fen) {
        this.fen = fen;
    }

    public GamePrizeModel getPrize() {
        return prize;
    }

    public void setPrize(GamePrizeModel prize) {
        this.prize = prize;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }
}
