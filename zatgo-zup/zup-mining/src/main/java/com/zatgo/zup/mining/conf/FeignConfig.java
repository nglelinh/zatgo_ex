package com.zatgo.zup.mining.conf;

import feign.RequestInterceptor;
import feign.RequestTemplate;

import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class FeignConfig implements RequestInterceptor {

	@Autowired
	RedisTemplate redisTemplate;
	
	@Override
    public void apply(RequestTemplate requestTemplate) {
		HttpServletRequest request = getHttpServletRequest();
		if(request != null) {
			Map<String, String> headers = getHeaders(request);
			if(MapUtils.isNotEmpty(headers)) {
				headers.forEach((key, value) -> {
					requestTemplate.header(key, value);
				});
			}
		}
        
        
        //内部访问标示
        //String innerAccessId = RedisKeyConstants.INNER_ACCESS_ID + UUIDUtils.getUuid(); 
        //redisTemplate.opsForValue().set(innerAccessId, "true");
        //redisTemplate.expire(innerAccessId, 60, TimeUnit.SECONDS);
        //requestTemplate.header("innerAccessId", innerAccessId);
    }

    private HttpServletRequest getHttpServletRequest() {
        try {
            return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        } catch (Exception e) {
            return null;
        }
    }

    private Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> map = new LinkedHashMap<>();
        Enumeration<String> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }

}
