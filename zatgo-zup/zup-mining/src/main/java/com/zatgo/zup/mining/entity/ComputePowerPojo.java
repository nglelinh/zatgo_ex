package com.zatgo.zup.mining.entity;

/**
 * Created by 46041 on 2018/7/25.
 */
public class ComputePowerPojo {

    private Long sort;
    private String userName;
    private Long computePower;
    private String iconUrl;
    private String nickname;


    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getComputePower() {
        return computePower;
    }

    public void setComputePower(Long computePower) {
        this.computePower = computePower;
    }

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
}
