package com.zatgo.zup.mining.controller;

import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AddComputePowerParams;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.IssuedCoinInfoData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.mining.entity.ComputeGetRecord;
import com.zatgo.zup.mining.entity.ComputePowerPojo;
import com.zatgo.zup.mining.entity.MiningConfig;
import com.zatgo.zup.mining.entity.MiningRecord;
import com.zatgo.zup.mining.entity.UserMiningRecord;
import com.zatgo.zup.mining.remoteservice.CoinInfoService;
import com.zatgo.zup.mining.service.MiningService;
import com.zatgo.zup.mining.util.entity.ConstantConfig;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by 46041 on 2018/7/24.
 */

@Api(value = "/mining",description = "用户挖矿相关",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/mining")
public class MiningController extends BaseController {
	
	@Value("${mining.virtualUserNum:19430}")
	private String virtualUserNum;
	
	@Value("${mining.virtualUserNumSeat:4900}")
	private String virtualUserNumSeat;

    private static final Logger logger = LoggerFactory.getLogger(MiningController.class);

    @Autowired
    private MiningService miningService;
    @Autowired
    private CoinInfoService coinInfoService;
    @Autowired
    private ConstantConfig constantConfig;

    @ApiOperation(value = "获取用户待挖矿信息")
    @GetMapping("/preMiningInfo")
    public ResponseData<List<MiningRecord>> getUserMiningInfo(){
        try {
            AuthUserInfo userInfo = getUserInfo();
            return miningService.getUserMiningInfo(userInfo.getUserId(), userInfo.getCloudUserId());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "算力排行")
    @GetMapping("/userComputePowerTop")
    public ResponseData<Map<String, Object>> userComputePowerTop(){
        AuthUserInfo userInfo = getUserInfo();
        String userId = userInfo.getUserId();
        List<ComputePowerPojo> computePowerPojos = miningService.userComputePowerTop();
        ComputePowerPojo userComputePower = miningService.getUserComputePower(userId);
        Map<String, Object> map = new HashMap();
        map.put("top", computePowerPojos);
        if(userComputePower != null && userComputePower.getSort() != null && userComputePower.getSort().longValue() > Integer.valueOf(virtualUserNumSeat)) {
        	userComputePower.setSort(userComputePower.getSort() + Integer.valueOf(virtualUserNum));
        }
        map.put("self", userComputePower);
        return BusinessResponseFactory.createSuccess(map);
    }

    @ApiOperation(value = "用户获得算力(内部调用)")
    @PostMapping("/addComputePower")
    public ResponseData addComputePower(@RequestBody AddComputePowerParams params){
        return miningService.addComputePower(params);
    }

    @ApiOperation(value = "获得算力历史记录")
    @GetMapping("/getComputePowerRecord")
    public ResponseData<PageInfo<ComputeGetRecord>> getComputePowerRecord(@RequestParam("page") Integer page,
                                                                @RequestParam("size") Integer size){
        AuthUserInfo userInfo = getUserInfo();
        return miningService.getComputePowerRecord(userInfo.getUserId(), page, size);
    }

    @ApiOperation(value = "用户挖矿")
    @PostMapping("/mining")
    public ResponseData<Boolean> mining(@RequestBody MiningRecord record){
        AuthUserInfo userInfo = getUserInfo();
        return miningService.mining(record, userInfo.getUserId(), userInfo.getUserName());
    }

    @ApiOperation(value = "挖矿记录列表")
    @GetMapping("/miningRecord")
    public ResponseData<PageInfo<UserMiningRecord>> miningRecord(@RequestParam("page") Integer page,
                                                                 @RequestParam("size") Integer size){
        AuthUserInfo userInfo = getUserInfo();
        return miningService.miningRecord(page, size, userInfo.getUserId());
    }

    @ApiOperation(value = "为矿池生成记录,由定时器调用")
    @PostMapping("/create")
    public void createMineralPoolRecord(){
        try {
            List<MiningConfig> miningConfig = miningService.getMiningConfig();
            Set<String> set = new HashSet<>();
            StringBuffer sb = new StringBuffer();
            for (MiningConfig config : miningConfig){
                String coinType = config.getCoinType();
                if (!set.contains(coinType)){
                    sb.append("," + coinType);
                    set.add(coinType);
                }
            }
            ResponseData<Map<String, IssuedCoinInfoData>> mapResponseData = coinInfoService.selectByCoinTypeRPC(sb.toString().replaceFirst("," , ""));
            if (BusinessExceptionCode.SUCCESS_CODE.equals(mapResponseData.getCode())){
                miningService.createMineralPoolRecord(mapResponseData.getData());
            }
        } catch (ParseException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }
    
    @ApiOperation(value = "用户当前算力")
    @GetMapping("/userComputePower")
    public ResponseData<ComputePowerPojo> userComputePower(){
        AuthUserInfo userInfo = getUserInfo();
        String userId = userInfo.getUserId();
        ComputePowerPojo userComputePower = miningService.getUserComputePower(userId);
        return BusinessResponseFactory.createSuccess(userComputePower);
    }


    @ApiOperation(value = "生成当前用户算力排行（内部定时器调用）")
    @GetMapping("/updateComputeSortTask")
    public ResponseData updateComputeSortTask(){
        miningService.updateComputeSortTask();
        return BusinessResponseFactory.createSuccess(null);
    }

    public static void main(String[] args) {
        for (int i =0; i < 5; i++){
            System.out.println(UUIDUtils.getUuid());
        }
    }
}
