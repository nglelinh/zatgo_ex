package com.zatgo.zup.suanli.mapper;

import com.github.pagehelper.Page;
import com.zatgo.zup.suanli.entity.Investor;
import com.zatgo.zup.suanli.model.InvestorData;
import com.zatgo.zup.suanli.model.InvestorSearchParams;

public interface InvestorMapper {
    int deleteByPrimaryKey(Long investorId);

    int insert(Investor record);

    int insertSelective(Investor record);

    Investor selectByPrimaryKey(Long investorId);

    int updateByPrimaryKeySelective(Investor record);

    int updateByPrimaryKey(Investor record);
    
    Page<InvestorData> selectInvestorByPage(InvestorSearchParams params);
    
    InvestorData selectInvestorByInvestorId(Long investorId);
    
    int insertInvestorSelective(Investor record);
    
    Investor selectInvestorByUserId(String userId);
}