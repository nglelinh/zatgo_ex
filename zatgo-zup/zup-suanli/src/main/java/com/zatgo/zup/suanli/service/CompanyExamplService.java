package com.zatgo.zup.suanli.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.suanli.entity.CompanyExample;

/**
 * Created by 46041 on 2019/9/3.
 */
public interface CompanyExamplService {

    CompanyExample detail(String id);

    PageInfo<CompanyExample> frontList(Integer pageSzie, Integer pageNo);

    void add(CompanyExample companyExample);

    void del(String id);

    void edit(CompanyExample companyExample);

    PageInfo<CompanyExample> backList(Integer pageSzie, Integer pageNo);
}
