package com.zatgo.zup.suanli.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.DeleteFileParams;
import com.zatgo.zup.suanli.model.FileLibraryData;
import com.zatgo.zup.suanli.model.FileLibraryParams;
import com.zatgo.zup.suanli.model.UploadFileParams;
import com.zatgo.zup.suanli.service.OssService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/suanli/oss")
public class OssController extends BaseController {

	@Autowired
	private OssService ossService;

	@ApiOperation(value = "上传文件")
	@PostMapping(value = "/upload/file", name = "上传文件")
	public ResponseData<Map<String, String>> uploadFile(@ModelAttribute UploadFileParams params) {
		String token = request.getHeader("token");
		getUserInfo(token);
		Map<String, String> data = ossService.uploagFile(params);
		return BusinessResponseFactory.createSuccess(data);
	}

	@ApiOperation(value = "删除文件")
	@PostMapping(value = "/delete/file", name = "删除文件")
	public ResponseData<Object> deleteFile(@RequestBody DeleteFileParams params) {
		String token = request.getHeader("token");
		getUserInfo(token);
		ossService.deleteFile(params);
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "文件库")
	@GetMapping(value = "/file/library", name = "文件库")
	public ResponseData<FileLibraryData> fileLibrary(@ModelAttribute FileLibraryParams params){
		getAdminUserInfo();
		FileLibraryData data = ossService.fileLibrary(params);
		return BusinessResponseFactory.createSuccess(data);
	}

}
