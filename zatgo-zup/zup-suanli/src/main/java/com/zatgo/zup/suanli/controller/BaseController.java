package com.zatgo.zup.suanli.controller;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.suanli.service.PartyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by 46041 on 2018/9/4.
 */
public class BaseController {

    @Resource
    protected HttpServletRequest request;

    @Resource
    protected HttpServletResponse response;

    @Resource
    protected HttpSession session;

    @Autowired
    protected RedisTemplate redisTemplate;
    
    @Autowired
    public PartyService partyService;

    protected AuthUserInfo getUserInfo(){
        String token = request.getHeader("token");
        AuthUserInfo authUserInfo = getUserInfo(token);
        
        if(authUserInfo.getIsAdmin()) {
        	throw new BusinessException(BusinessExceptionCode.USER_AUTH_ERROR);
        }
        
        partyService.addParty(authUserInfo.getUserId());
        return authUserInfo;
    }


    protected AuthUserInfo getUserInfo(String token){
        AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
        if(authUserInfo == null) {
            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
        }
        
        return authUserInfo;
    }
    
    protected String getUserId() {
    	String token = request.getHeader("token");
    	AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
    	if(authUserInfo == null) {
    		return null;
    	}
    	
    	if(authUserInfo.getIsAdmin()) {
    		return null;
        }
        
        partyService.addParty(authUserInfo.getUserId());
    	
    	return authUserInfo.getUserId();
    }
    
    protected AuthUserInfo getAdminUserInfo(){
        String token = request.getHeader("token");
        AuthUserInfo authUserInfo = getUserInfo(token);
        
        if(authUserInfo.getIsAdmin() == null || authUserInfo.getIsAdmin() == false) {
        	throw new BusinessException(BusinessExceptionCode.USER_AUTH_ERROR);
        }
        
        return authUserInfo;
    }

    /**
     * 校验操作用户数据的权限
     * @param userId 被操作的用户ID
     * @return
     */
    protected void verifyUserInterface(String userId) {

        //是否内部接口调用
        String innerAccessId = request.getHeader("innerAccessId");
        if(innerAccessId != null && !innerAccessId.trim().equals("")) {
            String isInnerAccessTrue = (String)redisTemplate.opsForValue().get(innerAccessId);
            if(isInnerAccessTrue != null) {
                return;
            }
        }

        //如果不是内部接口调用，则只能操作自己的数据
        AuthUserInfo userInfo = getUserInfo();
        if(userInfo.getUserId().equals(userId)) {
            return;
        }else {
            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
        }
    }

    protected String getClientIp(){
        return request.getHeader("client-ip");
    }

    protected String getAppType() {
        return request.getHeader("appType");
    }

    protected String getVersion() {
        return request.getHeader("appVersion");
    }
    
    protected Byte getLanguage() {
        return request.getHeader("language") == null ? null : new Byte(request.getHeader("language"));
    }
}
