package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.InstitutionInvestorRel;

public interface InstitutionInvestorRelMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InstitutionInvestorRel record);

    int insertSelective(InstitutionInvestorRel record);

    InstitutionInvestorRel selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(InstitutionInvestorRel record);

    int updateByPrimaryKey(InstitutionInvestorRel record);
    
    InstitutionInvestorRel selectByInvestorId(Long investorId);
}