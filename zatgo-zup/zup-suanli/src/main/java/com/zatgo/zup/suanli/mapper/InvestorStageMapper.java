package com.zatgo.zup.suanli.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.suanli.entity.InvestorStage;

public interface InvestorStageMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InvestorStage record);

    int insertSelective(InvestorStage record);

    InvestorStage selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(InvestorStage record);

    int updateByPrimaryKey(InvestorStage record);
    
    List<InvestorStage> selectInvestorStageByInvestor(@Param("investorId") Long investorId);
    
    int deleteByInvestorId(Long investorId);
}