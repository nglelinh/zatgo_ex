package com.zatgo.zup.suanli.model;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("增改投资人参数")
public class InvestorParams {
	
	@ApiModelProperty(value = "投资人id，修改使用", required = false)
	private Long investorId;
	
	@ApiModelProperty(value = "投资机构id", required = false)
	private Long institutionId;
	
	@ApiModelProperty(value = "投资人名称", required = true)
	private String name;
	
	@ApiModelProperty(value = "投资人简介", required = false)
	private String shotDesc;
	
	@ApiModelProperty(value = "投资人详细介绍", required = false)
	private String content;
	
	@ApiModelProperty(value = "最小投资额度，单位：万", required = false)
	private BigDecimal minInvestAmount;
	
	@ApiModelProperty(value = "最大投资额度，单位：万", required = false)
	private BigDecimal maxInvestAmount;
	
	@ApiModelProperty(value = "创建人", required = false, hidden = true)
	private String creator;
	
	@ApiModelProperty(value = "职位", required = false)
	private String position;
	
	@ApiModelProperty(value = "联系方式", required = false)
	private String contact;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = true, allowableValues = "0,1")
	private Byte language;
	
	@ApiModelProperty(value = "状态：0-已删除；1-使用中", required = false, allowableValues = "0,1")
	private Byte status;
	
	@ApiModelProperty(value = "标签", required = false)
	private List<Long> tagIds;
	
	@ApiModelProperty(value = "关注分类id", required = false)
	private List<Long> catalogIdList;
	
	@ApiModelProperty(value = "关注阶段", required = false)
	private List<Byte> stageDatas;
	
	@ApiModelProperty(value = "关注城市", required = false)
	private List<String> cityDatas;
	
	@ApiModelProperty(value = "partyId", required = true)
	private Long partyId;
	
	@ApiModelProperty(value = "投资人角色：0投资人", required = true, allowableValues = "0")
	private Long partyRoleId;

	public Long getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Long institutionId) {
		this.institutionId = institutionId;
	}

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShotDesc() {
		return shotDesc;
	}

	public void setShotDesc(String shotDesc) {
		this.shotDesc = shotDesc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public BigDecimal getMinInvestAmount() {
		return minInvestAmount;
	}

	public void setMinInvestAmount(BigDecimal minInvestAmount) {
		this.minInvestAmount = minInvestAmount;
	}

	public BigDecimal getMaxInvestAmount() {
		return maxInvestAmount;
	}

	public void setMaxInvestAmount(BigDecimal maxInvestAmount) {
		this.maxInvestAmount = maxInvestAmount;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public List<Long> getTagIds() {
		return tagIds;
	}

	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}

	public List<Long> getCatalogIdList() {
		return catalogIdList;
	}

	public void setCatalogIdList(List<Long> catalogIdList) {
		this.catalogIdList = catalogIdList;
	}

	public List<Byte> getStageDatas() {
		return stageDatas;
	}

	public void setStageDatas(List<Byte> stageDatas) {
		this.stageDatas = stageDatas;
	}

	public List<String> getCityDatas() {
		return cityDatas;
	}

	public void setCityDatas(List<String> cityDatas) {
		this.cityDatas = cityDatas;
	}

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public Long getPartyRoleId() {
		return partyRoleId;
	}

	public void setPartyRoleId(Long partyRoleId) {
		this.partyRoleId = partyRoleId;
	}

}
