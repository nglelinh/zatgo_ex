package com.zatgo.zup.suanli.model;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("基础分类列表")
public class CatalogBaseData {
	
	@ApiModelProperty(value = "目录id", required = true)
	private Long catalogId;
	
	@ApiModelProperty(value = "父目录id", required = true)
	private Long parentCatalogId;
	
	@ApiModelProperty(value = "目录名称", required = true)
	private String name;
	
	@ApiModelProperty(value = "目录类型 0：项目", required = true)
	private Byte catalogType;
	
	@ApiModelProperty(value = "目录级别：0为根目录", required = true)
	private Byte catalogLevel;
	
	@ApiModelProperty(value = "子目录", required = true)
	private List<CatalogBaseData> children;

	public Long getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}

	public Long getParentCatalogId() {
		return parentCatalogId;
	}

	public void setParentCatalogId(Long parentCatalogId) {
		this.parentCatalogId = parentCatalogId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Byte getCatalogType() {
		return catalogType;
	}

	public void setCatalogType(Byte catalogType) {
		this.catalogType = catalogType;
	}

	public Byte getCatalogLevel() {
		return catalogLevel;
	}

	public void setCatalogLevel(Byte catalogLevel) {
		this.catalogLevel = catalogLevel;
	}

	public List<CatalogBaseData> getChildren() {
		return children;
	}

	public void setChildren(List<CatalogBaseData> children) {
		this.children = children;
	}

}
