package com.zatgo.zup.suanli.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.SuanliEnum.CampaignStatus;
import com.zatgo.zup.common.enumtype.SuanliEnum.UserFavoriteType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.entity.Campaign;
import com.zatgo.zup.suanli.entity.CampaignCatalog;
import com.zatgo.zup.suanli.entity.CampaignRegistration;
import com.zatgo.zup.suanli.entity.CampaignSponsor;
import com.zatgo.zup.suanli.entity.City;
import com.zatgo.zup.suanli.entity.Company;
import com.zatgo.zup.suanli.entity.Party;
import com.zatgo.zup.suanli.entity.UserFavorite;
import com.zatgo.zup.suanli.mapper.CampaignCatalogMapper;
import com.zatgo.zup.suanli.mapper.CampaignMapper;
import com.zatgo.zup.suanli.mapper.CampaignRegistrationMapper;
import com.zatgo.zup.suanli.mapper.CampaignSponsorMapper;
import com.zatgo.zup.suanli.mapper.CityMapper;
import com.zatgo.zup.suanli.mapper.CompanyMapper;
import com.zatgo.zup.suanli.mapper.PartyMapper;
import com.zatgo.zup.suanli.mapper.UserFavoriteMapper;
import com.zatgo.zup.suanli.model.CampaignData;
import com.zatgo.zup.suanli.model.CampaignParams;
import com.zatgo.zup.suanli.model.CampaignRegistrationParams;
import com.zatgo.zup.suanli.model.CampaignSearchParams;
import com.zatgo.zup.suanli.service.CampaignService;

@Service
public class CampaignServiceImpl implements CampaignService {
	
	private static final Logger log = LoggerFactory.getLogger(CampaignServiceImpl.class);
	
	@Autowired
	private CampaignMapper campaignMapper;
	
	@Autowired
	private PartyMapper partyMapper;
	
	@Autowired
	private CampaignRegistrationMapper campaignRegistrationMapper;
	
	@Autowired
	private CampaignCatalogMapper campaignCatalogMapper;
	
	@Autowired
	private CompanyMapper companyMapper;
	
	@Autowired
	private CampaignSponsorMapper campaignSponsorMapper;
	
	@Autowired
	private UserFavoriteMapper userFavoriteMapper;
	
	@Autowired
	private CityMapper cityMapper;

	@Override
	public PageInfo<CampaignData> getCampaignList(CampaignSearchParams params) {
		params.setStatus(CampaignStatus.USING.getStatus());
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		Page<CampaignData> page = campaignMapper.selectCampaignDataByPage(params);
		PageInfo<CampaignData> pageInfo = new PageInfo<>(page.getPageNum(), page.getPageSize(), page.getTotal(),
				page.getResult());

		PageHelper.clearPage();
		return pageInfo;
	}

	@Override
	public CampaignData getCampaignDetail(Long campaignId, String userId) {
		CampaignData data = campaignMapper.selectCampaignDetail(campaignId);
		if(data == null) {
			return data;
		}
		
		List<Long> catalogIds = campaignCatalogMapper.selectCampaignCatalogByCampaignId(campaignId);
		data.setCatalogIds(catalogIds);
		
		data.setIsCollect(false);
		data.setIsRegister(false);
		for (;;) {
			
			if(StringUtils.isEmpty(userId)) {
				break;
			}
			
			UserFavorite favorite = userFavoriteMapper.selectByIdAndType(userId, campaignId, UserFavoriteType.CAMPAIGN.getType());
			if(favorite != null) {
				data.setIsCollect(true);
			}
			
			Party party = partyMapper.selectPartyByUserId(userId);
			if(party == null) {
				break;
			}
			
			CampaignRegistration registration = campaignRegistrationMapper.selectByUserIdAndPartyId(campaignId, party.getPartyId());
			if(registration == null) {
				break;
			}
			
			data.setIsRegister(true);
			break;
			
		}
		return data;
	}

	@Transactional
	@Override
	public void campaignRegistration(CampaignRegistrationParams params) {
		Party party = partyMapper.selectPartyByUserId(params.getUserId());
		if(party == null) {
			log.error("party 信息未获取");
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		
		CampaignRegistration registration = new CampaignRegistration();
		registration.setCampaignId(params.getCampaignId());
		registration.setPartyId(party.getPartyId());
		registration.setRegTime(new Date());
		campaignRegistrationMapper.insertSelective(registration);
	}

	@Transactional
	@Override
	public CampaignData addCampaign(CampaignParams params) {
		String name = params.getName();
		if(StringUtils.isEmpty(name)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "campaign name miss");
		}
		Date begin = params.getCampaignBegin();
		if(begin == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "campaign begin date miss");
		}
		Date end = params.getCampaignEnd();
		if(end == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "campaign end date miss");
		}
		Byte language = params.getLanguage();
		if(language == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "language miss");
		}
		Long companyId = params.getCompanyId();
		if(companyId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company id miss");
		}
		Company company = companyMapper.selectByPrimaryKey(companyId);
		if(company == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company not exist");
		}
		
		String companyName = company.getName();
		
		Campaign campaign = campaignMapper.selectCampaignByName(name);
		if(campaign != null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "campaign existed");
		}
		
		City city = cityMapper.selectByPrimaryKey(params.getCityCode());
		params.setCityName(city.getCityName());
		
		campaign = new Campaign();
		BeanUtils.copyProperties(params, campaign);
		campaign.setCreateTime(new Date());
		campaign.setUpdateTime(new Date());
		
		campaignMapper.insertCampaignSelective(campaign);
		Long campaignId = campaign.getCampaignId();
		List<Long> catalogIds = params.getCatalogIds();
		if(CollectionUtils.isNotEmpty(catalogIds)) {
			for (Long catalogId : catalogIds) {
				CampaignCatalog campaignCatalog = new CampaignCatalog();
				campaignCatalog.setCampaignId(campaignId);
				campaignCatalog.setCatalogId(catalogId);
				campaignCatalogMapper.insertSelective(campaignCatalog);
			}
		}
		
		CampaignSponsor sponsor = new CampaignSponsor();
		sponsor.setCampaignId(campaignId);
		sponsor.setCompanyId(companyId);
		sponsor.setCompanyName(companyName);
		campaignSponsorMapper.insertSelective(sponsor);
		
		CampaignData campaignData = new CampaignData();
		BeanUtils.copyProperties(campaign, campaignData);
		
		return campaignData;
	}

	@Transactional
	@Override
	public CampaignData updateCampaign(CampaignParams params) {
		Long campaignId = params.getCampaignId();
		if(campaignId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "campaign id miss");
		}
		Campaign campaign = campaignMapper.selectByPrimaryKey(campaignId);
		if(campaign == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "campaign not exist");
		}
		
		Long companyId = campaign.getCampaignId();
		
		if(StringUtils.isNotEmpty(params.getCityCode())) {
			City city = cityMapper.selectByPrimaryKey(params.getCityCode());
			params.setCityName(city.getCityName());
		}
		
		campaign = new Campaign();
		BeanUtils.copyProperties(params, campaign);
		campaign.setUpdateTime(new Date());
		
		campaignMapper.updateByPrimaryKeySelective(campaign);
		
		List<Long> catalogIds = params.getCatalogIds();
		if(CollectionUtils.isNotEmpty(catalogIds)) {
			campaignCatalogMapper.deleteByCampaignId(campaignId);
			for (Long catalogId : catalogIds) {
				CampaignCatalog campaignCatalog = new CampaignCatalog();
				campaignCatalog.setCampaignId(campaignId);
				campaignCatalog.setCatalogId(catalogId);
				campaignCatalogMapper.insertSelective(campaignCatalog);
			}
		}
		
		if(params.getCompanyId() != null && !companyId.equals(params.getCompanyId())) {
			CampaignSponsor sponsor = campaignSponsorMapper.selectByCampaignId(campaignId);
			Company company = companyMapper.selectByPrimaryKey(params.getCompanyId());
			if(company == null) {
				throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company not exist");
			}
			
			sponsor.setCompanyId(company.getCompanyId());
			sponsor.setCompanyName(company.getName());
			campaignSponsorMapper.updateByPrimaryKeySelective(sponsor);
		}
		
		CampaignData data = new CampaignData();
		BeanUtils.copyProperties(campaign, data);
		return data;
	}

	@Transactional
	@Override
	public void deleteCampaign(Long campaignId) {
		Campaign campaign = new Campaign();
		campaign.setCampaignId(campaignId);
		campaign.setStatus(CampaignStatus.DELETE.getStatus());
		campaign.setUpdateTime(new Date());
		campaignMapper.updateByPrimaryKeySelective(campaign);
	}

}
