package com.zatgo.zup.suanli.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.suanli.entity.Catalog;
import com.zatgo.zup.suanli.entity.InvestorCatalog;
import com.zatgo.zup.suanli.model.InvestorCatalogData;

public interface InvestorCatalogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InvestorCatalog record);

    int insertSelective(InvestorCatalog record);

    InvestorCatalog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(InvestorCatalog record);

    int updateByPrimaryKey(InvestorCatalog record);
    
    List<Catalog> selectInvestorCatalogByInvestor(@Param("investorId") Long investorId);
    
    int deleteByInvestorId(Long investorId);
    
    List<InvestorCatalogData> selectInvestorCatalog(Long investorId);
}