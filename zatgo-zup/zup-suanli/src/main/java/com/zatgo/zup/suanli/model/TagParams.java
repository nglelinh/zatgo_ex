package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("增改标签参数")
public class TagParams {
	
	@ApiModelProperty(value = "标签id。修改使用", required = false)
	private Long tagId;
	
	@ApiModelProperty(value = "标签名称", required = false)
	private String name;
	
	@ApiModelProperty(value = "标签类型：0：系统 1：系统自定义 2：用户自定义", required = false)
	private Byte type;
	
	@ApiModelProperty(value = "0：项目 1：投资人 2：投资机构", required = false)
	private Byte classType;
	
	@ApiModelProperty(value = "创建者", required = false, hidden = true)
	private String creator;

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public Byte getClassType() {
		return classType;
	}

	public void setClassType(Byte classType) {
		this.classType = classType;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

}
