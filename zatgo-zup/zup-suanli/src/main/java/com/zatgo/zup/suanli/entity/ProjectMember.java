package com.zatgo.zup.suanli.entity;

public class ProjectMember {
    private Long partyRoleRelId;

    private Long projectId;

    private String projectRole;

    public Long getPartyRoleRelId() {
        return partyRoleRelId;
    }

    public void setPartyRoleRelId(Long partyRoleRelId) {
        this.partyRoleRelId = partyRoleRelId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectRole() {
        return projectRole;
    }

    public void setProjectRole(String projectRole) {
        this.projectRole = projectRole == null ? null : projectRole.trim();
    }
}