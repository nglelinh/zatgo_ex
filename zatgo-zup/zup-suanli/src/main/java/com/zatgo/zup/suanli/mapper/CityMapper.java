package com.zatgo.zup.suanli.mapper;

import java.util.List;

import com.zatgo.zup.suanli.entity.City;
import com.zatgo.zup.suanli.model.CityData;

public interface CityMapper {
    int deleteByPrimaryKey(String cityCode);

    int insert(City record);

    int insertSelective(City record);

    City selectByPrimaryKey(String cityCode);

    int updateByPrimaryKeySelective(City record);

    int updateByPrimaryKey(City record);
    
    List<CityData> selectCityList();
}