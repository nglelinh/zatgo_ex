package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.CampaignSponsor;

public interface CampaignSponsorMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CampaignSponsor record);

    int insertSelective(CampaignSponsor record);

    CampaignSponsor selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CampaignSponsor record);

    int updateByPrimaryKey(CampaignSponsor record);
    
    CampaignSponsor selectByCampaignId(Long campaignId);
}