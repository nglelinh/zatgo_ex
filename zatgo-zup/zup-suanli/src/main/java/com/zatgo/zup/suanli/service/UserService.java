package com.zatgo.zup.suanli.service;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.model.FavoriteParams;
import com.zatgo.zup.suanli.model.InvestorData;
import com.zatgo.zup.suanli.model.ProjectData;
import com.zatgo.zup.suanli.model.UserData;
import com.zatgo.zup.suanli.model.UserFavoriteParams;

public interface UserService {
	
	/**
	 * 添加收藏
	 * @param params
	 * @return
	 */
	public Long addUserFavorite(UserFavoriteParams params);
	
	/**
	 * 删除收藏
	 * @param params
	 */
	public void deleteUserFavorite(UserFavoriteParams params);
	
	/**
	 * 关注的项目
	 * @param params
	 * @return
	 */
	public PageInfo<ProjectData> getFavoriteProjectList(FavoriteParams params);
	
	/**
	 * 关注的投资人
	 * @param params
	 * @return
	 */
	public PageInfo<InvestorData> getFavoriteInvestorList(FavoriteParams params);
	
	/**
	 * 获取用户投资认证id
	 * @param userId
	 * @return
	 */
	public Long getUserInvestor(String userId);
	
	/**
	 * 用户信息
	 * @param userId
	 * @return
	 */
	public UserData getUserData(String userId);

}
