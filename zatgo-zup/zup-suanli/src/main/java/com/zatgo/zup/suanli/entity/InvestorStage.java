package com.zatgo.zup.suanli.entity;

public class InvestorStage {
    private Long id;

    private Long investorId;

    private Byte stageType;

    private String stageName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInvestorId() {
        return investorId;
    }

    public void setInvestorId(Long investorId) {
        this.investorId = investorId;
    }

    public Byte getStageType() {
        return stageType;
    }

    public void setStageType(Byte stageType) {
        this.stageType = stageType;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName == null ? null : stageName.trim();
    }
}