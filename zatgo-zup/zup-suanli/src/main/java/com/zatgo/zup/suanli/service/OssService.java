package com.zatgo.zup.suanli.service;

import java.util.Map;

import com.zatgo.zup.suanli.model.DeleteFileParams;
import com.zatgo.zup.suanli.model.FileLibraryData;
import com.zatgo.zup.suanli.model.FileLibraryParams;
import com.zatgo.zup.suanli.model.UploadFileParams;

public interface OssService {
	
	/**
	 * 上传文件
	 * @param params
	 * @return
	 */
	public Map<String, String> uploagFile(UploadFileParams params);
	
	/**
	 * 删除文件
	 * @param params
	 */
	public void deleteFile(DeleteFileParams params);
	
	/**
	 * 文件库
	 * @param params
	 * @return
	 */
	public FileLibraryData fileLibrary(FileLibraryParams params);

}
