package com.zatgo.zup.suanli.controller;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.entity.NeedMessage;
import com.zatgo.zup.suanli.service.NeedMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2019/9/2.
 */

@RestController
@Api(value = "/suanli/message", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/message")
public class NeedMessageController {

    @Autowired
    private NeedMessageService needMessageService;

    @ApiOperation(value = "新增")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseData add(@RequestBody NeedMessage needMessage){
        needMessageService.add(needMessage);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "删除")
    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public ResponseData del(String id){
        needMessageService.del(id);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "修改")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseData edit(NeedMessage needMessage){
        needMessageService.edit(needMessage);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "查询")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseData<PageInfo<NeedMessage>> list (@RequestParam("pageSize") Integer pageSize,
                                                     @RequestParam("pageNo") Integer pageNo,
                                                     @RequestParam(value = "type", required = false) Integer type,
                                                     @RequestParam(value = "sources", required = false) Integer source){
        return BusinessResponseFactory.createSuccess(needMessageService.list(pageSize, pageNo, type, source));
    }

}
