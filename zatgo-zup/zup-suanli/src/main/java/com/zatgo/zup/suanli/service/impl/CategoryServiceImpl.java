package com.zatgo.zup.suanli.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zatgo.zup.common.enumtype.SuanliEnum.CatalogStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.suanli.entity.Catalog;
import com.zatgo.zup.suanli.mapper.CatalogMapper;
import com.zatgo.zup.suanli.mapper.CityMapper;
import com.zatgo.zup.suanli.mapper.StageMapper;
import com.zatgo.zup.suanli.model.CatalogBaseData;
import com.zatgo.zup.suanli.model.CatalogData;
import com.zatgo.zup.suanli.model.CatalogParams;
import com.zatgo.zup.suanli.model.CatalogSearchParams;
import com.zatgo.zup.suanli.model.CityData;
import com.zatgo.zup.suanli.model.StageData;
import com.zatgo.zup.suanli.model.StageSearchParams;
import com.zatgo.zup.suanli.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CityMapper cityMapper;

	@Autowired
	private CatalogMapper catalogMapper;

	@Autowired
	private StageMapper stageMapper;

	@Override
	public List<CityData> getCityList() {
		return cityMapper.selectCityList();
	}

	@Override
	public List<CatalogData> getCatalogList(CatalogSearchParams params) {
		params.setStatus(CatalogStatus.USING.getStatus());
		List<CatalogData> list = catalogMapper.selectCatalogByParams(params);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}

		List<CatalogData> data = new ArrayList<>();
		for (CatalogData catalogData : list) {
			if (catalogData.getParentCatalogId() == 0) {
				catalogData.setCatalogDatas(getCatalogList(catalogData.getCatalogId(), list));
				data.add(catalogData);
			}
		}

		return data;
	}

	private List<CatalogData> getCatalogList(Long parentId, List<CatalogData> catalogList) {
		List<CatalogData> childrenList = new ArrayList<>();
		CatalogData data = new CatalogData();
		data.setCatalogId(parentId);
		data.setName("不限");
		data.setParentCatalogId(parentId);
		childrenList.add(data);
		for (CatalogData catalogData : catalogList) {
			if (catalogData.getParentCatalogId() == parentId) {
				catalogData.setCatalogDatas(getCatalogList(catalogData.getCatalogId(), catalogList));
				childrenList.add(catalogData);
			}
		}

		return childrenList;
	}
	
	@Override
	public List<CatalogBaseData> getCatalogBaseList(CatalogSearchParams params) {
		params.setStatus(CatalogStatus.USING.getStatus());
		List<CatalogData> list = catalogMapper.selectCatalogByParams(params);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}

		List<CatalogBaseData> data = new ArrayList<>();
		for (CatalogData catalogData : list) {
			if (catalogData.getParentCatalogId() == 0) {
				CatalogBaseData baseData = new CatalogBaseData();
				BeanUtils.copyProperties(catalogData, baseData);
				baseData.setChildren(getCatalogChildrenList(catalogData.getCatalogId(), list));
				data.add(baseData);
			}
		}

		return data;
	}
	
	private List<CatalogBaseData> getCatalogChildrenList(Long parentId, List<CatalogData> catalogList) {
		List<CatalogBaseData> childrenList = new ArrayList<>();
		for (CatalogData catalogData : catalogList) {
			if (catalogData.getParentCatalogId() == parentId) {
				CatalogBaseData baseData = new CatalogBaseData();
				BeanUtils.copyProperties(catalogData, baseData);
				baseData.setChildren(getCatalogChildrenList(catalogData.getCatalogId(), catalogList));
				childrenList.add(baseData);
			}
		}

		return childrenList;
	}

	@Override
	public List<StageData> getStageList(StageSearchParams params) {
		return stageMapper.selectStageList(params);
	}

	@Transactional
	@Override
	public CatalogData addCatalog(CatalogParams params) {
		Long parentCatalogId = params.getParentCatalogId();
		if (parentCatalogId == null) {
			parentCatalogId = 0L;
		}
		String name = params.getName();
		if(StringUtils.isEmpty(name)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog name miss");
		}
		Byte catalogLevel = params.getCatalogLevel();
		if (catalogLevel == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog level miss");
		}
		Byte catalogType = params.getCatalogType();
		if (catalogType == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog type miss");
		}
		
		if (parentCatalogId != 0) {
			Catalog catalog = catalogMapper.selectByPrimaryKey(parentCatalogId);
			if (catalog == null) {
				throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog not exist");
			}
		}
		
		Catalog catalog = new Catalog();
		BeanUtils.copyProperties(params, catalog);
		catalog.setParentCatalogId(parentCatalogId);
		catalog.setCreateTime(new Date());
		catalog.setUpdateTime(new Date());
		catalogMapper.insertSelective(catalog);
		
		CatalogData data = new CatalogData();
		BeanUtils.copyProperties(catalog, data);
		return data;
	}

	@Transactional
	@Override
	public CatalogData updateCatalog(CatalogParams params) {
		
		Long catalogId = params.getCatalogId();
		if(catalogId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog id miss");
		}
		Long parentCatalogId = params.getParentCatalogId();
		if (parentCatalogId == null) {
			parentCatalogId = 0L;
		}
		String name = params.getName();
		if(StringUtils.isEmpty(name)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog name miss");
		}
		Byte catalogLevel = params.getCatalogLevel();
		if (catalogLevel == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog level miss");
		}
		Byte catalogType = params.getCatalogType();
		if (catalogType == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog type miss");
		}
		
		if (parentCatalogId != 0) {
			Catalog catalog = catalogMapper.selectByPrimaryKey(parentCatalogId);
			if (catalog == null) {
				throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "catalog not exist");
			}
		}
		
		Catalog catalog = new Catalog();
		BeanUtils.copyProperties(params, catalog);
		catalog.setUpdateTime(new Date());
		catalogMapper.updateByPrimaryKeySelective(catalog);
		CatalogData data = new CatalogData();
		BeanUtils.copyProperties(data, catalog);
		return data;
	}

	@Transactional
	@Override
	public void deleteCatalog(Long catalogId) {
		Catalog catalog = new Catalog();
		catalog.setCatalogId(catalogId);
		catalog.setStatus(CatalogStatus.DELETE.getStatus());
		catalog.setUpdateTime(new Date());
		catalogMapper.updateByPrimaryKeySelective(catalog);
	}

}
