package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "蒜粒用户信息")
public class UserData {
	
	@ApiModelProperty(value = "关注数", required = true)
	private Integer follow;
	
	@ApiModelProperty(value = "项目数", required = true)
	private Integer project;
	
	@ApiModelProperty(value = "投资人id", required = false)
	private Long investorId;

	public Integer getFollow() {
		return follow;
	}

	public void setFollow(Integer follow) {
		this.follow = follow;
	}

	public Integer getProject() {
		return project;
	}

	public void setProject(Integer project) {
		this.project = project;
	}

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

}
