package com.zatgo.zup.suanli.entity;

import java.util.Date;

public class PartyRole {
    private Long partyRoleId;

    private String partyRoleName;

    private Date createTime;

    private String creator;

    private Date updateTime;

    public Long getPartyRoleId() {
        return partyRoleId;
    }

    public void setPartyRoleId(Long partyRoleId) {
        this.partyRoleId = partyRoleId;
    }

    public String getPartyRoleName() {
        return partyRoleName;
    }

    public void setPartyRoleName(String partyRoleName) {
        this.partyRoleName = partyRoleName == null ? null : partyRoleName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}