package com.zatgo.zup.suanli.mapper;

import java.util.List;

import com.github.pagehelper.Page;
import com.zatgo.zup.suanli.entity.Project;
import com.zatgo.zup.suanli.model.ProjectData;
import com.zatgo.zup.suanli.model.ProjectSearchParams;

public interface ProjectMapper {
    int deleteByPrimaryKey(Long projectId);

    int insert(Project record);

    int insertSelective(Project record);

    Project selectByPrimaryKey(Long projectId);

    int updateByPrimaryKeySelective(Project record);

    int updateByPrimaryKey(Project record);
    
    Page<ProjectData> selectProjectByPage(ProjectSearchParams params);
    
    Project selectProjectByName(String name);
    
    int insertProjectSelective(Project record);
    
    List<ProjectData> selectProjectByUserId(String userId);
    
    int countUserProjectNum(String userId);
    
    Page<ProjectData> selectProjectAdminByPage(ProjectSearchParams params);
}