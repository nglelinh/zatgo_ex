package com.zatgo.zup.suanli.service;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.model.CampaignData;
import com.zatgo.zup.suanli.model.CampaignParams;
import com.zatgo.zup.suanli.model.CampaignRegistrationParams;
import com.zatgo.zup.suanli.model.CampaignSearchParams;

public interface CampaignService {
	
	/**
	 * 查询活动列表
	 * @param params
	 * @return
	 */
	public PageInfo<CampaignData> getCampaignList(CampaignSearchParams params);
	
	/**
	 * 查询活动详情
	 * @param params
	 * @return
	 */
	public CampaignData getCampaignDetail(Long campaignId, String userId);
	
	/**
	 * 活动报名
	 * @param params
	 */
	public void campaignRegistration(CampaignRegistrationParams params);
	
	/**
	 * 新增活动
	 * @param params
	 * @return
	 */
	public CampaignData addCampaign(CampaignParams params);
	
	/**
	 * 修改活动
	 * @param params
	 * @return
	 */
	public CampaignData updateCampaign(CampaignParams params);
	
	/**
	 * 删除活动
	 * @param campaignId
	 */
	public void deleteCampaign(Long campaignId);

}
