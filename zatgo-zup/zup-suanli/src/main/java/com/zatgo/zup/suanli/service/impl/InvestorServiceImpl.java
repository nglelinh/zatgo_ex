package com.zatgo.zup.suanli.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.SuanliEnum.InvestorStatus;
import com.zatgo.zup.common.enumtype.SuanliEnum.TagClassType;
import com.zatgo.zup.common.enumtype.SuanliEnum.UserFavoriteType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.entity.Catalog;
import com.zatgo.zup.suanli.entity.City;
import com.zatgo.zup.suanli.entity.Institution;
import com.zatgo.zup.suanli.entity.InstitutionInvestorRel;
import com.zatgo.zup.suanli.entity.Investor;
import com.zatgo.zup.suanli.entity.InvestorCatalog;
import com.zatgo.zup.suanli.entity.InvestorCity;
import com.zatgo.zup.suanli.entity.InvestorStage;
import com.zatgo.zup.suanli.entity.Party;
import com.zatgo.zup.suanli.entity.PartyInfo;
import com.zatgo.zup.suanli.entity.PartyRole;
import com.zatgo.zup.suanli.entity.PartyRoleRel;
import com.zatgo.zup.suanli.entity.Stage;
import com.zatgo.zup.suanli.entity.TagRel;
import com.zatgo.zup.suanli.entity.UserFavorite;
import com.zatgo.zup.suanli.mapper.CityMapper;
import com.zatgo.zup.suanli.mapper.InstitutionInvestorRelMapper;
import com.zatgo.zup.suanli.mapper.InstitutionMapper;
import com.zatgo.zup.suanli.mapper.InvestorCatalogMapper;
import com.zatgo.zup.suanli.mapper.InvestorCityMapper;
import com.zatgo.zup.suanli.mapper.InvestorMapper;
import com.zatgo.zup.suanli.mapper.InvestorProjectMapper;
import com.zatgo.zup.suanli.mapper.InvestorStageMapper;
import com.zatgo.zup.suanli.mapper.PartyInfoMapper;
import com.zatgo.zup.suanli.mapper.PartyMapper;
import com.zatgo.zup.suanli.mapper.PartyRoleMapper;
import com.zatgo.zup.suanli.mapper.PartyRoleRelMapper;
import com.zatgo.zup.suanli.mapper.StageMapper;
import com.zatgo.zup.suanli.mapper.TagRelMapper;
import com.zatgo.zup.suanli.mapper.UserFavoriteMapper;
import com.zatgo.zup.suanli.model.CityData;
import com.zatgo.zup.suanli.model.InvestorCatalogData;
import com.zatgo.zup.suanli.model.InvestorData;
import com.zatgo.zup.suanli.model.InvestorParams;
import com.zatgo.zup.suanli.model.InvestorPartyData;
import com.zatgo.zup.suanli.model.InvestorSearchParams;
import com.zatgo.zup.suanli.model.StageData;
import com.zatgo.zup.suanli.model.TagData;
import com.zatgo.zup.suanli.service.InvestorService;

@Service
public class InvestorServiceImpl implements InvestorService {

	@Autowired
	private InvestorMapper investorMapper;

	@Autowired
	private InvestorCatalogMapper investorCatalogMapper;

	@Autowired
	private InvestorCityMapper investorCityMapper;

	@Autowired
	private InvestorProjectMapper investorProjectMapper;

	@Autowired
	private InvestorStageMapper investorStageMapper;

	@Autowired
	private InstitutionInvestorRelMapper institutionInvestorRelMapper;

	@Autowired
	private PartyMapper partyMapper;

	@Autowired
	private PartyInfoMapper partyInfoMapper;

	@Autowired
	private PartyRoleRelMapper partyRoleRelMapper;
	
	@Autowired
	private PartyRoleMapper partyRoleMapper;
	
	@Autowired
	private TagRelMapper tagRelMapper;
	
	@Autowired
	private UserFavoriteMapper userFavoriteMapper;
	
	@Autowired
	private StageMapper stageMapper;
	
	@Autowired
	private CityMapper cityMapper;
	
	@Autowired
	private InstitutionMapper institutionMapper;

	@Override
	public PageInfo<InvestorData> getInvestorList(InvestorSearchParams params) {
		params.setStatus(InvestorStatus.USING.getStatus());
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		Page<InvestorData> page = investorMapper.selectInvestorByPage(params);
		PageInfo<InvestorData> pageInfo = new PageInfo<>(page.getPageNum(), page.getPageSize(), page.getTotal(),
				page.getResult());
		PageHelper.clearPage();
		return pageInfo;
	}

	@Override
	public InvestorData getInvestorDetail(Long investorId, String userId) {
		if (investorId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "investor id miss");
		}

		InvestorData investor = investorMapper.selectInvestorByInvestorId(investorId);
		if (investor == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "investor not exist");
		}
		
		investor.setIsCollect(false);
		if(StringUtils.isNotEmpty(userId)) {
			UserFavorite favorite = userFavoriteMapper.selectByIdAndType(userId, investorId, UserFavoriteType.INVESTOR.getType());
			if(favorite != null) {
				investor.setIsCollect(true);
			}
		}

		List<Catalog> catalogs = investorCatalogMapper.selectInvestorCatalogByInvestor(investorId);
		if (CollectionUtils.isNotEmpty(catalogs)) {
			List<String> catalogName = new ArrayList<String>();
			for (Catalog catalog : catalogs) {
				catalogName.add(catalog.getName());
			}
			investor.setCatalogName(catalogName);
		}

		List<InvestorStage> stages = investorStageMapper.selectInvestorStageByInvestor(investorId);
		if (CollectionUtils.isNotEmpty(stages)) {
			List<String> stageName = new ArrayList<String>();
			for (InvestorStage stage : stages) {
				stageName.add(stage.getStageName());
			}
			investor.setStageName(stageName);
		}

		List<InvestorCity> cities = investorCityMapper.selectInvestorCityByInvestor(investorId);
		if (CollectionUtils.isNotEmpty(cities)) {
			List<String> cityName = new ArrayList<>();
			for (InvestorCity city : cities) {
				cityName.add(city.getCityName());
			}
			investor.setCityName(cityName);
		}
		return investor;
	}
	
	@Override
	public InvestorPartyData getInvestorPartyDetail(Long investorId) {
		if (investorId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "investor id miss");
		}
		Investor investor = investorMapper.selectByPrimaryKey(investorId);
		if (investor == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "investor not exist");
		}
		
		InvestorPartyData data = new InvestorPartyData();
		data.setInvestorId(investorId);
		data.setContact(investor.getContact());
		data.setContent(investor.getContent());
		data.setLanguage(investor.getLanguage());
		data.setMaxInvestAmount(investor.getMaxInvestAmount());
		data.setMinInvestAmount(investor.getMinInvestAmount());
		data.setName(investor.getName());
		data.setPosition(investor.getPosition());
		data.setShotDesc(investor.getShotDesc());
		
		Institution institution = institutionMapper.selectInstitutionByInvestorId(investorId);
		data.setInstitutionId(institution.getInstitutionId());
		data.setInstitutionName(institution.getName());
		
		PartyRoleRel rel = partyRoleRelMapper.selectByPrimaryKey(investor.getPartyRoleRelId());
		PartyRole role = partyRoleMapper.selectByPrimaryKey(rel.getPartyRoleId());
		data.setPartyRoleId(role.getPartyRoleId());
		data.setPartyRoleName(role.getPartyRoleName());
		
		Party party = partyMapper.selectByPrimaryKey(rel.getPartyId());
		data.setUserId(party.getUserId());
		
		PartyInfo partyInfo = partyInfoMapper.selectByPrimaryKey(party.getPartyId());
		data.setBirthday(partyInfo.getBirthday());
		data.setCityCode(partyInfo.getCityCode());
		data.setCityName(partyInfo.getCityName());
		data.setCompanyId(partyInfo.getCompanyId());
		data.setCompanyName(partyInfo.getCompanyName());
		data.setEmail(partyInfo.getEmail());
		data.setEnName(partyInfo.getEnName());
		data.setIdcardCode(partyInfo.getIdcardCode());
		data.setIdcardType(partyInfo.getIdcardType());
		data.setPartyName(partyInfo.getName());
		data.setNationality(partyInfo.getNationality());
		data.setPhone(partyInfo.getPhone());
		data.setPhoto(partyInfo.getPhoto());
		data.setSex(partyInfo.getSex());
		
		List<InvestorCatalogData> catalogDatas = investorCatalogMapper.selectInvestorCatalog(investorId);
		data.setCatalogDatas(catalogDatas);
		
		List<InvestorStage> stageDatas = investorStageMapper.selectInvestorStageByInvestor(investorId);
		if(CollectionUtils.isNotEmpty(stageDatas)) {
			List<StageData> _stageDatas = new ArrayList<>();
			for (InvestorStage stage : stageDatas) {
				StageData stageData = new StageData();
				BeanUtils.copyProperties(stage, stageData);
				_stageDatas.add(stageData);
			}
			data.setStageDatas(_stageDatas);
		}
		
		List<InvestorCity> cityDatas = investorCityMapper.selectInvestorCityByInvestor(investorId);
		if(CollectionUtils.isNotEmpty(cityDatas)) {
			List<CityData> _cityDatas = new ArrayList<>();
			for (InvestorCity investorCity : cityDatas) {
				CityData cityData = new CityData();
				BeanUtils.copyProperties(investorCity, cityData);
				_cityDatas.add(cityData);
			}
			data.setCityDatas(_cityDatas);
		}
		
		List<TagData> tagDatas = tagRelMapper.selectTagDataByObjectId(investorId, TagClassType.INVESTOR.getType());
		data.setTagDatas(tagDatas);
		return data;
	}

	@Transactional
	@Override
	public InvestorData addInvestor(InvestorParams params) {
		
		Long institutionId = params.getInstitutionId();
		if (institutionId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "institution id miss");
		}
		String name = params.getName();
		if (StringUtils.isEmpty(name)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "investor name miss");
		}
		
		Long partyId = params.getPartyId();
		if (partyId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party id miss");
		}
		
		Long partyRoleId = params.getPartyRoleId();
		if (partyRoleId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party role id miss");
		}
		
		String creator = params.getCreator();

		Party party = partyMapper.selectByPrimaryKey(partyId);
		if(party == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party not exist");
		}
		
		PartyRoleRel roleRel = partyRoleRelMapper.selectPartyRoleRelByPartyIdAndPartyRoleId(partyId, partyRoleId);
		Long partyRoleRelId = null;
		if(roleRel == null) {
			roleRel = new PartyRoleRel();
			roleRel.setPartyId(partyId);
			roleRel.setPartyRoleId(1L);
			partyRoleRelMapper.insertPartyRoleRelSelective(roleRel);
		}
		
		partyRoleRelId = roleRel.getPartyRoleRelId();
		
		Investor investor = new Investor();
		investor.setContact(params.getContent());
		investor.setContent(params.getContent());
		investor.setCreateTime(new Date());
		investor.setCreator(creator);
		investor.setLanguage(params.getLanguage());
		investor.setMaxInvestAmount(params.getMaxInvestAmount());
		investor.setMinInvestAmount(params.getMinInvestAmount());
		investor.setName(name);
		investor.setPartyRoleRelId(partyRoleRelId);
		investor.setPosition(params.getPosition());
		investor.setShotDesc(params.getShotDesc());
		investor.setUpdateTime(new Date());
		investorMapper.insertInvestorSelective(investor);
		Long investorId = investor.getInvestorId();

		InstitutionInvestorRel institutionInvestorRel = new InstitutionInvestorRel();
		institutionInvestorRel.setInstitutionId(institutionId);
		institutionInvestorRel.setInvestorId(investorId);
		institutionInvestorRelMapper.insertSelective(institutionInvestorRel);

		List<String> cityDatas = params.getCityDatas();
		if (CollectionUtils.isNotEmpty(cityDatas)) {
			for (String cityCode : cityDatas) {
				InvestorCity investorCity = new InvestorCity();
				
				City city = cityMapper.selectByPrimaryKey(cityCode);
				if(city == null) {
					continue;
				}
				
				investorCity.setCityCode(cityCode);
				investorCity.setCityName(city.getCityName());
				investorCity.setInvestorId(investorId);
				investorCityMapper.insertSelective(investorCity);
			}
		}

		List<Byte> stageDatas = params.getStageDatas();
		if (CollectionUtils.isNotEmpty(stageDatas)) {
			for (Byte stageData : stageDatas) {
				InvestorStage investorStage = new InvestorStage();
				investorStage.setInvestorId(investorId);
				
				Stage stage = stageMapper.selectByPrimaryKey(stageData);
				if(stage == null) {
					continue;
				}
				investorStage.setStageName(stage.getStageName());
				investorStage.setStageType(stageData);
				investorStageMapper.insertSelective(investorStage);
			}
		}

		List<Long> catalogIdList = params.getCatalogIdList();
		if (CollectionUtils.isNotEmpty(catalogIdList)) {
			for (Long catalogId : catalogIdList) {
				InvestorCatalog investorCatalog = new InvestorCatalog();
				investorCatalog.setCatalogId(catalogId);
				investorCatalog.setInvestorId(investorId);
				investorCatalogMapper.insertSelective(investorCatalog);
			}
		}
		
		List<Long> tagIds = params.getTagIds();
		if(CollectionUtils.isNotEmpty(tagIds)) {
			for (Long tagId : tagIds) {
				TagRel rel = new TagRel();
				rel.setRelObjectId(investorId);
				rel.setRelObjectType(TagClassType.PROJECT.getType());
				rel.setTagId(tagId);
				tagRelMapper.insertSelective(rel);
			}
		}

		InvestorData data = new InvestorData();
		BeanUtils.copyProperties(investor, data);

		return data;
	}

	@Transactional
	@Override
	public InvestorData updateInvestor(InvestorParams params) {
		Long investorId = params.getInvestorId();
		if (investorId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "investor id miss");
		}
		Investor investor = investorMapper.selectByPrimaryKey(investorId);
		if (investor == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "investor not exist");
		}
		/*PartyRoleRel partyRoleRel = partyRoleRelMapper.selectByPrimaryKey(investor.getPartyRoleRelId());
		partyRoleRel.setPartyRoleId(params.getPartyRoleId());
		partyRoleRelMapper.updateByPrimaryKeySelective(partyRoleRel);
		
		Long partyId = partyRoleRel.getPartyId();

		Party party = new Party();
		party.setPartyId(partyId);
		party.setPartyId(partyId);
		party.setPartyName(params.getPartyName());
		party.setUpdateTime(new Date());
		partyMapper.updateByPrimaryKeySelective(party);

		PartyInfo partyInfo = new PartyInfo();
		partyInfo.setBirthday(params.getBirthday());
		partyInfo.setCityCode(params.getCityCode());
		partyInfo.setCityName(params.getCityName());
		partyInfo.setCompanyId(params.getCompanyId());
		partyInfo.setCompanyName(params.getCompanyName());
		partyInfo.setEmail(params.getEmail());
		partyInfo.setEnName(params.getEnName());
		partyInfo.setIdcardCode(params.getIdcardCode());
		partyInfo.setIdcardType(params.getIdcardType());
		partyInfo.setName(params.getPartyName());
		partyInfo.setNationality(params.getNationality());
		partyInfo.setPartyId(partyId);
		partyInfo.setPhone(params.getPhone());
		partyInfo.setPhoto(params.getPhoto());
		partyInfo.setSex(params.getSex());
		partyInfo.setUpdateTime(new Date());
		partyInfoMapper.updateByPrimaryKeySelective(partyInfo);*/

		if(params.getInstitutionId() != null) {
			InstitutionInvestorRel institutionInvestorRel = institutionInvestorRelMapper.selectByInvestorId(investorId);
			institutionInvestorRel.setInstitutionId(params.getInstitutionId());
			institutionInvestorRelMapper.updateByPrimaryKeySelective(institutionInvestorRel);
		}
		
		investor = new Investor();
		investor.setContact(params.getContact());
		investor.setContent(params.getContent());
		investor.setInvestorId(investorId);
		investor.setLanguage(params.getLanguage());
		investor.setMaxInvestAmount(params.getMaxInvestAmount());
		investor.setMinInvestAmount(params.getMinInvestAmount());
		investor.setName(params.getName());
		investor.setPosition(params.getPosition());
		investor.setShotDesc(params.getShotDesc());
		investor.setStatus(params.getStatus());
		investor.setUpdateTime(new Date());
		investorMapper.updateByPrimaryKeySelective(investor);
		
		List<String> cityDatas = params.getCityDatas();
		if (CollectionUtils.isNotEmpty(cityDatas)) {
			investorCityMapper.deleteByInvestorId(investorId);
			for (String cityCode : cityDatas) {
				InvestorCity investorCity = new InvestorCity();

				City city = cityMapper.selectByPrimaryKey(cityCode);
				if(city == null) {
					continue;
				}
				
				investorCity.setCityCode(cityCode);
				investorCity.setCityName(city.getCityName());
				investorCity.setInvestorId(investorId);
				investorCityMapper.insertSelective(investorCity);
			}
		}

		List<Byte> stageDatas = params.getStageDatas();
		if (CollectionUtils.isNotEmpty(stageDatas)) {
			investorStageMapper.deleteByInvestorId(investorId);
			for (Byte stageData : stageDatas) {
				InvestorStage investorStage = new InvestorStage();
				investorStage.setInvestorId(investorId);
				Stage stage = stageMapper.selectByPrimaryKey(stageData);
				if(stage == null) {
					continue;
				}
				investorStage.setStageName(stage.getStageName());
				investorStage.setStageType(stageData);
				investorStageMapper.insertSelective(investorStage);
			}
		}

		List<Long> catalogIdList = params.getCatalogIdList();
		if (CollectionUtils.isNotEmpty(catalogIdList)) {
			investorCatalogMapper.deleteByInvestorId(investorId);
			for (Long catalogId : catalogIdList) {
				InvestorCatalog investorCatalog = new InvestorCatalog();
				investorCatalog.setCatalogId(catalogId);
				investorCatalog.setInvestorId(investorId);
				investorCatalogMapper.insertSelective(investorCatalog);
			}
		}
		
		List<Long> tagIds = params.getTagIds();
		if(CollectionUtils.isNotEmpty(tagIds)) {
			tagRelMapper.deleteTagRelByObjectId(investorId, TagClassType.INVESTOR.getType());
			for (Long tagId : tagIds) {
				TagRel rel = new TagRel();
				rel.setRelObjectId(investorId);
				rel.setRelObjectType(TagClassType.INVESTOR.getType());
				rel.setTagId(tagId);
				tagRelMapper.insertSelective(rel);
			}
		}
		
		InvestorData data = new InvestorData();
		BeanUtils.copyProperties(investor, data);

		return data;
	}

	@Transactional
	@Override
	public void deleteInvestor(Long investorId) {
		if(investorId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "investor id miss");
		}
		
		Investor investor = new Investor();
		investor.setInvestorId(investorId);
		investor.setStatus(InvestorStatus.DELETE.getStatus());
		investorMapper.updateByPrimaryKeySelective(investor);
	}

}
