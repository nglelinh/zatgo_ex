package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.CompanyExample;
import com.zatgo.zup.suanli.entity.CompanyExampleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CompanyExampleMapper {
    int countByExample(CompanyExampleExample example);

    int deleteByExample(CompanyExampleExample example);

    int deleteByPrimaryKey(String exampleId);

    int insert(CompanyExample record);

    int insertSelective(CompanyExample record);

    List<CompanyExample> selectByExample(CompanyExampleExample example);

    CompanyExample selectByPrimaryKey(String exampleId);

    int updateByExampleSelective(@Param("record") CompanyExample record, @Param("example") CompanyExampleExample example);

    int updateByExample(@Param("record") CompanyExample record, @Param("example") CompanyExampleExample example);

    int updateByPrimaryKeySelective(CompanyExample record);

    int updateByPrimaryKey(CompanyExample record);
}