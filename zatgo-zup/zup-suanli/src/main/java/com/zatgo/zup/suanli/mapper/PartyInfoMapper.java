package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.PartyInfo;

public interface PartyInfoMapper {
    int deleteByPrimaryKey(Long partyId);

    int insert(PartyInfo record);

    int insertSelective(PartyInfo record);

    PartyInfo selectByPrimaryKey(Long partyId);

    int updateByPrimaryKeySelective(PartyInfo record);

    int updateByPrimaryKey(PartyInfo record);
}