package com.zatgo.zup.suanli.entity;

public class UserFavorite {
    private Long id;

    private String userId;

    private Byte relObjectType;

    private Long relObjectId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Byte getRelObjectType() {
        return relObjectType;
    }

    public void setRelObjectType(Byte relObjectType) {
        this.relObjectType = relObjectType;
    }

    public Long getRelObjectId() {
        return relObjectId;
    }

    public void setRelObjectId(Long relObjectId) {
        this.relObjectId = relObjectId;
    }
}