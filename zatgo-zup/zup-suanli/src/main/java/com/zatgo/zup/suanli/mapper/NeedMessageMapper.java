package com.zatgo.zup.suanli.mapper;

import java.util.List;

import com.zatgo.zup.suanli.entity.NeedMessage;
import com.zatgo.zup.suanli.entity.NeedMessageExample;
import org.apache.ibatis.annotations.Param;

public interface NeedMessageMapper {
    int countByExample(NeedMessageExample example);

    int deleteByExample(NeedMessageExample example);

    int deleteByPrimaryKey(String id);

    int insert(NeedMessage record);

    int insertSelective(NeedMessage record);

    List<NeedMessage> selectByExample(NeedMessageExample example);

    NeedMessage selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") NeedMessage record, @Param("example") NeedMessageExample example);

    int updateByExample(@Param("record") NeedMessage record, @Param("example") NeedMessageExample example);

    int updateByPrimaryKeySelective(NeedMessage record);

    int updateByPrimaryKey(NeedMessage record);
}