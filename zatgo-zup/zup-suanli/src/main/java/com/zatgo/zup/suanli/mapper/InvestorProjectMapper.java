package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.InvestorProject;

public interface InvestorProjectMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InvestorProject record);

    int insertSelective(InvestorProject record);

    InvestorProject selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(InvestorProject record);

    int updateByPrimaryKey(InvestorProject record);
}