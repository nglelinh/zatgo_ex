package com.zatgo.zup.suanli.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("投资人查询参数")
public class InvestorSearchParams {
	
	@ApiModelProperty(value = "页码", required = false)
	private Integer pageNo;
	
	@ApiModelProperty(value = "数量", required = false)
	private Integer pageSize;
	
	@ApiModelProperty(value = "投资人姓名", required = false)
	private String name;
	
	@ApiModelProperty(value = "最小投资额度，单位：万", required = false)
	private BigDecimal minInvestAmount;
	
	@ApiModelProperty(value = "最大投资额度，单位：万", required = false)
	private BigDecimal maxInvestAmount;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = false)
	private Byte language;
	
	@ApiModelProperty(value = "状态：0-已删除；1-使用中", required = false, hidden = true)
	private Byte status;

	public Integer getPageNo() {
		return pageNo == null ? 1 : pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize == null ? 10 : pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getMinInvestAmount() {
		return minInvestAmount;
	}

	public void setMinInvestAmount(BigDecimal minInvestAmount) {
		this.minInvestAmount = minInvestAmount;
	}

	public BigDecimal getMaxInvestAmount() {
		return maxInvestAmount;
	}

	public void setMaxInvestAmount(BigDecimal maxInvestAmount) {
		this.maxInvestAmount = maxInvestAmount;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

}
