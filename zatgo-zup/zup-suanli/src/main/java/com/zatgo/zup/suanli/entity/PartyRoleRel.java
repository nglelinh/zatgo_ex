package com.zatgo.zup.suanli.entity;

public class PartyRoleRel {
    private Long partyRoleRelId;

    private Long partyId;

    private Long partyRoleId;

    public Long getPartyRoleRelId() {
        return partyRoleRelId;
    }

    public void setPartyRoleRelId(Long partyRoleRelId) {
        this.partyRoleRelId = partyRoleRelId;
    }

    public Long getPartyId() {
        return partyId;
    }

    public void setPartyId(Long partyId) {
        this.partyId = partyId;
    }

    public Long getPartyRoleId() {
        return partyRoleId;
    }

    public void setPartyRoleId(Long partyRoleId) {
        this.partyRoleId = partyRoleId;
    }
}