package com.zatgo.zup.suanli.mapper;

import java.util.List;

import com.zatgo.zup.suanli.entity.CampaignCatalog;

public interface CampaignCatalogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CampaignCatalog record);

    int insertSelective(CampaignCatalog record);

    CampaignCatalog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CampaignCatalog record);

    int updateByPrimaryKey(CampaignCatalog record);
    
    int deleteByCampaignId(Long campaignId);
    
    List<Long> selectCampaignCatalogByCampaignId(Long campaignId);
}