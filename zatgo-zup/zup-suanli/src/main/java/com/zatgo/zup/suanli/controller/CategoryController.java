package com.zatgo.zup.suanli.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.CatalogBaseData;
import com.zatgo.zup.suanli.model.CatalogData;
import com.zatgo.zup.suanli.model.CatalogParams;
import com.zatgo.zup.suanli.model.CatalogSearchParams;
import com.zatgo.zup.suanli.model.CityData;
import com.zatgo.zup.suanli.model.StageData;
import com.zatgo.zup.suanli.model.StageSearchParams;
import com.zatgo.zup.suanli.service.CategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/category")
public class CategoryController extends BaseController {
	
	@Autowired
	private CategoryService categoryService;
	
	@ApiOperation(value = "城市列表")
	@RequestMapping(value = "/city/list", method = RequestMethod.GET)
	public ResponseData<List<CityData>> getCityList(){
		List<CityData> list = categoryService.getCityList();
		return BusinessResponseFactory.createSuccess(list);
	}
	
	@ApiOperation(value = "目录列表")
	@RequestMapping(value = "/catalog/list", method = RequestMethod.GET)
	public ResponseData<List<CatalogData>> getCatalogList(@ModelAttribute CatalogSearchParams params) {
		List<CatalogData> list = categoryService.getCatalogList(params);
		return BusinessResponseFactory.createSuccess(list);
	}
	
	@ApiOperation(value = "分类基础列表")
	@RequestMapping(value = "/catalog/base/list", method = RequestMethod.GET)
	public ResponseData<List<CatalogBaseData>> getCatalogBaseList(@ModelAttribute CatalogSearchParams params) {
		List<CatalogBaseData> list = categoryService.getCatalogBaseList(params);
		return BusinessResponseFactory.createSuccess(list);
	}
	
	@ApiOperation(value = "阶段列表")
	@RequestMapping(value = "/stage/list", method = RequestMethod.GET)
	public ResponseData<List<StageData>> getStageList(@ModelAttribute StageSearchParams params){
		List<StageData> list = categoryService.getStageList(params);
		return BusinessResponseFactory.createSuccess(list);
	}
	
	@ApiOperation(value = "新增分类")
	@RequestMapping(value = "/add/catalog", method = RequestMethod.POST)
	public ResponseData<CatalogData> addCatalog(@RequestBody CatalogParams params){
		String userName = getAdminUserInfo().getAdminName();
		params.setCreator(userName);
		CatalogData data = categoryService.addCatalog(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "修改分类")
	@RequestMapping(value = "/update/catalog", method = RequestMethod.POST)
	public ResponseData<CatalogData> updateCatalog(@RequestBody CatalogParams params){
		getAdminUserInfo().getAdminName();
		CatalogData data = categoryService.updateCatalog(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "删除分类")
	@RequestMapping(value = "/delete/catalog/{catalogId}", method = RequestMethod.DELETE)
	public ResponseData<Object> deleteCatalog(@PathVariable Long catalogId) {
		getAdminUserInfo();
		categoryService.deleteCatalog(catalogId);
		return BusinessResponseFactory.createSuccess(null);
	}

}
