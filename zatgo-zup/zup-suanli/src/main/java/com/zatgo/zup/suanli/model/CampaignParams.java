package com.zatgo.zup.suanli.model;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("增改活动参数")
public class CampaignParams {

	@ApiModelProperty(value = "活动id，修改使用", required = false)
	private Long campaignId;
	
	@ApiModelProperty(value = "活动名称", required = false)
	private String name;
	
	@ApiModelProperty(value = "活动开始时间", required = false)
	private Date campaignBegin;
	
	@ApiModelProperty(value = "活动结束时间", required = false)
	private Date campaignEnd;
	
	@ApiModelProperty(value = "活动地址", required = false)
	private String locale;
	
	@ApiModelProperty(value = "城市编码", required = false)
	private String cityCode;
	
	@ApiModelProperty(value = "城市名称", required = false)
	private String cityName;
	
	@ApiModelProperty(value = "创建者", required = false, hidden = true)
	private String creator;
	
	@ApiModelProperty(value = "活动分类", required = false)
	private List<Long> catalogIds;
	
	@ApiModelProperty(value = "公司id", required = false)
	private Long companyId;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = true)
	private Byte language;
	
	@ApiModelProperty(value = "活动详情", required = false)
	private String details;
	
	@ApiModelProperty(value = "活动logo", required = false)
	private String logo;
	
	@ApiModelProperty(value = "活动图片", required = false)
	private String img_url;

	public Long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCampaignBegin() {
		return campaignBegin;
	}

	public void setCampaignBegin(Date campaignBegin) {
		this.campaignBegin = campaignBegin;
	}

	public Date getCampaignEnd() {
		return campaignEnd;
	}

	public void setCampaignEnd(Date campaignEnd) {
		this.campaignEnd = campaignEnd;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public List<Long> getCatalogIds() {
		return catalogIds;
	}

	public void setCatalogIds(List<Long> catalogIds) {
		this.catalogIds = catalogIds;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

}
