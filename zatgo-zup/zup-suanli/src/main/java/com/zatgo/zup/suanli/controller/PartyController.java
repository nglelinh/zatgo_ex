package com.zatgo.zup.suanli.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.PartyData;
import com.zatgo.zup.suanli.model.PartyDetailData;
import com.zatgo.zup.suanli.model.PartyParams;
import com.zatgo.zup.suanli.model.PartySerchParams;
import com.zatgo.zup.suanli.service.PartyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/party")
public class PartyController extends BaseController {

	@Autowired
	private PartyService partyService;

	@ApiOperation(value = "参与人列表")
	@RequestMapping(value = "/list", method = RequestMethod.GET)

	public ResponseData<PageInfo<PartyData>> getPartyList(@ModelAttribute PartySerchParams params) {
		getAdminUserInfo();

		PageInfo<PartyData> info = partyService.getPartyList(params);
		return BusinessResponseFactory.createSuccess(info);
	}

	@ApiOperation(value = "添加参与人")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseData<Object> addParty(@RequestBody PartyParams params) {
		params.setCreator(getAdminUserInfo().getAdminName());
		partyService.addParty(params);
		return BusinessResponseFactory.createSuccess(null);
	}

	@ApiOperation(value = "修改参与人")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseData<Object> updateParty(@RequestBody PartyParams params) {
		params.setCreator(getAdminUserInfo().getAdminName());
		partyService.updateParty(params);
		return BusinessResponseFactory.createSuccess(null);
	}

	@ApiOperation(value = "参与人详情")
	@RequestMapping(value = "/detail/{partyId}", method = RequestMethod.GET)
	public ResponseData<PartyDetailData> getPartyDetail(@PathVariable Long partyId) {
		getAdminUserInfo();
		PartyDetailData data = partyService.getPartyDetail(partyId);
		return BusinessResponseFactory.createSuccess(data);
	}

}
