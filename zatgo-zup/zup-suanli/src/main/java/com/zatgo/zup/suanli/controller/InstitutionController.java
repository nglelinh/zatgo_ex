package com.zatgo.zup.suanli.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.InstitutionData;
import com.zatgo.zup.suanli.model.InstitutionParams;
import com.zatgo.zup.suanli.model.InstitutionSearchParams;
import com.zatgo.zup.suanli.service.InstitutionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(value = "/suanli/institution")
public class InstitutionController extends BaseController {
	
	@Autowired
	private InstitutionService institutionService;
	
	@ApiOperation(value = "查询投资机构")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseData<PageInfo<InstitutionData>> getInstitutionList(@ModelAttribute InstitutionSearchParams params){
		params.setLanguage(getLanguage());
		PageInfo<InstitutionData> info = institutionService.getInstitutionList(params);
		return BusinessResponseFactory.createSuccess(info);
	}
	
	@ApiOperation(value = "新增投资机构")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseData<InstitutionData> addInstitution(@RequestBody InstitutionParams params){
		String userName = getAdminUserInfo().getAdminName();
		params.setCreator(userName);
		InstitutionData data = institutionService.addInstitution(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "修改投资机构")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseData<InstitutionData> updateInstitution(@RequestBody InstitutionParams params){
		getAdminUserInfo();
		InstitutionData data = institutionService.updateInstitution(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "删除投资机构")
	@RequestMapping(value = "/delete/{institutionId}", method = RequestMethod.DELETE)
	public ResponseData<Object> deleteInstitution(@PathVariable Long institutionId){
		getAdminUserInfo();
		institutionService.deleteInstitution(institutionId);
		return BusinessResponseFactory.createSuccess(null);
	}

}
