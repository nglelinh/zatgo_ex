package com.zatgo.zup.suanli.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.suanli.entity.CompanyExample;
import com.zatgo.zup.suanli.entity.CompanyExampleExample;
import com.zatgo.zup.suanli.mapper.CompanyExampleMapper;
import com.zatgo.zup.suanli.service.CompanyExamplService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/9/3.
 */

@Service("companyExamplService")
public class CompanyExamplServiceImple implements CompanyExamplService {

    @Autowired
    private CompanyExampleMapper companyExampleMapper;


    @Override
    public CompanyExample detail(String id) {
        return companyExampleMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageInfo<CompanyExample> frontList(Integer pageSzie, Integer pageNo) {
        CompanyExampleExample example = new CompanyExampleExample();
        example.setOrderByClause("sort desc");
        PageHelper.startPage(pageNo, pageSzie);
        List<CompanyExample> companyExamples = companyExampleMapper.selectByExample(example);
        return new PageInfo<>(companyExamples);
    }

    @Override
    public void add(CompanyExample companyExample) {
        companyExample.setExampleId(UUIDUtils.getUuid());
        companyExample.setCreateTime(new Date());
        companyExampleMapper.insertSelective(companyExample);
    }

    @Override
    public void del(String id) {
        companyExampleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void edit(CompanyExample companyExample) {
        companyExampleMapper.updateByPrimaryKeySelective(companyExample);
    }

    @Override
    public PageInfo<CompanyExample> backList(Integer pageSzie, Integer pageNo) {
        CompanyExampleExample example = new CompanyExampleExample();
        example.setOrderByClause("create_time desc");
        PageHelper.startPage(pageNo, pageSzie);
        List<CompanyExample> companyExamples = companyExampleMapper.selectByExample(example);
        return new PageInfo<>(companyExamples);
    }
}
