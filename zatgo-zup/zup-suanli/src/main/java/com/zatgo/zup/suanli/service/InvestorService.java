package com.zatgo.zup.suanli.service;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.model.InvestorData;
import com.zatgo.zup.suanli.model.InvestorParams;
import com.zatgo.zup.suanli.model.InvestorPartyData;
import com.zatgo.zup.suanli.model.InvestorSearchParams;

public interface InvestorService {
	
	/**
	 * 查询投资人列表
	 * @param params
	 * @return
	 */
	public PageInfo<InvestorData> getInvestorList(InvestorSearchParams params);
	
	/**
	 * 查询投资人详情
	 * @param investorId
	 * @return
	 */
	public InvestorData getInvestorDetail(Long investorId, String userId);
	
	/**
	 * 查询投资人详情
	 * @param investorId
	 * @return
	 */
	public InvestorPartyData getInvestorPartyDetail(Long investorId);
	
	/**
	 * 新增投资人
	 * @param params
	 * @return
	 */
	public InvestorData addInvestor(InvestorParams params);
	
	/**
	 * 修改投资人
	 * @param params
	 * @return
	 */
	public InvestorData updateInvestor(InvestorParams params);
	
	/**
	 * 删除投资人
	 * @param params
	 * @return
	 */
	public void deleteInvestor(Long investorId);

}
