package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("我的关注参数")
public class FavoriteParams {
	
	@ApiModelProperty(name = "页码", required = true)
	private Integer pageNo;
	
	@ApiModelProperty(name = "数量", required = true)
	private Integer pageSize;
	
	@ApiModelProperty(name = "用户id", required = false, hidden = true)
	private String userId;

	public Integer getPageNo() {
		return pageNo == null ? 1 : pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize == null ? 10 : pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
