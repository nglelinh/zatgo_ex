package com.zatgo.zup.suanli.model;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("活动")
public class CampaignData {
	
	@ApiModelProperty(value = "活动id", required = true)
	private Long campaignId;
	
	@ApiModelProperty(value = "活动名称", required = true)
	private String name;
	
	@ApiModelProperty(value = "活动开始时间", required = true)
	private Date campaignBegin;
	
	@ApiModelProperty(value = "活动结束时间", required = true)
	private Date campaignEnd;
	
	@ApiModelProperty(value = "活动地址", required = true)
	private String locale;
	
	@ApiModelProperty(value = "城市编码", required = true)
	private String cityCode;
	
	@ApiModelProperty(value = "城市名称", required = true)
	private String cityName;
	
	@ApiModelProperty(value = "公司id", required = true)
	private Long companyId;
	
	@ApiModelProperty(value = "举办方", required = true)
	private String companyName;
	
	@ApiModelProperty(value = "图片", required = false)
	private String imgUrl;
	
	@ApiModelProperty(value = "详情", required = false)
	private String details;
	
	@ApiModelProperty(value = "是否收藏", required = false)
	private Boolean isCollect;
	
	@ApiModelProperty(value = "是否报名", required = false)
	private Boolean isRegister;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = false)
	private Byte language;
	
	@ApiModelProperty(value = "活动logo", required = false)
	private String logo;
	
	@ApiModelProperty(value = "分类id", required = false)
	private List<Long> catalogIds;

	public Long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCampaignBegin() {
		return campaignBegin;
	}

	public void setCampaignBegin(Date campaignBegin) {
		this.campaignBegin = campaignBegin;
	}

	public Date getCampaignEnd() {
		return campaignEnd;
	}

	public void setCampaignEnd(Date campaignEnd) {
		this.campaignEnd = campaignEnd;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Boolean getIsCollect() {
		return isCollect;
	}

	public void setIsCollect(Boolean isCollect) {
		this.isCollect = isCollect;
	}

	public Boolean getIsRegister() {
		return isRegister;
	}

	public void setIsRegister(Boolean isRegister) {
		this.isRegister = isRegister;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public List<Long> getCatalogIds() {
		return catalogIds;
	}

	public void setCatalogIds(List<Long> catalogIds) {
		this.catalogIds = catalogIds;
	}

}
