package com.zatgo.zup.suanli.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Investor {
    private Long investorId;

    private Long partyRoleRelId;

    private String name;

    private String shotDesc;

    private String content;

    private BigDecimal minInvestAmount;

    private BigDecimal maxInvestAmount;

    private Date createTime;

    private String creator;

    private Date updateTime;

    private String position;

    private String contact;

    private Byte language;

    private Byte status;

    private String imgUrl;

    private Integer harvest;

    private BigDecimal feedback;

    private BigDecimal interview;

    public Long getInvestorId() {
        return investorId;
    }

    public void setInvestorId(Long investorId) {
        this.investorId = investorId;
    }

    public Long getPartyRoleRelId() {
        return partyRoleRelId;
    }

    public void setPartyRoleRelId(Long partyRoleRelId) {
        this.partyRoleRelId = partyRoleRelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getShotDesc() {
        return shotDesc;
    }

    public void setShotDesc(String shotDesc) {
        this.shotDesc = shotDesc == null ? null : shotDesc.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public BigDecimal getMinInvestAmount() {
        return minInvestAmount;
    }

    public void setMinInvestAmount(BigDecimal minInvestAmount) {
        this.minInvestAmount = minInvestAmount;
    }

    public BigDecimal getMaxInvestAmount() {
        return maxInvestAmount;
    }

    public void setMaxInvestAmount(BigDecimal maxInvestAmount) {
        this.maxInvestAmount = maxInvestAmount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public Byte getLanguage() {
        return language;
    }

    public void setLanguage(Byte language) {
        this.language = language;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    public Integer getHarvest() {
        return harvest;
    }

    public void setHarvest(Integer harvest) {
        this.harvest = harvest;
    }

    public BigDecimal getFeedback() {
        return feedback;
    }

    public void setFeedback(BigDecimal feedback) {
        this.feedback = feedback;
    }

    public BigDecimal getInterview() {
        return interview;
    }

    public void setInterview(BigDecimal interview) {
        this.interview = interview;
    }
}