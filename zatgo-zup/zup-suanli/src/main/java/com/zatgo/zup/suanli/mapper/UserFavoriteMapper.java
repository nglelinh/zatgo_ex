package com.zatgo.zup.suanli.mapper;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.zatgo.zup.suanli.entity.UserFavorite;
import com.zatgo.zup.suanli.model.InvestorData;
import com.zatgo.zup.suanli.model.ProjectData;
import com.zatgo.zup.suanli.model.UserFavoriteParams;

public interface UserFavoriteMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserFavorite record);

    int insertSelective(UserFavorite record);

    UserFavorite selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserFavorite record);

    int updateByPrimaryKey(UserFavorite record);
    
    int insertUserFavoriteSelective(UserFavorite record);
    
    UserFavorite selectByIdAndType(@Param("userId") String userId, @Param("objectId") Long objectId, @Param("objectType") Byte objectType);
    
    int deleteByParams(UserFavoriteParams params);
    
    Page<ProjectData> selectFavoriteProjectList(@Param("favoriteType") Byte favoriteType, @Param("userId") String userId);
    
    Page<InvestorData> selectFavoriteInvestorList(@Param("favoriteType") Byte favoriteType, @Param("userId") String userId);
    
    int countFollowProjectAndInvestor(String userId);
}