package com.zatgo.zup.suanli.model;

import com.zatgo.zup.common.enumtype.BusinessEnum.OssBucket;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("媒体库")
public class FileLibraryParams {
	
	@ApiModelProperty(value = "上传空间名称： 活动-campaign；项目-project；投资人-investor；投资机构-institution；资讯-information；用户-user", required = true, allowableValues = "campaign,project,investor,institution")
	private OssBucket ossBucket;
	
	@ApiModelProperty(value = "分页起始参数，每次查询后由后台返回", required = false)
	private String nextMarker;
	
	@ApiModelProperty(value = "分页大小，最大1000", required = true)
	private Integer size;

	public OssBucket getOssBucket() {
		return ossBucket;
	}

	public void setOssBucket(String ossBucket) {
		this.ossBucket = OssBucket.getEnumByType(ossBucket);
	}

	public String getNextMarker() {
		return nextMarker;
	}

	public void setNextMarker(String nextMarker) {
		this.nextMarker = nextMarker;
	}

	public Integer getSize() {
		return size == null ? 10 : size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

}
