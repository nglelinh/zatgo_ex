package com.zatgo.zup.suanli.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.TagData;
import com.zatgo.zup.suanli.model.TagParams;
import com.zatgo.zup.suanli.model.TagSearchParams;
import com.zatgo.zup.suanli.service.TagService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/tag")
public class TagController extends BaseController {
	
	@Autowired
	private TagService tagService;
	
	@ApiOperation(value = "标签列表")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseData<PageInfo<TagData>> getTagList(@ModelAttribute TagSearchParams params){
		PageInfo<TagData> info = tagService.getTagList(params);
		return BusinessResponseFactory.createSuccess(info);
	}
	
	@ApiOperation(value = "新增标签")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseData<TagData> addTag(@RequestBody TagParams params){
		params.setCreator(getAdminUserInfo().getAdminName());
		TagData data = tagService.addTag(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "修改标签")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseData<TagData> updateTag(@RequestBody TagParams params){
		getAdminUserInfo().getAdminName();
		TagData data = tagService.updateTag(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "删除标签")
	@RequestMapping(value = "/delete/{tagId}", method = RequestMethod.DELETE)
	public ResponseData<Object> deleteTag(@PathVariable Long tagId){
		getAdminUserInfo();
		tagService.deleteTag(tagId);
		return BusinessResponseFactory.createSuccess(null);
	}

}
