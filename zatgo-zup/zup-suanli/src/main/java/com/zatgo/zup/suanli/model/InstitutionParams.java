package com.zatgo.zup.suanli.model;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("增改投资机构参数")
public class InstitutionParams {
	
	@ApiModelProperty(value = "投资机构id， 修改使用", required = false)
	private Long institutionId;
	
	@ApiModelProperty(value = "投资机构名称", required = true)
	private String name;
	
	@ApiModelProperty(value = "投资机构编号", required = false)
	private String code;
	
	@ApiModelProperty(value = "投资机构简介", required = false)
	private String shotDesc;
	
	@ApiModelProperty(value = "投资机构详细介绍", required = false)
	private String content;
	
	@ApiModelProperty(value = "创建者", required = false, hidden = true)
	private String creator;
	
	@ApiModelProperty(value = "投资机构LOGO", required = false)
	private String logo;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = false, allowableValues = "0,1")
	private Byte language;
	
	@ApiModelProperty(value = "标签id", required = false)
	private List<Long> tagIds;

	public Long getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Long institutionId) {
		this.institutionId = institutionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getShotDesc() {
		return shotDesc;
	}

	public void setShotDesc(String shotDesc) {
		this.shotDesc = shotDesc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public List<Long> getTagIds() {
		return tagIds;
	}

	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}

}
