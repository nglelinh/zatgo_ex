package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("企业查询参数")
public class CompanySearchParams {
	
	@ApiModelProperty(value = "页数", required = true)
	private Integer pageNo;
	
	@ApiModelProperty(value = "数量", required = true)
	private Integer pageSize;
	
	@ApiModelProperty(value = "企业名称", required = false)
	private String name;
	
	@ApiModelProperty(value = "状态：0-已删除；1-使用中", required = false, hidden = true)
	private Byte status;

	public Integer getPageNo() {
		return pageNo == null ? 1 : pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize == null ? 10 : pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

}
