package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("阶段数据")
public class StageData {
	
	@ApiModelProperty(value = "阶段类型：0:不限;1:种子轮;2:天使轮;3:Pre-A轮;4:A轮;5:A+轮;6:Pre-B轮;7:B轮;8:B+轮", required = false)
	private Byte stageType;
	
	@ApiModelProperty(value = "阶段名称", required = false)
	private String stageName;

	public Byte getStageType() {
		return stageType;
	}

	public void setStageType(Byte stageType) {
		this.stageType = stageType;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

}
