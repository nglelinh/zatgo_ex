package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("阶段数据")
public class StageSearchParams {
	
	@ApiModelProperty(value = "阶段类型", required = false)
	private Byte stageType;

	public Byte getStageType() {
		return stageType;
	}

	public void setStageType(Byte stageType) {
		this.stageType = stageType;
	}

}
