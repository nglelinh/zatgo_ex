package com.zatgo.zup.suanli.mapper;

import com.github.pagehelper.Page;
import com.zatgo.zup.suanli.entity.Company;
import com.zatgo.zup.suanli.model.CompanyData;
import com.zatgo.zup.suanli.model.CompanySearchParams;

public interface CompanyMapper {
    int deleteByPrimaryKey(Long companyId);

    int insert(Company record);

    int insertSelective(Company record);

    Company selectByPrimaryKey(Long companyId);

    int updateByPrimaryKeySelective(Company record);

    int updateByPrimaryKey(Company record);
    
    Page<CompanyData> selectCompanyListByParams(CompanySearchParams params);
}