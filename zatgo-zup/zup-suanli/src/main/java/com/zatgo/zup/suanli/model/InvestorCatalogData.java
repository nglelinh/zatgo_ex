package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("关注分类")
public class InvestorCatalogData {
	
	@ApiModelProperty(value = "分类id", required = true)
	private Long catalogId;
	
	@ApiModelProperty(value = "分类名称", required = true)
	private String name;

	public Long getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
