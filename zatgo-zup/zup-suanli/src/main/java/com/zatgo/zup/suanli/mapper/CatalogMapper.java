package com.zatgo.zup.suanli.mapper;

import java.util.List;

import com.zatgo.zup.suanli.entity.Catalog;
import com.zatgo.zup.suanli.model.CatalogData;
import com.zatgo.zup.suanli.model.CatalogSearchParams;

public interface CatalogMapper {
    int deleteByPrimaryKey(Long catalogId);

    int insert(Catalog record);

    int insertSelective(Catalog record);

    Catalog selectByPrimaryKey(Long catalogId);

    int updateByPrimaryKeySelective(Catalog record);

    int updateByPrimaryKey(Catalog record);
    
    List<CatalogData> selectCatalogByParams(CatalogSearchParams params);
}