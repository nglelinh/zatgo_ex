package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("投资机构信息")
public class InstitutionData {
	
	@ApiModelProperty(value = "投资机构id", required = true)
	private Long institutionId;
	
	@ApiModelProperty(value = "投资机构名称", required = true)
	private String name;
	
	@ApiModelProperty(value = "投资机构编号", required = false)
	private String code;
	
	@ApiModelProperty(value = "投资机构简介", required = false)
	private String shotDesc;
	
	@ApiModelProperty(value = "投资机构详细介绍", required = false)
	private String content;

	public Long getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Long institutionId) {
		this.institutionId = institutionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getShotDesc() {
		return shotDesc;
	}

	public void setShotDesc(String shotDesc) {
		this.shotDesc = shotDesc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
