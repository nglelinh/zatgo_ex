package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("目录查询参数")
public class CatalogSearchParams {
	
//	@ApiModelProperty(value = "目录id", required = false)
//	private Long catalogId;
//	
//	@ApiModelProperty(value = "父目录id", required = false)
//	private Long parentCatalogId;
//	
	@ApiModelProperty(value = "目录类型 0：项目", required = false)
	private Byte catalogType;
	
	@ApiModelProperty(value = "分类状态：0-已删除；1-使用中", required = false, hidden = true)
	private Byte status;

	public Byte getCatalogType() {
		return catalogType;
	}

	public void setCatalogType(Byte catalogType) {
		this.catalogType = catalogType;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
//	@ApiModelProperty(value = "目录级别：0为根目录", required = false)
//	private Byte catalogLevel;
//
//	public Long getCatalogId() {
//		return catalogId;
//	}
//
//	public void setCatalogId(Long catalogId) {
//		this.catalogId = catalogId;
//	}
//
//	public Long getParentCatalogId() {
//		return parentCatalogId;
//	}
//
//	public void setParentCatalogId(Long parentCatalogId) {
//		this.parentCatalogId = parentCatalogId;
//	}
//
//	public Byte getCatalogType() {
//		return catalogType;
//	}
//
//	public void setCatalogType(Byte catalogType) {
//		this.catalogType = catalogType;
//	}
//
//	public Byte getCatalogLevel() {
//		return catalogLevel;
//	}
//
//	public void setCatalogLevel(Byte catalogLevel) {
//		this.catalogLevel = catalogLevel;
//	}

}
