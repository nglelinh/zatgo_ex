package com.zatgo.zup.suanli.service.impl;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.SuanliEnum.TagStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.entity.Tag;
import com.zatgo.zup.suanli.mapper.TagMapper;
import com.zatgo.zup.suanli.model.TagData;
import com.zatgo.zup.suanli.model.TagParams;
import com.zatgo.zup.suanli.model.TagSearchParams;
import com.zatgo.zup.suanli.service.TagService;

@Service
public class TagServiceImpl implements TagService {
	
	@Autowired
	private TagMapper tagMapper;

	@Override
	public PageInfo<TagData> getTagList(TagSearchParams params) {
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		params.setStatus(TagStatus.USING.getStatus());
		Page<TagData> page = tagMapper.selectTagByParams(params);
		PageInfo<TagData> info = new PageInfo<>(page.getPages(), page.getPageSize(), page.getTotal(), page.getResult());
		PageHelper.clearPage();
		return info;
	}

	@Transactional
	@Override
	public TagData addTag(TagParams params) {
		String name = params.getName();
		if(StringUtils.isEmpty(name)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "tag name miss");
		}
		
		Byte type = params.getType();
		if(type == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "tag type miss");
		}
		
		Byte classType = params.getClassType();
		if(classType == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "tag class type miss");
		}
		
		Tag tag = tagMapper.selectTagByName(name);
		if(tag != null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "tag exists");
		}
		
		tag = new Tag();
		BeanUtils.copyProperties(params, tag);
		tag.setCreateTime(new Date());
		tag.setUpdateTime(new Date());
		tagMapper.insertSelective(tag);
		
		TagData data = new TagData();
		BeanUtils.copyProperties(tag, data);
		return data;
	}

	@Transactional
	@Override
	public TagData updateTag(TagParams params) {
		Long tagId = params.getTagId();
		if(tagId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "tag id miss");
		}
		
		String name = params.getName();
		Byte type = params.getType();
		Byte classType = params.getClassType();
		if(StringUtils.isEmpty(name) && type == null && classType == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "tag not change");
		}
		
		Tag tag = new Tag();
		BeanUtils.copyProperties(params, tag);
		tag.setUpdateTime(new Date());
		tagMapper.updateByPrimaryKeySelective(tag);
		
		TagData data = new TagData();
		BeanUtils.copyProperties(tag, data);
		return data;
	}

	@Transactional
	@Override
	public void deleteTag(Long tagId) {
		if(tagId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "tag id miss");
		}
		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setStatus(TagStatus.DELETE.getStatus());
		tag.setUpdateTime(new Date());
		tagMapper.updateByPrimaryKeySelective(tag);
	}

}
