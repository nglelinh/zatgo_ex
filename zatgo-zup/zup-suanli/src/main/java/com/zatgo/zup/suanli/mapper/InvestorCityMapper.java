package com.zatgo.zup.suanli.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.suanli.entity.InvestorCity;

public interface InvestorCityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InvestorCity record);

    int insertSelective(InvestorCity record);

    InvestorCity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(InvestorCity record);

    int updateByPrimaryKey(InvestorCity record);
    
    List<InvestorCity> selectInvestorCityByInvestor(@Param("investorId") Long investorId);
    
    int deleteByInvestorId(Long investorId);
}