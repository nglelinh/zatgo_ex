package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "参与人参数")
public class PartyParams {
	
	@ApiModelProperty(value = "参与人id，修改使用", required = false)
	private Long partyId;
	
	@ApiModelProperty(value = "参与人姓名", required = false)
	private String partyName;
	
	@ApiModelProperty(value = "真实姓名", required = false)
	private String realName;
	
	@ApiModelProperty(value = "公司id", required = false)
	private Long companyId;
	
	@ApiModelProperty(value = "公司名称", required = false)
	private String companyName;
	
	@ApiModelProperty(value = "城市编号", required = false)
	private String cityCode;
	
	@ApiModelProperty(value = "城市名称", required = false)
	private String cityName;
	
	@ApiModelProperty(value = "英文名称", required = false)
	private String enName;
	
	@ApiModelProperty(value = "国籍", required = false)
	private String nationality;
	
	@ApiModelProperty(value = "生日", required = false)
	private String birthday;
	
	@ApiModelProperty(value = "性别，1-男；2-女", required = false, allowableValues = "1,2")
	private String sex;
	
	@ApiModelProperty(value = "联系手机号", required = false)
	private String phone;
	
	@ApiModelProperty(value = "证件类型，0-身份证", required = false)
	private Byte idcardType;
	
	@ApiModelProperty(value = "证件编号", required = false)
	private String idcardCode;
	
	@ApiModelProperty(value = "联系邮箱", required = false)
	private String email;
	
	@ApiModelProperty(value = "照片", required = false)
	private String photo;
	
	@ApiModelProperty(value = "用户id", required = false)
	private String userId;
	
	@ApiModelProperty(value = "创建者", required = false, hidden = true)
	private String creator;

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Byte getIdcardType() {
		return idcardType;
	}

	public void setIdcardType(Byte idcardType) {
		this.idcardType = idcardType;
	}

	public String getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(String idcardCode) {
		this.idcardCode = idcardCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

}
