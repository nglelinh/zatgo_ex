package com.zatgo.zup.suanli.model;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("增改项目参数")
public class ProjectParams {
	
	@ApiModelProperty(value = "项目id，修改使用", required = false)
	private Long projectId;
	
	@ApiModelProperty(value = "项目名称", required = false)
	private String name;
	
	@ApiModelProperty(value = "项目简介-255", required = false)
	private String shotDesc;
	
	@ApiModelProperty(value = "项目描述-2018", required = false)
	private String content;
	
	@ApiModelProperty(value = "阶段名称", required = false)
	private String stageName;
	
	@ApiModelProperty(value = "城市编码", required = false)
	private String cityCode;
	
	@ApiModelProperty(value = "城市名称", required = false)
	private String cityName;
	
	@ApiModelProperty(value = "公司id", required = false)
	private Long companyId;
	
	@ApiModelProperty(value = "公司名称", required = false)
	private String companyName;
	
	@ApiModelProperty(value = "创建者", required = false, hidden = true)
	private String creator;
	
	@ApiModelProperty(value = "图片地址", required = false)
	private String imgUrl;
	
	@ApiModelProperty(value = "分类id", required = false)
	private List<Long> catalogIds;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = true, allowableValues = "0,1")
	private Byte language;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = false)
	private List<Long> tagIds;
	
	@ApiModelProperty(value = "用户映射id", required = false)
	private Long partyId;
	
	@ApiModelProperty(value = "参与人身份 2：项目方", required = false, allowableValues = "2")
	private Long partyRoleId;

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShotDesc() {
		return shotDesc;
	}

	public void setShotDesc(String shotDesc) {
		this.shotDesc = shotDesc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public List<Long> getCatalogIds() {
		return catalogIds;
	}

	public void setCatalogIds(List<Long> catalogIds) {
		this.catalogIds = catalogIds;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public List<Long> getTagIds() {
		return tagIds;
	}

	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public Long getPartyRoleId() {
		return partyRoleId;
	}

	public void setPartyRoleId(Long partyRoleId) {
		this.partyRoleId = partyRoleId;
	}

}
