package com.zatgo.zup.suanli.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.CompanyData;
import com.zatgo.zup.suanli.model.CompanyParams;
import com.zatgo.zup.suanli.model.CompanySearchParams;
import com.zatgo.zup.suanli.service.CompanyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(value = "/suanli/company")
public class CompanyController extends BaseController {
	
	@Autowired
	private CompanyService companyService;
	
	@ApiOperation("查询企业列表")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseData<PageInfo<CompanyData>> getCompanyList(@ModelAttribute CompanySearchParams params){
		PageInfo<CompanyData> info = companyService.getCompanyList(params);
		return BusinessResponseFactory.createSuccess(info);
	}
	
	@ApiOperation("新增企业")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseData<CompanyData> addCompany(@RequestBody CompanyParams params){
		params.setCreator(getAdminUserInfo().getAdminName());
		CompanyData data = companyService.addCompany(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation("修改企业")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseData<CompanyData> updateCompany(@RequestBody CompanyParams params){
		getAdminUserInfo();
		CompanyData data = companyService.updateCompany(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation("删除企业")
	@RequestMapping(value = "/delete/{companyId}", method = RequestMethod.DELETE)
	public ResponseData<Object> deleteCompany(@PathVariable Long companyId){
		getAdminUserInfo();
		companyService.deleteCompany(companyId);
		return BusinessResponseFactory.createSuccess(null);
	}

}
