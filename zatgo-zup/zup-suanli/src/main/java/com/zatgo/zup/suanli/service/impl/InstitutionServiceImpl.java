package com.zatgo.zup.suanli.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.SuanliEnum.InstitutionStatus;
import com.zatgo.zup.common.enumtype.SuanliEnum.TagClassType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.entity.Institution;
import com.zatgo.zup.suanli.entity.TagRel;
import com.zatgo.zup.suanli.mapper.InstitutionMapper;
import com.zatgo.zup.suanli.mapper.TagRelMapper;
import com.zatgo.zup.suanli.model.InstitutionData;
import com.zatgo.zup.suanli.model.InstitutionParams;
import com.zatgo.zup.suanli.model.InstitutionSearchParams;
import com.zatgo.zup.suanli.service.InstitutionService;

@Service
public class InstitutionServiceImpl implements InstitutionService {
	
	@Autowired
	private InstitutionMapper institutionMapper;
	
	@Autowired
	private TagRelMapper tagRelMapper;

	@Override
	public PageInfo<InstitutionData> getInstitutionList(InstitutionSearchParams params) {
		params.setStatus(InstitutionStatus.USING.getStatus());
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		Page<InstitutionData> page = institutionMapper.selectInstitutionListByParams(params);
		PageInfo<InstitutionData> info = new PageInfo<>(page.getPageNum(), page.getPageSize(), page.getTotal(), page.getResult());
		return info;
	}

	@Transactional
	@Override
	public InstitutionData addInstitution(InstitutionParams params) {
		String name = params.getName();
		if(StringUtils.isEmpty(name)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "institution name miss");
		}
		Byte language = params.getLanguage();
		if(language == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "language miss");
		}
		
		Institution institution = institutionMapper.selectInstitutionByName(name);
		if(institution != null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "institution existed");
		}
		
		institution = new Institution();
		BeanUtils.copyProperties(params, institution);
		institution.setCreateTime(new Date());
		institution.setUpdateTime(new Date());
		institutionMapper.insertInstitutionSelective(institution);
		Long institutionId = institution.getInstitutionId();
		
		List<Long> tagIds = params.getTagIds();
		if(CollectionUtils.isNotEmpty(tagIds)) {
			for (Long tagId : tagIds) {
				TagRel tagRel = new TagRel();
				tagRel.setRelObjectId(institutionId);
				tagRel.setRelObjectType(TagClassType.INSTITUTION.getType());
				tagRel.setTagId(tagId);
				tagRelMapper.insertSelective(tagRel);
			}
		}
		
		InstitutionData data = new InstitutionData();
		BeanUtils.copyProperties(institution, data);
		return data;
	}

	@Transactional
	@Override
	public InstitutionData updateInstitution(InstitutionParams params) {
		Long institutionId = params.getInstitutionId();
		if(institutionId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "institution id miss");
		}
		
		Institution institution = institutionMapper.selectByPrimaryKey(institutionId);
		if(institution == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "institution not exist");
		}
		
		institution = new Institution();
		BeanUtils.copyProperties(params, institution);
		institution.setUpdateTime(new Date());
		institutionMapper.updateByPrimaryKeySelective(institution);
		
		List<Long> tagIds = params.getTagIds();
		if(CollectionUtils.isNotEmpty(tagIds)) {
			tagRelMapper.deleteTagRelByObjectId(institutionId, TagClassType.INSTITUTION.getType());
			for (Long tagId : tagIds) {
				TagRel tagRel = new TagRel();
				tagRel.setRelObjectId(institutionId);
				tagRel.setRelObjectType(TagClassType.INSTITUTION.getType());
				tagRel.setTagId(tagId);
				tagRelMapper.insertSelective(tagRel);
			}
		}
		
		InstitutionData data = new InstitutionData();
		BeanUtils.copyProperties(institution, data);
		return data;
	}

	@Transactional
	@Override
	public void deleteInstitution(Long institutionId) {
		Institution institution = new Institution();
		institution.setInstitutionId(institutionId);
		institution.setStatus(InstitutionStatus.DELETE.getStatus());
		institution.setUpdateTime(new Date());
		institutionMapper.updateByPrimaryKeySelective(institution);
	}

}
