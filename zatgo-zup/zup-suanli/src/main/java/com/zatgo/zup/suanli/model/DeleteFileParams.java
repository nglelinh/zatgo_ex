package com.zatgo.zup.suanli.model;

import java.util.List;

import com.zatgo.zup.common.enumtype.BusinessEnum.OssBucket;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("删除文件参数")
public class DeleteFileParams {
	
	@ApiModelProperty(value = "上传空间名称： 活动-campaign；项目-project；投资人-investor；投资机构-institution", required = true, allowableValues = "campaign,project,investor,institution")
	private OssBucket ossBucket;
	
	@ApiModelProperty(value = "文件访问地址", required = false)
	private List<String> fileUrlList;

	public OssBucket getOssBucket() {
		return ossBucket;
	}

	public void setOssBucket(String ossBucket) {
		this.ossBucket = OssBucket.getEnumByType(ossBucket);
	}

	public List<String> getFileUrlList() {
		return fileUrlList;
	}

	public void setFileUrlList(List<String> fileUrlList) {
		this.fileUrlList = fileUrlList;
	}

}
