package com.zatgo.zup.suanli.mapper;

import java.util.List;

import com.zatgo.zup.suanli.entity.Catalog;
import com.zatgo.zup.suanli.entity.ProjectCatalog;

public interface ProjectCatalogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ProjectCatalog record);

    int insertSelective(ProjectCatalog record);

    ProjectCatalog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProjectCatalog record);

    int updateByPrimaryKey(ProjectCatalog record);
    
    List<Catalog> selectCatalogByProjectId(Long projectId);
}