package com.zatgo.zup.suanli.service;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.model.PartyData;
import com.zatgo.zup.suanli.model.PartyDetailData;
import com.zatgo.zup.suanli.model.PartyParams;
import com.zatgo.zup.suanli.model.PartySerchParams;

public interface PartyService {
	
	/**
	 * 添加参与人
	 * @param userId
	 */
	public void addParty(String userId);
	
	/**
	 * 查询参与人列表
	 * @param params
	 * @return
	 */
	public PageInfo<PartyData> getPartyList(PartySerchParams params);
	
	/**
	 * 添加参与人
	 * @param params
	 */
	public void addParty(PartyParams params);
	
	/**
	 * 修改参与人
	 * @param params
	 */
	public void updateParty(PartyParams params);
	
	/**
	 * 参与人详情
	 * @param partyId
	 * @return
	 */
	public PartyDetailData getPartyDetail(Long partyId);

}
