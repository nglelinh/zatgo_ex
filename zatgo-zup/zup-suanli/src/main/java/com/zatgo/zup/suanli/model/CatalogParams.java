package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("增改分类")
public class CatalogParams {
	
	@ApiModelProperty(value = "分类id，修改使用", required = false)
	private Long catalogId;
	
	@ApiModelProperty(value = "分类名称", required = false)
	private String name;
	
	@ApiModelProperty(value = "父分类id，顶级分类可不传或传0", required = false)
	private Long parentCatalogId;
	
	@ApiModelProperty(value = "l分类类型 0：项目", required = false)
	private Byte catalogType;
	
	@ApiModelProperty(value = "0为根目录", required = false)
	private Byte catalogLevel;
	
	@ApiModelProperty(value = "创建者", required = false, hidden = true)
	private String creator;

	public Long getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentCatalogId() {
		return parentCatalogId;
	}

	public void setParentCatalogId(Long parentCatalogId) {
		this.parentCatalogId = parentCatalogId;
	}

	public Byte getCatalogType() {
		return catalogType;
	}

	public void setCatalogType(Byte catalogType) {
		this.catalogType = catalogType;
	}

	public Byte getCatalogLevel() {
		return catalogLevel;
	}

	public void setCatalogLevel(Byte catalogLevel) {
		this.catalogLevel = catalogLevel;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

}
