package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("项目查询参数")
public class ProjectSearchParams {
	
	@ApiModelProperty(value = "页码", required = false)
	private Integer pageNo;
	
	@ApiModelProperty(value = "数量", required = false)
	private Integer pageSize;

	@ApiModelProperty(value = "领域编号", required = false)
	private Long catalogId;

	@ApiModelProperty(value = "阶段类型 0:不限 1:种子轮 2:天使轮 3:Pre-A轮 4:A轮 5:A+轮 6:Pre-B轮 7:B轮 8:B+轮", required = false, allowableValues = "0,1,2,3,4,5,6,7,8")
	private Byte stageType;

	@ApiModelProperty(value = "城市编号", required = false)
	private String cityCode;
	
	@ApiModelProperty(value = "阶段名称", required = false)
	private String stageName;
	
	@ApiModelProperty(value = "状态：0-已删除；1-使用中", required = false, hidden = true)
	private Byte status;
	
	private Byte language;

	public Integer getPageNo() {
		return pageNo == null ? 1 : pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize == null ? 10 : pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Long getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}

	public Byte getStageType() {
		return stageType;
	}

	public void setStageType(Byte stageType) {
		this.stageType = stageType;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

}
