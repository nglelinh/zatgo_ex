package com.zatgo.zup.suanli.model;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("投资人")
public class InvestorData {
	
	@ApiModelProperty(value = "投资人id", required = true)
	private Long investorId;
	
	@ApiModelProperty(value = "投资人姓名", required = true)
	private String name;
	
	@ApiModelProperty(value = "投资机构", required = true)
	private String institutionName;
	
	@ApiModelProperty(value = "职位", required = true)
	private String position;
	
	@ApiModelProperty(value = "简介", required = true)
	private String shotDesc;
	
	@ApiModelProperty(value = "关注分类", required = true)
	private List<String> catalogName;
	
	@ApiModelProperty(value = "关注阶段", required = true)
	private List<String> stageName;
	
	@ApiModelProperty(value = "最小投资额度，单位：万", required = true)
	private BigDecimal minInvestAmount;
	
	@ApiModelProperty(value = "最大投资额度，单位：万", required = true)
	private BigDecimal maxInvestAmount;
	
	@ApiModelProperty(value = "关注城市", required = true)
	private List<String> cityName;
	
	@ApiModelProperty(value = "联系方式", required = true)
	private String contact;
	
	@ApiModelProperty(value = "是否收藏", required = true)
	private Boolean isCollect;
	
	@ApiModelProperty(value = "收获数", required = true)
	private Integer harvest;
	
	@ApiModelProperty(value = "反馈率，小数", required = true)
	private BigDecimal feedback;
	
	@ApiModelProperty(value = "约谈率，小数", required = true)
	private BigDecimal interview;

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getShotDesc() {
		return shotDesc;
	}

	public void setShotDesc(String shotDesc) {
		this.shotDesc = shotDesc;
	}

	public List<String> getCatalogName() {
		return catalogName;
	}

	public void setCatalogName(List<String> catalogName) {
		this.catalogName = catalogName;
	}

	public List<String> getStageName() {
		return stageName;
	}

	public void setStageName(List<String> stageName) {
		this.stageName = stageName;
	}

	public BigDecimal getMinInvestAmount() {
		return minInvestAmount;
	}

	public void setMinInvestAmount(BigDecimal minInvestAmount) {
		this.minInvestAmount = minInvestAmount;
	}

	public BigDecimal getMaxInvestAmount() {
		return maxInvestAmount;
	}

	public void setMaxInvestAmount(BigDecimal maxInvestAmount) {
		this.maxInvestAmount = maxInvestAmount;
	}

	public List<String> getCityName() {
		return cityName;
	}

	public void setCityName(List<String> cityName) {
		this.cityName = cityName;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Boolean getIsCollect() {
		return isCollect;
	}

	public void setIsCollect(Boolean isCollect) {
		this.isCollect = isCollect;
	}

	public Integer getHarvest() {
		return harvest;
	}

	public void setHarvest(Integer harvest) {
		this.harvest = harvest;
	}

	public BigDecimal getFeedback() {
		return feedback;
	}

	public void setFeedback(BigDecimal feedback) {
		this.feedback = feedback;
	}

	public BigDecimal getInterview() {
		return interview;
	}

	public void setInterview(BigDecimal interview) {
		this.interview = interview;
	}

}
