package com.zatgo.zup.suanli.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.suanli.entity.NeedMessage;

/**
 * Created by 46041 on 2019/9/2.
 */
public interface NeedMessageService {


    void add(NeedMessage needMessage);

    void del(String id);

    void edit(NeedMessage needMessage);

    PageInfo<NeedMessage> list(Integer pageSzie, Integer pageNo, Integer type, Integer source);
}
