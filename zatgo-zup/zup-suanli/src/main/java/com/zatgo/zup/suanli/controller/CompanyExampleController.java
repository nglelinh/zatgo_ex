package com.zatgo.zup.suanli.controller;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.entity.CompanyExample;
import com.zatgo.zup.suanli.service.CompanyExamplService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2019/9/2.
 */

@RestController
@Api(value = "/suanli/company/example", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/company/example")
public class CompanyExampleController {

    @Autowired
    private CompanyExamplService companyExamplService;

    //公共
    @ApiOperation(value = "详情")
    @GetMapping(value = "/detail/{id}")
    public ResponseData<CompanyExample> detail(@PathVariable("id") String id){
        CompanyExample example = companyExamplService.detail(id);
        return BusinessResponseFactory.createSuccess(example);
    }


    //前端

    @ApiOperation(value = "查询")
    @GetMapping(value = "/front/list")
    public ResponseData<PageInfo<CompanyExample>> frontList (@RequestParam("pageSzie") Integer pageSzie,@RequestParam("pageNo") Integer pageNo){
        PageInfo<CompanyExample> res = companyExamplService.frontList(pageSzie, pageNo);
        return BusinessResponseFactory.createSuccess(res);
    }

    //管理后台

    @ApiOperation(value = "新增")
    @PostMapping(value = "/back/add")
    public ResponseData add(@RequestBody CompanyExample companyExample){
        companyExamplService.add(companyExample);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "删除")
    @GetMapping(value = "/back/del/{id}")
    public ResponseData del(@PathVariable("id") String id){
        companyExamplService.del(id);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "修改")
    @PostMapping(value = "/back/edit")
    public ResponseData edit(@RequestBody CompanyExample companyExample){
        companyExamplService.edit(companyExample);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "查询")
    @GetMapping(value = "/back/list")
    public ResponseData<PageInfo<CompanyExample>> backList (@RequestParam("pageSize") Integer pageSize,@RequestParam("pageNo") Integer pageNo){
        PageInfo<CompanyExample> res =  companyExamplService.backList(pageSize, pageNo);
        return BusinessResponseFactory.createSuccess(res);
    }

}
