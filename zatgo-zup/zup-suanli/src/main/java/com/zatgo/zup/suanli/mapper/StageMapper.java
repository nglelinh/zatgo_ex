package com.zatgo.zup.suanli.mapper;

import java.util.List;

import com.zatgo.zup.suanli.entity.Stage;
import com.zatgo.zup.suanli.model.StageData;
import com.zatgo.zup.suanli.model.StageSearchParams;

public interface StageMapper {
    int deleteByPrimaryKey(Byte stageType);

    int insert(Stage record);

    int insertSelective(Stage record);

    Stage selectByPrimaryKey(Byte stageType);

    int updateByPrimaryKeySelective(Stage record);

    int updateByPrimaryKey(Stage record);
    
    List<StageData> selectStageList(StageSearchParams params);
}