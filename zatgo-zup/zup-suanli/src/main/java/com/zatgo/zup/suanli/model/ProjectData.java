package com.zatgo.zup.suanli.model;

import java.util.List;

import com.zatgo.zup.suanli.entity.Stage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("投资项目")
public class ProjectData {
	
	@ApiModelProperty(value = "项目id", required = true)
	private Long projectId;
	
	@ApiModelProperty(value = "项目名称", required = true)
	private String name;
	
	@ApiModelProperty(value = "项目简介", required = true)
	private String shotDesc;
	
	@ApiModelProperty(value = "项目介绍", required = true)
	private String content;
	
	@ApiModelProperty(value = "城市名称", required = true)
	private String cityName;
	
	@ApiModelProperty(value = "公司名称", required = true)
	private String companyName;
	
	@ApiModelProperty(value = "阶段名称", required = false)
	private String stageName;
	
	@ApiModelProperty(value = "项目分类", required = false)
	private List<InvestorCatalogData> catalogDatas;
	
	@ApiModelProperty(value = "项目图片", required = false)
	private String imgUrl;
	
	@ApiModelProperty(value = "阶段", required = false)
	private List<Stage> stages;
	
	@ApiModelProperty(value = "标签", required = false)
	private List<TagData> tagDatas;
	
	@ApiModelProperty(value = "是否收藏", required = false)
	private Boolean isCollect;

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShotDesc() {
		return shotDesc;
	}

	public void setShotDesc(String shotDesc) {
		this.shotDesc = shotDesc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public List<InvestorCatalogData> getCatalogDatas() {
		return catalogDatas;
	}

	public void setCatalogDatas(List<InvestorCatalogData> catalogDatas) {
		this.catalogDatas = catalogDatas;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public List<TagData> getTagDatas() {
		return tagDatas;
	}

	public void setTagDatas(List<TagData> tagDatas) {
		this.tagDatas = tagDatas;
	}

	public Boolean getIsCollect() {
		return isCollect;
	}

	public void setIsCollect(Boolean isCollect) {
		this.isCollect = isCollect;
	}

	public List<Stage> getStages() {
		return stages;
	}

	public void setStages(List<Stage> stages) {
		this.stages = stages;
	}

}
