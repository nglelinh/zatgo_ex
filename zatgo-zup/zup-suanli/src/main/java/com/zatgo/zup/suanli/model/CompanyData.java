package com.zatgo.zup.suanli.model;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("企业信息")
public class CompanyData {
	
	@ApiModelProperty(value = "企业id", required = true)
	private Long companyId;
	
	@ApiModelProperty(value = "企业名称", required = true)
	private String name;
	
	@ApiModelProperty(value = "企业编码", required = false)
	private String code;
	
	@ApiModelProperty(value = "成立年份", required = false)
	private Date foundingYear;
	
	@ApiModelProperty(value = "税号", required = false)
	private String taxCode;

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getFoundingYear() {
		return foundingYear;
	}

	public void setFoundingYear(Date foundingYear) {
		this.foundingYear = foundingYear;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

}
