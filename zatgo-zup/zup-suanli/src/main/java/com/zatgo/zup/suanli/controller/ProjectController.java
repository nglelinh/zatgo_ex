package com.zatgo.zup.suanli.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.ProjectData;
import com.zatgo.zup.suanli.model.ProjectParams;
import com.zatgo.zup.suanli.model.ProjectSearchParams;
import com.zatgo.zup.suanli.service.ProjectService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/project")
public class ProjectController extends BaseController {
	
	@Autowired
	private ProjectService projectService;

	@ApiOperation(value = "项目列表")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseData<PageInfo<ProjectData>> getProjectList(@ModelAttribute ProjectSearchParams params) {
		params.setLanguage(getLanguage());
		PageInfo<ProjectData> info = projectService.getProjectList(params);
		return BusinessResponseFactory.createSuccess(info);
	}
	
	@ApiOperation(value = "项目详情")
	@RequestMapping(value = "/detail/{projectId}", method = RequestMethod.GET)
	public ResponseData<ProjectData> getProjectDetail(@PathVariable Long projectId) {
		String userId = getUserId();
		ProjectData data = projectService.getProjectDetail(projectId, userId);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "新增项目")
	@RequestMapping(value = "/add", name = "新增活动", method = RequestMethod.POST)
	public ResponseData<ProjectData> addProject(@RequestBody ProjectParams params) {
		params.setCreator(getAdminUserInfo().getAdminName());
		ProjectData data = projectService.addProject(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "修改项目")
	@RequestMapping(value = "/update", name = "修改活动", method = RequestMethod.POST)
	public ResponseData<Object> updateProject(@RequestBody ProjectParams params) {
		getAdminUserInfo();
		ProjectData data = projectService.updateProject(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "删除项目")
	@RequestMapping(value = "/delete/{projectId}", name = "删除活动", method = RequestMethod.DELETE)
	public ResponseData<Object> deleteProject(@PathVariable Long projectId) {
		getAdminUserInfo();
		projectService.deleteProject(projectId);
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "项目列表")
	@RequestMapping(value = "/list/admin", method = RequestMethod.GET)
	public ResponseData<PageInfo<ProjectData>> getProjectListAdmin(@ModelAttribute ProjectSearchParams params) {
		PageInfo<ProjectData> info = projectService.getProjectListAdmin(params);
		return BusinessResponseFactory.createSuccess(info);
	}

}
