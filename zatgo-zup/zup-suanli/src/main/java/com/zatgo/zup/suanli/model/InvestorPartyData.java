package com.zatgo.zup.suanli.model;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("投资人详情")
public class InvestorPartyData {
	
	@ApiModelProperty(value = "投资人id，修改使用", required = false)
	private Long investorId;
	
	@ApiModelProperty(value = "投资人角色id", required = true)
	private Long partyRoleId;
	
	@ApiModelProperty(value = "投资人角色", required = true)
	private String partyRoleName;
	
	@ApiModelProperty(value = "投资人名称", required = true)
	private String name;
	
	@ApiModelProperty(value = "投资人简介", required = false)
	private String shotDesc;
	
	@ApiModelProperty(value = "投资人详细介绍", required = false)
	private String content;
	
	@ApiModelProperty(value = "最小投资额度，单位：万", required = false)
	private BigDecimal minInvestAmount;
	
	@ApiModelProperty(value = "最大投资额度，单位：万", required = false)
	private BigDecimal maxInvestAmount;
	
	@ApiModelProperty(value = "创建人", required = false, hidden = true)
	private String creator;
	
	@ApiModelProperty(value = "职位", required = false)
	private String position;
	
	@ApiModelProperty(value = "联系方式", required = false)
	private String contact;
	
	@ApiModelProperty(value = "投资机构id", required = true)
	private Long institutionId;
	
	@ApiModelProperty(value = "投资机构名称", required = true)
	private String institutionName;
	
	@ApiModelProperty(value = "关注分类", required = false)
	private List<InvestorCatalogData> catalogDatas;
	
	@ApiModelProperty(value = "关注阶段", required = false)
	private List<StageData> stageDatas;
	
	@ApiModelProperty(value = "关注城市", required = false)
	private List<CityData> cityDatas;
	
	@ApiModelProperty(value = "参与人姓名", required = true)
	private String partyName;
	
	@ApiModelProperty(value = "公司id", required = true)
	private Long companyId;
	
	@ApiModelProperty(value = "公司名称", required = true)
	private String companyName;
	
	@ApiModelProperty(value = "城市编号", required = true)
	private String cityCode;
	
	@ApiModelProperty(value = "城市名称", required = true)
	private String cityName;
	
	@ApiModelProperty(value = "英文名称", required = false)
	private String enName;
	
	@ApiModelProperty(value = "国籍", required = false)
	private String nationality;
	
	@ApiModelProperty(value = "生日", required = false)
	private String birthday;
	
	@ApiModelProperty(value = "性别，1-男；2-女", required = false, allowableValues = "1,2")
	private String sex;
	
	@ApiModelProperty(value = "联系手机号", required = false)
	private String phone;
	
	@ApiModelProperty(value = "证件类型，0-身份证", required = false)
	private Byte idcardType;
	
	@ApiModelProperty(value = "证件编号", required = false)
	private String idcardCode;
	
	@ApiModelProperty(value = "联系邮箱", required = false)
	private String email;
	
	@ApiModelProperty(value = "照片", required = false)
	private String photo;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = true, allowableValues = "0,1")
	private Byte language;
	
	@ApiModelProperty(value = "用户id", required = true)
	private String userId;
	
	@ApiModelProperty(value = "状态：0-已删除；1-使用中", required = false, allowableValues = "0,1")
	private Byte status;
	
	@ApiModelProperty(value = "标签", required = false)
	private List<TagData> tagDatas;

	public Long getInvestorId() {
		return investorId;
	}

	public void setInvestorId(Long investorId) {
		this.investorId = investorId;
	}

	public Long getPartyRoleId() {
		return partyRoleId;
	}

	public void setPartyRoleId(Long partyRoleId) {
		this.partyRoleId = partyRoleId;
	}

	public String getPartyRoleName() {
		return partyRoleName;
	}

	public void setPartyRoleName(String partyRoleName) {
		this.partyRoleName = partyRoleName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShotDesc() {
		return shotDesc;
	}

	public void setShotDesc(String shotDesc) {
		this.shotDesc = shotDesc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public BigDecimal getMinInvestAmount() {
		return minInvestAmount;
	}

	public void setMinInvestAmount(BigDecimal minInvestAmount) {
		this.minInvestAmount = minInvestAmount;
	}

	public BigDecimal getMaxInvestAmount() {
		return maxInvestAmount;
	}

	public void setMaxInvestAmount(BigDecimal maxInvestAmount) {
		this.maxInvestAmount = maxInvestAmount;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Long getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Long institutionId) {
		this.institutionId = institutionId;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public List<InvestorCatalogData> getCatalogDatas() {
		return catalogDatas;
	}

	public void setCatalogDatas(List<InvestorCatalogData> catalogDatas) {
		this.catalogDatas = catalogDatas;
	}

	public List<StageData> getStageDatas() {
		return stageDatas;
	}

	public void setStageDatas(List<StageData> stageDatas) {
		this.stageDatas = stageDatas;
	}

	public List<CityData> getCityDatas() {
		return cityDatas;
	}

	public void setCityDatas(List<CityData> cityDatas) {
		this.cityDatas = cityDatas;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Byte getIdcardType() {
		return idcardType;
	}

	public void setIdcardType(Byte idcardType) {
		this.idcardType = idcardType;
	}

	public String getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(String idcardCode) {
		this.idcardCode = idcardCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public List<TagData> getTagDatas() {
		return tagDatas;
	}

	public void setTagDatas(List<TagData> tagDatas) {
		this.tagDatas = tagDatas;
	}

}
