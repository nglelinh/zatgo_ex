package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("标签查询参数")
public class TagSearchParams {
	
	@ApiModelProperty(value = "当前页码",required =true )
	private Integer pageNo;
	
	@ApiModelProperty(value = "每页条数",required =true )
	private Integer pageSize;
	
	@ApiModelProperty(value = "标签名称", required = false)
	private String name;
	
	@ApiModelProperty(value = "标签类型：0：系统 1：系统自定义 2：用户自定义", required = false)
	private Byte type;
	
	@ApiModelProperty(value = "0：项目 1：投资人 2：投资机构", required = false)
	private Byte classType;
	
	@ApiModelProperty(value = "标签状态：0-已删除；1-使用中", required = false, hidden = true)
	private Byte status;

	public Integer getPageNo() {
		return pageNo == null ? 1 : pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize == null ? 10 : pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public Byte getClassType() {
		return classType;
	}

	public void setClassType(Byte classType) {
		this.classType = classType;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

}
