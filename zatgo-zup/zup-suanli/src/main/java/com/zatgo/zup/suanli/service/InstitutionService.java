package com.zatgo.zup.suanli.service;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.model.InstitutionData;
import com.zatgo.zup.suanli.model.InstitutionParams;
import com.zatgo.zup.suanli.model.InstitutionSearchParams;

public interface InstitutionService {
	
	/**
	 * 查询投资机构列表
	 * @param params
	 * @return
	 */
	public PageInfo<InstitutionData> getInstitutionList(InstitutionSearchParams params);
	
	/**
	 * 新增投资机构
	 * @param params
	 * @return
	 */
	public InstitutionData addInstitution(InstitutionParams params);
	
	/**
	 * 修改投资机构
	 * @param params
	 * @return
	 */
	public InstitutionData updateInstitution(InstitutionParams params);
	
	/**
	 * 删除投资机构
	 * @param institutionId
	 */
	public void deleteInstitution(Long institutionId);

}
