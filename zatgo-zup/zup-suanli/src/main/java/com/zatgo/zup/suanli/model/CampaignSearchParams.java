package com.zatgo.zup.suanli.model;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("活动查询参数")
public class CampaignSearchParams {
	
	@ApiModelProperty(value = "当前页码",required =true )
	private Integer pageNo;
	
	@ApiModelProperty(value = "每页条数",required =true )
	private Integer pageSize;
	
	@ApiModelProperty(value = "活动开始时间", required = false)
	private Date campaignStart;
	
	@ApiModelProperty(value = "活动结束时间", required = false)
	private Date campaignEnd;
	
	@ApiModelProperty(value = "城市编号", required = false)
	private String cityCode;
	
	@ApiModelProperty(value = "费用", required = false)
	private BigDecimal fee;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = false)
	private Byte language;
	
	@ApiModelProperty(value = "状态：0-已删除；1-使用中", required = false, hidden = true)
	private Byte status;

	public Integer getPageNo() {
		return pageNo == null ? 1 : pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize == null ? 10 : pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Date getCampaignStart() {
		return campaignStart;
	}

	public void setCampaignStart(String campaignStart) {
		if(StringUtils.isNotEmpty(campaignStart)) {
			this.campaignStart = new Date(Long.valueOf(campaignStart));
		}
	}

	public Date getCampaignEnd() {
		return campaignEnd;
	}

	public void setCampaignEnd(String campaignEnd) {
		if(StringUtils.isNotEmpty(campaignEnd)) {
			this.campaignEnd = new Date(Long.valueOf(campaignEnd));
		}
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		if(StringUtils.isEmpty(cityCode)) {
			cityCode = null;
		}
		this.cityCode = cityCode;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

}
