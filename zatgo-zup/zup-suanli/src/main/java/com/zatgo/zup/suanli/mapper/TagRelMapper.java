package com.zatgo.zup.suanli.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.suanli.entity.TagRel;
import com.zatgo.zup.suanli.model.TagData;

public interface TagRelMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TagRel record);

    int insertSelective(TagRel record);

    TagRel selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TagRel record);

    int updateByPrimaryKey(TagRel record);
    
    int deleteTagRelByObjectId(@Param("objectId")Long objectId, @Param("objectType") Byte objectType);
    
    List<TagData> selectTagDataByObjectId(@Param("objectId")Long objectId, @Param("objectType") Byte objectType);
}