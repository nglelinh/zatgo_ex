package com.zatgo.zup.suanli.service.impl;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.SuanliEnum.CompanyStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.entity.Company;
import com.zatgo.zup.suanli.mapper.CompanyMapper;
import com.zatgo.zup.suanli.model.CompanyData;
import com.zatgo.zup.suanli.model.CompanyParams;
import com.zatgo.zup.suanli.model.CompanySearchParams;
import com.zatgo.zup.suanli.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {
	
	@Autowired
	private CompanyMapper companyMapperr;

	@Override
	public PageInfo<CompanyData> getCompanyList(CompanySearchParams params) {
		params.setStatus(CompanyStatus.USING.getStatus());
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		Page<CompanyData> page = companyMapperr.selectCompanyListByParams(params);
		PageInfo<CompanyData> info = new PageInfo<>(page.getPageNum(), page.getPageSize(), page.getTotal(), page.getResult());
		PageHelper.clearPage();
		return info;
	}

	@Transactional
	@Override
	public CompanyData addCompany(CompanyParams params) {
		String name = params.getName();
		if(StringUtils.isEmpty(name)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company name miss");
		}
		
		Company company = new Company();
		BeanUtils.copyProperties(params, company);
		company.setCreateTime(new Date());
		company.setUpdateTime(new Date());
		companyMapperr.insertSelective(company);
		
		CompanyData data = new CompanyData();
		BeanUtils.copyProperties(company, data);
		return data;
	}

	@Transactional
	@Override
	public CompanyData updateCompany(CompanyParams params) {
		Long companyId = params.getCompanyId();
		if(companyId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company id miss");
		}
		
		Company company = companyMapperr.selectByPrimaryKey(companyId);
		if(company == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company not existed");
		}
		
		company = new Company();
		BeanUtils.copyProperties(params, company);
		company.setUpdateTime(new Date());
		companyMapperr.updateByPrimaryKeySelective(company);
		
		CompanyData data = new CompanyData();
		BeanUtils.copyProperties(company, data);
		return data;
	}

	@Transactional
	@Override
	public void deleteCompany(Long companyId) {
		Company company = new Company();
		company.setCompanyId(companyId);
		company.setStatus(CompanyStatus.DELETE.getStatus());
		company.setUpdateTime(new Date());
		companyMapperr.updateByPrimaryKeySelective(company);
	}

}
