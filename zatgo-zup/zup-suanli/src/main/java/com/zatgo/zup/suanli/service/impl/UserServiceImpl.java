package com.zatgo.zup.suanli.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.SuanliEnum.UserFavoriteType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.entity.Investor;
import com.zatgo.zup.suanli.entity.UserFavorite;
import com.zatgo.zup.suanli.mapper.InvestorMapper;
import com.zatgo.zup.suanli.mapper.ProjectMapper;
import com.zatgo.zup.suanli.mapper.UserFavoriteMapper;
import com.zatgo.zup.suanli.model.FavoriteParams;
import com.zatgo.zup.suanli.model.InvestorData;
import com.zatgo.zup.suanli.model.ProjectData;
import com.zatgo.zup.suanli.model.UserData;
import com.zatgo.zup.suanli.model.UserFavoriteParams;
import com.zatgo.zup.suanli.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserFavoriteMapper userFavoriteMapper;

	@Autowired
	private InvestorMapper investorMapper;

	@Autowired
	private ProjectMapper projectMapper;

	@Transactional
	@Override
	public Long addUserFavorite(UserFavoriteParams params) {

		Byte type = params.getType();
		if (type == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "type miss");
		}
		Long id = params.getId();
		if (id == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "id miss");
		}

		UserFavorite favorite = userFavoriteMapper.selectByIdAndType(params.getUserId(), id, type);
		if (favorite != null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "User favorite existed");
		}

		favorite = new UserFavorite();
		favorite.setRelObjectId(params.getId());
		favorite.setRelObjectType(params.getType());
		favorite.setUserId(params.getUserId());
		userFavoriteMapper.insertUserFavoriteSelective(favorite);
		Long favoriteId = favorite.getId();
		return favoriteId;
	}

	@Transactional
	@Override
	public void deleteUserFavorite(UserFavoriteParams params) {
		userFavoriteMapper.deleteByParams(params);
	}

	@Override
	public PageInfo<ProjectData> getFavoriteProjectList(FavoriteParams params) {
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		Page<ProjectData> projectDatas = userFavoriteMapper
				.selectFavoriteProjectList(UserFavoriteType.PROJECT.getType(), params.getUserId());
		PageInfo<ProjectData> pageInfo = new PageInfo<>(projectDatas.getPageNum(), projectDatas.getPageSize(),
				projectDatas.getTotal(), projectDatas.getResult());
		return pageInfo;
	}

	@Override
	public PageInfo<InvestorData> getFavoriteInvestorList(FavoriteParams params) {
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		Page<InvestorData> investorDatas = userFavoriteMapper
				.selectFavoriteInvestorList(UserFavoriteType.INVESTOR.getType(), params.getUserId());
		PageInfo<InvestorData> pageInfo = new PageInfo<>(investorDatas.getPageNum(), investorDatas.getPageSize(),
				investorDatas.getTotal(), investorDatas.getResult());
		return pageInfo;
	}

	@Override
	public Long getUserInvestor(String userId) {
		Investor investor = investorMapper.selectInvestorByUserId(userId);
		if (investor == null) {
			return null;
		}
		return investor.getInvestorId();
	}

	@Override
	public UserData getUserData(String userId) {
		int follow = userFavoriteMapper.countFollowProjectAndInvestor(userId);
		int project = projectMapper.countUserProjectNum(userId);
		Long investorId = getUserInvestor(userId);

		UserData userData = new UserData();
		userData.setFollow(follow);
		userData.setInvestorId(investorId);
		userData.setProject(project);

		return userData;
	}

}
