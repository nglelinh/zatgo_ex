package com.zatgo.zup.suanli.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.CampaignParams;
import com.zatgo.zup.suanli.model.CampaignData;
import com.zatgo.zup.suanli.model.CampaignRegistrationParams;
import com.zatgo.zup.suanli.model.CampaignSearchParams;
import com.zatgo.zup.suanli.service.CampaignService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/campaign")
public class CampaignController extends BaseController {
	
	@Autowired
	private CampaignService campaignService;

	@ApiOperation(value = "活动列表查询")
	@RequestMapping(value = "/list", name = "活动列表查询", method = RequestMethod.GET)
	public ResponseData<PageInfo<CampaignData>> getCampaignList(@ModelAttribute CampaignSearchParams params) {
		params.setLanguage(getLanguage());
		PageInfo<CampaignData> info = campaignService.getCampaignList(params);
		return BusinessResponseFactory.createSuccess(info);
	}

	@ApiOperation(value = "活动详情查询")
	@RequestMapping(value = "/detail/{campaignId}", name = "活动列表查询", method = RequestMethod.GET)
	public ResponseData<CampaignData> getCampaignDetail(@PathVariable Long campaignId) {
		CampaignData data = campaignService.getCampaignDetail(campaignId, getUserId());
		return BusinessResponseFactory.createSuccess(data);
	}

	@ApiOperation(value = "活动报名")
	@RequestMapping(value = "/registration", name = "活动列表查询", method = RequestMethod.POST)
	public ResponseData<Object> campaignRegistration(@RequestBody CampaignRegistrationParams params) {
		AuthUserInfo user = getUserInfo();
		params.setUserId(user.getUserId());
		campaignService.campaignRegistration(params);
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "新增活动")
	@RequestMapping(value = "/add", name = "新增活动", method = RequestMethod.POST)
	public ResponseData<CampaignData> addCampaign(@RequestBody CampaignParams params) {
		String userName = getAdminUserInfo().getAdminName();
		params.setCreator(userName);
		CampaignData data = campaignService.addCampaign(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "修改活动")
	@RequestMapping(value = "/update", name = "修改活动", method = RequestMethod.POST)
	public ResponseData<Object> updateCampaign(@RequestBody CampaignParams params) {
		getAdminUserInfo();
		CampaignData data = campaignService.updateCampaign(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "删除活动")
	@RequestMapping(value = "/delete/{campaignId}", name = "删除活动", method = RequestMethod.DELETE)
	public ResponseData<Object> updateCampaign(@PathVariable Long campaignId) {
		getAdminUserInfo();
		campaignService.deleteCampaign(campaignId);
		return BusinessResponseFactory.createSuccess(null);
	}

}
