package com.zatgo.zup.suanli.service;

import java.util.List;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.model.ProjectData;
import com.zatgo.zup.suanli.model.ProjectParams;
import com.zatgo.zup.suanli.model.ProjectSearchParams;

public interface ProjectService {
	
	/**
	 * 查询项目列表
	 * @param params
	 * @return
	 */
	public PageInfo<ProjectData> getProjectList(ProjectSearchParams params);
	
	/**
	 * 查询项目详情
	 * @param projectId
	 * @return
	 */
	public ProjectData getProjectDetail(Long projectId, String userId);
	
	/**
	 * 新增项目
	 * @param params
	 * @return
	 */
	public ProjectData addProject(ProjectParams params);
	
	/**
	 * 修改项目
	 * @param params
	 * @return
	 */
	public ProjectData updateProject(ProjectParams params);
	
	/**
	 * 删除项目
	 * @param projectId
	 */
	public void deleteProject(Long projectId);
	
	/**
	 * 查询
	 * @param userId
	 * @return
	 */
	public List<ProjectData> getProjectByUser(String userId);
	
	/**
	 * 查询项目列表
	 * @param params
	 * @return
	 */
	public PageInfo<ProjectData> getProjectListAdmin(ProjectSearchParams params);

}
