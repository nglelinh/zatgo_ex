package com.zatgo.zup.suanli.service;

import java.util.List;

import com.zatgo.zup.suanli.model.CatalogBaseData;
import com.zatgo.zup.suanli.model.CatalogData;
import com.zatgo.zup.suanli.model.CatalogParams;
import com.zatgo.zup.suanli.model.CatalogSearchParams;
import com.zatgo.zup.suanli.model.CityData;
import com.zatgo.zup.suanli.model.StageData;
import com.zatgo.zup.suanli.model.StageSearchParams;

public interface CategoryService {
	
	/**
	 * 查询城市列表
	 * @return
	 */
	public List<CityData> getCityList();
	
	/**
	 * 查询目录
	 * @param params
	 * @return
	 */
	public List<CatalogData> getCatalogList(CatalogSearchParams params);
	
	/**
	 * 查询基础分类
	 * @param params
	 * @return
	 */
	public List<CatalogBaseData> getCatalogBaseList(CatalogSearchParams params);
	
	/**
	 * 查询阶段列表
	 * @param params
	 * @return
	 */
	public List<StageData> getStageList(StageSearchParams params);
	
	/**
	 * 新增分类
	 * @param params
	 * @return
	 */
	public CatalogData addCatalog(CatalogParams params);
	
	/**
	 * 修改分类
	 * @param params
	 * @return
	 */
	public CatalogData updateCatalog(CatalogParams params);
	
	/**
	 * 删除分类
	 * @param catalogId
	 */
	public void deleteCatalog(Long catalogId);

}
