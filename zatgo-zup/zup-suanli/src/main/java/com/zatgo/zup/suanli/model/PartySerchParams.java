package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "参与人查询参数")
public class PartySerchParams {
	
	@ApiModelProperty(value = "页数", required = false)
	private Integer pageNo;
	
	@ApiModelProperty(value = "数量", required = false)
	private Integer pageSize;
	
	@ApiModelProperty(value = "参与人姓名", required = false)
	private String partyName;
	
	@ApiModelProperty(value = "真实姓名", required = false)
	private String realName;
	
	@ApiModelProperty(value = "公司名称", required = false)
	private String companyName;
	
	@ApiModelProperty(value = "城市名称", required = false)
	private String cityName;
	
	@ApiModelProperty(value = "证件编号", required = false)
	private String idcardCode;

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(String idcardCode) {
		this.idcardCode = idcardCode;
	}

}
