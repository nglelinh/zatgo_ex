package com.zatgo.zup.suanli.mapper;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.suanli.entity.CampaignRegistration;

public interface CampaignRegistrationMapper {
    int deleteByPrimaryKey(Long regId);

    int insert(CampaignRegistration record);

    int insertSelective(CampaignRegistration record);

    CampaignRegistration selectByPrimaryKey(Long regId);

    int updateByPrimaryKeySelective(CampaignRegistration record);

    int updateByPrimaryKey(CampaignRegistration record);
    
    CampaignRegistration selectByUserIdAndPartyId(@Param("campaignId") Long campaignId, @Param("partyId") Long partyId);
}