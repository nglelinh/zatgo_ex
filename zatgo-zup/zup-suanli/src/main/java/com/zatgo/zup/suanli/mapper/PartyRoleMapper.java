package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.PartyRole;

public interface PartyRoleMapper {
    int deleteByPrimaryKey(Long partyRoleId);

    int insert(PartyRole record);

    int insertSelective(PartyRole record);

    PartyRole selectByPrimaryKey(Long partyRoleId);

    int updateByPrimaryKeySelective(PartyRole record);

    int updateByPrimaryKey(PartyRole record);
}