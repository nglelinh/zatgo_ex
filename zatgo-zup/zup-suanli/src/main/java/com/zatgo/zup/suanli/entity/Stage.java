package com.zatgo.zup.suanli.entity;

import java.util.Date;

public class Stage {
    private Byte stageType;

    private String stageName;

    private Date createTime;

    private String creator;

    private Date updateTime;

    public Byte getStageType() {
        return stageType;
    }

    public void setStageType(Byte stageType) {
        this.stageType = stageType;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName == null ? null : stageName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}