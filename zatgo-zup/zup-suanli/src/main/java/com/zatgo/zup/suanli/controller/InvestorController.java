package com.zatgo.zup.suanli.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.InvestorData;
import com.zatgo.zup.suanli.model.InvestorParams;
import com.zatgo.zup.suanli.model.InvestorPartyData;
import com.zatgo.zup.suanli.model.InvestorSearchParams;
import com.zatgo.zup.suanli.service.InvestorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/investor")
public class InvestorController extends BaseController {
	
	@Autowired
	private InvestorService investorService;
	
	@ApiOperation(value = "投资人列表")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseData<PageInfo<InvestorData>> getInvestorList(@ModelAttribute InvestorSearchParams params){
		params.setLanguage(getLanguage());
		PageInfo<InvestorData> info = investorService.getInvestorList(params);
		return BusinessResponseFactory.createSuccess(info);
	}
	
	@ApiOperation(value = "投资人详情")
	@RequestMapping(value = "/detail/{investorId}", method = RequestMethod.GET)
	public ResponseData<InvestorData> getInvestorDetail(@PathVariable Long investorId){
		String userId = getUserId();
		InvestorData investorData = investorService.getInvestorDetail(investorId, userId);
		return BusinessResponseFactory.createSuccess(investorData);
	}
	
	@ApiOperation(value = "投资人详情（后台使用）")
	@RequestMapping(value = "/party/detail/{investorId}", method = RequestMethod.GET)
	public ResponseData<InvestorPartyData> getPartyDetail(@PathVariable Long investorId){
		getAdminUserInfo();
		InvestorPartyData investorPartyData = investorService.getInvestorPartyDetail(investorId);
		return BusinessResponseFactory.createSuccess(investorPartyData);
	}
	
	@ApiOperation(value = "新增投资人")
	@RequestMapping(value = "/add", name = "新增活动", method = RequestMethod.POST)
	public ResponseData<InvestorData> addInvestor(@RequestBody InvestorParams params) {
		params.setCreator(getAdminUserInfo().getAdminName());
		InvestorData data = investorService.addInvestor(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "修改投资人")
	@RequestMapping(value = "/update", name = "修改投资人", method = RequestMethod.POST)
	public ResponseData<InvestorData> updateInvestor(@RequestBody InvestorParams params) {
		params.setCreator(getAdminUserInfo().getAdminName());
		InvestorData data = investorService.updateInvestor(params);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "删除投资人")
	@RequestMapping(value = "/delete/{investorId}", name = "删除活动", method = RequestMethod.DELETE)
	public ResponseData<Object> deleteInvestor(@PathVariable Long investorId) {
		getAdminUserInfo();
		investorService.deleteInvestor(investorId);
		return BusinessResponseFactory.createSuccess(null);
	}

}
