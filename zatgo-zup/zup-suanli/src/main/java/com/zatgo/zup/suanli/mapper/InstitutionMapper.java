package com.zatgo.zup.suanli.mapper;

import com.github.pagehelper.Page;
import com.zatgo.zup.suanli.entity.Institution;
import com.zatgo.zup.suanli.model.InstitutionData;
import com.zatgo.zup.suanli.model.InstitutionSearchParams;

public interface InstitutionMapper {
    int deleteByPrimaryKey(Long institutionId);

    int insert(Institution record);

    int insertSelective(Institution record);

    Institution selectByPrimaryKey(Long institutionId);

    int updateByPrimaryKeySelective(Institution record);

    int updateByPrimaryKey(Institution record);
    
    Page<InstitutionData> selectInstitutionListByParams(InstitutionSearchParams params);
    
    Institution selectInstitutionByName(String name);
    
    int insertInstitutionSelective(Institution record);
    
    Institution selectInstitutionByInvestorId(Long investorId);
}