package com.zatgo.zup.suanli.mapper;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.suanli.entity.PartyRoleRel;

public interface PartyRoleRelMapper {
    int deleteByPrimaryKey(Long partyRoleRelId);

    int insert(PartyRoleRel record);

    int insertSelective(PartyRoleRel record);

    PartyRoleRel selectByPrimaryKey(Long partyRoleRelId);

    int updateByPrimaryKeySelective(PartyRoleRel record);

    int updateByPrimaryKey(PartyRoleRel record);
    
    int insertPartyRoleRelSelective(PartyRoleRel record);
    
    PartyRoleRel selectPartyRoleRelByPartyIdAndPartyRoleId(@Param("partyId") Long partyId, @Param("partyRoleId") Long partyRoleId);
}