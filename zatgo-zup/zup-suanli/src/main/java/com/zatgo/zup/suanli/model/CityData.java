package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("城市数据")
public class CityData {
	
	@ApiModelProperty(value = "城市编码", required = true)
	private String cityCode;
	
	@ApiModelProperty(value = "城市名称", required = true)
	private String cityName;
	
	@ApiModelProperty(value = "省份", required = false)
	private String province;
	
	@ApiModelProperty(value = "国家", required = false)
	private String country;

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
