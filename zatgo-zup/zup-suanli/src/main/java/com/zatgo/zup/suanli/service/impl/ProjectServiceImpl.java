package com.zatgo.zup.suanli.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.SuanliEnum.ProjectStatus;
import com.zatgo.zup.common.enumtype.SuanliEnum.TagClassType;
import com.zatgo.zup.common.enumtype.SuanliEnum.UserFavoriteType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.entity.Catalog;
import com.zatgo.zup.suanli.entity.City;
import com.zatgo.zup.suanli.entity.Company;
import com.zatgo.zup.suanli.entity.PartyRoleRel;
import com.zatgo.zup.suanli.entity.Project;
import com.zatgo.zup.suanli.entity.ProjectCatalog;
import com.zatgo.zup.suanli.entity.ProjectMember;
import com.zatgo.zup.suanli.entity.Stage;
import com.zatgo.zup.suanli.entity.TagRel;
import com.zatgo.zup.suanli.entity.UserFavorite;
import com.zatgo.zup.suanli.mapper.CityMapper;
import com.zatgo.zup.suanli.mapper.CompanyMapper;
import com.zatgo.zup.suanli.mapper.PartyRoleRelMapper;
import com.zatgo.zup.suanli.mapper.ProjectCatalogMapper;
import com.zatgo.zup.suanli.mapper.ProjectMapper;
import com.zatgo.zup.suanli.mapper.ProjectMemberMapper;
import com.zatgo.zup.suanli.mapper.StageMapper;
import com.zatgo.zup.suanli.mapper.TagRelMapper;
import com.zatgo.zup.suanli.mapper.UserFavoriteMapper;
import com.zatgo.zup.suanli.model.InvestorCatalogData;
import com.zatgo.zup.suanli.model.ProjectData;
import com.zatgo.zup.suanli.model.ProjectParams;
import com.zatgo.zup.suanli.model.ProjectSearchParams;
import com.zatgo.zup.suanli.model.TagData;
import com.zatgo.zup.suanli.service.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {
	
	@Autowired
	private StageMapper stageMapper;
	
	@Autowired
	private ProjectMapper projectMapper;
	
	@Autowired
	private ProjectCatalogMapper projectCatalogMapper;

	@Autowired
	private PartyRoleRelMapper partyRoleRelMapper;
	
	@Autowired
	private TagRelMapper tagRelMapper;
	
	@Autowired
	private CityMapper cityMapper;
	
	@Autowired
	private CompanyMapper companyMapper;
	
	@Autowired
	private UserFavoriteMapper userFavoriteMapper;
	
	@Autowired
	private ProjectMemberMapper projectMemberMapper;

	@Override
	public PageInfo<ProjectData> getProjectList(ProjectSearchParams params) {
		params.setStatus(ProjectStatus.USING.getStatus());
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		
		if(params.getStageType() != null && !params.getStageType().equals(new Byte("0"))) {
			Stage stage = stageMapper.selectByPrimaryKey(params.getStageType());
			if(stage != null) {
				params.setStageName(stage.getStageName());
			}
		}
		
		Page<ProjectData> page = projectMapper.selectProjectByPage(params);
		PageInfo<ProjectData> res = new PageInfo<>(page.getPageNum(), page.getPageSize(), page.getTotal(), page.getResult());
		PageHelper.clearPage();
		return res;
	}

	@Override
	public ProjectData getProjectDetail(Long projectId, String userId) {
		if(projectId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "project id miss");
		}
		
		Project project = projectMapper.selectByPrimaryKey(projectId);
		if(project == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "project not exist");
		}
		
		ProjectData data = new ProjectData();
		BeanUtils.copyProperties(project, data);
		
		data.setIsCollect(false);
		if(StringUtils.isNotEmpty(userId)) {
			UserFavorite favorite = userFavoriteMapper.selectByIdAndType(userId, projectId, UserFavoriteType.PROJECT.getType());
			if(favorite != null) {
				data.setIsCollect(true);
			}
		}
		
		List<Catalog> catalogs = projectCatalogMapper.selectCatalogByProjectId(projectId);
		if(CollectionUtils.isNotEmpty(catalogs)) {
			List<InvestorCatalogData> catalogDatas = new ArrayList<InvestorCatalogData>();
			for (Catalog catalog : catalogs) {
				InvestorCatalogData catalogData = new InvestorCatalogData();
				BeanUtils.copyProperties(catalog, catalogData);
				catalogDatas.add(catalogData);
			}
			data.setCatalogDatas(catalogDatas);
		}
		
		List<TagData> tagDatas = tagRelMapper.selectTagDataByObjectId(projectId, TagClassType.PROJECT.getType());
		data.setTagDatas(tagDatas);
		return data;
	}

	@Transactional
	@Override
	public ProjectData addProject(ProjectParams params) {
		String cityCode = params.getCityCode();
		if(StringUtils.isEmpty(cityCode)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "city code miss");
		}
//		String cityName = params.getCityName();
//		if(StringUtils.isEmpty(cityName)) {
//			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "city name miss");
//		}
		Long companyId = params.getCompanyId();
		if(companyId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company id miss");
		}
//		String companyName = params.getCompanyName();
//		if(StringUtils.isEmpty(companyName)) {
//			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company name miss");
//		}
		Byte language = params.getLanguage();
		if(language == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "language miss");
		}
		String name = params.getName();
		if(StringUtils.isEmpty(name)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "name miss");
		}
		String stageName = params.getStageName();
		if(StringUtils.isEmpty(stageName)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "stage name miss");
		}
		Long partyId = params.getPartyId();
		if (partyId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party id miss");
		}
		
		Long partyRoleId = params.getPartyRoleId();
		if (partyRoleId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party role id miss");
		}
		
		Project project = projectMapper.selectProjectByName(name);
		if(project != null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "project exist");
		}
		
		String creator = params.getCreator();
		
		City city = cityMapper.selectByPrimaryKey(cityCode);
		String cityName = city.getCityName();
		Company company = companyMapper.selectByPrimaryKey(companyId);
		String companyName = company.getName();
		
		project = new Project();
		project.setCityCode(cityCode);
		project.setCityName(cityName);
		project.setCompanyId(companyId);
		project.setCompanyName(companyName);
		project.setContent(params.getContent());
		project.setCreateTime(new Date());
		project.setImgUrl(params.getImgUrl());
		project.setLanguage(language);
		project.setName(name);
		project.setShotDesc(params.getShotDesc());
		project.setStageName(stageName);
		project.setUpdateTime(new Date());
		project.setCreator(creator);
		projectMapper.insertProjectSelective(project);
		Long projectId = project.getProjectId();
		
		Long partyRoleRelId = null;
		PartyRoleRel partyRoleRel = partyRoleRelMapper.selectPartyRoleRelByPartyIdAndPartyRoleId(partyId, partyRoleId);
		if (partyRoleRel == null) {
			partyRoleRel = new PartyRoleRel();
			partyRoleRel.setPartyId(partyId);
			partyRoleRel.setPartyRoleId(partyRoleId);
			partyRoleRelMapper.insertPartyRoleRelSelective(partyRoleRel);
		}
		
		partyRoleRelId = partyRoleRel.getPartyRoleRelId();
		
		ProjectMember member = new ProjectMember();
		member.setPartyRoleRelId(partyRoleRelId);
		member.setProjectId(projectId);
		projectMemberMapper.insertSelective(member);
		
		List<Long> catalogIds = params.getCatalogIds();
		if(CollectionUtils.isNotEmpty(catalogIds)) {
			for (Long catalogId : catalogIds) {
				ProjectCatalog catalog = new ProjectCatalog();
				catalog.setCatalogId(catalogId);
				catalog.setProjectId(projectId);
				projectCatalogMapper.insertSelective(catalog);
			}
		}
		
		List<Long> tagIds = params.getTagIds();
		if(CollectionUtils.isNotEmpty(tagIds)) {
			for (Long tagId : tagIds) {
				TagRel rel = new TagRel();
				rel.setRelObjectId(projectId);
				rel.setRelObjectType(TagClassType.PROJECT.getType());
				rel.setTagId(tagId);
				tagRelMapper.insertSelective(rel);
			}
		}
		
		ProjectData data = new ProjectData();
		BeanUtils.copyProperties(project, data);
		
		return data;
	}

	@Transactional
	@Override
	public ProjectData updateProject(ProjectParams params) {
		Long projectId = params.getProjectId();
		if(projectId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "project id miss");
		}
		
		Project project = projectMapper.selectByPrimaryKey(projectId);
		if(project == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "project not exist");
		}
		
		project = new Project();
		String cityCode = params.getCityCode();
		if(StringUtils.isNotEmpty(cityCode)) {
			City city = cityMapper.selectByPrimaryKey(cityCode);
			project.setCityCode(params.getCityCode());
			project.setCityName(city.getCityName());
		}
		
		Long companyId = params.getCompanyId();
		if(companyId != null) {
			Company company = companyMapper.selectByPrimaryKey(companyId);
			project.setCompanyId(companyId);
			project.setCompanyName(company.getName());
		}
		
		project.setContent(params.getContent());
		project.setImgUrl(params.getImgUrl());
		project.setLanguage(params.getLanguage());
		project.setName(params.getName());
		project.setProjectId(projectId);
		project.setShotDesc(params.getShotDesc());
		project.setStageName(params.getStageName());
		project.setUpdateTime(new Date());
		
		projectMapper.updateByPrimaryKeySelective(project);
		
		List<Long> tagIds = params.getTagIds();
		if(CollectionUtils.isNotEmpty(tagIds)) {
			tagRelMapper.deleteTagRelByObjectId(projectId, TagClassType.PROJECT.getType());
			for (Long tagId : tagIds) {
				TagRel rel = new TagRel();
				rel.setRelObjectId(projectId);
				rel.setRelObjectType(TagClassType.PROJECT.getType());
				rel.setTagId(tagId);
				tagRelMapper.insertSelective(rel);
			}
		}
		
		ProjectData data = new ProjectData();
		BeanUtils.copyProperties(project, data);
		
		return data;
	}

	@Transactional
	@Override
	public void deleteProject(Long projectId) {
		if(projectId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "project id miss");
		}
		
		Project project = new Project();
		project.setProjectId(projectId);
		project.setStatus(ProjectStatus.DELETE.getStatus());
		projectMapper.updateByPrimaryKeySelective(project);
	}

	@Override
	public List<ProjectData> getProjectByUser(String userId) {
		
		return projectMapper.selectProjectByUserId(userId);
	}

	@Override
	public PageInfo<ProjectData> getProjectListAdmin(ProjectSearchParams params) {
		
		params.setStatus(ProjectStatus.USING.getStatus());
		PageHelper.startPage(params.getPageNo(), params.getPageSize());
		
		if(params.getStageType() != null && !params.getStageType().equals(new Byte("0"))) {
			Stage stage = stageMapper.selectByPrimaryKey(params.getStageType());
			if(stage != null) {
				params.setStageName(stage.getStageName());
			}
		}
		
		Page<ProjectData> page = projectMapper.selectProjectAdminByPage(params);
		PageHelper.clearPage();
		
		for (ProjectData projectData : page) {
			Long projectId = projectData.getProjectId();
			List<Catalog> catalogs = projectCatalogMapper.selectCatalogByProjectId(projectId);
			if(CollectionUtils.isNotEmpty(catalogs)) {
				List<InvestorCatalogData> catalogDatas = new ArrayList<InvestorCatalogData>();
				for (Catalog catalog : catalogs) {
					InvestorCatalogData catalogData = new InvestorCatalogData();
					BeanUtils.copyProperties(catalog, catalogData);
					catalogDatas.add(catalogData);
				}
				projectData.setCatalogDatas(catalogDatas);
			}
			
			List<TagData> tagDatas = tagRelMapper.selectTagDataByObjectId(projectId, TagClassType.PROJECT.getType());
			projectData.setTagDatas(tagDatas);
		}
		
		PageInfo<ProjectData> res = new PageInfo<>(page.getPageNum(), page.getPageSize(), page.getTotal(), page.getResult());
		
		return res;
	}

}
