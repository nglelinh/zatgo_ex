package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.ProjectMember;

public interface ProjectMemberMapper {
    int insert(ProjectMember record);

    int insertSelective(ProjectMember record);
}