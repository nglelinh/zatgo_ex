package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("活动报名参数")
public class CampaignRegistrationParams {
	
	@ApiModelProperty(value = "用户id", required = false, hidden = true)
	private String userId;
	
	@ApiModelProperty(value = "活动id", required = true)
	private Long campaignId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

}
