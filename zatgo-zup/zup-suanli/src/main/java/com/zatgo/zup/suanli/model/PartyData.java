package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "参与人身份信息")
public class PartyData {
	
	private Long partyId;
	
	private String partyName;
	
	private String realName;
	
	private String companyName;
	
	private String cityName;
	
	private Byte idcardType;
	
	private String idcardCode;

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Byte getIdcardType() {
		return idcardType;
	}

	public void setIdcardType(Byte idcardType) {
		this.idcardType = idcardType;
	}

	public String getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(String idcardCode) {
		this.idcardCode = idcardCode;
	}

}
