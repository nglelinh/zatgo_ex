package com.zatgo.zup.suanli.model;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("文件库")
public class FileLibraryData {
	
	@ApiModelProperty(value = "文件地址", required = true)
	private List<String> urls;
	
	@ApiModelProperty(value = "下页起始参数", required = true)
	private String nextMarker;
	
	@ApiModelProperty(value = "本页起始参数", required = true)
	private String marker;
	
	@ApiModelProperty(value = "是否查询完毕", required = true)
	private Boolean isTruncated;

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}

	public String getNextMarker() {
		return nextMarker;
	}

	public void setNextMarker(String nextMarker) {
		this.nextMarker = nextMarker;
	}

	public String getMarker() {
		return marker;
	}

	public void setMarker(String marker) {
		this.marker = marker;
	}

	public Boolean getIsTruncated() {
		return isTruncated;
	}

	public void setIsTruncated(Boolean isTruncated) {
		this.isTruncated = isTruncated;
	}

}
