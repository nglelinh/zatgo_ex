package com.zatgo.zup.suanli.model;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("企业参数")
public class CompanyParams {
	
	@ApiModelProperty(value = "企业id，修改使用", required = false)
	private Long companyId;
	
	@ApiModelProperty(value = "企业名称", required = false)
	private String name;
	
	@ApiModelProperty(value = "公司编码", required = false)
	private String code;
	
	@ApiModelProperty(value = "成立年份", required = false)
	private Date foundingYear;
	
	@ApiModelProperty(value = "税号", required = false)
	private String taxCode;
	
	@ApiModelProperty(value = "创建者", required = false, hidden = true)
	private String creator;

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getFoundingYear() {
		return foundingYear;
	}

	public void setFoundingYear(Date foundingYear) {
		this.foundingYear = foundingYear;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

}
