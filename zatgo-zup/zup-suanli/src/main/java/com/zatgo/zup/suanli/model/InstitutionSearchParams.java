package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("投资机构查询参数")
public class InstitutionSearchParams {
	
	@ApiModelProperty(value = "页码", required = true)
	private Integer pageNo;
	
	@ApiModelProperty(value = "数量", required = true)
	private Integer pageSize;
	
	@ApiModelProperty(value = "投资机构名称", required = false)
	private String name;
	
	@ApiModelProperty(value = "语言：0-中文；1-英文", required = false, allowableValues = "0,1")
	private Byte language;
	
	@ApiModelProperty(value = "状态：0-已删除；1-使用中", required = false, allowableValues = "0,1", hidden = true)
	private Byte status;

	public Integer getPageNo() {
		return pageNo == null ? 1 : pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize == null ? 10 : pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Byte getLanguage() {
		return language;
	}

	public void setLanguage(Byte language) {
		this.language = language;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

}
