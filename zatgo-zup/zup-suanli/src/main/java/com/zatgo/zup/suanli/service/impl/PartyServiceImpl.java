package com.zatgo.zup.suanli.service.impl;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.entity.City;
import com.zatgo.zup.suanli.entity.Company;
import com.zatgo.zup.suanli.entity.Party;
import com.zatgo.zup.suanli.entity.PartyInfo;
import com.zatgo.zup.suanli.mapper.CityMapper;
import com.zatgo.zup.suanli.mapper.CompanyMapper;
import com.zatgo.zup.suanli.mapper.PartyInfoMapper;
import com.zatgo.zup.suanli.mapper.PartyMapper;
import com.zatgo.zup.suanli.model.PartyData;
import com.zatgo.zup.suanli.model.PartyDetailData;
import com.zatgo.zup.suanli.model.PartyParams;
import com.zatgo.zup.suanli.model.PartySerchParams;
import com.zatgo.zup.suanli.service.PartyService;

@Service
public class PartyServiceImpl implements PartyService {

	@Autowired
	private PartyMapper partyMapper;

	@Autowired
	private PartyInfoMapper partyInfoMapper;

	@Autowired
	private CityMapper cityMapper;

	@Autowired
	private CompanyMapper companyMapper;

	@Transactional
	@Override
	public void addParty(String userId) {
		Party party = partyMapper.selectPartyByUserId(userId);
		if (party == null) {
			party = new Party();
			party.setUserId(userId);
			party.setCreateTime(new Date());
			party.setUpdateTime(new Date());
			party.setCreator("system");
			partyMapper.insertSelective(party);
		}
	}

	@Override
	public PageInfo<PartyData> getPartyList(PartySerchParams params) {
		if (params.getPageNo() != null) {
			PageHelper.startPage(params.getPageNo(), params.getPageSize());
		}

		Page<PartyData> datas = partyMapper.selectPartyListByParams(params);
		PageInfo<PartyData> info = new PageInfo<>(datas.getPageNum(), datas.getPageSize(), datas.getTotal(),
				datas.getResult());

		return info;
	}

	@Transactional
	@Override
	public void addParty(PartyParams params) {
		Long companyId = params.getCompanyId();
		if (companyId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "company id miss");
		}

		String cityCode = params.getCityCode();
		if (StringUtils.isEmpty(cityCode)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "city code miss");
		}
		
		String partyName = params.getPartyName();
		if (StringUtils.isEmpty(partyName)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party name miss");
		}

		String realName = params.getRealName();
		if (StringUtils.isEmpty(realName)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "real name miss");
		}
		String creator = params.getCreator();

		Long partyId = null;
		String userId = params.getUserId();
		if (StringUtils.isNotEmpty(userId)) {
			Party party = partyMapper.selectPartyByUserId(userId);
			if (party != null) {
				throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party exist");
			}
		}

		Party party = new Party();
		party.setCreateTime(new Date());
		party.setCreator(creator);
		party.setPartyName(partyName);
		party.setUpdateTime(new Date());
		party.setUserId(userId);
		partyMapper.insertPartySelective(party);
		partyId = party.getPartyId();

		City city = cityMapper.selectByPrimaryKey(cityCode);
		Company company = companyMapper.selectByPrimaryKey(companyId);

		PartyInfo partyInfo = new PartyInfo();
		partyInfo.setPartyId(partyId);
		partyInfo.setBirthday(params.getBirthday());
		partyInfo.setCityCode(cityCode);
		partyInfo.setCityName(city.getCityName());
		partyInfo.setCompanyId(companyId);
		partyInfo.setCompanyName(company.getName());
		partyInfo.setCreateTime(new Date());
		partyInfo.setCreator(creator);
		partyInfo.setEmail(params.getEmail());
		partyInfo.setEnName(params.getEnName());
		partyInfo.setIdcardCode(params.getIdcardCode());
		partyInfo.setIdcardType(params.getIdcardType());
		partyInfo.setName(realName);
		partyInfo.setNationality(params.getNationality());
		partyInfo.setPartyId(partyId);
		partyInfo.setPhone(params.getPhone());
		partyInfo.setPhoto(params.getPhoto());
		partyInfo.setSex(params.getSex());
		partyInfo.setUpdateTime(new Date());
		partyInfoMapper.insertSelective(partyInfo);
	}

	@Transactional
	@Override
	public void updateParty(PartyParams params) {

		Long partyId = params.getPartyId();
		if (partyId == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party id miss");
		}

		Long companyId = params.getCompanyId();
		String cityCode = params.getCityCode();
		String partyName = params.getPartyName();
		String realName = params.getRealName();
		String creator = params.getCreator();

		String userId = params.getUserId();
		if (StringUtils.isNotEmpty(userId)) {
			Party party = partyMapper.selectPartyByUserId(userId);
			if (party != null) {
				throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party exist");
			}
		}

		Party party = partyMapper.selectByPrimaryKey(partyId);
		if (party == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "party not exist");
		}

		party = new Party();
		party.setCreateTime(new Date());
		party.setCreator(creator);
		party.setPartyName(partyName);
		party.setUpdateTime(new Date());
		party.setUserId(userId);
		party.setPartyId(partyId);
		partyMapper.updateByPrimaryKeySelective(party);

		PartyInfo partyInfo = new PartyInfo();
		partyInfo.setPartyId(partyId);

		if (StringUtils.isNotEmpty(cityCode)) {
			City city = cityMapper.selectByPrimaryKey(cityCode);
			partyInfo.setCityName(city.getCityName());
		}

		if (companyId != null) {
			Company company = companyMapper.selectByPrimaryKey(companyId);
			partyInfo.setCompanyName(company.getName());
		}

		partyInfo.setBirthday(params.getBirthday());
		partyInfo.setCityCode(cityCode);

		partyInfo.setCompanyId(companyId);

		partyInfo.setCreateTime(new Date());
		partyInfo.setCreator(creator);
		partyInfo.setEmail(params.getEmail());
		partyInfo.setEnName(params.getEnName());
		partyInfo.setIdcardCode(params.getIdcardCode());
		partyInfo.setIdcardType(params.getIdcardType());
		partyInfo.setName(realName);
		partyInfo.setNationality(params.getNationality());
		partyInfo.setPartyId(partyId);
		partyInfo.setPhone(params.getPhone());
		partyInfo.setPhoto(params.getPhoto());
		partyInfo.setSex(params.getSex());
		partyInfo.setUpdateTime(new Date());
		partyInfoMapper.updateByPrimaryKeySelective(partyInfo);
	}

	@Override
	public PartyDetailData getPartyDetail(Long partyId) {
		return partyMapper.selectPartyDetailById(partyId);
	}

}
