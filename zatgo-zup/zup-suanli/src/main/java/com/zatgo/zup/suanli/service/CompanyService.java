package com.zatgo.zup.suanli.service;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.model.CompanyData;
import com.zatgo.zup.suanli.model.CompanyParams;
import com.zatgo.zup.suanli.model.CompanySearchParams;

public interface CompanyService {
	
	/**
	 * 查询企业列表
	 * @param params
	 * @return
	 */
	public PageInfo<CompanyData> getCompanyList(CompanySearchParams params);
	
	/**
	 * 新增企业
	 * @param params
	 * @return
	 */
	public CompanyData addCompany(CompanyParams params);
	
	/**
	 * 修改企业
	 * @param params
	 * @return
	 */
	public CompanyData updateCompany(CompanyParams params);
	
	/**
	 * 删除企业
	 * @param companyId
	 */
	public void deleteCompany(Long companyId);

}
