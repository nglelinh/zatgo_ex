package com.zatgo.zup.suanli.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.suanli.model.FavoriteParams;
import com.zatgo.zup.suanli.model.InvestorData;
import com.zatgo.zup.suanli.model.ProjectData;
import com.zatgo.zup.suanli.model.UserData;
import com.zatgo.zup.suanli.model.UserFavoriteParams;
import com.zatgo.zup.suanli.service.ProjectService;
import com.zatgo.zup.suanli.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/suanli", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/suanli/user")
public class UserController extends BaseController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private UserService userService;

	@ApiOperation(value = "项目库")
	@RequestMapping(value = "/project", method = RequestMethod.GET)
	public ResponseData<List<ProjectData>> getProjectList() {
		AuthUserInfo userInfo = getUserInfo();
		List<ProjectData> list = projectService.getProjectByUser(userInfo.getUserId());
		return BusinessResponseFactory.createSuccess(list);
	}

	@ApiOperation(value = "添加收藏")
	@RequestMapping(value = "/add/favorite", method = RequestMethod.POST)
	public ResponseData<Long> addUserFavorite(@RequestBody UserFavoriteParams params) {
		params.setUserId(getUserInfo().getUserId());
		Long id = userService.addUserFavorite(params);
		return BusinessResponseFactory.createSuccess(id);
	}

	@ApiOperation(value = "删除收藏")
	@RequestMapping(value = "/delete/favorite", method = RequestMethod.DELETE)
	public ResponseData<Long> deleteUserFavorite(@RequestBody UserFavoriteParams params) {
		params.setUserId(getUserInfo().getUserId());
		userService.deleteUserFavorite(params);
		return BusinessResponseFactory.createSuccess(null);
	}

	@ApiOperation(value = "收藏/关注的项目")
	@RequestMapping(value = "/favorite/project", method = RequestMethod.GET)
	public ResponseData<PageInfo<ProjectData>> getFavoriteProjectList(@ModelAttribute FavoriteParams params) {
		AuthUserInfo userInfo = getUserInfo();
		params.setUserId(userInfo.getUserId());
		PageInfo<ProjectData> info = userService.getFavoriteProjectList(params);
		return BusinessResponseFactory.createSuccess(info);
	}

	@ApiOperation(value = "收藏/关注的投资人")
	@RequestMapping(value = "/favorite/investor", method = RequestMethod.GET)
	public ResponseData<PageInfo<InvestorData>> getFavoriteInvestorList(@ModelAttribute FavoriteParams params) {
		AuthUserInfo userInfo = getUserInfo();
		params.setUserId(userInfo.getUserId());
		PageInfo<InvestorData> info = userService.getFavoriteInvestorList(params);
		return BusinessResponseFactory.createSuccess(info);
	}

	@ApiOperation(value = "获取投资人认证信息")
	@RequestMapping(value = "/investor", method = RequestMethod.GET)
	public ResponseData<Long> getUserInvestorId() {
		AuthUserInfo userInfo = getUserInfo();
		Long id = userService.getUserInvestor(userInfo.getUserId());
		return BusinessResponseFactory.createSuccess(id);
	}

	@ApiOperation(value = "获取用户关注数等信息")
	@RequestMapping(value = "/data", method = RequestMethod.GET)
	public ResponseData<UserData> getUserData() {
		AuthUserInfo userInfo = getUserInfo();
		UserData data = userService.getUserData(userInfo.getUserId());
		return BusinessResponseFactory.createSuccess(data);
	}

}
