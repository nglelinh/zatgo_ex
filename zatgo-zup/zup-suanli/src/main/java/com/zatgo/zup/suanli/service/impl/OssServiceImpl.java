package com.zatgo.zup.suanli.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.CreateBucketRequest;
import com.aliyun.oss.model.DeleteObjectsRequest;
import com.aliyun.oss.model.DeleteObjectsResult;
import com.aliyun.oss.model.ListObjectsRequest;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.aliyun.oss.model.ObjectMetadata;
import com.zatgo.zup.common.enumtype.BusinessEnum.OssBucket;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.suanli.model.DeleteFileParams;
import com.zatgo.zup.suanli.model.FileLibraryData;
import com.zatgo.zup.suanli.model.FileLibraryParams;
import com.zatgo.zup.suanli.model.UploadFileParams;
import com.zatgo.zup.suanli.service.OssService;

@Service
public class OssServiceImpl implements OssService {

	private static final Logger log = LoggerFactory.getLogger(OssServiceImpl.class);

	// #阿里云OSS
	@Value(value = "${oss.endpoint:}")
	private String endpoint;

	@Value(value = "${oss.accessKeyId:}")
	private String accessKeyId;

	@Value(value = "${oss.accessKeySecret:}")
	private String accessKeySecret;

	@Value(value = "${oss.bucketName:}")
	private String ossBucketName;

	@Value(value = "${oss.env:}")
	private String ossEnv;

	@Override
	public Map<String, String> uploagFile(UploadFileParams params) {

		OssBucket ossBucket = params.getOssBucket();
		if (ossBucket == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		List<MultipartFile> files = params.getFiles();
		if (CollectionUtils.isEmpty(files)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		String bucketName = ossBucketName;
		// createBucketName(bucketName);

		Map<String, String> data = new HashMap<String, String>();
		OSS client = new OSSClientBuilder().build("http://" + endpoint, accessKeyId, accessKeySecret);
		try {
			// 创建上传Object的Metadata
			ObjectMetadata meta = new ObjectMetadata();
			// 必须设置ContentLength
			meta.setCacheControl("no-cache");
			meta.setObjectAcl(CannedAccessControlList.PublicRead);
			for (MultipartFile multipartFile : files) {

				meta.setContentLength(multipartFile.getSize());

				String fileName = multipartFile.getOriginalFilename();
				String key = ossBucket.getName() + "-" + ossEnv + "/" + System.currentTimeMillis() + "-" + fileName;
				InputStream input = multipartFile.getInputStream();
				client.putObject(bucketName, key, input, meta);

				StringBuffer fileUrl = new StringBuffer().append("http://").append(bucketName).append(".")
						.append(endpoint.replaceFirst("-internal", "")).append("/").append(key);

				data.put(fileName, fileUrl.toString());
			}

		} catch (Exception e) {
			log.error("上传oss失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		} finally {
			client.shutdown();
		}

		return data;
	}

	/**
	 * 创建上传存储空间
	 * 
	 * @param bucketName
	 */
	private void createBucketName(String bucketName) {
		// 创建OSSClient实例。
		OSS client = new OSSClientBuilder().build("http://" + endpoint, accessKeyId, accessKeySecret);

		try {
			boolean exists = client.doesBucketExist(bucketName);
			if (!exists) {
				// 新建存储空间默认为标准存储类型，私有权限。
				// client.createBucket(bucketName);

				CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
				// 设置存储空间的权限为公共读，默认是私有。
				createBucketRequest.setCannedACL(CannedAccessControlList.PublicReadWrite);
				client.createBucket(createBucketRequest);
			}
		} catch (Exception e) {
			log.error("创建阿里云oss文件空间失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		} finally {
			// 关闭OSSClient。
			client.shutdown();
		}

	}

	@Override
	public void deleteFile(DeleteFileParams params) {
		OssBucket ossBucket = params.getOssBucket();
		if (ossBucket == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		List<String> fileUrlList = params.getFileUrlList();
		if (CollectionUtils.isEmpty(fileUrlList)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		String env = ossBucket.getName() + "-" + ossEnv;

		List<String> keys = new ArrayList<String>();
		fileUrlList.forEach(url -> {
			String fileName = url.substring(url.lastIndexOf(env));
			keys.add(fileName);
		});

		deleteOssFile(ossBucketName, keys);
	}

	private void deleteOssFile(String bucketName, List<String> keys) {
		OSS client = new OSSClientBuilder().build("http://" + endpoint, accessKeyId, accessKeySecret);
		try {

			if (keys.size() == 1) {
				String fileName = keys.get(0);
				boolean found = client.doesObjectExist(bucketName, fileName);
				if (found) {
					client.deleteObject(bucketName, fileName);
				}
			} else {
				DeleteObjectsResult deleteObjectsResult = client
						.deleteObjects(new DeleteObjectsRequest(bucketName).withKeys(keys));
				List<String> deletedObjects = deleteObjectsResult.getDeletedObjects();
				log.debug("详细删除信息：", JSON.toJSONString(deletedObjects));
			}

		} catch (Exception e) {
			log.error("删除oss文件失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		} finally {
			client.shutdown();
		}
	}

	@Override
	public FileLibraryData fileLibrary(FileLibraryParams params) {
		
		OssBucket ossBucket = params.getOssBucket();
		if (ossBucket == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		// 创建OSSClient实例。
		OSS client = new OSSClientBuilder().build("http://" + endpoint, accessKeyId, accessKeySecret);

		int maxKeys = params.getSize();
		ObjectListing objectListing;

		String bucketName = ossBucket.getName();
		String prefix = bucketName + "-" + ossEnv;

		ListObjectsRequest req = new ListObjectsRequest(ossBucketName).withPrefix(prefix).withMaxKeys(maxKeys);

		String nextMarker = params.getNextMarker();
		if (StringUtils.isNotEmpty(nextMarker)) {
			req.withMarker(nextMarker);
		}

//		req.setDelimiter("/");
		objectListing = client.listObjects(req);

		List<String> urls = new ArrayList<>();
		List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
		for (OSSObjectSummary s : sums) {
			StringBuffer fileUrl = new StringBuffer().append("http://").append(ossBucketName).append(".").append(endpoint)
					.append("/").append(s.getKey());

			urls.add(fileUrl.toString());
		}
		
		FileLibraryData data = new FileLibraryData();
		data.setIsTruncated(objectListing.isTruncated());
		data.setMarker(objectListing.getMarker());
		data.setNextMarker(objectListing.getNextMarker());
		data.setUrls(urls);

		// 关闭OSSClient。
		client.shutdown();
		return data;
	}

}
