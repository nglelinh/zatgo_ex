package com.zatgo.zup.suanli.mapper;

import com.github.pagehelper.Page;
import com.zatgo.zup.suanli.entity.Tag;
import com.zatgo.zup.suanli.model.TagData;
import com.zatgo.zup.suanli.model.TagSearchParams;

public interface TagMapper {
    int deleteByPrimaryKey(Long tagId);

    int insert(Tag record);

    int insertSelective(Tag record);

    Tag selectByPrimaryKey(Long tagId);

    int updateByPrimaryKeySelective(Tag record);

    int updateByPrimaryKey(Tag record);
    
    Page<TagData> selectTagByParams(TagSearchParams params);
    
    Tag selectTagByName(String name);
}