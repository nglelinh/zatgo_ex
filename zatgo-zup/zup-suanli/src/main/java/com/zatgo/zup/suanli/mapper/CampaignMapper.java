package com.zatgo.zup.suanli.mapper;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.zatgo.zup.suanli.entity.Campaign;
import com.zatgo.zup.suanli.model.CampaignData;
import com.zatgo.zup.suanli.model.CampaignSearchParams;

public interface CampaignMapper {
    int deleteByPrimaryKey(Long campaignId);

    int insert(Campaign record);

    int insertSelective(Campaign record);

    Campaign selectByPrimaryKey(Long campaignId);

    int updateByPrimaryKeySelective(Campaign record);

    int updateByPrimaryKey(Campaign record);
    
    Page<CampaignData> selectCampaignDataByPage(CampaignSearchParams params);
    
    CampaignData selectCampaignDetail(@Param("campaignId") Long campaignId);
    
    Campaign selectCampaignByName(String name);
    
    int insertCampaignSelective(Campaign record);
}