package com.zatgo.zup.suanli.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.suanli.entity.NeedMessage;
import com.zatgo.zup.suanli.entity.NeedMessageExample;
import com.zatgo.zup.suanli.mapper.NeedMessageMapper;
import com.zatgo.zup.suanli.service.NeedMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/9/2.
 */

@Service("needMessageService")
public class NeedMessageServiceImpl implements NeedMessageService {


    @Autowired
    private NeedMessageMapper needMessageMapper;

    @Override
    public void add(NeedMessage needMessage) {
        needMessage.setId(UUIDUtils.getUuid());
        needMessage.setCreateTime(new Date());
        needMessageMapper.insertSelective(needMessage);
    }

    @Override
    public void del(String id) {
        NeedMessage needMessage = new NeedMessage();
        needMessage.setId(id);
        needMessage.setDel(true);
        needMessageMapper.updateByPrimaryKey(needMessage);
    }

    @Override
    public void edit(NeedMessage needMessage) {
        needMessageMapper.updateByPrimaryKey(needMessage);
    }

    @Override
    public PageInfo<NeedMessage> list(Integer pageSzie, Integer pageNo, Integer type, Integer source) {
        NeedMessageExample example = new NeedMessageExample();
        example.setOrderByClause("create_time desc");
        NeedMessageExample.Criteria criteria = example.createCriteria();
        criteria.andIsDelEqualTo(false);
        // -1为全部 0为普通 1为伙伴
        if (type == null){
            type = 0;
        }
        if (source == null){
            source = 0;
        }
        if (type.intValue() != -1){
            criteria.andTypeEqualTo(type);
        }
        if (source.intValue() != -1){
            criteria.andSourcesEqualTo(source);
        }

        PageHelper.startPage(pageNo, pageSzie);
        List<NeedMessage> list = needMessageMapper.selectByExample(example);
        return new PageInfo<>(list);
    }
}
