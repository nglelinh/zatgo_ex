package com.zatgo.zup.suanli.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.zatgo.zup.common.enumtype.BusinessEnum.OssBucket;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("上传图片")
public class UploadFileParams {

	@ApiModelProperty(value = "上传空间名称： 活动-campaign；项目-project；投资人-investor；投资机构-institution；资讯-information；用户-user;用户证件-user_certificate", required = true, allowableValues = "campaign,project,investor,institution")
	private OssBucket ossBucket;

	@ApiModelProperty(value = "上传文件", required = true)
	private List<MultipartFile> files;

	public OssBucket getOssBucket() {
		return ossBucket;
	}

	public void setOssBucket(String ossBucket) {
		this.ossBucket = OssBucket.getEnumByType(ossBucket);
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

}
