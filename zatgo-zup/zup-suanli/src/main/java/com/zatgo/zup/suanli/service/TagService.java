package com.zatgo.zup.suanli.service;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.suanli.model.TagData;
import com.zatgo.zup.suanli.model.TagParams;
import com.zatgo.zup.suanli.model.TagSearchParams;

public interface TagService {
	
	/**
	 * 标签查询
	 * @param params
	 * @return
	 */
	public PageInfo<TagData> getTagList(TagSearchParams params);
	
	/**
	 * 新增tag
	 * @param params
	 * @return
	 */
	public TagData addTag(TagParams params);
	
	/**
	 * 修改tag
	 * @param params
	 * @return
	 */
	public TagData updateTag(TagParams params);
	
	/**
	 * 删除标签
	 * @param tagId
	 */
	public void deleteTag(Long tagId);

}
