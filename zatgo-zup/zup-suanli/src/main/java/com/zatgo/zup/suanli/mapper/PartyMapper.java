package com.zatgo.zup.suanli.mapper;

import com.github.pagehelper.Page;
import com.zatgo.zup.suanli.entity.Party;
import com.zatgo.zup.suanli.model.PartyData;
import com.zatgo.zup.suanli.model.PartyDetailData;
import com.zatgo.zup.suanli.model.PartySerchParams;

public interface PartyMapper {
    int deleteByPrimaryKey(Long partyId);

    int insert(Party record);

    int insertSelective(Party record);

    Party selectByPrimaryKey(Long partyId);

    int updateByPrimaryKeySelective(Party record);

    int updateByPrimaryKey(Party record);
    
    Party selectPartyByUserId(String userId);
    
    int insertPartySelective(Party record);
    
    Page<PartyData> selectPartyListByParams(PartySerchParams params);
    
    PartyDetailData selectPartyDetailById(Long partyId);
}