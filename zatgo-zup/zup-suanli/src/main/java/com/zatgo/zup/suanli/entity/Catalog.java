package com.zatgo.zup.suanli.entity;

import java.util.Date;

public class Catalog {
    private Long catalogId;

    private Long parentCatalogId;

    private String name;

    private Byte catalogType;

    private Byte catalogLevel;

    private Date createTime;

    private String creator;

    private Date updateTime;

    private Byte status;

    public Long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Long catalogId) {
        this.catalogId = catalogId;
    }

    public Long getParentCatalogId() {
        return parentCatalogId;
    }

    public void setParentCatalogId(Long parentCatalogId) {
        this.parentCatalogId = parentCatalogId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Byte getCatalogType() {
        return catalogType;
    }

    public void setCatalogType(Byte catalogType) {
        this.catalogType = catalogType;
    }

    public Byte getCatalogLevel() {
        return catalogLevel;
    }

    public void setCatalogLevel(Byte catalogLevel) {
        this.catalogLevel = catalogLevel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}