package com.zatgo.zup.suanli.entity;

public class TagRel {
    private Long id;

    private Long tagId;

    private Byte relObjectType;

    private Long relObjectId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public Byte getRelObjectType() {
        return relObjectType;
    }

    public void setRelObjectType(Byte relObjectType) {
        this.relObjectType = relObjectType;
    }

    public Long getRelObjectId() {
        return relObjectId;
    }

    public void setRelObjectId(Long relObjectId) {
        this.relObjectId = relObjectId;
    }
}