package com.zatgo.zup.suanli.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("添加用户关注")
public class UserFavoriteParams {
	
	@ApiModelProperty(value = "0：项目;1：投资人;2：投资机构;3：活动", required = true, allowableValues = "0,1,2,3")
	private Byte type;
	
	@ApiModelProperty(value = "id", required = true)
	private Long id;
	
	@ApiModelProperty(value = "用户id", required = false, hidden = true)
	private String userId;

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
