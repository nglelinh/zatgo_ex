package com.zatgo.zup.manage;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@SpringBootApplication
@EnableTurbine
//@EnableHystrixDashboard
@EnableHystrix
//@EnableZipkinServer
@EnableAdminServer
@EnableDiscoveryClient
public class ZupManageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZupManageApplication.class, args);
	}
}
