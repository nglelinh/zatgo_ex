package com.zatgo.zup.transaction.conf;

import com.zatgo.zup.common.model.CreateAddressRequest;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.transaction.entity.btc.Address;
import com.zatgo.zup.transaction.entity.btc.AddressTypeEnum;
import com.zatgo.zup.transaction.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.util.MongoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2018/11/2.
 */

@Component
public class UsdtConfig {

    private static final Logger logger = LoggerFactory.getLogger(UsdtConfig.class);

    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private SignRemotService signRemotService;


    @Value("${transaction.type}")
    private String type;


    @PostConstruct
    private void init(){
        if ("usdt".equalsIgnoreCase(type)){
            try {
                Query query = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.ADDRESS_CLASS_TYPE).is(AddressTypeEnum.SYSTEM_ADDRESS.getCode());
                query.addCriteria(criteria);
                List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
                if (CollectionUtils.isEmpty(data)){
                    CreateAddressRequest request = new CreateAddressRequest();
                    request.setNum(1);
                    request.setPassword(CoinConstant.SIGN_PASSWORD);
                    ResponseData<List<String>> responseData = signRemotService.getNewBtcAddresses(request);
                    if (responseData.isSuccessful()){
                        String address = responseData.getData().get(0);
                        Address a = new Address();
                        a.setCreateDate(new Date());
                        a.setAddress(address);
                        a.setType(AddressTypeEnum.SYSTEM_ADDRESS.getCode());
                        mongoUtil.insert(a, MongoConstant.ADDRESS_CLASS_NAME);
                    } else {
                        logger.error(responseData.getMessage());
                        logger.error("初始化usdt系统地址失败，拒绝启动");
                        Runtime.getRuntime().halt(-1);
                    }
                }
            } catch (Exception e){
                logger.error("", e);
                logger.error("初始化usdt系统地址失败，拒绝启动");
                Runtime.getRuntime().halt(-1);
            }
        }
    }
}
