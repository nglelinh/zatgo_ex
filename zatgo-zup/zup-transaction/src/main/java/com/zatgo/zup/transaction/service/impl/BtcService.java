package com.zatgo.zup.transaction.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CreateAddressRequest;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SignBTCTransactionParams;
import com.zatgo.zup.common.model.UnSpentBTCData;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.common.mq.MQProducer;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.transaction.entity.btc.*;
import com.zatgo.zup.transaction.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.entity.constant.Constants;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.entity.mq.NotifyCallbackMsg;
import com.zatgo.zup.transaction.entity.params.InsertRecordParam;
import com.zatgo.zup.transaction.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.remoteservice.WalletRemotService;
import com.zatgo.zup.transaction.service.CoinService;
import com.zatgo.zup.transaction.service.ThreadPoolTaskService;
import com.zatgo.zup.transaction.util.MongoUtil;
import com.zatgo.zup.transaction.util.btc.BitcoinUtil;
import com.zatgo.zup.transaction.util.btc.BtcUnSpentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by 46041 on 2018/10/10.
 */
@Service("btcService")
public class BtcService implements CoinService{

    private static final Logger logger = LoggerFactory.getLogger(BtcService.class);


    @Value("${transaction.height}")
    private Integer height;
    @Value("${transaction.fee}")
    private BigDecimal fee;
    @Value("${transaction.confirmed}")
    private Integer confirmed;
    @Value("${transaction.type}")
    private String type;
    @Value("${transaction.addressNum}")
    private Integer addressNum;


    @Autowired
    private BitcoinUtil bitcoinUtil;
    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private SignRemotService signRemotService;
    @Autowired
    private ThreadPoolTaskService btcThreadPoolTaskService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private WalletRemotService walletRemotService;
    @Autowired
    private MQProducer mqProducer;


    /**
     * 1、先校验输入金额
     * 2、交易from地址是否正确，避免找零到别人的地址
     * 3、获取未在交易地址上的未被花费的utxo
     * 4、拼数据去签名
     * 5、保存交易记录
     *
     * @param toAddress
     * @param fromAddress
     * @param amount
     * @param recordId
     * @return
     */
    @Override
    public void sendTransaction(String toAddress, String fromAddress, BigDecimal amount, String recordId, String coinType) {
        if (amount.compareTo(BigDecimal.ZERO) == -1){
            throw new BusinessException(BusinessExceptionCode.EXTRACT_MONEY_ERROR);
        }
        String extractLock = RedisKeyConstants.TRANSACTION_EXTRACT_LOCK + fromAddress;
        redisLockUtils.lock(extractLock);
        try {
            if (StringUtils.isEmpty(recordId)){
                logger.error("转账失败的业务Id：" + recordId);
                throw new BusinessException(BusinessExceptionCode.BUSINESS_TXID_NOT_IS_EMPTY);
            }
            if (hasRecordId(recordId) == null) {
                Map<String, Object> query = new HashMap();
                query.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, fromAddress);
                List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
                if (CollectionUtils.isEmpty(data)) {
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException(BusinessExceptionCode.EXTRACT_FROM_ADDRESS_NOT_EXIST);
                }
                //在所有的未被花费的utxo中找合适的utxo
                BtcUnSpentList btcUnSpentList = new BtcUnSpentList(mongoUtil, confirmed);
                BigDecimal count = amount.add(fee);
                List<UTXO> utxos = btcUnSpentList.get(count);
                if (CollectionUtils.isEmpty(utxos)) {
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
                }
                if (isBalanceNotEnough(amount.add(fee))){
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
                }
                for (UTXO utxo : utxos) {
                    count = count.subtract(utxo.getAmount());
                }
                if (count.compareTo(BigDecimal.ZERO) == 1) {
                    if (isBalanceNotEnough(amount.add(fee))){
                        logger.error("转账失败的业务Id：" + recordId);
                        throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
                    }
                }
                //获取了足够的utxo去签名
                ResponseData<String> responseData = signRemotService.btcSign(getParams(utxos, toAddress, CoinConstant.SIGN_PASSWORD, fromAddress, amount));
                if (!responseData.isSuccessful()) {
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException(BusinessExceptionCode.GO_TO_SIGN_ERROR);
                }
                String hex = responseData.getData();
                String txid = bitcoinUtil.sendTransaction(hex);
                //保存记录
                InsertRecordParam record = new InsertRecordParam();
                record.setAmount(amount);
                record.setFromAddress(fromAddress);
                record.setHex(hex);
                record.setTxid(txid);
                record.setRecordId(recordId);
                record.setPayList(utxos);
                record.setToAddress(toAddress);
                try {
                    insertTransactionRecord(record);
                } catch (ParseException e) {
                    logger.error("", e);
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException();
                }
            }
        } finally {
            redisLockUtils.releaseLock(extractLock);
        }
    }


    /**
     * 1、获取配置文件中和mongodb的mainchain表的中的区块高度，取最大值
     * 2、扫描此区块，获取该区块中的所有交易
     * 3、通过交易获取交易记录
     * 4、通过交易记录获取所有未被消费的utxo
     * 5、更新mainchain中的区块高度
     * 6、如果该条交易不是提币，那新增一条交易记录
     */
    @Override
    public void scanBlock() {
        //获取数据库中区块高度
        Integer maxValue = mongoUtil.getMaxValue("height", MongoConstant.MAIN_CHAIN_CLASS_NAME) + 1;
        maxValue = getMax(height, maxValue);
        Integer blockCount = bitcoinUtil.getBlockCount();
        //如果已经扫描到最高的区块了，休息10秒继续扫描
        if (maxValue - 1 == blockCount){
            try {
                logger.info("睡了10秒");
                Thread.sleep(10 * 1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return;
        }
        String blockHash = bitcoinUtil.getBlockHash(maxValue);
        List<String> blockTx = bitcoinUtil.getBlockTx(blockHash);
        Map<String, TransactionRecord> selfTransactionMap = getSelfTransaction(blockTx);
        List<Future> res = new ArrayList<>(blockTx.size());
        for (String txId : blockTx){
            Future<ResponseData<Object>> future = btcThreadPoolTaskService.tx(selfTransactionMap, txId, maxValue);
            res.add(future);
        }
        for (int i = 0; i < res.size() - 1; i++){
            try {
                Future<ResponseData<Object>> future = res.get(i);
                ResponseData<Object> result = future.get();
                if (!result.isSuccessful()){
                    Future<ResponseData<Object>> futureResult = btcThreadPoolTaskService.tx(selfTransactionMap, result.getMessage(), maxValue);
                    res.add(futureResult);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        MainChain mainChain = new MainChain();
        mainChain.setHeight(maxValue);
        mainChain.setHash(blockHash);
        mongoUtil.insert(mainChain, MongoConstant.MAIN_CHAIN_CLASS_NAME);
    }

    @Override
    public void checkTransaction() {
        List<TransactionRecord> unCheckTransction = BtcUnSpentList.getUnCheckTransction(confirmed, mongoUtil);
        for (TransactionRecord record : unCheckTransction){
            String txId = record.getTxId();
            TransactionRecord transaction = bitcoinUtil.getTransaction(txId);
            record.setConfirmed(transaction.getConfirmed());
        }
        //修改确认数
        batchExecuteUpdateConfirmed(unCheckTransction);
    }

    @Override
    public void GetBtcExtractRecord() {
        Map<String, String> map = new HashMap();
        map.put("symbol", "BTC");
        Map<String, ?> stringMap = walletRemotService.withdrawConsume(map);
        if (stringMap != null){
            String code = (String) stringMap.get("errno");
            if (Constants.CODE_SUCCESS.equals(code)){
                List<Map> withdrawResults = (List<Map>) stringMap.get("data");
                if (!CollectionUtils.isEmpty(withdrawResults)){
                    for (Map m : withdrawResults){
                        String fromAddress = (String) m.get("from_address");
                        String toAddress = (String) m.get("address_to");
                        BigDecimal amount = new BigDecimal(m.get("amount") + "");
//                        BigDecimal fee = new BigDecimal(m.get("fee") + "");
                        String transId = m.get("trans_id") + "";
                        sendTransaction(toAddress, fromAddress, amount, transId, "BTC");
                    }
                }
            }
        }
    }

    @Override
    public void collect() {

    }

    @Override
    @Async("scanExtractTxThreadPool")
    public void addAddress() {
        ResponseData<Integer> btc = walletRemotService.remainderAddress("BTC");
        if (!CoinConstant.createAddressLockMap.containsKey(CoinConstant.BTC_ADDRESS_LOCK)){
            CoinConstant.createAddressLockMap.put(CoinConstant.BTC_ADDRESS_LOCK, "");
            try {
                if (btc.getData() < 200){
                    CreateAddressRequest request = new CreateAddressRequest();
                    List<String> data = new ArrayList<>();
                    request.setPassword(CoinConstant.SIGN_PASSWORD);
                    request.setNum(100);
                    while (true){
                        try{
                            ResponseData<List<String>> newAddresses = signRemotService.getNewBtcAddresses(request);
                            data.addAll(newAddresses.getData());
                            if (addressNum <= data.size()){
                                break;
                            }
                        } catch (Exception e){
                            logger.error("", e);
                        }
                    }
                    List<Address> addresses = new ArrayList<>(data.size());
                    JSONArray array = new JSONArray();
                    Date date = new Date();
                    for (String a : data){
                        JSONObject object = new JSONObject();
                        object.put("address", a);
                        object.put("networkType", "BTC");
                        object.put("isUsed", 0);
                        object.put("create_time", date);
                        array.add(object);
                        Address address = new Address();
                        address.setType(AddressTypeEnum.MEMBER_ADDRESS.getCode());
                        address.setAddress(a);
                        address.setCreateDate(date);
                        addresses.add(address);
                    }
                    ResponseData<String> responseData = walletRemotService.addAddresses(array);
                    if (responseData.isSuccessful()){
                        mongoUtil.insert(addresses, MongoConstant.ADDRESS_CLASS_NAME);
                    }
                }
            } finally {
                CoinConstant.createAddressLockMap.remove(CoinConstant.BTC_ADDRESS_LOCK);
            }
        }
    }

    private Integer getMax(Integer v1, Integer v2){
        if (v1 == null){
            v1 = 1;
        }
        if (v2 == null){
            v2 = 1;
        }
        return v1 > v2 ? v1 : v2;
    }

    private SignBTCTransactionParams getParams(List<UTXO> payList, String toAddress,
                                               String password, String changeAddress, BigDecimal amount){
        SignBTCTransactionParams params = new SignBTCTransactionParams();
        BigDecimal unit = new BigDecimal("100000000");
        params.setAmount(amount.multiply(unit).longValue());
        params.setFee(fee.multiply(unit).longValue());
        params.setToAddress(toAddress);
        params.setChangeAddress(changeAddress);
        params.setPassword(password);
        List<UnSpentBTCData> list = new ArrayList<>();
        for (UTXO utxo : payList){
            UnSpentBTCData unSpentBTCData = new UnSpentBTCData();
            unSpentBTCData.setTxId(utxo.getTxId());
            unSpentBTCData.setHeight(utxo.getHeight());
            unSpentBTCData.setSatoshis(utxo.getAmount().multiply(unit).longValue());
            unSpentBTCData.setVout(Long.valueOf(utxo.getVout() + ""));
            unSpentBTCData.setScriptPubKey(utxo.getScriptPubKey());
            list.add(unSpentBTCData);
        }
        params.setUnSpentBtcs(list);
        return params;
    }

    private void insertTransactionRecord(InsertRecordParam param) throws ParseException {
        TransactionRecord record = new TransactionRecord();
        record.setCreateDate(new Date());
        record.setFee(fee);
        record.setConfirmed(0);
        record.setHex(param.getHex());
        record.setRecordId(param.getRecordId());
        record.setTxId(param.getTxid());
        record.setType(0);
        List<Vin> vins = new ArrayList<>();
        List<Vout> vouts = new ArrayList<>();
        BigDecimal count = BigDecimal.ZERO;
        List<UTXO> payList = param.getPayList();
        Query query = null;
        Criteria criteria = null;
        Update update = null;
        for (UTXO utxo : payList){
            Vin vin = new Vin();
            String utxoAddress = utxo.getAddress();
            vin.setAddress(utxoAddress);
            vin.setVout(utxo.getVout());
            vin.setTxId(utxo.getTxId());
            BigDecimal utxoAmount = utxo.getAmount();
            vin.setValue(utxoAmount);
            count = count.add(utxoAmount);
            vin.setScriptPubKey(utxo.getScriptPubKey());
            vins.add(vin);
            //地址的已确认金额扣钱
            query = new Query();
            criteria = new Criteria();
            criteria.and(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS).is(utxoAddress);
            query.addCriteria(criteria);
            update = new Update();
            BigDecimal addressBalance = getAddressBalance(utxoAddress);
            update.set(MongoConstant.ADDRESS_CLASS_NAME_UNCONFIRMED, addressBalance.add(utxoAmount.negate()).doubleValue());
            mongoUtil.update(query, MongoConstant.ADDRESS_CLASS_NAME, update);
            //修改utxo的状态
            query = new Query();
            criteria = new Criteria();
            criteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
            criteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
            query.addCriteria(criteria);
            update = new Update();
            update.set(MongoConstant.UTXO_CLASS_NAME_STATUS, 1);
            mongoUtil.update(query, MongoConstant.UTXO_CLASS_NAME, update);
        }
        BigDecimal amount = param.getAmount();
        Vout toVout = new Vout();
        toVout.setAddress(param.getToAddress());
        toVout.setValue(amount);
        vouts.add(toVout);
        BigDecimal subtract = count.subtract(amount).subtract(fee);
        if (subtract.compareTo(BigDecimal.ZERO) == 1){
            Vout changeVout = new Vout();
            changeVout.setAddress(param.getFromAddress());
            changeVout.setValue(subtract);
            vouts.add(changeVout);
        }
        record.setVinList(vins);
        record.setVoutList(vouts);
        Query q = new Query();
        Criteria c = new Criteria();
        c.and(MongoConstant.TRANSACTION_CLASS_TXID).is(record.getTxId());
        q.addCriteria(c);
        List<TransactionRecord> data = mongoUtil.getData(q, TransactionRecord.class, MongoConstant.TRANSACTION_CLASS);
        if (StringUtils.isEmpty(data)) {
            mongoUtil.insert(record, MongoConstant.TRANSACTION_CLASS);
        }
    }



    private Map<String, TransactionRecord> getSelfTransaction(List<String> txids){
        Map<String, TransactionRecord> map = new HashMap();
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.TRANSACTION_CLASS_TXID).in(txids);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.TRANSACTION_CLASS);
        if (!CollectionUtils.isEmpty(data)){
            for (TransactionRecord tx : data){
                map.put(tx.getTxId(), tx);
            }
        }
        return map;
    }

//    private void batchExecuteUpdateVin(Map<String, TransactionRecord> selfTransactionMap){
//        if (CollectionUtils.isEmpty(selfTransactionMap))
//            return;
//        Map<Query, Update> queryUpdateMap = new HashMap();
//        for (Map.Entry<String, TransactionRecord> map : selfTransactionMap.entrySet()){
//            TransactionRecord value = map.getValue();
//            Query query = new Query();
//            Criteria criteria = new Criteria();
//            criteria.and(MongoConstant.TRANSACTION_CLASS_TXID).is(value.getTxId());
//            query.addCriteria(criteria);
//            Update update = new Update();
//            update.set(value.getTxId(), value);
//            queryUpdateMap.put(query, update);
//        }
//        mongoUtil.batchExecuteUpdate(queryUpdateMap, MongoConstant.TRANSACTION_CLASS);
//    }


    private void batchExecuteUpdateConfirmed(List<TransactionRecord> list){
        Map<Query, Update> queryUpdateMap = new HashMap();
        if (CollectionUtils.isEmpty(list))
            return;
        for (TransactionRecord record : list){
            try{
                Query query = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.TRANSACTION_CLASS_TXID).is(record.getTxId());
                query.addCriteria(criteria);
                Update update = new Update();
                update.set(MongoConstant.TRANSACTION_CLASS_CONFIRMED, record.getConfirmed());
                //到达确认数的，修改地址余额
                if (confirmed <= record.getConfirmed()){
                    //通知钱包
                    Map<String, ?> res = null;
                    Integer type = record.getType();
                    Map<String, String> map = new HashMap();
                    if (type == 1){
                        map.put("timestamp", record.getBlockDate().getTime() + "");
                        map.put("symbol", "BTC");
                        map.put("txid", record.getTxId());
//                        map.put("amount", );
//                        map.put("address_to", );
                        map.put("confirm", record.getConfirmed() + "");
                        map.put("is_mining", "1");
                        setParam(map, record);
                        res = walletRemotService.depositNotify(map);
                    } else if (record.getType() == 0){
                        map.put("trans_id", record.getRecordId());
                        map.put("symbol", "BTC");
                        map.put("address_to", "0");
                        map.put("txid", record.getTxId());
                        map.put("amount", "0");
                        map.put("confirm", record.getConfirmed() + "");
                        map.put("real_fee", record.getFee().toString());
                        res = walletRemotService.withdrawNotify(map);
                    }
                    logger.info("钱包返回信息=====" + JSONObject.toJSONString(res));
                    if (res == null || !Constants.CODE_SUCCESS.equals(res.get("errno"))){
                        logger.error("通知钱包失败");
                        continue;
                    }
                    updateUnconfirmedAmount(record);
                }
                queryUpdateMap.put(query, update);
            } catch (Exception e){
                logger.error("", e);
            }
        }
        mongoUtil.batchExecuteUpdate(queryUpdateMap, MongoConstant.TRANSACTION_CLASS);
    }

    private void updateUnconfirmedAmount(TransactionRecord record) {
        List<String> addressList = new ArrayList<>();
        List<Vin> vinList = record.getVinList();
        for (Vin vin : vinList){
            addressList.add(vin.getAddress());
        }
        List<Vout> vouts = record.getVoutList();
        for (Vout vout : vouts){
            addressList.add(vout.getAddress());
        }
        Map<String, Collection<String>> query = new HashMap<>();
        query.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, addressList);
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        if (!CollectionUtils.isEmpty(addresses)){
            for (Vin vin : vinList){
                String address = vin.getAddress();
                for (Address a : addresses){
                    if (a.getAddress().equals(address)){
                        //扣钱
                        Query q = new Query();
                        Criteria criteria = new Criteria();
                        criteria.and(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS).is(address);
                        q.addCriteria(criteria);
                        Update update = new Update();
                        Address inAddress = bitcoinUtil.getAddress(address);
                        update.set(MongoConstant.ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(inAddress.getUnconfirmed() + "").add(vin.getValue()).doubleValue());
                        update.set(MongoConstant.ADDRESS_CLASS_NAME_CONFIRMED, new BigDecimal(inAddress.getConfirmed() + "").add(vin.getValue().negate()).doubleValue());
                        mongoUtil.update(q, MongoConstant.ADDRESS_CLASS_NAME, update);
                        //修改utxo的状态
                        Query utxoQuery = new Query();
                        Criteria utxoCriteria = new Criteria();
                        utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(vin.getTxId());
                        utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(vin.getVout());
                        utxoQuery.addCriteria(utxoCriteria);
                        Update utxoUpdate = new Update();
                        utxoUpdate.set(MongoConstant.UTXO_CLASS_NAME_STATUS, 2);
                        mongoUtil.update(utxoQuery, MongoConstant.UTXO_CLASS_NAME, utxoUpdate);
                    }
                }
            }
            for (Vout vout : vouts){
                String address = vout.getAddress();
                for (Address a : addresses){
                    if (a.getAddress().equals(address)){
                        //加钱
                        Query q = new Query();
                        Criteria criteria = new Criteria();
                        criteria.and(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS).is(address);
                        q.addCriteria(criteria);
                        Update update = new Update();
                        Address outAddress = bitcoinUtil.getAddress(address);
                        update.set(MongoConstant.ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(outAddress.getUnconfirmed() + "").add(vout.getValue().negate()).doubleValue());
                        update.set(MongoConstant.ADDRESS_CLASS_NAME_CONFIRMED, new BigDecimal(outAddress.getConfirmed() + "").add(vout.getValue()).doubleValue());
                        mongoUtil.update(q, MongoConstant.ADDRESS_CLASS_NAME, update);
                        //修改utxo的状态
                        if (TransactionTypeEnum.EXTRACT.getCode().equals(type)){
                            UTXO utxo = bitcoinUtil.getUtxo(record, vout);
                            utxo.setStatus(0);
                            mongoUtil.insert(utxo, MongoConstant.UTXO_CLASS_NAME);
                        } else {
                            Query utxoQuery = new Query();
                            Criteria utxoCriteria = new Criteria();
                            utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(record.getTxId());
                            utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(vout.getVout());
                            utxoQuery.addCriteria(utxoCriteria);
                            Update utxoUpdate = new Update();
                            utxoUpdate.set(MongoConstant.UTXO_CLASS_NAME_STATUS, 0);
                            mongoUtil.update(utxoQuery, MongoConstant.UTXO_CLASS_NAME, utxoUpdate);
                        }
                    }
                }
            }
        }
    }

    private TransactionRecord hasRecordId(String recordId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.TRANSACTION_CLASS_RECORD_ID).is(recordId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }

    private BigDecimal getAddressBalance(String address){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS).is(address);
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        Double confirmed = data.get(0).getConfirmed();
        return confirmed == null ? BigDecimal.ZERO : new BigDecimal(confirmed + "");
    }


    /**
     * 获取充值地址和充值金额专用
     * @param map
     * @param record
     */
    private void setParam(Map<String, String> map, TransactionRecord record){
        List<Vout> voutList = record.getVoutList();
        Map<String, Collection<String>> query = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (Vout vout : voutList){
            String address = vout.getAddress();
            if (address != null){
                list.add(address);
            }
        }
        query.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, list);
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        if (CollectionUtils.isEmpty(addresses))
            throw new BusinessException();
        //充值地址
        String depositAddress = addresses.get(0).getAddress();
        if (addresses.size() == 2){
            Query addressQuery = new Query();
            Criteria addressCriteria = new Criteria();
            addressCriteria.and(MongoConstant.ADDRESS_CLASS_TYPE).is(0);
            addressQuery.addCriteria(addressCriteria);
            List<Address> data = mongoUtil.getData(addressQuery, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
            if (CollectionUtils.isEmpty(data)){
                throw new BusinessException();
            }
            Address systemAddress = data.get(0);
            String address = systemAddress.getAddress();
            if (address.equals(addresses.get(0).getAddress())){
                depositAddress = addresses.get(1).getAddress();
            } else {
                depositAddress = addresses.get(0).getAddress();
            }
        }
        map.put("address_to", depositAddress);
        BigDecimal amount = BigDecimal.ZERO;
        for (Vout vout : voutList){
            String address = vout.getAddress();
            if (address != null && depositAddress.equals(vout.getAddress())){
                amount = vout.getValue();
            }
        }
        map.put("amount", amount.toString());
    }

    private boolean isBalanceNotEnough(BigDecimal amount){
        BtcUnSpentList addressUsedBtcUnSpentList = new BtcUnSpentList(mongoUtil, confirmed, null);
        List<UTXO> usedUtxos = addressUsedBtcUnSpentList.get(amount);
        if (CollectionUtils.isEmpty(usedUtxos)){
            return true;
        }
        for (UTXO utxo : usedUtxos) {
            amount = amount.subtract(utxo.getAmount());
        }
        if (amount.compareTo(BigDecimal.ZERO) == 1) {
            return true;
        }
        return false;
    }

}
