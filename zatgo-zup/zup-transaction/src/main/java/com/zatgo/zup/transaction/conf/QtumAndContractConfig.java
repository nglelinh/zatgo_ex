package com.zatgo.zup.transaction.conf;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.transaction.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.entity.qtum.Address;
import com.zatgo.zup.transaction.entity.qtum.AddressTypeEnum;
import com.zatgo.zup.transaction.util.MongoUtil;
import com.zatgo.zup.transaction.util.qtum.QtumUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by 46041 on 2018/10/25.
 */

@Component
@ConfigurationProperties(prefix="transaction.qtum")
public class QtumAndContractConfig {

    private List<Map<String, String>> config = new ArrayList<>();

    private static Map<String, ContractConfig> contractConfig = null;

    private static Map<String, ContractConfig> coinTypeConfig = null;
    @Autowired
    private QtumUtil qtumUtil;
    @Autowired
    private MongoUtil mongoUtil;


    @Value("${transaction.confirmed}")
    private Integer qtumConfirmed;
    @Value("${transaction.fee}")
    private BigDecimal fee;
    @Value("${transaction.type}")
    private String type;

    public Map<String, ContractConfig> getContractConfig(){
        if (contractConfig == null){
            init();
        }
        return contractConfig;
    }

    public Map<String, ContractConfig> getCoinTypeConfig(){
        if (coinTypeConfig == null){
            init();
        }
        return coinTypeConfig;
    }

    @PostConstruct
    private void init(){
        if ("qtum".equalsIgnoreCase(type)){
            if (contractConfig == null){
                if (!CollectionUtils.isEmpty(config)){
                    contractConfig = new HashMap<>();
                    coinTypeConfig = new HashMap<>();
                    for (Map<String, String> conf : config){
                        ContractConfig config = JSONObject.parseObject(JSONObject.toJSONString(conf), ContractConfig.class) ;
                        config.setName(config.getName().toUpperCase());
                        config.setContract(config.getContract());
                        Address systemAddress = qtumUtil.getSystemAddress(config.getName());
                        if (systemAddress != null){
                            config.setSystemAddress(systemAddress.getAddress());
                        } else {
                            systemAddress = createTokenAddress(config.getName());
                            config.setSystemAddress(systemAddress.getAddress());
                        }
                        Address systemServiceChargeAddress = qtumUtil.getSystemCollectAddress();
                        config.setSystemServiceChargeAddress(systemServiceChargeAddress.getAddress());
//                        config.setGasprice(qtumUtil.qtumAmountFormateSumN(new BigDecimal(config.getGasprice()), new BigInteger("9")).toBigInteger());
                        contractConfig.put(config.getContract(), config);
                        coinTypeConfig.put(config.getName(), config);
                    }
                }
            }
            putQtumConfig();
        }
    }

    private void putQtumConfig(){
        ContractConfig config = new ContractConfig();
        config.setName("QTUM");
        config.setSystemAddress(qtumUtil.getSystemAddress("QTUM").getAddress());
        config.setConfirmed(qtumConfirmed);
        config.setPrecision(CoinConstant.ETH_PRECISION);
        config.setSystemServiceChargeAddress(qtumUtil.getSystemCollectAddress().getAddress());
        config.setFee(fee);
        contractConfig.put(config.getContract(), config);
        coinTypeConfig.put(config.getName(), config);
    }

    private Address createTokenAddress(String coinType){
        Address address = new Address();
        Address qtum = qtumUtil.getSystemAddress("QTUM");
        if (qtum == null){
            List<String> addresses = qtumUtil.getAddress(2);
            if (!CollectionUtils.isEmpty(addresses)){
                List<Address> list = new ArrayList<>(2);
                qtum = new Address();
                qtum.setCreateDate(new Date());
                qtum.setAddress(addresses.get(0));
                qtum.setType(0);
                qtum.setUnconfirmed(BigDecimal.ZERO.doubleValue());
                qtum.setConfirmed(BigDecimal.ZERO.doubleValue());
                list.add(qtum);
                Address collectAddress = new Address();
                collectAddress.setAddress(addresses.get(1));
                collectAddress.setType(2);
                collectAddress.setCreateDate(new Date());
                collectAddress.setConfirmed(BigDecimal.ZERO.doubleValue());
                collectAddress.setUnconfirmed(BigDecimal.ZERO.doubleValue());
                list.add(collectAddress);
                mongoUtil.insert(list, qtumUtil.getTokenAddressName("QTUM"));
            } else {
                throw new BusinessException(BusinessExceptionCode.GET_ADDRESS_FAIL);
            }
        }
        address.setAddress(qtum.getAddress());
        address.setType(AddressTypeEnum.SYSTEM_ADDRESS.getCode());
        address.setCreateDate(new Date());
        address.setConfirmed(BigDecimal.ZERO.doubleValue());
        address.setUnconfirmed(BigDecimal.ZERO.doubleValue());
        mongoUtil.insert(address, qtumUtil.getTokenAddressName(coinType));
        return qtum;
    }

    public List<Map<String, String>> getConfig() {
        return config;
    }

    public void setConfig(List<Map<String, String>> config) {
        this.config = config;
    }
}
