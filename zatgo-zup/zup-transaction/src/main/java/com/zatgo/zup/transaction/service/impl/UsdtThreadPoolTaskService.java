//package com.zatgo.zup.transaction.service.impl;
//
//import com.alibaba.fastjson.JSONObject;
//import com.zatgo.zup.common.exception.BusinessResponseFactory;
//import com.zatgo.zup.common.model.ResponseData;
//import com.zatgo.zup.transaction.entity.BaseTransactionRecord;
//import com.zatgo.zup.transaction.entity.usdt.Address;
//import com.zatgo.zup.transaction.entity.usdt.TransactionRecord;
//import com.zatgo.zup.transaction.entity.usdt.UTXO;
//import com.zatgo.zup.transaction.entity.constant.MongoConstant;
//import com.zatgo.zup.transaction.service.ThreadPoolTaskService;
//import com.zatgo.zup.transaction.util.MongoUtil;
//import com.zatgo.zup.transaction.util.usdt.UsdtUtil;
//import org.apache.commons.collections.CollectionUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.data.mongodb.core.query.Update;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.scheduling.annotation.AsyncResult;
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//
//import java.math.BigDecimal;
//import java.util.*;
//import java.util.concurrent.Future;
//
///**
// * Created by 46041 on 2018/10/24.
// */
//
//@Service("usdtThreadPoolTaskService")
//public class UsdtThreadPoolTaskService implements ThreadPoolTaskService {
//    private static final Logger logger = LoggerFactory.getLogger(UsdtThreadPoolTaskService.class);
//
//    @Autowired
//    private UsdtUtil usdtUtil;
//    @Autowired
//    private MongoUtil mongoUtil;
//    @Value("${transaction.confirmed}")
//    private Integer confirmed;
//
//    @Override
//    @Async("threadPoolTask")
//    public Future<ResponseData<Object>> tx(Map<String, ? extends BaseTransactionRecord> selfTransactionMap, String txId, Integer maxValue){
//        ResponseData<Object> result = null;
//        try {
//            TransactionRecord transaction = usdtUtil.getTransaction(txId, Integer.valueOf(maxValue + ""));
//            //获取所有utxo
//            List<UTXO> unSpentUTXO = usdtUtil.getUnSpentUTXO(transaction);
//            //保存自己的utxo，返回是否保存成功
//            boolean isSelfTx = saveDepositUTXOInfo(unSpentUTXO, transaction.getConfirmed());
//            //更新已经存在提币的交易
//            if (selfTransactionMap.containsKey(txId)){
//                TransactionRecord record = (TransactionRecord) selfTransactionMap.get(txId);
//                transaction.setVoutList(record.getVoutList());
//                transaction.setRecordId(record.getRecordId());
//                transaction.setFee(record.getFee());
//                Map<String, Object> map = new HashMap();
//                map.put(MongoConstant.TRANSACTION_CLASS_TXID, txId);
//                mongoUtil.delte(map, MongoConstant.TRANSACTION_CLASS);
//                mongoUtil.insert(transaction, MongoConstant.TRANSACTION_CLASS);
//            } else if (isSelfTx){
//                //如果是自己的充值交易
//                mongoUtil.insert(transaction, MongoConstant.TRANSACTION_CLASS);
//            }
//            result = BusinessResponseFactory.createSuccess(null);
//        } catch (Exception e){
//            logger.error("", e);
//            result = BusinessResponseFactory.createBusinessError("-1", txId);
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
//        return new AsyncResult(result);
//    }
//
//    @Override
//    @Async("scanExtractTxThreadPool")
//    public Future<ResponseData<Object>> scanExtractTx(Map<String, ? extends BaseTransactionRecord> selfTransactionMap, String txId, Integer maxValue) {
//        ResponseData<Object> result = null;
//        try {
//            TransactionRecord transaction = usdtUtil.getTransaction(txId, Integer.valueOf(maxValue + ""));
//            //获取所有utxo
//            List<UTXO> spentUTXO = usdtUtil.getSpentUTXO(transaction);
//            //更新utxo并查看是否为自己的交易
//            boolean isSelfTx = updateExtractUTXOInfo(spentUTXO, transaction.getConfirmed());
//            //将提币交易记录过滤掉
//            if (selfTransactionMap.containsKey(txId)){
//                TransactionRecord record = (TransactionRecord) selfTransactionMap.get(txId);
//                transaction.setVoutList(record.getVoutList());
//                transaction.setRecordId(record.getRecordId());
//                transaction.setFee(record.getFee());
//                Map<String, Object> map = new HashMap();
//                map.put(MongoConstant.TRANSACTION_CLASS_TXID, txId);
//                mongoUtil.delte(map, MongoConstant.TRANSACTION_CLASS);
//                mongoUtil.insert(transaction, MongoConstant.TRANSACTION_CLASS);
//            } else if (isSelfTx){
//                //如果是自己的提币交易
//                mongoUtil.insert(transaction, MongoConstant.TRANSACTION_CLASS);
//            }
//            result = BusinessResponseFactory.createSuccess(null);
//        } catch (Throwable e){
//            logger.error("", e);
//            result = BusinessResponseFactory.createBusinessError("-1", txId);
//        }
//        return new AsyncResult(result);
//    }
//
//    private boolean saveDepositUTXOInfo(List<UTXO> utxos, Integer confirmed){
//        if (CollectionUtils.isEmpty(utxos))
//            return false;
//        Map<String, Collection<String>> query = new HashMap<>();
//        List<String> list = new ArrayList<>();
//        for (UTXO utxo : utxos){
//            String address = utxo.getAddress();
//            if (address != null){
//                list.add(address);
//            }
//        }
//        query.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, list);
//        //查询是否为自己的地址
//        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
//        if (!CollectionUtils.isEmpty(addresses)){
//            int i = utxos.size() - 1;
//            for ( ; i >= 0; i--){
//                UTXO utxo = utxos.get(i);
//                String utxoAddress = utxo.getAddress();
//                if (!hasAddress(addresses, utxoAddress)){
//                    continue;
//                }
//                Query utxoQuery = new Query();
//                Criteria criteria = new Criteria();
//                criteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
//                criteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
//                utxoQuery.addCriteria(criteria);
//                List<UTXO> data = mongoUtil.getData(utxoQuery, UTXO.class, MongoConstant.UTXO_CLASS_NAME);
//                //防止重复添加utxo
//                if (CollectionUtils.isEmpty(data)){
//                    Address address = usdtUtil.getAddress(utxo.getAddress());
//                    Map addBalance = new HashMap();
//                    Update update = new Update();
//                    try {
//                        //并加钱  确认数到了的改交易记录状态
//                        if (confirmed >= this.confirmed){
//                            update.set(MongoConstant.ADDRESS_CLASS_NAME_CONFIRMED, new BigDecimal(address.getConfirmed() + "").add(utxo.getAmount()).doubleValue());
//                            utxo.setStatus(0);
//                        } else {
//                            utxo.setStatus(1);
//                            update.set(MongoConstant.ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(address.getUnconfirmed() + "").add(utxo.getAmount()).doubleValue());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    addBalance.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, utxoAddress);
//                    mongoUtil.update(addBalance, MongoConstant.ADDRESS_CLASS_NAME, update);
//                    //保存utxo
//                    mongoUtil.insert(utxo, MongoConstant.UTXO_CLASS_NAME);
//                }
//            }
//            return true;
//        }
//        return false;
//    }
//
//
//    /**
//     * 提币时候用
//     * @param utxos
//     * @param confirmed
//     * @return
//     */
//    private boolean updateExtractUTXOInfo(List<UTXO> utxos, Integer confirmed){
//        if (CollectionUtils.isEmpty(utxos))
//            return false;
//        Map<String, Collection<String>> query = new HashMap<>();
//        List<String> list = new ArrayList<>();
//        for (UTXO utxo : utxos){
//            String txId = utxo.getTxId();
//            if (!StringUtils.isEmpty(txId)){
//                list.add(txId);
//            }
//        }
//        query.put(MongoConstant.UTXO_CLASS_NAME_TXID, list);
//        //查询是否为自己的地址
//        List<UTXO> utxoList = mongoUtil.getDataByListQuery(query, UTXO.class, MongoConstant.UTXO_CLASS_NAME);
//        if (!CollectionUtils.isEmpty(utxoList)){
//            int i = utxos.size() - 1;
//            for ( ; i >= 0; i--){
//                UTXO utxo = utxos.get(i);
//                utxo = hasUTXO(utxoList, utxo);
//                if (utxo == null){
//                    continue;
//                }
//                Query utxoQuery = new Query();
//                Criteria utxoCriteria = new Criteria();
//                utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
//                utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
//                utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_STATUS).is(2);
//                utxoQuery.addCriteria(utxoCriteria);
//                List<UTXO> data = mongoUtil.getData(utxoQuery, UTXO.class, MongoConstant.UTXO_CLASS_NAME);
//                if (CollectionUtils.isEmpty(data)) {
//                    //更新地址余额
//                    Map addBalance = new HashMap();
//                    Update update = new Update();
//                    try {
//                        Address address = usdtUtil.getAddress(utxo.getAddress());
//                        if (confirmed >= this.confirmed) {
//                            try {
//                                update.set(MongoConstant.ADDRESS_CLASS_NAME_CONFIRMED, new BigDecimal(address.getConfirmed() + "").subtract(utxo.getAmount()).doubleValue());
//                            } catch (Exception e) {
//                                logger.error(JSONObject.toJSONString(utxo));
//                                logger.error("", e);
//                            }
//                            Query updateUTXOQuery = new Query();
//                            Criteria criteria = new Criteria();
//                            criteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
//                            criteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
//                            updateUTXOQuery.addCriteria(criteria);
//                            Update updateUTXO = new Update();
//                            updateUTXO.set(MongoConstant.UTXO_CLASS_NAME_STATUS, 2);
//                            //更新utxo状态
//                            mongoUtil.update(updateUTXOQuery, MongoConstant.UTXO_CLASS_NAME, updateUTXO);
//                        } else {
//                            update.set(MongoConstant.ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(address.getUnconfirmed() + "").subtract(utxo.getAmount()).doubleValue());
//                        }
//                    } catch (Exception e) {
//                        logger.error("", e);
//                    }
//                    addBalance.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, utxo.getAddress());
//                    mongoUtil.update(addBalance, MongoConstant.ADDRESS_CLASS_NAME, update);
//                }
//            }
//            return true;
//        }
//        return false;
//    }
//
//
//
//    private boolean hasAddress(List<Address> addresses, String find){
//        for (Address address : addresses){
//            if (address.getAddress().equals(find)){
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private UTXO hasUTXO(List<UTXO> utxos, UTXO find){
//        if (find == null)
//            return null;
//        for (UTXO utxo : utxos){
//            if (utxo.getTxId().equals(find.getTxId()) && utxo.getVout().intValue() == find.getVout().intValue()){
//                return utxo;
//            }
//        }
//        return null;
//    }
//}
