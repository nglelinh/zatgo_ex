package com.zatgo.zup.transaction.entity.params;

import com.zatgo.zup.transaction.entity.qtum.UTXO;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 46041 on 2018/10/16.
 */
public class InsertQtumRecordParam {

    private String txid;

    private List<UTXO> payList;

    private String hex;

    private String recordId;

    private String toAddress;

    private String fromAddress;

    private BigDecimal amount;

    private String coinType;

    private BigDecimal gasPrice;

    private BigDecimal gasLimit;


    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public List<UTXO> getPayList() {
        return payList;
    }

    public void setPayList(List<UTXO> payList) {
        this.payList = payList;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public BigDecimal getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(BigDecimal gasPrice) {
        this.gasPrice = gasPrice;
    }

    public BigDecimal getGasLimit() {
        return gasLimit;
    }

    public void setGasLimit(BigDecimal gasLimit) {
        this.gasLimit = gasLimit;
    }
}
