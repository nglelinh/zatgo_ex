package com.zatgo.zup.transaction.controller;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.transaction.service.impl.EthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 46041 on 2018/11/8.
 */

@RestController
@Api(value="/eth", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/eth")
public class RepairDataController {

    @Autowired
    private EthService ethService;


    @ApiOperation(value = "重置eth提币失败的交易")
    @RequestMapping(value = "/repairData",name="获取新地址", method = RequestMethod.GET)
    @ResponseBody
    public ResponseData<List<String>> repairData(@RequestParam("txId") String txId){
        try {
            ethService.repairData(txId);
        } catch (BusinessException e) {
            BusinessResponseFactory.createBusinessError(e.getCode());
        }
        return BusinessResponseFactory.createSuccess(null);
    }
}
