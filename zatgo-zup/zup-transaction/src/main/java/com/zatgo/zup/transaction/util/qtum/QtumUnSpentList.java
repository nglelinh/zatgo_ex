package com.zatgo.zup.transaction.util.qtum;

import com.zatgo.zup.transaction.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.entity.qtum.TransactionRecord;
import com.zatgo.zup.transaction.entity.qtum.UTXO;
import com.zatgo.zup.transaction.entity.qtum.Vin;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.util.MongoUtil;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by 46041 on 2018/10/15.
 */
public class QtumUnSpentList {

    //从小到大排序
    private LinkedList<UTXO> list = new LinkedList<>();
    //由arrayList负责搜索
    private ArrayList<UTXO> arrayList = new ArrayList<>();

    //使用之前需初始化，二次使用初始化视情况而定
    public QtumUnSpentList(MongoUtil mongoUtil, Integer confirmed) {
        //获取正在使用中的address
        List<TransactionRecord> transactionRecords = getUnCheckTransction(confirmed, mongoUtil);
        Query query = new Query();
        Criteria criteria = new Criteria();
        Set<String> address = new HashSet<>();
        for (TransactionRecord record : transactionRecords){
            List<Vin> vinList = record.getVinList();
            for (Vin vin : vinList){
                address.add(vin.getAddress());
            }
        }
        criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_ADDRESS).nin(address);
        criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS).is(0);
        query.addCriteria(criteria);
        List<UTXO> data = mongoUtil.getData(query, UTXO.class, MongoConstant.QTUM_UTXO_CLASS_NAME);
        if (!CollectionUtils.isEmpty(data)){
            for (UTXO utxo : data){
                put(utxo);
            }
        }
        arrayList.addAll(list);
    }

    //使用之前需初始化，二次使用初始化视情况而定
    public QtumUnSpentList(MongoUtil mongoUtil, Integer confirmed, List<String> notInAddresses) {
        //获取正在使用中的address
        List<TransactionRecord> transactionRecords = getUnCheckTransction(confirmed, mongoUtil);
        Query query = new Query();
        Criteria criteria = new Criteria();
        Set<String> address = new HashSet<>();
        for (TransactionRecord record : transactionRecords){
            List<Vin> vinList = record.getVinList();
            for (Vin vin : vinList){
                address.add(vin.getAddress());
            }
        }
        if (!CollectionUtils.isEmpty(notInAddresses))
            address.addAll(notInAddresses);
        criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_ADDRESS).nin(address);
        criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS).is(0);
        query.addCriteria(criteria);
        List<UTXO> data = mongoUtil.getData(query, UTXO.class, MongoConstant.QTUM_UTXO_CLASS_NAME);
        if (!CollectionUtils.isEmpty(data)){
            for (UTXO utxo : data){
                put(utxo);
            }
        }
        arrayList.addAll(list);
    }

    /**
     * 获取地址被使用，但是依然为被使用的utxo
     * @param mongoUtil
     * @param confirmed
     * @param usedAddressUtxo
     */
    public QtumUnSpentList(MongoUtil mongoUtil, Integer confirmed, Integer usedAddressUtxo) {
        //获取正在使用中的address
        List<TransactionRecord> transactionRecords = getUnCheckTransction(confirmed, mongoUtil);
        Query query = new Query();
        Criteria criteria = new Criteria();
        Set<String> address = new HashSet<>();
        for (TransactionRecord record : transactionRecords){
            List<Vin> vinList = record.getVinList();
            for (Vin vin : vinList){
                address.add(vin.getAddress());
            }
        }
        criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_ADDRESS).in(address);
        criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS).is(0);
        query.addCriteria(criteria);
        List<UTXO> data = mongoUtil.getData(query, UTXO.class, MongoConstant.QTUM_UTXO_CLASS_NAME);
        if (!CollectionUtils.isEmpty(data)){
            for (UTXO utxo : data){
                put(utxo);
            }
        }
        arrayList.addAll(list);
    }

    /**
     * 获取某个地址中未被使用的utxo
     * @param mongoUtil
     * @param confirmed
     * @param fromAddress
     */
    public QtumUnSpentList(MongoUtil mongoUtil, Integer confirmed, String fromAddress) {
        //获取正在使用中的address
        List<TransactionRecord> transactionRecords = getUnCheckTransction(confirmed, mongoUtil);
        Query query = new Query();
        Criteria criteria = new Criteria();
        for (TransactionRecord record : transactionRecords){
            List<Vin> vinList = record.getVinList();
            for (Vin vin : vinList){
                if (fromAddress.equals(vin.getAddress()))
                    return;
            }
        }
        criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_ADDRESS).is(fromAddress);
        criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS).is(0);
        query.addCriteria(criteria);
        List<UTXO> data = mongoUtil.getData(query, UTXO.class, MongoConstant.QTUM_UTXO_CLASS_NAME);
        if (!CollectionUtils.isEmpty(data)){
            for (UTXO utxo : data){
                put(utxo);
            }
        }
        arrayList.addAll(list);
    }

    private void put(UTXO utxo){
        int size = list.size();
        if (size == 0){
            list.add(utxo);
        } else {
            putValue(0, utxo, size);
        }
    }

    public List<UTXO> get(BigDecimal amount){
        int size = arrayList.size();
        if (size == 0){
            return null;
        }
        List<UTXO> result = new ArrayList<>();
        BigDecimal money = getInit(result, amount);
        UTXO utxo = findUTXO(money);
        if (utxo != null)
            result.add(utxo);
        return result;
    }

    private UTXO findUTXO(BigDecimal amount){
        int size = arrayList.size();
        if (size == 0){
            return null;
        }
        int index = size / 2;
        int startIndex = 0;
        int endIndex = size - 1;
        while (true){
            UTXO utxo = arrayList.get(index);
            BigDecimal balance = utxo.getAmount();
//            System.out.println(startIndex + ":" + list.get(startIndex).getAmount());
//            System.out.println(index + ":" + list.get(index).getAmount());
//            System.out.println(endIndex + ":" + list.get(endIndex).getAmount());
//            System.out.println("================================");
            int res = balance.compareTo(amount);
            //值一样或找到了最右边，直接返回
            if (res == 0 ){
                arrayList.remove(index);
                return utxo;
            }
            //少于3个utxo
            if (arrayList.size() < 3){
                if (res == -1){
                    index += 1;
                    utxo = arrayList.get(index);
                }
                arrayList.remove(index);
                return utxo;
            }
            //找到了最左边，如果最左边的那个值比目标值小，索引+1
            if (index == startIndex){
                if (res == 0){
                    arrayList.remove(index);
                    return utxo;
                }
                index += 1;
                utxo = arrayList.get(index);
                arrayList.remove(index);
                return utxo;
            }
            if (index == endIndex){
                arrayList.remove(index);
                return utxo;
            }
            //大于需要金额的，往前找
            if (res == 1){
                endIndex = index;
                index = startIndex + (endIndex - startIndex) / 2;
                continue;
            }
            //小于需要金额的，往后找
            if (res == -1){
                startIndex = index;
                index = index + (endIndex - startIndex) / 2;
                continue;
            }
        }
    }

    /**
     * 返回一个需要通过搜索的金额，并初始化list
     * @param result
     * @param amount
     * @return
     */
    private BigDecimal getInit(List<UTXO> result, BigDecimal amount){
        int size = arrayList.size();
        int i = size - 1;
        BigDecimal res = amount;

        while (i >= 0){
            UTXO last = arrayList.get(i);
            if (last.getAmount().compareTo(res) == -1){
                res = res.subtract(last.getAmount());
                result.add(last);
                arrayList.remove(i);
                i--;
                continue;
            }
            if (last.getAmount().compareTo(res) != -1){
                return res;
            }
            i--;
        }
        return res;
    }


    private void putValue(int i, UTXO utxo, int size){
        UTXO u = list.get(i);
        if (utxo.getAmount().compareTo(u.getAmount()) == -1){
            list.add(i, utxo);
        } else {
            if (i == size - 1){
                list.add(utxo);
                return;
            } else {
                putValue(++i, utxo, size);
            }
        }
    }

    public static List<TransactionRecord> getUnCheckTransction(Integer confirmed, MongoUtil mongoUtil){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_CONFIRMED).lt(confirmed);
        query.addCriteria(criteria);
        return mongoUtil.getData(query, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
    }

    public static List<TransactionRecord> getUnCheckReturn(MongoUtil mongoUtil){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_CONFIRMED).lt(500);
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_TYPE).is(TransactionTypeEnum.RETURN.getCode());
        query.addCriteria(criteria);
        return mongoUtil.getData(query, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
    }

//    public static void main(String[] args) {
//        BigDecimal bigDecimal = new BigDecimal("489255.2");
//        BigDecimal target = bigDecimal;
//        for (int i = 0; i < 100; i++){
//            UTXO u = new UTXO();
//            BigDecimal bigDecimal1 = new BigDecimal(getId(5));
//            u.setAmount(bigDecimal1);
//            put(u);
//            BigDecimal subtract = bigDecimal.subtract(bigDecimal1);
//            if (subtract.compareTo(BigDecimal.ZERO) == 1 && subtract.compareTo(target) == -1){
//                target = subtract;
//            }
//        }
//        System.out.println("=====================================");
//        UTXO utxo = findUTXO(bigDecimal);
//        System.out.println("实际的值：" + bigDecimal.subtract(utxo.getAmount()));
//        System.out.println("目标值：" + target);
//        System.out.println(list.size());
//    }
//
//    private static String getId(int n){
//        return ((long)((Math.random()*9+1)* Math.pow(10,n)) + "");
//    }



//
//    private String getAddressLockKey(String address){
//        return RedisKeyConstants.TRANSACTION_ADDRESS_LOCK + address;
//    }

}
