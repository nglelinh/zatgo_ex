package com.zatgo.zup.transaction.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 46041 on 2018/10/19.
 */
public class Daemon implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(Daemon.class);

    private Runnable target;
    private Thread thread;

    @Override
    public void run() {
        while (true){
            try {
                if (!thread.isAlive()){
                    logger.info("======================================侦测到"+ target.getClass() +"线程挂掉，线程开始重启========================================================");
                    Thread t = new Thread(target);
                    t.start();
                    this.thread = t;
                    logger.info("======================================重启结束========================================================");
                }
                Thread.sleep(5000l);
            } catch (InterruptedException e) {
                logger.error("", e);
            }
        }
    }

    public Daemon(Runnable target, Thread thread) {
        this.target = target;
        this.thread = thread;
    }
}
