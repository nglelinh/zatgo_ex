package com.zatgo.zup.transaction.remoteservice;

import com.zatgo.zup.common.model.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by 46041 on 2018/10/10.
 */

@FeignClient("zup-sign")
public interface SignRemotService {


    //btc

    @RequestMapping(value = "/sign/btc/newaddresses", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<List<String>> getNewBtcAddresses(@RequestBody CreateAddressRequest request);


    @RequestMapping(value = "/sign/btc/sign", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<String> btcSign(@RequestBody SignBTCTransactionParams params);


    //eth

    @RequestMapping(value = "/sign/eth/newaddresses", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<List<String>> getNewEthAddresses(@RequestBody CreateAddressRequest request);

    @RequestMapping(value = "/sign/eth/sign", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<String> ethSign(@RequestBody EthSignParams params);

    @RequestMapping(value = "/sign/eth/signContract", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<String> ethSign(@RequestBody EthContractSignParams ethContractSignParams);


    //qtum

    @RequestMapping(value = "/sign/qtum/newaddresses", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<List<String>> getNewQtumAddresses(@RequestBody CreateAddressRequest request);

    @RequestMapping(value = "/sign/qtum/sign", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<String> qtumSign(@RequestBody SignQtumTransactionParams params);

    @RequestMapping(value = "/sign/qtum/signContract", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<String> qtumSign(@RequestBody QtumContractSignParams params);


    //usdt

    @RequestMapping(value = "/sign/usdt/newaddresses", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<List<String>> getNewUsdtAddresses(@RequestBody CreateAddressRequest request);


    @RequestMapping(value = "/sign/usdt/sign", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<String> usdtSign(@RequestBody SignUsdtTransactionParams params);


}
