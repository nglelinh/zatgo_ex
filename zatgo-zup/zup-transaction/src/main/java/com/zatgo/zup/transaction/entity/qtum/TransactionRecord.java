package com.zatgo.zup.transaction.entity.qtum;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2018/10/10.
 */
public class TransactionRecord implements Serializable {
    /**
     * 块上交易ID
     */
    private String txId;
    /**
     * 系统交易id
     */
    private String recordId;

    private String hex;
    /**
     * 旷工费
     */
    private BigDecimal fee;
    /**
     * 输入list
     */
    private List<Vin> vinList;
    /**
     * 输出list
     */
    private List<Vout> voutList;
    /**
     * 确认数
     */
    private Integer confirmed;
    /**
     * 区块高度
     */
    private Integer height;
    /**
     * 订单创建时间
     */
    private Date createDate;
    /**
     * 订单修改时间
     */
    private Date updateDate;
    /**
     * 出块时间
     */
    private Date blockDate;
    /**
     * 交易类型  1充值0提币
     */
    private Integer type;

    private String contractAddress;

    private BigDecimal gasLimit;

    private BigDecimal gasPrice;

    private String toAddress;

    private String fromAddress;

    private BigDecimal amount;


    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public List<Vin> getVinList() {
        return vinList;
    }

    public void setVinList(List<Vin> vinList) {
        this.vinList = vinList;
    }

    public List<Vout> getVoutList() {
        return voutList;
    }

    public void setVoutList(List<Vout> voutList) {
        this.voutList = voutList;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getBlockDate() {
        return blockDate;
    }

    public void setBlockDate(Date blockDate) {
        this.blockDate = blockDate;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContractAddress() {
        return contractAddress;
    }

    public void setContractAddress(String contractAddress) {
        this.contractAddress = contractAddress;
    }

    public BigDecimal getGasLimit() {
        return gasLimit;
    }

    public void setGasLimit(BigDecimal gasLimit) {
        this.gasLimit = gasLimit;
    }

    public BigDecimal getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(BigDecimal gasPrice) {
        this.gasPrice = gasPrice;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
