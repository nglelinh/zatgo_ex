package com.zatgo.zup.transaction.util.qtum;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.model.Commands;
import com.zatgo.zup.common.model.CreateAddressRequest;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.utils.ArgsUtils;
import com.zatgo.zup.transaction.conf.ContractConfig;
import com.zatgo.zup.transaction.conf.QtumAndContractConfig;
import com.zatgo.zup.transaction.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.entity.qtum.*;
import com.zatgo.zup.transaction.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.util.MongoUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.Utils;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptChunk;
import org.bitcoinj.script.ScriptOpCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by 46041 on 2018/11/19.
 */

@Component
public class QtumUtil {

    private static final Logger logger = LoggerFactory.getLogger(QtumUtil.class);

    @Value("${transaction.user}")
    private String user;
    @Value("${transaction.password}")
    private String password;
    @Value("${transaction.host}")
    private String host;
    @Value("${transaction.port}")
    private String port;

    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private SignRemotService signRemotService;
    @Autowired
    private QtumAndContractConfig qtumAndContractConfig;

    private JsonRpcHttpClient client = null;

    private static Pattern P2PKH = Pattern.compile("^OP_DUP OP_HASH160 [a-zA-Z0-9]* OP_EQUALVERIFY OP_CHECKSIG$");




    private JsonRpcHttpClient getClient(){
        try {
            if (client == null){
                client = new JsonRpcHttpClient(new URL("http://" + host + ":" + port));
                String cred = Base64.encodeBase64String((user + ":" + password).getBytes());
                Map<String, String> headers = new HashMap<String, String>(1);
                headers.put("Authorization", "Basic " + cred);
                client.setHeaders(headers);
            }
        } catch (Exception e){
            logger.error("", e);
        }
        return client;
    }

    private static JsonRpcHttpClient getClient(String ad){
        JsonRpcHttpClient client = null;
        try {
            if (client == null){
                client = new JsonRpcHttpClient(new URL("http://47.74.145.17:27732"));
                String cred = Base64.encodeBase64String(("111:222").getBytes());
                Map<String, String> headers = new HashMap<String, String>(1);
                headers.put("Authorization", "Basic " + cred);
                client.setHeaders(headers);
            }
        } catch (Exception e){
            logger.error("", e);
        }
        return client;
    }

    public static void main(String[] args) throws Throwable {
//        Integer invoke = getClient("1").invoke(Commands.GET_BLOCK_COUNT, null, Integer.class);
//        System.out.println(invoke);

        String hex = getClient("1").invoke(Commands.GET_RAW_TRANSACTION, ArgsUtils.asList("508e5efe96737da14c431c424ff7c24d3aedbea7b6d0c379f1bc43d0ecadf3fc"), String.class);
        JSONObject rawTransaction = getClient("1").invoke(Commands.DECODE_RAW_TRANSACTION, ArgsUtils.asList(hex), JSONObject.class);

        JSONArray gettransactionreceipt = getClient("1").invoke("gettransactionreceipt", ArgsUtils.asList("508e5efe96737da14c431c424ff7c24d3aedbea7b6d0c379f1bc43d0ecadf3fc"), JSONArray.class);
        String s = JSONObject.toJSONString(gettransactionreceipt);
        System.out.println(rawTransaction);

//        String hex = "010403f04902012844a9059cbb0000000000000000000000003c49afd0762c5461c8ff6211894bbd4196326c7600000000000000000000000000000000000000000000000170447a8769d5793814d8dec2b605005749abbf4b060edad3070e23cf5cc2";
////        String hex = "76a914cd693fc9a83bef10e734fa906f3ea0a5d390d2ec88ac";
//        byte[] bytes = Hex.decodeHex(hex);
//        Script script = new Script(Utils.parseAsHexOrBase58(hex));
//        System.out.println(script);

//        String hex = "e57e4a5f9ac130defb33a057729f10728fcdb9cb";
//        System.out.println(hex.length());
//        hex = hex.substring(hex.length() - 40, hex.length());
//        System.out.println(getClient("1").invoke(Commands.FROM_HEX_ADDRESS, ArgsUtils.asList(hex), String.class));
//        System.out.println(createConstructScript("", 250000, 40));

//        System.out.println(getClient("1").invoke(Commands.GET_HEX_ADDRESS, ArgsUtils.asList("QPQszTSN7jCazE5fbjxaekJB7tZZUA7dQE"), String.class));
//        Script constructScript = createConstructScript("1", 250000, 40);
//        System.out.println(constructScript);
    }

    public Address getSystemAddress(String coinType){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_TYPE).is(AddressTypeEnum.SYSTEM_ADDRESS.getCode());
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, getTokenAddressName(coinType));
        if (!CollectionUtils.isEmpty(data)){
            return data.get(0);
        }
        return null;
    }

    public Address getSystemCollectAddress(){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_TYPE).is(AddressTypeEnum.SYSTEM_COLLECT.getCode());
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, getTokenAddressName("QTUM"));
        if (!CollectionUtils.isEmpty(data)){
            return data.get(0);
        }
        return null;
    }

    /**
     * 获取最新区块高度
     * @return
     * @throws Throwable
     */
    public Integer getBlockCount() throws Throwable {
        return getClient().invoke(Commands.GET_BLOCK_COUNT, null, Integer.class);
    }

    public String getAddressByHex(String hex) throws Throwable {
        if (StringUtils.isEmpty(hex))
            return null;
        if (hex.length() > 40)
            hex = hex.substring(hex.length() - 40, hex.length());
        return getClient().invoke(Commands.FROM_HEX_ADDRESS, ArgsUtils.asList(hex), String.class);
    }

    public String getTokenAddressName(String token){
        return token.toUpperCase() + "_" + MongoConstant.QTUM_ADDRESS_CLASS_NAME;
    }


    /**
     * 获取块hash
     * @param height
     * @return
     */
    public String getBlockHash(Integer height) throws Throwable {
        return getClient().invoke(Commands.GET_BLOCK_HASH, ArgsUtils.asList(height), String.class);
    }


    /**
     * 发送交易
     * @param hex
     * @return
     */
    public String sendRawTransaction(String hex) throws Throwable {
        return getClient().invoke(Commands.SEND_RAW_TRANSACTION, ArgsUtils.asList(hex), String.class);
    }

    public JSONObject getTransactionReceipt(String txId) throws Throwable {
        JSONArray invoke = getClient().invoke(Commands.GET_TRANSACTION_RECEIPT, ArgsUtils.asList(txId), JSONArray.class);
        JSONObject res = null;
        if (invoke != null && !invoke.isEmpty()){
            res = invoke.getJSONObject(0);
        }
        return res;
    }

    public String getAddressHex(String address) throws Throwable {
        return getClient().invoke(Commands.GET_HEX_ADDRESS, ArgsUtils.asList(address), String.class);
    }

    public BigDecimal getServiceCharge(ContractConfig config){
        return qtumAmountFormate8(new BigDecimal(config.getGaslimit()).multiply(new BigDecimal(config.getGasprice())));
    }

    public BigDecimal qtumAmountFormate8(BigDecimal amount){
        BigDecimal pow = BigDecimal.TEN.pow(8);
        return amount.divide(pow);
    }

    public BigDecimal qtumAmountFormateN(BigDecimal amount, Integer n){
        BigDecimal pow = BigDecimal.TEN.pow(n);
        return amount.divide(pow);
    }

    /**
     * 获取区块中所有的交易ID list
     * @return
     */
    public List<String> getBlockTx(String blockHash) throws Throwable {
        JSONObject res = getClient().invoke(Commands.GET_BLOCK, ArgsUtils.asList(blockHash), JSONObject.class);
        if (res == null){
            return null;
        }
        return res.getJSONArray("tx").toJavaList(String.class);
    }


    /**
     * 根据交易Id获取交易信息
     * @param txId
     * @return
     */
    public TransactionRecord getTransaction(String txId, Integer height, int index) throws Throwable {
        String hex = getClient().invoke(Commands.GET_RAW_TRANSACTION, ArgsUtils.asList(txId), String.class);
        JSONObject rawTransaction = getClient().invoke(Commands.DECODE_RAW_TRANSACTION, ArgsUtils.asList(hex), JSONObject.class);
        if (rawTransaction == null){
            return null;
        }
        TransactionRecord record = new TransactionRecord();
        record.setHeight(height);
        record.setCreateDate(new Date());
        record.setConfirmed(0);
        record.setHex(hex);
        record.setTxId(rawTransaction.getString("txid"));
        record.setType(index == 1 ? TransactionTypeEnum.RETURN.getCode() : TransactionTypeEnum.DEPOSIT.getCode());
        getVin(record, rawTransaction);
        getVout(record, rawTransaction);
        return record;
    }




    /**
     * 根据交易记录获取未被消费的UTXO
     * @param record
     * @return
     */
    public List<UTXO> getUnSpentUTXO(TransactionRecord record) {
        List<UTXO> list = new ArrayList<>();
        List<Vout> voutList = record.getVoutList();
        for (Vout out : voutList){
            list.add(getUtxo(record, out));
        }
        return list;
    }

    public UTXO getUtxo(TransactionRecord record, Vout out){
        Date date = new Date();
        UTXO utxo = new UTXO();
        utxo.setTxId(record.getTxId());
        utxo.setAddress(out.getAddress());
        utxo.setBlockDate(record.getBlockDate());
        utxo.setVout(out.getVout());
        utxo.setHeight(record.getHeight());
        utxo.setScriptPubKey(out.getScriptPubKey());
        utxo.setAmount(out.getValue());
        utxo.setCreateDate(date);
        utxo.setCoinType(out.getCoinType());
        utxo.setStatus(0);
        return utxo;
    }

    private void getVin(TransactionRecord record, JSONObject rawTransaction){
        JSONArray ins = rawTransaction.getJSONArray("vin");
        List<Vin> list = new ArrayList<>();
        for (int i = 0; i < ins.size(); i++){
            JSONObject in = ins.getJSONObject(i);
            Vin vin = new Vin();
            vin.setTxId(in.getString("txid"));
            vin.setVout(in.getInteger("vout"));
            vin.setSequence(in.getString("sequence"));
            ScriptPubkey sp = new ScriptPubkey();
            JSONObject scriptSig = in.getJSONObject("scriptSig");
            if (scriptSig != null){
                sp.setAsm(scriptSig.getString("asm"));
                sp.setHex(scriptSig.getString("hex"));
            }
            vin.setScriptPubKey(sp);
            list.add(vin);
        }
        record.setVinList(list);
    }

    private void getVout(TransactionRecord record, JSONObject rawTransaction){
        JSONArray outs = rawTransaction.getJSONArray("vout");
        Map<String, ContractConfig> contractConfig = qtumAndContractConfig.getContractConfig();
        List<Vout> list = new ArrayList<>();
        for (int i = 0; i < outs.size(); i ++){
            try{
                JSONObject out = outs.getJSONObject(i);
                Vout vout = new Vout();
                String value = out.getString("value");
                String n = out.getString("n");
                JSONObject scriptPubkey = out.getJSONObject("scriptPubKey");
                ScriptPubkey sp = new ScriptPubkey();
                String type = scriptPubkey.getString("type");
                String asm = scriptPubkey.getString("asm");
                if ("call".equalsIgnoreCase(type)){
                    JSONObject transactionReceipt = getTransactionReceipt(record.getTxId());
                    if (transactionReceipt == null)
                        continue;
                    String contractAddress = transactionReceipt.getString("contractAddress");
                    if (!contractConfig.containsKey(contractAddress)){
                        continue;
                    }
                    JSONArray logs = transactionReceipt.getJSONArray("log");
                    if (CollectionUtils.isEmpty(logs))
                        continue;
                    JSONObject log = logs.getJSONObject(0);
                    String amount16 = new BigInteger(log.getString("data"), 16).toString(10);
                    String toAddress = getAddressByHex(log.getJSONArray("topics").getString(2));
                    ContractConfig config = contractConfig.get(contractAddress);
                    vout.setCoinType(config.getName());
                    vout.setValue(getAmount(amount16, config.getPrecision()));
                    vout.setAddress(toAddress);
                    vout.setContractAddress(contractAddress);
                } else {
                    if (!P2PKH.matcher(asm).matches())
                        continue;
                    sp.setAddress(scriptPubkey.getJSONArray("addresses").getString(0));
                    sp.setReqSigs(scriptPubkey.getString("reqSigs"));
                    vout.setCoinType("QTUM");
                    vout.setValue(new BigDecimal(value));
                    vout.setAddress(sp.getAddress());
                }
                sp.setAsm(asm);
                sp.setHex(scriptPubkey.getString("hex"));
                sp.setType(type);
                vout.setScriptPubKey(sp);
                vout.setVout(Integer.valueOf(n));
                list.add(vout);
            } catch (Exception e){
                logger.info("", e);
            } catch (Throwable throwable) {
                logger.error("", throwable);
            }
        }
        record.setVoutList(list);
    }

    public List<String> getAddress(Integer num){
        CreateAddressRequest request = new CreateAddressRequest();
        request.setNum(num);
        request.setPassword(CoinConstant.SIGN_PASSWORD);
        try {
            ResponseData<List<String>> newAddresses = signRemotService.getNewQtumAddresses(request);
            if (newAddresses.isSuccessful()){
                return newAddresses.getData();
            }
            throw new BusinessException(newAddresses.getCode());
        } catch (Exception e){
            logger.error("", e);
        }
        return new ArrayList<>();
    }

    public BigDecimal qtumAmountFormateSumN(BigDecimal amount, BigInteger n){
        BigDecimal pow = BigDecimal.TEN.pow(n.intValue());
        return amount.multiply(pow);
    }


    public BigDecimal getAmount(String amount16, BigInteger precision){
        amount16 = amount16.replaceFirst("^0x0+", "");
        BigDecimal amount10 = new BigDecimal(new BigInteger(amount16, 10));
        return qtumAmountFormateN(amount10, precision.intValue());
    }
}
