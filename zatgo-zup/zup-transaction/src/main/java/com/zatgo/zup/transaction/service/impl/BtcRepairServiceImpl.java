package com.zatgo.zup.transaction.service.impl;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.transaction.entity.btc.MainChain;
import com.zatgo.zup.transaction.entity.btc.TransactionRecord;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.service.RepairDataService;
import com.zatgo.zup.transaction.service.ThreadPoolTaskService;
import com.zatgo.zup.transaction.util.MongoUtil;
import com.zatgo.zup.transaction.util.btc.BitcoinUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by 46041 on 2018/10/19.
 */

@Service("btcRepairService")
public class BtcRepairServiceImpl implements RepairDataService {

    private static final Logger logger = LoggerFactory.getLogger(BtcRepairServiceImpl.class);

    @Value("${transaction.height}")
    private Integer height;
    @Value("${transaction.fee}")
    private BigDecimal fee;
    @Value("${transaction.confirmed}")
    private Integer confirmed;


    @Autowired
    private BitcoinUtil bitcoinUtil;
    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private ThreadPoolTaskService btcThreadPoolTaskService;


    @Override
    public void scanBlockGetExtract() {
        //获取数据库中区块高度
        Integer maxValue = mongoUtil.getMaxValue("height", "extract_" + MongoConstant.MAIN_CHAIN_CLASS_NAME) + 1;
        maxValue = getMax(height, maxValue);
        Integer blockCount = bitcoinUtil.getBlockCount();
        //如果已经扫描到最高的区块了，休息10秒继续扫描
        if (maxValue - 1 == blockCount){
            try {
                logger.info("睡了10秒");
                Thread.sleep(10 * 1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return;
        }
        logger.info("====================当前扫描区块高度：" + maxValue.toString());
        String blockHash = bitcoinUtil.getBlockHash(maxValue);
        List<String> blockTx = bitcoinUtil.getBlockTx(blockHash);
        Map<String, TransactionRecord> selfTransactionMap = getSelfTransaction(blockTx);
        List<Future> res = new ArrayList<>(blockTx.size());
        for (String txId : blockTx){
            Future<ResponseData<Object>> future = btcThreadPoolTaskService.scanExtractTx(selfTransactionMap, txId, maxValue);
            res.add(future);
        }

        for (int i = 0; i < res.size() - 1; i++){
            try {
                Future<ResponseData<Object>> future = res.get(i);
                ResponseData<Object> result = future.get();
                if (!result.isSuccessful()){
                    Future<ResponseData<Object>> futureResult = btcThreadPoolTaskService.scanExtractTx(selfTransactionMap, result.getMessage(), maxValue);
                    res.add(futureResult);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        MainChain mainChain = new MainChain();
        mainChain.setHeight(maxValue);
        mainChain.setHash(blockHash);
        mongoUtil.insert(mainChain, "extract_" + MongoConstant.MAIN_CHAIN_CLASS_NAME);
    }

    private Map<String, TransactionRecord> getSelfTransaction(List<String> txids){
        Map<String, TransactionRecord> map = new HashMap();
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.TRANSACTION_CLASS_TXID).in(txids);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.TRANSACTION_CLASS);
        if (!CollectionUtils.isEmpty(data)){
            for (TransactionRecord tx : data){
                map.put(tx.getTxId(), tx);
            }
        }
        return map;
    }


    private Integer getMax(Integer v1, Integer v2){
        if (v1 == null){
            v1 = 1;
        }
        if (v2 == null){
            v2 = 1;
        }
        return v1 > v2 ? v1 : v2;
    }
}
