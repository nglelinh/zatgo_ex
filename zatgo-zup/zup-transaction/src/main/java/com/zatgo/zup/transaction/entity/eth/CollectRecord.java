package com.zatgo.zup.transaction.entity.eth;

/**
 * Created by 46041 on 2018/11/1.
 */
public class CollectRecord {

    private String toAddress;

    private String fromAddress;

    private String coinType;


    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }
}
