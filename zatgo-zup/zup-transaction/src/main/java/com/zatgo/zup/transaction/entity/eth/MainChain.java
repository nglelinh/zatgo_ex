package com.zatgo.zup.transaction.entity.eth;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by 46041 on 2018/10/10.
 */
public class MainChain implements Serializable {
    /**
     * 区块高度
     */
    private Integer height;
    /**
     * 区块hash
     */
    private String hash;

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
