package com.zatgo.zup.transaction.listener;

import com.zatgo.zup.transaction.service.CoinService;

/**
 * Created by 46041 on 2018/10/19.
 */
public class ScanBlockListener implements Runnable {

    private CoinService coinService;

    @Override
    public void run() {
        while (true){
            try {
                coinService.scanBlock();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public ScanBlockListener(CoinService coinService) {
        this.coinService = coinService;
    }
}
