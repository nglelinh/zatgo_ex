package com.zatgo.zup.transaction.entity.qtum;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 46041 on 2018/10/29.
 */
public enum TransactionUseTypeEnum {
    NOT_USE(0), USED(1);

    private Integer code;

    private static final Map<Integer, TransactionUseTypeEnum> stringToEnum = new HashMap<Integer, TransactionUseTypeEnum>();
    static {
        for (TransactionUseTypeEnum enumType : values()) {
            stringToEnum.put(enumType.getCode(), enumType);
        }
    }

    TransactionUseTypeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
