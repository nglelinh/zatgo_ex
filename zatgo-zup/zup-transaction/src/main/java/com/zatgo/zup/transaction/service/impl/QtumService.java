package com.zatgo.zup.transaction.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.*;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.transaction.conf.ContractConfig;
import com.zatgo.zup.transaction.conf.QtumAndContractConfig;
import com.zatgo.zup.transaction.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.entity.constant.Constants;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.entity.eth.CollectRecord;
import com.zatgo.zup.transaction.entity.eth.SendServiceChargeRecord;
import com.zatgo.zup.transaction.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.entity.params.InsertQtumRecordParam;
import com.zatgo.zup.transaction.entity.qtum.*;
import com.zatgo.zup.transaction.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.remoteservice.WalletRemotService;
import com.zatgo.zup.transaction.service.CoinService;
import com.zatgo.zup.transaction.util.MongoUtil;
import com.zatgo.zup.transaction.util.qtum.QtumUnSpentList;
import com.zatgo.zup.transaction.util.qtum.QtumUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.*;

/**
 * Created by 46041 on 2018/11/22.
 */

@Service("qtumService")
public class QtumService implements CoinService {

    private static final Logger logger = LoggerFactory.getLogger(QtumService.class);

    @Autowired
    private QtumUtil qtumUtil;
    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private SignRemotService signRemotService;
    @Autowired
    private WalletRemotService walletRemotService;
    @Autowired
    private QtumAndContractConfig qtumAndContractConfig;


    @Value("${transaction.height}")
    private Integer height;
    @Value("${transaction.fee}")
    private BigDecimal fee;
    @Value("${transaction.confirmed}")
    private Integer confirmed;
    @Value("${transaction.type}")
    private String type;
    @Value("${transaction.addressNum}")
    private Integer addressNum;



    @Override
    public void sendTransaction(String toAddress, String fromAddress, BigDecimal amount, String businessTxId, String coinType) {
        send(toAddress, fromAddress, amount, businessTxId, coinType, TransactionTypeEnum.EXTRACT);
    }

    private String send(String toAddress, String fromAddress, BigDecimal amount, String businessTxId, String coinType, TransactionTypeEnum type){
        if (amount.compareTo(BigDecimal.ZERO) == -1){
            throw new BusinessException(BusinessExceptionCode.EXTRACT_MONEY_ERROR);
        }
        String extractLock = RedisKeyConstants.TRANSACTION_EXTRACT_LOCK + fromAddress;
        String txId = null;
        redisLockUtils.lock(extractLock);
        try {
            if (StringUtils.isEmpty(businessTxId)){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BUSINESS_TXID_NOT_IS_EMPTY);
            }
            if (hasRecordId(businessTxId) == null) {
                Map<String, Object> query = new HashMap();
                query.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, fromAddress);
                List<Address> data = mongoUtil.getData(query, Address.class, qtumUtil.getTokenAddressName("QTUM"));
                if (CollectionUtils.isEmpty(data)) {
                    logger.error("转账失败的业务Id：" + businessTxId);
                    throw new BusinessException(BusinessExceptionCode.EXTRACT_FROM_ADDRESS_NOT_EXIST);
                }
                if ("QTUM".equalsIgnoreCase(coinType)){
                    txId = sendQtum(amount, businessTxId, toAddress, fromAddress, coinType, type);
                } else {
                    txId = sendQtumContract(amount, businessTxId, toAddress, fromAddress, coinType, type);
                }
            }
        } catch (Throwable throwable) {
            logger.error("", throwable);
        } finally {
            redisLockUtils.releaseLock(extractLock);
        }
        return txId;
    }

    @Override
    public void scanBlock() throws IOException {
       try {
           //获取数据库中区块高度
           Integer maxValue = mongoUtil.getMaxValue("height", MongoConstant.QTUM_MAIN_CHAIN_CLASS) + 1;
           maxValue = getMax(height, maxValue);
           Integer blockCount = qtumUtil.getBlockCount();
           //如果已经扫描到最高的区块了，休息10秒继续扫描
           if (maxValue - 1 == blockCount){
               try {
                   logger.info("睡了10秒");
                   Thread.sleep(10 * 1000l);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               return;
           }
           String blockHash = qtumUtil.getBlockHash(maxValue);
           List<String> blockTx = qtumUtil.getBlockTx(blockHash);
           Map<String, TransactionRecord> selfTransactionMap = getSelfTransaction(blockTx);
            for (int i = 0; i < blockTx.size(); i++){
                String txId = blockTx.get(i);
                tx(selfTransactionMap, txId, maxValue, i);
            }
           MainChain mainChain = new MainChain();
           mainChain.setHeight(maxValue);
           mainChain.setHash(blockHash);
           mongoUtil.insert(mainChain, MongoConstant.QTUM_MAIN_CHAIN_CLASS);
       } catch (Exception e){
           logger.error("", e);
       } catch (Throwable throwable) {
           throwable.printStackTrace();
       }
    }

    @Override
    public void checkTransaction() {
        List<TransactionRecord> unCheckTransction = QtumUnSpentList.getUnCheckTransction(confirmed, mongoUtil);
        List<TransactionRecord> unCheckReturn = QtumUnSpentList.getUnCheckReturn(mongoUtil);
        if (!CollectionUtils.isEmpty(unCheckReturn)){
            unCheckTransction.addAll(unCheckReturn);
        }
        try{
            Integer blockCount = qtumUtil.getBlockCount();
            for (TransactionRecord record : unCheckTransction){
                String txId = record.getTxId();
                TransactionRecord transaction = null;
                try {
                    transaction = qtumUtil.getTransaction(txId, blockCount, -1);
                    if (transaction == null)
                        continue;
                } catch (Throwable throwable) {
                    logger.error("", throwable);
                    continue;
                }
                record.setVoutList(transaction.getVoutList());
                if (record.getHeight() != null){
                    record.setConfirmed(transaction.getHeight() - record.getHeight() + 1);
                } else {
                    record.setHeight(blockCount);
                }
            }
            //修改确认数
            batchExecuteUpdateConfirmed(unCheckTransction);
        } catch (Throwable throwable) {
            logger.error("", throwable);
        }
    }

    @Override
    public void GetBtcExtractRecord() {
        Map<String, ContractConfig> contractConfig = qtumAndContractConfig.getContractConfig();
        for (Map.Entry<String, ContractConfig> conf : contractConfig.entrySet()){
            Map<String, String> map = new HashMap();
            ContractConfig value = conf.getValue();
            map.put("symbol", value.getName());
            Map<String, ?> stringMap = walletRemotService.withdrawConsume(map);
            if (stringMap != null){
                String code = (String) stringMap.get("errno");
                if (Constants.CODE_SUCCESS.equals(code)){
                    List<Map> withdrawResults = (List<Map>) stringMap.get("data");
                    if (!CollectionUtils.isEmpty(withdrawResults)){
                        for (Map m : withdrawResults){
                            String fromAddress = (String) m.get("from_address");
                            String toAddress = (String) m.get("address_to");
                            BigDecimal amount = new BigDecimal(m.get("amount") + "");
//                            BigDecimal fee = new BigDecimal(m.get("fee") + "");
                            String transId = m.get("trans_id") + "";
                            sendTransaction(toAddress, fromAddress, amount, transId, value.getName());
                        }
                    }
                }
            }
        }
    }

    @Override
    public void collect() {
        Map<String, ContractConfig> coinTypeConfig = qtumAndContractConfig.getCoinTypeConfig();
        ContractConfig qtum = coinTypeConfig.get("QTUM");
        for (Map.Entry<String, ContractConfig> map : coinTypeConfig.entrySet()){
            try {
                ContractConfig value = map.getValue();
                String coinType = value.getName();
                if ("QTUM".equalsIgnoreCase(coinType))
                    continue;
                BigDecimal serviceCharge = getServiceCharge(value);
                Query query = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_NAME_CONFIRMED).gte(value.getCollectlimit().doubleValue());
                criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_TYPE).is(AddressTypeEnum.MEMBER_ADDRESS.getCode());
                query.addCriteria(criteria);
                List<Address> data = mongoUtil.getData(query, Address.class, qtumUtil.getTokenAddressName(coinType));
                if (!CollectionUtils.isEmpty(data)){
                    for (Address address : data){
                        //只有当实际金额+未到账金额大于指定金额的时候才会收集
                        BigDecimal relAmount = new BigDecimal(address.getConfirmed() + "");
                        if (relAmount.add(new BigDecimal(address.getUnconfirmed())).compareTo(value.getCollectlimit()) != -1 && relAmount.compareTo(value.getCollectlimit()) != -1){
                            String toAddress = address.getAddress();
                            String fromAddress = value.getSystemServiceChargeAddress();
                            //先看当前地址有没有足够的qtum
                            TransactionRecord sendCollectServerCharge = null;
                            if (hasNotEnoughServerCharge(toAddress, serviceCharge)){
                                //如果没有发送过手续费
                                sendCollectServerCharge = getSendCollectServerCharge(fromAddress, toAddress);
                                if (sendCollectServerCharge == null){
                                    send(toAddress, fromAddress, serviceCharge.add(fee), UUIDUtils.getUuid() + "_1", "QTUM", TransactionTypeEnum.COLLECT);
                                    continue;
                                }
                                //没到确认数的话，不算入账
                                if (sendCollectServerCharge.getConfirmed() < qtum.getConfirmed()){
                                    continue;
                                }
                            }
                            BigDecimal amount = new BigDecimal(address.getConfirmed() + "");
                            String recordId = null;
                            if (sendCollectServerCharge != null){
                                recordId = sendCollectServerCharge.getRecordId();
                            } else {
                                recordId = UUIDUtils.getUuid();
                            }
                            String txId = send(value.getSystemAddress(), toAddress, amount, recordId.replace("_1", "_2"), coinType, TransactionTypeEnum.COLLECT);
                            if (!StringUtils.isEmpty(txId)){
                                //将手续费标记为已花费
                                if (sendCollectServerCharge != null)
                                    useServiceCharge(sendCollectServerCharge.getTxId());
                                logger.info("===========从" + toAddress + "搜集了" + amount + "个" + coinType + "========");
                                continue;
                            }
                        }
                    }
                }
            } catch (Exception e){
                logger.error("", e);
            }
        }
    }

    @Override
    public void addAddress() {
        ResponseData<Integer> qtum = walletRemotService.remainderAddress("QTUM");
        if (!CoinConstant.createAddressLockMap.containsKey(CoinConstant.QTUM_ADDRESS_LOCK)){
            CoinConstant.createAddressLockMap.put(CoinConstant.QTUM_ADDRESS_LOCK, "");
            try {
                if (qtum.getData() < 200){
                    CreateAddressRequest request = new CreateAddressRequest();
                    List<String> data = new ArrayList<>();
                    request.setPassword(CoinConstant.SIGN_PASSWORD);
                    request.setNum(100);
                    while (true){
                        try{
                            ResponseData<List<String>> newAddresses = signRemotService.getNewQtumAddresses(request);
                            data.addAll(newAddresses.getData());
                            if (addressNum <= data.size()){
                                break;
                            }
                        } catch (Exception e){
                            logger.error("", e);
                        }
                    }
                    List<Address> addresses = new ArrayList<>(data.size());
                    JSONArray array = new JSONArray();
                    Date date = new Date();
                    for (String a : data){
                        JSONObject object = new JSONObject();
                        object.put("address", a);
                        object.put("networkType", "QTUM");
                        object.put("isUsed", 0);
                        object.put("create_time", date);
                        array.add(object);
                        Address address = new Address();
                        address.setType(AddressTypeEnum.MEMBER_ADDRESS.getCode());
                        address.setAddress(a);
                        address.setCreateDate(date);
                        addresses.add(address);
                        address.setConfirmed(0d);
                        address.setUnconfirmed(0d);
                    }
                    ResponseData<String> responseData = walletRemotService.addAddresses(array);
                    if (responseData.isSuccessful()){
                        mongoUtil.insert(addresses, qtumUtil.getTokenAddressName("QTUM"));
                    }
                }
            } finally {
                CoinConstant.createAddressLockMap.remove(CoinConstant.QTUM_ADDRESS_LOCK);
            }
        }
    }



    private BigDecimal getServiceCharge(ContractConfig config){
        return qtumUtil.qtumAmountFormate8(new BigDecimal(config.getGaslimit()).multiply(new BigDecimal(config.getGasprice())));
    }


    private void batchExecuteUpdateConfirmed(List<TransactionRecord> list){
        Map<Query, Update> queryUpdateMap = new HashMap();
        if (CollectionUtils.isEmpty(list))
            return;
        for (TransactionRecord record : list){
            try{
                Query query = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).is(record.getTxId());
                query.addCriteria(criteria);
                Update update = new Update();
                if (record.getConfirmed() == 0){
                    update.set(MongoConstant.QTUM_TRANSACTION_CLASS_HEIGHT, record.getHeight());
                }
                update.set(MongoConstant.QTUM_TRANSACTION_CLASS_CONFIRMED, record.getConfirmed());
                update.set(MongoConstant.QTUM_TRANSACTION_CLASS_VOUTLIST, record.getVoutList());
                //如果是旷工费退款，需要500个确认，如果不是，则需要到达当前币种指定的确认数
                if ((TransactionTypeEnum.RETURN.getCode().equals(record.getType()) && record.getConfirmed() >= 500)
                        || (!TransactionTypeEnum.RETURN.getCode().equals(record.getType()) && record.getConfirmed() >= confirmed)){
                    //通知钱包
                    Map<String, ?> res = null;
                    Integer type = record.getType();
                    Map<String, String> map = new HashMap();
                    if (TransactionTypeEnum.DEPOSIT.getCode().equals(type)){
                        map.put("timestamp", new Date().getTime() + "");
                        map.put("symbol", "QTUM");
                        map.put("txid", record.getTxId());
//                        map.put("amount", );
//                        map.put("address_to", );
                        map.put("confirm", record.getConfirmed() + "");
                        map.put("is_mining", "1");
                        setParam(map, record);
                        res = walletRemotService.depositNotify(map);
                        logger.info("钱包返回信息=====" + JSONObject.toJSONString(res));
                        if (res == null || !Constants.CODE_SUCCESS.equals(res.get("errno"))){
                            logger.error("通知钱包失败");
                            continue;
                        }
                    } else if (TransactionTypeEnum.EXTRACT.getCode().equals(type)){
                        map.put("trans_id", record.getRecordId());
                        map.put("symbol", "QTUM");
                        map.put("address_to", "0");
                        map.put("txid", record.getTxId());
                        map.put("amount", "0");
                        map.put("confirm", record.getConfirmed() + "");
                        map.put("real_fee", record.getFee().toString());
                        res = walletRemotService.withdrawNotify(map);
                        logger.info("钱包返回信息=====" + JSONObject.toJSONString(res));
                        if (res == null || !Constants.CODE_SUCCESS.equals(res.get("errno"))){
                            logger.error("通知钱包失败");
                            continue;
                        }
                    }
                    updateUnconfirmedAmount(record);
                }
                queryUpdateMap.put(query, update);
            } catch (Exception e){
                logger.error("", e);
            }
        }
        if (!queryUpdateMap.isEmpty())
            mongoUtil.batchExecuteUpdate(queryUpdateMap, MongoConstant.TRANSACTION_CLASS);

    }

    /**
     * 获取充值地址和充值金额专用
     * @param map
     * @param record
     */
    private void setParam(Map<String, String> map, TransactionRecord record){
        List<Vout> voutList = record.getVoutList();
        Map<String, Collection<String>> query = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (Vout vout : voutList){
            String address = vout.getAddress();
            if (address != null){
                list.add(address);
            }
        }
        query.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, list);
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, qtumUtil.getTokenAddressName("QTUM"));
        if (CollectionUtils.isEmpty(addresses))
            throw new BusinessException();
        //充值地址
        String depositAddress = addresses.get(0).getAddress();
        if (addresses.size() == 2){
            Query addressQuery = new Query();
            Criteria addressCriteria = new Criteria();
            addressCriteria.and(MongoConstant.QTUM_ADDRESS_CLASS_TYPE).is(0);
            addressQuery.addCriteria(addressCriteria);
            List<Address> data = mongoUtil.getData(addressQuery, Address.class, qtumUtil.getTokenAddressName("QTUM"));
            if (CollectionUtils.isEmpty(data)){
                throw new BusinessException();
            }
            Address systemAddress = data.get(0);
            String address = systemAddress.getAddress();
            if (address.equals(addresses.get(0).getAddress())){
                depositAddress = addresses.get(1).getAddress();
            } else {
                depositAddress = addresses.get(0).getAddress();
            }
        }
        map.put("address_to", depositAddress);
        BigDecimal amount = BigDecimal.ZERO;
        for (Vout vout : voutList){
            String address = vout.getAddress();
            if (address != null && depositAddress.equals(vout.getAddress())){
                amount = vout.getValue();
            }
        }
        map.put("amount", amount.toString());
    }

    private void updateUnconfirmedAmount(TransactionRecord record) {
        String updateAddressBalanceLock = RedisKeyConstants.QTUM_UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try {
            List<String> addressList = new ArrayList<>();
            List<Vin> vinList = record.getVinList();
            Integer type = record.getType();
            for (Vin vin : vinList){
                String address = vin.getAddress();
                if (!StringUtils.isEmpty(address))
                    addressList.add(address);
            }
            List<Vout> vouts = record.getVoutList();
            for (Vout vout : vouts){
                String address = vout.getAddress();
                if (!StringUtils.isEmpty(address))
                    addressList.add(address);
            }
            Map<String, Collection<String>> query = new HashMap<>();
            query.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, addressList);
            List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, qtumUtil.getTokenAddressName("QTUM"));
            if (!CollectionUtils.isEmpty(addresses)){
                for (Vin vin : vinList){
                    String address = vin.getAddress();
                    for (Address a : addresses){
                        if (a.getAddress().equals(address)){
                            //扣钱
                            Query q = new Query();
                            Criteria criteria = new Criteria();
                            criteria.and(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS).is(address);
                            q.addCriteria(criteria);
                            Update update = new Update();
                            Address inAddress = getAddress(address, "QTUM");
                            update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(inAddress.getUnconfirmed() + "").add(vin.getValue()).doubleValue());
                            update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_CONFIRMED, new BigDecimal(inAddress.getConfirmed() + "").add(vin.getValue().negate()).doubleValue());
                            mongoUtil.update(q, qtumUtil.getTokenAddressName("QTUM"), update);
                            //修改utxo的状态
                            Query utxoQuery = new Query();
                            Criteria utxoCriteria = new Criteria();
                            utxoCriteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(vin.getTxId());
                            utxoCriteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(vin.getVout());
                            utxoQuery.addCriteria(utxoCriteria);
                            Update utxoUpdate = new Update();
                            utxoUpdate.set(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS, 2);
                            mongoUtil.update(utxoQuery, MongoConstant.QTUM_UTXO_CLASS_NAME, utxoUpdate);
                        }
                    }
                }
                for (Vout vout : vouts){
                    String address = vout.getAddress();
                    for (Address a : addresses){
                        if (a.getAddress().equals(address)){
                            //加钱
                            Query q = new Query();
                            Criteria criteria = new Criteria();
                            criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS).is(address);
                            q.addCriteria(criteria);
                            Update update = new Update();
                            Address outAddress = getAddress(address, vout.getCoinType());
                            //如果是提币，则不去修改未确认数，因为自己发起的提币不会修改toAddress的未确认金额
                            if (!(TransactionTypeEnum.EXTRACT.getCode().equals(type) || TransactionTypeEnum.COLLECT.getCode().equals(type)))
                                update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(outAddress.getUnconfirmed() + "").add(vout.getValue().negate()).doubleValue());
                            update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_CONFIRMED, new BigDecimal(outAddress.getConfirmed() + "").add(vout.getValue()).doubleValue());
                            mongoUtil.update(q, qtumUtil.getTokenAddressName(vout.getCoinType()), update);
                            //修改utxo的状态
                            //提币的话需要加上找零的utxo
                            if (TransactionTypeEnum.EXTRACT.getCode().equals(type) || (TransactionTypeEnum.COLLECT.getCode().equals(type) && record.getRecordId().indexOf("_1") != -1)){
                                UTXO utxo = qtumUtil.getUtxo(record, vout);
                                utxo.setStatus(0);
                                mongoUtil.insert(utxo, MongoConstant.QTUM_UTXO_CLASS_NAME);
                            } else {
                                Query utxoQuery = new Query();
                                Criteria utxoCriteria = new Criteria();
                                utxoCriteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(record.getTxId());
                                utxoCriteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(vout.getVout());
                                utxoQuery.addCriteria(utxoCriteria);
                                Update utxoUpdate = new Update();
                                utxoUpdate.set(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS, 0);
                                mongoUtil.update(utxoQuery, MongoConstant.QTUM_UTXO_CLASS_NAME, utxoUpdate);
                            }
                        }
                    }
                }
            }
        } finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }

    private Address getAddress(String address, String coinType){
        String extractLock = RedisKeyConstants.TRANSACTION_ADD_ADDRESS_LOCK + coinType;
        redisLockUtils.lock(extractLock);
        try {
            Address newAddress = getAddress(address, coinType, 1);
            if (newAddress != null)
                return newAddress;
            newAddress = new Address();
            newAddress.setType(AddressTypeEnum.MEMBER_ADDRESS.getCode());
            newAddress.setAddress(address);
            newAddress.setCreateDate(new Date());
            newAddress.setConfirmed(BigDecimal.ZERO.doubleValue());
            newAddress.setUnconfirmed(BigDecimal.ZERO.doubleValue());
            mongoUtil.insert(newAddress, qtumUtil.getTokenAddressName(coinType));
            return newAddress;
        } finally {
            redisLockUtils.releaseLock(extractLock);
        }
    }

    private Address getAddress(String address, String coinType, Integer i){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS).is(address);
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, qtumUtil.getTokenAddressName(coinType));
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }

    private BigDecimal getAddressBalance(String address, String coinType){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS).is(address);
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, qtumUtil.getTokenAddressName(coinType));
        Double confirmed = data.get(0).getConfirmed();
        return confirmed == null ? BigDecimal.ZERO : new BigDecimal(confirmed + "");
    }

    private void insertContractTransactionRecord(InsertQtumRecordParam param, Integer type) {
        String updateAddressBalanceLock = RedisKeyConstants.QTUM_UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try {
            BigDecimal amount = param.getAmount();
            BigDecimal gasLimit = param.getGasLimit();
            BigDecimal gasPrice = param.getGasPrice();
            TransactionRecord record = new TransactionRecord();
            String coinType = param.getCoinType();
            record.setCreateDate(new Date());
            record.setFee(fee);
            record.setConfirmed(0);
            record.setHex(param.getHex());
            record.setRecordId(param.getRecordId());
            record.setTxId(param.getTxid());
            record.setType(type);
            record.setGasLimit(gasLimit);
            record.setGasPrice(gasPrice);
            List<Vin> vins = new ArrayList<>();
            List<Vout> vouts = new ArrayList<>();
            BigDecimal count = BigDecimal.ZERO;
            List<UTXO> payList = param.getPayList();
            Query query = null;
            Criteria criteria = null;
            Update update = null;
            //修改utxo的金额
            for (UTXO utxo : payList){
                Vin vin = new Vin();
                String utxoAddress = utxo.getAddress();
                vin.setAddress(utxoAddress);
                vin.setVout(utxo.getVout());
                vin.setTxId(utxo.getTxId());
                BigDecimal utxoAmount = utxo.getAmount();
                vin.setValue(utxoAmount);
                count = count.add(utxoAmount);
                vin.setScriptPubKey(utxo.getScriptPubKey());
                vins.add(vin);
                //地址的已确认金额扣钱
                query = new Query();
                criteria = new Criteria();
                criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS).is(utxoAddress);
                query.addCriteria(criteria);
                update = new Update();
                Address address = getAddress(utxoAddress, "QTUM");
                update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(address.getUnconfirmed() + "").add(utxoAmount.negate()).doubleValue());
                mongoUtil.update(query, qtumUtil.getTokenAddressName("QTUM"), update);
                //修改utxo的状态
                query = new Query();
                criteria = new Criteria();
                criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
                criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
                query.addCriteria(criteria);
                update = new Update();
                update.set(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS, 1);
                mongoUtil.update(query, MongoConstant.QTUM_UTXO_CLASS_NAME, update);
            }
            //修改代币地址金额
            updateAddressBalance(param.getFromAddress(), param.getCoinType(), amount.negate());
            //找零
            Vout toVout = new Vout();
            toVout.setAddress(param.getToAddress());
            toVout.setValue(amount);
            vouts.add(toVout);
            BigDecimal subtract = count.subtract(fee).subtract(qtumUtil.qtumAmountFormate8(gasLimit.multiply(gasPrice)));
            if (subtract.compareTo(BigDecimal.ZERO) == 1){
                Vout changeVout = new Vout();
                changeVout.setAddress(param.getFromAddress());
                changeVout.setValue(subtract);
                vouts.add(changeVout);
            }
            record.setVinList(vins);
            record.setVoutList(vouts);
            Query q = new Query();
            Criteria c = new Criteria();
            c.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).is(record.getTxId());
            q.addCriteria(c);
            List<TransactionRecord> data = mongoUtil.getData(q, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
            if (CollectionUtils.isEmpty(data)) {
                mongoUtil.insert(record, MongoConstant.QTUM_TRANSACTION_CLASS);
            }
        }finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }


    private void insertTransactionRecord(InsertQtumRecordParam param, Integer type) throws ParseException {
        String updateAddressBalanceLock = RedisKeyConstants.QTUM_UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try {
            TransactionRecord record = new TransactionRecord();
            String coinType = param.getCoinType();
            record.setCreateDate(new Date());
            record.setFee(fee);
            record.setConfirmed(0);
            record.setHex(param.getHex());
            record.setRecordId(param.getRecordId());
            record.setTxId(param.getTxid());
            record.setFromAddress(param.getFromAddress());
            record.setToAddress(param.getToAddress());
            record.setType(type);
            List<Vin> vins = new ArrayList<>();
            List<Vout> vouts = new ArrayList<>();
            BigDecimal count = BigDecimal.ZERO;
            List<UTXO> payList = param.getPayList();
            Query query = null;
            Criteria criteria = null;
            Update update = null;
            for (UTXO utxo : payList){
                Vin vin = new Vin();
                String utxoAddress = utxo.getAddress();
                vin.setAddress(utxoAddress);
                vin.setVout(utxo.getVout());
                vin.setTxId(utxo.getTxId());
                BigDecimal utxoAmount = utxo.getAmount();
                vin.setValue(utxoAmount);
                count = count.add(utxoAmount);
                vin.setScriptPubKey(utxo.getScriptPubKey());
                vins.add(vin);
                //地址的已确认金额扣钱
                query = new Query();
                criteria = new Criteria();
                criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS).is(utxoAddress);
                query.addCriteria(criteria);
                update = new Update();
                Address address = getAddress(utxoAddress, coinType);
                update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(address.getUnconfirmed() + "").add(utxoAmount.negate()).doubleValue());
                mongoUtil.update(query, qtumUtil.getTokenAddressName(coinType), update);
                //修改utxo的状态
                query = new Query();
                criteria = new Criteria();
                criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
                criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
                query.addCriteria(criteria);
                update = new Update();
                update.set(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS, 1);
                mongoUtil.update(query, MongoConstant.QTUM_UTXO_CLASS_NAME, update);
            }
            BigDecimal amount = param.getAmount();
            Vout toVout = new Vout();
            toVout.setAddress(param.getToAddress());
            toVout.setValue(amount);
            vouts.add(toVout);
            BigDecimal subtract = count.subtract(amount).subtract(fee);
            if (subtract.compareTo(BigDecimal.ZERO) == 1){
                Vout changeVout = new Vout();
                changeVout.setAddress(param.getFromAddress());
                changeVout.setValue(subtract);
                vouts.add(changeVout);
            }
            record.setVinList(vins);
            record.setVoutList(vouts);
            Query q = new Query();
            Criteria c = new Criteria();
            c.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).is(record.getTxId());
            q.addCriteria(c);
            List<TransactionRecord> data = mongoUtil.getData(q, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
            if (CollectionUtils.isEmpty(data)) {
                mongoUtil.insert(record, MongoConstant.QTUM_TRANSACTION_CLASS);
            }
        } finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }


    private SignQtumTransactionParams getParams(List<UTXO> payList, String toAddress,
                                               String password, String changeAddress, BigDecimal amount){
        SignQtumTransactionParams params = new SignQtumTransactionParams();
        BigDecimal unit = new BigDecimal("100000000");
        params.setAmount(amount.multiply(unit).longValue());
        params.setFee(fee.multiply(unit).longValue());
        params.setToAddress(toAddress);
        params.setChangeAddress(changeAddress);
        params.setPassword(password);
        List<UnSpentQtumData> list = new ArrayList<>();
        for (UTXO utxo : payList){
            UnSpentQtumData unSpentQtumData = new UnSpentQtumData();
            unSpentQtumData.setTxId(utxo.getTxId());
            unSpentQtumData.setHeight(utxo.getHeight());
            unSpentQtumData.setSatoshis(utxo.getAmount().multiply(unit).longValue());
            unSpentQtumData.setVout(Long.valueOf(utxo.getVout() + ""));
            unSpentQtumData.setScriptPubKey(utxo.getScriptPubKey().getHex());
            list.add(unSpentQtumData);
        }
        params.setUnSpentBtcs(list);
        return params;
    }

    private QtumContractSignParams getContractParams(List<UTXO> payList,String fromAddress, String toAddress,
                                     String password, String changeAddress, BigDecimal amount, ContractConfig config) throws Throwable {
        QtumContractSignParams params = new QtumContractSignParams();
        BigInteger precision = config.getPrecision();
        params.setFromAddress(fromAddress);
        params.setToAddressHex(qtumUtil.getAddressHex(toAddress));
        params.setContractAddress(config.getContract());
        params.setDecimal(precision);
        params.setGasLimit(config.getGaslimit());
        params.setGasPrice(config.getGasprice());
        params.setAmount(qtumUtil.qtumAmountFormateSumN(amount, config.getPrecision()));
        params.setToAddress(toAddress);
        params.setChangeAddress(changeAddress);
        params.setFee(qtumUtil.qtumAmountFormateSumN(fee, new BigInteger("8")).toBigInteger());
        params.setPassword(password);
        List<UnSpentQtumData> list = new ArrayList<>();
        for (UTXO utxo : payList){
            UnSpentQtumData unSpentQtumData = new UnSpentQtumData();
            unSpentQtumData.setTxId(utxo.getTxId());
            unSpentQtumData.setHeight(utxo.getHeight());
            unSpentQtumData.setSatoshis(qtumUtil.qtumAmountFormateSumN(utxo.getAmount(), precision).longValue());
            unSpentQtumData.setVout(Long.valueOf(utxo.getVout() + ""));
            unSpentQtumData.setScriptPubKey(utxo.getScriptPubKey().getHex());
            list.add(unSpentQtumData);
        }
        params.setUnSpentBtcs(list);
        return params;
    }

    private boolean isBalanceNotEnough(BigDecimal amount){
        QtumUnSpentList addressUsedBtcUnSpentList = new QtumUnSpentList(mongoUtil, confirmed, 1);
        List<UTXO> usedUtxos = addressUsedBtcUnSpentList.get(amount);
        if (CollectionUtils.isEmpty(usedUtxos)){
            return true;
        }
        for (UTXO utxo : usedUtxos) {
            amount = amount.subtract(utxo.getAmount());
        }
        if (amount.compareTo(BigDecimal.ZERO) == 1) {
            return true;
        }
        return false;
    }

    private TransactionRecord hasRecordId(String recordId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_RECORD_ID).is(recordId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }


    private void tx(Map<String, TransactionRecord> selfTransactionMap, String txId, Integer height, int index){
        try{
            TransactionRecord transaction = qtumUtil.getTransaction(txId, height, index);
            //获取所有utxo
            List<UTXO> unSpentUTXO = qtumUtil.getUnSpentUTXO(transaction);
            //保存自己的utxo，返回是否存在自己的交易
            if (selfTransactionMap.containsKey(txId))
                return;
            if (saveDepositUTXOInfo(unSpentUTXO, transaction.getConfirmed())){
                //如果是自己的充值交易
                mongoUtil.insert(transaction, MongoConstant.QTUM_TRANSACTION_CLASS);
            }
        } catch (Exception e){
            logger.error("", e);
        } catch (Throwable throwable) {
            logger.error("", throwable);
        }
    }


    private boolean saveDepositUTXOInfo(List<UTXO> utxos, Integer confirmed){
        boolean flag = false;
        if (!CollectionUtils.isEmpty(utxos)){
            Map<String, Collection<String>> query = new HashMap<>();
            List<String> list = new ArrayList<>();
            for (UTXO utxo : utxos){
                String address = utxo.getAddress();
                if (address != null){
                    list.add(address);
                }
            }
            query.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, list);
            //查询是否为自己的地址
            List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, qtumUtil.getTokenAddressName("QTUM"));
             if (!CollectionUtils.isEmpty(addresses)){
                int i = utxos.size() - 1;
                for ( ; i >= 0; i--){
                    UTXO utxo = utxos.get(i);
                    String utxoAddress = utxo.getAddress();
                    if (!hasAddress(addresses, utxoAddress)){
                        continue;
                    }
                    if ("call".equals(utxo.getScriptPubKey().getType())){
                        updateAddressUnconfirmed(utxo.getAddress(), utxo.getCoinType(), utxo.getAmount());
                        continue;
                    }
                    createUTXO(utxo, confirmed);
                }
                flag = true;
            }
        }
        return flag;
    }


    private String sendQtum(BigDecimal amount, String businessTxId, String toAddress, String fromAddress, String coinType, TransactionTypeEnum type) throws Throwable {
        //在所有的未被花费的utxo中找合适的utxo
        QtumUnSpentList qtumUnSpentList = null;
        //如果是搜集
        if (type.getCode() == TransactionTypeEnum.COLLECT.getCode()){
            List<String> addresses = new ArrayList<>();
            addresses.add(toAddress);
            qtumUnSpentList = new QtumUnSpentList(mongoUtil, confirmed, addresses);
        } else {
            qtumUnSpentList = new QtumUnSpentList(mongoUtil, confirmed);
        }
        BigDecimal count = amount.add(fee);
        List<UTXO> utxos = qtumUnSpentList.get(count);
        if (CollectionUtils.isEmpty(utxos)) {
            if (isBalanceNotEnough(amount.add(fee))){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
            }
        }
        for (UTXO utxo : utxos) {
            count = count.subtract(utxo.getAmount());
        }
        if (count.compareTo(BigDecimal.ZERO) == 1) {
            if (isBalanceNotEnough(amount.add(fee))){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
            }
        }
        //获取了足够的utxo去签名
        ResponseData<String> responseData = signRemotService.qtumSign(getParams(utxos, toAddress, CoinConstant.SIGN_PASSWORD, fromAddress, amount));
        if (!responseData.isSuccessful()) {
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException(BusinessExceptionCode.GO_TO_SIGN_ERROR);
        }
        String hex = responseData.getData();
        String txid = qtumUtil.sendRawTransaction(hex);
        //保存记录
        InsertQtumRecordParam record = new InsertQtumRecordParam();
        record.setAmount(amount);
        record.setFromAddress(fromAddress);
        record.setHex(hex);
        record.setTxid(txid);
        record.setRecordId(businessTxId);
        record.setPayList(utxos);
        record.setToAddress(toAddress);
        record.setCoinType(coinType);
        try {
            insertTransactionRecord(record, type.getCode());
        } catch (ParseException e) {
            logger.error("", e);
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException();
        }
        return txid;
    }

    private String sendQtumContract(BigDecimal amount, String businessTxId, String toAddress, String fromAddress, String coinType, TransactionTypeEnum type) throws Throwable {
        Map<String, ContractConfig> coinTypeConfig = qtumAndContractConfig.getCoinTypeConfig();
        ContractConfig config = coinTypeConfig.get(coinType);

        //在from地址中找手续费
        QtumUnSpentList qtumUnSpentList = new QtumUnSpentList(mongoUtil, confirmed, fromAddress);
        BigDecimal count = fee;
        count = count.add(qtumUtil.qtumAmountFormateN(new BigDecimal(config.getGaslimit().multiply(config.getGasprice())), 8));
//        BigDecimal payFee = count;
        List<UTXO> utxos = qtumUnSpentList.get(count);
        if (CollectionUtils.isEmpty(utxos)) {
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
        }
        for (UTXO utxo : utxos) {
            count = count.subtract(utxo.getAmount());
        }
        if (count.compareTo(BigDecimal.ZERO) == 1) {
            if (isBalanceNotEnough(amount.add(fee))){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
            }
        }
        Address address = getAddress(fromAddress, coinType);
        if (new BigDecimal(address.getConfirmed() + "").compareTo(amount) == -1){
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
        }
        //获取了足够的utxo去签名
        ResponseData<String> responseData = signRemotService.qtumSign(getContractParams(
                utxos,fromAddress, toAddress, CoinConstant.SIGN_PASSWORD, fromAddress, amount, config));
        if (!responseData.isSuccessful()) {
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException(BusinessExceptionCode.GO_TO_SIGN_ERROR);
        }
        String hex = responseData.getData();
        String txid = qtumUtil.sendRawTransaction(hex);
        //保存记录
        InsertQtumRecordParam record = new InsertQtumRecordParam();
        record.setAmount(amount);
        record.setFromAddress(fromAddress);
        record.setHex(hex);
        record.setTxid(txid);
        record.setRecordId(businessTxId);
        record.setPayList(utxos);
        record.setToAddress(toAddress);
        record.setCoinType(coinType);
        record.setGasPrice(new BigDecimal(config.getGasprice()));
        record.setGasLimit(new BigDecimal(config.getGaslimit()));
        try {
            insertContractTransactionRecord(record, type.getCode());
        } catch (Exception e) {
            logger.error("", e);
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException();
        }
        return txid;
    }

    private boolean hasAddress(List<Address> addresses, String find){
        for (Address address : addresses){
            if (address.getAddress().equals(find)){
                return true;
            }
        }
        return false;
    }

    private void updateAddressBalance(String address, String coinType, BigDecimal amount){
        String updateAddressBalanceLock = RedisKeyConstants.QTUM_UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try {
            Query query = new Query();
            Criteria criteria = new Criteria();
            criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS).is(address);
            query.addCriteria(criteria);
            List<Address> data = mongoUtil.getData(query, Address.class, qtumUtil.getTokenAddressName(coinType));
            if (!CollectionUtils.isEmpty(data)){
                Address addre = data.get(0);
                Update update = new Update();
                update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_CONFIRMED, new BigDecimal(addre.getConfirmed() + "").add(amount).doubleValue());
                mongoUtil.update(query, qtumUtil.getTokenAddressName(coinType), update);
            }
        }finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }

    private void updateAddressUnconfirmed(String address, String coinType, BigDecimal amount){
        String updateAddressBalanceLock = RedisKeyConstants.QTUM_UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try {
            Address addre = getAddress(address, coinType);
            Update update = new Update();
            update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(addre.getUnconfirmed() + "").add(amount).doubleValue());
            Query query = new Query();
            Criteria criteria = new Criteria();
            criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS).is(address);
            query.addCriteria(criteria);
            mongoUtil.update(query, qtumUtil.getTokenAddressName(coinType), update);
        } finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }

    private Map<String, TransactionRecord> getSelfTransaction(List<String> txids){
        Map<String, TransactionRecord> map = new HashMap();
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).in(txids);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
        if (!CollectionUtils.isEmpty(data)){
            for (TransactionRecord tx : data){
                map.put(tx.getTxId(), tx);
            }
        }
        return map;
    }

    private void createUTXO(UTXO utxo, Integer confirmed){
        String updateAddressBalanceLock = RedisKeyConstants.QTUM_UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try{
            Query utxoQuery = new Query();
            Criteria criteria = new Criteria();
            criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
            criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
            utxoQuery.addCriteria(criteria);
            List<UTXO> data = mongoUtil.getData(utxoQuery, UTXO.class, MongoConstant.UTXO_CLASS_NAME);
            //防止重复添加utxo
            if (CollectionUtils.isEmpty(data)){
                Map addBalance = new HashMap();
                Update update = new Update();
                try {
                    Address address = getAddress(utxo.getAddress(), "QTUM");
                    //并加钱  确认数到了的改交易记录状态
                    if (confirmed >= this.confirmed){
                        update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_CONFIRMED, new BigDecimal(address.getConfirmed() + "").add(utxo.getAmount()).doubleValue());
                        utxo.setStatus(0);
                    } else {
                        utxo.setStatus(1);
                        update.set(MongoConstant.QTUM_ADDRESS_CLASS_NAME_UNCONFIRMED, new BigDecimal(address.getUnconfirmed() + "").add(utxo.getAmount()).doubleValue());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                addBalance.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, utxo.getAddress());
                mongoUtil.update(addBalance, qtumUtil.getTokenAddressName("QTUM"), update);
                //保存utxo
                mongoUtil.insert(utxo, MongoConstant.QTUM_UTXO_CLASS_NAME);
            }
        } finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }

    private TransactionRecord getSendCollectServerCharge(String systemCollectServerChargeAddress, String toAddress){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_TOADDRESS).is(toAddress);
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_FROMADDRESS).is(systemCollectServerChargeAddress);
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_IS_USED).ne(TransactionUseTypeEnum.USED.getCode());
        query.addCriteria(criteria);
        query.limit(1);
        query.with(new Sort(new Sort.Order(Sort.Direction.DESC,"createDate")));
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }

    private void useServiceCharge(String txId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).is(txId);
        query.addCriteria(criteria);
        Update update = new Update();
        update.set(MongoConstant.QTUM_TRANSACTION_CLASS_IS_USED, TransactionUseTypeEnum.USED.getCode());
        mongoUtil.update(query, MongoConstant.QTUM_TRANSACTION_CLASS, update);
    }

    private boolean hasNotEnoughServerCharge(String address, BigDecimal serviceCharge){
        BigDecimal qtum = getAddressBalance(address, "QTUM");
        return qtum.subtract(serviceCharge).subtract(fee).compareTo(BigDecimal.ZERO) == -1;
    }

    private Integer getMax(Integer v1, Integer v2){
        if (v1 == null){
            v1 = 1;
        }
        if (v2 == null){
            v2 = 1;
        }
        return v1 > v2 ? v1 : v2;
    }
}
