package com.zatgo.zup.transaction.util.btc;

import com.zatgo.zup.transaction.entity.btc.*;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.util.MongoUtil;
import org.bitcoinj.wallet.UnreadableWalletException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Mikhail Yevchenko m.ṥῥẚɱ.ѓѐḿởύḙ@azazar.com
 */

@Component
public class BitcoinUtil {


    @Value("${transaction.user}")
    private String user;
    @Value("${transaction.password}")
    private String password;
    @Value("${transaction.host}")
    private String host;
    @Value("${transaction.port}")
    private String port;
    @Autowired
    private MongoUtil mongoUtil;


    private static BitcoinJSONRPCClient client = null;

    private static Pattern P2PKH = Pattern.compile("^OP_DUP OP_HASH160 [a-zA-Z0-9]* OP_EQUALVERIFY OP_CHECKSIG$");

    private static Pattern P2SH = Pattern.compile("^OP_HASH160 [a-zA-Z0-9]* OP_EQUAL$");

    /**
     * 获取块hash
     * @param height
     * @return
     */
    public String getBlockHash(Integer height){
        return client.getBlockHash(height);
    }

    /**
     * 获取当前区块高度
     * @return
     */
    public Integer getBlockCount() {
        return getBitcoinJSONRPCClient().getBlockCount();
    }

    /**
     * 获取区块中所有的交易ID list
     * @return
     */
    public List<String> getBlockTx(String blockHash) {
        BitcoindRpcClient.Block block = getBitcoinJSONRPCClient().getBlock(blockHash);
        if (block == null){
            return null;
        }
        return block.tx();
    }

    public List<BitcoindRpcClient.Transaction> getBlockListTx(Integer height) {
        BitcoinJSONRPCClient bitcoinJSONRPCClient = getBitcoinJSONRPCClient();
        BitcoindRpcClient.TransactionsSinceBlock transactionsSinceBlock = bitcoinJSONRPCClient.listSinceBlock(bitcoinJSONRPCClient.getBlockHash(height));
        if (transactionsSinceBlock == null){
            return null;
        }
        return transactionsSinceBlock.transactions();
    }

    /**
     * 根据交易Id获取交易信息
     * @param txId
     * @return
     */
    public TransactionRecord getTransaction(String txId, Integer height) {
        BitcoindRpcClient.RawTransaction rawTransaction = getBitcoinJSONRPCClient().getRawTransaction(txId);
        if (rawTransaction == null){
            return null;
        }
        TransactionRecord record = new TransactionRecord();
        record.setHeight(height);
        record.setCreateDate(rawTransaction.time());
        record.setConfirmed(rawTransaction.confirmations());
        record.setHex(rawTransaction.hex());
        record.setTxId(rawTransaction.txId());
        record.setType(TransactionTypeEnum.DEPOSIT.getCode());
        getVin(record, rawTransaction);
        getVout(record, rawTransaction);
        record.setBlockDate(rawTransaction.blocktime());
        record.setCreateDate(new Date());
        return record;
    }

    public TransactionRecord getTransaction(String txId) {
        BitcoindRpcClient.RawTransaction rawTransaction = getBitcoinJSONRPCClient().getRawTransaction(txId);
        if (rawTransaction == null){
            return null;
        }
        TransactionRecord record = new TransactionRecord();
        record.setCreateDate(rawTransaction.time());
        record.setConfirmed(rawTransaction.confirmations());
        record.setHex(rawTransaction.hex());
        record.setTxId(rawTransaction.txId());
        getVin(record, rawTransaction);
        getVout(record, rawTransaction);
        record.setBlockDate(rawTransaction.blocktime());
        record.setCreateDate(new Date());
        return record;
    }

    /**
     * 根据交易记录获取未被消费的UTXO
     * @param record
     * @return
     */
    public List<UTXO> getUnSpentUTXO(TransactionRecord record) {
        List<UTXO> list = new ArrayList<>();
        List<Vout> voutList = record.getVoutList();
        for (Vout out : voutList){
            list.add(getUtxo(record, out));
        }
        return list;
    }

    /**
     * 根据交易记录获取已消费的UTXO
     * @param record
     * @return
     */
    public List<UTXO> getSpentUTXO(TransactionRecord record) {
        List<UTXO> list = new ArrayList<>();
        List<Vin> vinList = record.getVinList();
        Date date = new Date();
        for (Vin in : vinList){
            UTXO utxo = new UTXO();
            String address = in.getAddress();
//            if (StringUtils.isEmpty(address)){
//                if (!StringUtils.isEmpty(txId)){
//                    TransactionRecord transaction = getTransaction(txId);
//                    if (transaction != null){
//                        List<Vout> voutList = transaction.getVoutList();
//                        Vout vout = voutList.get(in.getVout());
//                        address = vout.getAddress();
//                        utxo.setAmount(vout.getValue());
//                    }
//                }
//            }
            utxo.setTxId(in.getTxId());
            utxo.setAddress(address);
            utxo.setScriptPubKey(in.getScriptPubKey());
            utxo.setVout(in.getVout());
            utxo.setHeight(record.getHeight());
            utxo.setBlockDate(record.getBlockDate());
            utxo.setCreateDate(date);
            list.add(utxo);
        }
        return list;
    }

    public UTXO getUtxo(TransactionRecord record, Vout out){
        Date date = new Date();
        UTXO utxo = new UTXO();
        utxo.setTxId(record.getTxId());
        utxo.setAddress(out.getAddress());
        utxo.setBlockDate(record.getBlockDate());
        utxo.setVout(out.getVout());
        utxo.setHeight(record.getHeight());
        utxo.setScriptPubKey(out.getScriptPubKey());
        utxo.setAmount(out.getValue());
        utxo.setCreateDate(date);
        utxo.setStatus(0);
        return utxo;
    }

    /**
     * 发送交易
     * @param hex
     * @return
     */
    public String sendTransaction(String hex){
       return getBitcoinJSONRPCClient().sendRawTransaction(hex);
    }

    public String sendTransaction(List<UTXO> utxoList, String fromAddress, BigDecimal amount) {
        return null;
    }













    private BitcoinJSONRPCClient getBitcoinJSONRPCClient(){
        if (client == null){
            try {
                URL url = new URL("http://" + user + ':' + password + "@" + host + ":" + (port == null ? "8332" : port) + "/");
                client = new BitcoinJSONRPCClient(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return client;
    }


    private void getVin(TransactionRecord record, BitcoindRpcClient.RawTransaction rawTransaction){
        List<BitcoindRpcClient.RawTransaction.In> ins = rawTransaction.vIn();
        List<Vin> list = new ArrayList<>();
        for (BitcoindRpcClient.RawTransaction.In in : ins){
            Vin vin = new Vin();
            vin.setTxId(in.txid());
            Map<String, Object> map = in.scriptSig();
            if (map != null){
                Object hex = map.get("hex");
                vin.setScriptPubKey(hex == null ? "" : (String) hex);
            }
            vin.setVout(in.vout());
            list.add(vin);
        }
        record.setVinList(list);
    }

    private void getVout(TransactionRecord record, BitcoindRpcClient.RawTransaction rawTransaction){
        List<BitcoindRpcClient.RawTransaction.Out> outs = rawTransaction.vOut();
        List<Vout> list = new
                ArrayList<>();
        for (BitcoindRpcClient.RawTransaction.Out out : outs){
            Vout vout = new Vout();
            //addresses 可能为空的时候
            BitcoindRpcClient.RawTransaction.Out.ScriptPubKey scriptPubKey = out.scriptPubKey();
            String asm = scriptPubKey.asm();
            if (!P2PKH.matcher(asm).matches())
                continue;
            List<String> addresses = scriptPubKey.addresses();
            if (!CollectionUtils.isEmpty(addresses)){
                vout.setAddress(addresses.get(0));
            }
            vout.setScriptPubKey(out.scriptPubKey().hex());
            vout.setValue(out.value());
            vout.setVout(out.n());
            list.add(vout);
        }
        record.setVoutList(list);
    }

    public Address getAddress(String address){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS).is(address);
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        return data.get(0);
    }

    public static void main(String[] args) throws IOException, UnreadableWalletException {

        Matcher matcher = P2PKH.matcher("OP_DUP OP_HASH160 3d75d61e0c4fcd352e171ea31c586fd6da9ab0b0 OP_EQUALVERIFY OP_CHECKSIG");
        System.out.println(matcher.matches());
        matcher = P2SH.matcher("OP_HASH160 69f376f6d27898c2f0e05f1cd3cb19e58be1bfab OP_EQUAL");
        System.out.println(matcher.matches());
    }

}
