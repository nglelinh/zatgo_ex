package com.zatgo.zup.transaction.entity.mq;

/**
 * Created by 46041 on 2018/10/29.
 */
public class SendTransactionCallbackMsg {

    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
