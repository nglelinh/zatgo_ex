package com.zatgo.zup.transaction.util.eth;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.model.CreateAddressRequest;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.transaction.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.entity.eth.Address;
import com.zatgo.zup.transaction.entity.eth.AddressTypeEnum;
import com.zatgo.zup.transaction.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.util.MongoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.DefaultBlockParameterNumber;
import org.web3j.protocol.core.methods.response.*;
import org.web3j.protocol.http.HttpService;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 46041 on 2018/10/24.
 */

@Component
public class EthUtil {

    private static final Logger logger = LoggerFactory.getLogger(EthUtil.class);

    private Web3j web3j = null;

    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private SignRemotService signRemotService;


    @Value("${transaction.eth.url}")
    private String gethClientUrl;


    public EthBlock getBlockTx(BigInteger height) throws IOException {
        DefaultBlockParameter defaultBlockParameter = new DefaultBlockParameterNumber(height);
        return getWeb3j().ethGetBlockByNumber(defaultBlockParameter, true).send();
    }

    public BigInteger getBlockCount() throws IOException {
        return getWeb3j().ethBlockNumber().send().getBlockNumber();
    }

    public EthGetTransactionReceipt getTransactionReceipt(String txId) throws IOException {
        return getWeb3j().ethGetTransactionReceipt(txId).send();
    }

    public EthTransaction getTransactionByHash(String txId) throws IOException {
        return getWeb3j().ethGetTransactionByHash(txId).send();
    }

    public BigInteger getNonce(String address) throws IOException {
        return getWeb3j().ethGetTransactionCount(address, DefaultBlockParameterName.PENDING).send().getTransactionCount();
    }

    public EthSendTransaction sendTransaction(String sign) throws IOException {
        return getWeb3j().ethSendRawTransaction(sign).send();
    }


    public Integer getMax(Integer v1, Integer v2){
        if (v1 == null){
            v1 = 1;
        }
        if (v2 == null){
            v2 = 1;
        }
        return v1 > v2 ? v1 : v2;
    }

    public Address getSystemAddress(String coinType){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_ADDRESSES_TYPE).is(AddressTypeEnum.SYSTEM_ADDRESS.getCode());
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, getTokenAddressName(coinType));
        if (!CollectionUtils.isEmpty(data)){
            return data.get(0);
        }
        return null;
    }

    public Address getSystemCollectAddress(){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_ADDRESSES_TYPE).is(AddressTypeEnum.SYSTEM_COLLECT.getCode());
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, getTokenAddressName("ETH"));
        if (!CollectionUtils.isEmpty(data)){
            return data.get(0);
        }
        return null;
    }

    public List<String> getAddress(Integer num){
        CreateAddressRequest request = new CreateAddressRequest();
        request.setNum(num);
        request.setPassword(CoinConstant.SIGN_PASSWORD);
        try {
            ResponseData<List<String>> newAddresses = signRemotService.getNewEthAddresses(request);
            if (newAddresses.isSuccessful()){
                return newAddresses.getData();
            }
            throw new BusinessException(newAddresses.getCode());
        } catch (Exception e){
            logger.error("", e);
        }
        return new ArrayList<>();
    }

    public String getTokenAddressName(String token){
        return token.toUpperCase() + "_" + MongoConstant.ETH_ADDRESSES_CLASS;
    }


    public static void main(String[] args) throws IOException {
        Web3j web3 = Web3j.build(new HttpService("https://mainnet.infura.io/5pvtsQXdsSwiLLSAwnEq"));
//        EthBlock.Block block = web3.ethGetBlockByHash("0xc15cd45737885cafa98ed9c1210929f2ec475cf1548f126b0b8eaca965626c7a", true).send().getBlock();
//        List<EthBlock.TransactionResult> transactions = block.getTransactions();
//        transactions.forEach(transaction -> {
//            JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(transaction));
//            String hash = object.getString("hash");
//            if ("0".equals(object.getString("value"))){
//                try {
//                    Optional<TransactionReceipt> transactionReceipt = web3.ethGetTransactionReceipt(hash).send().getTransactionReceipt();
//                    List<Log> logs = transactionReceipt.get().getLogs();
//                    System.out.println(logs);
//                    System.out.println(transactionReceipt);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        Optional<Transaction> transaction = web3.ethGetTransactionByHash("0x7590c09a3fddacd01ba13618e6aeebbb421814f9c34a7eec0665485644d38977").send().getTransaction();
//        JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(transaction.get()));
//        Optional<TransactionReceipt> transactionReceipt = web3.ethGetTransactionReceipt("0x7590c09a3fddacd01ba13618e6aeebbb421814f9c34a7eec0665485644d38977").send().getTransactionReceipt();
//        JSONObject object1 = JSONObject.parseObject(JSONObject.toJSONString(transactionReceipt.get()));
//        System.out.println(web3.ethGetTransactionCount("0x8645b90886545ee611679de24ae2a4b2b57c4b17", DefaultBlockParameterName.PENDING).send().getTransactionCount());
    }

    public BigDecimal ethAmountFormate18(BigDecimal amount){
        BigDecimal pow = BigDecimal.TEN.pow(18);
        return amount.divide(pow);
    }

    public BigDecimal ethAmountFormate9(BigDecimal amount){
        BigDecimal pow = BigDecimal.TEN.pow(9);
        return amount.divide(pow);
    }

    public BigDecimal ethAmountFormateSumN(BigDecimal amount, BigInteger n){
        BigDecimal pow = BigDecimal.TEN.pow(n.intValue());
        return amount.multiply(pow);
    }


    private Web3j getWeb3j(){
        if (web3j == null){
            web3j = Web3j.build(getService());
        }
        return web3j;
    }

    private HttpService getService() {
        return new HttpService(gethClientUrl);
    }

}
