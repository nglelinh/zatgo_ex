package com.zatgo.zup.transaction.listener;

import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.transaction.service.CoinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * Created by 46041 on 2018/10/19.
 */

@Component
public class Main {

    private final Logger logger = LoggerFactory.getLogger(Main.class);

    private CoinService coinService;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private ConfigurableApplicationContext applicationContext;


    @Value("${transaction.type}")
    private String type;


    @PostConstruct
    public void init() {
        String transactionLiveKey = RedisKeyConstants.TRANSACTION_LIVE_KEY + type;
        int count = 0;
        while (true){
            if (redisUtils.hasKey(transactionLiveKey)){
                count++;
            } else {
                break;
            }
            if (count >= 2){
                logger.error("================检测到同币种的交易机已启动，强制停止当前启动===================");
                Runtime.getRuntime().halt(-1);
            }
            try {
                Thread.sleep(5 * 1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //装载配置
        coinService = (CoinService) applicationContext.getBean(type + "Service");
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                logger.info("=============================开始启动扫描区块线程===================================");
                ScanBlockListener scanBlockListener = new ScanBlockListener(coinService);
                Thread scanBlock = new Thread(scanBlockListener);
                scanBlock.setDaemon(true);
                scanBlock.start();
                Daemon scanBlockDaemon = new Daemon(scanBlockListener, scanBlock);
                Thread sbDaemon = new Thread(scanBlockDaemon);
                sbDaemon.setDaemon(true);
                sbDaemon.start();
                logger.info("=============================启动扫描区块线程成功===================================");



//                logger.info("=============================开始启动确认区块线程===================================");
//                CheckTransactionListener checkTransactionListener = new CheckTransactionListener(coinService);
//                Thread checkTransaction = new Thread(checkTransactionListener);
//                checkTransaction.setDaemon(true);
//                checkTransaction.start();
//                Daemon checkTransactionDaemon = new Daemon(checkTransactionListener, checkTransaction);
//                Thread ctDaemon = new Thread(checkTransactionDaemon);
//                ctDaemon.setDaemon(true);
//                ctDaemon.start();
//                logger.info("=============================启动确认区块线程成功===================================");
////
////
////
//                logger.info("=============================开始启动获取提币交易线程===================================");
//                GetExtractRecordListener getBtcExtractRecordListener = new GetExtractRecordListener(coinService);
//                Thread getBtcExtractRecordThread = new Thread(getBtcExtractRecordListener);
//                getBtcExtractRecordThread.setDaemon(true);
//                getBtcExtractRecordThread.start();
//                Daemon getBtcExtractRecordDaemon = new Daemon(getBtcExtractRecordListener, getBtcExtractRecordThread);
//                Thread gbeDaemon = new Thread(getBtcExtractRecordDaemon);
//                gbeDaemon.setDaemon(true);
//                gbeDaemon.start();
//                logger.info("=============================启动获取提币交易线程成功===================================");
////
////
////
////
//                logger.info("=============================开始启动零钱搜集线程===================================");
//                CollectListener collectListener = new CollectListener(coinService);
//                Thread collectThread = new Thread(collectListener);
//                collectThread.setDaemon(true);
//                collectThread.start();
//                Daemon collectDaemon = new Daemon(collectListener, collectThread);
//                Thread cDaemon = new Thread(collectDaemon);
//                cDaemon.setDaemon(true);
//                cDaemon.start();
//                logger.info("=============================启动零钱搜集线程成功===================================");
////
////
////
//                logger.info("=============================开始启动检测地址线程===================================");
//                AddAddressListener addAddressListener = new AddAddressListener(coinService);
//                Thread addAddressThread = new Thread(addAddressListener);
//                addAddressThread.setDaemon(true);
//                addAddressThread.start();
//                Daemon addAddressDaemon = new Daemon(addAddressListener, addAddressThread);
//                Thread addDaemon = new Thread(addAddressDaemon);
//                addDaemon.setDaemon(true);
//                addDaemon.start();
//                logger.info("=============================启动检测地址线程成功===================================");

                while(true){
                    try {
                        redisUtils.put(transactionLiveKey, "1", 7l, TimeUnit.SECONDS);
                        Thread.sleep(5 * 1000l);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        Thread mainThread = new Thread(runnable);
        mainThread.start();
    }
}
