package com.zatgo.zup.transaction.conf;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by 46041 on 2018/10/25.
 */
public class ContractConfig {

    private String name;

    private String contract;

    private Integer confirmed;

    private BigInteger precision;

    private BigInteger gasprice;

    private BigInteger gaslimit;

    private BigDecimal collectlimit;

    private BigDecimal fee;

    private String systemAddress;

    private String systemServiceChargeAddress;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public BigInteger getPrecision() {
        return precision;
    }

    public void setPrecision(BigInteger precision) {
        this.precision = precision;
    }

    public BigInteger getGasprice() {
        return gasprice;
    }

    public void setGasprice(BigInteger gasprice) {
        this.gasprice = gasprice;
    }

    public BigInteger getGaslimit() {
        return gaslimit;
    }

    public void setGaslimit(BigInteger gaslimit) {
        this.gaslimit = gaslimit;
    }

    public BigDecimal getCollectlimit() {
        return collectlimit;
    }

    public void setCollectlimit(BigDecimal collectlimit) {
        this.collectlimit = collectlimit;
    }

    public String getSystemAddress() {
        return systemAddress;
    }

    public void setSystemAddress(String systemAddress) {
        this.systemAddress = systemAddress;
    }

    public String getSystemServiceChargeAddress() {
        return systemServiceChargeAddress;
    }

    public void setSystemServiceChargeAddress(String systemServiceChargeAddress) {
        this.systemServiceChargeAddress = systemServiceChargeAddress;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
}
