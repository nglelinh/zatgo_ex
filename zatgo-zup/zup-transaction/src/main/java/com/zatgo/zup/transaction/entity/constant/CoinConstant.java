package com.zatgo.zup.transaction.entity.constant;

import java.math.BigInteger;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by 46041 on 2018/10/18.
 */
public class CoinConstant {

    /**
     *  btc签名密码
     */
    public static String SIGN_PASSWORD = null;

    /**
     *  eth 转账
     */
    public static BigInteger ETH_GAS_LIMIT = new BigInteger("21000");
    /**
     *  eth 精度
     */
    public static BigInteger ETH_PRECISION = new BigInteger("8");

    public static String ETH_ADDRESS_LOCK = "eth_address_lock";

    public static String BTC_ADDRESS_LOCK = "btc_address_lock";

    public static String USDT_ADDRESS_LOCK = "usdt_address_lock";

    public static String QTUM_ADDRESS_LOCK = "qtum_address_lock";

    public static final ConcurrentHashMap<String, String> createAddressLockMap = new ConcurrentHashMap();
}
