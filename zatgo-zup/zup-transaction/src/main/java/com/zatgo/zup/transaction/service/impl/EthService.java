package com.zatgo.zup.transaction.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CreateAddressRequest;
import com.zatgo.zup.common.model.EthContractSignParams;
import com.zatgo.zup.common.model.EthSignParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.transaction.conf.ContractConfig;
import com.zatgo.zup.transaction.conf.EthAndContractConfig;
import com.zatgo.zup.transaction.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.entity.constant.Constants;
import com.zatgo.zup.transaction.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.entity.eth.*;
import com.zatgo.zup.transaction.entity.qtum.TransactionUseTypeEnum;
import com.zatgo.zup.transaction.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.remoteservice.WalletRemotService;
import com.zatgo.zup.transaction.service.CoinService;
import com.zatgo.zup.transaction.util.MongoUtil;
import com.zatgo.zup.transaction.util.eth.EthUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.web3j.protocol.core.methods.response.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.*;

/**
 * Created by 46041 on 2018/10/24.
 */

@Service("ethService")
public class EthService implements CoinService {

    private static final Logger logger = LoggerFactory.getLogger(EthService.class);

    @Autowired
    private EthUtil ethUtil;
    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private EthAndContractConfig ethAndContractConfig;
    @Autowired
    private WalletRemotService walletRemotService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private SignRemotService signRemotService;

    @Value("${transaction.eth.height}")
    private Integer height;
    @Value("${transaction.type}")
    private String type;
    @Value("${transaction.eth.addressNum}")
    private Integer addressNum;

    @Override
    public void sendTransaction(String toAddress, String fromAddress, BigDecimal amount, String businessTxId, String coinType) {
        send(toAddress, fromAddress, amount, businessTxId, coinType, TransactionTypeEnum.EXTRACT);
    }

    @Override
    public void scanBlock() throws IOException {
        //获取数据库中区块高度
        Integer maxValue = mongoUtil.getMaxValue("height", MongoConstant.MAIN_CHAIN_CLASS_NAME) + 1;
        maxValue = ethUtil.getMax(height, maxValue);
        BigInteger blockCount = ethUtil.getBlockCount();
        //如果已经扫描到最高的区块了，休息10秒继续扫描
        if (maxValue - 1 == blockCount.intValue()){
            try {
                logger.info("睡了10秒");
                Thread.sleep(10 * 1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return;
        }
        try {
            EthBlock block = ethUtil.getBlockTx(new BigInteger(maxValue + ""));
            EthBlock.Block blockInfo = block.getBlock();
            if (blockInfo != null){
                BigInteger number = blockInfo.getNumber();
                List<EthBlock.TransactionResult> blockTx = blockInfo.getTransactions();
                //获取其中自己的交易
                List<TransactionRecord> selfTransactionRecord = getSelfTransactionRecord(blockTx, number);
                if (!CollectionUtils.isEmpty(selfTransactionRecord)){
                    for (TransactionRecord record : selfTransactionRecord){
                        saveTransactionRecord(record);
                    }
                }
                MainChain mainChain = new MainChain();
                mainChain.setHeight(maxValue);
                mainChain.setHash(blockInfo.getHash());
                mongoUtil.insert(mainChain, MongoConstant.MAIN_CHAIN_CLASS_NAME);
            } else {
                logger.error(JSONObject.toJSONString(block.getError()));
            }
        } catch (Exception e) {
            logger.error("", e);
        }

    }

    @Override
    public void checkTransaction() throws IOException {
        List<TransactionRecord> unConfirmRecord = getUnConfirmRecord();
        if (!CollectionUtils.isEmpty(unConfirmRecord)){
            BigInteger blockCount = ethUtil.getBlockCount();
            unConfirmRecord.forEach(record -> {
                try {
                    EthTransaction transactionByHash = ethUtil.getTransactionByHash(record.getTxId());
                    if (transactionByHash != null){
                        updateTransaction(transactionByHash, record, blockCount);
                    }
                } catch (Exception e){
                    logger.error("", e);
                }
            });
        }
    }

    @Override
    public void GetBtcExtractRecord() {
        Map<String, ContractConfig> contractConfig = ethAndContractConfig.getContractConfig();
        for (Map.Entry<String, ContractConfig> conf : contractConfig.entrySet()){
            Map<String, String> map = new HashMap();
            ContractConfig value = conf.getValue();
            map.put("symbol", value.getName());
            Map<String, ?> stringMap = walletRemotService.withdrawConsume(map);
            if (stringMap != null){
                String code = (String) stringMap.get("errno");https://insight.bitpay.com
                if (Constants.CODE_SUCCESS.equals(code)){
                    List<Map> withdrawResults = (List<Map>) stringMap.get("data");
                    if (!CollectionUtils.isEmpty(withdrawResults)){
                        for (Map m : withdrawResults){
                            String fromAddress = (String) m.get("from_address");
                            String toAddress = (String) m.get("address_to");
                            BigDecimal amount = new BigDecimal(m.get("amount") + "");
//                            BigDecimal fee = new BigDecimal(m.get("fee") + "");
                            String transId = m.get("trans_id") + "";
                            sendTransaction(toAddress, fromAddress, amount, transId, value.getName());
                        }
                    }
                }
            }
        }

    }

    @Override
    public void collect() {
        Map<String, ContractConfig> coinTypeConfig = ethAndContractConfig.getCoinTypeConfig();
        ContractConfig eth = coinTypeConfig.get("ETH");
        for (Map.Entry<String, ContractConfig> map : coinTypeConfig.entrySet()){
            try {
                ContractConfig value = map.getValue();
                String coinType = value.getName();
                BigDecimal serviceCharge = getServiceCharge(value);
                Query query = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.ETH_ADDRESS_CLASS_CONFIRMED).gte(value.getCollectlimit().doubleValue());
                criteria.and(MongoConstant.ETH_ADDRESSES_TYPE).is(AddressTypeEnum.MEMBER_ADDRESS.getCode());
                query.addCriteria(criteria);
                List<Address> data = mongoUtil.getData(query, Address.class, ethUtil.getTokenAddressName(coinType));
                if (!CollectionUtils.isEmpty(data)){
                    for (Address address : data){
                        //只有当实际金额+未到账金额大于指定金额的时候才会收集
                        BigDecimal relAmount = new BigDecimal(address.getConfirmed() + "");
                        if (relAmount.add(new BigDecimal(address.getUnconfirmed())).compareTo(value.getCollectlimit()) != -1 && relAmount.compareTo(value.getCollectlimit()) != -1){
                            String toAddress = address.getAddress();
                            String fromAddress = value.getSystemServiceChargeAddress();
                            //先看当前地址有没有足够的eth
                            TransactionRecord sendCollectServerCharge = getSendCollectServerCharge(fromAddress, toAddress);;
                            if (hasNotEnoughServerCharge(toAddress, serviceCharge)){
                                //如果没有发送过手续费
                                if (sendCollectServerCharge == null){
                                    send(toAddress, fromAddress, serviceCharge, UUIDUtils.getUuid() + "_1", "ETH", TransactionTypeEnum.COLLECT);
                                    continue;
                                }
                                //没到确认数的话，不算入账
                                if (sendCollectServerCharge.getConfirm() < eth.getConfirmed()){
                                    continue;
                                }
                            }
                            BigDecimal amount = new BigDecimal(address.getConfirmed() + "");
                            String recordId = null;
                            if (sendCollectServerCharge != null){
                                recordId = sendCollectServerCharge.getRecordId();
                            } else {
                                recordId = UUIDUtils.getUuid();
                            }
                            if ("ETH".equalsIgnoreCase(coinType)){
                                amount = amount.subtract(serviceCharge);
                            }
                            String txId = send(value.getSystemAddress(), toAddress, amount, recordId.replace("_1", "_2"), coinType, TransactionTypeEnum.COLLECT);
                            if (!StringUtils.isEmpty(txId)){
                                //将手续费标记为已花费
                                if (sendCollectServerCharge != null)
                                    useServiceCharge(sendCollectServerCharge.getTxId());
                                logger.info("===========从" + toAddress + "搜集了" + amount + "个" + coinType + "========");
                                continue;
                            }
                        }
                    }
                }
            } catch (Exception e){
                logger.error("", e);
            }
        }
    }

    private boolean hasNotEnoughServerCharge(String address, BigDecimal serviceCharge){
        Address eth = getAddress(address, "ETH");
        return new BigDecimal(eth.getConfirmed() + "").subtract(serviceCharge).compareTo(BigDecimal.ZERO) == -1;
    }

    private BigDecimal getServiceCharge(ContractConfig config){
        return ethUtil.ethAmountFormate18(new BigDecimal(config.getGaslimit()).multiply(new BigDecimal(config.getGasprice())));
    }

    @Override
    @Async("scanExtractTxThreadPool")
    public void addAddress() {
        ResponseData<Integer> eth = walletRemotService.remainderAddress("ETH");
        if (!CoinConstant.createAddressLockMap.containsKey(CoinConstant.ETH_ADDRESS_LOCK)){
            CoinConstant.createAddressLockMap.put(CoinConstant.ETH_ADDRESS_LOCK, "");
            try {
                if (eth.getData() < 200){
                    CreateAddressRequest request = new CreateAddressRequest();
                    List<String> data = new ArrayList<>();
                    request.setPassword(CoinConstant.SIGN_PASSWORD);
                    request.setNum(100);
                    while (true){
                        try{
                            ResponseData<List<String>> newAddresses = signRemotService.getNewEthAddresses(request);
                            data.addAll(newAddresses.getData());
                            if (addressNum <= data.size()){
                                break;
                            }
                        } catch (Exception e){
                            logger.error("", e);
                        }
                    }
                    List<Address> addresses = new ArrayList<>(data.size());
                    JSONArray array = new JSONArray();
                    Date date = new Date();
                    for (String a : data){
                        JSONObject object = new JSONObject();
                        object.put("address", a);
                        object.put("networkType", "ETH");
                        object.put("isUsed", 0);
                        object.put("create_time", date);
                        array.add(object);
                        Address address = new Address();
                        address.setCreateDate(date);
                        address.setType(AddressTypeEnum.MEMBER_ADDRESS.getCode());
                        address.setAddress(a);
                        address.setConfirmed(BigDecimal.ZERO.doubleValue());
                        address.setUnconfirmed(BigDecimal.ZERO.doubleValue());
                        addresses.add(address);
                    }
                    ResponseData<String> responseData = walletRemotService.addAddresses(array);
                    if (responseData.isSuccessful()){
                        mongoUtil.insert(addresses, ethUtil.getTokenAddressName("ETH"));
                    }
                }
            } finally {
                CoinConstant.createAddressLockMap.remove(CoinConstant.ETH_ADDRESS_LOCK);
            }
        }
    }


    private void useServiceCharge(String txId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TXID).is(txId);
        query.addCriteria(criteria);
        Update update = new Update();
        update.set(MongoConstant.ETH_TRANSACTION_CLASS_IS_USED, TransactionUseTypeEnum.USED.getCode());
        mongoUtil.update(query, MongoConstant.ETH_TRANSACTION_CLASS, update);
    }


    private boolean canExtract(String address, ContractConfig config){
        List<Integer> list = new ArrayList<>();
        list.add(TransactionTypeEnum.EXTRACT.getCode());
        list.add(TransactionTypeEnum.COLLECT.getCode());
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_FROMADDRESS).is(address);
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TYPE).in(list);
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_CONFIRM).lt(config.getConfirmed());
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data);
    }



    private void saveTransactionRecord(TransactionRecord record) throws ParseException {
        //先看看这笔交易是否已经存在，如果存在，不做任何操作
        try{
            Map<String, Object> query = new HashMap();
            query.put(MongoConstant.ETH_TRANSACTION_CLASS_TXID, record.getTxId());
            List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
            if (CollectionUtils.isEmpty(data)){
                String updateAddressBalanceLock = RedisKeyConstants.UPDATE_ADDRESS_BALANCE_LOCK;
                redisLockUtils.lock(updateAddressBalanceLock);
                try {
                    //地址加钱
                    Address address = createAddress(record.getToAddress(), record.getCoinType());
                    Query updateAddressAmountQuery = new Query();
                    Criteria criteria = new Criteria();
                    criteria.and(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS).is(record.getToAddress());
                    updateAddressAmountQuery.addCriteria(criteria);
                    Update update = new Update();
                    update.set(MongoConstant.ETH_ADDRESS_CLASS_UNCONFIRMED, new BigDecimal(address.getUnconfirmed()+"").add(record.getAmount()).doubleValue());
                    update.set(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS, address.getAddress());
                    update.set(MongoConstant.ETH_ADDRESSES_TYPE, getAddressType(address.getAddress()));
                    mongoUtil.insert(record, MongoConstant.ETH_TRANSACTION_CLASS);
                    mongoUtil.updateInsert(updateAddressAmountQuery, ethUtil.getTokenAddressName(record.getCoinType()), update);
                } finally {
                    redisLockUtils.releaseLock(updateAddressBalanceLock);
                }
            }
        } catch (Exception e){
            logger.error("", e);
        }
    }

    private TransactionRecord getTransactionRecord(JSONObject transaction) throws IOException {
        if (transaction == null)
            return null;
        EthGetTransactionReceipt transactionReceiptSend = null;
        try{
            TransactionRecord record = new TransactionRecord();
            record.setType(TransactionTypeEnum.DEPOSIT.getCode());
            record.setCreateDate(new Date());
            record.setTxId(transaction.getString("hash"));
            record.setConfirm(0);
            record.setFromAddress(transaction.getString("from"));
            record.setToAddress(transaction.getString("to"));
            record.setGasPrice(ethUtil.ethAmountFormate9(transaction.getBigDecimal("gasPrice")));
            record.setGasLimit(transaction.getBigDecimal("gas"));

            String input = transaction.getString("input");
            String value = transaction.getString("value").replaceFirst("0x", "0");
            //如果是代币，value为0大概率是代币，还需进一步判断
            if (!"0x".equals(input)){
                Map<String, ContractConfig> contractConfig = ethAndContractConfig.getContractConfig();
                String contractAddress = record.getToAddress();
                if (!StringUtils.isEmpty(contractAddress) && contractConfig.containsKey(contractAddress)) {
                    ContractConfig config = contractConfig.get(contractAddress);
                    transactionReceiptSend = ethUtil.getTransactionReceipt(record.getTxId());
                    JSONObject transactionReceipt = JSONObject.parseObject(JSONObject.toJSONString(transactionReceiptSend.getTransactionReceipt().get()));
                    JSONArray logs = transactionReceipt.getJSONArray("logs");
                    //没有logs的认为不是erc20规范的，或者就是eth
                    //如果不是erc20规范的，地址不在地址表中，认为是eth也无妨
                    if (!logs.isEmpty()) {
                        JSONObject log = logs.getJSONObject(0);
                        record.setContractAddress(contractAddress);
                        String data = log.getString("data");
                        if ("0x".equals(data)) {
                            record.setAmount(ethUtil.ethAmountFormate18(new BigDecimal(0)));
                        } else {
                            String amount = data.replaceFirst("^0x0+", "");
                            if (StringUtils.isEmpty(amount)) {
                                record.setAmount(ethUtil.ethAmountFormate18(new BigDecimal(0)));
                            } else {
                                try {
                                    record.setAmount(ethUtil.ethAmountFormate18(new BigDecimal(new BigInteger(amount, 16))));
                                } catch (Exception e) {
                                    logger.error("", e);
                                }
                            }
                        }
                        JSONArray topics = log.getJSONArray("topics");
                        record.setTopics(topics);
                        record.setCoinType(config.getName());
                        record.setAmount(ethUtil.ethAmountFormateSumN(record.getAmount(), new BigInteger("18").subtract(config.getPrecision())));
                        if (topics != null) {
                            String toAddress = (String) topics.get(2);
                            record.setToAddress("0x" + toAddress.substring(toAddress.length() - 40, toAddress.length()));
                        }
                        return record;
                    }
                }
            }
            record.setAmount(ethUtil.ethAmountFormate18(new BigDecimal(value)));
            record.setContractAddress(null);
            record.setCoinType("ETH");
            return record;
        } catch (Exception e){
            logger.error("", e);
            logger.error(JSONObject.toJSONString(transactionReceiptSend.getError()));
            logger.error(JSONObject.toJSONString(transaction));
        }
        return null;
    }

    private List<TransactionRecord> getSelfTransactionRecord(List<EthBlock.TransactionResult> blockTx, BigInteger number) throws IOException {
        //所有交易
        Map<String, List<TransactionRecord>> map = new HashMap<>();
        //to地址
        Set<String> addresses = new HashSet<>();
        for (EthBlock.TransactionResult transaction : blockTx){
            try {
                JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(transaction.get()));
                TransactionRecord transactionRecord = getTransactionRecord(object);
                if (transactionRecord != null){
                    transactionRecord.setHeight(number);
                    //没有币种的认为不是我们支持的币种
                    //把支持币种的到账地址拿出来查询有没有我们的地址
                    if (!StringUtils.isEmpty(transactionRecord.getCoinType())){
                        String toAddress = transactionRecord.getToAddress();
                        if (map.containsKey(toAddress)){
                            List<TransactionRecord> records = map.get(toAddress);
                            records.add(transactionRecord);
                        } else {
                            ArrayList<TransactionRecord> records = new ArrayList<>();
                            records.add(transactionRecord);
                            map.put(toAddress, records);
                        }
                        addresses.add(toAddress);
                    }
                }
            } catch (Exception e){
                logger.error("", e);
            }
        }
        //在数据库中获取自己的地址
        Map<String, Collection<String>> query = new HashMap<>();
        query.put(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS, addresses);
        List<Address> selfAddress = mongoUtil.getDataByListQuery(query, Address.class, ethUtil.getTokenAddressName("ETH"));
        List<TransactionRecord> selfTransactionRecord = new ArrayList<>();
        if (!CollectionUtils.isEmpty(selfAddress)){
            for (Address address : selfAddress){
                selfTransactionRecord.addAll(map.get(address.getAddress()));
            }
        }
        return selfTransactionRecord;
    }

    private void updateAddressBalance(String address, BigDecimal amount, String coinType, TransactionTypeEnum typeEnum, String toAddress, String fromAddress) {
        String updateAddressBalanceLock = RedisKeyConstants.UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try {
            Address oldAddress = getAddress(address, coinType);
            BigDecimal unconfirmed = new BigDecimal(oldAddress.getUnconfirmed() + "");
            if (unconfirmed == null){
                unconfirmed = BigDecimal.ZERO;
            }
            amount = unconfirmed.add(amount.negate());
            //所有的提币，都要从from的ETH地址中扣除手续费
            if ("ETH".equalsIgnoreCase(coinType)){
                ContractConfig config = ethAndContractConfig.getCoinTypeConfig().get("ETH");
                updateAddressBalance(fromAddress, "ETH", getServiceCharge(config));
            } else {
                ContractConfig config = ethAndContractConfig.getCoinTypeConfig().get(coinType);
                updateAddressBalance(fromAddress, "ETH", getServiceCharge(config));
            }

            Query query = new Query();
            Criteria criteria = new Criteria();
            criteria.and(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS).is(address);
            query.addCriteria(criteria);
            Update update = new Update();
            update.set(MongoConstant.ETH_ADDRESS_CLASS_UNCONFIRMED, amount.doubleValue());
            mongoUtil.update(query, ethUtil.getTokenAddressName(coinType), update);
        } finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }

    private void updateAddressBalance(String address, String coinType, BigDecimal amount){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS).is(address);
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, ethUtil.getTokenAddressName(coinType));
        if (!CollectionUtils.isEmpty(data)){
            Address addre = data.get(0);
            Update update = new Update();
            update.set(MongoConstant.ETH_ADDRESS_CLASS_CONFIRMED, new BigDecimal(addre.getConfirmed() + "").subtract(amount).doubleValue());
            mongoUtil.update(query, ethUtil.getTokenAddressName(coinType), update);
        }
    }


    private List<TransactionRecord> getUnConfirmRecord(){
        Map<String, ContractConfig> contractConfig = ethAndContractConfig.getContractConfig();
        Query query = new Query();
        Criteria criteria = new Criteria();
        List<Criteria> list = new ArrayList();
        for (Map.Entry<String, ContractConfig> map : contractConfig.entrySet()){
            ContractConfig value = map.getValue();
            Criteria cri = Criteria.where(MongoConstant.ETH_TRANSACTION_CLASS_COINTYPE).is(value.getName())
                    .and(MongoConstant.ETH_TRANSACTION_CLASS_CONFIRM).lt(value.getConfirmed());
            list.add(cri);
        }
        Criteria[] criterias = new Criteria[list.size()];
        list.toArray(criterias);
        criteria.orOperator(criterias);
        query.addCriteria(criteria);
        return mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
    }



    private void updateTransaction(EthTransaction tx, TransactionRecord record, BigInteger nowHeight) {
        Optional<Transaction> optional = tx.getTransaction();
        if (optional == null){
            String uuid = UUIDUtils.getUuid();
            logger.error(uuid + "：=======检测未到达确认数的交易时，未从链上查到该交易");
            logger.error(uuid + "：=======" + JSONObject.toJSONString(record));
            return;
        }
        Map<String, ContractConfig> coinTypeConfig = ethAndContractConfig.getCoinTypeConfig();
        ContractConfig config = coinTypeConfig.get(record.getCoinType());
        Transaction transaction = null;
        try {
            transaction = optional.get();
        } catch (Exception e){
            logger.error("交易异常" + JSONObject.toJSONString(tx.getError()));
            logger.error(JSONObject.toJSONString(record));
            logger.error(JSONObject.toJSONString(tx.getError()));
            return;
        }
        Integer confirmed = config.getConfirmed();
        BigInteger height = record.getHeight();
        BigInteger blockNumber = null;
        try {
            blockNumber = transaction.getBlockNumber();
        } catch (Exception e){
            logger.error("正在广播中");
            logger.error("=========" + JSONObject.toJSONString(record));
            return;
        }
        //更新交易中的块号
        height = updateTransactionRecordHeight(height, blockNumber, record.getTxId());
        record.setHeight(blockNumber);
        BigInteger nowConfirmed = nowHeight.subtract(height).add(BigInteger.ONE);
        record.setConfirm(nowConfirmed.intValue());
        if (nowConfirmed.intValue() >= confirmed){
            //通知钱包
            sendMessageToWallet(record);
            //更新地址余额
            updateAddressAmountInfo(record);
        }
        //更新确认数
        updateTranscationInfo(record);
    }

    private BigInteger updateTransactionRecordHeight(BigInteger beforeHeight, BigInteger blockNumber, String txId){
        if (beforeHeight == null && blockNumber != null && blockNumber.compareTo(BigInteger.TEN) == 1){
            Query query = new Query();
            Criteria criteria = new Criteria();
            criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TXID).is(txId);
            query.addCriteria(criteria);
            Update update = new Update();
            update.set(MongoConstant.ETH_TRANSACTION_CLASS_HEIGHT, blockNumber);
            mongoUtil.update(query, MongoConstant.ETH_TRANSACTION_CLASS, update);
        }
        return blockNumber;
    }

    private void sendMessageToWallet(TransactionRecord record){
        //通知钱包
        Map<String, ?> res = null;
        Integer type = record.getType();
        Map<String, String> map = new HashMap();
        Map<String, ContractConfig> coinTypeConfig = ethAndContractConfig.getCoinTypeConfig();
        ContractConfig config = coinTypeConfig.get(record.getCoinType().toUpperCase());
        BigDecimal serviceCharge = new BigDecimal(config.getGaslimit()).multiply(new BigDecimal(config.getGasprice()).divide(new BigDecimal(10).pow(9)));
        if (TransactionTypeEnum.DEPOSIT.getCode().equals(type)){
            map.put("timestamp", record.getCreateDate().getTime() + "");
            map.put("symbol", record.getCoinType());
            map.put("txid", record.getTxId());
            map.put("amount", record.getAmount().toPlainString());
            map.put("address_to", record.getToAddress());
            map.put("confirm", record.getConfirm() + "");
            map.put("is_mining", "1");
            res = walletRemotService.depositNotify(map);
            logger.info("钱包返回信息=====" + JSONObject.toJSONString(res));
            if (res == null || !Constants.CODE_SUCCESS.equals(res.get("errno"))){
                throw new BusinessException(BusinessExceptionCode.NOTIFY_WALLET_FAIL);
            }
        } else if (TransactionTypeEnum.EXTRACT.getCode().equals(type)){
            map.put("trans_id", record.getRecordId());
            map.put("symbol", record.getCoinType());
            map.put("address_to", record.getToAddress());
            map.put("txid", record.getTxId());
            map.put("amount", record.getAmount().toPlainString());
            map.put("confirm", record.getConfirm() + "");
            map.put("real_fee", serviceCharge.toPlainString());
            res = walletRemotService.withdrawNotify(map);
            logger.info("钱包返回信息=====" + JSONObject.toJSONString(res));
            if (res == null || !Constants.CODE_SUCCESS.equals(res.get("errno"))){
                throw new BusinessException(BusinessExceptionCode.NOTIFY_WALLET_FAIL);
            }
        }
    }

    private void updateTranscationInfo(TransactionRecord record){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TXID).is(record.getTxId());
        query.addCriteria(criteria);
        Update update = new Update();
        update.set(MongoConstant.ETH_TRANSACTION_CLASS_HEIGHT, record.getHeight());
        update.set(MongoConstant.ETH_TRANSACTION_CLASS_CONFIRM, record.getConfirm());
        mongoUtil.update(query, MongoConstant.ETH_TRANSACTION_CLASS, update);
    }

    private void updateAddressAmountInfo(TransactionRecord record) {
        String updateAddressBalanceLock = RedisKeyConstants.UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try {
            Address fromAddress = getAddress(record.getFromAddress(), record.getCoinType());
            Address toAddress = getAddress(record.getToAddress(), record.getCoinType());
            BigDecimal amount = record.getAmount();
            Integer type = record.getType();
            Query query = new Query();
            Criteria criteria = new Criteria();
            Update update = new Update();
            //零钱收集
            if (TransactionTypeEnum.COLLECT.getCode().equals(type)) {
                if (toAddress == null) {
                    toAddress = createAddress(record.getToAddress(), record.getCoinType());
                }
                criteria.and(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS).is(toAddress.getAddress());
//            update.inc(MongoConstant.ETH_ADDRESS_CLASS_UNCONFIRMED, df.parse(record.getAmount().negate().toString()));
                update.set(MongoConstant.ETH_ADDRESS_CLASS_CONFIRMED, new BigDecimal(toAddress.getConfirmed() + "").add(amount).doubleValue());
                query.addCriteria(criteria);
                mongoUtil.updateInsert(query, ethUtil.getTokenAddressName(record.getCoinType()), update);

                query = new Query();
                criteria = new Criteria();
                update = new Update();
                criteria.and(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS).is(record.getFromAddress());
                update.set(MongoConstant.ETH_ADDRESS_CLASS_UNCONFIRMED, new BigDecimal(fromAddress.getUnconfirmed() + "").add(amount).doubleValue());
                update.set(MongoConstant.ETH_ADDRESS_CLASS_CONFIRMED, new BigDecimal(fromAddress.getConfirmed() + "").add(amount.negate()).doubleValue());

                //删除搜集记录
//                clearCollectRecord(record.getFromAddress(), record.getCoinType());
            }
            //充值
            if (TransactionTypeEnum.DEPOSIT.getCode().equals(type)) {
                if (toAddress == null) {
                    toAddress = createAddress(record.getToAddress(), record.getCoinType());
                }
                criteria.and(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS).is(toAddress.getAddress());
                update.set(MongoConstant.ETH_ADDRESS_CLASS_UNCONFIRMED, new BigDecimal(toAddress.getUnconfirmed() + "").add(amount.negate()).doubleValue());
                update.set(MongoConstant.ETH_ADDRESS_CLASS_CONFIRMED, new BigDecimal(toAddress.getConfirmed() + "").add(amount).doubleValue());
            }
            //提币
            if (TransactionTypeEnum.EXTRACT.getCode().equals(type)) {
                criteria.and(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS).is(fromAddress.getAddress());
                update.set(MongoConstant.ETH_ADDRESS_CLASS_UNCONFIRMED, new BigDecimal(fromAddress.getUnconfirmed() + "").add(amount).doubleValue());
                update.set(MongoConstant.ETH_ADDRESS_CLASS_CONFIRMED, new BigDecimal(fromAddress.getConfirmed() + "").add(amount.negate()).doubleValue());
            }

            query.addCriteria(criteria);
            mongoUtil.updateInsert(query, ethUtil.getTokenAddressName(record.getCoinType()), update);
        }finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }

    private Address createAddress(String toAddress, String coinType){
        String extractLock = RedisKeyConstants.TRANSACTION_ADD_ADDRESS_LOCK + coinType;
        redisLockUtils.lock(extractLock);
        try {
            Address address = getAddress(toAddress, coinType);
            if (address != null)
                return address;
            address = new Address();
            address.setType(AddressTypeEnum.MEMBER_ADDRESS.getCode());
            address.setAddress(toAddress);
            address.setCreateDate(new Date());
            address.setConfirmed(BigDecimal.ZERO.doubleValue());
            address.setUnconfirmed(BigDecimal.ZERO.doubleValue());
            mongoUtil.insert(address, ethUtil.getTokenAddressName(coinType));
            return address;
        } finally {
            redisLockUtils.releaseLock(extractLock);
        }

    }

    private TransactionRecord hasBusinessTxId(String businessTxId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_RECORD_ID).is(businessTxId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }

    private TransactionRecord getSendCollectServerCharge(String systemCollectServerChargeAddress, String toAddress){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TOADDRESS).is(toAddress);
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_FROMADDRESS).is(systemCollectServerChargeAddress);
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_IS_USED).ne(TransactionUseTypeEnum.USED.getCode());
        query.addCriteria(criteria);
        query.limit(1);
        query.with(new Sort(new Sort.Order(Sort.Direction.DESC,"createDate")));
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }

    private String send(String toAddress, String fromAddress, BigDecimal amount, String businessTxId, String coinType, TransactionTypeEnum typeEnum){
        if (amount.compareTo(BigDecimal.ZERO) == -1){
            throw new BusinessException(BusinessExceptionCode.EXTRACT_MONEY_ERROR);
        }
        EthSendTransaction tx = null;
        TransactionRecord record = new TransactionRecord();
        String extractLock = RedisKeyConstants.TRANSACTION_EXTRACT_LOCK + fromAddress;
        redisLockUtils.lock(extractLock);
        try {
            if (StringUtils.isEmpty(businessTxId)){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BUSINESS_TXID_NOT_IS_EMPTY);
            }
            if (hasBusinessTxId(businessTxId) == null) {
                Address address = getAddress(fromAddress, coinType);
                if (address == null){
                    logger.error("转账失败的业务Id：" + businessTxId);
                    throw new BusinessException(BusinessExceptionCode.EXTRACT_FROM_ADDRESS_NOT_EXIST);
                }
                Map<String, ContractConfig> coinTypeConfig = ethAndContractConfig.getCoinTypeConfig();
                ContractConfig config = coinTypeConfig.get(coinType);
                //判断地址是否被使用了，如果被使用需要排队
                if (canExtract(address.getAddress(), config)) {
                    BigDecimal serviceCharge = getServiceCharge(config);
                    BigDecimal confirmed = new BigDecimal(address.getConfirmed() + "");
                    if (confirmed == null) {
                        confirmed = BigDecimal.ZERO;
                    }
                    BigDecimal balance = confirmed.subtract(amount);
                    if (balance.compareTo(BigDecimal.ZERO) == -1) {
                        logger.error("转账失败的业务Id：" + businessTxId);
                        throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
                    }
                    if ("ETH".equals(coinType)) {
                        if (balance.subtract(serviceCharge).compareTo(BigDecimal.ZERO) == -1) {
                            logger.error("转账失败的业务Id：" + businessTxId);
                            throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
                        }
                    } else {
                        Address eth = getAddress(fromAddress, "ETH");
                        BigDecimal money = new BigDecimal(eth.getConfirmed() + "");
                        if (money == null || money.subtract(serviceCharge).compareTo(BigDecimal.ZERO) == -1) {
                            logger.error("转账失败的业务Id：" + businessTxId);
                            throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
                        }
                    }
                    BigInteger nonce = ethUtil.getNonce(fromAddress);
                    if (nonce == null) {
                        logger.error("转账失败的业务Id：" + businessTxId);
                        throw new BusinessException(BusinessExceptionCode.BUSINESS_GET_NONCE_FAIL);
                    }
                    //签名
                    ResponseData<String> sign = null;
                    if ("ETH".equalsIgnoreCase(coinType)) {
                        EthSignParams params = new EthSignParams();
                        params.setAmount(amount);
                        params.setToAddress(toAddress);
                        params.setFromAddress(fromAddress);
                        params.setmNonce(nonce);
                        params.setPassword(CoinConstant.SIGN_PASSWORD);
                        params.setGasLimit(CoinConstant.ETH_GAS_LIMIT);
                        params.setGasPrice(config.getGasprice());
                        sign = signRemotService.ethSign(params);
                        record.setGasLimit(new BigDecimal(config.getGaslimit()));
                        record.setGasPrice(ethUtil.ethAmountFormate9(new BigDecimal(config.getGasprice())));
                    } else {
                        EthContractSignParams params = new EthContractSignParams();
                        params.setGasLimit(config.getGaslimit());
                        params.setGasPrice(config.getGasprice());
                        params.setPassword(CoinConstant.SIGN_PASSWORD);
                        params.setmNonce(nonce);
                        params.setAmount(amount);
                        params.setContractAddress(config.getContract());
                        params.setFromAddress(fromAddress);
                        params.setToAddress(toAddress);
                        params.setDecimal(config.getPrecision());
                        sign = signRemotService.ethSign(params);
                        record.setContractAddress(config.getContract());
                        record.setGasLimit(new BigDecimal(config.getGaslimit()));
                        record.setGasPrice(new BigDecimal(config.getGasprice()));
                    }
                    if (!sign.isSuccessful()) {
                        throw new BusinessException(BusinessExceptionCode.SIGN_ERROR);
                    }
                    String signData = sign.getData();
                    record.setToAddress(toAddress);
                    record.setFromAddress(fromAddress);
                    record.setAmount(amount);
                    record.setConfirm(0);
                    record.setCoinType(coinType);
                    record.setType(typeEnum.getCode());
                    record.setRecordId(businessTxId);
                    record.setCreateDate(new Date());
                    tx = ethUtil.sendTransaction(signData);
                    String txId = tx.getTransactionHash();
                    if (txId == null) {
                        throw new BusinessException(BusinessExceptionCode.ORDER_SEND_FAIL);
                    }
                    record.setTxId(txId);
                    mongoUtil.insert(record, MongoConstant.ETH_TRANSACTION_CLASS);
                    updateAddressBalance(fromAddress, amount, coinType, typeEnum, toAddress, fromAddress);
                }
            }
        } catch (Exception e) {
            if (tx != null){
                logger.error(JSONObject.toJSONString(tx.getError()));
                logger.error(JSONObject.toJSONString(tx));
            }
            logger.error("", e);
            logger.error(JSONObject.toJSONString(record));
            return null;
        } finally {
            redisLockUtils.releaseLock(extractLock);
        }
        return tx == null ? null : tx.getTransactionHash();
    }

    public void repairData(String txId) throws BusinessException {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TXID).is(txId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
        if (CollectionUtils.isEmpty(data)){
            throw new BusinessException(BusinessExceptionCode.RECORD_NOT_EXIST);
        }
        TransactionRecord record = data.get(0);
        if (record.getConfirm() > 0){
            throw new BusinessException(BusinessExceptionCode.ORDER_NOT_MEET_CONDITION);
        }
        updateAddressBalance(record.getFromAddress(), record.getAmount().negate(), record.getCoinType(), TransactionTypeEnum.EXTRACT, record.getToAddress(), record.getFromAddress());
        mongoUtil.delte(query, MongoConstant.ETH_TRANSACTION_CLASS);
    }

    private Integer getAddressType(String address){
        Map<String, ContractConfig> coinTypeConfig = ethAndContractConfig.getCoinTypeConfig();
        ContractConfig config = coinTypeConfig.get("ETH");
        if (config.getSystemAddress().equals(address)){
            return AddressTypeEnum.SYSTEM_ADDRESS.getCode();
        }
        if (config.getSystemServiceChargeAddress().equals(address)){
            return AddressTypeEnum.SYSTEM_COLLECT.getCode();
        }
        return AddressTypeEnum.MEMBER_ADDRESS.getCode();
    }

    private Address getAddress(String address, String coinType){
        Map<String, Object> query = new HashMap();
        query.put(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS, address);
        List<Address> data = mongoUtil.getData(query, Address.class, ethUtil.getTokenAddressName(coinType));
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }
}
