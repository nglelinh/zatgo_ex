/*
 * BitcoindRpcClient-JSON-RPC-Client License
 * 
 * Copyright (c) 2013, Mikhail Yevchenko.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 
 * Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 /*
 * Repackaged with simple additions for easier maven usage by Alessandro Polverini
 */
package com.zatgo.zup.transaction.util.btc;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.transaction.exception.BitcoinRPCError;
import com.zatgo.zup.transaction.exception.BitcoinRPCException;
import com.zatgo.zup.transaction.exception.GenericRpcException;
import com.zatgo.zup.transaction.json.Base64Coder;
import com.zatgo.zup.transaction.json.JSON;
import com.zatgo.zup.transaction.service.impl.BtcService;
import com.zatgo.zup.transaction.util.BigDecimalUtil;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.math.BigDecimal;
import java.net.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.zatgo.zup.transaction.util.btc.MapWrapper.mapInt;
import static com.zatgo.zup.transaction.util.btc.MapWrapper.mapStr;


/**
 *
 * @author Mikhail Yevchenko m.ṥῥẚɱ.ѓѐḿởύḙ at azazar.com Small modifications by
 * Alessandro Polverini polverini at gmail.com
 */
public class BitcoinJSONRPCClient implements BitcoindRpcClient {

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BitcoinJSONRPCClient.class);

  public URL rpcURL;

  private URL noAuthURL;
  private String authStr;

  public BitcoinJSONRPCClient(String rpcUrl) throws MalformedURLException {
    this(new URL(rpcUrl));
  }

  public BitcoinJSONRPCClient(URL rpc) {
    this.rpcURL = rpc;
    try {
      noAuthURL = new URI(rpc.getProtocol(), null, rpc.getHost(), rpc.getPort(), rpc.getPath(), rpc.getQuery(), null).toURL();
    } catch (MalformedURLException | URISyntaxException ex) {
      throw new IllegalArgumentException(rpc.toString(), ex);
    }
    authStr = rpc.getUserInfo() == null ? null : String.valueOf(Base64Coder.encode(rpc.getUserInfo().getBytes(Charset.forName("ISO8859-1"))));
  }

//  static {
//    String user = "111";
//    String password = "222";
//    String host = "47.74.145.17";
//    String port = "8332";
//
//    try {
//      File f;
//      File home = new File(System.getProperty("user.home"));
//
//      if ((f = new File(home, ".bitcoin" + File.separatorChar + "bitcoin.conf")).exists()) {
//      } else if ((f = new File(home, "AppData" + File.separatorChar + "Roaming" + File.separatorChar + "Bitcoin" + File.separatorChar + "bitcoin.conf")).exists()) {
//      } else {
//        f = null;
//      }
//
//      if (f != null) {
//        logger.fine("Bitcoin configuration file found");
//
//        Properties p = new Properties();
//        try (FileInputStream i = new FileInputStream(f)) {
//          p.load(i);
//        }
//
//        user = p.getProperty("rpcuser", user);
//        password = p.getProperty("rpcpassword", password);
//        host = p.getProperty("rpcconnect", host);
//        port = p.getProperty("rpcport", port);
//      }
//    } catch (Exception ex) {
//      logger.log(Level.SEVERE, null, ex);
//    }
//
//    try {
//      DEFAULT_JSONRPC_URL =
//      DEFAULT_JSONRPC_TESTNET_URL = new URL("http://" + user + ':' + password + "@" + host + ":" + (port == null ? "18332" : port) + "/");
//      DEFAULT_JSONRPC_REGTEST_URL = new URL("http://" + user + ':' + password + "@" + host + ":" + (port == null ? "18443" : port) + "/");
//    } catch (MalformedURLException ex) {
//      throw new RuntimeException(ex);
//    }
//  }


  private HostnameVerifier hostnameVerifier = null;
  private SSLSocketFactory sslSocketFactory = null;

  public HostnameVerifier getHostnameVerifier() {
    return hostnameVerifier;
  }

  public void setHostnameVerifier(HostnameVerifier hostnameVerifier) {
    this.hostnameVerifier = hostnameVerifier;
  }

  public SSLSocketFactory getSslSocketFactory() {
    return sslSocketFactory;
  }

  public void setSslSocketFactory(SSLSocketFactory sslSocketFactory) {
    this.sslSocketFactory = sslSocketFactory;
  }

  public static final Charset QUERY_CHARSET = Charset.forName("ISO8859-1");

  public byte[] prepareRequest(final String method, final Object... params) {
    return JSON.stringify(new LinkedHashMap() {
      {
        put("method", method);
        put("params", params);
        put("id", "1");
      }
    }).getBytes(QUERY_CHARSET);
  }

  private static byte[] loadStream(InputStream in, boolean close) throws IOException {
    ByteArrayOutputStream o = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    for (;;) {
      int nr = in.read(buffer);

      if (nr == -1)
        break;
      if (nr == 0)
        throw new IOException("Read timed out");

      o.write(buffer, 0, nr);
    }
    return o.toByteArray();
  }

  @SuppressWarnings("rawtypes")
  public Object loadResponse(InputStream in, Object expectedID, boolean close, Object o) throws IOException, GenericRpcException {
    try {
      String r = new String(loadStream(in, close), QUERY_CHARSET);
      try {
        JSONObject response = JSONObject.parseObject(r);
        if (response.get("error") != null)
          throw new BitcoinRPCException(new BitcoinRPCError((Map)response.get("error")));

        if (response.get("result") == null){
            return response;
        }

        return response.get("result");
      } catch (Exception ex) {
        logger.error("", ex);
        throw new BitcoinRPCException("Invalid server response format (data: \"" + r + "\")");
      }
    } finally {
      if (close)
        in.close();
    }
  }

  public Object query(String method, Object... o) throws GenericRpcException {
    HttpURLConnection conn = null;
    try {
      conn = (HttpURLConnection) noAuthURL.openConnection();

      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setRequestMethod("POST");

      if (conn instanceof HttpsURLConnection) {
        if (hostnameVerifier != null)
          ((HttpsURLConnection) conn).setHostnameVerifier(hostnameVerifier);
        if (sslSocketFactory != null)
          ((HttpsURLConnection) conn).setSSLSocketFactory(sslSocketFactory);
      }
      conn.setConnectTimeout(30 * 1000 * 6);
      conn.setReadTimeout(30 * 1000 * 6);
      ((HttpURLConnection) conn).setRequestProperty("Authorization", "Basic " + authStr);
      byte[] result = prepareRequest(method, o);
      conn.connect();
      conn.getOutputStream().write(result);
      conn.getOutputStream().close();
      Integer responseCode = conn.getResponseCode();
      if (responseCode != 200) {
        InputStream errorStream = conn.getErrorStream();
        throw new BitcoinRPCException(method,
                Arrays.deepToString(o),
                responseCode,
                conn.getResponseMessage(),
                errorStream == null ? null : new String(loadStream(errorStream, true)));
      }
      return loadResponse(conn.getInputStream(), "1", true, o);
    } catch (IOException ex) {
      throw new BitcoinRPCException(method, Arrays.deepToString(o), ex);
    }finally {
      if (conn != null){
        conn.disconnect();
      }
    }
  }

  @Override
  public String createRawTransaction(List<TxInput> inputs, List<TxOutput> outputs) throws GenericRpcException {
    List<Map> pInputs = new ArrayList<>();

    for (final TxInput txInput : inputs) {
      pInputs.add(new LinkedHashMap() {
        {
          put("txid", txInput.txid());
          put("vout", txInput.vout());
        }
      });
    }

    Map<String, BigDecimal> pOutputs = new LinkedHashMap();

    BigDecimal oldValue;
    for (TxOutput txOutput : outputs) {
      BigDecimal amount = txOutput.amount();
      if ((oldValue = pOutputs.put(txOutput.address(), amount)) != null)
        pOutputs.put(txOutput.address(), oldValue.add(amount));
//                throw new BitcoinRpcException("Duplicate output");
    }

    return (String) query("createrawtransaction", pInputs, pOutputs);
  }

  @Override
  public String dumpPrivKey(String address) throws GenericRpcException {
    return (String) query("dumpprivkey", address);
  }

  @Override
  public String getAccount(String address) throws GenericRpcException {
    return (String) query("getaccount", address);
  }

  @Override
  public String getAccountAddress(String account) throws GenericRpcException {
    return (String) query("getaccountaddress", account);
  }

  @Override
  public List<String> getAddressesByAccount(String account) throws GenericRpcException {
    return (List<String>) query("getaddressesbyaccount", account);
  }

  @Override
  public Double getBalance() throws GenericRpcException {
    return ((Number) query("getbalance")).doubleValue();
  }

  @Override
  public Double getBalance(String account) throws GenericRpcException {
    return ((Number) query("getbalance", account)).doubleValue();
  }

  @Override
  public Double getBalance(String account, Integer minConf) throws GenericRpcException {
    return ((Number) query("getbalance", account, minConf)).doubleValue();
  }

  @Override
  public SmartFeeResult getEstimateSmartFee(Integer blocks) {
    return new SmartFeeResultMapWrapper((Map) query("estimatesmartfee", blocks));
  }

  private class InfoWrapper extends MapWrapper implements Info, Serializable {

    public InfoWrapper(Map m) {
      super(m);
    }

    @Override
    public Double balance() {
      return mapDouble("balance");
    }

    @Override
    public Integer blocks() {
      return mapInt("blocks");
    }

    @Override
    public Integer connections() {
      return mapInt("connections");
    }

    @Override
    public Double difficulty() {
      return mapDouble("difficulty");
    }

    @Override
    public String errors() {
      return mapStr("errors");
    }

    @Override
    public Long keyPoolOldest() {
      return mapLong("keypoololdest");
    }

    @Override
    public Long keyPoolSize() {
      return mapLong("keypoolsize");
    }

    @Override
    public Double payTxFee() {
      return mapDouble("paytxfee");
    }

    @Override
    public Long protocolVersion() {
      return mapLong("protocolversion");
    }

    @Override
    public String proxy() {
      return mapStr("proxy");
    }

    @Override
    public Double relayFee() {
      return mapDouble("relayfee");
    }

    @Override
    public Boolean testnet() {
      return mapBool("testnet");
    }

    @Override
    public Integer timeOffset() {
      return mapInt("timeoffset");
    }

    @Override
    public Long version() {
      return mapLong("version");
    }

    @Override
    public Long walletVersion() {
      return mapLong("walletversion");
    }

  }

  private class TxOutSetInfoWrapper extends MapWrapper implements TxOutSetInfo, Serializable {

    public TxOutSetInfoWrapper(Map m) {
      super(m);
    }

    @Override
    public Long height() {
      return mapLong("height");
    }

    @Override
    public String bestBlock() {
      return mapStr("bestBlock");
    }

    @Override
    public Long transactions() {
      return mapLong("transactions");
    }

    @Override
    public Long txouts() {
      return mapLong("txouts");
    }

    @Override
    public Long bytesSerialized() {
      return mapLong("bytes_serialized");
    }

    @Override
    public String hashSerialized() {
      return mapStr("hash_serialized");
    }

    @Override
    public BigDecimal totalAmount() {
      return mapBigDecimal("total_amount");
    }
  }

  private class WalletInfoWrapper extends MapWrapper implements WalletInfo, Serializable {

    public WalletInfoWrapper(Map m) {
      super(m);
    }

    @Override
    public Long walletVersion() {
      return mapLong("walletversion");
    }

    @Override
    public BigDecimal balance() {
      return mapBigDecimal("balance");
    }

    @Override
    public BigDecimal unconfirmedBalance() {
      return mapBigDecimal("unconfirmed_balance");
    }

    @Override
    public BigDecimal immatureBalance() {
      return mapBigDecimal("immature_balance");
    }

    @Override
    public Long txCount() {
      return mapLong("txcount");
    }

    @Override
    public Long keyPoolOldest() {
      return mapLong("keypoololdest");
    }

    @Override
    public Long keyPoolSize() {
      return mapLong("keypoolsize");
    }

    @Override
    public Long unlockedUntil() {
      return mapLong("unlocked_until");
    }

    @Override
    public BigDecimal payTxFee() {
      return mapBigDecimal("paytxfee");
    }

    @Override
    public String hdMasterKeyId() {
      return mapStr("hdmasterkeyid");
    }
  }

  private class NetworkInfoWrapper extends MapWrapper implements NetworkInfo, Serializable {

    public NetworkInfoWrapper(Map m) {
      super(m);
    }

    @Override
    public Long version() {
      return mapLong("version");
    }

    @Override
    public String subversion() {
      return mapStr("subversion");
    }

    @Override
    public Long protocolVersion() {
      return mapLong("protocolversion");
    }

    @Override
    public String localServices() {
      return mapStr("localservices");
    }

    @Override
    public Boolean localRelay() {
      return mapBool("localrelay");
    }

    @Override
    public Long timeOffset() {
      return mapLong("timeoffset");
    }

    @Override
    public Long connections() {
      return mapLong("connections");
    }

    @Override
    public List<Network> networks() {
      List<Map> maps = (List<Map>) m.get("networks");
      List<Network> networks = new LinkedList<Network>();
      for (Map m : maps) {
        Network net = new NetworkWrapper(m);
        networks.add(net);
      }
      return networks;
    }

    @Override
    public BigDecimal relayFee() {
      return mapBigDecimal("relayfee");
    }

    @Override
    public List<String> localAddresses() {
      return (List<String>) m.get("localaddresses");
    }

    @Override
    public String warnings() {
      return mapStr("warnings");
    }
  }

  private class NetworkWrapper extends MapWrapper implements Network, Serializable {

    public NetworkWrapper(Map m) {
      super(m);
    }

    @Override
    public String name() {
      return mapStr("name");
    }

    @Override
    public Boolean limited() {
      return mapBool("limited");
    }

    @Override
    public Boolean reachable() {
      return mapBool("reachable");
    }

    @Override
    public String proxy() {
      return mapStr("proxy");
    }

    @Override
    public Boolean proxyRandomizeCredentials() {
      return mapBool("proxy_randomize_credentials");
    }
  }

  private class MultiSigWrapper extends MapWrapper implements MultiSig, Serializable {

    public MultiSigWrapper(Map m) {
      super(m);
    }

    @Override
    public String address() {
      return mapStr("address");
    }

    @Override
    public String redeemScript() {
      return mapStr("redeemScript");
    }
  }

  private class NodeInfoWrapper extends MapWrapper implements NodeInfo, Serializable {

    public NodeInfoWrapper(Map m) {
      super(m);
    }

    @Override
    public String addedNode() {
      return mapStr("addednode");
    }

    @Override
    public Boolean connected() {
      return mapBool("connected");
    }

    @Override
    public List<Address> addresses() {
      List<Map> maps = (List<Map>) m.get("addresses");
      List<Address> addresses = new LinkedList<Address>();
      for (Map m : maps) {
        Address add = new AddressWrapper(m);
        addresses.add(add);
      }
      return addresses;
    }
  }

  private class AddressWrapper extends MapWrapper implements Address, Serializable {

    public AddressWrapper(Map m) {
      super(m);
    }

    @Override
    public String address() {
      return mapStr("address");
    }

    @Override
    public String connected() {
      return mapStr("connected");
    }
  }

  @SuppressWarnings("serial")
  private class TransactionWrapper extends MapWrapper implements Transaction, Serializable {

    @SuppressWarnings("rawtypes")
    public TransactionWrapper(Map m) {
      super(m);
    }

    @Override
    public String account() {
      return mapStr(m, "account");
    }

    @Override
    public String address() {
      return mapStr(m, "address");
    }

    @Override
    public String category() {
      return mapStr(m, "category");
    }

    @Override
    public Double amount() {
      return mapDouble(m, "amount");
    }

    @Override
    public Double fee() {
      return mapDouble(m, "fee");
    }

    @Override
    public Integer confirmations() {
      return mapInt(m, "confirmations");
    }

    @Override
    public String blockHash() {
      return mapStr(m, "blockhash");
    }

    @Override
    public Integer blockIndex() {
      return mapInt(m, "blockindex");
    }

    @Override
    public Date blockTime() {
      return mapCTime(m, "blocktime");
    }

    @Override
    public String txId() {
      return mapStr(m, "txid");
    }

    @Override
    public Date time() {
      return mapCTime(m, "time");
    }

    @Override
    public Date timeReceived() {
      return mapCTime(m, "timereceived");
    }

    @Override
    public String comment() {
      return mapStr(m, "comment");
    }

    @Override
    public String commentTo() {
      return mapStr(m, "to");
    }

    private RawTransaction raw = null;

    @Override
    public RawTransaction raw() {
      if (raw == null)
        try {
          raw = getRawTransaction(txId());
        } catch (GenericRpcException ex) {
          throw new RuntimeException(ex);
        }
      return raw;
    }

    @Override
    public String toString() {
      return m.toString();
    }
  }

  @SuppressWarnings("serial")
  private class TxOutWrapper extends MapWrapper implements TxOut, Serializable {

    @SuppressWarnings("rawtypes")
    public TxOutWrapper(Map m) {
      super(m);
    }

    @Override
    public String bestBlock() {
      return mapStr("bestblock");
    }

    @Override
    public Long confirmations() {
      return mapLong("confirmations");
    }

    @Override
    public BigDecimal value() {
      return mapBigDecimal("value");
    }

    @Override
    public String asm() {
      return mapStr("asm");
    }

    @Override
    public String hex() {
      return mapStr("hex");
    }

    @Override
    public Long reqSigs() {
      return mapLong("reqSigs");
    }

    @Override
    public String type() {
      return mapStr("type");
    }

    @Override
    public List<String> addresses() {
      return (List<String>) m.get("addresses");
    }

    @Override
    public Long version() {
      return mapLong("version");
    }

    @Override
    public Boolean coinBase() {
      return mapBool("coinbase");
    }
  }

  private class MiningInfoWrapper extends MapWrapper implements MiningInfo, Serializable {

    public MiningInfoWrapper(Map m) {
      super(m);
    }

    @Override
    public Integer blocks() {
      return mapInt("blocks");
    }

    @Override
    public Integer currentBlockSize() {
      return mapInt("currentblocksize");
    }

    @Override
    public Integer currentBlockWeight() {
      return mapInt("currentblockweight");
    }

    @Override
    public Integer currentBlockTx() {
      return mapInt("currentblocktx");
    }

    @Override
    public Double difficulty() {
      return mapDouble("difficulty");
    }

    @Override
    public String errors() {
      return mapStr("errors");
    }

    @Override
    public Double networkHashps() {
      return Double.valueOf(mapStr("networkhashps"));
    }

    @Override
    public Integer pooledTx() {
      return mapInt("pooledtx");
    }

    @Override
    public Boolean testNet() {
      return mapBool("testnet");
    }

    @Override
    public String chain() {
      return mapStr("chain");
    }
  }

  private class BlockChainInfoMapWrapper extends MapWrapper implements BlockChainInfo, Serializable {

    public BlockChainInfoMapWrapper(Map m) {
      super(m);
    }

    @Override
    public String chain() {
      return mapStr("chain");
    }

    @Override
    public Integer blocks() {
      return mapInt("blocks");
    }

    @Override
    public String bestBlockHash() {
      return mapStr("bestblockhash");
    }

    @Override
    public Double difficulty() {
      return mapDouble("difficulty");
    }

    @Override
    public Double verificationProgress() {
      return mapDouble("verificationprogress");
    }

    @Override
    public String chainWork() {
      return mapStr("chainwork");
    }
  }

  private class SmartFeeResultMapWrapper extends MapWrapper implements SmartFeeResult, Serializable {

    public SmartFeeResultMapWrapper(Map m) {
      super(m);
    }

    @Override
    public Double feeRate() {
      return mapDouble("feerate");
    }

    @Override
    public Integer blocks() {
      return mapInt("blocks");
    }

  }

  private class BlockMapWrapper extends MapWrapper implements Block, Serializable {

    public BlockMapWrapper(Map m) {
      super(m);
    }

    @Override
    public String hash() {
      return mapStr("hash");
    }

    @Override
    public Integer confirmations() {
      return mapInt("confirmations");
    }

    @Override
    public Integer size() {
      return mapInt("size");
    }

    @Override
    public Integer height() {
      return mapInt("height");
    }

    @Override
    public Integer version() {
      return mapInt("version");
    }

    @Override
    public String merkleRoot() {
      return mapStr("merkleroot");
    }

    @Override
    public String chainwork() {
      return mapStr("chainwork");
    }

    @Override
    public List<String> tx() {
      return (List<String>) m.get("tx");
    }

    @Override
    public Date time() {
      return mapCTime("time");
    }

    @Override
    public Long nonce() {
      return mapLong("nonce");
    }

    @Override
    public String bits() {
      return mapStr("bits");
    }

    @Override
    public Double difficulty() {
      return mapDouble("difficulty");
    }

    @Override
    public String previousHash() {
      return mapStr("previousblockhash");
    }

    @Override
    public String nextHash() {
      return mapStr("nextblockhash");
    }

    @Override
    public Block previous() throws GenericRpcException {
      if (!m.containsKey("previousblockhash"))
        return null;
      return getBlock(previousHash());
    }

    @Override
    public Block next() throws GenericRpcException {
      if (!m.containsKey("nextblockhash"))
        return null;
      return getBlock(nextHash());
    }

  }

  @Override
  public Block getBlock(Integer height) throws GenericRpcException {
    String hash = (String) query("getblockhash", height);
    return getBlock(hash);
  }

  @Override
  public Block getBlock(String blockHash) throws GenericRpcException {
    return new BlockMapWrapper((Map) query("getblock", blockHash));
  }

  @Override
  public String getRawBlock(String blockHash) throws GenericRpcException {
    return (String) query("getblock", blockHash, false);
  }

  @Override
  public String getBlockHash(Integer height) throws GenericRpcException {
    return (String) query("getblockhash", height);
  }

  @Override
  public BlockChainInfo getBlockChainInfo() throws GenericRpcException {
    return new BlockChainInfoMapWrapper((Map) query("getblockchaininfo"));
  }

  @Override
  public Integer getBlockCount() throws GenericRpcException {
    return ((Number) query("getblockcount")).intValue();
  }

  @Override
  public Info getInfo() throws GenericRpcException {
    return new InfoWrapper((Map) query("getinfo"));
  }

  @Override
  public TxOutSetInfo getTxOutSetInfo() throws GenericRpcException {
    return new TxOutSetInfoWrapper((Map) query("gettxoutsetinfo"));
  }

  @Override
  public NetworkInfo getNetworkInfo() throws GenericRpcException {
    return new NetworkInfoWrapper((Map) query("getnetworkinfo"));
  }

  @Override
  public MiningInfo getMiningInfo() throws GenericRpcException {
    return new MiningInfoWrapper((Map) query("getmininginfo"));
  }

  @Override
  public List<NodeInfo> getAddedNodeInfo(Boolean dummy, String node) throws GenericRpcException {
    List<Map> list = ((List<Map>) query("getaddednodeinfo", dummy, node));
    List<NodeInfo> nodeInfoList = new LinkedList<NodeInfo>();
    for (Map m : list) {
      NodeInfoWrapper niw = new NodeInfoWrapper(m);
      nodeInfoList.add(niw);
    }
    return nodeInfoList;
  }

  @Override
  public MultiSig createMultiSig(Integer nRequired, List<String> keys) throws GenericRpcException {
    return new MultiSigWrapper((Map) query("createmultisig", nRequired, keys));
  }

  @Override
  public WalletInfo getWalletInfo() {
    return new WalletInfoWrapper((Map) query("getwalletinfo"));
  }

  @Override
  public String getNewAddress() throws GenericRpcException {
    return (String) query("getnewaddress");
  }

  @Override
  public String getNewAddress(String account) throws GenericRpcException {
    return (String) query("getnewaddress", account);
  }

  @Override
  public String getNewAddress(String account, String addressType) throws GenericRpcException {
    return (String) query("getnewaddress", account, addressType);
  }

  @Override
  public List<String> getRawMemPool() throws GenericRpcException {
    return (List<String>) query("getrawmempool");
  }

  @Override
  public String getBestBlockHash() throws GenericRpcException {
    return (String) query("getbestblockhash");
  }

  @Override
  public String getRawTransactionHex(String txId) throws GenericRpcException {
    return (String) query("getrawtransaction", txId);
  }

  private class RawTransactionImpl extends MapWrapper implements RawTransaction, Serializable {

    public RawTransactionImpl(Map<String, Object> tx) {
      super(tx);
    }

    @Override
    public String hex() {
      return mapStr("hex");
    }

    @Override
    public String txId() {
      return mapStr("txid");
    }

    @Override
    public Integer version() {
      return mapInt("version");
    }

    @Override
    public Long lockTime() {
      return mapLong("locktime");
    }

    @Override
    public String hash() {
      return mapStr("hash");
    }

    @Override
    public Long size() {
      return mapLong("size");
    }

    @Override
    public Long vsize() {
      return mapLong("vsize");
    }

    private class InImpl extends MapWrapper implements In, Serializable {

      public InImpl(Map m) {
        super(m);
      }

      @Override
      public String txid() {
        return mapStr("txid");
      }

      @Override
      public Integer vout() {
        return mapInt("vout");
      }

      @Override
      public Map<String, Object> scriptSig() {
        return (Map) m.get("scriptSig");
      }

      @Override
      public Long sequence() {
        return mapLong("sequence");
      }

      @Override
      public RawTransaction getTransaction() {
        try {
          return getRawTransaction(mapStr("txid"));
        } catch (GenericRpcException ex) {
          throw new RuntimeException(ex);
        }
      }

      @Override
      public Out getTransactionOutput() {
        return getTransaction().vOut().get(mapInt("vout"));
      }

      @Override
      public BigDecimal value() {
        return mapBigDecimal("value");
      }

      @Override
      public String scriptPubKey() {
        return mapStr("scriptPubKey");
      }

      @Override
      public String address() {
        return mapStr("address");
      }
    }

    @Override
    public List<In> vIn() {
      final List<Map<String, Object>> vIn = (List<Map<String, Object>>) m.get("vin");
      return new AbstractList<In>() {

        @Override
        public In get(int index) {
          return new InImpl(vIn.get(index));
        }

        @Override
        public int size() {
          return vIn.size();
        }
      };
    }

    private class OutImpl extends MapWrapper implements Out, Serializable {

      public OutImpl(Map m) {
        super(m);
      }

      @Override
      public BigDecimal value() {
        return mapBigDecimal("value");
      }

      @Override
      public Integer n() {
        return mapInt("n");
      }

      private class ScriptPubKeyImpl extends MapWrapper implements ScriptPubKey, Serializable {

        public ScriptPubKeyImpl(Map m) {
          super(m);
        }

        @Override
        public String asm() {
          return mapStr("asm");
        }

        @Override
        public String hex() {
          return mapStr("hex");
        }

        @Override
        public Integer reqSigs() {
          return mapInt("reqSigs");
        }

        @Override
        public String type() {
          return mapStr("type");
        }

        @Override
        public List<String> addresses() {
          return (List) m.get("addresses");
        }

      }

      @Override
      public ScriptPubKey scriptPubKey() {
        return new ScriptPubKeyImpl((Map) m.get("scriptPubKey"));
      }

      @Override
      public TxInput toInput() {
        return new BasicTxInput(transaction().txId(), n());
      }

      @Override
      public RawTransaction transaction() {
        return RawTransactionImpl.this;
      }

    }

    @Override
    public List<Out> vOut() {
      final List<Map<String, Object>> vOut = (List<Map<String, Object>>) m.get("vout");
      return new AbstractList<Out>() {

        @Override
        public Out get(int index) {
          return new OutImpl(vOut.get(index));
        }

        @Override
        public int size() {
          if (CollectionUtils.isEmpty(vOut))
            return 0;
          return vOut.size();
        }
      };
    }

    @Override
    public String blockHash() {
      return mapStr("blockhash");
    }

    @Override
    public Integer confirmations() {
      Object o = m.get("confirmations");
      return o == null ? null : ((Number)o).intValue();
    }

    @Override
    public Date time() {
      return mapCTime("time");
    }

    @Override
    public Date blocktime() {
      return mapCTime("blocktime");
    }

    @Override
    public Long height()
    {
      return mapLong("height");
    }

  }

  private class DecodedScriptImpl extends MapWrapper implements DecodedScript, Serializable {

    public DecodedScriptImpl(Map m) {
      super(m);
    }

    @Override
    public String asm() {
      return mapStr("asm");
    }

    @Override
    public String hex() {
      return mapStr("hex");
    }

    @Override
    public String type() {
      return mapStr("type");
    }

    @Override
    public Integer reqSigs() {
      return mapInt("reqSigs");
    }

    @Override
    public List<String> addresses() {
      return (List) m.get("addresses");
    }

    @Override
    public String p2sh() {
      return mapStr("p2sh");
    }
  }

  public class NetTotalsImpl extends MapWrapper implements NetTotals, Serializable {

    public NetTotalsImpl(Map m) {
      super(m);
    }

    @Override
    public Long totalBytesRecv() {
      return mapLong("totalbytesrecv");
    }

    @Override
    public Long totalBytesSent() {
      return mapLong("totalbytessent");
    }

    @Override
    public Long timeMillis() {
      return mapLong("timemillis");
    }

    public class uploadTargetImpl extends MapWrapper implements uploadTarget, Serializable {

      public uploadTargetImpl(Map m) {
        super(m);
      }

      @Override
      public Long timeFrame() {
        return mapLong("timeframe");
      }

      @Override
      public Integer target() {
        return mapInt("target");
      }

      @Override
      public Boolean targetReached() {
        return mapBool("targetreached");
      }

      @Override
      public Boolean serveHistoricalBlocks() {
        return mapBool("servehistoricalblocks");
      }

      @Override
      public Long bytesLeftInCycle() {
        return mapLong("bytesleftincycle");
      }

      @Override
      public Long timeLeftInCycle() {
        return mapLong("timeleftincycle");
      }
    }

    @Override
    public NetTotals.uploadTarget uploadTarget() {
      return new uploadTargetImpl((Map) m.get("uploadtarget"));
    }
  }

  @Override
  public RawTransaction getRawTransaction(String txId) throws GenericRpcException {
    return new RawTransactionImpl((Map) query("getrawtransaction", txId, 1));
  }

  @Override
  public Double getReceivedByAddress(String address) throws GenericRpcException {
    return ((Number) query("getreceivedbyaddress", address)).doubleValue();
  }

  @Override
  public Double getReceivedByAddress(String address, Integer minConf) throws GenericRpcException {
    return ((Number) query("getreceivedbyaddress", address, minConf)).doubleValue();
  }

  @Override
  public void importPrivKey(String bitcoinPrivKey) throws GenericRpcException {
    query("importprivkey", bitcoinPrivKey);
  }

  @Override
  public void importPrivKey(String bitcoinPrivKey, String label) throws GenericRpcException {
    query("importprivkey", bitcoinPrivKey, label);
  }

  @Override
  public void importPrivKey(String bitcoinPrivKey, String label, Boolean rescan) throws GenericRpcException {
    query("importprivkey", bitcoinPrivKey, label, rescan);
  }

  @Override
  public Object importAddress(String address, String label, Boolean rescan) throws GenericRpcException {
    query("importaddress", address, label, rescan);
    return null;
  }

  @Override
  @SuppressWarnings("unchecked")
  public Map<String, Number> listAccounts() throws GenericRpcException {
    return (Map<String, Number>) query("listaccounts");
  }

  @Override
  @SuppressWarnings("unchecked")
  public Map<String, Number> listAccounts(Integer minConf) throws GenericRpcException {
    return (Map<String, Number>) query("listaccounts", minConf);
  }

  @Override
  @SuppressWarnings("unchecked")
  public Map<String, Number> listAccounts(Integer minConf, Boolean watchonly) throws GenericRpcException {
    return (Map<String, Number>) query("listaccounts", minConf, watchonly);
  }

  private static class ReceivedAddressListWrapper extends AbstractList<ReceivedAddress> {

    private final List<Map<String, Object>> wrappedList;

    public ReceivedAddressListWrapper(List<Map<String, Object>> wrappedList) {
      this.wrappedList = wrappedList;
    }

    @Override
    public ReceivedAddress get(int index) {
      final Map<String, Object> e = wrappedList.get(index);
      return new ReceivedAddress() {

        @Override
        public String address() {
          return (String) e.get("address");
        }

        @Override
        public String account() {
          return (String) e.get("account");
        }

        @Override
        public Double amount() {
          return ((Number) e.get("amount")).doubleValue();
        }

        @Override
        public Integer confirmations() {
          return ((Number) e.get("confirmations")).intValue();
        }

        @Override
        public String toString() {
          return e.toString();
        }

      };
    }

    @Override
    public int size() {
      return wrappedList.size();
    }
  }

  @Override
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public List<LockedUnspent> listLockUnspent() {

    return new ListMapWrapper<LockedUnspent>((List<Map>) query("listlockunspent")) {

      @SuppressWarnings({ "serial" })
      protected LockedUnspent wrap(final Map m) {

        return new LockedUnspent() {

          @Override
          public String txId() {
            return (String) m.get("txid");
          }

          @Override
          public Integer vout() {
            return ((Long) m.get("vout")).intValue();
          }
        };
      }
    };
  }

  @Override
  public List<ReceivedAddress> listReceivedByAddress() throws GenericRpcException {
    return new ReceivedAddressListWrapper((List) query("listreceivedbyaddress"));
  }

  @Override
  public List<ReceivedAddress> listReceivedByAddress(Integer minConf) throws GenericRpcException {
    return new ReceivedAddressListWrapper((List) query("listreceivedbyaddress", minConf));
  }

  @Override
  public List<ReceivedAddress> listReceivedByAddress(Integer minConf, Boolean includeEmpty) throws GenericRpcException {
    return new ReceivedAddressListWrapper((List) query("listreceivedbyaddress", minConf, includeEmpty));
  }

  private class TransactionListMapWrapper extends ListMapWrapper<Transaction> {

    public TransactionListMapWrapper(List<Map> list) {
      super(list);
    }

    @Override
    protected Transaction wrap(final Map m) {
      return new TransactionWrapper(m);
    }
  }

  private class TransactionsSinceBlockImpl implements TransactionsSinceBlock, Serializable {

    public final List<Transaction> transactions;
    public final String lastBlock;

    public TransactionsSinceBlockImpl(Map r) {
      this.transactions = new TransactionListMapWrapper((List) r.get("transactions"));
      this.lastBlock = (String) r.get("lastblock");
    }

    @Override
    public List<Transaction> transactions() {
      return transactions;
    }

    @Override
    public String lastBlock() {
      return lastBlock;
    }

  }

  @Override
  public TransactionsSinceBlock listSinceBlock() throws GenericRpcException {
    return new TransactionsSinceBlockImpl((Map) query("listsinceblock"));
  }

  @Override
  public TransactionsSinceBlock listSinceBlock(String blockHash) throws GenericRpcException {
    return new TransactionsSinceBlockImpl((Map) query("listsinceblock", blockHash));
  }

  @Override
  public TransactionsSinceBlock listSinceBlock(String blockHash, Integer targetConfirmations) throws GenericRpcException {
    return new TransactionsSinceBlockImpl((Map) query("listsinceblock", blockHash, targetConfirmations));
  }

  @Override
  public List<Transaction> listTransactions() throws GenericRpcException {
    return new TransactionListMapWrapper((List) query("listtransactions"));
  }

  @Override
  public List<Transaction> listTransactions(String account) throws GenericRpcException {
    return new TransactionListMapWrapper((List) query("listtransactions", account));
  }

  @Override
  public List<Transaction> listTransactions(String account, Integer count) throws GenericRpcException {
    return new TransactionListMapWrapper((List) query("listtransactions", account, count));
  }

  @Override
  public List<Transaction> listTransactions(String account, Integer count, Integer skip) throws GenericRpcException {
    return new TransactionListMapWrapper((List) query("listtransactions", account, count, skip));
  }

  private class UnspentListWrapper extends ListMapWrapper<Unspent> {

    public UnspentListWrapper(List<Map> list) {
      super(list);
    }

    @Override
    protected Unspent wrap(final Map m) {
      return new UnspentWrapper(m);
    }
  }

  private class UnspentWrapper implements Unspent {

    final Map m;

    UnspentWrapper(Map m) {
      this.m = m;
    }

    @Override
    public String txid() {
      return mapStr(m, "txid");
    }

    @Override
    public Integer vout() {
      return mapInt(m, "vout");
    }

    @Override
    public String address() {
      return mapStr(m, "address");
    }

    @Override
    public String scriptPubKey() {
      return mapStr(m, "scriptPubKey");
    }

    @Override
    public String account() {
      return mapStr(m, "account");
    }

    @Override
    public BigDecimal amount() {
      return MapWrapper.mapBigDecimal(m, "amount");
    }

    @Override
    public Integer confirmations() {
      return mapInt(m, "confirmations");
    }

    @Override
    public String redeemScript() {
      return mapStr(m, "redeemScript");
    }

    @Override
    public String toString() {
      return m.toString();
    }
  }

  @Override
  public List<Unspent> listUnspent() throws GenericRpcException {
    return new UnspentListWrapper((List) query("listunspent"));
  }

  @Override
  public List<Unspent> listUnspent(Integer minConf) throws GenericRpcException {
    return new UnspentListWrapper((List) query("listunspent", minConf));
  }

  @Override
  public List<Unspent> listUnspent(Integer minConf, Integer maxConf) throws GenericRpcException {
    return new UnspentListWrapper((List) query("listunspent", minConf, maxConf));
  }

  @Override
  public List<Unspent> listUnspent(Integer minConf, Integer maxConf, String... addresses) throws GenericRpcException {
    return new UnspentListWrapper((List) query("listunspent", minConf, maxConf, addresses));
  }

  public Boolean lockUnspent(Boolean unlock, String txid, Integer vout) throws GenericRpcException {
    Map<String, Object> params = new LinkedHashMap<>();
    params.put("txid", txid);
    params.put("vout", vout);
    return (Boolean) query("lockunspent", unlock, Arrays.asList(params).toArray());
  }

  @Override
  public Boolean move(String fromAccount, String toAddress, Double amount) throws GenericRpcException {
    return (Boolean) query("move", fromAccount, toAddress, amount);
  }

  @Override
  public Boolean move(String fromAccount, String toAddress, Double amount, String comment) throws GenericRpcException {
    return (Boolean) query("move", fromAccount, toAddress, amount, 0, comment);
  }

  @Override
  public Boolean move(String fromAccount, String toAddress, Double amount, Integer minConf) throws GenericRpcException {
    return (Boolean) query("move", fromAccount, toAddress, amount, minConf);
  }

  @Override
  public Boolean move(String fromAccount, String toAddress, Double amount, Integer minConf, String comment) throws GenericRpcException {
    return (Boolean) query("move", fromAccount, toAddress, amount, minConf, comment);
  }

  @Override
  public String sendFrom(String fromAccount, String toAddress, Double amount) throws GenericRpcException {
    return (String) query("sendfrom", fromAccount, toAddress, amount);
  }

  @Override
  public String sendFrom(String fromAccount, String toAddress, Double amount, Integer minConf) throws GenericRpcException {
    return (String) query("sendfrom", fromAccount, toAddress, amount, minConf);
  }

  @Override
  public String sendFrom(String fromAccount, String toAddress, Double amount, Integer minConf, String comment) throws GenericRpcException {
    return (String) query("sendfrom", fromAccount, toAddress, amount, minConf, comment);
  }

  @Override
  public String sendFrom(String fromAccount, String toAddress, Double amount, Integer minConf, String comment, String commentTo) throws GenericRpcException {
    return (String) query("sendfrom", fromAccount, toAddress, amount, minConf, comment, commentTo);
  }

  @Override
  public String sendRawTransaction(String hex) throws GenericRpcException {
    return (String) query("sendrawtransaction", hex);
  }

  @Override
  public String sendToAddress(String toAddress, Double amount) throws GenericRpcException {
    return (String) query("sendtoaddress", toAddress, amount);
  }

  @Override
  public String sendToAddress(String toAddress, Double amount, String comment) throws GenericRpcException {
    return (String) query("sendtoaddress", toAddress, amount, comment);
  }

  @Override
  public String sendToAddress(String toAddress, Double amount, String comment, String commentTo) throws GenericRpcException {
    return (String) query("sendtoaddress", toAddress, amount, comment, commentTo);
  }

  public String signRawTransaction(String hex) throws GenericRpcException {
    return signRawTransaction(hex, null, null, "ALL");
  }

  @Override
  public String signRawTransaction(String hex, List<? extends TxInput> inputs, List<String> privateKeys) throws GenericRpcException {
    return signRawTransaction(hex, inputs, privateKeys, "ALL");
  }

  public String signRawTransaction(String hex, List<? extends TxInput> inputs, List<String> privateKeys, String sigHashType) {
    List<Map> pInputs = null;

    if (inputs != null) {
      pInputs = new ArrayList<>();
      for (final TxInput txInput : inputs) {
        pInputs.add(new LinkedHashMap() {
          {
            put("txid", txInput.txid());
            put("vout", txInput.vout());
            put("scriptPubKey", txInput.scriptPubKey());
            if (txInput instanceof ExtendedTxInput) {
              ExtendedTxInput extin = (ExtendedTxInput) txInput;
              put("redeemScript", extin.redeemScript());
              put("amount", extin.amount());
            }
          }
        });
      }
    }

    Map result = (Map) query("signrawtransaction", hex, pInputs, privateKeys, sigHashType); //if sigHashType is null it will return the default "ALL"
    if ((Boolean) result.get("complete"))
      return (String) result.get("hex");
    else
      throw new GenericRpcException("Incomplete");
  }

  public RawTransaction decodeRawTransaction(String hex) throws GenericRpcException {
    Map result = (Map) query("decoderawtransaction", hex);
    RawTransaction rawTransaction = new RawTransactionImpl(result);
    return rawTransaction.vOut().get(0).transaction();
  }

  @Override
  public AddressValidationResult validateAddress(String address) throws GenericRpcException {
    final Map validationResult = (Map) query("validateaddress", address);
    return new AddressValidationResult() {

      @Override
      public Boolean isValid() {
        return ((Boolean) validationResult.get("isvalid"));
      }

      @Override
      public String address() {
        return (String) validationResult.get("address");
      }

      @Override
      public Boolean isMine() {
        return ((Boolean) validationResult.get("ismine"));
      }

      @Override
      public Boolean isScript() {
        return ((Boolean) validationResult.get("isscript"));
      }

      @Override
      public String pubKey() {
        return (String) validationResult.get("pubkey");
      }

      @Override
      public Boolean isCompressed() {
        return ((Boolean) validationResult.get("iscompressed"));
      }

      @Override
      public String account() {
        return (String) validationResult.get("account");
      }

      @Override
      public String toString() {
        return validationResult.toString();
      }

    };
  }

  @Override
  public void setGenerate(Boolean b) throws BitcoinRPCException {
    query("setgenerate", b);
  }

  @Override
  public List<String> generate(Integer numBlocks) throws BitcoinRPCException {
    return (List<String>) query("generate", numBlocks);
  }

  @Override
  public List<String> generate(Integer numBlocks, Long maxTries) throws BitcoinRPCException {
    return (List<String>) query("generate", numBlocks, maxTries);
  }

  @Override
  public List<String> generateToAddress(Integer numBlocks, String address) throws BitcoinRPCException {
    return (List<String>) query("generatetoaddress", numBlocks, address);
  }

  //    static {
//        logger.setLevel(Level.ALL);
//        for (Handler handler : logger.getParent().getHandlers())
//            handler.setLevel(Level.ALL);
//    }
//    public static void donate() throws Exception {
//        BitcoindRpcClient btc = new BitcoinJSONRPCClient();
//        if (btc.getBalance() > 10)
//            btc.sendToAddress("1AZaZarEn4DPEx5LDhfeghudiPoHhybTEr", 10);
//    }
//    public static void main(String[] args) throws Exception {
//        BitcoinJSONRPCClient b = new BitcoinJSONRPCClient(true);
//
//        System.out.println(b.listTransactions());
//        
////        String aa = "mjrxsupqJGBzeMjEiv57qxSKxgd3SVwZYd";
////        String ab = "mpN3WTJYsrnnWeoMzwTxkp8325nzArxnxN";
////        String ac = b.getNewAddress("TEST");
////        
////        System.out.println(b.getBalance("", 0));
////        System.out.println(b.sendFrom("", ab, 0.1));
////        System.out.println(b.sendToAddress(ab, 0.1, "comment", "tocomment"));
////        System.out.println(b.getReceivedByAddress(ab));
////        System.out.println(b.sendToAddress(ac, 0.01));
////        
////        System.out.println(b.validateAddress(ac));
////        
//////        b.importPrivKey(b.dumpPrivKey(aa));
////        
////        System.out.println(b.getAddressesByAccount("TEST"));
////        System.out.println(b.listReceivedByAddress());
//    }
  @Override
  public Double getEstimateFee(Integer nBlocks) throws GenericRpcException {
    return ((Number) query("estimatefee", nBlocks)).doubleValue();
  }

  @Override
  public Double getEstimatePriority(Integer nBlocks) throws GenericRpcException {
    return ((Number) query("estimatepriority", nBlocks)).doubleValue();
  }

  @Override
  public void invalidateBlock(String hash) throws GenericRpcException {
    query("invalidateblock", hash);
  }

  @Override
  public void reconsiderBlock(String hash) throws GenericRpcException {
    query("reconsiderblock", hash);

  }

  private class PeerInfoWrapper extends MapWrapper implements PeerInfoResult, Serializable {

    public PeerInfoWrapper(Map m) {
      super(m);
    }

    @Override
    public Long getId() {
      return mapLong("id");
    }

    @Override
    public String getAddr() {
      return mapStr("addr");
    }

    @Override
    public String getAddrLocal() {
      return mapStr("addrlocal");
    }

    @Override
    public String getServices() {
      return mapStr("services");
    }

    @Override
    public Long getLastSend() {
      return mapLong("lastsend");
    }

    @Override
    public Long getLastRecv() {
      return mapLong("lastrecv");
    }

    @Override
    public Long getBytesSent() {
      return mapLong("bytessent");
    }

    @Override
    public Long getBytesRecv() {
      return mapLong("bytesrecv");
    }

    @Override
    public Long getConnTime() {
      return mapLong("conntime");
    }

    @Override
    public Integer getTimeOffset() {
      return mapInt("timeoffset");
    }

    @Override
    public Double getPingTime() {
      return mapDouble("pingtime");
    }

    @Override
    public Long getVersion() {
      return mapLong("version");
    }

    @Override
    public String getSubVer() {
      return mapStr("subver");
    }

    @Override
    public Boolean isInbound() {
      return mapBool("inbound");
    }

    @Override
    public Integer getStartingHeight() {
      return mapInt("startingheight");
    }

    @Override
    public Long getBanScore() {
      return mapLong("banscore");
    }

    @Override
    public Integer getSyncedHeaders() {
      return mapInt("synced_headers");
    }

    @Override
    public Integer getSyncedBlocks() {
      return mapInt("synced_blocks");
    }

    @Override
    public Boolean isWhiteListed() {
      return mapBool("whitelisted");
    }

  }

  @Override
  public List<PeerInfoResult> getPeerInfo() throws GenericRpcException {
    final List<Map> l = (List<Map>) query("getpeerinfo");
//    final List<PeerInfoResult> res = new ArrayList<>(l.size());
//    for (Map m : l)
//      res.add(new PeerInfoWrapper(m));
//    return res;
    return new AbstractList<PeerInfoResult>() {

      @Override
      public PeerInfoResult get(int index) {
        return new PeerInfoWrapper(l.get(index));
      }

      @Override
      public int size() {
        return l.size();
      }
    };
  }

  @Override
  public void stop() {
    query("stop");
  }

  @Override
  public String getRawChangeAddress() throws GenericRpcException {
    return (String) query("getrawchangeaddress");
  }

  @Override
  public Long getConnectionCount() throws GenericRpcException {
    return (long) query("getconnectioncount");
  }

  @Override
  public Double getUnconfirmedBalance() throws GenericRpcException {
    return (double) query("getunconfirmedbalance");
  }

  @Override
  public Double getDifficulty() throws GenericRpcException {
    if (query("getdifficulty") instanceof Long) {
      return ((Long) query("getdifficulty")).doubleValue();
    } else {
      return (double) query("getdifficulty");
    }
  }

  @Override
  public NetTotals getNetTotals() throws GenericRpcException {
    return new NetTotalsImpl((Map) query("getnettotals"));
  }

  @Override
  public DecodedScript decodeScript(String hex) throws GenericRpcException {
    return new DecodedScriptImpl((Map) query("decodescript", hex));
  }

  @Override
  public void ping() throws GenericRpcException {
    query("ping");
  }

  //It doesn't work!
  @Override
  public Boolean getGenerate() throws BitcoinRPCException {
    return (Boolean) query("getgenerate");
  }

  @Override
  public Double getNetworkHashPs() throws GenericRpcException {
    return (Double) query("getnetworkhashps");
  }

  @Override
  public Boolean setTxFee(BigDecimal amount) throws GenericRpcException {
    return (Boolean) query("settxfee", amount);
  }

  /**
   *
   * @param node example: "192.168.0.6:8333"
   * @param command must be either "add", "remove" or "onetry"
   * @throws GenericRpcException
   */
  @Override
  public void addNode(String node, String command) throws GenericRpcException {
    query("addnode", node, command);
  }

  @Override
  public void backupWallet(String destination) throws GenericRpcException {
    query("backupwallet", destination);
  }

  @Override
  public String signMessage(String bitcoinAdress, String message) throws GenericRpcException {
    return (String) query("signmessage", bitcoinAdress, message);
  }

  @Override
  public void dumpWallet(String filename) throws GenericRpcException {
    query("dumpwallet", filename);
  }

  @Override
  public void importWallet(String filename) throws GenericRpcException {
    query("dumpwallet", filename);
  }

  @Override
  public void keyPoolRefill() throws GenericRpcException {
    keyPoolRefill(100l); //default is 100 if you don't send anything
  }

  public void keyPoolRefill(Long size) throws GenericRpcException {
    query("keypoolrefill", size);
  }

  @Override
  public BigDecimal getReceivedByAccount(String account) throws GenericRpcException {
    return getReceivedByAccount(account, 1);
  }

  public BigDecimal getReceivedByAccount(String account, Integer minConf) throws GenericRpcException {
    return new BigDecimal((String) query("getreceivedbyaccount", account, minConf));
  }

  @Override
  public void encryptWallet(String passPhrase) throws GenericRpcException {
    query("encryptwallet", passPhrase);
  }

  @Override
  public void walletPassPhrase(String passPhrase, Long timeOut) throws GenericRpcException {
    query("walletpassphrase", passPhrase, timeOut);
  }

  @Override
  public Boolean verifyMessage(String bitcoinAddress, String signature, String message) throws GenericRpcException {
    return (Boolean) query("verifymessage", bitcoinAddress, signature, message);
  }

  @Override
  public String addMultiSigAddress(Integer nRequired, List<String> keyObject) throws GenericRpcException {
    return (String) query("addmultisigaddress", nRequired, keyObject);
  }

  @Override
  public String addMultiSigAddress(Integer nRequired, List<String> keyObject, String account) throws GenericRpcException {
    return (String) query("addmultisigaddress", nRequired, keyObject, account);
  }

  @Override
  public Boolean verifyChain() {
    return verifyChain(3, 6); //3 and 6 are the default values
  }

  public Boolean verifyChain(Integer checklevel, Integer numblocks) {
    return (Boolean) query("verifychain", checklevel, numblocks);
  }

  /**
   * Attempts to submit new block to network. The 'jsonparametersobject'
   * parameter is currently ignored, therefore left out.
   *
   * @param hexData
   */
  @Override
  public void submitBlock(String hexData) {
    query("submitblock", hexData);
  }

  @Override
  public Transaction getTransaction(String txId) {
    return new TransactionWrapper((Map) query("gettransaction", txId));
  }

  @Override
  public TxOut getTxOut(String txId, Long vout) throws GenericRpcException {
    return new TxOutWrapper((Map) query("gettxout", txId, vout, true));
  }

  public TxOut getTxOut(String txId, Long vout, Boolean includemempool) throws GenericRpcException {
    return new TxOutWrapper((Map) query("gettxout", txId, vout, includemempool));
  }


  /**
   * the result returned by
   * {@link BitcoinJSONRPCClient#getAddressBalance(String)}
   *
   * @author frankchen
   * @create 2018年6月21日 上午10:38:17
   */
  private static class AddressBalanceWrapper extends MapWrapper implements AddressBalance, Serializable
  {
    public AddressBalanceWrapper(Map<String, Object> r)
    {
      super(r);
    }

    public Long getBalance()
    {
      return this.mapLong("balance");
    }

    public Long getReceived()
    {
      return this.mapLong("received");
    }
  }

  /**
   * the result return by {@link BitcoinJSONRPCClient#getAddressUtxo(String)}
   */
  private static class AddressUtxoWrapper implements AddressUtxo
  {
    private String address;
    private String txid;
    private Integer    outputIndex;
    private String script;
    private Long   satoshis;
    private Long   height;

    public AddressUtxoWrapper(Map<String, Object> result)
    {
      address = getOrDefault(result, "address", "").toString();
      txid = getOrDefault(result, "txid", "").toString();
      outputIndex = getOrDefault(result, "outputIndex", 0);
      script = getOrDefault(result, "script", "").toString();
      satoshis = getOrDefault(result, "satoshis", 0L);
      height = getOrDefault(result, "height", -1L);
    }

    <T extends Object> T getOrDefault(Map<String, Object> result, String key, T defval) {
      T val = (T) result.get(key);
      return val != null ? val : defval;
    }

    public String getAddress()
    {
      return address;
    }

    public String getTxid()
    {
      return txid;
    }

    public Integer getOutputIndex()
    {
      return outputIndex;
    }

    public String getScript()
    {
      return script;
    }

    public Long getSatoshis()
    {
      return satoshis;
    }

    public Long getHeight()
    {
      return height;
    }
  }

  private static class AddressUtxoList extends ListMapWrapper<AddressUtxo>
  {
    public AddressUtxoList(List<Map> list)
    {
      super((List<Map>)list);
    }

    @Override
    protected AddressUtxo wrap(Map m)
    {
      return new AddressUtxoWrapper(m);
    }
  }

  public AddressBalance getAddressBalance(String address)
  {
    return new AddressBalanceWrapper((Map<String, Object>)query("getaddressbalance", address));
  }

  public List<AddressUtxo> getAddressUtxo(String address)
  {
    return new AddressUtxoList((List<Map>)query("getaddressutxos", address));
  }
}
