package com.zatgo.zup.transaction.entity.mq;

import java.io.Serializable;

/**
 * Created by 46041 on 2018/10/23.
 */
public class NotifyCallbackMsg implements Serializable {

    private Integer count;

    private String result;
    /**
     * 1充值0提币
     */
    private Integer type;


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
