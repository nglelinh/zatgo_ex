package com.zatgo.zup.transaction.service;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by 46041 on 2018/10/10.
 */
public interface CoinService {


    /**
     * 发送交易
     * @param toAddress
     * @param fromAddress
     * @param amount
     * @return
     */
    void sendTransaction(String toAddress, String fromAddress, BigDecimal amount, String businessTxId, String coinType);

    /**
     * 扫描区块,并更新相关充值信息
     */
    void scanBlock() throws IOException;

    /**
     * 确认交易是否达到确认数
     */
    void checkTransaction() throws IOException;

    /**
     *  获取待提币交易
     */
    void GetBtcExtractRecord();

    /**
     * 收集零钱到系统地址
     */
    void collect();

    void addAddress();

}
