package com.zatgo.zup.transaction.entity.usdt;

/**
 * Created by 46041 on 2018/11/26.
 */
public class ScriptPubkey {

    private String asm;

    private String hex;

    private String reqSigs;

    private String type;

    private String address;


    public String getAsm() {
        return asm;
    }

    public void setAsm(String asm) {
        this.asm = asm;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getReqSigs() {
        return reqSigs;
    }

    public void setReqSigs(String reqSigs) {
        this.reqSigs = reqSigs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
