package com.zatgo.zup.transaction.util.usdt;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.zatgo.zup.common.model.Commands;
import com.zatgo.zup.common.utils.ArgsUtils;
import com.zatgo.zup.transaction.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.util.MongoUtil;

/**
 * Created by 46041 on 2018/12/13.
 */

@Component
public class UsdtUtil {

    private static final Logger logger = LoggerFactory.getLogger(UsdtUtil.class);

    @Value("${transaction.user}")
    private String user;
    @Value("${transaction.password}")
    private String password;
    @Value("${transaction.host}")
    private String host;
    @Value("${transaction.port}")
    private String port;

    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private SignRemotService signRemotService;

    private JsonRpcHttpClient client = null;

    private static Pattern OPTRTURN = Pattern.compile("^OP_RETURN [a-zA-Z0-9]*");

    private static Pattern P2PKH = Pattern.compile("^OP_DUP OP_HASH160 [a-zA-Z0-9]* OP_EQUALVERIFY OP_CHECKSIG$");

    private static JsonRpcHttpClient getClient(String ad){
        JsonRpcHttpClient client = null;
        try {
            if (client == null){
                client = new JsonRpcHttpClient(new URL("http://47.74.145.17:8332"));
//                client = new JsonRpcHttpClient(new URL("http://47.74.145.17:8332"));
                String cred = Base64.encodeBase64String(("111:222").getBytes());
                Map<String, String> headers = new HashMap<String, String>(1);
                headers.put("Authorization", "Basic " + cred);
                client.setHeaders(headers);
            }
        } catch (Exception e){
            logger.error("", e);
        }
        return client;
    }

    public static Integer getBlockCount() throws Throwable {
        return getClient("sd").invoke(Commands.GET_BLOCK_COUNT, null, Integer.class);
    }

    public static JSONObject getTransaction(String txId) throws Throwable {
        String hex = getClient("").invoke(Commands.GET_RAW_TRANSACTION, ArgsUtils.asList(txId), String.class);

        return getClient("").invoke(Commands.DECODE_RAW_TRANSACTION, ArgsUtils.asList(hex), JSONObject.class);
    }

    public static JSONObject getUsdtTransaction(String txId) throws Throwable {

        return getClient("").invoke(Commands.GET_USDT_TRANSACTION, ArgsUtils.asList(txId), JSONObject.class);
    }

    /**
     * 获取区块中所有的交易ID list
     * @return
     */
    public static List<String> getBlockTx(String blockHash) throws Throwable {
        JSONObject res = getClient("").invoke(Commands.GET_BLOCK, ArgsUtils.asList(blockHash), JSONObject.class);
        if (res == null){
            return null;
        }
        return res.getJSONArray("tx").toJavaList(String.class);
    }


    /**
     * 获取块hash
     * @param height
     * @return
     */
    public static String getBlockHash(Integer height) throws Throwable {
        return getClient("").invoke(Commands.GET_BLOCK_HASH, ArgsUtils.asList(height), String.class);
    }


    public static void main(String[] args) throws Throwable {
//        "6a146f6d6e69000000000000001f000000004bb2a980"
//        Integer blockCount = getBlockCount();
//        System.out.println(blockCount);
//        String blockHash = getBlockHash(blockCount);
//        List<String> blockTx = getBlockTx(blockHash);
//        JSONObject transaction = getTransaction("e4ea604983f4cc2ce26c3e74a91e704e90183263d0748e3efb8715601eabf14d");
//        System.out.println(transaction);
//        transaction = getUsdtTransaction("d35526bc1837cb6a6b42353bd19e3dbfe8100892165ff30be3bc8eb456cb76b7");
//        System.out.println(transaction);
//        JSONObject transaction = getUsdtTransaction("1fc6ef3e006fce0f18d409e2a49f86472a47532913e954e4397e100a49ff0390");
//        System.out.println(blockTx);
//        Object a = getClient("").invoke("omni_getseedblocks", ArgsUtils.asList(509900, 510000), Object.class);
//        Object a = getClient("").invoke("omni_getseedblocks", null, Object.class);
//        System.out.println(a);
        Integer blockCount = getBlockCount();
        String blockHash = getBlockHash(blockCount);
        List<String> blockTx = getBlockTx(blockHash);
        String hex = getClient("").invoke(Commands.GET_RAW_TRANSACTION, ArgsUtils.asList(blockTx.get(3)), String.class);
        JSONObject rawTransaction = getClient("").invoke(Commands.DECODE_RAW_TRANSACTION, ArgsUtils.asList(hex), JSONObject.class);
        System.out.println(rawTransaction);
    }



//    private JsonRpcHttpClient getClient(){
//        try {
//            if (client == null){
//                client = new JsonRpcHttpClient(new URL("http://" + host + ":" + port));
//                String cred = Base64.encodeBase64String((user + ":" + password).getBytes());
//                Map<String, String> headers = new HashMap<String, String>(1);
//                headers.put("Authorization", "Basic " + cred);
//                client.setHeaders(headers);
//            }
//        } catch (Exception e){
//            logger.error("", e);
//        }
//        return client;
//    }
//
//
//    public Address getSystemAddress(String coinType){
//        Query query = new Query();
//        Criteria criteria = new Criteria();
//        criteria.and(MongoConstant.QTUM_ADDRESS_CLASS_TYPE).is(AddressTypeEnum.SYSTEM_ADDRESS.getCode());
//        query.addCriteria(criteria);
//        List<Address> data = mongoUtil.getData(query, Address.class, getTokenAddressName(coinType));
//        if (!CollectionUtils.isEmpty(data)){
//            return data.get(0);
//        }
//        return null;
//    }
//
//    /**
//     * 获取最新区块高度
//     * @return
//     * @throws Throwable
//     */
//    public Integer getBlockCount() throws Throwable {
//        return getClient().invoke(Commands.GET_BLOCK_COUNT, null, Integer.class);
//    }
//
//    public String getAddressByHex(String hex) throws Throwable {
//        if (StringUtils.isEmpty(hex))
//            return null;
//        if (hex.length() > 40)
//            hex = hex.substring(hex.length() - 40, hex.length());
//        return getClient().invoke(Commands.FROM_HEX_ADDRESS, ArgsUtils.asList(hex), String.class);
//    }
//
//    public String getTokenAddressName(String token){
//        return token.toUpperCase() + "_" + MongoConstant.QTUM_ADDRESS_CLASS_NAME;
//    }
//
//
//    /**
//     * 获取块hash
//     * @param height
//     * @return
//     */
//    public String getBlockHash(Integer height) throws Throwable {
//        return getClient().invoke(Commands.GET_BLOCK_HASH, ArgsUtils.asList(height), String.class);
//    }
//
//
//    /**
//     * 发送交易
//     * @param hex
//     * @return
//     */
//    public String sendRawTransaction(String hex) throws Throwable {
//        return getClient().invoke(Commands.SEND_RAW_TRANSACTION, ArgsUtils.asList(hex), String.class);
//    }
////
//    public JSONObject getTransactionReceipt(String txId) throws Throwable {
//        JSONArray invoke = getClient().invoke(Commands.GET_TRANSACTION_RECEIPT, ArgsUtils.asList(txId), JSONArray.class);
//        JSONObject res = null;
//        if (invoke != null && !invoke.isEmpty()){
//            res = invoke.getJSONObject(0);
//        }
//        return res;
//    }
//
//    public String getAddressHex(String address) throws Throwable {
//        return getClient().invoke(Commands.GET_HEX_ADDRESS, ArgsUtils.asList(address), String.class);
//    }
//
//    public BigDecimal usdtAmountFormate8(BigDecimal amount){
//        BigDecimal pow = BigDecimal.TEN.pow(8);
//        return amount.divide(pow);
//    }
//
//    public BigDecimal usdtAmountFormateN(BigDecimal amount, Integer n){
//        BigDecimal pow = BigDecimal.TEN.pow(n);
//        return amount.divide(pow);
//    }
//
//    /**
//     * 获取区块中所有的交易ID list
//     * @return
//     */
//    public List<String> getBlockTx(String blockHash) throws Throwable {
//        JSONObject res = getClient().invoke(Commands.GET_BLOCK, ArgsUtils.asList(blockHash), JSONObject.class);
//        if (res == null){
//            return null;
//        }
//        return res.getJSONArray("tx").toJavaList(String.class);
//    }
//
//
//    /**
//     * 根据交易Id获取交易信息
//     * @param txId
//     * @return
//     */
//    public TransactionRecord getTransaction(String txId, Integer height) throws Throwable {
//        String hex = getClient().invoke(Commands.GET_RAW_TRANSACTION, ArgsUtils.asList(txId), String.class);
//        JSONObject rawTransaction = getClient().invoke(Commands.DECODE_RAW_TRANSACTION, ArgsUtils.asList(hex), JSONObject.class);
//        if (rawTransaction == null){
//            return null;
//        }
//        TransactionRecord record = new TransactionRecord();
//        record.setHeight(height);
//        record.setCreateDate(new Date());
//        record.setConfirmed(0);
//        record.setHex(hex);
//        record.setTxId(rawTransaction.getString("txid"));
//        record.setType(TransactionTypeEnum.DEPOSIT.getCode());
//        getVin(record, rawTransaction);
//        getVout(record, rawTransaction);
//        return record;
//    }
//
//
//
//
//    /**
//     * 根据交易记录获取未被消费的UTXO
//     * @param record
//     * @return
//     */
//    public List<UTXO> getUnSpentUTXO(TransactionRecord record) {
//        List<UTXO> list = new ArrayList<>();
//        List<Vout> voutList = record.getVoutList();
//        for (Vout out : voutList){
//            UTXO utxo = getUtxo(record, out);
//            if (utxo != null)
//                list.add(utxo);
//        }
//        return list;
//    }
//
//    /**
//     * 根据交易记录获取已消费的UTXO
//     * @param record
//     * @return
//     */
//    public List<UTXO> getSpentUTXO(TransactionRecord record) {
//        List<UTXO> list = new ArrayList<>();
//        List<Vin> vinList = record.getVinList();
//        Date date = new Date();
//        for (Vin in : vinList){
//            UTXO utxo = new UTXO();
//            String address = in.getAddress();
//            utxo.setTxId(in.getTxId());
//            utxo.setAddress(address);
//            utxo.setScriptPubKey(in.getScriptPubKey());
//            utxo.setVout(in.getVout());
//            utxo.setHeight(record.getHeight());
//            utxo.setBlockDate(record.getBlockDate());
//            utxo.setCreateDate(date);
//            list.add(utxo);
//        }
//        return list;
//    }
//
//    public UTXO getUtxo(TransactionRecord record, Vout out){
//        String address = out.getAddress();
//        if (StringUtils.isEmpty(address))
//            return null;
//        Date date = new Date();
//        UTXO utxo = new UTXO();
//        utxo.setTxId(record.getTxId());
//        utxo.setAddress(out.getAddress());
//        utxo.setBlockDate(record.getBlockDate());
//        utxo.setVout(out.getVout());
//        utxo.setHeight(record.getHeight());
//        utxo.setScriptPubKey(out.getScriptPubKey());
//        utxo.setAmount(out.getValue());
//        utxo.setCreateDate(date);
//        utxo.setStatus(0);
//        return utxo;
//    }
//
//    private void getVin(TransactionRecord record, JSONObject rawTransaction){
//        JSONArray ins = rawTransaction.getJSONArray("vin");
//        List<Vin> list = new ArrayList<>();
//        for (int i = 0; i < ins.size(); i++){
//            JSONObject in = ins.getJSONObject(i);
//            Vin vin = new Vin();
//            vin.setTxId(in.getString("txid"));
//            vin.setVout(in.getInteger("vout"));
//            vin.setSequence(in.getString("sequence"));
//            ScriptPubkey sp = new ScriptPubkey();
//            JSONObject scriptSig = in.getJSONObject("scriptSig");
//            if (scriptSig != null){
//                sp.setAsm(scriptSig.getString("asm"));
//                sp.setHex(scriptSig.getString("hex"));
//            }
//            vin.setScriptPubKey(sp);
//            list.add(vin);
//        }
//        record.setVinList(list);
//    }
//
//    private void getVout(TransactionRecord record, JSONObject rawTransaction){
//        JSONArray outs = rawTransaction.getJSONArray("vout");
//        List<Vout> list = new ArrayList<>();
//        for (int i = 0; i < outs.size(); i ++){
//            try{
//                JSONObject out = outs.getJSONObject(i);
//                Vout vout = new Vout();
//                String value = out.getString("value");
//                String n = out.getString("n");
//                JSONObject scriptPubkey = out.getJSONObject("scriptPubKey");
//                ScriptPubkey sp = new ScriptPubkey();
//                String type = scriptPubkey.getString("type");
//                String asm = scriptPubkey.getString("asm");
//                if (OPTRTURN.matcher(asm).matches()){
//                    sp.setAsm(asm);
//                    sp.setHex(scriptPubkey.getString("hex"));
//                    sp.setType(type);
//                    vout.setScriptPubKey(sp);
//                    vout.setVout(Integer.valueOf(n));
//                    vout.setValue(new BigDecimal(value));
//                }
//                if (P2PKH.matcher(asm).matches()){
//                    JSONArray addresses = scriptPubkey.getJSONArray("addresses");
//                    if (addresses != null && !addresses.isEmpty()){
//                        vout.setAddress(addresses.getString(0));
//                    }
//                    sp.setAsm(asm);
//                    sp.setHex(scriptPubkey.getString("hex"));
//                    sp.setType(type);
//                    vout.setScriptPubKey(sp);
//                    vout.setValue(new BigDecimal(value));
//                    vout.setVout(Integer.valueOf(n));
//                }
//                list.add(vout);
//            } catch (Exception e){
//                logger.info("", e);
//            } catch (Throwable throwable) {
//                logger.error("", throwable);
//            }
//        }
//        record.setVoutList(list);
//    }
//
//    public List<String> getAddress(Integer num){
//        CreateAddressRequest request = new CreateAddressRequest();
//        request.setNum(num);
//        request.setPassword(CoinConstant.SIGN_PASSWORD);
//        try {
//            ResponseData<List<String>> newAddresses = signRemotService.getNewQtumAddresses(request);
//            if (newAddresses.isSuccessful()){
//                return newAddresses.getData();
//            }
//            throw new BusinessException(newAddresses.getCode());
//        } catch (Exception e){
//            logger.error("", e);
//        }
//        return new ArrayList<>();
//    }
//
//    public Address getAddress(String address){
//        Query query = new Query();
//        Criteria criteria = new Criteria();
//        criteria.and(MongoConstant.USDT_ADDRESS_CLASS_NAME_ADDRESS).is(address);
//        query.addCriteria(criteria);
//        List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.USDT_ADDRESS_CLASS_NAME);
//        return data.get(0);
//    }
}
