package com.zatgo.zup.transaction.entity.eth;

import java.util.Date;

/**
 * Created by 46041 on 2018/11/1.
 */
public class SendServiceChargeRecord {

    private String toAddress;

    private String fromAddress;

    private String coinType;

    private Date createDate;


    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
