package com.zatgo.zup.transaction.entity.constant;

/**
 * Created by 46041 on 2018/10/12.
 */
public class MongoConstant {

    /**
     *  utxo表
     */
    public static final String UTXO_CLASS_NAME = "utxo";

    public static final String UTXO_CLASS_NAME_ADDRESS = "address";

    public static final String UTXO_CLASS_NAME_CONFIRMED = "confirmed";

    public static final String UTXO_CLASS_NAME_STATUS = "status";

    public static final String UTXO_CLASS_NAME_TXID = "txId";

    public static final String UTXO_CLASS_NAME_VOUT = "vout";
    /**
     * addresses表
     */
    public static final String ADDRESS_CLASS_NAME = "addresses";

    public static final String ADDRESS_CLASS_NAME_UNCONFIRMED = "unconfirmed";

    public static final String ADDRESS_CLASS_NAME_CONFIRMED = "confirmed";

    public static final String ADDRESS_CLASS_TYPE = "type";
    /**
     * addresses表的address字段
     */
    public static final String ADDRESS_CLASS_NAME_ADDRESS = "address";

    public static final String MAIN_CHAIN_CLASS_NAME = "mainChain";

    /**
     * 交易
     */
    public static final String TRANSACTION_CLASS = "transaction";

    public static final String TRANSACTION_CLASS_RECORD_ID = "recordId";

    public static final String TRANSACTION_CLASS_TXID = "txId";

    public static final String TRANSACTION_CLASS_VIN_ADDRESS = "address";

    public static final String TRANSACTION_CLASS_CONFIRMED = "confirmed";


    //==================================================以上为btc==================================================


    //==================================================以下为eth==================================================

    /**
     * 交易
     */
    public static final String ETH_TRANSACTION_CLASS = "transaction";

    public static final String ETH_TRANSACTION_CLASS_TXID = "txId";

    public static final String ETH_TRANSACTION_CLASS_HEIGHT = "height";

    public static final String ETH_TRANSACTION_CLASS_TYPE = "type";

    public static final String ETH_TRANSACTION_CLASS_TOADDRESS = "toAddress";

    public static final String ETH_TRANSACTION_CLASS_RECORD_ID = "recordId";

    public static final String ETH_TRANSACTION_CLASS_FROMADDRESS = "fromAddress";

    public static final String ETH_TRANSACTION_CLASS_COINTYPE = "coinType";

    public static final String ETH_TRANSACTION_CLASS_CONFIRM = "confirm";

    public static final String ETH_TRANSACTION_CLASS_IS_USED = "isUsed";


    /**
     * 地址
     */
    public static final String ETH_ADDRESSES_CLASS = "addresses";

    public static final String ETH_ADDRESSES_TYPE = "type";

    public static final String ETH_ADDRESSES_CLASS_ADDRESS = "address";

    public static final String ETH_ADDRESS_CLASS_UNCONFIRMED = "unconfirmed";

    public static final String ETH_ADDRESS_CLASS_CONFIRMED = "confirmed";


    /**
     * 手续费
     */
//    public static final String ETH_SERVICE_CHARGE_CLASS = "serviceChargeRecord";
//
//    public static final String ETH_SERVICE_CHARGE_CLASS_TOADDRESS = "toAddress";
//
//    public static final String ETH_SERVICE_CHARGE_CLASS_FROMADDRESS = "fromAddress";
//
//    public static final String ETH_SERVICE_CHARGE_CLASS_COIN_TYPE = "coinType";

    /**
     *  搜集记录
     */
//    public static final String ETH_COLLECT_RECORD_CLASS = "collectRecord";
//
//    public static final String ETH_COLLECT_RECORD_CLASS_TO_ADDRESS = "toAddress";
//
//    public static final String ETH_COLLECT_RECORD_CLASS_FROM_ADDRESS = "fromAddress";
//
//    public static final String ETH_COLLECT_RECORD_CLASS_COIN_TYPE = "coinType";

    //==================================================以上为eth==================================================




    //==================================================以下为qtum==================================================

    /**
     *  MainChain
     */
    public static final String QTUM_MAIN_CHAIN_CLASS = "mainChain";

    public static final String QTUM_MAIN_CHAIN_CLASS_HEIGHT = "height";

    public static final String QTUM_MAIN_CHAIN_CLASS_HASH = "hash";

    /**
     * 交易
     */
    public static final String QTUM_TRANSACTION_CLASS = "transaction";

    public static final String QTUM_TRANSACTION_CLASS_RECORD_ID = "recordId";

    public static final String QTUM_TRANSACTION_CLASS_TXID = "txId";

    public static final String QTUM_TRANSACTION_CLASS_VIN_ADDRESS = "address";

    public static final String QTUM_TRANSACTION_CLASS_CONFIRMED = "confirmed";

    public static final String QTUM_TRANSACTION_CLASS_HEIGHT = "height";

    public static final String QTUM_TRANSACTION_CLASS_FROMADDRESS = "fromAddress";

    public static final String QTUM_TRANSACTION_CLASS_TYPE = "type";

    public static final String QTUM_TRANSACTION_CLASS_VOUTLIST = "voutList";

    public static final String QTUM_TRANSACTION_CLASS_TOADDRESS = "toAddress";

    public static final String QTUM_TRANSACTION_CLASS_CONFIRM = "confirm";

    public static final String QTUM_TRANSACTION_CLASS_IS_USED = "isUsed";

    /**
     *  utxo表
     */
    public static final String QTUM_UTXO_CLASS_NAME = "utxo";

    public static final String QTUM_UTXO_CLASS_NAME_ADDRESS = "address";

    public static final String QTUM_UTXO_CLASS_NAME_CONFIRMED = "confirmed";

    public static final String QTUM_UTXO_CLASS_NAME_STATUS = "status";

    public static final String QTUM_UTXO_CLASS_NAME_TXID = "txId";

    public static final String QTUM_UTXO_CLASS_NAME_VOUT = "vout";
    /**
     * addresses表
     */
    public static final String QTUM_ADDRESS_CLASS_NAME = "addresses";

    public static final String QTUM_ADDRESS_CLASS_NAME_ADDRESS = "address";

    public static final String QTUM_ADDRESS_CLASS_NAME_UNCONFIRMED = "unconfirmed";

    public static final String QTUM_ADDRESS_CLASS_NAME_CONFIRMED = "confirmed";

    public static final String QTUM_ADDRESS_CLASS_TYPE = "type";

    /**
     *  搜集记录
     */
    public static final String QTUM_COLLECT_RECORD_CLASS = "collectRecord";

    public static final String QTUM_COLLECT_RECORD_CLASS_TO_ADDRESS = "toAddress";

    public static final String QTUM_COLLECT_RECORD_CLASS_FROM_ADDRESS = "fromAddress";

    public static final String QTUM_COLLECT_RECORD_CLASS_COIN_TYPE = "coinType";

    /**
     * 手续费
     */
    public static final String QTUM_SERVICE_CHARGE_CLASS = "serviceChargeRecord";

    public static final String QTUM_SERVICE_CHARGE_CLASS_TOADDRESS = "toAddress";

    public static final String QTUM_SERVICE_CHARGE_CLASS_FROMADDRESS = "fromAddress";

    public static final String QTUM_SERVICE_CHARGE_CLASS_COIN_TYPE = "coinType";




    //==================================================以上为qtum==================================================


    //==================================================以下为usdt==================================================

    /**
     *  utxo表
     */
    public static final String USDT_UTXO_CLASS_NAME = "utxo";

    public static final String USDT_UTXO_CLASS_NAME_ADDRESS = "address";

    public static final String USDT_UTXO_CLASS_NAME_CONFIRMED = "confirmed";

    public static final String USDT_UTXO_CLASS_NAME_STATUS = "status";

    public static final String USDT_UTXO_CLASS_NAME_TXID = "txId";

    public static final String USDT_UTXO_CLASS_NAME_VOUT = "vout";
    /**
     * addresses表
     */
    public static final String USDT_ADDRESS_CLASS_NAME = "addresses";

    public static final String USDT_ADDRESS_CLASS_NAME_UNCONFIRMED = "unconfirmed";

    public static final String USDT_ADDRESS_CLASS_NAME_CONFIRMED = "confirmed";

    public static final String USDT_ADDRESS_CLASS_TYPE = "type";
    /**
     * addresses表的address字段
     */
    public static final String USDT_ADDRESS_CLASS_NAME_ADDRESS = "address";

    public static final String USDT_MAIN_CHAIN_CLASS_NAME = "mainChain";

    /**
     * 交易
     */
    public static final String USDT_TRANSACTION_CLASS = "transaction";

    public static final String USDT_TRANSACTION_CLASS_RECORD_ID = "recordId";

    public static final String USDT_TRANSACTION_CLASS_TXID = "txId";

    public static final String USDT_TRANSACTION_CLASS_VIN_ADDRESS = "address";

    public static final String USDT_TRANSACTION_CLASS_CONFIRMED = "confirmed";

    //==================================================以上为usdt==================================================
}
