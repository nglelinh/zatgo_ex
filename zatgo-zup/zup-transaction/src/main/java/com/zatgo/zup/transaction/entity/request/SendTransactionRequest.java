package com.zatgo.zup.transaction.entity.request;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2018/10/18.
 */
public class SendTransactionRequest {

    private String toAddress;

    private String fromAddress;

    private BigDecimal amount;

    private String recordId;


    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }
}
