package com.zatgo.zup.transaction.listener;

import com.zatgo.zup.transaction.service.CoinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 46041 on 2018/11/5.
 */
public class AddAddressListener implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(AddAddressListener.class);

    private CoinService coinService;

    @Override
    public void run() {
        while (true){
            try {
                coinService.addAddress();
                Thread.sleep(60 * 1000l);
            } catch (Exception e){
                logger.error("", e);
                try {
                    Thread.sleep(60 * 1000l);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public AddAddressListener(CoinService coinService) {
        this.coinService = coinService;
    }

    public CoinService getCoinService() {
        return coinService;
    }

    public void setCoinService(CoinService coinService) {
        this.coinService = coinService;
    }
}
