package com.zatgo.zup.exchange.strategy.huobi.model;

public class HuobiOrderMatchResultsReq extends HuobiReq {

	public HuobiOrderMatchResultsReq() {
		super();
	}

	public HuobiOrderMatchResultsReq(String url, String accessKeyId, String secretKey, String reqMethod, String method)
			throws Exception {
		super(accessKeyId, secretKey, reqMethod, method);
		sign(this, url);
	}

}
