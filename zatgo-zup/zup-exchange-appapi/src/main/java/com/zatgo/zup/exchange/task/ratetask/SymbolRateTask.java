package com.zatgo.zup.exchange.task.ratetask;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.exchange.appapi.model.RateData;

@Component
public class SymbolRateTask {

	private static final Logger log = LoggerFactory.getLogger(SymbolRateTask.class);

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private OkHttpService httpservice;

	public void updateSymbolRateToRedis() {

		List<String> virtualCoin = Arrays.asList("btc", "usdt", "eth");
		List<String> legalCoin = Arrays.asList("cny");
		RateData rateData = findSymbolRate(virtualCoin,legalCoin);

		ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
		valueOperations.set(RedisKeyConstants.COIN_RATE, JSON.toJSONString(rateData));
		
	}
	
	public RateData findSymbolRate(List<String> virtualCoin,List<String> legalCoin) {
		

		Map<String, Map<String, BigDecimal>> rateMap = new HashMap<String, Map<String, BigDecimal>>();

		for (String legal : legalCoin) {
			Map<String, BigDecimal> virtualRateMap = new HashMap<String, BigDecimal>();
			for (String virtual : virtualCoin) {
				String symbol = virtual + legal;
				String res = httpservice
						.doGet("http://rate.chainup.com/api/symbol/rate?token=1a9794c76b6d4d82bbe93241619618d6&symbol="
								+ symbol);
				if (StringUtils.isEmpty(res)) {
					continue;
				}

				BigDecimal rate = JSON.parseObject(res).getBigDecimal("rate");
				virtualRateMap.put(virtual, rate);
			}
			rateMap.put(legal, virtualRateMap);
		}

		RateData rateData = new RateData();
		rateData.setRates(rateMap);
		
		return rateData;
	}

}
