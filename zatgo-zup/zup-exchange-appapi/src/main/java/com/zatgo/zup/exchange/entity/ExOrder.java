package com.zatgo.zup.exchange.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ExOrder {
    private String orderId;

    private String userId;

    private String externalOrderId;

    private String externalExchangeType;

    private String side;

    private BigDecimal price;

    private BigDecimal volume;

    private BigDecimal feeRateMaker;

    private BigDecimal feeRateTaker;

    private BigDecimal fee;

    private BigDecimal dealVolume;

    private BigDecimal dealMoney;

    private BigDecimal avgPrice;

    private Byte status;

    private Byte type;

    private Date createDate;

    private Date updateDate;

    private Byte source;

    private String baseCoin;

    private String quoteCoin;

    private String coinPairSymbol;

    private BigDecimal makerFeeIncrease;

    private BigDecimal takerFeeIncrease;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getExternalOrderId() {
        return externalOrderId;
    }

    public void setExternalOrderId(String externalOrderId) {
        this.externalOrderId = externalOrderId == null ? null : externalOrderId.trim();
    }

    public String getExternalExchangeType() {
        return externalExchangeType;
    }

    public void setExternalExchangeType(String externalExchangeType) {
        this.externalExchangeType = externalExchangeType == null ? null : externalExchangeType.trim();
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side == null ? null : side.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public BigDecimal getFeeRateMaker() {
        return feeRateMaker;
    }

    public void setFeeRateMaker(BigDecimal feeRateMaker) {
        this.feeRateMaker = feeRateMaker;
    }

    public BigDecimal getFeeRateTaker() {
        return feeRateTaker;
    }

    public void setFeeRateTaker(BigDecimal feeRateTaker) {
        this.feeRateTaker = feeRateTaker;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getDealVolume() {
        return dealVolume;
    }

    public void setDealVolume(BigDecimal dealVolume) {
        this.dealVolume = dealVolume;
    }

    public BigDecimal getDealMoney() {
        return dealMoney;
    }

    public void setDealMoney(BigDecimal dealMoney) {
        this.dealMoney = dealMoney;
    }

    public BigDecimal getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(BigDecimal avgPrice) {
        this.avgPrice = avgPrice;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Byte getSource() {
        return source;
    }

    public void setSource(Byte source) {
        this.source = source;
    }

    public String getBaseCoin() {
        return baseCoin;
    }

    public void setBaseCoin(String baseCoin) {
        this.baseCoin = baseCoin == null ? null : baseCoin.trim();
    }

    public String getQuoteCoin() {
        return quoteCoin;
    }

    public void setQuoteCoin(String quoteCoin) {
        this.quoteCoin = quoteCoin == null ? null : quoteCoin.trim();
    }

    public String getCoinPairSymbol() {
        return coinPairSymbol;
    }

    public void setCoinPairSymbol(String coinPairSymbol) {
        this.coinPairSymbol = coinPairSymbol == null ? null : coinPairSymbol.trim();
    }

    public BigDecimal getMakerFeeIncrease() {
        return makerFeeIncrease;
    }

    public void setMakerFeeIncrease(BigDecimal makerFeeIncrease) {
        this.makerFeeIncrease = makerFeeIncrease;
    }

    public BigDecimal getTakerFeeIncrease() {
        return takerFeeIncrease;
    }

    public void setTakerFeeIncrease(BigDecimal takerFeeIncrease) {
        this.takerFeeIncrease = takerFeeIncrease;
    }
}