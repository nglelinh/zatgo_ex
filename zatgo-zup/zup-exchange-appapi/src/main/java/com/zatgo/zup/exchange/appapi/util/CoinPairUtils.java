package com.zatgo.zup.exchange.appapi.util;

import java.util.List;

import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeQuoteCoin;

public class CoinPairUtils {

	public static String getQuoteCoin(String symbol) {
		List<String> coins = ExchangeQuoteCoin.getCoins();
		for (String coin : coins) {
			if (symbol.endsWith(coin)) {
				return coin;
			}
		}

		return null;
	}
	
	public static String[] getSymbol(String symbol) {
		String quoteCoin = getQuoteCoin(symbol);
		String baseCoin = symbol.substring(0, symbol.length() - quoteCoin.length());
		String[] coins = new String[2];
		coins[0] = baseCoin;
		coins[1] = quoteCoin;
		return coins;
	}
}
