package com.zatgo.zup.exchange.strategy.bytex.model;

import com.zatgo.zup.exchange.entity.CoinPairConfig;

public class BytexPollingReq {

	private String event;
	
	private String buisType;
	
	private CoinPairConfig coinPairConfig;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getBuisType() {
		return buisType;
	}

	public void setBuisType(String buisType) {
		this.buisType = buisType;
	}

	public CoinPairConfig getCoinPairConfig() {
		return coinPairConfig;
	}

	public void setCoinPairConfig(CoinPairConfig coinPairConfig) {
		this.coinPairConfig = coinPairConfig;
	}
	
	
}
