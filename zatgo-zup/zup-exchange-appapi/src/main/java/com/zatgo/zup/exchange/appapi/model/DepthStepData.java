package com.zatgo.zup.exchange.appapi.model;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("盘口深度数据")
public class DepthStepData {
	
	@ApiModelProperty(value = "卖盘深度", required = true)
	private List<DepthData> asks;
	
	@ApiModelProperty(value = "买盘深度", required = true)
	private List<DepthData> bids;

	public List<DepthData> getAsks() {
		return asks;
	}

	public void setAsks(List<DepthData> asks) {
		this.asks = asks;
	}

	public List<DepthData> getBids() {
		return bids;
	}

	public void setBids(List<DepthData> bids) {
		this.bids = bids;
	}

}
