package com.zatgo.zup.exchange.appapi.model;

import java.math.BigDecimal;
import java.util.Map;

import io.swagger.annotations.ApiModelProperty;

public class RateData {

	@ApiModelProperty(value = "法币对应的汇率",required =true )
	private Map<String, Map<String, BigDecimal>> rates;

	public Map<String, Map<String, BigDecimal>> getRates() {
		return rates;
	}

	public void setRates(Map<String, Map<String, BigDecimal>> rates) {
		this.rates = rates;
	}
	
}
