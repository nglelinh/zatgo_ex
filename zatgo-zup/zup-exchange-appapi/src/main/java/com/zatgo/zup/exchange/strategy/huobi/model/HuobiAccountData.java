package com.zatgo.zup.exchange.strategy.huobi.model;

public class HuobiAccountData {
	
	/**
	 * account-id
	 */
	private Long id;
	
	/**
	 * working：正常, lock：账户被锁定
	 */
	private String state;
	
	/**
	 * spot：现货账户， margin：杠杆账户，otc：OTC账户，point：点卡账户
	 */
	private String type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
