package com.zatgo.zup.exchange.strategy.bytex.model;

public class BytexWsReq {
	
	private String event;
	
	private BytexWsParams params;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public BytexWsParams getParams() {
		return params;
	}

	public void setParams(BytexWsParams params) {
		this.params = params;
	}

}
