package com.zatgo.zup.exchange.strategy.huobi.model;

import java.math.BigDecimal;

import com.alibaba.fastjson.annotation.JSONField;

public class HuobiSymbolData {
	
	/**
	 * 基础币
	 */
	@JSONField(name = "base-currency")
	private String baseCurrency;
	
	/**
	 * 计价币
	 */
	@JSONField(name = "quote-currency")
	private String quoteCurrency;
	
	/**
	 * 价格精度
	 */
	@JSONField(name = "price-precision")
	private BigDecimal pricePrecision;
	
	/**
	 * 数量精度
	 */
	@JSONField(name = "amount-precision")
	private BigDecimal amountPrecision;
	
	/**
	 * 币对平台
	 */
	@JSONField(name = "symbol-partition")
	private String symbolPartition;
	
	/**
	 * 币对
	 */
	private String symbol;

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getQuoteCurrency() {
		return quoteCurrency;
	}

	public void setQuoteCurrency(String quoteCurrency) {
		this.quoteCurrency = quoteCurrency;
	}

	public BigDecimal getPricePrecision() {
		return pricePrecision;
	}

	public void setPricePrecision(BigDecimal pricePrecision) {
		this.pricePrecision = pricePrecision;
	}

	public BigDecimal getAmountPrecision() {
		return amountPrecision;
	}

	public void setAmountPrecision(BigDecimal amountPrecision) {
		this.amountPrecision = amountPrecision;
	}

	public String getSymbolPartition() {
		return symbolPartition;
	}

	public void setSymbolPartition(String symbolPartition) {
		this.symbolPartition = symbolPartition;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

}
