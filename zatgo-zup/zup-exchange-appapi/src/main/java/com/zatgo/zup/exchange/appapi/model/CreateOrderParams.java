package com.zatgo.zup.exchange.appapi.model;

import java.math.BigDecimal;

import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "创建订单入参")
public class CreateOrderParams extends BaseExchangeParams {

	@ApiModelProperty(value = "市场标记，如：ethbtc",required =true )
	private String symbol;
	
	@ApiModelProperty(value = "基准货币",required =true )
	private String baseCoin;
	
	@ApiModelProperty(value = "计价货币",required =true )
	private String quoteCoin;
	
	@ApiModelProperty(value = "购买数量（多义，复用字段）\r\n" + 
			"type=1:表示买卖数量\r\n" + 
			"type=2:买则表示总价格，卖表示总个数",required =true )
	private BigDecimal volume;
	
	@ApiModelProperty(value = "挂单类型：limitPriceEntrust限价单-1；marketPriceEntrust市价单-2",required =true, allowableValues = "1,2")
	private EntryOrdersType type;
	
	@ApiModelProperty(value = "买卖方向",required =true )
	private ExchangeSide side;
	
	@ApiModelProperty(value = "委托单价：type=2：不需要此参数",required =false )
	private BigDecimal price;
	
	@ApiModelProperty(value = "当交易所有平台币时，此参数表示是否使用用平台币支付手续费",required =false )
	private Boolean feeIsUserPlatformCoin;

	public String getSymbol() {
		return symbol;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public EntryOrdersType getType() {
		return type;
	}

	public void setType(String type) {
		this.type = EntryOrdersType.getEnumByType(type);
	}

	public ExchangeSide getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = ExchangeSide.getEnumByType(side.toUpperCase());
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getFeeIsUserPlatformCoin() {
		return feeIsUserPlatformCoin;
	}

	public void setFeeIsUserPlatformCoin(Boolean feeIsUserPlatformCoin) {
		this.feeIsUserPlatformCoin = feeIsUserPlatformCoin;
	}

}
