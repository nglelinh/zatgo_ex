package com.zatgo.zup.exchange.strategy.bytex.model;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class BytexOrderTradeData {
	
	private Long id;
	
	@JSONField(name = "ctime")
	private Date ctime;
	
	private BigDecimal price;
	
	private BigDecimal volume;
	
	@JSONField(name = "deal_price")
	private BigDecimal dealPrice;
	
	private String type;
	
	private BigDecimal fee;
	
	private String feeCoin;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getDealPrice() {
		return dealPrice;
	}

	public void setDealPrice(BigDecimal dealPrice) {
		this.dealPrice = dealPrice;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getFeeCoin() {
		return feeCoin;
	}

	public void setFeeCoin(String feeCoin) {
		this.feeCoin = feeCoin;
	}

}
