package com.zatgo.zup.exchange.appapi.listener;

import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.ExOrderDetailData;
import com.zatgo.zup.exchange.business.OrderService;
import com.zatgo.zup.exchange.entity.ExOrder;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.OrderStrategyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
public class SyncOrderDataListener {
	
	private static final Logger log = LoggerFactory.getLogger(SyncOrderDataListener.class);

	private static ExecutorService executor = Executors.newFixedThreadPool(10);

	@Autowired
	private ExchangeStrategy exchangeStrategyService;

	@Autowired
	private OrderService orderService;

	@PostConstruct
	private void init() {
		// 订单状态查询
		Runnable orderSync = new Runnable() {
			@Override
			public void run() {
				while (true) {
					Boolean pageEnd = false;
					int pageNo = 1;
					int pageSize = 1000;

					while (!pageEnd) {
						PageInfo<ExOrder> pageInfo = orderService.findUnfinishedByPage(pageNo, pageSize);

						for (ExOrder order : pageInfo.getList()) {
							OrderStrategyService orderStrategyService = exchangeStrategyService.getOrderService(
									ExchangeEnum.ExchangeType.getEnumByType(order.getExternalExchangeType()));
							try {
								ExOrderDetailData newOrderData = orderStrategyService.getOrderByOrderId(order);

								if (newOrderData == null) {
									// 同步订单状态失败
									orderStrategyService.dealExceptionOrder(order);
									continue;
								}

								orderService.syncOrder(newOrderData);
							} catch (Exception e) {
								log.error("同步订单处理失败", e);
							}
						}

						pageNo = pageNo + 1;

						if (pageInfo.getList().size() == 0) {
							pageEnd = true;
						}
					}

					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}

			}
		};
		executor.execute(orderSync);
	}
}
