package com.zatgo.zup.exchange.appapi.model;

import org.apache.commons.lang3.StringUtils;

import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;

import io.swagger.annotations.ApiModelProperty;

public class ReqWebsocketParams extends BaseExchangeParams {

	@ApiModelProperty(value = "websocket event",required =true )
	private WebSocketEvent event;
	
	@ApiModelProperty(value = "币对，市场标识",required =true )
	private String symbol;
	
	@ApiModelProperty(value = "基准货币",required =true )
	private String baseCoin;
	
	@ApiModelProperty(value = "计价货币",required =true )
	private String quoteCoin;
	
	@ApiModelProperty(value = "业务类型",required =true )
	private WebSocketBusiType type;
	
	@ApiModelProperty(value = "type=KLINE时,取值范围[1min,5min,15min,30min,60min,1day,1week,1month]",required =false )
	private String klineTime;
	
	@ApiModelProperty(value = "type=DEPTH_STEP时,取值范围[0-2]",required =false )
	private String depthStep;
	
	@ApiModelProperty(value = "用户登入的token",required =true )
	private String token;
	
	private String cloudUserId;

	public String getCloudUserId() {
		return cloudUserId;
	}

	public void setCloudUserId(String cloudUserId) {
		this.cloudUserId = cloudUserId;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public WebSocketEvent getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = WebSocketEvent.getEnumByType(event.toLowerCase());
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public WebSocketBusiType getType() {
		return type;
	}

	public void setType(String type) {
		this.type = WebSocketBusiType.getEnumByType(type);
	}

	public String getKlineTime() {
		return klineTime;
	}

	public void setKlineTime(String klineTime) {
		this.klineTime = klineTime;
	}

	public String getDepthStep() {
		return depthStep;
	}

	public void setDepthStep(String depthStep) {
		this.depthStep = depthStep;
	}
	
	/**
	 * 获取订阅的通道
	 * @return
	 */
	public String getChannel() {
		String type = getType().getCode();
		String channel = type;
		if (WebSocketBusiType.KLINE.getCode().equalsIgnoreCase(type)) {
			channel = channel + getSymbol().toLowerCase() + "_" + getKlineTime().trim();
		} else if (WebSocketBusiType.TICKER.getCode().equalsIgnoreCase(type)) {
			channel = channel + getSymbol().toLowerCase();
		} else if (WebSocketBusiType.TRADE_TICKER.getCode().equalsIgnoreCase(type)) {
			channel = channel + getSymbol().toLowerCase();
		} else if (WebSocketBusiType.DEPTH_STEP.getCode().equalsIgnoreCase(type)) {
			channel = channel + getSymbol().toLowerCase() + "_" + getDepthStep().trim();
		} else if (WebSocketBusiType.MARKET.getCode().equalsIgnoreCase(type)
				&& StringUtils.isNotEmpty(getQuoteCoin())) {
			channel = channel + getQuoteCoin().toLowerCase();
		}
		ExchangeType exchangeType = getExchangeType();
		if (exchangeType != null) {
			channel = channel + "_" + getExchangeType().getCode();
		}
		if(event.equals(ExchangeEnum.WebSocketEvent.REQ)) {
			channel += "_" + WebSocketEvent.REQ.getCode();
		}
		return channel;
	}
	
}
