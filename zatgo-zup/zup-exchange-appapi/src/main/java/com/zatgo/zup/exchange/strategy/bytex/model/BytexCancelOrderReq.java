package com.zatgo.zup.exchange.strategy.bytex.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.entity.ExOrder;

public class BytexCancelOrderReq extends BytexReq {

	@JSONField(name = "order_id")
	private Long order_id;

	private String symbol;

	public BytexCancelOrderReq() {
		super();
	}
	
	public BytexCancelOrderReq(String apiKey, String secretKey, CancelOrderParams params) {
		this.order_id = Long.valueOf(params.getOrderId());
		this.symbol = params.getSymbol().toLowerCase();
		sign(apiKey, secretKey, this);
	}

	public BytexCancelOrderReq(String apiKey, String secretKey, ExOrder order) {
		this.order_id = Long.valueOf(order.getExternalOrderId());
		this.symbol = order.getCoinPairSymbol().toLowerCase();
		sign(apiKey, secretKey, this);
	}

	public Long getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

}
