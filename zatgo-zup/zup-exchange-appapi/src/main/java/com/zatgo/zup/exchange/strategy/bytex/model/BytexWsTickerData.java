package com.zatgo.zup.exchange.strategy.bytex.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("bytex 24小时行情")
public class BytexWsTickerData {
	
	@ApiModelProperty(value = "冗余，无实际意义，时间戳", required = true)
	private Long id;
	
	@ApiModelProperty(value = "交易额", required = true)
	private BigDecimal amount;
	
	@ApiModelProperty(value = "交易量", required = true)
	private BigDecimal vol;
	
	@ApiModelProperty(value = "开盘价", required = true)
	private BigDecimal open;
	
	@ApiModelProperty(value = "收盘价", required = true)
	private BigDecimal close;
	
	@ApiModelProperty(value = "最高价", required = true)
	private BigDecimal high;
	
	@ApiModelProperty(value = "最低价", required = true)
	private BigDecimal low;
	
	@ApiModelProperty(value = "涨幅", required = true)
	private BigDecimal rose;
	
	@ApiModelProperty(value = "数据产生时间", required = true)
	private Long ts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getVol() {
		return vol;
	}

	public void setVol(BigDecimal vol) {
		this.vol = vol;
	}

	public BigDecimal getOpen() {
		return open;
	}

	public void setOpen(BigDecimal open) {
		this.open = open;
	}

	public BigDecimal getClose() {
		return close;
	}

	public void setClose(BigDecimal close) {
		this.close = close;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public BigDecimal getRose() {
		return rose;
	}

	public void setRose(BigDecimal rose) {
		this.rose = rose;
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

}
