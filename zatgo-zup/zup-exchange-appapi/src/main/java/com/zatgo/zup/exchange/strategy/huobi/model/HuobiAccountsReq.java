package com.zatgo.zup.exchange.strategy.huobi.model;

public class HuobiAccountsReq extends HuobiReq {

	public HuobiAccountsReq() {
		super();
	}

	public HuobiAccountsReq(String accessKeyId, String secretKey, String reqMethod, String url, String method)
			throws Exception {
		super(accessKeyId, secretKey, reqMethod, method);
		sign(this, url);
	}

}
