package com.zatgo.zup.exchange.appapi.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketOperateType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.exchange.appapi.controller.ApiWebSocketController;
import com.zatgo.zup.exchange.appapi.model.CloudUserCoinPairConfigData;
import com.zatgo.zup.exchange.appapi.model.CoinPair;
import com.zatgo.zup.exchange.appapi.model.CoinPairData;
import com.zatgo.zup.exchange.appapi.model.ReqWebsocketParams;
import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
import com.zatgo.zup.exchange.appapi.model.TickerData;
import com.zatgo.zup.exchange.appapi.util.WebSocketClientUtil;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.WebSocketClientService;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;

@Component
public class SubscriptionClientListener {
	
	private static final Logger logger = LoggerFactory.getLogger(SubscriptionClientListener.class);

	@Autowired
	private CommonService commonService;
	
	@Autowired
	private ExchangeStrategy exchangeStrategyService;
	
	@Autowired
	private RedisTemplate redisTemplate;
	
	@Autowired
	private ApiWebSocketController apiWebSocketController;

	@PostConstruct
	public void init() {

		//启动与交易所websocket的连接，并检查健康情况
		Runnable createWebSocketClient = new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						List<String> exchangeTypes = commonService.getExchangeType();
						exchangeTypes.forEach(exchangeType -> {
							try {
								createWebSocketClient(exchangeType);
							} catch (Exception e) {
								logger.error("创建WebSocket失败：exchangeType=" + exchangeType, e);
							}
							
						});

						logger.info("websocket连接后端交易所检查完成");
						TimeUnit.SECONDS.sleep(5);
					} catch (Exception e) {
						logger.error("创建WebSocket失败：", e);
					} catch(Throwable e) {
						logger.error("创建WebSocket失败：", e);
					}
				}
			}
		};

		Thread thread = new Thread(createWebSocketClient);
		thread.setDaemon(true);
		thread.start();
		
		
		// 推送-本地模式-首页市场币对的最新价和涨幅情况
		Runnable market = new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {

						pushLocalMarketData(WebSocketEvent.SUB);

						//pushDirectMarketData(WebSocketEvent.SUB);

						TimeUnit.SECONDS.sleep(5);
					} catch (InterruptedException e) {
						logger.error("订阅-首页市场币对的最新价和涨幅情况线程等待失败：", e);
					} catch (Exception e) {
						logger.error("订阅-首页市场币对的最新价和涨幅情况失败：", e);
					}
					
					logger.info("推送-本地模式-首页市场行情到客户端完成");
				}

			}
		};
		
		Thread marketThread = new Thread(market);
		marketThread.setDaemon(true);
		marketThread.start();
		
		
	}
	
	private WebSocketClientService createWebSocketClient(String exchangeType) {

		WebSocketClientService client = WebSocketClientUtil.getWebSocketClient(exchangeType);
		if(client != null && client.isOpen()) {
			return client;
		}

		ExchangeType type = ExchangeType.getEnumByType(exchangeType);
		WebSocketStrategyService service = exchangeStrategyService.getApiWebSocketService(type);
		
		WebSocketClientUtil.removeWebSocketClient(type.getCode());
		client = service.createWebSocketClient();
		client.connect();
		while (!client.getReadyState().equals(WebSocketClientService.ZUP_READYSTATE.OPEN)) {
			if(client.getReadyState().equals(WebSocketClientService.ZUP_READYSTATE.CONNECTING)
					|| client.getReadyState().equals(WebSocketClientService.ZUP_READYSTATE.NOT_YET_CONNECTED)) {
				logger.info(type.getCode() + "ws连接中");
			}else if(!client.getReadyState().equals(WebSocketClientService.ZUP_READYSTATE.OPEN)){
				throw new BusinessException(type.getCode(), "ws连接失败，status=" + client.getReadyState());
			}
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				logger.error("",e);
			}
			
		}
		logger.info("connect结束：" + System.currentTimeMillis());
		WebSocketClientUtil.addWebSocketClient(exchangeType, client);
		
		//订阅交易所的市场行情
		ReqWebsocketParams params = new ReqWebsocketParams();
		params.setEvent(WebSocketEvent.SUB.getCode());
		params.setExchangeType(exchangeType);
		params.setType(WebSocketBusiType.MARKET.getCode());
		sendMessage(params);

		return client;
	}

	
	public void sendMessage(ReqWebsocketParams params) {

		ExchangeEnum.ExchangeType type = params.getExchangeType();
		
		logger.debug("sendMessage开始：" + System.currentTimeMillis());
		if (params.getEvent().equals(WebSocketEvent.UNSUB)) {
			logger.debug("UNSUB unopened");
			return;
		}
		
		//如果是本地模式，则通过币对获取对应的交易所
		if (type == null) {
			String symbol = params.getSymbol();
			if (StringUtils.isNotEmpty(symbol)) {
				CloudUserCoinPairConfigData config = commonService.getCloudUserCoinPairCache(symbol, params.getCloudUserId());
				type = ExchangeType.getEnumByType(config.getExternalExchangeType());
			}
		}
		
		if (type == null) {
			logger.debug("ExchangeType is null");
			return;
		}

		WebSocketStrategyService service = exchangeStrategyService.getApiWebSocketService(type);

		String massage = service.getWebsocketSendMassage(params);
		if (StringUtils.isEmpty(massage)) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		WebSocketClientService client = WebSocketClientUtil.getWebSocketClient(type.getCode());
		
		if(client == null) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "webSocketClient=" + type.getCode() + "未启动");
		}

		client.send(massage);
	}
	
	public List<CoinPairConfig> transformCoinPairDataToCoinPairConfig(CoinPairData data){
		List<CoinPairConfig> configs = new ArrayList<>();
		
		Map<String, List<CoinPair>> pair = data.getPairs();
		if(MapUtils.isEmpty(pair)) {
			return configs;
		}
		
		
		Collection<List<CoinPair>> values = pair.values();
		
		for (List<CoinPair> coin : values) {
			
			if(CollectionUtils.isEmpty(coin)) {
				continue;
			}
			
			for (CoinPair coinPair : coin) {
				CoinPairConfig config = new CoinPairConfig();
				BeanUtils.copyProperties(coinPair, config);
				configs.add(config);
			}
		}
		
		return configs;
	}
	
	
	/**
	 * 推送-本地模式-市场行情
	 * 
	 */
	public void pushLocalMarketData(WebSocketEvent event) {

		Map<String,Map<String, List<TickerData>>> allData = getLocalMarketDataMap();
		if (MapUtils.isEmpty(allData)) {
			return;
		}

		allData.forEach((key, value) -> {
			Map<String,Map<String, List<TickerData>>> data = new HashMap<>();
			data.put(key, value);
			sendMarket(key, data, null,false,event);
		});

		sendMarket(null, allData, null,false,event);
	}

	

//	/**
//	 * 推送-直连模式-市场行情
//	 */
//	public void pushDirectMarketData(WebSocketEvent event) {
//		Set<ExchangeType> exchangeTypeSet = WebSocketSessionUtil.getDirectExchangeSet();
//		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
//			return;
//		}
//
//		for (ExchangeType exchangeType : exchangeTypeSet) {
//
//			Map<String,Map<String, List<TickerData>>> allData = getMarketDataMap(exchangeType);
//			if (MapUtils.isEmpty(allData)) {
//				return;
//			}
//
//			allData.forEach((key, value) -> {
//				Map<String,Map<String, List<TickerData>>> data = new HashMap<>();
//				data.put(key.toLowerCase(), value);
//				sendMarket(key.toLowerCase(), data, exchangeType,true,event);
//			});
//
//			sendMarket(null, allData, exchangeType,true,event);
//		}
//	}
	
//	/**
//	 * 推送-指定交易所-市场行情
//	 */
//	public void pushDirectMarketData(ExchangeType exchangeType,WebSocketEvent event) {
//
//		Map<String,Map<String, List<TickerData>>> allData = getMarketDataMap(exchangeType);
//		if (MapUtils.isEmpty(allData)) {
//			return;
//		}
//
//		allData.forEach((key, value) -> {
//			Map<String,Map<String, List<TickerData>>> data = new HashMap<>();
//			data.put(key.toLowerCase(), value);
//			sendMarket(key.toLowerCase(), data, exchangeType,true,event);
//		});
//
//		sendMarket(null, allData, exchangeType,true,event);
//
//	}

	
	/**
	 * 获取市场行情缓存
	 * @param configs
	 * @return
	 */
	public Map<String,Map<String, List<TickerData>>> getLocalMarketDataMap() {
		Map<String,Map<String, List<TickerData>>> allData = new HashMap<String,Map<String, List<TickerData>>>();
		
		List<CoinPairConfig> configs = commonService.getCoinPairCache();
		if (CollectionUtils.isEmpty(configs)) {
			return allData;
		}
		
		Set<String> quoteCoin = new HashSet<>();
		Set<String> exchangeTypeSet = new HashSet<>();
		configs.forEach(config -> {
			quoteCoin.add(config.getQuoteCoin());
			String[] exchangeTypes = config.getExternalExchangeTypes() == null?new String[0]:config.getExternalExchangeTypes().split(",");
			for(String exchangeType:exchangeTypes) {
				exchangeTypeSet.add(exchangeType);
			}
		});

		HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
		for (String coin : quoteCoin) {
			Map<String, List<TickerData>> resMap = new HashMap<>();
			for(String type:exchangeTypeSet) {
				List<String> tickerDatas = hashOperations.values(RedisKeyConstants.COIN_TICKER + type + "_" + coin + "_");
				if (CollectionUtils.isEmpty(tickerDatas)) {
					continue;
				}

				List<TickerData> res = resMap.get(type);
				if(res == null) {
					res = new ArrayList<TickerData>();
					resMap.put(type, res);
				}
						
				for (String _data : tickerDatas) {
					res.add(JSON.parseObject(_data, TickerData.class));
				}

				allData.put(coin, resMap);
			}
			
		}

		return allData;
	}
	
//	/**
//	 * 获取市场行情缓存
//	 * @param exchangeType
//	 * @return
//	 */
//	public Map<String,Map<String, List<TickerData>>> getMarketDataMap(ExchangeType exchangeType) {
//		CoinPairData coinPairData = commonService.getDirectCoinPair(exchangeType);
//
//		List<String> quoteCoinList = coinPairData.getQuoteCoinList();
//
//		Map<String,Map<String, List<TickerData>>> allData = new HashMap<>();
//		if (CollectionUtils.isEmpty(quoteCoinList)) {
//			return allData;
//		}
//
//		for (String quoteCoin : quoteCoinList) {
//			Map<String, List<TickerData>> resMap = new HashMap<>();
//			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
//			List<String> tickerDatas = hashOperations.values(
//					RedisKeyConstants.COIN_TICKER + exchangeType.getCode() + "_" + quoteCoin.toLowerCase() + "_");
//			if (CollectionUtils.isEmpty(tickerDatas)) {
//				break;
//			}
//
//			List<TickerData> res = resMap.get(exchangeType.getCode());
//			if(res == null) {
//				res = new ArrayList<TickerData>();
//				resMap.put(exchangeType.getCode(), res);
//			}
//					
//			for (String _data : tickerDatas) {
//				res.add(JSON.parseObject(_data, TickerData.class));
//			}
//
//			allData.put(quoteCoin, resMap);
//		}
//
//		return allData;
//	}
	
	
	/**
	 * 发送成交信息到客户端
	 * @param config
	 * @param data
	 * @param exchangeType
	 */
	public <T> void sendTradeTicker(CoinPairConfig config, T data, ExchangeType exchangeType,Boolean isDirect) {

		RespWebSocketData<T> resp = new RespWebSocketData<T>();

		resp.setData(data);

		resp.setBaseCoin(config.getBaseCoin());
		resp.setQuoteCoin(config.getQuoteCoin());
		resp.setSymbol(config.getSymbol());
		resp.setExchangeType(exchangeType);
		resp.setIsDirect(isDirect);
		resp.setType(WebSocketBusiType.TRADE_TICKER);
		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));

		resp.setOperateType(WebSocketOperateType.FULL);
		apiWebSocketController.pushMessageToClient(resp);
	}

	public <T> void sendTicker(CoinPairConfig config, T data, ExchangeType exchangeType,Boolean isDirect) {

		RespWebSocketData<T> resp = new RespWebSocketData<T>();

		resp.setData(data);

		resp.setBaseCoin(config.getBaseCoin());
		resp.setQuoteCoin(config.getQuoteCoin());
		resp.setSymbol(config.getSymbol());

		resp.setExchangeType(exchangeType);
		resp.setIsDirect(isDirect);

		resp.setType(WebSocketBusiType.TICKER);
		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));

		resp.setOperateType(WebSocketOperateType.FULL);
		apiWebSocketController.pushMessageToClient(resp);
	}

	public <T> void sendMarket(String quoteCoin, T data, ExchangeType exchangeType,Boolean isDirect,WebSocketEvent event) {

		RespWebSocketData<T> resp = new RespWebSocketData<T>();
		resp.setData(data);
		if (quoteCoin != null) {
			resp.setQuoteCoin(quoteCoin);
		}else {
			resp.setQuoteCoin(null);
		}
		resp.setExchangeType(exchangeType);
		resp.setIsDirect(isDirect);
		
		resp.setType(WebSocketBusiType.MARKET);
		resp.setEvent(event);
		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));

		resp.setOperateType(WebSocketOperateType.INCREMENT);
		apiWebSocketController.pushMessageToClient(resp);
	}

	public <T> void sendDepthStep(CoinPairConfig config, T data, String depthStep, ExchangeType exchangeType,Boolean isDirect) {

		RespWebSocketData<T> resp = new RespWebSocketData<T>();

		resp.setData(data);

		resp.setDepthStep(depthStep);

		resp.setBaseCoin(config.getBaseCoin());
		resp.setQuoteCoin(config.getQuoteCoin());
		resp.setSymbol(config.getSymbol());

		resp.setExchangeType(exchangeType);
		resp.setIsDirect(isDirect);

		resp.setType(WebSocketBusiType.DEPTH_STEP);
		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));

		resp.setOperateType(WebSocketOperateType.FULL);
		apiWebSocketController.pushMessageToClient(resp);
	}

	public <T> void sendKlineTime(CoinPairConfig config, T data, String klineTime, ExchangeType exchangeType,Boolean isDirect) {

		RespWebSocketData<T> resp = new RespWebSocketData<T>();

		resp.setData(data);
		resp.setKlineTime(klineTime);

		resp.setBaseCoin(config.getBaseCoin());
		resp.setQuoteCoin(config.getQuoteCoin());
		resp.setSymbol(config.getSymbol());

		resp.setExchangeType(exchangeType);
		resp.setIsDirect(isDirect);

		resp.setType(WebSocketBusiType.KLINE);
		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));

		resp.setOperateType(WebSocketOperateType.FULL);
		apiWebSocketController.pushMessageToClient(resp);
	}

}
