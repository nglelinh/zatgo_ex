package com.zatgo.zup.exchange.strategy.gsboms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.enumtype.BusinessEnum.CoinPairConfigStatus;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.KlineTime;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketOperateType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.exchange.appapi.model.DepthData;
import com.zatgo.zup.exchange.appapi.model.DepthStepData;
import com.zatgo.zup.exchange.appapi.model.KlineData;
import com.zatgo.zup.exchange.appapi.model.ReqWebsocketParams;
import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.appapi.model.TickerData;
import com.zatgo.zup.exchange.appapi.model.TradeData;
import com.zatgo.zup.exchange.appapi.util.CoinPairUtils;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.mapper.CoinPairConfigMapper;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.WebSocketClientService;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
import com.zatgo.zup.exchange.strategy.gsboms.model.GsbomsKlineDataResp;
import com.zatgo.zup.exchange.strategy.gsboms.model.GsbomsSendDataReq;
import com.zatgo.zup.exchange.strategy.gsboms.utils.GsbomsUtils;

@Component("ApiWebSocketService_gsboms")
public class ApiWebSocketServiceGsbomsImpl extends WebSocketStrategyService {

	private static final Logger log = LoggerFactory.getLogger(ApiWebSocketServiceGsbomsImpl.class);

	@Value("${exchange.gsboms.url:}")
	private String url;
	
	@Value("${exchange.gsboms.co_no:}")
	private Integer co_no;
	
	@Value("${exchange.gsboms.opor_no:}")
	private Long opor_no;
	
	@Value("${exchange.gsboms.key:}")
	private String key;
	
	@Value("${exchange.gsboms.port:}")
	private Integer port; 

	@Value("${exchange.gsboms.clientType:}")
	private String clientType;

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private OkHttpService httpService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private CoinPairConfigMapper coinPairConfigMapper;
	
	private ExchangeType exchangeType = ExchangeType.Gsboms;

	@Override
	public void receiveServerMessage(String message) {
		JSONObject resp = JSON.parseObject(message);
		String topic = (String)resp.get("topic");
		String api = (String)resp.get("api");
		
		if(api == null && api.trim().equals("")) {
			log.error("websocket推送失败(api不存在)：" + message);
			return;
		}
		
		RespWebSocketData<?> data = null;
		WebSocketEvent event = WebSocketEvent.SUB;
		
		if(api.equalsIgnoreCase("quote.depth")) {
			
			data = parseDepth(message);
			data.setType(WebSocketBusiType.DEPTH_STEP);
			data.setDepthStep("1");
			data.setOperateType(WebSocketOperateType.FULL);
			
			String symbol = GsbomsUtils.getDepthSymbolByTopic(topic);
			String[] coins = CoinPairUtils.getSymbol(symbol);
			data.setBaseCoin(coins[0]);
			data.setQuoteCoin(coins[1]);
			data.setSymbol(symbol);
			
		}else if(api.equalsIgnoreCase("quote.kline")) {
			KlineTime klineTime = GsbomsUtils.getKlineTimeByTopic(topic);
			//24小时行情-更新市场行情
			if(klineTime.equals(KlineTime.DAY)) {
				RespWebSocketData<?> marketDetail = getMarketDetail(GsbomsUtils.getKlineSymbolByTopic(topic), message, WebSocketBusiType.TICKER);
				updateMarketTickerCache((TickerData)marketDetail.getData());
			}
			
			data = parseKlineData(event, message);
			data.setType(WebSocketBusiType.KLINE);
			data.setKlineTime(GsbomsUtils.getKlineTimeByTopic(topic).getCode());
			data.setOperateType(WebSocketOperateType.INCREMENT);
			
			String symbol = GsbomsUtils.getKlineSymbolByTopic(topic);
			String[] coins = CoinPairUtils.getSymbol(symbol);
			data.setBaseCoin(coins[0]);
			data.setQuoteCoin(coins[1]);
			data.setSymbol(symbol);
			
		}else if(api.equalsIgnoreCase("quote.trade")) {
			data = parseTradeDetailData(event, message);
			data.setType(WebSocketBusiType.TRADE_TICKER);
			data.setOperateType(WebSocketOperateType.INCREMENT);
			
			String symbol = GsbomsUtils.getTradeSymbolByTopic(topic);
			String[] coins = CoinPairUtils.getSymbol(symbol);
			data.setBaseCoin(coins[0]);
			data.setQuoteCoin(coins[1]);
			data.setSymbol(symbol);
		}else{
			log.error("websocket推送失败(api不识别)：" + message);
			return;
		}

		//正常订阅的处理
		data.setEvent(event);
		data.setMD5(MD5.GetMD5Code(JSON.toJSONString(data.getData())));
		sendDataToClient(data, ExchangeType.Gsboms);

		//gsboms不支持 req，这里特殊处理
		data.setEvent(WebSocketEvent.REQ);
		data.setMD5(MD5.GetMD5Code(JSON.toJSONString(data.getData())));
		sendDataToClient(data, ExchangeType.Gsboms);
	}

	private RespWebSocketData<List<KlineData>> parseKlineData(WebSocketEvent event, String json) {
		List<GsbomsKlineDataResp> datas = null;
		JSONObject resp = JSON.parseObject(json);
		
		datas = JSON.parseArray(resp.get("data").toString(), GsbomsKlineDataResp.class);

		List<KlineData> klineDatas = new ArrayList<KlineData>();
		for (GsbomsKlineDataResp data : datas) {
			KlineData klineData = new KlineData();
			klineData.setAmount(data.getVol());
			klineData.setClose(data.getClose());
			klineData.setHigh(data.getHigh());
			klineData.setLow(data.getLow());
			klineData.setOpen(data.getOpen());
			klineData.setVol(data.getAmount());
			klineData.setTimePoint(data.getId());
			klineDatas.add(klineData);
		}

		RespWebSocketData<List<KlineData>> data = new RespWebSocketData<List<KlineData>>();
		data.setData(klineDatas);

		return data;
	}

	private RespWebSocketData<DepthStepData> parseDepth(String json) {
		JSONObject resp = JSONObject.parseObject(json);
		List<DepthData> bidList = new ArrayList<>();
		JSONArray bids = resp.getJSONArray("bids");
		bids.forEach(bid -> {
			Map bidMap = (Map)bid;
			DepthData depthData = new DepthData();
			depthData.setPrice((BigDecimal)bidMap.get("price"));
			depthData.setAmount((BigDecimal)bidMap.get("amount"));
			bidList.add(depthData);
		});

		List<DepthData> askList = new ArrayList<>();
		JSONArray asks = resp.getJSONArray("asks");
		asks.forEach(ask -> {
			Map askMap = (Map)ask;
			DepthData depthData = new DepthData();
			depthData.setPrice((BigDecimal)askMap.get("price"));
			depthData.setAmount((BigDecimal)askMap.get("amount"));
			askList.add(depthData);
		});

		DepthStepData stepData = new DepthStepData();
		stepData.setAsks(askList);
		stepData.setBids(bidList);

		RespWebSocketData<DepthStepData> data = new RespWebSocketData<>();
		data.setData(stepData);
		return data;
	}

	private RespWebSocketData<List<TradeData>> parseTradeDetailData(WebSocketEvent event, String json) {
		JSONObject resp = JSON.parseObject(json);
		JSONArray dataJson = resp.getJSONArray("data");

		List<TradeData> datas = new ArrayList<>();
		dataJson.forEach(wsResp -> {
			Map respMap = (Map)wsResp;
			TradeData data = new TradeData();
			data.setId(respMap.get("id").toString());
			data.setPrice(BigDecimal.valueOf(Double.valueOf((String)respMap.get("price"))));
			data.setVol(BigDecimal.valueOf(Double.valueOf((String)respMap.get("qty"))));
			Integer direction = (Integer)respMap.get("direction");
			if(direction.equals(1)) {
				data.setSide(ExchangeSide.BUY);
			}else {
				data.setSide(ExchangeSide.SELL);
			}
			data.setTs((Long)respMap.get("ts"));
			datas.add(data);
		});

		RespWebSocketData<List<TradeData>> data = new RespWebSocketData<>();
		data.setData(datas);

		return data;
	}


	private RespWebSocketData<TickerData> getMarketDetail(String symbol, String json, WebSocketBusiType type) {

		JSONObject resp = JSON.parseObject(json);
		JSONArray list =  resp.getJSONArray("data");
		JSONObject klineData = (JSONObject)list.get(0);

		String quoteCoin = CoinPairUtils.getQuoteCoin(symbol);
		if (StringUtils.isEmpty(quoteCoin)) {
			return null;
		}

		String baseCoin = CoinPairUtils.getSymbol(symbol)[0];
		TickerData tickerData = new TickerData();
		tickerData.setQuoteCoin(quoteCoin);
		tickerData.setBaseCoin(baseCoin);
		tickerData.setSymbol(symbol);
		tickerData.setHigh((BigDecimal)klineData.get("high"));
		tickerData.setLast((BigDecimal)klineData.get("close"));
		tickerData.setLow((BigDecimal)klineData.get("low"));
		tickerData.setQuoteCoin(quoteCoin);
		tickerData.setVol(new BigDecimal(klineData.get("vol").toString()));
		tickerData.setAmount((BigDecimal)klineData.get("amount"));
		tickerData.setCount(BigDecimal.valueOf(((Integer)klineData.get("count")).longValue()));
		tickerData.setOpen((BigDecimal)klineData.get("open"));
		tickerData.setClose((BigDecimal)klineData.get("close"));
		if(tickerData.getOpen().compareTo(BigDecimal.ZERO) == 0) {
			tickerData.setChg(BigDecimal.ZERO);
		} else {
			tickerData.setChg(tickerData.getClose().subtract(tickerData.getOpen()).divide(tickerData.getOpen(), 4, BigDecimal.ROUND_DOWN)
					.multiply(new BigDecimal("100")));
		}
		tickerData.setId(((Integer)klineData.get("id")).toString());

		RespWebSocketData<TickerData> data = new RespWebSocketData<>();
		data.setBaseCoin(baseCoin);
		data.setData(tickerData);
		data.setQuoteCoin(quoteCoin);
		data.setSymbol(symbol);
		data.setType(type);

		return data;
	}


	@Override
	public String getWebsocketSendMassage(ReqWebsocketParams params) {
		WebSocketBusiType socketBusiType = params.getType();
		String topic = "";
		String api = "";
		if(socketBusiType.equals(WebSocketBusiType.DEPTH_STEP)) {
			topic = "hb." + params.getSymbol().toLowerCase();
			api = "quote.depth";
		}else if(socketBusiType.equals(WebSocketBusiType.KLINE)) {
			topic = GsbomsUtils.getTopicKline(params.getSymbol().toLowerCase(), params.getKlineTime());
			api = "quote.kline";
		}else if(socketBusiType.equals(WebSocketBusiType.MARKET)) {
			topic = "hb";
			api = "quote.market";
		}else if(socketBusiType.equals(WebSocketBusiType.TICKER)) {
			topic = "hb." + params.getSymbol().toLowerCase() + ".86400";
			api = "quote.kline";
		}else if(socketBusiType.equals(WebSocketBusiType.TRADE_TICKER)) {
			topic = "hb." + params.getSymbol().toLowerCase();
			api = "quote.trade";
		}else {
			throw new BusinessException();
		}
		
		GsbomsSendDataReq wsParams = new GsbomsSendDataReq();

		wsParams.setEvent(params.getEvent());
		wsParams.setApi(api);
		wsParams.setTopic(topic);

		return JSON.toJSONString(wsParams);
	}

	@Override
	public WebSocketClientService createWebSocketClient() {
		try {
			GsbomsWebSocketClient client = new GsbomsWebSocketClient(url, port, co_no, opor_no, key);
			client.setExchangeType(ExchangeType.Gsboms.getCode());
			client.setService(this);
			return client;
		} catch (Exception e) {
			log.error("", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
	}

	@Override
	public void wsReq(ReqWebsocketParams params) {
		subscriptionClientListener.sendMessage(params);
	}

	@Override
	public Map<String, SymbolData> findSymbolData() {
		List<CoinPairConfig> configs = coinPairConfigMapper.selectListByCoinPairConfig(ExchangeType.Gsboms.getCode(),CoinPairConfigStatus.OPEN.getCode().intValue());
		if (CollectionUtils.isEmpty(configs)) {
			return new HashMap<>();
		}
		
		Map<String, SymbolData> symbolData = new HashMap<>();
		configs.forEach(value ->{
			SymbolData data = new SymbolData();
			data.setAmountPrecision(BigDecimal.valueOf(value.getPricePrecision()));
			data.setBaseCoin(value.getBaseCoin().toLowerCase());
			data.setPricePrecision(BigDecimal.valueOf(value.getPricePrecision()));
			data.setQuoteCoin(value.getQuoteCoin());
			data.setSymbol(value.getSymbol().toLowerCase());

			symbolData.put(value.getSymbol().toLowerCase(), data);
		});
		return symbolData;
	}
	
	@Override
	public ExchangeType getExchangeType() {
		return exchangeType;
	}

	@Override
	public DepthStepData findDepthStep(String symbol, String depthStep) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TradeData> findTradeTicker(String symbol) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TickerData findTicker24(String symbol) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<KlineData> findKline(String symbol, String klineTime) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
