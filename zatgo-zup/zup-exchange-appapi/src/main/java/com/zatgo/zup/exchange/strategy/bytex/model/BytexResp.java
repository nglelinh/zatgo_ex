package com.zatgo.zup.exchange.strategy.bytex.model;

import java.io.Serializable;
import java.util.List;

import com.alibaba.fastjson.JSON;

public class BytexResp<O> implements Serializable {

	private static final long serialVersionUID = 6639108628081553210L;
	
	private String code;
	
	private String msg;
	
	private O data;
	
	private Class<O> clazz;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public O getData() {
		return JSON.parseObject(JSON.toJSONString(this.data), this.clazz);
	}

	public void setData(O data) {
		this.data = data;
	}

	public Class<O> getClazz() {
		return clazz;
	}

	public void setClazz(Class<O> clazz) {
		this.clazz = clazz;
	}
	
	public List<O> getDatas() {
		return JSON.parseArray(JSON.toJSONString(this.data), this.clazz);
	}

}
