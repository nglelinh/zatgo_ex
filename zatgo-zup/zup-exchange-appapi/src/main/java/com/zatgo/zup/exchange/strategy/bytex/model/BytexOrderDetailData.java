package com.zatgo.zup.exchange.strategy.bytex.model;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class BytexOrderDetailData {
	
	private Long id;
	
	private String side;
	
	@JSONField(name = "side_msg")
	private String sideMsg;
	
	@JSONField(name = "created_at")
	private Date createdAt;
	
	private BigDecimal price;
	
	private BigDecimal volume;
	
	@JSONField(name = "deal_volume")
	private BigDecimal dealVolume;
	
	@JSONField(name = "total_price")
	private BigDecimal totalPrice;
	
	private BigDecimal fee;
	
	@JSONField(name = "avg_price")
	private BigDecimal avgPrice;
	
	@JSONField(name = "remain_volume")
	private BigDecimal remainVolume;
	
	private Byte status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getSideMsg() {
		return sideMsg;
	}

	public void setSideMsg(String sideMsg) {
		this.sideMsg = sideMsg;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getDealVolume() {
		return dealVolume;
	}

	public void setDealVolume(BigDecimal dealVolume) {
		if(dealVolume == null) {
			dealVolume = BigDecimal.ZERO;
		}
		this.dealVolume = dealVolume;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public BigDecimal getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(BigDecimal avgPrice) {
		this.avgPrice = avgPrice;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public BigDecimal getRemainVolume() {
		return remainVolume;
	}

	public void setRemainVolume(BigDecimal remainVolume) {
		this.remainVolume = remainVolume;
	}

}
