package com.zatgo.zup.exchange.strategy.bytex.model;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.annotation.JSONField;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;

public class BytexWsResp {

	@JSONField(name = "event_rep")
	private String eventRep;

	private String channel;

	@JSONField(name = "cb_id")
	private String cbid;

	private Long ts;

	private String status;

	private Object tick;

	private Object data;

	public String getEventRep() {
		return eventRep;
	}

	public void setEventRep(String eventRep) {
		this.eventRep = eventRep;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCbid() {
		return cbid;
	}

	public void setCbid(String cbid) {
		this.cbid = cbid;
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getTick() {
		return tick;
	}

	public void setTick(Object tick) {
		this.tick = tick;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public WebSocketEvent getWebSocketEvent() {
		if (StringUtils.isEmpty(eventRep)) {
			return WebSocketEvent.SUB;
		} else if (eventRep.equals("subed")) {
			return WebSocketEvent.SUB;
		} else if (eventRep.equals("rep")) {
			return WebSocketEvent.REQ;
		}

		return null;
	}

	public Object getWsData() {
		if (StringUtils.isEmpty(eventRep) || eventRep.equals("subed")) {
			return tick;
		} else if (eventRep.equals("rep")) {
			return data;
		}
		return null;
	}

}
