package com.zatgo.zup.exchange.appapi.model;

import java.util.List;

import com.zatgo.zup.exchange.entity.ExOrder;
import com.zatgo.zup.exchange.entity.ExTrade;

public class ExOrderDetailData {
	
	private ExOrder order;
	
	private List<ExTrade> exTrades;

	public ExOrder getOrder() {
		return order;
	}

	public void setOrder(ExOrder order) {
		this.order = order;
	}

	public List<ExTrade> getExTrades() {
		return exTrades;
	}

	public void setExTrades(List<ExTrade> exTrades) {
		this.exTrades = exTrades;
	}

}
