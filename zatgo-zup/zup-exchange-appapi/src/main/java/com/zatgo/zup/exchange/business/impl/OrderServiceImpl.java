package com.zatgo.zup.exchange.business.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.ExchangeUpdateBalanceParams;
import com.zatgo.zup.common.model.LockWalletBalanceParams;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.ExOrderDetailData;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.appapi.remoteservice.PayRemoteService;
import com.zatgo.zup.exchange.appapi.remoteservice.UserRemoteService;
import com.zatgo.zup.exchange.business.OrderService;
import com.zatgo.zup.exchange.entity.ExOrder;
import com.zatgo.zup.exchange.entity.ExTrade;
import com.zatgo.zup.exchange.mapper.ExOrderMapper;
import com.zatgo.zup.exchange.mapper.ExTradeMapper;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.OrderStrategyService;

@Service("businessOrderServiceImpl")
public class OrderServiceImpl implements OrderService {

	private static final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Autowired
	private ExchangeStrategy exchangeStrategy;

	@Autowired
	private ExOrderMapper exOrderMapper;

	@Autowired
	private ExTradeMapper exTradeMapper;

	@Autowired
	private CommonService commonService;

	@Autowired
	private PayRemoteService payRemoteService;
	
	@Autowired
	private UserRemoteService userRemoteService;

	@Override
	public PageInfo<ExOrder> findUnfinishedByPage(int pageNo, int pageSize) {

		PageHelper.startPage(pageNo, pageSize);

		OrderSearchParams params = new OrderSearchParams();

		OrderStatus[] status = new OrderStatus[] { OrderStatus.INIT, OrderStatus.NEW, OrderStatus.PART_FILLED,
				OrderStatus.PENDING_CANCEL, OrderStatus.EXCEPTION };
		params.setOrderStatuss(status);

		Page<ExOrder> orders = exOrderMapper.selectOrderByOrderSearchParams(params);

		PageInfo<ExOrder> info = new PageInfo<>(pageNo, pageSize, orders.getTotal(),
				CollectionUtils.isEmpty(orders.getResult()) ? new ArrayList<>() : orders.getResult());

		PageHelper.clearPage();

		return info;
	}

	@Transactional
	@Override
	public void syncOrder(ExOrderDetailData orderData) {

		//更 新本地订单信息
		ExOrder syncOrderData = orderData.getOrder();
		List<ExTrade> exTrades = orderData.getExTrades();
		if (syncOrderData == null) {
			return;
		}
		syncOrderData.setUpdateDate(new Date());
		ExOrder localOrder = exOrderMapper.selectOrderByOrderIdAndLock(syncOrderData.getOrderId());
		if (localOrder.getStatus().equals(new Byte(OrderStatus.FILLED.getCode()))
				|| localOrder.getStatus().equals(new Byte(OrderStatus.CANCELED.getCode()))) {
			return;
		}
		if (localOrder.getStatus().equals(syncOrderData.getStatus())
				&& !localOrder.getStatus().equals(new Byte(OrderStatus.PART_FILLED.getCode()))) {
			return;
		}
		if (localOrder.getStatus().equals(new Byte(OrderStatus.PART_FILLED.getCode()))
				&& syncOrderData.getDealVolume().equals(localOrder.getDealVolume())) {
			return;
		}
		exOrderMapper.updateByPrimaryKeySelective(syncOrderData);

		//撮合成功的需要操作账户余额
		String orderId = localOrder.getOrderId();
		if (CollectionUtils.isNotEmpty(exTrades)) {
			Map<String, ExTrade> exTradeMap = new LinkedHashMap<String, ExTrade>();
			for (ExTrade exTrade : exTrades) {
				exTradeMap.put(exTrade.getExternalTradeId(), exTrade);
			}

			List<ExTrade> trades = exTradeMapper.selectExTradeByOrderId(orderId);

			if (CollectionUtils.isNotEmpty(trades)) {
				for (ExTrade exTrade : trades) {
					exTradeMap.remove(exTrade.getExternalTradeId());
				}
			}

			BigDecimal buyIncrease = localOrder.getMakerFeeIncrease() == null ? BigDecimal.ZERO
					: localOrder.getMakerFeeIncrease();
			BigDecimal sellIncrease = localOrder.getTakerFeeIncrease() == null ? BigDecimal.ZERO
					: localOrder.getTakerFeeIncrease();
			String side = localOrder.getSide();
			String quoteCoin = localOrder.getQuoteCoin();
			String baseCoin = localOrder.getBaseCoin();
			
			ResponseData<UserData> userResp = userRemoteService.getUserById(localOrder.getUserId());
			if(!userResp.isSuccessful()) {
				log.error("查询用户信息失败：", userResp.getMessage());
				throw new BusinessException(userResp.getCode(), userResp.getMessage());
			}
			
			String userName = userResp.getData().getUserName();

			List<ExchangeUpdateBalanceParams> exchangeUpdateBalanceParams = new ArrayList<>();
			Set<Entry<String, ExTrade>> entries = exTradeMap.entrySet();
			for (Entry<String, ExTrade> entry : entries) {
				ExTrade value = entry.getValue();
				value.setTradeId(UUIDUtils.getUuid());
				value.setCreateTime(new Date());
				exTradeMapper.insertSelective(value);

				getMatchingLockBalanceParams(value, exchangeUpdateBalanceParams, buyIncrease, sellIncrease, side, quoteCoin,
						baseCoin, orderId, userName);
			}
			ResponseData<Object> resp = payRemoteService.matchingLockBalance(exchangeUpdateBalanceParams);
			if (!BusinessExceptionCode.SUCCESS_CODE.equals(resp.getCode())
					&& !BusinessExceptionCode.REPEAT_OPERATION_BALANCE.equals(resp.getCode())) {
				log.error("同步订单处理账户余额失败：" + resp.getMessage());
				throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
			}

			//完成数据是否存在需要解冻的
			if (OrderStatus.FILLED.getCode().equals(syncOrderData.getStatus().toString())
					|| OrderStatus.CANCELED.getCode().equals(syncOrderData.getStatus().toString())) {

				BigDecimal totalVolume = BigDecimal.ZERO;
				if(localOrder.getType().equals(new Byte(EntryOrdersType.limitPriceEntrust.getCode()))) {
					totalVolume = localOrder.getVolume().multiply(localOrder.getPrice());
				} else {
					totalVolume = localOrder.getVolume();
				}
				
				BigDecimal remainVolume = totalVolume.subtract(sumVolume(exTrades, localOrder.getSide()));
				if (remainVolume.compareTo(BigDecimal.ZERO) == 1) {
					LockWalletBalanceParams unlockParams = new LockWalletBalanceParams();
					if (side.equals(ExchangeSide.BUY.getCode())) {
						unlockParams.setCoinType(localOrder.getQuoteCoin());
					} else if (side.equals(ExchangeSide.SELL.getCode())) {
						unlockParams.setCoinType(localOrder.getBaseCoin());
					}

					unlockParams.setLockAmount(remainVolume);
					unlockParams.setLockBalanceType(BusinessEnum.LockBalanceOperateType.UNLOCK);
					unlockParams.setOrderId(orderId);
					
					ResponseData<Object> resp1 = payRemoteService.updateLockBalance(unlockParams);
					if (!BusinessExceptionCode.SUCCESS_CODE.equals(resp1.getCode())
							&& !BusinessExceptionCode.REPEAT_OPERATION_BALANCE.equals(resp1.getCode())) {
						log.error("同步订单处理账户余额失败：" + resp1.getMessage());
						throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
					}
				}
			}
			
		}
	}
	
	private BigDecimal sumVolume(List<ExTrade> exTrades, String side) {
		if(CollectionUtils.isEmpty(exTrades)) {
			return BigDecimal.ZERO;
		}
		
		BigDecimal sumVolume = BigDecimal.ZERO;
		for (ExTrade trade : exTrades) {
			BigDecimal dealVolume = trade.getVolume();
			if (side.equals(ExchangeSide.BUY.getCode())) {
				sumVolume = dealVolume.multiply(trade.getPrice());
			} else if (side.equals(ExchangeSide.SELL.getCode())) {
				sumVolume = dealVolume.divide(trade.getPrice(), 8, BigDecimal.ROUND_DOWN);
			}
			
		}
		
		return sumVolume;
	}

	private void getMatchingLockBalanceParams(ExTrade trade, List<ExchangeUpdateBalanceParams> lockWalletBalanceParams,
			BigDecimal buyIncrease, BigDecimal sellIncrease, String side, String quoteCoin, String baseCoin,
			String orderId, String userName) {
		// 处理账户余额增减
		BigDecimal buyFee = BigDecimal.ZERO;
		BigDecimal sellFee = BigDecimal.ZERO;
		BigDecimal dealVolume = trade.getVolume();
		BigDecimal deductVolume = BigDecimal.ZERO;

		ExchangeUpdateBalanceParams deductParams = new ExchangeUpdateBalanceParams();
		deductParams.setBusinessNumber(orderId + "_" + trade.getExternalTradeId());
		if (side.equals(ExchangeSide.SELL.getCode())) {
			deductParams.setSellOrderId(orderId);
			deductParams.setSellUserId(trade.getBidUserId());
			deductParams.setSellUserName(userName);
			deductVolume = dealVolume.add(trade.getSellFee());
			sellFee = sellFee.add(trade.getPrice().multiply(sellIncrease));
		} else {
			deductParams.setBuyOrderId(trade.getAskOrderId());
			deductParams.setSellUserId(trade.getAskUserId());
			deductParams.setSellUserName(userName);
			deductVolume = dealVolume.multiply(trade.getPrice()).add(trade.getBuyFee());
			buyFee = buyFee.add(buyIncrease.multiply(dealVolume));
		}

		deductParams.setBuyLockAmount(deductVolume);

		deductVolume = dealVolume.divide(trade.getPrice(), 8, BigDecimal.ROUND_DOWN);
		deductParams.setSellLockAmount(deductVolume);

		deductParams.setBaseCoinType(baseCoin);
		deductParams.setQuoteCoinType(quoteCoin);

		deductParams.setBuyfee(buyFee);
		deductParams.setSellfee(sellFee);

		lockWalletBalanceParams.add(deductParams);

	}

	@Transactional
	@Override
	public ExOrder createOrder(ExOrder order) {

		order = exOrderMapper.selectByPrimaryKey(order.getOrderId());
		if (order == null) {
			log.error("订单创建异常：订单不存在" + JSONObject.toJSONString(order));
			return null;
		}
		String orderStatus = order.getStatus().toString();
		if (OrderStatus.EXCEPTION.getCode().equals(orderStatus)
				|| OrderStatus.UNCOMMITTED.getCode().equals(orderStatus)) {
			// 不再处理订单精度，统一由下单校验精度，这里只将多余的0去除

			BigDecimal price = order.getPrice();
			if (price != null) {
				order.setPrice(price.stripTrailingZeros());
			}

			BigDecimal volume = order.getVolume();
			order.setVolume(volume.stripTrailingZeros());

			OrderStrategyService orderStrategyService = exchangeStrategy
					.getOrderService(ExchangeType.getEnumByType(order.getExternalExchangeType()));

			String externalOrderId = null;
			try {
				externalOrderId = orderStrategyService.createOrder(order);
				Byte code = updateExOrder(externalOrderId, order.getOrderId());
				order.setStatus(code);
				if (StringUtils.isEmpty(externalOrderId)) {
					try {
						commonService.unLockAccountBalance(order);
					} catch (Exception e) {
						log.error("解冻订单失败：", e);
					}
				}
			} catch (Exception e) {
				log.error("订单创建异常：", e);
				ExOrder record = new ExOrder();
				record.setOrderId(order.getOrderId());
				record.setStatus(new Byte(OrderStatus.EXCEPTION.getCode()));
				order.setStatus(new Byte(OrderStatus.EXCEPTION.getCode()));
				exOrderMapper.updateByPrimaryKeySelective(record);
			}
		}

		return order;
	}

	private Byte updateExOrder(String externalOrderId, String orderId) {
		ExOrder record = new ExOrder();
		if (StringUtils.isEmpty(externalOrderId)) {
			record.setStatus(new Byte(OrderStatus.CANCELED.getCode()));
		} else {

			record.setStatus(new Byte(OrderStatus.INIT.getCode()));
			record.setExternalOrderId(externalOrderId);
		}

		record.setOrderId(orderId);
		exOrderMapper.updateByPrimaryKeySelective(record);
		return record.getStatus();
	}

	@Override
	public OrderData cancelOrder(CancelOrderParams params, String userId) {
		OrderStrategyService orderService = exchangeStrategy.getOrderService(params.getBaseCoin());
		// OrderData orderData = orderService.cancelOrder(params);
		return null;
	}

	@Override
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params, String userId) {
		OrderStrategyService orderService = exchangeStrategy.getOrderService(params.getBaseCoin());
		PageInfo<OrderData> orderDataPageInfo = orderService.findOrderByPage(params);
		return orderDataPageInfo;
	}

	@Override
	public OrderData getOrderByOrderId(String orderId, String userId) {
		ExOrder exOrder = exOrderMapper.selectByPrimaryKey(orderId);
		return null;
		// return orderData;
	}

	@Override
	public PageInfo<ExOrder> findOrderBySearchParams(OrderSearchParams params) {

		PageHelper.startPage(params.getPageNo(), params.getPageSize());

		Page<ExOrder> orders = exOrderMapper.selectOrderByOrderSearchParams(params);

		PageInfo<ExOrder> info = new PageInfo<>(orders.getPageNum(), orders.getPageSize(), orders.getTotal(),
				CollectionUtils.isEmpty(orders.getResult()) ? new ArrayList<>() : orders.getResult());

		PageHelper.clearPage();
		return info;
	}

}
