package com.zatgo.zup.exchange.strategy.huobi.model;

import com.alibaba.fastjson.annotation.JSONField;

public class HuobiResp {
	
	private String status;
	
	@JSONField(name = "err-code")
	private String errCode;
	
	@JSONField(name = "err-msg")
	private String errMsg;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
