package com.zatgo.zup.exchange.strategy.huobi.model;

import java.util.List;

public class HuobiOrdersResp extends HuobiResp {
	
	private List<HuobiOrderData> data;

	public List<HuobiOrderData> getData() {
		return data;
	}

	public void setData(List<HuobiOrderData> data) {
		this.data = data;
	}

}
