package com.zatgo.zup.exchange.strategy.bytex.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;

public class BytexExOrder {
	
	private Long id;
	
	private String side;
	
	@JSONField(name = "side_msg")
	private String sideMsg;
	
	private Byte status;
	
	@JSONField(name = "status_msg")
	private String statusMsg;
	
	@JSONField(name = "created_at")
	private Date createdAt;
	
	private BigDecimal price;
	
	private BigDecimal volume;
	
	private Byte type;
	
	private Byte source;
	
	@JSONField(name = "sourceMsg")
	private String sourceMsg;
	
	@JSONField(name = "total_price")
	private BigDecimal totalPrice;
	
	@JSONField(name = "remain_volume")
	private BigDecimal remainVolume;
	
	@JSONField(name = "deal_volume")
	private BigDecimal dealVolume;
	
	@JSONField(name = "avg_price")
	private BigDecimal avgPrice;
	
	private String baseCoin;
	
	private String countCoin;
	
	private List<BytexExTrade> tradeList;
	
	@JSONField(name = "part_cancel_flag")
	private String partCancelFlag;
	
	private Date ctime;
	
	@JSONField(name = "deal_price")
	private BigDecimal dealPrice;
	
	private String feeCoin;
	
	private BigDecimal fee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getSideMsg() {
		return sideMsg;
	}

	public void setSideMsg(String sideMsg) {
		this.sideMsg = sideMsg;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Object type) {
		if(type instanceof Integer) {
			this.type = ((Integer) type).byteValue();
		} else if(type instanceof Byte) {
			this.type = (Byte) type;
		} else {
			this.type = null;
		}
		
	}

	public Byte getSource() {
		return source;
	}

	public void setSource(Byte source) {
		this.source = source;
	}

	public String getSourceMsg() {
		return sourceMsg;
	}

	public void setSourceMsg(String sourceMsg) {
		this.sourceMsg = sourceMsg;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getRemainVolume() {
		return remainVolume;
	}

	public void setRemainVolume(BigDecimal remainVolume) {
		this.remainVolume = remainVolume;
	}

	public BigDecimal getDealVolume() {
		return dealVolume;
	}

	public void setDealVolume(BigDecimal dealVolume) {
		this.dealVolume = dealVolume;
	}

	public BigDecimal getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(BigDecimal avgPrice) {
		this.avgPrice = avgPrice;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getCountCoin() {
		return countCoin;
	}

	public void setCountCoin(String countCoin) {
		this.countCoin = countCoin;
	}

	public List<BytexExTrade> getTradeList() {
		return tradeList;
	}

	public void setTradeList(List<BytexExTrade> tradeList) {
		this.tradeList = tradeList;
	}

	public String getPartCancelFlag() {
		return partCancelFlag;
	}

	public void setPartCancelFlag(String partCancelFlag) {
		this.partCancelFlag = partCancelFlag;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public BigDecimal getDealPrice() {
		return dealPrice;
	}

	public void setDealPrice(BigDecimal dealPrice) {
		this.dealPrice = dealPrice;
	}

	public String getFeeCoin() {
		return feeCoin;
	}

	public void setFeeCoin(String feeCoin) {
		this.feeCoin = feeCoin;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

}
