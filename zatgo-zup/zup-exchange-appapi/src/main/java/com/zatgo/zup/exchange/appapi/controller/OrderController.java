package com.zatgo.zup.exchange.appapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.service.OrderService;
import com.zatgo.zup.exchange.service.OrderStrategy;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/exchange/appapi", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/exchange/appapi")
public class OrderController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
	
	@Autowired
	private OrderStrategy orderStrategy;

	@ApiOperation(value = "创建订单接口")
	@RequestMapping(value = "/order/create", name = "创建订单接口", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<OrderData> createOrder(@RequestBody CreateOrderParams params) {
		AuthUserInfo userInfo = getUserInfo();
		OrderService orderService = orderStrategy.getOrderService(params.getExchangeType());
		OrderData orderData = orderService.createOrder(params, userInfo.getUserId());
		return BusinessResponseFactory.createSuccess(orderData);
	}

	@ApiOperation(value = "取消订单")
	@RequestMapping(value = "/order/cancel", name = "取消订单", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<OrderData> cancelOrder(@RequestBody CancelOrderParams params) {
		AuthUserInfo userInfo = getUserInfo();
		OrderService orderService = orderStrategy.getOrderService(params.getExchangeType());
		OrderData orderData = orderService.cancelOrder(params, userInfo.getUserId());
		return BusinessResponseFactory.createSuccess(orderData);
	}

	@ApiOperation(value = "订单分页查询")
	@RequestMapping(value = "/order/pageSearch", name = "订单分页查询", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<PageInfo<OrderData>> findOrderByPage(@RequestBody OrderSearchParams params) {
		AuthUserInfo userInfo = getUserInfo();
		params.setUserId(userInfo.getUserId());
		OrderService orderService = orderStrategy.getOrderService(params.getExchangeType());
		PageInfo<OrderData> orderDataPageInfo = orderService.findOrderByPage(params);
		return BusinessResponseFactory.createSuccess(orderDataPageInfo);
	}

	@ApiOperation(value = "获取订单详情")
	@RequestMapping(value = "/order", name = "获取订单详情", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<OrderData> getOrderByOrderId(@ModelAttribute OrderDetailParams params) {
		AuthUserInfo userInfo = getUserInfo();
		params.setUserId(userInfo.getUserId());
		OrderService orderService = orderStrategy.getOrderService(params.getExchangeType());
		OrderData orderData = orderService.getOrderByOrderId(params);
		return BusinessResponseFactory.createSuccess(orderData);
	}

}
