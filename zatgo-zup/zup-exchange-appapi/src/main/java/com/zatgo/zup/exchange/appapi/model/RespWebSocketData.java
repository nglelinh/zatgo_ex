package com.zatgo.zup.exchange.appapi.model;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.annotation.JSONField;
import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketOperateType;

import io.swagger.annotations.ApiModelProperty;

public class RespWebSocketData<T> {

	@ApiModelProperty(value = "websocket event",required =true )
	private WebSocketEvent event;
	
	@ApiModelProperty(value = "币对，市场标识",required =true )
	private String symbol;
	
	@ApiModelProperty(value = "基准货币",required =true )
	private String baseCoin;
	
	@ApiModelProperty(value = "计价货币",required =true )
	private String quoteCoin;
	
	@ApiModelProperty(value = "业务类型",required =true )
	private WebSocketBusiType Type;
	
	@ApiModelProperty(value = "channel=kline时,取值范围[1min,5min,15min,30min,60min,1day,1week,1month]",required =false )
	private String klineTime;
	
	@ApiModelProperty(value = "channel=depth_step时,取值范围[0-2]",required =false )
	private String depthStep;
	
	@ApiModelProperty(value = "交易所类型",required =false )
	private ExchangeType exchangeType;
	
	public T data;
	
	@JSONField(name = "MD5")
	@ApiModelProperty(value = "数据MD5值，依此校验数据是否更新",required =false )
	public String MD5;
	
	@ApiModelProperty(value = "操作类型：full-全量；increment-增量部分增加修改；del-删除",required =false )
	public WebSocketOperateType operateType;
	
	@ApiModelProperty(value = "是否直连模式",required =false )
	public Boolean isDirect;

	public Boolean getIsDirect() {
		return isDirect;
	}

	public void setIsDirect(Boolean isDirect) {
		this.isDirect = isDirect;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	public WebSocketEvent getEvent() {
		return event;
	}

	public void setEvent(WebSocketEvent event) {
		this.event = event;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public WebSocketBusiType getType() {
		return Type;
	}

	public void setType(WebSocketBusiType type) {
		Type = type;
	}

	public String getKlineTime() {
		return klineTime;
	}

	public void setKlineTime(String klineTime) {
		this.klineTime = klineTime;
	}

	public String getDepthStep() {
		return depthStep;
	}

	public void setDepthStep(String depthStep) {
		this.depthStep = depthStep;
	}

	public ExchangeType getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(ExchangeType exchangeType) {
		this.exchangeType = exchangeType;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMD5() {
		return MD5;
	}

	public void setMD5(String MD5) {
		this.MD5 = MD5;
	}

	public WebSocketOperateType getOperateType() {
		return operateType;
	}

	public void setOperateType(WebSocketOperateType operateType) {
		this.operateType = operateType;
	}
	
	/**
	 * 获取订阅的通道
	 * @return
	 */
	public String getChannel() {
		if(getType() == null) {
			return "";
		}
		String type = getType().getCode();
		String channel = type;
		if (WebSocketBusiType.KLINE.getCode().equalsIgnoreCase(type)) {
			channel = channel + getSymbol().toLowerCase() + "_" + getKlineTime().trim();
		} else if (WebSocketBusiType.TICKER.getCode().equalsIgnoreCase(type)) {
			channel = channel + getSymbol().toLowerCase();
		} else if (WebSocketBusiType.TRADE_TICKER.getCode().equalsIgnoreCase(type)) {
			channel = channel + getSymbol().toLowerCase();
		} else if (WebSocketBusiType.DEPTH_STEP.getCode().equalsIgnoreCase(type)) {
			channel = channel + getSymbol().toLowerCase() + "_" + getDepthStep().trim();
		} else if (WebSocketBusiType.MARKET.getCode().equalsIgnoreCase(type)
				&& StringUtils.isNotEmpty(getQuoteCoin())) {
			channel = channel + getQuoteCoin().toLowerCase();
		}
		ExchangeType exchangeType = getExchangeType();
		if (isDirect) {
			channel = channel + "_" + getExchangeType().getCode();
		}
		if(getEvent().equals(ExchangeEnum.WebSocketEvent.REQ)) {
			channel += "_" + WebSocketEvent.REQ.getCode();
		}
		
		return channel;
	}
	
}
