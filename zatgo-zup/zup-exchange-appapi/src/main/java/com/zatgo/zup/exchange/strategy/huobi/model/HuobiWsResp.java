package com.zatgo.zup.exchange.strategy.huobi.model;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.annotation.JSONField;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;

public class HuobiWsResp {

	private String id;

	private String status;

	@JSONField(name = "err-code")
	private String errCode;

	@JSONField(name = "err-msg")
	private String errMsg;

	private String rep;

	private String subbed;

	private String ch;

	private String unsubbed;

	private String ts;

	private Object tick;

	private Object data;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getRep() {
		return rep;
	}

	public void setRep(String rep) {
		this.rep = rep;
	}

	public String getSubbed() {
		return subbed;
	}

	public void setSubbed(String subbed) {
		this.subbed = subbed;
	}

	public String getCh() {
		return ch;
	}

	public void setCh(String ch) {
		this.ch = ch;
	}

	public String getUnsubbed() {
		return unsubbed;
	}

	public void setUnsubbed(String unsubbed) {
		this.unsubbed = unsubbed;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public Object getTick() {
		return tick;
	}

	public void setTick(Object tick) {
		this.tick = tick;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getTopic() {
		if (StringUtils.isNotEmpty(ch)) {
			return getCh();
		} else if (StringUtils.isNotEmpty(subbed)) {
			return getSubbed();
		} else if (StringUtils.isNotEmpty(rep)) {
			return getRep();
		} else if (StringUtils.isNotEmpty(unsubbed)) {
			return getUnsubbed();
		}

		return null;
	}

	public WebSocketEvent getWebSocketEvent() {
		if (StringUtils.isNotEmpty(ch)) {
			return WebSocketEvent.SUB;
		} else if (StringUtils.isNotEmpty(subbed)) {
			return WebSocketEvent.SUB;
		} else if (StringUtils.isNotEmpty(rep)) {
			return WebSocketEvent.REQ;
		} else if (StringUtils.isNotEmpty(unsubbed)) {
			return WebSocketEvent.UNSUB;
		}

		return null;
	}

	public Object getWsData() {
		if (tick != null) {
			return getTick();
		} else if (data != null) {
			return getData();
		}

		return null;
	}

}
