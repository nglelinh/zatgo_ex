//package com.zatgo.zup.exchange.appapi.listener;
//
//import java.util.List;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.locks.Lock;
//import java.util.concurrent.locks.ReentrantLock;
//
//import javax.annotation.PostConstruct;
//
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.zatgo.zup.common.enumtype.ExchangeEnum;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeClientType;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.SymbolClientType;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
//import com.zatgo.zup.common.exception.BusinessException;
//import com.zatgo.zup.common.exception.BusinessExceptionCode;
//import com.zatgo.zup.exchange.appapi.model.ReqWebsocketParams;
//import com.zatgo.zup.exchange.appapi.util.WebSocketClientUtil;
//import com.zatgo.zup.exchange.entity.CoinPairConfig;
//import com.zatgo.zup.exchange.service.CommonService;
//import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
//import com.zatgo.zup.exchange.strategy.WebSocketClientService;
//import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
//
//@Component
//public class SubscriptionWebSocketClientListener {
//
//	private static ExecutorService executor = Executors.newFixedThreadPool(10);
//
//	@Autowired
//	private CommonService commonService;
//
//	@Autowired
//	private ExchangeStrategy exchangeStrategyService;
//
//	private static final Logger logger = LoggerFactory.getLogger(SubscriptionWebSocketClientListener.class);
//
//	private Lock lock = new ReentrantLock();
//
//	public List<CoinPairConfig> getCoinPair() {
//
//		return commonService.getCoinPairCache();
//	}
//
//	@PostConstruct
//	public void init() {
//
//		Runnable createWebSocketClient = new Runnable() {
//
//			@Override
//			public void run() {
//				while (true) {
//					try {
//						List<CoinPairConfig> configs = getCoinPair();
//						configs.forEach(coin -> {
//							if (!SymbolClientType.WEBSOCKET
//									.equals(SymbolClientType.getEnumByType(coin.getClientType())))
//								return;
//							createWebSocketClient(coin.getExternalExchangeType());
//						});
//
//						TimeUnit.SECONDS.sleep(5);
//					} catch (Exception e) {
//						logger.error("创建WebSocket失败：", e);
//					}
//				}
//			}
//		};
//
//		executor.execute(createWebSocketClient);
//	}
//
//	private WebSocketClientService createWebSocketClient(String exchangeType) {
//
//		if (WebSocketClientUtil.getWebSocketClient(exchangeType) != null) {
//			return WebSocketClientUtil.getWebSocketClient(exchangeType);
//		}
//
//		ExchangeType type = ExchangeType.getEnumByType(exchangeType);
//		WebSocketStrategyService service = exchangeStrategyService.getApiWebSocketService(type);
//		WebSocketClientService client = service.createWebSocketClient();
//		WebSocketClientUtil.addWebSocketClient(exchangeType, client);
//
//		return client;
//	}
//
//	public void sendMessage(ReqWebsocketParams params, ExchangeEnum.ExchangeType type) {
//
//		logger.debug("sendMessage开始：" + System.currentTimeMillis());
//		if (params.getEvent().equals(WebSocketEvent.UNSUB)) {
//			logger.debug("UNSUB unopened");
//			return;
//		}
//		
//		String symbol = params.getSymbol();
//		if (StringUtils.isNotEmpty(symbol)) {
//			CoinPairConfig config = commonService.getSymbolData(symbol);
//
//			if (type == null) {
//				type = ExchangeType.getEnumByType(config.getExternalExchangeType());
//			}
//		}
//
//		if (type == null) {
//			logger.debug("ExchangeType is null");
//			return;
//		}
//
//		WebSocketStrategyService service = exchangeStrategyService.getApiWebSocketService(type);
//		
//		if (!ExchangeClientType.WEBSOCKET.equals(service.getExchangeClientType())) {
//			return;
//		}
//
//		String massage = service.getWebsocketSendMassage(params);
//		if (StringUtils.isEmpty(massage)) {
//			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
//		}
//
//		WebSocketClientService client = getWebSocketClient(type);
//
//		client.send(massage);
//	}
//
//	private WebSocketClientService getWebSocketClient(ExchangeType type) {
//
//		WebSocketClientService client = WebSocketClientUtil.getWebSocketClient(type.getCode());
//		if (client == null || !client.isOpen()) {
//			lock.lock();
//			try {
//				client = WebSocketClientUtil.getWebSocketClient(type.getCode());
//				if (client != null && client.isOpen()) {
//					return client;
//				}
//
//				WebSocketClientUtil.removeWebSocketClient(type.getCode());
//				client = createWebSocketClient(type.getCode());
//				logger.info("connect开始：" + System.currentTimeMillis());
//				client.connect();
//				while (!client.getReadyState().equals(WebSocketClientService.ZUP_READYSTATE.OPEN)) {
//					logger.debug(type.getCode() + "ws连接中");
//				}
//				logger.info("connect结束：" + System.currentTimeMillis());
//			} catch (Exception e) {
//				logger.error("", e);
//				throw new BusinessException();
//			} finally {
//				lock.unlock(); // 释放锁
//			}
//		}
//		return client;
//
//	}
//
//}
