package com.zatgo.zup.exchange.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.exchange.appapi.model.CloudUserCoinPairConfigData;
import com.zatgo.zup.exchange.entity.CloudUserCoinPair;


public interface CloudUserCoinPairMapper extends BaseMapper<CloudUserCoinPair>{
	
    CloudUserCoinPairConfigData selectCloudUserCoinPairConfig(@Param("symbol") String symbol,@Param("cloudUserId") String cloudUserId,@Param("status") Integer status);
    
    List<CloudUserCoinPairConfigData> selectCloudUserCoinPairConfigList(@Param("cloudUserId") String cloudUserId,@Param("status") Integer status);

}