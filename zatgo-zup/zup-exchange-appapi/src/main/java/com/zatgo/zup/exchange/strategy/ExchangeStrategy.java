package com.zatgo.zup.exchange.strategy;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.zatgo.zup.common.enumtype.ExchangeEnum;

@Service
public class ExchangeStrategy implements ApplicationContextAware{

	private ApplicationContext applicationContext = null;
	
	@Value("${exchange.bytex.coinSymbols:}")
	private String bytexCoinSymbols;
	
	@Value("${exchange.default:}")
	private String exchangeDefautl;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		
	}
	
	/**
	 * 获取币种指定的交易所
	 * @param coinSymbol
	 * @return
	 */
	public ExchangeEnum.ExchangeType getExchangeTypeByCoinSymbol(String baseCoinSymbol){
		String[] bytexCoinSymbolArray = bytexCoinSymbols.split(",");
		for(String coinName:bytexCoinSymbolArray) {
			if(coinName.trim().toUpperCase().equals(baseCoinSymbol.toUpperCase())) {
				return ExchangeEnum.ExchangeType.Bytex;
			}
		}
		
		return ExchangeEnum.ExchangeType.getEnumByType(exchangeDefautl);
	}
	
	public OrderStrategyService getOrderService(String baseCoinSymbol) {
		ExchangeEnum.ExchangeType type = getExchangeTypeByCoinSymbol(baseCoinSymbol);
		String exchangeType = "OrderService_" + type.getCode();
		return (OrderStrategyService)applicationContext.getBean(exchangeType);
	}
	
	public OrderStrategyService getOrderService(ExchangeEnum.ExchangeType type) {
		String exchangeType = "OrderService_" + type.getCode();
		return (OrderStrategyService)applicationContext.getBean(exchangeType);
	}

	
	public WebSocketStrategyService getApiWebSocketService(String baseCoinSymbol) {
		ExchangeEnum.ExchangeType type = getExchangeTypeByCoinSymbol(baseCoinSymbol);
		String exchangeType = "ApiWebSocketService_" + type.getCode();
		return (WebSocketStrategyService)applicationContext.getBean(exchangeType);
	}
	
	public WebSocketStrategyService getApiWebSocketService(ExchangeEnum.ExchangeType type) {
		String exchangeType = "ApiWebSocketService_" + type.getCode();
		return (WebSocketStrategyService)applicationContext.getBean(exchangeType);
	}
}
