package com.zatgo.zup.exchange.appapi.remoteservice;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zatgo.zup.common.model.ExchangeUpdateBalanceParams;
import com.zatgo.zup.common.model.LockWalletBalanceParams;
import com.zatgo.zup.common.model.ResponseData;

@FeignClient("zup-wallet")
public interface PayRemoteService {

	@RequestMapping(value = "/wallet/exchange/update/lockbalance", method = RequestMethod.POST)
	ResponseData<Object> updateLockBalance(@RequestBody LockWalletBalanceParams params);
	
	@RequestMapping(value = "/wallet/exchange/matching/lockbalance", method = RequestMethod.POST)
	ResponseData<Object> matchingLockBalance(@RequestBody List<ExchangeUpdateBalanceParams> params);
	
}
