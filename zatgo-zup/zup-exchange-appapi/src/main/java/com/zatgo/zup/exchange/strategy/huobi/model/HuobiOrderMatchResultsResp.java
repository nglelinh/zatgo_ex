package com.zatgo.zup.exchange.strategy.huobi.model;

import java.util.List;

public class HuobiOrderMatchResultsResp extends HuobiResp {
	
	private List<HuobiOrderMatchResultData> data;

	public List<HuobiOrderMatchResultData> getData() {
		return data;
	}

	public void setData(List<HuobiOrderMatchResultData> data) {
		this.data = data;
	}
	
}
