package com.zatgo.zup.exchange.appapi.model;

import com.zatgo.zup.exchange.entity.CoinPairConfig;

public class CloudUserCoinPairConfigData extends CoinPairConfig {

	private String externalExchangeType;

	public String getExternalExchangeType() {
		return externalExchangeType;
	}

	public void setExternalExchangeType(String externalExchangeType) {
		this.externalExchangeType = externalExchangeType;
	}
	
	
}
