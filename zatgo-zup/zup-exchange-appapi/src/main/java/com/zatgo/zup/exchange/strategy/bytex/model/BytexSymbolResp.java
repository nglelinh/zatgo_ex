package com.zatgo.zup.exchange.strategy.bytex.model;

import java.math.BigDecimal;

import com.alibaba.fastjson.annotation.JSONField;

public class BytexSymbolResp {
	
	private String symbol;
	
	@JSONField(name = "count_coin")
	private String countCoin;
	
	@JSONField(name = "amount_precision")
	private BigDecimal amountPrecision;
	
	@JSONField(name = "base_coin")
	private String baseCoin;
	
	@JSONField(name = "price_precision")
	private BigDecimal pricePrecision;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getCountCoin() {
		return countCoin;
	}

	public void setCountCoin(String countCoin) {
		this.countCoin = countCoin;
	}

	public BigDecimal getAmountPrecision() {
		return amountPrecision;
	}

	public void setAmountPrecision(BigDecimal amountPrecision) {
		this.amountPrecision = amountPrecision;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public BigDecimal getPricePrecision() {
		return pricePrecision;
	}

	public void setPricePrecision(BigDecimal pricePrecision) {
		this.pricePrecision = pricePrecision;
	}

}
