package com.zatgo.zup.exchange.strategy.huobi.impl;

import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketOperateType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.exchange.appapi.listener.SubscriptionClientListener;
import com.zatgo.zup.exchange.appapi.model.DepthData;
import com.zatgo.zup.exchange.appapi.model.DepthStepData;
import com.zatgo.zup.exchange.appapi.model.KlineData;
import com.zatgo.zup.exchange.appapi.model.ReqWebsocketParams;
import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.appapi.model.TickerData;
import com.zatgo.zup.exchange.appapi.model.TradeData;
import com.zatgo.zup.exchange.appapi.util.CoinPairUtils;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.WebSocketClientService;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiKlineWsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiMarketDepthWsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiMarketDetailWsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiMarketTickersWsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiSymbolData;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiSymbolResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiTradeDetailWsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiWsReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiWsResp;
import com.zatgo.zup.exchange.strategy.huobi.util.HuobiTopicUtil;

@Component("ApiWebSocketService_huobi")
public class ApiWebSocketServiceHuobiImpl extends WebSocketStrategyService {

	private static final Logger log = LoggerFactory.getLogger(ApiWebSocketServiceHuobiImpl.class);

	private static final String MARKET_DETAIL = "/market/detail";

	@Value("${exchange.huobi.websocket.url:}")
	private String huobiWsUrl;

	@Value("${exchange.huobi.http.url:}")
	private String huobiUrl;

	@Value("${exchange.huobi.clientType:}")
	private String clientType;

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private OkHttpService httpService;

	@Autowired
	private SubscriptionClientListener subscriptionClientListener;
	
	@Autowired
	private CommonService commonService;
	
	private ExchangeType exchangeType = ExchangeType.Huobi;

	@Override
	public void receiveServerMessage(String message) {
		HuobiWsResp wsResp = JSON.parseObject(message, HuobiWsResp.class);
		if ("error".equals(wsResp.getStatus())) {
			log.error("websocket推送失败：" + wsResp.getErrMsg());
			return;
		}

		Object json = wsResp.getWsData();
		if (json == null) {
			log.debug("ws data is null");
			return;
		}

		WebSocketEvent event = wsResp.getWebSocketEvent();
		String topic = wsResp.getTopic();
		String[] topicData = topic.split("\\.");
		int topicDataLength = topicData.length;

		RespWebSocketData<?> data = null;

		if (topicDataLength == 1) {

		} else if (topicDataLength == 2) {
			if (topic.equals(HuobiTopicUtil.getTopicMarketTickers())) {
				// Market Tickers
				Map<String, List<TickerData>> tickerMap = parseMarketTickersData(json);
				// Market Tickers单独处理
				sendMarketTickersToClient(event, tickerMap,ExchangeType.Huobi);
				return;
			}
		} else if (topicDataLength == 3) {
			// Market Detail
			data = parseMarketDetail(topicData[1], json, WebSocketBusiType.TICKER);
			data.setOperateType(WebSocketOperateType.INCREMENT);
		} else if (topicDataLength == 4) {
			String wsType = topicData[2];
			if (wsType.equals("kline")) {
				data = parseKlineData(event, json);
				data.setType(WebSocketBusiType.KLINE);
				data.setKlineTime(HuobiTopicUtil.getKlineTime(topicData[topicData.length - 1]));
				data.setOperateType(WebSocketOperateType.INCREMENT);
			} else if (wsType.equals("depth")) {
				data = parseDepth(json);
				data.setType(WebSocketBusiType.DEPTH_STEP);
				data.setDepthStep(topicData[topicData.length - 1].replace("step", ""));
				data.setOperateType(WebSocketOperateType.FULL);
			} else if (wsType.equals("trade")) {
				data = parseTradeDetailData(event, json);
				data.setType(WebSocketBusiType.TRADE_TICKER);
				data.setOperateType(WebSocketOperateType.INCREMENT);
			}
		}

		data.setEvent(event);

		if (WebSocketEvent.REQ.equals(event)) {
			data.setOperateType(null);
		}

		data.setMD5(MD5.GetMD5Code(JSON.toJSONString(data.getData())));

		String symbol = null;
		if (topicDataLength > 1 && !topicData[1].equals("tickers")) {
			symbol = topicData[1];
			String[] coins = CoinPairUtils.getSymbol(symbol);
			data.setBaseCoin(coins[0]);
			data.setQuoteCoin(coins[1]);
			data.setSymbol(symbol);
		}

		sendDataToClient(data, ExchangeType.Huobi);
	}

	private RespWebSocketData<List<KlineData>> parseKlineData(WebSocketEvent event, Object json) {
		List<HuobiKlineWsResp> datas = null;
		if (event.equals(WebSocketEvent.REQ)) {
			datas = JSON.parseArray(JSON.toJSONString(json), HuobiKlineWsResp.class);
		} else if (event.equals(WebSocketEvent.SUB)) {
			datas = Arrays.asList(JSON.parseObject(JSON.toJSONString(json), HuobiKlineWsResp.class));
		}

		List<KlineData> klineDatas = new ArrayList<KlineData>();
		for (HuobiKlineWsResp data : datas) {
			KlineData klineData = new KlineData();
			klineData.setAmount(data.getVol());
			klineData.setClose(data.getClose());
			klineData.setHigh(data.getHigh());
			klineData.setLow(data.getLow());
			klineData.setOpen(data.getOpen());
			klineData.setVol(data.getAmount());
			klineData.setTimePoint(data.getId());
			klineDatas.add(klineData);
		}

		RespWebSocketData<List<KlineData>> data = new RespWebSocketData<List<KlineData>>();
		data.setData(klineDatas);

		return data;
	}

	private RespWebSocketData<DepthStepData> parseDepth(Object json) {
		HuobiMarketDepthWsResp wsResp = JSON.parseObject(JSON.toJSONString(json), HuobiMarketDepthWsResp.class);
		List<DepthData> bidList = new ArrayList<>();
		List<List<BigDecimal>> bids = wsResp.getBids();
		bids.forEach(bid -> {
			if (CollectionUtils.isEmpty(bid) || bid.size() < 2) {
				return;
			}

			DepthData depthData = new DepthData();
			depthData.setPrice(bid.get(0));
			depthData.setAmount(bid.get(1));
			bidList.add(depthData);
		});

		List<DepthData> askList = new ArrayList<>();
		List<List<BigDecimal>> asks = wsResp.getAsks();
		asks.forEach(ask -> {
			if (CollectionUtils.isEmpty(ask) || ask.size() < 2) {
				return;
			}

			DepthData depthData = new DepthData();
			depthData.setPrice(ask.get(0));
			depthData.setAmount(ask.get(1));
			askList.add(depthData);
		});

		DepthStepData stepData = new DepthStepData();
		stepData.setAsks(askList);
		stepData.setBids(bidList);

		RespWebSocketData<DepthStepData> data = new RespWebSocketData<>();
		data.setData(stepData);
		return data;
	}

	private RespWebSocketData<List<TradeData>> parseTradeDetailData(WebSocketEvent event, Object json) {
		List<HuobiTradeDetailWsResp> wsResps = new ArrayList<>();
		if (event.equals(WebSocketEvent.REQ)) {
			wsResps = JSON.parseArray(JSON.toJSONString(json), HuobiTradeDetailWsResp.class);
		} else if (event.equals(WebSocketEvent.SUB)) {
			JSONObject object = JSON.parseObject(JSON.toJSONString(json));
			wsResps = JSON.parseArray(object.getJSONArray("data").toJSONString(), HuobiTradeDetailWsResp.class);
		}

		List<TradeData> datas = new ArrayList<>();
		wsResps.forEach(wsResp -> {
			TradeData data = new TradeData();
			data.setId(wsResp.getId());
			data.setPrice(wsResp.getPrice());
			data.setVol(wsResp.getAmount());
			data.setSide(ExchangeSide.getEnumByType(wsResp.getDirection()));
			data.setTs(wsResp.getTs());
			datas.add(data);
		});

		RespWebSocketData<List<TradeData>> data = new RespWebSocketData<>();
		data.setData(datas);

		return data;
	}

	private Map<String, List<TickerData>> parseMarketTickersData(Object json) {
		List<HuobiMarketTickersWsResp> resps = JSON.parseArray(JSON.toJSONString(json), HuobiMarketTickersWsResp.class);
		Map<String, List<TickerData>> allData = new HashMap<String, List<TickerData>>();
		resps.forEach(resp -> {
			String symbol = resp.getSymbol();
			String quoteCoin = CoinPairUtils.getQuoteCoin(resp.getSymbol());
			if (StringUtils.isEmpty(quoteCoin)) {
				return;
			}

			String baseCoin = CoinPairUtils.getSymbol(symbol)[0];
			TickerData data = new TickerData();
			data.setBaseCoin(baseCoin);
			data.setHigh(resp.getHigh());
			data.setLast(resp.getClose());
			data.setLow(resp.getLow());
			data.setQuoteCoin(quoteCoin);
			data.setVol(resp.getVol());
			data.setAmount(resp.getAmount());
			data.setCount(resp.getCount());
			data.setOpen(resp.getOpen());
			data.setClose(resp.getClose());
			if(resp.getOpen().compareTo(BigDecimal.ZERO) == 0) {
				data.setChg(BigDecimal.ZERO);
			} else {
				data.setChg(resp.getClose().subtract(resp.getOpen()).divide(resp.getOpen(), 4, BigDecimal.ROUND_DOWN)
						.multiply(new BigDecimal("100")));
			}

			List<TickerData> datas = allData.get(quoteCoin);
			if (CollectionUtils.isEmpty(datas)) {
				datas = new ArrayList<>();
				allData.put(quoteCoin, datas);
			}

			datas.add(data);

		});

		return allData;
	}

	private RespWebSocketData<TickerData> parseMarketDetail(String symbol, Object json, WebSocketBusiType type) {

		HuobiMarketDetailWsResp resp = JSON.parseObject(JSON.toJSONString(json), HuobiMarketDetailWsResp.class);

		String quoteCoin = CoinPairUtils.getQuoteCoin(symbol);
		if (StringUtils.isEmpty(quoteCoin)) {
			return null;
		}

		String baseCoin = CoinPairUtils.getSymbol(symbol)[0];
		TickerData tickerData = new TickerData();
		tickerData.setBaseCoin(baseCoin);
		tickerData.setHigh(resp.getHigh());
		tickerData.setLast(resp.getClose());
		tickerData.setLow(resp.getLow());
		tickerData.setQuoteCoin(quoteCoin);
		tickerData.setVol(resp.getAmount());
		tickerData.setAmount(resp.getAmount());
		tickerData.setCount(resp.getCount());
		tickerData.setOpen(resp.getOpen());
		tickerData.setClose(resp.getClose());
		if(resp.getOpen().compareTo(BigDecimal.ZERO) == 0) {
			tickerData.setChg(BigDecimal.ZERO);
		} else {
			tickerData.setChg(resp.getClose().subtract(resp.getOpen()).divide(resp.getOpen(), 4, BigDecimal.ROUND_DOWN)
					.multiply(new BigDecimal("100")));
		}
		tickerData.setId(resp.getId());

		RespWebSocketData<TickerData> data = new RespWebSocketData<>();
		data.setBaseCoin(baseCoin);
		data.setData(tickerData);
		data.setQuoteCoin(quoteCoin);
		data.setSymbol(symbol);
		data.setType(type);

		return data;
	}


	@Override
	public Map<String, SymbolData> findSymbolData() {
		HuobiSymbolResp resp = httpService.get(huobiUrl + "/v1/common/symbols", HuobiSymbolResp.class);
		if (resp == null) {
			log.error("火币币对信息获取失败");
			return null;
		}

		List<HuobiSymbolData> datas = resp.getData();
		if (CollectionUtils.isEmpty(datas)) {
			log.debug("火币币对信息为空");
			return null;
		}

		Map<String, SymbolData> symbolMap = new HashMap<>();
		datas.forEach(data -> {

			if (!data.getSymbolPartition().equals("main")) {
				return;
			}

			SymbolData symbolData = new SymbolData();
			symbolData.setAmountPrecision(data.getAmountPrecision());
			symbolData.setBaseCoin(data.getBaseCurrency());
			symbolData.setPricePrecision(data.getPricePrecision());
			symbolData.setQuoteCoin(data.getQuoteCurrency());
			symbolData.setSymbol(data.getSymbol());
			symbolMap.put(data.getSymbol(), symbolData);
		});

		return symbolMap;
	}

	@Override
	public String getWebsocketSendMassage(ReqWebsocketParams params) {
		WebSocketBusiType socketBusiType = params.getType();
		String topic = HuobiTopicUtil.getTopic(params.getType(), params.getSymbol(), params.getKlineTime(),
				params.getDepthStep());
		HuobiWsReq wsParams = new HuobiWsReq();

		WebSocketEvent event = params.getEvent();
		if (socketBusiType.equals(WebSocketBusiType.MARKET)) {
			wsParams.setSub(topic);
		} else if (event.equals(WebSocketEvent.REQ)) {
			wsParams.setReq(topic);
		} else if (event.equals(WebSocketEvent.SUB)) {
			wsParams.setSub(topic);
		}

		return JSON.toJSONString(wsParams);
	}

	@Override
	public WebSocketClientService createWebSocketClient() {
		try {
			URI uri = new URI(huobiWsUrl);
			HuobiWebSocketClient client = new HuobiWebSocketClient(uri);
			client.setExchangeType(ExchangeType.Huobi.getCode());
			client.setService(this);
			return client;
		} catch (Exception e) {
			log.error("", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
	}

	@Override
	public void wsReq(ReqWebsocketParams params) {
		subscriptionClientListener.sendMessage(params);
	}

	@Override
	public ExchangeType getExchangeType() {
		return exchangeType;
	}

	@Override
	public DepthStepData findDepthStep(String symbol, String depthStep) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TradeData> findTradeTicker(String symbol) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TickerData findTicker24(String symbol) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<KlineData> findKline(String symbol, String klineTime) {
		// TODO Auto-generated method stub
		return null;
	}
}
