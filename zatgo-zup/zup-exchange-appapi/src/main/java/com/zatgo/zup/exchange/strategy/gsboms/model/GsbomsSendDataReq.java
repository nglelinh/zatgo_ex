package com.zatgo.zup.exchange.strategy.gsboms.model;

import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;

public class GsbomsSendDataReq {

	private WebSocketEvent event;
	
	private String topic;
	
	private String api;

	public WebSocketEvent getEvent() {
		return event;
	}

	public void setEvent(WebSocketEvent event) {
		this.event = event;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}
	
	
}
