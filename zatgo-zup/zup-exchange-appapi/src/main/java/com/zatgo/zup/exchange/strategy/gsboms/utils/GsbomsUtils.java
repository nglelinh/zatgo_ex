package com.zatgo.zup.exchange.strategy.gsboms.utils;

import com.zatgo.zup.common.enumtype.ExchangeEnum.KlineTime;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;

public class GsbomsUtils {

	public static KlineTime getKlineTimeByTopic(String topic) {
		KlineTime time = null;
		if (topic.endsWith(".60")) {
			time = KlineTime.MIN;
		}else if(topic.endsWith(".300")) {
			time = KlineTime.MIN5;
		}else if(topic.endsWith(".900")) {
			time = KlineTime.MIN15;
		}else if(topic.endsWith(".1800")) {
			time = KlineTime.MIN30;
		}else if(topic.endsWith(".3600")) {
			time = KlineTime.MIN60;
		}else if(topic.endsWith(".86400")) {
			time = KlineTime.DAY;
		}else if(topic.endsWith(".604800")) {
			time = KlineTime.WEEK;
		}else if(topic.endsWith(".2419200")) {
			time = KlineTime.MONTH;
		}else {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"无法解析的topic=" + topic);
		}
		
		return time;
	}
	
	public static String getTopicKline(String symbol, String period) {
		if (period.equals(KlineTime.MIN.getCode())) {
			period = "60";
		} else if (period.equals(KlineTime.MIN5.getCode())) {
			period = "300";
		} else if (period.equals(KlineTime.MIN15.getCode())) {
			period = "900";
		} else if (period.equals(KlineTime.MIN30.getCode())) {
			period = "1800";
		} else if (period.equals(KlineTime.MIN60.getCode())) {
			period = "3600";
		} else if (period.equals(KlineTime.DAY.getCode())) {
			period = "86400";
		} else if (period.equals(KlineTime.WEEK.getCode())) {
			period = "604800";
		} else if (period.equals(KlineTime.MONTH.getCode())) {
			period = "2419200";
		}
		return "hb." + symbol + "." + period;
	}
	
	public static String getKlineSymbolByTopic(String topic) {
		return topic.substring(topic.indexOf(".")+1, topic.lastIndexOf("."));
	}
	
	public static String getDepthSymbolByTopic(String topic) {
		return topic.substring(topic.indexOf(".")+1, topic.length());
	}
	
	public static String getTradeSymbolByTopic(String topic) {
		return topic.substring(topic.indexOf(".")+1, topic.length());
	}
}
