package com.zatgo.zup.exchange.appapi.rocketMQ;

import com.zatgo.zup.common.mq.MQConsumer;
import com.zatgo.zup.common.mq.MQContants;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by 46041 on 2018/9/25.
 */

@Component
public class CreateOrderConsumer {

    private static final Logger logger = LoggerFactory.getLogger(CreateOrderConsumer.class);

    @Autowired
    private MQConsumer consumer;
    @Autowired
    private CreateOrderCallBack createOrderCallBack;
    @PostConstruct
    public void consume(){
        try {
            consumer.startConsumer(MQContants.CONSUMER_GROUP_ORDER_CALLBACK, MQContants.ZUP_ORDER_CREATE_TOPIC,
                    null, createOrderCallBack, MessageModel.CLUSTERING);
        } catch (Exception e) {
            logger.error("",e);
        }
    }
}
