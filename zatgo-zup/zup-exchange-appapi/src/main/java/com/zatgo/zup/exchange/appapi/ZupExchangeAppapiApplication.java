package com.zatgo.zup.exchange.appapi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zatgo.zup.exchange.appapi.util.CloudUserInfoMap;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
@EnableSwagger2
@MapperScan("com.zatgo.zup.exchange.mapper")
@ComponentScan(basePackages = "com.zatgo.zup")
@EnableDiscoveryClient
@EnableTransactionManagement
@EnableScheduling
@Configuration
public class ZupExchangeAppapiApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(ZupExchangeAppapiApplication.class, args);
	}

}