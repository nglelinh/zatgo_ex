package com.zatgo.zup.exchange.business;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.ExOrderDetailData;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.entity.ExOrder;

public interface OrderService {

	/**
	 * 查询未完成的订单数据（分页）
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public PageInfo<ExOrder> findUnfinishedByPage(int pageNo,int pageSize);
	
	/**
	 * 同步订单数据
	 * @param orderData
	 */
	public void syncOrder(ExOrderDetailData orderData);
	
	/**
	 * 创建订单
	 * @param params
	 * @return
	 */
	public ExOrder createOrder(ExOrder order);

	/**
	 * 取消订单
	 * @param params
	 * @return
	 */
	public OrderData cancelOrder(CancelOrderParams params,String userId);

	/**
	 * 订单查询
	 * @param params
	 * @return
	 */
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params,String userId);

	/**
	 * 获取订单明细
	 * @param orderId
	 * @return
	 */
	public OrderData getOrderByOrderId(String orderId,String userId);
	
	/**
	 * 查询订单
	 * @param params
	 * @return
	 */
	public PageInfo<ExOrder> findOrderBySearchParams(OrderSearchParams params);
}
