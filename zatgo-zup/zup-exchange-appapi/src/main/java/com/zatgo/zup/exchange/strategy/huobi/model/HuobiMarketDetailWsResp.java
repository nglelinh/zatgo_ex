package com.zatgo.zup.exchange.strategy.huobi.model;

import java.math.BigDecimal;

public class HuobiMarketDetailWsResp {
	
	/**
	 * 消息id
	 */
	private String id;
	
	/**
	 * 24小时统计时间
	 */
    private Long ts;
    
    /**
     * 24小时成交量
     */
    private BigDecimal amount;
    
    /**
     * 前推24小时成交价
     */
    private BigDecimal open;
    
    /**
     * 当前成交价
     */
    private BigDecimal close;
    
    /**
     * 近24小时最高价
     */
    private BigDecimal high;// ,
    
    /**
     * 近24小时最低价
     */
    private BigDecimal low;
    
    /**
     * 近24小时累积成交数
     */
    private BigDecimal count;
    
    /**
     * 近24小时累积成交额, 即 sum(每一笔成交价 * 该笔的成交量)
     */
    private BigDecimal vol;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getOpen() {
		return open;
	}

	public void setOpen(BigDecimal open) {
		this.open = open;
	}

	public BigDecimal getClose() {
		return close;
	}

	public void setClose(BigDecimal close) {
		this.close = close;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public BigDecimal getCount() {
		return count;
	}

	public void setCount(BigDecimal count) {
		this.count = count;
	}

	public BigDecimal getVol() {
		return vol;
	}

	public void setVol(BigDecimal vol) {
		this.vol = vol;
	}

}
