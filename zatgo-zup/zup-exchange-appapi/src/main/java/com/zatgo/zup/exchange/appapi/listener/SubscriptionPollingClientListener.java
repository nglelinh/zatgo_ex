//package com.zatgo.zup.exchange.appapi.listener;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.TimeUnit;
//
//import javax.annotation.PostConstruct;
//
//import org.apache.commons.collections4.CollectionUtils;
//import org.apache.commons.collections4.MapUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.HashOperations;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Component;
//
//import com.alibaba.fastjson.JSON;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.DepthStep;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.KlineTime;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
//import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketOperateType;
//import com.zatgo.zup.common.redis.RedisKeyConstants;
//import com.zatgo.zup.common.utils.MD5;
//import com.zatgo.zup.exchange.appapi.controller.ApiWebSocketController;
//import com.zatgo.zup.exchange.appapi.model.CoinPair;
//import com.zatgo.zup.exchange.appapi.model.CoinPairData;
//import com.zatgo.zup.exchange.appapi.model.DepthStepData;
//import com.zatgo.zup.exchange.appapi.model.KlineData;
//import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
//import com.zatgo.zup.exchange.appapi.model.TickerData;
//import com.zatgo.zup.exchange.appapi.model.TradeData;
//import com.zatgo.zup.exchange.appapi.util.WebSocketSessionUtil;
//import com.zatgo.zup.exchange.entity.CoinPairConfig;
//import com.zatgo.zup.exchange.service.CommonService;
//import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
//import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
//
//@Component
//public class SubscriptionPollingClientListener {
//
//	private static final Logger log = LoggerFactory.getLogger(SubscriptionPollingClientListener.class);
//
//	private static ExecutorService executor = Executors.newFixedThreadPool(10);
//
//	@Autowired
//	private RedisTemplate redisTemplate;
//
//	@Autowired
//	private ApiWebSocketController apiWebSocketController;
//
//	@Autowired
//	private ExchangeStrategy exchangeStrategyService;
//
//	@Autowired
//	private CommonService commonService;
//
//	public List<CoinPairConfig> getCoinPair() {
//
//		return commonService.getCoinPairCache();
//	}
//
//	public List<String> getExchangeType() {
//		return commonService.getExchangeType();
//	}
//
//	@PostConstruct
//	public void init() {
//
//		log.info("行情服务功能启动");
//
//		// 订阅-k线行情
//		Runnable kline = new Runnable() {
//
//			@Override
//			public void run() {
//				while (true) {
//					try {
//
//						List<String> exchangeType = getExchangeType();
//						if (CollectionUtils.isNotEmpty(exchangeType)) {
//							for (String type : exchangeType) {
//								subKlineData(ExchangeType.getEnumByType(type), null);
//							}
//						}
//
//						subDirectKlineData();
//
//						TimeUnit.SECONDS.sleep(5);
//					} catch (InterruptedException e) {
//						log.error("订阅-k线行情线程等待失败：", e);
//					} catch (Exception e) {
//						log.error("订阅-前24小时行情失败：", e);
//					}
//				}
//
//			}
//		};
//		executor.execute(kline);
//
//		// 订阅-前24小时行情
//		Runnable ticker = new Runnable() {
//
//			@Override
//			public void run() {
//				while (true) {
//					try {
//
//						List<String> exchangeType = getExchangeType();
//						if (CollectionUtils.isNotEmpty(exchangeType)) {
//							for (String type : exchangeType) {
//								subTickerData(ExchangeType.getEnumByType(type), null);
//							}
//						}
//
//						subDirectTickerData();
//
//						TimeUnit.SECONDS.sleep(5);
//					} catch (InterruptedException e) {
//						log.error("订阅-前24小时行情线程等待失败：", e);
//					} catch (Exception e) {
//						log.error("订阅-前24小时行情失败：", e);
//					}
//				}
//
//			}
//		};
//		executor.execute(ticker);
//
//		// 订阅-实时成交信息
//		Runnable tradeTicker = new Runnable() {
//
//			@Override
//			public void run() {
//				while (true) {
//					try {
//
//						List<String> exchangeType = getExchangeType();
//						if (CollectionUtils.isNotEmpty(exchangeType)) {
//							for (String type : exchangeType) {
//								subTradeTicker(ExchangeType.getEnumByType(type), null);
//							}
//						}
//
//						subDirectTradeTickerData();
//
//						TimeUnit.SECONDS.sleep(5);
//					} catch (InterruptedException e) {
//						log.error("订阅-实时成交信息线程等待失败：", e);
//					} catch (Exception e) {
//						log.error("订阅-实时成交信息失败：", e);
//					}
//				}
//
//			}
//		};
//		executor.execute(tradeTicker);
//
//		// 订阅-币对的深度
//		Runnable depthStep = new Runnable() {
//			@Override
//			public void run() {
//				while (true) {
//					try {
//
//						List<String> exchangeType = getExchangeType();
//						if (CollectionUtils.isNotEmpty(exchangeType)) {
//							for (String type : exchangeType) {
//								subDepthStepData(ExchangeType.getEnumByType(type), null);
//							}
//						}
//
//						subDirectDepthStepData();
//
//						TimeUnit.SECONDS.sleep(5);
//					} catch (InterruptedException e) {
//						log.error("订阅-深度盘口线程等待失败：", e);
//					} catch (Exception e) {
//						log.error("订阅-深度盘口失败：", e);
//					}
//				}
//
//			}
//		};
//		executor.execute(depthStep);
//
//		// 订阅-首页市场币对的最新价和涨幅情况
//		Runnable market = new Runnable() {
//			@Override
//			public void run() {
//				while (true) {
//					try {
//
//						List<CoinPairConfig> configs = getCoinPair();
//						if (CollectionUtils.isNotEmpty(configs)) {
//							subMarketData(configs);
//						}
//
//						subDirectMarketData();
//
//						TimeUnit.SECONDS.sleep(5);
//					} catch (InterruptedException e) {
//						log.error("订阅-首页市场币对的最新价和涨幅情况线程等待失败：", e);
//					} catch (Exception e) {
//						log.error("订阅-首页市场币对的最新价和涨幅情况失败：", e);
//					}
//				}
//
//			}
//		};
//		executor.execute(market);
//
//		log.info("行情服务功能启动完成");
//	}
//
//	/**
//	 * 获取具体业务实现类
//	 * 
//	 * @param config
//	 * @return
//	 */
//	private WebSocketStrategyService getWebSocketStrategyService(CoinPairConfig config) {
//		return exchangeStrategyService
//				.getApiWebSocketService(ExchangeType.getEnumByType(config.getExternalExchangeType()));
//	}
//
//	/**
//	 * 发送成交信息到客户端
//	 * @param config
//	 * @param data
//	 * @param exchangeType
//	 */
//	public <T> void sendTradeTicker(CoinPairConfig config, T data, ExchangeType exchangeType) {
//
//		RespWebSocketData<T> resp = new RespWebSocketData<T>();
//
//		resp.setData(data);
//
//		resp.setBaseCoin(config.getBaseCoin());
//		resp.setQuoteCoin(config.getQuoteCoin());
//		resp.setSymbol(config.getSymbol());
//
//		if (exchangeType != null) {
//			resp.setExchangeType(exchangeType);
//		}
//
//		resp.setType(WebSocketBusiType.TRADE_TICKER);
//		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));
//
//		resp.setOperateType(WebSocketOperateType.FULL);
//		apiWebSocketController.pushMessageToClient(resp);
//	}
//
//	public <T> void sendTicker(CoinPairConfig config, T data, ExchangeType exchangeType) {
//
//		RespWebSocketData<T> resp = new RespWebSocketData<T>();
//
//		resp.setData(data);
//
//		resp.setBaseCoin(config.getBaseCoin());
//		resp.setQuoteCoin(config.getQuoteCoin());
//		resp.setSymbol(config.getSymbol());
//
//		if (exchangeType != null) {
//			resp.setExchangeType(exchangeType);
//		}
//
//		resp.setType(WebSocketBusiType.TICKER);
//		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));
//
//		resp.setOperateType(WebSocketOperateType.FULL);
//		apiWebSocketController.pushMessageToClient(resp);
//	}
//
//	private <T> void sendMarket(String quoteCoin, T data, ExchangeType exchangeType) {
//
//		RespWebSocketData<T> resp = new RespWebSocketData<T>();
//		resp.setData(data);
//		if (quoteCoin != null) {
//			resp.setQuoteCoin(quoteCoin);
//		}
//		if (exchangeType != null) {
//			resp.setExchangeType(exchangeType);
//		}
//		resp.setType(WebSocketBusiType.MARKET);
//		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));
//
//		resp.setOperateType(WebSocketOperateType.INCREMENT);
//		apiWebSocketController.pushMessageToClient(resp);
//	}
//
//	public <T> void sendDepthStep(CoinPairConfig config, T data, String depthStep, ExchangeType exchangeType) {
//
//		RespWebSocketData<T> resp = new RespWebSocketData<T>();
//
//		resp.setData(data);
//
//		resp.setDepthStep(depthStep);
//
//		resp.setBaseCoin(config.getBaseCoin());
//		resp.setQuoteCoin(config.getQuoteCoin());
//		resp.setSymbol(config.getSymbol());
//
//		resp.setExchangeType(exchangeType);
//
//		resp.setType(WebSocketBusiType.DEPTH_STEP);
//		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));
//
//		resp.setOperateType(WebSocketOperateType.FULL);
//		apiWebSocketController.pushMessageToClient(resp);
//	}
//
//	public <T> void sendKlineTime(CoinPairConfig config, T data, String klineTime, ExchangeType exchangeType) {
//
//		RespWebSocketData<T> resp = new RespWebSocketData<T>();
//
//		resp.setData(data);
//		resp.setKlineTime(klineTime);
//
//		resp.setBaseCoin(config.getBaseCoin());
//		resp.setQuoteCoin(config.getQuoteCoin());
//		resp.setSymbol(config.getSymbol());
//
//		resp.setExchangeType(exchangeType);
//
//		resp.setType(WebSocketBusiType.KLINE);
//		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));
//
//		resp.setOperateType(WebSocketOperateType.FULL);
//		apiWebSocketController.pushMessageToClient(resp);
//	}
//
//	/**
//	 * 获取目前直连的交易所
//	 * @return
//	 */
//	private Set<ExchangeType> getDirectExchangeSet() {
//		List<ExchangeType> exchangeTypeList = WebSocketSessionUtil.getDirectExchangeTypeList();
//		if (CollectionUtils.isEmpty(exchangeTypeList)) {
//			return null;
//		}
//
//		Set<ExchangeType> exchangeTypeSet = new HashSet<ExchangeType>(exchangeTypeList);
//		return exchangeTypeSet;
//	}
//
//	/**
//	 * 订阅-直连模式-Kline
//	 */
//	private void subDirectKlineData() {
//
//		Set<ExchangeType> exchangeTypeSet = getDirectExchangeSet();
//		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
//			return;
//		}
//
//		for (ExchangeType exchangeType : exchangeTypeSet) {
//			subKlineData(exchangeType, exchangeType);
//		}
//	}
//
//
//	/**
//	 * 订阅-直连模式-前24小时行情
//	 */
//	private void subDirectTickerData() {
//
//		Set<ExchangeType> exchangeTypeSet = getDirectExchangeSet();
//		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
//			return;
//		}
//
//		for (ExchangeType exchangeType : exchangeTypeSet) {
//
//			subTickerData(exchangeType, exchangeType);
//		}
//	}
//
//
//	/**
//	 * 订阅-直连模式-成交信息
//	 */
//	private void subDirectTradeTickerData() {
//
//		Set<ExchangeType> exchangeTypeSet = getDirectExchangeSet();
//		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
//			return;
//		}
//
//		for (ExchangeType exchangeType : exchangeTypeSet) {
//			
//			subTradeTicker(exchangeType, exchangeType);
//		}
//
//	}
//
//
//	/**
//	 * 订阅-直连模式-深度
//	 */
//	private void subDirectDepthStepData() {
//
//		Set<ExchangeType> exchangeTypeSet = getDirectExchangeSet();
//		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
//			return;
//		}
//
//		for (ExchangeType exchangeType : exchangeTypeSet) {
//			subDepthStepData(exchangeType, exchangeType);
//		}
//
//	}
//
//	/**
//	 * 订阅-市场行情
//	 * 
//	 * @param configs
//	 */
//	private void subMarketData(List<CoinPairConfig> configs) {
//
//		Map<String, List<TickerData>> allData = getMarketDataMap(configs);
//		if (MapUtils.isEmpty(allData)) {
//			return;
//		}
//
//		allData.forEach((key, value) -> {
//			Map<String, List<TickerData>> data = new HashMap<>();
//			data.put(key, value);
//			sendMarket(key, data, null);
//		});
//
//		sendMarket(null, allData, null);
//	}
//
//	
//
//	/**
//	 * 订阅-直连模式-市场行情
//	 */
//	private void subDirectMarketData() {
//		Set<ExchangeType> exchangeTypeSet = getDirectExchangeSet();
//		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
//			return;
//		}
//
//		for (ExchangeType exchangeType : exchangeTypeSet) {
//
//			Map<String, List<TickerData>> allData = getMarketDataMap(exchangeType);
//			if (MapUtils.isEmpty(allData)) {
//				return;
//			}
//
//			allData.forEach((key, value) -> {
//				Map<String, List<TickerData>> data = new HashMap<>();
//				data.put(key.toLowerCase(), value);
//				sendMarket(key.toLowerCase(), data, exchangeType);
//			});
//
//			sendMarket(null, allData, exchangeType);
//		}
//	}
//
//	/**
//	 * 订阅-Kline
//	 * @param exchangeType
//	 * @param externalExchangeType
//	 */
//	private void subKlineData(ExchangeType exchangeType, ExchangeType externalExchangeType) {
//		
//		WebSocketStrategyService service = exchangeStrategyService.getApiWebSocketService(exchangeType);
//		service.subKline(exchangeType, externalExchangeType);
//	}
//
//	/**
//	 * 订阅前-24小时行情
//	 * @param exchangeType
//	 * @param externalExchangeType
//	 */
//	private void subTickerData(ExchangeType exchangeType, ExchangeType externalExchangeType) {
//
//		WebSocketStrategyService service = exchangeStrategyService.getApiWebSocketService(exchangeType);
//		service.subTicker(exchangeType, externalExchangeType);
//	}
//	
//	/**
//	 * 订阅-成交信息
//	 * @param exchangeType
//	 * @param externalExchangeType
//	 */
//	private void subTradeTicker(ExchangeType exchangeType, ExchangeType externalExchangeType) {
//		
//		WebSocketStrategyService service = exchangeStrategyService.getApiWebSocketService(exchangeType);
//		service.subTradeTicker(exchangeType, externalExchangeType);
//	}
//	
//	/**
//	 * 订阅-深度
//	 * @param exchangeType
//	 * @param externalExchangeType
//	 */
//	private void subDepthStepData(ExchangeType exchangeType, ExchangeType externalExchangeType) {
//		
//		WebSocketStrategyService service = exchangeStrategyService.getApiWebSocketService(exchangeType);
//		service.subDepthStepData(exchangeType, externalExchangeType);
//	}
//	
//	
//	/**
//	 * 获取市场行情缓存
//	 * @param configs
//	 * @return
//	 */
//	public Map<String, List<TickerData>> getMarketDataMap(List<CoinPairConfig> configs) {
//
//		Set<String> quoteCoin = new HashSet<>();
//		configs.forEach(config -> {
//			quoteCoin.add(config.getQuoteCoin());
//		});
//
//		Map<String, List<TickerData>> allData = new HashMap<String, List<TickerData>>();
//		for (String coin : quoteCoin) {
//			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
//			List<String> tickerDatas = hashOperations.values(RedisKeyConstants.COIN_TICKER + coin + "_");
//			if (CollectionUtils.isEmpty(tickerDatas)) {
//				break;
//			}
//
//			List<TickerData> res = new ArrayList<TickerData>();
//			for (String _data : tickerDatas) {
//				res.add(JSON.parseObject(_data, TickerData.class));
//			}
//
//			allData.put(coin, res);
//		}
//
//		return allData;
//	}
//	
//	/**
//	 * 获取市场行情缓存
//	 * @param exchangeType
//	 * @return
//	 */
//	public Map<String, List<TickerData>> getMarketDataMap(ExchangeType exchangeType) {
//		CoinPairData coinPairData = commonService.getDirectCoinPair(exchangeType);
//
//		List<String> quoteCoinList = coinPairData.getQuoteCoinList();
//
//		Map<String, List<TickerData>> allData = new HashMap<String, List<TickerData>>();
//		if (CollectionUtils.isEmpty(quoteCoinList)) {
//			return allData;
//		}
//
//		for (String quoteCoin : quoteCoinList) {
//			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
//			List<String> tickerDatas = hashOperations.values(
//					RedisKeyConstants.COIN_TICKER + exchangeType.getCode() + "_" + quoteCoin.toLowerCase() + "_");
//			if (CollectionUtils.isEmpty(tickerDatas)) {
//				return allData;
//			}
//
//			List<TickerData> res = new ArrayList<TickerData>();
//			for (String _data : tickerDatas) {
//				res.add(JSON.parseObject(_data, TickerData.class));
//			}
//
//			Map<String, List<TickerData>> data = new HashMap<>();
//			data.put(quoteCoin.toLowerCase(), res);
//
//			allData.put(quoteCoin.toLowerCase(), res);
//		}
//
//		return allData;
//	}
//
//}
