package com.zatgo.zup.exchange.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.BusinessEnum.CoinPairConfigStatus;
import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeQuoteCoin;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.ExchangeUpdateBalanceParams;
import com.zatgo.zup.common.model.LockWalletBalanceParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.exchange.appapi.model.CloudUserCoinPairConfigData;
import com.zatgo.zup.exchange.appapi.model.CoinPair;
import com.zatgo.zup.exchange.appapi.model.CoinPairData;
import com.zatgo.zup.exchange.appapi.model.RateData;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.appapi.model.TickerData;
import com.zatgo.zup.exchange.appapi.remoteservice.PayRemoteService;
import com.zatgo.zup.exchange.appapi.util.CloudUserInfoMap;
import com.zatgo.zup.exchange.entity.CloudUserCoinPair;
import com.zatgo.zup.exchange.entity.CloudUserCoinPairExample;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.entity.ExOrder;
import com.zatgo.zup.exchange.mapper.CloudUserCoinPairMapper;
import com.zatgo.zup.exchange.mapper.CoinPairConfigMapper;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
import com.zatgo.zup.exchange.task.ratetask.SymbolRateTask;
import com.zatgo.zup.exchange.task.ratetask.SymbolTask;

@Service
public class CommonServiceImpl implements CommonService {

	private static Logger log = LoggerFactory.getLogger(CommonServiceImpl.class);

	@Autowired
	private CoinPairConfigMapper coinPairConfigMapper;

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private PayRemoteService payRemoteService;

	@Autowired
	private ExchangeStrategy exchangeStrategy;

	@Autowired
	private CloudUserCoinPairMapper cloudUserCoinPairMapper;
	
	@Autowired
	private RedisLockUtils lockUtils;
	
	@Autowired
    private SymbolRateTask symbolRateTask;
	
	@Autowired
    private SymbolTask symbolTask;
	
	@Autowired
	private CloudUserInfoMap cloudUserInfoMap;

	@Override
	public CoinPairData getDirectCoinPair(ExchangeType exchangeType) {

		String key = RedisKeyConstants.COIN_PAIR_CACHE + "_" + exchangeType.getCode();

		ValueOperations<String, String> operations = redisTemplate.opsForValue();
		String res = operations.get(key);
		if (StringUtils.isNotEmpty(res)) {
			CoinPairData data = JSON.parseObject(res, CoinPairData.class);
			return data;
		}

		WebSocketStrategyService service = exchangeStrategy.getApiWebSocketService(exchangeType);
		Map<String, SymbolData> symbolMap = service.findSymbolData();
		if (MapUtils.isEmpty(symbolMap)) {
			return new CoinPairData();
		}

		Set<String> quoteCoinSet = new HashSet<>();
		Map<String, List<CoinPair>> _data = new HashMap<String, List<CoinPair>>();
		List<SymbolData> symbolValue = new ArrayList<>(symbolMap.values());
		for (SymbolData symbolData : symbolValue) {

			String quoteCoin = symbolData.getQuoteCoin();
			List<CoinPair> coinPairs = _data.get(quoteCoin.toLowerCase());
			if (CollectionUtils.isEmpty(coinPairs)) {
				coinPairs = new ArrayList<>();
			}

			CoinPair coinPair = new CoinPair();
			coinPair.setAmountPrecision(symbolData.getAmountPrecision());
			coinPair.setBaseCoin(symbolData.getBaseCoin().toLowerCase());
			coinPair.setPricePrecision(symbolData.getPricePrecision());
			coinPair.setQuoteCoin(symbolData.getQuoteCoin().toLowerCase());
			coinPair.setSymbol(symbolData.getSymbol().toLowerCase());
			coinPairs.add(coinPair);
			_data.put(quoteCoin.toLowerCase(), coinPairs);

			quoteCoinSet.add(quoteCoin.toLowerCase());
		}

		CoinPairData data = new CoinPairData();
		data.setPairs(_data);
		data.setQuoteCoinList(new ArrayList<String>(quoteCoinSet));

		operations.set(key, JSON.toJSONString(data), 1, TimeUnit.SECONDS);

		return data;
	}

	@Override
	public CoinPairData getLocalCoinPair(String cloudUserId) {
		ValueOperations<String, String> operations = redisTemplate.opsForValue();
		String symbolStr = operations.get(RedisKeyConstants.SYMBOL);
		Map<String,Map<String, Map<String, SymbolData>>> symbolMap = JSON.parseObject(symbolStr, Map.class);

		List<CloudUserCoinPairConfigData> pairs = getCloudUserCoinPairCache(cloudUserId);
		
		if (CollectionUtils.isEmpty(pairs) || MapUtils.isEmpty(symbolMap)) {
			pairs = new ArrayList<>();
		}

		Set<String> quoteCoinList = new HashSet<String>();
		Map<String, List<CoinPair>> _data = new HashMap<String, List<CoinPair>>();
		
		for (CloudUserCoinPairConfigData data : pairs) {
			CoinPair pair = new CoinPair();
			String quoteCoin = data.getQuoteCoin().toLowerCase();
			List<CoinPair> _pairs = _data.get(quoteCoin);
			if (_pairs == null) {
				_pairs = new ArrayList<>();
				_data.put(quoteCoin, _pairs);
				quoteCoinList.add(quoteCoin);
			}

			if (MapUtils.isNotEmpty(symbolMap)) {
				
				Map<String,Map<String, SymbolData>> _symbolMapExchangeType = symbolMap.get(quoteCoin);
				if (MapUtils.isEmpty(_symbolMapExchangeType))
					continue;
				
				if(data.getExternalExchangeType() == null) {
					log.error("云用户="+cloudUserId + ",币对=" + data.getSymbol().toLowerCase() + "没有配置对应的交易所");
					continue;
				}
				
				Map<String, SymbolData> _symbolMap = _symbolMapExchangeType.get(data.getExternalExchangeType());
				if(_symbolMap == null) {
					continue;
				}
				
				Object symbolDataObj =_symbolMap.get(data.getSymbol().toLowerCase());
				if(symbolDataObj == null) {
					continue;
				}
				
				SymbolData symbol = JSON.parseObject(JSON.toJSONString(symbolDataObj),
						SymbolData.class);
				pair.setBaseCoin(symbol.getBaseCoin());
				pair.setQuoteCoin(symbol.getQuoteCoin());
				pair.setSymbol(symbol.getSymbol());
				pair.setPricePrecision(symbol.getPricePrecision());
				pair.setAmountPrecision(symbol.getAmountPrecision());
				pair.setCoinImage(data.getCoinImage());
			
			}

			if (pair.getAmountPrecision() == null || pair.getPricePrecision() == null) {
				continue;
			}

			_pairs.add(pair);
		}
		
		if(MapUtils.isNotEmpty(_data)) {
			_data.forEach((key, value) -> {
				value.sort((low, high) -> {
					
					if(low.getSort() == null && high.getSort() == null) {
						return 0;
					}
					
					if(low.getSort() != null && high.getSort() == null) {
						return -1;
					}
					
					if(low.getSort() == null && high.getSort() != null) {
						return 1;
					}
					
					return low.getSort().compareTo(high.getSort());
				});
			});
		}

		CoinPairData res = new CoinPairData();
		res.setPairs(_data);
		res.setQuoteCoinList(new ArrayList<String>(quoteCoinList));

		return res;
	}

	@Override
	public RateData getCoinRateData(ExchangeType type,String cloudUserId) {
		return getCoinRate(type,cloudUserId);
	}

	@Override
	public void refreshCache() {
		rerefreshCoinPair();
	}

	public void rerefreshCoinPair() {

		ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
		redisTemplate.delete(RedisKeyConstants.SYMBOL);
		redisTemplate.delete(RedisKeyConstants.EXCHANGE_TYPE);
		redisTemplate.delete(RedisKeyConstants.COIN_PAIR_CACHE);
		
		Set<String> keys = redisTemplate.keys(RedisKeyConstants.CLOUD_USER_COIN_PAIR_CACHE + ":*");
		redisTemplate.delete(keys);
		
		keys = redisTemplate.keys(RedisKeyConstants.COIN_PAIR_CACHE + "_" + "*");
		redisTemplate.delete(keys);
//		
//		for (ExchangeType exchangeType : ExchangeType.values()) {
//			redisTemplate.delete(RedisKeyConstants.COIN_PAIR_CACHE + "_" + exchangeType);
//		}
		
		symbolRateTask.updateSymbolRateToRedis();
		symbolTask.updateSymbol();
		cloudUserInfoMap.initCoinPair();
		
	}

	@Override
	public void unLockAccountBalance(ExOrder order) {
		ExchangeUpdateBalanceParams balanceParams = new ExchangeUpdateBalanceParams();
		String coinType = null;
		BigDecimal lockAmount = BigDecimal.ZERO;
		BigDecimal dealAmount = order.getDealVolume() == null ? BigDecimal.ZERO : order.getDealVolume();
		if (order.getSide().equals(ExchangeSide.BUY.getCode())) {
			coinType = order.getQuoteCoin();

			if (order.getType().equals(new Byte(EntryOrdersType.limitPriceEntrust.getCode()))) {
				lockAmount = order.getPrice().multiply(order.getVolume().subtract(dealAmount));
			} else if (order.getType().equals(new Byte(EntryOrdersType.marketPriceEntrust.getCode()))) {
				lockAmount = order.getVolume().subtract(dealAmount);
			}

		} else if (order.getSide().equals(ExchangeSide.SELL.getCode())) {
			coinType = order.getBaseCoin();
			lockAmount = order.getVolume().subtract(dealAmount);
		}

		LockWalletBalanceParams walletParams = new LockWalletBalanceParams();
		walletParams.setCoinType(coinType.toUpperCase());
		walletParams.setLockAmount(lockAmount);
		walletParams.setLockBalanceType(BusinessEnum.LockBalanceOperateType.UNLOCK);
		walletParams.setOrderId(order.getOrderId());
		walletParams.setUserId(order.getUserId());
		walletParams.setUserName("");

		ResponseData<Object> data = payRemoteService.updateLockBalance(walletParams);
		if (!BusinessExceptionCode.SUCCESS_CODE.equals(data.getCode())
				&& !BusinessExceptionCode.REPEAT_OPERATION_BALANCE.equals(data.getCode())) {
			log.error("解除用户冻结余额失败：" + JSON.toJSONString(data));
			throw new BusinessException(data.getCode(), data.getMessage());
		}
	}

	@Override
	public SymbolData getSymbolDataCache(String baseCoin, String quoteCoin,ExchangeType type) {

		String symbol = (baseCoin + quoteCoin).toLowerCase();

		ValueOperations<String, String> operations = redisTemplate.opsForValue();
		String symbolStr = operations.get(RedisKeyConstants.SYMBOL);
		
		
		Map<String,Map<String, Map<String, SymbolData>>> symbolMap = JSON.parseObject(symbolStr, Map.class);
		if (MapUtils.isEmpty(symbolMap)) {
			return null;
		}
		
		Map<String, Map<String, SymbolData>> symbolMapExchangeType = symbolMap.get(type.getCode());
		if (MapUtils.isEmpty(symbolMapExchangeType)) {
			return null;
		}

		Map<String, SymbolData> _symbolMap = symbolMapExchangeType.get(quoteCoin.toLowerCase());
		if (MapUtils.isEmpty(_symbolMap)) {
			return null;
		}

		SymbolData symbolData = JSON.parseObject(JSON.toJSONString(_symbolMap.get(symbol)), SymbolData.class);

		return symbolData;
	}

	@Override
	public List<CloudUserCoinPairConfigData> getCloudCoinPairByCloudUserId(String cloudUserId) {
		return cloudUserCoinPairMapper.selectCloudUserCoinPairConfigList(cloudUserId, CoinPairConfigStatus.OPEN.getCode().intValue());
	}


	private RateData getCoinRate(ExchangeType type,String cloudUserId) {

		String redisKey = RedisKeyConstants.COIN_RATE + cloudUserId + "_ALL_COIN";
		if (type != null) {
			redisKey += "_" + type.getCode();
		}

		ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();

		String cache = valueOperations.get(redisKey);
		if (StringUtils.isNotEmpty(cache)) {
			return JSON.parseObject(cache, RateData.class);
		}

		String rateStr = valueOperations.get(RedisKeyConstants.COIN_RATE);
		if (StringUtils.isEmpty(rateStr)) {
			return null;
		}

		RateData rateData = JSON.parseObject(rateStr, RateData.class);
		if (rateData == null) {
			return null;
		}

		Map<String, Map<String, BigDecimal>> rateMap = rateData.getRates();
		if (MapUtils.isEmpty(rateMap)) {
			return null;
		}

		CoinPairData coinPairData = null;
		if(type == null) {
			coinPairData = getLocalCoinPair(cloudUserId);
		}else {
			coinPairData = getDirectCoinPair(type);
		}
		

		// 获取币对数据
		Map<String, List<CoinPair>> pairs = coinPairData.getPairs();

		Set<String> coinSet = new HashSet<>();

		HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();

		// 循环法币费率
		Set<Entry<String, Map<String, BigDecimal>>> rateEntry = rateMap.entrySet();
		for (Entry<String, Map<String, BigDecimal>> rate : rateEntry) {

			String _legalCoin = rate.getKey();

			Map<String, BigDecimal> newRateMap = new HashMap<>();
			// 获取对应虚拟币费率
			Set<Entry<String, BigDecimal>> virtualEntry = rate.getValue().entrySet();
			for (Entry<String, BigDecimal> virtual : virtualEntry) {
				String _virtualCoin = virtual.getKey();

				coinSet.add(_virtualCoin + "_" + _legalCoin);
				// 获取对应计价货币币对
				List<CoinPair> coinPairs = pairs.get(_virtualCoin);
				if (CollectionUtils.isEmpty(coinPairs)) {
					continue;
				}

				for (CoinPair coinPair : coinPairs) {

					// 不能过校验则认为已经通过别的虚拟币获得汇率
					if (!coinSet.add(coinPair.getBaseCoin() + "_" + _legalCoin)) {
						continue;
					}
					
					CloudUserCoinPairConfigData  cloudUserCoinPairConfigData  = getCloudUserCoinPairCache(coinPair.getSymbol().toLowerCase(), cloudUserId);
					if(cloudUserCoinPairConfigData == null) {
						log.debug("当前云用户没有配置当前币对，symbol=" + coinPair.getSymbol().toLowerCase() + ",cloudUserId=" + cloudUserId);
						continue;
					}

					String _res = hashOperations.get(RedisKeyConstants.COIN_TICKER + cloudUserCoinPairConfigData.getExternalExchangeType() + _virtualCoin.toLowerCase() + "_",
							coinPair.getSymbol().toLowerCase());
					if (StringUtils.isEmpty(_res)) {
						log.debug("未获取到币对24小时行情缓存数据：" + coinPair.getSymbol());
						coinSet.remove(coinPair.getBaseCoin() + "_" + _legalCoin);
						continue;
					}
					TickerData res = JSON.parseObject(_res, TickerData.class);
					BigDecimal lastPrice = res.getLast();
					if (lastPrice == null) {
						log.debug("未获取到币对最新成交价：" + coinPair.getSymbol());
						continue;
					}

					BigDecimal _rate = lastPrice.multiply(virtual.getValue());
					newRateMap.put(coinPair.getBaseCoin(), _rate);
				}
			}

			Map<String, BigDecimal> _rateMap = rateMap.get(_legalCoin);
			_rateMap.putAll(newRateMap);
			rateMap.put(_legalCoin, _rateMap);
		}

		rateData.setRates(rateMap);
		valueOperations.set(redisKey, JSON.toJSONString(rateData), 5, TimeUnit.SECONDS);
		return rateData;
	}

	private PageInfo<CloudUserCoinPair> selectCloudCoinPair(int pageNo, int pageSize) {
		CloudUserCoinPairExample example = new CloudUserCoinPairExample();
		example.setOrderByClause("id desc");
		example.createCriteria().andStatusEqualTo(CoinPairConfigStatus.OPEN.getCode());
		PageHelper.offsetPage(pageNo, pageSize);
		List<CloudUserCoinPair> cloudUserCoinPairs = cloudUserCoinPairMapper.selectByExample(example);
		return new PageInfo<>(cloudUserCoinPairs);
	}
	
	public CloudUserCoinPairConfigData getCloudUserCoinPairCache(String symbol,String cloudUserId) {
		List<CloudUserCoinPairConfigData> list = getCloudUserCoinPairCache(cloudUserId);
		for(CloudUserCoinPairConfigData data:list) {
			if(data.getSymbol().equals(symbol)) {
				return data;
			}
		}
		
		return null;
	}
	
	public List<CloudUserCoinPairConfigData> getCloudUserCoinPairCache(String cloudUserId){
		List<CloudUserCoinPairConfigData> configs = null;
		ValueOperations<String, String> operations = redisTemplate.opsForValue();
		String redisKey = RedisKeyConstants.CLOUD_USER_COIN_PAIR_CACHE+":"+cloudUserId;
		String res = operations.get(redisKey);
		if (StringUtils.isEmpty(res)) {

			lockUtils.lock(redisKey);

			try {
				res = operations.get(redisKey);

				if (StringUtils.isEmpty(res)) {
					configs = cloudUserCoinPairMapper.selectCloudUserCoinPairConfigList(cloudUserId, CoinPairConfigStatus.OPEN.getCode().intValue());
					operations.set(redisKey, JSON.toJSONString(configs));
					return configs;
				}
			} catch (Exception e) {
				log.error("", e);
				throw new BusinessException();
			} finally {
				lockUtils.releaseLock(redisKey);
			}

		}

		configs = JSON.parseArray(res, CloudUserCoinPairConfigData.class);
		return configs;
	}

	@Override
	public List<CoinPairConfig> getCoinPairCache() {
		List<CoinPairConfig> configs = null;
		ValueOperations<String, String> operations = redisTemplate.opsForValue();
		String redisKey = RedisKeyConstants.COIN_PAIR_CACHE;
		String res = operations.get(redisKey);
		if (StringUtils.isEmpty(res)) {

			lockUtils.lock(redisKey);

			try {
				res = operations.get(redisKey);

				if (StringUtils.isEmpty(res)) {
					configs = coinPairConfigMapper.selectListByCoinPairConfig(null,CoinPairConfigStatus.OPEN.getCode().intValue());
					operations.set(redisKey, JSON.toJSONString(configs));
					return configs;
				}
			} catch (Exception e) {
				log.error("", e);
				throw new BusinessException();
			} finally {
				lockUtils.releaseLock(redisKey);
			}

		}

		configs = JSON.parseArray(res, CoinPairConfig.class);
		return configs;
	}

	public CoinPairConfig getCoinPairCache(String symbol) {
		if(symbol == null) {
			return null;
		}
		List<CoinPairConfig> list = getCoinPairCache();
		for(CoinPairConfig config:list) {
			if(config.getSymbol().toLowerCase().equals(symbol.toLowerCase())) {
				return config;
			}
		}
		
		return null;
	}
	@Override
	public SymbolData getSymbolInfo(String symbol) {
		String quoteCoin = getQuoteCoin(symbol);
		String baseCoin = symbol.substring(0, symbol.length() - symbol.lastIndexOf(quoteCoin));
		
		SymbolData data = new SymbolData();
		data.setBaseCoin(baseCoin);
		data.setQuoteCoin(quoteCoin);
		data.setSymbol(symbol);
		return data;
	}
	
	@Override
	public String getQuoteCoin(String symbol) {
		List<String> coins = ExchangeQuoteCoin.getCoins();
		for (String coin : coins) {
			if (symbol.endsWith(coin)) {
				return coin;
			}
		}

		return null;
	}

	@Override
	public List<String> getExchangeType() {

		ValueOperations<String, String> operations = redisTemplate.opsForValue();
		String redisKey = RedisKeyConstants.EXCHANGE_TYPE;
		String json = operations.get(redisKey);
		if (StringUtils.isEmpty(json)) {

			lockUtils.lock(redisKey);

			try {
				json = operations.get(redisKey);
				if (StringUtils.isEmpty(json)) {
					List<String> exchangeLists = coinPairConfigMapper.selectExchangeType();
					Set<String> exchangeSet = new HashSet();
					for(String exchangeListStr:exchangeLists) {
						String[] exchangeTypes = exchangeListStr == null?new String[0]:exchangeListStr.split(",");
						for(String exchangeType:exchangeTypes) {
							exchangeSet.add(exchangeType);
						}
					}
					
					List<String> exchangeList = new ArrayList<>(exchangeSet);
					operations.set(redisKey, JSON.toJSONString(exchangeList));
					return exchangeList;
				}
			} catch (Exception e) {
				log.error("", e);
				throw new BusinessException();
			} finally {
				lockUtils.releaseLock(redisKey);
			}

		}

		List<String> exchangeList = JSON.parseArray(json, String.class);

		return exchangeList;
	}

	@Override
	public List<CoinPairConfig> getLocalCoinPairByExchangeType(String exchangeType) {

		List<CoinPairConfig> configs = getCoinPairCache();
		List<CoinPairConfig> results = new ArrayList<>();
		
		for(CoinPairConfig config:configs) {
			if(config.getExternalExchangeTypes() != null && config.getExternalExchangeTypes().indexOf(exchangeType) > -1) {
				results.add(config);
			}
		}
		return results;
	}

}
