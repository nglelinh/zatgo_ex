package com.zatgo.zup.exchange.strategy.bytex.model;

import com.alibaba.fastjson.annotation.JSONField;

public class BytexOrderDetailReq extends BytexReq {
	
	@JSONField(name = "order_id")
	private Long order_id;
	
	private String symbol;

	public Long getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol.toLowerCase();
	}
	
	public BytexOrderDetailReq() {
		super();
	}
	
	public BytexOrderDetailReq(String apiKey, String secretKey, String orderId, String symbol) {
		this.order_id = Long.valueOf(orderId);
		this.symbol = symbol.toLowerCase();
		sign(apiKey, secretKey, this);
	}

}
