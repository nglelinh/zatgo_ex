package com.zatgo.zup.exchange.appapi.model;

import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeMode;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;

import com.zatgo.zup.common.model.WebSocketBaseParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("交易所基础参数")
public class BaseExchangeParams extends WebSocketBaseParams {
	
	@ApiModelProperty(value = "apikey", required = false)
	private String apiKey;
	
	@ApiModelProperty(value = "secretKey", required = false)
	private String secretKey;
	
	@ApiModelProperty(value = "请求模式：direct，local(废弃)", required = false)
	private ExchangeMode exchangeMode;
	
	@ApiModelProperty(value = "直连的交易所类型：bytex", required = false)
	private ExchangeType exchangeType;

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	
	public void setExchangeMode(ExchangeMode exchangeMode) {
		this.exchangeMode = exchangeMode;
	}

	public ExchangeMode getExchangeMode() {
		return exchangeMode;
	}

	public void setExchangeMode(String exchangeMode) {
		this.exchangeMode = ExchangeMode.getEnumByType(exchangeMode);
	}

	public ExchangeType getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = ExchangeType.getEnumByType(exchangeType);
	}

}
