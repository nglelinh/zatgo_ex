package com.zatgo.zup.exchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.exchange.entity.ExTrade;

public interface ExTradeMapper {
    int deleteByPrimaryKey(String tradeId);

    int insert(ExTrade record);

    int insertSelective(ExTrade record);

    ExTrade selectByPrimaryKey(String tradeId);

    int updateByPrimaryKeySelective(ExTrade record);

    int updateByPrimaryKey(ExTrade record);
    
    List<ExTrade> selectExTradeByOrderId(@Param("orderId") String orderId);
}