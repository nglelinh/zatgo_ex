package com.zatgo.zup.exchange.appapi.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("当前行情数据")
public class TickerData {
	
	@ApiModelProperty(value = "最高值", required = true)
	private BigDecimal high;
	
	@ApiModelProperty(value = "交易量", required = true)
	private BigDecimal vol;
	
	@ApiModelProperty(value = "最新成交价", required = true)
	private BigDecimal last;
	
	@ApiModelProperty(value = "最低值", required = true)
	private BigDecimal low;
	
	@ApiModelProperty(value = "买一价", required = true)
	private BigDecimal buy;
	
	@ApiModelProperty(value = "卖一价", required = true)
	private BigDecimal sell;
	
	@ApiModelProperty(value = "时间", required = true)
	private Long time;
	
	@ApiModelProperty(value = "基础货币", required = true)
	private String baseCoin;

	@ApiModelProperty(value = "计价货币", required = true)
    private String quoteCoin;
	
	@ApiModelProperty(value = "开盘价", required = false, notes = "bytex无该值")
	private BigDecimal open;
	
	@ApiModelProperty(value = "收盘价", required = false, notes = "bytex无该值")
	private BigDecimal close;
	
	@ApiModelProperty(value = "24小时成交量", required = false, notes = "bytex无该值")
	private BigDecimal amount;
	
	@ApiModelProperty(value = "近24小时累积成交数", required = false, notes = "bytex无该值")
	private BigDecimal count;
	
	@ApiModelProperty(value = "涨跌", required = false, notes = "bytex无该值")
	private BigDecimal chg;
	
	@ApiModelProperty(value = "id", required = false)
	private String id;
	
	@ApiModelProperty(value = "交易对", required = true)
	private String symbol;

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public BigDecimal getVol() {
		return vol;
	}

	public void setVol(BigDecimal vol) {
		this.vol = vol;
	}

	public BigDecimal getLast() {
		return last;
	}

	public void setLast(BigDecimal last) {
		this.last = last;
	}

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public BigDecimal getBuy() {
		return buy;
	}

	public void setBuy(BigDecimal buy) {
		this.buy = buy;
	}

	public BigDecimal getSell() {
		return sell;
	}

	public void setSell(BigDecimal sell) {
		this.sell = sell;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	public BigDecimal getOpen() {
		return open;
	}

	public void setOpen(BigDecimal open) {
		this.open = open;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCount() {
		return count;
	}

	public void setCount(BigDecimal count) {
		this.count = count;
	}

	public BigDecimal getChg() {
		return chg;
	}

	public void setChg(BigDecimal chg) {
		this.chg = chg;
	}

	public BigDecimal getClose() {
		return close;
	}

	public void setClose(BigDecimal close) {
		this.close = close;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

}
