package com.zatgo.zup.exchange.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ExTrade {
    private String tradeId;

    private String externalTradeId;

    private String bidUserId;

    private String askUserId;

    private String bidOrderId;

    private String askOrderId;

    private String externalBidOrderId;

    private String externalAskOrderId;

    private BigDecimal price;

    private BigDecimal volume;

    private String trendSide;

    private BigDecimal buyFee;

    private BigDecimal sellFee;

    private String buyFeeCoin;

    private String sellFeeCoin;

    private String baseCoin;

    private String quoteCoin;

    private String coinPairSymbol;

    private Date createTime;

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId == null ? null : tradeId.trim();
    }

    public String getExternalTradeId() {
        return externalTradeId;
    }

    public void setExternalTradeId(String externalTradeId) {
        this.externalTradeId = externalTradeId == null ? null : externalTradeId.trim();
    }

    public String getBidUserId() {
        return bidUserId;
    }

    public void setBidUserId(String bidUserId) {
        this.bidUserId = bidUserId == null ? null : bidUserId.trim();
    }

    public String getAskUserId() {
        return askUserId;
    }

    public void setAskUserId(String askUserId) {
        this.askUserId = askUserId == null ? null : askUserId.trim();
    }

    public String getBidOrderId() {
        return bidOrderId;
    }

    public void setBidOrderId(String bidOrderId) {
        this.bidOrderId = bidOrderId == null ? null : bidOrderId.trim();
    }

    public String getAskOrderId() {
        return askOrderId;
    }

    public void setAskOrderId(String askOrderId) {
        this.askOrderId = askOrderId == null ? null : askOrderId.trim();
    }

    public String getExternalBidOrderId() {
        return externalBidOrderId;
    }

    public void setExternalBidOrderId(String externalBidOrderId) {
        this.externalBidOrderId = externalBidOrderId == null ? null : externalBidOrderId.trim();
    }

    public String getExternalAskOrderId() {
        return externalAskOrderId;
    }

    public void setExternalAskOrderId(String externalAskOrderId) {
        this.externalAskOrderId = externalAskOrderId == null ? null : externalAskOrderId.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public String getTrendSide() {
        return trendSide;
    }

    public void setTrendSide(String trendSide) {
        this.trendSide = trendSide == null ? null : trendSide.trim();
    }

    public BigDecimal getBuyFee() {
        return buyFee;
    }

    public void setBuyFee(BigDecimal buyFee) {
        this.buyFee = buyFee;
    }

    public BigDecimal getSellFee() {
        return sellFee;
    }

    public void setSellFee(BigDecimal sellFee) {
        this.sellFee = sellFee;
    }

    public String getBuyFeeCoin() {
        return buyFeeCoin;
    }

    public void setBuyFeeCoin(String buyFeeCoin) {
        this.buyFeeCoin = buyFeeCoin == null ? null : buyFeeCoin.trim();
    }

    public String getSellFeeCoin() {
        return sellFeeCoin;
    }

    public void setSellFeeCoin(String sellFeeCoin) {
        this.sellFeeCoin = sellFeeCoin == null ? null : sellFeeCoin.trim();
    }

    public String getBaseCoin() {
        return baseCoin;
    }

    public void setBaseCoin(String baseCoin) {
        this.baseCoin = baseCoin == null ? null : baseCoin.trim();
    }

    public String getQuoteCoin() {
        return quoteCoin;
    }

    public void setQuoteCoin(String quoteCoin) {
        this.quoteCoin = quoteCoin == null ? null : quoteCoin.trim();
    }

    public String getCoinPairSymbol() {
        return coinPairSymbol;
    }

    public void setCoinPairSymbol(String coinPairSymbol) {
        this.coinPairSymbol = coinPairSymbol == null ? null : coinPairSymbol.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}