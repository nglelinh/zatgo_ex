package com.zatgo.zup.exchange.strategy.huobi.model;

import java.math.BigDecimal;

import com.alibaba.fastjson.annotation.JSONField;

public class HuobiOrderMatchResultData {

	/**
	 * 成交时间
	 */
	@JSONField(name = "created-at")
	private Long createdAt;

	/**
	 * 成交数量
	 */
	@JSONField(name = "filled-amount")
	private BigDecimal filledAmount;

	/**
	 * 成交手续费
	 */
	@JSONField(name = "filled-fees")
	private BigDecimal filledFees;

	/**
	 * 订单成交记录ID
	 */
	private Long id;

	/**
	 * 撮合ID
	 */
	@JSONField(name = "match-id")
	private Long matchId;

	/**
	 * 订单 ID
	 */
	@JSONField(name = "order-id")
	private Long orderId;

	/**
	 * 成交价格
	 */
	private BigDecimal price;

	/**
	 * 订单来源
	 */
	private String source;

	/**
	 * 交易对
	 */
	private String symbol;

	/**
	 * 订单类型buy-market：市价买, sell-market：市价卖, buy-limit：限价买, sell-limit：限价卖,
	 * buy-ioc：IOC买单, sell-ioc：IOC卖单
	 */
	private String type;

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getFilledAmount() {
		return filledAmount;
	}

	public void setFilledAmount(BigDecimal filledAmount) {
		this.filledAmount = filledAmount;
	}

	public BigDecimal getFilledFees() {
		return filledFees;
	}

	public void setFilledFees(BigDecimal filledFees) {
		this.filledFees = filledFees;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
