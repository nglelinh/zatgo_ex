package com.zatgo.zup.exchange.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.ExOrderDetailData;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.service.OrderService;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.OrderStrategyService;

@Service("orderServiceImpl_direct")
public class OrderServiceDirectImpl implements OrderService {

	@Autowired
	private ExchangeStrategy exchangeStrategyService;
	
	@Override
	public OrderData createOrder(CreateOrderParams params, String userId) {
		OrderStrategyService orderService = exchangeStrategyService.getOrderService(params.getExchangeType());
		String orderId = orderService.createOrder(params);

		OrderData data = new OrderData();
		data.setBaseCoin(params.getBaseCoin());
		data.setFeeIsUserPlatformCoin(params.getFeeIsUserPlatformCoin());
		data.setQuoteCoin(params.getQuoteCoin());
		data.setSide(params.getSide());
		data.setSymbol(params.getSymbol());
		data.setType(params.getType());
		data.setVolume(params.getVolume());
		data.setOrderId(orderId);

		return data;
	}

	@Override
	public OrderData cancelOrder(CancelOrderParams params, String userId) {
		OrderStrategyService orderService = exchangeStrategyService.getOrderService(params.getExchangeType());
		orderService.cancelOrder(params);

		OrderData data = new OrderData();
		data.setOrderId(params.getOrderId());
		data.setBaseCoin(params.getBaseCoin());
		data.setQuoteCoin(params.getQuoteCoin());
		data.setSymbol(params.getSymbol());

		return data;
	}

	@Override
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params) {
		OrderStrategyService orderService = exchangeStrategyService.getOrderService(params.getExchangeType());
		return orderService.findOrderByPage(params);
	}

	@Override
	public OrderData getOrderByOrderId(OrderDetailParams params) {
		OrderStrategyService orderService = exchangeStrategyService.getOrderService(params.getExchangeType());

		ExOrderDetailData data = orderService.getOrderByOrderId(params);

		OrderData orderData = new OrderData();
		orderData.setOrderId(data.getOrder().getOrderId());
		orderData.setSide(ExchangeSide.getEnumByType(data.getOrder().getSide()));
		orderData.setCreateDate(data.getOrder().getCreateDate());
		orderData.setPrice(data.getOrder().getPrice());
		orderData.setVolume(data.getOrder().getVolume());
		orderData.setDealVolume(data.getOrder().getDealVolume());
		orderData.setAvgPrice(data.getOrder().getAvgPrice());

		return orderData;
	}

}
