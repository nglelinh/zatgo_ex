package com.zatgo.zup.exchange.strategy.bytex;

import java.net.URI;
import java.nio.ByteBuffer;

import org.apache.commons.lang3.StringUtils;
import org.java_websocket.WebSocket;
import org.java_websocket.WebSocket.READYSTATE;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.PongFrame;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.exchange.appapi.util.GZIPUtils;
import com.zatgo.zup.exchange.appapi.util.WebSocketClientUtil;
import com.zatgo.zup.exchange.strategy.WebSocketClientService;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;

public class BytexWebSocketServiceClient implements WebSocketClientService {

	private static final Logger log = LoggerFactory.getLogger(BytexWebSocketServiceClient.class);

	private String exchangeType;

	private WebSocketStrategyService service;
	
	private WebSocketClient webSocketClient;

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public WebSocketStrategyService getService() {
		return service;
	}

	public void setService(WebSocketStrategyService service) {
		this.service = service;
	}

	public BytexWebSocketServiceClient(URI uri) {
		webSocketClient = new WebSocketClient(uri) {
			
			@Override
			public void onOpen(ServerHandshake handshakedata) {
				log.debug(exchangeType + "ws已连接");
			}

			@Override
			public void onMessage(String message) {
				log.debug("收到消息" + exchangeType + ":" + message);
				service.receiveServerMessage(message);
			}

			@Override
			public void onClose(int code, String reason, boolean remote) {
				log.debug(exchangeType + "ws连接关闭：" + reason);
				WebSocketClientUtil.removeWebSocketClient(exchangeType);
			}

			@Override
			public void onError(Exception ex) {
				log.error("", ex);
				WebSocketClientUtil.removeWebSocketClient(exchangeType);
			}

			@Override
			public void onWebsocketPing(WebSocket conn, Framedata f) {
				PongFrame resp = new PongFrame();
				resp.setPayload(f.getPayloadData());
				conn.sendFrame(resp);
			}

			@Override
			public void onWebsocketPong(WebSocket conn, Framedata f) {
				super.onWebsocketPong(conn, f);
			}

			@Override
			public void onMessage(ByteBuffer bytes) {

				try {

					String message = GZIPUtils.uncompressToString(bytes.array(), "UTF-8");
					log.info("收到消息" + exchangeType + ":" + message);

					if (StringUtils.isEmpty(message)) {
						return;
					}

					JSONObject jsonObject = JSONObject.parseObject(message);
					String code = jsonObject.getString("code");
					if(StringUtils.isNotEmpty(code)) {
						log.error(exchangeType + "接收错误：" + message);
						return;
					}

					if (message.indexOf("ping") > 0) {
						String pong = jsonObject.toString();
						send(pong.replace("ping", "pong"));
					} else {
						log.debug(exchangeType + "响应：" + System.currentTimeMillis());
						log.debug(message);
						service.receiveServerMessage(message);
					}
				} catch (Exception e) {
					log.error(exchangeType + "ws消息处理失败：", e);
				}
			}
		};
	}

	@Override
	public Boolean isOpen() {
		return webSocketClient.isOpen();
	}

	@Override
	public void connect() {
		webSocketClient.connect();
	}

	@Override
	public ZUP_READYSTATE getReadyState() {
		WebSocketClient.READYSTATE status = webSocketClient.getReadyState();
		if(status.equals(READYSTATE.CLOSED)) {
			return ZUP_READYSTATE.CLOSED;
		}else if(status.equals(READYSTATE.CLOSING)) {
			return ZUP_READYSTATE.CLOSING;
		}else if(status.equals(READYSTATE.CONNECTING)) {
			return ZUP_READYSTATE.CONNECTING;
		}else if(status.equals(READYSTATE.NOT_YET_CONNECTED)) {
			return ZUP_READYSTATE.NOT_YET_CONNECTED;
		}else if(status.equals(READYSTATE.OPEN)) {
			return ZUP_READYSTATE.OPEN;
		}else {
			throw new BusinessException();
		}
	}

	@Override
	public void send(String message) {
		webSocketClient.send(message);
	}

	

}
