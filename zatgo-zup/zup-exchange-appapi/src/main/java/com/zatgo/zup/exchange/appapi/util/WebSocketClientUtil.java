package com.zatgo.zup.exchange.appapi.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections4.MapUtils;
import org.java_websocket.client.WebSocketClient;

import com.zatgo.zup.exchange.strategy.WebSocketClientService;

public class WebSocketClientUtil {

	private static final Map<String, WebSocketClientService> exchanges = new ConcurrentHashMap<String, WebSocketClientService>();
	
	/**
	 * 获取WebSocketClient
	 * @param exchangeType
	 * @return
	 */
	public static WebSocketClientService getWebSocketClient(String exchangeType) {
		if(MapUtils.isEmpty(exchanges)) {
			return null;
		}
		
		return exchanges.get(exchangeType);
	}

	/**
	 * 
	 * @param exchangeType
	 * @param client
	 */
	public static void addWebSocketClient(String exchangeType, WebSocketClientService client) {
		exchanges.put(exchangeType, client);
	}
	
	/**
	 * 
	 * @param exchangeType
	 */
	public static void removeWebSocketClient(String exchangeType) {
		exchanges.remove(exchangeType);
	}

}
