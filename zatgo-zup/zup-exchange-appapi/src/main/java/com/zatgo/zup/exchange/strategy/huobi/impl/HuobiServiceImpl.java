package com.zatgo.zup.exchange.strategy.huobi.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aliyun.openservices.shade.com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.exchange.appapi.model.ExOrderType;
import com.zatgo.zup.exchange.strategy.huobi.HuobiService;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiAccountData;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiAccountsReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiAccountsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderData;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderMatchResultsReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderMatchResultsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderPlaceReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderPlaceResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrdersReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrdersResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiSubmitCancelReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiSubmitCancelResp;

@Service
public class HuobiServiceImpl implements HuobiService {

	private static final Logger log = LoggerFactory.getLogger(HuobiServiceImpl.class);

	@Autowired
	private OkHttpService httpService;

	@Override
	public List<HuobiAccountData> getHuobiAccountList(String url, HuobiAccountsReq req) {
		HuobiAccountsResp resp = httpService.get(url + req.getMethod(), req.getUrlParams(), HuobiAccountsResp.class);
		if (resp == null) {
			log.error("获取火币账户列表参数未响应");
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		if (!"ok".equals(resp.getStatus())) {
			log.error("获取火币账户列表参数失败，err-code：" + resp.getErrCode() + "，err-msg：" + resp.getErrMsg());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		return resp.getData();
	}

	@Override
	public HuobiAccountData getHuobiAccount(String url, HuobiAccountsReq req, String accType) {
		List<HuobiAccountData> accountList = getHuobiAccountList(url, req);
		if (CollectionUtils.isEmpty(accountList)) {
			return null;
		}

		HuobiAccountData accountData = null;
		for (HuobiAccountData data : accountList) {
			if (accType.equals(data.getType())) {
				accountData = data;
				break;
			}
		}

		return accountData;
	}

	@Override
	public String createHuobiOrder(String url, HuobiAccountsReq accReq, String accType, HuobiOrderPlaceReq req) {

		HuobiAccountData accountData = null;
		try {
			accountData = getHuobiAccount(url, accReq, accType);
		} catch (Exception e) {
			log.error("获取火币交易账户失败：", e);
			throw new BusinessException(BusinessExceptionCode.HUOBI_CREATE_ORDER_FAIL);
		}

		if (accountData == null) {
			log.error("获取火币交易账户失败");
			throw new BusinessException(BusinessExceptionCode.HUOBI_ACCOUNT_NOT_FIND);
		}

		if (!"working".equals(accountData.getState())) {
			log.error("火币交易账户状态不可用");
			throw new BusinessException(BusinessExceptionCode.HUOBI_ACCOUNT_STATE_UNUSABLE);
		}

		req.setAccountId(accountData.getId());
		try {
			req.sign(req, url);
		} catch (Exception e) {
			log.error("获取火币下单参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.HUOBI_CREATE_ORDER_FAIL);
		}
		HuobiOrderPlaceResp resp = httpService.postObj(url + req.getMethod(), req, HuobiOrderPlaceResp.class,
				req.getUrlParams().entrySet().stream().map(entry -> {
					return entry.getValue();
				}).toArray(String[]::new));

		log.info("火币下单返回：" + JSON.toJSONString(resp));

		if (resp == null) {
			log.error("火币下单未响应");
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		if (!"ok".equalsIgnoreCase(resp.getStatus())) {
			log.error("火币下单失败，err-code：" + resp.getErrCode() + "，err-msg：" + resp.getErrMsg());
			throw new BusinessException(BusinessExceptionCode.HUOBI_CREATE_ORDER_FAIL);
		}

		return resp.getData();
	}

	@Override
	public String cancelHuobiOrder(String url, HuobiSubmitCancelReq req) {
		HuobiSubmitCancelResp resp = httpService.postObj(url + req.getMethod(), null, HuobiSubmitCancelResp.class,
				req.getUrlParams().entrySet().stream().map(entry -> {
					return entry.getValue();
				}).toArray(String[]::new));

		if (resp == null) {
			log.error("取消火币订单未响应：" + req.getMethod());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		if (!"ok".equalsIgnoreCase(resp.getStatus())) {
			log.error("取消火币订单失败，err-code：" + resp.getErrCode() + "，err-msg：" + resp.getErrMsg() + resp.getErrCode());
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_CANCEL_FAIL, resp.getErrMsg());
		}

		return resp.getData();
	}

	@Override
	public HuobiOrderData getOrderData(String url, HuobiOrderReq req) {
		HuobiOrderResp resp = httpService.postObj(url + req.getMethod(), null, HuobiOrderResp.class,
				req.getUrlParams().entrySet().stream().map(entry -> {
					return entry.getValue();
				}).toArray(String[]::new));

		if (resp == null) {
			log.error("获取火币订单信息未响应：" + req.getMethod());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		if (!"ok".equalsIgnoreCase(resp.getStatus())) {
			log.error("获取火币订单信息失败，err-code：" + resp.getErrCode() + "，err-msg：" + resp.getErrMsg() + resp.getErrCode());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR, resp.getErrMsg());
		}

		return resp.getData();
	}

	@Override
	public HuobiOrderMatchResultsResp getOrderMatchResults(String url, HuobiOrderMatchResultsReq req) {
		HuobiOrderMatchResultsResp resp = httpService.postObj(url + req.getMethod(), null,
				HuobiOrderMatchResultsResp.class, req.getUrlParams().entrySet().stream().map(entry -> {
					return entry.getValue();
				}).toArray(String[]::new));

		if (resp == null) {
			log.error("获取火币订单成交明细未响应：" + req.getMethod());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		if (!"ok".equalsIgnoreCase(resp.getStatus())) {
			log.error(
					"获取火币订单成交明细失败，err-code：" + resp.getErrCode() + "，err-msg：" + resp.getErrMsg() + resp.getErrCode());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR, resp.getErrMsg());
		}

		return resp;
	}

	@Override
	public List<HuobiOrderData> getOrdersList(String url, HuobiOrdersReq req) {
		HuobiOrdersResp resp = httpService.get(url + req.getMethod(), HuobiOrdersResp.class,
				req.getUrlParams().entrySet().stream().map(entry -> {
					return entry.getValue();
				}).toArray(String[]::new));

		if (resp == null) {
			log.error("获取火币订单列表未响应：" + req.getMethod());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		if (!"ok".equalsIgnoreCase(resp.getStatus())) {
			log.error("获取火币订单列表失败，err-code：" + resp.getErrCode() + "，err-msg：" + resp.getErrMsg() + resp.getErrCode());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR, resp.getErrMsg());
		}

		return resp.getData();
	}

	@Override
	public String getHuobiOrderType(String side, String type) {
		if (side.equals(ExchangeSide.BUY.getCode()) && type.equals(EntryOrdersType.marketPriceEntrust.getCode())) {
			return "buy-market";
		}

		if (side.equals(ExchangeSide.SELL.getCode()) && type.equals(EntryOrdersType.marketPriceEntrust.getCode())) {
			return "sell-market";
		}

		if (side.equals(ExchangeSide.BUY.getCode()) && type.equals(EntryOrdersType.limitPriceEntrust.getCode())) {
			return "buy-limit";
		}

		if (side.equals(ExchangeSide.SELL.getCode()) && type.equals(EntryOrdersType.limitPriceEntrust.getCode())) {
			return "sell-limit";
		}

		return null;
	}

	@Override
	public Set<String> getHuobiOrderStates(OrderStatus[] orderStatus) {
		Set<String> set = new HashSet<String>();
		for (OrderStatus status : orderStatus) {
			if (status.equals(OrderStatus.INIT)) {
				set.add("submitted");
			} else if (status.equals(OrderStatus.NEW)) {
				set.add("submitted");
			} else if (status.equals(OrderStatus.FILLED)) {
				set.add("filled");
			} else if (status.equals(OrderStatus.PART_FILLED)) {
				set.add("partial-filled");
			} else if (status.equals(OrderStatus.CANCELED)) {
				set.add("partial-canceled");
				set.add("canceled");
			} else if (status.equals(OrderStatus.PENDING_CANCEL)) {

			} else if (status.equals(OrderStatus.EXCEPTION)) {

			} else if (status.equals(OrderStatus.UNCOMMITTED)) {

			}
		}
		return set;
	}

	@Override
	public ExOrderType getOrderSideType(String type) {
		String[] strings = type.split("-");
		String side = strings[0];
		String orderType = strings[1];
		
		ExOrderType exOrderType = new ExOrderType();
		if(side.equalsIgnoreCase("buy")) {
			exOrderType.setSide(ExchangeSide.BUY);
		} else if(side.equalsIgnoreCase("sell")) {
			exOrderType.setSide(ExchangeSide.SELL);
		}
		
		if(orderType.equalsIgnoreCase("market")) {
			exOrderType.setType(EntryOrdersType.marketPriceEntrust);
		} else if(orderType.equalsIgnoreCase("limit")) {
			exOrderType.setType(EntryOrdersType.limitPriceEntrust);
		}
		
		return exOrderType;
	}

	@Override
	public OrderStatus getOrderStatus(String state) {
		switch (state) {

		case "submitting":
			return OrderStatus.INIT;

		case "submitted":
			return OrderStatus.NEW;

		case "partial-filled":
			return OrderStatus.PART_FILLED;

		case "partial-canceled":
			return OrderStatus.CANCELED;

		case "filled":
			return OrderStatus.FILLED;

		case "canceled":
			return OrderStatus.CANCELED;

		default:
			return null;
		}
	}

}
