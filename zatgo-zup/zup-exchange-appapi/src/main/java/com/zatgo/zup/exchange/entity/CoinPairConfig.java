package com.zatgo.zup.exchange.entity;

import java.math.BigDecimal;

public class CoinPairConfig {
    private String id;

    private String symbol;

    private String baseCoin;

    private String quoteCoin;

    private Integer pricePrecision;

    private String externalExchangeTypes;

    private BigDecimal makerFeeIncrease;

    private BigDecimal takerFeeIncrease;

    private Byte status;

    private BigDecimal defaultRate;

    private String clientType;

    private String coinImage;

    private Byte sort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol == null ? null : symbol.trim();
    }

    public String getBaseCoin() {
        return baseCoin;
    }

    public void setBaseCoin(String baseCoin) {
        this.baseCoin = baseCoin == null ? null : baseCoin.trim();
    }

    public String getQuoteCoin() {
        return quoteCoin;
    }

    public void setQuoteCoin(String quoteCoin) {
        this.quoteCoin = quoteCoin == null ? null : quoteCoin.trim();
    }

    public Integer getPricePrecision() {
        return pricePrecision;
    }

    public void setPricePrecision(Integer pricePrecision) {
        this.pricePrecision = pricePrecision;
    }

    public String getExternalExchangeTypes() {
		return externalExchangeTypes;
	}

	public void setExternalExchangeTypes(String externalExchangeTypes) {
		this.externalExchangeTypes = externalExchangeTypes;
	}

	public BigDecimal getMakerFeeIncrease() {
        return makerFeeIncrease;
    }

    public void setMakerFeeIncrease(BigDecimal makerFeeIncrease) {
        this.makerFeeIncrease = makerFeeIncrease;
    }

    public BigDecimal getTakerFeeIncrease() {
        return takerFeeIncrease;
    }

    public void setTakerFeeIncrease(BigDecimal takerFeeIncrease) {
        this.takerFeeIncrease = takerFeeIncrease;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public BigDecimal getDefaultRate() {
        return defaultRate;
    }

    public void setDefaultRate(BigDecimal defaultRate) {
        this.defaultRate = defaultRate;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType == null ? null : clientType.trim();
    }

    public String getCoinImage() {
        return coinImage;
    }

    public void setCoinImage(String coinImage) {
        this.coinImage = coinImage == null ? null : coinImage.trim();
    }

    public Byte getSort() {
        return sort;
    }

    public void setSort(Byte sort) {
        this.sort = sort;
    }
}