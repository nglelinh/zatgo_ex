package com.zatgo.zup.exchange.strategy.huobi.model;

import java.net.URL;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.alibaba.fastjson.annotation.JSONField;
import com.zatgo.zup.common.utils.BeanToMapUtils;
import com.zatgo.zup.common.utils.HMACSHA256;

public class HuobiReq {

	private String AccessKeyId;

	private String SignatureMethod;

	private String SignatureVersion;

	private String Timestamp;

	private String reqMethod;

	@JSONField(serialize = false)
	private String method;

	private String secretKey;

	@JSONField(serialize = false)
	private HashMap<String, String> urlParams;

	private void init() {
		SignatureMethod = "HmacSHA256";
		SignatureVersion = "2";
		Timestamp = new DateTime(DateTimeZone.UTC).toString("yyy-MM-dd'T'HH:mm:ss");
	}

	public HuobiReq() {
		init();
	}

	public HuobiReq(String accessKeyId, String secretKey, String reqMethod, String method) {
		AccessKeyId = accessKeyId;
		this.secretKey = secretKey;
		this.reqMethod = reqMethod.toUpperCase();
		this.method = method;
		init();
	}

	public void setAccessKeyId(String accessKeyId) {
		AccessKeyId = accessKeyId;
	}

	public void setReqMethod(String reqMethod) {
		this.reqMethod = reqMethod.toUpperCase();
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getMethod() {
		return method;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public HashMap<String, String> getUrlParams() {
		return urlParams;
	}

	public void sign(HuobiReq req, String url) throws Exception {

		URL uri = new URL(url);
		String hostName = uri.getHost();

		StringBuffer _sign = new StringBuffer();
		_sign.append(reqMethod).append("\n").append(hostName).append("\n").append(method).append("\n");

		Map<String, String> sortedParams = new TreeMap<String, String>();
		if (reqMethod.equalsIgnoreCase("GET")) {
			HashMap<String, String> data = new HashMap<>();
			data.put("AccessKeyId", AccessKeyId);
			data.put("SignatureMethod", SignatureMethod);
			data.put("SignatureVersion", SignatureVersion);
			data.put("Timestamp", Timestamp);
			sortedParams.putAll(data);
		} else if (reqMethod.equalsIgnoreCase("POST")) {
			HashMap<String, String> data = BeanToMapUtils.toHashMap(req);
			data.remove("reqMethod");
			data.remove("method");
			data.remove("secretKey");
			sortedParams.putAll(data);
		}

		HashMap<String, String> urlMap = new HashMap<>();
		StringBuffer paramsSign = new StringBuffer();
		Set<Entry<String, String>> entries = sortedParams.entrySet();
		for (Entry<String, String> entry : entries) {
			String key = entry.getKey();
			String value = URLEncoder.encode(entry.getValue(), "UTF-8");
			paramsSign.append("&").append(key).append("=").append(value);
			urlMap.put(key, value);
		}
		_sign.append(paramsSign.substring(1));

		String signStr = _sign.toString();
		System.out.println(signStr);

		byte[] signData = HMACSHA256.sha256_HMACByte(signStr, secretKey);
		Encoder encoder = Base64.getEncoder();
		String sign = encoder.encodeToString(signData);

		urlMap.put("Signature", sign);
		this.urlParams = urlMap;
	}

}
