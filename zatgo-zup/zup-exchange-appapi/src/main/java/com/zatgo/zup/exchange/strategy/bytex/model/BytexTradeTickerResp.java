package com.zatgo.zup.exchange.strategy.bytex.model;

import java.math.BigDecimal;

public class BytexTradeTickerResp {

	// 成交量
	private BigDecimal amount;

	// 成交价
	private BigDecimal price;

	private Long id;

	// 买卖type，买为buy，买sell
	private String type;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
