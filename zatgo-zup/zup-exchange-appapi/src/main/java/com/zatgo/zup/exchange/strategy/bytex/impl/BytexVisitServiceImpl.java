package com.zatgo.zup.exchange.strategy.bytex.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum.BytexRespCode;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.utils.BeanToMapUtils;
import com.zatgo.zup.exchange.strategy.bytex.BytexVisitService;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexAllOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexAllOrderResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCancelOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCreateOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCreateOrderResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexResp;

@Service
public class BytexVisitServiceImpl implements BytexVisitService {

	private static final Logger log = LoggerFactory.getLogger(BytexVisitServiceImpl.class);

	@Value("${exchange.bytex.http.url:}")
	private String bytexUrl;

	@Autowired
	private OkHttpService httpService;

	@Override
	public String createOrder(BytexCreateOrderReq req) {

		Map<String, String> params = new HashMap<>();
		try {
			params = BeanToMapUtils.beanToStringMap(req);
		} catch (Exception e) {
			log.error("获取订单参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_CREATE_FAIL);
		}

		String res = httpService.postFrom(bytexUrl + "/open/api/create_order", params);
		log.info("bytex下单接口返回：" + res);

		BytexResp<BytexCreateOrderResp> resp = JSON.parseObject(res, BytexResp.class);
		if (resp == null) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR, "bytex not return data");
		}

		if (!BytexRespCode.SUCCESS.getCode().equals(resp.getCode())) {
			log.error(resp.getMsg());
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_CREATE_FAIL);
		}

		resp.setClazz(BytexCreateOrderResp.class);
		BytexCreateOrderResp orderResp = resp.getData();
		Long orderId = orderResp.getOrderId();
		if (orderId == null) {
			throw new BusinessException(getExceptionCode(resp.getCode(), BusinessExceptionCode.EX_ORDER_CREATE_FAIL),
					"bytex not return order id");
		}

		return String.valueOf(orderId);
	}

	public String getExceptionCode(String bytexCode, String defaultCode) {

		switch (bytexCode) {
		case "3":
			return BusinessExceptionCode.USER_LOCK;

		case "4":
			return BusinessExceptionCode.BALANCE_NOT_ENOUGH;

		case "9":
			return BusinessExceptionCode.USER_LOCK;

		case "100004":
			return BusinessExceptionCode.API_KEY_ERROR;

		case "100005":
			return BusinessExceptionCode.SECRET_KEY_ERROR;

		case "100007":
			return BusinessExceptionCode.IP_ILLEGAL;

		default:
			break;
		}

		return defaultCode;
	}

	@Override
	public void cancelOrder(BytexCancelOrderReq req) {
		HashMap<String, String> reqParams;
		try {
			reqParams = BeanToMapUtils.beanToHashMap(req);
		} catch (Exception e) {
			log.error("", e);
			throw new BusinessException();
		}

		String respStr = httpService.postFrom(bytexUrl + "/open/api/cancel_order", reqParams);
		BytexResp<Object> resp = JSON.parseObject(respStr, BytexResp.class);
		if (resp == null) {
			log.error("bytex返回参数：" + respStr);
			throw new BusinessException();
		}

		if (!BytexRespCode.SUCCESS.getCode().equals(resp.getCode())
				&& !BytexRespCode.ORDER_NO_EXIST.getCode().equals(resp.getCode())) {
			log.error("取消订单失败：" + resp.getMsg());
			throw new BusinessException(getExceptionCode(resp.getCode(), BusinessExceptionCode.EX_ORDER_CANNOT_CANCEL),
					resp.getMsg());
		}
	}

	@Override
	public BytexAllOrderResp getOrderList(String url, BytexAllOrderReq req) {

		HashMap<String, String> params;
		try {
			params = BeanToMapUtils.beanToHashMap(req);
		} catch (Exception e) {
			log.error("获取bytex订单列表参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		String respStr = httpService.getStr(bytexUrl + "/open/api/" + url, params);
		log.debug("bytex订单列表查询返回：" + respStr);

		BytexResp<BytexAllOrderResp> resp = JSON.parseObject(respStr, BytexResp.class);
		if (resp == null) {
			log.error("bytex订单列表数据未返回");
			throw new BusinessException();
		}

		if (!BytexRespCode.SUCCESS.getCode().equals(resp.getCode())) {
			log.error("bytex获取订单列表失败：" + resp.getMsg());
			throw new BusinessException(getExceptionCode(resp.getCode(), BusinessExceptionCode.SYSTEM_ERROR),
					resp.getMsg());
		}

		resp.setClazz(BytexAllOrderResp.class);
		BytexAllOrderResp orderResp = resp.getData();

		return orderResp;
	}

	@Override
	public BytexOrderDetailResp getOrderDetail(BytexOrderDetailReq req) {
		HashMap<String, String> params;
		try {
			params = BeanToMapUtils.beanToHashMap(req);
		} catch (Exception e) {
			log.error("获取bytex订单列表参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		String respStr = httpService.getStr(bytexUrl + "/open/api/order_info", params);
		log.debug("bytex订单详情查询返回：" + respStr);

		BytexResp<BytexOrderDetailResp> resp = JSON.parseObject(respStr, BytexResp.class);
		if (resp == null) {
			log.error("bytex订单详情数据未返回");
			throw new BusinessException();
		}

		if (!BytexRespCode.SUCCESS.getCode().equals(resp.getCode())) {
			log.error("bytex获取订单详情失败：" + resp.getMsg());
			throw new BusinessException(getExceptionCode(resp.getCode(), BusinessExceptionCode.SYSTEM_ERROR),
					resp.getMsg());
		}

		resp.setClazz(BytexOrderDetailResp.class);

		return resp.getData();
	}

}
