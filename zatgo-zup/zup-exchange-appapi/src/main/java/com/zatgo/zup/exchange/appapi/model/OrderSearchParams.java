package com.zatgo.zup.exchange.appapi.model;

import com.zatgo.zup.common.enumtype.ExchangeEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "订单查询入参")
public class OrderSearchParams extends BaseExchangeParams {

	@ApiModelProperty(value = "当前页码",required =true )
	private Integer pageNo;
	
	@ApiModelProperty(value = "每页条数",required =true )
	private Integer pageSize;
	
	@ApiModelProperty(value = "币对，市场标记，如：ethbtc",required =true )
	private String symbol;
	
	@ApiModelProperty(value = "基准货币",required =true )
	private String baseCoin;
	
	@ApiModelProperty(value = "计价货币",required =true )
	private String quoteCoin;
	
	@ApiModelProperty(value = "用户id",required = false, hidden = true)
	private String userId;
	
	@ApiModelProperty(value = "订单状态", required = false, hidden = true)
	private Byte status;
	
	@ApiModelProperty(value = "apikey", required = false)
	private String apiKey;
	
	@ApiModelProperty(value = "secretKey", required = false)
	private String secretKey;
	
	private String orderId;
	
	@ApiModelProperty(value = "列表翻页方向：next下一页，prev前一页", required = false, allowableValues = "next,prev")
	private String direct;
	
	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	@ApiModelProperty(value = "订单状态",required =true )
	private ExchangeEnum.OrderStatus[] orderStatuss;
	
	@ApiModelProperty(value = "如果有成交记录，是否返回成交明细",required =false )
	private Boolean isReturnTradeDetail;

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Boolean getIsReturnTradeDetail() {
		return isReturnTradeDetail;
	}

	public void setIsReturnTradeDetail(Boolean isReturnTradeDetail) {
		this.isReturnTradeDetail = isReturnTradeDetail;
	}

	public ExchangeEnum.OrderStatus[] getOrderStatuss() {
		return orderStatuss;
	}

	public void setOrderStatuss(ExchangeEnum.OrderStatus[] orderStatuss) {
		this.orderStatuss = orderStatuss;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}
	
}
