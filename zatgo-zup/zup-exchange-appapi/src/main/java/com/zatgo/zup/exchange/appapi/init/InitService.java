package com.zatgo.zup.exchange.appapi.init;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.exchange.service.CommonService;

@Component
public class InitService {
	
	@Autowired
	private CommonService commonService;
	
	@PostConstruct
	public void init() {
		commonService.refreshCache();
	}

}
