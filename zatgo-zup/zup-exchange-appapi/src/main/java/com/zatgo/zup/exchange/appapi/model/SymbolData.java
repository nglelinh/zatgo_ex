package com.zatgo.zup.exchange.appapi.model;

import java.math.BigDecimal;

public class SymbolData {
	
	private String symbol;
	
	private String baseCoin;
	
	private String quoteCoin;
	
	private BigDecimal amountPrecision;
	
	private BigDecimal pricePrecision;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	public BigDecimal getAmountPrecision() {
		return amountPrecision;
	}

	public void setAmountPrecision(BigDecimal amountPrecision) {
		this.amountPrecision = amountPrecision;
	}

	public BigDecimal getPricePrecision() {
		return pricePrecision;
	}

	public void setPricePrecision(BigDecimal pricePrecision) {
		this.pricePrecision = pricePrecision;
	}

}
