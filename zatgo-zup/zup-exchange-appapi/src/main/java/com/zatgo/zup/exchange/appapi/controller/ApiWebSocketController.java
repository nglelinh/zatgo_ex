package com.zatgo.zup.exchange.appapi.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.exchange.appapi.model.CloudUserCoinPairConfigData;
import com.zatgo.zup.exchange.appapi.model.DepthStepData;
import com.zatgo.zup.exchange.appapi.model.KlineData;
import com.zatgo.zup.exchange.appapi.model.ReqWebsocketParams;
import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
import com.zatgo.zup.exchange.appapi.model.TickerData;
import com.zatgo.zup.exchange.appapi.model.TradeData;
import com.zatgo.zup.exchange.appapi.util.CloudUserInfoMap;
import com.zatgo.zup.exchange.appapi.util.WebSocketSessionUtil;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@ServerEndpoint(value = "/ws/exchange/appapi")
@Component
@RestController
@Api(value = "/ws/exchange/appapi", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/ws/exchange/appapi")
public class ApiWebSocketController extends WebSocketBaseController {

	private static final Logger logger = LoggerFactory.getLogger(ApiWebSocketController.class);
	
	@OnOpen
	public void onOpen(Session session) {
		logger.debug("ws connection  sessionId : " + session.getId());
		WebSocketSessionUtil.addSession(session);
		String cloudUserId = getParameter("cloudUserId", session);
		CloudUserInfoMap.putCloudUserId(session, cloudUserId);
	}

	@OnClose
	public void onClose(Session session) {
		logger.debug("session close , sessionId = " + session.getId());
		if (WebSocketSessionUtil.getSessionsSize() > 0) {
			WebSocketSessionUtil.closeSession(session);
			CloudUserInfoMap.removeSession(session);
		}
	}

	@OnMessage
	public void onMessage(String message, Session session) throws IOException {
		logger.debug("ws连接：" + System.currentTimeMillis());
		logger.debug("websocket 接收数据：" + message);
		if (StringUtils.isEmpty(message)) {
			throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
		}
		
		String cloudUserId = getParameter("cloudUserId", session);

		ReqWebsocketParams params = JSON.parseObject(message, ReqWebsocketParams.class);
		params.setCloudUserId(cloudUserId);

		if (params.getEvent() == null) {
			throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
		}

		// 以下为websocket鉴权代码,暂未开放
//		if (false) {
//			if (!webSocketSingeUtil.verify(params)) {
//				logger.error("websocket鉴权校验失败" + JSONObject.toJSONString(params));
//				onClose(session);
//				throw new BusinessException(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
//			}
//		}
		
		WebSocketEvent event = params.getEvent();
		if(event.equals(WebSocketEvent.PONG)) {
			WebSocketSessionUtil.removePingpongmapSession(session);
			return;
		}
		

		// 订阅的通道
		String channel = params.getChannel();
		

		if (event.equals(ExchangeEnum.WebSocketEvent.SUB)) {
			
			WebSocketSessionUtil.webSocketSub(session, channel, params.getExchangeType());
			subscriptionClientListener.sendMessage(params);
			
		} else if (event.equals(ExchangeEnum.WebSocketEvent.UNSUB)) {
			
			WebSocketSessionUtil.webSocketUnsub(session, channel, params.getExchangeType());
			subscriptionClientListener.sendMessage(params);
			
		} else if (event.equals(ExchangeEnum.WebSocketEvent.REQ)) {
			
			WebSocketSessionUtil.webSocketReq(session, channel);
			webSocketReq(params,cloudUserId);
			
		} else {
			throw new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
		}
	}


	@OnError
	public void onError(Session session, Throwable error) {
		logger.error("client error sessionId:" + session.getId() + "reason:", error);
		WebSocketSessionUtil.removeSession(session);
	}

	public void sendMessage(String message, Session session) {
		try {
			if (session.isOpen()) {
				session.getBasicRemote().sendText(message);
				logger.debug("send message to client sessionId :" + session.getId() + ",message:" + message);
			}
		} catch (IOException e) {
			logger.error("推送失败：" + e.getMessage());
		}
	}

	/**
	 * 推送数据到客户端
	 * @param resp
	 */
	public <T> void pushMessageToClient(RespWebSocketData<T> resp) {
		
		// 订阅的通道
		String channel = resp.getChannel();

		//获取通道对应的session
		Set<Session> sessions = WebSocketSessionUtil.getChannelSession(channel);
		if ((sessions == null) || (sessions.isEmpty())) {
			return;
		}
		
		//发送
		String json = JSON.toJSONString(resp);
		logger.debug("ws响应：" + System.currentTimeMillis());
		for (Session session : sessions) {
			String cloudUserId = CloudUserInfoMap.getCloudUserId(session);
			// 判断是否为直连,如果是,则不做任何处理
			if (resp.getExchangeType() == null) {
				if (resp.getType().equals(WebSocketBusiType.MARKET)) {
					RespWebSocketData newResp = cloudUserInfoMap.filterCloudCoinPairByMarket(resp, cloudUserId);
					String newJson = JSON.toJSONString(newResp);
					logger.debug("pushMessageToClient-" + cloudUserId + ": " + newJson);
					sendMessage(newJson, session);
				} else {
					if (!cloudUserInfoMap.isFilterCloudCoinPairByCoinPair(resp.getExchangeType().getCode(),resp.getSymbol(), cloudUserId)) {
						logger.debug("pushMessageToClient-" + cloudUserId + ": " + json);
						sendMessage(json, session);
					}
				}
			} else {
				sendMessage(json, session);
			}
		}
		
		//发起req的通道（只发一次）
		if(WebSocketEvent.REQ.equals(resp.getEvent())) {
			WebSocketSessionUtil.removeChannel(channel);
		}
	}

	/**
	 * 发起websocket查询请求
	 * @param params
	 */
	private void webSocketReq(ReqWebsocketParams params,String cloudUserId) {
		
		String symbol = params.getSymbol();
		ExchangeType exchangeType = params.getExchangeType();
		String baseCoin = params.getBaseCoin();
		String quoteCoin = params.getQuoteCoin();
		
		//本地market数据
		if(params.getType().equals(WebSocketBusiType.MARKET) && exchangeType == null) {
			subscriptionClientListener.pushLocalMarketData(WebSocketEvent.REQ);
		}else {
			if(exchangeType == null) {
				exchangeType = getExchangeType(params.getSymbol(),cloudUserId);
			}
			
			WebSocketStrategyService service = exchangeStrategy
					.getApiWebSocketService(exchangeType);
			service.wsReq(params);
		}
	}
	
	private ExchangeType getExchangeType(String symbol,String cloudUserId) {
		
		List<CloudUserCoinPairConfigData> list = commonService.getCloudCoinPairByCloudUserId(cloudUserId);

		ExchangeType _type = null; 
		for (CloudUserCoinPairConfigData coin : list) {
			if(coin.getSymbol().equalsIgnoreCase(symbol)) {
				_type = ExchangeType.getEnumByType(coin.getExternalExchangeType());
				break;
			}
		}
		
		if(_type == null) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"coin pair " + symbol + " is not exist");
		}
		return _type;
	}
	
	/**--------------------------------------文档说明开始-------------------------------------------**/
	
	@ApiOperation(value = "订阅-K线行情")
	@RequestMapping(value = "/1", name = "订阅-K线行情", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<RespWebSocketData<List<KlineData>>> kline(@RequestBody ReqWebsocketParams params) {
		return null;
	}

	@ApiOperation(value = "订阅-前24小时行情")
	@RequestMapping(value = "/2", name = "订阅-前24小时行情", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<TickerData> ticker(@RequestBody ReqWebsocketParams params) {
		return null;
	}

	@ApiOperation(value = "订阅-实时成交信息")
	@RequestMapping(value = "/3", name = "订阅-实时成交信息", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<RespWebSocketData<List<TradeData>>> tradeTicker(@RequestBody ReqWebsocketParams params) {
		return null;
	}

	@ApiOperation(value = "订阅-深度盘口")
	@RequestMapping(value = "/4", name = "订阅-深度盘口", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<RespWebSocketData<DepthStepData>> depthStep(@RequestBody ReqWebsocketParams params) {
		return null;
	}

	@ApiOperation(value = "订阅-行情")
	@RequestMapping(value = "/5", name = "订阅-行情", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<RespWebSocketData<List<TickerData>>> market(@RequestBody ReqWebsocketParams params) {
		return null;
	}
	
	/**--------------------------------------文档说明结束-------------------------------------------**/
}
