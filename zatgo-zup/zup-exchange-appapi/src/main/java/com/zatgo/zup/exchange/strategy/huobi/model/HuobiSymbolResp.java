package com.zatgo.zup.exchange.strategy.huobi.model;

import java.util.List;

public class HuobiSymbolResp extends HuobiResp {

	private List<HuobiSymbolData> data;

	public List<HuobiSymbolData> getData() {
		return data;
	}

	public void setData(List<HuobiSymbolData> data) {
		this.data = data;
	}

}
