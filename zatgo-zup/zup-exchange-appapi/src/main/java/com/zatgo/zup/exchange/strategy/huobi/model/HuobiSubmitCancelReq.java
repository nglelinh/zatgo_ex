package com.zatgo.zup.exchange.strategy.huobi.model;

public class HuobiSubmitCancelReq extends HuobiReq {

	public HuobiSubmitCancelReq() {
		super();
	}

	public HuobiSubmitCancelReq(String accessKeyId, String secretKey, String reqMethod, String url, String method)
			throws Exception {
		super(accessKeyId, secretKey, reqMethod, method);
		sign(this, url);
	}

}
