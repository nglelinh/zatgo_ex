package com.zatgo.zup.exchange.appapi.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zatgo.zup.common.model.UserData;

@FeignClient("zup-merchant")
public interface UserRemoteService {
	
	@RequestMapping("/user/byid/{userId}")
	ResponseData<UserData> getUserById(@PathVariable("userId") String userId);
}