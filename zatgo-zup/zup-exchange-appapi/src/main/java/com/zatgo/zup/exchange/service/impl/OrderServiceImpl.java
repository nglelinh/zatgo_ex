package com.zatgo.zup.exchange.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.BusinessEnum.CoinPairConfigStatus;
import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderSource;
import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CreateOrderCallbackMsg;
import com.zatgo.zup.common.model.LockWalletBalanceParams;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.common.mq.MQProducer;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CloudUserCoinPairConfigData;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.ExOrderDetailData;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.appapi.remoteservice.PayRemoteService;
import com.zatgo.zup.exchange.appapi.remoteservice.UserRemoteService;
import com.zatgo.zup.exchange.entity.ExOrder;
import com.zatgo.zup.exchange.mapper.CloudUserCoinPairMapper;
import com.zatgo.zup.exchange.mapper.CoinPairConfigMapper;
import com.zatgo.zup.exchange.mapper.ExOrderMapper;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.service.OrderService;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.OrderStrategyService;

@Service
public class OrderServiceImpl implements OrderService {

	private static final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Autowired
	private ExOrderMapper exOrderMapper;

	@Autowired
	private CoinPairConfigMapper coinPairConfigMapper;

	@Autowired
	private PayRemoteService accountRemoteService;

	@Autowired
	private ExchangeStrategy exchangeStrategy;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private ExchangeStrategy exchangeStrategyService;
	
	@Autowired
	private com.zatgo.zup.exchange.business.OrderService businessOrderService;
	
	@Autowired
	private UserRemoteService userRemoteService;

	@Autowired
	private MQProducer mqProducer;
	
	@Autowired
	private CloudUserCoinPairMapper cloudUserCoinPairMapper;
	
	@Transactional
	@Override
	public OrderData createOrder(CreateOrderParams params, String userId) {
		ResponseData<UserData> userData = userRemoteService.getUserById(userId);
		if(!userData.isSuccessful()) {
			throw new BusinessException(userData.getCode(),userData.getMessage());
		}
		
		ExOrder exOrder = new ExOrder();
		String orderId = UUIDUtils.getUuid();
		exOrder.setOrderId(orderId);
		exOrder.setUserId(userId);

		CloudUserCoinPairConfigData config = cloudUserCoinPairMapper.selectCloudUserCoinPairConfig(params.getSymbol(),userData.getData().getCloudUserId(),CoinPairConfigStatus.OPEN.getCode().intValue());

		if (config == null || !CoinPairConfigStatus.OPEN.getCode().equals(config.getStatus())) {
			throw new BusinessException(BusinessExceptionCode.COIN_PAIR_CONFIG_CLOSE);
		}
		
		SymbolData symbolData = commonService.getSymbolDataCache(params.getBaseCoin(), params.getQuoteCoin(),ExchangeType.getEnumByType(config.getExternalExchangeType()));
		if(symbolData == null) {
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_CREATE_FAIL);
		}
		
		int pricePrecision = symbolData.getPricePrecision().intValue();
		int amountPrecision = symbolData.getAmountPrecision().intValue();
		
		BigDecimal price = params.getPrice();
		if(price != null) {
			int precision = price.scale();
			if (precision > pricePrecision) {
				throw new BusinessException(BusinessExceptionCode.EX_ORDER_CREATE_FAIL, "price precision error");
			}
			
		}
		
		BigDecimal volume = params.getVolume();
		if (volume != null) {
			int precision = volume.scale();
			if (params.getSide().equals(ExchangeSide.BUY)
					&& params.getType().equals(EntryOrdersType.marketPriceEntrust)) {
				if (precision > pricePrecision) {
					throw new BusinessException(BusinessExceptionCode.EX_ORDER_CREATE_FAIL, "volume precision error");
				}
			} else {
				if (precision > amountPrecision) {
					throw new BusinessException(BusinessExceptionCode.EX_ORDER_CREATE_FAIL, "volume precision error");
				}
			}
		}

		if (volume != null && price != null) {
			int precision = volume.multiply(price).scale();
			if (precision > 16) {
				throw new BusinessException(BusinessExceptionCode.EX_ORDER_CREATE_FAIL, "precision error");
			}
		}

		exOrder.setExternalExchangeType(config.getExternalExchangeType());
		String side = params.getSide().getCode();

		BigDecimal lockAmount = params.getVolume();
		String type = params.getType().getCode();
		if (type.equals(EntryOrdersType.limitPriceEntrust.getCode())) {
			if (side.equals(ExchangeSide.BUY.getCode())) {
				lockAmount = price.multiply(lockAmount);
			}
			exOrder.setAvgPrice(price);
		} else {
			price = BigDecimal.ZERO;
		}

		exOrder.setExternalExchangeType(config.getExternalExchangeType());
		exOrder.setSide(side);
		exOrder.setPrice(price);
		exOrder.setVolume(volume);
		exOrder.setStatus(new Byte(OrderStatus.UNCOMMITTED.getCode()));
		exOrder.setType(new Byte(type));
		exOrder.setCreateDate(new Date());
		exOrder.setSource(OrderSource.APP.getCode());
		exOrder.setBaseCoin(params.getBaseCoin());
		exOrder.setQuoteCoin(params.getQuoteCoin());
		exOrder.setCoinPairSymbol(params.getSymbol());
		exOrder.setMakerFeeIncrease(config.getMakerFeeIncrease());
		exOrder.setTakerFeeIncrease(config.getTakerFeeIncrease());
		exOrder.setDealVolume(BigDecimal.ZERO);
		exOrderMapper.insertSelective(exOrder);

		String coinType = null;
		if (side.equals(ExchangeSide.BUY.getCode())) {
			coinType = params.getQuoteCoin();
		} else if (side.equals(ExchangeSide.SELL.getCode())) {
			coinType = params.getBaseCoin();
		}
		
		LockWalletBalanceParams walletParams = new LockWalletBalanceParams();

		walletParams.setLockBalanceType(BusinessEnum.LockBalanceOperateType.LOCK);
		walletParams.setCoinType(coinType);
		walletParams.setLockAmount(lockAmount);
		walletParams.setUserId(userId);
		walletParams.setOrderId(orderId);

		log.info("冻结用户账户余额：" + JSON.toJSONString(walletParams));

		ResponseData<Object> res = accountRemoteService.updateLockBalance(walletParams);
		log.info("冻结用户用户返回：" + JSON.toJSONString(res));

		if (!res.isSuccessful()) {
			throw new BusinessException(res.getCode(), res.getMessage());
		}
		CreateOrderCallbackMsg<ExOrder> msg = new CreateOrderCallbackMsg();
		msg.setCount(0);
		msg.setResult(exOrder);
		mqProducer.send(MQContants.ZUP_ORDER_CREATE_TOPIC, null, JSONObject.toJSONString(msg),
				exOrder.getUserId() + "_" +exOrder.getOrderId());

		OrderData orderData = new OrderData();
		BeanUtils.copyProperties(exOrder, orderData);
		orderData.setSymbol(exOrder.getCoinPairSymbol());
		return orderData;
	}

	@Transactional
	@Override
	public OrderData cancelOrder(CancelOrderParams params, String userId) {

		ExOrder order = exOrderMapper.selectByPrimaryKey(params.getOrderId());
		if (order == null || !order.getUserId().equals(userId)) {
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_NOT_EXIST);
		}

		// 订单状态：0 init，1 new，2 filled，3 part_filled，4 canceled，5 pending_cancel，6 expired
		if (order.getStatus().equals(new Byte(OrderStatus.FILLED.getCode()))
				|| order.getStatus().equals(new Byte(OrderStatus.CANCELED.getCode()))
				|| order.getStatus().equals(new Byte(OrderStatus.PENDING_CANCEL.getCode()))
				|| order.getStatus().equals(new Byte(OrderStatus.EXCEPTION.getCode()))) {
			throw new BusinessException(BusinessExceptionCode.EX_ORDER_CANNOT_CANCEL);
		}

		OrderStrategyService service = exchangeStrategy
				.getOrderService(ExchangeType.getEnumByType(order.getExternalExchangeType()));
		
		boolean res = service.cancelOrder(order);
		if(res) {
			ExOrder record = new ExOrder();
			record.setOrderId(order.getOrderId());
			record.setExternalOrderId(order.getExternalOrderId());
			record.setStatus(new Byte(OrderStatus.CANCELED.getCode()));
			record.setUpdateDate(new Date());
			exOrderMapper.updateByPrimaryKeySelective(record);

			if(StringUtils.isEmpty(order.getExternalOrderId())) {
				commonService.unLockAccountBalance(order);
			} else {
				OrderStrategyService orderStrategyService = exchangeStrategyService.getOrderService(
						ExchangeEnum.ExchangeType.getEnumByType(order.getExternalExchangeType()));
				ExOrderDetailData newOrderData = orderStrategyService.getOrderByOrderId(order);

				if (newOrderData == null) {
					// 同步订单状态失败
					throw new BusinessException(BusinessExceptionCode.EX_ORDER_CANCEL_FAIL, "sync order fail");
				}

				businessOrderService.syncOrder(newOrderData);
			}
			
			OrderData data = new OrderData();
			BeanUtils.copyProperties(data, order);
			data.setSymbol(order.getCoinPairSymbol());
			data.setStatus(OrderStatus.CANCELED);
			
			return data;
		}
		
		throw new BusinessException(BusinessExceptionCode.EX_ORDER_CANCEL_FAIL);
	}

	@Override
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params) {
		PageHelper.startPage(params.getPageNo(), params.getPageSize());

		OrderStatus[] status = params.getOrderStatuss();
		if(status != null && status.length > 0) {
			List<OrderStatus> _status = new ArrayList<>(Arrays.asList(status));
			for (OrderStatus orderStatus : status) {
				if(orderStatus.equals(OrderStatus.INIT)) {
					_status.add(OrderStatus.UNCOMMITTED);
					params.setOrderStatuss(_status.toArray(new OrderStatus[] {}));
					break;
				}
			}
		}
		Page<ExOrder> datas = exOrderMapper.selectOrderByOrderSearchParams(params);

		List<OrderData> result = new ArrayList<OrderData>();
		List<ExOrder> _result = datas.getResult();
		if (CollectionUtils.isNotEmpty(_result)) {
			for (ExOrder order : _result) {
				OrderData data = new OrderData();
				BeanUtils.copyProperties(order, data);
				data.setSymbol(order.getCoinPairSymbol());
				data.setFeeIsUserPlatformCoin(false);
				data.setType(EntryOrdersType.getEnumByType(order.getType().toString()));
				data.setSide(ExchangeSide.getEnumByType(order.getSide()));
				data.setStatus(OrderStatus.getEnumByType(order.getStatus().toString()));
				if (order.getType().equals(new Byte(EntryOrdersType.marketPriceEntrust.getCode()))) {
					if(order.getSide().equals(ExchangeSide.BUY.getCode())) {
						BigDecimal dealVolume = data.getAvgPrice().multiply(data.getDealVolume());
						data.setRemainVolume(order.getVolume().subtract(dealVolume));
					} else if(order.getSide().equals(ExchangeSide.SELL.getCode())) {
						BigDecimal dealVolume = order.getDealVolume() == null ? BigDecimal.ZERO : order.getDealVolume();
						data.setRemainVolume(order.getVolume().subtract(dealVolume));
					}
				} else if (order.getType().equals(new Byte(EntryOrdersType.limitPriceEntrust.getCode()))) {
					BigDecimal dealVolume = order.getDealVolume() == null ? BigDecimal.ZERO : order.getDealVolume();
					data.setRemainVolume(order.getVolume().subtract(dealVolume));
				}
				
				result.add(data);
			}
		}

		PageInfo<OrderData> pageInfo = new PageInfo<>(datas.getPageNum(), datas.getPageSize(), datas.getTotal(),
				result);

		PageHelper.clearPage();
		return pageInfo;
	}

	@Override
	public OrderData getOrderByOrderId(OrderDetailParams params) {
		
		String orderId = params.getOrderId();
		String userId = params.getUserId();

		ExOrder order = exOrderMapper.selectByPrimaryKey(orderId);

		OrderData data = new OrderData();
		if (order != null && order.getUserId().equals(userId)) {
			BeanUtils.copyProperties(order, data);
			data.setSymbol(order.getCoinPairSymbol());
			data.setFeeIsUserPlatformCoin(false);
			data.setType(EntryOrdersType.getEnumByType(order.getType().toString()));
			data.setSide(ExchangeSide.getEnumByType(order.getSide()));
			data.setStatus(OrderStatus.getEnumByType(order.getStatus().toString()));
		}
		return data;
	}

}
