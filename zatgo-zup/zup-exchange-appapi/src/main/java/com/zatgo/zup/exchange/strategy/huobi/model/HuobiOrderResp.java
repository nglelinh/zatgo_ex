package com.zatgo.zup.exchange.strategy.huobi.model;

public class HuobiOrderResp extends HuobiResp {
	
	private HuobiOrderData data;

	public HuobiOrderData getData() {
		return data;
	}

	public void setData(HuobiOrderData data) {
		this.data = data;
	}
	
}
