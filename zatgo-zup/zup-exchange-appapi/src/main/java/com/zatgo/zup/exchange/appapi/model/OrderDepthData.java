package com.zatgo.zup.exchange.appapi.model;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class OrderDepthData {

	@ApiModelProperty(value = "卖方深度列表",required =true )
	private List<OrderData> asks;
	
	@ApiModelProperty(value = "买方深度列表",required =true )
	private List<OrderData> bids;

	public List<OrderData> getAsks() {
		return asks;
	}

	public void setAsks(List<OrderData> asks) {
		this.asks = asks;
	}

	public List<OrderData> getBids() {
		return bids;
	}

	public void setBids(List<OrderData> bids) {
		this.bids = bids;
	}
	
	
}
