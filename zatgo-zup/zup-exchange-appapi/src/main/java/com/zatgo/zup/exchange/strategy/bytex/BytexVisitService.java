package com.zatgo.zup.exchange.strategy.bytex;

import com.zatgo.zup.exchange.strategy.bytex.model.BytexAllOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexAllOrderResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCancelOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCreateOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailResp;

public interface BytexVisitService {

	/**
	 * 创建订单
	 * 
	 * @param req
	 * @return
	 */
	public String createOrder(BytexCreateOrderReq req);

	/**
	 * 取消订单
	 * 
	 * @param req
	 */
	public void cancelOrder(BytexCancelOrderReq req);

	/**
	 * 查询订单列表
	 * 
	 * @param url
	 * @param params
	 * @return
	 */
	public BytexAllOrderResp getOrderList(String url, BytexAllOrderReq req);

	/**
	 * 查询订单详情
	 * 
	 * @return
	 */
	public BytexOrderDetailResp getOrderDetail(BytexOrderDetailReq req);

}
