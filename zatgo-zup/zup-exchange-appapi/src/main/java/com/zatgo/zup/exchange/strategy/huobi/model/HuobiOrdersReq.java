package com.zatgo.zup.exchange.strategy.huobi.model;

import com.alibaba.fastjson.annotation.JSONField;

public class HuobiOrdersReq extends HuobiReq {

	/**
	 * 交易对
	 */
	private String symbol;

	/**
	 * 查询的订单类型组合，使用','分割buy-market：市价买, sell-market：市价卖, buy-limit：限价买,
	 * sell-limit：限价卖, buy-ioc：IOC买单, sell-ioc：IOC卖单
	 */
	private String types;

	/**
	 * 查询开始日期, 日期格式yyyy-mm-dd
	 */
	@JSONField(name = "start-date")
	private String startDate;

	/**
	 * 查询结束日期, 日期格式yyyy-mm-dd
	 */
	@JSONField(name = "end-date")
	private String endDate;

	/**
	 * 查询的订单状态组合，使用','分割submitted 已提交, partial-filled 部分成交, partial-canceled 部分成交撤销,
	 * filled 完全成交, canceled 已撤销
	 */
	private String states;

	/**
	 * 查询起始 ID
	 */
	private String from;

	/**
	 * 查询方向prev 向前，next 向后
	 */
	private String direct;

	/**
	 * 查询记录大小
	 */
	private String size;
	
	public HuobiOrdersReq() {
		super();
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStates() {
		return states;
	}

	public void setStates(String states) {
		this.states = states;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

}
