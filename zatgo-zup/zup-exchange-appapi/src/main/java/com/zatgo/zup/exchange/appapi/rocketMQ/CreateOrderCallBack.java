package com.zatgo.zup.exchange.appapi.rocketMQ;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.model.CreateOrderCallbackMsg;
import com.zatgo.zup.common.mq.MQConsumerCallBack;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.common.mq.MQProducer;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.business.OrderService;
import com.zatgo.zup.exchange.entity.ExOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 46041 on 2018/9/25.
 */

@Component
public class CreateOrderCallBack implements MQConsumerCallBack {

    private static final Logger logger = LoggerFactory.getLogger(CreateOrderCallBack.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private MQProducer producer;

    private static final Map<Integer,Integer> delayMap = new HashMap<>();

    /**
     * value表示消息延时级别 1延时3s,2延时30s,3延时3m，4表示延时30m，5表示延时1h
     */
    static {
        delayMap.put(1,3*1000);
        delayMap.put(2,30*1000);
        delayMap.put(3,30*1000*6);
        delayMap.put(4,30*1000*60);
        delayMap.put(5,1000*60*60);
    }

    @Override
    public boolean callBack(String json) {
        CreateOrderCallbackMsg<ExOrder> createOrderCallbackMsg = JSONObject.parseObject(json, new TypeReference<CreateOrderCallbackMsg<ExOrder>>(){});
        ExOrder result = createOrderCallbackMsg.getResult();
        Integer count = createOrderCallbackMsg.getCount();
        CreateOrderCallbackMsg<ExOrder> msg = new CreateOrderCallbackMsg();
        try {
            ExOrder order = orderService.createOrder(result);
            String status = order.getStatus().toString();
            //失败重试
            if (ExchangeEnum.OrderStatus.EXCEPTION.getCode().equals(status)){
                if (count < 5){
                    count++;
                }

                BeanUtils.copyProperties(result, order);
                msg.setCount(count);
                msg.setResult(result);
                producer.sendDelay(MQContants.ZUP_ORDER_CREATE_TOPIC, null, JSONObject.toJSONString(msg),
                        result.getUserId() + "_" +result.getOrderId(), delayMap.get(count).longValue());
            }
        } catch (Exception e){
            logger.error("回调结果："+json);
            logger.error("", e);
            msg.setCount(count);
            msg.setResult(result);
            producer.sendDelay(MQContants.ZUP_ORDER_CREATE_TOPIC, null, JSONObject.toJSONString(msg),
                    result.getUserId() + "_" +result.getOrderId(), delayMap.get(count).longValue());
        }
        return true;
    }
}
