package com.zatgo.zup.exchange.strategy.huobi.model;

import java.math.BigDecimal;

public class HuobiKlineWsResp {

	// K线id
	private String id;

	// 成交量
	private BigDecimal amount;

	// 成交笔数
	private BigDecimal count;

	// 开盘价
	private BigDecimal open;

	// 收盘价,当K线为最晚的一根时，是最新成交价
	private BigDecimal close;

	// 最低价
	private BigDecimal low;

	// 最高价
	private BigDecimal high;

	// 成交额, 即 sum(每一笔成交价 * 该笔的成交量)
	private BigDecimal vol;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCount() {
		return count;
	}

	public void setCount(BigDecimal count) {
		this.count = count;
	}

	public BigDecimal getOpen() {
		return open;
	}

	public void setOpen(BigDecimal open) {
		this.open = open;
	}

	public BigDecimal getClose() {
		return close;
	}

	public void setClose(BigDecimal close) {
		this.close = close;
	}

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public BigDecimal getVol() {
		return vol;
	}

	public void setVol(BigDecimal vol) {
		this.vol = vol;
	}

}
