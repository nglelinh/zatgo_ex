package com.zatgo.zup.exchange.strategy.bytex.model;

import java.util.List;

public class BytexAllOrderResp {
	
	private Integer count;
	
	private List<BytexExOrder> orderList;
	
	private List<BytexExOrder> resultList;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<BytexExOrder> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<BytexExOrder> orderList) {
		this.orderList = orderList;
	}

	public List<BytexExOrder> getResultList() {
		return resultList;
	}

	public void setResultList(List<BytexExOrder> resultList) {
		this.resultList = resultList;
	}

}
