package com.zatgo.zup.exchange.service;

import java.util.List;
import java.util.Map;

import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.exchange.appapi.model.CloudUserCoinPairConfigData;
import com.zatgo.zup.exchange.appapi.model.CoinPair;
import com.zatgo.zup.exchange.appapi.model.CoinPairData;
import com.zatgo.zup.exchange.appapi.model.RateData;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.entity.ExOrder;

public interface CommonService {
	
	/**
	 * 查询直连模式的币对
	 * @param exchangeType
	 * @return
	 */
	public CoinPairData getDirectCoinPair(ExchangeType exchangeType);
	
	/**
	 * 查询本地模式币对
	 * @return
	 */
	public CoinPairData getLocalCoinPair(String cloudUserId);
	/**
	 * 查询汇率
	 * @return
	 */
	public RateData getCoinRateData(ExchangeEnum.ExchangeType type,String cloudUserId);
	
	/**
	 * 初始化币对列表
	 * @return
	 */
	public void refreshCache();
	
	/**
	 * 
	 * @param order
	 */
	public void unLockAccountBalance(ExOrder order);
	
	public SymbolData getSymbolDataCache(String baseCoin, String quoteCoin,ExchangeType type);

	/**
	 * 查询云用户的币对
	 * @param cloudUserId
	 * @return
	 */
	List<CloudUserCoinPairConfigData> getCloudCoinPairByCloudUserId(String cloudUserId);

	
	List<CoinPairConfig> getCoinPairCache(); 
	
	CoinPairConfig getCoinPairCache(String symbol); 
	
	List<CloudUserCoinPairConfigData> getCloudUserCoinPairCache(String cloudUserId); 
	
	CloudUserCoinPairConfigData getCloudUserCoinPairCache(String symbol,String cloudUserId); 
	
	SymbolData getSymbolInfo(String symbol);
	
	String getQuoteCoin(String symbol);
	
	List<String> getExchangeType();
	
	List<CoinPairConfig> getLocalCoinPairByExchangeType(String exchangeType);

}
