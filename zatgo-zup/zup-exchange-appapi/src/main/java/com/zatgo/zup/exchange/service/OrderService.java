package com.zatgo.zup.exchange.service;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;

public interface OrderService {

	/**
	 * 创建本地订单
	 * @param params
	 * @param userId
	 * @return
	 */
	public OrderData createOrder(CreateOrderParams params, String userId);
	
	/**
	 * 取消订单
	 * @param params
	 * @param userId
	 * @return
	 */
	public OrderData cancelOrder(CancelOrderParams params,String userId);
	
	/**
	 * 查询用户订单列表
	 * @param params
	 * @return
	 */
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params);
	
	/**
	 * 查询订单详情
	 * @param orderId
	 * @param userId
	 * @return
	 */
	public OrderData getOrderByOrderId(OrderDetailParams params);
}
