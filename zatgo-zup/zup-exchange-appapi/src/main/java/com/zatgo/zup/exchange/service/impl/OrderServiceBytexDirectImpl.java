package com.zatgo.zup.exchange.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.service.OrderService;
import com.zatgo.zup.exchange.strategy.bytex.BytexVisitService;
import com.zatgo.zup.exchange.strategy.bytex.OrderServiceBytexImpl;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexAllOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexAllOrderResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCancelOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCreateOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexExOrder;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailData;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailResp;

@Service("orderServiceImpl_bytex_direct")
public class OrderServiceBytexDirectImpl implements OrderService {

	private static final Logger log = LoggerFactory.getLogger(OrderServiceBytexImpl.class);

	@Autowired
	private BytexVisitService bytexService;

	@Override
	public OrderData createOrder(CreateOrderParams params, String userId) {

		BytexCreateOrderReq req = new BytexCreateOrderReq(params.getApiKey(), params.getSecretKey(), params);

		String orderId = bytexService.createOrder(req);

		OrderData data = new OrderData();
		data.setBaseCoin(params.getBaseCoin());
		data.setFeeIsUserPlatformCoin(params.getFeeIsUserPlatformCoin());
		data.setQuoteCoin(params.getQuoteCoin());
		data.setSide(params.getSide());
		data.setSymbol(params.getSymbol());
		data.setType(params.getType());
		data.setVolume(params.getVolume());
		data.setOrderId(orderId);

		return data;
	}

	@Override
	public OrderData cancelOrder(CancelOrderParams params, String userId) {

		BytexCancelOrderReq req = new BytexCancelOrderReq(params.getApiKey(), params.getSecretKey(), params);

		bytexService.cancelOrder(req);

		OrderData data = new OrderData();
		data.setOrderId(params.getOrderId());
		data.setBaseCoin(params.getBaseCoin());
		data.setQuoteCoin(params.getQuoteCoin());
		data.setSymbol(params.getSymbol());

		return data;
	}

	@Override
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params) {
		BytexAllOrderReq req = new BytexAllOrderReq(null, params.getSymbol(), params.getPageNo(), params.getPageSize(),
				params.getApiKey(), params.getSecretKey());

		OrderStatus[] status = params.getOrderStatuss();

		int size = 0;
		if (status != null && status.length > 0) {
			size = status.length;
		}

		String url = "all_order";
		switch (size) {
		case 4:
			url = "new_order";
			break;

		default:
			url = "all_order";
			break;
		}

		BytexAllOrderResp orderResp = bytexService.getOrderList(url, req);

		PageInfo<OrderData> pageInfo = new PageInfo<>(params.getPageNo(), params.getPageSize(), orderResp.getCount(),
				new ArrayList<>());

		if (orderResp.getCount() == 0) {
			return pageInfo;
		}

		List<OrderData> res = new ArrayList<>();
		List<BytexExOrder> orderList = new ArrayList<>();

		switch (size) {
		case 4:
			orderList = CollectionUtils.isEmpty(orderResp.getResultList()) ? new ArrayList<>()
					: orderResp.getResultList();
			break;

		case 2:
			orderList = CollectionUtils.isEmpty(orderResp.getResultList()) ? new ArrayList<>()
					: orderResp.getResultList();
			break;

		default:
			orderList = CollectionUtils.isEmpty(orderResp.getOrderList()) ? new ArrayList<>()
					: orderResp.getOrderList();
			break;
		}

		for (BytexExOrder bytexExOrder : orderList) {
			OrderData orderData = new OrderData();
			orderData.setAvgPrice(bytexExOrder.getAvgPrice());
			orderData.setBaseCoin(bytexExOrder.getBaseCoin());
			orderData.setCreateDate(bytexExOrder.getCreatedAt());
			orderData.setDealVolume(bytexExOrder.getDealVolume());
			orderData.setOrderId(String.valueOf(bytexExOrder.getId()));
			orderData.setPrice(bytexExOrder.getPrice());
			orderData.setQuoteCoin(bytexExOrder.getCountCoin());
			orderData.setSide(ExchangeSide.getEnumByType(bytexExOrder.getSide()));
			orderData.setStatus(OrderStatus.getEnumByType(bytexExOrder.getStatus().toString()));
			orderData.setSymbol(bytexExOrder.getBaseCoin() + bytexExOrder.getCountCoin());
			orderData.setType(EntryOrdersType.getEnumByType(bytexExOrder.getType().toString()));
			orderData.setVolume(bytexExOrder.getVolume());
			orderData.setRemainVolume(bytexExOrder.getRemainVolume());
			res.add(orderData);
		}

		pageInfo.setList(res);

		return pageInfo;
	}

	@Override
	public OrderData getOrderByOrderId(OrderDetailParams params) {

		BytexOrderDetailReq req = new BytexOrderDetailReq(params.getApiKey(), params.getSecretKey(),
				params.getOrderId(), params.getSymbol());

		BytexOrderDetailResp resp = bytexService.getOrderDetail(req);

		BytexOrderDetailData bytexOrder = resp.getOrderInfo();
		OrderData orderData = new OrderData();
		orderData.setOrderId(String.valueOf(bytexOrder.getId()));
		orderData.setSide(ExchangeSide.getEnumByType(bytexOrder.getSide()));
		orderData.setCreateDate(bytexOrder.getCreatedAt());
		orderData.setPrice(bytexOrder.getPrice());
		orderData.setVolume(bytexOrder.getVolume());
		orderData.setDealVolume(bytexOrder.getDealVolume());
		orderData.setAvgPrice(bytexOrder.getAvgPrice());

		return orderData;
	}

}
