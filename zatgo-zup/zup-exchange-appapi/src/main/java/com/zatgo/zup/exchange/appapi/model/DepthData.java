package com.zatgo.zup.exchange.appapi.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("深度数据")
public class DepthData {
	
	@ApiModelProperty(value = "价格", required = true)
	private BigDecimal price;
	
	@ApiModelProperty(value = "数量", required = true)
	private BigDecimal amount;

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
