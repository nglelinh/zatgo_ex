package com.zatgo.zup.exchange.appapi.model;

import java.math.BigDecimal;
import java.util.Date;

import com.zatgo.zup.common.enumtype.ExchangeEnum;

import io.swagger.annotations.ApiModelProperty;

public class TradeData {

	@ApiModelProperty(value = "交易ID",required =true )
	private String id;
	
	@ApiModelProperty(value = "买卖方向",required =true )
	private ExchangeEnum.ExchangeSide side;
	
	@ApiModelProperty(value = "单价",required =true )
	private BigDecimal price;
	
	@ApiModelProperty(value = "数量",required =true )
	private BigDecimal vol;
	
	@ApiModelProperty(value = "总额",required =true )
	private BigDecimal amount;
	
	@ApiModelProperty(value = "数据产生时间",required =true )
	private Long ts;
	
	@ApiModelProperty(value = "时间",required =true )
	private Date ds;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ExchangeEnum.ExchangeSide getSide() {
		return side;
	}

	public void setSide(ExchangeEnum.ExchangeSide side) {
		this.side = side;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getVol() {
		return vol;
	}

	public void setVol(BigDecimal vol) {
		this.vol = vol;
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

	public Date getDs() {
		return ds;
	}

	public void setDs(Date ds) {
		this.ds = ds;
	}
	
	
}
