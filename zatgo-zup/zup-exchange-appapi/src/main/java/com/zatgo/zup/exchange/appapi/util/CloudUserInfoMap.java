package com.zatgo.zup.exchange.appapi.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.openservices.shade.com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.exchange.appapi.model.CloudUserCoinPairConfigData;
import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
import com.zatgo.zup.exchange.appapi.model.TickerData;
import com.zatgo.zup.exchange.service.CommonService;

/**
 * Created by 46041 on 2018/9/18.
 */
@Component
public class CloudUserInfoMap {
	
	private static final Logger logger = LoggerFactory.getLogger(CloudUserInfoMap.class);

	@Autowired
    private RedisUtils redisUtils;
	
	@Autowired
    private CommonService commonService;


    /**
     *  key 为userId
     *  value 为 cloudUserId
     */
    private static final HashMap<String, String> USER_INFO_MAP = new HashMap<>();
    /**
     * key 为 session
     * value 为 云用户ID
     */
    private static final ConcurrentHashMap<Session, String> sessionMap = new ConcurrentHashMap();

    /**
     * key 为 币对
     * value 为  cloudUserID集合
     */
    private static HashMap<String, List<String>> COINPAIR_CLOUDUSERID_MAP = null;

    private static final String KEY = "CLOUD:COIN:PAIR:";

    //一小时
    private static final Long TIME = 1l;

    //调用count次后重新初始化
    private static Long count = 10000l;


    public static void put(String k, String v){
        USER_INFO_MAP.put(getUserKey(k), v);
    }

    public static String get(String k){
        return USER_INFO_MAP.get(getUserKey(k));
    }

    public static String getCloudUserId(Session session){
        return sessionMap.get(session);
    }

    public static void putCloudUserId(Session key, String cloudUserId){
        sessionMap.put(key, cloudUserId);
    }

    public static void removeSession(Session key){
        sessionMap.remove(key);
    }


    public void initCoinPair(){
        redisUtils.removeKey(getCloudUserKey("*"));
        redisUtils.removeKey(getCoinPairCloudUserIdKey());
    }

    /**
     * 是否过虑当前数据
     * @param exchangeType
     * @param coinPair
     * @param cloudUserId
     * @return
     */
    public boolean isFilterCloudCoinPairByCoinPair(String exchangeType,String coinPair, String cloudUserId){
        coinPair = coinPair.toLowerCase();
        if(coinPair.equals("bkbt")) {
        	System.out.println(exchangeType+coinPair+cloudUserId);
        }
        CloudUserCoinPairConfigData  data = commonService.getCloudUserCoinPairCache(coinPair.toLowerCase(), cloudUserId);
        if(data == null || !exchangeType.equals(data.getExternalExchangeType())) {
        	return true;
        }else {
        	return false;
        }
    }

    //coinPair 为币对   BTCETH
    public RespWebSocketData filterCloudCoinPairByMarket(RespWebSocketData resp, String cloudUserId){
        Map<String,Map<String, List<TickerData>>> data = (Map<String,Map<String, List<TickerData>>>)resp.getData();
        Map<String, List<TickerData>> newData = new HashMap<>();
        
        for (Map.Entry<String,Map<String, List<TickerData>>> m : data.entrySet()){
        	List<TickerData> newTickerDatas = new ArrayList<>();

        	Map<String, List<TickerData>> valueType = m.getValue();
        	for (Map.Entry<String, List<TickerData>> n : valueType.entrySet()){
        		
        		List<TickerData> value = n.getValue();
        		for (int i = value.size() - 1; i >= 0; i--){
        			
                    TickerData tickerData = value.get(i);
                    String coinPair = (tickerData.getBaseCoin() + tickerData.getQuoteCoin()).toLowerCase();
                    if (!isFilterCloudCoinPairByCoinPair(n.getKey(),coinPair, cloudUserId)){
                    	newTickerDatas.add(tickerData);
                    }
                    
                }
        	}
            
        	newData.put(m.getKey(), newTickerDatas);
        }

        RespWebSocketData newResp = new RespWebSocketData<>();
        newResp.setBaseCoin(resp.getBaseCoin());
        newResp.setData(newData);
        newResp.setDepthStep(resp.getDepthStep());
        newResp.setEvent(resp.getEvent());
        newResp.setExchangeType(resp.getExchangeType());
        newResp.setIsDirect(resp.getIsDirect());
        newResp.setKlineTime(resp.getKlineTime());
        newResp.setMD5(MD5.GetMD5Code(JSON.toJSONString(newData)));
        newResp.setOperateType(resp.getOperateType());
        newResp.setQuoteCoin(resp.getQuoteCoin());
        newResp.setSymbol(resp.getSymbol());
        newResp.setType(resp.getType());

        return newResp;
    }




    private static String getUserKey(String str){
        return KEY + "USER:" + str;
    }

    private static String getCloudUserKey(String str){
        return KEY + "CLOUD_USER:" + str;
    }

    private static String getCoinPairCloudUserIdKey(){
        return KEY + "COIN_PAIR:CLOUD_USER_ID:";
    }
}
