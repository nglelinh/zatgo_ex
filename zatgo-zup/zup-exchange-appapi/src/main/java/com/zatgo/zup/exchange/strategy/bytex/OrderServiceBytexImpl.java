package com.zatgo.zup.exchange.strategy.bytex;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.BusinessEnum.CoinPairConfigStatus;
import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CloudUserCoinPairConfigData;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.ExOrderDetailData;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.appapi.remoteservice.UserRemoteService;
import com.zatgo.zup.exchange.entity.ExOrder;
import com.zatgo.zup.exchange.entity.ExTrade;
import com.zatgo.zup.exchange.mapper.CloudUserCoinPairMapper;
import com.zatgo.zup.exchange.mapper.CoinPairConfigMapper;
import com.zatgo.zup.exchange.mapper.ExOrderMapper;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.OrderStrategyService;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexAllOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexAllOrderResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCancelOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexCreateOrderReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexExOrder;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailData;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderDetailResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexOrderTradeData;

@Service("OrderService_bytex")
public class OrderServiceBytexImpl implements OrderStrategyService {

	private static final Logger logger = LoggerFactory.getLogger(OrderServiceBytexImpl.class);

	@Value("${exchange.bytex.http.url:}")
	private String bytexUrl;

	@Value("${exchange.bytex.apiKey:}")
	private String apiKey;

	@Value("${exchange.bytex.secretKey:}")
	private String secretKey;

	@Autowired
	private ExOrderMapper exOrderMapper;

	@Autowired
	private CoinPairConfigMapper coinPairConfigMapper;

	@Autowired
	private CommonService commonService;

	@Autowired
	private BytexVisitService bytexService;
	
	@Autowired
	private UserRemoteService userRemoteService;
	
	@Autowired
	private CloudUserCoinPairMapper cloudUserCoinPairMapper;

	@Override
	public String createOrder(ExOrder order) {

		if (StringUtils.isEmpty(bytexUrl)) {
			logger.error("bytex url 未配置");
			return null;
		}
		
		ResponseData<UserData>  userData = userRemoteService.getUserById(order.getUserId());
		if(!userData.isSuccessful()) {
			throw new BusinessException(userData.getCode(),userData.getMessage());
		}

		CloudUserCoinPairConfigData config =  cloudUserCoinPairMapper.
				selectCloudUserCoinPairConfig(order.getCoinPairSymbol(), userData.getData().getCloudUserId(), CoinPairConfigStatus.OPEN.getCode().intValue());

		if (config == null || !CoinPairConfigStatus.OPEN.getCode().equals(config.getStatus())) {
			logger.error("当前币对不支持下单：" + order.getBaseCoin() + "/" + order.getQuoteCoin());
			return null;
		}

		BytexCreateOrderReq req = new BytexCreateOrderReq(apiKey, secretKey, order);
		String orderResStr = JSON.toJSONString(req);
		logger.info("bytex下单接口调用：" + orderResStr);

		try {
			String orderId = bytexService.createOrder(req);
			return orderId;
		} catch (BusinessException e) {
			if (BusinessExceptionCode.EX_ORDER_CREATE_FAIL.equals(e.getCode())) {
				return null;
			}
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Boolean cancelOrder(ExOrder order) {

		if (StringUtils.isEmpty(order.getExternalOrderId())) {
			BytexExOrder bytexExOrder = searchBytexExOrder(order);
			if (bytexExOrder == null) {
				return true;
			} else {
				order.setExternalOrderId(bytexExOrder.getId().toString());
			}
		}

		BytexCancelOrderReq req = new BytexCancelOrderReq(apiKey, secretKey, order);
		bytexService.cancelOrder(req);

		return true;

	}

	@Override
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params) {
		BytexAllOrderReq req = new BytexAllOrderReq(null, params.getSymbol(), params.getPageNo(), params.getPageSize(),
				params.getApiKey(), params.getSecretKey());

		OrderStatus[] status = params.getOrderStatuss();

		int size = 0;
		if (status != null && status.length > 0) {
			size = status.length;
		}

		String url = "all_order";
		switch (size) {
		case 4:
			url = "new_order";
			break;

		default:
			url = "all_order";
			break;
		}

		BytexAllOrderResp orderResp = bytexService.getOrderList(url, req);

		PageInfo<OrderData> pageInfo = new PageInfo<>(params.getPageNo(), params.getPageSize(), orderResp.getCount(),
				new ArrayList<>());

		if (orderResp.getCount() == 0) {
			return pageInfo;
		}

		List<OrderData> res = new ArrayList<>();
		List<BytexExOrder> orderList = new ArrayList<>();

		switch (size) {
		case 4:
			orderList = CollectionUtils.isEmpty(orderResp.getResultList()) ? new ArrayList<>()
					: orderResp.getResultList();
			break;

		case 2:
			orderList = CollectionUtils.isEmpty(orderResp.getResultList()) ? new ArrayList<>()
					: orderResp.getResultList();
			break;

		default:
			orderList = CollectionUtils.isEmpty(orderResp.getOrderList()) ? new ArrayList<>()
					: orderResp.getOrderList();
			break;
		}

		for (BytexExOrder bytexExOrder : orderList) {
			OrderData orderData = new OrderData();
			orderData.setAvgPrice(bytexExOrder.getAvgPrice());
			orderData.setBaseCoin(bytexExOrder.getBaseCoin());
			orderData.setCreateDate(bytexExOrder.getCreatedAt());
			orderData.setDealVolume(bytexExOrder.getDealVolume());
			orderData.setOrderId(String.valueOf(bytexExOrder.getId()));
			orderData.setPrice(bytexExOrder.getPrice());
			orderData.setQuoteCoin(bytexExOrder.getCountCoin());
			orderData.setSide(ExchangeSide.getEnumByType(bytexExOrder.getSide()));
			orderData.setStatus(OrderStatus.getEnumByType(bytexExOrder.getStatus().toString()));
			orderData.setSymbol(bytexExOrder.getBaseCoin() + bytexExOrder.getCountCoin());
			orderData.setType(EntryOrdersType.getEnumByType(bytexExOrder.getType().toString()));
			orderData.setVolume(bytexExOrder.getVolume());
			orderData.setRemainVolume(bytexExOrder.getRemainVolume());
			res.add(orderData);
		}

		pageInfo.setList(res);

		return pageInfo;
	}

	@Override
	public ExOrderDetailData getOrderByOrderId(ExOrder order) {
		try {

			String externalOrderId = order.getExternalOrderId();

			if (StringUtils.isEmpty(externalOrderId)) {
				return null;
			}

			String symbol = order.getCoinPairSymbol();
			BytexOrderDetailReq req = new BytexOrderDetailReq(apiKey, secretKey, order.getExternalOrderId(), symbol);
			BytexOrderDetailResp detail = bytexService.getOrderDetail(req);

			BytexOrderDetailData bytexOrder = detail.getOrderInfo();

			String orderId = order.getOrderId();
			ExOrder exOrder = new ExOrder();
			exOrder.setAvgPrice(bytexOrder.getAvgPrice());
			exOrder.setDealVolume(bytexOrder.getDealVolume());
			exOrder.setOrderId(orderId);
			exOrder.setExternalOrderId(externalOrderId);
			exOrder.setPrice(bytexOrder.getPrice());
			exOrder.setSide(ExchangeSide.getEnumByType(bytexOrder.getSide()).getCode());

			exOrder.setStatus(bytexOrder.getStatus());
			exOrder.setCoinPairSymbol(symbol);
			exOrder.setVolume(bytexOrder.getVolume());
			exOrder.setFee(bytexOrder.getFee());

			List<ExTrade> exTrades = new ArrayList<>();

			String baseCoin = order.getBaseCoin();
			String quoteCoin = order.getQuoteCoin();
			String coinPairSymbol = order.getCoinPairSymbol();
			String userId = order.getUserId();

			BigDecimal fee = BigDecimal.ZERO;
			List<BytexOrderTradeData> trades = detail.getTradeList();
			if (CollectionUtils.isNotEmpty(trades)) {
				for (BytexOrderTradeData trade : trades) {
					ExTrade exTrade = new ExTrade();
					exTrade.setBaseCoin(baseCoin);
					exTrade.setQuoteCoin(quoteCoin);
					exTrade.setCoinPairSymbol(coinPairSymbol);
					exTrade.setExternalTradeId(trade.getId().toString());
					if (trade.getType().equals("买入")
							|| ExchangeSide.BUY.equals(ExchangeSide.getEnumByType(trade.getType()))) {
						exTrade.setBuyFee(trade.getFee());
						exTrade.setBuyFeeCoin(trade.getFeeCoin());
						exTrade.setTrendSide(ExchangeSide.BUY.getCode());
						exTrade.setExternalBidOrderId(externalOrderId);
						exTrade.setBidOrderId(orderId);
						exTrade.setBidUserId(userId);
					} else if (trade.getType().equals("卖出")
							|| ExchangeSide.SELL.equals(ExchangeSide.getEnumByType(trade.getType()))) {
						exTrade.setSellFee(trade.getFee());
						exTrade.setSellFeeCoin(trade.getFeeCoin());
						exTrade.setTrendSide(ExchangeSide.SELL.getCode());
						exTrade.setExternalAskOrderId(externalOrderId);
						exTrade.setAskOrderId(orderId);
						exTrade.setAskUserId(userId);
					}

					exTrade.setPrice(trade.getPrice());
					exTrade.setVolume(trade.getVolume());
					exTrades.add(exTrade);

					fee = fee.add(trade.getFee());
				}
			}

			exOrder.setFee(fee);

			ExOrderDetailData data = new ExOrderDetailData();
			data.setOrder(exOrder);
			data.setExTrades(exTrades);
			return data;
		} catch (Exception e) {
			logger.error("查询bytex订单状态失败：", e);
			return null;
		}
	}

	@Transactional
	@Override
	public void dealExceptionOrder(ExOrder order) {

		BytexExOrder bytexExOrder = searchBytexExOrder(order);

		// 认为订单未成功进入bytex
		if (bytexExOrder == null) {
			ExOrder record = new ExOrder();
			record.setOrderId(order.getOrderId());
			record.setStatus(new Byte(OrderStatus.CANCELED.getCode()));
			record.setUpdateDate(new Date());
			exOrderMapper.updateByPrimaryKeySelective(record);

			commonService.unLockAccountBalance(order);
		} else {
			ExOrder record = new ExOrder();
			record.setOrderId(order.getOrderId());
			record.setExternalOrderId(bytexExOrder.getId().toString());
			record.setUpdateDate(new Date());
			record.setStatus(new Byte(OrderStatus.INIT.getCode()));
			exOrderMapper.updateByPrimaryKeySelective(record);
		}

	}

	private BytexExOrder searchBytexExOrder(ExOrder order) {

		ExOrder lastOrder = exOrderMapper.selectLatestOrderByTime(order.getOrderId(), order.getExternalExchangeType(),
				order.getCoinPairSymbol());

		Long externalOrderId = null;
		if (lastOrder != null) {
			externalOrderId = Long.valueOf(lastOrder.getExternalOrderId());
		}

		BytexExOrder bytexOrder = null;

		String symbol = order.getCoinPairSymbol();
		String exchangeType = order.getExternalExchangeType();

		int page = 1;
		int pageSize = 100;

		boolean hasOrder = false;

		while (true) {

			BytexAllOrderReq req = new BytexAllOrderReq(externalOrderId, symbol, page, pageSize, apiKey, secretKey);

			BytexAllOrderResp orderResp = bytexService.getOrderList("/open/api/all_order", req);

			if (orderResp.getCount() == 0) {
				break;
			}

			List<BytexExOrder> orderList = orderResp.getOrderList();

			Long createTime = order.getCreateDate().getTime();

			for (BytexExOrder _order : orderList) {
				if (_order.getBaseCoin().equalsIgnoreCase(order.getBaseCoin())
						&& _order.getCountCoin().equalsIgnoreCase(order.getQuoteCoin())
						&& _order.getVolume().compareTo(order.getVolume()) == 0
						&& (_order.getCreatedAt().getTime() - createTime) <= 1000) {

					ExOrder exOrder = exOrderMapper.selectExternalOrderId(_order.getId().toString(), exchangeType,
							symbol);
					if (exOrder == null) {
						bytexOrder = _order;
						hasOrder = true;
						break;
					}
				}
			}

			BytexExOrder _order = orderList.get(orderList.size() - 1);
			if (_order.getCreatedAt().getTime() - createTime > 5 * 1000) {
				break;
			}

			if (hasOrder) {
				break;
			}
		}

		return bytexOrder;
	}

	@Override
	public String createOrder(CreateOrderParams params) {
		BytexCreateOrderReq req = new BytexCreateOrderReq(params.getApiKey(), params.getSecretKey(), params);

		String orderId = bytexService.createOrder(req);

		return orderId;
	}

	@Override
	public Boolean cancelOrder(CancelOrderParams params) {
		BytexCancelOrderReq req = new BytexCancelOrderReq(params.getApiKey(), params.getSecretKey(), params);

		bytexService.cancelOrder(req);
		return true;
	}

	@Override
	public ExOrderDetailData getOrderByOrderId(OrderDetailParams params) {

		try {

			String externalOrderId = params.getOrderId();

			if (StringUtils.isEmpty(externalOrderId)) {
				return null;
			}

			String symbol = params.getSymbol();
			
			BytexOrderDetailReq req = new BytexOrderDetailReq(params.getApiKey(), params.getSecretKey(),
					params.getOrderId(), symbol);

			BytexOrderDetailResp detail = bytexService.getOrderDetail(req);

			BytexOrderDetailData bytexOrder = detail.getOrderInfo();

//			String orderId = order.getOrderId();
			ExOrder exOrder = new ExOrder();
			exOrder.setAvgPrice(bytexOrder.getAvgPrice());
			exOrder.setDealVolume(bytexOrder.getDealVolume());
			exOrder.setOrderId(externalOrderId);
			exOrder.setExternalOrderId(externalOrderId);
			exOrder.setPrice(bytexOrder.getPrice());
			exOrder.setSide(ExchangeSide.getEnumByType(bytexOrder.getSide()).getCode());

			exOrder.setStatus(bytexOrder.getStatus());
			exOrder.setCoinPairSymbol(symbol);
			exOrder.setVolume(bytexOrder.getVolume());
			exOrder.setFee(bytexOrder.getFee());

			List<ExTrade> exTrades = new ArrayList<>();

			String baseCoin = params.getBaseCoin();
			String quoteCoin = params.getQuoteCoin();
			String userId = params.getUserId();

			BigDecimal fee = BigDecimal.ZERO;
			List<BytexOrderTradeData> trades = detail.getTradeList();
			if (CollectionUtils.isNotEmpty(trades)) {
				for (BytexOrderTradeData trade : trades) {
					ExTrade exTrade = new ExTrade();
					exTrade.setBaseCoin(baseCoin);
					exTrade.setQuoteCoin(quoteCoin);
					exTrade.setCoinPairSymbol(symbol);
					exTrade.setExternalTradeId(trade.getId().toString());
					if (trade.getType().equals("买入")
							|| ExchangeSide.BUY.equals(ExchangeSide.getEnumByType(trade.getType()))) {
						exTrade.setBuyFee(trade.getFee());
						exTrade.setBuyFeeCoin(trade.getFeeCoin());
						exTrade.setTrendSide(ExchangeSide.BUY.getCode());
						exTrade.setExternalBidOrderId(externalOrderId);
						exTrade.setBidOrderId(externalOrderId);
						exTrade.setBidUserId(userId);
					} else if (trade.getType().equals("卖出")
							|| ExchangeSide.SELL.equals(ExchangeSide.getEnumByType(trade.getType()))) {
						exTrade.setSellFee(trade.getFee());
						exTrade.setSellFeeCoin(trade.getFeeCoin());
						exTrade.setTrendSide(ExchangeSide.SELL.getCode());
						exTrade.setExternalAskOrderId(externalOrderId);
						exTrade.setAskOrderId(externalOrderId);
						exTrade.setAskUserId(userId);
					}

					exTrade.setPrice(trade.getPrice());
					exTrade.setVolume(trade.getVolume());
					exTrades.add(exTrade);

					fee = fee.add(trade.getFee());
				}
			}

			exOrder.setFee(fee);

			ExOrderDetailData data = new ExOrderDetailData();
			data.setOrder(exOrder);
			data.setExTrades(exTrades);
			return data;
		} catch (Exception e) {
			logger.error("查询bytex订单状态失败：", e);
			return null;
		}
	}

}
