package com.zatgo.zup.exchange.strategy.huobi;

import java.util.List;
import java.util.Set;

import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.exchange.appapi.model.ExOrderType;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiAccountData;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiAccountsReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderData;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderMatchResultsReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderMatchResultsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderPlaceReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrdersReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiSubmitCancelReq;

public interface HuobiService {

	/**
	 * 查询火币账户列表
	 * 
	 * @param url
	 *            请求地址
	 * @param req
	 *            请求参数
	 * @return
	 */
	public List<HuobiAccountData> getHuobiAccountList(String url, HuobiAccountsReq req);

	/**
	 * 查询火币账户
	 * 
	 * @param url
	 *            请求地址
	 * @param req
	 *            请求参数
	 * @param accType
	 *            账户类型
	 * @return
	 */
	public HuobiAccountData getHuobiAccount(String url, HuobiAccountsReq req, String accType);

	/**
	 * 创建火币交易订单
	 * 
	 * @param url
	 *            请求地址
	 * @param accReq
	 *            账户请求参数
	 * @param accType
	 *            装了类型
	 * @param req
	 *            订单参数
	 * @return 订单id
	 */
	public String createHuobiOrder(String url, HuobiAccountsReq accReq, String accType, HuobiOrderPlaceReq req);

	/**
	 * 取消订单
	 * 
	 * @param url
	 *            请求地址
	 * @param req
	 *            请求参数
	 * @return 订单id
	 */
	public String cancelHuobiOrder(String url, HuobiSubmitCancelReq req);

	/**
	 * 查询订单信息
	 * 
	 * @param url
	 *            请求地址
	 * @param req
	 *            订单参数
	 * @return 订单信息
	 */
	public HuobiOrderData getOrderData(String url, HuobiOrderReq req);

	/**
	 * 查询订单成交明细
	 * 
	 * @param url
	 *            请求地址
	 * @param req
	 *            账户请求参数
	 * @return 订单成交明细
	 */
	public HuobiOrderMatchResultsResp getOrderMatchResults(String url, HuobiOrderMatchResultsReq req);

	/**
	 * 查询当前委托、历史委托
	 * 
	 * @param url
	 *            请求地址
	 * @param req
	 *            请求参数
	 * @return 订单列表
	 */
	public List<HuobiOrderData> getOrdersList(String url, HuobiOrdersReq req);
	
	/**
	 * 
	 * @param side
	 * @param type
	 * @return
	 */
	public String getHuobiOrderType(String side, String type);
	
	/**
	 * 
	 * @param orderStatus
	 * @return
	 */
	public Set<String> getHuobiOrderStates(OrderStatus[] orderStatus);
	
	/**
	 * 
	 * @param type
	 * @return
	 */
	public ExOrderType getOrderSideType(String type);
	
	/**
	 * 
	 * @param state
	 * @return
	 */
	public OrderStatus getOrderStatus(String state);

}
