package com.zatgo.zup.exchange.appapi.listener;

import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.websocket.Session;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.openservices.shade.com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
import com.zatgo.zup.exchange.appapi.controller.ApiWebSocketController;
import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
import com.zatgo.zup.exchange.appapi.util.WebSocketSessionUtil;

@Component
public class WebSocketPingPongListener {

	private static final Logger log = LoggerFactory.getLogger(WebSocketPingPongListener.class);

	private static ExecutorService executor = Executors.newFixedThreadPool(1);

	@Autowired
	private ApiWebSocketController apiWebSocketController;

	@PostConstruct
	public void init() {
		log.info("WebSocket Ping Pong 监听初始化");
		Runnable pingpongRunnable = new Runnable() {

			@Override
			public void run() {
				while (true) {
					CopyOnWriteArraySet<Session> sessions = WebSocketSessionUtil.getSessions();
					if (CollectionUtils.isNotEmpty(sessions)) {
						for (Session session : sessions) {

							RespWebSocketData<Long> data = new RespWebSocketData<>();
							data.setEvent(WebSocketEvent.PING);
							data.setData(System.currentTimeMillis());

							apiWebSocketController.sendMessage(JSON.toJSONString(data), session);

							WebSocketSessionUtil.addPingpongmapSession(session);
						}
					}

					try {
						TimeUnit.SECONDS.sleep(5);
					} catch (InterruptedException e) {
						log.error("", e);
					}

					WebSocketSessionUtil.checkPingpongmapSession();
				}
			}
		};

		executor.execute(pingpongRunnable);

		log.info("WebSocket Ping Pong 监听初始化完成");
	}

}
