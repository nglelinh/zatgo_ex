package com.zatgo.zup.exchange.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.zatgo.zup.common.enumtype.BusinessEnum.CoinPairConfigStatus;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeMode;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.mapper.CoinPairConfigMapper;

@Service
public class OrderStrategy implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext;
	
	@Autowired
	private CoinPairConfigMapper coinPairConfigMapper;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		OrderStrategy.applicationContext = applicationContext;
	}
	
	public OrderService getOrderService(ExchangeType type, ExchangeMode mode) {
		String exchangeType = "orderServiceImpl";
		
		if(type != null && mode != null && ExchangeMode.DIRECT.getCode().equals(mode.getCode())) {
			exchangeType += "_" + type.getCode() + "_" + mode.getCode();
		}
		
		return (OrderService)applicationContext.getBean(exchangeType);
	}
	
	public OrderService getOrderService(ExchangeType type) {
		String exchangeType = "orderServiceImpl";
		
		if(type != null) {
			exchangeType += "_" + type.getCode() + "_direct";
		}
		
		return (OrderService)applicationContext.getBean(exchangeType);
	}

}
