package com.zatgo.zup.exchange.strategy.bytex.model;

public class BytexCreateOrderResp {

	private Long orderId;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

}
