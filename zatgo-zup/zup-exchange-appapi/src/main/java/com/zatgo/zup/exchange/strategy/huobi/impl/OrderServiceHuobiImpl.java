package com.zatgo.zup.exchange.strategy.huobi.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.ExOrderDetailData;
import com.zatgo.zup.exchange.appapi.model.ExOrderType;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.entity.ExOrder;
import com.zatgo.zup.exchange.entity.ExTrade;
import com.zatgo.zup.exchange.mapper.ExOrderMapper;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.OrderStrategyService;
import com.zatgo.zup.exchange.strategy.huobi.HuobiService;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiAccountsReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderData;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderMatchResultData;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderMatchResultsReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderMatchResultsResp;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderPlaceReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrdersReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiSubmitCancelReq;

@Service("OrderService_huobi")
public class OrderServiceHuobiImpl implements OrderStrategyService {

	private static final Logger log = LoggerFactory.getLogger(OrderServiceHuobiImpl.class);
	
	private static final String ACCOUNTS_URL = "/v1/account/accounts";
	
	private static final String ORDER_PLACE_URL = "/v1/order/orders/place";

	/**
	 * 取消订单method
	 */
	private static final String SUBMIT_CANCEL_URL = "/v1/order/orders/%s/submitcancel";

	/**
	 * 用户订单信息method
	 */
	private static final String ORDER_URL = "/v1/order/orders/%s";

	/**
	 * 查询当前成交、历史成交method
	 */
	private static final String MATCH_RESULTS_URL = "/v1/order/orders/%s/matchresults";

	/**
	 * 查询当前委托、历史委托
	 */
	private static final String ORDERS_URL = "/v1/order/orders";

	@Value("${exchange.huobi.http.url:}")
	private String huobiUrl;

	@Value("${exchange.huobi.http.apiKey:}")
	private String apiKey;

	@Value("${exchange.huobi.http.secretKey:}")
	private String secretKey;

	@Autowired
	private HuobiService huobiService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private ExOrderMapper exOrderMapper;

	@Override
	public String createOrder(ExOrder order) {

		HuobiAccountsReq accReq = null;
		try {
			accReq = new HuobiAccountsReq(apiKey, secretKey, "GET", huobiUrl, ACCOUNTS_URL);
		} catch (Exception e) {
			log.error("获取火币账户参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.HUOBI_CREATE_ORDER_FAIL);
		}

		HuobiOrderPlaceReq orderReq = new HuobiOrderPlaceReq();
		orderReq.setAccessKeyId(apiKey);
		orderReq.setAmount(order.getVolume());
		orderReq.setMethod(ORDER_PLACE_URL);
		orderReq.setPrice(order.getPrice());
		orderReq.setReqMethod("POST");
		orderReq.setSecretKey(secretKey);
		orderReq.setSource("api");
		orderReq.setSymbol(order.getCoinPairSymbol());
		orderReq.setType(huobiService.getHuobiOrderType(order.getSide(), order.getType().toString()));

		return huobiService.createHuobiOrder(huobiUrl, accReq, "spot", orderReq);
	}

	@Override
	public Boolean cancelOrder(ExOrder order) {
		HuobiSubmitCancelReq req;
		try {
			String method = String.format(SUBMIT_CANCEL_URL, order.getExternalOrderId());
			req = new HuobiSubmitCancelReq(apiKey, secretKey, "POST", huobiUrl, method);
		} catch (Exception e) {
			log.error("获取火币取消订单参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		String orderId = huobiService.cancelHuobiOrder(huobiUrl, req);
		if (StringUtils.isEmpty(orderId)) {
			return false;
		}
		return true;
	}

	@Override
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params) {
		HuobiOrdersReq req = new HuobiOrdersReq();
		req.setAccessKeyId(params.getApiKey());
		req.setDirect(params.getDirect());
		req.setFrom(params.getOrderId());
		req.setMethod(ORDERS_URL);
		req.setReqMethod("GET");
		req.setSecretKey(params.getSecretKey());

		String pageSize = params.getPageSize() == null ? "10" : params.getPageSize().toString();
		req.setSize(pageSize);

		String symbol = params.getSymbol();
		req.setSymbol(symbol);
		req.setStates(String.join(",", huobiService.getHuobiOrderStates(params.getOrderStatuss())));

		List<HuobiOrderData> datas = huobiService.getOrdersList(huobiUrl, req);
		List<OrderData> list = null;
		if (CollectionUtils.isNotEmpty(datas)) {

			list = new ArrayList<OrderData>();

			for (HuobiOrderData huobiOrderData : datas) {
				OrderData orderData = getOrderData(huobiOrderData);
				list.add(orderData);
			}

		}

		PageInfo<OrderData> info = new PageInfo<OrderData>(params.getPageNo(), params.getPageSize(), 0L, list);
		return info;
	}
	
	private OrderData getOrderData(HuobiOrderData huobiOrderData) {
		OrderData orderData = new OrderData();
		orderData.setAvgPrice(huobiOrderData.getPrice());
		SymbolData symbolData = commonService.getSymbolInfo(huobiOrderData.getSymbol());
		String baseCoin = symbolData.getBaseCoin();
		String quoteCoin = symbolData.getQuoteCoin();
		orderData.setBaseCoin(baseCoin);
		orderData.setDealVolume(huobiOrderData.getFieldAmount());
		orderData.setFeeIsUserPlatformCoin(false);
		orderData.setOrderId(huobiOrderData.getId().toString());
		orderData.setPrice(huobiOrderData.getPrice());
		orderData.setQuoteCoin(quoteCoin);
		orderData.setRemainVolume(huobiOrderData.getAmount().subtract(huobiOrderData.getFieldAmount()));

		ExOrderType orderType = huobiService.getOrderSideType(huobiOrderData.getType());

		orderData.setSide(orderType.getSide());
		orderData.setStatus(huobiService.getOrderStatus(huobiOrderData.getState()));
		orderData.setSymbol(huobiOrderData.getSymbol());
		orderData.setType(orderType.getType());
		orderData.setVolume(huobiOrderData.getAmount());

		return orderData;
	}

	@Override
	public ExOrderDetailData getOrderByOrderId(ExOrder order) {

		String externalOrderId = order.getExternalOrderId();

		HuobiOrderReq orderReq;
		HuobiOrderMatchResultsReq resultsReq;
		try {
			orderReq = new HuobiOrderReq(huobiUrl, apiKey, secretKey, "GET", String.format(ORDER_URL, externalOrderId));
			resultsReq = new HuobiOrderMatchResultsReq(huobiUrl, apiKey, secretKey, "GET",
					String.format(MATCH_RESULTS_URL, externalOrderId));
		} catch (Exception e) {
			log.error("获取火币订单信息参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		HuobiOrderData orderData = huobiService.getOrderData(huobiUrl, orderReq);
		HuobiOrderMatchResultsResp resultsResp = huobiService.getOrderMatchResults(huobiUrl, resultsReq);
		List<HuobiOrderMatchResultData> resultDatas = resultsResp.getData();

		ExOrder exOrder = new ExOrder();
		exOrder.setAvgPrice(orderData.getPrice());

		String baseCoin = order.getBaseCoin();
		String quoteCoin = order.getQuoteCoin();
		exOrder.setBaseCoin(baseCoin);

		exOrder.setCoinPairSymbol(orderData.getSymbol());
		exOrder.setDealMoney(orderData.getFieldCashAmount());
		exOrder.setDealVolume(orderData.getFieldAmount());
		exOrder.setExternalExchangeType(order.getExternalExchangeType());
		exOrder.setExternalOrderId(orderData.getId().toString());
		exOrder.setFee(orderData.getFieldFees());

		String orderId = order.getOrderId();
		exOrder.setOrderId(orderId);
		exOrder.setPrice(order.getPrice());
		exOrder.setQuoteCoin(quoteCoin);

		String side = order.getSide();
		exOrder.setSide(side);
		exOrder.setStatus(getExOrderStatus(orderData.getState()));
		exOrder.setType(order.getType());

		String userId = order.getUserId();
		exOrder.setUserId(userId);
		exOrder.setVolume(order.getVolume());

		List<ExTrade> exTrades = resultDatas.stream().map(data -> {
			ExTrade trade = new ExTrade();

			if (side.equals(ExchangeSide.BUY.getCode())) {
				trade.setBidOrderId(orderId);
				trade.setBidUserId(userId);
				trade.setBuyFee(data.getFilledFees());

				trade.setBuyFeeCoin(baseCoin);
			} else if (side.equals(ExchangeSide.SELL.getCode())) {
				trade.setAskOrderId(orderId);
				trade.setAskUserId(userId);
				trade.setSellFee(data.getFilledFees());

				trade.setSellFeeCoin(quoteCoin);
			}

			trade.setBaseCoin(baseCoin);
			trade.setCoinPairSymbol(data.getSymbol());
			trade.setCreateTime(new Date());
			trade.setExternalTradeId(data.getId().toString());
			trade.setPrice(data.getPrice());
			trade.setQuoteCoin(quoteCoin);
			trade.setTrendSide(side);
			trade.setVolume(data.getFilledAmount());

			return trade;
		}).collect(Collectors.toList());

		ExOrderDetailData detailData = new ExOrderDetailData();
		detailData.setExTrades(exTrades);
		detailData.setOrder(exOrder);

		return detailData;
	}

	private Byte getExOrderStatus(String state) {

		switch (state) {

		case "submitting":
			return new Byte(OrderStatus.INIT.getCode());

		case "submitted":
			return new Byte(OrderStatus.NEW.getCode());

		case "partial-filled":
			return new Byte(OrderStatus.PART_FILLED.getCode());

		case "partial-canceled":
			return new Byte(OrderStatus.CANCELED.getCode());

		case "filled":
			return new Byte(OrderStatus.FILLED.getCode());

		case "canceled":
			return new Byte(OrderStatus.CANCELED.getCode());

		default:
			return null;
		}
	}

	@Override
	public void dealExceptionOrder(ExOrder order) {

		HuobiOrderData orderData = searchBytexExOrder(order);
		if (orderData == null) {
			ExOrder record = new ExOrder();
			record.setOrderId(order.getOrderId());
			record.setStatus(new Byte(OrderStatus.CANCELED.getCode()));
			record.setUpdateDate(new Date());
			exOrderMapper.updateByPrimaryKeySelective(record);

			commonService.unLockAccountBalance(order);
		} else {
			ExOrder record = new ExOrder();
			record.setOrderId(order.getOrderId());
			record.setExternalOrderId(orderData.getId().toString());
			record.setUpdateDate(new Date());
			record.setStatus(new Byte(OrderStatus.INIT.getCode()));
			exOrderMapper.updateByPrimaryKeySelective(record);
		}
	}

	private HuobiOrderData searchBytexExOrder(ExOrder order) {
		HuobiOrdersReq req = new HuobiOrdersReq();

		ExOrder lastOrder = exOrderMapper.selectLatestOrderByTime(order.getOrderId(), order.getExternalExchangeType(),
				order.getCoinPairSymbol());

		String fromId = null;
		if (lastOrder != null) {
			fromId = lastOrder.getExternalOrderId();
		}

		req.setAccessKeyId(apiKey);
		req.setDirect("next");
		req.setMethod(ORDERS_URL);
		req.setReqMethod("GET");
		req.setSecretKey(secretKey);
		req.setSize("100");

		String symbol = order.getCoinPairSymbol();
		req.setSymbol(symbol);
		req.setTypes(huobiService.getHuobiOrderType(order.getSide(), order.getType().toString()));

		Boolean falg = false;
		Long createTime = order.getCreateDate().getTime();
		String exchangeType = order.getExternalExchangeType();
		BigDecimal volume = order.getVolume();
		HuobiOrderData orderData = null;
		while (true) {

			req.setFrom(fromId);
			try {
				req.sign(req, huobiUrl);
			} catch (Exception e) {
				log.error("处理火币异常订单失败，获取请求参数错误：", e);
				break;
			}

			List<HuobiOrderData> datas = huobiService.getOrdersList(huobiUrl, req);
			if (CollectionUtils.isEmpty(datas)) {
				falg = true;
				datas = new ArrayList<HuobiOrderData>();
			}
			for (HuobiOrderData data : datas) {
				if (data.getSymbol().equalsIgnoreCase(symbol) && data.getAmount().compareTo(volume) == 0
						&& (data.getCreatedAt() - createTime) <= 1000) {

					ExOrder exOrder = exOrderMapper.selectExternalOrderId(data.getId().toString(), exchangeType,
							symbol);
					if (exOrder == null) {
						orderData = data;
						falg = true;
						break;
					} else if (data.getCreatedAt() - createTime > 5 * 1000) {
						falg = true;
						break;
					}

				}
			}

			if (falg) {
				break;
			} else {
				fromId = datas.get(datas.size() - 1).getId().toString();
			}
		}

		return orderData;
	}

	@Override
	public String createOrder(CreateOrderParams params) {
		HuobiAccountsReq accReq = null;
		try {
			accReq = new HuobiAccountsReq(params.getApiKey(), params.getSecretKey(), "GET", huobiUrl,
					"/v1/account/accounts");
		} catch (Exception e) {
			log.error("获取火币账户参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.HUOBI_CREATE_ORDER_FAIL);
		}

		HuobiOrderPlaceReq orderReq = new HuobiOrderPlaceReq();
		orderReq.setAccessKeyId(params.getApiKey());
		orderReq.setAmount(params.getVolume());
		orderReq.setMethod("/v1/order/orders/place");
		orderReq.setPrice(params.getPrice());
		orderReq.setReqMethod("POST");
		orderReq.setSecretKey(params.getSecretKey());
		orderReq.setSource("api");
		orderReq.setSymbol(params.getSymbol());
		orderReq.setType(huobiService.getHuobiOrderType(params.getSide().toString(), params.getType().toString()));

		String orderId = huobiService.createHuobiOrder(huobiUrl, accReq, "spot", orderReq);
		return orderId;
	}

	@Override
	public Boolean cancelOrder(CancelOrderParams params) {
		HuobiSubmitCancelReq req;
		try {
			String method = String.format(SUBMIT_CANCEL_URL, params.getOrderId());
			req = new HuobiSubmitCancelReq(params.getApiKey(), params.getSecretKey(), "POST", huobiUrl, method);
		} catch (Exception e) {
			log.error("获取火币取消订单参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		String orderId = huobiService.cancelHuobiOrder(huobiUrl, req);
		return true;
	}

	@Override
	public ExOrderDetailData getOrderByOrderId(OrderDetailParams params) {
		String externalOrderId = params.getOrderId();

		HuobiOrderReq orderReq;
		HuobiOrderMatchResultsReq resultsReq;
		try {
			orderReq = new HuobiOrderReq(huobiUrl, apiKey, secretKey, "GET", String.format(ORDER_URL, externalOrderId));
			resultsReq = new HuobiOrderMatchResultsReq(huobiUrl, apiKey, secretKey, "GET",
					String.format(MATCH_RESULTS_URL, externalOrderId));
		} catch (Exception e) {
			log.error("获取火币订单信息参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		HuobiOrderData orderData = huobiService.getOrderData(huobiUrl, orderReq);
		HuobiOrderMatchResultsResp resultsResp = huobiService.getOrderMatchResults(huobiUrl, resultsReq);
		List<HuobiOrderMatchResultData> resultDatas = resultsResp.getData();

		ExOrder exOrder = new ExOrder();
		exOrder.setAvgPrice(orderData.getPrice());

		String baseCoin = params.getBaseCoin();
		String quoteCoin = params.getQuoteCoin();
		exOrder.setBaseCoin(baseCoin);

		exOrder.setCoinPairSymbol(orderData.getSymbol());
		exOrder.setDealMoney(orderData.getFieldCashAmount());
		exOrder.setDealVolume(orderData.getFieldAmount());
		//exOrder.setExternalExchangeType(order.getExternalExchangeType());
		exOrder.setExternalOrderId(orderData.getId().toString());
		exOrder.setFee(orderData.getFieldFees());

		String orderId = params.getOrderId();
		exOrder.setOrderId(orderId);
		exOrder.setPrice(orderData.getPrice());
		exOrder.setQuoteCoin(quoteCoin);

		String side = null;//order.getSide();
		exOrder.setSide(side);
		exOrder.setStatus(getExOrderStatus(orderData.getState()));
		//exOrder.setType(order.getType());

		String userId = params.getUserId();
		exOrder.setUserId(userId);
		//exOrder.setVolume(params.getVolume());

		List<ExTrade> exTrades = resultDatas.stream().map(data -> {
			ExTrade trade = new ExTrade();

			if (side.equals(ExchangeSide.BUY.getCode())) {
				trade.setBidOrderId(orderId);
				trade.setBidUserId(userId);
				trade.setBuyFee(data.getFilledFees());

				trade.setBuyFeeCoin(baseCoin);
			} else if (side.equals(ExchangeSide.SELL.getCode())) {
				trade.setAskOrderId(orderId);
				trade.setAskUserId(userId);
				trade.setSellFee(data.getFilledFees());

				trade.setSellFeeCoin(quoteCoin);
			}

			trade.setBaseCoin(baseCoin);
			trade.setCoinPairSymbol(data.getSymbol());
			trade.setCreateTime(new Date());
			trade.setExternalTradeId(data.getId().toString());
			trade.setPrice(data.getPrice());
			trade.setQuoteCoin(quoteCoin);
			trade.setTrendSide(side);
			trade.setVolume(data.getFilledAmount());

			return trade;
		}).collect(Collectors.toList());

		ExOrderDetailData detailData = new ExOrderDetailData();
		detailData.setExTrades(exTrades);
		detailData.setOrder(exOrder);

		return detailData;
	}

}
