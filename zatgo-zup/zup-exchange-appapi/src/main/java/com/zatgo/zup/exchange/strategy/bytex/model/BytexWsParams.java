package com.zatgo.zup.exchange.strategy.bytex.model;

import java.math.BigDecimal;

import com.alibaba.fastjson.annotation.JSONField;

public class BytexWsParams {
	
	private String channel;
	
	@JSONField(name = "cb_id")
	private String cbid;
	
	private BigDecimal asks;
	
	private BigDecimal bids;
	
	private Long since;
	
	private Long top;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCbid() {
		return cbid;
	}

	public void setCbid(String cbid) {
		this.cbid = cbid;
	}

	public BigDecimal getAsks() {
		return asks;
	}

	public void setAsks(BigDecimal asks) {
		this.asks = asks;
	}

	public BigDecimal getBids() {
		return bids;
	}

	public void setBids(BigDecimal bids) {
		this.bids = bids;
	}

	public Long getSince() {
		return since;
	}

	public void setSince(Long since) {
		this.since = since;
	}

	public Long getTop() {
		return top;
	}

	public void setTop(Long top) {
		this.top = top;
	}

}
