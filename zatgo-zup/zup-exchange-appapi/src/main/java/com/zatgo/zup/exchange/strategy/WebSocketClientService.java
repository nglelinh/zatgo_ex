package com.zatgo.zup.exchange.strategy;

public interface WebSocketClientService {

	enum ZUP_READYSTATE {
		NOT_YET_CONNECTED, CONNECTING, OPEN, CLOSING, CLOSED
	}
	
	public Boolean isOpen();
	
	public void connect();
	
	public WebSocketClientService.ZUP_READYSTATE getReadyState();
	
	public void send(String message);
	
}
