package com.zatgo.zup.exchange.strategy;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.ExOrderDetailData;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.entity.ExOrder;

public interface OrderStrategyService {

	/**
	 * 创建订单
	 * @param params
	 * @return
	 */
	public String createOrder(ExOrder order);
	
	/**
	 * 创建订单
	 * @param params
	 * @return
	 */
	public String createOrder(CreateOrderParams params);

	/**
	 * 取消订单
	 * @param params
	 * @return
	 */
	public Boolean cancelOrder(ExOrder order);
	
	/**
	 * 取消订单
	 * @param params
	 * @return
	 */
	public Boolean cancelOrder(CancelOrderParams params);

	/**
	 * 订单查询
	 * @param params
	 * @return
	 */
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params);

	/**
	 * 获取订单明细
	 * @param order
	 * @return
	 */
	public ExOrderDetailData getOrderByOrderId(OrderDetailParams params);
	
	/**
	 * 获取订单明细
	 * @param order
	 * @return
	 */
	public ExOrderDetailData getOrderByOrderId(ExOrder order);
	
	/**
	 * 处理异常订单
	 * @param order
	 */
	public void dealExceptionOrder(ExOrder order);
}
