package com.zatgo.zup.exchange.strategy.huobi.model;

import java.math.BigDecimal;

import com.alibaba.fastjson.annotation.JSONField;

public class HuobiOrderPlaceReq extends HuobiReq {

	@JSONField(name = "account-id")
	private Long accountId;

	private BigDecimal amount;

	private BigDecimal price;

	private String source;

	private String symbol;

	private String type;

	public HuobiOrderPlaceReq() {
		super();
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
