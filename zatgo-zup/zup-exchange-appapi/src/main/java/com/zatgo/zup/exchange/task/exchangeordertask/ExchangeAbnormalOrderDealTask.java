package com.zatgo.zup.exchange.task.exchangeordertask;

import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.business.OrderService;
import com.zatgo.zup.exchange.entity.ExOrder;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.OrderStrategyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

//@Component
public class ExchangeAbnormalOrderDealTask {

	private static final Logger log = LoggerFactory.getLogger(ExchangeAbnormalOrderDealTask.class);

	@Autowired
	private OrderService orderService;

	@Autowired
	private ExchangeStrategy exchangeStrategy;

	public void exchangeAbnormalOrderDeal() {

		OrderSearchParams params = new OrderSearchParams();
		params.setStatus(new Byte(OrderStatus.EXCEPTION.getCode()));

		Boolean pageEnd = false;
		int pageNo = 1;
		int pageSize = 1000;

		params.setPageSize(pageSize);
		while (!pageEnd) {

			params.setPageNo(pageNo);

			PageInfo<ExOrder> info = orderService.findOrderBySearchParams(params);
			List<ExOrder> orders = info.getList();
			for (ExOrder order : orders) {

				OrderStrategyService orderStrategyService = exchangeStrategy
						.getOrderService(order.getExternalExchangeType());

				orderStrategyService.dealExceptionOrder(order);
			}

		}
	}

}
