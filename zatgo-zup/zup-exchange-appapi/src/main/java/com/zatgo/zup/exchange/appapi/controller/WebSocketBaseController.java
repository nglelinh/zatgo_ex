package com.zatgo.zup.exchange.appapi.controller;

import java.util.List;
import java.util.Map;

import javax.websocket.Session;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.core.RedisTemplate;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.exchange.appapi.listener.SubscriptionClientListener;
import com.zatgo.zup.exchange.appapi.util.CloudUserInfoMap;
import com.zatgo.zup.exchange.appapi.util.WebSocketSingeUtil;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;

/**
 * Created by 46041 on 2018/9/19.
 */
public class WebSocketBaseController implements ApplicationContextAware {

    
    protected static RedisTemplate redisTemplate;
    
    protected static WebSocketSingeUtil webSocketSingeUtil;
    
    protected static CommonService commonService;
    
    protected static ExchangeStrategy exchangeStrategy;
    
    protected static SubscriptionClientListener subscriptionClientListener;

    protected static CloudUserInfoMap cloudUserInfoMap;

    
    private static ApplicationContext applicationContext;
    
    @Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    	WebSocketBaseController.applicationContext = applicationContext;
    	WebSocketBaseController.redisTemplate = (RedisTemplate) applicationContext.getBean("redisTemplate");
    	WebSocketBaseController.webSocketSingeUtil = applicationContext.getBean(WebSocketSingeUtil.class);
    	WebSocketBaseController.subscriptionClientListener = applicationContext.getBean(SubscriptionClientListener.class);
    	WebSocketBaseController.commonService = applicationContext.getBean(CommonService.class);
    	WebSocketBaseController.exchangeStrategy = applicationContext.getBean(ExchangeStrategy.class);
    	WebSocketBaseController.cloudUserInfoMap = applicationContext.getBean(CloudUserInfoMap.class);
	}
    
    /**
     * 根据token获取用户
     * @param token
     * @return
     */
    protected AuthUserInfo getUserInfo(String token){
        AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
        if(authUserInfo == null) {
            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
        }
        return authUserInfo;
    }

    /**
     * 获取参数
     * @param parameName
     * @return
     */
    protected String getParameter(String parameName, Session session){
        Map<String, List<String>> requestParameterMap = session.getRequestParameterMap();
        List<String> list = requestParameterMap.get(parameName);
        return CollectionUtils.isEmpty(list) ? null : list.get(0);
    }
}
