package com.zatgo.zup.exchange.strategy.bytex.util;

import com.zatgo.zup.common.enumtype.ExchangeEnum.KlineTime;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;

public class BytexTopicUtil {

	/**
	 * K线行情 [1min/5min/15min/30min/60min/1day/1week/1month]
	 */
	private static final String TOPIC_KLINE = "market_%s_kline_%s";

	/**
	 * 前24小时行情
	 */
	private static final String TOPIC_TICKER = "market_%s_ticker";

	/**
	 * 实时成交信息
	 */
	private static final String TOPIC_TRADE_TICKER = "market_%s_trade_ticker";

	/**
	 * 深度盘口 [0-2]
	 */
	private static final String TOPIC_DEPTH_STEP = "market_%s_depth_%s";

	/**
	 * 1min
	 */
	private static final String PERIOD_1_MIN = "1min";

	/**
	 * 5min
	 */
	private static final String PERIOD_5_MIN = "5min";

	/**
	 * 15min
	 */
	private static final String PERIOD_15_MIN = "15min";

	/**
	 * 30min
	 */
	private static final String PERIOD_30_MIN = "30min";

	/**
	 * 60min
	 */
	private static final String PERIOD_60_MIN = "60min";

	/**
	 * 1day
	 */
	private static final String PERIOD_1_DAY = "1day";

	/**
	 * 1week
	 */
	private static final String PERIOD_1_WEEK = "1week";

	/**
	 * 1mon
	 */
	private static final String PERIOD_1_MON = "1month";

	/**
	 * 获取bytex topic
	 * 
	 * @param socketBusiType
	 *            websocet订阅接口业务类型
	 * @param symbol
	 *            币对
	 * @param period
	 *            周期
	 * @param type
	 *            深度
	 * @return
	 */
	public static String getTopic(WebSocketBusiType socketBusiType, String symbol, String period, String type) {
		if (socketBusiType.equals(WebSocketBusiType.KLINE)) {
			return getTopicKline(symbol, period);
		} else if (socketBusiType.equals(WebSocketBusiType.TRADE_TICKER)) {
			return getTopicTradeTicker(symbol);
		} else if (socketBusiType.equals(WebSocketBusiType.DEPTH_STEP)) {
			return getTopicDepthStep(symbol, type);
		} else if (socketBusiType.equals(WebSocketBusiType.TICKER)) {
			return getTopicTicker(symbol);
		} else {
			return null;
		}
	}

	/**
	 * 获取火币kline topic
	 * 
	 * @param symbol
	 *            币对
	 * @param period
	 *            kline周期
	 * @return
	 */
	public static String getTopicKline(String symbol, String period) {
		if (period.equals(KlineTime.MIN.getCode())) {
			period = PERIOD_1_MIN;
		} else if (period.equals(KlineTime.MIN5.getCode())) {
			period = PERIOD_5_MIN;
		} else if (period.equals(KlineTime.MIN15.getCode())) {
			period = PERIOD_15_MIN;
		} else if (period.equals(KlineTime.MIN30.getCode())) {
			period = PERIOD_30_MIN;
		} else if (period.equals(KlineTime.MIN60.getCode())) {
			period = PERIOD_60_MIN;
		} else if (period.equals(KlineTime.DAY.getCode())) {
			period = PERIOD_1_DAY;
		} else if (period.equals(KlineTime.WEEK.getCode())) {
			period = PERIOD_1_WEEK;
		} else if (period.equals(KlineTime.MONTH.getCode())) {
			period = PERIOD_1_MON;
		}
		return String.format(TOPIC_KLINE, symbol, period);
	}

	/**
	 * 24小时行情
	 * 
	 * @param symbol
	 *            币对
	 * @return
	 */
	public static String getTopicTicker(String symbol) {
		return String.format(TOPIC_TICKER, symbol);
	}

	/**
	 * 实时成交信息
	 * 
	 * @param symbol
	 *            币对
	 * @return
	 */
	public static String getTopicTradeTicker(String symbol) {
		return String.format(TOPIC_TRADE_TICKER, symbol);
	}

	/**
	 * 深度盘口
	 * 
	 * @param symbol
	 *            币对
	 * @param type
	 *            深度
	 * @return
	 */
	public static String getTopicDepthStep(String symbol, String type) {
		return String.format(TOPIC_DEPTH_STEP, symbol, type);
	}

	public static String getTopicKline() {
		return TOPIC_KLINE;
	}

	public static String getTopicTicker() {
		return TOPIC_TICKER;
	}

	public static String getTopicTradeTicker() {
		return TOPIC_TRADE_TICKER;
	}

	public static String getTopicDepthStep() {
		return TOPIC_DEPTH_STEP;
	}
	
	public static String getKlineTime(String period) {
		if(period.equals(PERIOD_1_MIN)) {
			return KlineTime.MIN.getCode();
		} else if(period.equals(PERIOD_5_MIN)) {
			return KlineTime.MIN5.getCode();
		} else if(period.equals(PERIOD_15_MIN)) {
			return KlineTime.MIN15.getCode();
		} else if(period.equals(PERIOD_30_MIN)) {
			return KlineTime.MIN30.getCode();
		} else if(period.equals(PERIOD_60_MIN)) {
			return KlineTime.MIN60.getCode();
		} else if(period.equals(PERIOD_1_DAY)) {
			return KlineTime.DAY.getCode();
		} else if(period.equals(PERIOD_1_WEEK)) {
			return KlineTime.WEEK.getCode();
		} else if(period.equals(PERIOD_1_MON)) {
			return KlineTime.MONTH.getCode();
		}
		
		return null;
	}

}
