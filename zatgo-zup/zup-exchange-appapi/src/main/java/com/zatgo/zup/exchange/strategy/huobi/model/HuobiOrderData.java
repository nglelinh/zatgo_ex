package com.zatgo.zup.exchange.strategy.huobi.model;

import java.math.BigDecimal;

import com.alibaba.fastjson.annotation.JSONField;

public class HuobiOrderData {

	/**
	 * 账户 ID
	 */
	@JSONField(name = "account-id")
	private Long accountId;

	/**
	 * 订单数量
	 */
	private BigDecimal amount;

	/**
	 * 接到撤单申请的时间
	 */
	@JSONField(name = "canceled-at")
	private Long canceledAt;

	/**
	 * 订单创建时间
	 */
	@JSONField(name = "created-at")
	private Long createdAt;

	/**
	 * 已成交数量
	 */
	@JSONField(name = "field-amount")
	private BigDecimal fieldAmount;

	/**
	 * 已成交总金额
	 */
	@JSONField(name = "field-cash-amount")
	private BigDecimal fieldCashAmount;

	/**
	 * 已成交手续费（买入为币，卖出为钱）
	 */
	@JSONField(name = "field-fees")
	private BigDecimal fieldFees;

	/**
	 * 最后成交时间
	 */
	@JSONField(name = "finished-at")
	private Long finishedAt;

	/**
	 * 订单ID
	 */
	private Long id;

	/**
	 * 订单价格
	 */
	private BigDecimal price;

	/**
	 * 订单来源
	 */
	private String source;

	/**
	 * 订单状态submitting , submitted 已提交, partial-filled 部分成交, partial-canceled 部分成交撤销,
	 * filled 完全成交, canceled 已撤销
	 */
	private String state;

	/**
	 * 交易对
	 */
	private String symbol;

	/**
	 * 订单类型submit-cancel：已提交撤单申请 ,buy-market：市价买, sell-market：市价卖, buy-limit：限价买,
	 * sell-limit：限价卖, buy-ioc：IOC买单, sell-ioc：IOC卖单
	 */
	private String type;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getCanceledAt() {
		return canceledAt;
	}

	public void setCanceledAt(Long canceledAt) {
		this.canceledAt = canceledAt;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getFieldAmount() {
		return fieldAmount;
	}

	public void setFieldAmount(BigDecimal fieldAmount) {
		this.fieldAmount = fieldAmount;
	}

	public BigDecimal getFieldCashAmount() {
		return fieldCashAmount;
	}

	public void setFieldCashAmount(BigDecimal fieldCashAmount) {
		this.fieldCashAmount = fieldCashAmount;
	}

	public BigDecimal getFieldFees() {
		return fieldFees;
	}

	public void setFieldFees(BigDecimal fieldFees) {
		this.fieldFees = fieldFees;
	}

	public Long getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(Long finishedAt) {
		this.finishedAt = finishedAt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
