package com.zatgo.zup.exchange.strategy.bytex.model;

import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;

public class BytexOrderDetailResp {
	
	@JSONField(name = "order_info")
	private BytexOrderDetailData orderInfo;
	
	@JSONField(name = "trade_list")
	private List<BytexOrderTradeData> tradeList;

	public BytexOrderDetailData getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(BytexOrderDetailData orderInfo) {
		this.orderInfo = orderInfo;
	}

	public List<BytexOrderTradeData> getTradeList() {
		return tradeList;
	}

	public void setTradeList(List<BytexOrderTradeData> tradeList) {
		this.tradeList = tradeList;
	}

}
