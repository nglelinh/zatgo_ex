package com.zatgo.zup.exchange.strategy.bytex.model;

public class BytexAllOrderReq extends BytexReq {
	
	private Long id;
	
	private String symbol;
	
	private Integer pageSize;
	
	private Integer page;
	
	public BytexAllOrderReq() {
		super();
	}
	
	public BytexAllOrderReq(Long id, String symbol, Integer page, Integer pageSize, String apiKey, String secretKey) {
		super();
		this.id = id;
		this.symbol = symbol.toLowerCase();
		this.page = page;
		this.pageSize = pageSize;
		sign(apiKey, secretKey, this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol.toLowerCase();
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

}
