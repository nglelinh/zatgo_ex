package com.zatgo.zup.exchange.strategy.huobi.model;

public class HuobiOrderReq extends HuobiReq {
	
	public HuobiOrderReq() {
		super();
	}
	
	public HuobiOrderReq(String url, String accessKeyId, String secretKey, String reqMethod, String method)
			throws Exception {
		super(accessKeyId, secretKey, reqMethod, method);
		sign(this, url);
	}

}
