package com.zatgo.zup.exchange.strategy.huobi.model;

import java.util.List;

public class HuobiAccountsResp extends HuobiResp {
	
	private List<HuobiAccountData> data;

	public List<HuobiAccountData> getData() {
		return data;
	}

	public void setData(List<HuobiAccountData> data) {
		this.data = data;
	}

}
