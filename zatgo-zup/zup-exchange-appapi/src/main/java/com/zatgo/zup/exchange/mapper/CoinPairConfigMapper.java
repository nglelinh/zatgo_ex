package com.zatgo.zup.exchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zatgo.zup.exchange.appapi.model.CoinPair;
import com.zatgo.zup.exchange.entity.CoinPairConfig;

public interface CoinPairConfigMapper {
    int deleteByPrimaryKey(String id);

    int insert(CoinPairConfig record);

    int insertSelective(CoinPairConfig record);

    CoinPairConfig selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CoinPairConfig record);

    int updateByPrimaryKey(CoinPairConfig record);
    
    List<CoinPairConfig> selectListByCoinPairConfig(@Param("exchangeType") String exchangeType,@Param("status") Integer status);

    @Select("SELECT symbol as symbol, base_coin as baseCoin, quote_coin as quoteCoin, coin_image, sort FROM coin_pair_config WHERE status = #{status}")
    List<CoinPair> selectCoinPairByStatus(@Param("status") Byte status);

    @Select("SELECT symbol as symbol, id FROM coin_pair_config WHERE status = 1")
    List<CoinPairConfig> selectAll();
    
    @Select("SELECT external_exchange_types FROM coin_pair_config WHERE `status` = 1 GROUP BY external_exchange_types")
    List<String> selectExchangeType();
    
    @Select("SELECT * FROM coin_pair_config WHERE external_exchange_types like CONCAT(CONCAT('%', #{exchangeType}), '%') AND `status` = 1")
    List<CoinPairConfig> selectCoinPairByExchangeType(String exchangeType);
}