package com.zatgo.zup.exchange.strategy.bytex.model;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("深度盘口")
public class BytexWsDepthData {
	
	@ApiModelProperty(value = "卖盘", required = true)
	private List<List<BigDecimal>> asks;
	
	@ApiModelProperty(value = "买盘", required = true)
	private List<List<BigDecimal>> bids;

	public List<List<BigDecimal>> getAsks() {
		return asks;
	}

	public void setAsks(List<List<BigDecimal>> asks) {
		this.asks = asks;
	}

	public List<List<BigDecimal>> getBids() {
		return bids;
	}

	public void setBids(List<List<BigDecimal>> bids) {
		this.bids = bids;
	}

}
