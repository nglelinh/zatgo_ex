package com.zatgo.zup.exchange.mapper;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.entity.ExOrder;

public interface ExOrderMapper {
	int deleteByPrimaryKey(String orderId);

	int insert(ExOrder record);

	int insertSelective(ExOrder record);

	ExOrder selectByPrimaryKey(String orderId);

	int updateByPrimaryKeySelective(ExOrder record);

	int updateByPrimaryKey(ExOrder record);

	Page<ExOrder> selectOrderByOrderSearchParams(OrderSearchParams params);

	ExOrder selectLatestOrderByTime(@Param("orderId") String orderId, @Param("exchangeType") String exchangeType,
			@Param("symbol") String symbol);

	ExOrder selectOrderByOrderIdAndLock(@Param("orderId") String orderId);

	ExOrder selectExternalOrderId(@Param("externalOrderId") String externalOrderId,
			@Param("exchangeType") String exchangeType, @Param("symbol") String symbol);
}