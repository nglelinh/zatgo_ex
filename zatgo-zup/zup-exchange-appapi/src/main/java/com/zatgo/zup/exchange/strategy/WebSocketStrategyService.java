package com.zatgo.zup.exchange.strategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum.DepthStep;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.KlineTime;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.exchange.appapi.controller.ApiWebSocketController;
import com.zatgo.zup.exchange.appapi.listener.SubscriptionClientListener;
import com.zatgo.zup.exchange.appapi.model.CoinPair;
import com.zatgo.zup.exchange.appapi.model.CoinPairData;
import com.zatgo.zup.exchange.appapi.model.DepthStepData;
import com.zatgo.zup.exchange.appapi.model.KlineData;
import com.zatgo.zup.exchange.appapi.model.ReqWebsocketParams;
import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.appapi.model.TickerData;
import com.zatgo.zup.exchange.appapi.model.TradeData;
import com.zatgo.zup.exchange.appapi.util.WebSocketSessionUtil;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.service.CommonService;

public abstract class WebSocketStrategyService {

	@Autowired
	protected ApiWebSocketController apiWebSocketController;
	
	@Autowired
	protected CommonService commonService;
	
	@Autowired
	protected SubscriptionClientListener subscriptionClientListener;
	
	@Autowired
	protected RedisTemplate redisTemplate;
	
	/**
	 * 推送数据到客户端
	 * @param resp
	 * @param type
	 */
	protected void sendDataToClient(RespWebSocketData<?> resp,ExchangeType type) {
		//本地模式发送
		resp.setExchangeType(type);
		resp.setIsDirect(false);
		apiWebSocketController.pushMessageToClient(resp);
		
		//直连模式发送
		resp.setExchangeType(type);
		resp.setIsDirect(true);
		apiWebSocketController.pushMessageToClient(resp);
	}

	
	/**
	 * 推送市场行情数据到客户端
	 * @param event
	 * @param tickerMap
	 * @param type
	 */
	protected void sendMarketTickersToClient(WebSocketEvent event, Map<String, List<TickerData>> tickerMap,ExchangeType type) {
		if (MapUtils.isEmpty(tickerMap)) {
			return;
		}

		//根据计价币推送
		tickerMap.forEach((key, value) -> {
			RespWebSocketData<List<TickerData>> data = new RespWebSocketData<>();
			data.setQuoteCoin(key);
			data.setData(value);
			data.setMD5(MD5.GetMD5Code(JSON.toJSONString(value)));
			data.setType(WebSocketBusiType.MARKET);
			data.setEvent(event);
			
			//直连发送
			data.setExchangeType(type);
			data.setIsDirect(true);
			apiWebSocketController.pushMessageToClient(data);
			
			//更新本地模式缓存
			for(TickerData ticker:value) {
				updateMarketTickerCache(ticker);
			}
			
		});

		//全量推送
		RespWebSocketData<Map<String, List<TickerData>>> data = new RespWebSocketData<>();
		data.setData(tickerMap);
		data.setMD5(MD5.GetMD5Code(JSON.toJSONString(tickerMap)));
		data.setType(WebSocketBusiType.MARKET);
		data.setEvent(event);
		
		//直连发送
		data.setExchangeType(type);
		data.setIsDirect(true);
		apiWebSocketController.pushMessageToClient(data);
	}
	
	public List<String> getExchangeTypes() {
		return commonService.getExchangeType();
	}

	
	/**
	 * 推送-直连模式-Kline
	 */
	public void pushDirectKlineData() {

		Set<ExchangeType> exchangeTypeSet = WebSocketSessionUtil.getDirectExchangeSet();
		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
			return;
		}

		if(exchangeTypeSet.contains(getExchangeType())) {
			pushKline(true);
		}

	}
	
	/**
	 * 订阅-直连模式-前24小时行情
	 */
	public void pushDirectTickerData() {

		Set<ExchangeType> exchangeTypeSet = WebSocketSessionUtil.getDirectExchangeSet();
		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
			return;
		}

		if(exchangeTypeSet.contains(getExchangeType())) {
			pushTicker(true);
		}
	}
	
	
	/**
	 * 订阅-直连模式-成交信息
	 */
	public void pushDirectTradeTickerData() {

		Set<ExchangeType> exchangeTypeSet = WebSocketSessionUtil.getDirectExchangeSet();
		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
			return;
		}

		if(exchangeTypeSet.contains(getExchangeType())) {
			pushTradeTicker(true);
		}

	}
	
	
	/**
	 * 订阅-直连模式-深度
	 */
	public void pushDirectDepthStepData() {

		Set<ExchangeType> exchangeTypeSet = WebSocketSessionUtil.getDirectExchangeSet();
		if (CollectionUtils.isEmpty(exchangeTypeSet)) {
			return;
		}

		if(exchangeTypeSet.contains(getExchangeType())) {
			pushDepthStepData(true);
		}

	}


	public void pushKline(boolean isDirect) {
		
		List<CoinPairConfig> configs = null;
		if(!isDirect) {
			configs = commonService.getLocalCoinPairByExchangeType(getExchangeType().getCode());
		} else {
			CoinPairData coinPairData = commonService.getDirectCoinPair(getExchangeType());
			Map<String, List<CoinPair>> pair = coinPairData.getPairs();
			if(MapUtils.isEmpty(pair)) {
				return;
			}
			configs = new ArrayList<>();
			
			Collection<List<CoinPair>> values = pair.values();
			
			for (List<CoinPair> coin : values) {
				for (CoinPair coinPair : coin) {
					CoinPairConfig config = new CoinPairConfig();
					BeanUtils.copyProperties(coinPair, config);
					configs.add(config);
				}
			}
		}
		
		if(CollectionUtils.isEmpty(configs)) {
			return;
		}

		for (CoinPairConfig config : configs) {

			KlineTime[] times = KlineTime.values();
			for (KlineTime time : times) {

				List<KlineData> klineDatas = findKline(config.getSymbol(), time.getCode());
				if (CollectionUtils.isEmpty(klineDatas)) {
					continue;
				}

				subscriptionClientListener.sendKlineTime(config, klineDatas, time.getCode(), getExchangeType(),isDirect);
			}

		}
	}
	
	public void updateMarketTickerCache(TickerData data) {
		if (data == null) {
			return;
		}

		data.setBaseCoin(data.getBaseCoin());
		data.setQuoteCoin(data.getQuoteCoin());
		data.setSymbol(data.getSymbol().toLowerCase());

		StringBuffer redisKey = new StringBuffer();
		redisKey.append(RedisKeyConstants.COIN_TICKER);
		redisKey.append(getExchangeType().getCode()).append("_");

		redisKey.append(data.getQuoteCoin().toLowerCase()).append("_");

		HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
		hashOperations.delete(redisKey.toString(), data.getSymbol().toLowerCase());
		hashOperations.put(redisKey.toString(), data.getSymbol().toLowerCase(), JSON.toJSONString(data));
	}

	public void pushTicker(boolean isDirect) {

		List<CoinPairConfig> configs = null;
		if(!isDirect) {
			configs = commonService.getLocalCoinPairByExchangeType(getExchangeType().getCode());
		} else {
			CoinPairData coinPairData = commonService.getDirectCoinPair(getExchangeType());
			Map<String, List<CoinPair>> pair = coinPairData.getPairs();
			if(MapUtils.isEmpty(pair)) {
				return;
			}
			configs = new ArrayList<>();
			
			Collection<List<CoinPair>> values = pair.values();
			
			for (List<CoinPair> coin : values) {
				
				if(CollectionUtils.isEmpty(coin)) {
					continue;
				}
				
				for (CoinPair coinPair : coin) {
					CoinPairConfig config = new CoinPairConfig();
					BeanUtils.copyProperties(coinPair, config);
					configs.add(config);
				}
			}
		}
		
		if(CollectionUtils.isEmpty(configs)) {
			return;
		}

		for (CoinPairConfig config : configs) {
			TickerData data = findTicker24(config.getSymbol());

			updateMarketTickerCache(data);

			subscriptionClientListener.sendTicker(config, data, getExchangeType(),isDirect);
			
		}

	}

	public void pushTradeTicker(boolean isDirect) {

		List<CoinPairConfig> configs = null;
		if(!isDirect) {
			configs = commonService.getLocalCoinPairByExchangeType(getExchangeType().getCode());
		} else {
			CoinPairData coinPairData = commonService.getDirectCoinPair(getExchangeType());
			Map<String, List<CoinPair>> pair = coinPairData.getPairs();
			if(MapUtils.isEmpty(pair)) {
				return;
			}
			configs = new ArrayList<>();
			
			Collection<List<CoinPair>> values = pair.values();
			
			for (List<CoinPair> coin : values) {
				
				if(CollectionUtils.isEmpty(coin)) {
					continue;
				}
				
				for (CoinPair coinPair : coin) {
					CoinPairConfig config = new CoinPairConfig();
					BeanUtils.copyProperties(coinPair, config);
					configs.add(config);
				}
			}
		}
		
		if(CollectionUtils.isEmpty(configs)) {
			return;
		}
		for (CoinPairConfig config : configs) {
			List<TradeData> datas = findTradeTicker(config.getBaseCoin() + config.getQuoteCoin());
			if (CollectionUtils.isEmpty(datas)) {
				datas = new ArrayList<>();
			}

			subscriptionClientListener.sendTradeTicker(config, datas, getExchangeType(),isDirect);
			
		}
		
	}

	public void pushDepthStepData(boolean isDirect) {

		List<CoinPairConfig> configs = null;
		if(!isDirect) {
			configs = commonService.getLocalCoinPairByExchangeType(getExchangeType().getCode());
		} else {
			CoinPairData coinPairData = commonService.getDirectCoinPair(getExchangeType());
			Map<String, List<CoinPair>> pair = coinPairData.getPairs();
			if(MapUtils.isEmpty(pair)) {
				return;
			}
			configs = new ArrayList<>();
			
			Collection<List<CoinPair>> values = pair.values();
			
			for (List<CoinPair> coin : values) {
				
				if(CollectionUtils.isEmpty(coin)) {
					continue;
				}
				
				for (CoinPair coinPair : coin) {
					CoinPairConfig config = new CoinPairConfig();
					BeanUtils.copyProperties(coinPair, config);
					configs.add(config);
				}
			}
		}
		
		if(CollectionUtils.isEmpty(configs)) {
			return;
		}
		
		DepthStep[] steps = DepthStep.values();
		for (CoinPairConfig config : configs) {
			
			for (DepthStep step : steps) {
				DepthStepData data = findDepthStep(config.getBaseCoin() + config.getQuoteCoin(), step.getCode());

				subscriptionClientListener.sendDepthStep(config, data, getDepthStepType(step.getCode()), getExchangeType(),isDirect);
				
			}
		}
	}
	
	protected String getDepthStepType(String code) {
		if (DepthStep.STEP0.getCode().equals(code)) {
			return "0";
		} else if (DepthStep.STEP1.getCode().equals(code)) {
			return "1";
		} else if (DepthStep.STEP2.getCode().equals(code)) {
			return "2";
		}
		return "";
	}

	
	/**
	 * 接收服务端的数据推送
	 * 
	 * @param message
	 */
	public abstract void receiveServerMessage(String message);

	/**
	 * 查询系统支持的所有交易对及精度
	 * 
	 * @return
	 */
	public abstract Map<String, SymbolData> findSymbolData();

	/**
	 * 生成websocket请求参数
	 * 
	 * @param params
	 * @return
	 */
	public abstract String getWebsocketSendMassage(ReqWebsocketParams params);

	/**
	 * 创建WebSocketClientService
	 * 
	 * @return
	 */
	public abstract WebSocketClientService createWebSocketClient();

	/**
	 * websocket req请求处理
	 * 
	 * @param params
	 */
	public abstract void wsReq(ReqWebsocketParams params);
	
	/**
	 * 获取交易所类型
	 * @return
	 */
	public abstract ExchangeType getExchangeType();
	
	/**
	 * 查询深度
	 * @param symbol
	 * @param depthStep
	 * @return
	 */
	public abstract DepthStepData findDepthStep(String symbol, String depthStep);
	
	/**
	 * 查询成交情况
	 * @param symbol
	 * @return
	 */
	public abstract List<TradeData> findTradeTicker(String symbol);
	
	/**
	 * 查询24小时行情
	 * @param symbol
	 * @return
	 */
	public abstract TickerData findTicker24(String symbol);
	
	/**
	 * 查询kline
	 * @param symbol
	 * @param klineTime
	 * @return
	 */
	public abstract List<KlineData> findKline(String symbol, String klineTime);
	
	public List<CoinPairConfig> getMarketCoinPairConfig(boolean isDirect){
		List<CoinPairConfig> configs = new ArrayList<>();
		if(!isDirect) {
			configs = commonService.getLocalCoinPairByExchangeType(getExchangeType().getCode());
		} else {
			CoinPairData coinPairData = commonService.getDirectCoinPair(getExchangeType());
			Map<String, List<CoinPair>> pair = coinPairData.getPairs();
			if(MapUtils.isEmpty(pair)) {
				return configs;
			}
			configs = new ArrayList<>();
			
			Collection<List<CoinPair>> values = pair.values();
			
			for (List<CoinPair> coin : values) {
				
				if(CollectionUtils.isEmpty(coin)) {
					continue;
				}
				
				for (CoinPair coinPair : coin) {
					CoinPairConfig config = new CoinPairConfig();
					BeanUtils.copyProperties(coinPair, config);
					configs.add(config);
				}
			}
		}
		
		return configs;
	}

}
