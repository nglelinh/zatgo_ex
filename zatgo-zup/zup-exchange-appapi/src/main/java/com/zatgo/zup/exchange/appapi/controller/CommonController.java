package com.zatgo.zup.exchange.appapi.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeClientType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.exchange.appapi.model.BaseExchangeParams;
import com.zatgo.zup.exchange.appapi.model.CoinPair;
import com.zatgo.zup.exchange.appapi.model.CoinPairData;
import com.zatgo.zup.exchange.appapi.model.RateData;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.appapi.util.CloudUserInfoMap;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
import com.zatgo.zup.exchange.task.ratetask.SymbolRateTask;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/exchange/appapi", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/exchange/appapi")
public class CommonController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(CommonController.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private ExchangeStrategy exchangeStrategy;
	
	@Autowired
    private SymbolRateTask symbolRateTask;
	
	@Autowired
	private CloudUserInfoMap cloudUserInfoMap;

	@ApiOperation(value = "汇率查询")
	@RequestMapping(value = "/rate", name = "汇率查询", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<RateData> getRate(@ModelAttribute BaseExchangeParams params) {
		String cloudUserId = getCloudUserId();
		RateData data = commonService.getCoinRateData(params.getExchangeType(),cloudUserId);
		return BusinessResponseFactory.createSuccess(data);
	}
	
	@ApiOperation(value = "查询指定币种的法币汇率")
	@RequestMapping(value = "/rateByVirtualCoin/{virtualCoin}/{legalCoin}", name = "指定币对汇率查询", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Map<String, Map<String, BigDecimal>>> getRateByCoinPair(@PathVariable("virtualCoin") String virtualCoin,@PathVariable("legalCoin") String legalCoin) {
		List<String> virtualCoins = new ArrayList<>();
		List<String> legalCoins = new ArrayList<>();
		virtualCoins.add(virtualCoin);
		legalCoins.add(legalCoin);
		
		RateData data = symbolRateTask.findSymbolRate(virtualCoins, legalCoins);
		return BusinessResponseFactory.createSuccess(data.getRates());
	}

	@ApiOperation(value = "币对查询")
	@RequestMapping(value = "/coinPairs", name = "币对查询", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<CoinPairData> getCoinPairs(@ModelAttribute BaseExchangeParams params) {
		String cloudUserId = getCloudUserId();
		CoinPairData pairs;
		if(params.getExchangeType() == null) {
			pairs = commonService.getLocalCoinPair(cloudUserId);
		}else {
			pairs = commonService.getDirectCoinPair(params.getExchangeType());
		}
		
		return BusinessResponseFactory.createSuccess(pairs);
	}

	@ApiOperation(value = "初始化币对")
	@RequestMapping(value = "/refresh/cache", name = "刷新缓存", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Object> initCoinPairs() {
		/* AuthUserInfo userInfo = getUserInfo(); */
		commonService.refreshCache();
		return BusinessResponseFactory.createSuccess(null);
	}


	@ApiOperation(value = "初始化币对脚本", hidden = true)
	@RequestMapping(value = "/sql", name = "初始化币对脚本", method = RequestMethod.GET)
	@ResponseBody
	public void sql() {
		Map<String, String> map1 = new HashMap<String, String>();
		map1.put(ExchangeType.Bytex.getCode(), ExchangeClientType.POLLING.getType());
		map1.put(ExchangeType.Huobi.getCode(), ExchangeClientType.WEBSOCKET.getType());
		map1.put(ExchangeType.Huobi_Hadax.getCode(), ExchangeClientType.WEBSOCKET.getType());

		Set<String> coinSet = new HashSet<>();

		ExchangeType[] types = ExchangeType.values();
		for (ExchangeType exchangeType : types) {

			System.out.println("#==============" + exchangeType.getCode() + "=============");
			logger.info("#==============" + exchangeType.getCode() + "=============");

			WebSocketStrategyService service = exchangeStrategy.getApiWebSocketService(exchangeType);
			Map<String, SymbolData> map = new HashMap<>();
			try {
				map = service.findSymbolData();
			} catch (Exception e) {
				logger.error("", e);
			}
			map.forEach((key, value) -> {

				String coinPairConfigSql = "INSERT INTO `zatgo_dev`.`coin_pair_config`(`id`, `symbol`, `base_coin`, `quote_coin`, `price_precision`, `external_exchange_type`, `maker_fee_increase`, `taker_fee_increase`, `status`, `default_rate`, `client_type`, `coin_image`) VALUES ('#id#', '#symbol#', '#base_coin#', '#quote_coin#', 8, '#external_exchange_type#', 0.00000000000000000000, 0.00000000000000000000, 1, 0.00000000000000000000, '#client_type#', 'http://work.flybycloud.com:6674/checkout/h5/images/coinImg/#coin#.png');";
				String cloudUserCoinPairSql = "INSERT INTO `zatgo_dev`.`cloud_user_coin_pair`(`id`, `cloud_user_id`, `pair_id`, `maker_fee_increase`, `taker_fee_increase`, `status`) VALUES ('#id#', '65c2c3a074774f8090a1e4544a56632a', '#pair_id#', 0.00000000000000000000, 0.00000000000000000000, 1);";
				String pairid = UUIDUtils.getUuid();
				coinPairConfigSql = coinPairConfigSql.replace("#id#", pairid);
				coinPairConfigSql = coinPairConfigSql.replaceAll("#symbol#", key);
				coinPairConfigSql = coinPairConfigSql.replaceAll("#base_coin#", value.getBaseCoin());
				coinPairConfigSql = coinPairConfigSql.replaceAll("#quote_coin#", value.getQuoteCoin());
				coinPairConfigSql = coinPairConfigSql.replaceAll("#external_exchange_type#", exchangeType.getCode());
				coinPairConfigSql = coinPairConfigSql.replaceAll("#client_type#", map1.get(exchangeType.getCode()));
				coinPairConfigSql = coinPairConfigSql.replaceAll("#coin#", value.getBaseCoin().toUpperCase());

				String cupairid = UUIDUtils.getUuid();
				cloudUserCoinPairSql = cloudUserCoinPairSql.replaceAll("#id#", cupairid);
				cloudUserCoinPairSql = cloudUserCoinPairSql.replaceAll("#pair_id#", pairid);

				System.out.println(coinPairConfigSql);
				logger.info(coinPairConfigSql);

				System.out.println(cloudUserCoinPairSql);
				logger.info(cloudUserCoinPairSql);

				coinSet.add(value.getBaseCoin().toUpperCase());

			});

			coinSet.forEach(coin -> {
				String issuedCoinInfoSql = "INSERT INTO `zatgo_dev`.`issued_coin_info`(`record_id`, `coin_full_name`, `coin_short_name`, `coin_symbol`, `coin_decimals`, `issued_number`, `issued_way`, `issued_datetime`, `coin_network_type`, `coin_type`, `coin_image`, `market_price_cny`, `sys_trading_fee_type`, `sys_trading_fee`, `miner_trading_fee`, `lowest_extract_number`, `is_auto_gather_coin`, `upper_limit_amount`, `wallet_main_acct`, `wallet_extract_address`, `gather_main_address`, `is_close_deposit`, `is_close_withdraw`) VALUES "
						+ "('#record_id#', '#coin#', '#coin#', '0', 8, 1, '1', '#date#', NULL, '#coin#', 'http://118.25.180.37:6674/checkout/h5/images/coinImg/#coin#.png', NULL, 0, 0.00000000000000000000, NULL, 0.00000000000000000000, NULL, NULL, NULL, NULL, NULL, 0, 0);";
				String cloudUserCoinRelSql = "INSERT INTO `zatgo_dev`.`cloud_user_coin_rel`(`id`, `cloud_user_id`, `record_id`, `is_close_deposit`, `is_close_withdraw`, `status`) VALUES ('#id#', '65c2c3a074774f8090a1e4544a56632a', '#record_id#', 0, 0, 1);";
				String recordid = UUIDUtils.getUuid();
				issuedCoinInfoSql = issuedCoinInfoSql.replaceAll("#record_id#", recordid);
				issuedCoinInfoSql = issuedCoinInfoSql.replaceAll("#coin#", coin);
				issuedCoinInfoSql = issuedCoinInfoSql.replaceAll("#date#",
						DateTimeUtils.parseDateToString(new Date(), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM_SS));

				String relid = UUIDUtils.getUuid();
				cloudUserCoinRelSql = cloudUserCoinRelSql.replaceAll("#id#", relid);
				cloudUserCoinRelSql = cloudUserCoinRelSql.replaceAll("#record_id#", recordid);

				System.out.println(issuedCoinInfoSql);
				logger.info(issuedCoinInfoSql);

				System.out.println(cloudUserCoinRelSql);
				logger.info(cloudUserCoinRelSql);
			});

			System.out.println("#==============" + exchangeType.getCode() + "=============");
			logger.info("#==============" + exchangeType.getCode() + "=============");
		}
	}
}
