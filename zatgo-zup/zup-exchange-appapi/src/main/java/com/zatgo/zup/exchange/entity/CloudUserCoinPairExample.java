package com.zatgo.zup.exchange.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CloudUserCoinPairExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CloudUserCoinPairExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andPairIdIsNull() {
            addCriterion("pair_id is null");
            return (Criteria) this;
        }

        public Criteria andPairIdIsNotNull() {
            addCriterion("pair_id is not null");
            return (Criteria) this;
        }

        public Criteria andPairIdEqualTo(String value) {
            addCriterion("pair_id =", value, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdNotEqualTo(String value) {
            addCriterion("pair_id <>", value, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdGreaterThan(String value) {
            addCriterion("pair_id >", value, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdGreaterThanOrEqualTo(String value) {
            addCriterion("pair_id >=", value, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdLessThan(String value) {
            addCriterion("pair_id <", value, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdLessThanOrEqualTo(String value) {
            addCriterion("pair_id <=", value, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdLike(String value) {
            addCriterion("pair_id like", value, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdNotLike(String value) {
            addCriterion("pair_id not like", value, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdIn(List<String> values) {
            addCriterion("pair_id in", values, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdNotIn(List<String> values) {
            addCriterion("pair_id not in", values, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdBetween(String value1, String value2) {
            addCriterion("pair_id between", value1, value2, "pairId");
            return (Criteria) this;
        }

        public Criteria andPairIdNotBetween(String value1, String value2) {
            addCriterion("pair_id not between", value1, value2, "pairId");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseIsNull() {
            addCriterion("maker_fee_increase is null");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseIsNotNull() {
            addCriterion("maker_fee_increase is not null");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseEqualTo(BigDecimal value) {
            addCriterion("maker_fee_increase =", value, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseNotEqualTo(BigDecimal value) {
            addCriterion("maker_fee_increase <>", value, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseGreaterThan(BigDecimal value) {
            addCriterion("maker_fee_increase >", value, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("maker_fee_increase >=", value, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseLessThan(BigDecimal value) {
            addCriterion("maker_fee_increase <", value, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseLessThanOrEqualTo(BigDecimal value) {
            addCriterion("maker_fee_increase <=", value, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseIn(List<BigDecimal> values) {
            addCriterion("maker_fee_increase in", values, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseNotIn(List<BigDecimal> values) {
            addCriterion("maker_fee_increase not in", values, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("maker_fee_increase between", value1, value2, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andMakerFeeIncreaseNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("maker_fee_increase not between", value1, value2, "makerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseIsNull() {
            addCriterion("taker_fee_increase is null");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseIsNotNull() {
            addCriterion("taker_fee_increase is not null");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseEqualTo(BigDecimal value) {
            addCriterion("taker_fee_increase =", value, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseNotEqualTo(BigDecimal value) {
            addCriterion("taker_fee_increase <>", value, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseGreaterThan(BigDecimal value) {
            addCriterion("taker_fee_increase >", value, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("taker_fee_increase >=", value, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseLessThan(BigDecimal value) {
            addCriterion("taker_fee_increase <", value, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseLessThanOrEqualTo(BigDecimal value) {
            addCriterion("taker_fee_increase <=", value, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseIn(List<BigDecimal> values) {
            addCriterion("taker_fee_increase in", values, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseNotIn(List<BigDecimal> values) {
            addCriterion("taker_fee_increase not in", values, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taker_fee_increase between", value1, value2, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andTakerFeeIncreaseNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taker_fee_increase not between", value1, value2, "takerFeeIncrease");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeIsNull() {
            addCriterion("external_exchange_type is null");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeIsNotNull() {
            addCriterion("external_exchange_type is not null");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeEqualTo(String value) {
            addCriterion("external_exchange_type =", value, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeNotEqualTo(String value) {
            addCriterion("external_exchange_type <>", value, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeGreaterThan(String value) {
            addCriterion("external_exchange_type >", value, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeGreaterThanOrEqualTo(String value) {
            addCriterion("external_exchange_type >=", value, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeLessThan(String value) {
            addCriterion("external_exchange_type <", value, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeLessThanOrEqualTo(String value) {
            addCriterion("external_exchange_type <=", value, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeLike(String value) {
            addCriterion("external_exchange_type like", value, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeNotLike(String value) {
            addCriterion("external_exchange_type not like", value, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeIn(List<String> values) {
            addCriterion("external_exchange_type in", values, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeNotIn(List<String> values) {
            addCriterion("external_exchange_type not in", values, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeBetween(String value1, String value2) {
            addCriterion("external_exchange_type between", value1, value2, "externalExchangeType");
            return (Criteria) this;
        }

        public Criteria andExternalExchangeTypeNotBetween(String value1, String value2) {
            addCriterion("external_exchange_type not between", value1, value2, "externalExchangeType");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}