package com.zatgo.zup.exchange.strategy.huobi.model;

import java.math.BigDecimal;
import java.util.List;

public class HuobiMarketDepthWsResp {
	
	private List<List<BigDecimal>> bids;
	
	private List<List<BigDecimal>> asks;

	public List<List<BigDecimal>> getBids() {
		return bids;
	}

	public void setBids(List<List<BigDecimal>> bids) {
		this.bids = bids;
	}

	public List<List<BigDecimal>> getAsks() {
		return asks;
	}

	public void setAsks(List<List<BigDecimal>> asks) {
		this.asks = asks;
	}

}
