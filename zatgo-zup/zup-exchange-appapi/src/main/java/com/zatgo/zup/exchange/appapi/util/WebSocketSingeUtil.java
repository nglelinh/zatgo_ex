package com.zatgo.zup.exchange.appapi.util;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.common.model.CloudUserApi;
import com.zatgo.zup.common.model.WebSocketBaseParams;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.common.utils.SignUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;

/**
 * Created by 46041 on 2018/9/18.
 */

@Component
public class WebSocketSingeUtil {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketSingeUtil.class);

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 鉴权验证
     * @param object
     */
    public <T extends WebSocketBaseParams> boolean verify(T object) {
        String cloudUserId = object.getCloudUserId();
        if (StringUtils.isEmpty(cloudUserId))
            cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
        JSONObject params = null;
        JSONObject jsonParams = null;
        //整理签名内容
        if(object!=null){
            jsonParams = getAppInfoJson(object);
            params = JSONObject.parseObject(JSONObject.toJSONString(object));
            Iterator paramsIter = params.keySet().iterator();
            while(paramsIter.hasNext()){
                Object obj = paramsIter.next();
                if(obj!=null){
                    try {
                        JSONObject json = (JSONObject)JSONObject.toJSON(obj);
                        jsonParams.putAll(json);
                    } catch (Exception e) {
                        logger.error("",e);
                    }
                }
            }
        }

        String key = RedisKeyConstants.CLOUD_USER_KEY + cloudUserId;
        CloudUserApi cloudUserApi = redisUtils.get(key, CloudUserApi.class);
        String publicKey = cloudUserApi == null ? null : cloudUserApi.getPublicKey();
        /**
         * 验签
         */
        return SignUtils.signCheck(jsonParams, object.getSign(), publicKey);

    }


    /**
     * 从请求header中获取AppInfo
     * @return
     */
    private <T extends WebSocketBaseParams> JSONObject getAppInfoJson(T params){
        String appId = params.getAppId();
        String appType = params.getAppType();
        String appVersion = params.getAppVersion();
        String ts = params.getTs();
        String token = params.getToken();
        String sign = params.getSign();

        if(StringUtils.isEmpty(appId)
                ||StringUtils.isEmpty(appType)
                ||StringUtils.isEmpty(appVersion)
                ||StringUtils.isEmpty(ts)){
            throw new BusinessException(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
        }

        JSONObject obj = new JSONObject();
        obj.put("appId", appId);
        obj.put("appType", appType);
        obj.put("appVersion", appVersion);
        obj.put("ts", ts);
        obj.put("token", token);
        obj.put("sign", sign);
        return obj;
    }
}
