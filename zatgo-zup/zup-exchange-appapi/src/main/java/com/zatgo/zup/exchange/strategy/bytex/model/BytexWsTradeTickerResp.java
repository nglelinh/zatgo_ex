package com.zatgo.zup.exchange.strategy.bytex.model;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("实时成交信息")
public class BytexWsTradeTickerResp {
	
	@ApiModelProperty(value = "data中最大交易ID", required = true)
	private Long id;
	
	@ApiModelProperty(value = "data中最大时间", required = true)
	private Long ts;
	
	@ApiModelProperty(value = "交易数据", required = true)
	private List<BytexWsTradeTickerData> data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

	public List<BytexWsTradeTickerData> getData() {
		return data;
	}

	public void setData(List<BytexWsTradeTickerData> data) {
		this.data = data;
	}

}
