package com.zatgo.zup.exchange.strategy.huobi.model;

public class HuobiOrderPlaceResp extends HuobiResp {
	
	/**
	 * 订单id
	 */
	private String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
