package com.zatgo.zup.exchange.appapi.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class CoinPair {

	@ApiModelProperty(value = "市场标记，如：ethbtc",required =true )
	private String symbol;
	
	@ApiModelProperty(value = "基准货币",required =true )
	private String baseCoin;
	
	@ApiModelProperty(value = "计价货币",required =true )
	private String quoteCoin;
	
	@ApiModelProperty(value = "价格精度 ：下单时需要校验输入的价格小数，需小于价格精度，价格精度0代表到个位",required =true )
	private BigDecimal pricePrecision;
	
	@ApiModelProperty(value = "数量精度：同上",required =true )
	private BigDecimal amountPrecision;
	
	@ApiModelProperty(value = "图片",required =false )
	private String coinImage;
	
	@ApiModelProperty(value = "币对排序",required =false )
	private Byte sort;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	public BigDecimal getPricePrecision() {
		return pricePrecision;
	}

	public void setPricePrecision(BigDecimal pricePrecision) {
		this.pricePrecision = pricePrecision;
	}

	public BigDecimal getAmountPrecision() {
		return amountPrecision;
	}

	public void setAmountPrecision(BigDecimal amountPrecision) {
		this.amountPrecision = amountPrecision;
	}

	public String getCoinImage() {
		return coinImage;
	}

	public void setCoinImage(String coinImage) {
		this.coinImage = coinImage;
	}

	public Byte getSort() {
		return sort;
	}

	public void setSort(Byte sort) {
		this.sort = sort;
	}
	
}
