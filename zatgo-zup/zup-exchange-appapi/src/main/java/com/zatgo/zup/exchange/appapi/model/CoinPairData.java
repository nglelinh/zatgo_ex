package com.zatgo.zup.exchange.appapi.model;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("币对数据")
public class CoinPairData {
	
	@ApiModelProperty(value = "计价货币", required = true)
	private List<String> quoteCoinList;
	
	@ApiModelProperty(value = "对应币对", required = true)
	private Map<String, List<CoinPair>> pairs;

	public List<String> getQuoteCoinList() {
		return quoteCoinList;
	}

	public void setQuoteCoinList(List<String> quoteCoinList) {
		this.quoteCoinList = quoteCoinList;
	}

	public Map<String, List<CoinPair>> getPairs() {
		return pairs;
	}

	public void setPairs(Map<String, List<CoinPair>> pairs) {
		this.pairs = pairs;
	}

}
