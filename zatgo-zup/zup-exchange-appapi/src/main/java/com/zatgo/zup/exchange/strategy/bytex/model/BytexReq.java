package com.zatgo.zup.exchange.strategy.bytex.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.annotation.JSONField;
import com.zatgo.zup.common.utils.BeanToMapUtils;
import com.zatgo.zup.common.utils.MD5;

public class BytexReq {

	@JSONField(name = "api_key")
	private String api_key;

	private String sign;
	
	private Long time;

	public String getApi_key() {
		return api_key;
	}

	public void setApi_key(String api_key) {
		this.api_key = api_key;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	protected void sign(String apiKey, String secretKey, BytexReq res) {
		this.time = System.currentTimeMillis();
		this.api_key = apiKey;
		String sign = "";
		try {
			StringBuffer _sign = new StringBuffer();
			HashMap<String, String> map = BeanToMapUtils.beanToHashMap(res);
			
			Map<String, String> sortedParams = new TreeMap<String, String>(map);
		    Set<Entry<String, String>> entrys = sortedParams.entrySet();
		    for (Entry<String, String> param : entrys) {
		    	if (StringUtils.isEmpty(param.getValue())) {
					return;
				}
				_sign.append(param.getKey()).append(param.getValue());
		    }
			_sign.append(secretKey);
			
			sign = MD5.GetMD5Code(_sign.toString());
		} catch (Exception e) {

		}

		this.sign = sign;
	}

}
