package com.zatgo.zup.exchange.task.ratetask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.ExchangeStrategy;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;

@Component
public class SymbolTask {
	
	private static final Logger log = LoggerFactory.getLogger(SymbolTask.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private ExchangeStrategy exchangeStrategy;

	public void updateSymbol() {

		List<CoinPairConfig> configs = commonService.getCoinPairCache();
		if (CollectionUtils.isEmpty(configs)) {
			return;
		}

		Map<String, List<CoinPairConfig>> coinPairMap = new HashMap<String, List<CoinPairConfig>>();
		configs.forEach(value -> {
			String[] exchangeTypes = value.getExternalExchangeTypes() ==null? new String[0]:value.getExternalExchangeTypes().split(",");
			for(String exchangeType:exchangeTypes) {
				List<CoinPairConfig> _coinList = coinPairMap.get(exchangeType);
				if (CollectionUtils.isEmpty(_coinList)) {
					_coinList = new ArrayList<>();
				}
				_coinList.add(value);
				coinPairMap.put(exchangeType, _coinList);
			}
			
		});

		Map<String,Map<String, Map<String, SymbolData>>> res = new HashMap<>();
		Set<String> exchangeTypeSet = coinPairMap.keySet();
		exchangeTypeSet.forEach(value -> {
			WebSocketStrategyService webSocketStrategyService = exchangeStrategy
					.getApiWebSocketService(ExchangeType.getEnumByType(value));
			Map<String, SymbolData> symbolMap = null;
			try {
				symbolMap = webSocketStrategyService.findSymbolData();
			} catch (Exception e) {
				log.error("", e);
			}
			if (MapUtils.isEmpty(symbolMap)) {
				String logInfo = "交易所exchangeType=" + value + ",不存在任何币对，数据库币对配置有问题";
				log.error(logInfo);
				return;
			}

			List<CoinPairConfig> _configs = coinPairMap.get(value);
			for (CoinPairConfig _config : _configs) {

				//计价币分类
				String quoteCoin = _config.getQuoteCoin().toLowerCase();
				Map<String,Map<String, SymbolData>> _res = res.get(quoteCoin);
				if (MapUtils.isEmpty(_res)) {
					_res = new HashMap<>();
				}

				//交易所分类
				Map<String, SymbolData> _res1 = _res.get(value);
				if (MapUtils.isEmpty(_res1)) {
					_res1 = new HashMap<>();
				}
				_res.put(value, _res1);
				
				//币对信息
				String symbol = _config.getSymbol().toLowerCase();
				if(symbolMap.get(symbol.toLowerCase()) == null) {
					log.error("数据库币对配置有问题，交易所exchangeType=" + value + ",不存在币对=" + symbol.toLowerCase());
				}
				_res1.put(symbol, symbolMap.get(symbol.toLowerCase()));

				res.put(quoteCoin, _res);
			}
		});

		ValueOperations<String, String> operations = redisTemplate.opsForValue();
		operations.set(RedisKeyConstants.SYMBOL, JSON.toJSONString(res));

	}

}
