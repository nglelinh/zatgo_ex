package com.zatgo.zup.exchange.strategy.huobi.model;

import java.math.BigDecimal;

public class HuobiTradeDetailWsResp {
	
	/**
	 * 消息ID(成交ID?)
	 */
	private String id;
	
	/**
	 * 成交价
	 */
	private BigDecimal price;
	
	/**
	 * 成交时间(空)
	 */
	private Long time;
	
	/**
	 * 成交量
	 */
	private BigDecimal amount;
	
	/**
	 * 成交方向
	 */
	private String direction;
	
	/**
	 * 成交ID(空)
	 */
	private String tradeId;
	
	/**
	 * ts
	 */
	private Long ts;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

}
