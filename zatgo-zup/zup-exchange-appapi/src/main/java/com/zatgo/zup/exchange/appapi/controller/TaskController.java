package com.zatgo.zup.exchange.appapi.controller;

import com.zatgo.zup.exchange.task.ratetask.SymbolRateTask;
import com.zatgo.zup.exchange.task.ratetask.SymbolTask;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 46041 on 2018/9/26.
 */

@RestController
@Api(value = "/exchange/task", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/exchange/task")
public class TaskController {

    @Autowired
    private SymbolRateTask symbolRateTask;

    @Autowired
    private SymbolTask symbolTask;


    @ApiOperation(value = "updateSymbolRate定时器内部调用")
    @GetMapping(value = "/symbol/rateTask", name = "定时器内部调用")
    public void updateSymbolRate() {
        symbolRateTask.updateSymbolRateToRedis();
    }

    @ApiOperation(value = "symbol定时器内部调用")
    @GetMapping(value = "/symbolTask", name = "定时器内部调用")
    public void updateSymbol() {
        symbolTask.updateSymbol();
    }
}
