package com.zatgo.zup.exchange.appapi.util;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.Session;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;

public class WebSocketSessionUtil {

	private static CopyOnWriteArraySet<Session> sessions = new CopyOnWriteArraySet<Session>();
	private static final Map<String, CopyOnWriteArraySet<Session>> sessionMap = new ConcurrentHashMap<String, CopyOnWriteArraySet<Session>>();

	private static CopyOnWriteArrayList<ExchangeType> directExchangeTypeList = new CopyOnWriteArrayList<ExchangeType>();

	private static final Map<Session, Integer> pingPongMap = new ConcurrentHashMap<Session, Integer>();

	private static final Integer PING_PONG_NUM = 3;

	public static void addSession(Session session) {
		sessions.add(session);
	}

	public static void removeSession(Session session) {
		if (CollectionUtils.isEmpty(sessions)) {
			return;
		}
		sessions.remove(session);
	}

	public static CopyOnWriteArraySet<Session> getSessions() {
		return sessions;
	}

	public static int getSessionsSize() {
		return sessions.size();
	}

	public static void removeSessionMap(Session session) {
		if (MapUtils.isEmpty(sessionMap)) {
			return;
		}
		sessionMap.forEach((key, value) -> {
			value.remove(session);
		});
	}

	public static void closeSession(Session session) {
		removeSession(session);
		removeSessionMap(session);
		removePingpongmapSession(session);
		try {
			session.close();
		} catch (IOException e) {

		}
	}

	synchronized public static CopyOnWriteArraySet<Session> getChannelSession(String channel) {
		return sessionMap.get(channel);
	}

	synchronized public static void webSocketSub(Session session, String channel, ExchangeType exchangeType) {
		CopyOnWriteArraySet<Session> channelSessions = WebSocketSessionUtil.getChannelSession(channel);
		if (CollectionUtils.isEmpty(channelSessions)) {
			channelSessions = new CopyOnWriteArraySet<Session>();
			sessionMap.put(channel, channelSessions);
		}
		channelSessions.add(session);
		if (exchangeType != null) {
			directExchangeTypeList.add(exchangeType);
		}
	}

	public static void webSocketUnsub(Session session, String channel, ExchangeType exchangeType) {
		Set<Session> channelSessions = WebSocketSessionUtil.getChannelSession(channel);
		if (CollectionUtils.isNotEmpty(channelSessions)) {
			channelSessions.remove(session);
		}

		if (exchangeType != null) {
			directExchangeTypeList.remove(exchangeType);
		}
	}

	public static void webSocketReq(Session session, String channel) {
		CopyOnWriteArraySet<Session> channelSessions = WebSocketSessionUtil.getChannelSession(channel);
		if (CollectionUtils.isEmpty(channelSessions)) {
			channelSessions = new CopyOnWriteArraySet<Session>();
			sessionMap.put(channel, channelSessions);
		}
		channelSessions.add(session);
	}

	public static List<ExchangeType> getDirectExchangeTypeList() {
		return directExchangeTypeList;
	}
	
	/**
	 * 获取目前直连的交易所
	 * @return
	 */
	public static Set<ExchangeType> getDirectExchangeSet() {
		List<ExchangeType> exchangeTypeList = getDirectExchangeTypeList();
		if (CollectionUtils.isEmpty(exchangeTypeList)) {
			return null;
		}

		Set<ExchangeType> exchangeTypeSet = new HashSet<ExchangeType>(exchangeTypeList);
		return exchangeTypeSet;
	}

	public static void addPingpongmapSession(Session session) {
		Integer num = pingPongMap.get(session);
		if (num == null) {
			num = 0;
		}

		pingPongMap.put(session, ++num);
	}

	public static Map<Session, Integer> getPingpongmap() {
		return pingPongMap;
	}

	public static void removePingpongmapSession(Session session) {
		pingPongMap.remove(session);
	}

	public static void checkPingpongmapSession() {
		pingPongMap.forEach((key, value) -> {
			if (value >= PING_PONG_NUM) {
				closeSession(key);
			}
		});
	}
	
	public static void removeChannel(String channel) {
		sessionMap.remove(channel);
	}

}
