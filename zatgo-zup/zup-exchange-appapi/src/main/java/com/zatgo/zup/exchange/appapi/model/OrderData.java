package com.zatgo.zup.exchange.appapi.model;

import java.math.BigDecimal;
import java.util.Date;

import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "订单信息")
public class OrderData {

	@ApiModelProperty(value = "订单ID",required =true )
	private String orderId;
	
	@ApiModelProperty(value = "市场标记，如：ethbtc",required =true )
	private String symbol;
	
	@ApiModelProperty(value = "基准货币",required =true )
	private String baseCoin;
	
	@ApiModelProperty(value = "计价货币",required =true )
	private String quoteCoin;

	@ApiModelProperty(value = "挂单类型：1-limitPriceEntrust现价单；2-marketPriceEntrust市价单", required =true, allowableValues = "1,2")
	private EntryOrdersType type;
	
	@ApiModelProperty(value = "购买数量（多义，复用字段）\r\n" + 
			"type=1:表示买卖数量\r\n" + 
			"type=2:买则表示总价格，卖表示总个数",required =true )
	private BigDecimal volume;
	
	@ApiModelProperty(value = "买卖方向：BUY-买；SELL-卖", required =true, allowableValues = "BUY,SELL")
	private ExchangeSide side;
	
	@ApiModelProperty(value = "委托单价：type=2：不需要此参数",required =false )
	private BigDecimal price;
	
	@ApiModelProperty(value = "当交易所有平台币时，此参数表示是否使用用平台币支付手续费",required =false )
	private Boolean feeIsUserPlatformCoin;
	
	@ApiModelProperty(value = "成交量",required =true )
	private BigDecimal dealVolume;
	
	@ApiModelProperty(value = "成交均价",required =true )
	private BigDecimal avgPrice;
	
	@ApiModelProperty(value = "订单状态：0 init，1 new，2 filled，3 part_filled，4 canceled，5 pending_cancel，6 expired", required =true, allowableValues = "0,1,2,3,4,5,6")
	private OrderStatus status;
	
	@ApiModelProperty(value = "创建时间", required = true)
	private Date createDate;
	
	@ApiModelProperty(value = "剩余数量", required = true)
	private BigDecimal remainVolume;
	
	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public EntryOrdersType getType() {
		return type;
	}

	public void setType(EntryOrdersType type) {
		this.type = type;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public ExchangeSide getSide() {
		return side;
	}

	public void setSide(ExchangeSide side) {
		this.side = side;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getFeeIsUserPlatformCoin() {
		return feeIsUserPlatformCoin;
	}

	public void setFeeIsUserPlatformCoin(Boolean feeIsUserPlatformCoin) {
		this.feeIsUserPlatformCoin = feeIsUserPlatformCoin;
	}

	public BigDecimal getDealVolume() {
		return dealVolume;
	}

	public void setDealVolume(BigDecimal dealVolume) {
		this.dealVolume = dealVolume;
	}

	public BigDecimal getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(BigDecimal avgPrice) {
		this.avgPrice = avgPrice;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getRemainVolume() {
		return remainVolume;
	}

	public void setRemainVolume(BigDecimal remainVolume) {
		this.remainVolume = remainVolume;
	}
	
	
}
