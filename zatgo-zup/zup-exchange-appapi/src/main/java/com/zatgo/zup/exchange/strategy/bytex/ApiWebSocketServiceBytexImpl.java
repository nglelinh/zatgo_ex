package com.zatgo.zup.exchange.strategy.bytex;

import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.enumtype.ExchangeEnum.BytexRespCode;
import com.zatgo.zup.common.enumtype.ExchangeEnum.BytexWebSocketEvent;
import com.zatgo.zup.common.enumtype.ExchangeEnum.DepthStep;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeClientType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketEvent;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketOperateType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.exchange.appapi.controller.ApiWebSocketController;
import com.zatgo.zup.exchange.appapi.listener.SubscriptionClientListener;
import com.zatgo.zup.exchange.appapi.model.DepthData;
import com.zatgo.zup.exchange.appapi.model.DepthStepData;
import com.zatgo.zup.exchange.appapi.model.KlineData;
import com.zatgo.zup.exchange.appapi.model.ReqWebsocketParams;
import com.zatgo.zup.exchange.appapi.model.RespWebSocketData;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.appapi.model.TickerData;
import com.zatgo.zup.exchange.appapi.model.TradeData;
import com.zatgo.zup.exchange.appapi.util.CoinPairUtils;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.strategy.WebSocketClientService;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexPollingReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexSymbolResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexTickerResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexTradeTickerResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexWsDepthData;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexWsKlineData;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexWsParams;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexWsReq;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexWsResp;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexWsTickerData;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexWsTradeTickerData;
import com.zatgo.zup.exchange.strategy.bytex.util.BytexTopicUtil;

@Component("ApiWebSocketService_bytex")
public class ApiWebSocketServiceBytexImpl extends WebSocketStrategyService {

	private static final Logger log = LoggerFactory.getLogger(ApiWebSocketServiceBytexImpl.class);

	@Value("${exchange.bytex.http.url}")
	private String bytexUrl;

	@Value("${exchange.bytex.websocket.url:}")
	private String wsUri;
	
	private String clientType = ExchangeClientType.POLLING.getType();

	@Autowired
	private OkHttpService httpService;

	@Autowired
	private ApiWebSocketController apiWebSocketController;

	@Autowired
	private SubscriptionClientListener subscriptionClientListener;

	@Autowired
	private CommonService commonService;
	
	@Autowired
	private RedisTemplate redisTemplate;
	
	private ExchangeType exchangeType = ExchangeType.Bytex;

	@Override
	public void receiveServerMessage(String message) {

		BytexWsResp resp = JSON.parseObject(message, BytexWsResp.class);
		
		Object json = resp.getWsData();
		
		if(json == null) {
			log.debug("bytex接收数据为null");
			return;
		}
		
		String channel = resp.getChannel();
		String[] channels = channel.split("_");
		String symbol = channels[1];
		
		WebSocketEvent event = resp.getWebSocketEvent();
		
		String type = channels[2];
		
		RespWebSocketData data = null;
		if(type.equals("kline")) {
			data = parseKlineData(event, json);
			data.setKlineTime(BytexTopicUtil.getKlineTime(channels[3]));
		} else if(type.equals("ticker")) {
			data = parseTickerData(symbol, json);
			updateMarketTickerCache((TickerData)data.data);
		} else if(type.equals("trade")) {
			data = parseTradeTickerData(event, json);
		} else if(type.equals("depth")) {
			data = parseDepthData(json, channels[3]);
		} else {
			return;
		}
		
		data.setEvent(event);

		if (WebSocketEvent.REQ.equals(event)) {
			data.setOperateType(null);
		}

		data.setMD5(MD5.GetMD5Code(JSON.toJSONString(data.getData())));

		String[] coins = CoinPairUtils.getSymbol(symbol);
		data.setBaseCoin(coins[0]);
		data.setQuoteCoin(coins[1]);
		data.setSymbol(symbol);
		
		sendDataToClient(data, ExchangeType.Bytex);
		
	}
	
	private RespWebSocketData<List<KlineData>> parseKlineData(WebSocketEvent event, Object json) {
		
		RespWebSocketData<List<KlineData>> res = new RespWebSocketData<List<KlineData>>();
		List<BytexWsKlineData> datas = null;
		if (event.equals(WebSocketEvent.REQ)) {
			datas = JSON.parseArray(JSON.toJSONString(json), BytexWsKlineData.class);
			res.setOperateType(WebSocketOperateType.FULL);
		} else if (event.equals(WebSocketEvent.SUB)) {
			datas = Arrays.asList(JSON.parseObject(JSON.toJSONString(json), BytexWsKlineData.class));
			res.setOperateType(WebSocketOperateType.INCREMENT);
		}

		List<KlineData> klineDatas = new ArrayList<KlineData>();
		for (BytexWsKlineData data : datas) {
			KlineData klineData = new KlineData();
			klineData.setAmount(data.getVol());
			klineData.setClose(data.getClose());
			klineData.setHigh(data.getHigh());
			klineData.setLow(data.getLow());
			klineData.setOpen(data.getOpen());
			klineData.setVol(data.getAmount());
			klineData.setTimePoint(data.getId());
			klineDatas.add(klineData);
		}

		res.setData(klineDatas);
		res.setType(WebSocketBusiType.KLINE);

		return res;
	}
	
	private RespWebSocketData<TickerData> parseTickerData(String symbol, Object json) {
		
		BytexWsTickerData resp = JSON.parseObject(JSON.toJSONString(json), BytexWsTickerData.class);

		String quoteCoin = CoinPairUtils.getQuoteCoin(symbol);
		if (StringUtils.isEmpty(quoteCoin)) {
			return null;
		}

		String baseCoin = CoinPairUtils.getSymbol(symbol)[0];
		TickerData tickerData = new TickerData();
		tickerData.setBaseCoin(baseCoin);
		tickerData.setHigh(resp.getHigh());
		tickerData.setLast(resp.getClose());
		tickerData.setLow(resp.getLow());
		tickerData.setQuoteCoin(quoteCoin);
		tickerData.setVol(resp.getVol());
		tickerData.setAmount(resp.getAmount());
		tickerData.setClose(resp.getClose());
		tickerData.setChg(resp.getRose());

		RespWebSocketData<TickerData> data = new RespWebSocketData<>();
		data.setBaseCoin(baseCoin);
		data.setData(tickerData);
		data.setQuoteCoin(quoteCoin);
		data.setSymbol(symbol);
		data.setType(WebSocketBusiType.TICKER);
		data.setOperateType(WebSocketOperateType.INCREMENT);
		
		return data;
	}
	
	private RespWebSocketData<List<TradeData>> parseTradeTickerData(WebSocketEvent event, Object json) {
		
		RespWebSocketData<List<TradeData>> res = new RespWebSocketData<>();
		
		List<BytexWsTradeTickerData> wsResps = new ArrayList<>();
		if (event.equals(WebSocketEvent.REQ)) {
			wsResps = JSON.parseArray(JSON.toJSONString(json), BytexWsTradeTickerData.class);
			res.setOperateType(WebSocketOperateType.FULL);
		} else if (event.equals(WebSocketEvent.SUB)) {
			JSONObject object = JSON.parseObject(JSON.toJSONString(json));
			wsResps = JSON.parseArray(object.getJSONArray("data").toJSONString(), BytexWsTradeTickerData.class);
			res.setOperateType(WebSocketOperateType.INCREMENT);
		}

		List<TradeData> datas = new ArrayList<>();
		wsResps.forEach(wsResp -> {
			TradeData data = new TradeData();
			data.setId(wsResp.getId().toString());
			data.setPrice(wsResp.getPrice());
			data.setVol(wsResp.getAmount());
			data.setSide(ExchangeSide.getEnumByType(wsResp.getSide()));
			data.setTs(wsResp.getTs());
			datas.add(data);
		});

		
		res.setData(datas);
		res.setType(WebSocketBusiType.TRADE_TICKER);

		return res;
	}
	
	private RespWebSocketData<DepthStepData> parseDepthData(Object json, String type) {
		BytexWsDepthData wsResp = JSON.parseObject(JSON.toJSONString(json), BytexWsDepthData.class);
		List<DepthData> bidList = new ArrayList<>();
		List<List<BigDecimal>> bids = wsResp.getBids();
		bids.forEach(bid -> {
			if (CollectionUtils.isEmpty(bid) || bid.size() < 2) {
				return;
			}

			DepthData depthData = new DepthData();
			depthData.setPrice(bid.get(0));
			depthData.setAmount(bid.get(1));
			bidList.add(depthData);
		});

		List<DepthData> askList = new ArrayList<>();
		List<List<BigDecimal>> asks = wsResp.getAsks();
		asks.forEach(ask -> {
			if (CollectionUtils.isEmpty(ask) || ask.size() < 2) {
				return;
			}

			DepthData depthData = new DepthData();
			depthData.setPrice(ask.get(0));
			depthData.setAmount(ask.get(1));
			askList.add(depthData);
		});

		DepthStepData stepData = new DepthStepData();
		stepData.setAsks(askList);
		stepData.setBids(bidList);

		RespWebSocketData<DepthStepData> data = new RespWebSocketData<>();
		data.setData(stepData);
		data.setOperateType(WebSocketOperateType.INCREMENT);
		data.setType(WebSocketBusiType.DEPTH_STEP);
		data.setDepthStep(DepthStep.getEnumByType(type.toLowerCase()).getCode());
		return data;
	}

	/**
	 * 查询K线行情
	 * 
	 * @param symbol
	 *            币对
	 * @param klineTime
	 *            1min,5min,15min,30min,60min,1day,1week,1month
	 * @return
	 */
	public List<KlineData> findKline(String symbol, String klineTime) {

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("symbol", symbol.toLowerCase());
		params.put("period", klineTime);

		String str = httpService.getStr(bytexUrl + "/open/api/get_records", params);

		List<KlineData> res = new ArrayList<KlineData>();

		@SuppressWarnings("unchecked")
		BytexResp<JSONArray> resp = JSON.parseObject(str, BytexResp.class);
		if (!BytexRespCode.SUCCESS.getCode().equals(resp.getCode())) {
			log.error("获取bytex kline数据失败：" + str);
			return res;
		}

		resp.setClazz(JSONArray.class);
		JSONArray data = resp.getData();
		if (data == null) {
			log.debug("获取bytex kline数据未返回：" + str);
			return res;
		}

		data.forEach(value -> {
			JSONArray _data = JSON.parseArray(JSON.toJSONString(value));

			if (_data.size() < 6) {
				return;
			}

			String timePoint = _data.getString(0);
			BigDecimal open = _data.getBigDecimal(1);
			BigDecimal high = _data.getBigDecimal(2);
			BigDecimal low = _data.getBigDecimal(3);
			BigDecimal close = _data.getBigDecimal(4);
			BigDecimal vol = _data.getBigDecimal(5);

			KlineData klineData = new KlineData();
			klineData.setTimePoint(timePoint);
			klineData.setClose(close);
			klineData.setHigh(high);
			klineData.setLow(low);
			klineData.setOpen(open);
			klineData.setVol(vol);
			res.add(klineData);
		});

		return res;
	}

	/**
	 * 查询盘口深度
	 * 
	 * @param symbol
	 *            币对
	 * @param depthStep
	 *            取值：0，1，2
	 * @return
	 */
	public DepthStepData findDepthStep(String symbol, String depthStep) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("symbol", symbol.toLowerCase());
		params.put("type", depthStep);

		String str = httpService.getStr(bytexUrl + "/open/api/market_dept", params);

		JSONObject data = JSON.parseObject(str);
		if (!BytexRespCode.SUCCESS.getCode().equals(data.getString("code"))) {
			log.error("获取bytex depth step数据失败：" + str);
			return null;
		}

		JSONObject tick = data.getJSONObject("data").getJSONObject("tick");
		if (tick == null) {
			log.debug("获取bytex depth step数据未返回：" + str);
			return null;
		}

		DepthStepData res = new DepthStepData();
		List<DepthData> asksList = new ArrayList<>();
		JSONArray asks = tick.getJSONArray("asks");
		if (CollectionUtils.isEmpty(asks)) {
			res.setAsks(asksList);
		} else {
			for (int i = 0; i < asks.size(); i++) {
				JSONArray _value = asks.getJSONArray(i);

				BigDecimal price = _value.getBigDecimal(0);
				BigDecimal amount = _value.getBigDecimal(1);

				DepthData ask = new DepthData();
				ask.setAmount(amount);
				ask.setPrice(price);
				asksList.add(ask);
			}
			asksList.sort((hight, low) -> hight.getPrice().compareTo(low.getPrice()));
			res.setAsks(asksList);
		}

		List<DepthData> bidsList = new ArrayList<>();
		JSONArray bids = tick.getJSONArray("bids");
		if (CollectionUtils.isEmpty(bids)) {
			res.setBids(bidsList);
		} else {
			for (int i = 0; i < bids.size(); i++) {
				JSONArray _value = bids.getJSONArray(i);

				BigDecimal price = _value.getBigDecimal(0);
				BigDecimal amount = _value.getBigDecimal(1);

				DepthData bid = new DepthData();
				bid.setAmount(amount);
				bid.setPrice(price);
				bidsList.add(bid);
			}
			bidsList.sort((hight, low) -> low.getPrice().compareTo(hight.getPrice()));
			res.setBids(bidsList);
		}

		return res;
	}

	/**
	 * 获取行情成交记录
	 * 
	 * @param symbol
	 *            币对
	 * @return
	 */
	public List<TradeData> findTradeTicker(String symbol) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("symbol", symbol.toLowerCase());

		String str = httpService.getStr(bytexUrl + "/open/api/get_trades", params);

		@SuppressWarnings("unchecked")
		BytexResp<BytexTradeTickerResp> resp = JSON.parseObject(str, BytexResp.class);

		List<TradeData> res = new ArrayList<TradeData>();
		if (!BytexRespCode.SUCCESS.getCode().equals(resp.getCode())) {
			log.error("获取bytex trade ticker数据失败：" + str);
			return res;
		}

		resp.setClazz(BytexTradeTickerResp.class);

		List<BytexTradeTickerResp> datas = resp.getDatas();
		if (CollectionUtils.isEmpty(datas)) {
			log.debug("获取bytex trade ticker数据未返回：" + str);
			return res;
		}

		datas.forEach(value -> {
			TradeData data = new TradeData();
			data.setId(value.getId().toString());
			data.setPrice(value.getPrice());
			data.setVol(value.getAmount());
			data.setSide(ExchangeSide.getEnumByType(value.getType().toUpperCase()));
			res.add(data);
		});

		return res;
	}

	/**
	 * 前24小时行情
	 * 
	 * @param symbol
	 * @return
	 */
	public TickerData findTicker24(String symbol) {

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("symbol", symbol.toLowerCase());

		String str = httpService.getStr(bytexUrl + "/open/api/get_ticker", params);

		@SuppressWarnings("unchecked")
		BytexResp<Map> resp = JSON.parseObject(str, BytexResp.class);

		if (!BytexRespCode.SUCCESS.getCode().equals(resp.getCode())) {
			log.error("获取bytex ticker数据失败：" + str);
			return null;
		}

		resp.setClazz(Map.class);
		Map<String, JSONObject> _data = resp.getData();
		if (MapUtils.isEmpty(_data)) {
			log.debug("获取bytex ticker数据未返回：" + str);
			return null;
		}

		BytexTickerResp data = JSON.toJavaObject(_data.get(symbol), BytexTickerResp.class);

		TickerData res = new TickerData();
		res.setBuy(data.getBuy());
		res.setHigh(data.getHigh());
		res.setLow(data.getLow());
		res.setLast(data.getLast());
		res.setSell(data.getSell());
		res.setTime(data.getTime());
		res.setVol(data.getVol());

		return res;
	}

	@Override
	public Map<String, SymbolData> findSymbolData() {

		Map<String, SymbolData> symbolData = new HashMap<>();

		try {
			String str = httpService.getStr(bytexUrl + "/open/api/common/symbols", null);
			log.debug("查询bytex币对：", str);

			BytexResp<BytexSymbolResp> bytexResp = JSON.parseObject(str, BytexResp.class);
			if (bytexResp == null) {
				log.error("bytex币对未返回");
				return null;
			}

			if (!BytexRespCode.SUCCESS.getCode().equals(bytexResp.getCode())) {
				log.error("bytex获取币对失败：", bytexResp.getMsg());
				return null;
			}

			bytexResp.setClazz(BytexSymbolResp.class);
			List<BytexSymbolResp> symbolResps = bytexResp.getDatas();
			for (BytexSymbolResp _resp : symbolResps) {

				SymbolData data = new SymbolData();
				data.setAmountPrecision(_resp.getAmountPrecision());
				data.setBaseCoin(_resp.getBaseCoin().toLowerCase());
				data.setPricePrecision(_resp.getPricePrecision());
				data.setQuoteCoin(_resp.getCountCoin().toLowerCase());
				data.setSymbol(_resp.getSymbol().toLowerCase());

				symbolData.put(_resp.getSymbol().toLowerCase(), data);
			}
		} catch (Exception e) {
			log.error("获取bytex币对失败：", e);
		}

		return symbolData;
	}

	@Override
	public String getWebsocketSendMassage(ReqWebsocketParams params) {

		if(clientType.equals(ExchangeClientType.POLLING.getType())) {
			
			BytexPollingReq req = new BytexPollingReq();
			req.setBuisType(params.getType().getCode());
			CoinPairConfig coinPairConfig = commonService.getCoinPairCache(params.getSymbol());
			req.setCoinPairConfig(coinPairConfig);
			req.setEvent(params.getEvent().getCode());
			return JSON.toJSONString(req);
			
		}else if(clientType.equals(ExchangeClientType.WEBSOCKET.getType())) {
			WebSocketEvent event = params.getEvent();
			String channel = BytexTopicUtil.getTopic(params.getType(), params.getSymbol(), params.getKlineTime(),
					params.getDepthStep());
			
			BytexWsReq req = new BytexWsReq();
			if(WebSocketEvent.REQ.equals(event)) {
				req.setEvent(BytexWebSocketEvent.REQ.getEvent());
			} else if(WebSocketEvent.SUB.equals(event)) {
				req.setEvent(BytexWebSocketEvent.SUB.getEvent());
			}else if(WebSocketEvent.UNSUB.equals(event)) {
				
			}
			
			BytexWsParams wsParams = new BytexWsParams();
			wsParams.setChannel(channel);
			
			req.setParams(wsParams);
			
			return JSON.toJSONString(req);
		}else {
			log.error("bytex 配置clientType不支持：" + clientType);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"bytex ");
		}

	}

	@Override
	public WebSocketClientService createWebSocketClient() {
		if(clientType.equals(ExchangeClientType.POLLING.getType())) {
			try {
				URI uri = new URI(wsUri);
				BytexWebsocketPollingClient client = new BytexWebsocketPollingClient(uri);
				client.setExchangeType(ExchangeType.Bytex.getCode());
				client.setService(this);
				return client;
			} catch (Exception e) {
				log.error("", e);
				throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
			}
		}else if(clientType.equals(ExchangeClientType.WEBSOCKET.getType())) {
			try {
				URI uri = new URI(wsUri);
				BytexWebSocketServiceClient client = new BytexWebSocketServiceClient(uri);
				client.setExchangeType(ExchangeType.Bytex.getCode());
				client.setService(this);
				return client;
			} catch (Exception e) {
				log.error("", e);
				throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
			}
		}else {
			log.error("bytex 配置clientType不支持：" + clientType);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"bytex ");
		}
		

	}

	@Override
	public void wsReq(ReqWebsocketParams params) {

		WebSocketBusiType busiType = params.getType();

		String symbol = params.getSymbol();
		ExchangeType exchangeType = ExchangeType.Bytex;
		String baseCoin = params.getBaseCoin();
		String quoteCoin = params.getQuoteCoin();

		CoinPairConfig config = new CoinPairConfig();
		config.setBaseCoin(baseCoin);
		config.setQuoteCoin(quoteCoin);
		config.setSymbol(symbol);

		RespWebSocketData<Object> resp = new RespWebSocketData<>();
		resp.setBaseCoin(baseCoin);
		resp.setQuoteCoin(quoteCoin);
		resp.setSymbol(symbol);
		resp.setType(params.getType());
		resp.setExchangeType(exchangeType);
		if(exchangeType == null) {
			resp.setIsDirect(false);
		}else {
			resp.setIsDirect(true);
		}
		

		Object data = null;
		if (busiType.equals(WebSocketBusiType.KLINE)) {
			
			String klineTime = params.getKlineTime();
			data = findKline(symbol, klineTime);
			resp.setKlineTime(params.getKlineTime());
			
		} else if (busiType.equals(WebSocketBusiType.TICKER)) {
			
			TickerData _data = findTicker24(symbol);
			_data.setBaseCoin(baseCoin);
			_data.setQuoteCoin(quoteCoin);
			data = _data;
			
		} else if (busiType.equals(WebSocketBusiType.TRADE_TICKER)) {
			
			data = findTradeTicker(symbol);

		} else if (busiType.equals(WebSocketBusiType.DEPTH_STEP)) {
			
			String depthStep = params.getDepthStep();
			data = findDepthStep(symbol, getDepthStep(depthStep));
			resp.setDepthStep(depthStep);
			
		}

		resp.setEvent(WebSocketEvent.REQ);
		resp.setData(data);
		resp.setMD5(MD5.GetMD5Code(JSON.toJSONString(data)));

		apiWebSocketController.pushMessageToClient(resp);

	}
	
	private String getDepthStep(String depthStep) {
		if (depthStep.equals("0")) {
			return DepthStep.STEP0.getCode();
		} else if (depthStep.equals("1")) {
			return DepthStep.STEP1.getCode();
		} else if (depthStep.equals("2")) {
			return DepthStep.STEP2.getCode();
		}

		return null;
	}


	@Override
	public ExchangeType getExchangeType() {
		return exchangeType;
	}

}
