package com.zatgo.zup.exchange.entity;

import java.math.BigDecimal;

public class CloudUserCoinPair {
    private String id;

    private String cloudUserId;

    private String pairId;

    private BigDecimal makerFeeIncrease;

    private BigDecimal takerFeeIncrease;

    private Byte status;

    private String externalExchangeType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getPairId() {
        return pairId;
    }

    public void setPairId(String pairId) {
        this.pairId = pairId == null ? null : pairId.trim();
    }

    public BigDecimal getMakerFeeIncrease() {
        return makerFeeIncrease;
    }

    public void setMakerFeeIncrease(BigDecimal makerFeeIncrease) {
        this.makerFeeIncrease = makerFeeIncrease;
    }

    public BigDecimal getTakerFeeIncrease() {
        return takerFeeIncrease;
    }

    public void setTakerFeeIncrease(BigDecimal takerFeeIncrease) {
        this.takerFeeIncrease = takerFeeIncrease;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExternalExchangeType() {
        return externalExchangeType;
    }

    public void setExternalExchangeType(String externalExchangeType) {
        this.externalExchangeType = externalExchangeType == null ? null : externalExchangeType.trim();
    }
}