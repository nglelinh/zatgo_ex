package com.zatgo.zup.exchange.strategy.bytex.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.alibaba.fastjson.annotation.JSONField;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.entity.ExOrder;

public class BytexCreateOrderReq extends BytexReq implements Serializable {

	private static final long serialVersionUID = 1149600007078138269L;

	private String side;

	private String type;

	private BigDecimal volume;

	private BigDecimal price;

	private String symbol;

	@JSONField(name = "fee_is_user_exchange_coin")
	private Boolean fee_is_user_exchange_coin;

	public BytexCreateOrderReq() {
		super();
	}
	
	public BytexCreateOrderReq(String apiKey, String secretKey, CreateOrderParams params) {
		this.side = params.getSide().getCode();
		this.type = params.getType().getCode();
		this.volume = params.getVolume();
		this.price = params.getPrice();
		this.symbol = params.getSymbol().toLowerCase();
		this.fee_is_user_exchange_coin = params.getFeeIsUserPlatformCoin();
		sign(apiKey, secretKey, this);
	}

	public BytexCreateOrderReq(String apiKey, String secretKey, ExOrder order) {
		this.side = order.getSide();
		this.type = order.getType().toString();
		this.volume = order.getVolume();
		this.price = order.getPrice();
		this.symbol = order.getCoinPairSymbol().toLowerCase();
		this.fee_is_user_exchange_coin = false;
		sign(apiKey, secretKey, this);
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Boolean getFee_is_user_exchange_coin() {
		return fee_is_user_exchange_coin;
	}

	public void setFee_is_user_exchange_coin(Boolean fee_is_user_exchange_coin) {
		this.fee_is_user_exchange_coin = fee_is_user_exchange_coin;
	}

}
