package com.zatgo.zup.exchange.strategy.huobi.model;

public class HuobiSubmitCancelResp extends HuobiResp {
	
	/**
	 * 订单 ID
	 */
	private String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
