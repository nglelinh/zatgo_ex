package com.zatgo.zup.exchange.strategy.bytex;

import java.net.URI;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.strategy.WebSocketClientService;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
import com.zatgo.zup.exchange.strategy.bytex.model.BytexWsReq;

public class BytexWebsocketPollingClient implements WebSocketClientService {

	private static final Logger log = LoggerFactory.getLogger(BytexWebsocketPollingClient.class);

	private String exchangeType;

	private WebSocketStrategyService service;
	
	private Boolean isConnect = false;
	
	private  Runnable kline;
	private  Runnable ticker;
	private  Runnable tradeTicker;
	private  Runnable depthStep;


	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public WebSocketStrategyService getService() {
		return service;
	}

	public void setService(WebSocketStrategyService service) {
		this.service = service;
	}

	public BytexWebsocketPollingClient(URI uri) {
		log.info("行情服务功能启动");

		// 订阅-k线行情
		kline = new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {

						List<String> exchangeTypes = service.getExchangeTypes();
						if (CollectionUtils.isNotEmpty(exchangeTypes)) {
							if(exchangeTypes.contains(exchangeType)) {
								service.pushKline(false);
							}
						}

						service.pushDirectKlineData();

						TimeUnit.SECONDS.sleep(5);
					} catch (InterruptedException e) {
						log.error("订阅-k线行情线程等待失败：", e);
					} catch (Exception e) {
						log.error("订阅-前24小时行情失败：", e);
					}
				}

			}
		};
		

		// 订阅-前24小时行情
		ticker = new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {

						List<String> exchangeTypes = service.getExchangeTypes();
						if (CollectionUtils.isNotEmpty(exchangeTypes)) {
							if(exchangeTypes.contains(exchangeType)) {
								service.pushTicker(false);
							}
						}

						service.pushDirectTickerData();

						TimeUnit.SECONDS.sleep(5);
					} catch (InterruptedException e) {
						log.error("订阅-前24小时行情线程等待失败：", e);
					} catch (Exception e) {
						log.error("订阅-前24小时行情失败：", e);
					}
				}

			}
		};
		

		// 订阅-实时成交信息
		tradeTicker = new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {

						List<String> exchangeTypes = service.getExchangeTypes();
						if (CollectionUtils.isNotEmpty(exchangeTypes)) {
							if(exchangeTypes.contains(exchangeType)) {
								service.pushTradeTicker(false);
							}
						}

						service.pushDirectTradeTickerData();

						TimeUnit.SECONDS.sleep(5);
					} catch (InterruptedException e) {
						log.error("订阅-实时成交信息线程等待失败：", e);
					} catch (Exception e) {
						log.error("订阅-实时成交信息失败：", e);
					}
				}

			}
		};
		

		// 订阅-币对的深度
		depthStep = new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {

						List<String> exchangeTypes = service.getExchangeTypes();
						if (CollectionUtils.isNotEmpty(exchangeTypes)) {
							if(exchangeTypes.contains(exchangeType)) {
								service.pushDepthStepData(false);
							}
						}

						service.pushDirectDepthStepData();

						TimeUnit.SECONDS.sleep(5);
					} catch (InterruptedException e) {
						log.error("订阅-深度盘口线程等待失败：", e);
					} catch (Exception e) {
						log.error("订阅-深度盘口失败：", e);
					}
				}

			}
		};
		


		log.info("行情服务功能启动完成");
	}

	@Override
	public Boolean isOpen() {
		if(isConnect) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public void connect() {
		Thread klineThread = new Thread(kline);
		klineThread.setDaemon(true);
		klineThread.start();
		
		Thread tickerThread = new Thread(ticker);
		tickerThread.setDaemon(true);
		tickerThread.start();
		
		Thread tradeTickerThread = new Thread(tradeTicker);
		tradeTickerThread.setDaemon(true);
		tradeTickerThread.start();
		
		Thread depthStepThread = new Thread(depthStep);
		depthStepThread.setDaemon(true);
		depthStepThread.start();

		isConnect = true;
	}

	@Override
	public ZUP_READYSTATE getReadyState() {
		if(isConnect) {
			return ZUP_READYSTATE.OPEN;
		}else {
			return ZUP_READYSTATE.CLOSED;
		}
	}

	@Override
	public void send(String message) {
		//BytexWsReq req = JSON.parseObject(message, BytexWsReq.class);
		//service.wsReq(req.getParams().getChannel(), params);
	}

	

}
