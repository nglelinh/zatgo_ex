package com.zatgo.zup.exchange.appapi.model;

import com.zatgo.zup.common.enumtype.ExchangeEnum.EntryOrdersType;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;

public class ExOrderType {
	
	private ExchangeSide side;
	
	private EntryOrdersType type;

	public ExchangeSide getSide() {
		return side;
	}

	public void setSide(ExchangeSide side) {
		this.side = side;
	}

	public EntryOrdersType getType() {
		return type;
	}

	public void setType(EntryOrdersType type) {
		this.type = type;
	}

}
