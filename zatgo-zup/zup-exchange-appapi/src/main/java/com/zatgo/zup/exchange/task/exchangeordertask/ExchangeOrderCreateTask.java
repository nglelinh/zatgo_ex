package com.zatgo.zup.exchange.task.exchangeordertask;

import com.zatgo.zup.common.enumtype.ExchangeEnum.OrderStatus;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.business.OrderService;
import com.zatgo.zup.exchange.entity.ExOrder;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

//@Component
public class ExchangeOrderCreateTask {

	private static final Logger log = LoggerFactory.getLogger(ExchangeOrderCreateTask.class);

	@Autowired
	private OrderService orderService;

//	@Scheduled(cron = "0 */1 * * * ?")
	public void createOrder() {

		OrderSearchParams params = new OrderSearchParams();
		params.setStatus(new Byte(OrderStatus.UNCOMMITTED.getCode()));

		Boolean pageEnd = false;
		int pageNo = 1;
		int pageSize = 1000;

		params.setPageSize(pageSize);
		while (!pageEnd) {
			params.setPageNo(pageNo);
			PageInfo<ExOrder> info = orderService.findOrderBySearchParams(params);
			List<ExOrder> orderDatas = info.getList();
			for (ExOrder order : orderDatas) {
				
				try {
					orderService.createOrder(order);
				} catch (Exception e) {
					log.error("创建交易所订单失败：", e);
				}
				
			}

			if (CollectionUtils.isEmpty(orderDatas)) {
				pageEnd = true;
			} else {
				pageNo++;
			}
		}

	}
}
