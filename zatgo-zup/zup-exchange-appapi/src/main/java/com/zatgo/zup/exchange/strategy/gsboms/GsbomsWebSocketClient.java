package com.zatgo.zup.exchange.strategy.gsboms;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.shade.com.alibaba.fastjson.JSON;
import com.gs.st.client.APIClient;
import com.gs.st.client.APIClientException;
import com.gs.st.client.IConsumer;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.exchange.entity.CoinPairConfig;
import com.zatgo.zup.exchange.strategy.WebSocketClientService;
import com.zatgo.zup.exchange.strategy.WebSocketStrategyService;
import com.zatgo.zup.exchange.strategy.gsboms.model.GsbomsSendDataReq;

public class GsbomsWebSocketClient implements WebSocketClientService {

	private static final Logger log = LoggerFactory.getLogger(GsbomsWebSocketClient.class);
	
	private String exchangeType;

	private WebSocketStrategyService service;
	
	private APIClient otherWebSockctClient;
	
	private IConsumer consumer;
	
	private String url;
	
	private int port; 
	
	private Boolean isConnect = false;


	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public WebSocketStrategyService getService() {
		return service;
	}

	public void setService(WebSocketStrategyService service) {
		this.service = service;
	}

	public GsbomsWebSocketClient(String url,int port,Integer co_no, long opor_no,String key) {
		this.url = url;
		this.port = port;
		
		otherWebSockctClient = new APIClient(co_no, opor_no);
		otherWebSockctClient.setKey(key);
		
		try {
			consumer = new IConsumer() {
				
				public void call(String topic, Object result) {
					JSONObject obj = (JSONObject)result;
					log.debug("收到消息" + exchangeType + ":" + "(Topic="+ topic + ",Api=" + obj.getString("api") + "):" + result);
					service.receiveServerMessage(((JSONObject)result).toJSONString());
				}
				
			};
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void subMarketWebSocket(boolean isDirect) {
		List<CoinPairConfig> configs = service.getMarketCoinPairConfig(isDirect);
		for (CoinPairConfig config : configs) {
			String topic = "hb." + config.getSymbol().toLowerCase() + ".86400";
			String  api = "quote.kline";
			Object resp = otherWebSockctClient.subscribe(api, topic, consumer);
		}
		
		
	}

	@Override
	public Boolean isOpen() {
		if(isConnect) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public void connect() {
		
		//otherWebSockctClient 连接
		try {
			otherWebSockctClient.connect(url, port);
			otherWebSockctClient.initToken();
			otherWebSockctClient.setCompress(true);
		} catch (APIClientException e) {
			log.error("",e);
			throw new BusinessException();
		}

		
		isConnect = true;
	}

	@Override
	public ZUP_READYSTATE getReadyState() {
		if(isConnect) {
			return ZUP_READYSTATE.OPEN;
		}else {
			return ZUP_READYSTATE.CLOSED;
		}
	}

	@Override
	public void send(String message) {
		GsbomsSendDataReq req = JSON.parseObject(message, GsbomsSendDataReq.class);
		if(req.getTopic().equals("hb") && req.getApi().equals("quote.market")) {
			// 订阅-直接模式-首页市场币对的最新价和涨幅情况
			subMarketWebSocket(true);
			
			// 订阅-本地模式-首页市场币对的最新价和涨幅情况
			subMarketWebSocket(false);
		}else {
			Object resp = otherWebSockctClient.subscribe(req.getApi(), req.getTopic(), consumer);
		}
		
		
		
	}

}
