package com.zatgo.zup.exchange.strategy.huobi.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 火币websocket请求参数
 * @author Administrator
 *
 */
public class HuobiWsReq {
	
	/**
	 * id generate by WebSocketClient
	 */
	private String id;
	
	/**
	 * 查询
	 */
	private String req;
	
	/**
	 * 订阅
	 */
	private String sub;
	
	/**
	 * 取消订阅
	 */
	private String unsub;
	
	/**
	 * 订阅延时{ 1000, 2000, 3000, 4000, 5000 }
	 */
	@JSONField(name = "freq-ms")
	private String freqMs;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReq() {
		return req;
	}

	public void setReq(String req) {
		this.req = req;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getUnsub() {
		return unsub;
	}

	public void setUnsub(String unsub) {
		this.unsub = unsub;
	}

	public String getFreqMs() {
		return freqMs;
	}

	public void setFreqMs(String freqMs) {
		this.freqMs = freqMs;
	}

}
