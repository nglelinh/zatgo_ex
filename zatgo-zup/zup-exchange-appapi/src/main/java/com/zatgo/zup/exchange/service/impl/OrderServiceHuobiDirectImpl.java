package com.zatgo.zup.exchange.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.exchange.appapi.model.CancelOrderParams;
import com.zatgo.zup.exchange.appapi.model.CreateOrderParams;
import com.zatgo.zup.exchange.appapi.model.ExOrderType;
import com.zatgo.zup.exchange.appapi.model.OrderData;
import com.zatgo.zup.exchange.appapi.model.OrderDetailParams;
import com.zatgo.zup.exchange.appapi.model.OrderSearchParams;
import com.zatgo.zup.exchange.appapi.model.SymbolData;
import com.zatgo.zup.exchange.service.CommonService;
import com.zatgo.zup.exchange.service.OrderService;
import com.zatgo.zup.exchange.strategy.huobi.HuobiService;
import com.zatgo.zup.exchange.strategy.huobi.impl.OrderServiceHuobiImpl;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiAccountsReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderData;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderPlaceReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrderReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiOrdersReq;
import com.zatgo.zup.exchange.strategy.huobi.model.HuobiSubmitCancelReq;

@Service("orderServiceImpl_huobi_direct")
public class OrderServiceHuobiDirectImpl implements OrderService {

	private static final Logger log = LoggerFactory.getLogger(OrderServiceHuobiImpl.class);

	/**
	 * 取消订单method
	 */
	private static final String SUBMIT_CANCEL_URL = "/v1/order/orders/%s/submitcancel";

	/**
	 * 用户订单信息method
	 */
	private static final String ORDER_URL = "/v1/order/orders/%s";

	/**
	 * 查询当前成交、历史成交method
	 */
	//private static final String MATCH_RESULTS_URL = "/v1/order/orders/%s/matchresults";

	/**
	 * 查询当前委托、历史委托
	 */
	private static final String ORDERS_URL = "/v1/order/orders";

	@Value("${exchange.huobi.http.url:}")
	private String huobiUrl;

	@Autowired
	private HuobiService huobiService;

	@Autowired
	private CommonService commonService;

	@Override
	public OrderData createOrder(CreateOrderParams params, String userId) {

		HuobiAccountsReq accReq = null;
		try {
			accReq = new HuobiAccountsReq(params.getApiKey(), params.getSecretKey(), "GET", huobiUrl,
					"/v1/account/accounts");
		} catch (Exception e) {
			log.error("获取火币账户参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.HUOBI_CREATE_ORDER_FAIL);
		}

		HuobiOrderPlaceReq orderReq = new HuobiOrderPlaceReq();
		orderReq.setAccessKeyId(params.getApiKey());
		orderReq.setAmount(params.getVolume());
		orderReq.setMethod("/v1/order/orders/place");
		orderReq.setPrice(params.getPrice());
		orderReq.setReqMethod("POST");
		orderReq.setSecretKey(params.getSecretKey());
		orderReq.setSource("api");
		orderReq.setSymbol(params.getSymbol());
		orderReq.setType(huobiService.getHuobiOrderType(params.getSide().toString(), params.getType().toString()));

		String orderId = huobiService.createHuobiOrder(huobiUrl, accReq, "spot", orderReq);

		OrderData data = new OrderData();
		data.setBaseCoin(params.getBaseCoin());
		data.setCreateDate(new Date());
		data.setOrderId(orderId);
		data.setPrice(params.getPrice());
		data.setQuoteCoin(params.getQuoteCoin());
		data.setSide(params.getSide());
		data.setSymbol(params.getSymbol());
		data.setType(params.getType());
		data.setVolume(params.getVolume());

		return data;
	}

	@Override
	public OrderData cancelOrder(CancelOrderParams params, String userId) {
		HuobiSubmitCancelReq req;
		try {
			String method = String.format(SUBMIT_CANCEL_URL, params.getOrderId());
			req = new HuobiSubmitCancelReq(params.getApiKey(), params.getSecretKey(), "POST", huobiUrl, method);
		} catch (Exception e) {
			log.error("获取火币取消订单参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		String orderId = huobiService.cancelHuobiOrder(huobiUrl, req);

		OrderData data = new OrderData();
		data.setOrderId(orderId);

		return data;
	}

	@Override
	public PageInfo<OrderData> findOrderByPage(OrderSearchParams params) {
		HuobiOrdersReq req = new HuobiOrdersReq();
		req.setAccessKeyId(params.getApiKey());
		req.setDirect(params.getDirect());
		req.setFrom(params.getOrderId());
		req.setMethod(ORDERS_URL);
		req.setReqMethod("GET");
		req.setSecretKey(params.getSecretKey());

		String pageSize = params.getPageSize() == null ? "10" : params.getPageSize().toString();
		req.setSize(pageSize);

		String symbol = params.getSymbol();
		req.setSymbol(symbol);
		req.setStates(String.join(",", huobiService.getHuobiOrderStates(params.getOrderStatuss())));

		List<HuobiOrderData> datas = huobiService.getOrdersList(huobiUrl, req);
		List<OrderData> list = null;
		if (CollectionUtils.isNotEmpty(datas)) {

			list = new ArrayList<OrderData>();

			for (HuobiOrderData huobiOrderData : datas) {
				OrderData orderData = getOrderData(huobiOrderData);
				list.add(orderData);
			}

		}

		PageInfo<OrderData> info = new PageInfo<OrderData>(params.getPageNo(), params.getPageSize(), 0L, list);
		return info;
	}

	private OrderData getOrderData(HuobiOrderData huobiOrderData) {
		OrderData orderData = new OrderData();
		orderData.setAvgPrice(huobiOrderData.getPrice());
		SymbolData symbolData = commonService.getSymbolInfo(huobiOrderData.getSymbol());
		String baseCoin = symbolData.getBaseCoin();
		String quoteCoin = symbolData.getQuoteCoin();
		orderData.setBaseCoin(baseCoin);
		orderData.setDealVolume(huobiOrderData.getFieldAmount());
		orderData.setFeeIsUserPlatformCoin(false);
		orderData.setOrderId(huobiOrderData.getId().toString());
		orderData.setPrice(huobiOrderData.getPrice());
		orderData.setQuoteCoin(quoteCoin);
		orderData.setRemainVolume(huobiOrderData.getAmount().subtract(huobiOrderData.getFieldAmount()));

		ExOrderType orderType = huobiService.getOrderSideType(huobiOrderData.getType());

		orderData.setSide(orderType.getSide());
		orderData.setStatus(huobiService.getOrderStatus(huobiOrderData.getState()));
		orderData.setSymbol(huobiOrderData.getSymbol());
		orderData.setType(orderType.getType());
		orderData.setVolume(huobiOrderData.getAmount());

		return orderData;
	}

	@Override
	public OrderData getOrderByOrderId(OrderDetailParams params) {
		HuobiOrderReq req;
		try {
			req = new HuobiOrderReq(huobiUrl, params.getApiKey(), params.getSecretKey(), "GET",
					String.format(ORDER_URL, params.getOrderId()));
		} catch (Exception e) {
			log.error("获取火币订单信息参数失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		HuobiOrderData data = huobiService.getOrderData(huobiUrl, req);
		OrderData orderData = getOrderData(data);
		return orderData;
	}

}
