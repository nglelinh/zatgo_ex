package com.zatgo.zup.exchange.strategy.huobi.util;

import org.apache.commons.lang3.StringUtils;

import com.zatgo.zup.common.enumtype.ExchangeEnum.DepthStep;
import com.zatgo.zup.common.enumtype.ExchangeEnum.KlineTime;
import com.zatgo.zup.common.enumtype.ExchangeEnum.WebSocketBusiType;

/**
 * 火币topic util
 * 
 * @author Administrator
 *
 */
public class HuobiTopicUtil {

	/**
	 * KLine K线 数据，包含单位时间区间的开盘价、收盘价、最高价、最低价、成交量、成交额、成交笔数等数据 $period 可选值：{ 1min,
	 * 5min, 15min, 30min, 60min, 4hour,1day, 1mon, 1week, 1year }
	 */
	private static final String TOPIC_KLINE = "market.%s.kline.%s";

	/**
	 * Market Depth 盘口深度，按照不同 step 聚合的买一、买二、买三等和卖一、卖二、卖三等数据 $type 可选值：{ step0,
	 * step1, step2, step3, step4, step5, percent10 } （合并深度0-5）；step0时，不合并深度
	 */
	private static final String TOPIC_MARKET_DEPTH = "market.%s.depth.%s";

	/**
	 * Trade Detail 成交记录，包含成交价格、成交量、成交方向等信息
	 */
	private static final String TOPIC_TRADE_DETAIL = "market.%s.trade.detail";

	/**
	 * Market Detail 最近24小时成交量、成交额、开盘价、收盘价、最高价、最低价、成交笔数等
	 */
	private static final String TOPIC_MARKET_DETAIL = "market.%s.detail";

	/**
	 * Market Tickers 所有对外公开交易对的 日K线、最近24小时成交量等信息
	 */
	private static final String TOPIC_MARKET_TICKERS = "market.tickers";

	/**
	 * Accounts 订阅账户资产变更
	 */
	private static final String TOPIC_ACCOUNTS = "accounts";

	/**
	 * Orders 订阅订单变更
	 */
	private static final String TOPIC_ORDERS = "orders.%s";

	/**
	 * Accounts list 请求账户资产信息
	 */
	private static final String TOPIC_ACCOUNTS_LIST = "accounts.list";

	/**
	 * Order list 请求订单信息
	 */
	private static final String TOPIC_ORDER_LIST = "orders.list";

	/**
	 * order detail 请求某个订单明细
	 */
	private static final String TOPIC_ORDER_DETAIL = "orders.detail";

	/**
	 * 1min
	 */
	private static final String PERIOD_1_MIN = "1min";

	/**
	 * 5min
	 */
	private static final String PERIOD_5_MIN = "5min";

	/**
	 * 15min
	 */
	private static final String PERIOD_15_MIN = "15min";

	/**
	 * 30min
	 */
	private static final String PERIOD_30_MIN = "30min";

	/**
	 * 60min
	 */
	private static final String PERIOD_60_MIN = "60min";

	/**
	 * 4hour
	 */
	private static final String PERIOD_4_HOUR = "4hour";

	/**
	 * 1day
	 */
	private static final String PERIOD_1_DAY = "1day";

	/**
	 * 1week
	 */
	private static final String PERIOD_1_WEEK = "1week";

	/**
	 * 1mon
	 */
	private static final String PERIOD_1_MON = "1mon";

	/**
	 * 1year
	 */
	private static final String PERIOD_1_YEAR = "1year";

	private static final String MARKET_DEPTH_0 = "step0";
	private static final String MARKET_DEPTH_1 = "step1";
	private static final String MARKET_DEPTH_2 = "step2";
	private static final String MARKET_DEPTH_3 = "step3";
	private static final String MARKET_DEPTH_4 = "step4";
	private static final String MARKET_DEPTH_5 = "step5";
	private static final String MARKET_PERCENT_10 = "percent10";

	/**
	 * 获取火币topic
	 * 
	 * @param socketBusiType
	 *            websocet订阅接口业务类型
	 * @param symbol
	 *            币对
	 * @param period
	 *            kline周期
	 * @param type
	 *            盘口深度
	 * @return
	 */
	public static String getTopic(WebSocketBusiType socketBusiType, String symbol, String period, String type) {
		if (socketBusiType.equals(WebSocketBusiType.KLINE)) {
			return getTopicKline(symbol, period);
		} else if (socketBusiType.equals(WebSocketBusiType.TICKER)) {
			return getTopicMarketDetail(symbol);
		} else if (socketBusiType.equals(WebSocketBusiType.TRADE_TICKER)) {
			return getTopicTradeDetail(symbol);
		} else if (socketBusiType.equals(WebSocketBusiType.DEPTH_STEP)) {
			return getTopicMarketDepth(symbol, type);
		} else if (socketBusiType.equals(WebSocketBusiType.MARKET)) {
			return getTopicMarketTickers();
		} else {
			return null;
		}
	}

	/**
	 * 获取火币kline topic
	 * 
	 * @param symbol
	 *            币对
	 * @param period
	 *            kline周期
	 * @return
	 */
	public static String getTopicKline(String symbol, String period) {
		if (period.equals(KlineTime.MIN.getCode())) {
			period = PERIOD_1_MIN;
		} else if (period.equals(KlineTime.MIN5.getCode())) {
			period = PERIOD_5_MIN;
		} else if (period.equals(KlineTime.MIN15.getCode())) {
			period = PERIOD_15_MIN;
		} else if (period.equals(KlineTime.MIN30.getCode())) {
			period = PERIOD_30_MIN;
		} else if (period.equals(KlineTime.MIN60.getCode())) {
			period = PERIOD_60_MIN;
		} else if (period.equals(KlineTime.DAY.getCode())) {
			period = PERIOD_1_DAY;
		} else if (period.equals(KlineTime.WEEK.getCode())) {
			period = PERIOD_1_WEEK;
		} else if (period.equals(KlineTime.MONTH.getCode())) {
			period = PERIOD_1_MON;
		}
		return String.format(TOPIC_KLINE, symbol, period);
	}
	
	public static String getKlineTime(String period) {
		if (period.equals(PERIOD_1_MIN)) {
			period = KlineTime.MIN.getCode();
		} else if (period.equals(PERIOD_5_MIN)) {
			period = KlineTime.MIN5.getCode();
		} else if (period.equals(PERIOD_15_MIN)) {
			period = KlineTime.MIN15.getCode();
		} else if (period.equals(PERIOD_30_MIN)) {
			period = KlineTime.MIN30.getCode();
		} else if (period.equals(PERIOD_60_MIN)) {
			period = KlineTime.MIN60.getCode();
		} else if (period.equals(PERIOD_1_DAY)) {
			period = KlineTime.DAY.getCode();
		} else if (period.equals(PERIOD_1_WEEK)) {
			period = KlineTime.WEEK.getCode();
		} else if (period.equals(PERIOD_1_MON)) {
			period = KlineTime.MONTH.getCode();
		}
		
		return period;
	}

	/**
	 * 获取火币Market Depth topic
	 * 
	 * @param symbol
	 *            币对
	 * @param type
	 *            盘口深度
	 * @return
	 */
	public static String getTopicMarketDepth(String symbol, String type) {
		
		if(StringUtils.isNotEmpty(type)) {
			type = DepthStep.getEnumByType("step" + type).getCode();
		}
		
		return String.format(TOPIC_MARKET_DEPTH, symbol, type);
	}

	/**
	 * 获取火币Trade Detail topic
	 * 
	 * @param symbol
	 *            币对
	 * @return
	 */
	public static String getTopicTradeDetail(String symbol) {
		return String.format(TOPIC_TRADE_DETAIL, symbol);
	}

	/**
	 * 获取火币Market Detail topic
	 * 
	 * @param symbol
	 *            币对
	 * @return
	 */
	public static String getTopicMarketDetail(String symbol) {
		return String.format(TOPIC_MARKET_DETAIL, symbol);
	}

	/**
	 * 获取火币Market Tickers topic
	 * 
	 * @return
	 */
	public static String getTopicMarketTickers() {
		return TOPIC_MARKET_TICKERS;
	}

	/**
	 * 获取火币Accounts topic
	 * 
	 * @return
	 */
	public static String getTopicAccounts() {
		return TOPIC_ACCOUNTS;
	}

	/**
	 * 获取火币Orders topic
	 * 
	 * @param symbol
	 *            币对
	 * @return
	 */
	public static String getTopicOrders(String symbol) {
		return String.format(TOPIC_ORDERS, symbol);
	}

	/**
	 * 获取火币Accounts list topic
	 * 
	 * @return
	 */
	public static String getTopicAccountsList() {
		return TOPIC_ACCOUNTS_LIST;
	}

	/**
	 * 获取火币Order list topic
	 * 
	 * @return
	 */
	public static String getTopicOrderList() {
		return TOPIC_ORDER_LIST;
	}

	/**
	 * 获取火币order detail topic
	 * 
	 * @return
	 */
	public static String getTopicOrderDetail() {
		return TOPIC_ORDER_DETAIL;
	}

}
