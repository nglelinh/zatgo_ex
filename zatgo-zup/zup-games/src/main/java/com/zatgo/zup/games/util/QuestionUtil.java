package com.zatgo.zup.games.util;

/**
 * Created by 46041 on 2019/2/12.
 */
public class QuestionUtil {


    /**
     *  获取x与y之间的随机数
     * @param x
     * @param y
     * @return
     */
    public static Long getRandomBetweenXAndY(Long x, Long y){
        if (x == null || y == null){
            return 0l;
        }
        if (x.longValue() == y.longValue()){
            return x;
        }
        //必须保证y比x大
        if (x.longValue() > y.longValue()){
            long type = x;
            x = y;
            y = type;
        }
        return (long) (Math.random() * (y - x) + x);
    }




}
