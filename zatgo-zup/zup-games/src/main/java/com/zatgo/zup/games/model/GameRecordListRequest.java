package com.zatgo.zup.games.model;

/**
 * Created by 46041 on 2019/1/10.
 */
public class GameRecordListRequest {

    private String gameId;

    private Integer pageNo;

    private Integer pageSize;


    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
