package com.zatgo.zup.games.init;

import com.zatgo.zup.games.service.IGameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ApplicationStartup{
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationStartup.class);

	@Autowired
	private IGameService gameService;
	
	@PostConstruct
	public void startup() {

		gameService.initCache();


	}

}
