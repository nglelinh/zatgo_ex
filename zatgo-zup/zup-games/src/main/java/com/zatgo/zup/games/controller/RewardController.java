package com.zatgo.zup.games.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.GetRewarRequest;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.ZhuanpanRunModel;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.games.cache.GamePrize;
import com.zatgo.zup.games.entity.Game;
import com.zatgo.zup.games.model.ZhuanpanRunData;
import com.zatgo.zup.games.service.IZhuanpanService;
import com.zatgo.zup.games.service.RewardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2019/1/11.
 */

@Api(value = "/game/reward",description = "游戏奖励",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/game/reward")
public class RewardController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(RewardController.class);

    @Autowired
    private RewardService rewardService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private IZhuanpanService zhuanpanService;



    @ApiOperation(value = "获取游戏奖励")
    @RequestMapping(value = "/getReward",method = RequestMethod.POST)
    @ResponseBody
    public ResponseData<ZhuanpanRunModel> getRewar(@RequestBody GetRewarRequest request){
        String gameId = request.getGameId();
        String lockKey = RedisKeyConstants.GAME_REWARD_LOCK_PRE + gameId;
        redisLockUtils.lock(lockKey);
        try{
            ZhuanpanRunData rewar = rewardService.getRewar(request);
            return BusinessResponseFactory.createSuccess(JSONObject.parseObject(JSONObject.toJSONString(rewar), new TypeReference<ZhuanpanRunModel>(){}));
        } catch (BusinessException e){
            logger.error("", e);
            return BusinessResponseFactory.createBusinessError(e.getCode());
        } finally {
            redisLockUtils.releaseLock(lockKey);
        }
    }


    @ApiOperation(value = "增加游戏永久免费次数(内部调用)")
    @RequestMapping(value = "/addGameFreeNum",method = RequestMethod.POST)
    @ResponseBody
    public ResponseData<ZhuanpanRunModel> addGameFreeNum(@RequestParam("num") Integer num,
                                                         @RequestParam("gameId") String gameId,
                                                         @RequestParam("userId") String userId){
        zhuanpanService.addGameFreeNum(num, gameId, userId);
        return BusinessResponseFactory.createSuccess(null);
    }
}
