package com.zatgo.zup.games.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * Created by 46041 on 2019/1/10.
 */
public class GameRecordData {

    @ApiModelProperty(value="记录ID")
    private String recordId;
    @ApiModelProperty(value="游戏ID")
    private String gameId;
    @ApiModelProperty(value="用户ID")
    private String userId;
    @ApiModelProperty(value="参与游戏的方式：1-免费参与，2-付费参与")
    private Byte feeType;
    @ApiModelProperty(value="付费币种，如: ZAT，BTC等")
    private String feeCoin;
    @ApiModelProperty(value="付费数量")
    private String feeAmount;
    @ApiModelProperty(value="是否获胜，0-否，1-是")
    private Byte isWin;
    @ApiModelProperty(value="获胜奖励类型：1-ZAT，2-算力，3-红包，4-实物")
    private Byte prizeType;
    @ApiModelProperty(value="获胜奖励内容")
    private String prizeInfo;
    @ApiModelProperty(value="奖励发放状态：0-未发放，1-发放中，2-已发放")
    private Byte prizeStatus;
    @ApiModelProperty(value="奖励币种类型")
    private String prizeCoinType;
    @ApiModelProperty(value="")
    private Date recordTime;
    @ApiModelProperty(value="别名")
    private String aliasName;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Byte getFeeType() {
        return feeType;
    }

    public void setFeeType(Byte feeType) {
        this.feeType = feeType;
    }

    public String getFeeCoin() {
        return feeCoin;
    }

    public void setFeeCoin(String feeCoin) {
        this.feeCoin = feeCoin;
    }

    public String getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Byte getIsWin() {
        return isWin;
    }

    public void setIsWin(Byte isWin) {
        this.isWin = isWin;
    }

    public Byte getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(Byte prizeType) {
        this.prizeType = prizeType;
    }

    public String getPrizeInfo() {
        return prizeInfo;
    }

    public void setPrizeInfo(String prizeInfo) {
        this.prizeInfo = prizeInfo;
    }

    public Byte getPrizeStatus() {
        return prizeStatus;
    }

    public void setPrizeStatus(Byte prizeStatus) {
        this.prizeStatus = prizeStatus;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public String getPrizeCoinType() {
        return prizeCoinType;
    }

    public void setPrizeCoinType(String prizeCoinType) {
        this.prizeCoinType = prizeCoinType;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }
}
