package com.zatgo.zup.games.config;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserExtModel;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.games.remoteservice.UserRemoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * Created by 46041 on 2019/1/22.
 */

@Component
public class BlackList {

    private static final Logger logger = LoggerFactory.getLogger(BlackList.class);


    @Autowired
    private UserRemoteService userRemoteService;

    @Autowired
    private RedisUtils redisUtils;


    public BigDecimal getGameRate(String userId){
        return getBlackListInfo(userId).getPrizeRateFactor();
    }



    private UserExtModel getBlackListInfo(String userId){
        UserExtModel userExtModel = null;
        try{
            String key = getKey(userId);
            if (redisUtils.hasKey(key)){
                userExtModel = redisUtils.get(key, UserExtModel.class);
            }

            if (userExtModel == null){
                ResponseData<UserExtModel> res = userRemoteService.getBlackList(userId);
                if (res != null && res.isSuccessful()){
                    userExtModel = res.getData();
                    redisUtils.put(key, userExtModel, 12l, TimeUnit.HOURS);
                }
            }
        } catch (Exception e){
            logger.error("", e);
            userExtModel = new UserExtModel();
        }
        return userExtModel;
    }


    public String getKey(String userId){
        return RedisKeyConstants.GAME_BLACK_LIST_PRE + userId;
    }
}
