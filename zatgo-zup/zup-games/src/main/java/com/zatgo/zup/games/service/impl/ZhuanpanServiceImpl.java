package com.zatgo.zup.games.service.impl;

import com.github.pagehelper.*;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.GameEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.*;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.games.cache.*;
import com.zatgo.zup.games.entity.GameRecord;
import com.zatgo.zup.games.mapper.GameMapper;
import com.zatgo.zup.games.mapper.GameParamsMapper;
import com.zatgo.zup.games.mapper.GameRecordMapper;
import com.zatgo.zup.games.method.RateMethod;
import com.zatgo.zup.games.model.GameRecordData;
import com.zatgo.zup.games.model.GameRecordListRequest;
import com.zatgo.zup.games.model.ZhuanpanLoadData;
import com.zatgo.zup.games.model.ZhuanpanRunData;
import com.zatgo.zup.games.remoteservice.CouponsRemoveService;
import com.zatgo.zup.games.remoteservice.MiningRemoteService;
import com.zatgo.zup.games.remoteservice.PayRemoteService;
import com.zatgo.zup.games.remoteservice.UserRemoteService;
import com.zatgo.zup.games.service.IZhuanpanService;
import com.zatgo.zup.games.service.RewardService;
import com.zatgo.zup.games.util.GameKeyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by chen on 2018/12/19.
 */
@Service
public class ZhuanpanServiceImpl implements IZhuanpanService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisLockUtils redisLockUtils;

    @Resource
    private GameMapper gameMapper;

    @Resource
    private GameParamsMapper gameParamsMapper;

    @Resource
    private GameRecordMapper gameRecordMapper;

    @Resource
    private PayRemoteService payRemoteService;

    @Resource
    private UserRemoteService userRemoteService;

    @Resource
    private MiningRemoteService miningRemoteService;

    @Autowired
    private RewardService rewardService;

    @Autowired
    private CouponsRemoveService couponsRemoveService;

    @Override
    public ZhuanpanLoadData load(AuthUserInfo user, String gameId) {

        if(user==null)
            throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);

        GameConfig gameConfig = GameCache.getGameConfig(gameId);

        if(gameConfig==null)
            throw new BusinessException(BusinessExceptionCode.GAME_IS_NOT_EXIST);


        ZhuanpanLoadData data = new ZhuanpanLoadData();
        data.setGameFee(gameConfig.getGameFee());
        data.setGamePrize(gameConfig.getGamePrize());

        //获取当日可免费次数

        data.setTodayFreeTimes(getFreeCount(gameId, user, gameConfig).intValue());

        return data;
    }

    @Transactional
    @Override
    public ZhuanpanRunData run(AuthUserInfo user, String gameId,String coin) {

        if(user==null)
            throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);

        GameConfig gameConfig = GameCache.getGameConfig(gameId);

        if(gameConfig==null)
            throw new BusinessException(BusinessExceptionCode.GAME_IS_NOT_EXIST);

        GameFee fee = getGameFee(gameConfig.getGameFee(),coin);
        if (fee == null)
            throw new BusinessException(BusinessExceptionCode.COIN_PAIR_CONFIG_CLOSE);

        redisLockUtils.lock(RedisKeyConstants.GAME_RUN_LOCK_PRE+user.getUserId());

        try{
            //获取今天的最后一秒
            Date nowTime = Calendar.getInstance().getTime();
            Date expireTime = DateTimeUtils.endOfTodDay(nowTime);

            //是否免费
            long freeTimes = Integer.parseInt(gameConfig.getFreeTimes());
            boolean isFree = false;

            if(freeTimes==-1L){
                //免费游戏
                isFree = true;
            }else{
                String key = getGameRunTimesKey(gameId,user.getUserId());
                String freeKey = GameKeyUtil.getGameRunFreeTimesKey(gameId,user.getUserId());
                //永久免费次数
                long free = redisTemplate.opsForValue().increment(freeKey, 0);
                if (free < 0)
                    free = 0;
                //今天使用次数
                long playTimes = redisTemplate.opsForValue().increment(key,0);

                if (playTimes >= freeTimes && free > 0){
                    free = redisTemplate.opsForValue().increment(freeKey, -1);
                } else {
                    playTimes = redisTemplate.opsForValue().increment(key, 1);
                    redisTemplate.expireAt(key,expireTime);
                }
                freeTimes = freeTimes - playTimes + free;
                isFree = freeTimes >= 0;
                if (!isFree)
                    freeTimes = 0;





//                if(!redisTemplate.hasKey(key)){
//                    isFree = true;
//                    redisTemplate.opsForValue().increment(key,1);
//                    redisTemplate.expireAt(key,expireTime);
//                    freeTimes += free;
//                    freeTimes--;
//                }else{
//                    freeTimes = freeTimes-playTimes;
//                    if(freeTimes>=0){
//                        //免费
//                        isFree = true;
//                        freeTimes = freeTimes + free;
//                    }else{
//                        //每日免费的扣完了，扣永久免费的
//                        free = redisTemplate.opsForValue().increment(freeKey, -1);
//                        freeTimes = freeTimes + free;
//                        if (freeTimes >=0){
//                            isFree = true;
//                        } else {
//                            freeTimes=0;
//                        }
//                    }
//                }


            }



            //获取平台游戏账户，用于游戏中心扣费和发奖计算
            UserData gameManageUserData = userRemoteService.getManageUserId(user.getUserId(), BusinessEnum.CloudManageUserType.GAME.getType().toString()).getData();

            //生成本次游戏记录ID
            String recordId = UUIDUtils.getUuid();

            //游戏费用
            if(!(new BigDecimal(fee.getFee()).compareTo(BigDecimal.ZERO) == 1)){
                //免费
                isFree = true;
            }


            //扣费
            if(!isFree){
                //id加1表示支付扣费
                doGameCharging(user,gameManageUserData,fee,recordId+1);
            }


            //更新缓存资金池
//            Long totalAmount = Long.parseLong(fee.getFee());
//            String totalAmountKey = getGameTotalAmountKey(gameId,coin);

//            if(isFree){
//                //免费游戏，不改动资金池
//                redisTemplate.opsForValue().increment(totalAmountKey,0);
//            }else{
//                //付费游戏，增加资金池
//                redisTemplate.opsForValue().increment(totalAmountKey,totalAmount);
//            }

            /**
             * 计算抽奖结果，从奖项高到低开始判断
             *
             * 奖品为ZAT的均按收入奖金池触发，中奖后奖金池减少
             * 奖品为算力的按游戏总次数触发，中奖后次数清空
             */

            GetRewarRequest request = new GetRewarRequest();
            request.setFree(isFree);
            request.setCoinType(coin);
            request.setGameId(gameId);
            request.setUserId(user.getUserId());
            ZhuanpanRunData data = rewardService.getRewar(request);
            data.setTodayFreeTimes(freeTimes);
            if(GameEnum.GameResult.WIN.getCode().equals(data.getResult())){
                //发奖,ID加2表示支付奖励
                doGamePrizeSend(user,gameManageUserData,data.getPrize(),recordId+2);
            }

            //保存记录
            GameRecord record = new GameRecord();
            record.setRecordId(recordId);
            record.setGameId(gameId);
            record.setUserId(user.getUserId());
            if(isFree){
                record.setFeeType((byte)1);
            }else{
                record.setFeeType((byte)2);
                record.setFeeCoin(fee.getCoin());
                record.setFeeAmount(fee.getFee());
            }
            if(GameEnum.GameResult.WIN.getCode().equals(data.getResult())){
                //中奖
                record.setIsWin((byte)1);
                record.setPrizeInfo(data.getPrize().getValue());
                record.setPrizeType(Byte.parseByte(data.getPrize().getPrize()));
                record.setPrizeStatus(GameEnum.GamePrizeStatus.SEND_DONE.getCode());
                record.setPrizeCoinType(data.getPrize().getCoinType());
                record.setAliasName(data.getPrize().getAliasName());
                data.setResult(GameEnum.GameResult.WIN.getCode());
            }else{
                //未中奖
                record.setIsWin((byte)0);
            }
            record.setRecordTime(nowTime);
            gameRecordMapper.insertSelective(record);


            return data;
        }catch(Exception e){
            logger.error("",e);
            throw e;
        }finally {
            redisLockUtils.releaseLock(RedisKeyConstants.GAME_RUN_LOCK_PRE+user.getUserId());
        }
    }

    @Override
    public PageInfo<GameRecordData> recordList(GameRecordListRequest request, String userId) {
        PageHelper.startPage(request.getPageNo(), request.getPageSize());
        PageInfo<GameRecordData> pageInfo = new PageInfo<>(gameRecordMapper.selectGameRecordList(request.getGameId(), userId));
        return pageInfo;
    }

    @Override
    public void addGameFreeNum(Integer num, String gameId, String userId) {
        String key = GameKeyUtil.getGameRunFreeTimesKey(gameId, userId);
        redisTemplate.opsForValue().increment(key, num.longValue());
    }


    /**
     * 扣费
     * @param userData
     * @param gameManageUserData
     * @param gameFee
     */
    private void doGameCharging(AuthUserInfo userData, UserData gameManageUserData,GameFee gameFee,String recordId) {
        String fee = gameFee.getFee();
        if (StringUtils.isEmpty(fee) || "0".equals(fee))
            return;
        PayParams params = new PayParams();
        params.setNetworkType(gameFee.getNetworkType());
        params.setCoinType(gameFee.getCoin());
        params.setAmount(new BigDecimal(fee));
        params.setReceiptUserId(gameManageUserData.getUserId());
        params.setReceiptUserName(gameManageUserData.getUserName());
        params.setPayUserName(userData.getUserName());
        params.setCheckoutOrderId(recordId);
        ResponseData<String> result = payRemoteService.payByUser(userData.getUserId(),params);
        if(!result.isSuccessful()){
            throw new BusinessException(result.getCode());
        }
    }

    /**
     * 发奖
     * @param user
     * @param gameManageUserData
     * @param prize
     */
    private void doGamePrizeSend(AuthUserInfo user,UserData gameManageUserData, GamePrize prize,String recordId) {
        /**
         * 判断奖品，ZAT则调用支付；算力则调用挖矿
         */
        if(GameEnum.GamePrizeType.COIN.getCode().equalsIgnoreCase(prize.getPrize())){
            //奖励代币，从游戏中心账户支付到用户账户
            PayParams params = new PayParams();
            params.setNetworkType(prize.getCoinNetworkType());
            params.setCoinType(prize.getCoinType());
            params.setAmount(new BigDecimal(prize.getValue()));
            params.setReceiptUserId(user.getUserId());
            params.setReceiptUserName(user.getUserName());
            params.setPayUserName(gameManageUserData.getUserName());
            params.setCheckoutOrderId(recordId);
            ResponseData<String> result = payRemoteService.payByUser(gameManageUserData.getUserId(),params);
            if(!result.isSuccessful()){
                throw new BusinessException(result.getCode());
            }

        }else if(GameEnum.GamePrizeType.SUANLI.getCode().equalsIgnoreCase(prize.getPrize())){
            //奖励算力
            AddComputePowerParams params = new AddComputePowerParams();
            params.setUserId(user.getUserId());
            params.setComputeNum(Long.parseLong(prize.getValue()));
            params.setComputePowerSourceType(BusinessEnum.ComputePowerSourceType.GAME);
            params.setComputePowerType(BusinessEnum.ComputePowerType.PERMANENT);

            Calendar cal = Calendar.getInstance();
            params.setValidStartDate(cal.getTime());
            cal.add(Calendar.YEAR,10);
            params.setValidEndDate(cal.getTime());

            ResponseData result = miningRemoteService.addComputePower(params);
            if(!result.isSuccessful()){
                throw new BusinessException(result.getCode());
            }
        }else if (GameEnum.GamePrizeType.COUPON.getCode().equalsIgnoreCase(prize.getPrize())){
            couponsRemoveService.triggerGetCoupons(user.getCloudUserId(), user.getUserId(), prize.getTriggerBusinessType(), prize.getTriggerBusinessCode());
        }

    }

    private GameFee getGameFee(List<GameFee> gameFee,String coin) {
        for(GameFee fee:gameFee){
            if(coin.equalsIgnoreCase(fee.getCoin())){
                return fee;
            }
        }
        return null;
    }


    private Long getFreeCount(String gameId, AuthUserInfo user, GameConfig gameConfig){
        String key = getGameRunTimesKey(gameId,user.getUserId());
        long freeTimes = Long.valueOf(gameConfig.getFreeTimes());
        if(redisTemplate.hasKey(key)){
            long times = redisTemplate.opsForValue().increment(key,0);
            freeTimes = freeTimes-times;
        }

        key = GameKeyUtil.getGameRunFreeTimesKey(gameId,user.getUserId());
        if(redisTemplate.hasKey(key)){
            long times = redisTemplate.opsForValue().increment(key,0);
            freeTimes = freeTimes+times;
        }

        if(freeTimes<0)
            freeTimes = 0;

        return freeTimes;
    }

    private String getGameRunTimesKey(String gameId,String userId){
        Date date = Calendar.getInstance().getTime();
        String dateStr = DateTimeUtils.parseDateToString(date,DateTimeUtils.PATTEN_YYYY_MM_DD);
        String key = new StringBuilder(RedisKeyConstants.GAME_RUN_TIMES_PRE).append(gameId)
                        .append("_")
                        .append(dateStr)
                        .append("_")
                        .append(userId).toString();
        return key;
    }

    private String getGameTotalTimesKey(String gameId){
        String key = RedisKeyConstants.GAME_TOTAL_TIMES_PRE+gameId;
        return key;
    }

    private String getGameTotalAmountKey(String gameId,String coin){
        String key = new StringBuilder(RedisKeyConstants.GAME_TOTAL_AMOUNT_PRE).append(gameId)
                .append("_")
                .append(coin).toString();

        return key;
    }


}
