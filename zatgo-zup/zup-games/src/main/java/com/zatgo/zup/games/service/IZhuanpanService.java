package com.zatgo.zup.games.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.games.model.GameRecordData;
import com.zatgo.zup.games.model.GameRecordListRequest;
import com.zatgo.zup.games.model.ZhuanpanLoadData;
import com.zatgo.zup.games.model.ZhuanpanRunData;

/**
 * 大转盘
 */
public interface IZhuanpanService {

	/**
	 * 加载游戏基本信息
	 * @param user
	 * @param gameId
     * @return
     */
	ZhuanpanLoadData load(AuthUserInfo user, String gameId);

	/**
	 * 启动
	 * @param user
	 * @param gameId
	 * @param coin
     * @return
     */
	ZhuanpanRunData run(AuthUserInfo user,String gameId,String coin);

	/**
	 * 用户游戏记录列表
	 * @param request
	 * @param userId
	 * @return
	 */
    PageInfo<GameRecordData> recordList(GameRecordListRequest request, String userId);

	/**
	 * 增加游戏永久免费次数
	 * @param num
	 * @param gameId
	 * @param userId
	 */
    void addGameFreeNum(Integer num, String gameId, String userId);
}
