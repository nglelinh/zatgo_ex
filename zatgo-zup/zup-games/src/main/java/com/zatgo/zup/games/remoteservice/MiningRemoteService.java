package com.zatgo.zup.games.remoteservice;

import com.zatgo.zup.common.model.AddComputePowerParams;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("zup-mining")
public interface MiningRemoteService {

    /**
     * 用户获得算力(内部调用)
     */
    @PostMapping("/mining/addComputePower")
    ResponseData addComputePower(@RequestBody AddComputePowerParams params);

}
