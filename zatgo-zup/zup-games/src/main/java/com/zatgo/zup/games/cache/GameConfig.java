package com.zatgo.zup.games.cache;

import com.zatgo.zup.games.entity.Game;

import java.util.List;

/**
 * Created by chen on 2018/12/20.
 */
public class GameConfig extends Game{

    private String freeTimes;

    private List<GameFee> gameFee;

    private List<PrizeRule> prizeRule;

    private List<GamePrize> gamePrize;

    private String gameRule;

    public String getFreeTimes() {
        return freeTimes;
    }

    public void setFreeTimes(String freeTimes) {
        this.freeTimes = freeTimes;
    }

    public List<GameFee> getGameFee() {
        return gameFee;
    }

    public void setGameFee(List<GameFee> gameFee) {
        this.gameFee = gameFee;
    }

    public List<PrizeRule> getPrizeRule() {
        return prizeRule;
    }

    public void setPrizeRule(List<PrizeRule> prizeRule) {
        this.prizeRule = prizeRule;
    }

    public List<GamePrize> getGamePrize() {
        return gamePrize;
    }

    public void setGamePrize(List<GamePrize> gamePrize) {
        this.gamePrize = gamePrize;
    }

    public String getGameRule() {
        return gameRule;
    }

    public void setGameRule(String gameRule) {
        this.gameRule = gameRule;
    }
}
