package com.zatgo.zup.games.mapper;


import com.zatgo.zup.games.entity.Game;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface GameMapper {
    int deleteByPrimaryKey(String gameId);

    int insert(Game record);

    int insertSelective(Game record);

    Game selectByPrimaryKey(String gameId);

    int updateByPrimaryKeySelective(Game record);

    int updateByPrimaryKey(Game record);

    @Select("select * from game")
    List<Game> selectAll();
}