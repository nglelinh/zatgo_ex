package com.zatgo.zup.games.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.model.UserExtModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("zup-merchant")
public interface UserRemoteService {

	/**
	 * 根据用户ID获取云用户的管理员用户ID(内部调用)")
	 */
	@PostMapping(value = "/user/cloud/get/manageUserId/{userId}/{cloudManageType}")
	@ResponseBody
	public ResponseData<UserData> getManageUserId(@PathVariable("userId") String userId,@PathVariable("cloudManageType") String cloudManageType);


	@PostMapping(value = "/user/black/list/{userId}")
	@ResponseBody
	ResponseData<UserExtModel> getBlackList(@PathVariable(value = "userId") String userId);
}
