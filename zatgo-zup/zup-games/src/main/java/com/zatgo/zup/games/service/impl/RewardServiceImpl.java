package com.zatgo.zup.games.service.impl;

import com.zatgo.zup.common.enumtype.GameEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.GetRewarRequest;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.games.cache.*;
import com.zatgo.zup.games.config.BlackList;
import com.zatgo.zup.games.method.RateMethod;
import com.zatgo.zup.games.model.ZhuanpanRunData;
import com.zatgo.zup.games.service.RewardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 46041 on 2019/1/11.
 */

@Service("rewardService")
public class RewardServiceImpl implements RewardService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private BlackList blackList;

    @Override
    public ZhuanpanRunData getRewar(GetRewarRequest request) {
        String gameId = request.getGameId();
        String coinType = request.getCoinType().toUpperCase();
        Boolean isFree = request.getFree();
        String userId = request.getUserId();
        if (StringUtils.isEmpty(userId))
            throw new BusinessException(BusinessExceptionCode.LACK_MUST_PARAMETER);
        GameConfig gameConfig = GameCache.getGameConfig(gameId);
        BigDecimal gameRate = blackList.getGameRate(userId);

        if(gameConfig==null)
            throw new BusinessException(BusinessExceptionCode.GAME_IS_NOT_EXIST);

        GameFee fee = getGameFee(gameConfig.getGameFee(), coinType);
        if (fee == null)
            throw new BusinessException(BusinessExceptionCode.COIN_PAIR_CONFIG_CLOSE);


        //更新缓存总次数
        String totalTimesKey = getGameTotalTimesKey(gameId);
        Long totalTimes = redisTemplate.opsForValue().increment(totalTimesKey,1);

        //更新缓存资金池
        String totalAmountKey = getGameTotalAmountKey(gameId, coinType);
        Long totalAmount = Long.parseLong(fee.getFee());

        if(isFree){
            //免费游戏，不改动资金池
            redisTemplate.opsForValue().increment(totalAmountKey,0);
        }else{
            //付费游戏，增加资金池
            redisTemplate.opsForValue().increment(totalAmountKey,totalAmount);
        }

        ZhuanpanRunData data = new ZhuanpanRunData();
        data.setResult(GameEnum.GameResult.LOSE.getCode());
        List<PrizeRule> rules = gameConfig.getPrizeRule();
        List<GamePrize> prizes = gameConfig.getGamePrize();
        for(int i=0;i<rules.size();i++){

            PrizeRule rule = rules.get(i);
            GamePrize prize = prizes.get(i);
            String rate = rule.getRate();
            //看是不是特殊用户，是否设置了中奖因子
            if (gameRate != null){
                rate = new BigDecimal(rate).multiply(gameRate).toPlainString();
            }

            if(GameEnum.GameWinRule.BY_AMOUNT.getCode().equalsIgnoreCase(rule.getType())){
                //累积达到金额后出一次奖
                if(totalAmount>=Long.parseLong(rule.getTypeValue())){
                    //出奖概率计算
                    if(doRate(gameId,rate)){
                        //中奖
                        data.setResult(GameEnum.GameResult.WIN.getCode());
                        data.setPrize(prize);

                        //更新缓存，减少资金池
                        redisTemplate.opsForValue().increment(totalAmountKey,-Long.parseLong(prize.getValue()));
                        break;
                    }
                }
            }else if(GameEnum.GameWinRule.BY_TIMES.getCode().equalsIgnoreCase(rule.getType())){
                //累积达到次数后出一次奖
                if(totalTimes>=Long.parseLong(rule.getTypeValue())){
                    //出奖概率计算
                    if(doRate(gameId,rate)){
                        //中奖
                        data.setResult(GameEnum.GameResult.WIN.getCode());
                        data.setPrize(prize);

                        //更新缓存，清空次数
                        redisTemplate.delete(totalTimesKey);
                        redisTemplate.opsForValue().increment(totalTimesKey,1);


                        break;
                    }
                }
            }
        }
        return data;
    }

    /**
     * 根据概率计算是否中奖
     * @param rate
     * @return
     */
    private boolean doRate(String gameId,String rate) {

        try{
            float winRate = Float.parseFloat(rate);
            return RateMethod.startOne(winRate);

        }catch (Exception e){
            logger.error("游戏中奖概率设置有误:"+gameId,e);
        }

        return false;
    }

    private String getGameTotalAmountKey(String gameId,String coin){
        String key = new StringBuilder(RedisKeyConstants.GAME_TOTAL_AMOUNT_PRE).append(gameId)
                .append("_")
                .append(coin).toString();

        return key;
    }

    private GameFee getGameFee(List<GameFee> gameFee,String coin) {
        for(GameFee fee:gameFee){
            if(coin.equalsIgnoreCase(fee.getCoin())){
                return fee;
            }
        }
        return null;
    }

    private String getGameTotalTimesKey(String gameId){
        String key = RedisKeyConstants.GAME_TOTAL_TIMES_PRE+gameId;
        return key;
    }
}
