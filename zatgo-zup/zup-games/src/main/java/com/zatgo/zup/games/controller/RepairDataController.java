package com.zatgo.zup.games.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.games.config.BlackList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 46041 on 2018/11/28.
 */


@Api(value = "/repairData",description = "修数据接口",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/repairData")
public class RepairDataController {

    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private BlackList blackList;


    @ApiOperation(value = "更新单个用户的黑名单缓存(内部调用)")
    @GetMapping("/update/cache/{userId}")
    public ResponseData updateBlackListCache(@PathVariable (value = "userId") String userId){
        String key = blackList.getKey(userId);
        redisUtils.removeKey(key);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "更新所有用户的黑名单缓存(内部调用)")
    @GetMapping("/update/cache/all")
    public ResponseData updateAllBlackListCache(){
        String key = blackList.getKey("*");
        redisUtils.removeKey(key);
        return BusinessResponseFactory.createSuccess(null);
    }
}
