package com.zatgo.zup.games.mapper;


import com.zatgo.zup.games.entity.GameParams;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface GameParamsMapper {
    int deleteByPrimaryKey(String paramId);

    int insert(GameParams record);

    int insertSelective(GameParams record);

    GameParams selectByPrimaryKey(String paramId);

    int updateByPrimaryKeySelective(GameParams record);

    int updateByPrimaryKey(GameParams record);

    @Select("select * from game_params where game_id = #{gameId}")
    List<GameParams> selectByGameId(@Param("gameId") String gameId);
}