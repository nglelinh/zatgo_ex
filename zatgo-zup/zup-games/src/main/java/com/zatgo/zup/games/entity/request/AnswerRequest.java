package com.zatgo.zup.games.entity.request;

import java.util.Date;

/**
 * Created by 46041 on 2019/2/12.
 */
public class AnswerRequest {


    //题目ID
    private String questionId;
    //答案
    private String answer;
    //显示的答案
    private String title;
    //答题时间
    private Date endDate;
    //任务ID
    private String gameId;
    //题目批号
    private String batchNumber;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }
}
