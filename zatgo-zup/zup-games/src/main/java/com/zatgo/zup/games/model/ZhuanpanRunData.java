package com.zatgo.zup.games.model;

import com.zatgo.zup.games.cache.GamePrize;
import io.lettuce.core.dynamic.annotation.Value;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

@ApiModel(value = "大转盘启动返回对象")
public class ZhuanpanRunData extends BaseData{

    @ApiModelProperty(value="中奖结果，0-未中奖、1-中奖")
    private String result;

    @ApiModelProperty(value="奖品")
    private GamePrize prize;

    @ApiModelProperty(value="今日剩余免费次数")
    private long todayFreeTimes;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public GamePrize getPrize() {
        return prize;
    }

    public void setPrize(GamePrize prize) {
        this.prize = prize;
    }

    public long getTodayFreeTimes() {
        return todayFreeTimes;
    }

    public void setTodayFreeTimes(long todayFreeTimes) {
        this.todayFreeTimes = todayFreeTimes;
    }
}
