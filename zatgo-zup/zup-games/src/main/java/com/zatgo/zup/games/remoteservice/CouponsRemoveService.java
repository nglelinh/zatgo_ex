package com.zatgo.zup.games.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by 46041 on 2019/3/28.
 */

@FeignClient("zup-coupons")
public interface CouponsRemoveService {



    @RequestMapping(value = "/coupons/user/internal/triggerGetCoupons/{cloudUserId}/{userId}/{triggerBusinessType}/{triggerBusinessCode}",method = RequestMethod.POST)
    @ResponseBody
    ResponseData<List<UserCouponsModel>> triggerGetCoupons(
            @PathVariable("cloudUserId") String cloudUserId,
            @PathVariable("userId") String userId,
            @PathVariable("triggerBusinessType") String triggerBusinessType,
            @PathVariable("triggerBusinessCode") String triggerBusinessCode);
}
