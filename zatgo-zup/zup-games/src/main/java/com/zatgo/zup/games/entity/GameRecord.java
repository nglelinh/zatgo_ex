package com.zatgo.zup.games.entity;

import java.util.Date;

public class GameRecord {
    private String recordId;

    private String gameId;

    private String userId;

    private Byte feeType;

    private String feeCoin;

    private String feeAmount;

    private Byte isWin;

    private Byte prizeType;

    private String prizeInfo;

    private Byte prizeStatus;

    private Date recordTime;

    private String prizeCoinType;

    private String aliasName;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId == null ? null : recordId.trim();
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId == null ? null : gameId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Byte getFeeType() {
        return feeType;
    }

    public void setFeeType(Byte feeType) {
        this.feeType = feeType;
    }

    public String getFeeCoin() {
        return feeCoin;
    }

    public void setFeeCoin(String feeCoin) {
        this.feeCoin = feeCoin == null ? null : feeCoin.trim();
    }

    public String getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        this.feeAmount = feeAmount == null ? null : feeAmount.trim();
    }

    public Byte getIsWin() {
        return isWin;
    }

    public void setIsWin(Byte isWin) {
        this.isWin = isWin;
    }

    public Byte getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(Byte prizeType) {
        this.prizeType = prizeType;
    }

    public String getPrizeInfo() {
        return prizeInfo;
    }

    public void setPrizeInfo(String prizeInfo) {
        this.prizeInfo = prizeInfo == null ? null : prizeInfo.trim();
    }

    public Byte getPrizeStatus() {
        return prizeStatus;
    }

    public void setPrizeStatus(Byte prizeStatus) {
        this.prizeStatus = prizeStatus;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public String getPrizeCoinType() {
        return prizeCoinType;
    }

    public void setPrizeCoinType(String prizeCoinType) {
        this.prizeCoinType = prizeCoinType;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }
}