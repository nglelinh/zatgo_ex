package com.zatgo.zup.games.cache;

/**
 * Created by chen on 2018/12/20.
 */
public class GameFee {


    /**
     * 网络类型：BTC、ETH、QTUM
     */
    private String NetworkType;
    /**
     * 币种
     */
    private String coin;

    /**
     * 数量
     */
    private String fee;

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getNetworkType() {
        return NetworkType;
    }

    public void setNetworkType(String networkType) {
        NetworkType = networkType;
    }
}
