package com.zatgo.zup.games.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.encrypt.MD5Util;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AnswerQuestionResultModel;
import com.zatgo.zup.common.model.AnswerRecordResult;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.QuestionListModel;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.games.cache.AnswerQuestionStyleConfig;
import com.zatgo.zup.games.cache.GameCache;
import com.zatgo.zup.games.cache.GameConfig;
import com.zatgo.zup.games.entity.AnswerQuestionResult;
import com.zatgo.zup.games.entity.AnswerQuestionResultExample;
import com.zatgo.zup.games.entity.GameAnswerQuestionBank;
import com.zatgo.zup.games.entity.request.AnswerRequest;
import com.zatgo.zup.games.entity.response.AnswerResponse;
import com.zatgo.zup.games.mapper.AnswerQuestionResultMapper;
import com.zatgo.zup.games.mapper.GameAnswerQuestionBankMapper;
import com.zatgo.zup.games.service.QuestionAnswerService;
import com.zatgo.zup.games.util.DateUtil;
import com.zatgo.zup.games.util.GameKeyUtil;
import com.zatgo.zup.games.util.QuestionUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 46041 on 2018/11/28.
 */

@Service("questionAnswerServiceImpl")
public class QuestionAnswerServiceImpl implements QuestionAnswerService {

    private static final Logger logger = LoggerFactory.getLogger(QuestionAnswerServiceImpl.class);

    @Autowired
    private AnswerQuestionResultMapper answerQuestionResultMapper;
    @Autowired
    private GameAnswerQuestionBankMapper gameAnswerQuestionBankMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public QuestionListModel list(String gameId, AuthUserInfo user) {
        QuestionListModel res = new QuestionListModel();
        String userId = user.getUserId();
        GameConfig gameConfig = GameCache.getGameConfig(gameId);
        if (gameConfig == null){
            throw new BusinessException(BusinessExceptionCode.GAME_IS_NOT_EXIST);
        }
        AnswerQuestionStyleConfig config = JSONObject.parseObject(gameConfig.getGameRule(), AnswerQuestionStyleConfig.class);
        if (config.getOneDayNum().intValue() != -1 && !canAnswer(userId, gameId, config)){
            throw new BusinessException(BusinessExceptionCode.GAME_ANSWER_ERROR);
        }
        Integer count = config.getOnceNum();
        res.setBatchNumber(UUIDUtils.getUuid());
        res.setTime(config.getAnswerTime());
        //获取总题量
        Long questionCount = gameAnswerQuestionBankMapper.questionCount(gameId);
        Long random = QuestionUtil.getRandomBetweenXAndY(1l, questionCount);
        //获取limit的开始和结束
        long limit = getLimit(random, count.longValue(), 5);
        List<GameAnswerQuestionBank> list = gameAnswerQuestionBankMapper.selectQuestionList(gameId, limit, 5 * count.longValue());
        setQuestion(res, list, userId, count, gameId);
        return res;
    }

    @Override
    public AnswerResponse answer(AnswerRequest request, AuthUserInfo user) {
        String questionId = request.getQuestionId();
        Date endDate = request.getEndDate();
        String gameId = request.getGameId();
        String answer = request.getAnswer();
        String title = request.getTitle();
        String userId = user.getUserId();
        String batchNumber = request.getBatchNumber();
        String check = encode(questionId, userId, gameId, title, endDate, batchNumber);
        GameConfig gameConfig = GameCache.getGameConfig(gameId);
        if (gameConfig != null){
            //先校验参数
            if (!StringUtils.isEmpty(title) && !check.equals(answer)){
                logger.error("答题参数校验失败：" + JSONObject.toJSONString(request) + ",userId :" + userId);
                throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
            }
            //获取题目
            GameAnswerQuestionBank question = gameAnswerQuestionBankMapper.selectByPrimaryKey(questionId);
            //判断对错
            Boolean result = isTrue(question.getAnswers(), title, endDate);
            //保存记录
            saveRecord(gameId, questionId, title, result, userId, batchNumber);
            AnswerResponse response = new AnswerResponse();
            response.setAnswer(question.getAnswers());
            response.setTrue(result);
            return response;
        }
        return null;
    }

    @Override
    public List<AnswerRecordResult> record(String gameId, AuthUserInfo user) {
        List<AnswerQuestionResultModel> answerQuestionResultModels = answerQuestionResultMapper.selectRecordByGameIdAndUserId(gameId, user.getUserId());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Map<String, AnswerRecordResult> map = new HashMap<>();
        List<AnswerRecordResult> list = new ArrayList<>();
        answerQuestionResultModels.forEach(model -> {
            Date createDate = model.getCreateDate();
            String key = sdf.format(createDate);
            if (map.containsKey(key)){
                AnswerRecordResult recordResult = map.get(key);
                List<AnswerQuestionResultModel> results = recordResult.getList();
                results.add(model);
            } else {
                AnswerRecordResult recordResult = new AnswerRecordResult();
                List<AnswerQuestionResultModel> results = new ArrayList<>();
                results.add(model);
                recordResult.setList(results);
                recordResult.setDate(key);
                map.put(key, recordResult);
            }
        });
        map.forEach((k, v) -> {
            list.add(v);
        });
        return list;
    }

    @Override
    public List<AnswerQuestionResultModel> recordList(String gameId, String userId, Date startDate, Date endDate, Boolean isTrue) {
        AnswerQuestionResultExample example = new AnswerQuestionResultExample();
        AnswerQuestionResultExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andGameIdEqualTo(gameId);
        if (isTrue != null){
            criteria.andIsTrueEqualTo(isTrue);
        }
        criteria.andCreateDateBetween(startDate, endDate);
        List<AnswerQuestionResult> record = answerQuestionResultMapper.selectByExample(example);
        List<AnswerQuestionResultModel> res = new ArrayList<>(record.size());
        record.forEach(result -> {
            AnswerQuestionResultModel model = new AnswerQuestionResultModel();
            try {
                BeanUtils.copyProperties(model, result);
                res.add(model);
            } catch (Exception e) {
                logger.error("", e);
            }
        });
        return res;
    }

    @Override
    public Integer countByDoneTaskFen(Date startDate, Date endDate, String gameId, Integer count) {
        Integer fen = answerQuestionResultMapper.countByDoneTaskFen(startDate, endDate, gameId, count);
        return fen == null ? 0 : fen;
    }

    private Boolean canAnswer(String userId, String gameId, AnswerQuestionStyleConfig config){
        try{
            Integer oneDayNum = config.getOneDayNum();
            Integer count = answerQuestionResultMapper.answerRecordCount(userId, gameId, DateUtil.getDate(0), DateUtil.getDate(1));
            if (count == null)
                count = 0;
            String key = GameKeyUtil.getGameRunFreeTimesKey(gameId, userId);
            Long unUsed = redisTemplate.opsForValue().increment(key, 0);
            if (oneDayNum.intValue() <= count.intValue()){
                if (unUsed.intValue() < 1){
                    return false;
                }
                redisTemplate.opsForValue().increment(key, -1);
            }
            return true;
        } catch (ParseException e) {
            logger.error("", e);
        }
        return true;
    }

    /**
     * 保存答题记录
     * @param gameId
     * @param questionId
     * @param answer
     * @param result
     */
    private void saveRecord(String gameId, String questionId, String answer, Boolean result, String userId, String batchNumber){
        //先保存答题记录
        AnswerQuestionResult answerQuestionResult = new AnswerQuestionResult();
        answerQuestionResult.setQuestionId(questionId);
        answerQuestionResult.setCreateDate(new Date());
        answerQuestionResult.setGameId(gameId);
        answerQuestionResult.setIsTrue(result);
        answerQuestionResult.setAnswers(answer);
        answerQuestionResult.setUserId(userId);
        answerQuestionResult.setBatchNumber(batchNumber);
       try {
           Date date = DateUtil.getDate(0);
           answerQuestionResult.setId(getRecordId(questionId, userId, gameId, date.getTime()));
           answerQuestionResultMapper.insertSelective(answerQuestionResult);
       } catch (ParseException e) {
           logger.error("", e);
       } catch (Exception e){
           logger.error("重复答题===========" , e);
           logger.error("gameId=" + gameId + ";questionId=" + questionId + ";answer=" + answer + ";userId" + userId);
       }
    }


    /**
     * 判断回答是否正确
     * @param trueAnswer
     * @param answer
     * @param endDate
     * @return
     */
    private Boolean isTrue(String trueAnswer, String answer, Date endDate){
        //判断有没有超时
        Date date = new Date();
        if (date.getTime() > endDate.getTime()){
            answer = "";
            return false;
        }
        return trueAnswer.equals(answer);
    }


    /**
     * 将数据做成前端要的样子
     * @param res
     * @param list
     */
    private void setQuestion(QuestionListModel res, List<GameAnswerQuestionBank> list, String userId,
                             Integer count, String gameId){
        if (CollectionUtils.isEmpty(list))
            return;
        long time = res.getTime();
        List<GameAnswerQuestionBank> target = new ArrayList<>();
        if (list.size() < count.intValue()){
            target.addAll(list);
        } else {
            Set<Integer> index = getIndex(count, list.size());
            index.forEach(i -> {
                target.add(list.get(i));
            });
        }
        Date date = getEndDate(new Date(), target.size(), time);
        List<QuestionListModel.QuestionModel> questionModels = new ArrayList<>();
        //将数据做成前端要的样子
        target.forEach(question -> {
            String questionId = question.getQuestionId();
            String questionType = question.getQuestionType();
            String answersList = question.getAnswersList();
            QuestionListModel.QuestionModel questionModel = new QuestionListModel().new QuestionModel();
            questionModel.setEndDate(date);
            questionModel.setType(questionType);
            questionModel.setQuestionId(questionId);
            questionModel.setShowTime(GameCache.RESULT_SHOW_TIME);
            questionModel.setTopics(question.getQuestionContent());
            setOption(questionModel, answersList, questionId, userId, gameId, date, res.getBatchNumber());
            questionModels.add(questionModel);
        });
        res.setQuestions(questionModels);
    }

    private Set<Integer> getIndex(int count, Integer size){
        Set<Integer> index = new HashSet<>();
        while (true){
            Long randomBetweenXAndY = QuestionUtil.getRandomBetweenXAndY(0l, size.longValue());
            index.add(randomBetweenXAndY.intValue());
            if (index.size() >= count)
                return index;
        }
    }

    private void setOption(QuestionListModel.QuestionModel model, String answersList, String questionId,
                           String userId, String gameId, Date date, String batchNumber){
        String[] split = answersList.split("\\|\\|");
        List<QuestionListModel.Option> options = new ArrayList<>();
        for (int i = 0; i <split.length; i++){
            QuestionListModel.Option option = new QuestionListModel().new Option();
            String title = split[i];
            option.setTitle(title);
            option.setAnswer(encode(questionId, userId, gameId, title, date, batchNumber));
            options.add(option);
        }
        model.setOptions(options);
    }

    /**
     *  获取分页的开始页
     * @param random
     * @param count
     * @param ratio  系数
     * @return
     */
    private long getLimit(long random, long count, long ratio){
        long v = count * ratio;
        long start = random - v;
        return start > 0 ? start : 0;
    }

    /**
     * 加密
     * @param questionId
     * @param userId
     * @param gameId
     * @param title
     * @param date
     * @return
     */
    private String encode(String questionId, String userId, String gameId, String title, Date date, String batchNumber){
        String key = questionId + userId + gameId + title + date.getTime() + batchNumber + GameCache.GAME_PRIVATE_KEY;
        return MD5Util.MD5(key);
    }

    private String getRecordId(String questionId, String userId, String gameId, Long date){
        String key = questionId + userId + gameId + date;
        return MD5Util.MD5(key);
    }


    private Date getEndDate(Date startDate, int i, long time){
        time = (time + GameCache.RESULT_SHOW_TIME + 1) * i + 10l;
        return new Date(startDate.getTime() + time * 1000l);
    }
}
