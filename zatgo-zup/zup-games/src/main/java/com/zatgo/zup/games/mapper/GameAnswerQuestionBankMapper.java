package com.zatgo.zup.games.mapper;

import com.zatgo.zup.games.entity.GameAnswerQuestionBank;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GameAnswerQuestionBankMapper extends BaseMapper<GameAnswerQuestionBank> {


    List<GameAnswerQuestionBank> selectQuestionList(@Param("gameId") String gameId,
                                                    @Param("pageNo") Long pageNo,
                                                    @Param("pageSize") Long pageSize);

    Long questionCount(String gameId);
}