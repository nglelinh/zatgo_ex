package com.zatgo.zup.games.method;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * 概率计算方法
 * Created by chen on 2018/12/21.
 */
public class RateMethod {


    /**
     * 单个概率计算是否命中
     * @param rate
     */
    public static boolean startOne(float rate){

        //单个概率分割成中和不中
        float missRate = 1-rate;

        float[] rateValues = new float[]{rate,missRate};

        int hit = startMany(rateValues);

        if(hit==0)
            return true;

        return false;
    }

    /**
     * 多个概率值命中哪个
     * @param rateValues
     * @return
     */
    public static int startMany(float[] rateValues){

        double n = Math.random();

        for(int i=0;i<rateValues.length;i++){
            if(n<rateValues[i]){
                return i;
            }else{
                n -= rateValues[i];
            }
        }

        return rateValues.length-1;
    }

    /**
     * 测试
     * @param args
     */
    public static void main(String[] args){

        float t = 0;
        float f = 0;
        float rate = 0.01f;
        for (int i=0;i<10000;i++)
        {
            if(RateMethod.startOne(rate)){
                t++;
            }else{
                f++;
            }
        }
        System.out.println("test startOne:");
        System.out.println(t);
        System.out.println(f);
        System.out.println(t/(t+f));

        System.out.println("");
        float[] hits = new float[]{0f,0f,0f,0f,0f};
        float[] rateValues = new float[]{0.01f,0.5f,0.09f,0.1f,0.3f};
        for (int i=0;i<10000;i++)
        {
            int hit = RateMethod.startMany(rateValues);
            hits[hit]++;
        }
        System.out.println("test startMany:");
        System.out.println(hits[0]/100000);
        System.out.println(hits[1]/100000);
        System.out.println(hits[2]/100000);
        System.out.println(hits[3]/100000);
        System.out.println(hits[4]/100000);

    }
}
