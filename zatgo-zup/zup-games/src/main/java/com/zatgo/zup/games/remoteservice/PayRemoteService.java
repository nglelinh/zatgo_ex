package com.zatgo.zup.games.remoteservice;

import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.RefundParams;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("zup-wallet")
public interface PayRemoteService {

	@RequestMapping(value = "/wallet/pay/{payUserId}",method = RequestMethod.POST)
	@ResponseBody
	ResponseData<String> payByUser(@PathVariable("payUserId") String payUserId,@RequestBody PayParams params);

//	@RequestMapping(value = "/wallet/refund",method = RequestMethod.POST)
//	@ResponseBody
//	ResponseData<String> refund(@RequestBody RefundParams params);



}
