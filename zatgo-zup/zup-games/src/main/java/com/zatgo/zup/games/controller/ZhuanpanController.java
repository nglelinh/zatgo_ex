package com.zatgo.zup.games.controller;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.games.cache.GameCache;
import com.zatgo.zup.games.model.GameRecordData;
import com.zatgo.zup.games.model.GameRecordListRequest;
import com.zatgo.zup.games.model.ZhuanpanLoadData;
import com.zatgo.zup.games.model.ZhuanpanRunData;
import com.zatgo.zup.games.service.IGameService;
import com.zatgo.zup.games.service.IZhuanpanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(value = "/game/zhuanpan",description = "转盘游戏",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/game/zhuanpan")
public class ZhuanpanController extends BaseController{

	@Resource
	private IZhuanpanService zhuanpanService;

	@Resource
	private IGameService gameService;


	@ApiOperation(value = "加载游戏参数")
	@RequestMapping(value = "/load/{gameId}",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<ZhuanpanLoadData> load(@PathVariable String gameId){

		AuthUserInfo user = getUserInfo();

		ZhuanpanLoadData result = zhuanpanService.load(user,gameId);

		return BusinessResponseFactory.createSuccess(result);
	}


	@ApiOperation(value = "更新游戏参数缓存")
	@RequestMapping(value = "/cache",method = RequestMethod.POST)
	@ResponseBody
	public void cache(){
		gameService.initCache();
	}

	@ApiOperation(value = "启动游戏")
	@RequestMapping(value = "/run/{gameId}/{coin}",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<ZhuanpanRunData> run(@PathVariable String gameId,@PathVariable String coin){

		AuthUserInfo user = getUserInfo();

		ZhuanpanRunData result = zhuanpanService.run(user,gameId,coin);

		return BusinessResponseFactory.createSuccess(result);
	}


	@ApiOperation(value = "游戏记录列表")
	@RequestMapping(value = "/recordList",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<PageInfo<GameRecordData>> gameRecordList(@RequestBody GameRecordListRequest request) {

		AuthUserInfo user = getUserInfo();
		PageInfo<GameRecordData> result = zhuanpanService.recordList(request, user.getUserId());

		return BusinessResponseFactory.createSuccess(result);
	}

}
