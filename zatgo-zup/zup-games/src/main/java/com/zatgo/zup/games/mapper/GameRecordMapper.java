package com.zatgo.zup.games.mapper;

import com.zatgo.zup.games.entity.GameRecord;
import com.zatgo.zup.games.model.GameRecordData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GameRecordMapper {
    int deleteByPrimaryKey(String recordId);

    int insert(GameRecord record);

    int insertSelective(GameRecord record);

    GameRecord selectByPrimaryKey(String recordId);

    int updateByPrimaryKeySelective(GameRecord record);

    int updateByPrimaryKey(GameRecord record);

    List<GameRecordData> selectGameRecordList(@Param("gameId") String gameId, @Param("userId") String userId);
}