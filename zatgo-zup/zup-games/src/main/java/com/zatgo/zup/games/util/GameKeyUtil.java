package com.zatgo.zup.games.util;

import com.zatgo.zup.common.redis.RedisKeyConstants;

/**
 * Created by 46041 on 2019/4/2.
 */
public class GameKeyUtil {

    /**
     * 获取游戏运行次数缓存
     * @param gameId
     * @param userId
     * @return
     */
    public static String getGameRunFreeTimesKey(String gameId,String userId){
        String key = new StringBuilder(RedisKeyConstants.GAME_RUN_FREE_TIMES_PRE).append(gameId)
                .append("_")
                .append(userId).toString();
        return key;
    }

}
