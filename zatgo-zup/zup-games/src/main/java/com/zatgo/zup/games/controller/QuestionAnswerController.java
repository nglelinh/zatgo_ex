package com.zatgo.zup.games.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.*;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.games.entity.AnswerQuestionResult;
import com.zatgo.zup.games.entity.request.AnswerRequest;
import com.zatgo.zup.games.entity.response.AnswerResponse;
import com.zatgo.zup.games.service.QuestionAnswerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2018/11/28.
 */


@Api(value = "/game/qa",description = "问答管理",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/game/qa")
public class QuestionAnswerController extends BaseController{


    @Autowired
    private QuestionAnswerService questionAnswerService;
    @Autowired
    private RedisLockUtils redisLockUtils;


    @ApiOperation(value = "问题列表")
    @GetMapping("/list")
    public ResponseData list(@RequestParam("gameId") String gameId){
        AuthUserInfo user = getUserInfo();
        String lockKey = RedisKeyConstants.GAME_QUESTION_LOCK_PRE + gameId + user.getUserId();
        QuestionListModel list = null;
        redisLockUtils.lock(lockKey);
        try {
            list = questionAnswerService.list(gameId, user);
        } finally {
            redisLockUtils.releaseLock(lockKey);
        }
        return BusinessResponseFactory.createSuccess(list);
    }



    @ApiOperation(value = "答题")
    @PostMapping("/answer")
    public ResponseData answer(@RequestBody AnswerRequest request){
        AuthUserInfo user = getUserInfo();
        AnswerResponse res = questionAnswerService.answer(request, user);
        return BusinessResponseFactory.createSuccess(res);
    }


    @ApiOperation(value = "答题历史")
    @PostMapping("/record")
    public ResponseData record(@RequestParam("gameId") String gameId){
        AuthUserInfo user = getUserInfo();
        List<AnswerRecordResult> record = questionAnswerService.record(gameId, user);
        return BusinessResponseFactory.createSuccess(record);
    }


    @ApiOperation(value = "答题历史(内部调用)")
    @PostMapping("/getAnswerRecord")
    public ResponseData recordList(@RequestBody AnswerRecordRequest request){
        String gameId = request.getGameId();
        String userId = request.getUserId();
        Date startDate = request.getStartDate();
        Date endDate = request.getEndDate();
        Boolean isTrue = request.getTrue();
        List<AnswerQuestionResultModel> record = questionAnswerService.recordList(gameId, userId, startDate, endDate, isTrue);
        return BusinessResponseFactory.createSuccess(record);
    }

    @ApiOperation(value = "查询总分数(内部调用)")
    @PostMapping("/countByDoneTaskFen")
    public ResponseData countByDoneTaskFen(@RequestBody AnswerDoneFenRequest request){
        Date startDate = request.getStartDate();
        Date endDate = request.getEndDate();
        String gameId = request.getGameId();
        Integer count = request.getCount();
        Integer value = questionAnswerService.countByDoneTaskFen(startDate, endDate, gameId, count);
        return BusinessResponseFactory.createSuccess(value);
    }

}
