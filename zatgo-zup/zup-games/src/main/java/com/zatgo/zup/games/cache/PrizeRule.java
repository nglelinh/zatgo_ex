package com.zatgo.zup.games.cache;

/**
 * Created by chen on 2018/12/20.
 */
public class PrizeRule {

    /**
     * 奖品等级
     */
    private String level;

    /**
     * 出奖标准，0-按概率出奖，1-累积次数后出奖一次，2-累积金额后出奖一次
     */
    private String type;

    /**
     * 出奖标准值
     */
    private String typeValue;

    /**
     * 概率
     */
    private String rate;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}
