package com.zatgo.zup.games.cache;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.GameEnum;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.games.entity.Game;
import com.zatgo.zup.games.entity.GameParams;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by chen on 2018/12/20.
 */
public class GameCache {

    private static Logger logger = LoggerFactory.getLogger(GameCache.class);

    public static HashMap<String,GameConfig> GAME_CONFIG_CACHE = new HashMap();

    /**
     * 答题错误展示时间（秒）
     */
    public static long RESULT_SHOW_TIME = 3l;
    /**
     *  游戏key
     */
    public static String GAME_PRIVATE_KEY = "b5609c25920c400c981ae19a7dd6f043";

    public static void initGameConfig(Game game, List<GameParams> params){

        if(game==null)
            return;

        GameConfig config = new GameConfig();
        config.setGameId(game.getGameId());
        config.setGameName(game.getGameName());
        config.setGameType(game.getGameType());

        if(params!=null){
            for(GameParams param:params){
                if("FREE_TIMES".equalsIgnoreCase(param.getParamCode())){
                    config.setFreeTimes(param.getParamValue());
                }
                if("GAME_FEE".equalsIgnoreCase(param.getParamCode())){
                    config.setGameFee(parseGameFee(param.getParamValue()));
                }
                if("PRIZE_RULE".equalsIgnoreCase(param.getParamCode())){
                    config.setPrizeRule(parseGameRule(param.getParamValue()));
                }
                if("GAME_PRIZE".equalsIgnoreCase(param.getParamCode())){
                    config.setGamePrize(parseGamePrize(param.getParamValue()));
                }
                if("GAME_RULE".equalsIgnoreCase(param.getParamCode())){
                    config.setGameRule(param.getParamValue());
                }
            }
        }

        GAME_CONFIG_CACHE.put(game.getGameId(),config);

    }

    private static List<GameFee> parseGameFee(String paramValue) {

        if(StringUtils.isEmpty(paramValue))
            return null;

        try{
            List<GameFee> fees = new ArrayList<>();

            String[] values = paramValue.split(",");
            for(String val:values){

                String[] v = val.split(":");

                GameFee fee = new GameFee();
                fee.setNetworkType(v[0]);
                fee.setCoin(v[1]);
                fee.setFee(v[2]);

                fees.add(fee);
            }

            return fees;

        }catch(Exception e){
            logger.error("Game fee params error",e);
            return null;
        }

    }

    private static List<GamePrize> parseGamePrize(String paramValue) {

        if(StringUtils.isEmpty(paramValue))
            return null;

        try{
            List<GamePrize> prizes = new ArrayList<>();

            String[] values = paramValue.split(",");
            for(String val:values){

                String[] v = val.split(":");

                GamePrize prize = new GamePrize();
                prize.setLevel(v[0]);
                prize.setType(v[1]);
                String prizeValue = v[2];
                if (GameEnum.GamePrizeType.COIN.getCode().equals(prizeValue.charAt(0) + "")) {
                    String[] split = prizeValue.split("/");
                    prize.setPrize(split[0]);
                    prize.setCoinNetworkType(split[1]);
                    prize.setCoinType(split[2]);
                    if (split.length > 3){
                        prize.setAliasName(split[3]);
                    }
                } else if (GameEnum.GamePrizeType.COUPON.getCode().equals(prizeValue.charAt(0) + "")) {
                    String[] split = prizeValue.split("/");
                    prize.setPrize(split[0]);
                    prize.setTriggerBusinessType(split[1]);
                    prize.setTriggerBusinessCode(split[2]);
                    if (split.length > 3){
                        prize.setAliasName(split[3]);
                    }
                } else {
                    prize.setPrize(prizeValue);
                }
                prize.setValue(v[3]);

                prizes.add(prize);
            }

            //根据LEVEL排序
            prizes.sort((r1,r2) -> {
                return r1.getLevel().compareTo(r2.getLevel());
            });

            return prizes;

        }catch(Exception e){
            logger.error("Game prize params error",e);
            return null;
        }
    }

    private static List<PrizeRule> parseGameRule(String paramValue) {
        if(StringUtils.isEmpty(paramValue))
            return null;

        try{
            List<PrizeRule> rules = new ArrayList<>();

            String[] values = paramValue.split(",");
            for(String val:values){

                String[] v = val.split(":");

                PrizeRule rule = new PrizeRule();
                rule.setLevel(v[0]);
                rule.setType(v[1]);
                rule.setTypeValue(v[2]);
                rule.setRate(v[3]);

                rules.add(rule);

            }

            //根据LEVEL排序
            rules.sort((r1,r2) -> {
                return r1.getLevel().compareTo(r2.getLevel());
            });

            return rules;

        }catch(Exception e){
            logger.error("Game rule params error",e);
            return null;
        }


    }

    public static GameConfig getGameConfig(String gameId){
        return GAME_CONFIG_CACHE.get(gameId);
    }
}
