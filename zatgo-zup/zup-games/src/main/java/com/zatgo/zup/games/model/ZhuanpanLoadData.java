package com.zatgo.zup.games.model;

import com.zatgo.zup.games.cache.GameFee;
import com.zatgo.zup.games.cache.GamePrize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "大转盘登陆返回对象")
public class ZhuanpanLoadData extends BaseData{

    /**
     * 游戏费用
     */
    @ApiModelProperty(value="游戏费用")
    private List<GameFee> gameFee;
    /**
     * 奖品
     */
    @ApiModelProperty(value="奖品")
    private List<GamePrize> gamePrize;

    /**
     * 今日剩余次数
     */
    @ApiModelProperty(value="今日剩余免费次数")
    private int todayFreeTimes;

    public List<GameFee> getGameFee() {
        return gameFee;
    }

    public void setGameFee(List<GameFee> gameFee) {
        this.gameFee = gameFee;
    }

    public List<GamePrize> getGamePrize() {
        return gamePrize;
    }

    public void setGamePrize(List<GamePrize> gamePrize) {
        this.gamePrize = gamePrize;
    }

    public int getTodayFreeTimes() {
        return todayFreeTimes;
    }

    public void setTodayFreeTimes(int todayFreeTimes) {
        this.todayFreeTimes = todayFreeTimes;
    }
}
