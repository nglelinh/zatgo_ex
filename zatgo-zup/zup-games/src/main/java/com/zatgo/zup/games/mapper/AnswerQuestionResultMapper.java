package com.zatgo.zup.games.mapper;


import com.zatgo.zup.common.model.AnswerQuestionResultModel;
import com.zatgo.zup.games.entity.AnswerQuestionResult;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface AnswerQuestionResultMapper extends BaseMapper<AnswerQuestionResult>{

    Integer countByDoneTask(@Param("startTime") Date startTime,
                        @Param("endTime") Date endTime,
                        @Param("gameId") String gameId,
                        @Param("count") Integer count);


    List<AnswerQuestionResultModel> selectRecordByGameIdAndUserId(@Param("gameId") String gameId, @Param("userId") String userId);

    Integer answerRecordCount(@Param("userId") String userId,
                          @Param("gameId") String gameId,
                          @Param("startTime") Date startTime,
                          @Param("endTime") Date endTime);

    Integer countByDoneTaskFen(@Param("startTime") Date startTime,
                           @Param("endTime") Date endTime,
                           @Param("gameId") String gameId,
                           @Param("count") Integer count);
}