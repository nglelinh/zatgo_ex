package com.zatgo.zup.games.service;

import com.zatgo.zup.common.model.AnswerQuestionResultModel;
import com.zatgo.zup.common.model.AnswerRecordResult;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.QuestionListModel;
import com.zatgo.zup.games.entity.request.AnswerRequest;
import com.zatgo.zup.games.entity.response.AnswerResponse;

import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2018/11/28.
 */
public interface QuestionAnswerService {

    /**
     * 答题列表
     * @param user
     * @param gameId
     * @return
     */
    QuestionListModel list(String gameId, AuthUserInfo user);

    /**
     * 答题
     * @param request
     * @param user
     */
    AnswerResponse answer(AnswerRequest request, AuthUserInfo user);

    /**
     * 答题历史
     * @param gameId
     * @param user
     */
    List<AnswerRecordResult> record(String gameId, AuthUserInfo user);

    /**
     * 答题历史（内部调用）
     * @param gameId
     * @param userId
     * @param startDate
     * @param endDate
     * @param isTrue
     * @return
     */
    List<AnswerQuestionResultModel> recordList(String gameId, String userId, Date startDate, Date endDate, Boolean isTrue);

    /**
     * 查分数
     * @param startDate
     * @param endDate
     * @param gameId
     * @param count
     * @return
     */
    Integer countByDoneTaskFen(Date startDate, Date endDate, String gameId, Integer count);
}
