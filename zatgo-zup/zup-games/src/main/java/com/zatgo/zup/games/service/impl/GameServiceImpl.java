package com.zatgo.zup.games.service.impl;

import com.zatgo.zup.games.cache.GameCache;
import com.zatgo.zup.games.entity.Game;
import com.zatgo.zup.games.entity.GameParams;
import com.zatgo.zup.games.mapper.GameMapper;
import com.zatgo.zup.games.mapper.GameParamsMapper;
import com.zatgo.zup.games.service.IGameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by chen on 2018/12/24.
 */
@Service
public class GameServiceImpl implements IGameService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private GameMapper gameMapper;

    @Resource
    private GameParamsMapper gameParamsMapper;

    @Override
    public void initCache() {
        //加载游戏规则信息
        List<Game> games = gameMapper.selectAll();

        for(Game game:games){
            List<GameParams> params = gameParamsMapper.selectByGameId(game.getGameId());
            GameCache.initGameConfig(game,params);
        }

        logger.info("initCache done.");
    }
}
