package com.zatgo.zup.games.service;

import com.zatgo.zup.common.model.GetRewarRequest;
import com.zatgo.zup.games.model.ZhuanpanRunData;

/**
 * Created by 46041 on 2019/1/11.
 */
public interface RewardService {

    /**
     * number在rule type 为1 的时候为累积达到金额后出一次奖  0的时候是累积达到次数后出一次奖
     * @param request
     * @return
     */
    ZhuanpanRunData getRewar(GetRewarRequest request);
}
