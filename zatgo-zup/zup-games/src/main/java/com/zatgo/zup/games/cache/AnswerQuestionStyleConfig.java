package com.zatgo.zup.games.cache;

/**
 * Created by 46041 on 2019/2/15.
 */
public class AnswerQuestionStyleConfig {

    /**
     * 一次游戏的题目数
     */
    private Integer onceNum;
    /**
     * 一天的游戏次数
     * -1为不限
     */
    private Integer oneDayNum;

    /**
     * 答题时间（秒）
     */
    private Long answerTime;



    public Integer getOnceNum() {
        return onceNum;
    }

    public void setOnceNum(Integer onceNum) {
        this.onceNum = onceNum;
    }

    public Integer getOneDayNum() {
        return oneDayNum;
    }

    public void setOneDayNum(Integer oneDayNum) {
        this.oneDayNum = oneDayNum;
    }

    public Long getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(Long answerTime) {
        this.answerTime = answerTime;
    }
}
