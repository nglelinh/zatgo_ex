package com.zatgo.zup.task.remoteTask.mining;

import com.zatgo.zup.task.remoteservice.MineralPoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2018/7/25.
 */
@Component
public class MineralPoolTask {

    private static final Logger logger = LoggerFactory.getLogger(MineralPoolTask.class);

    @Autowired
    private MineralPoolService mineralPoolService;

    @Scheduled(cron = "0 0 * * * ?")
    public void autoExtract(){
        logger.info("================================================MineralPoolTask start==============================================");
        mineralPoolService.createMineralPoolRecord();
        logger.info("================================================MineralPoolTask end==============================================");
    }

    @Scheduled(cron = "0 */10 * * * ?")
    public void updateComputeSortTask(){
        logger.info("================================================updateComputeSortTask start==============================================");
        mineralPoolService.updateComputeSortTask();
        logger.info("================================================updateComputeSortTask end==============================================");
    }
}
