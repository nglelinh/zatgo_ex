package com.zatgo.zup.task.remoteTask.payment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.zatgo.zup.task.remoteservice.FixedTimeDepositRemoteService;
import com.zatgo.zup.task.remoteservice.PaymentService;

/**
 * Created by 46041 on 2018/7/25.
 */
@Component
public class SyncCheckoutCoinRateTask {

    private static final Logger logger = LoggerFactory.getLogger(SyncCheckoutCoinRateTask.class);

    @Autowired
    private PaymentService paymentService;

    @Scheduled(cron = "0 */1 * * * ?")
    public void syncCheckoutCoinRate(){
        logger.info("================================================syncCheckoutCoinRate start==============================================");
        paymentService.syncCheckoutCoinRate();
        logger.info("================================================syncCheckoutCoinRate end==============================================");
    }
}
