package com.zatgo.zup.task.remoteTask.exchange;

import com.zatgo.zup.task.remoteservice.ExchangeRemoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2018/9/26.
 */

@Component
public class ExchangeTask {

    private static final Logger logger = LoggerFactory.getLogger(ExchangeTask.class);

    @Autowired
    private ExchangeRemoteService exchangeRemoteService;

    @Scheduled(cron = "0 */10 * * * ?")
    public void updateSymbolRate(){
        logger.info("================================================updateSymbolRate start==============================================");
        exchangeRemoteService.updateSymbolRate();
        logger.info("================================================updateSymbolRate end==============================================");
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void updateSymbol(){
        logger.info("================================================updateSymbol start==============================================");
        exchangeRemoteService.updateSymbol();
        logger.info("================================================updateSymbol end==============================================");
    }
}
