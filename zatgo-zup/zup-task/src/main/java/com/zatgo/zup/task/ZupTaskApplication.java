package com.zatgo.zup.task;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableScheduling
@EnableEurekaClient
@EnableFeignClients
@EnableDiscoveryClient
@MapperScan("com.zatgo.zup.task.mapper")
@ComponentScan(basePackages="com.zatgo.zup")
@EnableTransactionManagement
public class ZupTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZupTaskApplication.class, args);
	}



}
