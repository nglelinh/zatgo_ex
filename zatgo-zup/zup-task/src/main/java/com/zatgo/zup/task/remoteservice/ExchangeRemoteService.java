package com.zatgo.zup.task.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by 46041 on 2018/9/26.
 */

@FeignClient("zup-exchange-appapi")
public interface ExchangeRemoteService {

    @GetMapping(value = "/exchange/task/symbol/rateTask")
    void updateSymbolRate();

    @GetMapping(value = "/exchange/task/symbolTask")
    void updateSymbol();
}
