package com.zatgo.zup.task.remoteservice;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zatgo.zup.common.model.CheckoutOrderData;
import com.zatgo.zup.common.model.ResponseData;

@FeignClient("zup-payment")
public interface OrderReomoteService {

	@RequestMapping(value = "/payerror",method = RequestMethod.GET)
	@ResponseBody
	ResponseData<List<CheckoutOrderData>> selectPayErrorOrder();

	@RequestMapping(value = "/refunderror",method = RequestMethod.GET)
	@ResponseBody
	ResponseData<List<CheckoutOrderData>> selectRefundErrorOrder();


}
