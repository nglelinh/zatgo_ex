package com.zatgo.zup.task.transactiontask;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CheckoutOrderData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.task.remoteservice.OrderReomoteService;

@Component
public class OrderPayTask {

	private static final Logger logger = LoggerFactory.getLogger(OrderPayTask.class);

	@Autowired
	private OrderReomoteService orderReomoteService;

	public void orderTask(){
		ResponseData<List<CheckoutOrderData>> listResponseData = orderReomoteService.selectPayErrorOrder();
		if(listResponseData != null && listResponseData.getCode().equals(BusinessExceptionCode.SUCCESS_CODE)){
			List<CheckoutOrderData> data = listResponseData.getData();
			for (CheckoutOrderData order : data) {
				String orderId = order.getOrderId();
				
			}
		}
	}

}
