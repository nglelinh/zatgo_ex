package com.zatgo.zup.task.remoteservice;

import com.zatgo.zup.common.model.IssuedCoinInfoData;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("zup-wallet")
public interface CoinInfoRemoteService {

	@RequestMapping(value = "/{type}",method = RequestMethod.GET )
	@ResponseBody
	ResponseData<IssuedCoinInfoData> selectByCoinType(@PathVariable("type") String type);



}
