package com.zatgo.zup.task.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by 46041 on 2018/7/25.
 */
@FeignClient("zup-mining")
public interface MineralPoolService {

    @PostMapping(value = "/mining/create")
    @ResponseBody
    void createMineralPoolRecord();


    @GetMapping(value = "/mining/updateComputeSortTask")
    @ResponseBody
    void updateComputeSortTask();
}
