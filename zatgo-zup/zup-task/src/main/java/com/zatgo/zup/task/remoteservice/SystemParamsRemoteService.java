package com.zatgo.zup.task.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SysParams;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("zup-wallet")
public interface SystemParamsRemoteService {

	@RequestMapping(value = "/wallet/system/bycode/{code}",method = RequestMethod.GET)
	@ResponseBody
	ResponseData<SysParams> selectByCode(@PathVariable("code") String code);

}
