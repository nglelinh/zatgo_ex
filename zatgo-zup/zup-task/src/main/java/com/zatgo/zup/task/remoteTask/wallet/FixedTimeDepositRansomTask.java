package com.zatgo.zup.task.remoteTask.wallet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.zatgo.zup.task.remoteservice.FixedTimeDepositRemoteService;
import com.zatgo.zup.task.remoteservice.PaymentService;

/**
 * Created by 46041 on 2018/7/25.
 */
@Component
public class FixedTimeDepositRansomTask {

    private static final Logger logger = LoggerFactory.getLogger(FixedTimeDepositRansomTask.class);

    @Autowired
    private FixedTimeDepositRemoteService service;

    @Scheduled(cron = "0 0 */1 * * ?")
    public void ransomCheckTask(){
        logger.info("================================================ransomCheckTask start==============================================");
        service.ransomCheckTask();
        logger.info("================================================ransomCheckTask end==============================================");
    }
}
