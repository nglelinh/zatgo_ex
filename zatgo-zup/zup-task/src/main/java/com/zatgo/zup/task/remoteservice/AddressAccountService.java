package com.zatgo.zup.task.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wallet.WalletAccountData;

@FeignClient("zup-wallet")
public interface AddressAccountService {

	@RequestMapping(value = "wallet/account/aggregate/{address}/{type}",method = RequestMethod.GET )
	@ResponseBody
	ResponseData<WalletAccountData> selectByAddressAndCoinType(@PathVariable("address") String address
			, @PathVariable("type") String type);




}
