package com.zatgo.zup.task.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.zatgo.zup.common.model.ResponseData;


@FeignClient("zup-wallet")
public interface FixedTimeDepositRemoteService {

    @GetMapping(value = "/wallet/fixedTimeDeposit/task/ransomCheckTask")
    ResponseData<Boolean> ransomCheckTask();

}
