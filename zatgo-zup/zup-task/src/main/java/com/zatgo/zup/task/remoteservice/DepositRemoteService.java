package com.zatgo.zup.task.remoteservice;

import java.math.BigDecimal;
import java.util.List;

import com.zatgo.zup.common.model.DepositInsertParams;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import com.zatgo.zup.common.model.DepositRecordData;
import com.zatgo.zup.common.model.ResponseData;

@FeignClient("zup-wallet")
public interface DepositRemoteService {
	@RequestMapping(value = "/wallet/deposit/record",method = RequestMethod.POST)
	ResponseData<Object> insertDepositRecored(@RequestBody DepositInsertParams params);
	@RequestMapping(value = "/wallet/deposit/record/unconfirmed",method = RequestMethod.GET)
	List<DepositRecordData> selectUnConfirmed();

	@RequestMapping(value = "/wallet/deposit/increase",method = RequestMethod.PUT)
	ResponseData<Object> deposit(@RequestParam("depositId") String depositId);
}
