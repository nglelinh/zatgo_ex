package com.zatgo.zup.wallet.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class FixedTimeDepositSubscParams {

	@ApiModelProperty(value = "产品ID",required = true)
	private String prodId;
	
	@ApiModelProperty(value = "订购数量",required = true)
	private  BigDecimal subscNum;

	public String getProdId() {
		return prodId;
	}

	public void setProdId(String prodId) {
		this.prodId = prodId;
	}

	public BigDecimal getSubscNum() {
		return subscNum;
	}

	public void setSubscNum(BigDecimal subscNum) {
		this.subscNum = subscNum;
	}
	
	
}
