package com.zatgo.zup.wallet.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.web3j.protocol.Web3j;

import com.zatgo.zup.common.model.Commands;
import com.zatgo.zup.common.utils.ArgsUtils;
import com.zatgo.zup.wallet.blockchain.BtcService;
import com.zatgo.zup.wallet.blockchain.EthService;
import com.zatgo.zup.wallet.blockchain.QtumService;
import com.zatgo.zup.wallet.entity.TransactionRecord;
import com.zatgo.zup.wallet.mapper.AccountAddressMapper;
import com.zatgo.zup.wallet.mapper.TransactionRecordMapper;
import com.zatgo.zup.wallet.model.LocalTransaction;
import com.zatgo.zup.wallet.model.SendParams;
import com.zatgo.zup.wallet.model.TxRecord;
import com.zatgo.zup.wallet.service.TransactionService;
import com.zatgo.zup.wallet.utils.JsonRpcClient;

@Service
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	private JsonRpcClient jsonRpcClient;
	@Autowired
	private Web3j web3j;

	@Autowired
	private AccountAddressMapper accountAddressMapper;
//	@Autowired
//	private QtumService qtumService;
//	@Autowired
//	private EthService ethService;
//	@Autowired
//	private BtcService btcService;
	@Autowired
	private TransactionRecordMapper transactionRecordMapper;
	@Override
	public List<TxRecord> listAccountTransactions(String accountId) {


		return null;
	}

	@Override
	public TxRecord getTransactionByTransactionId(String accountId, String transactionId) {
		try {
			Object invoke = jsonRpcClient.invoke(Commands.GET_TRANSACTION, ArgsUtils.asList(transactionId), Object.class);
		} catch (Throwable throwable) {
			throwable.printStackTrace();
		}
		return null;
	}

	@Override
	public LocalTransaction sendMoney(SendParams params, String accountId) {
//		String currency = params.getCurrency();
//		String transactionId = null;
//		if(currency.equals(CurrencyEnum.QTUM.toString())){
//			transactionId = qtumService.sendTransaction(params);
//		} else if(currency.equals(CurrencyEnum.ZAT.toString())){
//			transactionId = qtumService.sendContractTransaction(params);
//		} else if(currency.equals(CurrencyEnum.ETH.toString())){
//			transactionId = ethService.sendTransaction(params);
//		}
//		TransactionRecord record = new TransactionRecord();
//		record.setAmount(params.getAmount().doubleValue());
//		record.setToAddress(params.getTo());
//		record.setFromAddress(params.getFrom());
//		record.setIsWebpay(params.getWebPay());
//		//record.setStatus(TxStatus.confirming.getCode());
//		transactionRecordMapper.insertSelective(record);
		return null;
	}

	@Override
	public LocalTransaction transferMoneyBetweenAccounts(SendParams params, String accountId) {
		return null;
	}

	@Override
	public LocalTransaction requestMoney(SendParams params, String accountId) {
		return null;
	}

	@Override
	public void completeRequestMoney(String accountId, String transactionId) {

	}

	@Override
	public void cancelRequestMoney(String accountId, String transactionId) {

	}
}
