package com.zatgo.zup.wallet.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	public static Logger logger = LoggerFactory.getLogger(HelloController.class);
	public static Log log = LogFactory.getLog(HelloController.class);
			
	@RequestMapping("/wallet/hello/{fallback}")
    public String hello(@PathVariable("fallback") String fallback){
		logger.info("调用方法hello");
		if("1".equals(fallback)){
            throw new RuntimeException("...");
        }
        return "wallet-hello";
    }

}
