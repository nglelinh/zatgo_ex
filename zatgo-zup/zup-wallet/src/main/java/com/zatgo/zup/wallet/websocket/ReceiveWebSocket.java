package com.zatgo.zup.wallet.websocket;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.wallet.utils.WebSocketTokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/ws/wallet/appwebsocket/{token}")
@Component
public class ReceiveWebSocket implements ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(ReceiveWebSocket.class);
	private Session session;
	private static ApplicationContext applicationContext;
	public void setApplicationContext(ApplicationContext context){
		applicationContext = context;
	}
	public volatile static ConcurrentHashMap<String,Session> cacheMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String,String> userCache = new ConcurrentHashMap<>();

	@OnOpen
	public void onOpen(@PathParam("token")String token,Session session){
		this.session =session;
		WebSocketTokenUtils tokenUtils = applicationContext.getBean(WebSocketTokenUtils.class);
		AuthUserInfo authUserInfo = tokenUtils.getUserInfo(token);
		if(authUserInfo == null){
			try {
				session.close();
			} catch (IOException e) {
				logger.error("close socket error",e);
			}
		}
		cacheMap.put(authUserInfo.getUserId(),session);
		userCache.put(token,authUserInfo.getUserId());

	}
	@OnClose
	public void onClose(@PathParam("token") String token){
		String userId = userCache.get(token);
		if(userId != null) {
			cacheMap.remove(userId);
		}
		logger.info("websocket disconnect");
	}
	public void sendMessage(String msg)  {
		try {
			this.session.getBasicRemote().sendText(msg);
		} catch (IOException e) {
			logger.error("send websocket error",e);
		}
	}
	@OnMessage
	public void onMessage(String message,Session session) throws IOException {

		sendMessage("websocket connect");
	}









}
