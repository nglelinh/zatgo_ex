package com.zatgo.zup.wallet.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.wallet.entity.AccountAddress;
import com.zatgo.zup.wallet.entity.Address;
import com.zatgo.zup.wallet.model.DepositAddressParams;
import com.zatgo.zup.wallet.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(value = "/address",description = "地址相关")
@RestController
@RequestMapping(value = "/wallet/address")
public class AddressController extends BaseController {
	@Autowired
	private AddressService addressService;
	@ApiOperation(value = "获取账户地址列表")
	@RequestMapping(value = "/list/{accountId}",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<List<String>> listAddresses(@PathVariable("accountId") String accountId){
		return BusinessResponseFactory.createSuccess(addressService.listAddresses(accountId));
	}
	@ApiOperation(value = "获取充值地址")
	@RequestMapping(value = "/deposit",method = RequestMethod.POST )
	@ResponseBody
	public ResponseData<String> createDepositAddress(@RequestBody DepositAddressParams params){
		AuthUserInfo userInfo = getUserInfo();
		String cloudUserId = userInfo.getCloudUserId();
		if (addressService.checkCoinType(cloudUserId, params.getCoinType(), params.getNetworkType())) {
			AccountAddress depositAddress = addressService.createDepostAddressV2(params);
			return BusinessResponseFactory.createSuccess(depositAddress.getAddress());
		}
		return BusinessResponseFactory.createSuccess("");
	}

	@ApiOperation(value = "添加地址")
	@RequestMapping(value = "/add",method = RequestMethod.POST )
	@ResponseBody
	public ResponseData<String> add(@RequestBody JSONArray array){
		List<Address> list = JSONObject.parseArray(JSONArray.toJSONString(array), Address.class);
		addressService.add(list);
		return BusinessResponseFactory.createSuccess("");
	}


	@ApiOperation(value = "获取某个币种未被使用的地址数量")
	@RequestMapping(value = "/remainderAddress",method = RequestMethod.POST )
	@ResponseBody
	public ResponseData<Integer> remainderAddress(@RequestParam("type") String type){
		return BusinessResponseFactory.createSuccess(addressService.checkRemainderAddress(type));
	}
}
