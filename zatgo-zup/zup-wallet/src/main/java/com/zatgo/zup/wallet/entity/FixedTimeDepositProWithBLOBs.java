package com.zatgo.zup.wallet.entity;

public class FixedTimeDepositProWithBLOBs extends FixedTimeDepositPro {
    private String proIntro;

    private String teamIntro;

    public String getProIntro() {
        return proIntro;
    }

    public void setProIntro(String proIntro) {
        this.proIntro = proIntro == null ? null : proIntro.trim();
    }

    public String getTeamIntro() {
        return teamIntro;
    }

    public void setTeamIntro(String teamIntro) {
        this.teamIntro = teamIntro == null ? null : teamIntro.trim();
    }
}