package com.zatgo.zup.wallet.model;

import jnr.ffi.annotations.In;

import java.math.BigDecimal;
import java.util.List;

public class TxRecord {

	private List<From> froms;

	private List<To> tos;

	private String txId;

	private String blockHash;

	private Double fee;

	private Integer createTime;
	private Integer updateTime;


	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public List<From> getFroms() {
		return froms;
	}

	public void setFroms(List<From> froms) {
		this.froms = froms;
	}

	public List<To> getTos() {
		return tos;
	}

	public void setTos(List<To> tos) {
		this.tos = tos;
	}

	public String getTxId() {
		return txId;
	}

	public void setTxId(String txId) {
		this.txId = txId;
	}

	public String getBlockHash() {
		return blockHash;
	}

	public void setBlockHash(String blockHash) {
		this.blockHash = blockHash;
	}

	public Integer getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Integer createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Integer updateTime) {
		this.updateTime = updateTime;
	}
}
