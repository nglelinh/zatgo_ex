package com.zatgo.zup.wallet.TxController;

/**
 * Created by nob.
 */
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.wallet.entity.ExtractRecord;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.mapper.IssuedCoinInfoMapper;
import com.zatgo.zup.wallet.service.ExtractService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nob.
 * wiki: http://wiki.365os.com/pages/viewpage.action?pageId=819516
 * 提币
 */
@RestController
@RequestMapping(value = "/wallet")
public class WithdrawFromNodeController {
	private static final Logger logger = LoggerFactory.getLogger(WithdrawFromNodeController.class);
	@Autowired
	private ExtractService extractService;
    @Autowired
    private IssuedCoinInfoMapper issuedCoinInfoMapper;

    @RequestMapping(value = "/withdrawNotify", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, ? extends Object> withdrawNotify(@RequestBody Map<String, String> paramsMap) {
//        logger.info("request withdrawNotify, paramsMap={}", paramsMap);

        String code = Constants.CODE_SUCCESS;
        String processMsg = "";
        Map responseMap = new HashMap();
        String transId = paramsMap.get("trans_id");
        String symbol = paramsMap.get("symbol");
        String addressTo = paramsMap.get("address_to");
        String txid = paramsMap.get("txid");
        String amount = paramsMap.get("amount");
        String confirm = paramsMap.get("confirm");
        String realFeeStr = paramsMap.get("real_fee");

        BigDecimal realFee = null;
        if (!StringUtils.isEmpty(realFeeStr)) {
            realFee = new BigDecimal(realFeeStr);
        }
        if (transId == null || symbol == null || addressTo == null
                || txid == null || amount == null
                || confirm == null) {
            code = Constants.CODE_ARGS_ILLEGAL;
        } else {
            try {
                if(Integer.valueOf(confirm) >= 1){
                    extractService.updateStatus(transId,realFee, txid);
                }
                code = Constants.CODE_SUCCESS;
            } catch (IllegalArgumentException e) {
                code = Constants.CODE_RPOCESS_FAIL;
                processMsg = e.getMessage();
            } catch (BusinessException e) {
                if (BusinessExceptionCode.REPETITIVE_DATA_PROCESSING.equals(e.getCode())){
                    code = Constants.CODE_SUCCESS;
                }
            } catch (Exception e) {
                code = Constants.CODE_RPOCESS_FAIL;
                processMsg = e.getMessage();
            }
        }
        responseMap.put("errno", code);
        if (StringUtils.isBlank(processMsg)){
            responseMap.put("errmsg", Constants.getMessage(code));
        } else {
            responseMap.put("errmsg", Constants.getMessage(code) + "[" + processMsg + "]");
        }

//        logger.info("response withdrawNotify, result={}", responseMap);
        return responseMap;
    }

    /**
     * 提币审核通过的列表
     * @param paramsMap
     * @return
     */
    @RequestMapping(value = "/withdrawConsume", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ? extends Object> withdrawConsume(@RequestBody Map<String, String> paramsMap) {
//        logger.info("request withdrawConsume, paramsMap={}", paramsMap);
        String code = Constants.CODE_SUCCESS;
        Map responseMap = new HashMap();
        List<Map> withdrawResults = new ArrayList<>();
        String symbol = paramsMap.get("symbol");
        String count = paramsMap.get("count");

        if (count == null) {
            count = "10";
        }
        if (symbol == null) {
            code = Constants.CODE_ARGS_ILLEGAL;
        } else {
            code = Constants.CODE_SUCCESS;
            //获取所有审核通过的提币
            IssuedCoinInfo info = issuedCoinInfoMapper.selectByCoinType(symbol);
            List<ExtractRecord> extractRecords = extractService.listApprovedRecord(symbol.toUpperCase());
	        for (ExtractRecord extractRecord : extractRecords) {
		        Map withdrawMap = new HashMap();
		        withdrawMap.put("trans_id", Long.valueOf(extractRecord.getExtractId()));
		        withdrawMap.put("symbol", extractRecord.getCoinType().toLowerCase());
		        withdrawMap.put("address_to", extractRecord.getExtractAddress());
		        withdrawMap.put("amount", extractRecord.getExtractNumber());
		        withdrawMap.put("fee", extractRecord.getFee());
                withdrawMap.put("from_address",info.getGatherMainAddress());
		        withdrawResults.add(withdrawMap);
	        }
        }
        responseMap.put("errno", code);
        responseMap.put("errmsg", Constants.getMessage(code));
        responseMap.put("data", withdrawResults);
//        logger.info("response withdrawConsume, result={}", responseMap);

        return responseMap;
    }

//    @RequestMapping(value = "/withdrawAudit", method = RequestMethod.POST)
//    public @ResponseBody
//    Map<String, ? extends Object> withdrawAudit(@RequestBody Map<String, String> paramsMap) {
//
//        String code = Constants.CODE_SUCCESS;
//        Map responseMap = new HashMap();
//        Map dataMap = new HashMap();
//        dataMap.put("withdraw_switch", "open");
//        if (checkSign(paramsMap)) {
//            String transId = paramsMap.get("trans_id");
//            String symbol = paramsMap.get("symbol");
//            String addressTo = paramsMap.get("address_to");
//            String amount = paramsMap.get("amount");
//
//            if (transId == null || symbol == null ||
//                    addressTo == null || amount == null) {
//                code = Constants.CODE_ARGS_ILLEGAL;
//            } else {
//                code = Constants.CODE_SUCCESS;
//                boolean auditRet = withdrawService.auditWithdraw(Integer.valueOf(transId),
//                        symbol.trim().toUpperCase(),
//                        addressTo,
//                        new BigDecimal(amount));
//                dataMap.put("trans_id", transId);
//                dataMap.put("is_right", auditRet);
//            }
//        } else {
//            code = Constants.CODE_SIGN_FAIL;
//        }
//
//        responseMap.put("errno", code);
//        responseMap.put("errmsg", Constants.getMessage(code));
//        responseMap.put("data", dataMap);
//
//        return responseMap;
//    }
}
