package com.zatgo.zup.wallet.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class QtumVout implements Serializable {

	private BigDecimal value;

	private String n;

	private QtumScriptPubKey scriptPubKey;

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getN() {
		return n;
	}

	public void setN(String n) {
		this.n = n;
	}

	public QtumScriptPubKey getScriptPubKey() {
		return scriptPubKey;
	}

	public void setScriptPubKey(QtumScriptPubKey scriptPubKey) {
		this.scriptPubKey = scriptPubKey;
	}
}
