package com.zatgo.zup.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class RefundRecord implements Serializable {
    private String refundId;

    private String receiptAccountId;

    private String refundAccountId;

    private String orderId;

    private BigDecimal amount;

    private Date refundDate;

    private static final long serialVersionUID = 1L;

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getReceiptAccountId() {
        return receiptAccountId;
    }

    public void setReceiptAccountId(String receiptAccountId) {
        this.receiptAccountId = receiptAccountId;
    }

    public String getRefundAccountId() {
        return refundAccountId;
    }

    public void setRefundAccountId(String refundAccountId) {
        this.refundAccountId = refundAccountId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(Date refundDate) {
        this.refundDate = refundDate;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        RefundRecord other = (RefundRecord) that;
        return (this.getRefundId() == null ? other.getRefundId() == null : this.getRefundId().equals(other.getRefundId()))
            && (this.getReceiptAccountId() == null ? other.getReceiptAccountId() == null : this.getReceiptAccountId().equals(other.getReceiptAccountId()))
            && (this.getRefundAccountId() == null ? other.getRefundAccountId() == null : this.getRefundAccountId().equals(other.getRefundAccountId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
            && (this.getRefundDate() == null ? other.getRefundDate() == null : this.getRefundDate().equals(other.getRefundDate()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRefundId() == null) ? 0 : getRefundId().hashCode());
        result = prime * result + ((getReceiptAccountId() == null) ? 0 : getReceiptAccountId().hashCode());
        result = prime * result + ((getRefundAccountId() == null) ? 0 : getRefundAccountId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getRefundDate() == null) ? 0 : getRefundDate().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", refundId=").append(refundId);
        sb.append(", receiptAccountId=").append(receiptAccountId);
        sb.append(", refundAccountId=").append(refundAccountId);
        sb.append(", orderId=").append(orderId);
        sb.append(", amount=").append(amount);
        sb.append(", refundDate=").append(refundDate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}