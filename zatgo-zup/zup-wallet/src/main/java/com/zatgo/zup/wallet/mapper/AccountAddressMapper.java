package com.zatgo.zup.wallet.mapper;

import com.zatgo.zup.wallet.entity.AccountAddress;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AccountAddressMapper {
    int deleteByPrimaryKey(String address);

	int insert(AccountAddress record);

	int insertSelective(AccountAddress record);

	AccountAddress selectByPrimaryKey(String addressId);

	int updateByPrimaryKeySelective(AccountAddress record);
	
	@Select("select * from account_address where address=#{address}")
	List<AccountAddress> selectByAddress(@Param("address") String address);

	int updateByPrimaryKey(AccountAddress record);
	
	@Select("select address from account_address where account_id = #{accountId}")
	List<String> selectAddressesByAccountId(@Param("accountId") String accountId);

	List<AccountAddress> selectAddressByUserId(@Param("userId") String userId,@Param("netWorkType") String netWorkType);

	AccountAddress selectFromAccountIdAndAddress(@Param("accountId") String accountId,@Param("address") String address);

	AccountAddress selectByAccountId(@Param("accountId") String accountId);
}