package com.zatgo.zup.wallet.controller;

import com.zatgo.zup.wallet.service.RepairDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 46041 on 2018/9/17.
 */

@RestController
@RequestMapping(value = "/wallet/modifyData")
public class RepairDataController extends BaseController {


    @Autowired
    private RepairDataService repairDataService;

    @RequestMapping(value = "/balance",method = RequestMethod.GET)
    @ResponseBody
    public void repairCloudUserWalletBalance(){
        repairDataService.repairCloudUserWalletBalance();
    }
}
