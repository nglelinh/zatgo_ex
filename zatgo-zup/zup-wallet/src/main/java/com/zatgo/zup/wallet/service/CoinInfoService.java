package com.zatgo.zup.wallet.service;

import com.zatgo.zup.wallet.entity.IssuedCoinInfo;

import java.util.List;

public interface CoinInfoService {

	IssuedCoinInfo selectByCoinType(String coinType, String cloudUserId);

	List<IssuedCoinInfo> selectCoinListByCloudUserId(String cloudUserId);

	IssuedCoinInfo selectSystemCoinInfoByCoinType(String coinType);
}
