package com.zatgo.zup.wallet.service.Impl;

import com.zatgo.zup.wallet.entity.TransactionRecord;
import com.zatgo.zup.wallet.mapper.TransactionRecordMapper;
import com.zatgo.zup.wallet.service.TransactionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionRecordServiceImpl implements TransactionRecordService {
	@Autowired
	private TransactionRecordMapper transactionRecordMapper;
	@Override
	public TransactionRecord queryUnfinishedTx() {
		return null;
	}
}
