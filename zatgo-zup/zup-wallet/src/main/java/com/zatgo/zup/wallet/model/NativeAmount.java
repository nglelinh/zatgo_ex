package com.zatgo.zup.wallet.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class NativeAmount implements Serializable{


	private BigDecimal nativeamount;

	private String currency;

	public BigDecimal getNativeamount() {
		return nativeamount;
	}

	public void setNativeamount(BigDecimal nativeamount) {
		this.nativeamount = nativeamount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
