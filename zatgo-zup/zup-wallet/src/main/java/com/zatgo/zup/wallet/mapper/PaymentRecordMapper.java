package com.zatgo.zup.wallet.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.github.pagehelper.Page;
import com.zatgo.zup.wallet.entity.PaymentRecord;

public interface PaymentRecordMapper {
    int deleteByPrimaryKey(String paymentId);

    int insert(PaymentRecord record);

    int insertSelective(PaymentRecord record);

    PaymentRecord selectByPrimaryKey(String paymentId);

    int updateByPrimaryKeySelective(PaymentRecord record);

    int updateByPrimaryKey(PaymentRecord record);
    
    Page<PaymentRecord> selectRecordPageByUserId(@Param("userId")String userId);
    
    Page<PaymentRecord> selectRecordPageByAccountId(@Param("accountId")String accountId);

    List<PaymentRecord> selectRecordByUserId(@Param("userId")String userId);
    
    List<PaymentRecord> selectRecordByAccountId(@Param("accountId")String accountId);
    
//    @Select("select * from payment_record where order_id = #{orderId} and pay_user_id = #{payUserId}")
    PaymentRecord selectRecordByOrderId(@Param("orderId") String orderId,@Param("payUserId") String payUserId);

    @Select("select * from payment_record where order_id = #{orderId} and receipt_user_id = #{receiptUserId}")
    PaymentRecord selectRecordByOrderIdAndReceiptUserId(@Param("orderId") String orderId,
                                                        @Param("receiptUserId") String receiptUserId);
    
    PaymentRecord selectRecordByUserIdAndBusinessNumberAndPaymentType(@Param("payUserId") String payUserId, 
    		@Param("orderId") String orderId, @Param("businessNumber") String businessNumber, 
    		@Param("paymentType") Byte paymentType);

}