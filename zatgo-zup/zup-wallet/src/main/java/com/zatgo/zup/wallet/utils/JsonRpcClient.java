package com.zatgo.zup.wallet.utils;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Component
public class JsonRpcClient {
	@Value("${JsonRpcClient.userName}")
	private String userName;
	@Value("${JsonRpcClient.passWord}")
	private String passWord;
	@Value("${JsonRpcClient.address}")
	private String address;
	private JsonRpcHttpClient client;
	@PostConstruct
	public void init() throws MalformedURLException {
		client = new JsonRpcHttpClient(new URL(address));
		String cred = Base64
				.encodeBase64String((userName + ":" + passWord).getBytes());
		Map<String, String> headers = new HashMap<String, String>(1);
		headers.put("Authorization", "Basic " + cred);
		client.setHeaders(headers);
	}

	public JsonRpcHttpClient getClient() {
		return client;
	}

	public void setClient(JsonRpcHttpClient client) {
		this.client = client;
	}

	public <T> T invoke(String methodName, Object argument, Class<T> clazz) throws Throwable {
		return client.invoke(methodName,argument,clazz);
	}

	public <T> T invoke(String methodName, Object argument, Class<T> clazz,Map<String, String> extraHeaders) throws Throwable {
		return client.invoke(methodName, argument, clazz, extraHeaders);
	}


}
