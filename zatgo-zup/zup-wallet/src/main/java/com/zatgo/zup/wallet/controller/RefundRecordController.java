package com.zatgo.zup.wallet.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.wallet.service.RefundRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping(value = "/wallet/refund")
@Api(value = "/wallet/refund",description = "交易记录查询接口",produces = MediaType.APPLICATION_JSON_VALUE)
public class RefundRecordController {

	@Autowired
	private RefundRecordService refundRecordService;

	@RequestMapping(value = "/record/{orderId}",method = RequestMethod.GET)
	@ApiOperation(value = "转账支付", notes = "转账支付", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<BigDecimal> sumRefund(@PathVariable("orderId") String orderId){
		return BusinessResponseFactory.createSuccess(refundRecordService.getOrderRefundMoney(orderId));
	}



}
