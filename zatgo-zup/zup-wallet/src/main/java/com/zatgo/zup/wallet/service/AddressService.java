package com.zatgo.zup.wallet.service;

import java.util.List;

import com.zatgo.zup.wallet.entity.AccountAddress;
import com.zatgo.zup.wallet.entity.Address;
import com.zatgo.zup.wallet.model.DepositAddressParams;

public interface AddressService {

	List<String> listAddresses(String accountId);

	/**
	 * 获取 充值地址
	 * @param params
	 * @return
	 */
//	AccountAddress createDepositAddress(DepositAddressParams params);

	AccountAddress createDepostAddressV2(DepositAddressParams params);

	/**
	 * 判断当前云用户是否开放当前币种
	 */
	Boolean checkCoinType(String cloudUserId, String coinType, String networkType);

	void add(List<Address> list);

	Integer checkRemainderAddress(String type);
}
