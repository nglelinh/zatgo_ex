package com.zatgo.zup.wallet.utils;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class WebSocketTokenUtils {

	@Autowired
	private RedisTemplate redisTemplate;

	public AuthUserInfo getUserInfo(String token){
		AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
		if(authUserInfo == null) {
			throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
		}
		return authUserInfo;
	}



}
