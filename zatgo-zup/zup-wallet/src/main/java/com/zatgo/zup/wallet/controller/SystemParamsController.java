package com.zatgo.zup.wallet.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.entity.SysParams;
import com.zatgo.zup.wallet.service.SystemParamsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@Api(value = "/wallet/system",description = "系统参数接口")
@RestController
@RequestMapping(value = "/wallet/system")
public class SystemParamsController extends BaseController{
	@Autowired
	private SystemParamsService systemParamsService;
	@RequestMapping(value = "/cointypes",method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "查询支持币种", notes = "查询支持币种", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData<List<IssuedCoinInfo>> queryCoinTypes(){
		AuthUserInfo userInfo = getUserInfo();
		return BusinessResponseFactory.createSuccess(systemParamsService.queryCoinTypes(userInfo.getCloudUserId()));
	}

	@RequestMapping(value = "/coin/rate/{coinType}",method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "查询币种价格", notes = "查询币种价格", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData<BigDecimal> queryCoinRate(@PathVariable("coinType")String coinType){
		return BusinessResponseFactory.createSuccess(systemParamsService.queryCoinRate(coinType));
	}
	@RequestMapping(value = "/bycode/{code}",method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<SysParams> selectByCode(@PathVariable("code") String code){
		return BusinessResponseFactory.createSuccess(systemParamsService.selectByCode(code));
	}
}
