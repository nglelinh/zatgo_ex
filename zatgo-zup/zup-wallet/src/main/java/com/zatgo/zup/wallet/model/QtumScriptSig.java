package com.zatgo.zup.wallet.model;

import java.io.Serializable;

public class QtumScriptSig implements Serializable {

	private String asm;
	
	private String hex;

	public String getAsm() {
		return asm;
	}

	public void setAsm(String asm) {
		this.asm = asm;
	}

	public String getHex() {
		return hex;
	}

	public void setHex(String hex) {
		this.hex = hex;
	}
}
