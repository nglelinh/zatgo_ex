package com.zatgo.zup.wallet.controller;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.DepositInsertParams;
import com.zatgo.zup.common.model.DepositRecordData;
import com.zatgo.zup.common.model.MiningDepositInsertParams;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wallet.DepositRecordByCloudUserParams;
import com.zatgo.zup.wallet.entity.DepositRecord;
import com.zatgo.zup.wallet.service.DepositService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/wallet/deposit")
@Api(value = "/wallet/deposit",description = "充值接口")
public class DepositController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(DepositController.class);
	
	@Autowired
	private DepositService depositService;

	@RequestMapping(value = "/record",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Object> insertDepositRecored(@RequestBody DepositInsertParams params){
		depositService.insertDepositRecored(params);
		return BusinessResponseFactory.createSuccess(null);
	}

	@PostMapping(value = "/miningRecord")
	@ResponseBody
	public ResponseData<Object> insertMiningDepositRecored(@RequestBody MiningDepositInsertParams params){
		depositService.insertMiningDepositRecored(params);
		return BusinessResponseFactory.createSuccess(null);
	}

	@RequestMapping(value = "/record/unconfirmed",method = RequestMethod.GET)
	@ResponseBody
	public List<DepositRecordData> selectUnConfirmed(){
		List<DepositRecord> records = depositService.selectUnConfirmed();
		List<DepositRecordData> recordDatas = new ArrayList<>();
		for(DepositRecord record:records) {
			DepositRecordData recordData = new DepositRecordData();
			try {
				BeanUtils.copyProperties(recordData, record);
				recordDatas.add(recordData);
			} catch (IllegalAccessException e) {
				logger.error("",e);
			} catch (InvocationTargetException e) {
				logger.error("",e);
			}
		}
		
		return recordDatas;
	}

	@RequestMapping(value = "/record/list",method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "充值记录查询", notes = "充值记录查询", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData<List<DepositRecord>> listRecordByUserId(){
		AuthUserInfo userInfo = getUserInfo();
		List<DepositRecord> result = depositService.listRecordByUserId(userInfo.getUserId(), userInfo.getCloudUserId());
		return BusinessResponseFactory.createSuccess(result);
	}

	@RequestMapping(value = "/increase",method = RequestMethod.PUT)
	@ResponseBody
	public ResponseData<Object> deposit(@RequestParam("depositId") String depositId){
		depositService.deposit(depositId);
		return BusinessResponseFactory.createSuccess(null);
	}


	@RequestMapping(value = "/clouduser/record",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<PageInfo<DepositRecordData>> selectDepositRecordByCloudUser(@RequestBody DepositRecordByCloudUserParams params){
		AuthUserInfo userInfo = getUserInfo();
		PageInfo<DepositRecordData>  pageInfo = depositService.selectDepositRecordByCloudUser(userInfo.getCloudUserId(), params);
		
		return BusinessResponseFactory.createSuccess(pageInfo);
	}


}
