package com.zatgo.zup.wallet.service.Impl;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.wallet.entity.CoinPresentRecord;
import com.zatgo.zup.wallet.entity.PaymentRecord;
import com.zatgo.zup.wallet.entity.WalletAccount;
import com.zatgo.zup.wallet.mapper.CoinPresentRecordMapper;
import com.zatgo.zup.wallet.mapper.PaymentRecordMapper;
import com.zatgo.zup.wallet.mapper.WalletAccountMapper;
import com.zatgo.zup.wallet.remoteservice.UserRemoteService;
import com.zatgo.zup.wallet.service.AccountService;
import com.zatgo.zup.wallet.service.PresentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class PresentServiceImpl implements PresentService {

	@Autowired
	private CoinPresentRecordMapper coinPresentRecordMapper;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private WalletAccountMapper walletAccountMapper;
	
	@Autowired
	private UserRemoteService userRemoteService;
	
	@Autowired
	private PaymentRecordMapper paymentRecordMapper;

	@Autowired
	private RedisLockUtils redisLockUtils;
	@Override
	@Transactional
	public void receive(String userName, String verifyCode, String cloudUserId) {
		if(userName == null || userName.trim().equals("")) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		
		ResponseData<UserData> responseData = userRemoteService.getUser(userName, cloudUserId);
		if(responseData.getData() == null) {
			throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		
		//云用户管理员不能送币
		if(responseData.getData().getIsCloudManager().equals(new Byte("1"))) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}

		if(verifyCode == null) {
			verifyCode = "";
		}
		String userId = responseData.getData().getUserId();
		List<CoinPresentRecord> list = coinPresentRecordMapper.selectNotReceiveByVerifyCode(userName, verifyCode);
		for(CoinPresentRecord record:list) {
			int updateNum = coinPresentRecordMapper.updateReceive(record.getPresentId());
			if(updateNum != 0) {

				WalletAccount walletAccount = walletAccountMapper.selectByUserIdAndType(userId, record.getCoinType());
				if(walletAccount ==null) {
					CreateAccountParams createAccount = new CreateAccountParams();
					createAccount.setCurrencyType(record.getCoinType());
					createAccount.setNetworkType(record.getCoinNetworkType());
					createAccount.setUserId(userId);
					createAccount.setName(record.getCoinType().toString());
					walletAccount = accountService.createAccount(createAccount);
				}
				
				BigDecimal bdCoinNum = new BigDecimal(record.getCoinNumber());

				//获取云用户管理员userId
				ResponseData<UserData> res = userRemoteService.getManageUserId(
						userId,BusinessEnum.CloudManageUserType.PRESENT.getType().toString());
				if (!res.isSuccessful()){
					throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
				}
				UserData cloudManageUserData = res.getData();
				
				//给云钱包扣钱
				WalletAccount yunWallet = walletAccountMapper.selectByUserIdAndTypeForUpdate(
						cloudManageUserData.getUserId(), walletAccount.getCoinType());
				if(yunWallet == null){
					CreateAccountParams yunAccount = new CreateAccountParams();
					yunAccount.setName(record.getCoinType());
					yunAccount.setUserId(cloudManageUserData.getUserId());
					yunAccount.setCurrencyType(record.getCoinType());
					yunAccount.setNetworkType(record.getCoinNetworkType());
					yunWallet = accountService.createAccount(yunAccount);
				}
				walletAccountMapper.updateBalance(yunWallet.getAccountId(), bdCoinNum.negate());
				
				//用户加钱
				walletAccountMapper.updateBalance(walletAccount.getAccountId(),bdCoinNum);

				PaymentRecord paymentRecord = new PaymentRecord();
				String paymentId = UUIDUtils.getUuid();
				paymentRecord.setPaymentId(paymentId);
				paymentRecord.setAmount(bdCoinNum);
				paymentRecord.setOrderId(null);
				paymentRecord.setPaymentDate(new Date());
				paymentRecord.setPaymentAccountId(null);
				paymentRecord.setReceiptAccountId(walletAccount.getAccountId());
				paymentRecord.setPayUserId(cloudManageUserData.getUserId());
				paymentRecord.setPayUserName(record.getPresentReason());
				paymentRecord.setPaymentAccountId(yunWallet.getAccountId());
				paymentRecord.setReceiptUserName(responseData.getData().getUserName());
				paymentRecord.setReceiptUserId(responseData.getData().getUserId());
				paymentRecord.setCoinType(record.getCoinType());
				paymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.present.getCode());
				paymentRecordMapper.insertSelective(paymentRecord);
			}
		}
	}
	@Override
	public List<CoinPresentRecord> findUnPresentList(String cloudUserId) {
		List<CoinPresentRecord> list = coinPresentRecordMapper.selectNotReceiveByAll(cloudUserId);
		return list;
	}

}
