package com.zatgo.zup.wallet.service;

import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.PaymentMessage;
import com.zatgo.zup.common.model.RefundParams;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.model.ExchangeUpdateBalanceParams;
import com.zatgo.zup.common.model.LockWalletBalanceParams;
import com.zatgo.zup.wallet.entity.PaymentRecord;

import java.util.List;

public interface PayService {

	/**
	 * 支付接口
	 * @param params 收款信息
	 * @param userId 付款方
	 */
	String pay(PayParams params,String userId,BusinessEnum.PaymentRecordType payType);


	String collectionCode(String userId);


	/**
	 * 转账接口
	 * @param params 收款信息
	 * @param userId 付款方
	 * @return
	 */
	String transfer(PayParams params,String userId);

	String refund(RefundParams params);
	
	/**
	 * 撮合变更余额
	 * @param params
	 * @return BusinessExceptionCode
	 */
	void exchangeMatchingBalance(List<ExchangeUpdateBalanceParams> params);
	
	/**
	 * 冻结解冻功能
	 * @param param
	 */
	void lockBalanceUpdate(LockWalletBalanceParams param);

}
