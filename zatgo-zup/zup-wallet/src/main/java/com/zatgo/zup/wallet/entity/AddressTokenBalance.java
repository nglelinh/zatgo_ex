package com.zatgo.zup.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class AddressTokenBalance implements Serializable{

	private String address;

	private BigDecimal tokenBalance;
	
	private String accountId;
	
	private String coinType;

	private BigDecimal coinBalance;

	public BigDecimal getCoinBalance() {
		return coinBalance;
	}

	public void setCoinBalance(BigDecimal coinBalance) {
		this.coinBalance = coinBalance;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BigDecimal getTokenBalance() {
		return tokenBalance;
	}

	public void setTokenBalance(BigDecimal tokenBalance) {
		this.tokenBalance = tokenBalance;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}
}
