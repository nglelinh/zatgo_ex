package com.zatgo.zup.wallet.service;

import java.util.List;

import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.wallet.WalletAccountData;
import com.zatgo.zup.wallet.entity.WalletAccount;

public interface AccountService {

	List<WalletAccountData> listAccount(String userId, String CloudUserId);

	WalletAccount selectByAccountId(String accountId);

	WalletAccountData getAccountByAccountId(String accountId);

	WalletAccount createAccount(CreateAccountParams params);

	String setAccountAsPrimary(String accountId,String userId);

	void updateAccountName(String accountId,String name);

	void deleteAccount(String accountId);

	List<WalletAccountData> selectByUserId(String userId);

	WalletAccount selectByUserIdAndType(String userId,String type);


	WalletAccount selectUserPrimaryAccount(String userId);

	WalletAccount selectByAddressAndCoinType(String address,String coinType);

	List<WalletAccount> selectAccountNeedAggregate(String coinType);

}
