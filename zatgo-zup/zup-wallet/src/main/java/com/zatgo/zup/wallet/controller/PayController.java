package com.zatgo.zup.wallet.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ExchangeUpdateBalanceParams;
import com.zatgo.zup.common.model.LockWalletBalanceParams;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.RefundParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.wallet.service.PayService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api(value = "/wallet",description = "支付接口",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(value = "/wallet")
public class PayController extends BaseController{
	@Autowired
	private PayService payService;

	@RequestMapping(value = "/transfer",method = RequestMethod.POST)
	@ApiOperation(value = "转账支付", notes = "转账支付", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<String> transfer(@RequestBody PayParams params){
		AuthUserInfo userInfo = getUserInfo();
		String userId = userInfo.getUserId();
		params.setPayUserName(userInfo.getUserName());
		return BusinessResponseFactory.createSuccess(payService.transfer(params,userId));
	}
	@RequestMapping(value = "/pay",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<String> pay(@RequestBody PayParams params){
		String userId = getUserInfo().getUserId();
		return BusinessResponseFactory.createSuccess(payService.pay(params,userId,BusinessEnum.PaymentRecordType.orderPay));
	}
	@RequestMapping(value = "/refund",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<String> refund(@RequestBody RefundParams params){
		return BusinessResponseFactory.createSuccess(payService.refund(params));
	}

//	@RequestMapping(value = "/sign/pay/{payUserId}",method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseData<String> signPay(@RequestBody PayParams params, @PathVariable("payUserId") String payUserId){
//		return BusinessResponseFactory.createSuccess(payService.pay(params,payUserId,BusinessEnum.PaymentRecordType.orderPay));
//	}
	
	@ApiOperation(value = "更新账户冻结余额")
	@RequestMapping(value = "/exchange/update/lockbalance", method = RequestMethod.POST)
	public ResponseData<Object> updatelockBalance(@RequestBody LockWalletBalanceParams params) {
		payService.lockBalanceUpdate(params);
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "撮合账户冻结余额")
	@RequestMapping(value = "/exchange/matching/lockbalance", method = RequestMethod.POST)
	public ResponseData<Object> matchinglockBalance(@RequestBody List<ExchangeUpdateBalanceParams> params) {
		payService.exchangeMatchingBalance(params);
		return BusinessResponseFactory.createSuccess(null);
	}

	@ApiOperation(value = "内部指定用户支付")
	@RequestMapping(value = "/pay/{payUserId}",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<String> payByUser(@PathVariable String payUserId,@RequestBody PayParams params){
		return BusinessResponseFactory.createSuccess(payService.pay(params,payUserId,BusinessEnum.PaymentRecordType.gamePay));
	}
}
