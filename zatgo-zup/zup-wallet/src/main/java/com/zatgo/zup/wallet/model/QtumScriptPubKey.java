package com.zatgo.zup.wallet.model;

import java.io.Serializable;
import java.util.List;

public class QtumScriptPubKey implements Serializable{

	private String asm;
	
	private String hex;
	
	private String reqSigs;
	private String type;
	private List<String> addresses;

	public String getAsm() {
		return asm;
	}

	public void setAsm(String asm) {
		this.asm = asm;
	}

	public String getHex() {
		return hex;
	}

	public void setHex(String hex) {
		this.hex = hex;
	}

	public String getReqSigs() {
		return reqSigs;
	}

	public void setReqSigs(String reqSigs) {
		this.reqSigs = reqSigs;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<String> addresses) {
		this.addresses = addresses;
	}
}
