package com.zatgo.zup.wallet.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;
import com.zatgo.zup.wallet.entity.DepositRecord;

public interface DepositRecordMapper {
    int deleteByPrimaryKey(String depositId);

    int insert(DepositRecord record);

    int insertSelective(DepositRecord record);

    DepositRecord selectByPrimaryKey(String depositId);

    int updateByPrimaryKeySelective(DepositRecord record);

    int updateByPrimaryKey(DepositRecord record);
//	@Select("select * from deposit_record where deposit_status = 0")
	List<DepositRecord> selectUnConfirmed();
	@Update("update deposit_record set deposit_status = 1 where deposit_id = #{depositId}")
	void confirmedRecord(@Param("depositId") String depositId);

	List<DepositRecord> selectRecordByUserId(@Param("userId") String userId, @Param("type") String type);
	
	@Select("select * from deposit_record where tx_hash = #{txHash}")
	List<DepositRecord> selectRecordByTxHash(@Param("txHash") String txHash);

	DepositRecord selectRecord(@Param("txHash") String txHash,@Param("type") String coinType,@Param("to") String to);

	@Select("SELECT ici.record_id as depositId ,sum(dr.deposit_number) as depositNumber," +
			" dr.coin_type as coinType, ici.coin_network_type as coinNetworkType from deposit_record dr LEFT JOIN issued_coin_info ici ON" +
			" dr.coin_type=ici.coin_type where dr.deposit_status='1' GROUP BY dr.coin_type")
	List<DepositRecord> selectSystemDepositRecord();
	
	Page<DepositRecord> selectDepositRecordByCloudUser(@Param("key") String key,@Param("cloudUserId") String cloudUserId,
			@Param("depositStatus") String depositStatus,@Param("coinType") String coinType);

}