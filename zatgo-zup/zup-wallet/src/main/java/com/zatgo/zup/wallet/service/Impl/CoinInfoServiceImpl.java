package com.zatgo.zup.wallet.service.Impl;

import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.mapper.CloudUserCoinRelMapper;
import com.zatgo.zup.wallet.mapper.IssuedCoinInfoMapper;
import com.zatgo.zup.wallet.service.CoinInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CoinInfoServiceImpl implements CoinInfoService {
	@Autowired
	private IssuedCoinInfoMapper issuedCoinInfoMapper;
	@Autowired
	private CloudUserCoinRelMapper cloudUserCoinRelMapper;

	@Override
	public IssuedCoinInfo selectByCoinType(String coinType, String cloudUserId) {
		//查系统币种设置
		IssuedCoinInfo cloudCoinInfo = null;
		IssuedCoinInfo issuedCoinInfo = issuedCoinInfoMapper.selectByCoinType(coinType);
		if (issuedCoinInfo != null) {
			//查云币种设置
			cloudCoinInfo = cloudUserCoinRelMapper.selectCloudUserCoinType(cloudUserId, coinType);
			if (cloudCoinInfo != null) {
				//系统设置为关闭
				if (issuedCoinInfo.getCloseWithdraw()) {
					cloudCoinInfo.setCloseWithdraw(true);
				}
			}
		}
		return cloudCoinInfo;
	}

	@Override
	public List<IssuedCoinInfo> selectCoinListByCloudUserId(String cloudUserId) {
		return cloudUserCoinRelMapper.selectCoinListByCloudUserId(cloudUserId);
	}

	@Override
	public IssuedCoinInfo selectSystemCoinInfoByCoinType(String coinType) {
		return issuedCoinInfoMapper.selectByCoinType(coinType);
	}
}
