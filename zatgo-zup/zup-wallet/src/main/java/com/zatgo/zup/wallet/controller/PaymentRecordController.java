package com.zatgo.zup.wallet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wallet.PaymentRecordData;
import com.zatgo.zup.common.model.wallet.WalletAccountData;
import com.zatgo.zup.wallet.entity.PaymentRecord;
import com.zatgo.zup.wallet.service.AccountService;
import com.zatgo.zup.wallet.service.PaymentRecordService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/wallet/payment")
@Api(value = "/wallet/payment",description = "交易记录查询接口",produces = MediaType.APPLICATION_JSON_VALUE)
public class PaymentRecordController extends BaseController {
	@Autowired
	private PaymentRecordService paymentRecordService;
	@Autowired
	private AccountService accountService;
	@ResponseBody
	@ApiOperation(value = "查询账户交易记录", notes = "查询账户交易记录", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/account/{accountId}",method = RequestMethod.GET)
	public ResponseData<List<PaymentRecord>> listRecordByAccountId(@PathVariable("accountId") String accountId){
		
		//只能查询自己的交易记录
		AuthUserInfo userInfo = getUserInfo();
		String currentAccountId = null;
		List<WalletAccountData> accounts = accountService.listAccount(userInfo.getUserId(), userInfo.getCloudUserId());
		for(WalletAccountData account: accounts) {
			if(account.getAccountId().equals(accountId)) {
				currentAccountId = account.getAccountId();
				break;
			}
		}
		if(currentAccountId == null) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
		}
		
		
		return BusinessResponseFactory.createSuccess(paymentRecordService.listRecordByAccountId(accountId));
	}
	@ResponseBody
	@ApiOperation(value = "查询用户交易记录", notes = "查询用户交易记录", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public ResponseData<List<PaymentRecord>> listRecordByUserId(){
		AuthUserInfo userInfo = getUserInfo();
		return BusinessResponseFactory.createSuccess(paymentRecordService.listRecordByUserId(userInfo.getUserId()));
	}
	
	
	@ResponseBody
	@ApiOperation(value = "分页查询账户交易记录", notes = "分页查询账户交易记录", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/account/pageinfo/{accountId}/{pageNo}/{pageSize}",method = RequestMethod.GET)
	public ResponseData<PageInfo<PaymentRecord>> listRecordPageByAccountId(@PathVariable("accountId") String accountId,@PathVariable("pageNo") String pageNo, 
			@PathVariable("pageSize") String pageSize){
		//只能查询自己的交易记录
		AuthUserInfo userInfo = getUserInfo();
		String currentAccountId = null;
		List<WalletAccountData> accounts = accountService.listAccount(userInfo.getUserId(), userInfo.getCloudUserId());
		for(WalletAccountData account: accounts) {
			if(account.getAccountId().equals(accountId)) {
				currentAccountId = account.getAccountId();
				break;
			}
		}
		if(currentAccountId == null) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
		}
				
		return BusinessResponseFactory.createSuccess(paymentRecordService.listRecordPageByAccountId(currentAccountId,pageNo,pageSize));
	}
	
	@ResponseBody
	@ApiOperation(value = "分页查询用户交易记录", notes = "分页查询用户交易记录", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/list/pageinfo/{pageNo}/{pageSize}",method = RequestMethod.GET)
	public ResponseData<PageInfo<PaymentRecord>> listRecordPageByUserId(@PathVariable("pageNo") String pageNo, 
			@PathVariable("pageSize") String pageSize){
		AuthUserInfo userInfo = getUserInfo();
		return BusinessResponseFactory.createSuccess(paymentRecordService.listRecordPageByUserId(userInfo.getUserId(),pageNo,pageSize));
	}
	
	@ResponseBody
	@ApiOperation(value = "根据唯一交易编号查询支付信息", notes = "根据唯一交易编号查询支付信息", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/paymentRecord/{tradeNo}",method = RequestMethod.GET)
	public ResponseData<PaymentRecordData> getPaymentRecordByTradeNo(@PathVariable("tradeNo") String tradeNo){
		AuthUserInfo userInfo = getUserInfo();
		return BusinessResponseFactory.createSuccess(paymentRecordService.getPaymentRecordByTradeNo(userInfo.getUserId(),tradeNo));
	}

}

