package com.zatgo.zup.wallet.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.IssuedCoinInfoData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.service.CoinInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Api(value = "/coin",description = "币种相关")
@RestController
@RequestMapping(value = "/wallet/coin")
public class CoinInfoController {

	@Autowired
	private CoinInfoService coinInfoService;

	@ApiOperation(value = "获取币种信息")
	@RequestMapping(value = "/{type}",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<IssuedCoinInfo> selectByCoinType(@PathVariable("type") String type,
														 @RequestParam(value = "cloudUserId", required = false) String cloudUserId){
		return BusinessResponseFactory.createSuccess(coinInfoService.selectByCoinType(type, cloudUserId));
	}


	@ApiOperation(value = "获取币种信息(远程调用)")
	@RequestMapping(value = "/rpc/{type}",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<Map<String, IssuedCoinInfoData>> selectByCoinTypeRPC(@PathVariable("type") String type){
		String[] split = type.split(",");
		Map<String, IssuedCoinInfoData> map = new HashMap();
		for (String t : split){
			IssuedCoinInfo issuedCoinInfo = coinInfoService.selectSystemCoinInfoByCoinType(t);
			IssuedCoinInfoData issuedCoinInfoData = new IssuedCoinInfoData();
			BeanUtils.copyProperties(issuedCoinInfo, issuedCoinInfoData);
			map.put(issuedCoinInfo.getCoinType(), issuedCoinInfoData);
		}
		return BusinessResponseFactory.createSuccess(map);
	}


//	@ApiOperation(value = "云用户钱包余额修数据接口(内网调用)")
//	@RequestMapping(value = "/cloudUser/wallet",method = RequestMethod.GET )
//	@ResponseBody
//	public void modifyCloudUserWalletBalance(){
//
//	}

}
