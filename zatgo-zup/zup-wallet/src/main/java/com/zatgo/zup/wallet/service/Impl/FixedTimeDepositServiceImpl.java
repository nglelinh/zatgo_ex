package com.zatgo.zup.wallet.service.Impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.wallet.entity.FixedDepositSubsc;
import com.zatgo.zup.wallet.entity.FixedDepositSubscExample;
import com.zatgo.zup.wallet.entity.FixedTimeDepositPro;
import com.zatgo.zup.wallet.entity.FixedTimeDepositProWithBLOBs;
import com.zatgo.zup.wallet.mapper.FixedDepositSubscMapper;
import com.zatgo.zup.wallet.mapper.FixedTimeDepositProMapper;
import com.zatgo.zup.wallet.model.FixedTimeDepositAddParams;
import com.zatgo.zup.wallet.model.FixedTimeDepositAdminSearchParams;
import com.zatgo.zup.wallet.model.FixedTimeDepositSubscParams;
import com.zatgo.zup.wallet.remoteservice.UserRemoteService;
import com.zatgo.zup.wallet.service.FixedTimeDepositService;
import com.zatgo.zup.wallet.service.PayService;

@Service
public class FixedTimeDepositServiceImpl implements FixedTimeDepositService {

	private static final Logger logger = LoggerFactory.getLogger(FixedTimeDepositServiceImpl.class);
	@Autowired
	private FixedTimeDepositProMapper fixedTimeDepositProMapper;
	
	@Autowired
	private FixedDepositSubscMapper fixedDepositSubscMapper;
	
	@Autowired
    private RedisUtils redisUtils;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@Autowired
	private PayService payService;
	
	@Autowired
	private UserRemoteService userRemoteService;
	
	@Override
	@Transactional
	public void updatePro(AuthUserInfo adminInfo, FixedTimeDepositAddParams params) {
		FixedTimeDepositProWithBLOBs  pro = fixedTimeDepositProMapper.selectByPrimaryKey(params.getProId());
		if(pro == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		
		if(!pro.getStatus().equals(Byte.valueOf(BusinessEnum.FixedTimeDepositStatus.inputStatus.getCode()))) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_DIRTY_DATA_ERROR);
		}

		try {
			BeanUtils.copyProperties(pro, params);
			pro.setBalanceSubscTotalNum(pro.getSubscTotalNum());
			pro.setCloudUserId(adminInfo.getCloudUserId());
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
		
		fixedTimeDepositProMapper.updateByPrimaryKeyWithBLOBs(pro);

	}

	@Override
	@Transactional
	public void addPro(AuthUserInfo adminInfo, FixedTimeDepositAddParams params) {
		FixedTimeDepositProWithBLOBs pro = new FixedTimeDepositProWithBLOBs();
		try {
			BeanUtils.copyProperties(pro, params);
			pro.setBalanceSubscTotalNum(pro.getSubscTotalNum());
			pro.setCloudUserId(adminInfo.getCloudUserId());
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
		pro.setProId(UUIDUtils.getUuid());
		
		fixedTimeDepositProMapper.insert(pro);
	}

	@Override
	public PageInfo<FixedTimeDepositProWithBLOBs> getAllProByAdmin(AuthUserInfo adminInfo,
			FixedTimeDepositAdminSearchParams params) {
		PageHelper.startPage(Integer.valueOf(params.getPageNo()), Integer.valueOf(params.getPageSize()));
		Page<FixedTimeDepositProWithBLOBs> pageData = fixedTimeDepositProMapper.findPageByStatus(adminInfo.getCloudUserId(), params.getStatus());
		
		PageInfo<FixedTimeDepositProWithBLOBs> pageInfoOrder =
				new PageInfo<FixedTimeDepositProWithBLOBs>(pageData.getPageNum(),pageData.getPageSize(),pageData.getTotal(),pageData.getResult());
		PageHelper.clearPage();
		return pageInfoOrder;
	}

	@Override
	public PageInfo<FixedTimeDepositProWithBLOBs> getReleasePro(AuthUserInfo userInfo,int pageNo,int pageSize) {
		PageHelper.startPage(Integer.valueOf(pageNo), Integer.valueOf(pageSize));
		Page<FixedTimeDepositProWithBLOBs> pageData = fixedTimeDepositProMapper.findPageByStatus(userInfo.getCloudUserId(), 
				Byte.valueOf(BusinessEnum.FixedTimeDepositStatus.releaseStatus.getCode()));
		
		PageInfo<FixedTimeDepositProWithBLOBs> pageInfoOrder =
				new PageInfo<FixedTimeDepositProWithBLOBs>(pageData.getPageNum(),pageData.getPageSize(),pageData.getTotal(),pageData.getResult());
		PageHelper.clearPage();
		return pageInfoOrder;
	}

	@Override
	@Transactional
	public void proSubsc(AuthUserInfo userInfo, FixedTimeDepositSubscParams params) {
		
		//从redis中查询认购信息，如果没有，从数据库同步到redis
		String key = RedisKeyConstants.FIXED_TIME_DEPOSIT_PRO_PRE + params.getProdId();
		FixedTimeDepositProWithBLOBs  pro = null;
		if (redisUtils.hasKey(key)){
			pro = redisUtils.get(key, FixedTimeDepositProWithBLOBs.class);
        } else {
        	redisLockUtils.lock(key);
        	try {
        		if(redisUtils.hasKey(key)) {
        			pro = redisUtils.get(key, FixedTimeDepositProWithBLOBs.class);
        		}else {
        			pro = fixedTimeDepositProMapper.selectByPrimaryKey(params.getProdId());
        			if(pro == null) {
        				throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
        			}
        			redisUtils.put(key, pro);
        		}
        	}finally {
        		redisLockUtils.releaseLock(key);
        	}

        }

		//判断产品状态
		if(!pro.getStatus().equals(Byte.valueOf(BusinessEnum.FixedTimeDepositStatus.releaseStatus.getCode()))) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_DIRTY_DATA_ERROR);
		}
		
		//认购最小数量限制
		if(pro.getMinSubscNum().compareTo(params.getSubscNum()) > 0) {
			throw new BusinessException(BusinessExceptionCode.FIXED_TIME_DEPOSIT_MIN_NUM_ERROR);
		}
		
		//认购最大数量限制
		BigDecimal sumSubsc = BigDecimal.ZERO;
		FixedDepositSubscExample example = new FixedDepositSubscExample();
		example.createCriteria().
			andUserIdEqualTo(userInfo.getUserId()).
			andProIdEqualTo(params.getProdId());
		List<FixedDepositSubsc> subscList = fixedDepositSubscMapper.selectByExample(example);
		for(FixedDepositSubsc subsc:subscList) {
			sumSubsc = sumSubsc.add(subsc.getSubscNum());
		}
		if(pro.getMaxSubscNum().compareTo(sumSubsc.add(params.getSubscNum())) < 0) {
			throw new BusinessException(BusinessExceptionCode.FIXED_TIME_DEPOSIT_MAX_NUM_ERROR);
		}

		//扣减认购余额
		redisLockUtils.lock(key);
		try {
			pro = redisUtils.get(key, FixedTimeDepositProWithBLOBs.class);
			BigDecimal balanceSubscNum = pro.getBalanceSubscTotalNum().subtract(params.getSubscNum());
			if(balanceSubscNum.compareTo(BigDecimal.ZERO) < 0) {
				throw new BusinessException(BusinessExceptionCode.FIXED_TIME_DEPOSIT_NOT_BALANCE_ERROR);
			}
			pro.setBalanceSubscTotalNum(balanceSubscNum);
			redisUtils.put(key, pro);
		}finally {
			redisLockUtils.releaseLock(key);
		}
		
		//认购记录
		FixedDepositSubsc subsc = new FixedDepositSubsc();
		subsc.setProId(params.getProdId());
		subsc.setStatus(Byte.valueOf(BusinessEnum.FixedTimeDepositSubscStatus.subscSuccess.getCode()));
		subsc.setSubscDate(new Date());
		subsc.setSubscNum(params.getSubscNum());
		subsc.setSubscRecordId(UUIDUtils.getUuid());
		subsc.setUserId(userInfo.getUserId());
		fixedDepositSubscMapper.insertSelective(subsc);
		
		
		//支付操作
		ResponseData<UserData> managerUser = userRemoteService.getManageUserByCloudUserId(
				userInfo.getCloudUserId(),BusinessEnum.CloudManageUserType.FIXED_TIME_DEPOSIT_SUBSC.getType().toString());
		if (managerUser == null || !managerUser.isSuccessful() || managerUser.getData() == null){
			throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
		}
		UserData cloudUserManageData = managerUser.getData();
		pay(params.getSubscNum(), subsc.getSubscRecordId(), pro.getCoinType(),
				pro.getCoinNetworkType(), userInfo.getUserName(),
				cloudUserManageData.getUserId(), cloudUserManageData.getUserName(),
				userInfo.getUserId(), BusinessEnum.PaymentRecordType.fixedDepositSubsc);
	}
	
	private void pay(BigDecimal amount, String chekoutOrderId, String coinType,
			 String coinNetworkType, String payUserName, String receiptUserId,
			 String receiptUserName, String payUserId, BusinessEnum.PaymentRecordType paymentRecordType){
		PayParams payFeeParams = new PayParams();
		payFeeParams.setAmount(amount);
		payFeeParams.setCheckoutOrderId(chekoutOrderId);
		payFeeParams.setCoinType(coinType);
		payFeeParams.setNetworkType(coinNetworkType);
		payFeeParams.setPayUserName(payUserName);
		payFeeParams.setReceiptUserId(receiptUserId);
		payFeeParams.setReceiptUserName(receiptUserName);
		payService.pay(payFeeParams, payUserId,paymentRecordType);
	}

	@Override
	public List<FixedDepositSubsc> getProSubscListByUser(AuthUserInfo userInfo) {
		FixedDepositSubscExample example = new FixedDepositSubscExample();
		example.createCriteria().
			andUserIdEqualTo(userInfo.getUserId());
		List<FixedDepositSubsc> subscList = fixedDepositSubscMapper.selectByExample(example);
		return subscList;
	}

	@Override
	public PageInfo<FixedDepositSubsc> getNeedRansomListByPage(int pageNo, int pageSize) {
		PageHelper.startPage(Integer.valueOf(pageNo), Integer.valueOf(pageSize));
		Page<FixedDepositSubsc> pageData = fixedDepositSubscMapper.getNeedRansomListByPage();
				
		PageInfo<FixedDepositSubsc> pageInfoOrder =
				new PageInfo<FixedDepositSubsc>(pageData.getPageNum(),pageData.getPageSize(),pageData.getTotal(),pageData.getResult());
		PageHelper.clearPage();
		return pageInfoOrder;
	}

	@Override
	@Transactional
	public void ransom(String subscRecordId) {
		FixedDepositSubsc subsc = fixedDepositSubscMapper.selectByPrimaryKey(subscRecordId);
		if(!subsc.getStatus().equals(Byte.valueOf(BusinessEnum.FixedTimeDepositSubscStatus.subscSuccess.getCode()))) {
			return;
		}
		
		FixedTimeDepositPro pro = fixedTimeDepositProMapper.selectByPrimaryKey(subsc.getProId());
		
		ResponseData<UserData> userDataResp = userRemoteService.getUserById(subsc.getUserId());
		if(!userDataResp.isSuccessful()) {
			throw new BusinessException(userDataResp.getCode(),userDataResp.getMessage());
		}
		
		//更新状态
		BigDecimal yieldNum = subsc.getSubscNum().multiply(pro.getYieldRate().divide(new BigDecimal(100),8,RoundingMode.HALF_DOWN));
		subsc.setYieldRate(pro.getYieldRate());
		subsc.setYieldNum(yieldNum);
		subsc.setStatus(Byte.valueOf(BusinessEnum.FixedTimeDepositSubscStatus.ransomSuccess.getCode()));
		fixedDepositSubscMapper.updateByPrimaryKeySelective(subsc);
				
		//本金赎回
		ResponseData<UserData> managerUserRansom = userRemoteService.getManageUserByCloudUserId(
				userDataResp.getData().getCloudUserId(),BusinessEnum.CloudManageUserType.FIXED_TIME_DEPOSIT_SUBSC.getType().toString());
		if (managerUserRansom == null || !managerUserRansom.isSuccessful() || managerUserRansom.getData() == null){
			throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
		}
		UserData cloudUserManageDataRansom = managerUserRansom.getData();
		pay(subsc.getSubscNum(), subsc.getSubscRecordId()+"ransom", pro.getCoinType(),
				pro.getCoinNetworkType(), cloudUserManageDataRansom.getUserName(),
				userDataResp.getData().getUserId(), userDataResp.getData().getUserName(),
				cloudUserManageDataRansom.getUserId(), BusinessEnum.PaymentRecordType.fixedDepositSubsc);
		
		//收益发放
		ResponseData<UserData> managerUserYeild = userRemoteService.getManageUserByCloudUserId(
				userDataResp.getData().getCloudUserId(),BusinessEnum.CloudManageUserType.FIXED_TIME_DEPOSIT_YIELD.getType().toString());
		if (managerUserYeild == null || !managerUserYeild.isSuccessful() || managerUserYeild.getData() == null){
			throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
		}
		UserData cloudUserManageDataYeild = managerUserYeild.getData();
		pay(yieldNum, subsc.getSubscRecordId()+"yeild", pro.getCoinType(),
				pro.getCoinNetworkType(), cloudUserManageDataYeild.getUserName(),
				userDataResp.getData().getUserId(), userDataResp.getData().getUserName(),
				cloudUserManageDataYeild.getUserId(), BusinessEnum.PaymentRecordType.fixedDepositSubscYield);
		
		
		
	}

	@Override
	public FixedTimeDepositProWithBLOBs getProById(String cloudUserId, String proId) {
		FixedTimeDepositProWithBLOBs data = fixedTimeDepositProMapper.selectByPrimaryKey(proId);
		return data;
	}


}
