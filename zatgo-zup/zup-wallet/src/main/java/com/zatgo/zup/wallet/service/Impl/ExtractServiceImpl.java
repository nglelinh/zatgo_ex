package com.zatgo.zup.wallet.service.Impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.ExtractParams;
import com.zatgo.zup.common.model.ExtractRecordData;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.model.wallet.ExtractApprovalParams;
import com.zatgo.zup.common.model.wallet.ExtractRecordByCloudUserParams;
import com.zatgo.zup.common.model.wallet.WalletAccountData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.sms.santi.SantiService;
import com.zatgo.zup.wallet.entity.AccountAddress;
import com.zatgo.zup.wallet.entity.ExtractRecord;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.entity.WalletAccount;
import com.zatgo.zup.wallet.mapper.AccountAddressMapper;
import com.zatgo.zup.wallet.mapper.ExtractRecordMapper;
import com.zatgo.zup.wallet.mapper.WalletAccountMapper;
import com.zatgo.zup.wallet.remoteservice.UserRemoteService;
import com.zatgo.zup.wallet.service.AccountService;
import com.zatgo.zup.wallet.service.CoinInfoService;
import com.zatgo.zup.wallet.service.ExtractService;
import com.zatgo.zup.wallet.service.PayService;
import com.zatgo.zup.wallet.service.SystemParamsService;
import com.zatgo.zup.wallet.utils.PasswordVerify;

@Service
public class ExtractServiceImpl implements ExtractService {

	private static final Logger logger = LoggerFactory.getLogger(ExtractServiceImpl.class);
	
	@Autowired
	private WalletAccountMapper walletAccountMapper;
//	@Autowired
//	private QtumService qtumService;
	@Autowired
	private ExtractRecordMapper extractRecordMapper;
	@Autowired
	private PasswordVerify passwordVerify;
	@Autowired
	private AccountAddressMapper accountAddressMapper;
	@Autowired
	private SystemParamsService systemParamsService;
	@Autowired
	private UserRemoteService userRemoteService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private RedisLockUtils redisLockUtils;
	@Value("${user.transfer:false}")
	private String autoTransfer;

	@Value("${notice.admin.coinDynamic.template.id:}")
	private String adminCoinDynamicTemplateId;

	@Value("${notice.admin.coinDynamic.template.content:}")
	private String adminCoinDynamicTemplateContent;

	@Value("${notice.admin.coinDynamic.template.timeout:}")
	private String adminCoinDynamicTemplateTimeout;

	@Value("${notice.admin.phone:}")
	private String adminPhone;

	@Autowired
	private SantiService santiService;

	@Autowired
	private CoinInfoService coinInfoService;
	
	@Autowired
	private PayService payService;

	public final static BigDecimal qtumFee  = new BigDecimal(0.01);
	public final static BigDecimal qtumTokenFee = new BigDecimal(0.1);
	@Override
	@Transactional
	public String extract(ExtractParams params,String userId) {
		
		//用户状态判断
		ResponseData<UserData> userDataResp = userRemoteService.getUserById(userId);
		if(!userDataResp.isSuccessful()) {
			throw new BusinessException(userDataResp.getCode(),userDataResp.getMessage());
		}
		if(userDataResp.getData().getIsLockWithdraw() != null &&
				userDataResp.getData().getIsLockWithdraw() == true) {
			throw new BusinessException(BusinessExceptionCode.MERCHANT_EXCEPTION_USER);
		}
				
		//解决同一订单并发问题
		String accountLockKey = RedisKeyConstants.BUSI_EXTRACT + userId + params.getType().toUpperCase();
		redisLockUtils.lock(accountLockKey);
		try {
			
			//判断用户账户余额是否足够
			IssuedCoinInfo issuedCoinInfo = coinInfoService.selectByCoinType(params.getType(), userDataResp.getData().getCloudUserId());
			if(issuedCoinInfo == null){
				throw new BusinessException(BusinessExceptionCode.NOT_SUPPORT_COIN_NETWORK);
			}
			if (issuedCoinInfo.getCloseWithdraw()){
				throw new BusinessException(BusinessExceptionCode.NOT_SUPPORT_EXTRACT_COIN_TYPE);
			}
			if(issuedCoinInfo.getLowestExtractNumber().compareTo(params.getAmount()) == 1){
				throw new BusinessException(BusinessExceptionCode.LOWEST_EXRACT_ERROR);
			}
			String txHash = null;
			String type = params.getType();
			WalletAccount walletAccount = walletAccountMapper.selectByUserIdAndTypeForUpdate(userId, type);
			if(walletAccount == null){
				throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
			}
			Byte feeType = issuedCoinInfo.getSysTradingFeeType();
			BigDecimal fee = BigDecimal.ZERO;
			if(feeType.equals(BusinessEnum.FeeType.FIXED.getCode())){
				fee = issuedCoinInfo.getSysTradingFee();
			} else{
				fee = params.getAmount().multiply(issuedCoinInfo.getSysTradingFee());
			}
			BigDecimal total = params.getAmount().add(fee);
			BigDecimal balance = walletAccount.getBalance();
			BigDecimal negate = total.negate();
			if(balance.compareTo(total) == -1){
				throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
			}
			
			//增加云提币审批管理账户的余额
			ResponseData<UserData> extractApproveRes = userRemoteService.getManageUserByCloudUserId(
					userDataResp.getData().getCloudUserId(),BusinessEnum.CloudManageUserType.EXTRACT_APPROVE.getType().toString());
			if (extractApproveRes == null || !extractApproveRes.isSuccessful() || extractApproveRes.getData() == null){
				throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
			}
			UserData cloudUserExtractApproveManageData = extractApproveRes.getData();
			WalletAccount extractWallet = walletAccountMapper.selectByUserIdAndType(cloudUserExtractApproveManageData.getUserId(), type.toUpperCase());
			if(extractWallet == null) {
				CreateAccountParams extractWalletParams = new CreateAccountParams();
				extractWalletParams.setCurrencyType(walletAccount.getCoinType());
				extractWalletParams.setNetworkType(walletAccount.getCoinNetworkType());
				extractWalletParams.setUserId(cloudUserExtractApproveManageData.getUserId());
				extractWalletParams.setName(walletAccount.getCoinType());
				extractWallet = accountService.createAccount(extractWalletParams);
			}
			walletAccountMapper.updateBalance(extractWallet.getAccountId(), params.getAmount());
			
			//增加云提币手续费审批管理账户的余额
			ResponseData<UserData> extractFeeRes = userRemoteService.getManageUserByCloudUserId(
					userDataResp.getData().getCloudUserId(),BusinessEnum.CloudManageUserType.EXTRACT_FEE.getType().toString());
			if (extractFeeRes == null || !extractFeeRes.isSuccessful() || extractFeeRes.getData() == null){
				throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
			}
			UserData cloudUserExtractFeeManageData = extractFeeRes.getData();
			WalletAccount extractFeeWallet = walletAccountMapper.selectByUserIdAndType(cloudUserExtractFeeManageData.getUserId(), type.toUpperCase());
			if(extractFeeWallet == null) {
				CreateAccountParams extractWalletFeeParams = new CreateAccountParams();
				extractWalletFeeParams.setCurrencyType(walletAccount.getCoinType());
				extractWalletFeeParams.setNetworkType(walletAccount.getCoinNetworkType());
				extractWalletFeeParams.setUserId(cloudUserExtractFeeManageData.getUserId());
				extractWalletFeeParams.setName(walletAccount.getCoinType());
				extractFeeWallet = accountService.createAccount(extractWalletFeeParams);
			}
			walletAccountMapper.updateBalance(extractFeeWallet.getAccountId(), fee);
			
			//校验支付密码
			passwordVerify.verify(params.getPassWord(),userId);
			
			//提币记录生成
			ExtractRecord extractRecord = new ExtractRecord();
			extractRecord.setExtractStatus(BusinessEnum.ExtractStatus.approvaling.getCode());
			String extractId = getId(1);
			extractRecord.setExtractId(extractId);
			extractRecord.setAccountId(walletAccount.getAccountId());
			extractRecord.setExtractAddress(params.getToAddress());
			extractRecord.setExtractDate(new Date());
			extractRecord.setTxHash(txHash);
			extractRecord.setExtractNumber(params.getAmount());
			extractRecord.setCoinNetworkType(walletAccount.getCoinNetworkType());
			extractRecord.setCoinType(walletAccount.getCoinType());
			extractRecord.setCharge(fee);
			extractRecord.setFee(issuedCoinInfo.getMinerTradingFee());
			extractId = insertRecord(extractRecord);
			
			//再扣自己账户余额
			walletAccountMapper.updateBalance(walletAccount.getAccountId(), negate);
			if(BusinessEnum.ExtractStatus.approvaling.getCode().equals(extractRecord.getExtractStatus())) {
				try {
					santiService.sendSms(CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_SMS_SIGN, "用户提币申请"+extractRecord.getCoinType()+extractRecord.getExtractNumber().toString().toString()+"手续费："+extractRecord.getCharge() + "|" + adminCoinDynamicTemplateTimeout, adminPhone, adminCoinDynamicTemplateId);
				}catch(Exception e) {
					logger.error("用户提币申请，发送短信给管理员失败");
				}
			}
			
			if(Boolean.valueOf(autoTransfer)){
				logger.info("auto approval extractId=" + extractId);
				ExtractApprovalParams approvalParams = new ExtractApprovalParams();
				approvalParams.setExtractRecordId(extractId);
				approvalParams.setStatus(BusinessEnum.ApprovalResult.pass);
				approvalExtractRecordByCloudUser(userDataResp.getData().getCloudUserId(), "自动审批", approvalParams);
			}
			return extractId;
		} finally {
			redisLockUtils.releaseLock(accountLockKey);
		}
	}

	@Override
	public List<ExtractRecord> listExtractRecord(String userId, String cloudUserId) {
		//获取云账户配置
		List<IssuedCoinInfo> issuedCoinInfos = coinInfoService.selectCoinListByCloudUserId(cloudUserId);
		StringBuffer coinType = new StringBuffer();
		if (!CollectionUtils.isEmpty(issuedCoinInfos)){
			for (IssuedCoinInfo info : issuedCoinInfos){
				if ("1".equals(info.getStatus().toString()))
					coinType.append(",'" + info.getCoinType() + "'");
			}
		}
		List<ExtractRecord> extractRecords = extractRecordMapper.listExtractRecord(userId, coinType.toString().replaceFirst(",", ""));
		return extractRecords;
	}
	
	@Override
	public List<ExtractRecord> listApprovedRecord(String coinType) {
		return extractRecordMapper.listApprovedRecord(coinType);
	}

	@Override
	@Transactional
	public String updateStatus(String extractId, BigDecimal realFee, String txId) {
		//获取提币记录
		ExtractRecord record = extractRecordMapper.selectByPrimaryKey(extractId);
		String extractStatus = record.getExtractStatus();
		if(record != null){
			if(extractStatus.equals(BusinessEnum.ExtractStatus.confirmed.getCode())){
				logger.info("提币已经完成，提币单号={}",record.getExtractId());
				throw new BusinessException(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING);
			}
			record.setExtractStatus(BusinessEnum.ExtractStatus.confirmed.getCode());
			record.setFee(realFee);
			record.setTxHash(txId);
			extractRecordMapper.updateByPrimaryKeySelective(record);
		} else {
			throw  new BusinessException(BusinessExceptionCode.ILLEGAL_PARAMS);
		}

		return null;
	}

	/**
	 * 生成时间戳后面带N位的随机数
	 * @param n
	 * @return
	 */
	private static String getId(int n){
		return System.currentTimeMillis() + ((long)((Math.random()*9+1)* Math.pow(10,n)) + "");
	}

	@Transactional
	private String insertRecord(ExtractRecord extractRecord){
		try {
			extractRecord.setExtractId(getId(1));
			extractRecordMapper.insertSelective(extractRecord);
			return extractRecord.getExtractId();
		} catch (DuplicateKeyException e){
			insertRecord(extractRecord);
		}
		return null;
	}

	@Override
	public PageInfo<ExtractRecordData> selectExtractRecordByCloudUser(String cloudUserId,
			ExtractRecordByCloudUserParams params) {
		String status = null;
		if(params.getStatus() != null) {
			status = params.getStatus().getCode();
		}
		PageHelper.startPage(Integer.valueOf(params.getPageNo()), Integer.valueOf(params.getPageSize()));
		Page<ExtractRecord> pageInfo = extractRecordMapper.selectExtractRecordByCloudUser(
				params.getKey(),cloudUserId,status,params.getCoinType());
		
		List<ExtractRecordData> list = new ArrayList<>();
		for(ExtractRecord record:pageInfo.getResult()) {
			ExtractRecordData data = new ExtractRecordData();
			try {
				BeanUtils.copyProperties(data, record);
				list.add(data);
			} catch (Exception e) {
				logger.error("",e);
				throw new BusinessException();
			}
		}
		
		PageInfo<ExtractRecordData> pageInfoData =
				new PageInfo<ExtractRecordData>(pageInfo.getPageNum(),pageInfo.getPageSize(),pageInfo.getTotal(),list);

		PageHelper.clearPage();
		return pageInfoData;
	}

	@Override
	@Transactional
	public boolean approvalExtractRecordByCloudUser(String cloudUserId,String adminId, ExtractApprovalParams param) {
		ExtractRecord record = extractRecordMapper.selectByPrimaryKey(param.getExtractRecordId());
		if(!record.getExtractStatus().equals(BusinessEnum.ExtractStatus.approvaling.getCode())) {
			throw new BusinessException(BusinessExceptionCode.EXTRACT_APPROVE_STATUS_ERROR);
		}
		BigDecimal charge = record.getCharge() == null ? BigDecimal.ZERO : record.getCharge();

		//提币数量付款账户信息
		ResponseData<UserData> extractApproveRes = userRemoteService.getManageUserByCloudUserId(
				cloudUserId,BusinessEnum.CloudManageUserType.EXTRACT_APPROVE.getType().toString());
		if (extractApproveRes == null || !extractApproveRes.isSuccessful() || extractApproveRes.getData() == null){
			throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
		}
		UserData cloudUserExtractApproveManageData = extractApproveRes.getData();

		//提币手续费付款账户信息
		ResponseData<UserData> extractFeeRes = userRemoteService.getManageUserByCloudUserId(
				cloudUserId,BusinessEnum.CloudManageUserType.EXTRACT_FEE.getType().toString());
		if (extractFeeRes == null || !extractFeeRes.isSuccessful() || extractFeeRes.getData() == null){
			throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
		}
		UserData cloudUserExtractFeeManageData = extractFeeRes.getData();
		
		//提币拒绝
		if(param.getStatus().equals(BusinessEnum.ApprovalResult.fail)) {
			//修改提币单状态
			record.setExtractStatus(BusinessEnum.ExtractStatus.approvalfail.getCode());
			record.setExtractSuccessDate(new Date());
			record.setApproveResult(param.getApproveResultMemo());
			record.setApproveReason(adminId);
			extractRecordMapper.updateByPrimaryKeySelective(record);
			WalletAccount account = walletAccountMapper.selectByPrimaryKey(record.getAccountId());
			ResponseData<UserData>  userData = userRemoteService.getUserById(account.getUserId());
			if(!userData.isSuccessful()) {
				throw new BusinessException(userData.getCode(), userData.getMessage());
			}


			//退款手续费付款账户信息
			pay(record.getExtractNumber(), record.getExtractId(), record.getCoinType(),
					record.getCoinNetworkType(), cloudUserExtractApproveManageData.getUserName(),
					account.getUserId(), userData.getData().getUserName(),
					cloudUserExtractApproveManageData.getUserId(), BusinessEnum.PaymentRecordType.extractTranfer);
			//退手续费
			pay(charge, record.getExtractId(), record.getCoinType(),
					record.getCoinNetworkType(), cloudUserExtractFeeManageData.getUserName(),
					account.getUserId(), userData.getData().getUserName(),
					cloudUserExtractFeeManageData.getUserId(), BusinessEnum.PaymentRecordType.extractTranfer);
			return true;
		}

		//获取云充值管理员钱包
		ResponseData<UserData> depositRes = userRemoteService.getManageUserByCloudUserId(
				cloudUserId,BusinessEnum.CloudManageUserType.DEPOSIT.getType().toString());
		if (depositRes == null || !depositRes.isSuccessful() || depositRes.getData() == null){
			throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
		}
		UserData cloudUserDepositManageData = depositRes.getData();
		WalletAccount depositWallet = walletAccountMapper.selectByUserIdAndTypeAddLock(cloudUserDepositManageData.getUserId(), record.getCoinType().toUpperCase());
		
		//查询云提币管理账户的余额
		ResponseData<UserData> extractRes = userRemoteService.getManageUserByCloudUserId(
				cloudUserId,BusinessEnum.CloudManageUserType.EXTRACT.getType().toString());
		if (extractRes == null || !extractRes.isSuccessful() || extractRes.getData() == null){
			throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
		}
		UserData cloudUserExtractManageData = extractRes.getData();
		WalletAccount extractWallet = walletAccountMapper.selectByUserIdAndTypeAddLock(cloudUserExtractManageData.getUserId(), record.getCoinType().toUpperCase());
		
		//判断云用户是否有足够提币余额
		BigDecimal depositBalance = depositWallet == null || depositWallet.getBalance() == null?BigDecimal.ZERO:depositWallet.getBalance();
		BigDecimal extractBalance = extractWallet == null || extractWallet.getBalance() == null?BigDecimal.ZERO:extractWallet.getBalance();
		if (depositBalance.subtract(extractBalance).compareTo(record.getExtractNumber()) == -1) {
			throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
		}
		
		
		//查询是否为内部用户转账
		List<AccountAddress> accountAddresss = accountAddressMapper.selectByAddress(record.getExtractAddress());
		if(accountAddresss.size() > 0) {
			//收款账户信息
			WalletAccountData walletAddress = walletAccountMapper.selectByAccountId(accountAddresss.get(0).getAccountId());
			ResponseData<UserData>  userData = userRemoteService.getUserById(walletAddress.getUserId());
			if(!userData.isSuccessful()) {
				throw new BusinessException(userData.getCode(), userData.getMessage());
			}
			
			//修改提币单状态
			record.setExtractStatus(BusinessEnum.ExtractStatus.confirmed.getCode());
			record.setExtractSuccessDate(new Date());
			record.setApproveResult("内部转账");
			record.setApproveReason(adminId);
			extractRecordMapper.updateByPrimaryKeySelective(record);
			
			
			//转账提币数量
			pay(record.getExtractNumber(), record.getExtractId(), record.getCoinType(),
					record.getCoinNetworkType(), cloudUserExtractApproveManageData.getUserName(),
					walletAddress.getUserId(), userData.getData().getUserName(),
					cloudUserExtractApproveManageData.getUserId(), BusinessEnum.PaymentRecordType.extractTranfer);
			
			
			//转账提币手续费
			pay(charge, record.getExtractId(), record.getCoinType(),
					record.getCoinNetworkType(), cloudUserExtractFeeManageData.getUserName(),
					walletAddress.getUserId(), userData.getData().getUserName(),
					cloudUserExtractFeeManageData.getUserId(), BusinessEnum.PaymentRecordType.extractTranfer);
		}else {
			//云提币管理员钱包修改余额
			if(extractWallet == null) {
				CreateAccountParams params = new CreateAccountParams();
				params.setCurrencyType(record.getCoinType());
				params.setNetworkType(record.getCoinNetworkType());
				params.setUserId(cloudUserExtractManageData.getUserId());
				params.setName(record.getCoinType());
				extractWallet = accountService.createAccount(params);
			}
			walletAccountMapper.updateBalance(extractWallet.getAccountId(), record.getExtractNumber());
			
			//修改提币单状态
			record.setExtractStatus(BusinessEnum.ExtractStatus.approvaled.getCode());
			record.setApproveReason(adminId);
			extractRecordMapper.updateByPrimaryKeySelective(record);
		}
		return true;
	}

	private void pay(BigDecimal amount, String chekoutOrderId, String coinType,
					 String coinNetworkType, String payUserName, String receiptUserId,
					 String receiptUserName, String payUserId, BusinessEnum.PaymentRecordType paymentRecordType){
		PayParams payFeeParams = new PayParams();
		payFeeParams.setAmount(amount);
		payFeeParams.setCheckoutOrderId(chekoutOrderId);
		payFeeParams.setCoinType(coinType);
		payFeeParams.setNetworkType(coinNetworkType);
		payFeeParams.setPayUserName(payUserName);
		payFeeParams.setReceiptUserId(receiptUserId);
		payFeeParams.setReceiptUserName(receiptUserName);
		payService.pay(payFeeParams, payUserId,paymentRecordType);
	}

}
