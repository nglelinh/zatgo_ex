package com.zatgo.zup.wallet.mapper;


import com.zatgo.zup.wallet.entity.CloudUserCoinRel;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CloudUserCoinRelMapper extends BaseMapper<CloudUserCoinRel>{


    IssuedCoinInfo selectCloudUserCoinType(@Param("cloudUserId") String cloudUserId,
                                           @Param("coinType") String coinType);

    List<IssuedCoinInfo> selectCoinListByCloudUserId(@Param("cloudUserId") String cloudUserId);

    int insertCloudUserCoinRelList(List<CloudUserCoinRel> list);
}