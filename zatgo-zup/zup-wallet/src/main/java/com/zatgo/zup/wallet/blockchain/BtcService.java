package com.zatgo.zup.wallet.blockchain;

import java.math.BigDecimal;

import com.zatgo.zup.wallet.model.SendParams;

public interface BtcService {
	String getNewAddress(String account);

	String sendTransaction(SendParams params);

	BigDecimal getBalance(String accountId);
}
