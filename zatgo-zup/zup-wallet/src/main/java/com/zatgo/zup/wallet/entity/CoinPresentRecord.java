package com.zatgo.zup.wallet.entity;

import java.io.Serializable;
import java.util.Date;

public class CoinPresentRecord implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String presentId;

    private String receiveUserName;

    private String receiveVerifyCode;

    private Byte presentStatus;

    private String coinNetworkType;

    private String coinType;

    private Float coinNumber;

    private String presentReason;

    private Date presentDatetime;

    private Date receiveDatetime;

    private String cloudUserId;

    public String getPresentId() {
        return presentId;
    }

    public void setPresentId(String presentId) {
        this.presentId = presentId == null ? null : presentId.trim();
    }

    public String getReceiveUserName() {
        return receiveUserName;
    }

    public void setReceiveUserName(String receiveUserName) {
        this.receiveUserName = receiveUserName == null ? null : receiveUserName.trim();
    }

    public String getReceiveVerifyCode() {
        return receiveVerifyCode;
    }

    public void setReceiveVerifyCode(String receiveVerifyCode) {
        this.receiveVerifyCode = receiveVerifyCode == null ? null : receiveVerifyCode.trim();
    }

    public Byte getPresentStatus() {
        return presentStatus;
    }

    public void setPresentStatus(Byte presentStatus) {
        this.presentStatus = presentStatus;
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType == null ? null : coinNetworkType.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public Float getCoinNumber() {
        return coinNumber;
    }

    public void setCoinNumber(Float coinNumber) {
        this.coinNumber = coinNumber;
    }

    public String getPresentReason() {
        return presentReason;
    }

    public void setPresentReason(String presentReason) {
        this.presentReason = presentReason == null ? null : presentReason.trim();
    }

    public Date getPresentDatetime() {
        return presentDatetime;
    }

    public void setPresentDatetime(Date presentDatetime) {
        this.presentDatetime = presentDatetime;
    }

    public Date getReceiveDatetime() {
        return receiveDatetime;
    }

    public void setReceiveDatetime(Date receiveDatetime) {
        this.receiveDatetime = receiveDatetime;
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }
}