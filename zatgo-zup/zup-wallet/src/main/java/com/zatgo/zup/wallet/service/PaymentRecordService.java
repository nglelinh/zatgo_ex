package com.zatgo.zup.wallet.service;

import java.util.List;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.wallet.PaymentRecordData;
import com.zatgo.zup.wallet.entity.PaymentRecord;

public interface PaymentRecordService {

	List<PaymentRecord> listRecordByAccountId(String accountId);

	List<PaymentRecord> listRecordByUserId(String userId);
	
	PageInfo<PaymentRecord> listRecordPageByUserId(String userId,String pageNo, String pageSize);

	PageInfo<PaymentRecord> listRecordPageByAccountId(String accountId,String pageNo, String pageSize);
	
	/**
	 * 根据交易编号查询交易信息
	 * @param userId
	 * @param tradeNo
	 * @return
	 */
	public PaymentRecordData getPaymentRecordByTradeNo(String userId,String tradeNo);
}
