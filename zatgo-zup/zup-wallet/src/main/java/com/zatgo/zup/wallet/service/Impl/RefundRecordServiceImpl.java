package com.zatgo.zup.wallet.service.Impl;

import com.zatgo.zup.wallet.mapper.RefundRecordMapper;
import com.zatgo.zup.wallet.service.RefundRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
@Component
public class RefundRecordServiceImpl implements RefundRecordService {
	@Autowired
	private RefundRecordMapper refundRecordMapper;
	@Override
	public BigDecimal getOrderRefundMoney(String orderId) {
		return refundRecordMapper.sumRefundMoney(orderId);
	}
}
