package com.zatgo.zup.wallet.service;

import java.util.List;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.wallet.entity.FixedDepositSubsc;
import com.zatgo.zup.wallet.entity.FixedTimeDepositProWithBLOBs;
import com.zatgo.zup.wallet.model.FixedTimeDepositAddParams;
import com.zatgo.zup.wallet.model.FixedTimeDepositAdminSearchParams;
import com.zatgo.zup.wallet.model.FixedTimeDepositSubscParams;

public interface FixedTimeDepositService {

	/**
	 * 修改理财产品
	 * @param adminInfo
	 * @param params
	 */
	void updatePro(AuthUserInfo adminInfo,FixedTimeDepositAddParams params);
	
	/**
	 * 增加理财产品
	 * @param adminInfo
	 * @param params
	 */
	void addPro(AuthUserInfo adminInfo,FixedTimeDepositAddParams params);
	
	/**
	 * 根据主键查询产品
	 * @param cloudUserId
	 * @param proId
	 * @return
	 */
	FixedTimeDepositProWithBLOBs getProById(String cloudUserId, String proId);
	
	/**
	 * 管理员查询理财产品列表
	 * @param adminInfo
	 * @param params
	 * @return
	 */
	PageInfo<FixedTimeDepositProWithBLOBs> getAllProByAdmin(AuthUserInfo adminInfo,FixedTimeDepositAdminSearchParams params);
	
	/**
	 * 用户查询有效的理财产品
	 * @param userInfo
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PageInfo<FixedTimeDepositProWithBLOBs> getReleasePro(AuthUserInfo userInfo,int pageNo,int pageSize);
	
	/**
	 * 用户认购理财产品
	 * @param userInfo
	 * @param params
	 */
	void proSubsc(AuthUserInfo userInfo, FixedTimeDepositSubscParams params);
	
	/**
	 * 查询认购记录
	 * @param userInfo
	 * @return
	 */
	List<FixedDepositSubsc> getProSubscListByUser(AuthUserInfo userInfo);
	
	/**
	 * 分页查询需要赎回的记录
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PageInfo<FixedDepositSubsc> getNeedRansomListByPage(int pageNo,int pageSize);
	/**
	 * 赎回操作
	 */
	void ransom(String subscRecordId);
}
