package com.zatgo.zup.wallet.mapper;

import com.zatgo.zup.wallet.entity.SysParams;
import org.apache.ibatis.annotations.Param;

public interface SysParamsMapper {
    int deleteByPrimaryKey(String paramId);

    int insert(SysParams record);

    int insertSelective(SysParams record);

    SysParams selectByPrimaryKey(String paramId);

    int updateByPrimaryKeySelective(SysParams record);

    int updateByPrimaryKey(SysParams record);

    SysParams selectByCode(@Param("code") String code);
}