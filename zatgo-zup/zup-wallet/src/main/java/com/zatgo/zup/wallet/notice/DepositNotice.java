package com.zatgo.zup.wallet.notice;

import javax.annotation.PostConstruct;

import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.mq.MQConsumer;
import com.zatgo.zup.common.mq.MQContants;

/**
 * 币充值通知
 * @Title
 * @author lincm
 * @date 2018年5月8日 上午11:49:06
 */
@Component
public class DepositNotice {
	private static final Logger logger = LoggerFactory.getLogger(DepositNotice.class);

	@Autowired
	MQConsumer consumer;

	@PostConstruct
	public void consume(){
		try {
			ReceiveMsgNoticeCallBack callBack = new ReceiveMsgNoticeCallBack();
			consumer.startConsumer(MQContants.CONSUMER_GROUP_COIN_RECEIVE,
					MQContants.TOPIC_COIN_RECEIVE,null, callBack, MessageModel.BROADCASTING);
		} catch (Exception e) {
			logger.error("consumer error",e);
		}
	}













}
