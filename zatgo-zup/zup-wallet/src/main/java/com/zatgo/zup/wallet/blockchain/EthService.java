package com.zatgo.zup.wallet.blockchain;

import java.math.BigDecimal;

import com.zatgo.zup.wallet.model.SendParams;
import com.zatgo.zup.wallet.model.TxRecord;

public interface EthService {

	String getNewAddress(String password,String accountId);

	String sendTransaction(SendParams params);

	TxRecord getTransaction(String transactionHash);

	BigDecimal getBalance(String address);

}
