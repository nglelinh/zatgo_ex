package com.zatgo.zup.wallet.mapper;

import com.github.pagehelper.Page;
import com.zatgo.zup.wallet.entity.DepositRecord;
import com.zatgo.zup.wallet.entity.ExtractRecord;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import jnr.ffi.annotations.In;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface ExtractRecordMapper {
    int deleteByPrimaryKey(String extractId);

    int insert(ExtractRecord record);

    int insertSelective(ExtractRecord record);

    ExtractRecord selectByPrimaryKey(String extractId);

    int updateByPrimaryKeySelective(ExtractRecord record);

    int updateByPrimaryKey(ExtractRecord record);

	List<ExtractRecord> listExtractRecord(@Param("userId") String userId, @Param("type") String type);

	List<ExtractRecord> listUnConfirmedRecord();

	List<ExtractRecord> listApprovedRecord(@Param("coinType") String coinType);
	@Update("update extract_record set extract_status = #{status} where extract_id =#{extractId}")
	Integer updateExtractStatus(@Param("extractId")String extractId, @Param("status") String status);

    @Select("SELECT ici.record_id as extractId ,sum(dr.Extract_number) as ExtractNumber, dr.coin_type as coinType, ici.coin_network_type as coinNetworkType" +
            " from extract_record dr LEFT JOIN issued_coin_info ici ON dr.coin_type=ici.coin_type where" +
            " dr.extract_status='1' GROUP BY dr.coin_type")
    List<ExtractRecord> selectSystemExtractRecord();
    
    Page<ExtractRecord> selectExtractRecordByCloudUser(@Param("key") String key,@Param("cloudUserId") String cloudUserId,
			@Param("extractStatus") String depositStatus,@Param("coinType") String coinType);
}