package com.zatgo.zup.wallet.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.wallet.entity.FixedDepositSubsc;
import com.zatgo.zup.wallet.entity.FixedTimeDepositProWithBLOBs;
import com.zatgo.zup.wallet.model.FixedTimeDepositAddParams;
import com.zatgo.zup.wallet.model.FixedTimeDepositAdminSearchParams;
import com.zatgo.zup.wallet.model.FixedTimeDepositSubscParams;
import com.zatgo.zup.wallet.service.FixedTimeDepositService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/wallet/fixedTimeDeposit",description = "固定时间存款接口")
@RestController
@RequestMapping(value = "/wallet/fixedTimeDeposit")
public class FixedTimeDepositController extends BaseController{
	
	@Autowired
	private FixedTimeDepositService fixedTimeDepositService;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	private static final Logger logger = LoggerFactory.getLogger(FixedTimeDepositController.class);

	@RequestMapping(value = "/admin/addPro",method = RequestMethod.POST)
	@ApiOperation(value = "管理员增加理财产品", notes = "管理员增加理财产品接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> addPro(@RequestBody FixedTimeDepositAddParams params){
		AuthUserInfo userInfo = getAdminInfo();
		fixedTimeDepositService.addPro(userInfo, params);
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@RequestMapping(value = "/admin/updatePro",method = RequestMethod.POST)
	@ApiOperation(value = "管理员修改理财产品", notes = "管理员修改理财产品接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> updatePro(@RequestBody FixedTimeDepositAddParams params){
		AuthUserInfo userInfo = getAdminInfo();
		fixedTimeDepositService.updatePro(userInfo, params);
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@RequestMapping(value = "/admin/getProList",method = RequestMethod.POST)
	@ApiOperation(value = "管理员查询理财产品列表", notes = "管理员查询理财产品列表", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<PageInfo<FixedTimeDepositProWithBLOBs>> getProListByAdmin(@RequestBody FixedTimeDepositAdminSearchParams params){
		AuthUserInfo userInfo = getAdminInfo();
		PageInfo<FixedTimeDepositProWithBLOBs>  pageInfo = fixedTimeDepositService.getAllProByAdmin(userInfo, params);
		return BusinessResponseFactory.createSuccess(pageInfo);
	}
	
	@RequestMapping(value = "/admin/getPro/{proId}",method = RequestMethod.GET)
	@ApiOperation(value = "管理员查询理财产品", notes = "管理员查询理财产品", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<FixedTimeDepositProWithBLOBs> getProByAdminAndProId(@PathVariable("proId") String proId){
		AuthUserInfo userInfo = getAdminInfo();
		FixedTimeDepositProWithBLOBs  pro = fixedTimeDepositService.getProById(userInfo.getCloudUserId(),proId);
		return BusinessResponseFactory.createSuccess(pro);
	}
	
	@RequestMapping(value = "/getPro/{proId}",method = RequestMethod.GET)
	@ApiOperation(value = "查询理财产品", notes = "查询理财产品", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<FixedTimeDepositProWithBLOBs> getProByProId(@PathVariable("proId") String proId){
		AuthUserInfo userInfo = getUserInfo();
		FixedTimeDepositProWithBLOBs  pro = fixedTimeDepositService.getProById(userInfo.getCloudUserId(),proId);
		return BusinessResponseFactory.createSuccess(pro);
	}
	
	@RequestMapping(value = "/getReleaseProList/{pageNo}/{pageSize}",method = RequestMethod.GET)
	@ApiOperation(value = "查询有效的理财产品列表", notes = "查询有效的理财产品列表", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<PageInfo<FixedTimeDepositProWithBLOBs>> getReleaseProList(@PathVariable("pageNo") String pageNo, @PathVariable("pageSize") String pageSize){
		AuthUserInfo userInfo = getUserInfo();
		PageInfo<FixedTimeDepositProWithBLOBs>  pageInfo = fixedTimeDepositService.getReleasePro(userInfo, Integer.valueOf(pageNo), Integer.valueOf(pageSize));
		return BusinessResponseFactory.createSuccess(pageInfo);
	}
	
	
	@RequestMapping(value = "/proSubsc",method = RequestMethod.POST)
	@ApiOperation(value = "理财产品认购", notes = "理财产品认购", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> proSubsc(@RequestBody FixedTimeDepositSubscParams params){
		AuthUserInfo userInfo = getUserInfo();
		
		//同个用户不能同一时间认购相同产品，要计算最大认购数量
		String key = RedisKeyConstants.FIXED_TIME_DEPOSIT_USER_PRE + params.getProdId() + "_" + userInfo.getUserId();
		redisLockUtils.lock(key);
		try {
			fixedTimeDepositService.proSubsc(userInfo, params);
		}finally {
			redisLockUtils.releaseLock(key);
		}
		
		return BusinessResponseFactory.createSuccess(true);
	}
	
	
	@RequestMapping(value = "/getProSubscList",method = RequestMethod.GET)
	@ApiOperation(value = "查询有效的理财产品列表", notes = "查询有效的理财产品列表", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<FixedDepositSubsc>> getProSubscList(){
		AuthUserInfo userInfo = getUserInfo();
		List<FixedDepositSubsc>  list = fixedTimeDepositService.getProSubscListByUser(userInfo);
		return BusinessResponseFactory.createSuccess(list);
	}
	
	
	@RequestMapping(value = "/task/ransomCheckTask",method = RequestMethod.GET)
	@ApiOperation(value = "定时赎回检查接口", notes = "定时赎回检查接口", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> ransomCheckTask(){
		int pageNo = 1;
		int pageSize = 100;
		while(true) {
			PageInfo<FixedDepositSubsc> pageInfo = fixedTimeDepositService.getNeedRansomListByPage(pageNo, pageSize);
			if(pageInfo.getPages() > 0) {
				logger.info("发现需要赎回的认购数据，数量：" + pageInfo.getPages());
				for(FixedDepositSubsc subsc:pageInfo.getList()) {
					fixedTimeDepositService.ransom(subsc.getSubscRecordId());
				}
			}else {
				break;
			}
		}
		
		return BusinessResponseFactory.createSuccess(true);
	}
	
}
