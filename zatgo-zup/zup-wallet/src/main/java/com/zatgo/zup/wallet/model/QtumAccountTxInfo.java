package com.zatgo.zup.wallet.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class QtumAccountTxInfo implements Serializable{
	
	private String account;

	private String address;

	private String category;

	private BigDecimal amount;

	private String label;

	private BigDecimal vout;

	private BigInteger confirmations;

	private String blockhash;
	
	private String blockindex;
	
	private Long blocktime;

	private String txid;

	private String[] walletconflicts;

	private Long time;

	private String timereceived;

	private String comment;

	private Boolean abandoned;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public BigDecimal getVout() {
		return vout;
	}

	public void setVout(BigDecimal vout) {
		this.vout = vout;
	}

	public BigInteger getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(BigInteger confirmations) {
		this.confirmations = confirmations;
	}

	public String getBlockhash() {
		return blockhash;
	}

	public void setBlockhash(String blockhash) {
		this.blockhash = blockhash;
	}

	public String getBlockindex() {
		return blockindex;
	}

	public void setBlockindex(String blockindex) {
		this.blockindex = blockindex;
	}

	public Long getBlocktime() {
		return blocktime;
	}

	public void setBlocktime(Long blocktime) {
		this.blocktime = blocktime;
	}

	public String getTxid() {
		return txid;
	}

	public void setTxid(String txid) {
		this.txid = txid;
	}

	public String[] getWalletconflicts() {
		return walletconflicts;
	}

	public void setWalletconflicts(String[] walletconflicts) {
		this.walletconflicts = walletconflicts;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getTimereceived() {
		return timereceived;
	}

	public void setTimereceived(String timereceived) {
		this.timereceived = timereceived;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Boolean getAbandoned() {
		return abandoned;
	}

	public void setAbandoned(Boolean abandoned) {
		this.abandoned = abandoned;
	}
}
