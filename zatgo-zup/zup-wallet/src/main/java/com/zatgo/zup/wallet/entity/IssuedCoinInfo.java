package com.zatgo.zup.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class IssuedCoinInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private String recordId;

    private String coinFullName;

    private String coinShortName;

    private String coinSymbol;

    private Integer coinDecimals;

    private Long issuedNumber;

    private String issuedWay;

    private Date issuedDatetime;

    private String coinNetworkType;

    private String coinType;

    private String coinImage;

    private BigDecimal marketPriceCny;

    /**
     * 0: 百分比
            1: 固定值
     */
    private Byte sysTradingFeeType;

    /**
     * 百分比：0.3------> 代表收取0.3%的手续费
            固定值：0.01---->代表收到0.01个币为手续费
     */
    private BigDecimal sysTradingFee;

    private BigDecimal minerTradingFee;

    private BigDecimal lowestExtractNumber;

    private Byte isAutoGatherCoin;

    private BigDecimal upperLimitAmount;
    
    private String walletMainAcct;

    private String walletExtractAddress;

    private String gatherMainAddress;

    private Boolean isCloseDeposit;

    private Boolean isCloseWithdraw;

    private String introduction;

    private Byte status;
    
    private Integer confirmNum;


    public Boolean getIsCloseDeposit() {
		return isCloseDeposit;
	}

	public void setIsCloseDeposit(Boolean isCloseDeposit) {
		this.isCloseDeposit = isCloseDeposit;
	}

	public Boolean getIsCloseWithdraw() {
		return isCloseWithdraw;
	}

	public void setIsCloseWithdraw(Boolean isCloseWithdraw) {
		this.isCloseWithdraw = isCloseWithdraw;
	}

	public Integer getConfirmNum() {
		return confirmNum;
	}

	public void setConfirmNum(Integer confirmNum) {
		this.confirmNum = confirmNum;
	}

	public String getWalletMainAcct() {
		return walletMainAcct;
	}

	public void setWalletMainAcct(String walletMainAcct) {
		this.walletMainAcct = walletMainAcct;
	}


	public String getWalletExtractAddress() {
		return walletExtractAddress;
	}

	public void setWalletExtractAddress(String walletExtractAddress) {
		this.walletExtractAddress = walletExtractAddress;
	}

	public String getGatherMainAddress() {
		return gatherMainAddress;
	}

	public void setGatherMainAddress(String gatherMainAddress) {
		this.gatherMainAddress = gatherMainAddress;
	}

	public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCoinFullName() {
        return coinFullName;
    }

    public void setCoinFullName(String coinFullName) {
        this.coinFullName = coinFullName;
    }

    public String getCoinShortName() {
        return coinShortName;
    }

    public void setCoinShortName(String coinShortName) {
        this.coinShortName = coinShortName;
    }

    public String getCoinSymbol() {
        return coinSymbol;
    }

    public void setCoinSymbol(String coinSymbol) {
        this.coinSymbol = coinSymbol;
    }

    public Integer getCoinDecimals() {
        return coinDecimals;
    }

    public void setCoinDecimals(Integer coinDecimals) {
        this.coinDecimals = coinDecimals;
    }

    public Long getIssuedNumber() {
        return issuedNumber;
    }

    public void setIssuedNumber(Long issuedNumber) {
        this.issuedNumber = issuedNumber;
    }

    public String getIssuedWay() {
        return issuedWay;
    }

    public void setIssuedWay(String issuedWay) {
        this.issuedWay = issuedWay;
    }

    public Date getIssuedDatetime() {
        return issuedDatetime;
    }

    public void setIssuedDatetime(Date issuedDatetime) {
        this.issuedDatetime = issuedDatetime;
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getCoinImage() {
        return coinImage;
    }

    public void setCoinImage(String coinImage) {
        this.coinImage = coinImage;
    }

    public BigDecimal getMarketPriceCny() {
        return marketPriceCny;
    }

    public void setMarketPriceCny(BigDecimal marketPriceCny) {
        this.marketPriceCny = marketPriceCny;
    }

    public Byte getSysTradingFeeType() {
        return sysTradingFeeType;
    }

    public void setSysTradingFeeType(Byte sysTradingFeeType) {
        this.sysTradingFeeType = sysTradingFeeType;
    }

    public BigDecimal getSysTradingFee() {
        return sysTradingFee;
    }

    public void setSysTradingFee(BigDecimal sysTradingFee) {
        this.sysTradingFee = sysTradingFee;
    }

    public BigDecimal getMinerTradingFee() {
        return minerTradingFee;
    }

    public void setMinerTradingFee(BigDecimal minerTradingFee) {
        this.minerTradingFee = minerTradingFee;
    }

    public BigDecimal getLowestExtractNumber() {
        return lowestExtractNumber;
    }

    public void setLowestExtractNumber(BigDecimal lowestExtractNumber) {
        this.lowestExtractNumber = lowestExtractNumber;
    }

    public Byte getIsAutoGatherCoin() {
        return isAutoGatherCoin;
    }

    public void setIsAutoGatherCoin(Byte isAutoGatherCoin) {
        this.isAutoGatherCoin = isAutoGatherCoin;
    }

    public BigDecimal getUpperLimitAmount() {
        return upperLimitAmount;
    }

    public void setUpperLimitAmount(BigDecimal upperLimitAmount) {
        this.upperLimitAmount = upperLimitAmount;
    }

    public Boolean getCloseDeposit() {
        return isCloseDeposit;
    }

    public void setCloseDeposit(Boolean closeDeposit) {
        isCloseDeposit = closeDeposit;
    }

    public Boolean getCloseWithdraw() {
        return isCloseWithdraw;
    }

    public void setCloseWithdraw(Boolean closeWithdraw) {
        isCloseWithdraw = closeWithdraw;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IssuedCoinInfo info = (IssuedCoinInfo) o;

        if (!recordId.equals(info.recordId)) return false;
        if (!coinFullName.equals(info.coinFullName)) return false;
        if (!coinShortName.equals(info.coinShortName)) return false;
        if (!coinSymbol.equals(info.coinSymbol)) return false;
        if (!coinDecimals.equals(info.coinDecimals)) return false;
        if (!issuedNumber.equals(info.issuedNumber)) return false;
        if (!issuedWay.equals(info.issuedWay)) return false;
        if (!issuedDatetime.equals(info.issuedDatetime)) return false;
        if (!coinNetworkType.equals(info.coinNetworkType)) return false;
        if (!coinType.equals(info.coinType)) return false;
        if (!coinImage.equals(info.coinImage)) return false;
        if (!marketPriceCny.equals(info.marketPriceCny)) return false;
        if (!sysTradingFeeType.equals(info.sysTradingFeeType)) return false;
        if (!sysTradingFee.equals(info.sysTradingFee)) return false;
        if (!minerTradingFee.equals(info.minerTradingFee)) return false;
        if (!lowestExtractNumber.equals(info.lowestExtractNumber)) return false;
        if (!isAutoGatherCoin.equals(info.isAutoGatherCoin)) return false;
        if (!upperLimitAmount.equals(info.upperLimitAmount)) return false;
        if (!walletMainAcct.equals(info.walletMainAcct)) return false;
        if (!walletExtractAddress.equals(info.walletExtractAddress)) return false;
        if (!gatherMainAddress.equals(info.gatherMainAddress)) return false;
        if (!isCloseDeposit.equals(info.isCloseDeposit)) return false;
        return isCloseWithdraw.equals(info.isCloseWithdraw);

    }

    @Override
    public int hashCode() {
        int result = recordId.hashCode();
        result = 31 * result + coinFullName.hashCode();
        result = 31 * result + coinShortName.hashCode();
        result = 31 * result + coinSymbol.hashCode();
        result = 31 * result + coinDecimals.hashCode();
        result = 31 * result + issuedNumber.hashCode();
        result = 31 * result + issuedWay.hashCode();
        result = 31 * result + issuedDatetime.hashCode();
        result = 31 * result + coinNetworkType.hashCode();
        result = 31 * result + coinType.hashCode();
        result = 31 * result + coinImage.hashCode();
        result = 31 * result + marketPriceCny.hashCode();
        result = 31 * result + sysTradingFeeType.hashCode();
        result = 31 * result + sysTradingFee.hashCode();
        result = 31 * result + minerTradingFee.hashCode();
        result = 31 * result + lowestExtractNumber.hashCode();
        result = 31 * result + isAutoGatherCoin.hashCode();
        result = 31 * result + upperLimitAmount.hashCode();
        result = 31 * result + walletMainAcct.hashCode();
        result = 31 * result + walletExtractAddress.hashCode();
        result = 31 * result + gatherMainAddress.hashCode();
        result = 31 * result + isCloseDeposit.hashCode();
        result = 31 * result + isCloseWithdraw.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "IssuedCoinInfo{" +
                "recordId='" + recordId + '\'' +
                ", coinFullName='" + coinFullName + '\'' +
                ", coinShortName='" + coinShortName + '\'' +
                ", coinSymbol='" + coinSymbol + '\'' +
                ", coinDecimals=" + coinDecimals +
                ", issuedNumber=" + issuedNumber +
                ", issuedWay='" + issuedWay + '\'' +
                ", issuedDatetime=" + issuedDatetime +
                ", coinNetworkType='" + coinNetworkType + '\'' +
                ", coinType='" + coinType + '\'' +
                ", coinImage='" + coinImage + '\'' +
                ", marketPriceCny=" + marketPriceCny +
                ", sysTradingFeeType=" + sysTradingFeeType +
                ", sysTradingFee=" + sysTradingFee +
                ", minerTradingFee=" + minerTradingFee +
                ", lowestExtractNumber=" + lowestExtractNumber +
                ", isAutoGatherCoin=" + isAutoGatherCoin +
                ", upperLimitAmount=" + upperLimitAmount +
                ", walletMainAcct='" + walletMainAcct + '\'' +
                ", walletExtractAddress='" + walletExtractAddress + '\'' +
                ", gatherMainAddress='" + gatherMainAddress + '\'' +
                ", isCloseDeposit=" + isCloseDeposit +
                ", isCloseWithdraw=" + isCloseWithdraw +
                '}';
    }
}