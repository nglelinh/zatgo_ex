package com.zatgo.zup.wallet.model;

import java.math.BigDecimal;

public class SendParams {

	private String type;
	private String from;
	private String to;

	private BigDecimal amount;

	private String currency;

	private String desc;

	private BigDecimal fee;

	private Byte webPay;

	public Byte getWebPay() {
		return webPay;
	}

	public void setWebPay(Byte webPay) {
		this.webPay = webPay;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
}
