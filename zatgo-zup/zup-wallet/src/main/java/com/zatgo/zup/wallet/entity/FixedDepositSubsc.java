package com.zatgo.zup.wallet.entity;

import java.math.BigDecimal;
import java.util.Date;

public class FixedDepositSubsc {
    private String subscRecordId;

    private String userId;

    private String proId;

    private BigDecimal subscNum;

    private Date subscDate;

    private BigDecimal yieldRate;

    private BigDecimal yieldNum;

    private Byte status;

    public String getSubscRecordId() {
        return subscRecordId;
    }

    public void setSubscRecordId(String subscRecordId) {
        this.subscRecordId = subscRecordId == null ? null : subscRecordId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId == null ? null : proId.trim();
    }

    public BigDecimal getSubscNum() {
        return subscNum;
    }

    public void setSubscNum(BigDecimal subscNum) {
        this.subscNum = subscNum;
    }

    public Date getSubscDate() {
        return subscDate;
    }

    public void setSubscDate(Date subscDate) {
        this.subscDate = subscDate;
    }

    public BigDecimal getYieldRate() {
        return yieldRate;
    }

    public void setYieldRate(BigDecimal yieldRate) {
        this.yieldRate = yieldRate;
    }

    public BigDecimal getYieldNum() {
        return yieldNum;
    }

    public void setYieldNum(BigDecimal yieldNum) {
        this.yieldNum = yieldNum;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}