package com.zatgo.zup.wallet.model;

import java.io.Serializable;

public class DepositAddressParams implements Serializable {

	private String userId;

	private String coinType;

	private String networkType;
	

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}
}
