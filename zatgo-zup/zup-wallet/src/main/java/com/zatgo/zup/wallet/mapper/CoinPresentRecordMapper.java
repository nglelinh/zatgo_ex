package com.zatgo.zup.wallet.mapper;

import java.util.List;

import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zatgo.zup.wallet.entity.CoinPresentRecord;

public interface CoinPresentRecordMapper extends BaseMapper{

    @Select("select * from coin_present_record where (receive_user_name = #{loginUserName} or receive_verify_code = #{verifyCode}) and present_status = 0")
    List<CoinPresentRecord> selectNotReceiveByVerifyCode(@Param("loginUserName") String loginUserName,@Param("verifyCode") String verifyCode);
    
    @Select("select * from coin_present_record where present_status = 0 and cloud_user_id = #{cloudUserId}")
    List<CoinPresentRecord> selectNotReceiveByAll(@Param("cloudUserId") String cloudUserId);
  
    @Update("update coin_present_record set present_status = 1 where present_id = #{presentId} and present_status = 0")
    int updateReceive(@Param("presentId") String presentId);

    @Select("SELECT ici.record_id as presentId ,sum(dr.coin_number) as coinNumber, " +
            "dr.coin_type from coin_present_record dr LEFT JOIN issued_coin_info ici ON dr.coin_type=ici.coin_type" +
            " where dr.present_status='1' GROUP BY dr.coin_type")
    List<CoinPresentRecord> selectSystemCoinPresentRecord();
}