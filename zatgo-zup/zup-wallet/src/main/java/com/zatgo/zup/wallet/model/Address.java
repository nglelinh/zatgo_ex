package com.zatgo.zup.wallet.model;

import java.io.Serializable;
import java.util.Date;

public class Address implements Serializable{

	private String id;

	private String name;

	private String address;

	private String netWork;

	private Date createTime;

	private Date updateTime;

	public Address() {

	}

	public Address(String id, String name, String address, String netWork, Date createTime, Date updateTime) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.netWork = netWork;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNetWork() {
		return netWork;
	}

	public void setNetWork(String netWork) {
		this.netWork = netWork;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
