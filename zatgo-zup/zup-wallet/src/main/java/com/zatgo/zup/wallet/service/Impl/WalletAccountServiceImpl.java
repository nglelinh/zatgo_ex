package com.zatgo.zup.wallet.service.Impl;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.wallet.entity.WalletAccount;
import com.zatgo.zup.wallet.mapper.WalletAccountMapper;
import com.zatgo.zup.wallet.service.WalletAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
@Component
public class WalletAccountServiceImpl implements WalletAccountService {
//	private static final Logger logger = LoggerFactory.getLogger(WalletAccountServiceImpl.class);
//	@Value("${pay.try-times}")
//	private int tryTimes;
//	@Autowired
//	private WalletAccountMapper walletAccountMapper;
//	@Override
//	@Transactional
//	public Boolean updateBalance(String accountId, BigDecimal amount) {
//		Boolean tag = false;
//		long sleepTime = 500;
//		for (int i= 0;i <= tryTimes;i++){
//			WalletAccount walletAccount = walletAccountMapper.selectByPrimaryKey(accountId);
//			int result = walletAccountMapper.updateBalance(accountId, amount, walletAccount.getBalance());
//			if(result == 1){
//				tag = true;
//				logger.error("<----------------database update success---------------->");
//				break;
//			}
//			try {
//				Thread.sleep(sleepTime);
//			} catch (InterruptedException e) {
//				logger.error("update balance thread sleep error",e);
//				throw new BusinessException(BusinessExceptionCode.PAYMENT_TIMEOUT);
//			}
//			sleepTime += 500;
//		}
//		if(!tag){
//			logger.error("<----------------database timeout---------------->");
//		    throw new BusinessException(BusinessExceptionCode.PAYMENT_TIMEOUT);
//		}
//		return tag;
//	}
}
