package com.zatgo.zup.wallet.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FixedDepositSubscExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FixedDepositSubscExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSubscRecordIdIsNull() {
            addCriterion("subsc_record_id is null");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdIsNotNull() {
            addCriterion("subsc_record_id is not null");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdEqualTo(String value) {
            addCriterion("subsc_record_id =", value, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdNotEqualTo(String value) {
            addCriterion("subsc_record_id <>", value, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdGreaterThan(String value) {
            addCriterion("subsc_record_id >", value, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdGreaterThanOrEqualTo(String value) {
            addCriterion("subsc_record_id >=", value, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdLessThan(String value) {
            addCriterion("subsc_record_id <", value, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdLessThanOrEqualTo(String value) {
            addCriterion("subsc_record_id <=", value, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdLike(String value) {
            addCriterion("subsc_record_id like", value, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdNotLike(String value) {
            addCriterion("subsc_record_id not like", value, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdIn(List<String> values) {
            addCriterion("subsc_record_id in", values, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdNotIn(List<String> values) {
            addCriterion("subsc_record_id not in", values, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdBetween(String value1, String value2) {
            addCriterion("subsc_record_id between", value1, value2, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andSubscRecordIdNotBetween(String value1, String value2) {
            addCriterion("subsc_record_id not between", value1, value2, "subscRecordId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andProIdIsNull() {
            addCriterion("pro_id is null");
            return (Criteria) this;
        }

        public Criteria andProIdIsNotNull() {
            addCriterion("pro_id is not null");
            return (Criteria) this;
        }

        public Criteria andProIdEqualTo(String value) {
            addCriterion("pro_id =", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdNotEqualTo(String value) {
            addCriterion("pro_id <>", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdGreaterThan(String value) {
            addCriterion("pro_id >", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdGreaterThanOrEqualTo(String value) {
            addCriterion("pro_id >=", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdLessThan(String value) {
            addCriterion("pro_id <", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdLessThanOrEqualTo(String value) {
            addCriterion("pro_id <=", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdLike(String value) {
            addCriterion("pro_id like", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdNotLike(String value) {
            addCriterion("pro_id not like", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdIn(List<String> values) {
            addCriterion("pro_id in", values, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdNotIn(List<String> values) {
            addCriterion("pro_id not in", values, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdBetween(String value1, String value2) {
            addCriterion("pro_id between", value1, value2, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdNotBetween(String value1, String value2) {
            addCriterion("pro_id not between", value1, value2, "proId");
            return (Criteria) this;
        }

        public Criteria andSubscNumIsNull() {
            addCriterion("subsc_num is null");
            return (Criteria) this;
        }

        public Criteria andSubscNumIsNotNull() {
            addCriterion("subsc_num is not null");
            return (Criteria) this;
        }

        public Criteria andSubscNumEqualTo(BigDecimal value) {
            addCriterion("subsc_num =", value, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumNotEqualTo(BigDecimal value) {
            addCriterion("subsc_num <>", value, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumGreaterThan(BigDecimal value) {
            addCriterion("subsc_num >", value, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("subsc_num >=", value, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumLessThan(BigDecimal value) {
            addCriterion("subsc_num <", value, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("subsc_num <=", value, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumIn(List<BigDecimal> values) {
            addCriterion("subsc_num in", values, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumNotIn(List<BigDecimal> values) {
            addCriterion("subsc_num not in", values, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("subsc_num between", value1, value2, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscNumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("subsc_num not between", value1, value2, "subscNum");
            return (Criteria) this;
        }

        public Criteria andSubscDateIsNull() {
            addCriterion("subsc_date is null");
            return (Criteria) this;
        }

        public Criteria andSubscDateIsNotNull() {
            addCriterion("subsc_date is not null");
            return (Criteria) this;
        }

        public Criteria andSubscDateEqualTo(Date value) {
            addCriterion("subsc_date =", value, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateNotEqualTo(Date value) {
            addCriterion("subsc_date <>", value, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateGreaterThan(Date value) {
            addCriterion("subsc_date >", value, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateGreaterThanOrEqualTo(Date value) {
            addCriterion("subsc_date >=", value, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateLessThan(Date value) {
            addCriterion("subsc_date <", value, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateLessThanOrEqualTo(Date value) {
            addCriterion("subsc_date <=", value, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateIn(List<Date> values) {
            addCriterion("subsc_date in", values, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateNotIn(List<Date> values) {
            addCriterion("subsc_date not in", values, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateBetween(Date value1, Date value2) {
            addCriterion("subsc_date between", value1, value2, "subscDate");
            return (Criteria) this;
        }

        public Criteria andSubscDateNotBetween(Date value1, Date value2) {
            addCriterion("subsc_date not between", value1, value2, "subscDate");
            return (Criteria) this;
        }

        public Criteria andYieldRateIsNull() {
            addCriterion("yield_rate is null");
            return (Criteria) this;
        }

        public Criteria andYieldRateIsNotNull() {
            addCriterion("yield_rate is not null");
            return (Criteria) this;
        }

        public Criteria andYieldRateEqualTo(BigDecimal value) {
            addCriterion("yield_rate =", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateNotEqualTo(BigDecimal value) {
            addCriterion("yield_rate <>", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateGreaterThan(BigDecimal value) {
            addCriterion("yield_rate >", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("yield_rate >=", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateLessThan(BigDecimal value) {
            addCriterion("yield_rate <", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("yield_rate <=", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateIn(List<BigDecimal> values) {
            addCriterion("yield_rate in", values, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateNotIn(List<BigDecimal> values) {
            addCriterion("yield_rate not in", values, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("yield_rate between", value1, value2, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("yield_rate not between", value1, value2, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldNumIsNull() {
            addCriterion("yield_num is null");
            return (Criteria) this;
        }

        public Criteria andYieldNumIsNotNull() {
            addCriterion("yield_num is not null");
            return (Criteria) this;
        }

        public Criteria andYieldNumEqualTo(BigDecimal value) {
            addCriterion("yield_num =", value, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumNotEqualTo(BigDecimal value) {
            addCriterion("yield_num <>", value, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumGreaterThan(BigDecimal value) {
            addCriterion("yield_num >", value, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("yield_num >=", value, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumLessThan(BigDecimal value) {
            addCriterion("yield_num <", value, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("yield_num <=", value, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumIn(List<BigDecimal> values) {
            addCriterion("yield_num in", values, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumNotIn(List<BigDecimal> values) {
            addCriterion("yield_num not in", values, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("yield_num between", value1, value2, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andYieldNumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("yield_num not between", value1, value2, "yieldNum");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}