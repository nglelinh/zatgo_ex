package com.zatgo.zup.wallet.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.zatgo.zup.wallet.entity.FixedDepositSubsc;
import com.zatgo.zup.wallet.entity.FixedDepositSubscExample;

public interface FixedDepositSubscMapper {
    int countByExample(FixedDepositSubscExample example);

    int deleteByExample(FixedDepositSubscExample example);

    int deleteByPrimaryKey(String subscRecordId);

    int insert(FixedDepositSubsc record);

    int insertSelective(FixedDepositSubsc record);

    List<FixedDepositSubsc> selectByExample(FixedDepositSubscExample example);

    FixedDepositSubsc selectByPrimaryKey(String subscRecordId);

    int updateByExampleSelective(@Param("record") FixedDepositSubsc record, @Param("example") FixedDepositSubscExample example);

    int updateByExample(@Param("record") FixedDepositSubsc record, @Param("example") FixedDepositSubscExample example);

    int updateByPrimaryKeySelective(FixedDepositSubsc record);

    int updateByPrimaryKey(FixedDepositSubsc record);
    
    Page<FixedDepositSubsc> getNeedRansomListByPage();
}