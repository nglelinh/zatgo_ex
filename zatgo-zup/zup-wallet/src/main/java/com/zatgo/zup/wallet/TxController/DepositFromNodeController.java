package com.zatgo.zup.wallet.TxController;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.DepositInsertParams;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.wallet.entity.DepositRecord;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.service.CoinInfoService;
import com.zatgo.zup.wallet.service.DepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nob.
 * wiki: http://wiki.365os.com/pages/viewpage.action?pageId=819516
 * 充币
 */
@RestController
@RequestMapping(value = "/wallet")
public class DepositFromNodeController {
	private static final Logger logger = LoggerFactory.getLogger(DepositFromNodeController.class);
	@Autowired
	private DepositService depositService;
	@Autowired
	private CoinInfoService coinInfoService;
	@Autowired
	private RedisLockUtils redisLockUtils;
	@RequestMapping(value = "/depositNotify", method = RequestMethod.POST)
	public @ResponseBody
	Map<String, ? extends Object> depositNotify(@RequestBody Map<String, String> paramsMap) {
		logger.info("request depositNotify, paramsMap={}", paramsMap);
		String code = null;
		Map responseMap = new HashMap();
		String timestamp = paramsMap.get("timestamp");
		String symbol = paramsMap.get("symbol");
		String addressTo = paramsMap.get("address_to");
		String txid = paramsMap.get("txid");
		String amount = paramsMap.get("amount");
		String confirm = paramsMap.get("confirm");
		//todo 是否为挖矿，zup目前没有记录挖矿收益
		String mining = paramsMap.get("is_mining");
		if (timestamp == null || symbol == null || addressTo == null
				|| txid == null || amount == null
				|| confirm == null || mining == null) {
			code = Constants.CODE_ARGS_ILLEGAL;
		} else {
			//解决同一订单并发问题
			String accountLockKey = RedisKeyConstants.BUSI_DEPOSIT + txid;
			try {
				redisLockUtils.lock(accountLockKey);
				symbol = symbol.toUpperCase();
				//获取这条充币记录
				DepositRecord depositRecord = depositService.selectRecord(symbol, addressTo, txid);
				
				//获取确认数
				IssuedCoinInfo issuedCoinInfo = coinInfoService.selectSystemCoinInfoByCoinType(symbol.toUpperCase());
				if(issuedCoinInfo == null) {
					logger.error("币种=" + symbol + "不存在");
					issuedCoinInfo = new IssuedCoinInfo();
					issuedCoinInfo.setCoinType(symbol.toUpperCase());
					issuedCoinInfo.setCoinNetworkType("未知");
				}
				Integer needConfirm = issuedCoinInfo.getConfirmNum();
				if(needConfirm == null) {
					logger.error("币种=" + symbol + " 未配置确认数");
					needConfirm = 9999;
				}
				
				
				//该订单区块确认数是否足够
				boolean hasChecked = Integer.valueOf(confirm) >= needConfirm;
				//看看充值是否存在
				if (depositRecord != null) {
					//看看充值是否已经到账
					if (depositRecord.getDepositStatus().equals(BusinessEnum.DepositStatus.confirmed.getCode())) {
						code = Constants.CODE_SUCCESS;
						logger.info("deposit already completed");
					} else {
						//如果当前确认数大于阀值
						if (hasChecked) {
							//给当前钱包的当前币种加钱,并标识后返回
							depositService.deposit(depositRecord.getDepositId());
							code = Constants.CODE_SUCCESS;
						}
					}
				} else {
					//记录不存在,则创建记录
					try {
						DepositInsertParams depositInsertParams = new DepositInsertParams();
						depositInsertParams.setTxHash(txid);
						depositInsertParams.setAmount(amount);
						depositInsertParams.setToAddress(addressTo);
						//depositInsertParams.setCoinType(BusinessEnum.CurrencyEnum.valueOf(symbol));
						//depositInsertParams.setNetWorkType(BusinessEnum.NetWorkType.valueOf(issuedCoinInfo.getCoinNetworkType()));
						depositInsertParams.setCoinType(issuedCoinInfo.getCoinType());
						//todo 交易机传入参数只有币种类型，需要根据币种获取下网络类型
						depositInsertParams.setNetWorkType(issuedCoinInfo.getCoinNetworkType());
						//创建记录
						String id = depositService.insertDepositRecored(depositInsertParams);
						if (hasChecked && !StringUtils.isEmpty(id)) {
							//给当前钱包的当前币种加钱,并标识后返回
							depositService.deposit(id);
							code = Constants.CODE_SUCCESS;
						}
						code = Constants.CODE_SUCCESS;
					} catch (BusinessException e) {
						if (BusinessExceptionCode.REPETITIVE_DATA_PROCESSING.equals(e.getCode())){
							code = Constants.CODE_SUCCESS;
						} else {
							logger.error("", e);
							logger.error("coin symbol not exist, paramsMap=", ">>>>>>>>>>>>>" + paramsMap + "<<<<<<<<<<<<<");
							code = Constants.CODE_BUSSINESS_FAIL;
						}
					} catch (Exception e) {
						logger.error("", e);
						logger.error("deposit update error, paramsMap={}", paramsMap);
						code = Constants.CODE_RPOCESS_FAIL;
					}
				}
			}finally {
				redisLockUtils.releaseLock(accountLockKey);
			}
		}
		responseMap.put("errno", code);
		responseMap.put("errmsg", Constants.getMessage(code));
		logger.info("response depositNotify, result={}", responseMap);

		return responseMap;
	}
}