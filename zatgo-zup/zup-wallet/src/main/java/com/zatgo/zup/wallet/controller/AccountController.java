package com.zatgo.zup.wallet.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wallet.WalletAccountData;
import com.zatgo.zup.wallet.entity.WalletAccount;
import com.zatgo.zup.wallet.service.AccountService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api(value = "/wallet/account",description = "账户相关",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/wallet/account")
public class AccountController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
	
	@Autowired
	private AccountService accountService;
	@ApiOperation(value = "获取账户列表")
	@RequestMapping(value = "/list",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<List<WalletAccountData>> listAccount(){
		AuthUserInfo userInfo = getUserInfo();
		return BusinessResponseFactory.createSuccess(accountService.listAccount(userInfo.getUserId(), userInfo.getCloudUserId()));
	}
	@ApiOperation(value = "根据账户ID获取账户")
	@RequestMapping(value = "/{accountId}",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<WalletAccountData> getAccountByAccountId(@PathVariable("accountId") String accountId){
		return BusinessResponseFactory.createSuccess(accountService.getAccountByAccountId(accountId));
	}
	@ApiOperation(value = "新建账户")
	@RequestMapping(value = "/create",method = RequestMethod.POST )
	@ResponseBody
	public ResponseData<WalletAccountData> createAccount(@RequestBody CreateAccountParams params){
		//只能为自己开账户
		AuthUserInfo userInfo = getUserInfo();
		if(!params.getUserId().equals(userInfo.getUserId())) {
			throw new BusinessException(BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
		}
		
		WalletAccount walletAccount = accountService.createAccount(params);
		WalletAccountData accountData = new WalletAccountData();
		try {
			BeanUtils.copyProperties(accountData, walletAccount);
		} catch (IllegalAccessException e) {
			logger.error("",e);
		} catch (InvocationTargetException e) {
			logger.error("",e);
		}
		return BusinessResponseFactory.createSuccess(accountData);
	}
	
	@ApiOperation(value = "新建账户")
	@RequestMapping(value = "/innerCreate",method = RequestMethod.POST )
	@ResponseBody
	public ResponseData<WalletAccountData> innerCreateAccount(@RequestBody CreateAccountParams params){
		WalletAccount walletAccount = accountService.createAccount(params);
		WalletAccountData accountData = new WalletAccountData();
		try {
			BeanUtils.copyProperties(accountData, walletAccount);
		} catch (IllegalAccessException e) {
			logger.error("",e);
		} catch (InvocationTargetException e) {
			logger.error("",e);
		}
		return BusinessResponseFactory.createSuccess(accountData);
	}
	
	@ApiOperation(value = "设置账户为主账户")
	@RequestMapping(value = "/primary",method = RequestMethod.PUT )
	@ResponseBody
	public ResponseData<String> setAccountAsPrimary(@RequestParam("accountId") String accountId){
		
		//校验入参的合法性
		AuthUserInfo userInfo = getUserInfo();
		List<WalletAccountData> accounts = accountService.listAccount(userInfo.getUserId(), userInfo.getCloudUserId());
		String currentAccountId = null;
		for(WalletAccountData account: accounts) {
			if(account.getAccountId().equals(accountId)) {
				currentAccountId = account.getAccountId();
				break;
			}
		}
		if(currentAccountId == null) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
		}
		
		return BusinessResponseFactory.createSuccess(accountService.setAccountAsPrimary(accountId,userInfo.getUserId()));
	}
	@ApiOperation(value = "更新账户名称")
	@RequestMapping(value = "/{accountId}/{name}",method = RequestMethod.PUT )
	@ResponseBody
	public ResponseData<Object> updateAccountName(@PathVariable("accountId") String accountId,@PathVariable("name") String name){
		accountService.updateAccountName(accountId,name);
		return BusinessResponseFactory.createSuccess(null);
	}
	@ApiOperation(value = "删除账户")
	@RequestMapping(value = "/{accountId}",method = RequestMethod.DELETE )
	@ResponseBody
	public ResponseData<Object> deleteAccount(@PathVariable("accountId") String accountId){
		accountService.deleteAccount(accountId);
		return BusinessResponseFactory.createSuccess(null);
	}

	@ApiOperation(value = "查询主账户")
	@RequestMapping(value = "/primary",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<WalletAccount> selectUserPrimaryAccount(){
		AuthUserInfo userInfo = getUserInfo();
		return BusinessResponseFactory.createSuccess(accountService.selectUserPrimaryAccount(userInfo.getUserId()));
	}

	@ApiOperation(value = "根据用户ID和账户类型查询账户信息")
	@RequestMapping(value = "/primary/{type}",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<WalletAccountData> selectByUserIdAndType(@PathVariable("type")String type){
		AuthUserInfo userInfo = getUserInfo();
		WalletAccount walletAccount = accountService.selectByUserIdAndType(userInfo.getUserId(),type);
		WalletAccountData data = null;
		if(walletAccount != null) {
			data = new WalletAccountData();
			try {
				BeanUtils.copyProperties(data, walletAccount);
			} catch (Exception e) {
				logger.error("",e);
				throw new BusinessException();
			}
		}
		
		return BusinessResponseFactory.createSuccess(data);
	}
	@RequestMapping(value = "/aggregate/{address}/{type}",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<WalletAccount> selectByAddressAndCoinType(@PathVariable("address") String address
															,@PathVariable("type") String type){
		return BusinessResponseFactory.createSuccess(accountService.selectByAddressAndCoinType(address,type));
	}
	
}
