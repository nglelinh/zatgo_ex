package com.zatgo.zup.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class DepositRecord implements Serializable {
    private String depositId;

    private String accountId;

    /**
     * 多个地址使用逗号隔开
     */
    private String fromAddresss;

    private String toAddress;

    private Date depositDate;

    private Date depositSuccessDate;

    private BigDecimal depositNumber;

    /**
     * 0：充值中
            1：充值成功
            2：充值失败
     */
    private String depositStatus;

    private String txHash;

    private String blockHash;

    private Integer blockNumber;

    private String coinType;
    
    private String coinNetworkType;

    public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}

	private static final long serialVersionUID = 1L;

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFromAddresss() {
        return fromAddresss;
    }

    public void setFromAddresss(String fromAddresss) {
        this.fromAddresss = fromAddresss;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public Date getDepositSuccessDate() {
        return depositSuccessDate;
    }

    public void setDepositSuccessDate(Date depositSuccessDate) {
        this.depositSuccessDate = depositSuccessDate;
    }

    public BigDecimal getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(BigDecimal depositNumber) {
        this.depositNumber = depositNumber;
    }

    public String getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(String depositStatus) {
        this.depositStatus = depositStatus;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public Integer getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(Integer blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DepositRecord other = (DepositRecord) that;
        return (this.getDepositId() == null ? other.getDepositId() == null : this.getDepositId().equals(other.getDepositId()))
            && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
            && (this.getFromAddresss() == null ? other.getFromAddresss() == null : this.getFromAddresss().equals(other.getFromAddresss()))
            && (this.getToAddress() == null ? other.getToAddress() == null : this.getToAddress().equals(other.getToAddress()))
            && (this.getDepositDate() == null ? other.getDepositDate() == null : this.getDepositDate().equals(other.getDepositDate()))
            && (this.getDepositSuccessDate() == null ? other.getDepositSuccessDate() == null : this.getDepositSuccessDate().equals(other.getDepositSuccessDate()))
            && (this.getDepositNumber() == null ? other.getDepositNumber() == null : this.getDepositNumber().equals(other.getDepositNumber()))
            && (this.getDepositStatus() == null ? other.getDepositStatus() == null : this.getDepositStatus().equals(other.getDepositStatus()))
            && (this.getTxHash() == null ? other.getTxHash() == null : this.getTxHash().equals(other.getTxHash()))
            && (this.getBlockHash() == null ? other.getBlockHash() == null : this.getBlockHash().equals(other.getBlockHash()))
            && (this.getBlockNumber() == null ? other.getBlockNumber() == null : this.getBlockNumber().equals(other.getBlockNumber()))
            && (this.getCoinType() == null ? other.getCoinType() == null : this.getCoinType().equals(other.getCoinType()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getDepositId() == null) ? 0 : getDepositId().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getFromAddresss() == null) ? 0 : getFromAddresss().hashCode());
        result = prime * result + ((getToAddress() == null) ? 0 : getToAddress().hashCode());
        result = prime * result + ((getDepositDate() == null) ? 0 : getDepositDate().hashCode());
        result = prime * result + ((getDepositSuccessDate() == null) ? 0 : getDepositSuccessDate().hashCode());
        result = prime * result + ((getDepositNumber() == null) ? 0 : getDepositNumber().hashCode());
        result = prime * result + ((getDepositStatus() == null) ? 0 : getDepositStatus().hashCode());
        result = prime * result + ((getTxHash() == null) ? 0 : getTxHash().hashCode());
        result = prime * result + ((getBlockHash() == null) ? 0 : getBlockHash().hashCode());
        result = prime * result + ((getBlockNumber() == null) ? 0 : getBlockNumber().hashCode());
        result = prime * result + ((getCoinType() == null) ? 0 : getCoinType().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", depositId=").append(depositId);
        sb.append(", accountId=").append(accountId);
        sb.append(", fromAddresss=").append(fromAddresss);
        sb.append(", toAddress=").append(toAddress);
        sb.append(", depositDate=").append(depositDate);
        sb.append(", depositSuccessDate=").append(depositSuccessDate);
        sb.append(", depositNumber=").append(depositNumber);
        sb.append(", depositStatus=").append(depositStatus);
        sb.append(", txHash=").append(txHash);
        sb.append(", blockHash=").append(blockHash);
        sb.append(", blockNumber=").append(blockNumber);
        sb.append(", coinType=").append(coinType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}