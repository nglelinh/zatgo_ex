package com.zatgo.zup.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ExtractRecordData implements Serializable {

	private String extractId;

	private String accountId;

	private String extractAddress;

	private Date extractDate;

	private Date extractSuccessDate;

	private String extractNumber;

	/**
	 * 0：提币中，1：提币成功，2：审批中，3：审批通过
	 */
	private String extractStatus;

	private String txHash;

	private String blockHash;

	private String fee;

	private String approveReason;

	private String approveResult;

	private String coinType;

	private String coinNetworkType;

	private String charge;

	public String getExtractId() {
		return extractId;
	}

	public void setExtractId(String extractId) {
		this.extractId = extractId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getExtractAddress() {
		return extractAddress;
	}

	public void setExtractAddress(String extractAddress) {
		this.extractAddress = extractAddress;
	}

	public Date getExtractDate() {
		return extractDate;
	}

	public void setExtractDate(Date extractDate) {
		this.extractDate = extractDate;
	}

	public Date getExtractSuccessDate() {
		return extractSuccessDate;
	}

	public void setExtractSuccessDate(Date extractSuccessDate) {
		this.extractSuccessDate = extractSuccessDate;
	}

	public String getExtractNumber() {
		return extractNumber;
	}

	public void setExtractNumber(String extractNumber) {
		this.extractNumber = extractNumber;
	}

	public String getExtractStatus() {
		return extractStatus;
	}

	public void setExtractStatus(String extractStatus) {
		this.extractStatus = extractStatus;
	}

	public String getTxHash() {
		return txHash;
	}

	public void setTxHash(String txHash) {
		this.txHash = txHash;
	}

	public String getBlockHash() {
		return blockHash;
	}

	public void setBlockHash(String blockHash) {
		this.blockHash = blockHash;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getApproveReason() {
		return approveReason;
	}

	public void setApproveReason(String approveReason) {
		this.approveReason = approveReason;
	}

	public String getApproveResult() {
		return approveResult;
	}

	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}
}
