package com.zatgo.zup.wallet.utils;

import com.zatgo.zup.common.encrypt.BCryptUtil;
import com.zatgo.zup.common.encrypt.RSAServerUtil;
import com.zatgo.zup.common.enumtype.ClientTypeEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.wallet.remoteservice.UserRemoteService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class PasswordVerify {
	/**
	 * 账号锁定判断次数
	 */
	@Value("${user.paypass.lock.times}")
	private int lockTimes;
	/**
	 * 账号锁定时长
	 */
	@Value("${user.paypass.lock.expire}")
	private int lockExpire;

	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private UserRemoteService userRemoteService;
	public void verify(String password,String userId){
		UserData userData = userRemoteService.getUserById(userId).getData();
		String lockKey = RedisKeyConstants.USER_PAY_PASS_LOCK + userId;
		if(isLockTime(lockKey)){
			throw new BusinessException(BusinessExceptionCode.PAY_PASSWORD_LOCK);
		} else {
			releaseLock(lockKey);
		}
//		String _password = "";
//		try {
//			_password = RSAServerUtil.decrypt(password, ClientTypeEnum.PHONE);
//		} catch (Exception e) {
//			throw new BusinessException(BusinessExceptionCode.PAY_PASSWORD_ERROR);
//		}
		boolean check = BCryptUtil.check("", password, userData.getUserPayPassword());
		if(!check){
			//密码校验失败，需要记录失败次数，超过N次则锁定账号N小时（配置文件可配）
			long residueNum = incPaypassFailNumber(lockKey);
			if(residueNum == 0) {
				throw new BusinessException(BusinessExceptionCode.PAY_PASSWORD_LOCK);
			}else {
				throw new BusinessException(BusinessExceptionCode.PAY_PASSWORD_ERROR);
			}
		}
		releaseLock(lockKey);
	}
	/**
	 * 判断账号是否在锁定期内
	 * @param lockKey
	 * @return
	 */
	private boolean isLockTime(String lockKey) {
		if(redisTemplate.hasKey(lockKey)){
			int times = Integer.parseInt((String)redisTemplate.opsForValue().get(lockKey));
			if(times>=lockTimes){
				return true;
			}
		}
		return false;
	}

	/**
	 * 释放锁定的计数
	 * @param lockKey
	 */
	private void releaseLock(String lockKey) {
		redisTemplate.delete(lockKey);
	}

	private long incPaypassFailNumber(String lockKey) {

		long times = 1;

		if(!redisTemplate.hasKey(lockKey)){
			redisTemplate.opsForValue().set(lockKey,String.valueOf(times));
		}else{
			times = redisTemplate.opsForValue().increment(lockKey, 1);
		}
		//设置失效时间
		redisTemplate.expire(lockKey, lockExpire, TimeUnit.SECONDS);

		return lockTimes - times;
	}

}
