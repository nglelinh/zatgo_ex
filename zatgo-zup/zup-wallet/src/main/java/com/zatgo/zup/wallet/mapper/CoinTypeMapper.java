package com.zatgo.zup.wallet.mapper;

import com.zatgo.zup.wallet.entity.CoinType;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface CoinTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CoinType record);

    int insertSelective(CoinType record);

    CoinType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CoinType record);

    int updateByPrimaryKey(CoinType record);

	List<CoinType> selectAllCoinTypes();

	BigDecimal selectCoinRate(@Param("coinType") String coinType);
}