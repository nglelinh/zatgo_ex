package com.zatgo.zup.wallet.utils;

import java.util.Properties;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.neemre.btcdcli4j.core.BitcoindException;
import com.neemre.btcdcli4j.core.CommunicationException;
import com.neemre.btcdcli4j.core.client.BtcdClient;
import com.neemre.btcdcli4j.core.client.BtcdClientImpl;
@Component
public class Btcd4j {
	
	private static final Logger logger = LoggerFactory.getLogger(Btcd4j.class);
	
	private BtcdClient client;
	@Value("${btcdcli4j.protocol}")
	private String protocol;
	@Value("${btcdcli4j.host}")
	private String host;
	@Value("${btcdcli4j.port}")
	private String port;
	@Value("${btcdcli4j.user}")
	private String user;
	@Value("${btcdcli4j.password}")
	private String password;
	@Value("${btcdcli4j.auth_scheme}")
	private String auth_scheme;
	
	private ReentrantLock lock = new ReentrantLock();
	
	public void init(){
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		CloseableHttpClient httpProvider = HttpClients.custom().setConnectionManager(cm)
				.build();
		Properties nodeConfig = new Properties();
		nodeConfig.put("node.bitcoind.rpc.protocol",protocol);
		nodeConfig.put("node.bitcoind.rpc.host",host);
		nodeConfig.put("node.bitcoind.rpc.port",port);
		nodeConfig.put("node.bitcoind.rpc.user",user);
		nodeConfig.put("node.bitcoind.rpc.password",password);
		nodeConfig.put("node.bitcoind.http.auth_scheme",auth_scheme);
		try {
			client = new BtcdClientImpl(httpProvider, nodeConfig);
		} catch (BitcoindException e) {
			logger.error("Bitcoind客户端连接失败",e);
		} catch (CommunicationException e) {
			logger.error("Bitcoind客户端连接失败",e);
		}
	}

	public BtcdClient getClient(){
		if(client == null) {
			lock.lock();
			try {
				if(client == null) {
					init();
				}
			}catch(Exception e) {
				logger.error("",e);
			}finally {
				lock.unlock();
			}
		}
		return this.client;
	}









}
