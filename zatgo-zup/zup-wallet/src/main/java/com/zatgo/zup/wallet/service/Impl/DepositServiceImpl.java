package com.zatgo.zup.wallet.service.Impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.BusinessEnum.DepositStatus;
import com.zatgo.zup.common.enumtype.BusinessEnum.PaymentRecordType;
import com.zatgo.zup.common.enumtype.BusinessEnum.ReceiveType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.DepositInsertParams;
import com.zatgo.zup.common.model.DepositRecordData;
import com.zatgo.zup.common.model.MiningDepositInsertParams;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ReceiveMsg;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.model.wallet.DepositRecordByCloudUserParams;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.common.mq.MQProducer;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.sms.santi.SantiService;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.wallet.entity.AccountAddress;
import com.zatgo.zup.wallet.entity.DepositRecord;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.entity.PaymentRecord;
import com.zatgo.zup.wallet.entity.WalletAccount;
import com.zatgo.zup.wallet.mapper.AccountAddressMapper;
import com.zatgo.zup.wallet.mapper.DepositRecordMapper;
import com.zatgo.zup.wallet.mapper.PaymentRecordMapper;
import com.zatgo.zup.wallet.mapper.WalletAccountMapper;
import com.zatgo.zup.wallet.model.DepositAddressParams;
import com.zatgo.zup.wallet.remoteservice.UserRemoteService;
import com.zatgo.zup.wallet.service.AccountService;
import com.zatgo.zup.wallet.service.AddressService;
import com.zatgo.zup.wallet.service.CoinInfoService;
import com.zatgo.zup.wallet.service.DepositService;
import com.zatgo.zup.wallet.service.WalletAccountService;

@Service
public class DepositServiceImpl implements DepositService {
	private static final Logger logger = LoggerFactory.getLogger(DepositServiceImpl.class);
	@Autowired
	private DepositRecordMapper depositRecordMapper;
	
	@Autowired
	private AccountAddressMapper accountAddressMapper;
	
	@Autowired
	private WalletAccountMapper walletAccountMapper;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private MQProducer mqProducer;
	
	@Autowired
	private WalletAccountService walletAccountService;
	
	@Autowired
	private CoinInfoService coinInfoService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private PaymentRecordMapper paymentRecordMapper;

	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@Autowired
	private SantiService santiService;
	@Autowired
	private UserRemoteService userRemoteService;

	@Value("${notice.admin.coinDynamic.template.id:}")
	private String adminCoinDynamicTemplateId;
	
	@Value("${notice.admin.coinDynamic.template.content:}")
	private String adminCoinDynamicTemplateContent;
	
	@Value("${notice.admin.coinDynamic.template.timeout:}")
	private String adminCoinDynamicTemplateTimeout;
	
	@Value("${notice.admin.phone:}")
	private String adminPhone;
	
	@Override
	@Transactional
	public String insertDepositRecored(DepositInsertParams params) {
		//幂等性处理
		List<DepositRecord>  oldReocrds = depositRecordMapper.selectRecordByTxHash(params.getTxHash());
		if(oldReocrds != null && oldReocrds.size() > 0) {
			throw new BusinessException(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING);
		}
		
		//当前地址是否有人用过
		List<AccountAddress> accountAddresss = 
				accountAddressMapper.selectByAddress(params.getToAddress());
		AccountAddress existAccountAddress = null;
		WalletAccount currentAccount = null;

		String id = UUIDUtils.getUuid();
		BigDecimal amount = new BigDecimal(params.getAmount());
		if(!CollectionUtils.isEmpty(accountAddresss)){
			for(AccountAddress accountAddress : accountAddresss) {
				WalletAccount account = walletAccountMapper.selectByPrimaryKey(accountAddress.getAccountId());
				if(currentAccount != null && !account.getUserId().equals(currentAccount.getUserId())) {//校验当前地址是否只有一个用户在使用
					logger.error("address repetitive use in different users (address=" + params.getToAddress() + ")");
					throw new BusinessException();
				}
				currentAccount = account;
				if(account.getCoinType().equals(params.getCoinType())) {//找到已使用的地址，则结束
					existAccountAddress = accountAddress;
					break;
				}
			}
			//如果有人用过，但没有确定指定币种，则新增
			if(existAccountAddress ==null) {
				//不存在账户地址
				DepositAddressParams addrParams = new DepositAddressParams();
				addrParams.setCoinType(params.getCoinType());
				addrParams.setNetworkType(params.getNetWorkType());
				addrParams.setUserId(currentAccount.getUserId());
				existAccountAddress = addressService.createDepostAddressV2(addrParams);
			}
			
			DepositRecord depositRecord = new DepositRecord();
			depositRecord.setAccountId(existAccountAddress.getAccountId());
			depositRecord.setDepositId(id);
			depositRecord.setToAddress(params.getToAddress());
			depositRecord.setDepositDate(new Date());
			depositRecord.setDepositStatus(DepositStatus.confirming.getCode());
			depositRecord.setTxHash(params.getTxHash());
			depositRecord.setDepositNumber(amount);
			depositRecord.setBlockNumber(params.getBlockNumber());
			depositRecord.setCoinType(params.getCoinType());
			depositRecord.setBlockHash(params.getBlockHash());
			depositRecord.setCoinNetworkType(params.getNetWorkType());
			depositRecordMapper.insertSelective(depositRecord);
			return id;
		}else {
//			IssuedCoinInfo issuedCoinInfo = coinInfoService.selectByCoinType(params.getCoinType());
//			if(issuedCoinInfo == null) {
//				logger.error(BusinessExceptionCode.getMessage(BusinessExceptionCode.NOT_SUPPORT_COIN_NETWORK));
//				return null;
//			}
			
			DepositRecord depositRecord = new DepositRecord();
			depositRecord.setDepositId(id);
			depositRecord.setToAddress(params.getToAddress());
			depositRecord.setDepositDate(new Date());
			depositRecord.setDepositStatus(DepositStatus.confirming.getCode());
			depositRecord.setTxHash(params.getTxHash());
			depositRecord.setDepositNumber(amount);
			depositRecord.setBlockNumber(params.getBlockNumber());
			depositRecord.setCoinType(params.getCoinType());
			depositRecord.setBlockHash(params.getBlockHash());
			depositRecord.setCoinNetworkType(params.getNetWorkType());
			depositRecordMapper.insertSelective(depositRecord);
			logger.error("==============address not use in users (data=" + JSONObject.toJSONString(depositRecord) + ")================");
			return id;
		}
		
	}

	@Override
	public List<DepositRecord> selectUnConfirmed() {
		return depositRecordMapper.selectUnConfirmed();
	}

	@Override
	public String getAddressForDeposit(String accountId, Byte type) {
		return null;
	}

	@Override
	public List<DepositRecord> listRecordByUserId(String userId, String cloudUserId) {
		//获取云账户配置
		List<IssuedCoinInfo> issuedCoinInfos = coinInfoService.selectCoinListByCloudUserId(cloudUserId);
		StringBuffer coinType = new StringBuffer();
		if (!CollectionUtils.isEmpty(issuedCoinInfos)){
			for (IssuedCoinInfo info : issuedCoinInfos){
				if ("1".equals(info.getStatus().toString()))
					coinType.append(",'" + info.getCoinType() + "'");
			}
		}
		return depositRecordMapper.selectRecordByUserId(userId, coinType.toString().replaceFirst(",", ""));
	}

	@Override
	@Transactional
	public void deposit(String depositId) {
		//根据冲币记录ID查记录
		DepositRecord depositRecord = depositRecordMapper.selectByPrimaryKey(depositId);
		logger.info(depositId);
		if(depositRecord == null) {
			throw new BusinessException(BusinessExceptionCode.RECORD_NOT_EXIST);
		}
		
		//幂等性处理
		if(depositRecord.getDepositStatus().equals(DepositStatus.confirmed.getCode())) {
			throw new BusinessException(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING);
		}
		//将该条记录的状态改为充值成功
		depositRecordMapper.confirmedRecord(depositId);
		if(depositRecord.getAccountId() != null) {
			//将该币种的钱包上锁
			WalletAccount walletAccount = walletAccountMapper.selectByAccountIdForUpdate(depositRecord.getAccountId());
			//给自己加钱
			walletAccountMapper.updateBalance(walletAccount.getAccountId(),depositRecord.getDepositNumber());
			//获取云用户管理员userId
			String userId = walletAccount.getUserId();
			ResponseData<UserData> res = userRemoteService.getManageUserId(
					userId,BusinessEnum.CloudManageUserType.DEPOSIT.getType().toString());
			if (res == null || !res.isSuccessful() || res.getData() == null){
				throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
			}
			UserData userData = res.getData();
			//如果他自己不是云管理员
			if (!userId.equals(userData.getUserId())) {
				//获取云管理员钱包
				CreateAccountParams params = new CreateAccountParams();
				params.setCurrencyType(depositRecord.getCoinType());
				params.setNetworkType(depositRecord.getCoinNetworkType());
				params.setUserId(userData.getUserId());
				params.setName(depositRecord.getCoinType());
				WalletAccount wallet = accountService.createAccount(params);
				//给云钱包加钱
				walletAccountMapper.updateBalance(wallet.getAccountId(), depositRecord.getDepositNumber());
			}
			ReceiveMsg receiveMsg = new ReceiveMsg();
			receiveMsg.setUserId(walletAccount.getUserId());
			receiveMsg.setAmount(depositRecord.getDepositNumber());
			receiveMsg.setNetWorkType(depositRecord.getCoinNetworkType());
			receiveMsg.setCoinType(depositRecord.getCoinType().toUpperCase());
			receiveMsg.setType(ReceiveType.deposit.getCode());
			try {
				mqProducer.send(MQContants.TOPIC_COIN_RECEIVE,null, JSON.toJSONString(receiveMsg),depositId);
			} catch (Exception e) {
				logger.error("充值成功，发送提醒消息失败：充值记录 ："+JSON.toJSONString(depositRecord));
			}
			
			try {
				santiService.sendSms(CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_SMS_SIGN, "用户充值"+receiveMsg.getCoinType()+receiveMsg.getAmount().toString() + "|" + adminCoinDynamicTemplateTimeout, adminPhone, adminCoinDynamicTemplateId);
			}catch(Exception e) {
				logger.error("用户充值到账，发送短信给管理员失败");
			}
		}else {
			logger.info("系统充值：" + JSON.toJSONString(depositRecord));
			
			try {
				santiService.sendSms(CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_SMS_SIGN, "系统充值"+depositRecord.getCoinType()+depositRecord.getDepositNumber().toString() + "|" + adminCoinDynamicTemplateTimeout, adminPhone, adminCoinDynamicTemplateId);
			}catch(Exception e) {
				logger.error("系统充值到账，发送短信给管理员失败");
			}
		}
	}

	@Override
	public DepositRecord selectRecord(String type, String address, String txId) {
		return depositRecordMapper.selectRecord(txId,type,address);
	}

	@Override
	public DepositRecord saveRecord(String symbol, String addrTo, String txid, BigDecimal amount, Integer confirm, Integer mining) {
		return null;
	}

	@Override
	@Transactional
	public void insertMiningDepositRecored(MiningDepositInsertParams params) {
		String coinType = params.getCoinType().toUpperCase();
		String netWorkType = params.getNetWorkType().toUpperCase();
		BigDecimal amount = new BigDecimal(params.getAmount());
		String recordId = params.getRecordId();
		String userId = params.getUserId();
		
		ResponseData<UserData> responseData = userRemoteService.getUserById(userId);
		if(!responseData.isSuccessful()) {
			throw new BusinessException(responseData.getCode());
		}
		if(responseData.getData() == null) {
			throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		//云用户管理员不能挖矿
		if(responseData.getData().getIsCloudManager().equals(new Byte("1"))) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		//解决同一用户并发问题
		String accountLockKey = RedisKeyConstants.MINING_LOCK + userId;
		redisLockUtils.lock(accountLockKey);
		try{
			//先查交易是否完成
			PaymentRecord paymentRecord = paymentRecordMapper.selectRecordByOrderIdAndReceiptUserId(recordId, userId);
			if (paymentRecord == null) {
				paymentRecord = new PaymentRecord();
				WalletAccount wallet = walletAccountMapper.selectByUserIdAndType(userId, coinType);
				//如果没有钱包则创建
				if (wallet == null) {
					CreateAccountParams account = new CreateAccountParams();
					account.setName(coinType);
					account.setUserId(userId);
					account.setCurrencyType(coinType);
					account.setNetworkType(netWorkType);
					wallet = accountService.createAccount(account);
				}
				
				//获取云用户管理员userId
				ResponseData<UserData> res = userRemoteService.getManageUserId(
						userId,BusinessEnum.CloudManageUserType.MINING.getType().toString());
				if (!res.isSuccessful()){
					throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
				}
				UserData cloudManageUserData = res.getData();
				
				//给云钱包扣钱
				WalletAccount yunWallet = walletAccountMapper.selectByUserIdAndTypeForUpdate(
						cloudManageUserData.getUserId(), wallet.getCoinType());
				if(yunWallet == null){
					CreateAccountParams yunAccount = new CreateAccountParams();
					yunAccount.setName(coinType);
					yunAccount.setUserId(cloudManageUserData.getUserId());
					yunAccount.setCurrencyType(coinType);
					yunAccount.setNetworkType(netWorkType);
					yunWallet = accountService.createAccount(yunAccount);
				}
				walletAccountMapper.updateBalance(yunWallet.getAccountId(), amount.negate());

				//用户钱包加钱
				walletAccountMapper.updateBalance(wallet.getAccountId(), amount);

				Byte code = params.getType().getCode();

				ReceiveMsg receiveMsg = new ReceiveMsg();
				receiveMsg.setUserId(userId);
				receiveMsg.setAmount(amount);
				receiveMsg.setNetWorkType(netWorkType);
				receiveMsg.setCoinType(coinType);
				receiveMsg.setType(ReceiveType.deposit.getCode());
				//添加记录
				paymentRecord.setPaymentId(UUIDUtils.getUuid());
				paymentRecord.setAmount(amount);
				paymentRecord.setOrderId(recordId);
				paymentRecord.setPaymentDate(new Date());
				paymentRecord.setPaymentAccountId(null);
				paymentRecord.setReceiptAccountId(wallet.getAccountId());
				paymentRecord.setPayUserId(cloudManageUserData.getUserId());
				paymentRecord.setPayUserName(getPayType(code));
				paymentRecord.setPaymentAccountId(yunWallet.getAccountId());
				paymentRecord.setReceiptUserName(params.getUserName());
				paymentRecord.setReceiptUserId(userId);
				paymentRecord.setCoinType(coinType);
				paymentRecord.setPaymentType(code);
				paymentRecordMapper.insertSelective(paymentRecord);
			}
		} finally {
			redisLockUtils.releaseLock(accountLockKey);
		}

	}

	private String getPayType(Byte type){
		if (PaymentRecordType.mining.getCode().equals(type))
			return "mining";
		if (PaymentRecordType.task.getCode().equals(type))
			return "任务奖励";

		return "";
	}

	@Override
	public PageInfo<DepositRecordData> selectDepositRecordByCloudUser(String cloudUserId,DepositRecordByCloudUserParams params) {
		String status = null;
		if(params.getStatus() != null) {
			status = params.getStatus().getCode();
		}
		PageHelper.startPage(Integer.valueOf(params.getPageNo()), Integer.valueOf(params.getPageSize()));
		Page<DepositRecord> pageInfo = depositRecordMapper.selectDepositRecordByCloudUser(
				params.getKey(),cloudUserId,status,params.getCoinType());
		
		List<DepositRecordData> list = new ArrayList<>();
		for(DepositRecord record:pageInfo.getResult()) {
			DepositRecordData data = new DepositRecordData();
			try {
				BeanUtils.copyProperties(data, record);
				list.add(data);
			} catch (Exception e) {
				logger.error("",e);
				throw new BusinessException();
			}
		}
		
		PageInfo<DepositRecordData> pageInfoData =
				new PageInfo<DepositRecordData>(pageInfo.getPageNum(),pageInfo.getPageSize(),pageInfo.getTotal(),list);

		PageHelper.clearPage();
		return pageInfoData;
	}
}
