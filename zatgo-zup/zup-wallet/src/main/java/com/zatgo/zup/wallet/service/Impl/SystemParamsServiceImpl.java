package com.zatgo.zup.wallet.service.Impl;

import java.math.BigDecimal;
import java.util.List;

import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.entity.SysParams;
import com.zatgo.zup.wallet.mapper.IssuedCoinInfoMapper;
import com.zatgo.zup.wallet.mapper.SysParamsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.wallet.entity.CoinType;
import com.zatgo.zup.wallet.mapper.CoinTypeMapper;
import com.zatgo.zup.wallet.service.SystemParamsService;
import org.springframework.util.StringUtils;

@Component
public class SystemParamsServiceImpl implements SystemParamsService {
	@Autowired
	private IssuedCoinInfoMapper issuedCoinInfoMapper;
	@Autowired
	private SysParamsMapper sysParamsMapper;


	@Override
	public List<IssuedCoinInfo> queryCoinTypes(String cloudUserId) {
		if (StringUtils.isEmpty(cloudUserId)){
			cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
		}
		return issuedCoinInfoMapper.selectAllCoins(cloudUserId);
	}

	@Override
	public BigDecimal queryCoinRate(String coinType) {
		return null;
	}

	@Override
	public SysParams selectByCode(String code) {
		return sysParamsMapper.selectByCode(code);
	}
}
