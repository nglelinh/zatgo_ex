package com.zatgo.zup.wallet.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.zatgo.zup.wallet.entity.FixedTimeDepositPro;
import com.zatgo.zup.wallet.entity.FixedTimeDepositProExample;
import com.zatgo.zup.wallet.entity.FixedTimeDepositProWithBLOBs;

public interface FixedTimeDepositProMapper {
    int countByExample(FixedTimeDepositProExample example);

    int deleteByExample(FixedTimeDepositProExample example);

    int deleteByPrimaryKey(String proId);

    int insert(FixedTimeDepositProWithBLOBs record);

    int insertSelective(FixedTimeDepositProWithBLOBs record);

    List<FixedTimeDepositProWithBLOBs> selectByExampleWithBLOBs(FixedTimeDepositProExample example);

    List<FixedTimeDepositPro> selectByExample(FixedTimeDepositProExample example);

    FixedTimeDepositProWithBLOBs selectByPrimaryKey(String proId);

    int updateByExampleSelective(@Param("record") FixedTimeDepositProWithBLOBs record, @Param("example") FixedTimeDepositProExample example);

    int updateByExampleWithBLOBs(@Param("record") FixedTimeDepositProWithBLOBs record, @Param("example") FixedTimeDepositProExample example);

    int updateByExample(@Param("record") FixedTimeDepositPro record, @Param("example") FixedTimeDepositProExample example);

    int updateByPrimaryKeySelective(FixedTimeDepositProWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(FixedTimeDepositProWithBLOBs record);

    int updateByPrimaryKey(FixedTimeDepositPro record);
    
    Page<FixedTimeDepositProWithBLOBs> findPageByStatus(@Param("cloudUserId") String cloudUserId,@Param("status")  Byte status);
}