package com.zatgo.zup.wallet.entity;

import java.math.BigDecimal;
import java.util.Date;

public class FixedTimeDepositPro {
    private String proId;

    private String proName;

    private Integer subscPeriodDay;

    private BigDecimal subscTotalNum;

    private BigDecimal balanceSubscTotalNum;

    private BigDecimal minSubscNum;

    private BigDecimal maxSubscNum;

    private Date buyEndDate;

    private Date valueDate;

    private Date dueDate;

    private Date redemptionDate;

    private String timeZone;

    private String coinType;

    private String coinNetworkType;

    private Byte yieldType;

    private BigDecimal yieldRate;

    private Byte riskLevel;

    private Byte status;

    private String teamName;

    private String cloudUserId;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId == null ? null : proId.trim();
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName == null ? null : proName.trim();
    }

    public Integer getSubscPeriodDay() {
        return subscPeriodDay;
    }

    public void setSubscPeriodDay(Integer subscPeriodDay) {
        this.subscPeriodDay = subscPeriodDay;
    }

    public BigDecimal getSubscTotalNum() {
        return subscTotalNum;
    }

    public void setSubscTotalNum(BigDecimal subscTotalNum) {
        this.subscTotalNum = subscTotalNum;
    }

    public BigDecimal getBalanceSubscTotalNum() {
        return balanceSubscTotalNum;
    }

    public void setBalanceSubscTotalNum(BigDecimal balanceSubscTotalNum) {
        this.balanceSubscTotalNum = balanceSubscTotalNum;
    }

    public BigDecimal getMinSubscNum() {
        return minSubscNum;
    }

    public void setMinSubscNum(BigDecimal minSubscNum) {
        this.minSubscNum = minSubscNum;
    }

    public BigDecimal getMaxSubscNum() {
        return maxSubscNum;
    }

    public void setMaxSubscNum(BigDecimal maxSubscNum) {
        this.maxSubscNum = maxSubscNum;
    }

    public Date getBuyEndDate() {
        return buyEndDate;
    }

    public void setBuyEndDate(Date buyEndDate) {
        this.buyEndDate = buyEndDate;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getRedemptionDate() {
        return redemptionDate;
    }

    public void setRedemptionDate(Date redemptionDate) {
        this.redemptionDate = redemptionDate;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone == null ? null : timeZone.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType == null ? null : coinNetworkType.trim();
    }

    public Byte getYieldType() {
        return yieldType;
    }

    public void setYieldType(Byte yieldType) {
        this.yieldType = yieldType;
    }

    public BigDecimal getYieldRate() {
        return yieldRate;
    }

    public void setYieldRate(BigDecimal yieldRate) {
        this.yieldRate = yieldRate;
    }

    public Byte getRiskLevel() {
        return riskLevel;
    }

    public void setRiskLevel(Byte riskLevel) {
        this.riskLevel = riskLevel;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName == null ? null : teamName.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }
}