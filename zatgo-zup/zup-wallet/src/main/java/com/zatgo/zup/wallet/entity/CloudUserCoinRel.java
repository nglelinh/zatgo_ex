package com.zatgo.zup.wallet.entity;

public class CloudUserCoinRel {
    private String id;

    private String cloudUserId;

    private String recordId;

    private Boolean isCloseDeposit;

    private Boolean isCloseWithdraw;

    private Byte status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId == null ? null : recordId.trim();
    }

    public Boolean getIsCloseDeposit() {
        return isCloseDeposit;
    }

    public void setIsCloseDeposit(Boolean isCloseDeposit) {
        this.isCloseDeposit = isCloseDeposit;
    }

    public Boolean getIsCloseWithdraw() {
        return isCloseWithdraw;
    }

    public void setIsCloseWithdraw(Boolean isCloseWithdraw) {
        this.isCloseWithdraw = isCloseWithdraw;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}