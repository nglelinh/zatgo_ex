package com.zatgo.zup.wallet.model;

import java.math.BigDecimal;
import java.util.Date;

public class LocalTransaction {

	private String id;

	private String type;

	private String status;

	private Amount amount;

	private NativeAmount nativeAmount;

	private String desc;

	private Date createTime;

	private Date updateTime;

	private Network network;

	private String to;

	private Boolean instantExchange;

	private TxDetails details;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Amount getAmount() {
		return amount;
	}

	public void setAmount(Amount amount) {
		this.amount = amount;
	}

	public NativeAmount getNativeAmount() {
		return nativeAmount;
	}

	public void setNativeAmount(NativeAmount nativeAmount) {
		this.nativeAmount = nativeAmount;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Boolean getInstantExchange() {
		return instantExchange;
	}

	public void setInstantExchange(Boolean instantExchange) {
		this.instantExchange = instantExchange;
	}

	public TxDetails getDetails() {
		return details;
	}

	public void setDetails(TxDetails details) {
		this.details = details;
	}
}
