package com.zatgo.zup.wallet.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CoinPresentRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CoinPresentRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPresentIdIsNull() {
            addCriterion("present_id is null");
            return (Criteria) this;
        }

        public Criteria andPresentIdIsNotNull() {
            addCriterion("present_id is not null");
            return (Criteria) this;
        }

        public Criteria andPresentIdEqualTo(String value) {
            addCriterion("present_id =", value, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdNotEqualTo(String value) {
            addCriterion("present_id <>", value, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdGreaterThan(String value) {
            addCriterion("present_id >", value, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdGreaterThanOrEqualTo(String value) {
            addCriterion("present_id >=", value, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdLessThan(String value) {
            addCriterion("present_id <", value, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdLessThanOrEqualTo(String value) {
            addCriterion("present_id <=", value, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdLike(String value) {
            addCriterion("present_id like", value, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdNotLike(String value) {
            addCriterion("present_id not like", value, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdIn(List<String> values) {
            addCriterion("present_id in", values, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdNotIn(List<String> values) {
            addCriterion("present_id not in", values, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdBetween(String value1, String value2) {
            addCriterion("present_id between", value1, value2, "presentId");
            return (Criteria) this;
        }

        public Criteria andPresentIdNotBetween(String value1, String value2) {
            addCriterion("present_id not between", value1, value2, "presentId");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameIsNull() {
            addCriterion("receive_user_name is null");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameIsNotNull() {
            addCriterion("receive_user_name is not null");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameEqualTo(String value) {
            addCriterion("receive_user_name =", value, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameNotEqualTo(String value) {
            addCriterion("receive_user_name <>", value, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameGreaterThan(String value) {
            addCriterion("receive_user_name >", value, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("receive_user_name >=", value, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameLessThan(String value) {
            addCriterion("receive_user_name <", value, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameLessThanOrEqualTo(String value) {
            addCriterion("receive_user_name <=", value, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameLike(String value) {
            addCriterion("receive_user_name like", value, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameNotLike(String value) {
            addCriterion("receive_user_name not like", value, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameIn(List<String> values) {
            addCriterion("receive_user_name in", values, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameNotIn(List<String> values) {
            addCriterion("receive_user_name not in", values, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameBetween(String value1, String value2) {
            addCriterion("receive_user_name between", value1, value2, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveUserNameNotBetween(String value1, String value2) {
            addCriterion("receive_user_name not between", value1, value2, "receiveUserName");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeIsNull() {
            addCriterion("receive_verify_code is null");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeIsNotNull() {
            addCriterion("receive_verify_code is not null");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeEqualTo(String value) {
            addCriterion("receive_verify_code =", value, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeNotEqualTo(String value) {
            addCriterion("receive_verify_code <>", value, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeGreaterThan(String value) {
            addCriterion("receive_verify_code >", value, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeGreaterThanOrEqualTo(String value) {
            addCriterion("receive_verify_code >=", value, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeLessThan(String value) {
            addCriterion("receive_verify_code <", value, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeLessThanOrEqualTo(String value) {
            addCriterion("receive_verify_code <=", value, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeLike(String value) {
            addCriterion("receive_verify_code like", value, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeNotLike(String value) {
            addCriterion("receive_verify_code not like", value, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeIn(List<String> values) {
            addCriterion("receive_verify_code in", values, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeNotIn(List<String> values) {
            addCriterion("receive_verify_code not in", values, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeBetween(String value1, String value2) {
            addCriterion("receive_verify_code between", value1, value2, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andReceiveVerifyCodeNotBetween(String value1, String value2) {
            addCriterion("receive_verify_code not between", value1, value2, "receiveVerifyCode");
            return (Criteria) this;
        }

        public Criteria andPresentStatusIsNull() {
            addCriterion("present_status is null");
            return (Criteria) this;
        }

        public Criteria andPresentStatusIsNotNull() {
            addCriterion("present_status is not null");
            return (Criteria) this;
        }

        public Criteria andPresentStatusEqualTo(Byte value) {
            addCriterion("present_status =", value, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusNotEqualTo(Byte value) {
            addCriterion("present_status <>", value, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusGreaterThan(Byte value) {
            addCriterion("present_status >", value, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("present_status >=", value, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusLessThan(Byte value) {
            addCriterion("present_status <", value, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusLessThanOrEqualTo(Byte value) {
            addCriterion("present_status <=", value, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusIn(List<Byte> values) {
            addCriterion("present_status in", values, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusNotIn(List<Byte> values) {
            addCriterion("present_status not in", values, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusBetween(Byte value1, Byte value2) {
            addCriterion("present_status between", value1, value2, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andPresentStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("present_status not between", value1, value2, "presentStatus");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNull() {
            addCriterion("coin_network_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNotNull() {
            addCriterion("coin_network_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeEqualTo(String value) {
            addCriterion("coin_network_type =", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotEqualTo(String value) {
            addCriterion("coin_network_type <>", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThan(String value) {
            addCriterion("coin_network_type >", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_network_type >=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThan(String value) {
            addCriterion("coin_network_type <", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_network_type <=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLike(String value) {
            addCriterion("coin_network_type like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotLike(String value) {
            addCriterion("coin_network_type not like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIn(List<String> values) {
            addCriterion("coin_network_type in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotIn(List<String> values) {
            addCriterion("coin_network_type not in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeBetween(String value1, String value2) {
            addCriterion("coin_network_type between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotBetween(String value1, String value2) {
            addCriterion("coin_network_type not between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNull() {
            addCriterion("coin_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNotNull() {
            addCriterion("coin_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeEqualTo(String value) {
            addCriterion("coin_type =", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotEqualTo(String value) {
            addCriterion("coin_type <>", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThan(String value) {
            addCriterion("coin_type >", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_type >=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThan(String value) {
            addCriterion("coin_type <", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_type <=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLike(String value) {
            addCriterion("coin_type like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotLike(String value) {
            addCriterion("coin_type not like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIn(List<String> values) {
            addCriterion("coin_type in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotIn(List<String> values) {
            addCriterion("coin_type not in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeBetween(String value1, String value2) {
            addCriterion("coin_type between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotBetween(String value1, String value2) {
            addCriterion("coin_type not between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinNumberIsNull() {
            addCriterion("coin_number is null");
            return (Criteria) this;
        }

        public Criteria andCoinNumberIsNotNull() {
            addCriterion("coin_number is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNumberEqualTo(Float value) {
            addCriterion("coin_number =", value, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberNotEqualTo(Float value) {
            addCriterion("coin_number <>", value, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberGreaterThan(Float value) {
            addCriterion("coin_number >", value, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberGreaterThanOrEqualTo(Float value) {
            addCriterion("coin_number >=", value, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberLessThan(Float value) {
            addCriterion("coin_number <", value, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberLessThanOrEqualTo(Float value) {
            addCriterion("coin_number <=", value, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberIn(List<Float> values) {
            addCriterion("coin_number in", values, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberNotIn(List<Float> values) {
            addCriterion("coin_number not in", values, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberBetween(Float value1, Float value2) {
            addCriterion("coin_number between", value1, value2, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andCoinNumberNotBetween(Float value1, Float value2) {
            addCriterion("coin_number not between", value1, value2, "coinNumber");
            return (Criteria) this;
        }

        public Criteria andPresentReasonIsNull() {
            addCriterion("present_reason is null");
            return (Criteria) this;
        }

        public Criteria andPresentReasonIsNotNull() {
            addCriterion("present_reason is not null");
            return (Criteria) this;
        }

        public Criteria andPresentReasonEqualTo(String value) {
            addCriterion("present_reason =", value, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonNotEqualTo(String value) {
            addCriterion("present_reason <>", value, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonGreaterThan(String value) {
            addCriterion("present_reason >", value, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonGreaterThanOrEqualTo(String value) {
            addCriterion("present_reason >=", value, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonLessThan(String value) {
            addCriterion("present_reason <", value, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonLessThanOrEqualTo(String value) {
            addCriterion("present_reason <=", value, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonLike(String value) {
            addCriterion("present_reason like", value, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonNotLike(String value) {
            addCriterion("present_reason not like", value, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonIn(List<String> values) {
            addCriterion("present_reason in", values, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonNotIn(List<String> values) {
            addCriterion("present_reason not in", values, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonBetween(String value1, String value2) {
            addCriterion("present_reason between", value1, value2, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentReasonNotBetween(String value1, String value2) {
            addCriterion("present_reason not between", value1, value2, "presentReason");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeIsNull() {
            addCriterion("present_datetime is null");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeIsNotNull() {
            addCriterion("present_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeEqualTo(Date value) {
            addCriterion("present_datetime =", value, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeNotEqualTo(Date value) {
            addCriterion("present_datetime <>", value, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeGreaterThan(Date value) {
            addCriterion("present_datetime >", value, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("present_datetime >=", value, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeLessThan(Date value) {
            addCriterion("present_datetime <", value, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("present_datetime <=", value, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeIn(List<Date> values) {
            addCriterion("present_datetime in", values, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeNotIn(List<Date> values) {
            addCriterion("present_datetime not in", values, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeBetween(Date value1, Date value2) {
            addCriterion("present_datetime between", value1, value2, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andPresentDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("present_datetime not between", value1, value2, "presentDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeIsNull() {
            addCriterion("receive_datetime is null");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeIsNotNull() {
            addCriterion("receive_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeEqualTo(Date value) {
            addCriterion("receive_datetime =", value, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeNotEqualTo(Date value) {
            addCriterion("receive_datetime <>", value, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeGreaterThan(Date value) {
            addCriterion("receive_datetime >", value, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("receive_datetime >=", value, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeLessThan(Date value) {
            addCriterion("receive_datetime <", value, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("receive_datetime <=", value, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeIn(List<Date> values) {
            addCriterion("receive_datetime in", values, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeNotIn(List<Date> values) {
            addCriterion("receive_datetime not in", values, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeBetween(Date value1, Date value2) {
            addCriterion("receive_datetime between", value1, value2, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andReceiveDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("receive_datetime not between", value1, value2, "receiveDatetime");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}