package com.zatgo.zup.wallet.service.Impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.BusinessEnum.LockBalanceOperateType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.ExchangeUpdateBalanceParams;
import com.zatgo.zup.common.model.LockWalletBalanceParams;
import com.zatgo.zup.common.model.PayParams;
import com.zatgo.zup.common.model.ReceiveMsg;
import com.zatgo.zup.common.model.RefundParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.mq.MQContants;
import com.zatgo.zup.common.mq.MQProducer;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.entity.PaymentRecord;
import com.zatgo.zup.wallet.entity.RefundRecord;
import com.zatgo.zup.wallet.entity.WalletAccount;
import com.zatgo.zup.wallet.mapper.PaymentRecordMapper;
import com.zatgo.zup.wallet.mapper.RefundRecordMapper;
import com.zatgo.zup.wallet.mapper.WalletAccountMapper;
import com.zatgo.zup.wallet.remoteservice.UserRemoteService;
import com.zatgo.zup.wallet.service.AccountService;
import com.zatgo.zup.wallet.service.CoinInfoService;
import com.zatgo.zup.wallet.service.PayService;
import com.zatgo.zup.wallet.service.WalletAccountService;
import com.zatgo.zup.wallet.utils.PasswordVerify;

@Service
public class PayServiceImpl implements PayService {
	private static final Logger logger = LoggerFactory.getLogger(PayServiceImpl.class);
	@Autowired
	private PaymentRecordMapper paymentRecordMapper;
	@Autowired
	private MQProducer mqProducer;
	@Autowired
	private WalletAccountMapper walletAccountMapper;
	@Autowired
	private UserRemoteService userRemoteService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private RedisLockUtils redisLockUtils;
	@Autowired
	private PasswordVerify passwordVerify;
	@Autowired
	private RefundRecordMapper refundRecordMapper;
	@Autowired
	private WalletAccountService walletAccountService;
	@Autowired
	private CoinInfoService coinInfoService;

	@Override
	@Transactional
	public String pay(PayParams params,String userId,BusinessEnum.PaymentRecordType payType) {
		PaymentRecord paymentRecord = paymentRecordMapper.selectRecordByOrderId(params.getCheckoutOrderId(), userId);
		if(paymentRecord != null) {
			throw new BusinessException(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING);
		}
		
		//用户状态判断
		ResponseData<UserData> userDataResp = userRemoteService.getUserById(userId);
		if(!userDataResp.isSuccessful()) {
			throw new BusinessException(userDataResp.getCode(),userDataResp.getMessage());
		}
		if(userDataResp.getData().getIsLockPay() != null &&
				userDataResp.getData().getIsLockPay() == true) {
			throw new BusinessException(BusinessExceptionCode.MERCHANT_EXCEPTION_USER);
		}
		
		//校验支付密码(支付和转账)
		if(BusinessEnum.PaymentRecordType.orderPay.getCode().equals(payType.getCode())
				|| BusinessEnum.PaymentRecordType.transfer.getCode().equals(payType.getCode())){
			passwordVerify.verify(params.getPayPassword(),userId);
		}


		String paymentId = null;
		paymentRecord = new PaymentRecord();
		WalletAccount payAccount = null;
		try {
			payAccount = walletAccountMapper.selectByUserIdAndTypeForUpdate(userId, params.getCoinType());
		} catch (Exception e) {
			logger.error("支付超时",e);
			throw new BusinessException(BusinessExceptionCode.PAYMENT_TIMEOUT);
		}
		
		BigDecimal balance = BigDecimal.ZERO;
		if(payAccount != null) {
			balance = payAccount.getBalance();
		}
		
		if (!BusinessEnum.PaymentRecordType.fixedDepositSubscYield.getCode().equals(payType.getCode()) && balance.compareTo(params.getAmount()) < 0) {
			throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
		}
		WalletAccount receiveAccount = null;
		try {
			receiveAccount = walletAccountMapper.selectByUserIdAndTypeForUpdate(params.getReceiptUserId(), params.getCoinType());
		} catch (Exception e) {
			logger.error("支付超时",e);
			throw new BusinessException(BusinessExceptionCode.PAYMENT_TIMEOUT);
		}
		//新增账户
		if (receiveAccount == null) {
			CreateAccountParams cap = new CreateAccountParams();
			cap.setCurrencyType(params.getCoinType());
			cap.setName(params.getCoinType());
			cap.setNetworkType(params.getNetworkType());
			cap.setUserId(params.getReceiptUserId());
			receiveAccount = accountService.createAccount(cap);
			try {
				receiveAccount = walletAccountMapper.selectByUserIdAndTypeForUpdate(params.getReceiptUserId(), params.getCoinType());
			} catch (Exception e) {
				logger.error("支付超时",e);
				throw new BusinessException(BusinessExceptionCode.PAYMENT_TIMEOUT);
			}
		}
		walletAccountMapper.updateBalance(payAccount.getAccountId(), params.getAmount().negate());
		walletAccountMapper.updateBalance(receiveAccount.getAccountId(), params.getAmount());
		paymentId = UUIDUtils.getUuid();
		paymentRecord.setPaymentId(paymentId);
		paymentRecord.setAmount(params.getAmount());
		paymentRecord.setOrderId(params.getCheckoutOrderId());
		paymentRecord.setPaymentDate(new Date());
		paymentRecord.setPaymentAccountId(payAccount.getAccountId());
		paymentRecord.setReceiptAccountId(receiveAccount.getAccountId());
		paymentRecord.setPayUserId(userId);
		paymentRecord.setPayUserName(params.getPayUserName());
		paymentRecord.setReceiptUserName(params.getReceiptUserName());
		paymentRecord.setReceiptUserId(params.getReceiptUserId());
		paymentRecord.setCoinType(params.getCoinType());
		paymentRecord.setPayHash(MD5.GetMD5Code(JSON.toJSONString(paymentRecord)));
		paymentRecord.setPaymentType(payType.getCode());
		paymentRecordMapper.insertSelective(paymentRecord);
		return paymentId;
	}



	@Override
	public String collectionCode(String userId) {
		return null;
	}



	@Override
	@Transactional(rollbackFor = Exception.class)
	public String transfer(PayParams params,String userId) {
		if(!(params.getAmount().compareTo(BigDecimal.ZERO) ==1)){
			throw new BusinessException(BusinessExceptionCode.ERROR_AMOUNT_OF_MONEY);
		}
		String recordId = pay(params, userId,BusinessEnum.PaymentRecordType.transfer);
		ReceiveMsg receiveMsg = new ReceiveMsg();
		receiveMsg.setAmount(params.getAmount());
		receiveMsg.setNetWorkType(params.getNetworkType());
		receiveMsg.setCoinType(params.getCoinType());
		receiveMsg.setUserId(params.getReceiptUserId());
		receiveMsg.setType(BusinessEnum.ReceiveType.transfer.getCode());

		try {
			mqProducer.send(MQContants.TOPIC_COIN_RECEIVE,null, JSON.toJSONString(receiveMsg),userId+ "-" +recordId);
		} catch (Exception e) {
			logger.error("转账成功，消息发送失败，支付记录ID："+recordId, e);
		}
		return recordId;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public String refund(RefundParams params) {
		if(params.getReceiveUserId().equals(params.getRefundUserId())) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		String refundId = null;
		WalletAccount receiveAccount = walletAccountMapper.selectByUserIdAndTypeForUpdate(params.getReceiveUserId(), params.getCoinType());
		WalletAccount refundAccount = walletAccountMapper.selectByUserIdAndTypeForUpdate(params.getRefundUserId(), params.getCoinType());
		if(refundAccount.getBalance().compareTo(params.getAmount()) == -1){
			throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
		}
		BigDecimal refundMoney = refundRecordMapper.sumRefundMoney(params.getOderId());
		BigDecimal totalRefund = params.getAmount().add(refundMoney);
		if(params.getOrderMoney().compareTo(totalRefund) == -1){
		    throw new BusinessException(BusinessExceptionCode.REFUND_AMOUNT_MORETHAN_ORDER_AMOUNT);
		}
		try {
			walletAccountMapper.updateBalance(refundAccount.getAccountId(), params.getAmount().negate());
			walletAccountMapper.updateBalance(receiveAccount.getAccountId(), params.getAmount());
			PaymentRecord paymentRecord = new PaymentRecord();
			paymentRecord.setPaymentId(UUIDUtils.getUuid());
			paymentRecord.setAmount(params.getAmount());
			paymentRecord.setOrderId(params.getOderId());
			paymentRecord.setPaymentDate(new Date());
			paymentRecord.setPaymentAccountId(refundAccount.getAccountId());
			paymentRecord.setReceiptAccountId(receiveAccount.getAccountId());
			paymentRecord.setPayUserId(refundAccount.getUserId());
			paymentRecord.setPayUserName(params.getRefundUserName());
			paymentRecord.setReceiptUserName(params.getReceiveUserName());
			paymentRecord.setReceiptUserId(receiveAccount.getUserId());
			paymentRecord.setCoinType(params.getCoinType());
			paymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.orderRefund.getCode());
			paymentRecordMapper.insertSelective(paymentRecord);
			RefundRecord record = new RefundRecord();
			refundId = UUIDUtils.getUuid();
			record.setRefundId(refundId);
			record.setAmount(params.getAmount());
			record.setOrderId(params.getOderId());
			record.setReceiptAccountId(receiveAccount.getAccountId());
			record.setRefundAccountId(refundAccount.getAccountId());
			record.setRefundDate(new Date());
			refundRecordMapper.insertSelective(record);
		} catch (Exception e) {
			throw new BusinessException(BusinessExceptionCode.PAY_FAILED);
		}
		return refundId;
	}

	@Transactional
	@Override
	public void lockBalanceUpdate(LockWalletBalanceParams param) {
		String userId = param.getUserId();
		String orderId = param.getOrderId();
		
		LockBalanceOperateType lockBalanceOperateType = param.getLockBalanceType();
		PaymentRecord record = paymentRecordMapper.selectRecordByUserIdAndBusinessNumberAndPaymentType(userId, orderId,
				orderId, lockBalanceOperateType.getCode());
		
		if(record != null) {
			logger.error("重复操作账户余额：" + JSON.toJSONString(record));
			throw new BusinessException(BusinessExceptionCode.REPEAT_OPERATION_BALANCE);
		}
		
		String coinType = param.getCoinType();
		
		WalletAccount account = walletAccountMapper.selectByUserIdAndTypeAddLock(userId, coinType);
		if(account == null) {
			throw new BusinessException(BusinessExceptionCode.WALLETACCOUNT_NOT_EXIST);
		}
		
		if(account.getBalance() == null) {
			account.setBalance(BigDecimal.ZERO);
		}
		
		if(account.getLockBalance() == null) {
			account.setLockBalance(BigDecimal.ZERO);
		}
		
		PaymentRecord paymentRecord = new PaymentRecord();
		BigDecimal amount = param.getLockAmount();
		
		if(lockBalanceOperateType.equals(LockBalanceOperateType.LOCK)) {
			lockAccountBalance(amount, account);
			
			paymentRecord.setPaymentAccountId(account.getAccountId());
			paymentRecord.setPayUserId(userId);
			paymentRecord.setPayUserName(param.getUserName());
			
			paymentRecord.setReceiptUserName(param.getUserName());
			paymentRecord.setReceiptUserId(param.getUserName());
			paymentRecord.setReceiptAccountId(account.getAccountId());
			paymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.exchangeLock.getCode());
		} else if(lockBalanceOperateType.equals(LockBalanceOperateType.UNLOCK)) {
			unlockAccountBalance(amount, account);
			
			paymentRecord.setPayUserName(param.getUserName());
			paymentRecord.setPaymentAccountId(account.getAccountId());
			paymentRecord.setPayUserId(userId);
			paymentRecord.setPayUserName(param.getUserName());
			
			paymentRecord.setReceiptAccountId(account.getAccountId());
			paymentRecord.setReceiptUserName(param.getUserName());
			paymentRecord.setReceiptUserId(userId);
			paymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.exchangeUnLock.getCode());
		}else {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "LockBalanceType=" + lockBalanceOperateType.getCode() + "不支持");
		}
		
		paymentRecord.setPaymentId(UUIDUtils.getUuid());
		paymentRecord.setAmount(amount);
		paymentRecord.setOrderId(orderId);
		paymentRecord.setPaymentDate(new Date());
		
		paymentRecord.setCoinType(coinType);
		paymentRecord.setBusinessNumber(orderId);
		paymentRecordMapper.insertSelective(paymentRecord);
	}
	
	@Transactional
	@Override
	public void exchangeMatchingBalance(List<ExchangeUpdateBalanceParams> params) {

		for (ExchangeUpdateBalanceParams param : params) {

			String businessNumber = param.getBusinessNumber();

			WalletAccount buyReceiptAccount = null;
			WalletAccount buyPayAccount = null;
			WalletAccount sellReceiptAccount = null;
			WalletAccount sellPayAccount = null;
			WalletAccount buyFeeAccount = null;
			WalletAccount sellFeeAccount = null;
			UserData buyFeeReceiptUser = null;
			UserData sellFeeReceiptUser = null;
			if (param.getBuyUserId() != null) {
				PaymentRecord record = paymentRecordMapper.selectRecordByUserIdAndBusinessNumberAndPaymentType(
						param.getBuyUserId(), param.getBuyOrderId(), businessNumber,
						BusinessEnum.PaymentRecordType.exchange.getCode());

				if (record != null) {
					logger.error("重复操作账户余额：" + JSON.toJSONString(record));
					throw new BusinessException(BusinessExceptionCode.REPEAT_OPERATION_BALANCE);
				}

				// 买方收款账户操作
				buyReceiptAccount = walletAccountMapper.selectByUserIdAndTypeAddLock(param.getBuyUserId(),
						param.getBaseCoinType());
				if (buyReceiptAccount == null) {
					buyReceiptAccount = createWalletAccount(param.getBaseCoinType(), param.getBuyUserId());
				}
				if (buyReceiptAccount.getBalance() == null) {
					buyReceiptAccount.setBalance(BigDecimal.ZERO);
				}
				if (buyReceiptAccount.getLockBalance() == null) {
					buyReceiptAccount.setLockBalance(BigDecimal.ZERO);
				}
				addAccountBalance(param.getSellLockAmount(), buyReceiptAccount);

				// 买方付款账户操作
				buyPayAccount = walletAccountMapper.selectByUserIdAndTypeAddLock(param.getBuyUserId(),
						param.getQuoteCoinType());
				if (buyPayAccount == null) {
					throw new BusinessException(BusinessExceptionCode.WALLETACCOUNT_NOT_EXIST);
				}
				if (buyReceiptAccount.getBalance() == null) {
					buyReceiptAccount.setBalance(BigDecimal.ZERO);
				}
				if (buyReceiptAccount.getLockBalance() == null) {
					buyReceiptAccount.setLockBalance(BigDecimal.ZERO);
				}
				deductLockAccountBalance(param.getBuyLockAmount(), buyPayAccount);

				// TODO 手续费处理
				if (param.getBuyfee().compareTo(BigDecimal.ZERO) == 1) {

					PaymentRecord feeRecord = paymentRecordMapper.selectRecordByUserIdAndBusinessNumberAndPaymentType(
							param.getBuyUserId(), param.getBuyOrderId(), businessNumber,
							BusinessEnum.PaymentRecordType.exchangeFee.getCode());

					if (feeRecord != null) {
						logger.error("重复操作账户余额：" + JSON.toJSONString(record));
						throw new BusinessException(BusinessExceptionCode.REPEAT_OPERATION_BALANCE);
					}

					ResponseData<UserData> adminUserResp = userRemoteService.getManageUserId(
							param.getBuyUserId(),BusinessEnum.CloudManageUserType.EXCHANGE_FEE.getType().toString());
					if (!adminUserResp.isSuccessful()) {
						logger.error("查询云管理账户失败：" + adminUserResp.getMessage());
						throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
					}

					buyFeeReceiptUser = adminUserResp.getData();

					buyFeeAccount = walletAccountMapper.selectByUserIdAndTypeAddLock(buyFeeReceiptUser.getUserId(),
							param.getBaseCoinType());
					if (buyFeeAccount == null) {
						buyFeeAccount = createWalletAccount(param.getBaseCoinType(), buyFeeReceiptUser.getUserId());
					}

					addAccountBalance(param.getBuyfee(), buyFeeAccount);
				}

			}
			if (param.getSellUserId() != null) {
				PaymentRecord record = paymentRecordMapper.selectRecordByUserIdAndBusinessNumberAndPaymentType(
						param.getSellUserId(), param.getSellOrderId(), businessNumber,
						BusinessEnum.PaymentRecordType.exchange.getCode());

				if (record != null) {
					logger.error("重复操作账户余额：" + JSON.toJSONString(record));
					throw new BusinessException(BusinessExceptionCode.REPEAT_OPERATION_BALANCE);
				}

				// 卖方收款账户操作
				sellReceiptAccount = walletAccountMapper.selectByUserIdAndTypeAddLock(param.getSellUserId(),
						param.getQuoteCoinType());
				if (sellReceiptAccount == null) {
					sellReceiptAccount = createWalletAccount(param.getQuoteCoinType(), param.getSellUserId());
				}
				if (sellReceiptAccount.getBalance() == null) {
					sellReceiptAccount.setBalance(BigDecimal.ZERO);
				}
				if (sellReceiptAccount.getLockBalance() == null) {
					sellReceiptAccount.setLockBalance(BigDecimal.ZERO);
				}
				addAccountBalance(param.getBuyLockAmount(), sellReceiptAccount);

				// 卖方付款账户操作
				sellPayAccount = walletAccountMapper.selectByUserIdAndTypeAddLock(param.getSellUserId(),
						param.getBaseCoinType());
				if (sellPayAccount == null) {
					throw new BusinessException(BusinessExceptionCode.WALLETACCOUNT_NOT_EXIST);
				}
				if (sellPayAccount.getBalance() == null) {
					sellPayAccount.setBalance(BigDecimal.ZERO);
				}
				if (sellPayAccount.getLockBalance() == null) {
					sellPayAccount.setLockBalance(BigDecimal.ZERO);
				}
				deductLockAccountBalance(param.getSellLockAmount(), sellPayAccount);

				// TODO 手续费处理
				if (param.getSellfee().compareTo(BigDecimal.ZERO) == 1) {

					PaymentRecord feeRecord = paymentRecordMapper.selectRecordByUserIdAndBusinessNumberAndPaymentType(
							param.getSellUserId(), param.getSellOrderId(), businessNumber,
							BusinessEnum.PaymentRecordType.exchangeFee.getCode());

					if (feeRecord != null) {
						logger.error("重复操作账户余额：" + JSON.toJSONString(record));
						throw new BusinessException(BusinessExceptionCode.REPEAT_OPERATION_BALANCE);
					}

					ResponseData<UserData> adminUserResp = userRemoteService.getManageUserId(
							param.getSellUserId(),BusinessEnum.CloudManageUserType.EXCHANGE_FEE.getType().toString());
					if (!adminUserResp.isSuccessful()) {
						logger.error("查询云管理账户失败：" + adminUserResp.getMessage());
						throw new BusinessException(BusinessExceptionCode.GET_CLOUD_USER_MANAGE_ID_ERROR);
					}

					sellFeeReceiptUser = adminUserResp.getData();

					sellFeeAccount = walletAccountMapper.selectByUserIdAndTypeAddLock(sellFeeReceiptUser.getUserId(),
							param.getBaseCoinType());
					if (sellFeeAccount == null) {
						sellFeeAccount = createWalletAccount(param.getBaseCoinType(), sellFeeReceiptUser.getUserId());
					}

					addAccountBalance(param.getSellfee(), sellFeeAccount);
				}
			}

			// 基准币进出记录
			PaymentRecord baseCoinPaymentRecord = new PaymentRecord();
			if (buyReceiptAccount != null) {
				baseCoinPaymentRecord.setReceiptAccountId(buyReceiptAccount.getAccountId());
				baseCoinPaymentRecord.setReceiptUserId(param.getBuyUserId());
				baseCoinPaymentRecord.setReceiptUserName(param.getBuyUserName());
			}
			if (sellPayAccount != null) {
				baseCoinPaymentRecord.setPaymentAccountId(sellPayAccount.getAccountId());
				baseCoinPaymentRecord.setPayUserId(param.getSellUserId());
				baseCoinPaymentRecord.setPayUserName(param.getSellUserName());

			}
			baseCoinPaymentRecord.setPaymentId(UUIDUtils.getUuid());
			baseCoinPaymentRecord.setAmount(param.getSellLockAmount());
			baseCoinPaymentRecord.setOrderId(param.getBuyOrderId());
			baseCoinPaymentRecord.setPaymentDate(new Date());
			baseCoinPaymentRecord.setCoinType(param.getBaseCoinType());
			baseCoinPaymentRecord.setBusinessNumber(businessNumber);
			baseCoinPaymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.exchange.getCode());
			paymentRecordMapper.insertSelective(baseCoinPaymentRecord);

			// 计价币进出记录
			PaymentRecord quoteCoinPaymentRecord = new PaymentRecord();
			if (sellReceiptAccount != null) {
				quoteCoinPaymentRecord.setReceiptAccountId(sellReceiptAccount.getAccountId());
				quoteCoinPaymentRecord.setReceiptUserId(param.getSellUserId());
				quoteCoinPaymentRecord.setReceiptUserName(param.getSellUserName());
			}
			if (buyPayAccount != null) {
				quoteCoinPaymentRecord.setPaymentAccountId(buyPayAccount.getAccountId());
				quoteCoinPaymentRecord.setPayUserId(param.getBuyUserId());
				quoteCoinPaymentRecord.setPayUserName(param.getBuyUserName());
			}
			quoteCoinPaymentRecord.setPaymentId(UUIDUtils.getUuid());
			quoteCoinPaymentRecord.setAmount(param.getBuyLockAmount());
			quoteCoinPaymentRecord.setOrderId(param.getSellOrderId());
			quoteCoinPaymentRecord.setPaymentDate(new Date());
			quoteCoinPaymentRecord.setCoinType(param.getQuoteCoinType());
			quoteCoinPaymentRecord.setBusinessNumber(businessNumber);
			quoteCoinPaymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.exchange.getCode());
			paymentRecordMapper.insertSelective(quoteCoinPaymentRecord);

			// 买单手续费进出记录
			if (buyFeeAccount != null) {
				PaymentRecord buyfeePaymentRecord = new PaymentRecord();

				buyfeePaymentRecord.setReceiptAccountId(buyFeeAccount.getAccountId());
				buyfeePaymentRecord.setReceiptUserId(buyFeeReceiptUser.getUserId());
				buyfeePaymentRecord.setReceiptUserName(buyFeeReceiptUser.getUserName());

				buyfeePaymentRecord.setPaymentAccountId(buyReceiptAccount.getAccountId());
				buyfeePaymentRecord.setPayUserId(param.getBuyUserId());
				buyfeePaymentRecord.setPayUserName(param.getBuyUserName());

				buyfeePaymentRecord.setPaymentId(UUIDUtils.getUuid());
				buyfeePaymentRecord.setAmount(param.getBuyfee());
				buyfeePaymentRecord.setOrderId(param.getBuyOrderId());
				buyfeePaymentRecord.setPaymentDate(new Date());
				buyfeePaymentRecord.setCoinType(param.getBaseCoinType());
				buyfeePaymentRecord.setBusinessNumber(businessNumber);
				buyfeePaymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.exchangeFee.getCode());
				paymentRecordMapper.insertSelective(buyfeePaymentRecord);
			}

			// 卖单手续费进出记录
			if (sellFeeAccount != null) {
				PaymentRecord sellfeePaymentRecord = new PaymentRecord();

				sellfeePaymentRecord.setReceiptAccountId(sellFeeAccount.getAccountId());
				sellfeePaymentRecord.setReceiptUserId(sellFeeReceiptUser.getUserId());
				sellfeePaymentRecord.setReceiptUserName(sellFeeReceiptUser.getUserName());

				sellfeePaymentRecord.setPaymentAccountId(sellReceiptAccount.getAccountId());
				sellfeePaymentRecord.setPayUserId(param.getSellUserId());
				sellfeePaymentRecord.setPayUserName(param.getSellUserName());

				sellfeePaymentRecord.setPaymentId(UUIDUtils.getUuid());
				sellfeePaymentRecord.setAmount(param.getSellfee());
				sellfeePaymentRecord.setOrderId(param.getSellOrderId());
				sellfeePaymentRecord.setPaymentDate(new Date());
				sellfeePaymentRecord.setCoinType(param.getQuoteCoinType());
				sellfeePaymentRecord.setBusinessNumber(businessNumber);
				sellfeePaymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.exchangeFee.getCode());
				paymentRecordMapper.insertSelective(sellfeePaymentRecord);
			}
		}

	}
	
	/**
	 * 冻结用户账户余额
	 * @param lockAmount
	 * @param account
	 * @return
	 */
	private void lockAccountBalance(BigDecimal lockAmount, WalletAccount account) {
		
		if(lockAmount.compareTo(account.getBalance()) == 1 ) {
			throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
		}
		
		WalletAccount record = new WalletAccount();
		record.setAccountId(account.getAccountId());
		record.setBalance(account.getBalance().subtract(lockAmount));
		record.setLockBalance(account.getLockBalance().add(lockAmount));
		record.setUpdateDate(new Date());
		int i = walletAccountMapper.updateByPrimaryKeySelective(record);
		if(i != 1) {
			logger.error("冻结账户余额结果数不等于1：", record);
			throw new BusinessException(BusinessExceptionCode.LOCK_BALANCE_FAIL);
		}
		
	}
	
	/**
	 * 解冻用户账户余额
	 * @param lockAmount
	 * @param account
	 * @return
	 */
	private void unlockAccountBalance(BigDecimal lockAmount, WalletAccount account) {
		
		if (account.getLockBalance().compareTo(BigDecimal.ZERO) != 1
				|| lockAmount.compareTo(account.getLockBalance()) == 1) {
			logger.error("冻结账户余额不足：" + JSON.toJSONString(account) + "，lockAmount：" + lockAmount);
			throw new BusinessException(BusinessExceptionCode.LOCK_BALANCE_NOT_ENOUGH);
		}
		
		WalletAccount record = new WalletAccount();
		record.setAccountId(account.getAccountId());
		record.setBalance(account.getBalance().add(lockAmount));
		record.setLockBalance(account.getLockBalance().subtract(lockAmount));
		record.setUpdateDate(new Date());
		int i = walletAccountMapper.updateByPrimaryKeySelective(record);
		if(i != 1) {
			throw new BusinessException(BusinessExceptionCode.UNLOCK_BALANCE_FAIL);
		}
		
	}
	
	/**
	 * 扣除用户冻结账户余额
	 * @param lockAmount
	 * @param account
	 * @return
	 */
	private void deductLockAccountBalance(BigDecimal lockAmount, WalletAccount account) {
		
		if (account.getLockBalance().compareTo(BigDecimal.ZERO) != 1
				|| lockAmount.compareTo(account.getLockBalance()) == 1) {
			logger.error("冻结账户余额不足：" + JSON.toJSONString(account) + "，lockAmount：" + lockAmount);
			throw new BusinessException(BusinessExceptionCode.LOCK_BALANCE_NOT_ENOUGH);
		}
		
		WalletAccount record = new WalletAccount();
		record.setAccountId(account.getAccountId());
		record.setLockBalance(account.getLockBalance().subtract(lockAmount));
		record.setUpdateDate(new Date());
		int i = walletAccountMapper.updateByPrimaryKeySelective(record);
		if(i != 1) {
			throw new BusinessException(BusinessExceptionCode.DEDUCT_LOCK_BALANCE_FAIL);
		}
		
	}
	
	/**
	 * 增加用户账户余额
	 * @param amount
	 * @param account
	 * @return
	 */
	private void addAccountBalance(BigDecimal amount, WalletAccount account) {
		
		WalletAccount record = new WalletAccount();
		record.setAccountId(account.getAccountId());
		record.setBalance(account.getBalance().add(amount));
		record.setUpdateDate(new Date());
		int i = walletAccountMapper.updateByPrimaryKeySelective(record);
		if(i != 1) {
			throw new BusinessException(BusinessExceptionCode.ADD_EXCHANGE_BALANCE_FAIL);
		}
		
	}
	
	/**
	 * 根据币种创建钱包账户
	 * 
	 * @param coin
	 * @param userId
	 * @return
	 */
	private WalletAccount createWalletAccount(String coin, String userId) {

		IssuedCoinInfo coinInfo = coinInfoService.selectSystemCoinInfoByCoinType(coin);
		if (coinInfo == null) {
			logger.error("issued coin info not exist: " + coin);
			throw new BusinessException(BusinessExceptionCode.NOT_EXIST_COIN_TYPE);
		}

		CreateAccountParams cap = new CreateAccountParams();
		cap.setCurrencyType(coinInfo.getCoinType());
		cap.setName(coinInfo.getCoinType());
		cap.setNetworkType(coinInfo.getCoinNetworkType());
		cap.setUserId(userId);
		WalletAccount account = accountService.createAccount(cap);
		try {
			account = walletAccountMapper.selectByUserIdAndTypeForUpdate(userId, coin);
		} catch (Exception e) {
			logger.error("支付超时", e);
			throw new BusinessException(BusinessExceptionCode.PAYMENT_TIMEOUT);
		}

		return account;
	}

}
