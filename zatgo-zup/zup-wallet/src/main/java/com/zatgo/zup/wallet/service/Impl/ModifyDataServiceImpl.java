package com.zatgo.zup.wallet.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.wallet.entity.CoinPresentRecord;
import com.zatgo.zup.wallet.entity.DepositRecord;
import com.zatgo.zup.wallet.entity.ExtractRecord;
import com.zatgo.zup.wallet.entity.WalletAccount;
import com.zatgo.zup.wallet.mapper.CoinPresentRecordMapper;
import com.zatgo.zup.wallet.mapper.DepositRecordMapper;
import com.zatgo.zup.wallet.mapper.ExtractRecordMapper;
import com.zatgo.zup.wallet.mapper.WalletAccountMapper;
import com.zatgo.zup.wallet.service.AccountService;
import com.zatgo.zup.wallet.service.RepairDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2018/9/17.
 */


@Service("modifyDataService")
public class ModifyDataServiceImpl implements RepairDataService {

    private static final Logger logger = LoggerFactory.getLogger(ModifyDataServiceImpl.class);

    @Autowired
    private DepositRecordMapper depositRecordMapper;
    @Autowired
    private ExtractRecordMapper extractRecordMapper;
    @Autowired
    private CoinPresentRecordMapper coinPresentRecordMapper;
    @Autowired
    private WalletAccountMapper walletAccountMapper;
    @Autowired
    private AccountService accountService;

    private static final String CHENGYUN_USER_ID="b5f56ab0-b69d-49d6-87fb-a93c66a95bac";


    @Transactional
    @Override
    public void repairCloudUserWalletBalance() {

        List<DepositRecord> DepositList = depositRecordMapper.selectSystemDepositRecord();
        List<ExtractRecord> ExtractList = extractRecordMapper.selectSystemExtractRecord();
        List<CoinPresentRecord> coinPresentRecords = coinPresentRecordMapper.selectSystemCoinPresentRecord();

        Map<String, BigDecimal> extractMap = new HashMap<>();
        for (ExtractRecord record : ExtractList){
            extractMap.put(record.getExtractId(), record.getExtractNumber());
        }

        Map<String, BigDecimal> coinPresentMap = new HashMap<>();
        for (CoinPresentRecord record : coinPresentRecords){
            coinPresentMap.put(record.getPresentId(), new BigDecimal(record.getCoinNumber()));
        }

        for (DepositRecord record : DepositList){
            logger.error("开始计算:" + JSONObject.toJSONString(record));
            BigDecimal balance = record.getDepositNumber();

            String id = record.getDepositId();
            if (extractMap.containsKey(id)){
                BigDecimal extract = extractMap.get(id);
                if(balance.compareTo(extract) == -1){
                    logger.error("账目异常:" + extract + "提币数量大于余额");
                }
                balance = balance.subtract(extract);
                logger.error("减去提币的:" + extract + ", 余额:" + balance);
            }

            if (coinPresentMap.containsKey(id)){
                BigDecimal coinPresent = coinPresentMap.get(id);
                if(balance.compareTo(coinPresent) == -1){
                    logger.error("账目异常:" + coinPresent + "提币数量大于余额");
                }
                balance = balance.subtract(coinPresent);
                logger.error("减去赠送的:" + coinPresent + ", 余额:" + balance);
            }
            CreateAccountParams params = new CreateAccountParams();
            params.setUserId(CHENGYUN_USER_ID);
            params.setNetworkType(record.getCoinNetworkType());
            params.setName(record.getCoinType());
            params.setCurrencyType(record.getCoinType());
            WalletAccount account = accountService.createAccount(params);
            walletAccountMapper.updateBalance(account.getAccountId(), balance);
            logger.error("==============================" + account.getCoinType() + "钱包余额修改完毕");
        }
    }
}
