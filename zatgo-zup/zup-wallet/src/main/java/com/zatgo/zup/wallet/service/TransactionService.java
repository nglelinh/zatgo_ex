package com.zatgo.zup.wallet.service;

import java.util.List;

import com.zatgo.zup.wallet.model.LocalTransaction;
import com.zatgo.zup.wallet.model.SendParams;
import com.zatgo.zup.wallet.model.TxRecord;

public interface TransactionService {

	List<TxRecord> listAccountTransactions(String accountId);

	TxRecord getTransactionByTransactionId(String accountId, String transactionId);

	LocalTransaction sendMoney(SendParams params, String accountId);

	LocalTransaction transferMoneyBetweenAccounts(SendParams params, String accountId);

	LocalTransaction requestMoney(SendParams params, String accountId);

	void completeRequestMoney(String accountId,String transactionId);

	void cancelRequestMoney(String accountId,String transactionId);

}
