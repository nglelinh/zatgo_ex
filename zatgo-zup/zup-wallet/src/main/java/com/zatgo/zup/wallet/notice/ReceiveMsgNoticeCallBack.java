package com.zatgo.zup.wallet.notice;

import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.model.ReceiveMsg;
import com.zatgo.zup.common.mq.MQConsumerCallBack;
import com.zatgo.zup.wallet.websocket.ReceiveWebSocket;

/**
 * 支付通知回调类
 * @Title
 * @author lincm
 * @date 2018年5月8日 上午11:48:10
 */
public class ReceiveMsgNoticeCallBack implements MQConsumerCallBack{

	private static final Logger logger = LoggerFactory.getLogger(ReceiveMsgNoticeCallBack.class);

	@Override
	public boolean callBack(String json) {
		try {
			ReceiveMsg receiveMsg = JSON.parseObject(json, ReceiveMsg.class);
			String key = receiveMsg.getUserId();
			ConcurrentHashMap<String, Session> cacheMap = ReceiveWebSocket.cacheMap;
			Session session = cacheMap.get(key);
			if(session != null){
				session.getBasicRemote().sendText(json);
			}

		}catch(Exception e) {
			logger.error(json,e);
		}

		return true;
	}

}
