package com.zatgo.zup.wallet.model;

import java.io.Serializable;

public class TxDetails implements Serializable {

	private String title;

	private String subTile;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTile() {
		return subTile;
	}

	public void setSubTile(String subTile) {
		this.subTile = subTile;
	}
}
