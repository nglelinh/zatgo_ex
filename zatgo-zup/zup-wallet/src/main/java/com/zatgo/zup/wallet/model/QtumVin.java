package com.zatgo.zup.wallet.model;

import java.io.Serializable;

public class QtumVin implements Serializable {

	private String txid;

	private int vout;
	
	private QtumScriptSig scriptSig;
	
	private String sequence;

	public String getTxid() {
		return txid;
	}

	public void setTxid(String txid) {
		this.txid = txid;
	}

	public int getVout() {
		return vout;
	}

	public void setVout(int vout) {
		this.vout = vout;
	}

	public QtumScriptSig getScriptSig() {
		return scriptSig;
	}

	public void setScriptSig(QtumScriptSig scriptSig) {
		this.scriptSig = scriptSig;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
}
