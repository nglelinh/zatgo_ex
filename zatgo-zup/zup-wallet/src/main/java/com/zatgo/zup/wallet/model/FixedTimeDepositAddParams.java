package com.zatgo.zup.wallet.model;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class FixedTimeDepositAddParams {
	
	@ApiModelProperty(value = "产品ID,修改数据时使用",required = false)
	private String proId;

	@ApiModelProperty(value = "产品名称",required = true)
	private String proName;

	@ApiModelProperty(value = "认购天数",required = true)
    private Integer subscPeriodDay;

	@ApiModelProperty(value = "认购总量",required = true)
    private BigDecimal subscTotalNum;
	
	@ApiModelProperty(value = "剩余认购总量",required = true)
	private BigDecimal balanceSubscTotalNum;

	@ApiModelProperty(value = "最小认购数量",required = true)
    private BigDecimal minSubscNum;

	@ApiModelProperty(value = "最大认购数量",required = true)
    private BigDecimal maxSubscNum;

	@ApiModelProperty(value = "认购结束日",required = true)
    private Date buyEndDate;

	@ApiModelProperty(value = "起息日",required = true)
    private Date valueDate;

	@ApiModelProperty(value = "到期日",required = true)
    private Date dueDate;

	@ApiModelProperty(value = "赎回日",required = true)
    private Date redemptionDate;

	@ApiModelProperty(value = "时区",required = true)
    private String timeZone;

	@ApiModelProperty(value = "币种类型",required = true)
    private String coinType;

	@ApiModelProperty(value = "币种网络类型",required = true)
    private String coinNetworkType;

	@ApiModelProperty(value = "收益方式：0=预期年化收益，1=预期七天年化收益",required = true)
    private Byte yieldType;

	@ApiModelProperty(value = "收益率（百份之）",required = true)
    private BigDecimal yieldRate;

	@ApiModelProperty(value = "风险等级：0=极低\r\n" + 
			"1=较低\r\n" + 
			"2=中等\r\n" + 
			"3=高",required = true)
    private Byte riskLevel;

	@ApiModelProperty(value = "状态：0=录入状态\r\n" + 
			"1=发布状态",required = true)
    private Byte status;

	@ApiModelProperty(value = "团队名",required = false)
    private String teamName;
    
	@ApiModelProperty(value = "产品简介",required = false)
    private String proIntro;

	@ApiModelProperty(value = "团队简介",required = false)
    private String teamIntro;

	public BigDecimal getBalanceSubscTotalNum() {
		return balanceSubscTotalNum;
	}

	public void setBalanceSubscTotalNum(BigDecimal balanceSubscTotalNum) {
		this.balanceSubscTotalNum = balanceSubscTotalNum;
	}

	public String getProId() {
		return proId;
	}

	public void setProId(String proId) {
		this.proId = proId;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public Integer getSubscPeriodDay() {
		return subscPeriodDay;
	}

	public void setSubscPeriodDay(Integer subscPeriodDay) {
		this.subscPeriodDay = subscPeriodDay;
	}

	public BigDecimal getSubscTotalNum() {
		return subscTotalNum;
	}

	public void setSubscTotalNum(BigDecimal subscTotalNum) {
		this.subscTotalNum = subscTotalNum;
	}

	public BigDecimal getMinSubscNum() {
		return minSubscNum;
	}

	public void setMinSubscNum(BigDecimal minSubscNum) {
		this.minSubscNum = minSubscNum;
	}

	public BigDecimal getMaxSubscNum() {
		return maxSubscNum;
	}

	public void setMaxSubscNum(BigDecimal maxSubscNum) {
		this.maxSubscNum = maxSubscNum;
	}

	public Date getBuyEndDate() {
		return buyEndDate;
	}

	public void setBuyEndDate(Date buyEndDate) {
		this.buyEndDate = buyEndDate;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getRedemptionDate() {
		return redemptionDate;
	}

	public void setRedemptionDate(Date redemptionDate) {
		this.redemptionDate = redemptionDate;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}

	public Byte getYieldType() {
		return yieldType;
	}

	public void setYieldType(Byte yieldType) {
		this.yieldType = yieldType;
	}

	public BigDecimal getYieldRate() {
		return yieldRate;
	}

	public void setYieldRate(BigDecimal yieldRate) {
		this.yieldRate = yieldRate;
	}

	public Byte getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(Byte riskLevel) {
		this.riskLevel = riskLevel;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getProIntro() {
		return proIntro;
	}

	public void setProIntro(String proIntro) {
		this.proIntro = proIntro;
	}

	public String getTeamIntro() {
		return teamIntro;
	}

	public void setTeamIntro(String teamIntro) {
		this.teamIntro = teamIntro;
	}
	
	
}
