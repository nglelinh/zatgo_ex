package com.zatgo.zup.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class ExtractRecord implements Serializable {
    private String extractId;

    private String accountId;

    private String extractAddress;

    private Date extractDate;

    private Date extractSuccessDate;

    private BigDecimal extractNumber;

    /**
     * 0：提币中，1：提币成功，2：审批中，3：审批通过
     */
    private String extractStatus;

    private String txHash;

    private String blockHash;

    private BigDecimal fee;

    private String approveReason;

    private String approveResult;

    private String coinType;

    private String coinNetworkType;

    private BigDecimal charge;

    private static final long serialVersionUID = 1L;

    public String getExtractId() {
        return extractId;
    }

    public void setExtractId(String extractId) {
        this.extractId = extractId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getExtractAddress() {
        return extractAddress;
    }

    public void setExtractAddress(String extractAddress) {
        this.extractAddress = extractAddress;
    }

    public Date getExtractDate() {
        return extractDate;
    }

    public void setExtractDate(Date extractDate) {
        this.extractDate = extractDate;
    }

    public Date getExtractSuccessDate() {
        return extractSuccessDate;
    }

    public void setExtractSuccessDate(Date extractSuccessDate) {
        this.extractSuccessDate = extractSuccessDate;
    }

    public BigDecimal getExtractNumber() {
        return extractNumber;
    }

    public void setExtractNumber(BigDecimal extractNumber) {
        this.extractNumber = extractNumber;
    }

    public String getExtractStatus() {
        return extractStatus;
    }

    public void setExtractStatus(String extractStatus) {
        this.extractStatus = extractStatus;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getApproveReason() {
        return approveReason;
    }

    public void setApproveReason(String approveReason) {
        this.approveReason = approveReason;
    }

    public String getApproveResult() {
        return approveResult;
    }

    public void setApproveResult(String approveResult) {
        this.approveResult = approveResult;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType;
    }

    public BigDecimal getCharge() {
        return charge;
    }

    public void setCharge(BigDecimal charge) {
        this.charge = charge;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ExtractRecord other = (ExtractRecord) that;
        return (this.getExtractId() == null ? other.getExtractId() == null : this.getExtractId().equals(other.getExtractId()))
            && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
            && (this.getExtractAddress() == null ? other.getExtractAddress() == null : this.getExtractAddress().equals(other.getExtractAddress()))
            && (this.getExtractDate() == null ? other.getExtractDate() == null : this.getExtractDate().equals(other.getExtractDate()))
            && (this.getExtractSuccessDate() == null ? other.getExtractSuccessDate() == null : this.getExtractSuccessDate().equals(other.getExtractSuccessDate()))
            && (this.getExtractNumber() == null ? other.getExtractNumber() == null : this.getExtractNumber().equals(other.getExtractNumber()))
            && (this.getExtractStatus() == null ? other.getExtractStatus() == null : this.getExtractStatus().equals(other.getExtractStatus()))
            && (this.getTxHash() == null ? other.getTxHash() == null : this.getTxHash().equals(other.getTxHash()))
            && (this.getBlockHash() == null ? other.getBlockHash() == null : this.getBlockHash().equals(other.getBlockHash()))
            && (this.getFee() == null ? other.getFee() == null : this.getFee().equals(other.getFee()))
            && (this.getApproveReason() == null ? other.getApproveReason() == null : this.getApproveReason().equals(other.getApproveReason()))
            && (this.getApproveResult() == null ? other.getApproveResult() == null : this.getApproveResult().equals(other.getApproveResult()))
            && (this.getCoinType() == null ? other.getCoinType() == null : this.getCoinType().equals(other.getCoinType()))
            && (this.getCoinNetworkType() == null ? other.getCoinNetworkType() == null : this.getCoinNetworkType().equals(other.getCoinNetworkType()))
            && (this.getCharge() == null ? other.getCharge() == null : this.getCharge().equals(other.getCharge()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getExtractId() == null) ? 0 : getExtractId().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getExtractAddress() == null) ? 0 : getExtractAddress().hashCode());
        result = prime * result + ((getExtractDate() == null) ? 0 : getExtractDate().hashCode());
        result = prime * result + ((getExtractSuccessDate() == null) ? 0 : getExtractSuccessDate().hashCode());
        result = prime * result + ((getExtractNumber() == null) ? 0 : getExtractNumber().hashCode());
        result = prime * result + ((getExtractStatus() == null) ? 0 : getExtractStatus().hashCode());
        result = prime * result + ((getTxHash() == null) ? 0 : getTxHash().hashCode());
        result = prime * result + ((getBlockHash() == null) ? 0 : getBlockHash().hashCode());
        result = prime * result + ((getFee() == null) ? 0 : getFee().hashCode());
        result = prime * result + ((getApproveReason() == null) ? 0 : getApproveReason().hashCode());
        result = prime * result + ((getApproveResult() == null) ? 0 : getApproveResult().hashCode());
        result = prime * result + ((getCoinType() == null) ? 0 : getCoinType().hashCode());
        result = prime * result + ((getCoinNetworkType() == null) ? 0 : getCoinNetworkType().hashCode());
        result = prime * result + ((getCharge() == null) ? 0 : getCharge().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", extractId=").append(extractId);
        sb.append(", accountId=").append(accountId);
        sb.append(", extractAddress=").append(extractAddress);
        sb.append(", extractDate=").append(extractDate);
        sb.append(", extractSuccessDate=").append(extractSuccessDate);
        sb.append(", extractNumber=").append(extractNumber);
        sb.append(", extractStatus=").append(extractStatus);
        sb.append(", txHash=").append(txHash);
        sb.append(", blockHash=").append(blockHash);
        sb.append(", fee=").append(fee);
        sb.append(", approveReason=").append(approveReason);
        sb.append(", approveResult=").append(approveResult);
        sb.append(", coinType=").append(coinType);
        sb.append(", coinNetworkType=").append(coinNetworkType);
        sb.append(", charge=").append(charge);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}