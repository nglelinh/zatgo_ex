package com.zatgo.zup.wallet.service.Impl;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.wallet.PaymentRecordData;
import com.zatgo.zup.wallet.entity.PaymentRecord;
import com.zatgo.zup.wallet.mapper.PaymentRecordMapper;
import com.zatgo.zup.wallet.service.PaymentRecordService;
@Component
public class PaymentRecordServiceImpl implements PaymentRecordService {
	
	private static final Logger logger = LoggerFactory.getLogger(PaymentRecordServiceImpl.class);
	
	@Autowired
	private PaymentRecordMapper paymentRecordMapper;
	@Override
	public List<PaymentRecord> listRecordByAccountId(String accountId) {
		return paymentRecordMapper.selectRecordByAccountId(accountId);
	}

	@Override
	public List<PaymentRecord> listRecordByUserId(String userId) {
		return paymentRecordMapper.selectRecordByUserId(userId);
	}

	@Override
	public PageInfo<PaymentRecord> listRecordPageByUserId(String userId, String pageNo, String pageSize) {
		PageHelper.startPage(Integer.valueOf(pageNo), Integer.valueOf(pageSize));
		Page<PaymentRecord> pageData = paymentRecordMapper.selectRecordPageByUserId(userId);
		
		PageInfo<PaymentRecord> pageInfoOrder =
				new PageInfo<PaymentRecord>(pageData.getPageNum(),pageData.getPageSize(),pageData.getTotal(),pageData.getResult());

		PageHelper.clearPage();
		return pageInfoOrder;
	}

	@Override
	public PageInfo<PaymentRecord> listRecordPageByAccountId(String accountId, String pageNo, String pageSize) {
		PageHelper.startPage(Integer.valueOf(pageNo), Integer.valueOf(pageSize));
		Page<PaymentRecord> pageData = paymentRecordMapper.selectRecordPageByAccountId(accountId);
		
		PageInfo<PaymentRecord> pageInfoOrder =
				new PageInfo<PaymentRecord>(pageData.getPageNum(),pageData.getPageSize(),pageData.getTotal(),pageData.getResult());

		PageHelper.clearPage();
		return pageInfoOrder;
	}

	@Override
	public PaymentRecordData getPaymentRecordByTradeNo(String userId,String tradeNo) {
		PaymentRecord paymentRecord = paymentRecordMapper.selectRecordByOrderId(tradeNo, userId);
		
		PaymentRecordData data = new PaymentRecordData();
		try {
			BeanUtils.copyProperties(data, paymentRecord);
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
		
		return data;
	}
}
