package com.zatgo.zup.wallet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ExtractParams;
import com.zatgo.zup.common.model.ExtractRecordData;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wallet.ExtractApprovalParams;
import com.zatgo.zup.common.model.wallet.ExtractRecordByCloudUserParams;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.wallet.entity.ExtractRecord;
import com.zatgo.zup.wallet.service.ExtractService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/wallet/extract",description = "提币接口")
@RestController
@RequestMapping(value = "/wallet/extract")
public class ExtractController  extends BaseController{
	@Autowired
	private ExtractService extractService;
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private RedisLockUtils redisLockUtils;
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@ApiOperation(value = "提币接口", notes = "提币接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<String> extract(@RequestBody ExtractParams params){
		AuthUserInfo userInfo = getUserInfo();
		String userId = userInfo.getUserId();
		return BusinessResponseFactory.createSuccess(extractService.extract(params,userId));
	}

	@RequestMapping(value = "/record/list",method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "提币记录查询", notes = "提币记录查询", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseData<List<ExtractRecord>> listRecordByUserId(){
		AuthUserInfo userInfo = getUserInfo();
		List<ExtractRecord> result = extractService.listExtractRecord(userInfo.getUserId(), userInfo.getCloudUserId());
		return BusinessResponseFactory.createSuccess(result);
	}
	
	
	@RequestMapping(value = "/clouduser/record",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<PageInfo<ExtractRecordData>> selectExtractRecordByCloudUser(@RequestBody ExtractRecordByCloudUserParams params){
		AuthUserInfo userInfo = getUserInfo();
		PageInfo<ExtractRecordData>  pageInfo = extractService.selectExtractRecordByCloudUser(userInfo.getCloudUserId(), params);
		
		return BusinessResponseFactory.createSuccess(pageInfo);
	}
	
	
	@RequestMapping(value = "/clouduser/approval",method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Boolean> approvalExtractRecordByCloudUser(@RequestBody ExtractApprovalParams params){
		AuthUserInfo userInfo = getAdminInfo();
		String redisLock = RedisKeyConstants.BUSI_EXTRACT + params.getExtractRecordId();
		redisLockUtils.lock(redisLock);
		try {
			Boolean  isSuccess = extractService.approvalExtractRecordByCloudUser(userInfo.getCloudUserId(),userInfo.getAdminId(), params);
			return BusinessResponseFactory.createSuccess(isSuccess);
		}finally {
			redisLockUtils.releaseLock(redisLock);
		}
	}

}
