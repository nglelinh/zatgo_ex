package com.zatgo.zup.wallet.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zatgo.zup.common.model.UserData;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("zup-merchant")
public interface UserRemoteService {

	@RequestMapping("/user/byname/{userName}")
	ResponseData<UserData> getUser(@PathVariable("userName") String userName, @RequestParam("cloudUserId") String cloudUserId);
	
	@RequestMapping("/user/updatelogintime/{userId}")
	void updateLastLoginTime(String userId);
	
	@RequestMapping("/user/byid/{userId}")
	ResponseData<UserData> getUserById(@PathVariable("userId") String userId);

	@PostMapping("/user/cloud/get/manageUserId/{userId}/{cloudManageType}")
	ResponseData<UserData> getManageUserId(@PathVariable("userId") String userId,@PathVariable("cloudManageType") String cloudManageType);
	
	@PostMapping("/user/cloud/get/manageUserByCloudUserId/{cloudUserId}/{cloudManageType}")
	ResponseData<UserData> getManageUserByCloudUserId(@PathVariable("cloudUserId") String userId,@PathVariable("cloudManageType") String cloudManageType);
}