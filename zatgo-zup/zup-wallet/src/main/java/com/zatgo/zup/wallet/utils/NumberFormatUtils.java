package com.zatgo.zup.wallet.utils;

public class NumberFormatUtils {

	public static String format(String s){
		if(s.indexOf(".") > 0){
			s = s.replaceAll("0+?$", "");
			s = s.replaceAll("[.]$", "");
		}
		return s;
	}

}
