package com.zatgo.zup.wallet.service;

import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.entity.SysParams;

import java.math.BigDecimal;
import java.util.List;

public interface SystemParamsService {

	List<IssuedCoinInfo> queryCoinTypes(String cloudUserId);

	BigDecimal queryCoinRate(String coinType);

	SysParams selectByCode(String code);
}
