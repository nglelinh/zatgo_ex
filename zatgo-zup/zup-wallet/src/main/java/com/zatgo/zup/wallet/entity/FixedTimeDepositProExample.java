package com.zatgo.zup.wallet.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FixedTimeDepositProExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FixedTimeDepositProExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andProIdIsNull() {
            addCriterion("pro_id is null");
            return (Criteria) this;
        }

        public Criteria andProIdIsNotNull() {
            addCriterion("pro_id is not null");
            return (Criteria) this;
        }

        public Criteria andProIdEqualTo(String value) {
            addCriterion("pro_id =", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdNotEqualTo(String value) {
            addCriterion("pro_id <>", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdGreaterThan(String value) {
            addCriterion("pro_id >", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdGreaterThanOrEqualTo(String value) {
            addCriterion("pro_id >=", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdLessThan(String value) {
            addCriterion("pro_id <", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdLessThanOrEqualTo(String value) {
            addCriterion("pro_id <=", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdLike(String value) {
            addCriterion("pro_id like", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdNotLike(String value) {
            addCriterion("pro_id not like", value, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdIn(List<String> values) {
            addCriterion("pro_id in", values, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdNotIn(List<String> values) {
            addCriterion("pro_id not in", values, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdBetween(String value1, String value2) {
            addCriterion("pro_id between", value1, value2, "proId");
            return (Criteria) this;
        }

        public Criteria andProIdNotBetween(String value1, String value2) {
            addCriterion("pro_id not between", value1, value2, "proId");
            return (Criteria) this;
        }

        public Criteria andProNameIsNull() {
            addCriterion("pro_name is null");
            return (Criteria) this;
        }

        public Criteria andProNameIsNotNull() {
            addCriterion("pro_name is not null");
            return (Criteria) this;
        }

        public Criteria andProNameEqualTo(String value) {
            addCriterion("pro_name =", value, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameNotEqualTo(String value) {
            addCriterion("pro_name <>", value, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameGreaterThan(String value) {
            addCriterion("pro_name >", value, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameGreaterThanOrEqualTo(String value) {
            addCriterion("pro_name >=", value, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameLessThan(String value) {
            addCriterion("pro_name <", value, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameLessThanOrEqualTo(String value) {
            addCriterion("pro_name <=", value, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameLike(String value) {
            addCriterion("pro_name like", value, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameNotLike(String value) {
            addCriterion("pro_name not like", value, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameIn(List<String> values) {
            addCriterion("pro_name in", values, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameNotIn(List<String> values) {
            addCriterion("pro_name not in", values, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameBetween(String value1, String value2) {
            addCriterion("pro_name between", value1, value2, "proName");
            return (Criteria) this;
        }

        public Criteria andProNameNotBetween(String value1, String value2) {
            addCriterion("pro_name not between", value1, value2, "proName");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayIsNull() {
            addCriterion("subsc_period_day is null");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayIsNotNull() {
            addCriterion("subsc_period_day is not null");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayEqualTo(Integer value) {
            addCriterion("subsc_period_day =", value, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayNotEqualTo(Integer value) {
            addCriterion("subsc_period_day <>", value, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayGreaterThan(Integer value) {
            addCriterion("subsc_period_day >", value, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayGreaterThanOrEqualTo(Integer value) {
            addCriterion("subsc_period_day >=", value, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayLessThan(Integer value) {
            addCriterion("subsc_period_day <", value, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayLessThanOrEqualTo(Integer value) {
            addCriterion("subsc_period_day <=", value, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayIn(List<Integer> values) {
            addCriterion("subsc_period_day in", values, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayNotIn(List<Integer> values) {
            addCriterion("subsc_period_day not in", values, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayBetween(Integer value1, Integer value2) {
            addCriterion("subsc_period_day between", value1, value2, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscPeriodDayNotBetween(Integer value1, Integer value2) {
            addCriterion("subsc_period_day not between", value1, value2, "subscPeriodDay");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumIsNull() {
            addCriterion("subsc_total_num is null");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumIsNotNull() {
            addCriterion("subsc_total_num is not null");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumEqualTo(BigDecimal value) {
            addCriterion("subsc_total_num =", value, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumNotEqualTo(BigDecimal value) {
            addCriterion("subsc_total_num <>", value, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumGreaterThan(BigDecimal value) {
            addCriterion("subsc_total_num >", value, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("subsc_total_num >=", value, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumLessThan(BigDecimal value) {
            addCriterion("subsc_total_num <", value, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("subsc_total_num <=", value, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumIn(List<BigDecimal> values) {
            addCriterion("subsc_total_num in", values, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumNotIn(List<BigDecimal> values) {
            addCriterion("subsc_total_num not in", values, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("subsc_total_num between", value1, value2, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andSubscTotalNumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("subsc_total_num not between", value1, value2, "subscTotalNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumIsNull() {
            addCriterion("min_subsc_num is null");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumIsNotNull() {
            addCriterion("min_subsc_num is not null");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumEqualTo(BigDecimal value) {
            addCriterion("min_subsc_num =", value, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumNotEqualTo(BigDecimal value) {
            addCriterion("min_subsc_num <>", value, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumGreaterThan(BigDecimal value) {
            addCriterion("min_subsc_num >", value, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("min_subsc_num >=", value, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumLessThan(BigDecimal value) {
            addCriterion("min_subsc_num <", value, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("min_subsc_num <=", value, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumIn(List<BigDecimal> values) {
            addCriterion("min_subsc_num in", values, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumNotIn(List<BigDecimal> values) {
            addCriterion("min_subsc_num not in", values, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("min_subsc_num between", value1, value2, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMinSubscNumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("min_subsc_num not between", value1, value2, "minSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumIsNull() {
            addCriterion("max_subsc_num is null");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumIsNotNull() {
            addCriterion("max_subsc_num is not null");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumEqualTo(BigDecimal value) {
            addCriterion("max_subsc_num =", value, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumNotEqualTo(BigDecimal value) {
            addCriterion("max_subsc_num <>", value, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumGreaterThan(BigDecimal value) {
            addCriterion("max_subsc_num >", value, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("max_subsc_num >=", value, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumLessThan(BigDecimal value) {
            addCriterion("max_subsc_num <", value, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("max_subsc_num <=", value, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumIn(List<BigDecimal> values) {
            addCriterion("max_subsc_num in", values, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumNotIn(List<BigDecimal> values) {
            addCriterion("max_subsc_num not in", values, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("max_subsc_num between", value1, value2, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andMaxSubscNumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("max_subsc_num not between", value1, value2, "maxSubscNum");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateIsNull() {
            addCriterion("buy_end_date is null");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateIsNotNull() {
            addCriterion("buy_end_date is not null");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateEqualTo(Date value) {
            addCriterion("buy_end_date =", value, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateNotEqualTo(Date value) {
            addCriterion("buy_end_date <>", value, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateGreaterThan(Date value) {
            addCriterion("buy_end_date >", value, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("buy_end_date >=", value, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateLessThan(Date value) {
            addCriterion("buy_end_date <", value, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateLessThanOrEqualTo(Date value) {
            addCriterion("buy_end_date <=", value, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateIn(List<Date> values) {
            addCriterion("buy_end_date in", values, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateNotIn(List<Date> values) {
            addCriterion("buy_end_date not in", values, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateBetween(Date value1, Date value2) {
            addCriterion("buy_end_date between", value1, value2, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andBuyEndDateNotBetween(Date value1, Date value2) {
            addCriterion("buy_end_date not between", value1, value2, "buyEndDate");
            return (Criteria) this;
        }

        public Criteria andValueDateIsNull() {
            addCriterion("value_date is null");
            return (Criteria) this;
        }

        public Criteria andValueDateIsNotNull() {
            addCriterion("value_date is not null");
            return (Criteria) this;
        }

        public Criteria andValueDateEqualTo(Date value) {
            addCriterion("value_date =", value, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateNotEqualTo(Date value) {
            addCriterion("value_date <>", value, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateGreaterThan(Date value) {
            addCriterion("value_date >", value, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateGreaterThanOrEqualTo(Date value) {
            addCriterion("value_date >=", value, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateLessThan(Date value) {
            addCriterion("value_date <", value, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateLessThanOrEqualTo(Date value) {
            addCriterion("value_date <=", value, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateIn(List<Date> values) {
            addCriterion("value_date in", values, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateNotIn(List<Date> values) {
            addCriterion("value_date not in", values, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateBetween(Date value1, Date value2) {
            addCriterion("value_date between", value1, value2, "valueDate");
            return (Criteria) this;
        }

        public Criteria andValueDateNotBetween(Date value1, Date value2) {
            addCriterion("value_date not between", value1, value2, "valueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateIsNull() {
            addCriterion("due_date is null");
            return (Criteria) this;
        }

        public Criteria andDueDateIsNotNull() {
            addCriterion("due_date is not null");
            return (Criteria) this;
        }

        public Criteria andDueDateEqualTo(Date value) {
            addCriterion("due_date =", value, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateNotEqualTo(Date value) {
            addCriterion("due_date <>", value, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateGreaterThan(Date value) {
            addCriterion("due_date >", value, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateGreaterThanOrEqualTo(Date value) {
            addCriterion("due_date >=", value, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateLessThan(Date value) {
            addCriterion("due_date <", value, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateLessThanOrEqualTo(Date value) {
            addCriterion("due_date <=", value, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateIn(List<Date> values) {
            addCriterion("due_date in", values, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateNotIn(List<Date> values) {
            addCriterion("due_date not in", values, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateBetween(Date value1, Date value2) {
            addCriterion("due_date between", value1, value2, "dueDate");
            return (Criteria) this;
        }

        public Criteria andDueDateNotBetween(Date value1, Date value2) {
            addCriterion("due_date not between", value1, value2, "dueDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateIsNull() {
            addCriterion("redemption_date is null");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateIsNotNull() {
            addCriterion("redemption_date is not null");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateEqualTo(Date value) {
            addCriterion("redemption_date =", value, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateNotEqualTo(Date value) {
            addCriterion("redemption_date <>", value, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateGreaterThan(Date value) {
            addCriterion("redemption_date >", value, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateGreaterThanOrEqualTo(Date value) {
            addCriterion("redemption_date >=", value, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateLessThan(Date value) {
            addCriterion("redemption_date <", value, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateLessThanOrEqualTo(Date value) {
            addCriterion("redemption_date <=", value, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateIn(List<Date> values) {
            addCriterion("redemption_date in", values, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateNotIn(List<Date> values) {
            addCriterion("redemption_date not in", values, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateBetween(Date value1, Date value2) {
            addCriterion("redemption_date between", value1, value2, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andRedemptionDateNotBetween(Date value1, Date value2) {
            addCriterion("redemption_date not between", value1, value2, "redemptionDate");
            return (Criteria) this;
        }

        public Criteria andTimeZoneIsNull() {
            addCriterion("time_zone is null");
            return (Criteria) this;
        }

        public Criteria andTimeZoneIsNotNull() {
            addCriterion("time_zone is not null");
            return (Criteria) this;
        }

        public Criteria andTimeZoneEqualTo(String value) {
            addCriterion("time_zone =", value, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneNotEqualTo(String value) {
            addCriterion("time_zone <>", value, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneGreaterThan(String value) {
            addCriterion("time_zone >", value, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneGreaterThanOrEqualTo(String value) {
            addCriterion("time_zone >=", value, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneLessThan(String value) {
            addCriterion("time_zone <", value, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneLessThanOrEqualTo(String value) {
            addCriterion("time_zone <=", value, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneLike(String value) {
            addCriterion("time_zone like", value, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneNotLike(String value) {
            addCriterion("time_zone not like", value, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneIn(List<String> values) {
            addCriterion("time_zone in", values, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneNotIn(List<String> values) {
            addCriterion("time_zone not in", values, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneBetween(String value1, String value2) {
            addCriterion("time_zone between", value1, value2, "timeZone");
            return (Criteria) this;
        }

        public Criteria andTimeZoneNotBetween(String value1, String value2) {
            addCriterion("time_zone not between", value1, value2, "timeZone");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNull() {
            addCriterion("coin_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNotNull() {
            addCriterion("coin_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeEqualTo(String value) {
            addCriterion("coin_type =", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotEqualTo(String value) {
            addCriterion("coin_type <>", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThan(String value) {
            addCriterion("coin_type >", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_type >=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThan(String value) {
            addCriterion("coin_type <", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_type <=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLike(String value) {
            addCriterion("coin_type like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotLike(String value) {
            addCriterion("coin_type not like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIn(List<String> values) {
            addCriterion("coin_type in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotIn(List<String> values) {
            addCriterion("coin_type not in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeBetween(String value1, String value2) {
            addCriterion("coin_type between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotBetween(String value1, String value2) {
            addCriterion("coin_type not between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNull() {
            addCriterion("coin_network_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNotNull() {
            addCriterion("coin_network_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeEqualTo(String value) {
            addCriterion("coin_network_type =", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotEqualTo(String value) {
            addCriterion("coin_network_type <>", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThan(String value) {
            addCriterion("coin_network_type >", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_network_type >=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThan(String value) {
            addCriterion("coin_network_type <", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_network_type <=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLike(String value) {
            addCriterion("coin_network_type like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotLike(String value) {
            addCriterion("coin_network_type not like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIn(List<String> values) {
            addCriterion("coin_network_type in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotIn(List<String> values) {
            addCriterion("coin_network_type not in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeBetween(String value1, String value2) {
            addCriterion("coin_network_type between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotBetween(String value1, String value2) {
            addCriterion("coin_network_type not between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeIsNull() {
            addCriterion("yield_type is null");
            return (Criteria) this;
        }

        public Criteria andYieldTypeIsNotNull() {
            addCriterion("yield_type is not null");
            return (Criteria) this;
        }

        public Criteria andYieldTypeEqualTo(Byte value) {
            addCriterion("yield_type =", value, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeNotEqualTo(Byte value) {
            addCriterion("yield_type <>", value, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeGreaterThan(Byte value) {
            addCriterion("yield_type >", value, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("yield_type >=", value, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeLessThan(Byte value) {
            addCriterion("yield_type <", value, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeLessThanOrEqualTo(Byte value) {
            addCriterion("yield_type <=", value, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeIn(List<Byte> values) {
            addCriterion("yield_type in", values, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeNotIn(List<Byte> values) {
            addCriterion("yield_type not in", values, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeBetween(Byte value1, Byte value2) {
            addCriterion("yield_type between", value1, value2, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("yield_type not between", value1, value2, "yieldType");
            return (Criteria) this;
        }

        public Criteria andYieldRateIsNull() {
            addCriterion("yield_rate is null");
            return (Criteria) this;
        }

        public Criteria andYieldRateIsNotNull() {
            addCriterion("yield_rate is not null");
            return (Criteria) this;
        }

        public Criteria andYieldRateEqualTo(BigDecimal value) {
            addCriterion("yield_rate =", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateNotEqualTo(BigDecimal value) {
            addCriterion("yield_rate <>", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateGreaterThan(BigDecimal value) {
            addCriterion("yield_rate >", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("yield_rate >=", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateLessThan(BigDecimal value) {
            addCriterion("yield_rate <", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("yield_rate <=", value, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateIn(List<BigDecimal> values) {
            addCriterion("yield_rate in", values, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateNotIn(List<BigDecimal> values) {
            addCriterion("yield_rate not in", values, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("yield_rate between", value1, value2, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andYieldRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("yield_rate not between", value1, value2, "yieldRate");
            return (Criteria) this;
        }

        public Criteria andRiskLevelIsNull() {
            addCriterion("risk_level is null");
            return (Criteria) this;
        }

        public Criteria andRiskLevelIsNotNull() {
            addCriterion("risk_level is not null");
            return (Criteria) this;
        }

        public Criteria andRiskLevelEqualTo(Byte value) {
            addCriterion("risk_level =", value, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelNotEqualTo(Byte value) {
            addCriterion("risk_level <>", value, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelGreaterThan(Byte value) {
            addCriterion("risk_level >", value, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelGreaterThanOrEqualTo(Byte value) {
            addCriterion("risk_level >=", value, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelLessThan(Byte value) {
            addCriterion("risk_level <", value, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelLessThanOrEqualTo(Byte value) {
            addCriterion("risk_level <=", value, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelIn(List<Byte> values) {
            addCriterion("risk_level in", values, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelNotIn(List<Byte> values) {
            addCriterion("risk_level not in", values, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelBetween(Byte value1, Byte value2) {
            addCriterion("risk_level between", value1, value2, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andRiskLevelNotBetween(Byte value1, Byte value2) {
            addCriterion("risk_level not between", value1, value2, "riskLevel");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andTeamNameIsNull() {
            addCriterion("team_name is null");
            return (Criteria) this;
        }

        public Criteria andTeamNameIsNotNull() {
            addCriterion("team_name is not null");
            return (Criteria) this;
        }

        public Criteria andTeamNameEqualTo(String value) {
            addCriterion("team_name =", value, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameNotEqualTo(String value) {
            addCriterion("team_name <>", value, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameGreaterThan(String value) {
            addCriterion("team_name >", value, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameGreaterThanOrEqualTo(String value) {
            addCriterion("team_name >=", value, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameLessThan(String value) {
            addCriterion("team_name <", value, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameLessThanOrEqualTo(String value) {
            addCriterion("team_name <=", value, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameLike(String value) {
            addCriterion("team_name like", value, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameNotLike(String value) {
            addCriterion("team_name not like", value, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameIn(List<String> values) {
            addCriterion("team_name in", values, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameNotIn(List<String> values) {
            addCriterion("team_name not in", values, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameBetween(String value1, String value2) {
            addCriterion("team_name between", value1, value2, "teamName");
            return (Criteria) this;
        }

        public Criteria andTeamNameNotBetween(String value1, String value2) {
            addCriterion("team_name not between", value1, value2, "teamName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}