package com.zatgo.zup.wallet.blockchain;

import java.math.BigDecimal;

import com.zatgo.zup.common.model.QtumTransaction;
import com.zatgo.zup.wallet.model.LocalTransaction;
import com.zatgo.zup.wallet.model.SendParams;
import com.zatgo.zup.wallet.model.TxRecord;

public interface QtumService {

	String getNewAddress();

	String sendTransaction(SendParams params);

	String sendContractTransaction(SendParams params);

	LocalTransaction listTransactions(String accountId);

	TxRecord getTransactionByTxId(String txId);
	BigDecimal getTokenBalance(String address);

	BigDecimal getQtumBalance(String address);

	TxRecord getTokenTransactionRecord(String id);

	QtumTransaction getTransaction(String txId);
}
