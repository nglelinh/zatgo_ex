package com.zatgo.zup.wallet.service;

import java.math.BigDecimal;
import java.util.List;

import com.zatgo.zup.common.model.ExtractParams;
import com.zatgo.zup.common.model.ExtractRecordData;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.wallet.ExtractApprovalParams;
import com.zatgo.zup.common.model.wallet.ExtractRecordByCloudUserParams;
import com.zatgo.zup.wallet.entity.ExtractRecord;

public interface ExtractService {

	String extract(ExtractParams params,String userId);

	/**
	 * 查询用户的所有提币记录
	 * @param userId
	 * @return
	 */
	List<ExtractRecord> listExtractRecord(String userId, String cloudUserId);
	
	/**
	 * 查询审批通过的记录
	 * @return
	 */
	List<ExtractRecord> listApprovedRecord(String type);


	/**
	 * 充值成功后，更新状态
	 * @param extractId
	 * @param realFee
	 * @param txId
	 * @return
	 */
	String updateStatus(String extractId, BigDecimal realFee, String txId);
	
	/**
	 * 云用户查询提币记录
	 * @param cloudUserId
	 * @param params
	 * @return
	 */
	PageInfo<ExtractRecordData> selectExtractRecordByCloudUser(
			String cloudUserId,ExtractRecordByCloudUserParams params);
	
	/**
	 * 审批
	 * @param cloudUserId
	 * @param param
	 * @return
	 */
	boolean approvalExtractRecordByCloudUser(String cloudUserId,String adminId, ExtractApprovalParams param);
}
