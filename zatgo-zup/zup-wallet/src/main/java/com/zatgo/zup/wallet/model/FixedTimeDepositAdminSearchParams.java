package com.zatgo.zup.wallet.model;

import io.swagger.annotations.ApiModelProperty;

public class FixedTimeDepositAdminSearchParams {

	@ApiModelProperty(value = "状态：0=录入状态,1=发布状态,空为所有",required = false)
	private Byte status;
	
	@ApiModelProperty(value = "页码",required = true)
	private int pageNo;
	
	@ApiModelProperty(value = "每页数量",required = true)
	private int pageSize;



	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	} 
	
	
}
