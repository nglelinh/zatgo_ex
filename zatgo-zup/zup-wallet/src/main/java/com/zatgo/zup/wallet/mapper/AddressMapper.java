package com.zatgo.zup.wallet.mapper;

import com.zatgo.zup.wallet.entity.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressMapper {
    int deleteByPrimaryKey(String id);

    int insert(Address record);

    int insertSelective(Address record);

    Address selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Address record);

    int updateByPrimaryKey(Address record);

	Address selectUnusedAddress(@Param("type") String type);

	Address selectByAddress(@Param("address")String address);

    int insertAddresses(List<Address> list);

    Integer countByNetworkTypeAndIsUsed(@Param("networkType") String networkType,@Param("isUsed") Byte isUsed);
}