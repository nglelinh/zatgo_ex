package com.zatgo.zup.wallet.service.Impl;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.zatgo.zup.wallet.entity.*;
import com.zatgo.zup.wallet.mapper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.wallet.model.DepositAddressParams;
import com.zatgo.zup.wallet.service.AccountService;
import com.zatgo.zup.wallet.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService {
	private static final Logger logger = LoggerFactory.getLogger(AddressServiceImpl.class);
	@Autowired
	private AccountService accountService;
	@Autowired
	private AccountAddressMapper accountAddressMapper;
//	@Autowired
//	private QtumService qtumService;
	@Autowired
	private WalletAccountMapper walletAccountMapper;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	@Autowired
	private AddressMapper addressMapper;
	@Autowired
	private IssuedCoinInfoMapper issuedCoinInfoMapper;
	@Autowired
	private CloudUserCoinRelMapper cloudUserCoinRelMapper;



	@Override
	public List<String> listAddresses(String accountId) {
		List<String> addresses = accountAddressMapper.selectAddressesByAccountId(accountId);
		return addresses;
	}
	public final static String GET_ADDRESS  = "GET_NEW_ADDRESS";

	private static ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//	@Override
//	@Transactional
//	public AccountAddress createDepositAddress(DepositAddressParams params) {
//		//解决同一用户并发问题
//		String addressLockKey = RedisKeyConstants.BUSI_ADDRESS+params.getUserId();
//		redisLockUtils.lock(addressLockKey);
//
//		try	{
//			//如果没有钱包，先创建钱包
//			WalletAccount walletAccount = walletAccountMapper.selectByUserIdAndType(params.getUserId(), params.getCoinType());
//			if(walletAccount == null){
//				CreateAccountParams createAccount = new CreateAccountParams();
//				createAccount.setCurrencyType(params.getCoinType());
//				createAccount.setNetworkType(params.getNetworkType());
//				createAccount.setUserId(params.getUserId());
//				createAccount.setName(params.getCoinType().toString());
//				walletAccount = accountService.createAccount(createAccount);
//			}
//
//			//获取地址
//			String address = null;
//			List<AccountAddress> accountAddresses = accountAddressMapper.selectAddressByUserId(params.getUserId(), params.getNetworkType());
//			if(CollectionUtils.isEmpty(accountAddresses)){
//				if(params.getNetworkType().equals(BusinessEnum.NetWorkType.QTUM.getCode())){
//					address = qtumService.getNewAddress();
//				}else {
//					throw new BusinessException(BusinessExceptionCode.NOT_SUPPORT_COIN_NETWORK);
//				}
//			} else {
//				address = accountAddresses.get(0).getAddress();
//			}
//			AccountAddress accountAddress = accountAddressMapper.selectFromAccountIdAndAddress(walletAccount.getAccountId(),address);
//			if(accountAddress == null){
//				accountAddress = new AccountAddress();
//				accountAddress.setAccountId(walletAccount.getAccountId());
//				accountAddress.setAddress(address);
//				accountAddress.setAddressId(UUIDUtils.getUuid());
//				accountAddressMapper.insertSelective(accountAddress);
//			}
//			return accountAddress;
//		}finally {
//			redisLockUtils.releaseLock(addressLockKey);
//		}
//
//	}

	@Override
	@Transactional
	public AccountAddress createDepostAddressV2(DepositAddressParams params) {
		//解决同一用户并发问题
		String addressLockKey = RedisKeyConstants.BUSI_ADDRESS+params.getNetworkType();
		redisLockUtils.lock(addressLockKey);
		try {
			//如果有地址
			WalletAccount walletAccount = null;
			List<AccountAddress> accountAddresses = accountAddressMapper.selectAddressByUserId(params.getUserId(), params.getNetworkType());
			Address address = null;
			if (!CollectionUtils.isEmpty(accountAddresses)){
				AccountAddress aa = accountAddresses.get(0);
				address = new Address();
				address.setAddress(aa.getAddress());
				//如果有钱包
				walletAccount = walletAccountMapper.selectByUserIdAndType(params.getUserId(), params.getCoinType());
				if(walletAccount != null){
					for (AccountAddress accountAddress : accountAddresses){
						if (walletAccount.getAccountId().equals(accountAddress.getAccountId()))
							return accountAddress;
					}
				}
			}
			//如果没有
			AccountAddress accountAddress = new AccountAddress();
			if(address == null){
				address = addressMapper.selectUnusedAddress(params.getNetworkType());
				if(address == null) {
					logger.error("all address used,networktype={}", params.getNetworkType());
					throw new BusinessException(BusinessExceptionCode.NOT_SUPPORT_COIN_NETWORK);
				}
				//更新address
				address.setIsUsed(BusinessEnum.AddressStatus.USED.getCode());
				addressMapper.updateByPrimaryKey(address);
			}
			//创建钱包
			CreateAccountParams createAccount = new CreateAccountParams();
			createAccount.setCurrencyType(params.getCoinType());
			createAccount.setNetworkType(params.getNetworkType());
			createAccount.setUserId(params.getUserId());
			createAccount.setName(params.getCoinType().toString());
			walletAccount = accountService.createAccount(createAccount);
			//在addressAccount表中没有记录
			accountAddress.setAccountId(walletAccount.getAccountId());
			accountAddress.setAddress(address.getAddress());
			accountAddress.setAddressId(UUIDUtils.getUuid());
			accountAddressMapper.insertSelective(accountAddress);
			accountAddresses.add(accountAddress);
			return accountAddress;
		} catch (Exception e) {
			logger.error("获取地址失败");
			throw e;
		} finally {
			redisLockUtils.releaseLock(addressLockKey);
		}
	}

	@Override
	public Boolean checkCoinType(String cloudUserId, String coinType, String networkType) {
		IssuedCoinInfoExample example = new IssuedCoinInfoExample();
		example.createCriteria().andCoinTypeEqualTo(coinType).andCoinNetworkTypeEqualTo(networkType);
		List<IssuedCoinInfo> issuedCoinInfos = issuedCoinInfoMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(issuedCoinInfos)){
			return false;
		}
		IssuedCoinInfo issuedCoinInfo = issuedCoinInfos.get(0);
		if (issuedCoinInfo.getCloseDeposit()){
			return false;
		}
		CloudUserCoinRelExample cloudUserCoinRelExample = new CloudUserCoinRelExample();
		cloudUserCoinRelExample.createCriteria().andCloudUserIdEqualTo(cloudUserId).andRecordIdEqualTo(issuedCoinInfo.getRecordId());
		List<CloudUserCoinRel> list = cloudUserCoinRelMapper.selectByExample(cloudUserCoinRelExample);
		if (CollectionUtils.isEmpty(list)){
			return false;
		}
		CloudUserCoinRel cloudUserCoinRel = list.get(0);
		if (cloudUserCoinRel.getIsCloseDeposit()){
			return false;
		}
		return true;
	}

	@Override
	public void add(List<Address> list) {
		if (CollectionUtils.isEmpty(list))
			return;
		addressMapper.insertAddresses(list);
	}


	public Integer checkRemainderAddress(String networkType){
		return addressMapper.countByNetworkTypeAndIsUsed(networkType, new Byte("0"));
	}
}
