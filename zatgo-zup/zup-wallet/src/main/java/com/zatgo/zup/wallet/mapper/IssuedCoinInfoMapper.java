package com.zatgo.zup.wallet.mapper;

import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IssuedCoinInfoMapper extends BaseMapper<IssuedCoinInfo>{

	IssuedCoinInfo selectByCoinType(@Param("coinType") String coinType);

	List<IssuedCoinInfo> selectAllCoins(String cloudUserId);
}