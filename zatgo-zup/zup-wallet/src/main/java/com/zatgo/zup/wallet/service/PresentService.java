package com.zatgo.zup.wallet.service;

import java.util.List;

import com.zatgo.zup.wallet.entity.CoinPresentRecord;

public interface PresentService {

	/**
	 * 领取赠送币
	 * @param userName
	 * @param verifyCode
	 */
	void receive(String userName,String verifyCode, String cloudUserId);
	
	/**
	 * 查询未赠送的列表
	 * @param cloudUserId
	 */
	List<CoinPresentRecord> findUnPresentList(String cloudUserId);
}
