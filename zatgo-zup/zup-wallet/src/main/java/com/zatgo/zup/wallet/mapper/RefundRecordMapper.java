package com.zatgo.zup.wallet.mapper;

import com.zatgo.zup.wallet.entity.RefundRecord;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

public interface RefundRecordMapper {
    int deleteByPrimaryKey(String refundId);

    int insert(RefundRecord record);

    int insertSelective(RefundRecord record);

    RefundRecord selectByPrimaryKey(String refundId);

    int updateByPrimaryKeySelective(RefundRecord record);

    int updateByPrimaryKey(RefundRecord record);

	RefundRecord selectByOrderId(@Param("orderId")String orderId);

	BigDecimal sumRefundMoney(@Param("orderId")String orderId);

}