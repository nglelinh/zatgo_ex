package com.zatgo.zup.wallet.service.Impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.wallet.WalletAccountData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.wallet.entity.IssuedCoinInfo;
import com.zatgo.zup.wallet.entity.WalletAccount;
import com.zatgo.zup.wallet.mapper.WalletAccountMapper;
import com.zatgo.zup.wallet.remoteservice.UserRemoteService;
import com.zatgo.zup.wallet.service.AccountService;
import com.zatgo.zup.wallet.service.CoinInfoService;
import com.zatgo.zup.wallet.service.WalletAccountService;
import com.zatgo.zup.wallet.utils.NumberFormatUtils;

@Service
public class AccountServiceImpl implements AccountService {

	private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);
	@Autowired
	private WalletAccountMapper walletAccountMapper;
//	@Autowired
//	private QtumService qtumService;
//	@Autowired
//	private BtcService btcService;
//	@Autowired
//	private EthService ethService;
	@Autowired
	private WalletAccountService walletAccountService;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	@Autowired
	private CoinInfoService coinInfoService;
	@Autowired
	private UserRemoteService userRemoteService;
	@Override
	public List<WalletAccountData> listAccount(String userId, String cloudUserId) {
		//获取云账户配置
		List<IssuedCoinInfo> issuedCoinInfos = coinInfoService.selectCoinListByCloudUserId(cloudUserId);
		Set<String> set = new HashSet<>();
		if (!CollectionUtils.isEmpty(issuedCoinInfos)){
			for (IssuedCoinInfo info : issuedCoinInfos){
				if ("1".equals(info.getStatus().toString()))
					set.add(info.getCoinType());
			}
		}
		List<WalletAccountData> walletAccounts = walletAccountMapper.selectByUserId(userId);
		//将云用户没开通的币种剔除
		if (!CollectionUtils.isEmpty(walletAccounts)) {
			for (int i = walletAccounts.size() - 1; i >= 0; i--) {
				WalletAccountData walletAccount = walletAccounts.get(i);
				if (!set.contains(walletAccount.getCoinType()))
					walletAccounts.remove(i);
				walletAccount.setBalance(walletAccount.getBalance());
			}
			walletAccounts.sort((a1, a2) -> a2.getBalance().compareTo(a1.getBalance()));
		}
		return walletAccounts;
	}

	@Override
	public WalletAccountData getAccountByAccountId(String accountId) {
		return walletAccountMapper.selectByAccountId(accountId);
	}

	@Override
	@Transactional
	public WalletAccount createAccount(CreateAccountParams params) {
		//解决同一用户并发问题
		String accountLockKey = RedisKeyConstants.BUSI_ACCOUNT+params.getUserId();
		redisLockUtils.lock(accountLockKey);
		
		try	{
			String coinType = params.getCurrencyType();
			IssuedCoinInfo issuedCoinInfo = coinInfoService.selectSystemCoinInfoByCoinType(coinType);
			String userId = params.getUserId();
			WalletAccount walletAccount = walletAccountMapper.selectByUserIdAndType(userId, coinType);
			if(walletAccount != null){
			    return walletAccount;
			}
			WalletAccount newAccount = new WalletAccount();
			String accountId = UUIDUtils.getUuid();
			List<WalletAccountData> walletAccounts = walletAccountMapper.selectByUserId(userId);
			newAccount.setAccountType(BusinessEnum.AccountType.wallet.getCode());
			newAccount.setAccountId(accountId);
			newAccount.setAccountName(params.getName());
			newAccount.setCoinNetworkType(params.getNetworkType());
			newAccount.setUserId(userId);
			newAccount.setCoinImage(issuedCoinInfo.getCoinImage());
			newAccount.setCoinType(params.getCurrencyType());
			newAccount.setIsPrimary(BusinessEnum.PrimaryAccount.FALSE.getCode());
			if(CollectionUtils.isEmpty(walletAccounts)){
				newAccount.setIsPrimary(BusinessEnum.PrimaryAccount.TRUE.getCode());
			}
			newAccount.setCreateDate(new Date());
			newAccount.setUpdateDate(new Date());
			newAccount.setBalance(BigDecimal.ZERO);
			walletAccountMapper.insertSelective(newAccount);
			//暂时不生成地址
//			if(coinType.equals(BusinessEnum.NetWorkType.QTUM.getCode()) ||
//					coinType.equals(BusinessEnum.CurrencyEnum.ZAT.getCode())){
//			    qtumService.getNewAddress(accountId);
//			} else if(coinType.equals(BusinessEnum.NetWorkType.ETH.getCode())){
//				ethService.getNewAddress("",accountId);
//			} else if(coinType.equals(BusinessEnum.NetWorkType.BTC.getCode())){
//			    btcService.getNewAddress(accountId);
//			}
			
			return newAccount;
		}finally {
			redisLockUtils.releaseLock(accountLockKey);
		}
	}

	@Override
	public String setAccountAsPrimary(String accountId,String userId) {
		WalletAccount walletAccount = walletAccountMapper.selectByUserPrimaryAccount(userId);
		if(walletAccount != null){
			walletAccountMapper.setPrimaryAccount(walletAccount.getAccountId(),BusinessEnum.PrimaryAccount.FALSE.getCode());
		}
		walletAccountMapper.setPrimaryAccount(accountId,BusinessEnum.PrimaryAccount.TRUE.getCode());
		return accountId;
	}

	@Override
	@Transactional
	public void updateAccountName(String accountId, String name) {

	}

	@Override
	@Transactional
	public void deleteAccount(String accountId) {

	}

	@Override
	public List<WalletAccountData> selectByUserId(String userId) {
		return walletAccountMapper.selectByUserId(userId);
	}

	@Override
	public WalletAccount selectByUserIdAndType(String userId, String type) {
		return walletAccountMapper.selectByUserIdAndType(userId,type);
	}

	@Override
	public WalletAccount selectByAccountId(String accountId) {
		return walletAccountMapper.selectByPrimaryKey(accountId);
	}


	@Override
	public WalletAccount selectUserPrimaryAccount(String userId) {
		return walletAccountMapper.selectByUserPrimaryAccount(userId);
	}

	@Override
	public WalletAccount selectByAddressAndCoinType(String address,String coinType) {
		return walletAccountMapper.selectByAddress(address,coinType);
	}

	@Override
	public List<WalletAccount> selectAccountNeedAggregate(String coinType) {
		IssuedCoinInfo issuedCoinInfo = coinInfoService.selectSystemCoinInfoByCoinType(coinType);
		List<WalletAccount> walletAccounts = walletAccountMapper.selectAccountNeedAggregate(coinType, issuedCoinInfo.getUpperLimitAmount());

		return null;
	}

}
