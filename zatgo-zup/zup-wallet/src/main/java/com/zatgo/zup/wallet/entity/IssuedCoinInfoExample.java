package com.zatgo.zup.wallet.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IssuedCoinInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public IssuedCoinInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRecordIdIsNull() {
            addCriterion("record_id is null");
            return (Criteria) this;
        }

        public Criteria andRecordIdIsNotNull() {
            addCriterion("record_id is not null");
            return (Criteria) this;
        }

        public Criteria andRecordIdEqualTo(String value) {
            addCriterion("record_id =", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotEqualTo(String value) {
            addCriterion("record_id <>", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThan(String value) {
            addCriterion("record_id >", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThanOrEqualTo(String value) {
            addCriterion("record_id >=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThan(String value) {
            addCriterion("record_id <", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThanOrEqualTo(String value) {
            addCriterion("record_id <=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLike(String value) {
            addCriterion("record_id like", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotLike(String value) {
            addCriterion("record_id not like", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdIn(List<String> values) {
            addCriterion("record_id in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotIn(List<String> values) {
            addCriterion("record_id not in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdBetween(String value1, String value2) {
            addCriterion("record_id between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotBetween(String value1, String value2) {
            addCriterion("record_id not between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameIsNull() {
            addCriterion("coin_full_name is null");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameIsNotNull() {
            addCriterion("coin_full_name is not null");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameEqualTo(String value) {
            addCriterion("coin_full_name =", value, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameNotEqualTo(String value) {
            addCriterion("coin_full_name <>", value, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameGreaterThan(String value) {
            addCriterion("coin_full_name >", value, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameGreaterThanOrEqualTo(String value) {
            addCriterion("coin_full_name >=", value, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameLessThan(String value) {
            addCriterion("coin_full_name <", value, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameLessThanOrEqualTo(String value) {
            addCriterion("coin_full_name <=", value, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameLike(String value) {
            addCriterion("coin_full_name like", value, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameNotLike(String value) {
            addCriterion("coin_full_name not like", value, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameIn(List<String> values) {
            addCriterion("coin_full_name in", values, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameNotIn(List<String> values) {
            addCriterion("coin_full_name not in", values, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameBetween(String value1, String value2) {
            addCriterion("coin_full_name between", value1, value2, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinFullNameNotBetween(String value1, String value2) {
            addCriterion("coin_full_name not between", value1, value2, "coinFullName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameIsNull() {
            addCriterion("coin_short_name is null");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameIsNotNull() {
            addCriterion("coin_short_name is not null");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameEqualTo(String value) {
            addCriterion("coin_short_name =", value, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameNotEqualTo(String value) {
            addCriterion("coin_short_name <>", value, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameGreaterThan(String value) {
            addCriterion("coin_short_name >", value, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameGreaterThanOrEqualTo(String value) {
            addCriterion("coin_short_name >=", value, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameLessThan(String value) {
            addCriterion("coin_short_name <", value, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameLessThanOrEqualTo(String value) {
            addCriterion("coin_short_name <=", value, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameLike(String value) {
            addCriterion("coin_short_name like", value, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameNotLike(String value) {
            addCriterion("coin_short_name not like", value, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameIn(List<String> values) {
            addCriterion("coin_short_name in", values, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameNotIn(List<String> values) {
            addCriterion("coin_short_name not in", values, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameBetween(String value1, String value2) {
            addCriterion("coin_short_name between", value1, value2, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinShortNameNotBetween(String value1, String value2) {
            addCriterion("coin_short_name not between", value1, value2, "coinShortName");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolIsNull() {
            addCriterion("coin_symbol is null");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolIsNotNull() {
            addCriterion("coin_symbol is not null");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolEqualTo(String value) {
            addCriterion("coin_symbol =", value, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolNotEqualTo(String value) {
            addCriterion("coin_symbol <>", value, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolGreaterThan(String value) {
            addCriterion("coin_symbol >", value, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolGreaterThanOrEqualTo(String value) {
            addCriterion("coin_symbol >=", value, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolLessThan(String value) {
            addCriterion("coin_symbol <", value, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolLessThanOrEqualTo(String value) {
            addCriterion("coin_symbol <=", value, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolLike(String value) {
            addCriterion("coin_symbol like", value, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolNotLike(String value) {
            addCriterion("coin_symbol not like", value, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolIn(List<String> values) {
            addCriterion("coin_symbol in", values, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolNotIn(List<String> values) {
            addCriterion("coin_symbol not in", values, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolBetween(String value1, String value2) {
            addCriterion("coin_symbol between", value1, value2, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinSymbolNotBetween(String value1, String value2) {
            addCriterion("coin_symbol not between", value1, value2, "coinSymbol");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsIsNull() {
            addCriterion("coin_decimals is null");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsIsNotNull() {
            addCriterion("coin_decimals is not null");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsEqualTo(Integer value) {
            addCriterion("coin_decimals =", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsNotEqualTo(Integer value) {
            addCriterion("coin_decimals <>", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsGreaterThan(Integer value) {
            addCriterion("coin_decimals >", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsGreaterThanOrEqualTo(Integer value) {
            addCriterion("coin_decimals >=", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsLessThan(Integer value) {
            addCriterion("coin_decimals <", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsLessThanOrEqualTo(Integer value) {
            addCriterion("coin_decimals <=", value, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsIn(List<Integer> values) {
            addCriterion("coin_decimals in", values, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsNotIn(List<Integer> values) {
            addCriterion("coin_decimals not in", values, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsBetween(Integer value1, Integer value2) {
            addCriterion("coin_decimals between", value1, value2, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andCoinDecimalsNotBetween(Integer value1, Integer value2) {
            addCriterion("coin_decimals not between", value1, value2, "coinDecimals");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberIsNull() {
            addCriterion("issued_number is null");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberIsNotNull() {
            addCriterion("issued_number is not null");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberEqualTo(Long value) {
            addCriterion("issued_number =", value, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberNotEqualTo(Long value) {
            addCriterion("issued_number <>", value, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberGreaterThan(Long value) {
            addCriterion("issued_number >", value, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberGreaterThanOrEqualTo(Long value) {
            addCriterion("issued_number >=", value, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberLessThan(Long value) {
            addCriterion("issued_number <", value, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberLessThanOrEqualTo(Long value) {
            addCriterion("issued_number <=", value, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberIn(List<Long> values) {
            addCriterion("issued_number in", values, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberNotIn(List<Long> values) {
            addCriterion("issued_number not in", values, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberBetween(Long value1, Long value2) {
            addCriterion("issued_number between", value1, value2, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedNumberNotBetween(Long value1, Long value2) {
            addCriterion("issued_number not between", value1, value2, "issuedNumber");
            return (Criteria) this;
        }

        public Criteria andIssuedWayIsNull() {
            addCriterion("issued_way is null");
            return (Criteria) this;
        }

        public Criteria andIssuedWayIsNotNull() {
            addCriterion("issued_way is not null");
            return (Criteria) this;
        }

        public Criteria andIssuedWayEqualTo(String value) {
            addCriterion("issued_way =", value, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayNotEqualTo(String value) {
            addCriterion("issued_way <>", value, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayGreaterThan(String value) {
            addCriterion("issued_way >", value, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayGreaterThanOrEqualTo(String value) {
            addCriterion("issued_way >=", value, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayLessThan(String value) {
            addCriterion("issued_way <", value, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayLessThanOrEqualTo(String value) {
            addCriterion("issued_way <=", value, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayLike(String value) {
            addCriterion("issued_way like", value, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayNotLike(String value) {
            addCriterion("issued_way not like", value, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayIn(List<String> values) {
            addCriterion("issued_way in", values, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayNotIn(List<String> values) {
            addCriterion("issued_way not in", values, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayBetween(String value1, String value2) {
            addCriterion("issued_way between", value1, value2, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedWayNotBetween(String value1, String value2) {
            addCriterion("issued_way not between", value1, value2, "issuedWay");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeIsNull() {
            addCriterion("issued_datetime is null");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeIsNotNull() {
            addCriterion("issued_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeEqualTo(Date value) {
            addCriterion("issued_datetime =", value, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeNotEqualTo(Date value) {
            addCriterion("issued_datetime <>", value, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeGreaterThan(Date value) {
            addCriterion("issued_datetime >", value, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("issued_datetime >=", value, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeLessThan(Date value) {
            addCriterion("issued_datetime <", value, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("issued_datetime <=", value, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeIn(List<Date> values) {
            addCriterion("issued_datetime in", values, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeNotIn(List<Date> values) {
            addCriterion("issued_datetime not in", values, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeBetween(Date value1, Date value2) {
            addCriterion("issued_datetime between", value1, value2, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andIssuedDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("issued_datetime not between", value1, value2, "issuedDatetime");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNull() {
            addCriterion("coin_network_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNotNull() {
            addCriterion("coin_network_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeEqualTo(String value) {
            addCriterion("coin_network_type =", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotEqualTo(String value) {
            addCriterion("coin_network_type <>", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThan(String value) {
            addCriterion("coin_network_type >", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_network_type >=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThan(String value) {
            addCriterion("coin_network_type <", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_network_type <=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLike(String value) {
            addCriterion("coin_network_type like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotLike(String value) {
            addCriterion("coin_network_type not like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIn(List<String> values) {
            addCriterion("coin_network_type in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotIn(List<String> values) {
            addCriterion("coin_network_type not in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeBetween(String value1, String value2) {
            addCriterion("coin_network_type between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotBetween(String value1, String value2) {
            addCriterion("coin_network_type not between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNull() {
            addCriterion("coin_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNotNull() {
            addCriterion("coin_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeEqualTo(String value) {
            addCriterion("coin_type =", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotEqualTo(String value) {
            addCriterion("coin_type <>", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThan(String value) {
            addCriterion("coin_type >", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_type >=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThan(String value) {
            addCriterion("coin_type <", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_type <=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLike(String value) {
            addCriterion("coin_type like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotLike(String value) {
            addCriterion("coin_type not like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIn(List<String> values) {
            addCriterion("coin_type in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotIn(List<String> values) {
            addCriterion("coin_type not in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeBetween(String value1, String value2) {
            addCriterion("coin_type between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotBetween(String value1, String value2) {
            addCriterion("coin_type not between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinImageIsNull() {
            addCriterion("coin_image is null");
            return (Criteria) this;
        }

        public Criteria andCoinImageIsNotNull() {
            addCriterion("coin_image is not null");
            return (Criteria) this;
        }

        public Criteria andCoinImageEqualTo(String value) {
            addCriterion("coin_image =", value, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageNotEqualTo(String value) {
            addCriterion("coin_image <>", value, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageGreaterThan(String value) {
            addCriterion("coin_image >", value, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageGreaterThanOrEqualTo(String value) {
            addCriterion("coin_image >=", value, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageLessThan(String value) {
            addCriterion("coin_image <", value, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageLessThanOrEqualTo(String value) {
            addCriterion("coin_image <=", value, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageLike(String value) {
            addCriterion("coin_image like", value, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageNotLike(String value) {
            addCriterion("coin_image not like", value, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageIn(List<String> values) {
            addCriterion("coin_image in", values, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageNotIn(List<String> values) {
            addCriterion("coin_image not in", values, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageBetween(String value1, String value2) {
            addCriterion("coin_image between", value1, value2, "coinImage");
            return (Criteria) this;
        }

        public Criteria andCoinImageNotBetween(String value1, String value2) {
            addCriterion("coin_image not between", value1, value2, "coinImage");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyIsNull() {
            addCriterion("market_price_cny is null");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyIsNotNull() {
            addCriterion("market_price_cny is not null");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyEqualTo(BigDecimal value) {
            addCriterion("market_price_cny =", value, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyNotEqualTo(BigDecimal value) {
            addCriterion("market_price_cny <>", value, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyGreaterThan(BigDecimal value) {
            addCriterion("market_price_cny >", value, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("market_price_cny >=", value, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyLessThan(BigDecimal value) {
            addCriterion("market_price_cny <", value, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("market_price_cny <=", value, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyIn(List<BigDecimal> values) {
            addCriterion("market_price_cny in", values, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyNotIn(List<BigDecimal> values) {
            addCriterion("market_price_cny not in", values, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("market_price_cny between", value1, value2, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andMarketPriceCnyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("market_price_cny not between", value1, value2, "marketPriceCny");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeIsNull() {
            addCriterion("sys_trading_fee_type is null");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeIsNotNull() {
            addCriterion("sys_trading_fee_type is not null");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeEqualTo(Byte value) {
            addCriterion("sys_trading_fee_type =", value, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeNotEqualTo(Byte value) {
            addCriterion("sys_trading_fee_type <>", value, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeGreaterThan(Byte value) {
            addCriterion("sys_trading_fee_type >", value, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("sys_trading_fee_type >=", value, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeLessThan(Byte value) {
            addCriterion("sys_trading_fee_type <", value, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeLessThanOrEqualTo(Byte value) {
            addCriterion("sys_trading_fee_type <=", value, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeIn(List<Byte> values) {
            addCriterion("sys_trading_fee_type in", values, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeNotIn(List<Byte> values) {
            addCriterion("sys_trading_fee_type not in", values, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeBetween(Byte value1, Byte value2) {
            addCriterion("sys_trading_fee_type between", value1, value2, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("sys_trading_fee_type not between", value1, value2, "sysTradingFeeType");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeIsNull() {
            addCriterion("sys_trading_fee is null");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeIsNotNull() {
            addCriterion("sys_trading_fee is not null");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeEqualTo(BigDecimal value) {
            addCriterion("sys_trading_fee =", value, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeNotEqualTo(BigDecimal value) {
            addCriterion("sys_trading_fee <>", value, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeGreaterThan(BigDecimal value) {
            addCriterion("sys_trading_fee >", value, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("sys_trading_fee >=", value, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeLessThan(BigDecimal value) {
            addCriterion("sys_trading_fee <", value, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("sys_trading_fee <=", value, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeIn(List<BigDecimal> values) {
            addCriterion("sys_trading_fee in", values, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeNotIn(List<BigDecimal> values) {
            addCriterion("sys_trading_fee not in", values, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sys_trading_fee between", value1, value2, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andSysTradingFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sys_trading_fee not between", value1, value2, "sysTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeIsNull() {
            addCriterion("miner_trading_fee is null");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeIsNotNull() {
            addCriterion("miner_trading_fee is not null");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeEqualTo(BigDecimal value) {
            addCriterion("miner_trading_fee =", value, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeNotEqualTo(BigDecimal value) {
            addCriterion("miner_trading_fee <>", value, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeGreaterThan(BigDecimal value) {
            addCriterion("miner_trading_fee >", value, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("miner_trading_fee >=", value, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeLessThan(BigDecimal value) {
            addCriterion("miner_trading_fee <", value, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("miner_trading_fee <=", value, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeIn(List<BigDecimal> values) {
            addCriterion("miner_trading_fee in", values, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeNotIn(List<BigDecimal> values) {
            addCriterion("miner_trading_fee not in", values, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("miner_trading_fee between", value1, value2, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andMinerTradingFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("miner_trading_fee not between", value1, value2, "minerTradingFee");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberIsNull() {
            addCriterion("lowest_extract_number is null");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberIsNotNull() {
            addCriterion("lowest_extract_number is not null");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberEqualTo(BigDecimal value) {
            addCriterion("lowest_extract_number =", value, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberNotEqualTo(BigDecimal value) {
            addCriterion("lowest_extract_number <>", value, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberGreaterThan(BigDecimal value) {
            addCriterion("lowest_extract_number >", value, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("lowest_extract_number >=", value, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberLessThan(BigDecimal value) {
            addCriterion("lowest_extract_number <", value, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberLessThanOrEqualTo(BigDecimal value) {
            addCriterion("lowest_extract_number <=", value, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberIn(List<BigDecimal> values) {
            addCriterion("lowest_extract_number in", values, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberNotIn(List<BigDecimal> values) {
            addCriterion("lowest_extract_number not in", values, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("lowest_extract_number between", value1, value2, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andLowestExtractNumberNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("lowest_extract_number not between", value1, value2, "lowestExtractNumber");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinIsNull() {
            addCriterion("is_auto_gather_coin is null");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinIsNotNull() {
            addCriterion("is_auto_gather_coin is not null");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinEqualTo(Byte value) {
            addCriterion("is_auto_gather_coin =", value, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinNotEqualTo(Byte value) {
            addCriterion("is_auto_gather_coin <>", value, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinGreaterThan(Byte value) {
            addCriterion("is_auto_gather_coin >", value, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_auto_gather_coin >=", value, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinLessThan(Byte value) {
            addCriterion("is_auto_gather_coin <", value, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinLessThanOrEqualTo(Byte value) {
            addCriterion("is_auto_gather_coin <=", value, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinIn(List<Byte> values) {
            addCriterion("is_auto_gather_coin in", values, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinNotIn(List<Byte> values) {
            addCriterion("is_auto_gather_coin not in", values, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinBetween(Byte value1, Byte value2) {
            addCriterion("is_auto_gather_coin between", value1, value2, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andIsAutoGatherCoinNotBetween(Byte value1, Byte value2) {
            addCriterion("is_auto_gather_coin not between", value1, value2, "isAutoGatherCoin");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountIsNull() {
            addCriterion("upper_limit_amount is null");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountIsNotNull() {
            addCriterion("upper_limit_amount is not null");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountEqualTo(BigDecimal value) {
            addCriterion("upper_limit_amount =", value, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountNotEqualTo(BigDecimal value) {
            addCriterion("upper_limit_amount <>", value, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountGreaterThan(BigDecimal value) {
            addCriterion("upper_limit_amount >", value, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("upper_limit_amount >=", value, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountLessThan(BigDecimal value) {
            addCriterion("upper_limit_amount <", value, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("upper_limit_amount <=", value, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountIn(List<BigDecimal> values) {
            addCriterion("upper_limit_amount in", values, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountNotIn(List<BigDecimal> values) {
            addCriterion("upper_limit_amount not in", values, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("upper_limit_amount between", value1, value2, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andUpperLimitAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("upper_limit_amount not between", value1, value2, "upperLimitAmount");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctIsNull() {
            addCriterion("wallet_main_acct is null");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctIsNotNull() {
            addCriterion("wallet_main_acct is not null");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctEqualTo(String value) {
            addCriterion("wallet_main_acct =", value, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctNotEqualTo(String value) {
            addCriterion("wallet_main_acct <>", value, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctGreaterThan(String value) {
            addCriterion("wallet_main_acct >", value, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctGreaterThanOrEqualTo(String value) {
            addCriterion("wallet_main_acct >=", value, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctLessThan(String value) {
            addCriterion("wallet_main_acct <", value, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctLessThanOrEqualTo(String value) {
            addCriterion("wallet_main_acct <=", value, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctLike(String value) {
            addCriterion("wallet_main_acct like", value, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctNotLike(String value) {
            addCriterion("wallet_main_acct not like", value, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctIn(List<String> values) {
            addCriterion("wallet_main_acct in", values, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctNotIn(List<String> values) {
            addCriterion("wallet_main_acct not in", values, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctBetween(String value1, String value2) {
            addCriterion("wallet_main_acct between", value1, value2, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletMainAcctNotBetween(String value1, String value2) {
            addCriterion("wallet_main_acct not between", value1, value2, "walletMainAcct");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressIsNull() {
            addCriterion("wallet_extract_address is null");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressIsNotNull() {
            addCriterion("wallet_extract_address is not null");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressEqualTo(String value) {
            addCriterion("wallet_extract_address =", value, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressNotEqualTo(String value) {
            addCriterion("wallet_extract_address <>", value, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressGreaterThan(String value) {
            addCriterion("wallet_extract_address >", value, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressGreaterThanOrEqualTo(String value) {
            addCriterion("wallet_extract_address >=", value, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressLessThan(String value) {
            addCriterion("wallet_extract_address <", value, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressLessThanOrEqualTo(String value) {
            addCriterion("wallet_extract_address <=", value, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressLike(String value) {
            addCriterion("wallet_extract_address like", value, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressNotLike(String value) {
            addCriterion("wallet_extract_address not like", value, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressIn(List<String> values) {
            addCriterion("wallet_extract_address in", values, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressNotIn(List<String> values) {
            addCriterion("wallet_extract_address not in", values, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressBetween(String value1, String value2) {
            addCriterion("wallet_extract_address between", value1, value2, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andWalletExtractAddressNotBetween(String value1, String value2) {
            addCriterion("wallet_extract_address not between", value1, value2, "walletExtractAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressIsNull() {
            addCriterion("gather_main_address is null");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressIsNotNull() {
            addCriterion("gather_main_address is not null");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressEqualTo(String value) {
            addCriterion("gather_main_address =", value, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressNotEqualTo(String value) {
            addCriterion("gather_main_address <>", value, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressGreaterThan(String value) {
            addCriterion("gather_main_address >", value, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressGreaterThanOrEqualTo(String value) {
            addCriterion("gather_main_address >=", value, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressLessThan(String value) {
            addCriterion("gather_main_address <", value, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressLessThanOrEqualTo(String value) {
            addCriterion("gather_main_address <=", value, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressLike(String value) {
            addCriterion("gather_main_address like", value, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressNotLike(String value) {
            addCriterion("gather_main_address not like", value, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressIn(List<String> values) {
            addCriterion("gather_main_address in", values, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressNotIn(List<String> values) {
            addCriterion("gather_main_address not in", values, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressBetween(String value1, String value2) {
            addCriterion("gather_main_address between", value1, value2, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andGatherMainAddressNotBetween(String value1, String value2) {
            addCriterion("gather_main_address not between", value1, value2, "gatherMainAddress");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositIsNull() {
            addCriterion("is_close_deposit is null");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositIsNotNull() {
            addCriterion("is_close_deposit is not null");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositEqualTo(Boolean value) {
            addCriterion("is_close_deposit =", value, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositNotEqualTo(Boolean value) {
            addCriterion("is_close_deposit <>", value, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositGreaterThan(Boolean value) {
            addCriterion("is_close_deposit >", value, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_close_deposit >=", value, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositLessThan(Boolean value) {
            addCriterion("is_close_deposit <", value, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositLessThanOrEqualTo(Boolean value) {
            addCriterion("is_close_deposit <=", value, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositIn(List<Boolean> values) {
            addCriterion("is_close_deposit in", values, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositNotIn(List<Boolean> values) {
            addCriterion("is_close_deposit not in", values, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositBetween(Boolean value1, Boolean value2) {
            addCriterion("is_close_deposit between", value1, value2, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseDepositNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_close_deposit not between", value1, value2, "isCloseDeposit");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawIsNull() {
            addCriterion("is_close_withdraw is null");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawIsNotNull() {
            addCriterion("is_close_withdraw is not null");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawEqualTo(Boolean value) {
            addCriterion("is_close_withdraw =", value, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawNotEqualTo(Boolean value) {
            addCriterion("is_close_withdraw <>", value, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawGreaterThan(Boolean value) {
            addCriterion("is_close_withdraw >", value, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_close_withdraw >=", value, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawLessThan(Boolean value) {
            addCriterion("is_close_withdraw <", value, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawLessThanOrEqualTo(Boolean value) {
            addCriterion("is_close_withdraw <=", value, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawIn(List<Boolean> values) {
            addCriterion("is_close_withdraw in", values, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawNotIn(List<Boolean> values) {
            addCriterion("is_close_withdraw not in", values, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawBetween(Boolean value1, Boolean value2) {
            addCriterion("is_close_withdraw between", value1, value2, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsCloseWithdrawNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_close_withdraw not between", value1, value2, "isCloseWithdraw");
            return (Criteria) this;
        }

        public Criteria andIntroductionIsNull() {
            addCriterion("introduction is null");
            return (Criteria) this;
        }

        public Criteria andIntroductionIsNotNull() {
            addCriterion("introduction is not null");
            return (Criteria) this;
        }

        public Criteria andIntroductionEqualTo(String value) {
            addCriterion("introduction =", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotEqualTo(String value) {
            addCriterion("introduction <>", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionGreaterThan(String value) {
            addCriterion("introduction >", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionGreaterThanOrEqualTo(String value) {
            addCriterion("introduction >=", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLessThan(String value) {
            addCriterion("introduction <", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLessThanOrEqualTo(String value) {
            addCriterion("introduction <=", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLike(String value) {
            addCriterion("introduction like", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotLike(String value) {
            addCriterion("introduction not like", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionIn(List<String> values) {
            addCriterion("introduction in", values, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotIn(List<String> values) {
            addCriterion("introduction not in", values, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionBetween(String value1, String value2) {
            addCriterion("introduction between", value1, value2, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotBetween(String value1, String value2) {
            addCriterion("introduction not between", value1, value2, "introduction");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}