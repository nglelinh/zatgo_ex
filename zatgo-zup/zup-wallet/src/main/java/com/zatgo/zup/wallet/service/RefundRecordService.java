package com.zatgo.zup.wallet.service;

import java.math.BigDecimal;

public interface RefundRecordService {

	BigDecimal getOrderRefundMoney(String orderId);


}
