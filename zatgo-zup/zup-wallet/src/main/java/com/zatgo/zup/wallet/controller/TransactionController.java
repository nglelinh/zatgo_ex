package com.zatgo.zup.wallet.controller;

import com.zatgo.zup.wallet.model.LocalTransaction;
import com.zatgo.zup.wallet.model.SendParams;
import com.zatgo.zup.wallet.model.TxRecord;
import com.zatgo.zup.wallet.service.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(value = "/transaction",description = "交易相关")
@RestController
@RequestMapping(value = "/transaction")
public class TransactionController extends BaseController {
	@Autowired
	private TransactionService transactionService;
	@ApiOperation(value = "获取账户交易列表")
	@RequestMapping(value = "/list/{accountId}",method = RequestMethod.GET )
	@ResponseBody
	List<TxRecord> listAccountTransactions(@PathVariable("accountId") String accountId){
		return transactionService.listAccountTransactions(accountId);
	}
	@ApiOperation(value = "根据交易ID获取交易详情")
	@RequestMapping(value = "/{transactionId}/{accountId}",method = RequestMethod.GET )
	@ResponseBody
	TxRecord getTransactionByTransactionId(@PathVariable("accountId") String accountId, @PathVariable("transactionId") String transactionId){
		return transactionService.getTransactionByTransactionId(accountId,transactionId);
	}
	@ApiOperation(value = "发起交易")
	@RequestMapping(value = "/send",method = RequestMethod.POST )
	@ResponseBody
	LocalTransaction sendMoney(SendParams params, String accountId){
		return transactionService.sendMoney(params,accountId);
	}
	@ApiOperation(value = "账户间转账")
	@RequestMapping(value = "/transfer",method = RequestMethod.POST )
	@ResponseBody
	LocalTransaction transferMoneyBetweenAccounts(SendParams params, String accountId){
		return transactionService.transferMoneyBetweenAccounts(params,accountId);
	}
	@ApiOperation(value = "请求对方支付")
	@RequestMapping(value = "/request",method = RequestMethod.POST )
	@ResponseBody
	LocalTransaction requestMoney(SendParams params, String accountId){
		return transactionService.requestMoney(params,accountId);
	}
	@ApiOperation(value = "完成支付请求")
	@RequestMapping(value = "/complete",method = RequestMethod.POST )
	@ResponseBody
	void completeRequestMoney(String accountId,String transactionId){
		transactionService.completeRequestMoney(accountId,transactionId);
	}
	@ApiOperation(value = "获取账户列表")
	@RequestMapping(value = "/cancel",method = RequestMethod.DELETE )
	@ResponseBody
	void cancelRequestMoney(String accountId,String transactionId){
		transactionService.cancelRequestMoney(accountId,transactionId);
	}









}
