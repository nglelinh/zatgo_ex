package com.zatgo.zup.wallet.blockchain.utils;

import com.neemre.btcdcli4j.core.BitcoindException;
import com.neemre.btcdcli4j.core.CommunicationException;
import com.neemre.btcdcli4j.core.client.BtcdClient;
import com.neemre.btcdcli4j.core.client.BtcdClientImpl;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BtcClient {
	private volatile static BtcdClient client;
	public BtcClient() {
	}

	public static BtcdClient getClient(){
		if(client==null){
			synchronized (BtcdClient.class){
				if(client==null){
					PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
					CloseableHttpClient httpProvider = HttpClients.custom().setConnectionManager(cm)
							.build();
					Properties nodeConfig = new Properties();
					try {
						InputStream is = new BufferedInputStream(new FileInputStream("E:\\gitOSCHINA\\zatgo-zup\\zup-payment-web\\src\\main\\resources\\nodeconfig.properties"));
						nodeConfig.load(is);
						is.close();
						client = new BtcdClientImpl(httpProvider, nodeConfig);
					} catch (IOException e) {
						e.printStackTrace();
					} catch (BitcoindException e) {
						e.printStackTrace();
					} catch (CommunicationException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return client;
	}


}
