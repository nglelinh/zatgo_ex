package com.zatgo.zup.wallet.service;

import java.math.BigDecimal;
import java.util.List;

import com.zatgo.zup.common.model.DepositInsertParams;
import com.zatgo.zup.common.model.DepositRecordData;
import com.zatgo.zup.common.model.MiningDepositInsertParams;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.wallet.DepositRecordByCloudUserParams;
import com.zatgo.zup.wallet.entity.DepositRecord;

public interface DepositService {

	String insertDepositRecored(DepositInsertParams params);

	List<DepositRecord> selectUnConfirmed();

	String getAddressForDeposit(String accountId,Byte type);

	List<DepositRecord> listRecordByUserId(String userId, String cloudUserId);

	void deposit(String depositId);

	DepositRecord selectRecord(String type,String address,String txId);

	DepositRecord saveRecord(String symbol, String addrTo, String txid, BigDecimal amount, Integer confirm, Integer mining);

	void insertMiningDepositRecored(MiningDepositInsertParams params);
	
	PageInfo<DepositRecordData> selectDepositRecordByCloudUser(String cloudUserId,DepositRecordByCloudUserParams params);
}
