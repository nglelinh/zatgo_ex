package com.zatgo.zup.wallet.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.RequestData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.wallet.entity.CoinPresentRecord;
import com.zatgo.zup.wallet.service.PresentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/wallet/present")
@Api(value = "/wallet/present",description = "虚拟币自动赠送接口",produces = MediaType.APPLICATION_JSON_VALUE)
public class PresentController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(PresentController.class);
	
	@Autowired
	private PresentService presentService;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@ResponseBody
	@ApiOperation(value = "虚拟币指定用户赠送接口", notes = "虚拟币指定用户赠送接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/{loginUserName}/{cloudUserId}",method = RequestMethod.POST)
	public ResponseData<Object> present(@PathVariable("loginUserName") String loginUserName,
										@PathVariable("cloudUserId") String cloudUserId,
										@RequestBody RequestData<String> verifyCode){
		String presentUserLock = RedisKeyConstants.COIN_PRESENT + loginUserName;
		redisLockUtils.lock(presentUserLock);
		try {
			presentService.receive(loginUserName, verifyCode.getData(), cloudUserId);
		}catch(Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}finally {
			redisLockUtils.releaseLock(presentUserLock);
		}
		
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ResponseBody
	@ApiOperation(value = "虚拟币自动赠送接口", notes = "虚拟币自动赠送接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/{cloudUserId}",method = RequestMethod.POST)
	public ResponseData<Object> presentAll(@PathVariable("cloudUserId") String cloudUserId){
		List<CoinPresentRecord> list = presentService.findUnPresentList(cloudUserId);
		for(CoinPresentRecord record:list) {
			String presentUserLock = RedisKeyConstants.COIN_PRESENT + record.getReceiveUserName();
			redisLockUtils.lock(presentUserLock);
			try {
				presentService.receive(record.getReceiveUserName(), record.getReceiveVerifyCode(), cloudUserId);
			}catch(Exception e) {
				logger.error("",e);
				throw new BusinessException();
			}finally {
				redisLockUtils.releaseLock(presentUserLock);
			}
		}

		
		return BusinessResponseFactory.createSuccess(null);
	}
}
