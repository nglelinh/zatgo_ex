package com.zatgo.zup.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DepositRecordData implements Serializable {

	private String depositId;

	private String accountId;

	/**
	 * 多个地址使用逗号隔开
	 */
	private String fromAddresss;

	private String toAddress;

	private Date depositDate;

	private Date depositSuccessDate;

	private String depositNumber;

	/**
	 * 0：充值中
	 1：充值成功
	 2：充值失败
	 */
	private String depositStatus;

	private String txHash;

	private String blockHash;

	private Integer blockNumber;

	private String coinType;

	private String coinNetworkType;


	public String getDepositId() {
		return depositId;
	}

	public void setDepositId(String depositId) {
		this.depositId = depositId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getFromAddresss() {
		return fromAddresss;
	}

	public void setFromAddresss(String fromAddresss) {
		this.fromAddresss = fromAddresss;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public Date getDepositSuccessDate() {
		return depositSuccessDate;
	}

	public void setDepositSuccessDate(Date depositSuccessDate) {
		this.depositSuccessDate = depositSuccessDate;
	}

	public String getDepositNumber() {
		return depositNumber;
	}

	public void setDepositNumber(String depositNumber) {
		this.depositNumber = depositNumber;
	}

	public String getDepositStatus() {
		return depositStatus;
	}

	public void setDepositStatus(String depositStatus) {
		this.depositStatus = depositStatus;
	}

	public String getTxHash() {
		return txHash;
	}

	public void setTxHash(String txHash) {
		this.txHash = txHash;
	}

	public String getBlockHash() {
		return blockHash;
	}

	public void setBlockHash(String blockHash) {
		this.blockHash = blockHash;
	}

	public Integer getBlockNumber() {
		return blockNumber;
	}

	public void setBlockNumber(Integer blockNumber) {
		this.blockNumber = blockNumber;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}
}
