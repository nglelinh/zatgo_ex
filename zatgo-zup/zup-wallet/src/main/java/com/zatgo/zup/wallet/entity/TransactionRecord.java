package com.zatgo.zup.wallet.entity;

import java.io.Serializable;

/**
 * @author 
 */
public class TransactionRecord implements Serializable {
    private Integer id;

    private String fromAddress;

    private String toAddress;

    private Double fee;

    private Double amount;

    private String blockhash;

    /**
     * 0交易确认中1已确认
     */
    private Byte status;

    private String transactionhash;

    private Integer confirrmations;

    /**
     * 交易类型send为1receive0
     */
    private String type;

    private Byte isWebpay;

    private String userId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getBlockhash() {
        return blockhash;
    }

    public void setBlockhash(String blockhash) {
        this.blockhash = blockhash;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getTransactionhash() {
        return transactionhash;
    }

    public void setTransactionhash(String transactionhash) {
        this.transactionhash = transactionhash;
    }

    public Integer getConfirrmations() {
        return confirrmations;
    }

    public void setConfirrmations(Integer confirrmations) {
        this.confirrmations = confirrmations;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Byte getIsWebpay() {
        return isWebpay;
    }

    public void setIsWebpay(Byte isWebpay) {
        this.isWebpay = isWebpay;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TransactionRecord other = (TransactionRecord) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getFromAddress() == null ? other.getFromAddress() == null : this.getFromAddress().equals(other.getFromAddress()))
            && (this.getToAddress() == null ? other.getToAddress() == null : this.getToAddress().equals(other.getToAddress()))
            && (this.getFee() == null ? other.getFee() == null : this.getFee().equals(other.getFee()))
            && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
            && (this.getBlockhash() == null ? other.getBlockhash() == null : this.getBlockhash().equals(other.getBlockhash()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getTransactionhash() == null ? other.getTransactionhash() == null : this.getTransactionhash().equals(other.getTransactionhash()))
            && (this.getConfirrmations() == null ? other.getConfirrmations() == null : this.getConfirrmations().equals(other.getConfirrmations()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getIsWebpay() == null ? other.getIsWebpay() == null : this.getIsWebpay().equals(other.getIsWebpay()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getFromAddress() == null) ? 0 : getFromAddress().hashCode());
        result = prime * result + ((getToAddress() == null) ? 0 : getToAddress().hashCode());
        result = prime * result + ((getFee() == null) ? 0 : getFee().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getBlockhash() == null) ? 0 : getBlockhash().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getTransactionhash() == null) ? 0 : getTransactionhash().hashCode());
        result = prime * result + ((getConfirrmations() == null) ? 0 : getConfirrmations().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getIsWebpay() == null) ? 0 : getIsWebpay().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", fromAddress=").append(fromAddress);
        sb.append(", toAddress=").append(toAddress);
        sb.append(", fee=").append(fee);
        sb.append(", amount=").append(amount);
        sb.append(", blockhash=").append(blockhash);
        sb.append(", status=").append(status);
        sb.append(", transactionhash=").append(transactionhash);
        sb.append(", confirrmations=").append(confirrmations);
        sb.append(", type=").append(type);
        sb.append(", isWebpay=").append(isWebpay);
        sb.append(", userId=").append(userId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}