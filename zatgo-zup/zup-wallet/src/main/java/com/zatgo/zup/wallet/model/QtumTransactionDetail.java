package com.zatgo.zup.wallet.model;

import java.io.Serializable;
import java.util.List;

public class QtumTransactionDetail implements Serializable {

	private String txid;
	
	private String hash;
	
	private String size;
	
	private String vsize;
	
	private String version;
	private String locktime;
	private List<QtumVin> vin;
	private List<QtumVout> vout;

	public String getTxid() {
		return txid;
	}

	public void setTxid(String txid) {
		this.txid = txid;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getVsize() {
		return vsize;
	}

	public void setVsize(String vsize) {
		this.vsize = vsize;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLocktime() {
		return locktime;
	}

	public void setLocktime(String locktime) {
		this.locktime = locktime;
	}

	public List<QtumVin> getVin() {
		return vin;
	}

	public void setVin(List<QtumVin> vin) {
		this.vin = vin;
	}

	public List<QtumVout> getVout() {
		return vout;
	}

	public void setVout(List<QtumVout> vout) {
		this.vout = vout;
	}
}
