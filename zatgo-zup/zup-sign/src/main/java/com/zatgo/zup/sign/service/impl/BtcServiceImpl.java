package com.zatgo.zup.sign.service.impl;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.ScriptException;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionOutPoint;
import org.bitcoinj.core.UTXO;
import org.bitcoinj.core.Utils;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.script.Script;
import org.bitcoinj.wallet.UnreadableWalletException;
import org.bitcoinj.wallet.Wallet;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.SignBTCTransactionParams;
import com.zatgo.zup.common.model.UnSpentBTCData;
import com.zatgo.zup.common.utils.StringUtil;
import com.zatgo.zup.sign.enumtype.SignEnum;
import com.zatgo.zup.sign.service.BtcService;
import com.zatgo.zup.sign.utils.MnemonicUtil;

import io.github.novacrypto.base58.Base58;
import io.github.novacrypto.bip32.ExtendedPrivateKey;
import io.github.novacrypto.hashing.Sha256;

@Component
public class BtcServiceImpl implements BtcService {
	
	private static final Logger logger = LoggerFactory.getLogger(BtcServiceImpl.class);

	private  static final String BTC_DATA_BASE = "data" + File.separator + "btc";
	
	private  static final String BTC_WALLET_PATH = BTC_DATA_BASE + File.separator + "wallet";
	
	private static final ReentrantLock lock = new ReentrantLock();
	
	@Override
	public List<String> getNewAddresses(String password, Integer num) {
		//get network
		NetworkParameters networkParameters = null;
        if (true) {
        	networkParameters = MainNetParams.get();
        }else {
        	networkParameters = TestNet3Params.get();
        }
        
		lock.lock();
		try {
			List<String> addresses = new ArrayList<>();
			
			//get mnemonic
			String mnemonic = MnemonicUtil.generateMnemonic();
			
			//exist private key number
			Integer existPrivateKeyNum = 0;
			File path = new File(BTC_WALLET_PATH);
			if(!path.exists()){
				path.mkdirs();
			}
			File walletFile = new File(BTC_WALLET_PATH + File.separator + "wallet.dat");
			Wallet wallet = null;
			if(walletFile.exists()) {
				try {
					wallet = Wallet.loadFromFile(walletFile);
				} catch (UnreadableWalletException e) {
					logger.error("",e);
					throw new BusinessException();
				}
				List<ECKey> list = wallet.getImportedKeys();
				existPrivateKeyNum = list.size();
			}else {
				wallet = new Wallet(networkParameters);
				wallet.encrypt(password);
			}
			
			//get private key
			List<ExtendedPrivateKey> childPrivateKeys = MnemonicUtil.getExtendedPrivateKey(mnemonic, SignEnum.SignCoinType.BTC, num,existPrivateKeyNum);
			
			List<ECKey> keys = new ArrayList<>();
			
			//BINCOINJ HD
//			String wordsList = "one misery space industry hen mistake typical prison plunge yellow disagree arm";
//			DeterministicSeed deterministicSeed;
//			try {
//				deterministicSeed = new DeterministicSeed(wordsList, null, "", 0L);
//				DeterministicKeyChain deterministicKeyChain = DeterministicKeyChain.builder().seed(deterministicSeed).build();
//				DeterministicKey deterministicKey = deterministicKeyChain.getKeyByPath(HDUtils.parsePath("44H / 1H / 0H / 0 / 2"), true);
//				BigInteger privKey = deterministicKey.getPrivKey();
//			} catch (UnreadableWalletException e1) {
//			}
			
			for(ExtendedPrivateKey childPrivateKey:childPrivateKeys) {
				String[] addressInfo = getBitcoinAddress(childPrivateKey);
		        DumpedPrivateKey dumpedPrivateKey = DumpedPrivateKey.fromBase58(networkParameters, addressInfo[0]);
		        ECKey ecKey = dumpedPrivateKey.getKey();

		        logger.debug("---------eckey bitcoin privateKey_hex:" + ecKey.getPrivateKeyAsHex());
		        logger.debug("---------eckey bitcoin privateKey_Base58:" + Base58.base58Encode(StringUtil.hexStringToByteArray(ecKey.getPrivateKeyAsHex())));
		        logger.debug("---------eckey bitcoin publicKey:" + ecKey.getPublicKeyAsHex());
		        logger.debug("---------eckey bitcoin address:" + ecKey.toAddress(wallet.getNetworkParameters()).toString());
		        
		        keys.add(ecKey);
		        addresses.add(addressInfo[2]);
			}
			
			wallet.importKeysAndEncrypt(keys, password);
			
			try {
				wallet.saveToFile(walletFile);
			} catch (Exception e) {
				logger.error("",e);
				throw new BusinessException();
			}
			
			return addresses;
		}finally {
			lock.unlock();
		}
		
	}

	@Override
	public String sign(SignBTCTransactionParams params) {
		
		NetworkParameters networkParameters = null;
        networkParameters = MainNetParams.get();

        Transaction transaction = new Transaction(networkParameters);

        long totalMoney = 0;
        List<UTXO> utxos = new ArrayList<>();
        List<ECKey> ecKeys = new ArrayList<>();
        HashMap<String, ECKey> ecKeyMap = new HashMap<>();
        //遍历未花费列表，组装合适的item
        for (UnSpentBTCData us : params.getUnSpentBtcs()) {
        	if(totalMoney >= params.getAmount() + params.getFee()) {
        		break;
        	}

            UTXO utxo;
			Script script = null;
			try {
				script = new Script(Hex.decodeHex(us.getScriptPubKey()));
				utxo = new UTXO(Sha256Hash.wrap(us.getTxId()), us.getVout(), Coin.valueOf(us.getSatoshis()),
				        us.getHeight(), false, script);
				
				utxos.add(utxo);
			} catch (Exception e) {
				logger.error("",e);
				throw new BusinessException();
			}
            
            Address fromAddress;
			try {
//				fromAddress = Address.fromP2SHScript(networkParameters, new Script(Hex.decodeHex(us.getScriptPubKey())));
				fromAddress = script.getToAddress(networkParameters);
			} catch (Exception e) {
				logger.error("",e);
				throw new BusinessException();
			}
        	String addressBase58 = fromAddress.toBase58();
        	ECKey ecKey = ecKeyMap.get(addressBase58);
        	if(ecKey == null) {
        		ecKey = getECKeyByAddressAndPass(addressBase58,params.getPassword());
        		if(ecKey == null) {
        			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"address[" + addressBase58 +"] is not private key");
        		}
        		ecKeyMap.put(addressBase58, ecKey);
        	}
        	ecKeys.add(ecKey);
        	
            totalMoney += us.getSatoshis();
        }

        transaction.addOutput(Coin.valueOf(params.getAmount()), Address.fromBase58(networkParameters, params.getToAddress()));

        //消费列表总金额 - 已经转账的金额 - 手续费 就等于需要返回给自己的金额了
        long balance = totalMoney - params.getAmount() - params.getFee();
        //输出-转给自己
        if (balance > 0) {
            transaction.addOutput(Coin.valueOf(balance), Address.fromBase58(networkParameters, params.getChangeAddress()));
        }
        //输入未消费列表项
        int index = 0;
        for (UTXO utxo : utxos) {
//        	String privateBase58 = Base58.base58Encode(StringUtil.hexStringToByteArray(ecKeys.get(index).getPrivateKeyAsHex()));
//        	DumpedPrivateKey dumpedPrivateKey = DumpedPrivateKey.fromBase58(networkParameters, privateBase58);
//            ECKey ecKey = dumpedPrivateKey.getKey();
            TransactionOutPoint outPoint = new TransactionOutPoint(networkParameters, utxo.getIndex(), utxo.getHash());
//            transaction.addSignedInput(outPoint, utxo.getScript(), ecKey, Transaction.SigHash.ALL, true);
			transaction.addSignedInput(outPoint, utxo.getScript(), ecKeys.get(index), Transaction.SigHash.ALL, true);
            index++;
        }

        return Hex.encodeHexString(transaction.bitcoinSerialize());
	}
	
	private ECKey getECKeyByAddressAndPass(String address,String password) {
		File walletFile = new File(BTC_WALLET_PATH + File.separator + "wallet.dat");
		if(!walletFile.exists()) {
			logger.error("from address is not exist");
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"from address is not exist");
		}
		
		try {
			Wallet wallet = Wallet.loadFromFile(walletFile);
			
			List<ECKey> list = wallet.getImportedKeys();
			for(ECKey ecKey:list) {

		        if(ecKey.toAddress(wallet.getNetworkParameters()).toString().equals(address)) {
					ecKey = ecKey.decrypt(wallet.getKeyCrypter(),wallet.getKeyCrypter().deriveKey(password));
					logger.debug("---------sign bitcoin privateKey_hex:" + ecKey.getPrivateKeyAsHex());
			        logger.debug("---------sign bitcoin privateKey_Base58:" + Base58.base58Encode(StringUtil.hexStringToByteArray(ecKey.getPrivateKeyAsHex())));
			        logger.debug("---------sign bitcoin publicKey:" + ecKey.getPublicKeyAsHex());
			        logger.debug("---------sign bitcoin address:" + ecKey.toAddress(wallet.getNetworkParameters()).toString());

		        	return ecKey;
		        	
		        }
			}
			
			return null;
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
		
	}


	/**
     * generate bitcoin privatekey, publickey and address.
     *
     * @param childPrivateKey
     */
    private String[] getBitcoinAddress(ExtendedPrivateKey childPrivateKey) {
        // 获取比特币私钥
        String privateKey = childPrivateKey.getPrivateKey();
        // 加80前缀和01后缀
        String rk = "80" + privateKey + "01";
        // 生成校验和
        byte[] checksum = Sha256.sha256(StringUtil.hexStringToByteArray(rk));
        checksum = Sha256.sha256(checksum);
        // 取校验和前4位（32bits）
        String end = String.valueOf(Hex.encodeHex(checksum)).substring(0, 8);
        rk = rk + end;
        // 进行base58编码
        String privateK = Base58.base58Encode(StringUtil.hexStringToByteArray(rk));


        // 获取比特币公钥
        String publicKey = childPrivateKey.neuter().getPublicKey();
        // 对公钥进行一次sha256
        byte[] pk256 = StringUtil.hexStringToByteArray(publicKey);
        pk256 = Sha256.sha256(pk256);
        // 进行ripe160加密（20位）
        RIPEMD160Digest digest = new RIPEMD160Digest();
        digest.update(pk256, 0, pk256.length);
        byte[] ripemd160Bytes = new byte[digest.getDigestSize()];
        digest.doFinal(ripemd160Bytes, 0);
        // 加00前缀（比特币主网）变成21位
        byte[] extendedRipemd160Bytes = StringUtil.hexStringToByteArray("00" + String.valueOf(Hex.encodeHex(ripemd160Bytes)));
        // 计算校验和
        checksum = Sha256.sha256(extendedRipemd160Bytes);
        checksum = Sha256.sha256(checksum);
        // 加校验和前4位，变成25位
        String pk = String.valueOf(Hex.encodeHex(extendedRipemd160Bytes)) + String.valueOf(Hex.encodeHex(checksum)).substring(0, 8);
        // base58加密
        String address = Base58.base58Encode(StringUtil.hexStringToByteArray(pk));

        logger.debug("\\r\\n---------create bitcoin privateKey_hex:" + rk);
        logger.debug("\\r\\n---------create bitcoin privateKey_Base58:" + privateK);
        logger.debug("\\r\\n---------create bitcoin publicKey:" + publicKey);
        logger.debug("\\r\\n---------create bitcoin address:" + address);

        return new String[] {privateK,publicKey,address};
    }
    
    private String generateSegwitAddress(String address) {
        byte[] decoded = Utils.parseAsHexOrBase58(address);
        // We should throw off header byte that is 0 for Bitcoin (Main)
        byte[] pureBytes = new byte[20];
        System.arraycopy(decoded, 1, pureBytes, 0, 20);
        // Than we should prepend the following bytes:
        byte[] scriptSig = new byte[pureBytes.length + 2];
        scriptSig[0] = 0x00;
        scriptSig[1] = 0x14;
        System.arraycopy(pureBytes, 0, scriptSig, 2, pureBytes.length);
        byte[] addressBytes = org.bitcoinj.core.Utils.sha256hash160(scriptSig);
        // Here are the address bytes
        byte[] readyForAddress = new byte[addressBytes.length + 1 + 4];
        // prepending p2sh header:
        readyForAddress[0] = (byte) 5;
        System.arraycopy(addressBytes, 0, readyForAddress, 1, addressBytes.length);
        // But we should also append check sum:
        byte[] checkSum = Sha256Hash.hashTwice(readyForAddress, 0, addressBytes.length + 1);
        System.arraycopy(checkSum, 0, readyForAddress, addressBytes.length + 1, 4);
        // To get the final address:
        String segwitAddress = Base58.base58Encode(readyForAddress);
        return segwitAddress;
    }
    
    public static void main(String[] args) {
    	BtcService btcService = new BtcServiceImpl();
    	List<String> addresses = btcService.getNewAddresses("123456", 2);

    	SignBTCTransactionParams params = new SignBTCTransactionParams();
    	params.setAmount(100000000);
    	params.setChangeAddress(addresses.get(0));
    	params.setFee(1000000);
    	params.setPassword("123456");
    	params.setToAddress(addresses.get(1));

    	UnSpentBTCData unSpentBTCData = new UnSpentBTCData();
    	unSpentBTCData.setHeight(545119);
    	unSpentBTCData.setSatoshis(100000);
    	unSpentBTCData.setScriptPubKey("a914a0df29deb2862db664e7179877a04f215dd562d587");
    	unSpentBTCData.setTxId("2251e851458813d3345f057cdca022dc4d3e1331a58e6d23c0fda2e2e9b8672a");
    	unSpentBTCData.setVout(0L);

    	List<UnSpentBTCData> list = new ArrayList<>();
    	list.add(unSpentBTCData);

    	params.setUnSpentBtcs(list);
    	btcService.sign(params);
    }

}
