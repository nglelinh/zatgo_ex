package com.zatgo.zup.sign.service;

import com.zatgo.zup.common.model.QtumContractSignParams;
import com.zatgo.zup.common.model.SignQtumTransactionParams;

import java.util.List;

/**
 * Created by 46041 on 2018/11/22.
 */
public interface QtumService {



    /**
     * 获取新地址列表
     * @return
     */
    List<String> getNewAddresses(String password, Integer num);

    /**
     * 交易签名
     * @param params
     * @return
     */
    String sign(SignQtumTransactionParams params);

    /**
     * qtum合约签名
     * @param params
     * @return
     */
    String signContract(QtumContractSignParams params);
}
