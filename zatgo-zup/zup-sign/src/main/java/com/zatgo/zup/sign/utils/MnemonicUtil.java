package com.zatgo.zup.sign.utils;

import static io.github.novacrypto.toruntime.CheckedExceptionToRuntime.toRuntime;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.utils.FileUtil;
import com.zatgo.zup.common.utils.StringUtil;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.sign.enumtype.SignEnum.SignCoinType;

import io.github.novacrypto.bip32.ExtendedPrivateKey;
import io.github.novacrypto.bip32.networks.Bitcoin;
import io.github.novacrypto.bip44.AddressIndex;
import io.github.novacrypto.bip44.BIP44;
import io.github.novacrypto.hashing.Sha256;
import io.github.novacrypto.toruntime.CheckedExceptionToRuntime;
import org.web3j.utils.Numeric;

public class MnemonicUtil{
	
	public static final Logger logger = LoggerFactory.getLogger(MnemonicUtil.class);
	
	private static final ReentrantLock lock = new ReentrantLock();
	
	private static final String[] dict =
        {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000",
                "1001", "1010", "1011", "1100", "1101", "1110", "1111"};
	
	private  static final String MNEMONIC_PATH = "data" + File.separator + "mnemonic";

	
	
	/**
	 * 生成助记词
	 * @return
	 */
	public static String generateMnemonic() {
		lock.lock();
		try {
			//如果存在直接返回
			String mnemonic = getExistMnemonic();
			if(!StringUtils.isEmpty(mnemonic)) {
				return mnemonic;
			}
			
			//开始重新生成助记词
			String entropy = UUIDUtils.getUuid(); 
			
			String encodeStr = "";
	        byte[] hash = Sha256.sha256(StringUtil.hexStringToByteArray(entropy));
	        encodeStr = String.valueOf(Hex.encodeHex(hash));
	        logger.debug("entropy_to_Sha256: "+encodeStr);
	        
	        char firstSHA = encodeStr.charAt(0);
	        String new_entropy = entropy + firstSHA;
	        String bin_entropy = "";
	        for (int i = 0; i < new_entropy.length(); i++) {
	            bin_entropy += dict[Integer.parseInt(new_entropy.substring(i, i + 1), 16)];
	        }
	        String[] segments = new String[12];
	        //hardcode
	        for (int i = 0; i <= 11; i++) {
	            segments[i] = bin_entropy.substring(i * 11, (i + 1) * 11);
	        }

	        //请修改文件的绝对路径
	        String fileName = "mnemonic_en";
	        String[] mnemonicEnArray = FileUtil.readTextFile(fileName, "utf-8");
	        mnemonic = "";

	        //generate mnemonic
	        mnemonic += mnemonicEnArray[Integer.valueOf(segments[0], 2)];
	        for (int j = 1; j < segments.length; j++) {
	            mnemonic += " " + (mnemonicEnArray[Integer.valueOf(segments[j], 2)]);
	        }
	        
	        //保存
	        saveMnemonic(mnemonic);
	        return mnemonic;
		}finally {
			lock.unlock();
		}
		
	}

	/**
	 * 根据助记词获取
	 * @param mnemonic 助记词
	 * @param coinType 币种类型
	 * @param addressNum 生成数量
	 * @return
	 */
	public static List<ExtendedPrivateKey> getExtendedPrivateKey(String mnemonic, SignCoinType coinType,Integer addressNum,Integer startNum) {

        // 1. calculate seed from mnemonics , then get master/root key ; Note that the bip39 passphrase we set "" for common
        String seed;
        String salt = "mnemonic";
        try {
			seed = getSeed(mnemonic, salt);
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
        logger.debug("seed: " + seed);
        ExtendedPrivateKey rootKey = ExtendedPrivateKey.fromSeed(StringUtil.hexStringToByteArray(seed), Bitcoin.MAIN_NET);
        logger.debug("rootKey:" + rootKey);
        
        // 2. get child private key deriving from master/root key
        List<ExtendedPrivateKey> childPrivateKeys = new ArrayList<>();
        if(coinType.equals(SignCoinType.ETH)) {
        	for(int i = 0 ; addressNum > i ; i++) {
        		AddressIndex ethAddressIndex = BIP44.m().purpose44().coinType(60).account(0).external().address(i+startNum);
        		ExtendedPrivateKey childPrivateKey = rootKey.derive(ethAddressIndex, AddressIndex.DERIVATION);
        		childPrivateKeys.add(childPrivateKey);
        	}
        }else if(coinType.equals(SignCoinType.BTC)) {
        	for(int i = 0 ; addressNum > i ; i++) {
        		AddressIndex btcAddressIndex = BIP44.m().purpose44().coinType(0).account(0).external().address(i+startNum);
        		ExtendedPrivateKey childPrivateKey = rootKey.derive(btcAddressIndex, AddressIndex.DERIVATION);
        		childPrivateKeys.add(childPrivateKey);
        	}
        }else if (SignCoinType.QTUM.equals(coinType)){
			for(int i = 0 ; addressNum > i ; i++) {
				AddressIndex qtumAddressIndex = BIP44.m().purpose44().coinType(2301).account(0).external().address(i+startNum);
				ExtendedPrivateKey childPrivateKey = rootKey.derive(qtumAddressIndex, AddressIndex.DERIVATION);
				childPrivateKeys.add(childPrivateKey);
			}
		}else {
        	throw new BusinessException();
        }
        
        return childPrivateKeys;
	}
	
	/**
	 * 获取存在的助记词
	 * @return
	 */
	private static String getExistMnemonic() {
		File file = new File(MNEMONIC_PATH);
		if(!file.exists()){
			return null;
		}	
		
		String[] existMnemonic = file.list();
		if(existMnemonic.length > 0) {
			return existMnemonic[0];
		}else {
			return null;
		}
	}
	
	/**
	 * 保存助记词
	 * @param mnemonic
	 * @return
	 */
	private static boolean saveMnemonic(String mnemonic) {
		File file = new File(MNEMONIC_PATH);
		if(!file.exists()){
			file.mkdirs();
		}else if(!file.isDirectory()){
			throw new RuntimeException(MNEMONIC_PATH + " isn't directory");
		}

		String fileName = MNEMONIC_PATH + File.separator + mnemonic;
		try {
			FileUtils.touch(new File(fileName));
			return true;
		} catch (IOException e) {
			logger.error("",e);
			return false;
		}
	}
	
	
	private static String getSeed(String mnemonic, String salt) throws NoSuchAlgorithmException,
	    InvalidKeySpecException {

		char[] chars = Normalizer.normalize(mnemonic, Normalizer.Form.NFKD).toCharArray();
		byte[] salt_ = getUtf8Bytes(salt);
		KeySpec spec = new PBEKeySpec(chars, salt_, 2048, 512);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
		return String.valueOf(Hex.encodeHex(f.generateSecret(spec).getEncoded()));
	}

	private static byte[] getUtf8Bytes(final String str) {
        return toRuntime(new CheckedExceptionToRuntime.Func<byte[]>() {
            @Override
            public byte[] run() throws Exception {
                return str.getBytes("UTF-8");
            }
        });
    }
}
