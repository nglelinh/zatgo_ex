package com.zatgo.zup.sign.enumtype;

import java.util.HashMap;
import java.util.Map;

public class SignEnum {

	public enum SignCoinType {
		BTC("BTC"), ETH("ETH"), QTUM("QTUM"), USDT("USDT");

		String code;

		private static final Map<String, SignCoinType> stringToEnum = new HashMap<String, SignCoinType>();
	    static {
	        for(SignCoinType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static SignCoinType getEnumByType(String type) {
	    	return stringToEnum.get(type.toUpperCase());
	    }

		private SignCoinType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}


	}
}
