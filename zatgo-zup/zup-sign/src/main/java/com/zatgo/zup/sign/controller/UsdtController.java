package com.zatgo.zup.sign.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.CreateAddressRequest;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SignBTCTransactionParams;
import com.zatgo.zup.common.model.SignUsdtTransactionParams;
import com.zatgo.zup.sign.service.BtcService;
import com.zatgo.zup.sign.service.UsdtService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="/sign/usdt", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/sign/usdt")
public class UsdtController extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(UsdtController.class);
	
	@Autowired
	private UsdtService usdtService;
	
	@ApiOperation(value = "获取新地址")
	@RequestMapping(value = "/newaddresses",name="获取新地址", method = RequestMethod.POST)
	@ResponseBody
    public ResponseData<List<String>> getNewAddresses(@RequestBody CreateAddressRequest request){
		List<String> addresses = usdtService.getNewAddresses(request.getPassword(), request.getNum());
		return BusinessResponseFactory.createSuccess(addresses);
	}
	
	@ApiOperation(value = "签名")
	@RequestMapping(value = "/sign",name="签名", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<String> sign(@RequestBody SignUsdtTransactionParams params){
		String signData = usdtService.sign(params);
		return BusinessResponseFactory.createSuccess(signData);
	}
}
