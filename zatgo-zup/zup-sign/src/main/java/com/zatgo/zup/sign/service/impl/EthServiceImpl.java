package com.zatgo.zup.sign.service.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.zatgo.zup.sign.entity.SignConstant;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.crypto.Wallet;
import org.web3j.crypto.WalletFile;
import org.web3j.crypto.WalletUtils;
import org.web3j.tx.ChainId;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.EthContractSignParams;
import com.zatgo.zup.common.model.EthSignParams;
import com.zatgo.zup.sign.enumtype.SignEnum;
import com.zatgo.zup.sign.service.EthService;
import com.zatgo.zup.sign.utils.MnemonicUtil;

import io.github.novacrypto.bip32.ExtendedPrivateKey;

@Component
public class EthServiceImpl implements EthService {

	private static final Logger logger = LoggerFactory.getLogger(EthServiceImpl.class);
	
	private  static final String ETH_DATA_BASE = "data" + File.separator + "eth";
	
	private  static final String ETH_WALLET_PATH = ETH_DATA_BASE + File.separator + "keystore";
	
	@Override
	public List<String> getNewAddresses(Integer num) {
		List<String> addresses = new ArrayList<>();
		
		//get mnemonic
		String mnemonic = MnemonicUtil.generateMnemonic();
		
		//privatekey path
		File path = new File(ETH_WALLET_PATH);
		if(!path.exists()){
			path.mkdirs();
		}

		//get private key
		List<ExtendedPrivateKey> childPrivateKeys = MnemonicUtil.getExtendedPrivateKey(mnemonic, SignEnum.SignCoinType.ETH, num,path.list().length);
		
		// 4. get key pair,save file
        for(ExtendedPrivateKey childPrivateKey:childPrivateKeys) {
        	byte[] privateKeyBytes = childPrivateKey.getKey(); //child private key
            ECKeyPair keyPair = ECKeyPair.create(privateKeyBytes);
            try {
				WalletFile walletFile = Wallet.createLight(SignConstant.SIGN_PASSWORD, keyPair);
				WalletUtils.generateWalletFile(SignConstant.SIGN_PASSWORD, keyPair, path, false);
				String address = Keys.getAddress(keyPair);
				addresses.add("0x" + address);
			} catch (Exception e) {
				logger.error("",e);
			}
        }

		return addresses;
	}

	@Override
	public String sign(EthSignParams ethsignParams) {
		
		Credentials credentials = getWalletFileByAddress(ethsignParams.getFromAddress(),ethsignParams.getPassword());
		if(credentials == null) {
			logger.error("from address is not keystore");
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"from address is not keystore");
		}
		
		try {
			String signData = signedEthTransactionData(ethsignParams.getToAddress(), 
					ethsignParams.getmNonce(), ethsignParams.getGasPrice(), 
					ethsignParams.getGasLimit(), ethsignParams.getAmount(), 
					credentials, ethsignParams.getPassword());
			
			
			return signData;
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
	}
	
	@Override
	public String signContract(EthContractSignParams ethContractSignParams) {
		Credentials credentials = getWalletFileByAddress(ethContractSignParams.getFromAddress(),ethContractSignParams.getPassword());
		if(credentials == null) {
			logger.error("from address is not keystore");
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"from address is not keystore");
		}
		
		try {
			String signData = signContractTransaction(ethContractSignParams.getContractAddress(), 
					ethContractSignParams.getToAddress(), ethContractSignParams.getmNonce(), 
					ethContractSignParams.getGasPrice(), 
					ethContractSignParams.getGasLimit(), ethContractSignParams.getAmount(), 
					ethContractSignParams.getDecimal(), credentials, ethContractSignParams.getPassword());
			
			return signData;
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
	}
	
	private Credentials getWalletFileByAddress(String address,String password) {
		if(address.startsWith("0x")) {
			address = address.substring(2);
		}
        File fileDir = new File(ETH_WALLET_PATH);
        if(!fileDir.exists()){
			return null;
		}
        
        Credentials credentials = null;
        String[] keyStores = fileDir.list();
        for(String keyStore:keyStores) {
        	if(keyStore.toLowerCase().contains(address.toLowerCase())) {
        		try {
        			credentials = WalletUtils.loadCredentials(password, ETH_WALLET_PATH + File.separator + keyStore);
				} catch (Exception e) {
					logger.error("",e);
				}
        		break;
        	}
        }
        
        return credentials;
	}
	
	
	
	
	
	/**
     * ETH 转账离线签名
     *
     * @param to         转入的钱包地址
     * @param nonce      以太坊nonce
     * @param gasPrice   gasPrice
     * @param gasLimit   gasLimit
     * @param amount     转账的eth数量
     * @param credentials 钱包对象
     * @param password   密码
     * @return 签名data
     */
    private String signedEthTransactionData(String to, BigInteger nonce, BigInteger gasPrice,
                                           BigInteger gasLimit, BigDecimal amount, Credentials credentials,
                                           String password) throws Exception {
        // 把十进制的转换成ETH的Wei, 1ETH = 10^18 Wei
        BigDecimal amountInWei = Convert.toWei(amount.toString(), Convert.Unit.ETHER);
        RawTransaction rawTransaction =
                RawTransaction.createEtherTransaction(nonce, gasPrice, gasLimit, to,
                        amountInWei.toBigInteger());
        return signData(rawTransaction, credentials, password);
    }

    /**
     * ETH RawTransaction 离线签名
     * @param rawTransaction
     * @param credentials
     * @param password
     * @return
     * @throws Exception
     */
    private String signData(RawTransaction rawTransaction, Credentials credentials, String password)
            throws Exception {
        byte[] signMessage =
                TransactionEncoder.signMessage(rawTransaction, ChainId.MAINNET, credentials);
        return Numeric.toHexString(signMessage);

    }

    /**
     * ETH 智能合约  转账离线签名
     * @param contractAddress 合约地址
     * @param to 转入的钱包地址
     * @param nonce 以太坊nonce
     * @param gasPrice gasPrice
     * @param gasLimit gasLimit
     * @param amount 转账的代币数量
     * @param decimal 精度
     * @param credentials 钱包对象
     * @param password 密码
     * @return 签名data
     * @throws Exception
     */
    private String signContractTransaction(String contractAddress,
                                          String to,
                                          BigInteger nonce,
                                          BigInteger gasPrice,
                                          BigInteger gasLimit,
                                          BigDecimal amount,
                                          BigInteger decimal,
                                          Credentials credentials,
                                          String password) throws Exception {
    	
        BigDecimal realValue = amount.multiply(BigDecimal.valueOf(Math.pow(10,decimal.doubleValue())));
        Function function = new Function("transfer",
                Arrays.asList(new Address(to), new Uint256(realValue.toBigInteger())),
                Collections.emptyList());
        String data = FunctionEncoder.encode(function);
        RawTransaction rawTransaction = RawTransaction.createTransaction(
                nonce,
                gasPrice,
                gasLimit,
                contractAddress,
                data);
        return signData(rawTransaction, credentials, password);
    }
    
//    public static void main(String[] args) {
//    	EthService ethService = new EthServiceImpl();
//    	List<String> addresses = ethService.getNewAddresses("123456", 10);
//
//    	EthSignParams ethSignParams = new EthSignParams();
//    	ethSignParams.setFromAddress(addresses.get(0));
//    	ethSignParams.setAmount(new BigDecimal(1));
//    	ethSignParams.setGasLimit(new BigInteger("21000"));
//    	ethSignParams.setGasPrice(new BigInteger("100"));
//    	ethSignParams.setmNonce(new BigInteger("1"));
//    	ethSignParams.setPassword("123456");
//    	ethSignParams.setToAddress(addresses.get(1));
//
//    	String signData = ethService.sign(ethSignParams);
//    	System.out.println(signData);
//
//
//    	EthContractSignParams ethContractSignParams = new EthContractSignParams();
//    	ethContractSignParams.setFromAddress(addresses.get(0));
//    	ethContractSignParams.setAmount(new BigDecimal(1));
//    	ethContractSignParams.setGasLimit(new BigInteger("21000"));
//    	ethContractSignParams.setGasPrice(new BigInteger("100"));
//    	ethContractSignParams.setmNonce(new BigInteger("1"));
//    	ethContractSignParams.setPassword("123456");
//    	ethContractSignParams.setToAddress(addresses.get(1));
//    	ethContractSignParams.setContractAddress("32423rrferfg34t43tft34t");
//    	ethContractSignParams.setDecimal(new BigInteger("8"));
//
//    	String signContractData = ethService.signContract(ethContractSignParams);
//    	System.out.println(signContractData);
//
//    }

}
