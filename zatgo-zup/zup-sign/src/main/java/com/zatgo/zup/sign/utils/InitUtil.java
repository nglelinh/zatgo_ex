package com.zatgo.zup.sign.utils;

import com.zatgo.zup.sign.entity.SignConstant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by 46041 on 2019/4/2.
 */
@Component
public class InitUtil {

    @Value("${password}")
    private String password;

    @PostConstruct
    public void init(){
        SignConstant.SIGN_PASSWORD = password;
    }
}
