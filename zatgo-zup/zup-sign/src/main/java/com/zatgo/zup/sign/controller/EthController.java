package com.zatgo.zup.sign.controller;

import java.util.List;

import com.zatgo.zup.common.model.CreateAddressRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.EthContractSignParams;
import com.zatgo.zup.common.model.EthSignParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.sign.service.EthService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value="/sign/eth", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/sign/eth")
public class EthController  extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(EthController.class);
	
	@Autowired
	private EthService ethService;
	
	@ApiOperation(value = "获取新地址")
	@RequestMapping(value = "/newaddresses",name="获取新地址", method = RequestMethod.POST)
	@ResponseBody
    public ResponseData<List<String>> getNewAddresses(@RequestBody CreateAddressRequest request){
		List<String> addresses = ethService.getNewAddresses(request.getNum());
		return BusinessResponseFactory.createSuccess(addresses);
	}
	
	@ApiOperation(value = "签名")
	@RequestMapping(value = "/sign",name="签名", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<String> sign(@RequestBody EthSignParams ethSignParams){
		String signData = ethService.sign(ethSignParams);
		return BusinessResponseFactory.createSuccess(signData);
	}
	
	@ApiOperation(value = "合约签名")
	@RequestMapping(value = "/signContract",name="签名", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<String> sign(@RequestBody EthContractSignParams ethContractSignParams){
		String signData = ethService.signContract(ethContractSignParams);
		return BusinessResponseFactory.createSuccess(signData);
	}
}
