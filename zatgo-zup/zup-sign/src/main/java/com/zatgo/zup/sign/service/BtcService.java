package com.zatgo.zup.sign.service;

import java.util.List;

import com.zatgo.zup.common.model.EthContractSignParams;
import com.zatgo.zup.common.model.EthSignParams;
import com.zatgo.zup.common.model.SignBTCTransactionParams;

public interface BtcService {

	/**
	 * 获取新地址列表
	 * @return
	 */
	List<String> getNewAddresses(String password, Integer num);
	
	/**
	 * 交易签名
	 * @param params
	 * @return
	 */
	String sign(SignBTCTransactionParams params);

}
