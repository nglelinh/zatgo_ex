package com.zatgo.zup.sign.service;

import com.zatgo.zup.common.model.SignUsdtTransactionParams;

import java.util.List;

public interface UsdtService {

	/**
	 * 获取新地址列表
	 * @return
	 */
	List<String> getNewAddresses(String password, Integer num);
	
	/**
	 * 交易签名
	 * @param params
	 * @return
	 */
	String sign(SignUsdtTransactionParams params);

}
