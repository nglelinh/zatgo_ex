package com.zatgo.zup.sign.controller;

import java.util.List;

import com.zatgo.zup.common.model.CreateAddressRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SignBTCTransactionParams;
import com.zatgo.zup.sign.service.BtcService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value="/sign/btc", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/sign/btc")
public class BtcController  extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(BtcController.class);
	
	@Autowired
	private BtcService btcService;
	
	@ApiOperation(value = "获取新地址")
	@RequestMapping(value = "/newaddresses",name="获取新地址", method = RequestMethod.POST)
	@ResponseBody
    public ResponseData<List<String>> getNewAddresses(@RequestBody CreateAddressRequest request){
		List<String> addresses = btcService.getNewAddresses(request.getPassword(), request.getNum());
		return BusinessResponseFactory.createSuccess(addresses);
	}
	
	@ApiOperation(value = "签名")
	@RequestMapping(value = "/sign",name="签名", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<String> sign(@RequestBody SignBTCTransactionParams params){
		String signData = btcService.sign(params);
		return BusinessResponseFactory.createSuccess(signData);
	}
}
