package com.zatgo.zup.sign.service;

import java.util.List;

import com.zatgo.zup.common.model.EthContractSignParams;
import com.zatgo.zup.common.model.EthSignParams;

public interface EthService {

	/**
	 * 获取新地址列表
	 * @return
	 */
	List<String> getNewAddresses(Integer num);
	
	/**
	 * 交易签名
	 * @param ethsignParams
	 * @return
	 */
	String sign(EthSignParams ethsignParams);
	
	
	/**
	 * 合约交易签名
	 * @param ethContractSignParams
	 * @return
	 */
	String signContract(EthContractSignParams ethContractSignParams);
}
