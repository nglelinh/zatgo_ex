package com.zatgo.zup.sign;

import com.zatgo.zup.sign.entity.SignConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Scanner;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
@EnableSwagger2
@ComponentScan(basePackages = "com.zatgo.zup")
@EnableDiscoveryClient
@EnableTransactionManagement
public class ZupSignApplication
{
	public static void main(String[] args) {
//		System.out.println("请输入密码");
//		Scanner scan = new Scanner(System.in);
//		if (scan.hasNext()){
//			String password = scan.next();
//			SignConstant.SIGN_PASSWORD = password;
//		}
		SpringApplication.run(ZupSignApplication.class, args);
	}
}
