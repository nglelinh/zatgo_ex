package com.zatgo.zup.sign.controller;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.*;
import com.zatgo.zup.sign.service.QtumService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 46041 on 2018/11/22.
 */

@RestController
@Api(value="/sign/qtum", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/sign/qtum")
public class QtumController {


    private static final Logger logger = LoggerFactory.getLogger(QtumController.class);

    @Autowired
    private QtumService qtumService;

    @ApiOperation(value = "获取新地址")
    @RequestMapping(value = "/newaddresses",name="获取新地址", method = RequestMethod.POST)
    @ResponseBody
    public ResponseData<List<String>> getNewAddresses(@RequestBody CreateAddressRequest request){
        List<String> addresses = qtumService.getNewAddresses(request.getPassword(), request.getNum());
        return BusinessResponseFactory.createSuccess(addresses);
    }

    @ApiOperation(value = "签名")
    @RequestMapping(value = "/sign",name="签名", method = RequestMethod.POST)
    @ResponseBody
    public ResponseData<String> sign(@RequestBody SignQtumTransactionParams params){
        String signData = qtumService.sign(params);
        return BusinessResponseFactory.createSuccess(signData);
    }

    @ApiOperation(value = "签名")
    @RequestMapping(value = "/signContract",name="签名", method = RequestMethod.POST)
    @ResponseBody
    public ResponseData<String> signContract(@RequestBody QtumContractSignParams params){
        String signData = qtumService.signContract(params);
        return BusinessResponseFactory.createSuccess(signData);
    }
}
