package com.zatgo.zup.sign.service.impl;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.QtumContractSignParams;
import com.zatgo.zup.common.model.SignQtumTransactionParams;
import com.zatgo.zup.common.model.UnSpentQtumData;
import com.zatgo.zup.common.utils.ContractMethodParameter;
import com.zatgo.zup.common.utils.StringUtil;
import com.zatgo.zup.sign.entity.Keccak;
import com.zatgo.zup.sign.entity.Parameters;
import com.zatgo.zup.sign.entity.QtumMainNetParams;
import com.zatgo.zup.sign.enumtype.SignEnum;
import com.zatgo.zup.sign.service.QtumService;
import com.zatgo.zup.sign.utils.MnemonicUtil;
import io.github.novacrypto.base58.Base58;
import io.github.novacrypto.bip32.ExtendedPrivateKey;
import io.github.novacrypto.hashing.Sha256;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bitcoinj.core.*;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;
import org.bitcoinj.script.ScriptChunk;
import org.bitcoinj.script.ScriptOpCodes;
import org.bitcoinj.wallet.UnreadableWalletException;
import org.bitcoinj.wallet.Wallet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import io.github.novacrypto.bip32.ExtendedPrivateKey;

/**
 * Created by 46041 on 2018/11/22.
 */

@Service("qtumService")
public class QtumServiceImpl implements QtumService{

    private static final Logger logger = LoggerFactory.getLogger(QtumServiceImpl.class);

    private  static final String QTUM_DATA_BASE = "data" + File.separator + "qtum";

    private  static final String QTUM_WALLET_PATH = QTUM_DATA_BASE + File.separator + "wallet";

    private static final ReentrantLock lock = new ReentrantLock();

    private final String ARRAY_PARAMETER_CHECK_PATTERN = ".*?\\d+\\[\\d*\\]";

    private final int radix = 16;
    private final String TYPE_INT = "int";
    private final String TYPE_STRING = "string";
    private final String TYPE_ADDRESS = "address";
    private final String TYPE_BOOL = "bool";
    final static int OP_PUSHDATA_4 = 0x04;
    final static int OP_PUSHDATA_8 = 8;
    final static int OP_EXEC = 193;

    long paramsCount;
    long currStringOffset = 0;

    private String hashPattern = "0000000000000000000000000000000000000000000000000000000000000000";

    @Override
    public List<String> getNewAddresses(String password, Integer num) {
        //get network
//        NetworkParameters networkParameters = null;
        QtumMainNetParams networkParameters = new QtumMainNetParams().get();
//        if (true) {
//            networkParameters = ;
//        }else {
////            networkParameters = TestNet3Params.get();
//        }

        lock.lock();
        try {
            List<String> addresses = new ArrayList<>();

            //get mnemonic
            String mnemonic = MnemonicUtil.generateMnemonic();

            //exist private key number
            Integer existPrivateKeyNum = 0;
            File path = new File(QTUM_WALLET_PATH);
            if(!path.exists()){
                path.mkdirs();
            }
            File walletFile = new File(QTUM_WALLET_PATH + File.separator + "wallet.dat");
            Wallet wallet = null;
            if(walletFile.exists()) {
                try {
                    wallet = Wallet.loadFromFile(walletFile);
                } catch (UnreadableWalletException e) {
                    logger.error("",e);
                    throw new BusinessException();
                }
                List<ECKey> list = wallet.getImportedKeys();
                existPrivateKeyNum = list.size();
            }else {
                wallet = new Wallet(networkParameters);
                wallet.encrypt(password);
            }

            //get private key
            List<ExtendedPrivateKey> childPrivateKeys = MnemonicUtil.getExtendedPrivateKey(mnemonic, SignEnum.SignCoinType.QTUM, num,existPrivateKeyNum);
            List<ECKey> keys = new ArrayList<>();

            //BINCOINJ HD
//			String wordsList = "one misery space industry hen mistake typical prison plunge yellow disagree arm";
//			DeterministicSeed deterministicSeed;
//			try {
//				deterministicSeed = new DeterministicSeed(wordsList, null, "", 0L);
//				DeterministicKeyChain deterministicKeyChain = DeterministicKeyChain.builder().seed(deterministicSeed).build();
//				DeterministicKey deterministicKey = deterministicKeyChain.getKeyByPath(HDUtils.parsePath("44H / 1H / 0H / 0 / 2"), true);
//				BigInteger privKey = deterministicKey.getPrivKey();
//			} catch (UnreadableWalletException e1) {
//			}

            for(ExtendedPrivateKey childPrivateKey:childPrivateKeys) {
                String privateKey = getPrivateKey(childPrivateKey);
                DumpedPrivateKey dumpedPrivateKey = DumpedPrivateKey.fromBase58(networkParameters, privateKey);
                ECKey ecKey = dumpedPrivateKey.getKey();
                String address = ecKey.toAddress(wallet.getNetworkParameters()).toString();
                logger.debug("---------eckey bitcoin privateKey_hex:" + ecKey.getPrivateKeyAsHex());
                logger.debug("---------eckey bitcoin privateKey_Base58:" + Base58.base58Encode(StringUtil.hexStringToByteArray(ecKey.getPrivateKeyAsHex())));
                logger.debug("---------eckey bitcoin publicKey:" + ecKey.getPublicKeyAsHex());
                logger.debug("---------eckey bitcoin address:" + address);

                keys.add(ecKey);
                addresses.add(address);
            }

            wallet.importKeysAndEncrypt(keys, password);

            try {
                wallet.saveToFile(walletFile);
            } catch (Exception e) {
                logger.error("",e);
                throw new BusinessException();
            }

            return addresses;
        }finally {
            lock.unlock();
        }

    }

    @Override
    public String sign(SignQtumTransactionParams params) {

        NetworkParameters networkParameters = QtumMainNetParams.get();

        Transaction transaction = new Transaction(networkParameters);

        long totalMoney = 0;
        List<UTXO> utxos = new ArrayList<>();
        List<ECKey> ecKeys = new ArrayList<>();
        HashMap<String, ECKey> ecKeyMap = new HashMap<>();
        //遍历未花费列表，组装合适的item
        for (UnSpentQtumData us : params.getUnSpentBtcs()) {
            if(totalMoney >= params.getAmount() + params.getFee()) {
                break;
            }

            UTXO utxo;
            Script script = null;
            try {
                script = new Script(Hex.decodeHex(us.getScriptPubKey()));
                utxo = new UTXO(Sha256Hash.wrap(us.getTxId()), us.getVout(), Coin.valueOf(us.getSatoshis()),
                        us.getHeight(), false, script);

                utxos.add(utxo);
            } catch (Exception e) {
                logger.error("",e);
                throw new BusinessException();
            }

            Address fromAddress;
            try {
//				fromAddress = Address.fromP2SHScript(networkParameters, new Script(Hex.decodeHex(us.getScriptPubKey())));
                fromAddress = script.getToAddress(networkParameters);
            } catch (Exception e) {
                logger.error("",e);
                throw new BusinessException();
            }
            String addressBase58 = fromAddress.toBase58();
            ECKey ecKey = ecKeyMap.get(addressBase58);
            if(ecKey == null) {
                ecKey = getECKeyByAddressAndPass(addressBase58,params.getPassword());
                if(ecKey == null) {
                    throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"address[" + addressBase58 +"] is not private key");
                }
                ecKeyMap.put(addressBase58, ecKey);
            }
            ecKeys.add(ecKey);

            totalMoney += us.getSatoshis();
        }

        transaction.addOutput(Coin.valueOf(params.getAmount()), Address.fromBase58(networkParameters, params.getToAddress()));

        //消费列表总金额 - 已经转账的金额 - 手续费 就等于需要返回给自己的金额了
        long balance = totalMoney - params.getAmount() - params.getFee();
        //输出-转给自己
        if (balance > 0) {
            transaction.addOutput(Coin.valueOf(balance), Address.fromBase58(networkParameters, params.getChangeAddress()));
        }
        //输入未消费列表项
        int index = 0;
        for (UTXO utxo : utxos) {
//        	String privateBase58 = Base58.base58Encode(StringUtil.hexStringToByteArray(ecKeys.get(index).getPrivateKeyAsHex()));
//        	DumpedPrivateKey dumpedPrivateKey = DumpedPrivateKey.fromBase58(networkParameters, privateBase58);
//            ECKey ecKey = dumpedPrivateKey.getKey();
            TransactionOutPoint outPoint = new TransactionOutPoint(networkParameters, utxo.getIndex(), utxo.getHash());
//            transaction.addSignedInput(outPoint, utxo.getScript(), ecKey, Transaction.SigHash.ALL, true);
            transaction.addSignedInput(outPoint, utxo.getScript(), ecKeys.get(index), Transaction.SigHash.ALL, true);
            index++;
        }

        return Hex.encodeHexString(transaction.bitcoinSerialize());
    }

    @Override
    public String signContract(QtumContractSignParams params) {
        NetworkParameters networkParameters = QtumMainNetParams.get();

        Transaction transaction = new Transaction(networkParameters);

        long totalMoney = 0;
        long fee = params.getGasLimit().multiply(params.getGasPrice()).add(params.getFee()).longValue();
        List<UTXO> utxos = new ArrayList<>();
        List<ECKey> ecKeys = new ArrayList<>();
        HashMap<String, ECKey> ecKeyMap = new HashMap<>();
        //遍历未花费列表，组装合适的item
        for (UnSpentQtumData us : params.getUnSpentBtcs()) {
            if(totalMoney >= fee) {
                break;
            }

            UTXO utxo;
            Script script = null;
            try {
                script = new Script(Hex.decodeHex(us.getScriptPubKey()));
                utxo = new UTXO(Sha256Hash.wrap(us.getTxId()), us.getVout(), Coin.valueOf(us.getSatoshis()),
                        us.getHeight(), false, script);

                utxos.add(utxo);
            } catch (Exception e) {
                logger.error("",e);
                throw new BusinessException();
            }

            Address fromAddress;
            try {
//				fromAddress = Address.fromP2SHScript(networkParameters, new Script(Hex.decodeHex(us.getScriptPubKey())));
                fromAddress = script.getToAddress(networkParameters);
            } catch (Exception e) {
                logger.error("",e);
                throw new BusinessException();
            }
            String addressBase58 = fromAddress.toBase58();
            ECKey ecKey = ecKeyMap.get(addressBase58);
            if(ecKey == null) {
                ecKey = getECKeyByAddressAndPass(addressBase58,params.getPassword());
                if(ecKey == null) {
                    throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"address[" + addressBase58 +"] is not private key");
                }
                ecKeyMap.put(addressBase58, ecKey);
            }
            ecKeys.add(ecKey);

            totalMoney += us.getSatoshis();
        }
        String opcall = getOPCALL(params.getGasPrice(), params.getGasLimit(), params.getToAddressHex(), params.getContractAddress(), params.getAmount().toBigInteger());
        Script script = new Script(Utils.parseAsHexOrBase58(opcall));
        transaction.addOutput(Coin.ZERO, script);

//        transaction.addOutput(Coin.valueOf(params.getFee().longValue()), Address.fromBase58(networkParameters, params.getToAddress()));

        //消费列表总金额 - 已经转账的金额 - 手续费 就等于需要返回给自己的金额了
        long balance = totalMoney - fee;
        //输出-转给自己
        if (balance > 0) {
            transaction.addOutput(Coin.valueOf(balance), Address.fromBase58(networkParameters, params.getChangeAddress()));
        }
        //输入未消费列表项
        int index = 0;
        for (UTXO utxo : utxos) {
//        	String privateBase58 = Base58.base58Encode(StringUtil.hexStringToByteArray(ecKeys.get(index).getPrivateKeyAsHex()));
//        	DumpedPrivateKey dumpedPrivateKey = DumpedPrivateKey.fromBase58(networkParameters, privateBase58);
//            ECKey ecKey = dumpedPrivateKey.getKey();
            TransactionOutPoint outPoint = new TransactionOutPoint(networkParameters, utxo.getIndex(), utxo.getHash());
//            transaction.addSignedInput(outPoint, utxo.getScript(), ecKey, Transaction.SigHash.ALL, true);
            transaction.addSignedInput(outPoint, utxo.getScript(), ecKeys.get(index), Transaction.SigHash.ALL, true);
            index++;
        }

        return Hex.encodeHexString(transaction.bitcoinSerialize());
    }

//    public static void main(String[] args) {
////        Script script = new Script(Utils.parseAsHexOrBase58("01040390d003012844a9059cbb000000000000000000000000f1946b8b16740f3fa4a715fd40c118cdd88b80630000000000000000000000000000000000000000000060b7faae5e7811740000145a4b7889cad562d6c099bf877c8f5e3d66d579f8c2"));
//        String opcall = getOPCALL(new BigInteger("250000"), new BigInteger("10000000000"), "e57e4a5f9ac130defb33a057729f10728fcdb9cb", "fe59cbc1704e89a698571413a81f0de9d8f00c69", new BigDecimal("1965000000000").toBigInteger());
//        System.out.println(opcall);
//        String ss = "01040390d003012844a9059cbb000000000000000000000000e57e4a5f9ac130defb33a057729f10728fcdb9cb000000000000000000000000000000000000000000000000000001c98320820014fe59cbc1704e89a698571413a81f0de9d8f00c69c2";
//        String aa = "010403280190d00344a9059cbb000000000000000000000000e57e4a5f9ac130defb33a057729f10728fcdb9cb000000000000000000000000000000000000000000000000000001c98320820014fe59cbc1704e89a698571413a81f0de9d8f00c69c2";
//        System.out.println(opcall.length() - ss.length());
//        Script script = new Script(Utils.parseAsHexOrBase58(opcall));
//        System.out.println(script);
////        System.out.println("010403" + getEncodeLimit(new BigInteger("250000")) + "01" + getEncodeLimit(new BigInteger("40")));
//    }

    /**
     * 获取OP_CALL
     * @return
     */
    private static String getOPCALL(BigInteger gas, BigInteger limit, String hexToAddress, String contractAddress, BigInteger amount){
        hexToAddress = "000000000000000000000000" + hexToAddress;
        String amountHex = amount.toString(16);
        String encodeLimit = getEncodeLimit(limit);
        String encodeGas = getEncodeLimit(gas);
        String supplement = supplement(amountHex, 64);
        return "010403" + encodeLimit + "01" + encodeGas + "44a9059cbb" + hexToAddress + supplement + "14" + contractAddress + "c2";
    }

    private static String getEncodeLimit(BigInteger limit){
        String hex = limit.toString(16);
        if (hex.length() % 2 != 0)
            hex = "0" + hex;
        StringBuffer sb = new StringBuffer();
        while (true){
            int length = hex.length();
            if (length < 2){
                break;
            }
            String substring = hex.substring(length - 2, length);
            sb.append(substring);
            hex = hex.substring(0, length - 2);
        }
        return sb.toString();
    }

    private static String supplement(String str, int n){
        int length = str.length();
        if (length < n){
            str = BigDecimal.TEN.pow(n - length).toString().replaceFirst("1", "") + str;
        }
        return str;
    }


    public Script createConstructScript(String abiParams, int gasLimitInt, int gasPriceInt) {
        byte[] version = org.spongycastle.util.encoders.Hex.decode("04000000");
        byte[] arrayGasLimit = org.spongycastle.util.Arrays.reverse((new BigInteger(String.valueOf(gasLimitInt))).toByteArray());
        byte[] gasLimit = new byte[]{0, 0, 0, 0, 0, 0, 0, 0};
        System.arraycopy(arrayGasLimit, 0, gasLimit, 0, arrayGasLimit.length);
        byte[] arrayGasPrice = org.spongycastle.util.Arrays.reverse((new BigInteger(String.valueOf(gasPriceInt))).toByteArray());
        byte[] gasPrice = new byte[]{0, 0, 0, 0, 0, 0, 0, 0};
        System.arraycopy(arrayGasPrice, 0, gasPrice, 0, arrayGasPrice.length);
        byte[] data = org.spongycastle.util.encoders.Hex.decode(abiParams);
        byte[] program;
        ScriptChunk versionChunk = new ScriptChunk(OP_PUSHDATA_4, version);
        ScriptChunk gasLimitChunk = new ScriptChunk(OP_PUSHDATA_8, gasLimit);
        ScriptChunk gasPriceChunk = new ScriptChunk(OP_PUSHDATA_8, gasPrice);
        ScriptChunk dataChunk = new ScriptChunk(ScriptOpCodes.OP_PUSHDATA2, data);
        ScriptChunk opExecChunk = new ScriptChunk(OP_EXEC, null);
        List<ScriptChunk> chunkList = new ArrayList<>();
        chunkList.add(versionChunk);
        chunkList.add(gasLimitChunk);
        chunkList.add(gasPriceChunk);
        chunkList.add(dataChunk);
        chunkList.add(opExecChunk);
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            for (ScriptChunk chunk : chunkList) {
                chunk.write(bos);
            }
            program = bos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new Script(program);
    }

    public String createAbiMethodParamsObservable(String address, String resultAmount) {
        List<ContractMethodParameter> contractMethodParameterList = new ArrayList<>();
        ContractMethodParameter contractMethodParameterAddress = new ContractMethodParameter("_to", "address", address);

        ContractMethodParameter contractMethodParameterAmount = new ContractMethodParameter("_value", "uint256", resultAmount);
        contractMethodParameterList.add(contractMethodParameterAddress);
        contractMethodParameterList.add(contractMethodParameterAmount);
        return call("transfer", contractMethodParameterList);
    }

    public String call(final String _methodName, final List<ContractMethodParameter> contractMethodParameterList) {
        String methodName = _methodName;
        String parameters = "";
        String abiParams = "";
        paramsCount = contractMethodParameterList.size();
//        List<ContractMethodParameter> mContractMethodParameterList = contractMethodParameterList;
        if (contractMethodParameterList != null && contractMethodParameterList.size() != 0) {
            for (ContractMethodParameter parameter : contractMethodParameterList) {
                abiParams += convertParameter(parameter);
                parameters = parameters + parameter.getType() + ",";
            }
            methodName = methodName + "(" + parameters.substring(0, parameters.length() - 1) + ")";
        } else {
            methodName = methodName + "()";
        }
        Keccak keccak = new Keccak();
        String hashMethod = keccak.getHash(org.spongycastle.util.encoders.Hex.toHexString((methodName).getBytes()), Parameters.KECCAK_256).substring(0, 8);
        abiParams = hashMethod + abiParams;
        return abiParams;
    }

    private String convertParameter(ContractMethodParameter parameter) {
        String _value = parameter.getValue();
        if (!parameterIsArray(parameter)) {
            if (parameter.getType().contains(TYPE_INT)) {
                return appendNumericPattern(convertToByteCode(new BigDecimal(_value).toBigInteger()));
            } else if (parameter.getType().contains(TYPE_STRING)) {
                return getStringOffset(parameter);
            } else if (parameter.getType().contains(TYPE_ADDRESS) && _value.length() == 34) {
                byte[] decode = org.bitcoinj.core.Base58.decode(_value);
                String toHexString = org.spongycastle.util.encoders.Hex.toHexString(decode);
                String substring = toHexString.substring(2, 42);
                return appendAddressPattern(substring);
            } else if (parameter.getType().contains(TYPE_ADDRESS)) {
                return getStringOffset(parameter);
            } else if (parameter.getType().contains(TYPE_BOOL)) {
                return appendBoolean(_value);
            }
        } else {
            return getStringOffset(parameter);
        }
        return "";
    }

    private String appendAddressPattern(String _value) {
        return hashPattern.substring(_value.length()) + _value;
    }

    private String appendBoolean(String parameter) {
        return Boolean.valueOf(parameter) ? appendNumericPattern("1") : appendNumericPattern("0");
    }

    private String getStringOffset(ContractMethodParameter parameter) {
        long currOffset = ((paramsCount + currStringOffset) * 32);
        currStringOffset = getStringHash(parameter.getValue()).length() / hashPattern.length() + 1;
        return appendNumericPattern(convertToByteCode(currOffset));
    }

    private String getStringHash(String _value) {
        if (_value.length() <= hashPattern.length()) {
            return formNotFullString(_value);
        } else {
            int ost = _value.length() % hashPattern.length();
            return _value + hashPattern.substring(0, hashPattern.length() - ost);
        }
    }

    private String formNotFullString(String _value) {
        return _value + hashPattern.substring(_value.length());
    }

    private String convertToByteCode(long _value) {
        return Long.toString(_value, radix);
    }

    private String convertToByteCode(BigInteger _value) {
        return _value.toString(radix);
    }

    private String appendNumericPattern(String _value) {
        return hashPattern.substring(0, hashPattern.length() - _value.length()) + _value;
    }

    private boolean parameterIsArray(ContractMethodParameter contractMethodParameter) {
        Pattern p = Pattern.compile(ARRAY_PARAMETER_CHECK_PATTERN);
        Matcher m = p.matcher(contractMethodParameter.getType());
        return m.matches();
    }


    private ECKey getECKeyByAddressAndPass(String address,String password) {
        File walletFile = new File(QTUM_WALLET_PATH + File.separator + "wallet.dat");
        if(!walletFile.exists()) {
            logger.error("from address is not exist");
            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"from address is not exist");
        }

        try {
            Wallet wallet = Wallet.loadFromFile(walletFile);

            List<ECKey> list = wallet.getImportedKeys();
            for(ECKey ecKey:list) {

                if(ecKey.toAddress(wallet.getNetworkParameters()).toString().equals(address)) {
                    ecKey = ecKey.decrypt(wallet.getKeyCrypter(),wallet.getKeyCrypter().deriveKey(password));
                    logger.debug("---------sign bitcoin privateKey_hex:" + ecKey.getPrivateKeyAsHex());
                    logger.debug("---------sign bitcoin privateKey_Base58:" + Base58.base58Encode(StringUtil.hexStringToByteArray(ecKey.getPrivateKeyAsHex())));
                    logger.debug("---------sign bitcoin publicKey:" + ecKey.getPublicKeyAsHex());
                    logger.debug("---------sign bitcoin address:" + ecKey.toAddress(wallet.getNetworkParameters()).toString());

                    return ecKey;

                }
            }

            return null;
        } catch (Exception e) {
            logger.error("",e);
            throw new BusinessException();
        }

    }

    public static void main(String[] args) {
        ECKey ecKey = getECKeyByAddressAndPass123("QZ5SUWACYSM5YL3h34qqes8bbrTAkjkeRd", "123456");
        DumpedPrivateKey privateKeyEncoded = ecKey.getPrivateKeyEncoded(new QtumMainNetParams().get());
        String s = privateKeyEncoded.toBase58();
        System.out.println(s);
        System.out.println(Base58.base58Encode(StringUtil.hexStringToByteArray(ecKey.getPrivateKeyAsHex())));
    }

    private static ECKey getECKeyByAddressAndPass123(String address,String password) {
        File walletFile = new File(QTUM_WALLET_PATH + File.separator + "wallet.dat");
        if(!walletFile.exists()) {
            logger.error("from address is not exist");
            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"from address is not exist");
        }

        try {
            Wallet wallet = Wallet.loadFromFile(walletFile);

            List<ECKey> list = wallet.getImportedKeys();
            for(ECKey ecKey:list) {

                if(ecKey.toAddress(wallet.getNetworkParameters()).toString().equals(address)) {
                    ecKey = ecKey.decrypt(wallet.getKeyCrypter(),wallet.getKeyCrypter().deriveKey(password));
                    logger.debug("---------sign bitcoin privateKey_hex:" + ecKey.getPrivateKeyAsHex());
                    logger.debug("---------sign bitcoin privateKey_Base58:" + Base58.base58Encode(StringUtil.hexStringToByteArray(ecKey.getPrivateKeyAsHex())));
                    logger.debug("---------sign bitcoin publicKey:" + ecKey.getPublicKeyAsHex());
                    logger.debug("---------sign bitcoin address:" + ecKey.toAddress(wallet.getNetworkParameters()).toString());

                    return ecKey;

                }
            }

            return null;
        } catch (Exception e) {
            logger.error("",e);
            throw new BusinessException();
        }

    }


    /**
     * generate bitcoin privatekey, publickey and address.
     *
     * @param
     */
    private String getPrivateKey(ExtendedPrivateKey childPrivateKey) {
        // 获取比特币私钥
        String privateKey = childPrivateKey.getPrivateKey();
        // 加80前缀和01后缀
        String rk = "80" + privateKey + "01";
        // 生成校验和
        byte[] checksum = Sha256.sha256(StringUtil.hexStringToByteArray(rk));
        checksum = Sha256.sha256(checksum);
        // 取校验和前4位（32bits）
        String end = String.valueOf(Hex.encodeHex(checksum)).substring(0, 8);
        rk = rk + end;
        // 进行base58编码
        return Base58.base58Encode(StringUtil.hexStringToByteArray(rk));
    }
}
