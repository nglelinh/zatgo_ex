package com.zatgo.zup.trade.api.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductArticleDetail;
import com.ykb.mall.common.model.ProductActivityArticle;
import com.zatgo.zup.trade.api.dao.PmsProductArticleDao;
import com.zatgo.zup.trade.api.mapper.PmsProductMapper;
import com.zatgo.zup.trade.api.mapper.ProductActivityArticleMapper;
import com.zatgo.zup.trade.api.service.PmsProductArticleService;
import com.zatgo.zup.trade.api.service.PmsProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PmsProductArticleServiceImpl implements PmsProductArticleService {
	
	@Autowired
    private PmsProductArticleDao pmsProductArticleDao;
	
	@Autowired
	private PmsProductService pmsProductService;

	@Autowired
	private PmsProductMapper pmsProductMapper;

	@Override
	public PmsProductArticleDetail selectProductArticleDetail(Long id) {
		PmsProductArticleDetail detail = pmsProductArticleDao.selectProductArticleDetail(id);
		pmsProductService.addProductClickNum(id);
		return detail;
	}

	@Override
	public PageInfo<PmsProduct> activityArticleList(Integer pageNo, Integer pageSize) {
		PageHelper.startPage(pageNo, pageSize);
		List<PmsProduct> list = pmsProductMapper.selectArticleList();
		return new PageInfo<PmsProduct>(list);
	}

}
