package com.zatgo.zup.trade.api.repository;//package com.ykb.mall.mapi.repository;
//
//import com.ykb.mall.mapi.domain.MemberReadHistory;
//import org.springframework.data.mongodb.repository.MongoRepository;
//
//import java.util.List;
//
///**
// * 会员商品浏览历史Repository
// * Created by chen on 2018/8/3.
// */
//public interface MemberReadHistoryRepository extends MongoRepository<MemberReadHistory,String> {
//    List<MemberReadHistory> findByMemberIdOrderByCreateTimeDesc(Long memberId);
//}
