package com.zatgo.zup.trade.api.controller;

import com.ykb.mall.common.model.UmsMember;
import com.ykb.mall.common.model.UmsMemberExample;
import com.ykb.mall.common.model.UmsMemberWeixin;
import com.ykb.mall.common.model.UmsMemberWeixinExample;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.model.wx.ThirdWxInfo;
import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.mapper.UmsMemberMapper;
import com.zatgo.zup.trade.api.mapper.UmsMemberWeixinMapper;
import com.zatgo.zup.trade.api.remoteService.WechatRemoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 46041 on 2019/5/15.
 */

@RestController
@Api(tags = "ThirdGetDataController", description = "三方获取数据")
@RequestMapping("/trade/api/third")
public class ThirdGetDataController {

    private static final Logger logger = LoggerFactory.getLogger(ThirdGetDataController.class);

    @Autowired
    private UmsMemberWeixinMapper umsMemberWeixinMapper;
    @Autowired
    private UmsMemberMapper memberMapper;
    @Autowired
    private WechatRemoteService wechatRemoteService;


//    @Value("${jwt.tokenHead}")
//    private String tokenHead;
//
//    @Value("${jwt.tokenHeader}")
//    private String tokenHeader;



    @ApiOperation("根据微信openId获取用户信息")
    @RequestMapping(value = "/get/userInfo", method = RequestMethod.GET)
    @ResponseBody
    public Object getUserInfo(@RequestParam("wxOpenId") String wxOpenId){
        UmsMemberWeixinExample example = new UmsMemberWeixinExample();
        example.createCriteria().andOpenIdEqualTo(wxOpenId);
        List<UmsMemberWeixin> umsMemberWeixins = umsMemberWeixinMapper.selectByExample(example);
        ThirdLoginInfo loginInfo = new ThirdLoginInfo();
        ThirdWxInfo wxInfo = null;
        if (!CollectionUtils.isEmpty(umsMemberWeixins)){
            UmsMemberWeixin umsMemberWeixin = umsMemberWeixins.get(0);
            wxInfo = new ThirdWxInfo();
            wxInfo.setOpenId(umsMemberWeixin.getOpenId());
            wxInfo.setName(umsMemberWeixin.getNickName());
            wxInfo.setHeadUrl(umsMemberWeixin.getHeadimgUrl());
            wxInfo.setGender(umsMemberWeixin.getSex());
            UmsMemberExample umsMemberExample = new UmsMemberExample();
            umsMemberExample.createCriteria().andWeixinIdEqualTo(umsMemberWeixin.getId());
            List<UmsMember> umsMembers = memberMapper.selectByExample(umsMemberExample);
            if (!CollectionUtils.isEmpty(umsMembers)){
                UmsMember umsMember = umsMembers.get(0);
                loginInfo.setUserId(umsMember.getId() + "");
                loginInfo.setUserName(umsMember.getUsername());
            }
        }
        loginInfo.setWxInfo(wxInfo);
        return new CommonResult().success(loginInfo);
    }

    @ApiOperation("根据微信openId获取用户信息")
    @RequestMapping(value = "/get/userInfo/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public Object getUserInfoByUserId(@PathVariable("userId") Long userId){
        UmsMember umsMember = memberMapper.selectByPrimaryKey(userId);
        ThirdLoginInfo loginInfo = new ThirdLoginInfo();
        ThirdWxInfo wxInfo = null;
        if (umsMember != null){
            Long weixinId = umsMember.getWeixinId();
            UmsMemberWeixin umsMemberWeixin = umsMemberWeixinMapper.selectByPrimaryKey(weixinId);
            wxInfo = new ThirdWxInfo();
            wxInfo.setOpenId(umsMemberWeixin.getOpenId());
            wxInfo.setName(umsMemberWeixin.getNickName());
            wxInfo.setHeadUrl(umsMemberWeixin.getHeadimgUrl());
            wxInfo.setGender(umsMemberWeixin.getSex());
            loginInfo.setUserId(umsMember.getId() + "");
            loginInfo.setUserName(umsMember.getUsername());
        }
            loginInfo.setWxInfo(wxInfo);
        return new CommonResult().success(loginInfo);
    }

//    @ApiOperation("获取微信access_token")
//    @RequestMapping(value = "/get/accessToken", method = RequestMethod.GET)
//    @ResponseBody
//    public Object getAccessToken(){
//        String accessToken = wechatRemoteService.getWechatAccessToken();
//        return new CommonResult().success(accessToken);
//    }


//    @PostMapping("/login")
//    @ApiOperation("三方登陆")
//    public Object thirdLogin(HttpServletRequest request){
//        CommonResult result = new CommonResult();
//        String token = request.getHeader(tokenHeader);
//        try{
//            if (StringUtils.isEmpty(token) || !token.startsWith(this.tokenHead)) {
//                logger.error("token不可为空");
//                return new CommonResult().failed("token不可为空");
//            }
//            String authToken = token.substring(this.tokenHead.length());
//            String username = jwtTokenUtil.getUserNameFromToken(authToken);
//            String userTokenKey = RedisKeyConstants.getUserTokenKey(username);
//            String userStr = redisService.get(userTokenKey);
//            UmsMember member = null;
//            if (StringUtils.isEmpty(userStr)){
//                member = memberService.getByUsername(username);
//                if (member == null){
//                    logger.error("用户不存在");
//                    return new CommonResult().failed("用户不存在");
//                }
//                member.setPassword(null);
//                redisService.set(userTokenKey, JSONObject.toJSONString(member));
//                redisService.expire(userTokenKey, 2592000l);
//            } else {
//                member = JSONObject.parseObject(userStr, UmsMember.class);
//            }
//            Long weixinId = member.getWeixinId();
//            UmsMemberWeixin umsMemberWeixin = memberWeixinMapper.selectByPrimaryKey(weixinId);
//            ThirdLoginInfo info = new ThirdLoginInfo();
//            ThirdWxInfo wxInfo = new ThirdWxInfo();
//            if (umsMemberWeixin != null) {
//                info.setUserId(member.getId() + "");
//                info.setUserName(member.getUsername());
//                wxInfo.setGender(umsMemberWeixin.getSex());
//                wxInfo.setHeadUrl(umsMemberWeixin.getHeadimgUrl());
//                wxInfo.setName(umsMemberWeixin.getNickName());
//                wxInfo.setOpenId(umsMemberWeixin.getOpenId());
//            }
//            info.setWxInfo(wxInfo);
//            result.success(info);
//        } catch (Exception e){
//            logger.error("", e);
//            result.failed("未知异常");
//        }
//        return result;
//    }
}
