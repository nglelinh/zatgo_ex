package com.zatgo.zup.trade.api.controller;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.trade.api.domain.PassengerIdcardParams;
import com.zatgo.zup.trade.api.domain.PassengerParams;
import com.zatgo.zup.trade.api.service.UmsMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 会员登录注册管理Controller
 * Created by chen on 2018/8/3.
 */
@Controller
@Api(tags = "UmsMemberController", description = "会员登录注册管理")
@RequestMapping("/trade/api/sso")
public class UmsMemberController extends BaseController{

    @Autowired
    private UmsMemberService memberService;

//    @Autowired
//    private PasswordEncoder passwordEncoder;

//    @ApiOperation("访问登录")
//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    @ResponseBody
//    public Object login(@RequestBody UmsMemberParam param){
//        if (BusinessEnum.LoginType.LOGIN.getCode().equals(param.getVisitType())) {
//            return memberService.login(param);
//        } else if (BusinessEnum.LoginType.REGISTER.getCode().equals(param.getVisitType())) {
//            return memberService.wxRigester(param);
//        }
//        return new CommonResult().failed("未知的登录方式");
//    }
//
//    @ApiOperation("注册")
//    @RequestMapping(value = "/register", method = RequestMethod.POST)
//    @ResponseBody
//    public Object register(@RequestParam String username,
//                           @RequestParam String password,
//                           @RequestParam String telephone,
//                           @RequestParam String authCode) {
//        return memberService.register(username, password, telephone, authCode);
//    }

    @ApiOperation("获取验证码")
    @RequestMapping(value = "/getAuthCode", method = RequestMethod.GET)
    @ResponseBody
    public Object getAuthCode(@RequestParam String telephone) {
        return memberService.generateAuthCode(telephone);
    }

//    @ApiOperation("修改密码")
//    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
//    @ResponseBody
//    public Object updatePassword(@RequestParam String telephone,
//                                 @RequestParam String password,
//                                 @RequestParam String authCode) {
//        return memberService.updatePassword(telephone,password,authCode);
//    }
    
    @ApiOperation("添加出行人，可添加证件")
    @RequestMapping(value = "/addPassenger", method = RequestMethod.POST)
    @ResponseBody
    public Object addPassenger(@RequestBody PassengerParams params) {
        AuthUserInfo userInfo = getUserInfo();
        return memberService.addPassenger(params, userInfo.getUserId());
    }
    
    @ApiOperation("修改出行人")
    @RequestMapping(value = "/updatePassenger/{passengerId}", method = RequestMethod.POST)
    @ResponseBody
    public Object updatePassenger(@PathVariable Long passengerId, @RequestBody PassengerParams params) {
        AuthUserInfo userInfo = getUserInfo();
        return memberService.addPassenger(params, userInfo.getUserId());
    }
    
    @ApiOperation("修改证件")
    @RequestMapping(value = "/updateIdCard/{idCardId}", method = RequestMethod.POST)
    @ResponseBody
    public Object updatePassengerIdCard(@PathVariable Long idCardId, @RequestBody PassengerIdcardParams params) {
        AuthUserInfo userInfo = getUserInfo();
        return memberService.updatePassengerIdCard(idCardId, params, userInfo.getUserId());
    }
    
    @ApiOperation("新增证件")
    @RequestMapping(value = "/addIdCard", method = RequestMethod.POST)
    @ResponseBody
    public Object addPassengerIdCard(@PathVariable Long idCardId, @RequestBody PassengerIdcardParams params) {
        AuthUserInfo userInfo = getUserInfo();
        return memberService.updatePassengerIdCard(idCardId, params, userInfo.getUserId());
    }
    
    @ApiOperation("出行人列表")
    @RequestMapping(value = "/passengerList", method = RequestMethod.GET)
    @ResponseBody
    public Object passengerList(@RequestParam(required = false) String name) {
        AuthUserInfo userInfo = getUserInfo();
        return memberService.passengerList(name, userInfo.getUserId());
    }
    
    @ApiOperation("删除出行人")
    @RequestMapping(value = "/delete/passenger/{passengerId}", method = RequestMethod.GET)
    @ResponseBody
    public Object deletePassenger(@PathVariable Long passengerId) {
        AuthUserInfo userInfo = getUserInfo();
        return memberService.deletePassenger(passengerId, userInfo.getUserId());
    }
}
