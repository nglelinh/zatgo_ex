package com.zatgo.zup.trade.api.mapper;

import com.ykb.mall.common.model.WechatMessageRecord;
import com.ykb.mall.common.model.WechatMessageRecordExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WechatMessageRecordMapper {
    int countByExample(WechatMessageRecordExample example);

    int deleteByExample(WechatMessageRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WechatMessageRecord record);

    int insertSelective(WechatMessageRecord record);

    List<WechatMessageRecord> selectByExample(WechatMessageRecordExample example);

    WechatMessageRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WechatMessageRecord record, @Param("example") WechatMessageRecordExample example);

    int updateByExample(@Param("record") WechatMessageRecord record, @Param("example") WechatMessageRecordExample example);

    int updateByPrimaryKeySelective(WechatMessageRecord record);

    int updateByPrimaryKey(WechatMessageRecord record);
}