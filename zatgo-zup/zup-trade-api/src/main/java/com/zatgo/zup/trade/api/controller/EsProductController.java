package com.zatgo.zup.trade.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.domain.EsProduct;
import com.zatgo.zup.trade.api.domain.EsProductRelatedInfo;
import com.zatgo.zup.trade.api.domain.PmsProductFlightResult;
import com.zatgo.zup.trade.api.service.EsProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 搜索商品管理Controller
 * Created by chen on 2018/6/19.
 */
@Controller
@Api(tags = "EsProductController", description = "搜索商品管理")
//@RequestMapping("/ykbBase/esProduct")
@RequestMapping("/trade/api/esProduct")
public class EsProductController {
    @Autowired
    private EsProductService esProductService;

    @ApiOperation(value = "导入所有数据库中商品到ES")
    @RequestMapping(value = "/importAll", method = RequestMethod.POST)
    @ResponseBody
    public Object importAllList() {
        int count = esProductService.importAll();
        return new CommonResult().success(count);
    }

    @ApiOperation(value = "根据id删除商品")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object delete(@PathVariable Long id) {
        esProductService.delete(id);
        return new CommonResult().success(null);
    }

    @ApiOperation(value = "根据id批量删除商品")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.POST)
    @ResponseBody
    public Object delete(@RequestParam("ids") List<Long> ids) {
        esProductService.delete(ids);
        return new CommonResult().success(null);
    }

    @ApiOperation(value = "根据id创建商品")
    @RequestMapping(value = "/create/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object create(@PathVariable Long id) {
        EsProduct esProduct = esProductService.create(id);
        if (esProduct != null) {
            return new CommonResult().success(esProduct);
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation(value = "简单搜索")
    @RequestMapping(value = "/search/simple", method = RequestMethod.GET)
    @ResponseBody
    public Object search(@RequestParam(required = false) String keyword,
                         @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                         @RequestParam(required = false, defaultValue = "5") Integer pageSize) {
        Page<EsProduct> esProductPage = esProductService.search(keyword, pageNum, pageSize);
        return new CommonResult().pageSuccess(esProductPage);
    }

    @ApiOperation(value = "综合搜索、筛选、排序")
    @ApiImplicitParam(name = "sort", value = "排序字段:0->按相关度；1->按新品；2->按销量；3->价格从低到高；4->价格从高到低",
            defaultValue = "0", allowableValues = "0,1,2,3,4", paramType = "query", dataType = "integer")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public Object search(@RequestParam(required = false) String keyword,
                         @RequestParam(required = false) Long brandId, //品牌id
                         @RequestParam(required = false) Long productCategoryId,  //目的地id
                         @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                         @RequestParam(required = false, defaultValue = "5") Integer pageSize,
                         @RequestParam(required = false, defaultValue = "0") Integer sort,
                         @RequestParam(required = false) Byte businessType, //业务类型 1-旅游；2-机票；3-酒店；4-文章; 5-门票; 6-其他
                         @RequestParam(required = false) String moduleId,
                         @RequestParam(required = false) Long subjectId, //专题id
                         @RequestParam(required = false) Integer discountStatus, // 是否特惠 1是 0否
                         @RequestParam(required = false) Integer recommandStatus, //是否推荐  1是 0 否
                         @RequestParam(required = false) String tag) { //标签
        Page<EsProduct> esProductPage = esProductService.search(keyword, brandId, productCategoryId, pageNum, pageSize,
                sort, businessType, moduleId, subjectId, discountStatus, recommandStatus, tag);
        return new CommonResult().pageSuccess(esProductPage);
    }

    @ApiOperation(value = "综合搜索、筛选、排序")
    @ApiImplicitParam(name = "sort", value = "排序字段:0->按相关度；1->按新品；2->按销量；3->价格从低到高；4->价格从高到低",
            defaultValue = "0", allowableValues = "0,1,2,3,4", paramType = "query", dataType = "integer")
    @RequestMapping(value = "/flightSearch", method = RequestMethod.GET)
    @ResponseBody
    public Object flightSearch(@RequestParam(required = false) String keyword,
                         @RequestParam(required = false) Long brandId, //品牌id
                         @RequestParam(required = false) Long productCategoryId,  //目的地id
                         @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                         @RequestParam(required = false, defaultValue = "5") Integer pageSize,
                         @RequestParam(required = false, defaultValue = "0") Integer sort,
                         @RequestParam(required = false) String moduleId,
                         @RequestParam(required = false) Long subjectId, //专题id
                         @RequestParam(required = false) Integer discountStatus, // 是否特惠 1是 0否
                         @RequestParam(required = false) Integer recommandStatus, //是否推荐  1是 0 否
                         @RequestParam(required = false) String tag) { //标签
        JSONObject esProductPage = esProductService.flightSearch(keyword, brandId, productCategoryId, pageNum, pageSize,
                sort, new Byte("2"), moduleId, subjectId, discountStatus, recommandStatus, tag);
        return new CommonResult().success(esProductPage);
    }

    @ApiOperation(value = "根据商品id推荐商品")
    @RequestMapping(value = "/recommend/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Object recommend(@PathVariable Long id,
                            @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                            @RequestParam(required = false, defaultValue = "5") Integer pageSize){
        Page<EsProduct> esProductPage = esProductService.recommend(id, pageNum, pageSize);
        return new CommonResult().pageSuccess(esProductPage);
    }

    @ApiOperation(value = "获取搜索的相关品牌、分类及筛选属性")
    @RequestMapping(value = "/search/relate",method = RequestMethod.GET)
    @ResponseBody
    public Object searchRelatedInfo(@RequestParam(required = false) String keyword){
        EsProductRelatedInfo productRelatedInfo = esProductService.searchRelatedInfo(keyword);
        return new CommonResult().success(productRelatedInfo);
    }
    
    @ApiOperation(value = "关联商品查询")
    @RequestMapping(value = "/search/relate/product/{productId}",method = RequestMethod.GET)
    @ResponseBody
	public CommonResult searchRelatedProductId(@PathVariable Long productId, @RequestParam(required = false, defaultValue = "1") Byte relationType) {
		Page<EsProduct> esProductPage = esProductService.searchRelatedProductId(productId, relationType);
		return new CommonResult().success(esProductPage);
	}
}
