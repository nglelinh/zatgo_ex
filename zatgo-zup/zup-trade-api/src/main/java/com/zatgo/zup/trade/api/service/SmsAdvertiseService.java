package com.zatgo.zup.trade.api.service;


import com.zatgo.zup.trade.api.domain.SmsAdvertiseData;

import java.util.List;

public interface SmsAdvertiseService {
	
	List<SmsAdvertiseData> homeAdvertiseList(Integer type);

}
