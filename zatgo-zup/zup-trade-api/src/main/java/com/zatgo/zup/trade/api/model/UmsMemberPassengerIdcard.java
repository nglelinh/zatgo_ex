package com.zatgo.zup.trade.api.model;

import java.io.Serializable;
import java.util.Date;

public class UmsMemberPassengerIdcard implements Serializable {
    private Long id;

    /**
     * 乘客编号
     *
     * @mbggenerated
     */
    private Long passengerId;

    /**
     * 证件类型，1-身份证；2-护照；3-军官证；4-港澳通行证；5-其他证件
     *
     * @mbggenerated
     */
    private Byte idcardType;

    /**
     * 证件号码
     *
     * @mbggenerated
     */
    private String idcardCode;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 更新时间
     *
     * @mbggenerated
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(Long passengerId) {
        this.passengerId = passengerId;
    }

    public Byte getIdcardType() {
        return idcardType;
    }

    public void setIdcardType(Byte idcardType) {
        this.idcardType = idcardType;
    }

    public String getIdcardCode() {
        return idcardCode;
    }

    public void setIdcardCode(String idcardCode) {
        this.idcardCode = idcardCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", passengerId=").append(passengerId);
        sb.append(", idcardType=").append(idcardType);
        sb.append(", idcardCode=").append(idcardCode);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}