package com.zatgo.zup.trade.api.mapper;

import com.ykb.mall.common.model.PmsProductGroupTransport;
import com.ykb.mall.common.model.PmsProductGroupTransportExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductGroupTransportMapper {
    int countByExample(PmsProductGroupTransportExample example);

    int deleteByExample(PmsProductGroupTransportExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductGroupTransport record);

    int insertSelective(PmsProductGroupTransport record);

    List<PmsProductGroupTransport> selectByExample(PmsProductGroupTransportExample example);

    PmsProductGroupTransport selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductGroupTransport record, @Param("example") PmsProductGroupTransportExample example);

    int updateByExample(@Param("record") PmsProductGroupTransport record, @Param("example") PmsProductGroupTransportExample example);

    int updateByPrimaryKeySelective(PmsProductGroupTransport record);

    int updateByPrimaryKey(PmsProductGroupTransport record);
}