package com.zatgo.zup.trade.api.controller;

import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductArticleDetail;
import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.service.PmsProductArticleService;
import com.zatgo.zup.trade.api.service.PmsProductGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static com.zatgo.zup.common.model.WeixinConstants.detail;

/**
 * @Author wangyucong
 * @Date 2019/2/16 10:52
 */
@RestController
@Api(tags = "PmsProductArticleController", description = "文章信息")
@RequestMapping("/trade/api/pmsArticle")
public class PmsProductArticleController {

    @Resource
    private PmsProductArticleService pmsProductArticleService;

    @Resource
    private PmsProductGroupService pmsProductGroupService;

    @ApiOperation(value = "获取产品类型详情")
    @RequestMapping(value = "/detail/{id}",method = RequestMethod.GET)
    public Object selectProductDetail(@ApiParam("产品编号") @PathVariable Long id) {
    	PmsProductArticleDetail detail = pmsProductArticleService.selectProductArticleDetail(id);
        return new CommonResult().success(detail);
    }

    @ApiOperation(value = "获取关联活动的文章列表")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public Object activityArticleList(@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        PageInfo<PmsProduct> info = pmsProductArticleService.activityArticleList(pageNo, pageSize);
        return new CommonResult().success(info);
    }
}