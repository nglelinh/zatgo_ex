package com.zatgo.zup.trade.api.service;

import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.domain.PassengerIdcardParams;
import com.zatgo.zup.trade.api.domain.PassengerParams;
import org.springframework.transaction.annotation.Transactional;

/**
 * 会员管理Service
 * Created by chen on 2018/8/3.
 */
public interface UmsMemberService {
    /**
     * 根据用户名获取会员
     */
//    UmsMember getByUsername(String username);

    /**
     * 登录
     * @param param
     * @return
     */
//    CommonResult login(UmsMemberParam param);

    /**
     * 微信注册
     * @return
     */
//    CommonResult wxRigester(UmsMemberParam param);

    /**
     * 根据会员编号获取会员
     */
//    UmsMember getById(Long id);

    /**
     * 用户注册
     */
//    @Transactional
//    CommonResult register(String username, String password, String telephone, String authCode);

    /**
     * 生成验证码
     */
    CommonResult generateAuthCode(String telephone);

    /**
     * 修改密码
     */
//    @Transactional
//    CommonResult updatePassword(String telephone, String password, String authCode);

    /**
     * 获取当前登录会员
     */
//    UmsMember getCurrentMember();

    /**
     * 根据会员id修改会员积分
     */
//    void updateIntegration(Long id, Integer integration);
    
    /**
     * 添加出行人
     * @param params
     * @return
     */
    CommonResult addPassenger(PassengerParams params, String userId);
    
    /**
     * 更新出行人证件信息
     * @param idCardId
     * @param params
     * @return
     */
    CommonResult updatePassengerIdCard(Long idCardId, PassengerIdcardParams params, String userId);
    
    /**
     * 新增出行人证件
     * @param params
     * @return
     */
    CommonResult addPassengerIdCard(Long passengerId, PassengerIdcardParams params, String userId);
    
    /**
     * 更新出行人信息
     * @param passengerId
     * @param params
     * @return
     */
    CommonResult updatePassenger(Long passengerId, PassengerParams params, String userId);
    
    /**
     * 出行人列表
     * @param name
     * @return
     */
    CommonResult passengerList(String name, String userId);
    
    /**
     * 删除乘客
     * @param passengerId
     * @return
     */
    CommonResult deletePassenger(Long passengerId, String userId);
}
