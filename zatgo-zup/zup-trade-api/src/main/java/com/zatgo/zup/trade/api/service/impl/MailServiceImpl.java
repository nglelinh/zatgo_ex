//package com.zatgo.zup.trade.api.service.impl;
//
//import com.zatgo.zup.common.exception.BusinessException;
//import com.ykb.mall.common.exception.BusinessExceptionCode;
//import com.zatgo.zup.trade.api.mapper.MessageTemplateMapper;
//import com.ykb.mall.common.model.MessageTemplate;
//import com.ykb.mall.common.model.MessageTemplateExample;
//import com.zatgo.zup.trade.api.service.MailService;
//import com.zatgo.zup.trade.api.service.MessageService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.MimeMessageHelper;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
//import org.springframework.stereotype.Service;
//import org.springframework.util.CollectionUtils;
//import org.springframework.util.StringUtils;
//
//import javax.annotation.Resource;
//import javax.mail.internet.MimeMessage;
//import java.util.List;
//import java.util.Map;
//
///**
// * @Author wangyucong
// * @Date 2019/3/25 16:52
// */
//@Service
//public class MailServiceImpl implements MailService {
//
//    private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);
//
//    @Value("${mail.from}")
//    private String MAIL_FROM;
//
//    @Autowired
//    private JavaMailSender mailSender;
//
//    @Resource
//    private MessageService messageService;
//
//    @Resource
//    private MessageTemplateMapper messageTemplateMapper;
//
//    @Autowired
//    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
//
//    /**
//     * 发送邮件
//     *
//     * @param subject
//     * @param text
//     */
//    @Override
//    public Byte sendHtmlMail(String receiveEmail, String subject, String text) {
//        try {
//            MimeMessage mimeMessage = mailSender.createMimeMessage();
//            //是否发送的邮件是富文本（附件，图片，html等）
//            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
//            messageHelper.setFrom(MAIL_FROM);
//            messageHelper.setTo(receiveEmail);
//            messageHelper.setSubject(subject);
//            messageHelper.setText(text, true);//重点，默认为false，显示原始html代码，无效果
//            mailSender.send(mimeMessage);
//            return 1;
//        } catch (Exception e) {
//            logger.error("邮件发送失败，", e);
//            return 2;
//        }
//    }
//
//    /**
//     * 发送邮件
//     *
//     * @param templateCode
//     * @param receiver
//     * @param contentMap
//     */
//    @Override
//    public void sendEmail(String templateCode, String receiver, Map<String, String> contentMap) {
//
//    	try {
//    		threadPoolTaskExecutor.execute(() -> {
//        		MessageTemplateExample example = new MessageTemplateExample();
//                example.createCriteria().andTemplateCodeEqualTo(templateCode);
//                List<MessageTemplate> templates = messageTemplateMapper.selectByExample(example);
//                if (CollectionUtils.isEmpty(templates)) {
//                    throw new BusinessException(BusinessExceptionCode.MESSAGE_TEMPLATE_ERROR, "未找到相应消息模板");
//                }
//                MessageTemplate template = templates.get(0);
//                String messageContent = messageService.getMessageContent(template.getTemplateContent(), contentMap);
//                if (StringUtils.isEmpty(messageContent)) {
//                    throw new BusinessException(BusinessExceptionCode.MESSAGE_TEMPLATE_ERROR, "消息内容为空");
//                }
//                // 发邮件
//                Byte result = sendHtmlMail(receiver, template.getTemplateName(), messageContent);
//                // 新增消息记录
//                messageService.createMessageRecord(template, receiver, messageContent, result, null);
//        	});
//		} catch (Exception e) {
//			logger.error("", e);
//		}
//
//    }
//}