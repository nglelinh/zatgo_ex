package com.zatgo.zup.trade.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel("酒店价格")
public class PmsHotelData {
	
	private Long id;
	
	@ApiModelProperty("酒店名称")
	private String hotelName;
	
	@ApiModelProperty("房型")
	private String roomType;
	
	@ApiModelProperty("入住日期")
	private String arrivalDate;
	
	@ApiModelProperty("销售价")
	private BigDecimal salePrice;
	
	@ApiModelProperty("入住人数")
	private Integer guestNumber;
	
	@ApiModelProperty("状态（0 -待发布，1 - 已上架，2 - 已下架）")
	private Byte status;
	
	@ApiModelProperty("是否为删除状态（0 - 否，1 - 是）")
	private Byte deleteStatus;
	
	@ApiModelProperty("库存")
	private Integer stock;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public BigDecimal getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	public Integer getGuestNumber() {
		return guestNumber;
	}

	public void setGuestNumber(Integer guestNumber) {
		this.guestNumber = guestNumber;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Byte getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Byte deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

}
