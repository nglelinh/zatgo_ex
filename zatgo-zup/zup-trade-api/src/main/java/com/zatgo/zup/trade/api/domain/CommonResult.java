package com.zatgo.zup.trade.api.domain;

import com.zatgo.zup.common.exception.BusinessExceptionCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.domain.Page;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用返回对象
 * Created by chen on 2018/4/26.
 */
@ApiModel("返回数据")
public class CommonResult {
    //操作成功
    public static final String SUCCESS = BusinessExceptionCode.SUCCESS_CODE;
    //操作失败
    public static final String FAILED = BusinessExceptionCode.SYSTEM_ERROR;
    // 未知的微信用户
    public static final String WX_REGISTER = "401";
    private String code;
    private String message;
    @ApiModelProperty("返回信息")
    private Object data;

    /**
     * 普通成功返回
     *
     * @param data 获取的数据
     */
    public CommonResult success(Object data) {
        this.code = SUCCESS;
        this.message = "操作成功";
        this.data = data;
        return this;
    }

    /**
     * 普通成功返回
     */
    public CommonResult success(String message,Object data) {
        this.code = SUCCESS;
        this.message = message;
        this.data = data;
        return this;
    }

    /**
     * 返回分页成功数据
     */
    public CommonResult pageSuccess(Page pageInfo) {
        Map<String, Object> result = new HashMap<>();
        result.put("pageSize", pageInfo.getSize());
        result.put("totalPage", pageInfo.getTotalPages());
        result.put("total", pageInfo.getTotalElements());
        result.put("pageNum", pageInfo.getNumber());
        result.put("list", pageInfo.getContent());
        this.code = SUCCESS;
        this.message = "操作成功";
        this.data = result;
        return this;
    }
    
    public <T> CommonResult pageSuccess(com.github.pagehelper.Page<T> pageInfo) {
        Map<String, Object> result = new HashMap<>();
        result.put("pageSize", pageInfo.getPageSize());
        result.put("totalPage", pageInfo.getPages());
        result.put("total", pageInfo.getTotal());
        result.put("pageNum", pageInfo.getPageNum());
        result.put("list", pageInfo.getResult());
        this.code = SUCCESS;
        this.message = "操作成功";
        this.data = result;
        return this;
    }

    /**
     * 普通失败提示信息
     */
    public CommonResult failed() {
        this.code = FAILED;
        this.message = "操作失败";
        return this;
    }

    public CommonResult failed(String message){
        this.code = FAILED;
        this.message = message;
        return this;
    }

    /**
     * 未知的微信用户，用于提示前端绑定微信
     * @param message
     * @return
     */
    public CommonResult refused(String message){
        this.code = WX_REGISTER;
        this.message = message;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
