package com.zatgo.zup.trade.api.service.impl;

import com.zatgo.zup.trade.api.dao.PmsProductGroupDao;
import com.zatgo.zup.trade.api.domain.PmsProductGroupData;
import com.zatgo.zup.trade.api.service.PmsProductGroupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/4/4 11:07
 */
@Service
public class PmsProductGroupServiceImpl implements PmsProductGroupService {

    @Resource
    private PmsProductGroupDao pmsProductGroupDao;

    /**
     * 产品班期列表
     *
     * @param productId
     * @return
     */
    @Override
    public List<PmsProductGroupData> selectProductGroupList(Long productId) {
        return pmsProductGroupDao.selectProductGroupList(productId);
    }
}