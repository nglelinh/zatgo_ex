package com.zatgo.zup.trade.api.service;

import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductArticleDetail;

public interface PmsProductArticleService {
	
	/**
     * 产品详情
     * @param id
     * @return
     */
	PmsProductArticleDetail selectProductArticleDetail(Long id);

    PageInfo<PmsProduct> activityArticleList(Integer pageNo, Integer pageSize);
}
