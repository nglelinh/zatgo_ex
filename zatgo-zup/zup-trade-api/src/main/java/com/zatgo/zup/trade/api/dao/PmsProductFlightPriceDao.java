package com.zatgo.zup.trade.api.dao;

import com.ykb.mall.common.model.PmsProductFlightPrice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductFlightPriceDao {

	List<PmsProductFlightPrice> selectProductFlightMinList(@Param("flightId") Long flightId,
                                                           @Param("startDate") String startDate, @Param("endDate") String endDate);

}
