package com.zatgo.zup.trade.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/7/31.
 */
public class TicketDateList {

    List<TicketList> list;

    private BigDecimal price;

    private Date startDate;

    private String skuAttr;

    private String productId;

    public List<TicketList> getList() {
        return list;
    }

    public void setList(List<TicketList> list) {
        this.list = list;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getSkuAttr() {
        return skuAttr;
    }

    public void setSkuAttr(String skuAttr) {
        this.skuAttr = skuAttr;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
