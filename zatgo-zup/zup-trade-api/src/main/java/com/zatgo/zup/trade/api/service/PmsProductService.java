package com.zatgo.zup.trade.api.service;

import com.ykb.mall.common.model.PmsBrand;
import com.ykb.mall.common.model.PmsProductCategoryData;
import com.ykb.mall.common.model.PmsProductDetail;
import com.ykb.mall.common.model.PmsProductGroup;
import com.zatgo.zup.trade.api.model.PmsProduct;

import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/16 10:55
 */
public interface PmsProductService {

    /**
     * 获取产品品牌列表
     * @return
     */
    List<PmsBrand> selectBrandList();

    /**
     * 获取产品类型列表
     * @return
     */
    List<PmsProductCategoryData> selectProductCategoryList();

    /**
     * 产品详情
     * @param id
     * @return
     */
    PmsProductDetail selectProductDetail(Long id);
    
    /**
     * 获取产品类型列表
     * @return
     */
    List<PmsProductCategoryData> selectProductAllCategoryList();
    
    /**
     * 增加产品阅读数
     */
    void addProductClickNum(Long productId);
    
    /**
     * 点赞
     * @param productId
     */
    void addProductLikeNum(Long productId, String userId);
    
    /**
     * 查询目的地详情
     * @param id
     * @return
     */
    PmsProductCategoryData selectProductCategoryDetail(Long id);

    /**
     * 根据产品id获取产品信息
     * @param id
     * @return
     */
    PmsProduct getProductInfo(Long id);

    List<PmsProduct> getProductInfoList(List<Long> ids);

    PmsProductGroup getTravelGroupInfo(Long id);
}
