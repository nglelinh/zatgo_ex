package com.zatgo.zup.trade.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ykb.mall.common.model.CmsSubjectProductRelation;
import com.ykb.mall.common.model.CmsSubjectProductRelationExample;
import com.ykb.mall.common.model.PmsProductRelation;
import com.ykb.mall.common.model.PmsProductRelationExample;
import com.zatgo.zup.trade.api.dao.EsProductDao;
import com.zatgo.zup.trade.api.domain.EsProduct;
import com.zatgo.zup.trade.api.domain.EsProductRelatedInfo;
import com.zatgo.zup.trade.api.domain.PmsProductFlightResult;
import com.zatgo.zup.trade.api.mapper.CmsSubjectProductRelationMapper;
import com.zatgo.zup.trade.api.mapper.PmsProductRelationMapper;
import com.zatgo.zup.trade.api.repository.EsProductRepository;
import com.zatgo.zup.trade.api.service.EsProductService;
import com.zatgo.zup.trade.api.service.PmsProductFlightService;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.lucene.search.function.FieldValueFactorFunction;
import org.elasticsearch.common.lucene.search.function.FunctionScoreQuery;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FieldValueFactorFunctionBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.InternalFilter;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 商品搜索管理Service实现类
 * Created by chen on 2018/6/19.
 */
@Service
public class EsProductServiceImpl implements EsProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EsProductServiceImpl.class);
    @Autowired
    private EsProductDao productDao;
    @Autowired
    private EsProductRepository productRepository;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private PmsProductRelationMapper pmsProductRelationMapper;
    @Autowired
    private CmsSubjectProductRelationMapper cmsSubjectProductRelationMapper;
    @Autowired
    private PmsProductFlightService pmsProductFlightService;
    
    @Override
    public int importAll() {
        List<EsProduct> esProductList = productDao.getAllEsProductList(null);
        Iterable<EsProduct> esProductIterable = productRepository.saveAll(esProductList);
        Iterator<EsProduct> iterator = esProductIterable.iterator();
        int result = 0;
        while (iterator.hasNext()) {
            result++;
            iterator.next();
        }
        return result;
    }

    @Override
    public void delete(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public EsProduct create(Long id) {
        EsProduct result = null;
        List<EsProduct> esProductList = productDao.getAllEsProductList(id);
        if (esProductList.size() > 0) {
            EsProduct esProduct = esProductList.get(0);
            result = productRepository.save(esProduct);
        }
        return result;
    }

    @Override
    public void delete(List<Long> ids) {
        if (!CollectionUtils.isEmpty(ids)) {
            List<EsProduct> esProductList = new ArrayList<>();
            for (Long id : ids) {
                EsProduct esProduct = new EsProduct();
                esProduct.setId(id);
                esProductList.add(esProduct);
            }
            productRepository.deleteAll(esProductList);
        }
    }

    @Override
    public Page<EsProduct> search(String keyword, Integer pageNum, Integer pageSize) {
        pageNum = pageNum.intValue() < 1 ? 1 : pageNum;
        pageNum = pageNum.intValue() - 1;
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        return productRepository.findByNameOrSubTitleOrKeywords(keyword, keyword, keyword, pageable);
    }

//    @Override
//    public Page<EsProduct> search(String keyword, Long brandId, Long productCategoryId, Integer pageNum, Integer pageSize,Integer sort) {
//        Pageable pageable = PageRequest.of(pageNum, pageSize);
//        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
//        //分页
//        nativeSearchQueryBuilder.withPageable(pageable);
//        //过滤
//        if (brandId != null || productCategoryId != null) {
//            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//            if (brandId != null) {
//                boolQueryBuilder.must(QueryBuilders.termQuery("brandId", brandId));
//            }
//            if (productCategoryId != null) {
//                boolQueryBuilder.must(QueryBuilders.termQuery("productCategoryId", productCategoryId));
//            }
//            nativeSearchQueryBuilder.withFilter(boolQueryBuilder);
//        }
//        //搜索
//        FieldValueFactorFunctionBuilder nameField = ScoreFunctionBuilders.fieldValueFactorFunction("name")
//                .modifier(FieldValueFactorFunction.Modifier.LN1P).factor(10);
//        FieldValueFactorFunctionBuilder subTitleField = ScoreFunctionBuilders.fieldValueFactorFunction("subTitle")
//                .modifier(FieldValueFactorFunction.Modifier.LN1P).factor(5);
//        FieldValueFactorFunctionBuilder keywordsField = ScoreFunctionBuilders.fieldValueFactorFunction("keywords")
//                .modifier(FieldValueFactorFunction.Modifier.LN1P).factor(2);
//
//        FunctionScoreQueryBuilder.FilterFunctionBuilder[] filterFunctionBuilders
//                = new FunctionScoreQueryBuilder.FilterFunctionBuilder[]{
//                new FunctionScoreQueryBuilder.FilterFunctionBuilder(nameField),
//                new FunctionScoreQueryBuilder.FilterFunctionBuilder(subTitleField),
//                new FunctionScoreQueryBuilder.FilterFunctionBuilder(keywordsField)};
//
//        FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(filterFunctionBuilders)
//                .scoreMode(FunctionScoreQuery.ScoreMode.SUM)
//                .setMinScore(2);
//        if (StringUtils.isEmpty(keyword)) {
//            nativeSearchQueryBuilder.withQuery(QueryBuilders.matchAllQuery());
//        } else {
//            nativeSearchQueryBuilder.withQuery(functionScoreQueryBuilder);
//        }
//        //排序
//        if(sort==1){
//            //按新品从新到旧
//            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("id").order(SortOrder.DESC));
//        }else if(sort==2){
//            //按销量从高到低
//            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("sale").order(SortOrder.DESC));
//        }else if(sort==3){
//            //按价格从低到高
//            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("price").order(SortOrder.ASC));
//        }else if(sort==4){
//            //按价格从高到低
//            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("price").order(SortOrder.DESC));
//        }else{
//            //按相关度
//            nativeSearchQueryBuilder.withSort(SortBuilders.scoreSort().order(SortOrder.DESC));
//        }
//        nativeSearchQueryBuilder.withSort(SortBuilders.scoreSort().order(SortOrder.DESC));
//        nativeSearchQueryBuilder.withSort(SortBuilders.scoreSort().order(SortOrder.DESC));
//        NativeSearchQuery searchQuery = nativeSearchQueryBuilder.build();
//        LOGGER.info("DSL:{}", searchQuery.getQuery().toString());
//        return productRepository.search(searchQuery);
//    }

    @Override
    public Page<EsProduct> search(String keyword, Long brandId, Long productCategoryId, Integer pageNum,
                                  Integer pageSize, Integer sort, Byte businessType, String moduleId, Long subjectId,
                                  Integer discountStatus, Integer recommandStatus, String tag) {
        pageNum = pageNum.intValue() < 1 ? 1 : pageNum;
        pageNum = pageNum.intValue() - 1;
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        // 分页
        nativeSearchQueryBuilder.withPageable(pageable);
        // 设置条件
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (!StringUtils.isEmpty(keyword) || null != brandId || null != productCategoryId || null != businessType ||
                null != moduleId || null != subjectId || discountStatus != null || recommandStatus != null || tag != null) {
            if (null != brandId) {
                boolQueryBuilder.must(QueryBuilders.termQuery("brandId", brandId));
            }
            if (null != productCategoryId) {
                boolQueryBuilder.must(QueryBuilders.termQuery("productCategoryId", productCategoryId));
            }
            if (!StringUtils.isEmpty(keyword)) {
                boolQueryBuilder.must(QueryBuilders.multiMatchQuery(keyword, "name", "subTitle", "keywords", "productCategoryName", "tag").type(MultiMatchQueryBuilder.Type.MOST_FIELDS));
            }
            if (null != businessType) {
                boolQueryBuilder.must(QueryBuilders.termQuery("businessType", businessType));
            }
            if ( discountStatus != null) {
                boolQueryBuilder.must(QueryBuilders.termQuery("discountStatus", discountStatus));
            }
            if ( recommandStatus != null) {
                boolQueryBuilder.must(QueryBuilders.termQuery("recommandStatus", recommandStatus));
            }
            if (null != moduleId) {
                boolQueryBuilder.must(QueryBuilders.wildcardQuery("moduleId", "*" + moduleId + "*"));

            }
            if (null != tag) {
                boolQueryBuilder.must(QueryBuilders.wildcardQuery("tag", "*" + tag + "*"));
            }
            if (null != subjectId) {
            	CmsSubjectProductRelationExample example = new CmsSubjectProductRelationExample();
            	example.createCriteria().andSubjectIdEqualTo(subjectId);
            	List<CmsSubjectProductRelation> relations = cmsSubjectProductRelationMapper.selectByExample(example);
            	
            	List<Long> productIds = new ArrayList<>();
            	if(!CollectionUtils.isEmpty(relations)) {
            		productIds = relations.stream().map(CmsSubjectProductRelation::getProductId).collect(Collectors.toList());
            	}
            	
            	boolQueryBuilder.must(QueryBuilders.termsQuery("id", productIds));
            }
        } else {
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        }
        boolQueryBuilder.must(QueryBuilders.termQuery("deleteStatus", 0));
        boolQueryBuilder.must(QueryBuilders.termQuery("publishStatus", 1));
        nativeSearchQueryBuilder.withQuery(boolQueryBuilder);
        // 排序
        if (sort == 1) {
            //按新品从新到旧
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("id").order(SortOrder.DESC));
        } else if (sort == 2) {
            //按销量从高到低
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("sale").order(SortOrder.DESC));
        } else if (sort == 3) {
            //按价格从低到高
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("price").order(SortOrder.ASC));
        } else if (sort == 4) {
            //按价格从高到低
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("price").order(SortOrder.DESC));
        } else {
            //按sort字段
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("sort").order(SortOrder.DESC));
        }
        NativeSearchQuery searchQuery = nativeSearchQueryBuilder.build();
        LOGGER.info("DSL:{}", searchQuery.getQuery().toString());
        return productRepository.search(searchQuery);
    }

    @Override
    public JSONObject flightSearch(String keyword, Long brandId, Long productCategoryId, Integer pageNum,
                                   Integer pageSize, Integer sort, Byte businessType, String moduleId, Long subjectId,
                                   Integer discountStatus, Integer recommandStatus, String tag) {
        pageNum = pageNum.intValue() < 1 ? 1 : pageNum;
        pageNum = pageNum.intValue() - 1;
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        // 分页
        nativeSearchQueryBuilder.withPageable(pageable);
        // 设置条件
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (!StringUtils.isEmpty(keyword) || null != brandId || null != productCategoryId || null != businessType ||
                null != moduleId || null != subjectId || discountStatus != null || recommandStatus != null || tag != null) {
            if (null != brandId) {
                boolQueryBuilder.must(QueryBuilders.termQuery("brandId", brandId));
            }
            if (null != productCategoryId) {
                boolQueryBuilder.must(QueryBuilders.termQuery("productCategoryId", productCategoryId));
            }
            if (!StringUtils.isEmpty(keyword)) {
                boolQueryBuilder.must(QueryBuilders.multiMatchQuery(keyword, "name", "subTitle", "keywords", "productCategoryName", "tag").type(MultiMatchQueryBuilder.Type.MOST_FIELDS));
            }
            if (null != businessType) {
                boolQueryBuilder.must(QueryBuilders.termQuery("businessType", businessType));
            }
            if ( discountStatus != null) {
                boolQueryBuilder.must(QueryBuilders.termQuery("discountStatus", discountStatus));
            }
            if ( recommandStatus != null) {
                boolQueryBuilder.must(QueryBuilders.termQuery("recommandStatus", recommandStatus));
            }
            if (null != moduleId) {
                boolQueryBuilder.must(QueryBuilders.wildcardQuery("moduleId", "*" + moduleId + "*"));

            }
            if (null != tag) {
                boolQueryBuilder.must(QueryBuilders.wildcardQuery("tag", "*" + tag + "*"));
            }
            if (null != subjectId) {
                CmsSubjectProductRelationExample example = new CmsSubjectProductRelationExample();
                example.createCriteria().andSubjectIdEqualTo(subjectId);
                List<CmsSubjectProductRelation> relations = cmsSubjectProductRelationMapper.selectByExample(example);

                List<Long> productIds = new ArrayList<>();
                if(!CollectionUtils.isEmpty(relations)) {
                    productIds = relations.stream().map(CmsSubjectProductRelation::getProductId).collect(Collectors.toList());
                }

                boolQueryBuilder.must(QueryBuilders.termsQuery("id", productIds));
            }
        } else {
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        }
        boolQueryBuilder.must(QueryBuilders.termQuery("deleteStatus", 0));
        boolQueryBuilder.must(QueryBuilders.termQuery("publishStatus", 1));
        nativeSearchQueryBuilder.withQuery(boolQueryBuilder);
        // 排序
        if (sort == 1) {
            //按新品从新到旧
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("id").order(SortOrder.DESC));
        } else if (sort == 2) {
            //按销量从高到低
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("sale").order(SortOrder.DESC));
        } else if (sort == 3) {
            //按价格从低到高
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("price").order(SortOrder.ASC));
        } else if (sort == 4) {
            //按价格从高到低
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("price").order(SortOrder.DESC));
        } else {
            //按sort字段
            nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("sort").order(SortOrder.DESC));
        }
        NativeSearchQuery searchQuery = nativeSearchQueryBuilder.build();
        LOGGER.info("DSL:{}", searchQuery.getQuery().toString());
        Page<EsProduct> search = productRepository.search(searchQuery);
        if (search != null){
            List<EsProduct> content = search.getContent();
            if (!CollectionUtils.isEmpty(content)){
                List<Long> ids = new ArrayList<>();
                Map<Long, Integer> map = new HashMap<>();
                for (int i = 0; i < content.size(); i++) {
                    EsProduct esProduct = content.get(i);
                    Long id = esProduct.getId();
                    ids.add(id);
                    map.put(id, i);
                }
                List<PmsProductFlightResult> pmsProductFlightResults = pmsProductFlightService.selectProductFlightDetail(ids);
                PmsProductFlightResult[] resList = new PmsProductFlightResult [pmsProductFlightResults.size()];
                for (PmsProductFlightResult pmsProductFlightResult : pmsProductFlightResults) {
                    Long id = pmsProductFlightResult.getId();
                    Integer index = map.get(id);
                    resList [index] = pmsProductFlightResult;
                }

                JSONObject res = new JSONObject();
                res.put("pageNum", pageNum);
                res.put("pageSize", pageSize);
                res.put("list", resList);
                return res;
            }
        }
        return null;
    }

    @Override
    public Page<EsProduct> recommend(Long id, Integer pageNum, Integer pageSize) {
        pageNum = pageNum.intValue() < 1 ? 1 : pageNum;
        pageNum = pageNum.intValue() - 1;
        Pageable pageable = new PageRequest(pageNum, pageSize);
        List<EsProduct> esProductList = productDao.getAllEsProductList(id);
        if (esProductList.size() > 0) {
            EsProduct esProduct = esProductList.get(0);
            String keyword = esProduct.getName();
            Long brandId = esProduct.getBrandId();
            Long productCategoryId = esProduct.getProductCategoryId();
            //根据商品标题、品牌、分类进行搜索
            FieldValueFactorFunctionBuilder nameField = ScoreFunctionBuilders.fieldValueFactorFunction("name")
                    .modifier(FieldValueFactorFunction.Modifier.LN1P).factor(8);
            FieldValueFactorFunctionBuilder subTitleField = ScoreFunctionBuilders.fieldValueFactorFunction("subTitle")
                    .modifier(FieldValueFactorFunction.Modifier.LN1P).factor(2);
            FieldValueFactorFunctionBuilder keywordsField = ScoreFunctionBuilders.fieldValueFactorFunction("keywords")
                    .modifier(FieldValueFactorFunction.Modifier.LN1P).factor(2);
            FieldValueFactorFunctionBuilder brandIdField = ScoreFunctionBuilders.fieldValueFactorFunction("brandId")
                    .modifier(FieldValueFactorFunction.Modifier.LN1P).factor(10);
            FieldValueFactorFunctionBuilder productCategoryIdField = ScoreFunctionBuilders.fieldValueFactorFunction("productCategoryId")
                    .modifier(FieldValueFactorFunction.Modifier.LN1P).factor(6);

            FunctionScoreQueryBuilder.FilterFunctionBuilder[] filterFunctionBuilders
                    = new FunctionScoreQueryBuilder.FilterFunctionBuilder[]{
                    new FunctionScoreQueryBuilder.FilterFunctionBuilder(nameField),
                    new FunctionScoreQueryBuilder.FilterFunctionBuilder(subTitleField),
                    new FunctionScoreQueryBuilder.FilterFunctionBuilder(keywordsField),
                    new FunctionScoreQueryBuilder.FilterFunctionBuilder(brandIdField),
                    new FunctionScoreQueryBuilder.FilterFunctionBuilder(productCategoryIdField)
            };


            FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(filterFunctionBuilders)
                    .scoreMode(FunctionScoreQuery.ScoreMode.SUM)
                    .setMinScore(2);
            NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
            builder.withQuery(functionScoreQueryBuilder);
            builder.withPageable(pageable);
            NativeSearchQuery searchQuery = builder.build();
            LOGGER.info("DSL:{}", searchQuery.getQuery().toString());
            return productRepository.search(searchQuery);
        }
        return new PageImpl<>(null);
    }

    @Override
    public EsProductRelatedInfo searchRelatedInfo(String keyword) {
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        //搜索条件
        if(StringUtils.isEmpty(keyword)){
            builder.withQuery(QueryBuilders.matchAllQuery());
        }else{
            builder.withQuery(QueryBuilders.multiMatchQuery(keyword,"name","subTitle","keywords"));
        }
        //聚合搜索品牌名称
        builder.addAggregation(AggregationBuilders.terms("brandNames").field("brandName"));
        //集合搜索分类名称
        builder.addAggregation(AggregationBuilders.terms("productCategoryNames").field("productCategoryName"));
        //聚合搜索商品属性，去除type=1的属性
        AbstractAggregationBuilder aggregationBuilder = AggregationBuilders.nested("allAttrValues","attrValueList")
                .subAggregation(AggregationBuilders.filter("productAttrs",QueryBuilders.termQuery("attrValueList.type",1))
                .subAggregation(AggregationBuilders.terms("attrIds")
                        .field("attrValueList.productAttributeId")
                        .subAggregation(AggregationBuilders.terms("attrValues")
                                .field("attrValueList.value"))
                        .subAggregation(AggregationBuilders.terms("attrNames")
                                .field("attrValueList.name"))));
        builder.addAggregation(aggregationBuilder);
        NativeSearchQuery searchQuery = builder.build();
        return elasticsearchTemplate.query(searchQuery, response -> {
            LOGGER.info("DSL:{}",searchQuery.getQuery().toString());
            return convertProductRelatedInfo(response);
        });
    }

    /**
     * 将返回结果转换为对象
     */
    private EsProductRelatedInfo convertProductRelatedInfo(SearchResponse response) {
        EsProductRelatedInfo productRelatedInfo = new EsProductRelatedInfo();
        Map<String, Aggregation> aggregationMap = response.getAggregations().getAsMap();
        //设置品牌
        Aggregation brandNames = aggregationMap.get("brandNames");
        List<String> brandNameList = new ArrayList<>();
        for(int i = 0; i<((StringTerms) brandNames).getBuckets().size(); i++){
            brandNameList.add(((StringTerms) brandNames).getBuckets().get(i).getKeyAsString());
        }
        productRelatedInfo.setBrandNames(brandNameList);
        //设置分类
        Aggregation productCategoryNames = aggregationMap.get("productCategoryNames");
        List<String> productCategoryNameList = new ArrayList<>();
        for(int i=0;i<((StringTerms) productCategoryNames).getBuckets().size();i++){
            productCategoryNameList.add(((StringTerms) productCategoryNames).getBuckets().get(i).getKeyAsString());
        }
        productRelatedInfo.setProductCategoryNames(productCategoryNameList);
        //设置参数
        Aggregation productAttrs = aggregationMap.get("allAttrValues");
        List<LongTerms.Bucket> attrIds = ((LongTerms) ((InternalFilter)productAttrs.getMetaData().get("productAttrs")).getAggregations().getAsMap().get("attrIds")).getBuckets();
        List<EsProductRelatedInfo.ProductAttr> attrList = new ArrayList<>();
        for (Terms.Bucket attrId : attrIds) {
            EsProductRelatedInfo.ProductAttr attr = new EsProductRelatedInfo.ProductAttr();
            attr.setAttrId((Long) attrId.getKey());
            List<String> attrValueList = new ArrayList<>();
            List<StringTerms.Bucket> attrValues = ((StringTerms) attrId.getAggregations().get("attrValues")).getBuckets();
            List<StringTerms.Bucket> attrNames = ((StringTerms) attrId.getAggregations().get("attrNames")).getBuckets();
            for (Terms.Bucket attrValue : attrValues) {
                attrValueList.add(attrValue.getKeyAsString());
            }
            attr.setAttrValues(attrValueList);
            if(!CollectionUtils.isEmpty(attrNames)){
                String attrName = attrNames.get(0).getKeyAsString();
                attr.setAttrName(attrName);
            }
            attrList.add(attr);
        }
        productRelatedInfo.setProductAttrs(attrList);
        return productRelatedInfo;
    }

	@Override
	public Page<EsProduct> searchRelatedProductId(Long productId, Byte relationType) {
		PmsProductRelationExample example = new PmsProductRelationExample();
		example.createCriteria().andProductIdEqualTo(productId).andRelationTypeEqualTo(relationType);

		List<PmsProductRelation> relations = pmsProductRelationMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(relations)) {
			return null;
		}

		List<Long> productIds = relations.stream().map(PmsProductRelation::getSlaveProductId).collect(Collectors.toList());

//		Pageable pageable = PageRequest.of(pageNum, pageSize);
		NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
//		nativeSearchQueryBuilder.withPageable(pageable);
		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
		boolQueryBuilder.must(QueryBuilders.termsQuery("id", productIds));
		boolQueryBuilder.must(QueryBuilders.termQuery("deleteStatus", 0));
		boolQueryBuilder.must(QueryBuilders.termQuery("publishStatus", 1));
		nativeSearchQueryBuilder.withQuery(boolQueryBuilder);
		nativeSearchQueryBuilder.withSort(SortBuilders.fieldSort("id").order(SortOrder.DESC));
		NativeSearchQuery searchQuery = nativeSearchQueryBuilder.build();
		return productRepository.search(searchQuery);
	}
}
