package com.zatgo.zup.trade.api.mq.aliyunmq.listener;//package com.ykb.mall.mapi.mq.aliyunmq.listener;
//
//import com.aliyun.openservices.ons.api.Action;
//import com.aliyun.openservices.ons.api.ConsumeContext;
//import com.aliyun.openservices.ons.api.Message;
//import com.aliyun.openservices.ons.api.MessageListener;
//import com.ykb.mall.mapi.mq.MQConsumerCallBack;
//import org.apache.rocketmq.remoting.common.RemotingHelper;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.UnsupportedEncodingException;
//
//
//public class NoticeListener implements MessageListener {
//
//    private Logger logger = LoggerFactory.getLogger(getClass().getName());
//
//    private MQConsumerCallBack callBack;
//
//    public NoticeListener(MQConsumerCallBack callBack) {
//    	this.callBack = callBack;
//    }
//    @Override
//    public Action consume(Message message, ConsumeContext consumeContext) {
//
//        String body = null;
//        try {
//            body = new String(message.getBody(), RemotingHelper.DEFAULT_CHARSET);
//        } catch (UnsupportedEncodingException e) {
//            logger.error("字节码转String失败！",e);
//            return Action.ReconsumeLater;
//        }
//        logger.debug("ZupNoticeListener=" + body);
//
//        try {
//        	callBack.callBack(body);
//        }catch(Exception e) {
//        	logger.error("",body);
//        }
//
//
//        return Action.CommitMessage;
//
//    }
//
//}
