package com.zatgo.zup.trade.api.model;

import java.io.Serializable;
import java.util.Date;

public class UmsMemberPassenger implements Serializable {
    private Long id;

    /**
     * 用户编号
     *
     * @mbggenerated
     */
    private String memberId;

    /**
     * 乘客姓名
     *
     * @mbggenerated
     */
    private String name;

    /**
     * 手机号
     *
     * @mbggenerated
     */
    private String phone;

    /**
     * 性别：1-男；2-女
     *
     * @mbggenerated
     */
    private Byte sex;

    /**
     * 生日
     *
     * @mbggenerated
     */
    private Date birthday;

    /**
     * 创建日期
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 是否成人：0-否；1-是
     *
     * @mbggenerated
     */
    private Byte isAdult;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Byte getSex() {
        return sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Byte getIsAdult() {
        return isAdult;
    }

    public void setIsAdult(Byte isAdult) {
        this.isAdult = isAdult;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", memberId=").append(memberId);
        sb.append(", name=").append(name);
        sb.append(", phone=").append(phone);
        sb.append(", sex=").append(sex);
        sb.append(", birthday=").append(birthday);
        sb.append(", createTime=").append(createTime);
        sb.append(", isAdult=").append(isAdult);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}