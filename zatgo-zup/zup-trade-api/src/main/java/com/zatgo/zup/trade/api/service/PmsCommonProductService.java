package com.zatgo.zup.trade.api.service;

import com.zatgo.zup.common.model.CommonProductInfoResponse;
import com.zatgo.zup.trade.api.model.TicketDateList;
import com.zatgo.zup.trade.api.model.TicketListResponse;

import java.util.List;

/**
 * Created by 46041 on 2019/7/17.
 */
public interface PmsCommonProductService {


    CommonProductInfoResponse getProductInfo(Long id);

    TicketDateList ticketList(Long productId);
}
