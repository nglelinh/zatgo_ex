package com.zatgo.zup.trade.api.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sys")
public class SystemController {

    @RequestMapping(value = "/alive")
    public String alive() {
        return "OK";
    }
}
