package com.zatgo.zup.trade.api.dao;

import com.zatgo.zup.trade.api.domain.PmsProductHotelData;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PmsProductHotelDao {

	@Select("select * from pms_product_hotel where product_id = #{productId} and status = 1 and delete_status = 0")
	List<PmsProductHotelData> selectProductHotelList(@Param("productId") Long productId);

	List<PmsProductHotelData> selectProductHotelMinList(@Param("productId") Long productId,
                                                        @Param("startDate") String startDate, @Param("endDate") String endDate);
	
	int updateHotelStock(@Param("list") List<Long> ids, @Param("number") int number);

}
