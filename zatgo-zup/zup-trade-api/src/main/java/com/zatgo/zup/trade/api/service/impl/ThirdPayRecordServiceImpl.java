package com.zatgo.zup.trade.api.service.impl;

import com.ykb.mall.common.enumType.BusinessEnum.MoneyFlow;
import com.ykb.mall.common.enumType.BusinessEnum.PayType;

import com.zatgo.zup.trade.api.mapper.ThirdPayRecordMapper;
import com.ykb.mall.common.model.ThirdPayRecord;
import com.zatgo.zup.trade.api.service.ThirdPayRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class ThirdPayRecordServiceImpl implements ThirdPayRecordService {

	@Autowired
	private ThirdPayRecordMapper thirdPayRecordMapper;

	@Override
	public void insertPaymentRecord(String orderNumber, PayType payType, BigDecimal money, String thirdBusinessNumber) {
		ThirdPayRecord payRecord = new ThirdPayRecord();
		payRecord.setMoney(money);
		payRecord.setMoneyFlow(MoneyFlow.PAYMENT.getCode());
		payRecord.setOrderNumber(orderNumber);
		payRecord.setPayType(payType.getCode().byteValue());
		payRecord.setRecordTime(new Date());
		payRecord.setThirdBusinessNumber(thirdBusinessNumber);
		thirdPayRecordMapper.insertSelective(payRecord);
	}

	@Override
	public void insertRefundRecord(String orderNumber, String refundNumber, PayType payType, BigDecimal money, String thirdBusinessNumber,
                                   String thirdRefundNumber) {
		ThirdPayRecord payRecord = new ThirdPayRecord();
		payRecord.setMoney(money);
		payRecord.setMoneyFlow(MoneyFlow.REFUND.getCode());
		payRecord.setOrderNumber(orderNumber);
		payRecord.setPayType(payType.getCode().byteValue());
		payRecord.setRecordTime(new Date());
		payRecord.setThirdBusinessNumber(thirdBusinessNumber);
		payRecord.setThirdRefundNumber(thirdRefundNumber);
		payRecord.setRefundNumber(refundNumber);
		thirdPayRecordMapper.insertSelective(payRecord);
	}

}
