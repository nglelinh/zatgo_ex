package com.zatgo.zup.trade.api.dao;

import com.zatgo.zup.trade.api.domain.PmsProductGroupData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/4/4 10:39
 */
public interface PmsProductGroupDao {

    /**
     * 产品班期列表
     * @param productId
     * @return
     */
    List<PmsProductGroupData> selectProductGroupList(@Param("productId") Long productId);
}
