package com.zatgo.zup.trade.api.domain;

import com.ykb.mall.common.model.PassengerData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/22 16:33
 */
@ApiModel("生单参数")
public class OmsOrderCreateParam {

    @ApiModelProperty(value = "产品编号", required = true)
    private Long productId;

    @ApiModelProperty(value = "成年人数", required = false)
    private Integer adultNum;

    @ApiModelProperty(value = "儿童人数", required = false)
    private Integer childNum;

    @ApiModelProperty(value = "联系人名称", required = true)
    private String contactName;

    @ApiModelProperty(value = "联系人手机", required = true)
    private String contactPhone;

    @ApiModelProperty(value = "订单总价", required = false)
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "期望日期", required = false)
    private List<String> desirableDates;

    @ApiModelProperty(value = "业务类型（1-旅游；2-机票；3-酒店；4-文章）", required = true)
    private Byte businessType;
    
    @ApiModelProperty(value = "机票乘机人信息")
    private List<PassengerData> passengers;
    
    @ApiModelProperty(value = "价格编号")
    private List<Long> priceIds;

    @ApiModelProperty(value = "活动id")
    private String activityId;

    @ApiModelProperty(value = "订单类型 (0-普通;1-助力；2-拼团)")
    private String createOrderType;

    @ApiModelProperty(value = "参加拼团id")
    private String groupOrderId;

    @ApiModelProperty(value = "skuId")
    private String skuId;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getAdultNum() {
        return adultNum;
    }

    public void setAdultNum(Integer adultNum) {
        this.adultNum = adultNum;
    }

    public Integer getChildNum() {
        return childNum;
    }

    public void setChildNum(Integer childNum) {
        this.childNum = childNum;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<String> getDesirableDates() {
        return desirableDates;
    }

    public void setDesirableDates(List<String> desirableDates) {
        this.desirableDates = desirableDates;
    }

    public Byte getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Byte businessType) {
        this.businessType = businessType;
    }

	public List<PassengerData> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<PassengerData> passengers) {
		this.passengers = passengers;
	}

	public List<Long> getPriceIds() {
		return priceIds;
	}

	public void setPriceIds(List<Long> priceIds) {
		this.priceIds = priceIds;
	}

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getCreateOrderType() {
        return createOrderType;
    }

    public void setCreateOrderType(String createOrderType) {
        this.createOrderType = createOrderType;
    }

    public String getGroupOrderId() {
        return groupOrderId;
    }

    public void setGroupOrderId(String groupOrderId) {
        this.groupOrderId = groupOrderId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }
}