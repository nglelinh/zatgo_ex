package com.zatgo.zup.trade.api.controller;

import com.ykb.mall.common.model.PmsProductCategoryData;
import com.ykb.mall.common.model.PmsProductDetail;
import com.ykb.mall.common.model.PmsBrand;
import com.ykb.mall.common.model.PmsProductGroup;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.domain.PmsProductGroupData;
import com.zatgo.zup.trade.api.model.PmsProduct;
import com.zatgo.zup.trade.api.service.PmsProductFlightService;
import com.zatgo.zup.trade.api.service.PmsProductGroupService;
import com.zatgo.zup.trade.api.service.PmsProductHotelService;
import com.zatgo.zup.trade.api.service.PmsProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/16 10:52
 */
@RestController
@Api(tags = "PmsProductController", description = "产品信息")
@RequestMapping("/trade/api/pmsProduct")
public class PmsProductController extends BaseController{

    @Resource
    private PmsProductService pmsProductService;

    @Resource
    private PmsProductGroupService pmsProductGroupService;
    
    @Resource
    private PmsProductFlightService pmsroductFlightService;
    
    @Resource
    private PmsProductHotelService pmsProductHotelService;

    @ApiOperation(value = "获取产品品牌列表")
    @RequestMapping(value = "/brand/list",method = RequestMethod.GET)
    public Object selectBrandList(){
        List<PmsBrand> brands = pmsProductService.selectBrandList();
        return new CommonResult().success(brands);
    }

    @ApiOperation(value = "获取产品类型列表")
    @RequestMapping(value = "/category/list",method = RequestMethod.GET)
    public Object selectProductCategoryList(){
        List<PmsProductCategoryData> categoryList = pmsProductService.selectProductCategoryList();
        return new CommonResult().success(categoryList);
    }

    @ApiOperation(value = "获取产品类型详情")
    @RequestMapping(value = "/detail/{id}",method = RequestMethod.GET)
    public Object selectProductDetail(@ApiParam("产品编号") @PathVariable("id") Long id) {
        PmsProductDetail detail = pmsProductService.selectProductDetail(id);
        return new CommonResult().success(detail);
    }

    @ApiOperation(value = "获取产品班期列表")
    @RequestMapping(value = "/group/list/{productId}", method = RequestMethod.GET)
    public Object selectProductGroupList(@PathVariable("productId") Long productId) {
        List<PmsProductGroupData> list = pmsProductGroupService.selectProductGroupList(productId);
        return new CommonResult().success(list);
    }
    
    @ApiOperation(value = "获取目的地列表")
    @RequestMapping(value = "/category/all/list",method = RequestMethod.GET)
    public Object selectProductAllCategoryList(){
        List<PmsProductCategoryData> categoryList = pmsProductService.selectProductAllCategoryList();
        return new CommonResult().success(categoryList);
    }
    
    @ApiOperation(value = "获取酒店产品列表")
    @RequestMapping(value = "/hotel/list/{productId}", method = RequestMethod.GET)
    public CommonResult selectProductHotelList(@PathVariable("productId") Long productId,
                                               @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
    	return new CommonResult().success(pmsProductHotelService.selectProductHotelList(productId, startDate, endDate));
    }
    
    @ApiOperation(value = "获取酒店最低价产品列表")
    @RequestMapping(value = "/hotel/min/list/{productId}", method = RequestMethod.GET)
    public CommonResult selectProductHotelMinList(@PathVariable("productId") Long productId,
                                                  @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
    	return new CommonResult().success(pmsProductHotelService.selectProductHotelMinList(productId, startDate, endDate));
    }
    
    @ApiOperation(value = "获取产品类型列表")
    @RequestMapping(value = "/category/detail/{id}",method = RequestMethod.GET)
    public Object selectProductCategoryDetail(@PathVariable("id") Long id){
        PmsProductCategoryData category = pmsProductService.selectProductCategoryDetail(id);
        return new CommonResult().success(category);
    }
    
    @ApiOperation(value = "获取产品类型列表")
    @RequestMapping(value = "/like/{id}",method = RequestMethod.GET)
    public Object productLike(@PathVariable("id") Long id){
        AuthUserInfo userInfo = getUserInfo();
        pmsProductService.addProductLikeNum(id, userInfo.getUserId());
        return new CommonResult().success(null);
    }

    @ApiOperation(value = "获取产品类型列表")
    @RequestMapping(value = "/get/{id}",method = RequestMethod.GET)
    public Object getProductInfo(@PathVariable("id") Long id){
        PmsProduct pmsProduct = pmsProductService.getProductInfo(id);
        return new CommonResult().success(pmsProduct);
    }

    @RequestMapping(value = "/get/list",method = RequestMethod.GET)
    public Object getProductInfoList(@RequestParam("ids") List<Long> ids){
        List<PmsProduct> pmsProduct = pmsProductService.getProductInfoList(ids);
        return new CommonResult().success(pmsProduct);
    }

    @RequestMapping(value = "/travel",method = RequestMethod.GET)
    public Object getTravelGroupInfo(@RequestParam("id") Long id){
        PmsProductGroup pmsProduct = pmsProductService.getTravelGroupInfo(id);
        return new CommonResult().success(pmsProduct);
    }
    
}