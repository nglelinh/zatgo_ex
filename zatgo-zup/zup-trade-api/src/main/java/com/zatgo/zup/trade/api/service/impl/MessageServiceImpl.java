package com.zatgo.zup.trade.api.service.impl;

import com.zatgo.zup.trade.api.mapper.MessageRecordMapper;
import com.ykb.mall.common.model.MessageRecord;
import com.ykb.mall.common.model.MessageTemplate;
import com.zatgo.zup.trade.api.service.MessageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * @Author wangyucong
 * @Date 2019/3/26 10:10
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Resource
    private MessageRecordMapper messageRecordMapper;


    /**
     * 新增消息记录
     *
     * @param template
     * @param receiveNumber
     * @param content
     * @param result
     * @param resultContent
     */
    @Override
    public void createMessageRecord(MessageTemplate template, String receiveNumber, String content, Byte result, String resultContent) {
        MessageRecord record = new MessageRecord();
        record.setTemplateId(template.getId());
        record.setReceiverType(template.getTemplateTarget());
        record.setReceiveNumber(receiveNumber);
        record.setTemplateName(template.getTemplateName());
        record.setMessageContent(content);
        record.setCreateTime(new Date());
        record.setResult(result);
        record.setResultContent(resultContent);
        record.setMessageType(template.getTemplateType());
        messageRecordMapper.insertSelective(record);
    }

    /**
     * 拼装消息内容
     *
     * @param templateContent
     * @param contentMap
     * @return
     */
    @Override
    public String getMessageContent(String templateContent, Map<String, String> contentMap) {
        final String[] messageContent = {null};
        contentMap.forEach((k, v) -> {
            messageContent[0] = templateContent.replaceAll(k, v);
        });
        return messageContent[0];
    }
}