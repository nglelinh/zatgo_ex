package com.zatgo.zup.trade.api.remoteService;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by 46041 on 2019/6/5.
 */

@FeignClient("zup-merchant")
public interface UserRemoteService {

//    @GetMapping(value = "/user/userId/invitationCode/{invitationCode}")
//    @ResponseBody
//    String getUserIdByInvitationCode(@PathVariable("invitationCode") String invitationCode);
        @RequestMapping("/user/byid/{userId}")
        ResponseData<UserData> getUserById(@PathVariable("userId") String userId);
}
