package com.zatgo.zup.trade.api.service;

import com.ykb.mall.common.model.OmsCartItem;
import com.zatgo.zup.trade.api.domain.CartPromotionItem;

import java.util.List;

/**
 * Created by chen on 2018/8/27.
 * 促销管理Service
 */
public interface OmsPromotionService {
    /**
     * 计算购物车中的促销活动信息
     * @param cartItemList 购物车
     */
    List<CartPromotionItem> calcCartPromotion(List<OmsCartItem> cartItemList);
}
