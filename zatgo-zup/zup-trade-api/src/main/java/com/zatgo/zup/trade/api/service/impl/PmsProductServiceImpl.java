package com.zatgo.zup.trade.api.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ykb.mall.common.model.*;
import com.zatgo.zup.trade.api.dao.PmsProductDao;
import com.zatgo.zup.trade.api.mapper.PmsProductGroupMapper;
import com.zatgo.zup.trade.api.mapper.PmsProductMapper;
import com.zatgo.zup.trade.api.mapper.UmsMemberProductLikeRelationMapper;
import com.zatgo.zup.trade.api.model.PmsProduct;
import com.zatgo.zup.trade.api.service.PmsProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/16 10:56
 */
@Service
public class PmsProductServiceImpl implements PmsProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EsProductServiceImpl.class);

    @Autowired
    private PmsProductDao pmsProductDao;
	@Autowired
	private PmsProductMapper pmsProductMapper;
    @Autowired
    private UmsMemberProductLikeRelationMapper umsMemberProductLikeRelationMapper;
	@Autowired
	private PmsProductGroupMapper pmsproductgroupMapper;

    /**
     * 获取产品品牌列表
     * @return
     */
    @Override
    public List<PmsBrand> selectBrandList() {
        return pmsProductDao.selectBrandList();
    }

    /**
     * 获取产品类型列表
     * @return
     */
    @Override
    public List<PmsProductCategoryData> selectProductCategoryList() {
        List<PmsProductCategoryData> categoryList = new ArrayList<>();
        categoryList = pmsProductDao.selectProductCategoryList();
//        if (!CollectionUtils.isEmpty(categoryList)) {
//            // 产品类型处理
//            dealWithProductCategory(categoryList, categoryList);
//        }
		int i = categoryList.size() - 1;
		for (; i >= 0; i--){
			PmsProductCategoryData pmsProductCategoryData = categoryList.get(i);
			Integer level = pmsProductCategoryData.getLevel();
			Long parentId = pmsProductCategoryData.getParentId();
			if (0 == level && 0 == parentId){
				categoryList.remove(i);
			}
		}
        return categoryList;
    }

    /**
     * 产品详情
     * @param id
     * @return
     */
    @Override
    public PmsProductDetail selectProductDetail(Long id) {
    	PmsProductDetail detail = pmsProductDao.selectProductDetail(id);
    	addProductClickNum(id);
        return detail;
    }

    /**
     * 处理产品类型数据
     * @param currentList 当前待处理的子级
     * @param categoryList  当前待处理的父级
     * @return
     */
    private List<PmsProductCategoryData> dealWithProductCategory(List<PmsProductCategoryData> currentList, List<PmsProductCategoryData> categoryList){
    	// 自循环存在bug
        for (int i = 0; i < categoryList.size(); i ++){
            PmsProductCategoryData category = categoryList.get(i);
            List<PmsProductCategoryData> categoryChildList = new ArrayList<>();
            Iterator<PmsProductCategoryData> iterator = currentList.iterator();
            while (iterator.hasNext()) {
                PmsProductCategoryData c = iterator.next();
                if (c.getParentId().equals(category.getId())) {
                    categoryChildList.add(c);
                    iterator.remove();
                }
            }
            category.setCategoryChildList(categoryChildList);
            if (!CollectionUtils.isEmpty(currentList) && !CollectionUtils.isEmpty(categoryChildList)) {
                dealWithProductCategory(currentList, categoryChildList);
            }
        }
        return categoryList;
    }

	@Override
	public List<PmsProductCategoryData> selectProductAllCategoryList() {
		List<PmsProductCategoryData> categoryList = pmsProductDao.selectProductAllCategoryList();
		if(CollectionUtils.isEmpty(categoryList)) {
			return new ArrayList<>();
		}
		
		return dealWithProductCategory(categoryList);
	}
	
	private List<PmsProductCategoryData> dealWithProductCategory(List<PmsProductCategoryData> categoryList) {
		
		List<PmsProductCategoryData> datas = new ArrayList<>();
		for (PmsProductCategoryData category : categoryList) {
			if(category.getLevel().equals(0) && category.getParentId().equals(0L)) {
				category.setCategoryChildList(getChildProductCategory(category.getId(), categoryList));
				datas.add(category);
			}
		}
		
		return datas;
	}
	
	private List<PmsProductCategoryData> getChildProductCategory(Long parentId, List<PmsProductCategoryData> categoryList){
		List<PmsProductCategoryData> datas = new ArrayList<>();
		for (PmsProductCategoryData category : categoryList) {
			if(parentId.equals(category.getParentId())) {
				category.setCategoryChildList(getChildProductCategory(category.getId(), categoryList));
				datas.add(category);
			}
		}
		return datas;
	}

	@Transactional
	@Override
	public void addProductClickNum(Long productId) {
		try {
			pmsProductDao.addProductClickNum(productId);
		} catch (Exception e) {
			LOGGER.error("保存产品点击量失败：", e);
		}
	}

	@Transactional
	@Override
	public void addProductLikeNum(Long productId, String userId) {
		UmsMemberProductLikeRelationExample example = new UmsMemberProductLikeRelationExample();
		example.createCriteria().andMemberIdEqualTo(userId).andProductIdEqualTo(productId);
		List<UmsMemberProductLikeRelation> relations = umsMemberProductLikeRelationMapper.selectByExample(example);
		if(CollectionUtils.isEmpty(relations)) {
			return;
		}
		
		int res = pmsProductDao.addProductLikeNum(productId);
		if(res > 0) {
			UmsMemberProductLikeRelation relation = new UmsMemberProductLikeRelation();
			relation.setMemberId(userId);
			relation.setProductId(productId);
			relation.setCreateTime(new Date());
			umsMemberProductLikeRelationMapper.insertSelective(relation);
		}
	}

	@Override
	public PmsProductCategoryData selectProductCategoryDetail(Long id) {
		
		return pmsProductDao.selectProductCategoryDetail(id);
	}

	@Override
	public PmsProduct getProductInfo(Long id) {
		com.ykb.mall.common.model.PmsProduct pmsProduct = pmsProductMapper.selectByPrimaryKey(id);
		PmsProduct res = JSONObject.parseObject(JSONObject.toJSONString(pmsProduct), PmsProduct.class);
		return res;
	}

	@Override
	public List<PmsProduct> getProductInfoList(List<Long> ids) {
		PmsProductExample example = new PmsProductExample();
		example.createCriteria().andIdIn(ids);
		List<com.ykb.mall.common.model.PmsProduct> pmsProducts = pmsProductMapper.selectByExample(example);
		List<PmsProduct> list = null;
		if (!CollectionUtils.isEmpty(pmsProducts)){
			list = JSONArray.parseArray(JSONArray.toJSONString(pmsProducts), PmsProduct.class);
		}
		return list;
	}

	@Override
	public PmsProductGroup getTravelGroupInfo(Long id) {
		return pmsproductgroupMapper.selectByPrimaryKey(id);
	}
}