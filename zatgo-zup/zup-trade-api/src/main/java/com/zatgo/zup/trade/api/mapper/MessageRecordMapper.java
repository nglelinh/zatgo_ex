package com.zatgo.zup.trade.api.mapper;

import com.ykb.mall.common.model.MessageRecord;
import com.ykb.mall.common.model.MessageRecordExample;
import com.zatgo.zup.trade.api.model.MoveUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageRecordMapper {
    int countByExample(MessageRecordExample example);

    int deleteByExample(MessageRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(MessageRecord record);

    int insertSelective(MessageRecord record);

    List<MessageRecord> selectByExample(MessageRecordExample example);

    MessageRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") MessageRecord record, @Param("example") MessageRecordExample example);

    int updateByExample(@Param("record") MessageRecord record, @Param("example") MessageRecordExample example);

    int updateByPrimaryKeySelective(MessageRecord record);

    int updateByPrimaryKey(MessageRecord record);

    List<MoveUser> getUserInfo();

}