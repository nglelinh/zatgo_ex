package com.zatgo.zup.trade.api.service;

import com.github.pagehelper.Page;
import com.ykb.mall.common.model.OmsOrder;
import com.zatgo.zup.trade.api.domain.*;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * 前台订单管理Service
 * Created by chen on 2018/8/30.
 */
public interface OmsPortalOrderService {

    /**
     * 创建订单
     * @param param
     * @return
     */
    @Transactional
    CommonResult createOmsOrder(OmsOrderCreateParam param, String userId, String userName);

    /**
     * 会员个人订单列表
     * @param param
     * @return
     */
    Page<PortalOrderBaseInfo> selectOrderList(OrderListParam param, Integer pageSize, Integer pageNum);

    /**
     * 会员个人订单详情
     * @param id
     * @return
     */
    PortalOrderDetailInfo selectOrderDetail(Long id, String userId);

    /**
     * 根据用户购物车信息生成确认单信息
     */
//    ConfirmOrderResult generateConfirmOrder();
//
//    /**
//     * 根据提交信息生成订单
//     */
//    @Transactional
//    CommonResult generateOrder(OrderParam orderParam);

    /**
     * 支付成功后的回调
     */
    @Transactional
    CommonResult paySuccess(Long orderId, Integer payType);

    /**
     * 自动取消超时订单
     */
//    @Transactional
//    CommonResult cancelTimeOutOrder();

    /**
     * 取消单个超时订单
     */
//    @Transactional
//    void cancelOrder(Long orderId);

    /**
     * 发送延迟消息取消订单
     */
//    void sendDelayMessageCancelOrder(Long orderId);
    
    /**
     * 根据订单号查询订单
     * @param orderSn
     * @return
     */
    PortalOrderBaseInfo selectOrderByOrderSn(String orderSn);
    
    /**
     * 用户订单支付操作记录
     * @return
     */
    int insertOrderPayHistory(Long orderId, BigDecimal fee);

    OmsOrder selectByPrimaryKey(Long orderId);

    void updateInfo(OmsOrder omsOrder);

    void autoRefund(List<String> list);

    Boolean getOrderInfo(Long orderId);

    void cancelUnpaidOrderTask();

}
