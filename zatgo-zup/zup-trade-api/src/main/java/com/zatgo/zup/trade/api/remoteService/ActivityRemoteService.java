package com.zatgo.zup.trade.api.remoteService;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ActivityBargain;
import com.zatgo.zup.common.model.GroupBookingActivity;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/6/6.
 */

@FeignClient("zup-activity")
public interface ActivityRemoteService {



    @PostMapping(value = "activity/internal/getActivityOrderPrice")
    @ResponseBody
    ResponseData<BigDecimal> getActivityOrderPrice(@RequestParam("activityId") String activityId,
                                                   @RequestParam("userId") String userId);


    @PostMapping(value = "activity/internal/saveOrder")
    @ResponseBody
    ResponseData<String> saveOrder(@RequestParam("activityId") String activityId,
                           @RequestParam("userId") String userId,
                           @RequestParam("orderId") String orderId,
                           @RequestParam("type") String type);

    @PostMapping(value = "activity/groupBooking/internal/saveOrder")
    @ResponseBody
    ResponseData<String> groupSaveOrder(@RequestParam(value = "groupOrderId", required = false) String groupOrderId,
                           @RequestParam("userId") String userId,
                           @RequestParam("orderId") String orderId,
                           @RequestParam("type") String type,
                           @RequestParam("activityId") String activityId);

    @PostMapping(value = "activity/groupBooking/internal/getActivityOrderPrice")
    @ResponseBody
    ResponseData<BigDecimal> getGroupBookingOrderPrice(@RequestParam(value = "groupOrderId", required = false) String groupOrderId,
                                                       @RequestParam("userId") String userId,
                                                       @RequestParam("activityId") String activityId);

    @PostMapping(value = "/activity/internal/orderCallBack")
    @ResponseBody
    ResponseData<Boolean> orderCallBack(@RequestParam("activityId") String activityId,
                                        @RequestParam("userId") String userId,
                                        @RequestParam("orderId") String orderId,
                                        @RequestParam("isSuccess") Boolean isSuccess);

    @PostMapping(value = "/activity/groupBooking/internal/orderCallBack")
    @ResponseBody
    ResponseData<Boolean> GroupOrderCallBack(@RequestParam("activityId") String activityId,
                                        @RequestParam("userId") String userId,
                                        @RequestParam("orderId") String orderId,
                                        @RequestParam("isSuccess") Boolean isSuccess);


    @GetMapping(value = "/activity/groupBooking/internal/updateRefundStatus")
    @ResponseBody
    void updateRefundStatus(@RequestParam("activityId") String activityId);

    @GetMapping(value = "/activity/get/activiTypayTime")
    @ResponseBody
    ResponseData<Long> getPayTime(@RequestParam("activityId") String activityId, @RequestParam("orderType") String orderType);


    @GetMapping(value = "/activity/internal/selectActivityId/{activityId}")
    @ResponseBody
    ResponseData<ActivityBargain> selectActivityIdByOrderId(@PathVariable("activityId") String activityId);


    @GetMapping(value = "/activity/groupBooking/internal/selectById/{activityId}")
    @ResponseBody
    ResponseData<GroupBookingActivity> selectById(@PathVariable("activityId") String activityId);

}
