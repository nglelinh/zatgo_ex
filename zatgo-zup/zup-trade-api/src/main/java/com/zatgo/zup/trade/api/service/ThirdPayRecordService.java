package com.zatgo.zup.trade.api.service;

import com.ykb.mall.common.enumType.BusinessEnum.PayType;

import java.math.BigDecimal;

public interface ThirdPayRecordService {

	/**
	 * 保存第三方支付记录
	 * 
	 * @param orderNumber
	 * @param payType
	 * @param money
	 * @param thirdBusinessNumber
	 */
	void insertPaymentRecord(String orderNumber, PayType payType, BigDecimal money, String thirdBusinessNumber);

	void insertRefundRecord(String orderNumber, String refundNumber, PayType payType, BigDecimal money,
                                   String thirdBusinessNumber, String thirdRefundNumber);

}
