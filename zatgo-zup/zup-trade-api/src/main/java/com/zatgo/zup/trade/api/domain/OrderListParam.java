package com.zatgo.zup.trade.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Author wangyucong
 * @Date 2019/2/25 15:39
 */
@ApiModel("个人订单列表入参")
public class OrderListParam {
    @ApiModelProperty(value = "会员编号", required = false)
    private String memberId;

    @ApiModelProperty(value = "订单类型（0 - 全部；1 - 待付款；2 - 已支付 3-已完成 4-退款）", required = true)
    private Byte orderStatus;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Byte getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Byte orderStatus) {
        this.orderStatus = orderStatus;
    }
}