package com.zatgo.zup.trade.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.ykb.mall.common.enumType.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.model.OmsOrder;
import com.ykb.mall.common.redis.RedisKeyConstants;
import com.ykb.mall.common.utils.MD5Utils;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.WechatRefundRecordRequest;
import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.trade.api.domain.*;
import com.zatgo.zup.trade.api.service.OmsPortalOrderService;
import com.zatgo.zup.trade.api.service.ThirdPayRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * 订单管理Controller
 * Created by chen on 2018/8/30.
 */
@Controller
@Api(tags = "OmsPortalOrderController",description = "订单管理")
@RequestMapping("/trade/api/order")
public class OmsPortalOrderController extends BaseController{

	private static Logger log = LoggerFactory.getLogger(OmsPortalOrderController.class);

    @Autowired
    private OmsPortalOrderService portalOrderService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private ThirdPayRecordService thirdPayRecordService;
    @Autowired
    private OmsPortalOrderService omsPortalOrderService;

    @ApiOperation("申请下单")
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    @ResponseBody
    public Object createOmsOrder(@RequestBody OmsOrderCreateParam param) {
        AuthUserInfo userInfo = getUserInfo();
        String key = redisLockUtils.getRedisKey(RedisKeyConstants.CREATE_OMS_ORDER,
                userInfo.getUserId(), MD5Utils.springMD5Encode(JSON.toJSONString(param)));

    	boolean res = redisLockUtils.lock(key);
    	if(!res) {
    		return new CommonResult().failed("请勿多次提交订单");
    	}

    	try {
            return portalOrderService.createOmsOrder(param, userInfo.getUserId(), userInfo.getUserName());
		} finally {
            redisLockUtils.releaseLock(key);
		}

    }

    @ApiOperation("个人订单列表")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ResponseBody
    public Object selectOrderList(OrderListParam param,
                                  @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum){
        param.setMemberId(getUserInfo().getUserId());
        Page<PortalOrderBaseInfo> list = portalOrderService.selectOrderList(param, pageSize, pageNum);
        return new CommonResult().pageSuccess(list);
    }

    @ApiOperation("个人订单详情")
    @RequestMapping(value = "/detail/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Object selectOrderDetail(@PathVariable Long id){
        AuthUserInfo userInfo = getUserInfo();
        PortalOrderDetailInfo detail = portalOrderService.selectOrderDetail(id, userInfo.getUserId());
        return new CommonResult().success(detail);
    }

    @ApiOperation("个人订单详情（内部接口调用）")
    @RequestMapping(value = "/detail/{id}/{userId}",method = RequestMethod.GET)
    @ResponseBody
    public Object selectOrderDetail(@PathVariable("id") Long id, @PathVariable("userId") String userId){
        PortalOrderDetailInfo detail = portalOrderService.selectOrderDetail(id, userId);
        return new CommonResult().success(detail);
    }

    @ApiOperation("修改订单信息")
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public void update(@RequestBody OmsOrder omsOrder){
        portalOrderService.updateInfo(omsOrder);
    }

//    @ApiOperation("根据购物车信息生成确认单信息")
//    @RequestMapping(value = "/generateConfirmOrder",method = RequestMethod.POST)
//    @ResponseBody
//    public Object generateConfirmOrder(){
//        ConfirmOrderResult confirmOrderResult = portalOrderService.generateConfirmOrder();
//        return new CommonResult().success(confirmOrderResult);
//    }

//    @ApiOperation("根据购物车信息生成订单")
//    @RequestMapping(value = "/generateOrder",method = RequestMethod.POST)
//    @ResponseBody
//    public Object generateOrder(@RequestBody OrderParam orderParam){
//        return portalOrderService.generateOrder(orderParam);
//    }
    @ApiOperation("支付成功的回调")
    @RequestMapping(value = "/paySuccess",method = RequestMethod.GET)
    @ResponseBody
    public Object paySuccess(@RequestParam Long orderId, @RequestParam("payType") Integer payType){
        return portalOrderService.paySuccess(orderId, payType);
    }

//    @ApiOperation("自动取消超时订单")
//    @RequestMapping(value = "/cancelTimeOutOrder",method = RequestMethod.POST)
//    @ResponseBody
//    public Object cancelTimeOutOrder(){
//        return portalOrderService.cancelTimeOutOrder();
//    }

    @ApiOperation("根据微信订单号查订单信息")
    @RequestMapping(value = "/selectOrderByOrderSn/{orderNumber}",method = RequestMethod.POST)
    @ResponseBody
    public Object selectOrderByOrderSn(@PathVariable("orderNumber") String orderNumber){
        PortalOrderBaseInfo portalOrderBaseInfo = portalOrderService.selectOrderByOrderSn(orderNumber);
        return new CommonResult().success(portalOrderBaseInfo);
    }

    @ApiOperation("保存退款记录")
    @RequestMapping(value = "/save/refundRecord",method = RequestMethod.POST)
    @ResponseBody
    public void insertRefundRecord(@RequestBody WechatRefundRecordRequest request){
        thirdPayRecordService.insertRefundRecord(request.getOrderNumber(), request.getRefundNumber(),
                request.getPayType(), request.getMoney(), request.getThirdBusinessNumber(), request.getThirdRefundNumber());
    }

    @ApiOperation("保存支付记录")
    @RequestMapping(value = "/save/insertPaymentRecord",method = RequestMethod.POST)
    @ResponseBody
    public void insertRefundRecord(@RequestParam("orderNumber") String orderNumber,
                                   @RequestParam("payType") BusinessEnum.PayType payType,
                                   @RequestParam("money")  BigDecimal money,
                                   @RequestParam("thirdBusinessNumber") String thirdBusinessNumber){
        thirdPayRecordService.insertPaymentRecord(orderNumber, payType, money, thirdBusinessNumber);
    }


    @ApiOperation("用户订单支付操作记录")
    @RequestMapping(value = "/insertOrderPayHistory",method = RequestMethod.GET)
    @ResponseBody
    public void insertOrderPayHistory(@RequestParam("orderId") Long orderId,@RequestParam("fee") BigDecimal fee){
        omsPortalOrderService.insertOrderPayHistory(orderId, fee);
    }

    @RequestMapping(value = "/getOrderInfo/{orderId}",method = RequestMethod.GET)
    @ApiOperation(value = "获取订单信息", notes = "获取订单信息", httpMethod = "GET")
    @ResponseBody
    public ResponseData<Boolean> getOrderInfo(@PathVariable("orderId") Long orderId) {
        Boolean flag = omsPortalOrderService.getOrderInfo(orderId);
        return BusinessResponseFactory.createSuccess(flag);
    }

    @ApiOperation("根据订单id查订单信息")
    @RequestMapping(value = "/selectById",method = RequestMethod.GET)
    @ResponseBody
    public ResponseData<OmsOrder> selectById(@RequestParam("orderId") Long orderId){
        OmsOrder order = omsPortalOrderService.selectByPrimaryKey(orderId);
        return BusinessResponseFactory.createSuccess(order);
    }


    @ApiOperation("微信自动退款(由activity调用)")
    @RequestMapping(value = "/auto/refund",method = RequestMethod.POST)
    @ResponseBody
    public void autoRefund(@RequestParam("groupUserOrderList") String groupUserOrderList){
        List<String> list = JSONArray.parseArray(groupUserOrderList, String.class);
        omsPortalOrderService.autoRefund(list);
    }


//    @ApiOperation("取消单个超时订单")
//    @RequestMapping(value = "/cancelOrder",method = RequestMethod.POST)
//    @ResponseBody
//    public Object cancelOrder(Long orderId){
//        portalOrderService.sendDelayMessageCancelOrder(orderId);
//        return new CommonResult().success(null);
//    }
}
