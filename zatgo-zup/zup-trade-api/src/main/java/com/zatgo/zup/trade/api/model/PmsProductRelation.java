package com.zatgo.zup.trade.api.model;

import java.io.Serializable;

public class PmsProductRelation implements Serializable {
    private Long id;

    /**
     * 主产品编号
     *
     * @mbggenerated
     */
    private Long productId;

    /**
     * 从产品编号
     *
     * @mbggenerated
     */
    private Long slaveProductId;

    /**
     * 业务类型（1-旅游；2-机票；3-酒店；4-文章）
     *
     * @mbggenerated
     */
    private Byte businessType;

    /**
     * 关联类型：1-相关推荐；2-关联 
     *
     * @mbggenerated
     */
    private Byte relationType;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getSlaveProductId() {
        return slaveProductId;
    }

    public void setSlaveProductId(Long slaveProductId) {
        this.slaveProductId = slaveProductId;
    }

    public Byte getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Byte businessType) {
        this.businessType = businessType;
    }

    public Byte getRelationType() {
        return relationType;
    }

    public void setRelationType(Byte relationType) {
        this.relationType = relationType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", productId=").append(productId);
        sb.append(", slaveProductId=").append(slaveProductId);
        sb.append(", businessType=").append(businessType);
        sb.append(", relationType=").append(relationType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}