package com.zatgo.zup.trade.api.service.impl;

import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.ykb.mall.common.model.CmsSubject;
import com.ykb.mall.common.model.CmsSubjectExample;
import com.zatgo.zup.trade.api.dao.SmsSubjectDao;
import com.zatgo.zup.trade.api.domain.SmsSubjectData;
import com.zatgo.zup.trade.api.mapper.CmsSubjectMapper;
import com.zatgo.zup.trade.api.service.SmsSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SmsSubjectServiceImpl implements SmsSubjectService {
	
	@Autowired
	private SmsSubjectDao smsSubjectDao;
	
	@Autowired
	private CmsSubjectMapper cmsSubjectMapper;

	@Override
	public List<SmsSubjectData> homeSubjectList(Byte businessType) {
		if (businessType != null){
			return smsSubjectDao.selectHomeSubjectListByBusinessType(businessType);
		} else {
			return smsSubjectDao.selectHomeSubjectList();
		}
	}

	@Override
	public SmsSubjectData subjectDetails(Long id) {
		return smsSubjectDao.selectSubjectDetails(id);
	}

	@Override
	public List<CmsSubject> subjectList(Byte businessType) {
		CmsSubjectExample example = new CmsSubjectExample();
		example.setOrderByClause(" create_time desc ");
		CmsSubjectExample.Criteria criteria = example.createCriteria();
		criteria.andShowStatusEqualTo(YesOrNo.YES.getCode().intValue());
		if (businessType != null){
			criteria.andBusinessTypeEqualTo(businessType);
		}
		return cmsSubjectMapper.selectByExample(example);
	}

}
