package com.zatgo.zup.trade.api.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 46041 on 2019/7/31.
 */
public class TicketList {

    private BigDecimal price;

    private String skuId;

    private Long stock;

    private String spec;

    private Date startTime;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
}
