package com.zatgo.zup.trade.api.controller;

import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.service.SmsAdvertiseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "SmsAdvertiseController", description = "广告信息")
@RequestMapping("/trade/api/smsAdvertise")
public class SmsAdvertiseController {

	@Autowired
	private SmsAdvertiseService smsAdvertiseService;

	@ApiOperation("首页banner")
	@GetMapping(value = "/home/list")
	public CommonResult homeAdvertiseList(@RequestParam(required = false, defaultValue = "1") Integer type) {
		return new CommonResult().success(smsAdvertiseService.homeAdvertiseList(type));
	}

}
