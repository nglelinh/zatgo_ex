package com.zatgo.zup.trade.api.remoteService;

import com.zatgo.zup.common.model.wx.WechatRefundRequest;
import com.zatgo.zup.trade.api.domain.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2019/6/5.
 */

@FeignClient("zup-wechat")
public interface WechatRemoteService {

//    @GetMapping(value = "/wechat/getWechatAccessToken")
//    @ResponseBody
//    String getWechatAccessToken();


    @PostMapping(value = "/wechat/weixinRefund")
    @ResponseBody
    String weixinRefund(@RequestBody WechatRefundRequest request);


    @GetMapping(value = "/wechat/wx/msg/support/{userId}")
    @ResponseBody
    CommonResult sendWechatSupportMsg(@PathVariable("openId") String openId);

    @GetMapping(value = "/support/success/{userId}/{id}")
    @ResponseBody
    CommonResult sendWechatSupportSuccessMsg(@PathVariable("userId") Long userId, @PathVariable("id") String id);

    @GetMapping(value = "/wechat/jsapiConfig")
    @ResponseBody
    CommonResult selectJsapiConfig(@RequestParam("url") String url);

    @GetMapping(value = "/wechat/wx/pay/params/{orderId}/{wxPayType}/{wxOpenId}/{userId}")
    @ResponseBody
    CommonResult getOrderPayWeixinParams(@PathVariable("orderId") Long orderId,
                                         @PathVariable("wxPayType") Integer wxPayType,
                                         @PathVariable("wxOpenId") String wxOpenId,
                                         @PathVariable("userId") String userId);

}
