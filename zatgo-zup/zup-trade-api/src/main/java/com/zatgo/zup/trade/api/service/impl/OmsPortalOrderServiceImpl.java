package com.zatgo.zup.trade.api.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ykb.mall.common.enumType.BusinessEnum;
import com.ykb.mall.common.enumType.BusinessEnum.*;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.OmsOrder;
import com.ykb.mall.common.model.OmsOrderExample;
import com.ykb.mall.common.model.OmsOrderItem;
import com.ykb.mall.common.model.OmsOrderItemExample;
import com.ykb.mall.common.model.OmsOrderOperateHistory;
import com.ykb.mall.common.model.PassengerData;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductFlight;
import com.ykb.mall.common.model.PmsProductFlightExample;
import com.ykb.mall.common.model.PmsProductFlightPrice;
import com.ykb.mall.common.model.PmsProductFlightPriceExample;
import com.ykb.mall.common.model.PmsProductGroup;
import com.ykb.mall.common.model.PmsProductGroupPrice;
import com.ykb.mall.common.model.PmsProductGroupPriceExample;
import com.ykb.mall.common.model.PmsProductHotel;
import com.ykb.mall.common.model.PmsProductHotelExample;
import com.ykb.mall.common.model.PmsSkuStock;
import com.ykb.mall.common.model.SmsCoupon;
import com.ykb.mall.common.model.SmsCouponHistory;
import com.ykb.mall.common.model.SmsCouponHistoryExample;
import com.ykb.mall.common.model.SmsCouponProductCategoryRelation;
import com.ykb.mall.common.model.SmsCouponProductRelation;
import com.ykb.mall.common.model.UmsIntegrationConsumeSetting;
import com.ykb.mall.common.model.UmsMember;
import com.ykb.mall.common.utils.IdcardUtils;
import com.ykb.mall.common.utils.PatternUtils;
import com.ykb.mall.common.model.*;
import com.zatgo.zup.common.email.MailService;
import com.zatgo.zup.common.model.ActivityBargain;
import com.zatgo.zup.common.model.GroupBookingActivity;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.WechatRefundRequest;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.trade.api.dao.*;
import com.zatgo.zup.trade.api.domain.*;
import com.zatgo.zup.trade.api.domain.PortalOrderBaseInfo;
import com.zatgo.zup.trade.api.domain.PortalOrderDetailInfo;
import com.zatgo.zup.trade.api.mapper.*;
import com.zatgo.zup.trade.api.model.*;
import com.zatgo.zup.trade.api.remoteService.ActivityRemoteService;
import com.zatgo.zup.trade.api.remoteService.WechatRemoteService;
import com.zatgo.zup.trade.api.service.OmsPortalOrderService;
import com.zatgo.zup.trade.api.service.RedisService;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 前台订单管理Service
 * Created by chen on 2018/8/30.
 */
@Service
public class OmsPortalOrderServiceImpl implements OmsPortalOrderService {
    private static final Logger logger = LoggerFactory.getLogger(OmsPortalOrderServiceImpl.class);

    @Value("${mail.to}")
    private String MAIL_TO;
    
    @Autowired
    private UmsIntegrationConsumeSettingMapper integrationConsumeSettingMapper;
    @Autowired
    private PmsSkuStockMapper skuStockMapper;
//    @Autowired
//    private SmsCouponHistoryDao couponHistoryDao;
    @Autowired
    private OmsOrderMapper orderMapper;
//    @Autowired
//    private PortalOrderItemDao orderItemDao;
    @Autowired
    private SmsCouponHistoryMapper couponHistoryMapper;
    @Autowired
    private RedisService redisService;
//    @Value("${redis.key.prefix.orderId}")
    private String REDIS_KEY_PREFIX_ORDER_ID = "portal:orderId:";
    @Autowired
    private PortalOrderDao portalOrderDao;
//    @Autowired
//    private OmsOrderSettingMapper orderSettingMapper;
//    @Autowired
//    private OmsOrderItemMapper orderItemMapper;
//    @Autowired
//    private CancelOrderSender cancelOrderSender;
    @Autowired
    private PmsProductMapper pmsProductMapper;
    @Autowired
    private OmsOrderItemMapper omsOrderItemMapper;

    @Autowired
    private MailService apiMailService;
    
    @Resource
    private PmsProductFlightDao pmsProductFlightDao;
    
    @Resource
    private PmsProductHotelDao pmsProductHotelDao;
    
    @Resource
    private OmsOrderOperateHistoryMapper omsOrderOperateHistoryMapper;

    @Autowired
    private PmsProductGroupMapper pmsProductGroupMapper;

    @Autowired
    private PmsProductFlightMapper pmsProductFlightMapper;

    @Autowired
    private PmsProductFlightPriceMapper pmsProductFlightPriceMapper;

    @Autowired
    private PmsProductHotelMapper pmsProductHotelMapper;

//    @Value("${third.api.url}")
//    private String thirdApiUrl;

    @Autowired
    private ActivityRemoteService activityRemoteService;
    @Autowired
    private OmsOrderMapper omsOrderMapper;
    @Autowired
    private WechatRemoteService wechatRemoteService;
    @Autowired
    private PmsProductGroupPriceMapper pmsProductGroupPriceMapper;
    @Autowired
    private PmsProductDao pmsProductDao;
    @Autowired
    private PmsCommonProductSkuMapper pmsCommonProductSkuMapper;

    /**
     * 创建订单
     * @param param
     * @return
     */
    @Override
//    @Transactional
    public CommonResult createOmsOrder(OmsOrderCreateParam param, String userId, String userName) {

        logger.info("支付请求参数：" + JSONObject.toJSONString(param));
    	
    	Byte businessType = null == param.getBusinessType() ? BusinessType.TRAVEL.getCode() : param.getBusinessType();

        String activityId = param.getActivityId();
        String orderType = param.getCreateOrderType();

        List<PassengerData> passengers = param.getPassengers();
//        int adult = 0;
//        int child = 0;
        if (passengers == null){
            passengers = new ArrayList<>();
            PassengerData passenger = new PassengerData();
            passenger.setName(param.getContactName());
            passenger.setPhone(param.getContactPhone());
            passenger.setIsAdult(new Byte("1"));
            passengers.add(passenger);
        }
//        if (!CollectionUtils.isEmpty(passengers)){
//            for (PassengerData passenger : passengers) {
//                if (passenger.getIsAdult().intValue() == 1){
//                    adult++;
//                }
//                if (passenger.getIsAdult().intValue() == 0){
//                    child++;
//                }
//                param.setChildNum(child);
//                param.setAdultNum(adult);
//            }
//        }

        // 检验联系人信息和乘客信息
		CommonResult commonResult = cheackPerson(param.getContactName(), param.getContactPhone(), businessType,
				param.getPassengers());
    	if(commonResult != null) {
    		return commonResult;
    	}
    	
        PmsProduct pmsProduct = pmsProductMapper.selectByPrimaryKey(param.getProductId());
        if (null == param.getAdultNum()) {
            param.setAdultNum(0);
        }
        if (null == param.getChildNum()) {
            param.setChildNum(0);
        }
        //是活动的订单，需要获取活动priceIds
        if (!StringUtils.isEmpty(activityId)){
            String createOrderType = param.getCreateOrderType();
            String activityPriceIds = null;
            if ("1".equals(createOrderType)){
                ResponseData<ActivityBargain> activityBargainResponseData = activityRemoteService.selectActivityIdByOrderId(activityId);
                if (activityBargainResponseData != null && activityBargainResponseData.isSuccessful()){
                    ActivityBargain data = activityBargainResponseData.getData();
                    activityPriceIds = data.getPriceIds();
                }
            }
            if ("2".equals(createOrderType)){
                ResponseData<GroupBookingActivity> groupBookingActivityResponseData = activityRemoteService.selectById(activityId);
                if (groupBookingActivityResponseData != null && groupBookingActivityResponseData.isSuccessful()){
                    GroupBookingActivity data = groupBookingActivityResponseData.getData();
                    activityPriceIds = data.getPriceIds();
                }
            }
            if (!StringUtils.isEmpty(activityPriceIds)){
                List<Long> list = JSONArray.parseArray(activityPriceIds, Long.class);
                if (!CollectionUtils.isEmpty(list)){
                    Long id = list.get(0);
                    PmsProductGroupPriceExample example = new PmsProductGroupPriceExample();
                    example.createCriteria().andGroupIdEqualTo(id);
                    List<PmsProductGroupPrice> pmsProductGroupPrices = pmsProductGroupPriceMapper.selectByExample(example);
                    list = new ArrayList<>();
                    list.add(pmsProductGroupPrices.get(0).getId());
                    param.setPriceIds(list);
                }
            }
        }
        try {
            // 校验产品状态
            checkProductInfo(pmsProduct, param.getPriceIds(), param.getAdultNum(), param.getChildNum(), param.getTotalPrice(), !StringUtils.isEmpty(activityId), param.getSkuId());
        } catch (BusinessException e) {
            logger.error("产品：{}，{}",param.getProductId(), e.getMessage());
            return new CommonResult().failed(e.getMessage());
        }
        // 当前会员

        OmsOrder order = new OmsOrder();
        order.setMemberId(userId);
        order.setCreateTime(new Date());
        order.setMemberUsername(userName);
        
        order.setTotalAmount(param.getTotalPrice());

        order.setPayAmount(param.getTotalPrice());
        //如果是砍价团，需要获取价格
        if (!StringUtils.isEmpty(activityId) && CreateOrderType.BARGAIN.getCode().equals(orderType)){
            ResponseData<BigDecimal> responseData = activityRemoteService.getActivityOrderPrice(param.getActivityId(), userId);
            if (responseData == null || !responseData.isSuccessful()){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, responseData.getMessage());
            }
            BigDecimal activityOrderPrice = responseData.getData();
            order.setPayAmount(activityOrderPrice);
        }
        //如果是拼团，需要获取价格
        if (!StringUtils.isEmpty(activityId) && CreateOrderType.GROUPBOOKING.getCode().equals(orderType)){
            ResponseData<BigDecimal> responseData = activityRemoteService.getGroupBookingOrderPrice(param.getGroupOrderId(), userId, activityId);
            if (responseData == null || !responseData.isSuccessful()){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, responseData.getMessage());
            }
            BigDecimal activityOrderPrice = responseData.getData();
            order.setPayAmount(activityOrderPrice);
            order.setTotalAmount(pmsProduct.getPrice());
        }
        Long payTime = pmsProduct.getPayTime();
        order.setPayType(PayType.unpaid.getCode());
        order.setSourceType(SourceType.weixin_H5.getCode());
        order.setStatus(OrderStatus.wait_pay.getCode());
        order.setOrderType(OrderType.ordinary.getCode());
        order.setReceiverName(param.getContactName());
        order.setReceiverPhone(param.getContactPhone());
        order.setOrderSn(generateOrderSn(order));
        order.setBusinessType(businessType);
        order.setPayDeadline(payTime == null ? null : new Date(new Date().getTime() + payTime.longValue() * 60 * 1000l));
        order.setIsActivityOrder(false);
        order.setPromotionAmount(order.getTotalAmount().subtract(order.getPayAmount()));
        if (!StringUtils.isEmpty(orderType) && !"0".equals(orderType)){
            order.setIsActivityOrder(true);
            ResponseData<Long> responseData = activityRemoteService.getPayTime(activityId, orderType);
            if (responseData == null || !responseData.isSuccessful()){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, responseData.getMessage());
            }
            Long time = responseData.getData();
            order.setPayDeadline(payTime == null ? null : new Date(new Date().getTime() + time.longValue() * 60 * 1000l));
        }
        order.setNote(param.getActivityId() + "_" + userId);
        // 1砍价 2拼团
        order.setActivityType(new Byte(orderType));
		if (!CollectionUtils.isEmpty(param.getPassengers())) {
			order.setPassengerData(JSON.toJSONString(param.getPassengers()));
		}
        orderMapper.insertSelective(order);

        //保存订单
        if (!StringUtils.isEmpty(activityId) && CreateOrderType.BARGAIN.getCode().equals(orderType)){
            ResponseData<String> responseData = activityRemoteService.saveOrder(param.getActivityId(), userId, order.getId() + "", orderType);
            if (responseData == null || !responseData.isSuccessful()){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, responseData.getMessage());
            }
            OmsOrder omsOrder = new OmsOrder();
            omsOrder.setActivityId(responseData.getData());
            omsOrder.setId(order.getId());
            omsOrderMapper.updateByPrimaryKeySelective(omsOrder);
        }
        //保存订单
        if (!StringUtils.isEmpty(activityId) && CreateOrderType.GROUPBOOKING.getCode().equals(orderType)){
            ResponseData<String> responseData = activityRemoteService.groupSaveOrder(param.getGroupOrderId(), userId, order.getId() + "", orderType, param.getActivityId());
            if (responseData == null || !responseData.isSuccessful()){
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, responseData.getMessage());
            }
        }

        Map<String, String> map = new HashMap<>();
        map.put("#ORDER_NUM#", order.getOrderSn());

        Map<String,Object> result = new HashMap<>();
        result.put("order",order);
        OmsOrderItem item = null;
        if(BusinessType.TRAVEL.getCode().equals(businessType)) {
        	item = createTravelOrderItem(pmsProduct, param, order.getId(), order.getOrderSn(), param.getPriceIds());
        }
        if(BusinessType.FLIGHT.getCode().equals(businessType)) {
        	item = createFlightOrderItem(pmsProduct, param, order.getId(), order.getOrderSn(), param.getPriceIds().get(0));
        }
        if(BusinessType.HOTEL.getCode().equals(businessType)) {
        	item = createHotelOrderItem(pmsProduct, param, order.getId(), order.getOrderSn(), param.getPriceIds());
        }
        if(BusinessType.COMMON.getCode().equals(businessType) || BusinessType.OTHER.getCode().equals(businessType)) {
            item = createCommonOrderItem(pmsProduct, param, order.getId(), order.getOrderSn());
        }
        result.put("orderItem", item);
        
        // 发送邮件通知
        apiMailService.sendEmail(TemplateCode.APPLY_NOTICE.getCode(), MAIL_TO, map);
        
        return new CommonResult().success("下单成功", result);
    }

    /**
     * 校验订单信息
     * @param contactName
     * @param contactPhone
     * @param businessType
     * @param passengerList
     * @return
     */
	private CommonResult cheackPerson(String contactName, String contactPhone, Byte businessType,
			List<PassengerData> passengerList) {
		CommonResult result = cheackContact(contactName, contactPhone);
		if (result != null) {
			return result;
		}

		return cheackPassenger(businessType, passengerList);
	}
    
    /**
     * 联系人信息校验
     * @param contactName
     * @param contactPhone
     * @return
     */
    private CommonResult cheackContact(String contactName, String contactPhone) {
    	if(StringUtils.isBlank(contactName)) {
    		return new CommonResult().failed("请输入联系人姓名");
    	}
    	
    	if(StringUtils.isBlank(contactPhone)) {
    		return new CommonResult().failed("请输入联系人联系方式");
    	}
    	
    	boolean res = PatternUtils.isMobile(contactPhone);
    	if(!res) {
    		return new CommonResult().failed("手机号格式不正确");
    	}
    	
    	return null;
    }
    
    /**
     * 校验乘机人信息
     * @param businessType
     * @param passengerList
     * @return
     */
    private CommonResult cheackPassenger(Byte businessType, List<PassengerData> passengerList) {
    	
    	if(!BusinessType.FLIGHT.getCode().equals(businessType)) {
    		return null;
    	}
    	
    	if(CollectionUtils.isEmpty(passengerList)) {
    		return new CommonResult().failed("请输入乘机人信息");
    	}
    	
    	for (PassengerData passenger : passengerList) {
			if (StringUtils.isBlank(passenger.getIdcardType()) || StringUtils.isBlank(passenger.getIdcardCode())
					|| StringUtils.isBlank(passenger.getName()) || StringUtils.isBlank(passenger.getPhone())) {
				return new CommonResult().failed("请输入必要的乘机人信息");
			}
			
			if(IdcardType.IDENTITY_CARD.getCode().equals(new Byte(passenger.getIdcardType()))) {
				boolean res = IdcardUtils.isValidatedAllIdcard(passenger.getIdcardCode());
				if(!res) {
					return new CommonResult().failed(passenger.getName() + "乘机人身份证件错误");
				}
			}
			
		}
    	
    	return null;
    }
    
    private OmsOrderItem createTravelOrderItem(PmsProduct pmsProduct, OmsOrderCreateParam param, Long orderId,
                                               String orderSn, List<Long> prices) {
    	OmsOrderItem orderItem = new OmsOrderItem();
        orderItem.setProductId(pmsProduct.getId());
        orderItem.setProductName(pmsProduct.getName());
        orderItem.setProductPic(pmsProduct.getPic());
        orderItem.setProductBrand(pmsProduct.getBrandName());
        orderItem.setProductSn(pmsProduct.getProductSn());
        orderItem.setProductPrice(pmsProduct.getPrice());
        BigDecimal childPrice = null == pmsProduct.getChildPrice() ? pmsProduct.getPrice() : pmsProduct.getChildPrice();
        orderItem.setProductChildPrice(childPrice);
        orderItem.setProductQuantity(param.getAdultNum() + param.getChildNum());
        orderItem.setProductAttr(null);
        orderItem.setGiftGrowth(pmsProduct.getGiftGrowth());
//        orderItem.setRealAmount(pmsProduct.getPrice());
        orderItem.setAdultNum(param.getAdultNum());
        orderItem.setChildNum(param.getChildNum());
        orderItem.setDesirableTime(JSON.toJSONString(param.getDesirableDates()));
        orderItem.setOrderId(orderId);
        orderItem.setOrderSn(orderSn);
        TravelExtInfo extInfo = new TravelExtInfo();
        SimpleDateFormat ymd = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat md = new SimpleDateFormat("yyyy-MM-dd");
        PmsProductGroupPriceExample example = new PmsProductGroupPriceExample();
        example.createCriteria().andIdIn(prices);
        List<PmsProductGroupPrice> pmsProductGroupPrices = pmsProductGroupPriceMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(pmsProductGroupPrices)){
            PmsProductGroupPrice pmsProductGroupPrice = pmsProductGroupPrices.get(0);
            Long groupId = pmsProductGroupPrice.getGroupId();
            PmsProductGroup pmsProductGroup = pmsProductGroupMapper.selectByPrimaryKey(groupId);
            Date collectionTime = pmsProductGroup.getCollectionTime();
            Date tripEndTime = pmsProductGroup.getTripEndTime();
            extInfo.setStartDate(md.format(collectionTime) + " " + DateTimeUtils.dateToWeek(ymd.format(collectionTime)));
            extInfo.setEndDate(md.format(tripEndTime) + " " + DateTimeUtils.dateToWeek(ymd.format(tripEndTime)));
            extInfo.setPrices(prices);
            orderItem.setSp3(JSONObject.toJSONString(extInfo));
        }
        omsOrderItemMapper.insertSelective(orderItem);
        
        return orderItem;
    }
    
	private OmsOrderItem createFlightOrderItem(PmsProduct pmsProduct, OmsOrderCreateParam param, Long orderId,
			String orderSn, Long priceId) {

		PmsProductFlightPrice flightPrice = pmsProductFlightPriceMapper.selectByPrimaryKey(priceId);

		OmsOrderItem orderItem = new OmsOrderItem();
		orderItem.setProductId(pmsProduct.getId());
		orderItem.setProductName(pmsProduct.getName());
		orderItem.setProductPic(pmsProduct.getPic());
		orderItem.setProductBrand(flightPrice.getShippingSpaceName());
		orderItem.setProductSn(pmsProduct.getProductSn());
		orderItem.setProductPrice(flightPrice.getPurchasePrice());

		orderItem.setProductChildPrice(flightPrice.getChildPurchasePrice());
		orderItem.setProductQuantity(param.getAdultNum() + param.getChildNum());
		orderItem.setProductAttr(null);
		orderItem.setGiftGrowth(pmsProduct.getGiftGrowth());
		// orderItem.setRealAmount(pmsProduct.getPrice());
		orderItem.setAdultNum(param.getAdultNum());
		orderItem.setChildNum(param.getChildNum());
		orderItem.setDesirableTime(JSON.toJSONString(param.getDesirableDates()));
		orderItem.setOrderId(orderId);
		orderItem.setOrderSn(orderSn);
        PmsProductFlightExample example = new PmsProductFlightExample();
        example.createCriteria().andProductIdEqualTo(pmsProduct.getId());
        List<PmsProductFlight> pmsProductFlights = pmsProductFlightMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(pmsProductFlights)){
            PmsProductFlight pmsProductFlight = pmsProductFlights.get(0);
            Long id = pmsProductFlight.getId();
            PmsProductFlightPriceExample pmsProductFlightPriceExample = new PmsProductFlightPriceExample();
            pmsProductFlightPriceExample.createCriteria().andFlightIdEqualTo(id);
            List<PmsProductFlightPrice> pmsProductFlightPrices = pmsProductFlightPriceMapper.selectByExample(pmsProductFlightPriceExample);
            FlightExtInfo extInfo = new FlightExtInfo();
            if (!CollectionUtils.isEmpty(pmsProductFlightPrices)){
                SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
                SimpleDateFormat m = new SimpleDateFormat("HH:mm");
                PmsProductFlightPrice pmsProductFlightPrice = pmsProductFlightPrices.get(0);
                extInfo.setStartDate(sdf.format(pmsProductFlightPrice.getFlightDate()) + " " + m.format(pmsProductFlight.getTripDepartureTime()));
                Date returnFlightData = pmsProductFlightPrice.getReturnFlightData();
                Byte tripFlightType = pmsProductFlight.getTripFlightType();
                if (tripFlightType != null && tripFlightType.equals(new Byte("2"))){
                    extInfo.setReturnStartDate(sdf.format(pmsProductFlightPrice.getReturnFlightData()) + " " + m.format(pmsProductFlight.getReturnDepartureTime()));
                    extInfo.setShippingSpace(pmsProductFlightPrice.getShippingSpaceName());
                }
            }
            extInfo.setCompanyName(pmsProductFlight.getTripAirlineCompany());
            extInfo.setFlight(pmsProductFlight.getTripFlightNumber());
            extInfo.setFromAddr(pmsProductFlight.getTripDepartureCityName());
            extInfo.setToAddr(pmsProductFlight.getTripDestinationCityName());
            extInfo.setTripIsTransfer(pmsProductFlight.getTripIsTransfer());
            extInfo.setStartAirport(pmsProductFlight.getTripDepartureAirportName() + " " + pmsProductFlight.getTripDepartureTerminal());
            extInfo.setEndAirport(pmsProductFlight.getTripDestinationAirportName() + " " + pmsProductFlight.getTripDestinationTerminal());
            extInfo.setReturnCompanyName(pmsProductFlight.getReturnAirlineCompany());
            extInfo.setReturnFlight(pmsProductFlight.getReturnFlightNumber());
            extInfo.setReturnFromAddr(pmsProductFlight.getReturnDepartureCityName());
            extInfo.setReturnToAddr(pmsProductFlight.getReturnDestinationCityName());
            extInfo.setReturnStartAirport(pmsProductFlight.getReturnDepartureAirportName() + " " + pmsProductFlight.getReturnDepartureTerminal());
            extInfo.setReturnEndAirport(pmsProductFlight.getReturnDestinationAirportName() + " " + pmsProductFlight.getReturnDestinationTerminal());
            extInfo.setReturnTripIsTransfer(pmsProductFlight.getReturnIsTransfer());
            extInfo.setPrices(Arrays.asList(priceId));
            orderItem.setSp3(JSONObject.toJSONString(extInfo));
        }
		omsOrderItemMapper.insertSelective(orderItem);

		return orderItem;
	}

    private OmsOrderItem createCommonOrderItem(PmsProduct pmsProduct, OmsOrderCreateParam param, Long orderId, String orderSn) {
        String skuId = param.getSkuId();
        PmsCommonProductSku sku = pmsCommonProductSkuMapper.selectByPrimaryKey(skuId);
        CommonExtInfo extInfo = new CommonExtInfo();
        OmsOrderItem orderItem = new OmsOrderItem();
        orderItem.setAdultNum(param.getAdultNum());
        orderItem.setChildNum(param.getChildNum());
        orderItem.setSp3(JSONObject.toJSONString(extInfo));
        orderItem.setOrderId(orderId);
        orderItem.setOrderSn(orderSn);
        orderItem.setProductId(pmsProduct.getId());
        orderItem.setProductName(pmsProduct.getName());
        orderItem.setProductPic(pmsProduct.getPic());
        orderItem.setProductChildPrice(sku.getPrice());
        orderItem.setProductPrice(sku.getPrice());
        orderItem.setProductQuantity(param.getAdultNum() + param.getChildNum());
        orderItem.setChildNum(param.getChildNum());
        orderItem.setAdultNum(param.getAdultNum());
        omsOrderItemMapper.insertSelective(orderItem);
        return orderItem;
    }
    
	private OmsOrderItem createHotelOrderItem(PmsProduct pmsProduct, OmsOrderCreateParam param, Long orderId,
			String orderSn, List<Long> priceIds) {
		
		PmsProductHotelExample example = new PmsProductHotelExample();
		example.setOrderByClause(" arrival_date asc ");
		example.createCriteria().andIdIn(priceIds);
		List<PmsProductHotel> hotels = pmsProductHotelMapper.selectByExample(example);
		
		List<PmsHotelData> priceList = new ArrayList<PmsHotelData>();
		for (PmsProductHotel hotel : hotels) {
			PmsHotelData price = new PmsHotelData();
//			price.setId(hotel.getId());
			price.setSalePrice(hotel.getSalePrice());
//			price.setRoomType(hotel.getRoomType());
			price.setArrivalDate(DateTimeUtils.parseDateToString(hotel.getArrivalDate(), DateTimeUtils.PATTEN_YYYY_MM_DD));
			priceList.add(price);
		}

        List<String> desirableDates = param.getDesirableDates();
        OmsOrderItem orderItem = new OmsOrderItem();
		orderItem.setProductId(pmsProduct.getId());
		orderItem.setProductName(pmsProduct.getName());
		orderItem.setProductPic(pmsProduct.getPic());
		orderItem.setProductBrand(hotels.get(0).getRoomType());
		orderItem.setProductSn(pmsProduct.getProductSn());
//		orderItem.setProductPrice();
//		orderItem.setProductChildPrice();
		orderItem.setProductQuantity(param.getAdultNum() + param.getChildNum());
		orderItem.setProductAttr(null);
		orderItem.setGiftGrowth(pmsProduct.getGiftGrowth());
		// orderItem.setRealAmount(pmsProduct.getPrice());
		orderItem.setAdultNum(param.getAdultNum());
		orderItem.setChildNum(param.getChildNum());
		orderItem.setDesirableTime(JSON.toJSONString(desirableDates));
		orderItem.setOrderId(orderId);
		orderItem.setOrderSn(orderSn);
		orderItem.setPrices(JSON.toJSONString(priceList));
        PmsProductHotelExample hotelExample = new PmsProductHotelExample();
        hotelExample.createCriteria().andProductIdEqualTo(pmsProduct.getId());
        List<PmsProductHotel> pmsProductHotels = pmsProductHotelMapper.selectByExample(hotelExample);
        if (!CollectionUtils.isEmpty(pmsProductHotels)){
            SimpleDateFormat md = new SimpleDateFormat("MM-dd");
            SimpleDateFormat ymd = new SimpleDateFormat("yyyy-MM-dd");
            PmsProductHotel pmsProductHotel = pmsProductHotels.get(0);
            HotelExtInfo extInfo = new HotelExtInfo();
            String startDate = desirableDates.get(0);
            String endDate = ymd.format(getDate(getDateTime(desirableDates.get(desirableDates.size() - 1)), 0));
            extInfo.setStartDate(md.format(getDateTime(startDate)) + " " + DateTimeUtils.dateToWeek(startDate));
            extInfo.setEndDate(md.format(getDateTime(endDate)) + " " + DateTimeUtils.dateToWeek(endDate));
            extInfo.setHotelName(pmsProductHotel.getHotelName());
            extInfo.setRoomType(pmsProductHotel.getRoomType());
            extInfo.setPrices(priceIds);
            orderItem.setSp3(JSONObject.toJSONString(extInfo));
        }
		omsOrderItemMapper.insertSelective(orderItem);
		return orderItem;
	}

	private static Date getDateTime(String ymd) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return formatter.parse(ymd);
        } catch (ParseException e) {
            logger.error("", e);
        }
        return null;
    }

    private static Date getDate(Date date, int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, i);
        date = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf.format(date) + " 00:00:00";
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf1.parse(format);
        } catch (ParseException e) {
            logger.error("", e);
        }
        return null;
    }

    /**
     * 会员个人订单列表
     * @param param
     * @return
     */
    @Override
    public Page<PortalOrderBaseInfo> selectOrderList(OrderListParam param, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        return portalOrderDao.selectOrderPage(param);
    }

    /**
     * 会员个人订单详情
     * @param id
     * @return
     */
    @Override
    public PortalOrderDetailInfo selectOrderDetail(Long id, String userId) {
        PortalOrderDetailInfo detailInfo = portalOrderDao.selectOrderDetail(id, userId);
        detailInfo.setDesirableDates(JSONArray.parseArray(detailInfo.getDesirableTimeStr(), String.class));
        detailInfo.setDesirableTimeStr(null);
        return detailInfo;
    }

//    @Override
//    public ConfirmOrderResult generateConfirmOrder() {
//        ConfirmOrderResult result = new ConfirmOrderResult();
//        //获取购物车信息
//        UmsMember currentMember = memberService.getCurrentMember();
//        List<CartPromotionItem> cartPromotionItemList = cartItemService.listPromotion(currentMember.getId());
//        result.setCartPromotionItemList(cartPromotionItemList);
//        //获取用户收货地址列表
//        List<UmsMemberReceiveAddress> memberReceiveAddressList = memberReceiveAddressService.list();
//        result.setMemberReceiveAddressList(memberReceiveAddressList);
//        //获取用户可用优惠券列表
//        List<SmsCouponHistoryDetail> couponHistoryDetailList = memberCouponService.listCart(cartPromotionItemList, 1);
//        result.setCouponHistoryDetailList(couponHistoryDetailList);
//        //获取用户积分
//        result.setMemberIntegration(currentMember.getIntegration());
//        //获取积分使用规则
//        UmsIntegrationConsumeSetting integrationConsumeSetting = integrationConsumeSettingMapper.selectByPrimaryKey(1L);
//        result.setIntegrationConsumeSetting(integrationConsumeSetting);
//        //计算总金额、活动优惠、应付金额
//        ConfirmOrderResult.CalcAmount calcAmount = calcCartAmount(cartPromotionItemList);
//        result.setCalcAmount(calcAmount);
//        return result;
//    }

//    @Override
//    public CommonResult generateOrder(OrderParam orderParam) {
//        List<OmsOrderItem> orderItemList = new ArrayList<>();
//        //获取购物车及优惠信息
//        UmsMember currentMember = memberService.getCurrentMember();
//        List<CartPromotionItem> cartPromotionItemList = cartItemService.listPromotion(currentMember.getId());
//        for (CartPromotionItem cartPromotionItem : cartPromotionItemList) {
//            //生成下单商品信息
//            OmsOrderItem orderItem = new OmsOrderItem();
//            orderItem.setProductId(cartPromotionItem.getProductId());
//            orderItem.setProductName(cartPromotionItem.getProductName());
//            orderItem.setProductPic(cartPromotionItem.getProductPic());
//            orderItem.setProductAttr(cartPromotionItem.getProductAttr());
//            orderItem.setProductBrand(cartPromotionItem.getProductBrand());
//            orderItem.setProductSn(cartPromotionItem.getProductSn());
//            orderItem.setProductPrice(cartPromotionItem.getPrice());
//            orderItem.setProductQuantity(cartPromotionItem.getQuantity());
//            orderItem.setProductSkuId(cartPromotionItem.getProductSkuId());
//            orderItem.setProductSkuCode(cartPromotionItem.getProductSkuCode());
//            orderItem.setProductCategoryId(cartPromotionItem.getProductCategoryId());
//            orderItem.setPromotionAmount(cartPromotionItem.getReduceAmount());
//            orderItem.setPromotionName(cartPromotionItem.getPromotionMessage());
//            orderItem.setGiftIntegration(cartPromotionItem.getIntegration());
//            orderItem.setGiftGrowth(cartPromotionItem.getGrowth());
//            orderItemList.add(orderItem);
//        }
//        //判断购物车中商品是否都有库存
//        if (!hasStock(cartPromotionItemList)) {
//            return new CommonResult().failed("库存不足，无法下单");
//        }
//        //判断使用使用了优惠券
//        if (orderParam.getCouponId() == null) {
//            //不用优惠券
//            for (OmsOrderItem orderItem : orderItemList) {
//                orderItem.setCouponAmount(new BigDecimal(0));
//            }
//        } else {
//            //使用优惠券
//            SmsCouponHistoryDetail couponHistoryDetail = getUseCoupon(cartPromotionItemList, orderParam.getCouponId());
//            if (couponHistoryDetail == null) {
//                return new CommonResult().failed("该优惠券不可用");
//            }
//            //对下单商品的优惠券进行处理
//            handleCouponAmount(orderItemList, couponHistoryDetail);
//        }
//        //判断是否使用积分
//        if (orderParam.getUseIntegration() == null) {
//            //不使用积分
//            for (OmsOrderItem orderItem : orderItemList) {
//                orderItem.setIntegrationAmount(new BigDecimal(0));
//            }
//        } else {
//            //使用积分
//            BigDecimal totalAmount = calcTotalAmount(orderItemList);
//            BigDecimal integrationAmount = getUseIntegrationAmount(orderParam.getUseIntegration(), totalAmount, currentMember, orderParam.getCouponId() != null);
//            if (integrationAmount.compareTo(new BigDecimal(0)) == 0) {
//                return new CommonResult().failed("积分不可用");
//            } else {
//                //可用情况下分摊到可用商品中
//                for (OmsOrderItem orderItem : orderItemList) {
//                    BigDecimal perAmount = orderItem.getProductPrice().divide(totalAmount, 3,RoundingMode.HALF_EVEN).multiply(integrationAmount);
//                    orderItem.setIntegrationAmount(perAmount);
//                }
//            }
//        }
//        //计算order_item的实付金额
//        handleRealAmount(orderItemList);
//        //进行库存锁定
//        lockStock(cartPromotionItemList);
//        //根据商品合计、运费、活动优惠、优惠券、积分计算应付金额
//        OmsOrder order = new OmsOrder();
//        order.setDiscountAmount(new BigDecimal(0));
//        order.setTotalAmount(calcTotalAmount(orderItemList));
//        order.setFreightAmount(new BigDecimal(0));
//        order.setPromotionAmount(calcPromotionAmount(orderItemList));
//        order.setPromotionInfo(getOrderPromotionInfo(orderItemList));
//        if (orderParam.getCouponId() == null) {
//            order.setCouponAmount(new BigDecimal(0));
//        } else {
//            order.setCouponId(orderParam.getCouponId());
//            order.setCouponAmount(calcCouponAmount(orderItemList));
//        }
//        if (orderParam.getUseIntegration() == null) {
//            order.setIntegration(0);
//            order.setIntegrationAmount(new BigDecimal(0));
//        } else {
//            order.setIntegration(orderParam.getUseIntegration());
//            order.setIntegrationAmount(calcIntegrationAmount(orderItemList));
//        }
//        order.setPayAmount(calcPayAmount(order));
//        //转化为订单信息并插入数据库
//        order.setMemberId(currentMember.getId());
//        order.setCreateTime(new Date());
//        order.setMemberUsername(currentMember.getUsername());
//        //支付方式：0->未支付；1->支付宝；2->微信
//        order.setPayType(orderParam.getPayType());
//        //订单来源：0->PC订单；1->app订单
//        order.setSourceType(1);
//        //订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单
//        order.setStatus(0);
//        //订单类型：0->正常订单；1->秒杀订单
//        order.setOrderType(0);
//        //收货人信息：姓名、电话、邮编、地址
//        UmsMemberReceiveAddress address = memberReceiveAddressService.getItem(orderParam.getMemberReceiveAddressId());
//        order.setReceiverName(address.getName());
//        order.setReceiverPhone(address.getPhoneNumber());
//        order.setReceiverPostCode(address.getPostCode());
//        order.setReceiverProvince(address.getProvince());
//        order.setReceiverCity(address.getCity());
//        order.setReceiverRegion(address.getRegion());
//        order.setReceiverDetailAddress(address.getDetailAddress());
//        //0->未确认；1->已确认
//        order.setConfirmStatus(0);
//        order.setDeleteStatus(0);
//        //计算赠送积分
//        order.setIntegration(calcGifIntegration(orderItemList));
//        //计算赠送成长值
//        order.setGrowth(calcGiftGrowth(orderItemList));
//        //生成订单号
//        order.setOrderSn(generateOrderSn(order));
//        // TODO: 2018/9/3 bill_*,delivery_*
//        //插入order表和order_item表
//        orderMapper.insert(order);
//        for (OmsOrderItem orderItem : orderItemList) {
//            orderItem.setOrderId(order.getId());
//            orderItem.setOrderSn(order.getOrderSn());
//        }
//        orderItemDao.insertList(orderItemList);
//        //如使用优惠券更新优惠券使用状态
//        if(orderParam.getCouponId()!=null){
//            updateCouponStatus(orderParam.getCouponId(),currentMember.getId(),1);
//        }
//        //如使用积分需要扣除积分
//        if(orderParam.getUseIntegration()!=null){
//            order.setUseIntegration(orderParam.getUseIntegration());
//            memberService.updateIntegration(currentMember.getId(),currentMember.getIntegration()-orderParam.getUseIntegration());
//        }
//        //删除购物车中的下单商品
//        deleteCartItemList(cartPromotionItemList,currentMember);
//        Map<String,Object> result = new HashMap<>();
//        result.put("order",order);
//        result.put("orderItemList",orderItemList);
//        return new CommonResult().success("下单成功", result);
//    }

    @Override
    public CommonResult paySuccess(Long orderId, Integer payType) {
        //修改订单支付状态
        OmsOrder order = new OmsOrder();
        order.setId(orderId);
        order.setStatus(1);
        order.setPaymentTime(new Date());
        order.setPayType(payType);
        orderMapper.updateByPrimaryKeySelective(order);
        //恢复所有下单商品的锁定库存，扣减真实库存
        OmsOrderDetail orderDetail = portalOrderDao.getDetail(orderId);
        int count = portalOrderDao.updateSkuStock(orderDetail.getOrderItemList());
        OmsOrderItemExample example = new OmsOrderItemExample();
        example.createCriteria().andOrderIdEqualTo(orderId);
        List<OmsOrderItem> omsOrderItems = omsOrderItemMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(omsOrderItems)){
            OmsOrderItem omsOrderItem = omsOrderItems.get(0);
            Long productId = omsOrderItem.getProductId();
            Integer productQuantity = omsOrderItem.getProductQuantity();
            pmsProductDao.updateSale(productId, productQuantity);
        }
        return new CommonResult().success("支付成功",count);
    }

//    @Override
//    public CommonResult cancelTimeOutOrder() {
//        OmsOrderSetting orderSetting = orderSettingMapper.selectByPrimaryKey(1L);
//        //查询超时、未支付的订单及订单详情
//        List<OmsOrderDetail> timeOutOrders = portalOrderDao.getTimeOutOrders(orderSetting.getNormalOrderOvertime());
//        if(CollectionUtils.isEmpty(timeOutOrders)){
//            return new CommonResult().failed("暂无超时订单");
//        }
//        //修改订单状态为交易取消
//        List<Long> ids = new ArrayList<>();
//        for (OmsOrderDetail timeOutOrder : timeOutOrders) {
//            ids.add(timeOutOrder.getId());
//        }
//        portalOrderDao.updateOrderStatus(ids,4);
//        for (OmsOrderDetail timeOutOrder : timeOutOrders) {
//            //解除订单商品库存锁定
//            portalOrderDao.releaseSkuStockLock(timeOutOrder.getOrderItemList());
//            //修改优惠券使用状态
//            updateCouponStatus(timeOutOrder.getCouponId(),timeOutOrder.getMemberId(),0);
//            //返还使用积分
//            if(timeOutOrder.getUseIntegration()!=null){
//                UmsMember member = memberService.getById(timeOutOrder.getMemberId());
//                memberService.updateIntegration(timeOutOrder.getMemberId(),member.getIntegration()+timeOutOrder.getUseIntegration());
//            }
//        }
//        return new CommonResult().success(null);
//    }

//    @Override
//    public void cancelOrder(Long orderId) {
//        //查询为付款的取消订单
//        OmsOrderExample example = new OmsOrderExample();
//        example.createCriteria().andIdEqualTo(orderId).andStatusEqualTo(0).andDeleteStatusEqualTo(0);
//        List<OmsOrder> cancelOrderList = orderMapper.selectByExample(example);
//        if(CollectionUtils.isEmpty(cancelOrderList)){
//            return;
//        }
//        OmsOrder cancelOrder = cancelOrderList.get(0);
//        if(cancelOrder!=null){
//            //修改订单状态为取消
//            cancelOrder.setStatus(4);
//            orderMapper.updateByPrimaryKeySelective(cancelOrder);
//            OmsOrderItemExample orderItemExample=new OmsOrderItemExample();
//            orderItemExample.createCriteria().andOrderIdEqualTo(orderId);
//            List<OmsOrderItem> orderItemList = orderItemMapper.selectByExample(orderItemExample);
//            //解除订单商品库存锁定
//            portalOrderDao.releaseSkuStockLock(orderItemList);
//            //修改优惠券使用状态
//            updateCouponStatus(cancelOrder.getCouponId(),cancelOrder.getMemberId(),0);
//            //返还使用积分
//            if(cancelOrder.getUseIntegration()!=null){
//                UmsMember member = memberService.getById(cancelOrder.getMemberId());
//                memberService.updateIntegration(cancelOrder.getMemberId(),member.getIntegration()+cancelOrder.getUseIntegration());
//            }
//        }
//    }

//    @Override
//    public void sendDelayMessageCancelOrder(Long orderId) {
//        //获取订单超时时间
//        OmsOrderSetting orderSetting = orderSettingMapper.selectByPrimaryKey(1L);
//        long delayTimes = orderSetting.getNormalOrderOvertime()*60*1000;
//        //发送延迟消息
//        cancelOrderSender.sendMessage(orderId,delayTimes);
//    }

    /**
     * 校验产品信息
     * @param pmsProduct
     */
    private void checkProductInfo(PmsProduct pmsProduct, List<Long> priceIds, int adultNum
            , int childNum, BigDecimal totalPrice, Boolean isActivity, String skuId) {
        if (null == pmsProduct) {
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "未找到产品信息");
        }
        if (YesOrNo.YES.getCode().intValue() == pmsProduct.getDeleteStatus()) {
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品不可购买");
        }
        if (YesOrNo.NO.getCode().intValue() == pmsProduct.getPublishStatus()) {
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品已下架");
        }
//        if (YesOrNo.NO.getCode().equals(pmsProduct.getVerifyStatus())) {
//            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "该产品审核未通过");
//        }
//        if (0 >= pmsProduct.getStock()) {
//            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品已售罄");
//        }
        
        Byte businessType = pmsProduct.getBusinessType();
//        if(null == businessType) {
//        	throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品类型丢失");
//        }
        
//        if(null == businessType || BusinessType.TRAVEL.getCode().equals(businessType)) {
//        	if (0 >= pmsProduct.getStock()) {
//                throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品已售罄");
//            }
//        	return;
//        }
        
        if(CollectionUtils.isEmpty(priceIds) && (BusinessType.FLIGHT.getCode().equals(businessType)
                || BusinessType.HOTEL.getCode().equals(businessType) || BusinessType.TRAVEL.getCode().equals(businessType)) ) {
        	throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品价格丢失");
        }
        
        if(BusinessType.FLIGHT.getCode().equals(businessType)) {
        	checkFlightProduct(priceIds, adultNum, childNum, totalPrice, isActivity);
        }
        if(BusinessType.HOTEL.getCode().equals(businessType)) {
        	checkHotelProduct(priceIds, adultNum + childNum, totalPrice, isActivity);
        }
        if(BusinessType.TRAVEL.getCode().equals(businessType)) {
            checkTravelProduct(priceIds, adultNum, childNum, totalPrice, isActivity);
        }
        if(BusinessType.COMMON.getCode().equals(businessType) || BusinessType.OTHER.getCode().equals(businessType)) {
            checkCommonProduct(adultNum + childNum, totalPrice, isActivity, skuId);
        }
    }

    @Transactional
	private void checkFlightProduct(List<Long> priceIds, int adultNum, int childNum,
                                    BigDecimal totalPrice, Boolean isActivity) {
		PmsProductFlightPrice price = pmsProductFlightPriceMapper.selectByPrimaryKey(priceIds.get(0));
		if (null == price) {
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品价格丢失");
		}

		if (YesOrNo.NO.getCode().intValue() == price.getShowStatus()) {
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品已下架");
		}

		int number = adultNum + childNum;
		int stock = price.getStock() - number;
		if (0 > stock) {
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
		}

		// 扣减库存，stock属性设置为无符号，扣减为负数sql自动执行失败
		int res = 0;
		try {
			res = pmsProductFlightDao.updateFlightStock(price.getId(), number);
		} catch (Exception e) {
			logger.error("机票扣减库存失败：", e);
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
		}
		if (res < 1) {
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
		}
		
		BigDecimal childPurchasePrice = price.getPurchasePrice();
		if(price.getChildPurchasePrice() != null && price.getChildPurchasePrice().compareTo(BigDecimal.ZERO) == 1) {
			childPurchasePrice = price.getChildPurchasePrice();
		}
		
		BigDecimal adultPrice = price.getPurchasePrice().multiply(new BigDecimal(adultNum));
		BigDecimal childPrice = childPurchasePrice.multiply(new BigDecimal(childNum));
		BigDecimal orderPrice = adultPrice.add(childPrice);
		if(orderPrice.compareTo(totalPrice) != 0 && !isActivity) {
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "订单总价错误");
		}
	}
    
	@Transactional
	private void checkHotelProduct(List<Long> priceIds, int number, BigDecimal totalPrice, Boolean isActivity) {

		PmsProductHotelExample example = new PmsProductHotelExample();
		example.createCriteria().andIdIn(priceIds);

		List<PmsProductHotel> hotels = pmsProductHotelMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(hotels)) {
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品价格丢失");
		}

		BigDecimal orderPrice = BigDecimal.ZERO;
		for (PmsProductHotel hotel : hotels) {
			if (YesOrNo.NO.getCode().equals(hotel.getStatus())
					|| YesOrNo.YES.getCode().equals(hotel.getDeleteStatus())) {
				throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR,
						DateTimeUtils.parseDateToString(hotel.getArrivalDate(), DateTimeUtils.PATTEN_YYYY_MM_DD)
								+ hotel.getRoomType() + "产品已下架");
			}

			int stock = hotel.getStock() - number;
			if (0 > stock) {
				throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR,
						DateTimeUtils.parseDateToString(hotel.getArrivalDate(), DateTimeUtils.PATTEN_YYYY_MM_DD)
								+ hotel.getRoomType() + "产品库存不足");
			}
			
			orderPrice = orderPrice.add(hotel.getSalePrice().multiply(new BigDecimal(number)));
		}
		
		if(orderPrice.compareTo(totalPrice) != 0 && !isActivity) {
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "订单总价错误");
		}

		int res = 0;
		try {
			res = pmsProductHotelDao.updateHotelStock(priceIds, number);
		} catch (Exception e) {
			logger.error("酒店扣减库存失败：", e);
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
		}
		if (res < priceIds.size()) {
			throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
		}
		
	}

	@Transactional
    private void checkCommonProduct(int num, BigDecimal totalPrice, Boolean isActivity, String skuId) {
        if (num < 1)
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "商品数量错误");
        if (StringUtils.isEmpty(skuId))
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "sku不能为空");
        PmsCommonProductSku pmsCommonProductSku = pmsCommonProductSkuMapper.selectByPrimaryKey(skuId);
        Long stock = pmsCommonProductSku.getStock();
        if (stock.intValue() < num)
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
        if (!isActivity && pmsCommonProductSku.getPrice().multiply(new BigDecimal(num + "")).compareTo(totalPrice) != 0)
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "订单总价错误");
        try{
            pmsCommonProductSkuMapper.updateStock(skuId, -num);
        } catch (Exception e){
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
        }
    }

    @Transactional
    private void checkTravelProduct(List<Long> priceIds, int adultNum, int childNum, BigDecimal totalPrice, Boolean isActivity) {
        if (CollectionUtils.isEmpty(priceIds))
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "班期不能为空");
        PmsProductGroupPriceExample example = new PmsProductGroupPriceExample();
        example.createCriteria().andIdIn(priceIds);
        List<PmsProductGroupPrice> pmsProductGroupPrices = pmsProductGroupPriceMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(pmsProductGroupPrices)) {
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品价格丢失");
        }
        PmsProductGroupPrice ppgp = pmsProductGroupPrices.get(0);
        Long groupId = ppgp.getGroupId();
        PmsProductGroup pmsProductGroup = pmsProductGroupMapper.selectByPrimaryKey(groupId);
        if (new Date().getTime() > pmsProductGroup.getEnrollEndTime().getTime())
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "当前时间已超过班期报名截止时间");
        BigDecimal adultPrice = null;
        BigDecimal childPrice = null;

        for (PmsProductGroupPrice pmsProductGroupPrice : pmsProductGroupPrices) {
            Byte priceType = pmsProductGroupPrice.getPriceType();
            if (PriceType.ADULT.getCode().equals(priceType)){
                adultPrice = pmsProductGroupPrice.getSupplyPrice();
            }
            if (PriceType.CHILD.getCode().equals(priceType)){
                childPrice = pmsProductGroupPrice.getSupplyPrice();
            }
        }
        childPrice = childPrice == null ? adultPrice : childPrice;
        BigDecimal totalFee = adultPrice.multiply(new BigDecimal(adultNum + "")).add(childPrice.multiply(new BigDecimal(childNum + "")));


        int adultStock = 0;

        int childStock = 0;

        if (!CollectionUtils.isEmpty(pmsProductGroupPrices)){
            int count = 0;
            for (PmsProductGroupPrice pmsProductGroupPrice : pmsProductGroupPrices) {
                Long id = pmsProductGroupPrice.getId();
                Byte showStatus = pmsProductGroupPrice.getShowStatus();
                if (new Byte("1").equals(showStatus)){
                    Byte priceType = pmsProductGroupPrice.getPriceType();
                    if (new Byte("1").equals(priceType)){
                        adultStock = pmsProductGroupPrice.getMaxPeople();
                        if (adultStock - adultNum < 0){
                            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
                        }
                        try {
                            pmsProductGroupPriceMapper.updateStock(id, -adultNum);
                            count+=adultNum;
                        } catch (Exception e) {
                            logger.error("旅游产品扣减库存失败：", e);
                            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
                        }
                    }
                    if (new Byte("2").equals(priceType)){
                        childStock = pmsProductGroupPrice.getMaxPeople();
                        if (childStock - childNum < 0){
                            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
                        }
                        try {
                            pmsProductGroupPriceMapper.updateStock(id, -childNum);
                            count+=childNum;
                        } catch (Exception e) {
                            logger.error("旅游产品扣减库存失败：", e);
                            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "产品库存不足");
                        }
                    }
                }
            }
            pmsProductGroupMapper.updateStock(pmsProductGroupPrices.get(0).getGroupId(), -count);
        }

        if(!isActivity && totalFee.compareTo(totalPrice) != 0) {
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "订单总价错误");
        }
    }

    /**
     * 生成18位订单编号:8位日期+2位平台号码+2位支付方式+6位以上自增id
     */
    private String generateOrderSn(OmsOrder order) {
        if (null == order.getSourceType() || null == order.getPayType()) {
            throw new BusinessException(BusinessExceptionCode.CREATE_ORDER_ERROR, "生成订单号失败");
        }
        StringBuilder sb = new StringBuilder();
        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String key = REDIS_KEY_PREFIX_ORDER_ID + date;
        Long increment = redisService.increment(key, 1);
        sb.append(date);
        sb.append(String.format("%02d",order.getSourceType()));
        sb.append(String.format("%02d",order.getPayType()));
        String incrementStr = increment.toString();
        if(incrementStr.length()<=6){
            sb.append(String.format("%06d",increment));
        }else{
            sb.append(incrementStr);
        }
        return sb.toString();
    }

    /**
     * 删除下单商品的购物车信息
     */
//    private void deleteCartItemList(List<CartPromotionItem> cartPromotionItemList, UmsMember currentMember) {
//        List<Long> ids = new ArrayList<>();
//        for (CartPromotionItem cartPromotionItem : cartPromotionItemList) {
//            ids.add(cartPromotionItem.getId());
//        }
//        cartItemService.delete(currentMember.getId(),ids);
//    }

    /**
     * 计算该订单赠送的成长值
     */
    private Integer calcGiftGrowth(List<OmsOrderItem> orderItemList) {
        Integer sum=0;
        for (OmsOrderItem orderItem : orderItemList) {
            sum=sum+orderItem.getGiftGrowth()*orderItem.getProductQuantity();
        }
        return sum;
    }

    /**
     * 计算该订单赠送的积分
     */
    private Integer calcGifIntegration(List<OmsOrderItem> orderItemList) {
        int sum=0;
        for (OmsOrderItem orderItem : orderItemList) {
            sum+=orderItem.getGiftIntegration()*orderItem.getProductQuantity();
        }
        return sum;
    }

    /**
     * 将优惠券信息更改为指定状态
     * @param couponId 优惠券id
     * @param memberId 会员id
     * @param useStatus 0->未使用；1->已使用
     */
    private void updateCouponStatus(Long couponId, Long memberId,Integer useStatus) {
        if(couponId==null)return;
        //查询第一张优惠券
        SmsCouponHistoryExample example = new SmsCouponHistoryExample();
        example.createCriteria().andMemberIdEqualTo(memberId)
                .andCouponIdEqualTo(couponId).andUseStatusEqualTo(useStatus==0?1:0);
        List<SmsCouponHistory> couponHistoryList = couponHistoryMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(couponHistoryList)){
            SmsCouponHistory couponHistory = couponHistoryList.get(0);
            couponHistory.setUseTime(new Date());
            couponHistory.setUseStatus(useStatus);
            couponHistoryMapper.updateByPrimaryKeySelective(couponHistory);
        }
    }

    private void handleRealAmount(List<OmsOrderItem> orderItemList) {
        for (OmsOrderItem orderItem : orderItemList) {
            //原价-促销价格-优惠券抵扣-积分抵扣
            BigDecimal realAmount = orderItem.getProductPrice()
                    .subtract(orderItem.getPromotionAmount())
                    .subtract(orderItem.getCouponAmount())
                    .subtract(orderItem.getIntegrationAmount());
            orderItem.setRealAmount(realAmount);
        }
    }

    /**
     * 获取订单促销信息
     */
    private String getOrderPromotionInfo(List<OmsOrderItem> orderItemList) {
        StringBuilder sb = new StringBuilder();
        for (OmsOrderItem orderItem : orderItemList) {
            sb.append(orderItem.getPromotionName());
            sb.append(",");
        }
        String result = sb.toString();
        if (result.endsWith(",")) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    /**
     * 计算订单应付金额
     */
    private BigDecimal calcPayAmount(OmsOrder order) {
        //总金额+运费-促销优惠-优惠券优惠-积分抵扣
        BigDecimal payAmount = order.getTotalAmount()
                .add(order.getFreightAmount())
                .subtract(order.getPromotionAmount())
                .subtract(order.getCouponAmount())
                .subtract(order.getIntegrationAmount());
        return payAmount;
    }

    /**
     * 计算订单优惠券金额
     */
    private BigDecimal calcIntegrationAmount(List<OmsOrderItem> orderItemList) {
        BigDecimal integrationAmount = new BigDecimal(0);
        for (OmsOrderItem orderItem : orderItemList) {
            if (orderItem.getIntegrationAmount() != null) {
                integrationAmount = integrationAmount.add(orderItem.getIntegrationAmount().multiply(new BigDecimal(orderItem.getProductQuantity())));
            }
        }
        return integrationAmount;
    }

    /**
     * 计算订单优惠券金额
     */
    private BigDecimal calcCouponAmount(List<OmsOrderItem> orderItemList) {
        BigDecimal couponAmount = new BigDecimal(0);
        for (OmsOrderItem orderItem : orderItemList) {
            if (orderItem.getCouponAmount() != null) {
                couponAmount = couponAmount.add(orderItem.getCouponAmount().multiply(new BigDecimal(orderItem.getProductQuantity())));
            }
        }
        return couponAmount;
    }

    /**
     * 计算订单活动优惠
     */
    private BigDecimal calcPromotionAmount(List<OmsOrderItem> orderItemList) {
        BigDecimal promotionAmount = new BigDecimal(0);
        for (OmsOrderItem orderItem : orderItemList) {
            if (orderItem.getPromotionAmount() != null) {
                promotionAmount = promotionAmount.add(orderItem.getPromotionAmount().multiply(new BigDecimal(orderItem.getProductQuantity())));
            }
        }
        return promotionAmount;
    }

    /**
     * 获取可用积分抵扣金额
     *
     * @param useIntegration 使用的积分数量
     * @param totalAmount    订单总金额
     * @param currentMember  使用的用户
     * @param hasCoupon      是否已经使用优惠券
     */
    private BigDecimal getUseIntegrationAmount(Integer useIntegration, BigDecimal totalAmount, UmsMember currentMember, boolean hasCoupon) {
        BigDecimal zeroAmount = new BigDecimal(0);
        //判断用户是否有这么多积分
        if (useIntegration.compareTo(currentMember.getIntegration()) > 0) {
            return zeroAmount;
        }
        //根据积分使用规则判断使用可用
        //是否可用于优惠券共用
        UmsIntegrationConsumeSetting integrationConsumeSetting = integrationConsumeSettingMapper.selectByPrimaryKey(1L);
        if (hasCoupon && integrationConsumeSetting.getCouponStatus().equals(0)) {
            //不可与优惠券共用
            return zeroAmount;
        }
        //是否达到最低使用积分门槛
        if (useIntegration.compareTo(integrationConsumeSetting.getUseUnit()) < 0) {
            return zeroAmount;
        }
        //是否超过订单抵用最高百分比
        BigDecimal integrationAmount = new BigDecimal(useIntegration).divide(new BigDecimal(integrationConsumeSetting.getUseUnit()), 2,RoundingMode.HALF_EVEN);
        BigDecimal maxPercent = new BigDecimal(integrationConsumeSetting.getMaxPercentPerOrder()).divide(new BigDecimal(100), 2, RoundingMode.HALF_EVEN);
        if (integrationAmount.compareTo(totalAmount.multiply(maxPercent)) > 0) {
            return zeroAmount;
        }
        return integrationAmount;
    }

    /**
     * 对优惠券优惠进行处理
     *
     * @param orderItemList       order_item列表
     * @param couponHistoryDetail 可用优惠券详情
     */
    private void handleCouponAmount(List<OmsOrderItem> orderItemList, SmsCouponHistoryDetail couponHistoryDetail) {
        SmsCoupon coupon = couponHistoryDetail.getCoupon();
        if (coupon.getUseType().equals(0)) {
            //全场通用
            calcPerCouponAmount(orderItemList, coupon);
        } else if (coupon.getUseType().equals(1)) {
            //指定分类
            List<OmsOrderItem> couponOrderItemList = getCouponOrderItemByRelation(couponHistoryDetail, orderItemList, 0);
            calcPerCouponAmount(couponOrderItemList, coupon);
        } else if (coupon.getUseType().equals(2)) {
            //指定商品
            List<OmsOrderItem> couponOrderItemList = getCouponOrderItemByRelation(couponHistoryDetail, orderItemList, 1);
            calcPerCouponAmount(couponOrderItemList, coupon);
        }
    }

    /**
     * 对每个下单商品进行优惠券金额分摊的计算
     *
     * @param orderItemList 可用优惠券的下单商品商品
     */
    private void calcPerCouponAmount(List<OmsOrderItem> orderItemList, SmsCoupon coupon) {
        BigDecimal totalAmount = calcTotalAmount(orderItemList);
        for (OmsOrderItem orderItem : orderItemList) {
            //(商品价格/可用商品总价)*优惠券面额
            BigDecimal couponAmount = orderItem.getProductPrice().divide(totalAmount, 3, RoundingMode.HALF_EVEN).multiply(coupon.getAmount());
            orderItem.setCouponAmount(couponAmount);
        }
    }

    /**
     * 获取与优惠券有关系的下单商品
     *
     * @param couponHistoryDetail 优惠券详情
     * @param orderItemList       下单商品
     * @param type                使用关系类型：0->相关分类；1->指定商品
     */
    private List<OmsOrderItem> getCouponOrderItemByRelation(SmsCouponHistoryDetail couponHistoryDetail, List<OmsOrderItem> orderItemList, int type) {
        List<OmsOrderItem> result = new ArrayList<>();
        if (type == 0) {
            List<Long> categoryIdList = new ArrayList<>();
            for (SmsCouponProductCategoryRelation productCategoryRelation : couponHistoryDetail.getCategoryRelationList()) {
                categoryIdList.add(productCategoryRelation.getProductCategoryId());
            }
            for (OmsOrderItem orderItem : orderItemList) {
                if (categoryIdList.contains(orderItem.getProductCategoryId())) {
                    result.add(orderItem);
                } else {
                    orderItem.setCouponAmount(new BigDecimal(0));
                }
            }
        } else if (type == 1) {
            List<Long> productIdList = new ArrayList<>();
            for (SmsCouponProductRelation productRelation : couponHistoryDetail.getProductRelationList()) {
                productIdList.add(productRelation.getProductId());
            }
            for (OmsOrderItem orderItem : orderItemList) {
                if (productIdList.contains(orderItem.getProductId())) {
                    result.add(orderItem);
                } else {
                    orderItem.setCouponAmount(new BigDecimal(0));
                }
            }
        }
        return result;
    }

    /**
     * 获取该用户可以使用的优惠券
     *
     * @param cartPromotionItemList 购物车优惠列表
     * @param couponId              使用优惠券id
     */
//    private SmsCouponHistoryDetail getUseCoupon(List<CartPromotionItem> cartPromotionItemList, Long couponId) {
//        List<SmsCouponHistoryDetail> couponHistoryDetailList = memberCouponService.listCart(cartPromotionItemList, 1);
//        for (SmsCouponHistoryDetail couponHistoryDetail : couponHistoryDetailList) {
//            if (couponHistoryDetail.getCoupon().getId().equals(couponId)) {
//                return couponHistoryDetail;
//            }
//        }
//        return null;
//    }

    /**
     * 计算总金额
     */
    private BigDecimal calcTotalAmount(List<OmsOrderItem> orderItemList) {
        BigDecimal totalAmount = new BigDecimal("0");
        for (OmsOrderItem item : orderItemList) {
            totalAmount = totalAmount.add(item.getProductPrice().multiply(new BigDecimal(item.getProductQuantity())));
        }
        return totalAmount;
    }

    /**
     * 锁定下单商品的所有库存
     */
    private void lockStock(List<CartPromotionItem> cartPromotionItemList) {
        for (CartPromotionItem cartPromotionItem : cartPromotionItemList) {
            PmsSkuStock skuStock = skuStockMapper.selectByPrimaryKey(cartPromotionItem.getProductSkuId());
            skuStock.setLockStock(skuStock.getLockStock() + cartPromotionItem.getQuantity());
            skuStockMapper.updateByPrimaryKeySelective(skuStock);
        }
    }

    /**
     * 判断下单商品是否都有库存
     */
    private boolean hasStock(List<CartPromotionItem> cartPromotionItemList) {
        for (CartPromotionItem cartPromotionItem : cartPromotionItemList) {
            if (cartPromotionItem.getRealStock() <= 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * 计算购物车中商品的价格
     */
    private ConfirmOrderResult.CalcAmount calcCartAmount(List<CartPromotionItem> cartPromotionItemList) {
        ConfirmOrderResult.CalcAmount calcAmount = new ConfirmOrderResult.CalcAmount();
        calcAmount.setFreightAmount(new BigDecimal(0));
        BigDecimal totalAmount = new BigDecimal("0");
        BigDecimal promotionAmount = new BigDecimal("0");
        for (CartPromotionItem cartPromotionItem : cartPromotionItemList) {
            totalAmount = totalAmount.add(cartPromotionItem.getPrice().multiply(new BigDecimal(cartPromotionItem.getQuantity())));
            promotionAmount = promotionAmount.add(cartPromotionItem.getReduceAmount().multiply(new BigDecimal(cartPromotionItem.getQuantity())));
        }
        calcAmount.setTotalAmount(totalAmount);
        calcAmount.setPromotionAmount(promotionAmount);
        calcAmount.setPayAmount(totalAmount.subtract(promotionAmount));
        return calcAmount;
    }

	@Override
	public PortalOrderBaseInfo selectOrderByOrderSn(String orderSn) {
		return portalOrderDao.selectOrderByOrderSn(orderSn);
	}

	@Override
	public int insertOrderPayHistory(Long orderId, BigDecimal fee) {

		try { // 捕获异常，避免影响其他业务
			OmsOrderOperateHistory record = new OmsOrderOperateHistory();
			record.setCreateTime(new Date());
			record.setNote("用户成功支付金额 ：" + String.valueOf(fee));
			record.setOperateMan(OrderOperateMan.USER.getCode());
			record.setOrderId(orderId);
			record.setOrderStatus(OrderStatus.wait_deliver.getCode());

			return omsOrderOperateHistoryMapper.insertSelective(record);
		} catch (Exception e) {
			logger.error("记录用户支付操作记录失败：", e);
		}
		return 0;
	}

    @Override
    public OmsOrder selectByPrimaryKey(Long orderId) {
        return omsOrderMapper.selectByPrimaryKey(orderId);
    }

    @Override
    public void updateInfo(OmsOrder omsOrder) {
        omsOrderMapper.updateByPrimaryKeySelective(omsOrder);
    }

    @Override
    public void autoRefund(List<String> list) {
        OmsOrderExample example = new OmsOrderExample();
        example.createCriteria().andActivityIdIn(list).andActivityTypeEqualTo(new Byte(CreateOrderType.GROUPBOOKING.getCode()));
        List<OmsOrder> omsOrders = omsOrderMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(omsOrders)){
            for (OmsOrder omsOrder : omsOrders) {
                try{
                    activityRemoteService.updateRefundStatus(omsOrder.getActivityId());
                    WechatRefundRequest request = new WechatRefundRequest();
                    request.setOrderNumber(omsOrder.getOrderSn());
                    request.setRefundAmount(omsOrder.getPayAmount());
                    wechatRemoteService.weixinRefund(request);
                } catch (Exception e){
                    logger.error("", e);
                }
            }
        }
    }

    @Override
    public Boolean getOrderInfo(Long orderId) {
        OmsOrder order = omsOrderMapper.selectByPrimaryKey(orderId);
        int status = order.getStatus().intValue();
        return !(status == 0 || status == 7 || status == 5 || status == 4);
    }

    @Override
    @Transactional
    public void cancelUnpaidOrderTask() {
        OmsOrderExample example = new OmsOrderExample();
        example.createCriteria().andPayDeadlineLessThanOrEqualTo(new Date()).andStatusEqualTo(0);
        //查出所有需要取消的订单
        List<OmsOrder> omsOrders = omsOrderMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(omsOrders)){
            List<Long> ids = new ArrayList<>();
            omsOrders.forEach(o -> {
                ids.add(o.getId());
            });
            OmsOrderItemExample itemExample = new OmsOrderItemExample();
            itemExample.createCriteria().andOrderIdIn(ids);
            //查出所有orderItem
            List<OmsOrderItem> omsOrderItems = omsOrderItemMapper.selectByExample(itemExample);
            Map<Long, OmsOrderItem> map = new HashedMap();
            if (!CollectionUtils.isEmpty(omsOrderItems)){
                omsOrderItems.forEach(item -> {
                    map.put(item.getOrderId(), item);
                });
            }
            for (OmsOrder o : omsOrders) {
                Byte businessType = o.getBusinessType();
                Long id = o.getId();
                OmsOrderItem omsOrderItem = map.get(id);
                String sp3 = omsOrderItem.getSp3();
                //旅游
                if (BusinessType.TRAVEL.getCode().equals(businessType)){
                    addTravelStock(sp3, omsOrderItem);
                    continue;
                }
                //机票
                if (BusinessType.FLIGHT.getCode().equals(businessType)){
                    addFlightStock(sp3, omsOrderItem);
                    continue;
                }
                //酒店
                if (BusinessType.HOTEL.getCode().equals(businessType)){
                    addHotelStock(sp3, omsOrderItem);
                    continue;
                }
                //门票、普通商品
                if (BusinessType.COMMON.getCode().equals(businessType) || BusinessType.OTHER.getCode().equals(businessType)){
                    addCommonStock(sp3, omsOrderItem);
                    continue;
                }
            }
            OmsOrder order = new OmsOrder();
            order.setStatus(4);
            omsOrderMapper.updateByExampleSelective(order, example);
        }
    }

    private void addCommonStock(String sp3, OmsOrderItem omsOrderItem) {
        CommonExtInfo commonExtInfo = JSONObject.parseObject(sp3, CommonExtInfo.class);
        String skuId = commonExtInfo.getSkuId();
        Integer adultNum = omsOrderItem.getAdultNum();
        Integer childNum = omsOrderItem.getChildNum();
        pmsCommonProductSkuMapper.updateStock(skuId, adultNum + childNum);
    }


    private void addHotelStock(String sp3, OmsOrderItem omsOrderItem){
        HotelExtInfo hotelExtInfo = JSONObject.parseObject(sp3, HotelExtInfo.class);
        List<Long> prices = hotelExtInfo.getPrices();
        Integer adultNum = omsOrderItem.getAdultNum();
        Integer childNum = omsOrderItem.getChildNum();
        for (Long price : prices) {
            pmsProductHotelMapper.updateStock(price, adultNum + childNum);
        }
    }

    private void addFlightStock(String sp3, OmsOrderItem omsOrderItem){
        FlightExtInfo flightExtInfo = JSONObject.parseObject(sp3, FlightExtInfo.class);
        List<Long> prices = flightExtInfo.getPrices();
        Integer adultNum = omsOrderItem.getAdultNum();
        Integer childNum = omsOrderItem.getChildNum();
        for (Long price : prices) {
            pmsProductFlightPriceMapper.updateStock(price, adultNum + childNum);
        }

    }

    private void addTravelStock(String sp3, OmsOrderItem omsOrderItem){
        TravelExtInfo extInfo = JSONObject.parseObject(sp3, TravelExtInfo.class);
        List<Long> prices = extInfo.getPrices();
        PmsProductGroupPriceExample priceExample = new PmsProductGroupPriceExample();
        priceExample.createCriteria().andIdIn(prices);
        List<PmsProductGroupPrice> pmsProductGroupPrices = pmsProductGroupPriceMapper.selectByExample(priceExample);
        Long groupId = null;
        Integer adultNum = omsOrderItem.getAdultNum();
        Integer childNum = omsOrderItem.getChildNum();
        for (PmsProductGroupPrice ppgp : pmsProductGroupPrices) {
            Byte priceType = ppgp.getPriceType();
            Long priceId = ppgp.getId();
            groupId = ppgp.getGroupId();
            if (PriceType.ADULT.getCode().equals(priceType)){
                pmsProductGroupPriceMapper.updateStock(priceId, adultNum);
            }
            if (PriceType.CHILD.getCode().equals(priceType)){
                pmsProductGroupPriceMapper.updateStock(priceId, childNum);
            }
        }
        //加库存
        pmsProductGroupMapper.updateStock(groupId, adultNum + childNum);
    }

}
