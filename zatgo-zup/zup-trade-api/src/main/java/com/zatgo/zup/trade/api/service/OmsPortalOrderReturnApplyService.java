package com.zatgo.zup.trade.api.service;


import com.zatgo.zup.trade.api.domain.OmsOrderReturnApplyParam;

/**
 * 订单退货管理Service
 * Created by chen on 2018/10/17.
 */
public interface OmsPortalOrderReturnApplyService {
    /**
     * 提交申请
     */
    int create(OmsOrderReturnApplyParam returnApply);
}
