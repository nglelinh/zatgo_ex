package com.zatgo.zup.trade.api.dao;

import com.zatgo.zup.trade.api.domain.PmsProductFlightResult;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface PmsProductFlightDao {
	
	PmsProductFlightResult selectProductFlightDetail(@Param("productId") Long productId);

	List<PmsProductFlightResult> selectProductFlightDetailByIds(@Param("ids") String ids);
	
	@Update("update pms_product_flight_price set stock = stock - #{number} where id = #{id}")
	int updateFlightStock(@Param("id") Long id, @Param("number") int number);

}
