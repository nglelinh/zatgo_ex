package com.zatgo.zup.trade.api.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.trade.api.mapper.MessageRecordMapper;
import com.zatgo.zup.trade.api.model.MoveUser;
import com.zatgo.zup.trade.api.service.impl.OmsPortalOrderServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2018/11/28.
 */


@Api(value = "/repairData",description = "修数据接口",produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/repairData")
public class RepairDataController {

    private static final Logger logger = LoggerFactory.getLogger(RepairDataController.class);
    @Autowired
    private MessageRecordMapper messageRecordMapper;


    @ApiOperation(value = "修复算力数据")
    @GetMapping("/getMoveUserSql")
    public void getMoveUserSql(HttpServletResponse response){
        int pageNo = 1;
        int pageSize = 100;
        int total = 2;
        List<MoveUser> all = new ArrayList<>();
        while (true) {
            if (pageNo > total){
                break;
            }
            PageHelper.startPage(pageNo, pageSize);
            List<MoveUser> list = messageRecordMapper.getUserInfo();
            PageInfo<MoveUser> info = new PageInfo<>(list);
            all.addAll(list);
            total = info.getPages();
            pageNo++;
        }
        StringBuffer sb = new StringBuffer();
        if (!CollectionUtils.isEmpty(all)){
            all.forEach(user -> {
                getSql(sb, user);
            });
        }
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/plain");
        response.addHeader("Content-Disposition","attachment;filename="
                + genAttachmentFileName("111", "JSON_FOR_UCC_")//设置名称格式，没有这个中文名称无法显示
                + ".txt");
        BufferedOutputStream buff = null;
        ServletOutputStream outStr = null;
        try {
            outStr = response.getOutputStream();
            buff = new BufferedOutputStream(outStr);
            buff.write(sb.toString().getBytes("utf-8"));
            buff.flush();
            buff.close();
        } catch (Exception e) {
            //LOGGER.error("导出文件文件出错:{}",e);
        } finally {try {
            buff.close();
            outStr.close();
        } catch (Exception e) {
            //LOGGER.error("关闭流对象出错 e:{}",e);
        }
        }

    }

    public  String genAttachmentFileName(String cnName, String defaultName) {
        try {
            cnName = new String(cnName.getBytes("gb2312"), "ISO8859-1");
        } catch (Exception e) {
            cnName = defaultName;
        }
        return cnName;
    }



    private void getSql(StringBuffer sb, MoveUser user){
        String openId = user.getOpenId();
        String nickName = user.getNickName();
        nickName = nickName.replace("'", "‘");
        String uuid = UUIDUtils.getUuid();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(new Date());
        String str = "INSERT INTO `user`(`user_id`, `user_name`, `user_password`, `user_pay_password`, `create_date`," +
                " `update_date`, `last_login_date`, `mnemonic_word`, `invite_code`, `invited_code`, `email_country_code`," +
                " `email`, `mobile`, `mobile_country_code`, `regist_type`, `cloud_user_id`, `is_cloud_manager`, `icon_url`," +
                " `nickname`, `cloud_manage_type`, `auth_level`, `wechat_open_id`) VALUES" +
                " ('"+ user.getUserId() +"', '"+ uuid +"', '$2a$12$iyNWwg/JzDhQbU1.uM4e0OfidHqvnSvpheH/z6zJ4PXhSABfMV7JK', '"+ uuid +"', '"+format+"', '"+format+"', '"+format+"'" +
                ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '8ce72a140c57480296cd48412677081f', 0, '"+user.getIcon()+"'," +
                " '"+nickName+"', NULL, 0, '"+openId+"');\n";
        logger.info(str);
        sb.append(str);
    }

}
