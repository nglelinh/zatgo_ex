package com.zatgo.zup.trade.api.dao;

import com.ykb.mall.common.model.PmsProductCategoryData;
import com.ykb.mall.common.model.PmsProductDetail;
import com.ykb.mall.common.model.PmsBrand;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/16 14:40
 */
public interface PmsProductDao {

    /**
     * 查询品牌
     * @return
     */
    @Select("select id, name, logo, sort from pms_brand where show_status = 1 order by sort desc")
    List<PmsBrand> selectBrandList();

    /**
     * 查询分类
     * @return
     */
    @Select("select id, parent_id, name,description, level, icon, sort, product_count from pms_product_category where nav_status = 1 and show_status = 1 order by sort desc")
    List<PmsProductCategoryData> selectProductCategoryList();

    /**
     * 查询产品详情
     * @param id
     * @return
     */
    PmsProductDetail selectProductDetail(@Param("id") Long id);
    
    @Select("select id, parent_id, name, level, icon, sort, product_count from pms_product_category where show_status = 1 order by parent_id, `level` asc, sort desc")
    List<PmsProductCategoryData> selectProductAllCategoryList();
    
    @Update("update pms_product set watch_num = watch_num + 1 where id = #{id}")
    int addProductWatchNum(@Param("id") Long id);
    
    @Update("update pms_product set click_num = click_num + 1 where id = #{id}")
    int addProductClickNum(@Param("id") Long id);
    
    @Select("select * from pms_product_category where id = #{id}")
    PmsProductCategoryData selectProductCategoryDetail(@Param("id") Long id);
    
    @Update("update pms_product set like_num = like_num + 1 where id = #{id}")
    int addProductLikeNum(@Param("id") Long id);

    void updateSale(@Param("productId") Long productId, @Param("productQuantity") Integer productQuantity);
}
