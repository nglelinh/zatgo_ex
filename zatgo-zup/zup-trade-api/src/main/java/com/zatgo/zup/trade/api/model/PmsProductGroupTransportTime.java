package com.zatgo.zup.trade.api.model;

import java.io.Serializable;
import java.util.Date;

public class PmsProductGroupTransportTime implements Serializable {
    /**
     * 交通时间编号
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 交通编号
     *
     * @mbggenerated
     */
    private Long transportId;

    /**
     * 出发时间
     *
     * @mbggenerated
     */
    private Date departureTime;

    /**
     * 抵达时间
     *
     * @mbggenerated
     */
    private Date destinationTime;

    /**
     * 展示状态（0 - 不展示，1 - 展示）
     *
     * @mbggenerated
     */
    private Byte showStatus;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTransportId() {
        return transportId;
    }

    public void setTransportId(Long transportId) {
        this.transportId = transportId;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getDestinationTime() {
        return destinationTime;
    }

    public void setDestinationTime(Date destinationTime) {
        this.destinationTime = destinationTime;
    }

    public Byte getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(Byte showStatus) {
        this.showStatus = showStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", transportId=").append(transportId);
        sb.append(", departureTime=").append(departureTime);
        sb.append(", destinationTime=").append(destinationTime);
        sb.append(", showStatus=").append(showStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}