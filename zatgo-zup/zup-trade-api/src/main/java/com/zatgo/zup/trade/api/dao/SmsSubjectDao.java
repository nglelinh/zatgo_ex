package com.zatgo.zup.trade.api.dao;

import com.zatgo.zup.trade.api.domain.SmsSubjectData;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SmsSubjectDao {
	
	@Select("SELECT s.id, s.title, s.pic, s.album_pics FROM sms_home_recommend_subject AS rs "
			+ "INNER JOIN cms_subject AS s ON rs.subject_id = s.id "
			+ "WHERE rs.recommend_status = 1 AND s.business_type=#{businessType} AND s.show_status = 1 ORDER BY rs.sort")
	List<SmsSubjectData> selectHomeSubjectListByBusinessType(@Param("businessType")Byte businessType);

	@Select("SELECT s.id, s.title, s.pic, s.album_pics FROM sms_home_recommend_subject AS rs "
			+ "INNER JOIN cms_subject AS s ON rs.subject_id = s.id "
			+ "WHERE rs.recommend_status = 1 AND s.show_status = 1 ORDER BY rs.sort")
	List<SmsSubjectData> selectHomeSubjectList();
	
	@Select("SELECT id, title, pic, album_pics, description, content FROM cms_subject WHERE id = #{id} AND show_status = 1")
	SmsSubjectData selectSubjectDetails(@Param("id") Long id);

}
