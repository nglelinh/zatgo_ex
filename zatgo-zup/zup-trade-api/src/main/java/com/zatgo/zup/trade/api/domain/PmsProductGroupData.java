package com.zatgo.zup.trade.api.domain;

import com.ykb.mall.common.model.PmsProductGroup;
import com.ykb.mall.common.model.PmsProductGroupPrice;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



import java.util.Date;
import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/4/4 10:46
 */
@ApiModel("班期信息")
public class PmsProductGroupData extends PmsProductGroup {

    @ApiModelProperty("出行时间")
    private Date date;

    @ApiModelProperty("班期价格")
    private List<PmsProductGroupPrice> prices;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<PmsProductGroupPrice> getPrices() {
        return prices;
    }

    public void setPrices(List<PmsProductGroupPrice> prices) {
        this.prices = prices;
    }
}