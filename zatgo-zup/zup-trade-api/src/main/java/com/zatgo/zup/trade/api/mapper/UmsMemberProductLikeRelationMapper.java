package com.zatgo.zup.trade.api.mapper;

import com.ykb.mall.common.model.UmsMemberProductLikeRelation;
import com.ykb.mall.common.model.UmsMemberProductLikeRelationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsMemberProductLikeRelationMapper {
    int countByExample(UmsMemberProductLikeRelationExample example);

    int deleteByExample(UmsMemberProductLikeRelationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsMemberProductLikeRelation record);

    int insertSelective(UmsMemberProductLikeRelation record);

    List<UmsMemberProductLikeRelation> selectByExample(UmsMemberProductLikeRelationExample example);

    UmsMemberProductLikeRelation selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsMemberProductLikeRelation record, @Param("example") UmsMemberProductLikeRelationExample example);

    int updateByExample(@Param("record") UmsMemberProductLikeRelation record, @Param("example") UmsMemberProductLikeRelationExample example);

    int updateByPrimaryKeySelective(UmsMemberProductLikeRelation record);

    int updateByPrimaryKey(UmsMemberProductLikeRelation record);
}