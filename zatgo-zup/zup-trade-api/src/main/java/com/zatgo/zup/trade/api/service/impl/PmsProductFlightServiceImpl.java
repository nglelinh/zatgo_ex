package com.zatgo.zup.trade.api.service.impl;

import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.ykb.mall.common.utils.DateTimeUtils;
import com.ykb.mall.common.model.PmsProductFlightPrice;
import com.ykb.mall.common.model.PmsProductFlightPriceExample;
import com.zatgo.zup.trade.api.dao.PmsProductFlightDao;
import com.zatgo.zup.trade.api.dao.PmsProductFlightPriceDao;
import com.zatgo.zup.trade.api.domain.PmsProductFlightResult;
import com.zatgo.zup.trade.api.mapper.PmsProductFlightPriceMapper;
import com.zatgo.zup.trade.api.service.PmsProductFlightService;
import com.zatgo.zup.trade.api.service.PmsProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class PmsProductFlightServiceImpl implements PmsProductFlightService {

	@Autowired
	private PmsProductFlightDao pmsProductFlightDao;

	@Autowired
	private PmsProductFlightPriceMapper pmsProductFlightPriceMapper;
	
	@Autowired
	private PmsProductFlightPriceDao pmsProductFlightPriceDao;
	
	@Autowired
	private PmsProductService pmsProductService;

	@Override
	public PmsProductFlightResult selectProductFlightDetail(Long productId) {
		pmsProductService.addProductClickNum(productId);
		PmsProductFlightResult pmsProductFlightResult = pmsProductFlightDao.selectProductFlightDetail(productId);
		return pmsProductFlightResult;
	}

	@Override
	public List<PmsProductFlightResult> selectProductFlightDetail(List<Long> ids) {
		List<PmsProductFlightResult> pmsProductFlightResults = null;
		if (!CollectionUtils.isEmpty(ids)){
			StringBuffer sb = new StringBuffer();
			for (Long id : ids) {
				sb.append(",");
				sb.append(id);
			}
			pmsProductFlightResults = pmsProductFlightDao.selectProductFlightDetailByIds(sb.toString().replaceFirst(",", ""));
		}
		return pmsProductFlightResults;
	}

	@Override
	public List<PmsProductFlightPrice> selectProductFlightPriceList(Long flightId, String startDate, String endDate) {
		PmsProductFlightPriceExample example = new PmsProductFlightPriceExample();
		PmsProductFlightPriceExample.Criteria criteria = example.createCriteria();
		criteria.andFlightIdEqualTo(flightId).andShowStatusEqualTo(YesOrNo.YES.getCode());
		if (StringUtils.isNotEmpty(startDate)) {
			criteria.andFlightDateGreaterThanOrEqualTo(DateTimeUtils
					.startOfTodDay(DateTimeUtils.parseStringToDate(startDate, DateTimeUtils.PATTEN_YYYY_MM_DD)));
		}
		if (StringUtils.isNotEmpty(endDate)) {
			criteria.andFlightDateLessThanOrEqualTo(DateTimeUtils
					.endOfTodDay(DateTimeUtils.parseStringToDate(endDate, DateTimeUtils.PATTEN_YYYY_MM_DD)));
		}
		return pmsProductFlightPriceMapper.selectByExample(example);
	}

	@Override
	public List<PmsProductFlightPrice> selectProductFlightMinList(Long flightId, String startDate, String endDate) {
		return pmsProductFlightPriceDao.selectProductFlightMinList(flightId, startDate, endDate);
	}

}
