package com.zatgo.zup.trade.api.model;

import java.io.Serializable;
import java.util.Date;

public class MessageRecord implements Serializable {
    /**
     * 消息记录编号
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 消息模板编号
     *
     * @mbggenerated
     */
    private Long templateId;

    /**
     * 接收人类型，1-后台管理员；2-用户
     *
     * @mbggenerated
     */
    private Byte receiverType;

    /**
     * 接收账号，手机号、邮箱等
     *
     * @mbggenerated
     */
    private String receiveNumber;

    /**
     * 短信模板名称
     *
     * @mbggenerated
     */
    private String templateName;

    /**
     * 短信内容
     *
     * @mbggenerated
     */
    private String messageContent;

    /**
     * 发送时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 发送结果，0-等待发送；1-已发送；2-发送失败
     *
     * @mbggenerated
     */
    private Byte result;

    /**
     * 发送结果内容，记录返回的结果内容信息
     *
     * @mbggenerated
     */
    private String resultContent;

    /**
     * 模板类型，1-邮件模板；2-站内消息模板；3-短信模板；4-APP推送
     *
     * @mbggenerated
     */
    private Byte messageType;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public Byte getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(Byte receiverType) {
        this.receiverType = receiverType;
    }

    public String getReceiveNumber() {
        return receiveNumber;
    }

    public void setReceiveNumber(String receiveNumber) {
        this.receiveNumber = receiveNumber;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Byte getResult() {
        return result;
    }

    public void setResult(Byte result) {
        this.result = result;
    }

    public String getResultContent() {
        return resultContent;
    }

    public void setResultContent(String resultContent) {
        this.resultContent = resultContent;
    }

    public Byte getMessageType() {
        return messageType;
    }

    public void setMessageType(Byte messageType) {
        this.messageType = messageType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", templateId=").append(templateId);
        sb.append(", receiverType=").append(receiverType);
        sb.append(", receiveNumber=").append(receiveNumber);
        sb.append(", templateName=").append(templateName);
        sb.append(", messageContent=").append(messageContent);
        sb.append(", createTime=").append(createTime);
        sb.append(", result=").append(result);
        sb.append(", resultContent=").append(resultContent);
        sb.append(", messageType=").append(messageType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}