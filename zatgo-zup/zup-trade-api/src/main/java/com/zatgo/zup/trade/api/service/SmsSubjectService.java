package com.zatgo.zup.trade.api.service;


import com.ykb.mall.common.model.CmsSubject;
import com.zatgo.zup.trade.api.domain.SmsSubjectData;

import java.util.List;

public interface SmsSubjectService {
	
	/**
	 * 首页推荐专题
	 * @return
	 */
	List<SmsSubjectData> homeSubjectList(Byte businessType);
	
	/**
	 * 专题信息
	 * @param id
	 * @return
	 */
	SmsSubjectData subjectDetails(Long id);
	
	/**
	 * 专题列表
	 * @return
	 */
	List<CmsSubject> subjectList(Byte businessType);

}
