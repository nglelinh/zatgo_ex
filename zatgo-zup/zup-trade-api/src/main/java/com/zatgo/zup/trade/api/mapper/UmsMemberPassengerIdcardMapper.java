package com.zatgo.zup.trade.api.mapper;

import com.zatgo.zup.trade.api.model.UmsMemberPassengerIdcard;
import com.zatgo.zup.trade.api.model.UmsMemberPassengerIdcardExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsMemberPassengerIdcardMapper {
    int countByExample(UmsMemberPassengerIdcardExample example);

    int deleteByExample(UmsMemberPassengerIdcardExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsMemberPassengerIdcard record);

    int insertSelective(UmsMemberPassengerIdcard record);

    List<UmsMemberPassengerIdcard> selectByExample(UmsMemberPassengerIdcardExample example);

    UmsMemberPassengerIdcard selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsMemberPassengerIdcard record, @Param("example") UmsMemberPassengerIdcardExample example);

    int updateByExample(@Param("record") UmsMemberPassengerIdcard record, @Param("example") UmsMemberPassengerIdcardExample example);

    int updateByPrimaryKeySelective(UmsMemberPassengerIdcard record);

    int updateByPrimaryKey(UmsMemberPassengerIdcard record);
}