package com.zatgo.zup.trade.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



import java.math.BigDecimal;
import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/27 16:04
 */
@ApiModel("个人订单详情")
public class PortalOrderDetailInfo extends PortalOrderBaseInfo {
    @ApiModelProperty("订单售价")
    private BigDecimal totalPrice;

    @ApiModelProperty("期望出行时间，json格式")
    private String desirableTimeStr;

    @ApiModelProperty("期望出行时间")
    private List<String> desirableDates;

    @ApiModelProperty("成人人数")
    private Integer adultNum;

    @ApiModelProperty("儿童人数")
    private Integer childNum;

    @ApiModelProperty("联系人名称")
    private String contactName;

    @ApiModelProperty("联系人手机号")
    private String contactPhone;

    @ApiModelProperty("成人价")
    private BigDecimal productPrice;

    @ApiModelProperty("儿童价")
    private BigDecimal productChildPrice;
    
    @ApiModelProperty("类型")
    private String productBrand;
    
    @ApiModelProperty("价格")
    private String prices;

    @ApiModelProperty("订单信息")
    private String orderInfo;

    @ApiModelProperty("联系人信息")
    private String passengers;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("副标题")
    private String subTitle;

    @ApiModelProperty("行李额说明")
    private String baggageAllowance;

    @ApiModelProperty("产品Id")
    private Long productId;

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getDesirableTimeStr() {
        return desirableTimeStr;
    }

    public void setDesirableTimeStr(String desirableTimeStr) {
        this.desirableTimeStr = desirableTimeStr;
    }

    public List<String> getDesirableDates() {
        return desirableDates;
    }

    public void setDesirableDates(List<String> desirableDates) {
        this.desirableDates = desirableDates;
    }

    public Integer getAdultNum() {
        return adultNum;
    }

    public void setAdultNum(Integer adultNum) {
        this.adultNum = adultNum;
    }

    public Integer getChildNum() {
        return childNum;
    }

    public void setChildNum(Integer childNum) {
        this.childNum = childNum;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public BigDecimal getProductChildPrice() {
        return productChildPrice;
    }

    public void setProductChildPrice(BigDecimal productChildPrice) {
        this.productChildPrice = productChildPrice;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }

    @Override
    public String getOrderInfo() {
        return orderInfo;
    }

    @Override
    public void setOrderInfo(String orderInfo) {
        this.orderInfo = orderInfo;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getBaggageAllowance() {
        return baggageAllowance;
    }

    public void setBaggageAllowance(String baggageAllowance) {
        this.baggageAllowance = baggageAllowance;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}