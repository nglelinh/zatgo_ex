package com.zatgo.zup.trade.api.domain;

import com.ykb.mall.common.model.ProductAttributeValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("航班详情")
public class PmsProductFlightResult extends PmsProductFlightParams {
	
	@ApiModelProperty("产品属性详情")
    private List<ProductAttributeValue> attrValueList;

	public List<ProductAttributeValue> getAttrValueList() {
		return attrValueList;
	}

	public void setAttrValueList(List<ProductAttributeValue> attrValueList) {
		this.attrValueList = attrValueList;
	}

}
