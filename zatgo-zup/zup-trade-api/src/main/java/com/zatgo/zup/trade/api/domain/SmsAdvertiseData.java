package com.zatgo.zup.trade.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("广告")
public class SmsAdvertiseData {
	
	@ApiModelProperty("")
	private Long id;
	
	@ApiModelProperty("名称")
	private String name;
	
	@ApiModelProperty("图片")
	private String pic;
	
	@ApiModelProperty("链接")
	private String url;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
