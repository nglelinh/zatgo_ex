//package com.zatgo.zup.trade.api.service.impl;
//
//import com.alibaba.fastjson.JSON;
//import com.zatgo.zup.common.exception.BusinessException;
//import com.ykb.mall.common.exception.BusinessExceptionCode;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import java.io.UnsupportedEncodingException;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
///**
// * @Author wangyucong
// * @Date 2019/2/19 16:52
// */
//@Service
//public class OkHttpService {
//
//    private static final Logger logger = LoggerFactory.getLogger(OkHttpService.class);
//
//    @Autowired
//    private RestTemplate restTemplate;
//
////    public Object doGet(){
////
////        String uri="http://wthrcdn.etouch.cn/weather_mini?city=";
////        HttpHeaders headers = new HttpHeaders();
////        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
////        HttpEntity<String> entity = new HttpEntity<String>(headers);
////        UmsMember member = new UmsMember();
////        String requestBody = JSONObject.toJSONString(member);
////        ResponseEntity<String> strbody=restTemplate.exchange(uri, HttpMethod.GET, entity,String.class);
//////        Object weatherResponse= JSONObject.parseObject(strbody,Object.class);
////        return strbody;
////    }
//
//    /**
//     * GET请求，带路径，获取JSON对象
//     * @param path
//     * @return
//     */
//    public String doGet(String path) {
//        logger.info("get请求体：{}", path);
//        ResponseEntity<String> response = null;
//        try {
//            response = restTemplate.getForEntity(path, String.class);
//            logger.info("get请求响应体：{}", JSON.toJSONString(response));
//        } catch (Exception e) {
//            logger.error("get请求失败", e);
//            throw new BusinessException(BusinessExceptionCode.HTTP_FAIL_CODE, "服务调用失败");
//        }
//        return response.getBody();
//    }
//
//    /**
//     * GET请求，带参数，返回JAVA对象
//     * @param path
//     * @param clazz
//     * @param pathParams
//     * @return
//     */
//    public <T> T get(String path, Map<String, String> params, Class<T> clazz, String... pathParams) {
//        path = getUrl(path, params, pathParams);
//        return JSON.parseObject(doGet(path), clazz);
//    }
//
//    /**
//     * GET请求，不带参数，返回JAVA对象
//     * @param path
//     * @param clazz
//     * @param pathParams
//     * @return
//     */
//    public <T> T get(String path, Class<T> clazz, String... pathParams) {
//        return JSON.parseObject(doGet(createUrl(path, pathParams)), clazz);
//    }
//
//    /**
//     * POST请求，传对象
//     * @param path
//     * @param paramObj
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    public <T> T post(String path, Object paramObj, Class<T> clazz) {
//        ResponseEntity<String> response = restTemplate.postForEntity(path, paramObj, String.class);
//        T responseBody = JSON.parseObject(response.getBody(), clazz);
//        return responseBody;
//    }
//
//    /**
//     * 微信GET请求
//     * @param path
//     * @param clazz
//     * @param pathParams
//     * @return
//     */
//    public <T> T wxGet(String path, Map<String, String> params, Class<T> clazz, String... pathParams){
//        path = getUrl(path, params, pathParams);
//        return JSON.parseObject(isoToUtf(doGet(path)), clazz);
//    }
//
//    public <T> T wxGetUtf8(String path, HashMap<String, String> params, Class<T> clazz, String... pathParams){
//        path = getUrl(path, params, pathParams);
//        String res = doGet(path);
//        return JSON.parseObject(res, clazz);
//    }
//
//    /**
//     * 封装get请求url
//     *
//     * @param path
//     * @param params
//     * @param pathParams
//     * @return
//     */
//    private String getUrl(String path, Map<String, String> params, String... pathParams) {
//        path = createUrl(path, pathParams);
//        StringBuffer sb = new StringBuffer(path);
//        if (params != null) {
//            Iterator<Map.Entry<String, String>> iter = params.entrySet().iterator();
//            int index = 1;
//            while (iter.hasNext()) {
//                Map.Entry<String, String> entry = iter.next();
//                String key = entry.getKey();
//                String val = entry.getValue();
//                if (index == 1) {
//                    //第一个
//                    sb.append("?");
//                }
//                sb.append(key).append("=").append(val).append("&");
//                index++;
//            }
//            if (index > 1) {
//                //至少有一个参数，则去掉最后的&符`号
//                path = sb.substring(0, sb.length() - 1);
//            }
//        }
//        return path;
//    }
//
//    private String createUrl(String path, String... pathParams) {
//        StringBuffer p = new StringBuffer(getServiceUrl(path));
//        if (pathParams != null) {
//            for (String param : pathParams) {
//                p.append("/").append(param);
//            }
//        }
//        return p.toString();
//    }
//
//    private String getServiceUrl(String path) {
//        if (path.endsWith("/")) {
//            path = path.substring(0, path.length() - 1);
//        }
//        return path;
//    }
//
//    private String isoToUtf(String data) {
//        try {
//            data = new String(data.getBytes("ISO-8859-1"), "UTF-8");
//            logger.info("数据编码 ISO-8859-1 转 UTF-8后，data:{}", data);
//            return data;
//        } catch (UnsupportedEncodingException e) {
//            throw new BusinessException(BusinessExceptionCode.HTTP_FAIL_CODE, "数据编码 ISO-8859-1 转 UTF-8 时发生错误");
//        }
//    }
//}