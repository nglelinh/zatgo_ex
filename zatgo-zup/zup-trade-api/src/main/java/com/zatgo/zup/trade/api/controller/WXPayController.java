package com.zatgo.zup.trade.api.controller;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.remoteService.WechatRemoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Api(tags = "WXPayController", description = "微信支付")
@RequestMapping("/trade/api")
public class WXPayController extends BaseController{

	@Autowired
	private WechatRemoteService wechatRemoteService;

	@ApiOperation("获取订单微信支付参数")
	@GetMapping(value = "/params/{orderId}/{wxPayType}")
	@ResponseBody
	public CommonResult getOrderPayWeixinParams(@PathVariable("orderId") Long orderId, @PathVariable("wxPayType") Integer wxPayType) {
		AuthUserInfo userInfo = getUserInfo();
		return wechatRemoteService.getOrderPayWeixinParams(orderId, wxPayType, userInfo.getWechatOpenId(), userInfo.getUserId());
	}

}
