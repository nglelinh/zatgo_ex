package com.zatgo.zup.trade.api.mapper;

import com.ykb.mall.common.model.PmsProductFlight;
import com.ykb.mall.common.model.PmsProductFlightExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductFlightMapper {
    int countByExample(PmsProductFlightExample example);

    int deleteByExample(PmsProductFlightExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductFlight record);

    int insertSelective(PmsProductFlight record);

    List<PmsProductFlight> selectByExample(PmsProductFlightExample example);

    PmsProductFlight selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductFlight record, @Param("example") PmsProductFlightExample example);

    int updateByExample(@Param("record") PmsProductFlight record, @Param("example") PmsProductFlightExample example);

    int updateByPrimaryKeySelective(PmsProductFlight record);

    int updateByPrimaryKey(PmsProductFlight record);
}