package com.zatgo.zup.trade.api.mq.aliyunmq;//package com.ykb.mall.mapi.mq.aliyunmq;
//
//import com.aliyun.openservices.ons.api.Consumer;
//import com.ykb.mall.mapi.mq.MQConsumerCallBack;
//import com.ykb.mall.mapi.mq.aliyunmq.listener.NoticeListener;
//import com.ykb.mall.mapi.mq.aliyunmq.tool.AliyunMQClient;
//import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class AliYunMQConsumer {
//	private static final Logger logger = LoggerFactory.getLogger(AliYunMQConsumer.class);
//
//	@Autowired
//	private AliyunMQClient aliyunMQClient;
//
//
//	public void startConsumer(String consumerGroup,String topic,String tags,MQConsumerCallBack callBack,MessageModel model){
//		Consumer zupNoticeConsumer = null;
//		String consumerId = "CID_" + topic;
//		if(model.equals(MessageModel.BROADCASTING)){
//			zupNoticeConsumer = aliyunMQClient.getBroadCastConsumer(consumerId);
//		} else {
//			zupNoticeConsumer = aliyunMQClient.getClusteringConsumer(consumerId);
//		}
//		NoticeListener zupNoticeListener = new NoticeListener(callBack);
//		zupNoticeConsumer.subscribe(topic,tags, zupNoticeListener);
//		logger.info("zup 提醒消息消费者已启动 " + consumerId);
//	}
//}
