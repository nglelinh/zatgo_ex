package com.zatgo.zup.trade.api.service.impl;

import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.ykb.mall.common.utils.DateTimeUtils;
import com.ykb.mall.common.model.PmsProductHotel;
import com.ykb.mall.common.model.PmsProductHotelExample;
import com.zatgo.zup.trade.api.dao.PmsProductHotelDao;
import com.zatgo.zup.trade.api.domain.PmsProductHotelData;
import com.zatgo.zup.trade.api.mapper.PmsProductHotelMapper;
import com.zatgo.zup.trade.api.service.PmsProductHotelService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PmsProductHotelServiceImpl implements PmsProductHotelService {

	@Autowired
	private PmsProductHotelMapper pmsProductHotelMapper;
	
	@Autowired
	private PmsProductHotelDao pmsProductHotelDao;

	public List<PmsProductHotel> selectProductHotelList(Long productId, String startDate, String endDate) {

		PmsProductHotelExample example = new PmsProductHotelExample();
		PmsProductHotelExample.Criteria criteria = example.createCriteria();
		criteria.andProductIdEqualTo(productId).andStatusEqualTo(YesOrNo.YES.getCode())
				.andDeleteStatusEqualTo(YesOrNo.NO.getCode());
		if (StringUtils.isNotEmpty(startDate)) {
			criteria.andArrivalDateGreaterThanOrEqualTo(DateTimeUtils
					.startOfTodDay(DateTimeUtils.parseStringToDate(startDate, DateTimeUtils.PATTEN_YYYY_MM_DD)));
		}

		if (StringUtils.isNotEmpty(endDate)) {
			criteria.andArrivalDateLessThan(DateTimeUtils
					.startOfTodDay(DateTimeUtils.parseStringToDate(endDate, DateTimeUtils.PATTEN_YYYY_MM_DD)));
		}

		return pmsProductHotelMapper.selectByExample(example);
	}

	@Override
	public List<PmsProductHotelData> selectProductHotelMinList(Long productId, String startDate, String endDate) {
		return pmsProductHotelDao.selectProductHotelMinList(productId, startDate, endDate);
	}

}
