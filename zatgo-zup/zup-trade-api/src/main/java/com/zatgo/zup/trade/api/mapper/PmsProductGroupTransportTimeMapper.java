package com.zatgo.zup.trade.api.mapper;

import com.ykb.mall.common.model.PmsProductGroupTransportTime;
import com.ykb.mall.common.model.PmsProductGroupTransportTimeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductGroupTransportTimeMapper {
    int countByExample(PmsProductGroupTransportTimeExample example);

    int deleteByExample(PmsProductGroupTransportTimeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductGroupTransportTime record);

    int insertSelective(PmsProductGroupTransportTime record);

    List<PmsProductGroupTransportTime> selectByExample(PmsProductGroupTransportTimeExample example);

    PmsProductGroupTransportTime selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductGroupTransportTime record, @Param("example") PmsProductGroupTransportTimeExample example);

    int updateByExample(@Param("record") PmsProductGroupTransportTime record, @Param("example") PmsProductGroupTransportTimeExample example);

    int updateByPrimaryKeySelective(PmsProductGroupTransportTime record);

    int updateByPrimaryKey(PmsProductGroupTransportTime record);
}