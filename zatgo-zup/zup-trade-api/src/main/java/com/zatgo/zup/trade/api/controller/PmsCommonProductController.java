package com.zatgo.zup.trade.api.controller;

import com.ykb.mall.common.model.PmsBrand;
import com.zatgo.zup.common.model.CommonProductInfoResponse;
import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.model.TicketDateList;
import com.zatgo.zup.trade.api.model.TicketListResponse;
import com.zatgo.zup.trade.api.service.PmsCommonProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by 46041 on 2019/7/16.
 */
@RestController
@Api(tags = "PmsCommonProductController", description = "普通商品")
@RequestMapping("/trade/api/pmsCommonProduct")
public class PmsCommonProductController {

    @Autowired
    private PmsCommonProductService pmsCommonProductService;

    @ApiOperation(value = "商品详情")
    @RequestMapping(value = "/info/{id}",method = RequestMethod.GET)
    public Object getProductInfo(@PathVariable("id") Long id){
        CommonProductInfoResponse res = pmsCommonProductService.getProductInfo(id);
        return new CommonResult().success(res);
    }

    @ApiOperation(value = "门票班期")
    @RequestMapping(value = "/ticket/list/{productId}",method = RequestMethod.GET)
    public Object ticketList(@PathVariable("productId") Long productId){
        TicketDateList ticketDateList = pmsCommonProductService.ticketList(productId);
        return new CommonResult().success(ticketDateList);
    }
}
