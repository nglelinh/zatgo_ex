package com.zatgo.zup.trade.api.dao;

import com.github.pagehelper.Page;
import com.ykb.mall.common.model.OmsOrderItem;
import com.zatgo.zup.trade.api.domain.OmsOrderDetail;
import com.zatgo.zup.trade.api.domain.OrderListParam;
import com.zatgo.zup.trade.api.domain.PortalOrderBaseInfo;
import com.zatgo.zup.trade.api.domain.PortalOrderDetailInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 前台订单自定义Dao
 * Created by chen on 2018/9/4.
 */
public interface PortalOrderDao {

    /**
     * 会员个人订单列表
     * @param param
     * @return
     */
    List<PortalOrderBaseInfo> selectOrderList(OrderListParam param);
    
    Page<PortalOrderBaseInfo> selectOrderPage(OrderListParam param);

    /**
     * 会员个人订单详情
     * @param id
     * @return
     */
    PortalOrderDetailInfo selectOrderDetail(@Param("id") Long id, @Param("memberId") String memberId);

    /**
     * 获取订单及下单商品详情
     */
    OmsOrderDetail getDetail(@Param("orderId") Long orderId);

    /**
     * 修改 pms_sku_stock表的锁定库存及真实库存
     */
    int updateSkuStock(@Param("itemList") List<OmsOrderItem> orderItemList);

    /**
     * 获取超时订单
     * @param minute 超时时间（分）
     */
    List<OmsOrderDetail> getTimeOutOrders(@Param("minute") Integer minute);

    /**
     * 批量修改订单状态
     */
    int updateOrderStatus(@Param("ids") List<Long> ids, @Param("status") Integer status);

    /**
     * 解除取消订单的库存锁定
     */
    int releaseSkuStockLock(@Param("itemList") List<OmsOrderItem> orderItemList);
    
    /**
     * 根据订单号查询订单
     * @param orderSn
     * @return
     */
    PortalOrderBaseInfo selectOrderByOrderSn(@Param("orderSn") String orderSn);

}
