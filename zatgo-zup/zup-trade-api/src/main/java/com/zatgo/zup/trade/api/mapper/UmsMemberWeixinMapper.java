package com.zatgo.zup.trade.api.mapper;

import com.ykb.mall.common.model.UmsMemberWeixin;
import com.ykb.mall.common.model.UmsMemberWeixinExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsMemberWeixinMapper {
    int countByExample(UmsMemberWeixinExample example);

    int deleteByExample(UmsMemberWeixinExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsMemberWeixin record);

    int insertSelective(UmsMemberWeixin record);

    List<UmsMemberWeixin> selectByExample(UmsMemberWeixinExample example);

    UmsMemberWeixin selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsMemberWeixin record, @Param("example") UmsMemberWeixinExample example);

    int updateByExample(@Param("record") UmsMemberWeixin record, @Param("example") UmsMemberWeixinExample example);

    int updateByPrimaryKeySelective(UmsMemberWeixin record);

    int updateByPrimaryKey(UmsMemberWeixin record);
}