package com.zatgo.zup.trade.api.service;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.trade.api.domain.EsProduct;
import com.zatgo.zup.trade.api.domain.EsProductRelatedInfo;
import com.zatgo.zup.trade.api.domain.PmsProductFlightResult;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 商品搜索管理Service
 * Created by chen on 2018/6/19.
 */
public interface EsProductService {
    /**
     * 从数据库中导入所有商品到ES
     */
    int importAll();

    /**
     * 根据id删除商品
     */
    void delete(Long id);

    /**
     * 根据id创建商品
     */
    EsProduct create(Long id);

    /**
     * 批量删除商品
     */
    void delete(List<Long> ids);

    /**
     * 根据关键字搜索名称或者副标题
     */
    Page<EsProduct> search(String keyword, Integer pageNum, Integer pageSize);

    /**
     * 根据关键字搜索名称或者副标题复合查询
     */
    Page<EsProduct> search(String keyword, Long brandId, Long productCategoryId, Integer pageNum, Integer pageSize, Integer sort,
                           Byte businessType, String moduleId, Long subjectId, Integer discountStatus, Integer recommandStatus, String tag);

    /**
     * 根据商品id推荐相关商品
     */
    Page<EsProduct> recommend(Long id, Integer pageNum, Integer pageSize);

    /**
     * 获取搜索词相关品牌、分类、属性
     */
    EsProductRelatedInfo searchRelatedInfo(String keyword);
    
    /**
     * 关联商品
     * @param productId
     * @return
     */
    Page<EsProduct> searchRelatedProductId(Long productId, Byte relationType);


    JSONObject flightSearch(String keyword, Long brandId, Long productCategoryId, Integer pageNum, Integer pageSize, Integer sort, Byte aByte, String moduleId, Long subjectId, Integer discountStatus, Integer recommandStatus, String tag);
}
