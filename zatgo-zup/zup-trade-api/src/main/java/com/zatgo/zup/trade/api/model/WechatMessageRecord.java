package com.zatgo.zup.trade.api.model;

import java.io.Serializable;
import java.util.Date;

public class WechatMessageRecord implements Serializable {
    private Long id;

    /**
     * 模板编号
     *
     * @mbggenerated
     */
    private Integer templateId;

    /**
     * 消息内容
     *
     * @mbggenerated
     */
    private String msg;

    /**
     * 微信消息id
     *
     * @mbggenerated
     */
    private String msgId;

    private String openId;

    /**
     * 消息状态：0-已发送（微信服务器成功接收消息）；1-成功（微信服务器成功下发消息给用户）；2-失败（微信服务器下发消息失败）
     *
     * @mbggenerated
     */
    private Byte status;

    /**
     * 微信状态消息
     *
     * @mbggenerated
     */
    private String statusMsg;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", templateId=").append(templateId);
        sb.append(", msg=").append(msg);
        sb.append(", msgId=").append(msgId);
        sb.append(", openId=").append(openId);
        sb.append(", status=").append(status);
        sb.append(", statusMsg=").append(statusMsg);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}