package com.zatgo.zup.trade.api.service;


import com.ykb.mall.common.model.MessageTemplate;

import java.util.Map;

/**
 * @Author wangyucong
 * @Date 2019/3/26 10:10
 */
public interface MessageService {

    /**
     * 新增消息记录
     * @param template
     * @param receiveNumber
     * @param content
     * @param result
     * @param resultContent
     */
    void createMessageRecord(MessageTemplate template, String receiveNumber, String content, Byte result, String resultContent);

    /**
     * 拼装消息内容
     * @param templateContent
     * @param contentMap
     * @return
     */
    String getMessageContent(String templateContent, Map<String, String> contentMap);
}
