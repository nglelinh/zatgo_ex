//package com.zatgo.zup.trade.api.service;
//
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Map;
//
///**
// * 邮箱管理
// * @Author wangyucong
// * @Date 2019/3/25 16:51
// */
//public interface MailService {
//
//    /**
//     * 发送邮件
//     * @param subject
//     * @param text
//     */
//    Byte sendHtmlMail(String receiveEmail, String subject, String text);
//
//    /**
//     * 发送邮件
//     * @param templateCode
//     * @param receiver
//     * @param contentMap
//     */
//    @Transactional
//    void sendEmail(String templateCode, String receiver, Map<String, String> contentMap);
//}
