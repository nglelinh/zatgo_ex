package com.zatgo.zup.trade.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
@EnableSwagger2
@EnableScheduling
@MapperScan({"com.zatgo.zup.trade.api.mapper", "com.zatgo.zup.trade.api.dao"})
@ComponentScan(basePackages="com.zatgo.zup")
@EnableTransactionManagement
@EnableDiscoveryClient
@Configuration
@SpringBootApplication
public class ZupTradeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZupTradeApiApplication.class, args);
	}

}
