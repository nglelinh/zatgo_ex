package com.zatgo.zup.trade.api.service;


import com.zatgo.zup.trade.api.domain.PmsProductGroupData;

import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/4/4 11:07
 */
public interface PmsProductGroupService {

    /**
     * 产品班期列表
     * @param productId
     * @return
     */
    List<PmsProductGroupData> selectProductGroupList(Long productId);
}
