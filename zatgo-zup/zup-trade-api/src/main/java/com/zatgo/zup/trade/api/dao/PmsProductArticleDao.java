package com.zatgo.zup.trade.api.dao;

import com.ykb.mall.common.model.PmsProductArticleDetail;
import org.apache.ibatis.annotations.Param;

public interface PmsProductArticleDao {

	/**
     * 查询文章详情
     * @param id
     * @return
     */
    PmsProductArticleDetail selectProductArticleDetail(@Param("id") Long id);
	
}
