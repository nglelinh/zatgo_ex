package com.zatgo.zup.trade.api.service;


import com.ykb.mall.common.model.PmsProductHotel;
import com.zatgo.zup.trade.api.domain.PmsProductHotelData;

import java.util.List;

public interface PmsProductHotelService {
	
	/**
	 *  查询酒店
	 * @param productId
	 */
	List<PmsProductHotel> selectProductHotelList(Long productId, String startDate, String endDate);
	
	/**
	 *  查询酒店低价列表
	 * @param productId
	 */
	List<PmsProductHotelData> selectProductHotelMinList(Long productId, String startDate, String endDate);

}
