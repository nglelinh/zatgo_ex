package com.zatgo.zup.trade.api.controller;

import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.service.SmsSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "SmsSubjectController", description = "专题信息")
@RequestMapping("/trade/api/smsSubject")
public class SmsSubjectController {
	
	@Autowired
	private SmsSubjectService smsSubjectService;
	
	@ApiOperation("首页专题")
	@GetMapping(value = "/home/list")
	public CommonResult homeSubjectList(@RequestParam(value = "businessType", required = false) Byte businessType ) {
		return new CommonResult().success(smsSubjectService.homeSubjectList(businessType));
	}
	
	@ApiOperation("专题信息")
	@GetMapping(value = "/details/{id}")
	public CommonResult subjectDetails(@PathVariable Long id) {
		return new CommonResult().success(smsSubjectService.subjectDetails(id));
	}
	
	@ApiOperation("专题列表")
	@GetMapping(value = "/list")
	public CommonResult subjectList(@RequestParam(value = "businessType", required = false) Byte businessType) {
		return new CommonResult().success(smsSubjectService.subjectList(businessType));
	}

}
