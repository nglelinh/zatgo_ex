package com.zatgo.zup.trade.api.model;

import java.io.Serializable;
import java.util.Date;

public class PmsProductGroupTransport implements Serializable {
    /**
     * 交通编号
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 班期编号
     *
     * @mbggenerated
     */
    private Long groupId;

    /**
     * 行程类型（1 - 接，2 - 送，3 - 转场）
     *
     * @mbggenerated
     */
    private Byte type;

    /**
     * 出发地
     *
     * @mbggenerated
     */
    private String departurePlace;

    /**
     * 目的地
     *
     * @mbggenerated
     */
    private String destinationPlace;

    /**
     * 展示状态（0 - 不展示，1 - 展示）
     *
     * @mbggenerated
     */
    private Byte showStatus;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getDeparturePlace() {
        return departurePlace;
    }

    public void setDeparturePlace(String departurePlace) {
        this.departurePlace = departurePlace;
    }

    public String getDestinationPlace() {
        return destinationPlace;
    }

    public void setDestinationPlace(String destinationPlace) {
        this.destinationPlace = destinationPlace;
    }

    public Byte getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(Byte showStatus) {
        this.showStatus = showStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", groupId=").append(groupId);
        sb.append(", type=").append(type);
        sb.append(", departurePlace=").append(departurePlace);
        sb.append(", destinationPlace=").append(destinationPlace);
        sb.append(", showStatus=").append(showStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}