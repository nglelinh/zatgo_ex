package com.zatgo.zup.trade.api.controller;

import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.service.PmsProductFlightService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Api(tags = "PmsProductFlightController", description = "机票产品信息")
@RequestMapping("/trade/api/pmsProduct/flight")
public class PmsProductFlightController {

	@Resource
	private PmsProductFlightService pmsroductFlightService;

	@ApiOperation("机票产品信息")
	@GetMapping("/detail/{productId}")
	public CommonResult productFlightDetail(@PathVariable Long productId) {

		return new CommonResult().success(pmsroductFlightService.selectProductFlightDetail(productId));
	}

	@ApiOperation(value = "获取产品班期列表")
	@GetMapping(value = "/price/list/{flightId}")
	public CommonResult selectProductFlightList(@PathVariable Long flightId,
                                                @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
		return new CommonResult()
				.success(pmsroductFlightService.selectProductFlightPriceList(flightId, startDate, endDate));
	}
	
	@ApiOperation(value = "获取机票低价列表")
	@GetMapping(value = "/price/min/list/{flightId}")
	public CommonResult selectProductFlightMinList(@PathVariable Long flightId,
                                                   @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
		return new CommonResult()
				.success(pmsroductFlightService.selectProductFlightMinList(flightId, startDate, endDate));
	}

}
