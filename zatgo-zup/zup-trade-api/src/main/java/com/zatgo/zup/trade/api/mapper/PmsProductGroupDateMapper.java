package com.zatgo.zup.trade.api.mapper;

import com.ykb.mall.common.model.PmsProductGroupDate;
import com.ykb.mall.common.model.PmsProductGroupDateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductGroupDateMapper {
    int countByExample(PmsProductGroupDateExample example);

    int deleteByExample(PmsProductGroupDateExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductGroupDate record);

    int insertSelective(PmsProductGroupDate record);

    List<PmsProductGroupDate> selectByExample(PmsProductGroupDateExample example);

    PmsProductGroupDate selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductGroupDate record, @Param("example") PmsProductGroupDateExample example);

    int updateByExample(@Param("record") PmsProductGroupDate record, @Param("example") PmsProductGroupDateExample example);

    int updateByPrimaryKeySelective(PmsProductGroupDate record);

    int updateByPrimaryKey(PmsProductGroupDate record);
}