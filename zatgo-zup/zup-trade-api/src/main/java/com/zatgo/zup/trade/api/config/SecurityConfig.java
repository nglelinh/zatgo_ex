//package com.zatgo.zup.trade.api.config;
//
//import com.ykb.mall.mapi.component.GoAccessDeniedHandler;
//import com.ykb.mall.mapi.component.GoAuthenticationEntryPoint;
//import com.ykb.mall.mapi.component.JwtAuthenticationTokenFilter;
//import com.ykb.mall.mapi.domain.MemberDetails;
//import com.ykb.mall.mapi.service.UmsMemberService;
//import com.ykb.mall.common.model.UmsMember;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//import org.springframework.web.filter.CorsFilter;
//
///**
// * SpringSecurity的配置
// * Created by chen on 2018/8/3.
// */
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled=true)
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//    @Autowired
//    private UmsMemberService memberService;
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // 开启允许iframe 嵌套
//        http.headers().frameOptions().disable();
//
//        http.csrf()
//                .disable()//开启basic认证登录后可以调用需要认证的接口
//                .cors() // 开启跨域
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, // 允许对于网站静态资源的无授权访问
//                        "/",
//                        "/*.html",
//                        "/favicon.ico",
//                        "/**/*.html",
//                        "/**/*.css",
//                        "/**/*.js",
//                        "/swagger-resources/**",
//                        "/v2/api-docs/**",
//                        "/sys/isAlive"
//                )
//                .permitAll()
//                .antMatchers(HttpMethod.OPTIONS)//跨域请求会先进行一次options请求
//                .permitAll()
//                .antMatchers("/sso/*")// 对登录注册要允许匿名访问
//                .permitAll()
//                // 非登录状态也可浏览产品内容
//                .antMatchers("/esProduct/search",
//                        "/wechat/jsapiConfig",
//                        "/pmsProduct/**",
//                        "/third/**")
//                .permitAll()
//                /**
//                 * 测试时全部运行访问
//                 */
//                .antMatchers("/**")
//                .permitAll()
//                .anyRequest()// 除上面外的所有请求全部需要鉴权认证
//                .authenticated();
////                .and()
////                .formLogin()
////                .loginPage("/sso/login")
////                .successHandler(new GoAuthenticationSuccessHandler())
////                .failureHandler(new GoAuthenticationFailureHandler())
////                .and()
////                .logout()
////                .logoutUrl("/sso/logout")
////                .logoutSuccessHandler(new GoLogoutSuccessHandler())
////                .invalidateHttpSession(true)
////                .deleteCookies("JSESSIONID")
////                .and()
////                .requiresChannel()
////                .antMatchers("/sso/*")
////                .requiresSecure()
////                .anyRequest()
////                .requiresInsecure()
////                .and()
////                .rememberMe()
////                .tokenValiditySeconds(1800)
////                .key("token_key")
//        // 禁用缓存
//        http.headers().cacheControl();
//        // 添加JWT filter
//        http.addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
//        http.exceptionHandling()
//                .accessDeniedHandler(new GoAccessDeniedHandler())
//                .authenticationEntryPoint(new GoAuthenticationEntryPoint());
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService())
//                .passwordEncoder(passwordEncoder());
//    }
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Bean
//    @Override
//    public AuthenticationManager authenticationManagerBean() throws Exception {
//        return super.authenticationManagerBean();
//    }
//
//    @Bean
//    public UserDetailsService userDetailsService() {
//        //获取登录用户信息
//        return new UserDetailsService() {
//            @Override
//            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//                UmsMember member = memberService.getByUsername(username);
//                if(member != null){
//                    return new MemberDetails(member);
//                }
//                throw new UsernameNotFoundException("用户名或密码错误");
//            }
//        };
//    }
//
//    /**
//     * 允许跨域调用的过滤器
//     */
//    @Bean
//    public CorsFilter corsFilter() {
//        CorsConfiguration config = new CorsConfiguration();
//        config.addAllowedOrigin("*");
//        config.setAllowCredentials(true);
//        config.addAllowedHeader("*");
//        config.addAllowedMethod("*");
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", config);
//        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
//        bean.setOrder(0);
//        return new CorsFilter(source);
//    }
//
//    @Bean
//    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter(){
//        return new JwtAuthenticationTokenFilter();
//    }
//}
