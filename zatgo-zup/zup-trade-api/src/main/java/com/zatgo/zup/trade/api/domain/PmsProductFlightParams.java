package com.zatgo.zup.trade.api.domain;

import com.ykb.mall.common.model.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

@ApiModel("机票信息")
public class PmsProductFlightParams extends PmsProduct {
	
	@ApiModelProperty("航班id")
	private Long flightId;
	
	@ApiModelProperty("产品编号")
	private Long productId;
	
	@ApiModelProperty("去程航班编号")
	private String tripFlightNumber;
	
	@ApiModelProperty("去程航空公司")
	private String tripAirlineCompany;
	
	@ApiModelProperty("返程航班编号")
	private String returnFlightNumber;
	
	@ApiModelProperty("返程航空公司")
	private String returnAirlineCompany;
	
	@ApiModelProperty("库存是否限量（0 - 否，1 - 是）")
	private Byte stockLimitp;
	
	@ApiModelProperty("库存")
	private Integer stock;
	
	@ApiModelProperty("去程出发城市名称")
	private String tripDepartureCityName;
	
	@ApiModelProperty("去程出发机场")
	private String tripDepartureAirportName;
	
	@ApiModelProperty("去程出发航站楼")
	private String tripDepartureTerminal;
	
	@ApiModelProperty("去程到达城市名称")
	private String tripDestinationCityName;
	
	@ApiModelProperty("去程到达机场名称")
	private String tripDestinationAirportName;
	
	@ApiModelProperty("去程到达航站楼")
	private String tripDestinationTerminal;
	
	@ApiModelProperty("去程机型")
	private String tripPlaneType;
	
	@ApiModelProperty("去程出发时间")
	private Date tripDepartureTime;
	
	@ApiModelProperty("去程到达时间")
	private Date tripDestinationTime;
	
	@ApiModelProperty("去程订单航程类型 1-单程；2-往返；3-联程")
	private Byte tripFlightType;
	
	@ApiModelProperty("去程是否中转：1-直飞；2-中转")
	private Byte tripIsTransfer;
	
	@ApiModelProperty("去程历时")
	private String tripCostTime;
	
	@ApiModelProperty("返程出发城市名称")
	private String returnDepartureCityName;
	
	@ApiModelProperty("返程出发机场")
	private String returnDepartureAirportName;
	
	@ApiModelProperty("返程出发航站楼")
	private String returnDepartureTerminal;
	
	@ApiModelProperty("返程到达城市名称")
	private String returnDestinationCityName;
	
	@ApiModelProperty("返程到达机场名称")
	private String returnDestinationAirportName;
	
	@ApiModelProperty("返程到达航站楼")
	private String returnDestinationTerminal;
	
	@ApiModelProperty("返程机型")
	private String returnPlaneType;
	
	@ApiModelProperty("返程出发时间")
	private Date returnDepartureTime;
	
	@ApiModelProperty("返程到达时间")
	private Date returnDestinationTime;
	
	@ApiModelProperty("返程订单航程类型 1-单程；2-往返；3-联程")
	private Byte returnFlightType;
	
	@ApiModelProperty("返程是否中转：1-直飞；2-中转")
	private Byte returnIsTransfer;
	
	@ApiModelProperty("返程历时")
	private String returnCostTime;
	
	@ApiModelProperty("商品阶梯价格设置")
    private List<PmsProductLadder> productLadderList;
    @ApiModelProperty("商品满减价格设置")
    private List<PmsProductFullReduction> productFullReductionList;
    @ApiModelProperty("商品会员价格设置")
    private List<PmsMemberPrice> memberPriceList;
    @ApiModelProperty("商品的sku库存信息")
    private List<PmsSkuStock> skuStockList;
    @ApiModelProperty("商品参数及自定义规格属性")
    private List<PmsProductAttributeValue> productAttributeValueList;
    @ApiModelProperty("专题和商品关系")
    private List<CmsSubjectProductRelation> subjectProductRelationList;
    @ApiModelProperty("优选专区和商品的关系")
    private List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList;
    @ApiModelProperty("商品和商品的关系")
    private List<PmsProductRelation> pmsProductRelationList;

	public Long getFlightId() {
		return flightId;
	}

	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getTripFlightNumber() {
		return tripFlightNumber;
	}

	public void setTripFlightNumber(String tripFlightNumber) {
		this.tripFlightNumber = tripFlightNumber;
	}

	public String getTripAirlineCompany() {
		return tripAirlineCompany;
	}

	public void setTripAirlineCompany(String tripAirlineCompany) {
		this.tripAirlineCompany = tripAirlineCompany;
	}

	public String getReturnFlightNumber() {
		return returnFlightNumber;
	}

	public void setReturnFlightNumber(String returnFlightNumber) {
		this.returnFlightNumber = returnFlightNumber;
	}

	public String getReturnAirlineCompany() {
		return returnAirlineCompany;
	}

	public void setReturnAirlineCompany(String returnAirlineCompany) {
		this.returnAirlineCompany = returnAirlineCompany;
	}

	public Byte getStockLimitp() {
		return stockLimitp;
	}

	public void setStockLimitp(Byte stockLimitp) {
		this.stockLimitp = stockLimitp;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getTripDepartureCityName() {
		return tripDepartureCityName;
	}

	public void setTripDepartureCityName(String tripDepartureCityName) {
		this.tripDepartureCityName = tripDepartureCityName;
	}

	public String getTripDepartureAirportName() {
		return tripDepartureAirportName;
	}

	public void setTripDepartureAirportName(String tripDepartureAirportName) {
		this.tripDepartureAirportName = tripDepartureAirportName;
	}

	public String getTripDepartureTerminal() {
		return tripDepartureTerminal;
	}

	public void setTripDepartureTerminal(String tripDepartureTerminal) {
		this.tripDepartureTerminal = tripDepartureTerminal;
	}

	public String getTripDestinationCityName() {
		return tripDestinationCityName;
	}

	public void setTripDestinationCityName(String tripDestinationCityName) {
		this.tripDestinationCityName = tripDestinationCityName;
	}

	public String getTripDestinationAirportName() {
		return tripDestinationAirportName;
	}

	public void setTripDestinationAirportName(String tripDestinationAirportName) {
		this.tripDestinationAirportName = tripDestinationAirportName;
	}

	public String getTripDestinationTerminal() {
		return tripDestinationTerminal;
	}

	public void setTripDestinationTerminal(String tripDestinationTerminal) {
		this.tripDestinationTerminal = tripDestinationTerminal;
	}

	public String getTripPlaneType() {
		return tripPlaneType;
	}

	public void setTripPlaneType(String tripPlaneType) {
		this.tripPlaneType = tripPlaneType;
	}

	public Date getTripDepartureTime() {
		return tripDepartureTime;
	}

	public void setTripDepartureTime(Date tripDepartureTime) {
		this.tripDepartureTime = tripDepartureTime;
	}

	public Date getTripDestinationTime() {
		return tripDestinationTime;
	}

	public void setTripDestinationTime(Date tripDestinationTime) {
		this.tripDestinationTime = tripDestinationTime;
	}

	public Byte getTripFlightType() {
		return tripFlightType;
	}

	public void setTripFlightType(Byte tripFlightType) {
		this.tripFlightType = tripFlightType;
	}

	public Byte getTripIsTransfer() {
		return tripIsTransfer;
	}

	public void setTripIsTransfer(Byte tripIsTransfer) {
		this.tripIsTransfer = tripIsTransfer;
	}

	public String getTripCostTime() {
		return tripCostTime;
	}

	public void setTripCostTime(String tripCostTime) {
		this.tripCostTime = tripCostTime;
	}

	public String getReturnDepartureCityName() {
		return returnDepartureCityName;
	}

	public void setReturnDepartureCityName(String returnDepartureCityName) {
		this.returnDepartureCityName = returnDepartureCityName;
	}

	public String getReturnDepartureAirportName() {
		return returnDepartureAirportName;
	}

	public void setReturnDepartureAirportName(String returnDepartureAirportName) {
		this.returnDepartureAirportName = returnDepartureAirportName;
	}

	public String getReturnDepartureTerminal() {
		return returnDepartureTerminal;
	}

	public void setReturnDepartureTerminal(String returnDepartureTerminal) {
		this.returnDepartureTerminal = returnDepartureTerminal;
	}

	public String getReturnDestinationCityName() {
		return returnDestinationCityName;
	}

	public void setReturnDestinationCityName(String returnDestinationCityName) {
		this.returnDestinationCityName = returnDestinationCityName;
	}

	public String getReturnDestinationAirportName() {
		return returnDestinationAirportName;
	}

	public void setReturnDestinationAirportName(String returnDestinationAirportName) {
		this.returnDestinationAirportName = returnDestinationAirportName;
	}

	public String getReturnDestinationTerminal() {
		return returnDestinationTerminal;
	}

	public void setReturnDestinationTerminal(String returnDestinationTerminal) {
		this.returnDestinationTerminal = returnDestinationTerminal;
	}

	public String getReturnPlaneType() {
		return returnPlaneType;
	}

	public void setReturnPlaneType(String returnPlaneType) {
		this.returnPlaneType = returnPlaneType;
	}

	public Date getReturnDepartureTime() {
		return returnDepartureTime;
	}

	public void setReturnDepartureTime(Date returnDepartureTime) {
		this.returnDepartureTime = returnDepartureTime;
	}

	public Date getReturnDestinationTime() {
		return returnDestinationTime;
	}

	public void setReturnDestinationTime(Date returnDestinationTime) {
		this.returnDestinationTime = returnDestinationTime;
	}

	public Byte getReturnFlightType() {
		return returnFlightType;
	}

	public void setReturnFlightType(Byte returnFlightType) {
		this.returnFlightType = returnFlightType;
	}

	public Byte getReturnIsTransfer() {
		return returnIsTransfer;
	}

	public void setReturnIsTransfer(Byte returnIsTransfer) {
		this.returnIsTransfer = returnIsTransfer;
	}

	public String getReturnCostTime() {
		return returnCostTime;
	}

	public void setReturnCostTime(String returnCostTime) {
		this.returnCostTime = returnCostTime;
	}

	public List<PmsProductLadder> getProductLadderList() {
		return productLadderList;
	}

	public void setProductLadderList(List<PmsProductLadder> productLadderList) {
		this.productLadderList = productLadderList;
	}

	public List<PmsProductFullReduction> getProductFullReductionList() {
		return productFullReductionList;
	}

	public void setProductFullReductionList(List<PmsProductFullReduction> productFullReductionList) {
		this.productFullReductionList = productFullReductionList;
	}

	public List<PmsMemberPrice> getMemberPriceList() {
		return memberPriceList;
	}

	public void setMemberPriceList(List<PmsMemberPrice> memberPriceList) {
		this.memberPriceList = memberPriceList;
	}

	public List<PmsSkuStock> getSkuStockList() {
		return skuStockList;
	}

	public void setSkuStockList(List<PmsSkuStock> skuStockList) {
		this.skuStockList = skuStockList;
	}

	public List<PmsProductAttributeValue> getProductAttributeValueList() {
		return productAttributeValueList;
	}

	public void setProductAttributeValueList(List<PmsProductAttributeValue> productAttributeValueList) {
		this.productAttributeValueList = productAttributeValueList;
	}

	public List<CmsSubjectProductRelation> getSubjectProductRelationList() {
		return subjectProductRelationList;
	}

	public void setSubjectProductRelationList(List<CmsSubjectProductRelation> subjectProductRelationList) {
		this.subjectProductRelationList = subjectProductRelationList;
	}

	public List<CmsPrefrenceAreaProductRelation> getPrefrenceAreaProductRelationList() {
		return prefrenceAreaProductRelationList;
	}

	public void setPrefrenceAreaProductRelationList(
			List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList) {
		this.prefrenceAreaProductRelationList = prefrenceAreaProductRelationList;
	}

	public List<PmsProductRelation> getPmsProductRelationList() {
		return pmsProductRelationList;
	}

	public void setPmsProductRelationList(List<PmsProductRelation> pmsProductRelationList) {
		this.pmsProductRelationList = pmsProductRelationList;
	}

}
