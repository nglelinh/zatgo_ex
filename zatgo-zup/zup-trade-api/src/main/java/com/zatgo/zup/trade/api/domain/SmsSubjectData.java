package com.zatgo.zup.trade.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("专题信息")
public class SmsSubjectData {
	
	@ApiModelProperty("")
	private Long id;
	
	@ApiModelProperty("标题")
	private String title;
	
	@ApiModelProperty("专题主图")
	private String pic;
	
	@ApiModelProperty("画册图片用逗号分割")
	private String albumPics;
	
	@ApiModelProperty("描述")
	private String description;
	
	@ApiModelProperty("内容")
	private String content;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getAlbumPics() {
		return albumPics;
	}

	public void setAlbumPics(String albumPics) {
		this.albumPics = albumPics;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
