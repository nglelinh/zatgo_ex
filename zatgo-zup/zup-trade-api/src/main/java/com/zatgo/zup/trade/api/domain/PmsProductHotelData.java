package com.zatgo.zup.trade.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel("酒店产品")
public class PmsProductHotelData {
	
	@ApiModelProperty("酒店名称")
	private String hotelName;
	
	@ApiModelProperty("房型")
	private String roomType;

	@ApiModelProperty("入住人数")
	private String guestNumber;
	
	@ApiModelProperty("入住日期")
	private Date arrivalDate;
	
	@ApiModelProperty("销售价")
	private BigDecimal salePrice;

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getGuestNumber() {
		return guestNumber;
	}

	public void setGuestNumber(String guestNumber) {
		this.guestNumber = guestNumber;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public BigDecimal getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

}
