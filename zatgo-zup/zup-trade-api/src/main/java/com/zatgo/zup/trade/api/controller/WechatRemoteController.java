package com.zatgo.zup.trade.api.controller;

import com.ykb.mall.common.enumType.BusinessEnum;
import com.ykb.mall.common.model.OmsOrder;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.WXPayConstants;
import com.zatgo.zup.trade.api.domain.PortalOrderBaseInfo;
import com.zatgo.zup.trade.api.mapper.PmsProductMapper;
import com.zatgo.zup.trade.api.remoteService.ActivityRemoteService;
import com.zatgo.zup.trade.api.service.OmsPortalOrderService;
import com.zatgo.zup.trade.api.service.ThirdPayRecordService;
import com.zatgo.zup.trade.api.util.WXPayUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by 46041 on 2019/6/13.
 */



@Controller
@RequestMapping(value = "/trade/api/wx/remote")
public class WechatRemoteController {

    private static final Logger logger = LoggerFactory.getLogger(WechatRemoteController.class);


    @Autowired
    private OmsPortalOrderService portalOrderService;
    @Autowired
    private ThirdPayRecordService thirdPayRecordService;
    @Autowired
    private ActivityRemoteService activityRemoteService;

    @PostMapping(value = "/wxPayNotify")
    @ResponseBody
    public void wxPayNotify(@RequestBody Map<String, String> xmlMap, @RequestParam("wxPayKey") String wxPayKey) throws Exception {
        WXPayConstants.SignType signType = WXPayConstants.SignType.HMACSHA256;

        if (!WXPayUtil.isSignatureValid(xmlMap, wxPayKey, signType)) {
            logger.error("微信支付通知签名验证失败：{}", xmlMap);
            throw new BusinessException(BusinessExceptionCode.GO_TO_SIGN_ERROR);
        }

        String orderNumber = xmlMap.get(WXPayConstants.FIELD_OUT_TRADE_NO);
        if (StringUtils.isEmpty(orderNumber)) {
            logger.error("微信支付通知订单号丢失：{}", xmlMap);
            throw new BusinessException(BusinessExceptionCode.LACK_MUST_PARAMETER);
        }

        PortalOrderBaseInfo order = portalOrderService.selectOrderByOrderSn(orderNumber);
        if (order == null) {
            logger.error("微信支付通知未查询到订单：{}", xmlMap);
            throw new BusinessException(BusinessExceptionCode.EX_ORDER_NOT_EXIST);
        }
        if (BusinessEnum.OrderStatus.wait_pay.getCode() != order.getStatus()) {
            logger.error("微信支付重复通知：{}", xmlMap);
            return;
        }
        String totalFee = xmlMap.get(WXPayConstants.FIELD_TOTAL_FEE);
        if (StringUtils.isEmpty(totalFee)) {
            logger.error("微信支付通知支付金额丢失：{}", xmlMap);
            throw new BusinessException(BusinessExceptionCode.LACK_MUST_PARAMETER);
        }

        if (new BigDecimal(totalFee)
                .compareTo(order.getPayPrice().multiply(new BigDecimal("100"))) != 0) {
            logger.error("微信支付通知支付金额不匹配：{}", xmlMap);
            throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
        }

        OmsOrder omsOrder = portalOrderService.selectByPrimaryKey(order.getOrderId());
        // 更改状态，扣除库存
        portalOrderService.paySuccess(order.getOrderId(), 2);

        // 保存用户订单支付操作记录
        portalOrderService.insertOrderPayHistory(order.getOrderId(), order.getPayPrice());

        // 保存支付记录
        thirdPayRecordService.insertPaymentRecord(orderNumber, BusinessEnum.PayType.weixinPay, order.getPayPrice(),
                xmlMap.get(WXPayConstants.FIELD_TRANSACTION_ID));

        String note = omsOrder.getNote();
        if (!StringUtils.isEmpty(note)){
            String[] split = note.split("_");
            String activityId = split[0];
            String userId = split[1];
            ResponseData<Boolean> res = null;
            if (BusinessEnum.CreateOrderType.BARGAIN.getCode().equals(omsOrder.getActivityType().toString())){
                res = activityRemoteService.orderCallBack(activityId, userId, order.getOrderId() + "", true);
                if (res == null || !res.isSuccessful()){
                    logger.info("返回信息 ============== activityId: " + activityId + "_userId:" + userId +  "_orderId: " + order.getOrderId() + "_isSuccess :" + true);
                    throw new BusinessException("", "支付回调通知失败");
                }
            }
            if (BusinessEnum.CreateOrderType.GROUPBOOKING.getCode().equals(omsOrder.getActivityType().toString())){
                res = activityRemoteService.GroupOrderCallBack(activityId, userId, order.getOrderId() + "", true);
                if (res == null || !res.isSuccessful()){
                    logger.info("返回信息 ============== activityId: " + activityId + "_userId:" + userId +  "_orderId: " + order.getOrderId() + "_isSuccess :" + true);
                    throw new BusinessException("", "支付回调通知失败");
                }
            }
        }
    }

}
