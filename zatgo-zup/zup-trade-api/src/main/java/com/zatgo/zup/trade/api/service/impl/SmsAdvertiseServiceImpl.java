package com.zatgo.zup.trade.api.service.impl;

import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.ykb.mall.common.model.SmsHomeAdvertise;
import com.ykb.mall.common.model.SmsHomeAdvertiseExample;
import com.zatgo.zup.trade.api.domain.SmsAdvertiseData;
import com.zatgo.zup.trade.api.mapper.SmsHomeAdvertiseMapper;
import com.zatgo.zup.trade.api.service.SmsAdvertiseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SmsAdvertiseServiceImpl implements SmsAdvertiseService {

	@Autowired
	private SmsHomeAdvertiseMapper smsHomeAdvertiseMapper;

	@Override
	public List<SmsAdvertiseData> homeAdvertiseList(Integer type) {
		SmsHomeAdvertiseExample example = new SmsHomeAdvertiseExample();
		SmsHomeAdvertiseExample.Criteria criteria = example.createCriteria();
		criteria.andTypeEqualTo(type).andStatusEqualTo(YesOrNo.YES.getCode().intValue())
				.andStartTimeLessThanOrEqualTo(new Date()).andEndTimeGreaterThanOrEqualTo(new Date());
		List<SmsHomeAdvertise> advertises = smsHomeAdvertiseMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(advertises)) {
			return new ArrayList<>();
		}

		advertises.sort((fir, sec) -> sec.getSort().compareTo(fir.getSort()));

		List<SmsAdvertiseData> data = new ArrayList<>();
		for (SmsHomeAdvertise advertise : advertises) {
			SmsAdvertiseData advertiseData = new SmsAdvertiseData();
			BeanUtils.copyProperties(advertise, advertiseData);
			data.add(advertiseData);
		}
		return data;
	}

}
