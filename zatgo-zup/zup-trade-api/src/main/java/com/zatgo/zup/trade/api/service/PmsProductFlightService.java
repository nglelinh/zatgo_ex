package com.zatgo.zup.trade.api.service;


import com.ykb.mall.common.model.PmsProductFlightPrice;
import com.zatgo.zup.trade.api.domain.PmsProductFlightResult;

import java.util.List;

public interface PmsProductFlightService {
	
	/**
	 * 查询机票产品详情
	 * @param productId
	 * @return
	 */
	PmsProductFlightResult selectProductFlightDetail(Long productId);

	/**
	 * 查询机票产品详情
	 * @param ids
	 * @return
	 */
	List<PmsProductFlightResult> selectProductFlightDetail(List<Long> ids);
	
	/**
	 * 查询机票价格
	 * @param productId
	 * @return
	 */
	List<PmsProductFlightPrice> selectProductFlightPriceList(Long flightId, String startDate, String endDate);
	
	/**
	 * 查询机票低价列表
	 * @param flightId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<PmsProductFlightPrice> selectProductFlightMinList(Long flightId, String startDate, String endDate);

}
