package com.zatgo.zup.trade.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PmsProductHotel implements Serializable {
    private Long id;

    /**
     * 产品编号
     *
     * @mbggenerated
     */
    private Long productId;

    /**
     * 酒店名称
     *
     * @mbggenerated
     */
    private String hotelName;

    /**
     * 房型
     *
     * @mbggenerated
     */
    private String roomType;

    /**
     * 入住人数
     *
     * @mbggenerated
     */
    private Integer guestNumber;

    /**
     * 入住日期
     *
     * @mbggenerated
     */
    private Date arrivalDate;

    /**
     * 销售价
     *
     * @mbggenerated
     */
    private BigDecimal salePrice;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 状态（0 -待发布，1 - 已上架，2 - 已下架）
     *
     * @mbggenerated
     */
    private Byte status;

    /**
     * 是否为删除状态（0 - 否，1 - 是）
     *
     * @mbggenerated
     */
    private Byte deleteStatus;

    /**
     * 库存
     *
     * @mbggenerated
     */
    private Integer stock;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public Integer getGuestNumber() {
        return guestNumber;
    }

    public void setGuestNumber(Integer guestNumber) {
        this.guestNumber = guestNumber;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Byte deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", productId=").append(productId);
        sb.append(", hotelName=").append(hotelName);
        sb.append(", roomType=").append(roomType);
        sb.append(", guestNumber=").append(guestNumber);
        sb.append(", arrivalDate=").append(arrivalDate);
        sb.append(", salePrice=").append(salePrice);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append(", deleteStatus=").append(deleteStatus);
        sb.append(", stock=").append(stock);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}