package com.zatgo.zup.trade.api.mapper;

import java.util.List;

import com.ykb.mall.common.model.ProductActivityArticle;
import com.ykb.mall.common.model.ProductActivityArticleExample;
import org.apache.ibatis.annotations.Param;

public interface ProductActivityArticleMapper {
    int countByExample(ProductActivityArticleExample example);

    int deleteByExample(ProductActivityArticleExample example);

    int deleteByPrimaryKey(String id);

    int insert(ProductActivityArticle record);

    int insertSelective(ProductActivityArticle record);

    List<ProductActivityArticle> selectByExample(ProductActivityArticleExample example);

    ProductActivityArticle selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ProductActivityArticle record, @Param("example") ProductActivityArticleExample example);

    int updateByExample(@Param("record") ProductActivityArticle record, @Param("example") ProductActivityArticleExample example);

    int updateByPrimaryKeySelective(ProductActivityArticle record);

    int updateByPrimaryKey(ProductActivityArticle record);
}