package com.zatgo.zup.trade.api.mq.rocketmq;//package com.ykb.mall.mapi.mq.rocketmq;
//
//import com.ykb.mall.mapi.exception.BusinessException;
//import com.ykb.mall.mapi.mq.MQService;
//import com.ykb.mall.mapi.mq.rocketmq.tool.RocketMQProducer;
//import org.apache.rocketmq.client.producer.DefaultMQProducer;
//import org.apache.rocketmq.client.producer.MessageQueueSelector;
//import org.apache.rocketmq.common.message.Message;
//import org.apache.rocketmq.common.message.MessageQueue;
//import org.apache.rocketmq.remoting.common.RemotingHelper;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.io.UnsupportedEncodingException;
//import java.util.Date;
//import java.util.List;
//
//@Component("RocketMQServiceImpl")
//public class RocketMQServiceImpl implements MQService {
//
//	private static final Logger logger = LoggerFactory.getLogger(RocketMQServiceImpl.class);
//
//	private static final String PRODUCER_GROUP = "ZUP_PRODUCER_GROUP";
//
//	@Autowired
//	private RocketMQProducer rocketMQProducer;
//
//	@Override
//	public void sendMQmessage(String topic, String tag, String body, String key) throws UnsupportedEncodingException {
//		try {
//			DefaultMQProducer producer = rocketMQProducer.getProducer(PRODUCER_GROUP);
//			producer.send(new Message(topic,tag,body.getBytes(RemotingHelper.DEFAULT_CHARSET)));
//		} catch (Exception e){
//			logger.error(body,e);
//		}
//
//	}
//
//	@Override
//	public void sendMQSeqMessage(String topic, String tag, String body, String key, String shardingKey) {
//		try {
//			DefaultMQProducer producer = rocketMQProducer.getProducer(PRODUCER_GROUP);
//			Message message = new Message(topic, tag, body.getBytes(RemotingHelper.DEFAULT_CHARSET));
//			producer.send(message, new MessageQueueSelector() {
//				@Override
//				public MessageQueue select(List<MessageQueue> list, Message message, Object o) {
//					Long id = (Long) o;
//                    long index = id % list.size();
//                    return list.get((int)index);
//				}
//			},shardingKey);
//		} catch (Exception e) {
//			logger.error(body,e);
//		}
//	}
//
//	@Override
//	public void sendMQDelayMessage(String topic, String tag, String body, String key, Long delayTime) {
//		try {
//			DefaultMQProducer producer = rocketMQProducer.getProducer(PRODUCER_GROUP);
//			Message message = new Message(topic, tag, body.getBytes(RemotingHelper.DEFAULT_CHARSET));
//			message.setDelayTimeLevel(delayTime.intValue());
//			producer.send(message);
//		} catch (Exception e) {
//			logger.error(body,e);
//		}
//
//	}
//
//	@Override
//	public void sendMQTimingMessage(String topic, String tag, String body, String key, Date Timing) {
//		throw new BusinessException();
//	}
//
//	@Override
//	public void sendMQTranscctionMessage(String topic, String tag, String body, String key) {
//		throw new BusinessException();
//	}
//
//}
