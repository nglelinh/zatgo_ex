package com.zatgo.zup.trade.api.task;

import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.trade.api.mapper.OmsOrderMapper;
import com.zatgo.zup.trade.api.service.OmsPortalOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2019/7/25.
 */

@Component
public class CancelUnpaidOrderTask {

    @Autowired
    private OmsPortalOrderService orderService;
    @Autowired
    private RedisLockUtils redisLockUtils;

    @Scheduled(cron = "0 */1 * * * ?")
    public void execute(){
        boolean flag = redisLockUtils.taskLock(RedisKeyConstants.CANCEL_ORDER_TASK_PRE);
        if (flag){
            try{
                orderService.cancelUnpaidOrderTask();
            } finally {
                redisLockUtils.releaseLock(RedisKeyConstants.CANCEL_ORDER_TASK_PRE);
            }
        }
    }
}
