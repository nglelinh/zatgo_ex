package com.zatgo.zup.trade.api.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PmsProductExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PmsProductExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNull() {
            addCriterion("brand_id is null");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNotNull() {
            addCriterion("brand_id is not null");
            return (Criteria) this;
        }

        public Criteria andBrandIdEqualTo(Long value) {
            addCriterion("brand_id =", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotEqualTo(Long value) {
            addCriterion("brand_id <>", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThan(Long value) {
            addCriterion("brand_id >", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThanOrEqualTo(Long value) {
            addCriterion("brand_id >=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThan(Long value) {
            addCriterion("brand_id <", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThanOrEqualTo(Long value) {
            addCriterion("brand_id <=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIn(List<Long> values) {
            addCriterion("brand_id in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotIn(List<Long> values) {
            addCriterion("brand_id not in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdBetween(Long value1, Long value2) {
            addCriterion("brand_id between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotBetween(Long value1, Long value2) {
            addCriterion("brand_id not between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdIsNull() {
            addCriterion("product_category_id is null");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdIsNotNull() {
            addCriterion("product_category_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdEqualTo(Long value) {
            addCriterion("product_category_id =", value, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdNotEqualTo(Long value) {
            addCriterion("product_category_id <>", value, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdGreaterThan(Long value) {
            addCriterion("product_category_id >", value, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdGreaterThanOrEqualTo(Long value) {
            addCriterion("product_category_id >=", value, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdLessThan(Long value) {
            addCriterion("product_category_id <", value, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdLessThanOrEqualTo(Long value) {
            addCriterion("product_category_id <=", value, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdIn(List<Long> values) {
            addCriterion("product_category_id in", values, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdNotIn(List<Long> values) {
            addCriterion("product_category_id not in", values, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdBetween(Long value1, Long value2) {
            addCriterion("product_category_id between", value1, value2, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCategoryIdNotBetween(Long value1, Long value2) {
            addCriterion("product_category_id not between", value1, value2, "productCategoryId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdIsNull() {
            addCriterion("feight_template_id is null");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdIsNotNull() {
            addCriterion("feight_template_id is not null");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdEqualTo(Long value) {
            addCriterion("feight_template_id =", value, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdNotEqualTo(Long value) {
            addCriterion("feight_template_id <>", value, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdGreaterThan(Long value) {
            addCriterion("feight_template_id >", value, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdGreaterThanOrEqualTo(Long value) {
            addCriterion("feight_template_id >=", value, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdLessThan(Long value) {
            addCriterion("feight_template_id <", value, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdLessThanOrEqualTo(Long value) {
            addCriterion("feight_template_id <=", value, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdIn(List<Long> values) {
            addCriterion("feight_template_id in", values, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdNotIn(List<Long> values) {
            addCriterion("feight_template_id not in", values, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdBetween(Long value1, Long value2) {
            addCriterion("feight_template_id between", value1, value2, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andFeightTemplateIdNotBetween(Long value1, Long value2) {
            addCriterion("feight_template_id not between", value1, value2, "feightTemplateId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdIsNull() {
            addCriterion("product_attribute_category_id is null");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdIsNotNull() {
            addCriterion("product_attribute_category_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdEqualTo(Long value) {
            addCriterion("product_attribute_category_id =", value, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdNotEqualTo(Long value) {
            addCriterion("product_attribute_category_id <>", value, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdGreaterThan(Long value) {
            addCriterion("product_attribute_category_id >", value, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdGreaterThanOrEqualTo(Long value) {
            addCriterion("product_attribute_category_id >=", value, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdLessThan(Long value) {
            addCriterion("product_attribute_category_id <", value, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdLessThanOrEqualTo(Long value) {
            addCriterion("product_attribute_category_id <=", value, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdIn(List<Long> values) {
            addCriterion("product_attribute_category_id in", values, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdNotIn(List<Long> values) {
            addCriterion("product_attribute_category_id not in", values, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdBetween(Long value1, Long value2) {
            addCriterion("product_attribute_category_id between", value1, value2, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductAttributeCategoryIdNotBetween(Long value1, Long value2) {
            addCriterion("product_attribute_category_id not between", value1, value2, "productAttributeCategoryId");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeIsNull() {
            addCriterion("business_type is null");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeIsNotNull() {
            addCriterion("business_type is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeEqualTo(Byte value) {
            addCriterion("business_type =", value, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeNotEqualTo(Byte value) {
            addCriterion("business_type <>", value, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeGreaterThan(Byte value) {
            addCriterion("business_type >", value, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("business_type >=", value, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeLessThan(Byte value) {
            addCriterion("business_type <", value, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeLessThanOrEqualTo(Byte value) {
            addCriterion("business_type <=", value, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeIn(List<Byte> values) {
            addCriterion("business_type in", values, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeNotIn(List<Byte> values) {
            addCriterion("business_type not in", values, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeBetween(Byte value1, Byte value2) {
            addCriterion("business_type between", value1, value2, "businessType");
            return (Criteria) this;
        }

        public Criteria andBusinessTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("business_type not between", value1, value2, "businessType");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNull() {
            addCriterion("module_id is null");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNotNull() {
            addCriterion("module_id is not null");
            return (Criteria) this;
        }

        public Criteria andModuleIdEqualTo(String value) {
            addCriterion("module_id =", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotEqualTo(String value) {
            addCriterion("module_id <>", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThan(String value) {
            addCriterion("module_id >", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThanOrEqualTo(String value) {
            addCriterion("module_id >=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThan(String value) {
            addCriterion("module_id <", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThanOrEqualTo(String value) {
            addCriterion("module_id <=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLike(String value) {
            addCriterion("module_id like", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotLike(String value) {
            addCriterion("module_id not like", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdIn(List<String> values) {
            addCriterion("module_id in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotIn(List<String> values) {
            addCriterion("module_id not in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdBetween(String value1, String value2) {
            addCriterion("module_id between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotBetween(String value1, String value2) {
            addCriterion("module_id not between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andPicIsNull() {
            addCriterion("pic is null");
            return (Criteria) this;
        }

        public Criteria andPicIsNotNull() {
            addCriterion("pic is not null");
            return (Criteria) this;
        }

        public Criteria andPicEqualTo(String value) {
            addCriterion("pic =", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotEqualTo(String value) {
            addCriterion("pic <>", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThan(String value) {
            addCriterion("pic >", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThanOrEqualTo(String value) {
            addCriterion("pic >=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThan(String value) {
            addCriterion("pic <", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThanOrEqualTo(String value) {
            addCriterion("pic <=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLike(String value) {
            addCriterion("pic like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotLike(String value) {
            addCriterion("pic not like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicIn(List<String> values) {
            addCriterion("pic in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotIn(List<String> values) {
            addCriterion("pic not in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicBetween(String value1, String value2) {
            addCriterion("pic between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotBetween(String value1, String value2) {
            addCriterion("pic not between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andProductSnIsNull() {
            addCriterion("product_sn is null");
            return (Criteria) this;
        }

        public Criteria andProductSnIsNotNull() {
            addCriterion("product_sn is not null");
            return (Criteria) this;
        }

        public Criteria andProductSnEqualTo(String value) {
            addCriterion("product_sn =", value, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnNotEqualTo(String value) {
            addCriterion("product_sn <>", value, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnGreaterThan(String value) {
            addCriterion("product_sn >", value, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnGreaterThanOrEqualTo(String value) {
            addCriterion("product_sn >=", value, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnLessThan(String value) {
            addCriterion("product_sn <", value, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnLessThanOrEqualTo(String value) {
            addCriterion("product_sn <=", value, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnLike(String value) {
            addCriterion("product_sn like", value, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnNotLike(String value) {
            addCriterion("product_sn not like", value, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnIn(List<String> values) {
            addCriterion("product_sn in", values, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnNotIn(List<String> values) {
            addCriterion("product_sn not in", values, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnBetween(String value1, String value2) {
            addCriterion("product_sn between", value1, value2, "productSn");
            return (Criteria) this;
        }

        public Criteria andProductSnNotBetween(String value1, String value2) {
            addCriterion("product_sn not between", value1, value2, "productSn");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIsNull() {
            addCriterion("delete_status is null");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIsNotNull() {
            addCriterion("delete_status is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusEqualTo(Integer value) {
            addCriterion("delete_status =", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotEqualTo(Integer value) {
            addCriterion("delete_status <>", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusGreaterThan(Integer value) {
            addCriterion("delete_status >", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("delete_status >=", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusLessThan(Integer value) {
            addCriterion("delete_status <", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusLessThanOrEqualTo(Integer value) {
            addCriterion("delete_status <=", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIn(List<Integer> values) {
            addCriterion("delete_status in", values, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotIn(List<Integer> values) {
            addCriterion("delete_status not in", values, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusBetween(Integer value1, Integer value2) {
            addCriterion("delete_status between", value1, value2, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("delete_status not between", value1, value2, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusIsNull() {
            addCriterion("publish_status is null");
            return (Criteria) this;
        }

        public Criteria andPublishStatusIsNotNull() {
            addCriterion("publish_status is not null");
            return (Criteria) this;
        }

        public Criteria andPublishStatusEqualTo(Integer value) {
            addCriterion("publish_status =", value, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusNotEqualTo(Integer value) {
            addCriterion("publish_status <>", value, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusGreaterThan(Integer value) {
            addCriterion("publish_status >", value, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("publish_status >=", value, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusLessThan(Integer value) {
            addCriterion("publish_status <", value, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusLessThanOrEqualTo(Integer value) {
            addCriterion("publish_status <=", value, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusIn(List<Integer> values) {
            addCriterion("publish_status in", values, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusNotIn(List<Integer> values) {
            addCriterion("publish_status not in", values, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusBetween(Integer value1, Integer value2) {
            addCriterion("publish_status between", value1, value2, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andPublishStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("publish_status not between", value1, value2, "publishStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusIsNull() {
            addCriterion("new_status is null");
            return (Criteria) this;
        }

        public Criteria andNewStatusIsNotNull() {
            addCriterion("new_status is not null");
            return (Criteria) this;
        }

        public Criteria andNewStatusEqualTo(Integer value) {
            addCriterion("new_status =", value, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusNotEqualTo(Integer value) {
            addCriterion("new_status <>", value, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusGreaterThan(Integer value) {
            addCriterion("new_status >", value, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("new_status >=", value, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusLessThan(Integer value) {
            addCriterion("new_status <", value, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusLessThanOrEqualTo(Integer value) {
            addCriterion("new_status <=", value, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusIn(List<Integer> values) {
            addCriterion("new_status in", values, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusNotIn(List<Integer> values) {
            addCriterion("new_status not in", values, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusBetween(Integer value1, Integer value2) {
            addCriterion("new_status between", value1, value2, "newStatus");
            return (Criteria) this;
        }

        public Criteria andNewStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("new_status not between", value1, value2, "newStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusIsNull() {
            addCriterion("recommand_status is null");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusIsNotNull() {
            addCriterion("recommand_status is not null");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusEqualTo(Integer value) {
            addCriterion("recommand_status =", value, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusNotEqualTo(Integer value) {
            addCriterion("recommand_status <>", value, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusGreaterThan(Integer value) {
            addCriterion("recommand_status >", value, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("recommand_status >=", value, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusLessThan(Integer value) {
            addCriterion("recommand_status <", value, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusLessThanOrEqualTo(Integer value) {
            addCriterion("recommand_status <=", value, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusIn(List<Integer> values) {
            addCriterion("recommand_status in", values, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusNotIn(List<Integer> values) {
            addCriterion("recommand_status not in", values, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusBetween(Integer value1, Integer value2) {
            addCriterion("recommand_status between", value1, value2, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andRecommandStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("recommand_status not between", value1, value2, "recommandStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusIsNull() {
            addCriterion("verify_status is null");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusIsNotNull() {
            addCriterion("verify_status is not null");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusEqualTo(Integer value) {
            addCriterion("verify_status =", value, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusNotEqualTo(Integer value) {
            addCriterion("verify_status <>", value, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusGreaterThan(Integer value) {
            addCriterion("verify_status >", value, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("verify_status >=", value, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusLessThan(Integer value) {
            addCriterion("verify_status <", value, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusLessThanOrEqualTo(Integer value) {
            addCriterion("verify_status <=", value, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusIn(List<Integer> values) {
            addCriterion("verify_status in", values, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusNotIn(List<Integer> values) {
            addCriterion("verify_status not in", values, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusBetween(Integer value1, Integer value2) {
            addCriterion("verify_status between", value1, value2, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andVerifyStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("verify_status not between", value1, value2, "verifyStatus");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSaleIsNull() {
            addCriterion("sale is null");
            return (Criteria) this;
        }

        public Criteria andSaleIsNotNull() {
            addCriterion("sale is not null");
            return (Criteria) this;
        }

        public Criteria andSaleEqualTo(Integer value) {
            addCriterion("sale =", value, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleNotEqualTo(Integer value) {
            addCriterion("sale <>", value, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleGreaterThan(Integer value) {
            addCriterion("sale >", value, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleGreaterThanOrEqualTo(Integer value) {
            addCriterion("sale >=", value, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleLessThan(Integer value) {
            addCriterion("sale <", value, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleLessThanOrEqualTo(Integer value) {
            addCriterion("sale <=", value, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleIn(List<Integer> values) {
            addCriterion("sale in", values, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleNotIn(List<Integer> values) {
            addCriterion("sale not in", values, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleBetween(Integer value1, Integer value2) {
            addCriterion("sale between", value1, value2, "sale");
            return (Criteria) this;
        }

        public Criteria andSaleNotBetween(Integer value1, Integer value2) {
            addCriterion("sale not between", value1, value2, "sale");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(BigDecimal value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(BigDecimal value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(BigDecimal value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(BigDecimal value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<BigDecimal> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<BigDecimal> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIsNull() {
            addCriterion("promotion_price is null");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIsNotNull() {
            addCriterion("promotion_price is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceEqualTo(BigDecimal value) {
            addCriterion("promotion_price =", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotEqualTo(BigDecimal value) {
            addCriterion("promotion_price <>", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceGreaterThan(BigDecimal value) {
            addCriterion("promotion_price >", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_price >=", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceLessThan(BigDecimal value) {
            addCriterion("promotion_price <", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_price <=", value, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceIn(List<BigDecimal> values) {
            addCriterion("promotion_price in", values, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotIn(List<BigDecimal> values) {
            addCriterion("promotion_price not in", values, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_price between", value1, value2, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_price not between", value1, value2, "promotionPrice");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthIsNull() {
            addCriterion("gift_growth is null");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthIsNotNull() {
            addCriterion("gift_growth is not null");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthEqualTo(Integer value) {
            addCriterion("gift_growth =", value, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthNotEqualTo(Integer value) {
            addCriterion("gift_growth <>", value, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthGreaterThan(Integer value) {
            addCriterion("gift_growth >", value, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthGreaterThanOrEqualTo(Integer value) {
            addCriterion("gift_growth >=", value, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthLessThan(Integer value) {
            addCriterion("gift_growth <", value, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthLessThanOrEqualTo(Integer value) {
            addCriterion("gift_growth <=", value, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthIn(List<Integer> values) {
            addCriterion("gift_growth in", values, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthNotIn(List<Integer> values) {
            addCriterion("gift_growth not in", values, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthBetween(Integer value1, Integer value2) {
            addCriterion("gift_growth between", value1, value2, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftGrowthNotBetween(Integer value1, Integer value2) {
            addCriterion("gift_growth not between", value1, value2, "giftGrowth");
            return (Criteria) this;
        }

        public Criteria andGiftPointIsNull() {
            addCriterion("gift_point is null");
            return (Criteria) this;
        }

        public Criteria andGiftPointIsNotNull() {
            addCriterion("gift_point is not null");
            return (Criteria) this;
        }

        public Criteria andGiftPointEqualTo(Integer value) {
            addCriterion("gift_point =", value, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointNotEqualTo(Integer value) {
            addCriterion("gift_point <>", value, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointGreaterThan(Integer value) {
            addCriterion("gift_point >", value, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointGreaterThanOrEqualTo(Integer value) {
            addCriterion("gift_point >=", value, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointLessThan(Integer value) {
            addCriterion("gift_point <", value, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointLessThanOrEqualTo(Integer value) {
            addCriterion("gift_point <=", value, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointIn(List<Integer> values) {
            addCriterion("gift_point in", values, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointNotIn(List<Integer> values) {
            addCriterion("gift_point not in", values, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointBetween(Integer value1, Integer value2) {
            addCriterion("gift_point between", value1, value2, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andGiftPointNotBetween(Integer value1, Integer value2) {
            addCriterion("gift_point not between", value1, value2, "giftPoint");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitIsNull() {
            addCriterion("use_point_limit is null");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitIsNotNull() {
            addCriterion("use_point_limit is not null");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitEqualTo(Integer value) {
            addCriterion("use_point_limit =", value, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitNotEqualTo(Integer value) {
            addCriterion("use_point_limit <>", value, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitGreaterThan(Integer value) {
            addCriterion("use_point_limit >", value, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitGreaterThanOrEqualTo(Integer value) {
            addCriterion("use_point_limit >=", value, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitLessThan(Integer value) {
            addCriterion("use_point_limit <", value, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitLessThanOrEqualTo(Integer value) {
            addCriterion("use_point_limit <=", value, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitIn(List<Integer> values) {
            addCriterion("use_point_limit in", values, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitNotIn(List<Integer> values) {
            addCriterion("use_point_limit not in", values, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitBetween(Integer value1, Integer value2) {
            addCriterion("use_point_limit between", value1, value2, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andUsePointLimitNotBetween(Integer value1, Integer value2) {
            addCriterion("use_point_limit not between", value1, value2, "usePointLimit");
            return (Criteria) this;
        }

        public Criteria andSubTitleIsNull() {
            addCriterion("sub_title is null");
            return (Criteria) this;
        }

        public Criteria andSubTitleIsNotNull() {
            addCriterion("sub_title is not null");
            return (Criteria) this;
        }

        public Criteria andSubTitleEqualTo(String value) {
            addCriterion("sub_title =", value, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleNotEqualTo(String value) {
            addCriterion("sub_title <>", value, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleGreaterThan(String value) {
            addCriterion("sub_title >", value, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleGreaterThanOrEqualTo(String value) {
            addCriterion("sub_title >=", value, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleLessThan(String value) {
            addCriterion("sub_title <", value, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleLessThanOrEqualTo(String value) {
            addCriterion("sub_title <=", value, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleLike(String value) {
            addCriterion("sub_title like", value, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleNotLike(String value) {
            addCriterion("sub_title not like", value, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleIn(List<String> values) {
            addCriterion("sub_title in", values, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleNotIn(List<String> values) {
            addCriterion("sub_title not in", values, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleBetween(String value1, String value2) {
            addCriterion("sub_title between", value1, value2, "subTitle");
            return (Criteria) this;
        }

        public Criteria andSubTitleNotBetween(String value1, String value2) {
            addCriterion("sub_title not between", value1, value2, "subTitle");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIsNull() {
            addCriterion("original_price is null");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIsNotNull() {
            addCriterion("original_price is not null");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceEqualTo(BigDecimal value) {
            addCriterion("original_price =", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotEqualTo(BigDecimal value) {
            addCriterion("original_price <>", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceGreaterThan(BigDecimal value) {
            addCriterion("original_price >", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("original_price >=", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceLessThan(BigDecimal value) {
            addCriterion("original_price <", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("original_price <=", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIn(List<BigDecimal> values) {
            addCriterion("original_price in", values, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotIn(List<BigDecimal> values) {
            addCriterion("original_price not in", values, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("original_price between", value1, value2, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("original_price not between", value1, value2, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andStockIsNull() {
            addCriterion("stock is null");
            return (Criteria) this;
        }

        public Criteria andStockIsNotNull() {
            addCriterion("stock is not null");
            return (Criteria) this;
        }

        public Criteria andStockEqualTo(Integer value) {
            addCriterion("stock =", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotEqualTo(Integer value) {
            addCriterion("stock <>", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThan(Integer value) {
            addCriterion("stock >", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThanOrEqualTo(Integer value) {
            addCriterion("stock >=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThan(Integer value) {
            addCriterion("stock <", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThanOrEqualTo(Integer value) {
            addCriterion("stock <=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockIn(List<Integer> values) {
            addCriterion("stock in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotIn(List<Integer> values) {
            addCriterion("stock not in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockBetween(Integer value1, Integer value2) {
            addCriterion("stock between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotBetween(Integer value1, Integer value2) {
            addCriterion("stock not between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andLowStockIsNull() {
            addCriterion("low_stock is null");
            return (Criteria) this;
        }

        public Criteria andLowStockIsNotNull() {
            addCriterion("low_stock is not null");
            return (Criteria) this;
        }

        public Criteria andLowStockEqualTo(Integer value) {
            addCriterion("low_stock =", value, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockNotEqualTo(Integer value) {
            addCriterion("low_stock <>", value, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockGreaterThan(Integer value) {
            addCriterion("low_stock >", value, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockGreaterThanOrEqualTo(Integer value) {
            addCriterion("low_stock >=", value, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockLessThan(Integer value) {
            addCriterion("low_stock <", value, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockLessThanOrEqualTo(Integer value) {
            addCriterion("low_stock <=", value, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockIn(List<Integer> values) {
            addCriterion("low_stock in", values, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockNotIn(List<Integer> values) {
            addCriterion("low_stock not in", values, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockBetween(Integer value1, Integer value2) {
            addCriterion("low_stock between", value1, value2, "lowStock");
            return (Criteria) this;
        }

        public Criteria andLowStockNotBetween(Integer value1, Integer value2) {
            addCriterion("low_stock not between", value1, value2, "lowStock");
            return (Criteria) this;
        }

        public Criteria andUnitIsNull() {
            addCriterion("unit is null");
            return (Criteria) this;
        }

        public Criteria andUnitIsNotNull() {
            addCriterion("unit is not null");
            return (Criteria) this;
        }

        public Criteria andUnitEqualTo(String value) {
            addCriterion("unit =", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotEqualTo(String value) {
            addCriterion("unit <>", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThan(String value) {
            addCriterion("unit >", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThanOrEqualTo(String value) {
            addCriterion("unit >=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThan(String value) {
            addCriterion("unit <", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThanOrEqualTo(String value) {
            addCriterion("unit <=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLike(String value) {
            addCriterion("unit like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotLike(String value) {
            addCriterion("unit not like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitIn(List<String> values) {
            addCriterion("unit in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotIn(List<String> values) {
            addCriterion("unit not in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitBetween(String value1, String value2) {
            addCriterion("unit between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotBetween(String value1, String value2) {
            addCriterion("unit not between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andWeightIsNull() {
            addCriterion("weight is null");
            return (Criteria) this;
        }

        public Criteria andWeightIsNotNull() {
            addCriterion("weight is not null");
            return (Criteria) this;
        }

        public Criteria andWeightEqualTo(BigDecimal value) {
            addCriterion("weight =", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotEqualTo(BigDecimal value) {
            addCriterion("weight <>", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThan(BigDecimal value) {
            addCriterion("weight >", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("weight >=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThan(BigDecimal value) {
            addCriterion("weight <", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThanOrEqualTo(BigDecimal value) {
            addCriterion("weight <=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightIn(List<BigDecimal> values) {
            addCriterion("weight in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotIn(List<BigDecimal> values) {
            addCriterion("weight not in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("weight between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("weight not between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusIsNull() {
            addCriterion("preview_status is null");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusIsNotNull() {
            addCriterion("preview_status is not null");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusEqualTo(Integer value) {
            addCriterion("preview_status =", value, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusNotEqualTo(Integer value) {
            addCriterion("preview_status <>", value, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusGreaterThan(Integer value) {
            addCriterion("preview_status >", value, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("preview_status >=", value, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusLessThan(Integer value) {
            addCriterion("preview_status <", value, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusLessThanOrEqualTo(Integer value) {
            addCriterion("preview_status <=", value, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusIn(List<Integer> values) {
            addCriterion("preview_status in", values, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusNotIn(List<Integer> values) {
            addCriterion("preview_status not in", values, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusBetween(Integer value1, Integer value2) {
            addCriterion("preview_status between", value1, value2, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andPreviewStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("preview_status not between", value1, value2, "previewStatus");
            return (Criteria) this;
        }

        public Criteria andServiceIdsIsNull() {
            addCriterion("service_ids is null");
            return (Criteria) this;
        }

        public Criteria andServiceIdsIsNotNull() {
            addCriterion("service_ids is not null");
            return (Criteria) this;
        }

        public Criteria andServiceIdsEqualTo(String value) {
            addCriterion("service_ids =", value, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsNotEqualTo(String value) {
            addCriterion("service_ids <>", value, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsGreaterThan(String value) {
            addCriterion("service_ids >", value, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsGreaterThanOrEqualTo(String value) {
            addCriterion("service_ids >=", value, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsLessThan(String value) {
            addCriterion("service_ids <", value, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsLessThanOrEqualTo(String value) {
            addCriterion("service_ids <=", value, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsLike(String value) {
            addCriterion("service_ids like", value, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsNotLike(String value) {
            addCriterion("service_ids not like", value, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsIn(List<String> values) {
            addCriterion("service_ids in", values, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsNotIn(List<String> values) {
            addCriterion("service_ids not in", values, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsBetween(String value1, String value2) {
            addCriterion("service_ids between", value1, value2, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andServiceIdsNotBetween(String value1, String value2) {
            addCriterion("service_ids not between", value1, value2, "serviceIds");
            return (Criteria) this;
        }

        public Criteria andKeywordsIsNull() {
            addCriterion("keywords is null");
            return (Criteria) this;
        }

        public Criteria andKeywordsIsNotNull() {
            addCriterion("keywords is not null");
            return (Criteria) this;
        }

        public Criteria andKeywordsEqualTo(String value) {
            addCriterion("keywords =", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotEqualTo(String value) {
            addCriterion("keywords <>", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsGreaterThan(String value) {
            addCriterion("keywords >", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsGreaterThanOrEqualTo(String value) {
            addCriterion("keywords >=", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLessThan(String value) {
            addCriterion("keywords <", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLessThanOrEqualTo(String value) {
            addCriterion("keywords <=", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLike(String value) {
            addCriterion("keywords like", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotLike(String value) {
            addCriterion("keywords not like", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsIn(List<String> values) {
            addCriterion("keywords in", values, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotIn(List<String> values) {
            addCriterion("keywords not in", values, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsBetween(String value1, String value2) {
            addCriterion("keywords between", value1, value2, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotBetween(String value1, String value2) {
            addCriterion("keywords not between", value1, value2, "keywords");
            return (Criteria) this;
        }

        public Criteria andNoteIsNull() {
            addCriterion("note is null");
            return (Criteria) this;
        }

        public Criteria andNoteIsNotNull() {
            addCriterion("note is not null");
            return (Criteria) this;
        }

        public Criteria andNoteEqualTo(String value) {
            addCriterion("note =", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteNotEqualTo(String value) {
            addCriterion("note <>", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteGreaterThan(String value) {
            addCriterion("note >", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteGreaterThanOrEqualTo(String value) {
            addCriterion("note >=", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteLessThan(String value) {
            addCriterion("note <", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteLessThanOrEqualTo(String value) {
            addCriterion("note <=", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteLike(String value) {
            addCriterion("note like", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteNotLike(String value) {
            addCriterion("note not like", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteIn(List<String> values) {
            addCriterion("note in", values, "note");
            return (Criteria) this;
        }

        public Criteria andNoteNotIn(List<String> values) {
            addCriterion("note not in", values, "note");
            return (Criteria) this;
        }

        public Criteria andNoteBetween(String value1, String value2) {
            addCriterion("note between", value1, value2, "note");
            return (Criteria) this;
        }

        public Criteria andNoteNotBetween(String value1, String value2) {
            addCriterion("note not between", value1, value2, "note");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsIsNull() {
            addCriterion("album_pics is null");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsIsNotNull() {
            addCriterion("album_pics is not null");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsEqualTo(String value) {
            addCriterion("album_pics =", value, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsNotEqualTo(String value) {
            addCriterion("album_pics <>", value, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsGreaterThan(String value) {
            addCriterion("album_pics >", value, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsGreaterThanOrEqualTo(String value) {
            addCriterion("album_pics >=", value, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsLessThan(String value) {
            addCriterion("album_pics <", value, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsLessThanOrEqualTo(String value) {
            addCriterion("album_pics <=", value, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsLike(String value) {
            addCriterion("album_pics like", value, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsNotLike(String value) {
            addCriterion("album_pics not like", value, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsIn(List<String> values) {
            addCriterion("album_pics in", values, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsNotIn(List<String> values) {
            addCriterion("album_pics not in", values, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsBetween(String value1, String value2) {
            addCriterion("album_pics between", value1, value2, "albumPics");
            return (Criteria) this;
        }

        public Criteria andAlbumPicsNotBetween(String value1, String value2) {
            addCriterion("album_pics not between", value1, value2, "albumPics");
            return (Criteria) this;
        }

        public Criteria andDetailTitleIsNull() {
            addCriterion("detail_title is null");
            return (Criteria) this;
        }

        public Criteria andDetailTitleIsNotNull() {
            addCriterion("detail_title is not null");
            return (Criteria) this;
        }

        public Criteria andDetailTitleEqualTo(String value) {
            addCriterion("detail_title =", value, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleNotEqualTo(String value) {
            addCriterion("detail_title <>", value, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleGreaterThan(String value) {
            addCriterion("detail_title >", value, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleGreaterThanOrEqualTo(String value) {
            addCriterion("detail_title >=", value, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleLessThan(String value) {
            addCriterion("detail_title <", value, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleLessThanOrEqualTo(String value) {
            addCriterion("detail_title <=", value, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleLike(String value) {
            addCriterion("detail_title like", value, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleNotLike(String value) {
            addCriterion("detail_title not like", value, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleIn(List<String> values) {
            addCriterion("detail_title in", values, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleNotIn(List<String> values) {
            addCriterion("detail_title not in", values, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleBetween(String value1, String value2) {
            addCriterion("detail_title between", value1, value2, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andDetailTitleNotBetween(String value1, String value2) {
            addCriterion("detail_title not between", value1, value2, "detailTitle");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeIsNull() {
            addCriterion("promotion_start_time is null");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeIsNotNull() {
            addCriterion("promotion_start_time is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeEqualTo(Date value) {
            addCriterion("promotion_start_time =", value, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeNotEqualTo(Date value) {
            addCriterion("promotion_start_time <>", value, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeGreaterThan(Date value) {
            addCriterion("promotion_start_time >", value, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("promotion_start_time >=", value, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeLessThan(Date value) {
            addCriterion("promotion_start_time <", value, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("promotion_start_time <=", value, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeIn(List<Date> values) {
            addCriterion("promotion_start_time in", values, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeNotIn(List<Date> values) {
            addCriterion("promotion_start_time not in", values, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeBetween(Date value1, Date value2) {
            addCriterion("promotion_start_time between", value1, value2, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("promotion_start_time not between", value1, value2, "promotionStartTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeIsNull() {
            addCriterion("promotion_end_time is null");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeIsNotNull() {
            addCriterion("promotion_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeEqualTo(Date value) {
            addCriterion("promotion_end_time =", value, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeNotEqualTo(Date value) {
            addCriterion("promotion_end_time <>", value, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeGreaterThan(Date value) {
            addCriterion("promotion_end_time >", value, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("promotion_end_time >=", value, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeLessThan(Date value) {
            addCriterion("promotion_end_time <", value, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("promotion_end_time <=", value, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeIn(List<Date> values) {
            addCriterion("promotion_end_time in", values, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeNotIn(List<Date> values) {
            addCriterion("promotion_end_time not in", values, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeBetween(Date value1, Date value2) {
            addCriterion("promotion_end_time between", value1, value2, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("promotion_end_time not between", value1, value2, "promotionEndTime");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitIsNull() {
            addCriterion("promotion_per_limit is null");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitIsNotNull() {
            addCriterion("promotion_per_limit is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitEqualTo(Integer value) {
            addCriterion("promotion_per_limit =", value, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitNotEqualTo(Integer value) {
            addCriterion("promotion_per_limit <>", value, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitGreaterThan(Integer value) {
            addCriterion("promotion_per_limit >", value, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitGreaterThanOrEqualTo(Integer value) {
            addCriterion("promotion_per_limit >=", value, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitLessThan(Integer value) {
            addCriterion("promotion_per_limit <", value, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitLessThanOrEqualTo(Integer value) {
            addCriterion("promotion_per_limit <=", value, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitIn(List<Integer> values) {
            addCriterion("promotion_per_limit in", values, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitNotIn(List<Integer> values) {
            addCriterion("promotion_per_limit not in", values, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitBetween(Integer value1, Integer value2) {
            addCriterion("promotion_per_limit between", value1, value2, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionPerLimitNotBetween(Integer value1, Integer value2) {
            addCriterion("promotion_per_limit not between", value1, value2, "promotionPerLimit");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeIsNull() {
            addCriterion("promotion_type is null");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeIsNotNull() {
            addCriterion("promotion_type is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeEqualTo(Integer value) {
            addCriterion("promotion_type =", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeNotEqualTo(Integer value) {
            addCriterion("promotion_type <>", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeGreaterThan(Integer value) {
            addCriterion("promotion_type >", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("promotion_type >=", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeLessThan(Integer value) {
            addCriterion("promotion_type <", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeLessThanOrEqualTo(Integer value) {
            addCriterion("promotion_type <=", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeIn(List<Integer> values) {
            addCriterion("promotion_type in", values, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeNotIn(List<Integer> values) {
            addCriterion("promotion_type not in", values, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeBetween(Integer value1, Integer value2) {
            addCriterion("promotion_type between", value1, value2, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("promotion_type not between", value1, value2, "promotionType");
            return (Criteria) this;
        }

        public Criteria andBrandNameIsNull() {
            addCriterion("brand_name is null");
            return (Criteria) this;
        }

        public Criteria andBrandNameIsNotNull() {
            addCriterion("brand_name is not null");
            return (Criteria) this;
        }

        public Criteria andBrandNameEqualTo(String value) {
            addCriterion("brand_name =", value, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameNotEqualTo(String value) {
            addCriterion("brand_name <>", value, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameGreaterThan(String value) {
            addCriterion("brand_name >", value, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameGreaterThanOrEqualTo(String value) {
            addCriterion("brand_name >=", value, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameLessThan(String value) {
            addCriterion("brand_name <", value, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameLessThanOrEqualTo(String value) {
            addCriterion("brand_name <=", value, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameLike(String value) {
            addCriterion("brand_name like", value, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameNotLike(String value) {
            addCriterion("brand_name not like", value, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameIn(List<String> values) {
            addCriterion("brand_name in", values, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameNotIn(List<String> values) {
            addCriterion("brand_name not in", values, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameBetween(String value1, String value2) {
            addCriterion("brand_name between", value1, value2, "brandName");
            return (Criteria) this;
        }

        public Criteria andBrandNameNotBetween(String value1, String value2) {
            addCriterion("brand_name not between", value1, value2, "brandName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameIsNull() {
            addCriterion("product_category_name is null");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameIsNotNull() {
            addCriterion("product_category_name is not null");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameEqualTo(String value) {
            addCriterion("product_category_name =", value, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameNotEqualTo(String value) {
            addCriterion("product_category_name <>", value, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameGreaterThan(String value) {
            addCriterion("product_category_name >", value, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameGreaterThanOrEqualTo(String value) {
            addCriterion("product_category_name >=", value, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameLessThan(String value) {
            addCriterion("product_category_name <", value, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameLessThanOrEqualTo(String value) {
            addCriterion("product_category_name <=", value, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameLike(String value) {
            addCriterion("product_category_name like", value, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameNotLike(String value) {
            addCriterion("product_category_name not like", value, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameIn(List<String> values) {
            addCriterion("product_category_name in", values, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameNotIn(List<String> values) {
            addCriterion("product_category_name not in", values, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameBetween(String value1, String value2) {
            addCriterion("product_category_name between", value1, value2, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andProductCategoryNameNotBetween(String value1, String value2) {
            addCriterion("product_category_name not between", value1, value2, "productCategoryName");
            return (Criteria) this;
        }

        public Criteria andChildPriceIsNull() {
            addCriterion("child_price is null");
            return (Criteria) this;
        }

        public Criteria andChildPriceIsNotNull() {
            addCriterion("child_price is not null");
            return (Criteria) this;
        }

        public Criteria andChildPriceEqualTo(BigDecimal value) {
            addCriterion("child_price =", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceNotEqualTo(BigDecimal value) {
            addCriterion("child_price <>", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceGreaterThan(BigDecimal value) {
            addCriterion("child_price >", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("child_price >=", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceLessThan(BigDecimal value) {
            addCriterion("child_price <", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("child_price <=", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceIn(List<BigDecimal> values) {
            addCriterion("child_price in", values, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceNotIn(List<BigDecimal> values) {
            addCriterion("child_price not in", values, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("child_price between", value1, value2, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("child_price not between", value1, value2, "childPrice");
            return (Criteria) this;
        }

        public Criteria andDayNightIsNull() {
            addCriterion("day_night is null");
            return (Criteria) this;
        }

        public Criteria andDayNightIsNotNull() {
            addCriterion("day_night is not null");
            return (Criteria) this;
        }

        public Criteria andDayNightEqualTo(String value) {
            addCriterion("day_night =", value, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightNotEqualTo(String value) {
            addCriterion("day_night <>", value, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightGreaterThan(String value) {
            addCriterion("day_night >", value, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightGreaterThanOrEqualTo(String value) {
            addCriterion("day_night >=", value, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightLessThan(String value) {
            addCriterion("day_night <", value, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightLessThanOrEqualTo(String value) {
            addCriterion("day_night <=", value, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightLike(String value) {
            addCriterion("day_night like", value, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightNotLike(String value) {
            addCriterion("day_night not like", value, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightIn(List<String> values) {
            addCriterion("day_night in", values, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightNotIn(List<String> values) {
            addCriterion("day_night not in", values, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightBetween(String value1, String value2) {
            addCriterion("day_night between", value1, value2, "dayNight");
            return (Criteria) this;
        }

        public Criteria andDayNightNotBetween(String value1, String value2) {
            addCriterion("day_night not between", value1, value2, "dayNight");
            return (Criteria) this;
        }

        public Criteria andLikeNumIsNull() {
            addCriterion("like_num is null");
            return (Criteria) this;
        }

        public Criteria andLikeNumIsNotNull() {
            addCriterion("like_num is not null");
            return (Criteria) this;
        }

        public Criteria andLikeNumEqualTo(Integer value) {
            addCriterion("like_num =", value, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumNotEqualTo(Integer value) {
            addCriterion("like_num <>", value, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumGreaterThan(Integer value) {
            addCriterion("like_num >", value, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("like_num >=", value, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumLessThan(Integer value) {
            addCriterion("like_num <", value, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumLessThanOrEqualTo(Integer value) {
            addCriterion("like_num <=", value, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumIn(List<Integer> values) {
            addCriterion("like_num in", values, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumNotIn(List<Integer> values) {
            addCriterion("like_num not in", values, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumBetween(Integer value1, Integer value2) {
            addCriterion("like_num between", value1, value2, "likeNum");
            return (Criteria) this;
        }

        public Criteria andLikeNumNotBetween(Integer value1, Integer value2) {
            addCriterion("like_num not between", value1, value2, "likeNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumIsNull() {
            addCriterion("watch_num is null");
            return (Criteria) this;
        }

        public Criteria andWatchNumIsNotNull() {
            addCriterion("watch_num is not null");
            return (Criteria) this;
        }

        public Criteria andWatchNumEqualTo(Integer value) {
            addCriterion("watch_num =", value, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumNotEqualTo(Integer value) {
            addCriterion("watch_num <>", value, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumGreaterThan(Integer value) {
            addCriterion("watch_num >", value, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("watch_num >=", value, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumLessThan(Integer value) {
            addCriterion("watch_num <", value, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumLessThanOrEqualTo(Integer value) {
            addCriterion("watch_num <=", value, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumIn(List<Integer> values) {
            addCriterion("watch_num in", values, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumNotIn(List<Integer> values) {
            addCriterion("watch_num not in", values, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumBetween(Integer value1, Integer value2) {
            addCriterion("watch_num between", value1, value2, "watchNum");
            return (Criteria) this;
        }

        public Criteria andWatchNumNotBetween(Integer value1, Integer value2) {
            addCriterion("watch_num not between", value1, value2, "watchNum");
            return (Criteria) this;
        }

        public Criteria andClickNumIsNull() {
            addCriterion("click_num is null");
            return (Criteria) this;
        }

        public Criteria andClickNumIsNotNull() {
            addCriterion("click_num is not null");
            return (Criteria) this;
        }

        public Criteria andClickNumEqualTo(Long value) {
            addCriterion("click_num =", value, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumNotEqualTo(Long value) {
            addCriterion("click_num <>", value, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumGreaterThan(Long value) {
            addCriterion("click_num >", value, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumGreaterThanOrEqualTo(Long value) {
            addCriterion("click_num >=", value, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumLessThan(Long value) {
            addCriterion("click_num <", value, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumLessThanOrEqualTo(Long value) {
            addCriterion("click_num <=", value, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumIn(List<Long> values) {
            addCriterion("click_num in", values, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumNotIn(List<Long> values) {
            addCriterion("click_num not in", values, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumBetween(Long value1, Long value2) {
            addCriterion("click_num between", value1, value2, "clickNum");
            return (Criteria) this;
        }

        public Criteria andClickNumNotBetween(Long value1, Long value2) {
            addCriterion("click_num not between", value1, value2, "clickNum");
            return (Criteria) this;
        }

        public Criteria andTravelTypeIsNull() {
            addCriterion("travel_type is null");
            return (Criteria) this;
        }

        public Criteria andTravelTypeIsNotNull() {
            addCriterion("travel_type is not null");
            return (Criteria) this;
        }

        public Criteria andTravelTypeEqualTo(Byte value) {
            addCriterion("travel_type =", value, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeNotEqualTo(Byte value) {
            addCriterion("travel_type <>", value, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeGreaterThan(Byte value) {
            addCriterion("travel_type >", value, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("travel_type >=", value, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeLessThan(Byte value) {
            addCriterion("travel_type <", value, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeLessThanOrEqualTo(Byte value) {
            addCriterion("travel_type <=", value, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeIn(List<Byte> values) {
            addCriterion("travel_type in", values, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeNotIn(List<Byte> values) {
            addCriterion("travel_type not in", values, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeBetween(Byte value1, Byte value2) {
            addCriterion("travel_type between", value1, value2, "travelType");
            return (Criteria) this;
        }

        public Criteria andTravelTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("travel_type not between", value1, value2, "travelType");
            return (Criteria) this;
        }

        public Criteria andStarRatedIsNull() {
            addCriterion("star_rated is null");
            return (Criteria) this;
        }

        public Criteria andStarRatedIsNotNull() {
            addCriterion("star_rated is not null");
            return (Criteria) this;
        }

        public Criteria andStarRatedEqualTo(String value) {
            addCriterion("star_rated =", value, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedNotEqualTo(String value) {
            addCriterion("star_rated <>", value, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedGreaterThan(String value) {
            addCriterion("star_rated >", value, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedGreaterThanOrEqualTo(String value) {
            addCriterion("star_rated >=", value, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedLessThan(String value) {
            addCriterion("star_rated <", value, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedLessThanOrEqualTo(String value) {
            addCriterion("star_rated <=", value, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedLike(String value) {
            addCriterion("star_rated like", value, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedNotLike(String value) {
            addCriterion("star_rated not like", value, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedIn(List<String> values) {
            addCriterion("star_rated in", values, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedNotIn(List<String> values) {
            addCriterion("star_rated not in", values, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedBetween(String value1, String value2) {
            addCriterion("star_rated between", value1, value2, "starRated");
            return (Criteria) this;
        }

        public Criteria andStarRatedNotBetween(String value1, String value2) {
            addCriterion("star_rated not between", value1, value2, "starRated");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIsNull() {
            addCriterion("check_time is null");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIsNotNull() {
            addCriterion("check_time is not null");
            return (Criteria) this;
        }

        public Criteria andCheckTimeEqualTo(String value) {
            addCriterion("check_time =", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotEqualTo(String value) {
            addCriterion("check_time <>", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeGreaterThan(String value) {
            addCriterion("check_time >", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeGreaterThanOrEqualTo(String value) {
            addCriterion("check_time >=", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeLessThan(String value) {
            addCriterion("check_time <", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeLessThanOrEqualTo(String value) {
            addCriterion("check_time <=", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeLike(String value) {
            addCriterion("check_time like", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotLike(String value) {
            addCriterion("check_time not like", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIn(List<String> values) {
            addCriterion("check_time in", values, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotIn(List<String> values) {
            addCriterion("check_time not in", values, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeBetween(String value1, String value2) {
            addCriterion("check_time between", value1, value2, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotBetween(String value1, String value2) {
            addCriterion("check_time not between", value1, value2, "checkTime");
            return (Criteria) this;
        }

        public Criteria andFoodServiceIsNull() {
            addCriterion("food_service is null");
            return (Criteria) this;
        }

        public Criteria andFoodServiceIsNotNull() {
            addCriterion("food_service is not null");
            return (Criteria) this;
        }

        public Criteria andFoodServiceEqualTo(String value) {
            addCriterion("food_service =", value, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceNotEqualTo(String value) {
            addCriterion("food_service <>", value, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceGreaterThan(String value) {
            addCriterion("food_service >", value, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceGreaterThanOrEqualTo(String value) {
            addCriterion("food_service >=", value, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceLessThan(String value) {
            addCriterion("food_service <", value, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceLessThanOrEqualTo(String value) {
            addCriterion("food_service <=", value, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceLike(String value) {
            addCriterion("food_service like", value, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceNotLike(String value) {
            addCriterion("food_service not like", value, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceIn(List<String> values) {
            addCriterion("food_service in", values, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceNotIn(List<String> values) {
            addCriterion("food_service not in", values, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceBetween(String value1, String value2) {
            addCriterion("food_service between", value1, value2, "foodService");
            return (Criteria) this;
        }

        public Criteria andFoodServiceNotBetween(String value1, String value2) {
            addCriterion("food_service not between", value1, value2, "foodService");
            return (Criteria) this;
        }

        public Criteria andScenicSpotIsNull() {
            addCriterion("scenic_spot is null");
            return (Criteria) this;
        }

        public Criteria andScenicSpotIsNotNull() {
            addCriterion("scenic_spot is not null");
            return (Criteria) this;
        }

        public Criteria andScenicSpotEqualTo(String value) {
            addCriterion("scenic_spot =", value, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotNotEqualTo(String value) {
            addCriterion("scenic_spot <>", value, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotGreaterThan(String value) {
            addCriterion("scenic_spot >", value, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotGreaterThanOrEqualTo(String value) {
            addCriterion("scenic_spot >=", value, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotLessThan(String value) {
            addCriterion("scenic_spot <", value, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotLessThanOrEqualTo(String value) {
            addCriterion("scenic_spot <=", value, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotLike(String value) {
            addCriterion("scenic_spot like", value, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotNotLike(String value) {
            addCriterion("scenic_spot not like", value, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotIn(List<String> values) {
            addCriterion("scenic_spot in", values, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotNotIn(List<String> values) {
            addCriterion("scenic_spot not in", values, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotBetween(String value1, String value2) {
            addCriterion("scenic_spot between", value1, value2, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andScenicSpotNotBetween(String value1, String value2) {
            addCriterion("scenic_spot not between", value1, value2, "scenicSpot");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationIsNull() {
            addCriterion("traffic_information is null");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationIsNotNull() {
            addCriterion("traffic_information is not null");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationEqualTo(String value) {
            addCriterion("traffic_information =", value, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationNotEqualTo(String value) {
            addCriterion("traffic_information <>", value, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationGreaterThan(String value) {
            addCriterion("traffic_information >", value, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationGreaterThanOrEqualTo(String value) {
            addCriterion("traffic_information >=", value, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationLessThan(String value) {
            addCriterion("traffic_information <", value, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationLessThanOrEqualTo(String value) {
            addCriterion("traffic_information <=", value, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationLike(String value) {
            addCriterion("traffic_information like", value, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationNotLike(String value) {
            addCriterion("traffic_information not like", value, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationIn(List<String> values) {
            addCriterion("traffic_information in", values, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationNotIn(List<String> values) {
            addCriterion("traffic_information not in", values, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationBetween(String value1, String value2) {
            addCriterion("traffic_information between", value1, value2, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andTrafficInformationNotBetween(String value1, String value2) {
            addCriterion("traffic_information not between", value1, value2, "trafficInformation");
            return (Criteria) this;
        }

        public Criteria andParkIsNull() {
            addCriterion("park is null");
            return (Criteria) this;
        }

        public Criteria andParkIsNotNull() {
            addCriterion("park is not null");
            return (Criteria) this;
        }

        public Criteria andParkEqualTo(String value) {
            addCriterion("park =", value, "park");
            return (Criteria) this;
        }

        public Criteria andParkNotEqualTo(String value) {
            addCriterion("park <>", value, "park");
            return (Criteria) this;
        }

        public Criteria andParkGreaterThan(String value) {
            addCriterion("park >", value, "park");
            return (Criteria) this;
        }

        public Criteria andParkGreaterThanOrEqualTo(String value) {
            addCriterion("park >=", value, "park");
            return (Criteria) this;
        }

        public Criteria andParkLessThan(String value) {
            addCriterion("park <", value, "park");
            return (Criteria) this;
        }

        public Criteria andParkLessThanOrEqualTo(String value) {
            addCriterion("park <=", value, "park");
            return (Criteria) this;
        }

        public Criteria andParkLike(String value) {
            addCriterion("park like", value, "park");
            return (Criteria) this;
        }

        public Criteria andParkNotLike(String value) {
            addCriterion("park not like", value, "park");
            return (Criteria) this;
        }

        public Criteria andParkIn(List<String> values) {
            addCriterion("park in", values, "park");
            return (Criteria) this;
        }

        public Criteria andParkNotIn(List<String> values) {
            addCriterion("park not in", values, "park");
            return (Criteria) this;
        }

        public Criteria andParkBetween(String value1, String value2) {
            addCriterion("park between", value1, value2, "park");
            return (Criteria) this;
        }

        public Criteria andParkNotBetween(String value1, String value2) {
            addCriterion("park not between", value1, value2, "park");
            return (Criteria) this;
        }

        public Criteria andOtherIsNull() {
            addCriterion("other is null");
            return (Criteria) this;
        }

        public Criteria andOtherIsNotNull() {
            addCriterion("other is not null");
            return (Criteria) this;
        }

        public Criteria andOtherEqualTo(String value) {
            addCriterion("other =", value, "other");
            return (Criteria) this;
        }

        public Criteria andOtherNotEqualTo(String value) {
            addCriterion("other <>", value, "other");
            return (Criteria) this;
        }

        public Criteria andOtherGreaterThan(String value) {
            addCriterion("other >", value, "other");
            return (Criteria) this;
        }

        public Criteria andOtherGreaterThanOrEqualTo(String value) {
            addCriterion("other >=", value, "other");
            return (Criteria) this;
        }

        public Criteria andOtherLessThan(String value) {
            addCriterion("other <", value, "other");
            return (Criteria) this;
        }

        public Criteria andOtherLessThanOrEqualTo(String value) {
            addCriterion("other <=", value, "other");
            return (Criteria) this;
        }

        public Criteria andOtherLike(String value) {
            addCriterion("other like", value, "other");
            return (Criteria) this;
        }

        public Criteria andOtherNotLike(String value) {
            addCriterion("other not like", value, "other");
            return (Criteria) this;
        }

        public Criteria andOtherIn(List<String> values) {
            addCriterion("other in", values, "other");
            return (Criteria) this;
        }

        public Criteria andOtherNotIn(List<String> values) {
            addCriterion("other not in", values, "other");
            return (Criteria) this;
        }

        public Criteria andOtherBetween(String value1, String value2) {
            addCriterion("other between", value1, value2, "other");
            return (Criteria) this;
        }

        public Criteria andOtherNotBetween(String value1, String value2) {
            addCriterion("other not between", value1, value2, "other");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductIsNull() {
            addCriterion("is_activity_product is null");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductIsNotNull() {
            addCriterion("is_activity_product is not null");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductEqualTo(Boolean value) {
            addCriterion("is_activity_product =", value, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductNotEqualTo(Boolean value) {
            addCriterion("is_activity_product <>", value, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductGreaterThan(Boolean value) {
            addCriterion("is_activity_product >", value, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_activity_product >=", value, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductLessThan(Boolean value) {
            addCriterion("is_activity_product <", value, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductLessThanOrEqualTo(Boolean value) {
            addCriterion("is_activity_product <=", value, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductIn(List<Boolean> values) {
            addCriterion("is_activity_product in", values, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductNotIn(List<Boolean> values) {
            addCriterion("is_activity_product not in", values, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductBetween(Boolean value1, Boolean value2) {
            addCriterion("is_activity_product between", value1, value2, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andIsActivityProductNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_activity_product not between", value1, value2, "isActivityProduct");
            return (Criteria) this;
        }

        public Criteria andSkuAttrIsNull() {
            addCriterion("sku_attr is null");
            return (Criteria) this;
        }

        public Criteria andSkuAttrIsNotNull() {
            addCriterion("sku_attr is not null");
            return (Criteria) this;
        }

        public Criteria andSkuAttrEqualTo(String value) {
            addCriterion("sku_attr =", value, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrNotEqualTo(String value) {
            addCriterion("sku_attr <>", value, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrGreaterThan(String value) {
            addCriterion("sku_attr >", value, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrGreaterThanOrEqualTo(String value) {
            addCriterion("sku_attr >=", value, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrLessThan(String value) {
            addCriterion("sku_attr <", value, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrLessThanOrEqualTo(String value) {
            addCriterion("sku_attr <=", value, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrLike(String value) {
            addCriterion("sku_attr like", value, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrNotLike(String value) {
            addCriterion("sku_attr not like", value, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrIn(List<String> values) {
            addCriterion("sku_attr in", values, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrNotIn(List<String> values) {
            addCriterion("sku_attr not in", values, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrBetween(String value1, String value2) {
            addCriterion("sku_attr between", value1, value2, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andSkuAttrNotBetween(String value1, String value2) {
            addCriterion("sku_attr not between", value1, value2, "skuAttr");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdIsNull() {
            addCriterion("product_common_category_id is null");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdIsNotNull() {
            addCriterion("product_common_category_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdEqualTo(String value) {
            addCriterion("product_common_category_id =", value, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdNotEqualTo(String value) {
            addCriterion("product_common_category_id <>", value, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdGreaterThan(String value) {
            addCriterion("product_common_category_id >", value, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdGreaterThanOrEqualTo(String value) {
            addCriterion("product_common_category_id >=", value, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdLessThan(String value) {
            addCriterion("product_common_category_id <", value, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdLessThanOrEqualTo(String value) {
            addCriterion("product_common_category_id <=", value, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdLike(String value) {
            addCriterion("product_common_category_id like", value, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdNotLike(String value) {
            addCriterion("product_common_category_id not like", value, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdIn(List<String> values) {
            addCriterion("product_common_category_id in", values, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdNotIn(List<String> values) {
            addCriterion("product_common_category_id not in", values, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdBetween(String value1, String value2) {
            addCriterion("product_common_category_id between", value1, value2, "productCommonCategoryId");
            return (Criteria) this;
        }

        public Criteria andProductCommonCategoryIdNotBetween(String value1, String value2) {
            addCriterion("product_common_category_id not between", value1, value2, "productCommonCategoryId");
            return (Criteria) this;
        }
        public Criteria andDiscountStatusEqualTo(Integer value) {
            addCriterion("discount_status =", value, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusNotEqualTo(Integer value) {
            addCriterion("discount_status <>", value, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusGreaterThan(Integer value) {
            addCriterion("discount_status >", value, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("discount_status >=", value, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusLessThan(Integer value) {
            addCriterion("discount_status <", value, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusLessThanOrEqualTo(Integer value) {
            addCriterion("discount_status <=", value, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusIn(List<Integer> values) {
            addCriterion("discount_status in", values, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusNotIn(List<Integer> values) {
            addCriterion("discount_status not in", values, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusBetween(Integer value1, Integer value2) {
            addCriterion("discount_status between", value1, value2, "discountStatus");
            return (Criteria) this;
        }

        public Criteria andDiscountStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("discount_status not between", value1, value2, "discountStatus");
            return (Criteria) this;
        }
        public Criteria andTagsIsNull() {
            addCriterion("tags is null");
            return (Criteria) this;
        }

        public Criteria andTagsIsNotNull() {
            addCriterion("tags is not null");
            return (Criteria) this;
        }

        public Criteria andTagsEqualTo(String value) {
            addCriterion("tags =", value, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsNotEqualTo(String value) {
            addCriterion("tags <>", value, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsGreaterThan(String value) {
            addCriterion("tags >", value, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsGreaterThanOrEqualTo(String value) {
            addCriterion("tags >=", value, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsLessThan(String value) {
            addCriterion("tags <", value, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsLessThanOrEqualTo(String value) {
            addCriterion("tags <=", value, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsLike(String value) {
            addCriterion("tags like", value, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsNotLike(String value) {
            addCriterion("tags not like", value, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsIn(List<String> values) {
            addCriterion("tags in", values, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsNotIn(List<String> values) {
            addCriterion("tags not in", values, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsBetween(String value1, String value2) {
            addCriterion("tags between", value1, value2, "tags");
            return (Criteria) this;
        }

        public Criteria andTagsNotBetween(String value1, String value2) {
            addCriterion("tags not between", value1, value2, "tags");
            return (Criteria) this;
        }

        public Criteria andNarrowImageEqualTo(String value) {
            addCriterion("narrow_image =", value, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageNotEqualTo(String value) {
            addCriterion("narrow_image <>", value, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageGreaterThan(String value) {
            addCriterion("narrow_image >", value, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageGreaterThanOrEqualTo(String value) {
            addCriterion("narrow_image >=", value, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageLessThan(String value) {
            addCriterion("narrow_image <", value, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageLessThanOrEqualTo(String value) {
            addCriterion("narrow_image <=", value, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageLike(String value) {
            addCriterion("narrow_image like", value, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageNotLike(String value) {
            addCriterion("narrow_image not like", value, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageIn(List<String> values) {
            addCriterion("narrow_image in", values, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageNotIn(List<String> values) {
            addCriterion("narrow_image not in", values, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageBetween(String value1, String value2) {
            addCriterion("narrow_image between", value1, value2, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andNarrowImageNotBetween(String value1, String value2) {
            addCriterion("narrow_image not between", value1, value2, "narrowImage");
            return (Criteria) this;
        }

        public Criteria andPayTimeEqualTo(Long value) {
            addCriterion("pay_time =", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotEqualTo(Long value) {
            addCriterion("pay_time <>", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThan(Long value) {
            addCriterion("pay_time >", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("pay_time >=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThan(Long value) {
            addCriterion("pay_time <", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThanOrEqualTo(Long value) {
            addCriterion("pay_time <=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIn(List<Long> values) {
            addCriterion("pay_time in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotIn(List<Long> values) {
            addCriterion("pay_time not in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeBetween(Long value1, Long value2) {
            addCriterion("pay_time between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotBetween(Long value1, Long value2) {
            addCriterion("pay_time not between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIsNull() {
            addCriterion("price_ids is null");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIsNotNull() {
            addCriterion("price_ids is not null");
            return (Criteria) this;
        }

        public Criteria andPriceIdsEqualTo(String value) {
            addCriterion("price_ids =", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotEqualTo(String value) {
            addCriterion("price_ids <>", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsGreaterThan(String value) {
            addCriterion("price_ids >", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsGreaterThanOrEqualTo(String value) {
            addCriterion("price_ids >=", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLessThan(String value) {
            addCriterion("price_ids <", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLessThanOrEqualTo(String value) {
            addCriterion("price_ids <=", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsLike(String value) {
            addCriterion("price_ids like", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotLike(String value) {
            addCriterion("price_ids not like", value, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsIn(List<String> values) {
            addCriterion("price_ids in", values, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotIn(List<String> values) {
            addCriterion("price_ids not in", values, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsBetween(String value1, String value2) {
            addCriterion("price_ids between", value1, value2, "priceIds");
            return (Criteria) this;
        }

        public Criteria andPriceIdsNotBetween(String value1, String value2) {
            addCriterion("price_ids not between", value1, value2, "priceIds");
            return (Criteria) this;
        }

        public Criteria andTripDateIsNull() {
            addCriterion("trip_date is null");
            return (Criteria) this;
        }

        public Criteria andTripDateIsNotNull() {
            addCriterion("trip_date is not null");
            return (Criteria) this;
        }

        public Criteria andTripDateEqualTo(String value) {
            addCriterion("trip_date =", value, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateNotEqualTo(String value) {
            addCriterion("trip_date <>", value, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateGreaterThan(String value) {
            addCriterion("trip_date >", value, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateGreaterThanOrEqualTo(String value) {
            addCriterion("trip_date >=", value, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateLessThan(String value) {
            addCriterion("trip_date <", value, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateLessThanOrEqualTo(String value) {
            addCriterion("trip_date <=", value, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateLike(String value) {
            addCriterion("trip_date like", value, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateNotLike(String value) {
            addCriterion("trip_date not like", value, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateIn(List<String> values) {
            addCriterion("trip_date in", values, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateNotIn(List<String> values) {
            addCriterion("trip_date not in", values, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateBetween(String value1, String value2) {
            addCriterion("trip_date between", value1, value2, "tripDate");
            return (Criteria) this;
        }

        public Criteria andTripDateNotBetween(String value1, String value2) {
            addCriterion("trip_date not between", value1, value2, "tripDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}