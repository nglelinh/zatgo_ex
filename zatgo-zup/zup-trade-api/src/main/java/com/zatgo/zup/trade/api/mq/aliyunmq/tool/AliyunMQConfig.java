package com.zatgo.zup.trade.api.mq.aliyunmq.tool;//package com.ykb.mall.mapi.mq.aliyunmq.tool;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
///**
// *
// * @Title
// * @author lincm
// * @date 2018年5月8日 下午3:24:41
// */
//@Component
//public class AliyunMQConfig {
//
//    @Value("${mq.aliyunmq.url:}")
//    private String url;
//
//    //身份验证码
//    @Value("${mq.aliyunmq.accessKey:}")
//    private String accessKey;
//
//    //身份验证密钥
//    @Value("${mq.aliyunmq.secretKey:}")
//    private String secretKey;
//
//	//顺序消息消费失败进行重试前的等待时间 单位(毫秒)
//    @Value("${mq.aliyunmq.suspendTimeMillis:}")
//    private String suspendTimeMillis;
//
//    //消息消费失败时的最大重试次数
//    @Value("${mq.aliyunmq.maxReconsumeTimes:}")
//    private String maxReconsumeTimes;
//
//    //consumer 消费线程数
//    @Value("${mq.aliyunmq.consumer.thread.num:}")
//    private String consumerThreadNum;
//
//    public String getConsumerThreadNum() {
//        return consumerThreadNum;
//    }
//
//    public void setConsumerThreadNum(String consumerThreadNum) {
//        this.consumerThreadNum = consumerThreadNum;
//    }
//
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
//    public String getAccessKey() {
//        return accessKey;
//    }
//
//    public void setAccessKey(String accessKey) {
//        this.accessKey = accessKey;
//    }
//
//    public String getSecretKey() {
//        return secretKey;
//    }
//
//    public void setSecretKey(String secretKey) {
//        this.secretKey = secretKey;
//    }
//
//    public String getSuspendTimeMillis() {
//        return suspendTimeMillis;
//    }
//
//    public void setSuspendTimeMillis(String suspendTimeMillis) {
//        this.suspendTimeMillis = suspendTimeMillis;
//    }
//
//    public String getMaxReconsumeTimes() {
//        return maxReconsumeTimes;
//    }
//
//    public void setMaxReconsumeTimes(String maxReconsumeTimes) {
//        this.maxReconsumeTimes = maxReconsumeTimes;
//    }
//
//}
