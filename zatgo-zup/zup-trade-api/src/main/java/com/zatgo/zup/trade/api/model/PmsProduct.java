package com.zatgo.zup.trade.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PmsProduct implements Serializable {
    private Long id;

    private Long brandId;

    private Long productCategoryId;

    private Long feightTemplateId;

    private Long productAttributeCategoryId;

    /**
     * 业务类型（1-旅游；2-机票；3-酒店；4-文章）
     *
     * @mbggenerated
     */
    private Byte businessType;

    /**
     * 模块编号（用于前端产品按模块分类，编号使用英文逗号,分隔）
     *
     * @mbggenerated
     */
    private String moduleId;

    /**
     * 商品名称
     *
     * @mbggenerated
     */
    private String name;

    private String pic;

    /**
     * 货号
     *
     * @mbggenerated
     */
    private String productSn;

    /**
     * 删除状态：0->未删除；1->已删除
     *
     * @mbggenerated
     */
    private Integer deleteStatus;

    /**
     * 上架状态：0->下架；1->上架
     *
     * @mbggenerated
     */
    private Integer publishStatus;

    /**
     * 新品状态:0->不是新品；1->新品
     *
     * @mbggenerated
     */
    private Integer newStatus;

    /**
     * 推荐状态；0->不推荐；1->推荐
     *
     * @mbggenerated
     */
    private Integer recommandStatus;

    /**
     * 审核状态：0->未审核；1->审核通过
     *
     * @mbggenerated
     */
    private Integer verifyStatus;

    /**
     * 排序
     *
     * @mbggenerated
     */
    private Integer sort;

    /**
     * 销量
     *
     * @mbggenerated
     */
    private Integer sale;

    private BigDecimal price;

    /**
     * 促销价格
     *
     * @mbggenerated
     */
    private BigDecimal promotionPrice;

    /**
     * 赠送的成长值
     *
     * @mbggenerated
     */
    private Integer giftGrowth;

    /**
     * 赠送的积分
     *
     * @mbggenerated
     */
    private Integer giftPoint;

    /**
     * 限制使用的积分数
     *
     * @mbggenerated
     */
    private Integer usePointLimit;

    /**
     * 副标题
     *
     * @mbggenerated
     */
    private String subTitle;

    /**
     * 市场价
     *
     * @mbggenerated
     */
    private BigDecimal originalPrice;

    /**
     * 库存
     *
     * @mbggenerated
     */
    private Integer stock;

    /**
     * 库存预警值
     *
     * @mbggenerated
     */
    private Integer lowStock;

    /**
     * 单位
     *
     * @mbggenerated
     */
    private String unit;

    /**
     * 商品重量，默认为克
     *
     * @mbggenerated
     */
    private BigDecimal weight;

    /**
     * 是否为预告商品：0->不是；1->是
     *
     * @mbggenerated
     */
    private Integer previewStatus;

    /**
     * 以逗号分割的产品服务：1->无忧退货；2->快速退款；3->免费包邮
     *
     * @mbggenerated
     */
    private String serviceIds;

    private String keywords;

    private String note;

    /**
     * 画册图片，连产品图片限制为5张，以逗号分割
     *
     * @mbggenerated
     */
    private String albumPics;

    private String detailTitle;

    /**
     * 促销开始时间
     *
     * @mbggenerated
     */
    private Date promotionStartTime;

    /**
     * 促销结束时间
     *
     * @mbggenerated
     */
    private Date promotionEndTime;

    /**
     * 活动限购数量
     *
     * @mbggenerated
     */
    private Integer promotionPerLimit;

    /**
     * 促销类型：0->没有促销使用原价;1->使用促销价；2->使用会员价；3->使用阶梯价格；4->使用满减价格；5->限时购
     *
     * @mbggenerated
     */
    private Integer promotionType;

    /**
     * 品牌名称
     *
     * @mbggenerated
     */
    private String brandName;

    /**
     * 商品分类名称
     *
     * @mbggenerated
     */
    private String productCategoryName;

    /**
     * 儿童价
     *
     * @mbggenerated
     */
    private BigDecimal childPrice;

    /**
     * 持续天/夜（天与夜之间用英文逗号,分隔）
     *
     * @mbggenerated
     */
    private String dayNight;

    /**
     * 点赞数
     *
     * @mbggenerated
     */
    private Integer likeNum;

    /**
     * 观看数（后台录入，起始值）
     *
     * @mbggenerated
     */
    private Integer watchNum;

    /**
     * 点击量（用户实际观看值）
     *
     * @mbggenerated
     */
    private Long clickNum;

    /**
     * 旅游产品类型:1-自由行，2-跟团游，3-周边游
     *
     * @mbggenerated
     */
    private Byte travelType;

    /**
     * 酒店星级
     *
     * @mbggenerated
     */
    private String starRated;

    /**
     * 酒店地址
     *
     * @mbggenerated
     */
    private String address;

    /**
     * 入离时间
     *
     * @mbggenerated
     */
    private String checkTime;

    /**
     * 餐饮服务
     *
     * @mbggenerated
     */
    private String foodService;

    /**
     * 附近景点
     *
     * @mbggenerated
     */
    private String scenicSpot;

    /**
     * 交通信息
     *
     * @mbggenerated
     */
    private String trafficInformation;

    /**
     * 停车信息
     *
     * @mbggenerated
     */
    private String park;

    /**
     * 其他
     *
     * @mbggenerated
     */
    private String other;

    /**
     * 是否为活动商品
     *
     * @mbggenerated
     */
    private Boolean isActivityProduct;

    /**
     * 商品描述
     *
     * @mbggenerated
     */
    private String description;

    private String detailDesc;

    /**
     * 产品详情网页内容
     *
     * @mbggenerated
     */
    private String detailHtml;

    /**
     * 移动端网页详情
     *
     * @mbggenerated
     */
    private String detailMobileHtml;

    /**
     * 费用说明
     *
     * @mbggenerated
     */
    private String chargeDetail;

    /**
     * 预订须知
     *
     * @mbggenerated
     */
    private String bookTips;

    /**
     * 舱位
     *
     * @mbggenerated
     */
    private String shippingSpace;

    /**
     * 行李限额
     *
     * @mbggenerated
     */
    private String baggageAllowance;

    /**
     * 房型
     *
     * @mbggenerated
     */
    private String roomType;

    /**
     * 地理位置
     *
     * @mbggenerated
     */
    private String geoPosition;

    private String skuAttr;

    private String productCommonCategoryId;

    private Integer discountStatus;

    private String tags;

    private String narrowImage;

    private Long payTime;

    private String priceIds;

    private String tripDate;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Long getFeightTemplateId() {
        return feightTemplateId;
    }

    public void setFeightTemplateId(Long feightTemplateId) {
        this.feightTemplateId = feightTemplateId;
    }

    public Long getProductAttributeCategoryId() {
        return productAttributeCategoryId;
    }

    public void setProductAttributeCategoryId(Long productAttributeCategoryId) {
        this.productAttributeCategoryId = productAttributeCategoryId;
    }

    public Byte getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Byte businessType) {
        this.businessType = businessType;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public Integer getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Integer deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public Integer getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(Integer publishStatus) {
        this.publishStatus = publishStatus;
    }

    public Integer getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(Integer newStatus) {
        this.newStatus = newStatus;
    }

    public Integer getRecommandStatus() {
        return recommandStatus;
    }

    public void setRecommandStatus(Integer recommandStatus) {
        this.recommandStatus = recommandStatus;
    }

    public Integer getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(Integer verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSale() {
        return sale;
    }

    public void setSale(Integer sale) {
        this.sale = sale;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Integer getGiftGrowth() {
        return giftGrowth;
    }

    public void setGiftGrowth(Integer giftGrowth) {
        this.giftGrowth = giftGrowth;
    }

    public Integer getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(Integer giftPoint) {
        this.giftPoint = giftPoint;
    }

    public Integer getUsePointLimit() {
        return usePointLimit;
    }

    public void setUsePointLimit(Integer usePointLimit) {
        this.usePointLimit = usePointLimit;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getLowStock() {
        return lowStock;
    }

    public void setLowStock(Integer lowStock) {
        this.lowStock = lowStock;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Integer getPreviewStatus() {
        return previewStatus;
    }

    public void setPreviewStatus(Integer previewStatus) {
        this.previewStatus = previewStatus;
    }

    public String getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(String serviceIds) {
        this.serviceIds = serviceIds;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAlbumPics() {
        return albumPics;
    }

    public void setAlbumPics(String albumPics) {
        this.albumPics = albumPics;
    }

    public String getDetailTitle() {
        return detailTitle;
    }

    public void setDetailTitle(String detailTitle) {
        this.detailTitle = detailTitle;
    }

    public Date getPromotionStartTime() {
        return promotionStartTime;
    }

    public void setPromotionStartTime(Date promotionStartTime) {
        this.promotionStartTime = promotionStartTime;
    }

    public Date getPromotionEndTime() {
        return promotionEndTime;
    }

    public void setPromotionEndTime(Date promotionEndTime) {
        this.promotionEndTime = promotionEndTime;
    }

    public Integer getPromotionPerLimit() {
        return promotionPerLimit;
    }

    public void setPromotionPerLimit(Integer promotionPerLimit) {
        this.promotionPerLimit = promotionPerLimit;
    }

    public Integer getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Integer promotionType) {
        this.promotionType = promotionType;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public BigDecimal getChildPrice() {
        return childPrice;
    }

    public void setChildPrice(BigDecimal childPrice) {
        this.childPrice = childPrice;
    }

    public String getDayNight() {
        return dayNight;
    }

    public void setDayNight(String dayNight) {
        this.dayNight = dayNight;
    }

    public Integer getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(Integer likeNum) {
        this.likeNum = likeNum;
    }

    public Integer getWatchNum() {
        return watchNum;
    }

    public void setWatchNum(Integer watchNum) {
        this.watchNum = watchNum;
    }

    public Long getClickNum() {
        return clickNum;
    }

    public void setClickNum(Long clickNum) {
        this.clickNum = clickNum;
    }

    public Byte getTravelType() {
        return travelType;
    }

    public void setTravelType(Byte travelType) {
        this.travelType = travelType;
    }

    public String getStarRated() {
        return starRated;
    }

    public void setStarRated(String starRated) {
        this.starRated = starRated;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(String checkTime) {
        this.checkTime = checkTime;
    }

    public String getFoodService() {
        return foodService;
    }

    public void setFoodService(String foodService) {
        this.foodService = foodService;
    }

    public String getScenicSpot() {
        return scenicSpot;
    }

    public void setScenicSpot(String scenicSpot) {
        this.scenicSpot = scenicSpot;
    }

    public String getTrafficInformation() {
        return trafficInformation;
    }

    public void setTrafficInformation(String trafficInformation) {
        this.trafficInformation = trafficInformation;
    }

    public String getPark() {
        return park;
    }

    public void setPark(String park) {
        this.park = park;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Boolean getIsActivityProduct() {
        return isActivityProduct;
    }

    public void setIsActivityProduct(Boolean isActivityProduct) {
        this.isActivityProduct = isActivityProduct;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetailDesc() {
        return detailDesc;
    }

    public void setDetailDesc(String detailDesc) {
        this.detailDesc = detailDesc;
    }

    public String getDetailHtml() {
        return detailHtml;
    }

    public void setDetailHtml(String detailHtml) {
        this.detailHtml = detailHtml;
    }

    public String getDetailMobileHtml() {
        return detailMobileHtml;
    }

    public void setDetailMobileHtml(String detailMobileHtml) {
        this.detailMobileHtml = detailMobileHtml;
    }

    public String getChargeDetail() {
        return chargeDetail;
    }

    public void setChargeDetail(String chargeDetail) {
        this.chargeDetail = chargeDetail;
    }

    public String getBookTips() {
        return bookTips;
    }

    public void setBookTips(String bookTips) {
        this.bookTips = bookTips;
    }

    public String getShippingSpace() {
        return shippingSpace;
    }

    public void setShippingSpace(String shippingSpace) {
        this.shippingSpace = shippingSpace;
    }

    public String getBaggageAllowance() {
        return baggageAllowance;
    }

    public void setBaggageAllowance(String baggageAllowance) {
        this.baggageAllowance = baggageAllowance;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(String geoPosition) {
        this.geoPosition = geoPosition;
    }

    public String getSkuAttr() {
        return skuAttr;
    }

    public void setSkuAttr(String skuAttr) {
        this.skuAttr = skuAttr;
    }

    public String getProductCommonCategoryId() {
        return productCommonCategoryId;
    }

    public void setProductCommonCategoryId(String productCommonCategoryId) {
        this.productCommonCategoryId = productCommonCategoryId;
    }

    public Integer getDiscountStatus() {
        return discountStatus;
    }

    public void setDiscountStatus(Integer discountStatus) {
        this.discountStatus = discountStatus;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getNarrowImage() {
        return narrowImage;
    }

    public void setNarrowImage(String narrowImage) {
        this.narrowImage = narrowImage;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public String getPriceIds() {
        return priceIds;
    }

    public void setPriceIds(String priceIds) {
        this.priceIds = priceIds;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", brandId=").append(brandId);
        sb.append(", productCategoryId=").append(productCategoryId);
        sb.append(", feightTemplateId=").append(feightTemplateId);
        sb.append(", productAttributeCategoryId=").append(productAttributeCategoryId);
        sb.append(", businessType=").append(businessType);
        sb.append(", moduleId=").append(moduleId);
        sb.append(", name=").append(name);
        sb.append(", pic=").append(pic);
        sb.append(", productSn=").append(productSn);
        sb.append(", deleteStatus=").append(deleteStatus);
        sb.append(", publishStatus=").append(publishStatus);
        sb.append(", newStatus=").append(newStatus);
        sb.append(", recommandStatus=").append(recommandStatus);
        sb.append(", verifyStatus=").append(verifyStatus);
        sb.append(", sort=").append(sort);
        sb.append(", sale=").append(sale);
        sb.append(", price=").append(price);
        sb.append(", promotionPrice=").append(promotionPrice);
        sb.append(", giftGrowth=").append(giftGrowth);
        sb.append(", giftPoint=").append(giftPoint);
        sb.append(", usePointLimit=").append(usePointLimit);
        sb.append(", subTitle=").append(subTitle);
        sb.append(", originalPrice=").append(originalPrice);
        sb.append(", stock=").append(stock);
        sb.append(", lowStock=").append(lowStock);
        sb.append(", unit=").append(unit);
        sb.append(", weight=").append(weight);
        sb.append(", previewStatus=").append(previewStatus);
        sb.append(", serviceIds=").append(serviceIds);
        sb.append(", keywords=").append(keywords);
        sb.append(", note=").append(note);
        sb.append(", albumPics=").append(albumPics);
        sb.append(", detailTitle=").append(detailTitle);
        sb.append(", promotionStartTime=").append(promotionStartTime);
        sb.append(", promotionEndTime=").append(promotionEndTime);
        sb.append(", promotionPerLimit=").append(promotionPerLimit);
        sb.append(", promotionType=").append(promotionType);
        sb.append(", brandName=").append(brandName);
        sb.append(", productCategoryName=").append(productCategoryName);
        sb.append(", childPrice=").append(childPrice);
        sb.append(", dayNight=").append(dayNight);
        sb.append(", likeNum=").append(likeNum);
        sb.append(", watchNum=").append(watchNum);
        sb.append(", clickNum=").append(clickNum);
        sb.append(", travelType=").append(travelType);
        sb.append(", starRated=").append(starRated);
        sb.append(", address=").append(address);
        sb.append(", checkTime=").append(checkTime);
        sb.append(", foodService=").append(foodService);
        sb.append(", scenicSpot=").append(scenicSpot);
        sb.append(", trafficInformation=").append(trafficInformation);
        sb.append(", park=").append(park);
        sb.append(", other=").append(other);
        sb.append(", isActivityProduct=").append(isActivityProduct);
        sb.append(", description=").append(description);
        sb.append(", detailDesc=").append(detailDesc);
        sb.append(", detailHtml=").append(detailHtml);
        sb.append(", detailMobileHtml=").append(detailMobileHtml);
        sb.append(", chargeDetail=").append(chargeDetail);
        sb.append(", bookTips=").append(bookTips);
        sb.append(", shippingSpace=").append(shippingSpace);
        sb.append(", baggageAllowance=").append(baggageAllowance);
        sb.append(", roomType=").append(roomType);
        sb.append(", geoPosition=").append(geoPosition);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}