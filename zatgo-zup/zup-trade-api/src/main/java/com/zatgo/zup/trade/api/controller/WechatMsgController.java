package com.zatgo.zup.trade.api.controller;

import com.zatgo.zup.trade.api.domain.CommonResult;
import com.zatgo.zup.trade.api.remoteService.WechatRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trade/api/wx/msg")
public class WechatMsgController {

	@Autowired
	private WechatRemoteService wechatRemoteService;

	@GetMapping("/support/{userId}")
	public CommonResult sendWechatSupportMsg(@PathVariable String userId) {
		return wechatRemoteService.sendWechatSupportMsg(userId);
	}

	@GetMapping("/support/success/{userId}/{id}")
	public CommonResult sendWechatSupportSuccessMsg(@PathVariable Long userId, @PathVariable String id) {
		return wechatRemoteService.sendWechatSupportSuccessMsg(userId, id);
	}

}
