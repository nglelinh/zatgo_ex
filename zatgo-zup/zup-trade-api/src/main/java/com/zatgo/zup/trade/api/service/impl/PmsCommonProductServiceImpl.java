package com.zatgo.zup.trade.api.service.impl;

import com.ykb.mall.common.model.PmsCommonProductSku;
import com.ykb.mall.common.model.PmsCommonProductSkuExample;
import com.ykb.mall.common.model.PmsProduct;
import com.zatgo.zup.common.model.CommonProductInfoResponse;
import com.zatgo.zup.trade.api.mapper.PmsCommonProductSkuMapper;
import com.zatgo.zup.trade.api.mapper.PmsProductMapper;
import com.zatgo.zup.trade.api.model.TicketDateList;
import com.zatgo.zup.trade.api.model.TicketList;
import com.zatgo.zup.trade.api.model.TicketListResponse;
import com.zatgo.zup.trade.api.service.PmsCommonProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 46041 on 2019/7/17.
 */

@Service("pmsCommonProductService")
public class PmsCommonProductServiceImpl implements PmsCommonProductService {

    @Autowired
    private PmsProductMapper pmsProductMapper;
    @Autowired
    private PmsCommonProductSkuMapper pmsCommonProductSkuMapper;

    @Override
    public CommonProductInfoResponse getProductInfo(Long id) {
        PmsProduct pmsProduct = pmsProductMapper.selectByPrimaryKey(id);
        PmsCommonProductSkuExample example = new PmsCommonProductSkuExample();
        example.createCriteria().andProductIdEqualTo(id + "");
        List<PmsCommonProductSku> pmsCommonProductSkus = pmsCommonProductSkuMapper.selectByExample(example);
        CommonProductInfoResponse response = new CommonProductInfoResponse();
        response.setList(pmsCommonProductSkus);
        response.setPmsProduct(pmsProduct);
        return response;
    }

    @Override
    public TicketDateList ticketList(Long productId) {
        TicketDateList data = new TicketDateList();
        List<TicketListResponse> ticketListResponses = pmsCommonProductSkuMapper.selectTicketList(productId, new Date());
        if (!CollectionUtils.isEmpty(ticketListResponses)){
            List<TicketList> tickes = new ArrayList<>();
            for (TicketListResponse ticket : ticketListResponses) {
                BigDecimal oldPrice = data.getPrice();
                oldPrice = oldPrice == null ? BigDecimal.ZERO : oldPrice;
                BigDecimal newPrice = ticket.getPrice();
                if (newPrice.compareTo(oldPrice) == -1){
                    data.setPrice(newPrice);
                }
                TicketList ticketList = new TicketList();
                ticketList.setPrice(newPrice);
                ticketList.setSkuId(ticket.getSkuId());
                ticketList.setSpec(ticket.getSpec());
                ticketList.setStock(ticket.getStock());
                ticketList.setStartTime(ticket.getStartDate());
                data.setSkuAttr(ticket.getSkuAttr());
                data.setProductId(ticket.getProductId());
                tickes.add(ticketList);
            }
            data.setList(tickes);
        }
        return data;
    }
}
