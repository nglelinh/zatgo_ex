package com.zatgo.zup.matching.exchange.bitshop.mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.zatgo.zup.matching.exchange.bitshop.entity.Account;
import com.zatgo.zup.matching.exchange.bitshop.entity.AccountExample;

@Mapper
public interface AccountMapper{
    int countByExample(AccountExample example);

    int deleteByExample(AccountExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectOnlyOneByExample(AccountExample example);

    List<Account> selectByExample(AccountExample example);

    Account selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Account record, @Param("example") AccountExample example);

    int updateByExample(@Param("record") Account record, @Param("example") AccountExample example);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);
    
    Account selectByUidAndTypeAddLock(Account record);
    
    @Update("update account set balance = balance + (#{amount}) where uid = #{uid} and type=#{type}")
	int addBalance(@Param("uid") Integer uid, @Param("amount")BigDecimal amount,@Param("type")Integer type);
    
    @Update("update account set balance = balance - #{amount} where uid = #{uid} and type=#{type}")
	int subtractBalance(@Param("uid") Integer uid, @Param("amount")BigDecimal amount,@Param("type")Integer type);

    Map<String, BigDecimal> selectUserBalanceSumByCoin(Map<String, Object> param);

    Map<String, BigDecimal> selectOtcUserBalanceSumByCoin(Map<String, Object> param);
    /***
     *
     * 检查该用户uid是否存在
     *
     * @param uid
     * @return
     */
    int checkByUid(int uid);
    
    /**
     * 根据账户类型查询用户持币数据，为空的不查询出来
     * @param type
     * @return
     */
    List<Account> selectHoldCoinByType(Integer type);

    /**
     * Find account balance
     * @param type
     * @return
     */
    List<Account> selectBalanceByType(Integer type);

    Account selectByIdAndType(@Param("uid") Integer uid,@Param("type") Integer type);
}