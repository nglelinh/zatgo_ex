package com.zatgo.zup.matching.exchange;

import java.math.BigDecimal;
import java.util.List;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.matching.config.CoinPairConfig;
import com.zatgo.zup.matching.exception.AccountException;
import com.zatgo.zup.matching.exception.OrderException;
import com.zatgo.zup.matching.exchange.bitshop.entity.StatsSymbolRate;
import com.zatgo.zup.matching.model.DebtOrderModel;
import com.zatgo.zup.matching.model.FinancingAccountBalance;
import com.zatgo.zup.matching.model.OrderModel;
import com.zatgo.zup.matching.model.TradeModel;

public interface ExchangeService {

	/**
	 * 查询卖单
	 * @param coinPairConfig
	 * @return
	 */
	PageInfo<OrderModel> searchSellOrders(CoinPairConfig coinPairConfig);
	
	/**
	 * 查询买单
	 * @param coinPairConfig
	 * @return
	 */
	PageInfo<OrderModel> searchBuyOrders(CoinPairConfig coinPairConfig);
	
	/**
	 * 查询市价单
	 * @param coinPairConfig
	 * @return
	 */
	PageInfo<OrderModel> searchMarketOrders(CoinPairConfig coinPairConfig);
	
	/**
	 * 变更新订单进入撮合状态
	 * @param coinPairConfig
	 * @return
	 */
	Integer changeOrderStatusToNew(CoinPairConfig coinPairConfig);
	
	/**
	 * 锁订单
	 * @param orderId
	 * @param coinPairConfig
	 * @return
	 */
	OrderModel getLockOrderById(String orderId,CoinPairConfig coinPairConfig);
		
	/**
	 * 撮合成功信息
	 * @param record
	 * @param coinPairConfig
	 * @return
	 */
	int insertTrade(TradeModel record,CoinPairConfig coinPairConfig);
	
	/**
	 * 更新撮合成功的订单
	 * @param record
	 * @param coinPairConfig
	 * @return
	 */
	int updateOrderByMatchingStatusAndPrimaryKeySelective(OrderModel record,CoinPairConfig coinPairConfig);
	
	/**
	 * 计价币解冻
	 * @param order
	 * @param amount
	 * @param coinPairConfig
	 */
	void returnRemainQuoteCoinBalance(OrderModel order,BigDecimal amount,CoinPairConfig coinPairConfig)  throws AccountException,OrderException;
	
	/**
	 * 基准币解冻
	 * @param order
	 * @param amount
	 * @param coinPairConfig
	 */
	void returnRemainBaseCoinBalance(OrderModel order,BigDecimal amount,CoinPairConfig coinPairConfig)  throws AccountException,OrderException;
	
	/**
	 * 买方订单撮合修改余额
	 * @param buyOrder
	 * @param sellOrder
	 * @param baseVolume
	 * @param quoteAmount
	 */
	void buyMatchingUpdateLockBalance(OrderModel buyOrder,OrderModel sellOrder,BigDecimal baseVolume,
			BigDecimal quoteAmount,BigDecimal fee,TradeModel trade,CoinPairConfig coinPairConfig)  throws AccountException,OrderException;
	
	/**
	 * 卖方订单撮合修改余额
	 * @param buyOrder
	 * @param sellOrder
	 * @param baseVolume
	 * @param quoteAmount
	 */
	void sellMatchingUpdateLockBalance(OrderModel buyOrder,OrderModel sellOrder,BigDecimal baseVolume,
			BigDecimal quoteAmount,BigDecimal fee,TradeModel trade, CoinPairConfig coinPairConfig)  throws AccountException,OrderException;
	
	/**
	 * 根据订单锁账户
	 * @param orderData
	 * @param coinPairConfig
	 * @throws OrderException
	 */
	public void lockAccountByOrder(OrderModel orderData, CoinPairConfig coinPairConfig) throws OrderException;
	
	/**
	 * 根据币对查询汇率
	 */
	StatsSymbolRate selectOneSymbolRate(CoinPairConfig coinPairConfig);
	
	/**
	 * 查询未完成的借款订单(根据用户统计)
	 * @return
	 */
	List<DebtOrderModel> selectByUnfinishedDebtOrder(CoinPairConfig coinPairConfig);
	
	/**
	 * 触发配资账户爆仓
	 * @param userId
	 * @param baseSymbol
	 * @param quoteSymbol
	 */
	void outOfStock(String userId,CoinPairConfig coinPairConfig) throws AccountException,OrderException;
	
	/**
	 * 查询配资账户余额
	 * @param userId
	 * @param coinPairConfig
	 * @return
	 */
	FinancingAccountBalance selectFinancingAccountBalance(String userId,CoinPairConfig coinPairConfig);
	
	
}
