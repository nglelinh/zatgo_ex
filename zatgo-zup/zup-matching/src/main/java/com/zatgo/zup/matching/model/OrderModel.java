package com.zatgo.zup.matching.model;

import java.math.BigDecimal;

import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.matching.config.CoinPairConfig;
import com.zatgo.zup.matching.enumtype.MatchingEnum;
import com.zatgo.zup.matching.exchange.zatgo.entity.ExOrder;

public class OrderModel extends ExOrder{

	
	public BigDecimal getUnfilledQuantity() {
		return this.getVolume().subtract(this.getDealVolume());
	}
	
	public boolean isLimitOrder() {
		if (this.getType().equals(Integer.valueOf(ExchangeEnum.EntryOrdersType.limitPriceEntrust.getCode()).byteValue())) {
			return true;
		}
		return false;
	}

	public boolean isMarketOrder() {
		if (this.getType().equals(Integer.valueOf(ExchangeEnum.EntryOrdersType.marketPriceEntrust.getCode()).byteValue())) {
			return true;
		}
		return false;
	}
	  
	public boolean matchingPriceCheck(BigDecimal bestCounterLimitPrice) {
		if (this.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
			return getPrice().compareTo(bestCounterLimitPrice) >= 0;
		}else if(this.getSide().equals(ExchangeEnum.ExchangeSide.SELL.getCode())) {
			return getPrice().compareTo(bestCounterLimitPrice) <= 0;
		}else {
			throw new BusinessException();
		}
			
	}
	
	public void addFilledQuntity(BigDecimal tradedQuantity) {
		this.setDealVolume(this.getDealVolume().add(tradedQuantity));
	}

	public void addFilledMoney(BigDecimal tradedQuoteAmount) {
		this.setDealMoney(this.getDealMoney().add(tradedQuoteAmount));
	}
	
	public boolean isFilled(CoinPairConfig config) {
		if (isLimitOrder()) {
			return getUnfilledQuantity().compareTo(config.MIN_TRADE_VOL) < 0;
		}
		if (isMarketOrder()) {
			if (ExchangeEnum.ExchangeSide.BUY.getCode().equals(getSide())) {
				BigDecimal unfilledAmount = this.getVolume().subtract(this.getDealMoney());
				return unfilledAmount.compareTo(config.MIN_TRADE_AMOUNT) <= 0;
			}
			return getUnfilledQuantity().compareTo(config.MIN_TRADE_VOL) < 0;
		}
		return false;
	}
	
	/**
	 * 是否为配资交易订单
	 * @return
	 */
	public boolean isFinancingOrder() {
		if(getOrderType() != null && getOrderType().toString().equals(MatchingEnum.OrderType.financingTrade.getCode())) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 是否为普通的币币交易订单
	 * @return
	 */
	public boolean isGeneralOrder() {
		if(getOrderType() == null || getOrderType().toString().equals(MatchingEnum.OrderType.generalTrade.getCode())) {
			return true;
		}else {
			return false;
		}
	}

	
}
