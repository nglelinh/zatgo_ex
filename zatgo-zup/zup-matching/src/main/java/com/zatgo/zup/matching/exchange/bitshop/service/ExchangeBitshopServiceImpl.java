package com.zatgo.zup.matching.exchange.bitshop.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.matching.config.CoinPairConfig;
import com.zatgo.zup.matching.enumtype.MatchingEnum;
import com.zatgo.zup.matching.exception.AccountException;
import com.zatgo.zup.matching.exception.OrderException;
import com.zatgo.zup.matching.exchange.ExchangeService;
import com.zatgo.zup.matching.exchange.bitshop.entity.Account;
import com.zatgo.zup.matching.exchange.bitshop.entity.ExOrder;
import com.zatgo.zup.matching.exchange.bitshop.entity.ExTrade;
import com.zatgo.zup.matching.exchange.bitshop.entity.StatsSymbolRate;
import com.zatgo.zup.matching.exchange.bitshop.entity.StatsSymbolRateExample;
import com.zatgo.zup.matching.exchange.bitshop.entity.Transaction;
import com.zatgo.zup.matching.exchange.bitshop.entity.WithFundingLoan;
import com.zatgo.zup.matching.exchange.bitshop.mapper.AccountMapper;
import com.zatgo.zup.matching.exchange.bitshop.mapper.ExOrderMapper;
import com.zatgo.zup.matching.exchange.bitshop.mapper.ExTradeMapper;
import com.zatgo.zup.matching.exchange.bitshop.mapper.StatsSymbolRateMapper;
import com.zatgo.zup.matching.exchange.bitshop.mapper.TransactionMapper;
import com.zatgo.zup.matching.exchange.bitshop.mapper.WithFundingLoanMapper;
import com.zatgo.zup.matching.model.DebtOrderModel;
import com.zatgo.zup.matching.model.FinancingAccountBalance;
import com.zatgo.zup.matching.model.OrderModel;
import com.zatgo.zup.matching.model.TradeModel;

@Component("ExchangeService_bitshop")
public class ExchangeBitshopServiceImpl implements ExchangeService {

	private static final Logger logger = LoggerFactory.getLogger(ExchangeBitshopServiceImpl.class);

	@Autowired
	private ExOrderMapper exOrderMapper;
	
	@Autowired
	private ExTradeMapper exTradeMapper;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private TransactionMapper transactionMapper;
	
	@Autowired
	private StatsSymbolRateMapper statsSymbolRateMapper;
	
	@Autowired
	private WithFundingLoanMapper withFundingLoanMapper;
	
	
	
	@Override
	public PageInfo<OrderModel> searchSellOrders(CoinPairConfig coinPairConfig) {
		PageHelper.startPage(Integer.valueOf(1), Integer.valueOf(3000));
		
		Page<ExOrder> page = exOrderMapper.searchSellOrders(ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase());
		List<OrderModel> list = new ArrayList<>();
		for(ExOrder order:page) {
			OrderModel orderModel = getOrderModel(order,coinPairConfig);
			list.add(orderModel);
		}
		PageInfo<OrderModel> pageInfoOrder =
				new PageInfo<OrderModel>(page.getPageNum(),
						page.getPageSize(),page.getTotal(),list);

		PageHelper.clearPage();
		
		return pageInfoOrder;
	}

	
	
	@Override
	public PageInfo<OrderModel> searchBuyOrders(CoinPairConfig coinPairConfig) {
		PageHelper.startPage(Integer.valueOf(1), Integer.valueOf(3000));
		
		Page<ExOrder> page = exOrderMapper.searchBuyOrders(ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase());
		List<OrderModel> list = new ArrayList<>();
		for(ExOrder order:page) {
			OrderModel orderModel = getOrderModel(order,coinPairConfig);
			list.add(orderModel);
		}
		PageInfo<OrderModel> pageInfoOrder =
				new PageInfo<OrderModel>(page.getPageNum(),
						page.getPageSize(),page.getTotal(),list);

		PageHelper.clearPage();
		
		return pageInfoOrder;
	}

	@Override
	public PageInfo<OrderModel> searchMarketOrders(CoinPairConfig coinPairConfig) {
		PageHelper.startPage(Integer.valueOf(1), Integer.valueOf(3000));
		
		Page<ExOrder> page = exOrderMapper.searchMarketOrders(ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase());
		List<OrderModel> list = new ArrayList<>();
		for(ExOrder order:page) {
			OrderModel orderModel = getOrderModel(order,coinPairConfig);

			list.add(orderModel);
		}
		PageInfo<OrderModel> pageInfoOrder =
				new PageInfo<OrderModel>(page.getPageNum(),
						page.getPageSize(),page.getTotal(),list);

		PageHelper.clearPage();
		
		return pageInfoOrder;
	}

	@Override
	public Integer changeOrderStatusToNew(CoinPairConfig coinPairConfig) {
		return exOrderMapper.changeOrderStatusToNew(ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase());
	}

	@Override
	public OrderModel getLockOrderById(String orderId,CoinPairConfig coinPairConfig) {
		ExOrder exOrder = exOrderMapper.getLockOrderById(orderId,ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase());
		OrderModel orderModel = getOrderModel(exOrder,coinPairConfig);
		return orderModel;
	}

	@Override
	public int insertTrade(TradeModel record, CoinPairConfig coinPairConfig) {
		ExTrade exTrade = new ExTrade();
		exTrade.setAskUserId(Integer.valueOf(record.getAskUserId()));
		exTrade.setAskId(Long.valueOf(record.getAskOrderId()));
		exTrade.setBidId(Long.valueOf(record.getBidOrderId()));
		exTrade.setBidUserId(Integer.valueOf(record.getBidUserId()));
		exTrade.setBuyFee(record.getBuyFee());
		exTrade.setBuyFeeCoin(record.getBuyFeeCoin());
		exTrade.setCtime(record.getCreateTime());
		exTrade.setMtime(record.getCreateTime());
		exTrade.setPrice(record.getPrice());
		exTrade.setSellFee(record.getSellFee());
		exTrade.setSellFeeCoin(record.getSellFeeCoin());
		exTrade.setTableName(ExTrade.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase());
		exTrade.setTrendSide(record.getTrendSide());
		exTrade.setVolume(record.getVolume());
		return exTradeMapper.insert(exTrade);
	}

	@Override
	public int updateOrderByMatchingStatusAndPrimaryKeySelective(OrderModel record, CoinPairConfig coinPairConfig) {
		ExOrder exOrder = new ExOrder();
		exOrder.setAvgPrice(record.getAvgPrice());
		exOrder.setMtime(new Date());
		exOrder.setDealMoney(record.getDealMoney());
		exOrder.setDealVolume(record.getDealVolume());
		exOrder.setFee(record.getFee());
		exOrder.setStatus(record.getStatus());
		exOrder.setId(Long.valueOf(record.getOrderId()));
		exOrder.setTableName(ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase());
		
		return exOrderMapper.updateByMatchingStatusAndPrimaryKeySelective(exOrder);
	}
	
	@Override
	public void returnRemainQuoteCoinBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig)  throws AccountException,OrderException{
		Account toAccount = addQutoeCoinBalance(order, amount, coinPairConfig);
		Account fromAccount = decreaseQuoteCoinLockBalance(order, amount, coinPairConfig);
		
		String refType = ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase();
		savePaymentRecord(fromAccount, toAccount, amount, Long.valueOf(order.getOrderId()), "order.unlock.remainQuoteAmount", "cancel_trade", refType);
	}

	@Override
	public void returnRemainBaseCoinBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig)  throws AccountException,OrderException{
		Account toAccount = addBaseCoinBalance(order, amount, coinPairConfig);
		Account fromAccount = decreaseBaseCoinLockBalance(order, amount, coinPairConfig);
		
		String refType = ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase();
		savePaymentRecord(fromAccount, toAccount, amount, Long.valueOf(order.getOrderId()), "order.unlock.remainBaseAmount", "cancel_trade", refType);
	}

	@Override
	public void buyMatchingUpdateLockBalance(OrderModel buyOrder, OrderModel sellOrder, BigDecimal baseVolume,
			BigDecimal quoteAmount,BigDecimal fee,TradeModel trade, CoinPairConfig coinPairConfig) throws AccountException,OrderException{
		Account fromAccount = decreaseQuoteCoinLockBalance(buyOrder, quoteAmount, coinPairConfig);
		BigDecimal changeQuoteAmount = quoteAmount.subtract(fee);
		Account toAccount = addQutoeCoinBalance(sellOrder, changeQuoteAmount, coinPairConfig);
		Account toAccountFee = addQuoteCoinFeeBalance(sellOrder, fee, coinPairConfig);

		String refType = ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase();
		savePaymentRecord(fromAccount, toAccount, changeQuoteAmount, Long.valueOf(buyOrder.getOrderId()), "trade.transfer.quoteAmount", "trade", refType);
		if(toAccountFee != null) {
			savePaymentRecord(fromAccount, toAccountFee, fee, Long.valueOf(buyOrder.getOrderId()), "trade.transfer.quoteFeeAmount", "trade", refType);
		}
	}

	@Override
	public void sellMatchingUpdateLockBalance(OrderModel buyOrder, OrderModel sellOrder, BigDecimal baseVolume,
			BigDecimal quoteAmount,BigDecimal fee,TradeModel trade, CoinPairConfig coinPairConfig)  throws AccountException,OrderException{
		Account fromAccount = decreaseBaseCoinLockBalance(sellOrder, baseVolume, coinPairConfig);
		BigDecimal changeBaseVolume = baseVolume.subtract(fee);
		Account toAccount = addBaseCoinBalance(buyOrder, changeBaseVolume, coinPairConfig);
		Account toAccountFee = addBaseCoinFeeBalance(buyOrder, fee, coinPairConfig);

		String refType = ExOrder.TABLE_NAME_PREFIX + coinPairConfig.getSymbol().toLowerCase();
		savePaymentRecord(fromAccount, toAccount, changeBaseVolume, Long.valueOf(sellOrder.getOrderId()), "trade.transfer.baseVolume", "trade", refType);
		if(toAccountFee != null) {
			savePaymentRecord(fromAccount, toAccountFee, fee, Long.valueOf(sellOrder.getOrderId()), "trade.transfer.baseFeeVolume", "trade", refType);
		}
	}
	
	@Override
	public StatsSymbolRate selectOneSymbolRate(CoinPairConfig coinPairConfig) {
		String baseSymbol = coinPairConfig.getBaseCoin();
		String quoteSymbol = coinPairConfig.getQuoteCoin();
		
		StatsSymbolRateExample example = new StatsSymbolRateExample();
		example.createCriteria()
		.andBaseSymbolEqualTo(baseSymbol)
		.andQuoteSymbolEqualTo(quoteSymbol);
		List<StatsSymbolRate> result = statsSymbolRateMapper.selectByExample(example);
		if (result==null || result.size()==0) {
			return null;
		}else {
			return result.get(0);
		}
	}

	/**
	 * 扣减基准币冻结余额
	 * @param order
	 * @param amount
	 * @param coinPairConfig
	 * @return
	 * @throws AccountException
	 */
	private Account decreaseBaseCoinLockBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig)  throws AccountException,OrderException{
		if(amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount <= 0");
		}
		
		Account account = null;
		if(order.isGeneralOrder()) {
			
			Account accountParam = new Account();
			accountParam.setUid(Integer.valueOf(order.getUserId()));
			accountParam.setType(Integer.valueOf(coinPairConfig.getBaseAccountLock()));
			account = accountMapper.selectByUidAndTypeAddLock(accountParam);
			if(account.getBalance().compareTo(amount) < 0) {
				throw new AccountException("lockamount balance not enough, orderid=" + order.getOrderId() + ",baseAmount=" + amount,order);
			}
			accountMapper.subtractBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getBaseAccountLock()));

		}else if(order.isFinancingOrder()) {
			
			Account accountParam = new Account();
			accountParam.setUid(Integer.valueOf(order.getUserId()));
			accountParam.setType(Integer.valueOf(coinPairConfig.getBaseFinancingAccountLock()));
			account = accountMapper.selectByUidAndTypeAddLock(accountParam);
			if(account.getBalance().compareTo(amount) < 0) {
				throw new AccountException("lockamount balance not enough, orderid=" + order.getOrderId() + ",baseAmount=" + amount,order);
			}
			accountMapper.subtractBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getBaseFinancingAccountLock()));

		}else {
			throw new OrderException("order type error, orderid=" + order.getOrderId() + ",orderType=" + order.getOrderType(),order);
		}
				
		return account;
	}

	/**
	 * 扣减计价币冻结余额
	 * @param order
	 * @param amount
	 * @param coinPairConfig
	 * @return
	 * @throws AccountException
	 */
	private Account decreaseQuoteCoinLockBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) throws AccountException,OrderException{
		if(amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount <= 0");
		}
		
		Account account = null;
		if(order.isGeneralOrder()) {
			
			Account accountParam = new Account();
			accountParam.setUid(Integer.valueOf(order.getUserId()));
			accountParam.setType(Integer.valueOf(coinPairConfig.getQuoteAccountLock()));
			account = accountMapper.selectByUidAndTypeAddLock(accountParam);
			
			if(account.getBalance().compareTo(amount) < 0) {
				throw new AccountException("lockamount balance not enough, orderid=" + order.getOrderId() + ",quoteAmount=" + amount,order);
			}
			accountMapper.subtractBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getQuoteAccountLock()));
			
		}else if(order.isFinancingOrder()) {
			
			Account accountParam = new Account();
			accountParam.setUid(Integer.valueOf(order.getUserId()));
			accountParam.setType(Integer.valueOf(coinPairConfig.getQuoteFinancingAccountLock()));
			account = accountMapper.selectByUidAndTypeAddLock(accountParam);
			if(account.getBalance().compareTo(amount) < 0) {
				throw new AccountException("lockamount balance not enough, orderid=" + order.getOrderId() + ",quoteAmount=" + amount,order);
			}
			accountMapper.subtractBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getQuoteFinancingAccountLock()));
		
		}else {
			throw new OrderException("order type error, orderid=" + order.getOrderId() + ",orderType=" + order.getOrderType(),order);
		}
		
		return account;
	}

	/**
	 * 加基准币余额
	 * @param order
	 * @param amount
	 * @param coinPairConfig
	 * @return
	 */
	private Account addBaseCoinBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) throws OrderException{
		if(amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount < 0");
		}
		
		Account account = null;
		if(order.isGeneralOrder()) {
			
			account = accountMapper.selectByIdAndType(Integer.valueOf(order.getUserId()),
					Integer.valueOf(coinPairConfig.getBaseAccountNormal()));
			accountMapper.addBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getBaseAccountNormal()));
		
		}else if(order.isFinancingOrder()) {
			account = accountMapper.selectByIdAndType(Integer.valueOf(order.getUserId()),
					Integer.valueOf(coinPairConfig.getBaseFinancingAccountNormal()));
			accountMapper.addBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getBaseFinancingAccountNormal()));
		}else {
			throw new OrderException("order type error, orderid=" + order.getOrderId() + ",orderType=" + order.getOrderType(),order);
		}
		
		return account;
	}


	/**
	 * 加计价币余额
	 * @param order
	 * @param amount
	 * @param coinPairConfig
	 * @return
	 */
	private Account addQutoeCoinBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) throws OrderException{
		if(amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount < 0");
		}
		
		Account account = null;
		if(order.isGeneralOrder()) {
			
			account = accountMapper.selectByIdAndType(Integer.valueOf(order.getUserId()),
					Integer.valueOf(coinPairConfig.getQuoteAccountNormal()));
			accountMapper.addBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getQuoteAccountNormal()));
		
		}else if(order.isFinancingOrder()) {
			
			account = accountMapper.selectByIdAndType(Integer.valueOf(order.getUserId()),
					Integer.valueOf(coinPairConfig.getQuoteFinancingAccountNormal()));
			accountMapper.addBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getQuoteFinancingAccountNormal()));
		
		}else {
			throw new OrderException("order type error, orderid=" + order.getOrderId() + ",orderType=" + order.getOrderType(),order);
		}

		return account;
	}

	/**
	 * 加公司基准币手续费金额
	 * @param order
	 * @param amount
	 * @param coinPairConfig
	 * @return
	 */
	private Account addBaseCoinFeeBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig)  throws OrderException{
		if(amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount < 0");
		}
		if(amount.compareTo(BigDecimal.ZERO) == 0) {
			return null;
		}
		
		Account account = null;
		if(order.isGeneralOrder()) {
			account = accountMapper.selectByIdAndType(Integer.valueOf(coinPairConfig.getSysUid()),
					Integer.valueOf(coinPairConfig.getSysBaseAccount()));
			if(account == null) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, 
						"system account is not exist,uid=" + coinPairConfig.getSysUid() + " type=" + coinPairConfig.getSysBaseAccount()); 
			}
			accountMapper.addBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getSysBaseAccount()));
		}else if(order.isFinancingOrder()) {
			account = accountMapper.selectByIdAndType(Integer.valueOf(coinPairConfig.getSysUid()),
					Integer.valueOf(coinPairConfig.getSysBaseFinancingAccount()));
			if(account == null) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, 
						"system account is not exist,uid=" + coinPairConfig.getSysUid() + " type=" + coinPairConfig.getSysBaseFinancingAccount()); 
			}
			accountMapper.addBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getSysBaseFinancingAccount()));
		}else {
			throw new OrderException("order type error, orderid=" + order.getOrderId() + ",orderType=" + order.getOrderType(),order);
		}

		return account;
	}

	/**
	 * 加公司计价币手续费金额
	 * @param order
	 * @param amount
	 * @param coinPairConfig
	 * @return
	 */
	private Account addQuoteCoinFeeBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) throws OrderException{
		if(amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount < 0");
		}
		if(amount.compareTo(BigDecimal.ZERO) == 0) {
			return null;
		}
		
		Account account = null;
		if(order.isGeneralOrder()) {
			account = accountMapper.selectByIdAndType(
					Integer.valueOf(coinPairConfig.getSysUid()),Integer.valueOf(coinPairConfig.getSysQuoteAccount()));
			if(account == null) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, 
						"system account is not exist,uid=" + coinPairConfig.getSysUid() + " type=" + coinPairConfig.getSysQuoteAccount()); 
			}
			accountMapper.addBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getSysQuoteAccount()));
		}else if(order.isFinancingOrder()) {
			account = accountMapper.selectByIdAndType(
					Integer.valueOf(coinPairConfig.getSysUid()),Integer.valueOf(coinPairConfig.getSysQuoteFinancingAccount()));
			if(account == null) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, 
						"system account is not exist,uid=" + coinPairConfig.getSysUid() + " type=" + coinPairConfig.getSysQuoteFinancingAccount()); 
			}
			accountMapper.addBalance(account.getUid(), amount, Integer.valueOf(coinPairConfig.getSysQuoteFinancingAccount()));
		}else {
			throw new OrderException("order type error, orderid=" + order.getOrderId() + ",orderType=" + order.getOrderType(),order);
		}

		return account;
	}
	
	/**
	 * 爆仓基准币账户
	 * @param outOfStockAccount（必须是加锁的数据）
	 * @param coinPairConfig
	 */
	private void outOfStockBaseCoin(Account outOfStockAccount,CoinPairConfig coinPairConfig) {
		Account account = accountMapper.selectByIdAndType(
				Integer.valueOf(coinPairConfig.getSysUid()),Integer.valueOf(coinPairConfig.getSysBaseFinancingAccount()));
		accountMapper.addBalance(account.getUid(), outOfStockAccount.getBalance(), Integer.valueOf(coinPairConfig.getSysBaseFinancingAccount()));
		
		accountMapper.subtractBalance(outOfStockAccount.getUid(), outOfStockAccount.getBalance(), outOfStockAccount.getType());
		
		savePaymentRecord(outOfStockAccount, account, outOfStockAccount.getBalance(), 0L, "withFundingLoan", "out_of_stock", "with_funding_loan");
	}
	
	/**
	 * 爆仓计价币账户
	 * @param outOfStockAccount（必须是加锁的数据）
	 * @param coinPairConfig
	 */
	private void outOfStockQuoteCoin(Account outOfStockAccount,CoinPairConfig coinPairConfig) {
		Account account = accountMapper.selectByIdAndType(
				Integer.valueOf(coinPairConfig.getSysUid()),Integer.valueOf(coinPairConfig.getSysQuoteFinancingAccount()));
		accountMapper.addBalance(account.getUid(), outOfStockAccount.getBalance(), Integer.valueOf(coinPairConfig.getSysQuoteFinancingAccount()));
		
		accountMapper.subtractBalance(outOfStockAccount.getUid(), outOfStockAccount.getBalance(), outOfStockAccount.getType());
		
		savePaymentRecord(outOfStockAccount, account, outOfStockAccount.getBalance(), 0L, "withFundingLoan", "out_of_stock", "with_funding_loan");
	}
	
	/**
	 * Model and entity 类型转换
	 * @param order
	 * @param coinPairConfig
	 * @return
	 */
	private OrderModel getOrderModel(ExOrder order,CoinPairConfig coinPairConfig) {
		OrderModel orderModel = new OrderModel();
		orderModel.setAvgPrice(order.getAvgPrice());
		orderModel.setBaseCoin(coinPairConfig.getBaseCoin());
		orderModel.setCoinPairSymbol(coinPairConfig.getSymbol());
		orderModel.setCreateDate(order.getCtime());
		orderModel.setDealMoney(order.getDealMoney());
		orderModel.setDealVolume(order.getDealVolume());
		orderModel.setFee(order.getFee());
		orderModel.setFeeRateMaker(BigDecimal.valueOf(order.getFeeRateMaker()));
		orderModel.setFeeRateTaker(BigDecimal.valueOf(order.getFeeRateTaker()));
		orderModel.setOrderId(String.valueOf(order.getId()));
		orderModel.setPrice(order.getPrice());
		orderModel.setQuoteCoin(coinPairConfig.getQuoteCoin());
		orderModel.setSide(order.getSide());
		orderModel.setSource(order.getSource());
		orderModel.setStatus(order.getStatus());
		orderModel.setType(order.getType());
		orderModel.setUpdateDate(order.getMtime());
		orderModel.setUserId(String.valueOf(order.getUserId()));
		orderModel.setVolume(order.getVolume());
		orderModel.setOrderType(order.getOrderType());
		
		return orderModel;
	}

	/**
	 * 支付记录保存
	 * @param fromAccount
	 * @param toAccount
	 * @param amount
	 * @param orderId
	 * @param meta
	 * @param scene
	 * @param coinPairConfig
	 */
	private void savePaymentRecord(Account fromAccount, Account toAccount, BigDecimal amount, Long orderId, String meta, String scene, String refType) {
		
		Integer opUid = new Integer(1);
		Transaction transaction = new Transaction();
	    transaction.setFromUid(fromAccount.getUid());
	    transaction.setFromType(fromAccount.getType());
	    transaction.setToUid(toAccount.getUid());
	    transaction.setToType(toAccount.getType());
	    transaction.setAmount(amount);
	    transaction.setRefType(refType);
	    transaction.setRefId(orderId);
	    if (StringUtils.isBlank(meta)) {
	      transaction.setMeta("transfer");
	    } else {
	      transaction.setMeta(meta);
	    }
	    if (StringUtils.isBlank(scene)) {
	      transaction.setScene("");
	    } else {
	      transaction.setScene(scene);
	    }
	    transaction.setOpUid(opUid);
	    transaction.setFromBalance(fromAccount.getBalance().subtract(amount));
	    transaction.setToBalance(toAccount.getBalance().add(amount));
	    transaction.setOpIp("");
	    Date currentDate = new Date();
	    transaction.setCtime(currentDate);
	    transaction.setMtime(currentDate);
	    this.transactionMapper.insert(transaction);
	}
	
	@Override
	public void lockAccountByOrder(OrderModel orderData, CoinPairConfig coinPairConfig) throws OrderException {
		if (orderData.isGeneralOrder()) {

			if(orderData.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
				Account accountParam = new Account();
				accountParam.setUid(Integer.valueOf(orderData.getUserId()));
				accountParam.setType(Integer.valueOf(coinPairConfig.getQuoteAccountLock()));
				accountMapper.selectByUidAndTypeAddLock(accountParam);
			}else if(orderData.getSide().equals(ExchangeEnum.ExchangeSide.SELL.getCode())){
				Account accountParam = new Account();
				accountParam.setUid(Integer.valueOf(orderData.getUserId()));
				accountParam.setType(Integer.valueOf(coinPairConfig.getBaseAccountLock()));
				accountMapper.selectByUidAndTypeAddLock(accountParam);
			}else {
				throw new OrderException(
						"order type error, orderid=" + orderData.getOrderId() + ",side=" + orderData.getSide(), orderData);
			}
			

		} else if (orderData.isFinancingOrder()) {

			if(orderData.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
				Account accountParam = new Account();
				accountParam.setUid(Integer.valueOf(orderData.getUserId()));
				accountParam.setType(Integer.valueOf(coinPairConfig.getQuoteFinancingAccountLock()));
				accountMapper.selectByUidAndTypeAddLock(accountParam);
			}else if(orderData.getSide().equals(ExchangeEnum.ExchangeSide.SELL.getCode())){
				Account accountParam = new Account();
				accountParam.setUid(Integer.valueOf(orderData.getUserId()));
				accountParam.setType(Integer.valueOf(coinPairConfig.getBaseFinancingAccountLock()));
				accountMapper.selectByUidAndTypeAddLock(accountParam);
			}else {
				throw new OrderException(
						"order type error, orderid=" + orderData.getOrderId() + ",side=" + orderData.getSide(), orderData);
			}
			

		} else {
			throw new OrderException(
					"order type error, orderid=" + orderData.getOrderId() + ",orderType=" + orderData.getOrderType(), orderData);
		}
	}



	@Override
	public List<DebtOrderModel> selectByUnfinishedDebtOrder(CoinPairConfig coinPairConfig) {
		String baseSymbol = coinPairConfig.getBaseCoin();
		String quoteSymbol = coinPairConfig.getQuoteCoin();
		String coinPairSymbol = baseSymbol.toLowerCase() + quoteSymbol.toLowerCase();
		List<WithFundingLoan> list = withFundingLoanMapper.selectByStatus(
				Integer.valueOf(MatchingEnum.WithFundingLoanStatus.loan.getCode()).byteValue(),coinPairSymbol);
		HashMap<String,DebtOrderModel> modelMap = new HashMap<>();
		for(WithFundingLoan order:list) {
			String key = order.getUid() + order.getSymbol().toLowerCase();
			DebtOrderModel model = modelMap.get(key);
			if(model == null) {
				model = new DebtOrderModel();
				model.setUserId(order.getUid().toString());
				if(order.getCoin().toLowerCase().equals(coinPairConfig.getBaseCoin().toLowerCase())) {
					model.setBaseBalanceDebtAmount(order.getNoReturnAmount());
					model.setBaseSumDebtAmount(order.getAmount());
					model.setQuoteBalanceDebtAmount(BigDecimal.ZERO);
					model.setQuoteSumDebtAmount(BigDecimal.ZERO);
				}else if(order.getCoin().toLowerCase().equals(coinPairConfig.getQuoteCoin().toLowerCase())) {
					model.setBaseBalanceDebtAmount(BigDecimal.ZERO);
					model.setBaseSumDebtAmount(BigDecimal.ZERO);
					model.setQuoteBalanceDebtAmount(order.getNoReturnAmount());
					model.setQuoteSumDebtAmount(order.getAmount());
				}else {
					throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"WithFundingLoan coin no support,coinSymbol=" + order.getCoin() + " id=" + order.getId());
				}
				model.setBaseSymbol(baseSymbol.toLowerCase());
				model.setQuoteSymbol(quoteSymbol.toLowerCase());
				modelMap.put(key, model);
			}else {
				if(order.getCoin().toLowerCase().equals(coinPairConfig.getBaseCoin().toLowerCase())) {
					model.setBaseBalanceDebtAmount(model.getBaseBalanceDebtAmount().add(order.getNoReturnAmount()));
					model.setBaseSumDebtAmount(model.getBaseSumDebtAmount().add(order.getAmount()));
				}else if(order.getCoin().toLowerCase().equals(coinPairConfig.getQuoteCoin().toLowerCase())) {
					model.setQuoteBalanceDebtAmount(model.getQuoteBalanceDebtAmount().add(order.getNoReturnAmount()));
					model.setQuoteSumDebtAmount(model.getQuoteSumDebtAmount().add(order.getAmount()));
				}else {
					throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"WithFundingLoan coin no support,coinSymbol=" + order.getCoin() + " id=" + order.getId());
				}
			}
		}
		
		ArrayList<DebtOrderModel> modelList = new ArrayList<>();
		Iterator iterator = modelMap.keySet().iterator();
		while(iterator.hasNext()) {
			modelList.add((DebtOrderModel)modelMap.get(iterator.next()));
		}
		return modelList;
	}



	@Override
	@Transactional(propagation=Propagation.REQUIRED, isolation=Isolation.REPEATABLE_READ, rollbackFor={Throwable.class})
	public void outOfStock(String userId, CoinPairConfig coinPairConfig) throws AccountException,OrderException{
		List<ExOrder> orderList = null;
		Account baseFinancingAccount = null;
		Account quoteFinancingAccount = null;
		Integer maxGetLockCount = 3;
		Integer retryGetlockCount = 1;
		while(maxGetLockCount.compareTo(retryGetlockCount) >= 0) {
			try {
				//锁配资交易订单
				String tableName = ExOrder.TABLE_NAME_PREFIX + 
						coinPairConfig.getBaseCoin().toLowerCase() + 
						coinPairConfig.getQuoteCoin().toLowerCase();
				orderList = exOrderMapper.searchWithFundingByUserIdAndLock(userId, tableName);
						
				//锁账户
				Account baseFinancingAccountParam = new Account();
				baseFinancingAccountParam.setUid(Integer.valueOf(userId));
				baseFinancingAccountParam.setType(Integer.valueOf(coinPairConfig.getBaseFinancingAccountNormal()));
				baseFinancingAccount = accountMapper.selectByUidAndTypeAddLock(baseFinancingAccountParam);
				
				Account quoteFinancingAccountParam = new Account();
				quoteFinancingAccountParam.setUid(Integer.valueOf(userId));
				quoteFinancingAccountParam.setType(Integer.valueOf(coinPairConfig.getQuoteFinancingAccountNormal()));
				quoteFinancingAccount = accountMapper.selectByUidAndTypeAddLock(quoteFinancingAccountParam);
				
				Account baseFinancingAccountLockParam = new Account();
				baseFinancingAccountLockParam.setUid(Integer.valueOf(userId));
				baseFinancingAccountLockParam.setType(Integer.valueOf(coinPairConfig.getBaseFinancingAccountLock()));
				accountMapper.selectByUidAndTypeAddLock(baseFinancingAccountLockParam);
				
				Account quoteFinancingAccountLockParam = new Account();
				quoteFinancingAccountLockParam.setUid(Integer.valueOf(userId));
				quoteFinancingAccountLockParam.setType(Integer.valueOf(coinPairConfig.getQuoteFinancingAccountLock()));
				accountMapper.selectByUidAndTypeAddLock(quoteFinancingAccountLockParam);
				
				//再一次锁配资交易订单，防止在锁订单与锁账户之间，创建订单
				orderList = exOrderMapper.searchWithFundingByUserIdAndLock(userId, tableName);
				
				break;
			}catch(Exception e) {
				logger.error("锁获取失败,retryGetlockCount="+retryGetlockCount,e);
				retryGetlockCount = retryGetlockCount + 1;
			}
		}
		

		
		//取消配资交易订单
		if(orderList != null) {
			for(ExOrder order:orderList) {
				OrderModel orderModel = getOrderModel(order, coinPairConfig);
				cancelOrder(orderModel, coinPairConfig);
			}
		}
		
		
		//锁借贷单
		String coinPairSymbol = coinPairConfig.getBaseCoin().toLowerCase() + 
				coinPairConfig.getQuoteCoin().toLowerCase();
		List<WithFundingLoan> withFundingLoanlist = withFundingLoanMapper.selectByStatusAndUserIdLock(Integer.valueOf(userId),
				Integer.valueOf(MatchingEnum.WithFundingLoanStatus.loan.getCode()).byteValue(),coinPairSymbol);

		//强制还款
		//重新查询账户余额
		Account baseFinancingAccountParam = new Account();
		baseFinancingAccountParam.setUid(Integer.valueOf(userId));
		baseFinancingAccountParam.setType(Integer.valueOf(coinPairConfig.getBaseFinancingAccountNormal()));
		baseFinancingAccount = accountMapper.selectByUidAndTypeAddLock(baseFinancingAccountParam);
		//重新查询账户余额
		Account quoteFinancingAccountParam = new Account();
		quoteFinancingAccountParam.setUid(Integer.valueOf(userId));
		quoteFinancingAccountParam.setType(Integer.valueOf(coinPairConfig.getQuoteFinancingAccountNormal()));
		quoteFinancingAccount = accountMapper.selectByUidAndTypeAddLock(quoteFinancingAccountParam);
		
		Integer status = Integer.valueOf(MatchingEnum.WithFundingLoanStatus.outOfStock.getCode());
		WithFundingLoan loanParam = new WithFundingLoan();
		loanParam.setStatus(status.byteValue());
		for(WithFundingLoan loan:withFundingLoanlist) {
			loanParam.setId(loan.getId());
			withFundingLoanMapper.updateByPrimaryKeySelective(loanParam);
		}
		outOfStockBaseCoin(baseFinancingAccount, coinPairConfig);
		outOfStockQuoteCoin(quoteFinancingAccount, coinPairConfig);
		
	}



	@Override
	public FinancingAccountBalance selectFinancingAccountBalance(String userId, CoinPairConfig coinPairConfig) {
		Account baseFinancingAccount = accountMapper.selectByIdAndType(
				Integer.valueOf(userId),Integer.valueOf(coinPairConfig.getBaseFinancingAccountNormal()));
		Account baseFinancingLockAccount = accountMapper.selectByIdAndType(
				Integer.valueOf(userId),Integer.valueOf(coinPairConfig.getBaseFinancingAccountLock()));
		
		Account quoteFinancingAccount = accountMapper.selectByIdAndType(
				Integer.valueOf(userId),Integer.valueOf(coinPairConfig.getQuoteFinancingAccountNormal()));
		Account quoteFinancingLockAccount = accountMapper.selectByIdAndType(
				Integer.valueOf(userId),Integer.valueOf(coinPairConfig.getQuoteFinancingAccountLock()));
		
		FinancingAccountBalance accountBalance = new FinancingAccountBalance();
		
		accountBalance.setBaseCoin(coinPairConfig.getBaseCoin());
		if(baseFinancingAccount.getBalance() == null) {
			accountBalance.setBaseCoinBalance(BigDecimal.ZERO);
		}else {
			accountBalance.setBaseCoinBalance(baseFinancingAccount.getBalance());
		}
		if(baseFinancingLockAccount.getBalance() == null) {
			accountBalance.setBaseCoinLockBalance(BigDecimal.ZERO);
		}else {
			accountBalance.setBaseCoinLockBalance(baseFinancingLockAccount.getBalance());
		}
		
		
		accountBalance.setQuoteCoin(coinPairConfig.getQuoteCoin());
		if(quoteFinancingAccount.getBalance() == null) {
			accountBalance.setQuoteCoinBalance(BigDecimal.ZERO);
		}else {
			accountBalance.setQuoteCoinBalance(quoteFinancingAccount.getBalance());
		}
		if(quoteFinancingLockAccount.getBalance() == null) {
			accountBalance.setQuoteCoinLockBalance(BigDecimal.ZERO);
		}else {
			accountBalance.setQuoteCoinLockBalance(quoteFinancingLockAccount.getBalance());
		}
		
		
		return accountBalance;
	}
	
	/**
	 * 取消交易订单
	 * @param order
	 * @param coinPairConfig
	 * @throws AccountException
	 * @throws OrderException
	 */
	private void cancelOrder(OrderModel order,CoinPairConfig coinPairConfig)  throws AccountException,OrderException{
		returnRemain(order,coinPairConfig);
		Integer status = Integer.valueOf(ExchangeEnum.OrderStatus.CANCELED.getCode());
		order.setStatus(status.byteValue());
		Integer num = updateOrderByMatchingStatusAndPrimaryKeySelective(order,coinPairConfig);
		if(num == 0) {
			throw new OrderException("market order status is not in (1,3)", order);
		}
	}
	
	/**
	 * 交易订单金额退还
	 * @param order
	 * @param coinPairConfig
	 * @throws AccountException
	 * @throws OrderException
	 */
	private void returnRemain(OrderModel order,CoinPairConfig coinPairConfig) throws AccountException,OrderException{
		if (order.isLimitOrder()) {
			if (order.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
				BigDecimal totalQuoteAmount = order.getPrice().multiply(order.getVolume());
				BigDecimal remainQuoteAmount = totalQuoteAmount.subtract(order.getDealMoney());
				if (remainQuoteAmount.compareTo(BigDecimal.ZERO) > 0) {
					returnRemainQuoteCoinBalance(order, remainQuoteAmount, coinPairConfig);
				}
			}else {
				BigDecimal totalBaseAmount = order.getVolume();
				BigDecimal remainBaseAmount = totalBaseAmount.subtract(order.getDealMoney());
				if (remainBaseAmount.compareTo(BigDecimal.ZERO) > 0) {
					returnRemainBaseCoinBalance(order, remainBaseAmount, coinPairConfig);
				}
			}
		} else if (order.isMarketOrder()) {
			if (order.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
				BigDecimal remainQuoteAmount = order.getVolume().subtract(order.getDealMoney());
				if (remainQuoteAmount.compareTo(BigDecimal.ZERO) > 0) {
					returnRemainQuoteCoinBalance(order, remainQuoteAmount, coinPairConfig);
				}
			} else {
				BigDecimal remainBaseVolume = order.getVolume().subtract(order.getDealVolume());
				if (remainBaseVolume.compareTo(BigDecimal.ZERO) > 0) {
					returnRemainBaseCoinBalance(order, remainBaseVolume, coinPairConfig);
				}
			}
		}
	}
	

}
