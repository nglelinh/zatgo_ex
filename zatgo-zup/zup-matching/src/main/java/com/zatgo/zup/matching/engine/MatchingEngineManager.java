package com.zatgo.zup.matching.engine;

import java.util.ArrayList;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.matching.comparator.OrderTimeComparator;
import com.zatgo.zup.matching.config.CoinPairConfig;
import com.zatgo.zup.matching.exception.AccountException;
import com.zatgo.zup.matching.exception.OrderException;
import com.zatgo.zup.matching.exchange.ExchangeStrategy;
import com.zatgo.zup.matching.model.OrderModel;

@Component
public class MatchingEngineManager {

	private static final Logger logger = LoggerFactory.getLogger(MatchingEngineManager.class);
	
	@Autowired
	private MatchingEngine matchingEngine;
	
	@Autowired
	private ExchangeStrategy exchangeStrategy;
	
	private CoinPairConfig config;

	private final TreeSet<OrderModel> roundOrders = new TreeSet(new OrderTimeComparator());
	
	public void run(CoinPairConfig config) {
		logger.info("start matching engine manager");
		this.config = config;
		matchingEngine.initConfig(config);

		while(true) {
			try {
				OrderModel orderData = takeOrder();
				if(orderData == null) {
					Thread.sleep(1000);
					continue;
				}
				
				if(orderData.getStatus().equals(new Byte(ExchangeEnum.OrderStatus.INIT.getCode()))) {
					continue;
				}
				
				try {
					matchingEngine.acceptOrder(orderData);
				} catch (AccountException e) {
					logger.error("", e);
					OrderModel eOrder = e.getOrder();
					if (eOrder != null) {
						matchingEngine.changeOrderToException(eOrder);
						if (!orderData.getOrderId().equals(eOrder.getOrderId())) {
							matchingEngine.removeBook(eOrder);
						}
					}
				} catch (OrderException e) {
					logger.error("", e);
					OrderModel eOrder = e.getOrder();
					if (eOrder != null) {
						matchingEngine.changeOrderToException(eOrder);
						if (!orderData.getOrderId().equals(eOrder.getOrderId())) {
							matchingEngine.removeBook(eOrder);
						}
					}
				}catch(Throwable e) {
					logger.error("", e);
				}
				
			}catch(Throwable e) {
				logger.error("", e);
			}
			
		}
		
	}
	
	private OrderModel takeOrder() {
		if(this.roundOrders.isEmpty()) {
			loadRoundOrders();
			if(this.roundOrders.isEmpty()) {
				return null;
			}
		}
		
		return roundOrders.pollFirst();
	}
	
	private void loadRoundOrders() {
		logger.info("round match order loading");
		roundOrders.clear();
		matchingEngine.reset();
		
		try {
			Integer newNum = exchangeStrategy.getExchangeService(config).changeOrderStatusToNew(config);
			logger.info("change order status to new number:" + newNum);
			
			PageInfo<OrderModel> marketOrders = exchangeStrategy.getExchangeService(config).searchMarketOrders(config);
			
			PageInfo<OrderModel> buyOrders = exchangeStrategy.getExchangeService(config).searchBuyOrders(config);
			
			PageInfo<OrderModel> sellOrders = exchangeStrategy.getExchangeService(config).searchSellOrders(config);
			
			if(marketOrders != null && marketOrders.getList() != null) {
				for(OrderModel model:marketOrders.getList()) {
					roundOrders.add(model);
				}
				logger.info("market order number:" + marketOrders.getList().size());
			}else {
				logger.info("market order number:" + 0);
			}
			
			if(buyOrders != null && buyOrders.getList() != null) {
				for(OrderModel model:buyOrders.getList()) {
					roundOrders.add(model);
				}
				logger.info("buy order number:" + buyOrders.getList().size());
			}else {
				logger.info("buy order number:" + 0);
			}
			if(sellOrders != null && sellOrders.getList() != null) {
				for(OrderModel model:sellOrders.getList()) {
					roundOrders.add(model);
				}
				logger.info("sell order number:" + sellOrders.getList().size());
			}else {
				logger.info("sell order number:" + 0);
			}

		}catch(Exception e) {
			logger.error("load order fail",e);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				logger.error("",e1);
			}
		}
		
	}
}
