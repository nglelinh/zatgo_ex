package com.zatgo.zup.matching.exchange.zatgo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.zatgo.zup.matching.exchange.zatgo.entity.PaymentRecord;

@Mapper
public interface PaymentRecordMapper {
    int deleteByPrimaryKey(String paymentId);

    int insert(PaymentRecord record);

    int insertSelective(PaymentRecord record);

    PaymentRecord selectByPrimaryKey(String paymentId);

    int updateByPrimaryKeySelective(PaymentRecord record);

    int updateByPrimaryKey(PaymentRecord record);


}