package com.zatgo.zup.matching.exchange.zatgo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.zatgo.zup.matching.exchange.zatgo.entity.ExTrade;

@Mapper
public interface TradeMapper {
    int deleteByPrimaryKey(String tradeId);

    int insert(ExTrade record);

    int insertSelective(ExTrade record);

    ExTrade selectByPrimaryKey(String tradeId);

    int updateByPrimaryKeySelective(ExTrade record);

    int updateByPrimaryKey(ExTrade record);
}