package com.zatgo.zup.matching.exchange.zatgo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.github.pagehelper.Page;
import com.zatgo.zup.matching.exchange.zatgo.entity.ExOrder;

@Mapper
public interface OrderMapper {
	int deleteByPrimaryKey(String orderId);

	int insert(ExOrder record);

	int insertSelective(ExOrder record);

	ExOrder selectByPrimaryKey(String orderId);

	int updateByPrimaryKeySelective(ExOrder record);
	
	int updateByMatchingStatusAndPrimaryKeySelective(ExOrder record);

	int updateByPrimaryKey(ExOrder record);
	
	public Page<ExOrder> searchSellOrders();
	
	public Page<ExOrder> searchBuyOrders();
	
	public Page<ExOrder> searchMarketOrders();
	
	public Integer changeOrderStatusToNew();
	
	public ExOrder getLockOrderById(String orderId);

}