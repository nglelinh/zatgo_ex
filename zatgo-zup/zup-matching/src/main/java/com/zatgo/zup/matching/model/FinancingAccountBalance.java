package com.zatgo.zup.matching.model;

import java.math.BigDecimal;

public class FinancingAccountBalance {

	private String userId;
	
	private String baseCoin;

    private String quoteCoin;

    private String coinPairSymbol;
    
    private BigDecimal baseCoinBalance;
    
    private BigDecimal quoteCoinBalance;
    
    private BigDecimal baseCoinLockBalance;
    
    private BigDecimal quoteCoinLockBalance;

	public BigDecimal getBaseCoinLockBalance() {
		return baseCoinLockBalance;
	}

	public void setBaseCoinLockBalance(BigDecimal baseCoinLockBalance) {
		this.baseCoinLockBalance = baseCoinLockBalance;
	}

	public BigDecimal getQuoteCoinLockBalance() {
		return quoteCoinLockBalance;
	}

	public void setQuoteCoinLockBalance(BigDecimal quoteCoinLockBalance) {
		this.quoteCoinLockBalance = quoteCoinLockBalance;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}

	public String getCoinPairSymbol() {
		return coinPairSymbol;
	}

	public void setCoinPairSymbol(String coinPairSymbol) {
		this.coinPairSymbol = coinPairSymbol;
	}

	public BigDecimal getBaseCoinBalance() {
		return baseCoinBalance;
	}

	public void setBaseCoinBalance(BigDecimal baseCoinBalance) {
		this.baseCoinBalance = baseCoinBalance;
	}

	public BigDecimal getQuoteCoinBalance() {
		return quoteCoinBalance;
	}

	public void setQuoteCoinBalance(BigDecimal quoteCoinBalance) {
		this.quoteCoinBalance = quoteCoinBalance;
	}
    
    
}
