package com.zatgo.zup.matching.exception;

import com.zatgo.zup.matching.model.OrderModel;

public class OrderException extends MatchingException {

	private OrderModel order;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrderException(String msg) {
		super(msg);
	}

	public OrderException(String msg, OrderModel order) {
		super(msg);
		setOrder(order);
	}

	public OrderModel getOrder() {
		return this.order;
	}

	public void setOrder(OrderModel order) {
		this.order = order;
	}

}
