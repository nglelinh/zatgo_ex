package com.zatgo.zup.matching.enumtype;

import java.util.HashMap;
import java.util.Map;

import com.zatgo.zup.common.enumtype.BusinessEnum.ExtractStatus;

public class MatchingEnum {

	public enum OrderType{

		generalTrade("0"),financingTrade("1");

		private String code;
		
		private static final Map<String, OrderType> stringToEnum = new HashMap<String, OrderType>();
	    static {
	        for(OrderType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static OrderType getEnumByType(String type) {
	    	return stringToEnum.get(type);
	    }

		private OrderType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
	}
	
	
	public enum WithFundingLoanStatus{

		noApprove("0"),approve("1"),refuse("2"),loan("3"),refund("4"),outOfStock("5");

		private String code;
		
		private static final Map<String, WithFundingLoanStatus> stringToEnum = new HashMap<String, WithFundingLoanStatus>();
	    static {
	        for(WithFundingLoanStatus enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static WithFundingLoanStatus getEnumByType(String type) {
	    	return stringToEnum.get(type);
	    }

		private WithFundingLoanStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
	}
}
