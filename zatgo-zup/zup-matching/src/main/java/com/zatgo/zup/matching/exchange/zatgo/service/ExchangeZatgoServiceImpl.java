package com.zatgo.zup.matching.exchange.zatgo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.matching.config.CoinPairConfig;
import com.zatgo.zup.matching.exception.AccountException;
import com.zatgo.zup.matching.exception.OrderException;
import com.zatgo.zup.matching.exchange.ExchangeService;
import com.zatgo.zup.matching.exchange.bitshop.entity.StatsSymbolRate;
import com.zatgo.zup.matching.exchange.zatgo.entity.ExOrder;
import com.zatgo.zup.matching.exchange.zatgo.entity.PaymentRecord;
import com.zatgo.zup.matching.exchange.zatgo.entity.User;
import com.zatgo.zup.matching.exchange.zatgo.entity.WalletAccount;
import com.zatgo.zup.matching.exchange.zatgo.mapper.OrderMapper;
import com.zatgo.zup.matching.exchange.zatgo.mapper.TradeMapper;
import com.zatgo.zup.matching.exchange.zatgo.mapper.PaymentRecordMapper;
import com.zatgo.zup.matching.exchange.zatgo.mapper.UserMapper;
import com.zatgo.zup.matching.exchange.zatgo.mapper.WalletAccountMapper;
import com.zatgo.zup.matching.model.DebtOrderModel;
import com.zatgo.zup.matching.model.FinancingAccountBalance;
import com.zatgo.zup.matching.model.OrderModel;
import com.zatgo.zup.matching.model.TradeModel;

@Component("ExchangeService_zatgo")
public class ExchangeZatgoServiceImpl implements ExchangeService {
	
	private static final Logger logger = LoggerFactory.getLogger(ExchangeZatgoServiceImpl.class);

	@Autowired
	private OrderMapper exOrderMapper;
	
	@Autowired
	private TradeMapper exTradeMapper;
	
	@Autowired
	private WalletAccountMapper walletAccountMapper;
	
	@Autowired
	private UserMapper userMapper; 
	
	@Autowired
	private PaymentRecordMapper paymentRecordMapper;
	
	@Override
	public PageInfo<OrderModel> searchSellOrders(CoinPairConfig coinPairConfig) {
		PageHelper.startPage(Integer.valueOf(1), Integer.valueOf(3000));
		
		Page<ExOrder> page = exOrderMapper.searchSellOrders();
		List<OrderModel> list = new ArrayList<>();
		for(ExOrder order:page) {
			OrderModel orderModel = new OrderModel();
			try {
				BeanUtils.copyProperties(orderModel, order);
			} catch (Exception e) {
				logger.error("",e);
			}

			list.add(orderModel);
		}
		PageInfo<OrderModel> pageInfoOrder =
				new PageInfo<OrderModel>(page.getPageNum(),
						page.getPageSize(),page.getTotal(),list);

		PageHelper.clearPage();
		
		return pageInfoOrder;
	}

	@Override
	public PageInfo<OrderModel> searchBuyOrders(CoinPairConfig coinPairConfig) {
		PageHelper.startPage(Integer.valueOf(1), Integer.valueOf(3000));
		
		Page<ExOrder> page = exOrderMapper.searchBuyOrders();
		List<OrderModel> list = new ArrayList<>();
		for(ExOrder order:page) {
			OrderModel orderModel = new OrderModel();
			try {
				BeanUtils.copyProperties(orderModel, order);
			} catch (Exception e) {
				logger.error("",e);
			}

			list.add(orderModel);
		}
		PageInfo<OrderModel> pageInfoOrder =
				new PageInfo<OrderModel>(page.getPageNum(),
						page.getPageSize(),page.getTotal(),list);

		PageHelper.clearPage();
		
		return pageInfoOrder;
	}

	@Override
	public PageInfo<OrderModel> searchMarketOrders(CoinPairConfig coinPairConfig) {
		PageHelper.startPage(Integer.valueOf(1), Integer.valueOf(3000));
		
		Page<ExOrder> page = exOrderMapper.searchMarketOrders();
		List<OrderModel> list = new ArrayList<>();
		for(ExOrder order:page) {
			OrderModel orderModel = new OrderModel();
			try {
				BeanUtils.copyProperties(orderModel, order);
			} catch (Exception e) {
				logger.error("",e);
				throw new BusinessException();
			}

			list.add(orderModel);
		}
		PageInfo<OrderModel> pageInfoOrder =
				new PageInfo<OrderModel>(page.getPageNum(),
						page.getPageSize(),page.getTotal(),list);

		PageHelper.clearPage();
		
		return pageInfoOrder;
	}

	@Override
	public Integer changeOrderStatusToNew(CoinPairConfig coinPairConfig) {
		return exOrderMapper.changeOrderStatusToNew();
	}

	@Override
	public OrderModel getLockOrderById(String orderId,CoinPairConfig coinPairConfig) {
		ExOrder exOrder = exOrderMapper.getLockOrderById(orderId);
		OrderModel orderModel = new OrderModel();
		try {
			BeanUtils.copyProperties(orderModel, exOrder);
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
		return orderModel;
	}

	@Override
	public int insertTrade(TradeModel record, CoinPairConfig coinPairConfig) {
		record.setTradeId(UUIDUtils.getUuid());
		return exTradeMapper.insert(record);
	}

	@Override
	public int updateOrderByMatchingStatusAndPrimaryKeySelective(OrderModel record, CoinPairConfig coinPairConfig) {
		return exOrderMapper.updateByMatchingStatusAndPrimaryKeySelective(record);
	}

	private WalletAccount decreaseBaseCoinLockBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig)  throws AccountException{
		if(amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount <= 0");
		}
		WalletAccount account = walletAccountMapper.selectByUserIdAndTypeForUpdate(order.getUserId(), coinPairConfig.getBaseCoin());
		if(account.getLockBalance().compareTo(amount) < 0) {
			throw new AccountException("lockamount balance not enough, orderid=" + order.getOrderId() + ",quoteAmount=" + amount,order);
		}
		walletAccountMapper.decreaseLockBalance(account.getAccountId(), amount);
		
		return account;
	}

	private WalletAccount decreaseQuoteCoinLockBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) throws AccountException{
		if(amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount <= 0");
		}
		WalletAccount account = walletAccountMapper.selectByUserIdAndTypeForUpdate(order.getUserId(), coinPairConfig.getQuoteCoin());
		if(account.getLockBalance().compareTo(amount) < 0) {
			throw new AccountException("lockamount balance not enough, orderid=" + order.getOrderId() + ",quoteAmount=" + amount,order);
		}
		walletAccountMapper.decreaseLockBalance(account.getAccountId(), amount);
		return account;
	}

	private WalletAccount addBaseCoinBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) {
		if(amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount < 0");
		}
		WalletAccount account = walletAccountMapper.selectByUserIdAndType(order.getUserId(), coinPairConfig.getBaseCoin());
		walletAccountMapper.addBalance(account.getAccountId(), amount);
		return account;
	}


	private WalletAccount addQutoeCoinBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) {
		if(amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount < 0");
		}
		WalletAccount account = walletAccountMapper.selectByUserIdAndType(order.getUserId(), coinPairConfig.getQuoteCoin());
		walletAccountMapper.addBalance(account.getAccountId(), amount);
		return account;
	}

	private WalletAccount addBaseCoinFeeBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) {
		if(amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount < 0");
		}
		if(amount.compareTo(BigDecimal.ZERO) == 0) {
			return null;
		}
		
		User user = userMapper.selectByPrimaryKey(order.getUserId());
		User feeManageUser = userMapper.selectByCloudUserIdAndManageType(user.getCloudUserId(), BusinessEnum.CloudManageUserType.EXCHANGE_FEE.getType().toString());
		WalletAccount account = walletAccountMapper.selectByUserIdAndType(feeManageUser.getUserId(), coinPairConfig.getBaseCoin());
		walletAccountMapper.addBalance(account.getAccountId(), amount);
		
		return account;
	}

	private WalletAccount addQutoeCoinFeeBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig) {
		if(amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "amount < 0");
		}
		if(amount.compareTo(BigDecimal.ZERO) == 0) {
			return null;
		}
		User user = userMapper.selectByPrimaryKey(order.getUserId());
		User feeManageUser = userMapper.selectByCloudUserIdAndManageType(user.getCloudUserId(), BusinessEnum.CloudManageUserType.EXCHANGE_FEE.getType().toString());
		WalletAccount account = walletAccountMapper.selectByUserIdAndType(feeManageUser.getUserId(), coinPairConfig.getQuoteCoin());
		walletAccountMapper.addBalance(account.getAccountId(), amount);
		return account;
	}

	@Override
	public void returnRemainQuoteCoinBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig)  throws AccountException{
		WalletAccount toAccount = addQutoeCoinBalance(order, amount, coinPairConfig);
		WalletAccount fromAccount = decreaseQuoteCoinLockBalance(order, amount, coinPairConfig);
		savePaymentRecord(fromAccount, toAccount, amount, order.getOrderId(), fromAccount.getCoinType(), order.getOrderId() + "_returnRemain");
	}

	@Override
	public void returnRemainBaseCoinBalance(OrderModel order, BigDecimal amount, CoinPairConfig coinPairConfig)  throws AccountException{
		WalletAccount toAccount = addBaseCoinBalance(order, amount, coinPairConfig);
		WalletAccount fromAccount = decreaseBaseCoinLockBalance(order, amount, coinPairConfig);
		savePaymentRecord(fromAccount, toAccount, amount, order.getOrderId(), fromAccount.getCoinType(), order.getOrderId() + "_returnRemain");
	}

	@Override
	public void buyMatchingUpdateLockBalance(OrderModel buyOrder, OrderModel sellOrder, BigDecimal baseVolume,
			BigDecimal quoteAmount,BigDecimal fee,TradeModel trade, CoinPairConfig coinPairConfig) throws AccountException{
		WalletAccount fromAccount = decreaseQuoteCoinLockBalance(buyOrder, quoteAmount, coinPairConfig);
		BigDecimal changeQuoteAmount = quoteAmount.subtract(fee);
		WalletAccount toAccount = addQutoeCoinBalance(sellOrder, changeQuoteAmount, coinPairConfig);
		WalletAccount toAccountFee = addQutoeCoinFeeBalance(sellOrder, fee, coinPairConfig);
		savePaymentRecord(fromAccount, toAccount, changeQuoteAmount, buyOrder.getOrderId(), fromAccount.getCoinType(), buyOrder.getOrderId() + "_" + trade.getTradeId());
		savePaymentRecord(fromAccount, toAccountFee, fee, buyOrder.getOrderId(), fromAccount.getCoinType(), buyOrder.getOrderId() + "_" + trade.getTradeId());
	}

	@Override
	public void sellMatchingUpdateLockBalance(OrderModel buyOrder, OrderModel sellOrder, BigDecimal baseVolume,
			BigDecimal quoteAmount,BigDecimal fee,TradeModel trade, CoinPairConfig coinPairConfig)  throws AccountException{
		WalletAccount fromAccount = decreaseBaseCoinLockBalance(sellOrder, baseVolume, coinPairConfig);
		BigDecimal changeBaseVolume = baseVolume.subtract(fee);
		WalletAccount toAccount = addBaseCoinBalance(buyOrder, changeBaseVolume, coinPairConfig);
		WalletAccount toAccountFee = addBaseCoinFeeBalance(buyOrder, fee, coinPairConfig);
		
		savePaymentRecord(fromAccount, toAccount, changeBaseVolume, sellOrder.getOrderId(), fromAccount.getCoinType(), sellOrder.getOrderId() + "_" + trade.getTradeId());
		savePaymentRecord(fromAccount, toAccountFee, fee, sellOrder.getOrderId(), fromAccount.getCoinType(), sellOrder.getOrderId() + "_" + trade.getTradeId());
	}
	
	private void savePaymentRecord(WalletAccount fromAccount,WalletAccount toAccount,BigDecimal amount,String orderId,String coinType,String businessNumber) {
		PaymentRecord buyfeePaymentRecord = new PaymentRecord();

		buyfeePaymentRecord.setReceiptAccountId(toAccount.getAccountId());
		buyfeePaymentRecord.setReceiptUserId(toAccount.getUserId());
		buyfeePaymentRecord.setReceiptUserName("");

		buyfeePaymentRecord.setPaymentAccountId(fromAccount.getAccountId());
		buyfeePaymentRecord.setPayUserId(fromAccount.getUserId());
		buyfeePaymentRecord.setPayUserName("");

		buyfeePaymentRecord.setPaymentId(UUIDUtils.getUuid());
		buyfeePaymentRecord.setAmount(amount);
		buyfeePaymentRecord.setOrderId(orderId);
		buyfeePaymentRecord.setPaymentDate(new Date());
		buyfeePaymentRecord.setCoinType(coinType);
		buyfeePaymentRecord.setBusinessNumber(businessNumber);
		buyfeePaymentRecord.setPaymentType(BusinessEnum.PaymentRecordType.exchangeFee.getCode());
		paymentRecordMapper.insertSelective(buyfeePaymentRecord);
	}

	@Override
	public void lockAccountByOrder(OrderModel orderData, CoinPairConfig coinPairConfig) throws OrderException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public StatsSymbolRate selectOneSymbolRate(CoinPairConfig coinPairConfig) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DebtOrderModel> selectByUnfinishedDebtOrder(CoinPairConfig coinPairConfig) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void outOfStock(String userId, CoinPairConfig coinPairConfig) throws AccountException, OrderException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FinancingAccountBalance selectFinancingAccountBalance(String userId, CoinPairConfig coinPairConfig) {
		// TODO Auto-generated method stub
		return null;
	}


}
