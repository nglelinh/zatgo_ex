package com.zatgo.zup.matching.init;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.aliyun.openservices.shade.com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.matching.config.CoinPairConfig;
import com.zatgo.zup.matching.engine.MatchingEngineManager;
import com.zatgo.zup.matching.engine.WithFundingEngineManager;

@Component
public class ApplicationStartup{
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationStartup.class);

	@Value("${system.matching.symbol}")
	private String symbol;
	@Value("${system.matching.baseCoin}")
	private String baseCoin;
	@Value("${system.matching.quoteCoin}")
	private String quoteCoin;
	@Value("${system.matching.sysUid}")
	private String  sysUid;                    //公司账户UID
	
	//币币交易
	@Value("${system.matching.isOpenMatching:false}")
	private Boolean isOpenMatching;
	@Value("${system.matching.sysBaseAccount}")
	private String sysBaseAccount;   //公司基准货币账户，用于接受手续费
	@Value("${system.matching.sysQuoteAccount}")
	private String sysQuoteAccount;   //公司计价货币账户，用于接受手续费
	@Value("${system.matching.baseAccountNormal}")
	private String baseAccountNormal;  //基准货币账户正常
	@Value("${system.matching.baseAccountLock}")
	private String baseAccountLock;//基准货币账户冻结
	@Value("${system.matching.quoteAccountNormal}")
	private String quoteAccountNormal;//计价货币账户正常
	@Value("${system.matching.quoteAccountLock}")
	private String quoteAccountLock;//计价货币账户冻结
	
	//配资交易
	@Value("${system.matching.isOpenFinancing:false}")
	private Boolean isOpenFinancing;
	@Value("${system.matching.sysBaseFinancingAccount}")
	private String sysBaseFinancingAccount;   //公司基准货币配资账户，用于接受爆仓或还款金额
	@Value("${system.matching.sysQuoteFinancingAccount}")
	private String sysQuoteFinancingAccount;   //公司计价货币配资账户，用于接受爆仓或还款金额
	@Value("${system.matching.sysBaseFinancingFeeAccount}")
	private String sysBaseFinancingFeeAccount;   //公司基准货币配资账户，用于接受利息
	@Value("${system.matching.sysQuoteFinancingFeeAccount}")
	private String sysQuoteFinancingFeeAccount;   //公司计价货币配资账户，用于接受利息
	@Value("${system.matching.baseFinancingAccountNormal}")
	private String baseFinancingAccountNormal;  //基准货币配资账户正常
	@Value("${system.matching.baseFinancingAccountLock}")
	private String baseFinancingAccountLock;//基准货币配资账户冻结
	@Value("${system.matching.quoteFinancingAccountNormal}")
	private String quoteFinancingAccountNormal;//计价货币配资账户正常
	@Value("${system.matching.quoteFinancingAccountLock}")
	private String quoteFinancingAccountLock;//计价货币配资账户冻结
	
	
	
	@Autowired
	private MatchingEngineManager matchingEngineManager;
	
	@Autowired
	private WithFundingEngineManager withFundingEngineManager;
	
	@PostConstruct
	public void startup() {

    	
    	CoinPairConfig config = new CoinPairConfig();
    	config.setSymbol(symbol);
    	config.setBaseCoin(baseCoin);
    	config.setQuoteCoin(quoteCoin);
    	config.setSysUid(sysUid);                
    	config.setSysBaseAccount(sysBaseAccount);  
    	config.setSysQuoteAccount(sysQuoteAccount);
    	config.setBaseAccountNormal(baseAccountNormal);
    	config.setBaseAccountLock(baseAccountLock);
    	config.setQuoteAccountNormal(quoteAccountNormal);
    	config.setQuoteAccountLock(quoteAccountLock);
    	config.setExchangeType(ExchangeEnum.ExchangeType.Bitshop.getCode());

    	config.setSysBaseFinancingAccount(sysBaseFinancingAccount);  
    	config.setSysQuoteFinancingAccount(sysQuoteFinancingAccount);
    	config.setSysBaseFinancingFeeAccount(sysBaseFinancingFeeAccount);  
    	config.setSysQuoteFinancingFeeAccount(sysQuoteFinancingFeeAccount);
    	config.setBaseFinancingAccountNormal(baseFinancingAccountNormal);
    	config.setBaseFinancingAccountLock(baseFinancingAccountLock);
    	config.setQuoteFinancingAccountNormal(quoteFinancingAccountNormal);
    	config.setQuoteFinancingAccountLock(quoteFinancingAccountLock);
    	
    	
    	logger.info("before start,config={}",JSON.toJSONString(config));
    	
    	if(isOpenMatching) {
    		Thread matchingThread = new Thread(new Runnable() {
    			@Override
    			public void run() {
    				logger.info("matching start");
    				matchingEngineManager.run(config);
    			}
    		});
        	matchingThread.start();
    	}else {
    		logger.info("不启动币币撮合业务");
    	}
    	
    	
    	if(isOpenFinancing) {
    		Thread withFundingThread = new Thread(new Runnable() {
    			@Override
    			public void run() {
    				logger.info("with funding out of stock start");
    				withFundingEngineManager.run(config);
    			}
    		});
        	withFundingThread.start();
    	}else {
    		logger.info("不启动爆仓检查业务");
    	}
    	
	}

}
