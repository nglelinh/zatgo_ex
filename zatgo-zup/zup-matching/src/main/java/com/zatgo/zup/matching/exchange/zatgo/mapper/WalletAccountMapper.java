package com.zatgo.zup.matching.exchange.zatgo.mapper;


import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.zatgo.zup.matching.exchange.zatgo.entity.WalletAccount;

@Mapper
public interface WalletAccountMapper {
    int deleteByPrimaryKey(String accountId);

    int insert(WalletAccount record);

    int insertSelective(WalletAccount record);

    WalletAccount selectByPrimaryKey(String accountId);

    int updateByPrimaryKeySelective(WalletAccount record);

    int updateByPrimaryKey(WalletAccount record);

//	@Select("select * from wallet_account where user_id = #{userId} and coin_type = #{type}")
	WalletAccount selectByUserIdAndType(@Param("userId")String userId,@Param("type") String type);

	@Update("update wallet_account set balance = balance + (#{amount}) where account_id = #{accountId}")
	int updateBalance(@Param("accountId") String accountId, @Param("amount")BigDecimal amount);

	@Update("update wallet_account set balance = balance - #{amount} where account_id = #{accountId}")
	int decreaseBalance(@Param("accountId") String accountId, @Param("amount")BigDecimal amount);
	
	@Update("update wallet_account set lock_balance = lock_balance - #{amount} where account_id = #{accountId}")
	int decreaseLockBalance(@Param("accountId") String accountId, @Param("amount")BigDecimal amount);

	WalletAccount selectByUserIdAndTypeForUpdate(@Param("userId")String userId,@Param("type") String type);

	WalletAccount selectByAccountIdForUpdate(@Param("accountId")String accountId);
	@Update("UPDATE wallet_account set is_primary = #{status} WHERE account_id = #{accountId}")
	int setPrimaryAccount(@Param("accountId")String accountId,@Param("status") Byte status);

	WalletAccount selectByUserPrimaryAccount(@Param("userId")String userId);

	@Update("update wallet_account set balance = balance + (#{amount}) where account_id = #{accountId}")
	void addBalance(@Param("accountId") String accountId, @Param("amount")BigDecimal amount);

	WalletAccount selectByAddress(@Param("address") String address,@Param("coinType") String coinType);
	List<WalletAccount> selectAccountNeedAggregate(@Param("coinType") String coinType,@Param("amount") BigDecimal amount);

}