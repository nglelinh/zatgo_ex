package com.zatgo.zup.matching.comparator;

import java.util.Comparator;

import com.zatgo.zup.matching.model.OrderModel;

public class OrderTimeComparator implements Comparator<OrderModel> {

	@Override
	public int compare(OrderModel o1, OrderModel o2) {
		int sort = o1.getCreateDate().compareTo(o2.getCreateDate());
		if(sort == 0) {
			sort = -1;
		}
		
		return sort;
	} 

}
