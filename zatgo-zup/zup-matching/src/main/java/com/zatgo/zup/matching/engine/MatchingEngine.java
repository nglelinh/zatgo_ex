package com.zatgo.zup.matching.engine;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.utils.DateTimeUtils;
import com.zatgo.zup.matching.config.CoinPairConfig;
import com.zatgo.zup.matching.exception.AccountException;
import com.zatgo.zup.matching.exception.OrderException;
import com.zatgo.zup.matching.exchange.ExchangeStrategy;
import com.zatgo.zup.matching.exchange.bitshop.entity.Account;
import com.zatgo.zup.matching.model.OrderModel;
import com.zatgo.zup.matching.model.TradeModel;

@Component
public class MatchingEngine {
	
	private static final Logger logger = LoggerFactory.getLogger(MatchingEngine.class);

	private CoinPairConfig coinPairConfig;
	
	private OrderBook buyBook;
	
	private OrderBook sellBook;
	
	@Autowired
	private ExchangeStrategy exchangeStrategy;
	
	public MatchingEngine(){
		this.buyBook = new OrderBook(ExchangeSide.BUY);
		this.sellBook = new OrderBook(ExchangeSide.SELL);
	}
	
	public void initConfig(CoinPairConfig coinPairConfig) {
		this.coinPairConfig = coinPairConfig;
	}
	
	public void reset() {
		buyBook.clear();
		sellBook.clear();
	}
	
	public void removeBook(OrderModel order) {
		ExchangeSide side = ExchangeSide.getEnumByType(order.getSide());
		OrderBook book = (side == ExchangeSide.BUY ? this.buyBook : this.sellBook);
		book.removeOrder(order);
	}
	
	private OrderBook getBook(ExchangeEnum.ExchangeSide side) {
		return side == ExchangeEnum.ExchangeSide.BUY ? this.buyBook : this.sellBook;
	}

	@Transactional(propagation=Propagation.REQUIRED, isolation=Isolation.REPEATABLE_READ, rollbackFor={Throwable.class})
	public void acceptOrder(OrderModel orderData) throws Throwable{
		ExchangeSide side = ExchangeSide.getEnumByType(orderData.getSide());
		
		OrderBook counterBook = (side == ExchangeSide.BUY ? this.sellBook : this.buyBook);
		
		OrderModel unMatchedOrder = tryMatchIteration(orderData, counterBook);
		if (unMatchedOrder.isLimitOrder()) {
			if (!unMatchedOrder.isFilled(coinPairConfig)) {
				getBook(side).add(unMatchedOrder);
			}
		} else {
			cancelMarketRemain(unMatchedOrder);
		}
	}
	
	public void changeOrderToException(OrderModel order) {
		OrderModel changeOrder = new OrderModel();
		changeOrder.setOrderId(order.getOrderId());
		changeOrder.setStatus(Integer.valueOf(ExchangeEnum.OrderStatus.EXCEPTION.getCode()).byteValue());
		exchangeStrategy.getExchangeService(coinPairConfig).updateOrderByMatchingStatusAndPrimaryKeySelective(changeOrder, coinPairConfig);
	}
	
	private OrderModel tryMatchIteration(OrderModel orderData, OrderBook counterBook) throws Throwable {
		boolean isUpdateOrder = false;
		boolean isOrderLock = false;
		while(!orderData.isFilled(coinPairConfig)) {
			OrderModel topCounterOrder = counterBook.getTopOrder();
			if(topCounterOrder == null) {
				break;
			}
			
			TradeModel trade = tryTrade(orderData, topCounterOrder);
			
			if(trade == null) {
				break;
			}
			
			//锁撮合订单和相应需要扣减的账户，保证其它系统死锁（所有系统规则都是先获取订单的锁，再获取订单相应账户的锁）
			if(!isOrderLock) {
				exchangeStrategy.getExchangeService(coinPairConfig).getLockOrderById(orderData.getOrderId(),coinPairConfig);
				exchangeStrategy.getExchangeService(coinPairConfig).lockAccountByOrder(orderData, coinPairConfig);
				isOrderLock = true;
			}
			exchangeStrategy.getExchangeService(coinPairConfig).getLockOrderById(topCounterOrder.getOrderId(),coinPairConfig);
			exchangeStrategy.getExchangeService(coinPairConfig).lockAccountByOrder(topCounterOrder, coinPairConfig);

			BigDecimal tradeBaseQuantity = trade.getVolume();
	        BigDecimal tradeQuoteQuantity = trade.getVolume().multiply(trade.getPrice());
	        BigDecimal baseFeeQuantity;
	        BigDecimal quoteFeeQuantity;
	        if(orderData.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
	        	baseFeeQuantity = tradeBaseQuantity.multiply(BigDecimal.valueOf(orderData.getFeeRateTaker().doubleValue()));
	            quoteFeeQuantity = tradeQuoteQuantity.multiply(BigDecimal.valueOf(topCounterOrder.getFeeRateMaker().doubleValue()));
	        }else {
	        	baseFeeQuantity = tradeBaseQuantity.multiply(BigDecimal.valueOf(topCounterOrder.getFeeRateMaker().doubleValue()));
	        	quoteFeeQuantity = tradeQuoteQuantity.multiply(BigDecimal.valueOf(orderData.getFeeRateTaker().doubleValue()));
	        }
	        
	        trade.setBuyFee(baseFeeQuantity);
	        trade.setBuyFeeCoin(this.coinPairConfig.getBaseCoin());
	        trade.setSellFee(quoteFeeQuantity);
	        trade.setSellFeeCoin(this.coinPairConfig.getQuoteCoin());
	        exchangeStrategy.getExchangeService(coinPairConfig).insertTrade(trade,coinPairConfig);
	        
	        
	        changeAccountBalance(orderData,topCounterOrder,trade);
	        
	        counterBook.removeTop(trade);
	        
	        changeOrderItself(topCounterOrder,trade);
	        changeOrderItself(orderData,trade);
	        
	        isUpdateOrder = true;
	        
	        int affectRows = exchangeStrategy.getExchangeService(coinPairConfig).updateOrderByMatchingStatusAndPrimaryKeySelective(topCounterOrder,coinPairConfig);
	        if(affectRows == 0) {
	        	throw new OrderException("change order status fail", topCounterOrder);
	        }
	        
			if (topCounterOrder.isFilled(coinPairConfig)) {
				returnRemain(topCounterOrder);
			}
		}
		
		if (orderData.isFilled(coinPairConfig)) {
			returnRemain(orderData);
		}
		
		//如果撮合过，就需要更新订单信息
		if(isUpdateOrder == true) {
			exchangeStrategy.getExchangeService(coinPairConfig).getLockOrderById(orderData.getOrderId(),coinPairConfig);
			int affectRows = exchangeStrategy.getExchangeService(coinPairConfig).updateOrderByMatchingStatusAndPrimaryKeySelective(orderData,coinPairConfig);
			if(affectRows == 0) {
	        	throw new OrderException("change order status fail", orderData);
	        }
		}
		

		return orderData;
	}

	
	/**
	 * 市价单取消
	 * @param order
	 */
	private void cancelMarketRemain(OrderModel order)  throws AccountException,OrderException{
		if(!order.isMarketOrder()) {
			return;
		}
		returnRemain(order);
		Integer status = Integer.valueOf(ExchangeEnum.OrderStatus.CANCELED.getCode());
		order.setStatus(status.byteValue());
		Integer num = exchangeStrategy.getExchangeService(coinPairConfig).updateOrderByMatchingStatusAndPrimaryKeySelective(order,coinPairConfig);
		if(num == 0) {
			throw new OrderException("market order status is not in (1,3)", order);
		}
	}
	
	/**
	 * 订单未完成数据退回
	 * @param order
	 */
	private void returnRemain(OrderModel order) throws AccountException,OrderException{
		if (order.isLimitOrder()) {
			if (order.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
				BigDecimal totalQuoteAmount = order.getPrice().multiply(order.getVolume());
				BigDecimal remainQuoteAmount = totalQuoteAmount.subtract(order.getDealMoney());
				if (remainQuoteAmount.compareTo(BigDecimal.ZERO) > 0) {
					exchangeStrategy.getExchangeService(coinPairConfig).returnRemainQuoteCoinBalance(order, remainQuoteAmount, coinPairConfig);
				}
			}else {
				BigDecimal totalBaseAmount = order.getVolume();
				BigDecimal remainBaseAmount = totalBaseAmount.subtract(order.getDealVolume());
				if (remainBaseAmount.compareTo(BigDecimal.ZERO) > 0) {
					exchangeStrategy.getExchangeService(coinPairConfig).returnRemainBaseCoinBalance(order, remainBaseAmount, coinPairConfig);
				}
			}
		} else if (order.isMarketOrder()) {
			if (order.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
				BigDecimal remainQuoteAmount = order.getVolume().subtract(order.getDealMoney());
				if (remainQuoteAmount.compareTo(BigDecimal.ZERO) > 0) {
					exchangeStrategy.getExchangeService(coinPairConfig).returnRemainQuoteCoinBalance(order, remainQuoteAmount, coinPairConfig);
				}
			} else {
				BigDecimal remainBaseVolume = order.getVolume().subtract(order.getDealVolume());
				if (remainBaseVolume.compareTo(BigDecimal.ZERO) > 0) {
					exchangeStrategy.getExchangeService(coinPairConfig).returnRemainBaseCoinBalance(order, remainBaseVolume, coinPairConfig);
				}
			}
		}
	}
	
	private void changeOrderItself(OrderModel order, TradeModel trade) {
		BigDecimal tradeBaseVolume = trade.getVolume();
		BigDecimal tradeQuoteAmount = tradeBaseVolume.multiply(trade.getPrice());
		order.addFilledQuntity(tradeBaseVolume);
		order.addFilledMoney(tradeQuoteAmount);
		BigDecimal orderAvgPrice = order.getDealMoney().divide(order.getDealVolume(), 8, BigDecimal.ROUND_HALF_EVEN);
		order.setAvgPrice(orderAvgPrice);
		if (order.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
			order.setFee(order.getFee().add(trade.getBuyFee()));
		} else {
			order.setFee(order.getFee().add(trade.getSellFee()));
		}
		
		ExchangeEnum.OrderStatus orderStatus = ExchangeEnum.OrderStatus.NEW;
		if (order.isFilled(coinPairConfig)) {
			orderStatus = ExchangeEnum.OrderStatus.FILLED;
		} else if (order.getDealVolume().compareTo(new BigDecimal("0")) > 0) {
			orderStatus = ExchangeEnum.OrderStatus.PART_FILLED;
		}
		order.setStatus(Integer.valueOf(orderStatus.getCode()).byteValue());
	}
	
	/**
	 * 修改账户余额
	 * @param order
	 * @param trade
	 * @throws AccountException
	 */
	private void changeAccountBalance(OrderModel order,OrderModel topCounterOrder, TradeModel trade) throws AccountException,OrderException {
		BigDecimal baseVolume = trade.getVolume();
	    BigDecimal quoteAmount = baseVolume.multiply(trade.getPrice());
	    
		if(order.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
			exchangeStrategy.getExchangeService(coinPairConfig).buyMatchingUpdateLockBalance(
					order, topCounterOrder, baseVolume, quoteAmount, trade.getSellFee(),trade, coinPairConfig);
			
			exchangeStrategy.getExchangeService(coinPairConfig).sellMatchingUpdateLockBalance(
					order, topCounterOrder, baseVolume, quoteAmount, trade.getBuyFee(),trade, coinPairConfig);
			
		}else {
			exchangeStrategy.getExchangeService(coinPairConfig).buyMatchingUpdateLockBalance(
					topCounterOrder, order, baseVolume, quoteAmount, trade.getSellFee(),trade, coinPairConfig);

			exchangeStrategy.getExchangeService(coinPairConfig).sellMatchingUpdateLockBalance(
					topCounterOrder, order, baseVolume, quoteAmount, trade.getBuyFee(),trade, coinPairConfig);

		}
	}
	
	private TradeModel tryTrade(OrderModel orderData,OrderModel bestCounterOrder) throws OrderException {
		
		if(orderData.isLimitOrder()) {
			BigDecimal bestCounterLimitPrice = bestCounterOrder.getPrice();
			if(orderData.matchingPriceCheck(bestCounterLimitPrice)) {
				TradeModel tradeData = createTrade(orderData,bestCounterOrder);
				return tradeData;
			}else {
				return null;
			}
		}else if(orderData.isMarketOrder()) {
			TradeModel tradeData = createTrade(orderData,bestCounterOrder);
			return tradeData;
		}else {
			throw new OrderException("error order data:" + JSON.toJSONString(orderData),orderData);
		}
		
	}
	
	private TradeModel createTrade(OrderModel orderData,OrderModel bestCounterOrder) throws OrderException{
		if(orderData.getSide().equals(bestCounterOrder.getSide())) {
			throw new BusinessException();
		}
		
		Date currDate = DateTimeUtils.getCurrentDate();
		
		if(orderData.isLimitOrder()) {//限价单
			BigDecimal orderQuantity = orderData.getUnfilledQuantity();
			BigDecimal counterOrderQuantity = bestCounterOrder.getUnfilledQuantity();
			BigDecimal canTradeQuantity = orderQuantity.min(counterOrderQuantity);
			
			TradeModel trade = new TradeModel();
			
			trade.setPrice(bestCounterOrder.getPrice());
			trade.setVolume(canTradeQuantity);
			if(orderData.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
				trade.setBidOrderId(orderData.getOrderId());
		        trade.setBidUserId(orderData.getUserId());
		        trade.setAskOrderId(bestCounterOrder.getOrderId());
		        trade.setAskUserId(bestCounterOrder.getUserId());
			}else{
				trade.setBidOrderId(bestCounterOrder.getOrderId());
		        trade.setBidUserId(bestCounterOrder.getUserId());
		        trade.setAskOrderId(orderData.getOrderId());
		        trade.setAskUserId(orderData.getUserId());
			}
			
			trade.setTrendSide(orderData.getSide());
			trade.setCreateTime(currDate);
			trade.setBuyFee(new BigDecimal("0"));
			trade.setSellFee(new BigDecimal("0"));
			
			return trade;
		}else if(orderData.isMarketOrder()) {//市价单
			TradeModel trade = new TradeModel();
			trade.setPrice(bestCounterOrder.getPrice());
			
			BigDecimal counterOrderUnfilledQuantity = bestCounterOrder.getUnfilledQuantity();
			BigDecimal counterOrderUnfilledQuantityToCounter = counterOrderUnfilledQuantity.multiply(bestCounterOrder.getPrice());
			
			BigDecimal canTradeQuantity = BigDecimal.ZERO;
			if(orderData.getSide().equals(ExchangeEnum.ExchangeSide.BUY.getCode())) {
				BigDecimal orderUnfilledQuantity = orderData.getVolume().subtract(orderData.getDealMoney());
				if(counterOrderUnfilledQuantityToCounter.compareTo(orderUnfilledQuantity) <= 0) {
					canTradeQuantity = counterOrderUnfilledQuantity;
				}else {
					canTradeQuantity = orderUnfilledQuantity.divide(bestCounterOrder.getPrice(),18,BigDecimal.ROUND_HALF_EVEN);
					BigDecimal overReminderQuantity = canTradeQuantity.divideAndRemainder(coinPairConfig.MIN_TRADE_AMOUNT)[1];
					if(overReminderQuantity.compareTo(BigDecimal.ZERO) >= 0) {
						canTradeQuantity = canTradeQuantity.subtract(overReminderQuantity);
					}else{
						throw new OrderException("code fail", orderData);
					}
				}
				
				//市价买单最后一次撮合如果不能再买大于0的数量，则结束当前市价单的撮合
				if(canTradeQuantity.compareTo(BigDecimal.ZERO) <= 0) {
					return null;
				}
				
				trade.setVolume(canTradeQuantity);
		        trade.setBidOrderId(orderData.getOrderId());
		        trade.setBidUserId(orderData.getUserId());
		        trade.setAskOrderId(bestCounterOrder.getOrderId());
		        trade.setAskUserId(bestCounterOrder.getUserId());
			}else {
				BigDecimal orderUnfilledQuantity = orderData.getUnfilledQuantity();
		        canTradeQuantity = orderUnfilledQuantity.min(counterOrderUnfilledQuantity);
		        trade.setVolume(canTradeQuantity);
		        trade.setBidOrderId(bestCounterOrder.getOrderId());
		        trade.setBidUserId(bestCounterOrder.getUserId());
		        trade.setAskOrderId(orderData.getOrderId());
		        trade.setAskUserId(orderData.getUserId());
			}
			
			trade.setTrendSide(orderData.getSide());
		    trade.setCreateTime(currDate);
		    trade.setBuyFee(new BigDecimal("0"));
		    trade.setSellFee(new BigDecimal("0"));
		    
		    return trade;
		}else {
			throw new OrderException("order data fail:", orderData);
		}

	}
	
}
