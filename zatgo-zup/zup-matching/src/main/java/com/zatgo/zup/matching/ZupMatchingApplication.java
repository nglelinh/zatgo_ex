package com.zatgo.zup.matching;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@EnableEurekaClient
//@EnableFeignClients
//@EnableHystrix
//@EnableSwagger2
@MapperScan("com.zatgo.zup.matching")
@ComponentScan(basePackages = "com.zatgo.zup")
@EnableTransactionManagement
@Configuration
public class ZupMatchingApplication 
{
	private static final Logger logger = LoggerFactory.getLogger(ZupMatchingApplication.class);
	
    public static void main( String[] args )
    {
    	SpringApplication.run(ZupMatchingApplication.class, args);
    }
}