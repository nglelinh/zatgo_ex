package com.zatgo.zup.matching.model;

import java.math.BigDecimal;

public class DebtOrderModel {

	private String userId;
	
	private String baseSymbol;

    private String quoteSymbol;

    private BigDecimal baseSumDebtAmount;
    
    private BigDecimal baseBalanceDebtAmount;
    
    private BigDecimal quoteSumDebtAmount;
    
    private BigDecimal quoteBalanceDebtAmount;


	public BigDecimal getBaseSumDebtAmount() {
		return baseSumDebtAmount;
	}

	public void setBaseSumDebtAmount(BigDecimal baseSumDebtAmount) {
		this.baseSumDebtAmount = baseSumDebtAmount;
	}

	public BigDecimal getBaseBalanceDebtAmount() {
		return baseBalanceDebtAmount;
	}

	public void setBaseBalanceDebtAmount(BigDecimal baseBalanceDebtAmount) {
		this.baseBalanceDebtAmount = baseBalanceDebtAmount;
	}

	public BigDecimal getQuoteSumDebtAmount() {
		return quoteSumDebtAmount;
	}

	public void setQuoteSumDebtAmount(BigDecimal quoteSumDebtAmount) {
		this.quoteSumDebtAmount = quoteSumDebtAmount;
	}

	public BigDecimal getQuoteBalanceDebtAmount() {
		return quoteBalanceDebtAmount;
	}

	public void setQuoteBalanceDebtAmount(BigDecimal quoteBalanceDebtAmount) {
		this.quoteBalanceDebtAmount = quoteBalanceDebtAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBaseSymbol() {
		return baseSymbol;
	}

	public void setBaseSymbol(String baseSymbol) {
		this.baseSymbol = baseSymbol;
	}

	public String getQuoteSymbol() {
		return quoteSymbol;
	}

	public void setQuoteSymbol(String quoteSymbol) {
		this.quoteSymbol = quoteSymbol;
	}
    
}
