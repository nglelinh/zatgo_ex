package com.zatgo.zup.matching.exception;

public class MatchingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MatchingException(String msg) {
		super(msg);
	}

}
