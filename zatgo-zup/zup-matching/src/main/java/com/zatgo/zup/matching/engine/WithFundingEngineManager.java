package com.zatgo.zup.matching.engine;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.matching.config.CoinPairConfig;
import com.zatgo.zup.matching.exception.AccountException;
import com.zatgo.zup.matching.exception.OrderException;
import com.zatgo.zup.matching.exchange.ExchangeService;
import com.zatgo.zup.matching.exchange.ExchangeStrategy;
import com.zatgo.zup.matching.exchange.bitshop.entity.StatsSymbolRate;
import com.zatgo.zup.matching.model.DebtOrderModel;
import com.zatgo.zup.matching.model.FinancingAccountBalance;

@Component
public class WithFundingEngineManager {

	private static final Logger logger = LoggerFactory.getLogger(WithFundingEngineManager.class);
	
	@Autowired
	private ExchangeStrategy exchangeStrategy;
	
	private CoinPairConfig config;
	
	public void run(CoinPairConfig config) {
		this.config = config;
		
		while(true) {
			logger.debug("start out of stock checking");
			try {
				ExchangeService exchangeService = exchangeStrategy.getExchangeService(config);
				
				List<DebtOrderModel> list = exchangeService.selectByUnfinishedDebtOrder(config);
				
				StatsSymbolRate statsSymbolRate = exchangeService.selectOneSymbolRate(config);
				
				if(statsSymbolRate == null) {
					throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "coin pair is not rate, symbol=" + 
							config.getBaseCoin().toLowerCase() + config.getQuoteCoin().toLowerCase());
				}
				
				for(DebtOrderModel model:list) {
					FinancingAccountBalance balance = exchangeService.selectFinancingAccountBalance(model.getUserId(), config);
					
					BigDecimal baseCoinAmount = balance.getBaseCoinBalance().add(balance.getBaseCoinLockBalance());
					BigDecimal quoteCoinAmount = balance.getQuoteCoinBalance().add(balance.getQuoteCoinLockBalance());
					
					BigDecimal sumBalanceQuoteCoin = quoteCoinAmount.add(baseCoinAmount.multiply(statsSymbolRate.getRate()));
					BigDecimal quoteBalanceDebtAmount = model.getQuoteBalanceDebtAmount() == null?BigDecimal.ZERO:model.getQuoteBalanceDebtAmount();
					BigDecimal baseBalanceDebtAmount = model.getBaseBalanceDebtAmount() == null?BigDecimal.ZERO:model.getBaseBalanceDebtAmount();
					BigDecimal sumBalanceQuoteCoinDebt = quoteBalanceDebtAmount.add(baseBalanceDebtAmount.multiply(statsSymbolRate.getRate()));
					
					BigDecimal outOfStockAmount = sumBalanceQuoteCoinDebt.multiply(new BigDecimal("1.1"));//爆仓值（借款金额*1.1）

					if(sumBalanceQuoteCoin.compareTo(outOfStockAmount) <= 0) {
						logger.info("out of stock,userId=" + model.getUserId() + " coinPairSymbol=" + 
								config.getBaseCoin().toLowerCase() + config.getQuoteCoin().toLowerCase() + " debt=" + JSON.toJSONString(model));
						exchangeService.outOfStock(model.getUserId(), config);
					}
				}
				
				
				logger.debug("end out of stock checking");
				Thread.sleep(1000);
				
			}catch(Throwable e) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					
				}
				logger.error("",e);
			}
		}
	}

}
