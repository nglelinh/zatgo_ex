package com.zatgo.zup.matching.exchange.bitshop.entity;

import java.math.BigDecimal;
import java.util.Date;

public class WithFundingLoan {
    private Long id;

    private Integer uid;

    private String symbol;

    private String coin;

    private BigDecimal amount;

    private BigDecimal rate;

    private BigDecimal noReturnAmount;

    private Byte status;

    private Date ctime;

    private Date mtime;

    private Date loanTime;

    public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin == null ? null : coin.trim();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getNoReturnAmount() {
        return noReturnAmount;
    }

    public void setNoReturnAmount(BigDecimal noReturnAmount) {
        this.noReturnAmount = noReturnAmount;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getMtime() {
        return mtime;
    }

    public void setMtime(Date mtime) {
        this.mtime = mtime;
    }

    public Date getLoanTime() {
        return loanTime;
    }

    public void setLoanTime(Date loanTime) {
        this.loanTime = loanTime;
    }
}