package com.zatgo.zup.matching.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import com.zatgo.zup.common.utils.YamlUtils;
import com.zatgo.zup.matching.exception.ConfigParseException;

public class YamlLoader {

	public static CoinPairConfig loadPairConfig(String filePath)
		    throws ConfigParseException
		  {
			CoinPairConfig pairConfig = new CoinPairConfig();
		    try
		    {
		      InputStream input = new FileInputStream(filePath);
		      Map map = (Map)YamlUtils.readYaml(input);
		      pairConfig.setSymbol((String)map.get("symbol"));
		      pairConfig.setBaseCoin((String)map.get("baseCoin"));
		      pairConfig.setQuoteCoin((String)map.get("quoteCoin"));
		    }
		    catch (IOException e)
		    {
		      throw new ConfigParseException(e.getMessage());
		    }
		    catch (ClassCastException e)
		    {
		      throw new ConfigParseException(e.getMessage());
		    }
		    return pairConfig;
		  }
}