package com.zatgo.zup.matching.exchange.bitshop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.matching.exchange.bitshop.entity.WithFundingLoan;
import com.zatgo.zup.matching.exchange.bitshop.entity.WithFundingLoanExample;

@Mapper
public interface WithFundingLoanMapper{
    long countByExample(WithFundingLoanExample example);

    int deleteByExample(WithFundingLoanExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WithFundingLoan record);

    int insertSelective(WithFundingLoan record);

    List<WithFundingLoan> selectByExample(WithFundingLoanExample example);

    WithFundingLoan selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WithFundingLoan record, @Param("example") WithFundingLoanExample example);

    int updateByExample(@Param("record") WithFundingLoan record, @Param("example") WithFundingLoanExample example);

    int updateByPrimaryKeySelective(WithFundingLoan record);

    int updateByPrimaryKey(WithFundingLoan record);
    
    List<WithFundingLoan> selectByStatus(@Param("status") Byte status,@Param("coinPairSymbol") String coinPairSymbol);
    
    List<WithFundingLoan> selectByStatusAndUserIdLock(@Param("userId") Integer userId, @Param("status") Byte status,@Param("coinPairSymbol") String coinPairSymbol);
}