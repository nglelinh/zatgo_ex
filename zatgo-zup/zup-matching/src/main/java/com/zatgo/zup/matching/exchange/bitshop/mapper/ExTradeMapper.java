package com.zatgo.zup.matching.exchange.bitshop.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.matching.exchange.bitshop.entity.ExTrade;

@Mapper
public interface ExTradeMapper{

	int deleteByPrimaryKey(@Param("tradeId") String tradeId,@Param("tableName") String tableName);

    int insert(ExTrade record);

    int insertSelective(ExTrade record);

    ExTrade selectByPrimaryKey(@Param("tradeId") String tradeId,@Param("tableName") String tableName);

    int updateByPrimaryKeySelective(ExTrade record);

    int updateByPrimaryKey(ExTrade record);
}