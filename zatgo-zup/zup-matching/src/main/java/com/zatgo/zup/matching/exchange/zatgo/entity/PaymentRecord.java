package com.zatgo.zup.matching.exchange.zatgo.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class PaymentRecord implements Serializable {
    private String paymentId;

    private String receiptAccountId;

    private String paymentAccountId;

    private String orderId;

    private BigDecimal amount;

    private Date paymentDate;

    private String payUserName;

    private String receiptUserName;

    private String payUserId;

    private String receiptUserId;

    private String coinType;
    
    private String payHash;
    
    private Byte paymentType;
    
    private String businessNumber;

    private static final long serialVersionUID = 1L;

	public String getPayHash() {
		return payHash;
	}

	public void setPayHash(String payHash) {
		this.payHash = payHash;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getReceiptAccountId() {
        return receiptAccountId;
    }

    public void setReceiptAccountId(String receiptAccountId) {
        this.receiptAccountId = receiptAccountId;
    }

    public String getPaymentAccountId() {
        return paymentAccountId;
    }

    public void setPaymentAccountId(String paymentAccountId) {
        this.paymentAccountId = paymentAccountId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPayUserName() {
        return payUserName;
    }

    public void setPayUserName(String payUserName) {
        this.payUserName = payUserName;
    }

    public String getReceiptUserName() {
        return receiptUserName;
    }

    public void setReceiptUserName(String receiptUserName) {
        this.receiptUserName = receiptUserName;
    }

    public String getPayUserId() {
        return payUserId;
    }

    public void setPayUserId(String payUserId) {
        this.payUserId = payUserId;
    }

    public String getReceiptUserId() {
        return receiptUserId;
    }

    public void setReceiptUserId(String receiptUserId) {
        this.receiptUserId = receiptUserId;
    }

    public Byte getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Byte paymentType) {
		this.paymentType = paymentType;
	}

	public String getBusinessNumber() {
		return businessNumber;
	}

	public void setBusinessNumber(String businessNumber) {
		this.businessNumber = businessNumber;
	}

	@Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PaymentRecord other = (PaymentRecord) that;
        return (this.getPaymentId() == null ? other.getPaymentId() == null : this.getPaymentId().equals(other.getPaymentId()))
            && (this.getReceiptAccountId() == null ? other.getReceiptAccountId() == null : this.getReceiptAccountId().equals(other.getReceiptAccountId()))
            && (this.getPaymentAccountId() == null ? other.getPaymentAccountId() == null : this.getPaymentAccountId().equals(other.getPaymentAccountId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
            && (this.getPaymentDate() == null ? other.getPaymentDate() == null : this.getPaymentDate().equals(other.getPaymentDate()))
            && (this.getPayUserName() == null ? other.getPayUserName() == null : this.getPayUserName().equals(other.getPayUserName()))
            && (this.getReceiptUserName() == null ? other.getReceiptUserName() == null : this.getReceiptUserName().equals(other.getReceiptUserName()))
            && (this.getPayUserId() == null ? other.getPayUserId() == null : this.getPayUserId().equals(other.getPayUserId()))
            && (this.getReceiptUserId() == null ? other.getReceiptUserId() == null : this.getReceiptUserId().equals(other.getReceiptUserId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPaymentId() == null) ? 0 : getPaymentId().hashCode());
        result = prime * result + ((getReceiptAccountId() == null) ? 0 : getReceiptAccountId().hashCode());
        result = prime * result + ((getPaymentAccountId() == null) ? 0 : getPaymentAccountId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getPaymentDate() == null) ? 0 : getPaymentDate().hashCode());
        result = prime * result + ((getPayUserName() == null) ? 0 : getPayUserName().hashCode());
        result = prime * result + ((getReceiptUserName() == null) ? 0 : getReceiptUserName().hashCode());
        result = prime * result + ((getPayUserId() == null) ? 0 : getPayUserId().hashCode());
        result = prime * result + ((getReceiptUserId() == null) ? 0 : getReceiptUserId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", paymentId=").append(paymentId);
        sb.append(", receiptAccountId=").append(receiptAccountId);
        sb.append(", paymentAccountId=").append(paymentAccountId);
        sb.append(", orderId=").append(orderId);
        sb.append(", amount=").append(amount);
        sb.append(", paymentDate=").append(paymentDate);
        sb.append(", payUserName=").append(payUserName);
        sb.append(", receiptUserName=").append(receiptUserName);
        sb.append(", payUserId=").append(payUserId);
        sb.append(", receiptUserId=").append(receiptUserId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}