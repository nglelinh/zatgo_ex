package com.zatgo.zup.matching.exchange.zatgo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.matching.exchange.zatgo.entity.User;

@Mapper
public interface UserMapper{

	User selectByCloudUserIdAndManageType(@Param("cloudUserId") String cloudUserId,@Param("cloudManageType")String cloudManageType);
	
	User selectByPrimaryKey(String userId);
}