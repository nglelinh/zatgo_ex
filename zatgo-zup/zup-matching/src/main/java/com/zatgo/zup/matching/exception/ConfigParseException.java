package com.zatgo.zup.matching.exception;

public class ConfigParseException extends MatchingException {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ConfigParseException(String msg) {
		super(msg);
	}

}
