package com.zatgo.zup.matching.exception;

import com.zatgo.zup.matching.model.OrderModel;

public class AccountException extends MatchingException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private OrderModel order;
	
	public AccountException(String msg) {
		super(msg);
	}

	public AccountException(String msg,OrderModel order) {
		super(msg);
		this.order = order;
	}

	public OrderModel getOrder() {
		return order;
	}

	public void setOrder(OrderModel order) {
		this.order = order;
	}
	
	

}
