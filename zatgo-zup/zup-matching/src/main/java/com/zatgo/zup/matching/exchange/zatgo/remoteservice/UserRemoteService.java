package com.zatgo.zup.matching.exchange.zatgo.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;

@FeignClient("zup-merchant")
public interface UserRemoteService {

	@PostMapping("/user/cloud/get/manageUserId/{userId}/{cloudManageType}")
	ResponseData<UserData> getManageUserId(@PathVariable("userId") String userId,@PathVariable("cloudManageType") String cloudManageType);

}
