package com.zatgo.zup.matching.exchange.bitshop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.zatgo.zup.matching.exchange.bitshop.entity.ExOrder;


@Mapper
public interface ExOrderMapper{

	int deleteByPrimaryKey(String orderId,String tableName);

	int insert(ExOrder record);

	int insertSelective(ExOrder record);

	ExOrder selectByPrimaryKey(@Param("orderId") String orderId,@Param("tableName") String tableName);

	int updateByPrimaryKeySelective(ExOrder record);
	
	int updateByMatchingStatusAndPrimaryKeySelective(ExOrder record);

	int updateByPrimaryKey(ExOrder record);
	
	public Page<ExOrder> searchSellOrders(@Param("tableName") String tableName);
	
	public Page<ExOrder> searchBuyOrders(@Param("tableName") String tableName);
	
	public Page<ExOrder> searchMarketOrders(@Param("tableName") String tableName);
	
	public Integer changeOrderStatusToNew(@Param("tableName") String tableName);
	
	public List<ExOrder> searchWithFundingByUserIdAndLock(@Param("userId") String userId,@Param("tableName") String tableName);
	
	public ExOrder getLockOrderById(@Param("orderId") String orderId, @Param("tableName") String tableName);
}