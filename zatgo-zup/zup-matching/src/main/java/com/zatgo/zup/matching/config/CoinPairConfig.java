package com.zatgo.zup.matching.config;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Value;

public class CoinPairConfig {

	private String symbol;
	
	private String baseCoin;
	
	private String baseCoinNetworkType;
	
	private String quoteCoin;
	
	private String quoteCoinNetworkType;
	
	private String  sysUid;                    //公司账户UID
	private String sysBaseAccount;   //公司基准货币账户，用于接受手续费
	private String sysQuoteAccount;   //公司计价货币账户，用于接受手续费
	private String baseAccountNormal;  //基准货币账户正常
	private String baseAccountLock;//基准货币账户冻结
	private String quoteAccountNormal;//计价货币账户正常
	private String quoteAccountLock;//计价货币账户冻结
	
	private String sysBaseFinancingAccount;   //公司基准货币配资账户，用于接受爆仓或还款金额
	private String sysQuoteFinancingAccount;   //公司计价货币配资账户，用于接受爆仓或还款金额
	private String sysBaseFinancingFeeAccount;   //公司基准货币配资账户，用于接受利息
	private String sysQuoteFinancingFeeAccount;   //公司计价货币配资账户，用于接受利息
	private String baseFinancingAccountNormal;  //基准货币配资账户正常
	private String baseFinancingAccountLock;//基准货币配资账户冻结
	private String quoteFinancingAccountNormal;//计价货币配资账户正常
	private String quoteFinancingAccountLock;//计价货币配资账户冻结
	
	public static BigDecimal MIN_TRADE_VOL = new BigDecimal("0.00000001");
	
	public static BigDecimal MIN_TRADE_AMOUNT = new BigDecimal("0.00000001");
	
	public String exchangeType;

	public String getSysBaseFinancingFeeAccount() {
		return sysBaseFinancingFeeAccount;
	}

	public void setSysBaseFinancingFeeAccount(String sysBaseFinancingFeeAccount) {
		this.sysBaseFinancingFeeAccount = sysBaseFinancingFeeAccount;
	}

	public String getSysQuoteFinancingFeeAccount() {
		return sysQuoteFinancingFeeAccount;
	}

	public void setSysQuoteFinancingFeeAccount(String sysQuoteFinancingFeeAccount) {
		this.sysQuoteFinancingFeeAccount = sysQuoteFinancingFeeAccount;
	}

	public String getSysBaseFinancingAccount() {
		return sysBaseFinancingAccount;
	}

	public void setSysBaseFinancingAccount(String sysBaseFinancingAccount) {
		this.sysBaseFinancingAccount = sysBaseFinancingAccount;
	}

	public String getSysQuoteFinancingAccount() {
		return sysQuoteFinancingAccount;
	}

	public void setSysQuoteFinancingAccount(String sysQuoteFinancingAccount) {
		this.sysQuoteFinancingAccount = sysQuoteFinancingAccount;
	}

	public String getBaseFinancingAccountNormal() {
		return baseFinancingAccountNormal;
	}

	public void setBaseFinancingAccountNormal(String baseFinancingAccountNormal) {
		this.baseFinancingAccountNormal = baseFinancingAccountNormal;
	}

	public String getBaseFinancingAccountLock() {
		return baseFinancingAccountLock;
	}

	public void setBaseFinancingAccountLock(String baseFinancingAccountLock) {
		this.baseFinancingAccountLock = baseFinancingAccountLock;
	}

	public String getQuoteFinancingAccountNormal() {
		return quoteFinancingAccountNormal;
	}

	public void setQuoteFinancingAccountNormal(String quoteFinancingAccountNormal) {
		this.quoteFinancingAccountNormal = quoteFinancingAccountNormal;
	}

	public String getQuoteFinancingAccountLock() {
		return quoteFinancingAccountLock;
	}

	public void setQuoteFinancingAccountLock(String quoteFinancingAccountLock) {
		this.quoteFinancingAccountLock = quoteFinancingAccountLock;
	}

	public String getBaseAccountNormal() {
		return baseAccountNormal;
	}

	public void setBaseAccountNormal(String baseAccountNormal) {
		this.baseAccountNormal = baseAccountNormal;
	}

	public String getBaseAccountLock() {
		return baseAccountLock;
	}

	public void setBaseAccountLock(String baseAccountLock) {
		this.baseAccountLock = baseAccountLock;
	}

	public String getQuoteAccountNormal() {
		return quoteAccountNormal;
	}

	public void setQuoteAccountNormal(String quoteAccountNormal) {
		this.quoteAccountNormal = quoteAccountNormal;
	}

	public String getQuoteAccountLock() {
		return quoteAccountLock;
	}

	public void setQuoteAccountLock(String quoteAccountLock) {
		this.quoteAccountLock = quoteAccountLock;
	}

	public String getSysUid() {
		return sysUid;
	}

	public void setSysUid(String sysUid) {
		this.sysUid = sysUid;
	}

	public String getSysBaseAccount() {
		return sysBaseAccount;
	}

	public void setSysBaseAccount(String sysBaseAccount) {
		this.sysBaseAccount = sysBaseAccount;
	}

	public String getSysQuoteAccount() {
		return sysQuoteAccount;
	}

	public void setSysQuoteAccount(String sysQuoteAccount) {
		this.sysQuoteAccount = sysQuoteAccount;
	}

	public String getBaseCoinNetworkType() {
		return baseCoinNetworkType;
	}

	public void setBaseCoinNetworkType(String baseCoinNetworkType) {
		this.baseCoinNetworkType = baseCoinNetworkType;
	}

	public String getQuoteCoinNetworkType() {
		return quoteCoinNetworkType;
	}

	public void setQuoteCoinNetworkType(String quoteCoinNetworkType) {
		this.quoteCoinNetworkType = quoteCoinNetworkType;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getBaseCoin() {
		return baseCoin;
	}

	public void setBaseCoin(String baseCoin) {
		this.baseCoin = baseCoin;
	}

	public String getQuoteCoin() {
		return quoteCoin;
	}

	public void setQuoteCoin(String quoteCoin) {
		this.quoteCoin = quoteCoin;
	}
	
	
}
