package com.zatgo.zup.matching.engine;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeSide;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.matching.comparator.PriceBuyComparator;
import com.zatgo.zup.matching.comparator.PriceSellComparator;
import com.zatgo.zup.matching.model.OrderModel;
import com.zatgo.zup.matching.model.TradeModel;

public class OrderBook {

	private static final Logger logger = LoggerFactory.getLogger(OrderBook.class);

	private ExchangeSide side;

	private TreeMap<BigDecimal, LinkedList<OrderModel>> limitBook;

	public OrderBook(ExchangeSide side) {
		this.side = side;

		if (side.equals(ExchangeSide.BUY)) {
			this.limitBook = new TreeMap<>(new PriceBuyComparator());
		} else if (side.equals(ExchangeSide.SELL)) {
			this.limitBook = new TreeMap<>(new PriceSellComparator());
		} else {
			throw new BusinessException();
		}
	}

	public void add(OrderModel order) {
		if (order.isLimitOrder()) {
			addLimitOrder(order);
		} else {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,
					"order type is illegal, type=" + order.getType());
		}
	}

	public OrderModel getTopOrder() {
		OrderModel returnOrder = null;

		if (!this.limitBook.isEmpty()) {
			Entry<BigDecimal, LinkedList<OrderModel>> entry = this.limitBook.firstEntry();
			LinkedList<OrderModel> topLimitOrders = entry.getValue();
			if (!topLimitOrders.isEmpty()) {
				returnOrder = topLimitOrders.get(0);
				if (!returnOrder.isLimitOrder()) {
					logger.error("OrderBook exist is not limit data:" + JSON.toJSONString(returnOrder));
					returnOrder = null;
				}
			}
		}

		return returnOrder;
	}

	public void addLimitOrder(OrderModel order) {
		BigDecimal limitPrice = order.getPrice();
		if (this.limitBook.containsKey(limitPrice)) {
			LinkedList<OrderModel> orderList = (LinkedList) this.limitBook.get(limitPrice);
			orderList.add(order);
			this.limitBook.put(limitPrice, orderList);
		} else {
			LinkedList<OrderModel> orderList = new LinkedList();
			orderList.add(order);
			this.limitBook.put(limitPrice, orderList);
		}
	}
	
	

	public void removeTop(TradeModel trade) {
		BigDecimal tradedQuantity = trade.getVolume();
		if (tradedQuantity == null || tradedQuantity.compareTo(BigDecimal.ZERO) <= 0) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,
					"quantity must be greater than zero");
		}

		if (!this.limitBook.isEmpty()) {
			BigDecimal topPriceLevel = (BigDecimal) this.limitBook.firstKey();
			if (!topPriceLevel.equals(trade.getPrice())) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,
						"top-price of orderbook by more must be equal trade-price");
			}

			LinkedList<OrderModel> topLimitOrders = (LinkedList) this.limitBook.get(topPriceLevel);
			if (topLimitOrders.isEmpty()) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "No top order in empty book");
			}

			OrderModel topLimitOrder = (OrderModel) topLimitOrders.peek();
			int compareRet = topLimitOrder.getUnfilledQuantity().compareTo(tradedQuantity);
			if (compareRet < 0) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,
						"cannot decrease top of orderbook by more than available quantity");
			}

			if (compareRet == 0) {
				if (topLimitOrders.size() == 1) {
					this.limitBook.remove(topPriceLevel);
				} else {
					topLimitOrders.poll();
					this.limitBook.put(topPriceLevel, topLimitOrders);
				}
			}
		} else {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "No top order in empty book");
		}
	}

	public void removeOrder(OrderModel order) {
		if (!this.limitBook.isEmpty()) {
			BigDecimal priceLevel = order.getPrice();
			LinkedList<OrderModel> priceOrderList = (LinkedList) this.limitBook.get(order.getPrice());
			if (!priceOrderList.isEmpty()) {
				for (OrderModel itemOrder : priceOrderList) {
					if (itemOrder.getOrderId().equals(order.getOrderId())) {
						priceOrderList.remove(order);
					}
				}
				if (priceOrderList.size() > 0) {
					this.limitBook.put(priceLevel, priceOrderList);
				} else {
					this.limitBook.remove(priceLevel);
				}
			}
		}
	}

	public void clear() {
		limitBook.clear();
	}

	public ExchangeSide getSide() {
		return side;
	}

	public void setSide(ExchangeSide side) {
		this.side = side;
	}

	public TreeMap<BigDecimal, LinkedList<OrderModel>> getLimitBook() {
		return limitBook;
	}

	public void setLimitBook(TreeMap<BigDecimal, LinkedList<OrderModel>> limitBook) {
		this.limitBook = limitBook;
	}

}
