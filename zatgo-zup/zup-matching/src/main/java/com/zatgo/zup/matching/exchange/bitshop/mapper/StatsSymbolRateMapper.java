package com.zatgo.zup.matching.exchange.bitshop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.matching.exchange.bitshop.entity.StatsSymbolRate;
import com.zatgo.zup.matching.exchange.bitshop.entity.StatsSymbolRateExample;

@Mapper
public interface StatsSymbolRateMapper{
    int countByExample(StatsSymbolRateExample example);

    int deleteByExample(StatsSymbolRateExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(StatsSymbolRate record);

    int insertSelective(StatsSymbolRate record);

    List<StatsSymbolRate> selectByExample(StatsSymbolRateExample example);

    StatsSymbolRate selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") StatsSymbolRate record, @Param("example") StatsSymbolRateExample example);

    int updateByExample(@Param("record") StatsSymbolRate record, @Param("example") StatsSymbolRateExample example);

    int updateByPrimaryKeySelective(StatsSymbolRate record);

    int updateByPrimaryKey(StatsSymbolRate record);
}