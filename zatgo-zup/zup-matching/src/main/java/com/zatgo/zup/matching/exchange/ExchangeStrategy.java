package com.zatgo.zup.matching.exchange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.enumtype.ExchangeEnum;
import com.zatgo.zup.common.enumtype.ExchangeEnum.ExchangeType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.matching.config.CoinPairConfig;

@Component
public class ExchangeStrategy  implements ApplicationContextAware{
	
	private static final Logger logger = LoggerFactory.getLogger(ExchangeStrategy.class);

	private ApplicationContext applicationContext = null;
	
	@Value("${exchange.bytex.coinSymbols:}")
	private String bytexCoinSymbols;
	
	@Value("${exchange.default:}")
	private String exchangeDefautl;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		
	}
	
	public ExchangeService getExchangeService(CoinPairConfig coinPairConfig) {
		ExchangeEnum.ExchangeType type = ExchangeType.getEnumByType(coinPairConfig.getExchangeType());
		if(type == null) {
			logger.error("exchange type is not exist!");
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "exchange type is not exist");
		}
		String className = "ExchangeService_" + type.getCode();
		return (ExchangeService)applicationContext.getBean(className);
	}
}

