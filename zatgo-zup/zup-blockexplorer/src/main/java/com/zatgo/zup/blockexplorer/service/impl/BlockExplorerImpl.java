package com.zatgo.zup.blockexplorer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zatgo.zup.blockexplorer.bitcoin.BitCoinBlockExplorer;
import com.zatgo.zup.blockexplorer.ethereum.EthBlockExplorer;
import com.zatgo.zup.blockexplorer.metaverse.EtpBlockExplorer;
import com.zatgo.zup.blockexplorer.qtum.QtumBlockExplorer;
import com.zatgo.zup.blockexplorer.service.BlockExplorer;
import com.zatgo.zup.blockexplorer.service.entity.Address;
import com.zatgo.zup.blockexplorer.service.entity.Balance;
import com.zatgo.zup.blockexplorer.service.entity.Block;
import com.zatgo.zup.blockexplorer.service.entity.BlockchainNetworkEnum;
import com.zatgo.zup.blockexplorer.service.entity.Transaction;

@Service
public class BlockExplorerImpl implements BlockExplorer {

	@Autowired
	private BitCoinBlockExplorer bitCoinBlockExplorer;
	@Autowired
	private EthBlockExplorer ethBlockExplorer;
	@Autowired
	private EtpBlockExplorer etpBlockExplorer;
	@Autowired
	private QtumBlockExplorer qtumBlockExplorer;
	
	@Override
	public Transaction getTransaction(String txHash, BlockchainNetworkEnum network) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Block getBlock(long blockIndex, BlockchainNetworkEnum network) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Block getBlock(String blockHash, BlockchainNetworkEnum network) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address getAddress(String address,BlockchainNetworkEnum network) {
		if(network.equals(BlockchainNetworkEnum.ethereum)) {
			ethBlockExplorer.getAddress(address);
		}else {
			throw new RuntimeException("network=" + network.name() + " network is not exist");
		}
		return null;
	}

	@Override
	public Balance getBalance(String address,BlockchainNetworkEnum network) {
		Balance balance = null;
		if(network.equals(BlockchainNetworkEnum.ethereum)) {
			balance = ethBlockExplorer.getBalance(address);
		}else {
			throw new RuntimeException("network=" + network.name() + " network is not exist");
		}
		return balance;
	}

}
