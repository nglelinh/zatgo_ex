package com.zatgo.zup.blockexplorer.service.entity;

public class Block {
	private String hash;
	private String version;
	private String prevBlock;
	private String mrklRoot;
	private long time;
	private long bits;
	private long size;
	private long blockIndex;
	private long height;
	private long receivedTime;
	private String relayedBy;
	private BlockchainNetworkEnum network;
	
	public BlockchainNetworkEnum getNetwork() {
		return network;
	}
	public void setNetwork(BlockchainNetworkEnum network) {
		this.network = network;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getPrevBlock() {
		return prevBlock;
	}
	public void setPrevBlock(String prevBlock) {
		this.prevBlock = prevBlock;
	}
	public String getMrklRoot() {
		return mrklRoot;
	}
	public void setMrklRoot(String mrklRoot) {
		this.mrklRoot = mrklRoot;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public long getBits() {
		return bits;
	}
	public void setBits(long bits) {
		this.bits = bits;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public long getBlockIndex() {
		return blockIndex;
	}
	public void setBlockIndex(long blockIndex) {
		this.blockIndex = blockIndex;
	}
	public long getHeight() {
		return height;
	}
	public void setHeight(long height) {
		this.height = height;
	}
	public long getReceivedTime() {
		return receivedTime;
	}
	public void setReceivedTime(long receivedTime) {
		this.receivedTime = receivedTime;
	}
	public String getRelayedBy() {
		return relayedBy;
	}
	public void setRelayedBy(String relayedBy) {
		this.relayedBy = relayedBy;
	}
	
	
}
