package com.zatgo.zup.blockexplorer.service;

import com.zatgo.zup.blockexplorer.service.entity.Address;
import com.zatgo.zup.blockexplorer.service.entity.Balance;
import com.zatgo.zup.blockexplorer.service.entity.Block;
import com.zatgo.zup.blockexplorer.service.entity.BlockchainNetworkEnum;
import com.zatgo.zup.blockexplorer.service.entity.Transaction;

public interface BlockExplorer {

	public Transaction getTransaction(String txHash,BlockchainNetworkEnum network);
	
	public Block getBlock(long blockIndex,BlockchainNetworkEnum network);
	
	public Block getBlock(String blockHash,BlockchainNetworkEnum network); 
	
	public Address getAddress(String address,BlockchainNetworkEnum network);
	
	public Balance getBalance(String address,BlockchainNetworkEnum network);
}
