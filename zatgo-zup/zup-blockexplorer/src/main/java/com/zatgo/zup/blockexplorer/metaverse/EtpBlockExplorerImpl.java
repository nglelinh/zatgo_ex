package com.zatgo.zup.blockexplorer.metaverse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.zatgo.zup.blockexplorer.service.entity.Address;
import com.zatgo.zup.blockexplorer.service.entity.Balance;
import com.zatgo.zup.blockexplorer.service.entity.Block;
import com.zatgo.zup.blockexplorer.service.entity.BlockchainNetworkEnum;
import com.zatgo.zup.blockexplorer.service.entity.Transaction;

@Service
public class EtpBlockExplorerImpl implements EtpBlockExplorer {

	@Value("{zup-blockexplorer.etp.url}")
	private String url;
	
	@Override
	public Transaction getTransaction(String txHash) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Block getBlock(long blockIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Block getBlock(String blockHash) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address getAddress(String address) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Balance getBalance(String address) {
		// TODO Auto-generated method stub
		return null;
	}

}
