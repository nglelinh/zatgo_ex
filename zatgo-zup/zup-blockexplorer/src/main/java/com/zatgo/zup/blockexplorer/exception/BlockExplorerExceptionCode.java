package com.zatgo.zup.blockexplorer.exception;

import com.zatgo.zup.common.exception.BusinessExceptionCode;

public class BlockExplorerExceptionCode extends BusinessExceptionCode {

	public static String BLOCK_EXPLORER_SEARCH_ERROR = "2001";
	
	static {
		EXCEPTION_MESSAGE.put(BLOCK_EXPLORER_SEARCH_ERROR, "blockchain search fail");
	}
}
