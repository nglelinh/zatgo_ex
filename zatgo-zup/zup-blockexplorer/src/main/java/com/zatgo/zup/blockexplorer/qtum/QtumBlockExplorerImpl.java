package com.zatgo.zup.blockexplorer.qtum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

import com.zatgo.zup.blockexplorer.service.entity.Address;
import com.zatgo.zup.blockexplorer.service.entity.Balance;
import com.zatgo.zup.blockexplorer.service.entity.Block;
import com.zatgo.zup.blockexplorer.service.entity.Transaction;

@Service
public class QtumBlockExplorerImpl implements QtumBlockExplorer {

	private static final Logger logger = LoggerFactory.getLogger(QtumBlockExplorerImpl.class); 
	
	@Value("{zup-blockexplorer.qtum.url}")
	private String url;
	
	private static Web3j web3j;
	
	static {
		Web3j web3j = Web3j.build(new HttpService(
                "http://192.168.1.94:3889"));
	}
	
	@Override
	public Transaction getTransaction(String txHash) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Block getBlock(long blockIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Block getBlock(String blockHash) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address getAddress(String address) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Balance getBalance(String address) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean transferFrom(String fromAddress, String toAddress, long value) {
		//web3j.ethCoinbase().
		return null;
	}

}
