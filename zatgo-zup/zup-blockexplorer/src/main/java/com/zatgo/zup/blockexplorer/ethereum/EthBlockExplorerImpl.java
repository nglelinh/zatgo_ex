package com.zatgo.zup.blockexplorer.ethereum;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthGetBalance;

import com.zatgo.zup.blockexplorer.exception.BlockExplorerExceptionCode;
import com.zatgo.zup.blockexplorer.service.entity.Address;
import com.zatgo.zup.blockexplorer.service.entity.Balance;
import com.zatgo.zup.blockexplorer.service.entity.Block;
import com.zatgo.zup.blockexplorer.service.entity.Transaction;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;

@Service
public class EthBlockExplorerImpl implements EthBlockExplorer {

	private static final Logger logger = LoggerFactory.getLogger(EthBlockExplorerImpl.class); 
	@Autowired
	private Web3j web3j;
	
	@Override
	public Transaction getTransaction(String txHash) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Block getBlock(long blockIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Block getBlock(String blockHash) {
		try {
			EthBlock ethBlock = web3j.ethGetBlockByHash(blockHash, true).send();
			EthBlock.Block blockSub = ethBlock.getBlock();
			
			Block block = new Block();
			//block.setBits(blockSub.get);
			return null;
		} catch (IOException e) {
			throw new BusinessException(BlockExplorerExceptionCode.BLOCK_EXPLORER_SEARCH_ERROR);
		}
	}

	@Override
	public Address getAddress(String address) {
		
		return null;
	}

	@Override
	public Balance getBalance(String address) {
		try {
			DefaultBlockParameter param = DefaultBlockParameter.valueOf("latest");
			EthGetBalance ethGetBalance = web3j.ethGetBalance(address, param).send();
			Balance balance = new Balance();
			balance.setFinalBalance(ethGetBalance.getBalance());
			return balance;
		}catch(Exception e) {
			throw new BusinessException(BlockExplorerExceptionCode.BLOCK_EXPLORER_SEARCH_ERROR);
		}
		
	}

	@Override
	public Boolean transferFrom(String fromAddress, String toAddress, long value) {
		//web3j.eth
		return null;
	}

}
