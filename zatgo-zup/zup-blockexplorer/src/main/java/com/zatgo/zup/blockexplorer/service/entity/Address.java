package com.zatgo.zup.blockexplorer.service.entity;

public class Address {

	private String address;
	private Balance balance;
	private BlockchainNetworkEnum network;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public BlockchainNetworkEnum getNetwork() {
		return network;
	}
	public void setNetwork(BlockchainNetworkEnum network) {
		this.network = network;
	}
	public Balance getBalance() {
		return balance;
	}
	public void setBalance(Balance balance) {
		this.balance = balance;
	}
	
	
}
