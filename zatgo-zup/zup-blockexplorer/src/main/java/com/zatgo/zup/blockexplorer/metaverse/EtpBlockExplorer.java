package com.zatgo.zup.blockexplorer.metaverse;

import com.zatgo.zup.blockexplorer.service.entity.Address;
import com.zatgo.zup.blockexplorer.service.entity.Balance;
import com.zatgo.zup.blockexplorer.service.entity.Block;
import com.zatgo.zup.blockexplorer.service.entity.BlockchainNetworkEnum;
import com.zatgo.zup.blockexplorer.service.entity.Transaction;

public interface EtpBlockExplorer {

	public Transaction getTransaction(String txHash);
	
	public Block getBlock(long blockIndex);
	
	public Block getBlock(String blockHash); 
	
	public Address getAddress(String address);
	
	public Balance getBalance(String address);
}
