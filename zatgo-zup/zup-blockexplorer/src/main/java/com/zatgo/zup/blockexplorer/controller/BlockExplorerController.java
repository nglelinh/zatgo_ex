package com.zatgo.zup.blockexplorer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.blockexplorer.service.BlockExplorer;
import com.zatgo.zup.blockexplorer.service.entity.Address;
import com.zatgo.zup.blockexplorer.service.entity.Balance;
import com.zatgo.zup.blockexplorer.service.entity.BlockchainNetworkEnum;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;

@RestController
@RequestMapping("/blockexplorer")
public class BlockExplorerController {

	@Autowired
	private BlockExplorer blockExplorer;
	
	@Value("${spring.datasource.druid.url}")
	private String foo;

	@RequestMapping("/getAddress/{network}/{address}")
	public ResponseData<Address> getAddress(@PathVariable("address") String address,@PathVariable("network") BlockchainNetworkEnum network) {
		Address businessData = blockExplorer.getAddress(address, network);
		return BusinessResponseFactory.createSuccess(businessData);

	}
	
	@RequestMapping("/getBalance/{network}/{address}")
	public ResponseData<Balance> getBalance(@PathVariable("address") String address,@PathVariable("network") BlockchainNetworkEnum network) {
		Balance businessData = blockExplorer.getBalance(address, network);
		return BusinessResponseFactory.createSuccess(businessData);
	}
}
