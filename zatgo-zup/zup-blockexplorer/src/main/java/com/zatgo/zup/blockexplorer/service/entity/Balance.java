package com.zatgo.zup.blockexplorer.service.entity;

import java.math.BigInteger;

public class Balance {

	private BigInteger totalReceived;
	private BigInteger totalSend;
	private BigInteger finalBalance;
	private int txCount;
	public BigInteger getTotalReceived() {
		return totalReceived;
	}
	public void setTotalReceived(BigInteger totalReceived) {
		this.totalReceived = totalReceived;
	}
	public BigInteger getTotalSend() {
		return totalSend;
	}
	public void setTotalSend(BigInteger totalSend) {
		this.totalSend = totalSend;
	}
	public BigInteger getFinalBalance() {
		return finalBalance;
	}
	public void setFinalBalance(BigInteger finalBalance) {
		this.finalBalance = finalBalance;
	}
	public int getTxCount() {
		return txCount;
	}
	public void setTxCount(int txCount) {
		this.txCount = txCount;
	}
	
	
}
