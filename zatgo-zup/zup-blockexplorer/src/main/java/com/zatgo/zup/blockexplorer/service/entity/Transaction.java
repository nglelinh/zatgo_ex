package com.zatgo.zup.blockexplorer.service.entity;

public class Transaction {

	private String hash;
	private String version;
	private long size;
	private long relayedBy;
	private long blockHeight;
	private long txIndex;
	private BlockchainNetworkEnum network;
	
	public BlockchainNetworkEnum getNetwork() {
		return network;
	}
	public void setNetwork(BlockchainNetworkEnum network) {
		this.network = network;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public long getRelayedBy() {
		return relayedBy;
	}
	public void setRelayedBy(long relayedBy) {
		this.relayedBy = relayedBy;
	}
	public long getBlockHeight() {
		return blockHeight;
	}
	public void setBlockHeight(long blockHeight) {
		this.blockHeight = blockHeight;
	}
	public long getTxIndex() {
		return txIndex;
	}
	public void setTxIndex(long txIndex) {
		this.txIndex = txIndex;
	}
	
	
}
