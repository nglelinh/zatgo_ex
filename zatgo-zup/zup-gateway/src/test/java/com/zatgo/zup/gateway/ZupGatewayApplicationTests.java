package com.zatgo.zup.gateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.common.sms.santi.SantiService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZupGatewayApplicationTests {
	
	@Autowired
	private SantiService santiService;

	@Test
	public void contextLoads() {
	}
	
//	@Test
	public void testIntlSms() {
		santiService.sendInternationalSMS(CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID, "0000|15", "840", "1", "207222");
	}

}
