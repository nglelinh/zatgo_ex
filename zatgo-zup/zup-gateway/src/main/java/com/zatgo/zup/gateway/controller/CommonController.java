package com.zatgo.zup.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gateway")
public class CommonController {

    @GetMapping("/ischeck")
    public Boolean isCheck(){
        return true;
    }
}
