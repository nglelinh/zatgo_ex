package com.zatgo.zup.gateway.model;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SignInterfaceRecord;
import com.zatgo.zup.gateway.remoteservice.RecordRemoteService;

@Component
public class ExternalSignCallBackMethod implements StringCallBackMethod {

	private static final Logger logger = LoggerFactory.getLogger(ExternalSignCallBackMethod.class);
	@Autowired
    private RecordRemoteService recordRemoteService;
	
	@Override
	public boolean callBack(String json,String appId,ServerHttpRequest request) {
		String requestURI = request.getURI().getPath();
		SignInterfaceRecord record = new SignInterfaceRecord();
		record.setAppId(appId);
        record.setInterfaceName(requestURI);
        record.setRequestContent(json);
        record.setRequestDatetime(new Date());
        record.setRequestType((byte) 0);
    	ResponseData<Boolean> rsp = recordRemoteService.insertRecord(record);
    	if (rsp == null || !rsp.isSuccessful() || !rsp.getData()) {
    		logger.error(JSON.toJSONString(record));
        	logger.error(rsp.getCode() + rsp.getMessage());
        }
    	return true;
	}

}
