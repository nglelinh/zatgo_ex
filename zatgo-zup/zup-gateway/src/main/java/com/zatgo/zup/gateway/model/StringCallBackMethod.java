package com.zatgo.zup.gateway.model;

import org.springframework.http.server.reactive.ServerHttpRequest;

public interface StringCallBackMethod {

	boolean callBack(String json,String appId,ServerHttpRequest request);
}