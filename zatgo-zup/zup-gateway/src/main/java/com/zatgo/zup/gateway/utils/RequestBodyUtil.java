package com.zatgo.zup.gateway.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.NettyDataBufferFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zatgo.zup.gateway.filter.whitelist.SignAccessList;
import com.zatgo.zup.gateway.model.StringCallBackMethod;

import io.netty.buffer.UnpooledByteBufAllocator;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

@Component
public class RequestBodyUtil {

	private static Logger logger = LoggerFactory.getLogger(RequestBodyUtil.class);
	
	
	public ServerHttpRequestDecorator getServerHttpRequestDecorator(ServerWebExchange exchange,
			StringCallBackMethod method) {
		String requestURI = exchange.getRequest().getURI().getPath();
    	ServerHttpRequestDecorator requestDecorator = new ServerHttpRequestDecorator(exchange.getRequest()) {
        	
        	private final StringWriter cachedCopy = new StringWriter();
            private InputStream dataBuffer;
            private DataBuffer bodyDataBuffer;
            private int getBufferTime = 0;
            private byte[] bytes;

        	@Override
            public Flux<DataBuffer> getBody() {
                if (getBufferTime == 0) {
                    getBufferTime++;
                    Flux<DataBuffer> flux = super.getBody();
                    return flux.publishOn(Schedulers.newElastic("publish-thread"))
                            .map(this::cache)
                            .doOnComplete(() -> trace(getDelegate(), cachedCopy.toString()));
         
         
                } else {
                    return Flux.just(getBodyMore());
                }
         
            }
         
         
            private DataBuffer getBodyMore() {
                NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(new UnpooledByteBufAllocator(false));
                DataBuffer dataBuffer = nettyDataBufferFactory.wrap(new String(cachedCopy.getBuffer()).getBytes());
                bodyDataBuffer = dataBuffer;
                return bodyDataBuffer;
            }
         
            private DataBuffer cache(DataBuffer buffer) {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    dataBuffer = buffer.asInputStream();
                    bytes = IOUtils.toByteArray(dataBuffer);
                    cachedCopy.write(new String(bytes));
                    NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(new UnpooledByteBufAllocator(false));
                    DataBuffer dataBuffer = nettyDataBufferFactory.wrap(bytes);
                    bodyDataBuffer = dataBuffer;
                    return bodyDataBuffer;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
         
            private void trace(ServerHttpRequest request, String requestBody) {
            	//此处必须把请求传下去，否则会导致后续请求无法传入
                //详见https://github.com/spring-cloud/spring-cloud-gateway/issues/280
                
                try {
                    String bodyContent = "";
                    String appId = "";
                    if(SignAccessList.isInCheckoutSignAccessList(requestURI)) {
                        JSONObject json = JSONObject.parseObject(requestBody);
                        if (json == null){
                            json = new JSONObject();
                        }
                        bodyContent = json.toJSONString();
                        appId = json.getString("appId");
                    }else {
                        bodyContent = requestBody;
                    }

                    method.callBack(bodyContent, appId, request);
                }catch(Exception e) {
                    if (StringUtils.isEmpty(requestBody)){
                        logger.error(JSONObject.toJSONString(requestBody));
                    }
                	logger.error("",e);
                }
            }

        };
        
        return requestDecorator;
	}
}
