package com.zatgo.zup.gateway.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.NettyDataBufferFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.gateway.filter.whitelist.AdminValidateList;
import com.zatgo.zup.gateway.filter.whitelist.AppTokenList;
import com.zatgo.zup.gateway.filter.whitelist.PublicAccessList;
import com.zatgo.zup.gateway.filter.whitelist.SignAccessList;
import com.zatgo.zup.gateway.swagger.DocumentationConfig;
import com.zatgo.zup.gateway.utils.FilterResultUtil;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by 46041 on 2018/9/5.
 */
@Component
public class ApiDocsReWriteResponseFilter implements GlobalFilter, Ordered {
    private static final Logger logger = LoggerFactory.getLogger(UserAccessFilter.class);
    
    @Autowired
    private DocumentationConfig documentationConfig;
    
    public ApiDocsReWriteResponseFilter() {
        logger.debug("=============================创建ApiDocsReWriteResponseFilter==================================");
    }

    @Override
    public int getOrder() {
    	return -2;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, GatewayFilterChain gatewayFilterChain) {
        ServerHttpRequest request = serverWebExchange.getRequest();
        String requestURI = request.getURI().getPath();

        //判断是不是访问swagger文档URL
        if(PublicAccessList.isInPublicAccessList(requestURI) && requestURI.contains("/v2/api-docs")){
        	String type = request.getQueryParams().getFirst(DocumentationConfig.URL_API_TYPE_KEY);
            ServerHttpResponse response = serverWebExchange.getResponse();
            NettyDataBufferFactory factory = (NettyDataBufferFactory) response.bufferFactory();
            ServerHttpResponseDecorator decoratedResponse = new ServerHttpResponseDecorator(response) {
            	
            	@Override
                public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
            		Flux<? extends DataBuffer> flux = (Flux<? extends DataBuffer>) body;
            		Flux<? extends DataBuffer> f = flux.buffer().map( dataBuffers  -> {

            			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            			dataBuffers.forEach(dataBuffer -> {
            				byte[] origRespContent = new byte[dataBuffer.readableByteCount()];
                            dataBuffer.read(origRespContent);
                            try {
								outputStream.write(origRespContent);
							} catch (IOException e) {
								logger.error("",e);
							}
            			});
                        

                        String origRespContentStr = new String(outputStream.toByteArray());
                        logger.debug("swagger doc content::: " + (origRespContentStr));
                        
                        origRespContentStr = reWriteApiList(origRespContentStr, type);
                        
                        logger.debug("swagger doc content new::: " + (origRespContentStr));
                        
                        return factory.wrap(origRespContentStr.getBytes());
             
                    });
            		
            		return getDelegate().writeWith(f);
            	}
            	
            	@Override
                public Mono<Void> writeAndFlushWith(Publisher<? extends Publisher<? extends DataBuffer>> body) {
                    return writeWith(Flux.from(body)
                            .flatMapSequential(p -> p));
                }
            };
            ServerWebExchange swe = serverWebExchange.mutate().response(decoratedResponse).build();
            return gatewayFilterChain.filter(swe);
        }else {
        	return FilterResultUtil.getSuccessResult(serverWebExchange, gatewayFilterChain);
        }

        
    }
    
    
    public String reWriteApiList(String content,String type) {
    	ObjectMapper mapper = new ObjectMapper();
    	HashMap parseMap = null;
    	try {
    		parseMap = mapper.readValue(content, HashMap.class);
		} catch (Exception e) {
			throw new BusinessException();
		}
    	HashMap pathMap = (HashMap)parseMap.get("paths");
    	HashMap pathMapNew = new HashMap<>();
    	Iterator<String> keys = pathMap.keySet().iterator();
		
    	if(type.equals(DocumentationConfig.APP_USER_CHECKOUT_API_TYPE)) {
    		while(keys.hasNext()) {
    			String key = keys.next();
    			boolean isExist = SignAccessList.isInCheckoutSignAccessList(key);
    			if(isExist) {
    				pathMapNew.put(key, pathMap.get(key));
    			}
    		}
    	}else if(type.equals(DocumentationConfig.CLOUD_USER_ADMIN_API_TYPE)){
    		while(keys.hasNext()) {
    			String key = keys.next();
    			boolean isExist = AdminValidateList.isInWhiteList(key);
    			isExist = isExist || PublicAccessList.isInPublicAccessList(key);
    			if(isExist) {
    				pathMapNew.put(key, pathMap.get(key));
    			}
    		}
    	}else if(type.equals(DocumentationConfig.CLOUD_USER_APP_API_TYPE)) {
    		
    		while(keys.hasNext()) {
    			String key = keys.next();
    			boolean isExist = AppTokenList.isInWhiteList(key);
    			isExist = isExist || PublicAccessList.isInPublicAccessList(key);
    			if(isExist) {
    				pathMapNew.put(key, pathMap.get(key));
    			}
    		}
    	}else if(documentationConfig.isTestEnv && type.equals(DocumentationConfig.TEST_EVN_API_TYPE)){
    		pathMapNew = pathMap;
    	}
    	
    	if(documentationConfig.isTestEnv) {
    		parseMap.put("paths", pathMapNew);
    		try {
    			return mapper.writeValueAsString(parseMap);
    		} catch (JsonProcessingException e) {
    			throw new BusinessException();
    		}
    	}else {
    		return "";
    	}
    	
    	
    }

}
