package com.zatgo.zup.gateway.filter.whitelist;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FlyBy Open Api 验签白名单
 * 
 * @Title
 * @author lincm
 * @date 2018年4月3日 下午3:17:52
 */
public class FlyByOpenApiPublicAccessList {

	private static final Logger logger = LoggerFactory.getLogger(FlyByOpenApiPublicAccessList.class);

	// url列表
	private static final String SYNC_APPROVE = "/openapi/corp/syncProApproveInfo";
	private static final String SIGN_LOGIN = "/openapi/corp/signLogin";
	private static final String ORDER_SEARCH = "/openapi/corp/order/search";
	private static final String USER_SYNC_BATH = "/openapi/corp/user/syncBatch";

	private static List<String> publicList = null;
	private static Lock lock = new ReentrantLock();

	/**
	 * 获取白名单列表
	 * 
	 * @return
	 */
	public static List<String> getPublicList() {
		if (publicList == null) {
			lock.lock();
			try {
				if (publicList == null) {
					publicList = new ArrayList<>();
					publicList.add(SYNC_APPROVE);
					publicList.add(ORDER_SEARCH);
					publicList.add(USER_SYNC_BATH);
					publicList.add(SIGN_LOGIN);
				}
			} catch (Exception e) {
				logger.error("", e);
			} finally {
				lock.unlock();
			}
		}

		return publicList;
	}

	/**
	 * 判断是否为签名校验
	 * 
	 * @param url
	 * @return
	 */
	public static boolean isInPublicAccessList(String url) {
		List<String> publicList = getPublicList();
		for (String signAccessUrl : publicList) {
			if (url.startsWith(signAccessUrl)) {
				return true;
			}
		}
		return false;
	}
}
