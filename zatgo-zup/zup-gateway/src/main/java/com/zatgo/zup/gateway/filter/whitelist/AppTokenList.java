package com.zatgo.zup.gateway.filter.whitelist;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 登录白名单
 * @Title
 * @author lincm
 * @date 2018年4月3日 下午3:17:52
 */
public class AppTokenList {

	private static final Logger logger = LoggerFactory.getLogger(AppTokenList.class);

	//用户中心
	private static final String USER_BYNAME = "/user/basicbyname";
	private static final String USER_BYID = "/user/basicbyid";
	private static final String USER_INFO = "/user/basicInfo";
	private static final String USER_EDITLOGINPASSWORD = "/user/editLoginPassword";
	private static final String USER_EDITPAYPASSWORD = "/user/editPayPassword";
	private static final String USER_FORGETPAYPASSWORD = "/user/forgetPayPassword";
	private static final String USER_UPDATE_BASICINFO = "/user/update/basicInfo";
	private static final String USER_INVITE_LIST = "/user/invite/list";
	private static final String USER_CERTIFICATE_BASICINFO = "/user/certificate/basicInfo";
	private static final String USER_CERTIFICATE_UPDATE = "/user/certificate/update";
	private static final String USER_INVITECODE_IMAGE = "/user/inviteCodeImage";

	//钱包中心
	private static final String WALLET_ACCOUNT_LIST = "/wallet/account/list";
	private static final String WALLET_PAYMENT_ACCOUNT = "/wallet/payment/account";
	private static final String WALLET_PAYMENT_LIST = "/wallet/payment/list";
	private static final String WALLET_EXTRACT_RECORD_LIST = "/wallet/extract/record/list";
	private static final String WALLET_DEPOSIT_RECORD_LIST = "/wallet/deposit/record/list";
	private static final String WALLET_SYSTEM_COINTYPES = "/wallet/system/cointypes";
	private static final String WALLET_PAY = "/wallet/pay";
	private static final String WALLET_TRANSFER = "/wallet/transfer";
	private static final String WALLET_ADDRESS_DEPOSIT = "/wallet/address/deposit";
	private static final String WALLET_ACCOUNT_PRIMARY = "/wallet/account/primary";
	private static final String WALLET_ACCOUNT_CREATE = "/wallet/account/create";
	private static final String WALLET_EXTRACT_ADD = "/wallet/extract/add";
	private static final String WALLET_EXTRACT_LIST = "/wallet/extract/add";
	private static final String WALLET_FIXED_TIME_DEPOSIT_RELEASE_PRO_LIST = "/wallet/fixedTimeDeposit/getReleaseProList";
	private static final String WALLET_FIXED_TIME_DEPOSIT_PRO_SUBSC = "/wallet/fixedTimeDeposit/proSubsc";
	private static final String WALLET_FIXED_TIME_DEPOSIT_PRO_SUBSC_LIST = "/wallet/fixedTimeDeposit/getProSubscList";


	//收银台
	private static final String PAY_ORDERPAY = "/pay/orderpay";
	private static final String PAY_ORDER_PAGEINFO = "/pay/orders/pageinfo";
	private static final String PAY_COMPUTE_MONEY = "/pay/compute/money";
	private static final String PAY_ORDERS_INFO = "/pay/orders/info";
	private static final String PAY_ORDERS_STATUS = "/pay/orders/status";
	private static final String PAY_INFO = "/pay/info";

	private static final String PAY_MERCHANT_APP_LIST = "/pay/merchant/app/list";

	//挖矿
	private static final String MINING_USER = "/mining/preMiningInfo";
	private static final String MINING_USER_COMPUTE_POWER = "/mining/userComputePower";
	private static final String MINING_USER_COMPUTE_POWER_TOP = "/mining/userComputePowerTop";
	private static final String MINING_GET_COMPUTE_POWER_RECORD = "/mining/getComputePowerRecord";
	private static final String MINING_MINING = "/mining/mining";
	private static final String MINING_MINING_RECORD = "/mining/miningRecord";

	//任务
	private static final String TASK_CRATE_TASK = "/mining/task/create/task";
	private static final String TASK_EDIT_TASK = "/mining/task/edit/task";
	private static final String TASK_DELETE_TASK = "/mining/task/delete/task";
	private static final String TASK_TASK_LIST = "/mining/task/list/task";
	private static final String TASK_TASK_LISTDATE = "/mining/task/list/taskData";
	private static final String TASK_DONE_TASK = "/mining/task/done";
	private static final String TASK_LOGIN_TASK = "/mining/task/loginTaskDone";
	private static final String TASK_INVITATION_CODE = "/mining/task/invitationCode";
	private static final String TASK_RECORD_LIST = "/mining/task/record/list";
	private static final String TASK_REWARDRECORD = "/mining/task/get/rewardRecord";

	//交易所
	private static final String EXCHANGE_ORDER_CREATE = "/exchange/appapi/order/create";
	private static final String EXCHANGE_ORDER_SEARCH = "/exchange/appapi/order/pageSearch";
	private static final String EXCHANGE_ORDER = "/exchange/appapi/order";

	// 蒜粒
	private static final String SUANLI_CATEGORY_DELETE_CATALOG = "/suanli/category/delete/catalog";
	private static final String SUANLI_INVESTOR_PARTY_DETAIL = "/suanli/investor/party/detail";
	private static final String SUANLI_PROJECT_ADD = "/suanli/project/add";
	private static final String SUANLI_PROJECT_DELETE = "/suanli/project/delete";
	private static final String SUANLI_USER_ADD_FAVORITE = "/suanli/user/add/favorite";
	private static final String SUANLI_USER_DELETE_FAVORITE = "/suanli/user/delete/favorite";
	private static final String SUANLI_USER_PROJECT = "/suanli/user/project";
	private static final String SUANLI_USER_DATA = "/suanli/user/data";
	private static final String SUANLI_OSS_UPLOAD_FILE = "/suanli/oss/upload/file";
	private static final String SUANLI_USER_FAVORITE_PROJECT = "/suanli/user/favorite/project";
	private static final String SUANLI_USER_FAVORITE_INVESTOR = "/suanli/user/favorite/investor";
	private static final String SUNALI_COMPANY_LIST = "/suanli/company/list";
	private static final String SUANLI_INSTITUTION_LIST = "/suanli/institution/list";

	private static final String NEED_MESSAGE_DEL = "/suanli/message/del";
	private static final String NEED_MESSAGE_EDIT = "/suanli/message/edit";
	private static final String NEED_MESSAGE_LIST = "/suanli/message/list";

	private static final String COMPANY_EXAMPLE_ADD = "/suanli/company/example/back/add";
	private static final String COMPANY_EXAMPLE_DEL = "/suanli/company/example/back/del";
	private static final String COMPANY_EXAMPLE_EDIT = "/suanli/company/example/back/edit";
	private static final String COMPANY_EXAMPLE_LIST = "/suanli/company/example/back/list";

	//游戏中心
	private static final String GAME_ZHUANPAN_LOAD = "/game/zhuanpan/load";
	private static final String GAME_ZHUANPAN_RUN = "/game/zhuanpan/run";
	private static final String GAME_RECORD_LIST = "/game/zhuanpan/recordList";
	private static final String GAME_QA_LIST = "/game/qa/list";
	private static final String GAME_QA_ANSWER_LIST = "/game/qa/answer";
	private static final String GAME_QA_GET_REWARD_LIST = "/mining/task/get/reward";
	private static final String GAME_QA_RECORD_LIST = "/game/qa/record";

	private static final String WALLET_APPWEBSOCKET = "/ws/wallet/appwebsocket";
	private static final String PAY_PAYWEBSOCKET = "/ws/pay/paywebsocket";
	private static final String PAY_RETURN = "/ws/pay/return/";
	
	private static final String COUPONS_GET_USER_COUPONS_BY_ORDER = "/coupons/user/getUserCouponsByOrder";
	private static final String COUPONS_GET_USER_COUPONS_LIST = "/coupons/user/getUserCouponsList";
	private static final String COUPONS_USER_USAGE_COUPONS = "/coupons/user/usageCoupons";
	private static final String COUPONS_USER_MANUAL_GET_COUPONS = "/coupons/user/userManualGetCoupons";
	private static final String COUPONS_USER_MANUAL_CAN_GET_COUPONS_LIST = "/coupons/user/canManualGetCouponsList";



	//拼团
	private static final String  ACTIVITY_GROUP_BOOKING_USER = "/activity/groupBooking/user";
	//拼团admin
	private static final String  ACTIVITY_GROUP_BOOKING_ADMIN = "/activity/groupBooking/admin";
	//拼团internal
	private static final String  ACTIVITY_GROUP_BOOKING_INTERNAL = "/activity/groupBooking/internal";

	//砍价
	private static final String  ACTIVITY_BARGAIN_USER = "/activity/bargain/user";
	//砍价admin
	private static final String  ACTIVITY_BARGAIN_ADMIN = "/activity/bargain/admin";
	//获取用户id
	private static final String  ACTIVITY_GET_USERID = "/activity/get/userId";
	//根据joinId获取活动id
	private static final String  ACTIVITY_GET_ACTIVITY_ID = "/activity/getActivityId";

	//一块伴 订单
	private static final String TRADE_API_ORDER = "/trade/api/order";

	//一块伴 通用商品
	private static final String TRADE_API_PMSCOMMONPRODUCT = "/trade/api/pmsCommonProduct";


	//一块伴 订单
	private static final String TRADE_API_SSO = "/trade/api/sso";


	//一块伴 admin
	private static final String TRADE_ADMIN = "/trade/admin";

	private static List<String> whiteList = null;
	private static List<String> appSignList = null;
	private static Lock lock = new ReentrantLock();

	public static List<String> getAppSignList(){
		if(appSignList == null) {
			lock.lock();
			try {
				if(appSignList == null) {
					appSignList = new ArrayList<>();

					//挖矿
					appSignList.add(MINING_GET_COMPUTE_POWER_RECORD);
					appSignList.add(MINING_MINING);
					appSignList.add(MINING_MINING_RECORD);
					appSignList.add(MINING_USER);
					appSignList.add(MINING_USER_COMPUTE_POWER);
					appSignList.add(MINING_USER_COMPUTE_POWER_TOP);

					//任务
					appSignList.add(TASK_CRATE_TASK);
					appSignList.add(TASK_DELETE_TASK);
					appSignList.add(TASK_EDIT_TASK);
					appSignList.add(TASK_TASK_LIST);
					appSignList.add(TASK_TASK_LISTDATE);
					appSignList.add(TASK_DONE_TASK);
					appSignList.add(TASK_LOGIN_TASK);
					appSignList.add(TASK_INVITATION_CODE);
					appSignList.add(TASK_RECORD_LIST);

					//游戏大转盘
					appSignList.add(GAME_ZHUANPAN_LOAD);
					appSignList.add(GAME_ZHUANPAN_RUN);
					appSignList.add(GAME_RECORD_LIST);

					appSignList.add(GAME_QA_ANSWER_LIST);
					appSignList.add(GAME_QA_GET_REWARD_LIST);

				}
			}catch(Exception e) {
				logger.error("",e);
			}finally {
				lock.unlock();
			}
		}

		return appSignList;
	}
	/**
	 * 获取白名单列表
	 * @return
	 */
	public static List<String> getWhiteList(){
		if(whiteList == null) {
			lock.lock();
			try {
				if(whiteList == null) {
					whiteList = new ArrayList<>();
					//用户中心
					whiteList.add(USER_BYNAME);
					whiteList.add(USER_BYID);
					whiteList.add(USER_EDITLOGINPASSWORD);
					whiteList.add(USER_EDITPAYPASSWORD);
					whiteList.add(USER_FORGETPAYPASSWORD);
					whiteList.add(USER_INFO);
					whiteList.add(USER_UPDATE_BASICINFO);
					whiteList.add(USER_INVITE_LIST);
					whiteList.add(USER_CERTIFICATE_BASICINFO);
					whiteList.add(USER_CERTIFICATE_UPDATE);
					whiteList.add(USER_INVITECODE_IMAGE);

					//钱包中心
					whiteList.add(WALLET_ACCOUNT_LIST);
					whiteList.add(WALLET_PAYMENT_ACCOUNT);
					whiteList.add(WALLET_PAYMENT_LIST);
					whiteList.add(WALLET_EXTRACT_RECORD_LIST);
					whiteList.add(WALLET_DEPOSIT_RECORD_LIST);
					whiteList.add(WALLET_SYSTEM_COINTYPES);
					whiteList.add(WALLET_PAY);
					whiteList.add(WALLET_TRANSFER);
					whiteList.add(WALLET_ADDRESS_DEPOSIT);
					whiteList.add(WALLET_ACCOUNT_PRIMARY);
					whiteList.add(WALLET_ACCOUNT_CREATE);
					whiteList.add(WALLET_EXTRACT_ADD);
					whiteList.add(WALLET_EXTRACT_LIST);
					whiteList.add(WALLET_FIXED_TIME_DEPOSIT_RELEASE_PRO_LIST);
					whiteList.add(WALLET_FIXED_TIME_DEPOSIT_PRO_SUBSC);
					whiteList.add(WALLET_FIXED_TIME_DEPOSIT_PRO_SUBSC_LIST);

					//收银台
					whiteList.add(PAY_ORDERPAY);
					whiteList.add(PAY_ORDER_PAGEINFO);
					whiteList.add(PAY_COMPUTE_MONEY);
					whiteList.add(PAY_ORDERS_INFO);
					whiteList.add(PAY_ORDERS_STATUS);
					whiteList.add(PAY_INFO);
					whiteList.add(PAY_MERCHANT_APP_LIST);

					//挖矿
					whiteList.add(MINING_GET_COMPUTE_POWER_RECORD);
					whiteList.add(MINING_MINING);
					whiteList.add(MINING_MINING_RECORD);
					whiteList.add(MINING_USER);
					whiteList.add(MINING_USER_COMPUTE_POWER);
					whiteList.add(MINING_USER_COMPUTE_POWER_TOP);

					//任务
					whiteList.add(TASK_CRATE_TASK);
					whiteList.add(TASK_DELETE_TASK);
					whiteList.add(TASK_EDIT_TASK);
					whiteList.add(TASK_TASK_LIST);
					whiteList.add(TASK_TASK_LISTDATE);
					whiteList.add(TASK_DONE_TASK);
					whiteList.add(TASK_LOGIN_TASK);
					whiteList.add(TASK_INVITATION_CODE);
					whiteList.add(TASK_RECORD_LIST);
					whiteList.add(TASK_REWARDRECORD);


					//交易所
					whiteList.add(EXCHANGE_ORDER_CREATE);
					whiteList.add(EXCHANGE_ORDER_SEARCH);
					whiteList.add(EXCHANGE_ORDER);

					// 蒜粒
					whiteList.add(SUANLI_CATEGORY_DELETE_CATALOG);
					whiteList.add(SUANLI_INVESTOR_PARTY_DETAIL);
					whiteList.add(SUANLI_PROJECT_ADD);
					whiteList.add(SUANLI_PROJECT_DELETE);
					whiteList.add(SUANLI_USER_ADD_FAVORITE);
					whiteList.add(SUANLI_USER_DELETE_FAVORITE);
					whiteList.add(SUANLI_USER_PROJECT);
					whiteList.add(SUANLI_USER_DATA);
					whiteList.add(SUANLI_OSS_UPLOAD_FILE);
					whiteList.add(SUANLI_USER_FAVORITE_PROJECT);
					whiteList.add(SUANLI_USER_FAVORITE_INVESTOR);
					whiteList.add(SUNALI_COMPANY_LIST);
					whiteList.add(SUANLI_INSTITUTION_LIST);

					whiteList.add(NEED_MESSAGE_DEL);
					whiteList.add(NEED_MESSAGE_EDIT);
					whiteList.add(NEED_MESSAGE_LIST);

					whiteList.add(COMPANY_EXAMPLE_ADD);
					whiteList.add(COMPANY_EXAMPLE_DEL);
					whiteList.add(COMPANY_EXAMPLE_EDIT);
					whiteList.add(COMPANY_EXAMPLE_LIST);

					//游戏大转盘
					whiteList.add(GAME_ZHUANPAN_LOAD);
					whiteList.add(GAME_ZHUANPAN_RUN);
					whiteList.add(GAME_RECORD_LIST);

					whiteList.add(GAME_QA_LIST);
					whiteList.add(GAME_QA_RECORD_LIST);
					whiteList.add(GAME_QA_ANSWER_LIST);
					whiteList.add(GAME_QA_GET_REWARD_LIST);

					//websocket
					whiteList.add(WALLET_APPWEBSOCKET);
					whiteList.add(PAY_PAYWEBSOCKET);
					whiteList.add(PAY_RETURN);
					
					//优惠券
					whiteList.add(COUPONS_GET_USER_COUPONS_BY_ORDER);
					whiteList.add(COUPONS_GET_USER_COUPONS_LIST);
					whiteList.add(COUPONS_USER_USAGE_COUPONS);
					whiteList.add(COUPONS_USER_MANUAL_GET_COUPONS);
                    whiteList.add(COUPONS_USER_MANUAL_CAN_GET_COUPONS_LIST);
					//拼团
					whiteList.add(ACTIVITY_GROUP_BOOKING_USER);

					//砍价
					whiteList.add(ACTIVITY_BARGAIN_USER);
					whiteList.add(ACTIVITY_GET_ACTIVITY_ID);

					//一块伴
					whiteList.add(TRADE_API_ORDER);
					whiteList.add(TRADE_API_PMSCOMMONPRODUCT);
					whiteList.add(TRADE_ADMIN);
					whiteList.add(TRADE_API_SSO);
					whiteList.add(ACTIVITY_GROUP_BOOKING_ADMIN);
					whiteList.add(ACTIVITY_GROUP_BOOKING_INTERNAL);
					whiteList.add(ACTIVITY_BARGAIN_ADMIN);
					whiteList.add(ACTIVITY_GET_USERID);

				}
			}catch(Exception e) {
				logger.error("",e);
			}finally {
				lock.unlock();
			}
		}

		return whiteList;
	}
	/**
	 * 判断是否在白名单内
	 * @param url
	 * @return
	 */
	public static boolean isInWhiteList(String url) {
		List<String> whiteList = getWhiteList();
		for(String whiteUrl:whiteList) {
			if(url.startsWith(whiteUrl)) {
				return true;
			}
		}
		return false;
	}


	public static boolean isInAppSignWhiteList(String url) {
		List<String> appSignShiteList = getAppSignList();
		for(String whiteUrl:appSignShiteList) {
			if(url.startsWith(whiteUrl)) {
				return true;
			}
		}
		return false;
	}
}
