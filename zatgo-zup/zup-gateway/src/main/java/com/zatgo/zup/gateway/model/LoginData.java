package com.zatgo.zup.gateway.model;

import io.swagger.annotations.ApiModelProperty;

public class LoginData {

	@ApiModelProperty(value="登录token",required=true)
	private String token;
	
	@ApiModelProperty(value="登录用户名",required=true)
	private String userName;
	
	@ApiModelProperty(value="登录用户ID",required=true)
	private String userId;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
