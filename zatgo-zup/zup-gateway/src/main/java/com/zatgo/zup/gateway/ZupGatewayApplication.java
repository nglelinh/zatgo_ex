package com.zatgo.zup.gateway;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
import org.springframework.boot.web.reactive.server.ReactiveWebServerFactory;
import org.springframework.boot.web.server.WebServer;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.server.reactive.HttpHandler;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.utils.HttpServerUtil;
import com.zatgo.zup.gateway.utils.AuthTokenUtil;
import com.zatgo.zup.gateway.utils.WhiteListUtils;

import reactor.core.publisher.Mono;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@ComponentScan(basePackages="com.zatgo.zup")
public class ZupGatewayApplication{

	@Value("${server.port}")
	private String httpsPort;
	@Autowired
	HttpHandler httpHandler;
	@Autowired
	private AuthTokenUtil authTokenUtil;

	WebServer http;


	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(ZupGatewayApplication.class, args);
	}

//	@PostConstruct
//	public void start() {
//		ReactiveWebServerFactory factory = new NettyReactiveWebServerFactory(Integer.valueOf(httpsPort) + 1);
//		this.http = factory.getWebServer(this.httpHandler);
//		this.http.start();
//	}

	@PreDestroy
	public void stop() {
		this.http.stop();
	}

	@Bean
	public KeyResolver userKeyResolver() {
	    return exchange -> {
	    	String userId = null;
	    	try {
	    		String userToken = HttpServerUtil.getHeader(exchange.getRequest(), "token");
	    		AuthUserInfo userInfo = authTokenUtil.getAuthUserInfo(userToken);
	    		if(userInfo == null) {
	    			userId = WhiteListUtils.getRemoteIP(exchange.getRequest());
	    		}else {
	    			if(userInfo.getIsAdmin()) {
	    				userId = userInfo.getAdminId();
	    			}else {
	    				userId = userInfo.getUserId();
	    			}
	    		}
	    	}catch(Throwable e) {
	    		userId = WhiteListUtils.getRemoteIP(exchange.getRequest());
	    	}
	    	
	    	return Mono.just("USER_"+userId);
	    };
	}

}
