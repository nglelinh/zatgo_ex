package com.zatgo.zup.gateway.filter.whitelist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AdminValidateList {

	private static final Logger logger = LoggerFactory.getLogger(AdminValidateList.class);

	private static List<String> adminValidateList = null;
	private static Lock lock = new ReentrantLock();
	
	// 蒜粒
	private static final String SUANLI_USER_ADD_FAVORITE = "/suanli/user/add/favorite";
    private static final String SUANLI_USER_DELETE_FAVORITE = "/suanli/user/delete/favorite";
    private static final String SUANLI_CAMPAIGN_ADD = "/suanli/campaign/add";
    private static final String SUANLI_CAMPAIGN_UPDATE = "/suanli/campaign/update";
    private static final String SUANLI_CAMPAIGN_DELETE = "/suanli/campaign/delete";
    private static final String SUANLI_COMPANY_ADD = "/suanli/company/add";
    private static final String SUANLI_COMPANY_UPDATE = "/suanli/company/update";
    private static final String SUANLI_COMPANY_DELETE = "/suanli/company/delete";
    private static final String SUANLI_INSTITUTION_ADD = "/suanli/institution/add";
    private static final String SUANLI_INSTITUTION_UPDATE = "/suanli/institution/update";
    private static final String SUANLI_INSTITUTION_DELETE = "/suanli/institution/delete";
    private static final String SUANLI_INVESTOR_ADD = "/suanli/investor/add";
    private static final String SUANLI_INVESTOR_UPDATE = "/suanli/investor/update";
    private static final String SUANLI_INVESTOR_DELETE = "/suanli/investor/delete";
    private static final String SUANLI_CATEGORY_UPDATE_CATALOG = "/suanli/category/update/catalog";
    private static final String SUANLI_CATEGORY_ADD_CATALOG = "/suanli/category/add/catalog";
    private static final String SUANLI_TAG_ADD = "/suanli/tag/add";
    private static final String SUANLI_TAG_UPDATE = "/suanli/tag/update";
    private static final String SUANLI_TAG_DELETE = "/suanli/tag/delete";
    private static final String SUANLI_PROJECT_LIST_ADMIN = "/suanli/project/list/admin";
    private static final String SUANLI_OSS_FILE_LIBRARY = "/suanli/oss/file/library";
    private static final String SUANLI_PARTY_LIST = "/suanli/party/list";
    private static final String SUANLI_PARTY_ADD = "/suanli/party/add";
    private static final String SUANLI_PARTY_UPDATE = "/suanli/party/update";
    private static final String SUANLI_PARTY_DETAIL = "/suanli/party/detail";

	private static final String NEED_MESSAGE_DEL = "/suanli/message/del";
	private static final String NEED_MESSAGE_EDIT = "/suanli/message/edit";
	private static final String NEED_MESSAGE_LIST = "/suanli/message/list";

	private static final String COMPANY_EXAMPLE_ADD = "/suanli/company/example/back/add";
	private static final String COMPANY_EXAMPLE_DEL = "/suanli/company/example/back/del";
	private static final String COMPANY_EXAMPLE_EDIT = "/suanli/company/example/back/edit";
	private static final String COMPANY_EXAMPLE_LIST = "/suanli/company/example/back/list";

    //钱包
    private static final String  WALLET_EXTRACT_RECORD_SELECT = "/wallet/extract/clouduser/record";
    private static final String  WALLET_EXTRACT_RECORD_APPROVAL = "/wallet/extract/clouduser/approval";
    private static final String  WALLET_DEPOSIT_RECORD_SELECT = "/wallet/deposit/clouduser/record";
    private static final String  WALLET_FIXED_TIME_DEPOSIT_ADD_PRO = "/wallet/fixedTimeDeposit/clouduser/addPro";
    private static final String  WALLET_FIXED_TIME_DEPOSIT_UPDATE_PRO = "/wallet/fixedTimeDeposit/clouduser/updatePro";
    private static final String  WALLET_FIXED_TIME_DEPOSIT_PRO_LIST = "/wallet/fixedTimeDeposit/clouduser/getProList";
    
    //用户中心
    private static final String  USER_CERTIFICATE_ADMIN_PAGEINFO = "/user/certificate/admin/pageInfo";
    private static final String  USER_CERTIFICATE_ADMIN_APPROVE = "/user/certificate/admin/approve";
    
    //优惠券
    private static final String  COUPONS_ACTIVITY_ADMIN_PRO = "/coupons/activity/admin";
    private static final String  COUPONS_ADMIN_PRO = "/coupons/admin";

	//拼团
	private static final String  ACTIVITY_GROUP_BOOKING_ADMIN = "/activity/groupBooking/admin";

	//砍价
	private static final String  ACTIVITY_BARGAIN_ADMIN = "/activity/bargain/admin";

	public static List<String> getAdminValidateList() {
		if (adminValidateList == null) {
			lock.lock();
			try {
				if (adminValidateList == null) {
					adminValidateList = new ArrayList<String>();
					adminValidateList.add(SUANLI_USER_ADD_FAVORITE);
					adminValidateList.add(SUANLI_USER_DELETE_FAVORITE);
					adminValidateList.add(SUANLI_TAG_ADD);
					adminValidateList.add(SUANLI_TAG_UPDATE);
					adminValidateList.add(SUANLI_TAG_DELETE);
					adminValidateList.add(SUANLI_CATEGORY_UPDATE_CATALOG);
					adminValidateList.add(SUANLI_CATEGORY_ADD_CATALOG);
					adminValidateList.add(SUANLI_INVESTOR_ADD);
					adminValidateList.add(SUANLI_INVESTOR_UPDATE);
					adminValidateList.add(SUANLI_INVESTOR_DELETE);
					adminValidateList.add(SUANLI_CAMPAIGN_ADD);
					adminValidateList.add(SUANLI_CAMPAIGN_UPDATE);
					adminValidateList.add(SUANLI_CAMPAIGN_DELETE);
					adminValidateList.add(SUANLI_COMPANY_ADD);
					adminValidateList.add(SUANLI_COMPANY_UPDATE);
					adminValidateList.add(SUANLI_COMPANY_DELETE);
					adminValidateList.add(SUANLI_INSTITUTION_ADD);
					adminValidateList.add(SUANLI_INSTITUTION_UPDATE);
					adminValidateList.add(SUANLI_INSTITUTION_DELETE);
					adminValidateList.add(SUANLI_PROJECT_LIST_ADMIN);
					adminValidateList.add(SUANLI_OSS_FILE_LIBRARY);
					adminValidateList.add(SUANLI_PARTY_LIST);
					adminValidateList.add(SUANLI_PARTY_ADD);
					adminValidateList.add(SUANLI_PARTY_UPDATE);
					adminValidateList.add(SUANLI_PARTY_DETAIL);
					adminValidateList.add(NEED_MESSAGE_DEL);
					adminValidateList.add(NEED_MESSAGE_EDIT);
					adminValidateList.add(NEED_MESSAGE_LIST);

					adminValidateList.add(COMPANY_EXAMPLE_ADD);
					adminValidateList.add(COMPANY_EXAMPLE_DEL);
					adminValidateList.add(COMPANY_EXAMPLE_EDIT);
					adminValidateList.add(COMPANY_EXAMPLE_LIST);

					adminValidateList.add(WALLET_EXTRACT_RECORD_SELECT);
					adminValidateList.add(WALLET_EXTRACT_RECORD_APPROVAL);
					adminValidateList.add(WALLET_DEPOSIT_RECORD_SELECT);
					adminValidateList.add(WALLET_FIXED_TIME_DEPOSIT_ADD_PRO);
					adminValidateList.add(WALLET_FIXED_TIME_DEPOSIT_UPDATE_PRO);
					adminValidateList.add(WALLET_FIXED_TIME_DEPOSIT_PRO_LIST);
					
					adminValidateList.add(USER_CERTIFICATE_ADMIN_PAGEINFO);
					adminValidateList.add(USER_CERTIFICATE_ADMIN_APPROVE);
					
					
					adminValidateList.add(COUPONS_ACTIVITY_ADMIN_PRO);
					adminValidateList.add(COUPONS_ADMIN_PRO);

					adminValidateList.add(ACTIVITY_GROUP_BOOKING_ADMIN);
					adminValidateList.add(ACTIVITY_BARGAIN_ADMIN);
					
				}
			} catch (Exception e) {
				logger.error("", e);
			} finally {
				lock.unlock();
			}
		}

		return adminValidateList;
	}

	public static boolean isInWhiteList(String url) {
		List<String> adminValidateList = getAdminValidateList();
		for (String adminValidateUrl : adminValidateList) {
			if (url.startsWith(adminValidateUrl)) {
				return true;
			}
		}
		return false;
	}

}
