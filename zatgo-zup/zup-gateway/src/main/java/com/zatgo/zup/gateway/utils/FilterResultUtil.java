package com.zatgo.zup.gateway.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import io.netty.buffer.ByteBufAllocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.NettyDataBufferFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 46041 on 2018/8/28.
 */
public class FilterResultUtil {

    private static final Logger logger = LoggerFactory.getLogger(FilterResultUtil.class);

    private static final Pattern PATTERN = Pattern.compile("^\\/ws\\/.*");

    /**
     * 失败的返回
     * @param serverWebExchange
     * @return
     */
    public static Mono<Void> getFailResult(ServerWebExchange serverWebExchange){
        ServerHttpResponse serverHttpResponse = serverWebExchange.getResponse();
        serverHttpResponse.setStatusCode(HttpStatus.OK);
        ResponseData respData = BusinessResponseFactory.createSystemError();
        DataBuffer buffer = serverWebExchange.getResponse().bufferFactory().wrap(JSON.toJSONString(respData).getBytes(StandardCharsets.UTF_8));
        return serverWebExchange.getResponse().writeWith(Flux.just(buffer));
    }

    public static Mono<Void> getFailResult(ServerWebExchange serverWebExchange, String businessExceptionCode){
        ServerHttpResponse serverHttpResponse = serverWebExchange.getResponse();
        serverHttpResponse.setStatusCode(HttpStatus.OK);
        ResponseData respData = BusinessResponseFactory.
                createBusinessError(businessExceptionCode);
        DataBuffer buffer = serverWebExchange.getResponse().bufferFactory().wrap(JSON.toJSONString(respData).getBytes(StandardCharsets.UTF_8));
        return serverWebExchange.getResponse().writeWith(Flux.just(buffer));
    }

    /**
     * 成功的返回
     * @param serverWebExchange
     * @param gatewayFilterChain
     * @return
     */
    public static Mono<Void> getSuccessResult(ServerWebExchange serverWebExchange, GatewayFilterChain gatewayFilterChain){
        return gatewayFilterChain.filter(serverWebExchange);
    }

    public static Mono<Void> getSuccessResult(ServerWebExchange serverWebExchange,
                                        GatewayFilterChain gatewayFilterChain,
                                        ResponseData<UserData> userData,
                                        RedisTemplate redisTemplate){
        AuthUserInfo data = new AuthUserInfo();
        data.setUserName(userData.getData().getUserName());
        data.setUserId(userData.getData().getUserId());
        String key = RedisKeyConstants.AUTH_TOKEN_PRE + userData.getData().getUserId();
        redisTemplate.opsForValue().set(key, data);
        ServerHttpRequest host = serverWebExchange.getRequest().mutate().header("token", userData.getData().getUserId()).build();
        ServerWebExchange build = serverWebExchange.mutate().request(host).build();
        return gatewayFilterChain.filter(build);
    }

    /**
     * 获取body内容
     *  调用 此方法后必须调用 afterGetBodyParams 方法去重新封装 ServerWebExchange
     * @param serverWebExchange
     * @return
     */
    public static JSONObject getBodyJSON(ServerWebExchange serverWebExchange) {
        try {
        	String requestBody = getBodyString(serverWebExchange);
            JSONObject json = JSONObject.parseObject(requestBody);
            if (json == null){
                json = new JSONObject();
            }
            return json;
        } catch (Exception e) {
            logger.error("",e);
            throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
        }
    }
    
    /**
     * 获取body内容
     *  调用 此方法后必须调用 afterGetBodyParams 方法去重新封装 ServerWebExchange
     * @param serverWebExchange
     * @return
     */
    public static String getBodyString(ServerWebExchange serverWebExchange) {
        try {
            ServerHttpRequest req = serverWebExchange.getRequest();
            Flux<DataBuffer> body = req.getBody();
            AtomicReference<String> bodyRef = new AtomicReference<>();
            body.subscribe(dataBuffer -> {
                CharBuffer charBuffer = StandardCharsets.UTF_8.decode(dataBuffer.asByteBuffer());
                DataBufferUtils.release(dataBuffer);
                bodyRef.set(charBuffer.toString());
            });
            String requestBody = bodyRef.get();
            return requestBody;
        } catch (Exception e) {
            logger.error("",e);
            throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
        }
    }

    /**
     *  调用 getBodyParams 之后必须调用此方法
     * @param serverWebExchange
     * @param requestBody
     * @return
     */
    public static ServerWebExchange afterGetBodyParams(ServerWebExchange serverWebExchange, String requestBody){
        ServerHttpRequest request = serverWebExchange.getRequest();
        DataBuffer bodyDataBuffer = stringBuffer(requestBody);
        Flux<DataBuffer> bf = Flux.just(bodyDataBuffer);
        request = new ServerHttpRequestDecorator(request){
            @Override
            public Flux<DataBuffer> getBody() {
                return bf;
            }
        };
        return serverWebExchange.mutate().request(request).build();
    }

    /**
     *  规定websocket请求都是以ws开头
     * @param uri
     * @return
     */
    public static boolean isWebSocket(String uri){
        Matcher isWebSocket = PATTERN.matcher(uri);
        return isWebSocket.matches();
    }


    public static DataBuffer stringBuffer(String value) {
     byte[] bytes = value.getBytes(StandardCharsets.UTF_8);

        NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(ByteBufAllocator.DEFAULT);
        DataBuffer buffer = nettyDataBufferFactory.allocateBuffer(bytes.length);
        buffer.write(bytes);
        return buffer;
    }

}
