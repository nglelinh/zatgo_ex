package com.zatgo.zup.gateway.swagger;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

@Component
@Primary
//@AllArgsConstructor
public class DocumentationConfig implements SwaggerResourcesProvider {

    public static final String API_URI = "/v2/api-docs";
    
    public static final String CLOUD_USER_APP_API_TYPE = "1";
    public static final String CLOUD_USER_ADMIN_API_TYPE = "2";
    public static final String APP_USER_CHECKOUT_API_TYPE = "3";
    public static final String TEST_EVN_API_TYPE = "4";
    
    public static final String URL_API_TYPE_KEY = "type";
    
    @Value("${system.isTestEnv:false}")
    public Boolean isTestEnv;


    @Override
    public List<SwaggerResource> get() {
        List resources = new ArrayList<>();
        resources.add(swaggerResource("----------------------------云用户APP接口---------------------------", "/wallet/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));
        resources.add(swaggerResource("钱包", "/wallet/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));
        resources.add(swaggerResource("收银台", "/pay/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));
        resources.add(swaggerResource("用户中心", "/user/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));
        resources.add(swaggerResource("挖矿", "/mining/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));
        resources.add(swaggerResource("交易所", "/exchange/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));
        resources.add(swaggerResource("创投业务", "/suanli/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));
        resources.add(swaggerResource("优惠券业务", "/coupons/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));
        resources.add(swaggerResource("活动中心", "/activity/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_APP_API_TYPE, "2.0"));

        resources.add(swaggerResource("", "", "2.0"));
        resources.add(swaggerResource("----------------------------商家APP接口----------------------------", "/pay/v2/api-docs?" + URL_API_TYPE_KEY+"="+APP_USER_CHECKOUT_API_TYPE, "2.0"));
        
        resources.add(swaggerResource("支付平台", "/pay/v2/api-docs?" + URL_API_TYPE_KEY+"="+APP_USER_CHECKOUT_API_TYPE, "2.0"));
        
        resources.add(swaggerResource("", "", "2.0"));
        resources.add(swaggerResource("---------------------------云用户管理后台接口---------------------------", "/wallet/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));
        
        resources.add(swaggerResource("钱包", "/wallet/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));
        resources.add(swaggerResource("收银台", "/pay/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));
        resources.add(swaggerResource("用户中心", "/user/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));
        resources.add(swaggerResource("挖矿", "/mining/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));
        resources.add(swaggerResource("交易所", "/exchange/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));
        resources.add(swaggerResource("创投业务", "/suanli/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));
        resources.add(swaggerResource("优惠券业务", "/coupons/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));
        resources.add(swaggerResource("活动中心", "/activity/v2/api-docs?" + URL_API_TYPE_KEY+"="+CLOUD_USER_ADMIN_API_TYPE, "2.0"));

        if(isTestEnv) {
        	resources.add(swaggerResource("", "", "2.0"));
        	resources.add(swaggerResource("--------------------------微服务内部接口--------------------------", "/wallet/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
        	resources.add(swaggerResource("钱包", "/wallet/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("收银台", "/pay/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("用户中心", "/user/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("挖矿", "/mining/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("交易所", "/exchange/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("创投业务", "/suanli/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("活动中心", "/activity/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("优惠券业务", "/coupons/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("trade-api", "/trade/api/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
            resources.add(swaggerResource("trade-admin", "/trade/admin/v2/api-docs?" + URL_API_TYPE_KEY+"="+TEST_EVN_API_TYPE, "2.0"));
        }
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }

}
