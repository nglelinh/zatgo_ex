package com.zatgo.zup.gateway.remoteservice;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SignInterfaceRecord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("zup-payment-web")
public interface RecordRemoteService {

	@RequestMapping(value = "/pay/sign/interface/record",method = RequestMethod.POST)
	@ResponseBody
	ResponseData<Boolean> insertRecord(@RequestBody SignInterfaceRecord record);


}
