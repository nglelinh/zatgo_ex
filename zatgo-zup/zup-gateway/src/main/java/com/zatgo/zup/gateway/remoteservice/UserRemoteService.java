package com.zatgo.zup.gateway.remoteservice;

import com.zatgo.zup.common.model.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("zup-merchant")
public interface UserRemoteService {

	@RequestMapping("/user/byname/{userName}")
	public ResponseData<UserData> getUser(@RequestParam("userName") String userName, @RequestParam("cloudUserId") String cloudUserId);
	
	@RequestMapping("/user/updatelogintime")
	public ResponseData<Object> updateLastLoginTime(@RequestBody String token);
	
	@RequestMapping("/user/create")
	public ResponseData<Object> createUser(@RequestBody UserData userData);
	
	@RequestMapping("/user/editPayPasswordByMW")
	public ResponseData<Object> editPayPasswordByMW(@RequestBody EditPasswordParams params);
	
	@RequestMapping("/user/getUserByMW")
	public ResponseData<UserData> getUserByMW(@RequestBody RequestData<String> params);

	@PostMapping("/user/cloud/get/manageUserId/{userId}/{cloudManageType}")
	ResponseData<UserData> getManageUserId(@PathVariable("userId") String userId,@PathVariable("cloudManageType") String cloudManageType);

	@PostMapping("/user/cloud/userApi/detail")
	ResponseData<CloudUserApi> selectUserApiDetail(@RequestParam("cloudUserId") String cloudUserId);

	@GetMapping("/user/system/country/info")
	List<Country> getCountryInfo();
	
	 @GetMapping(value = "/user/cloud/account/detail/{cloudUserId}")
	ResponseData<CloudUser> selectCloudUserDetail(@PathVariable("cloudUserId") String cloudUserId);
	
	@GetMapping(value = "/user/cloud/admin/byName/{cloudAdminName}")
	ResponseData<CloudAdminUserData> selectCloudAdminByName(@PathVariable("cloudAdminName") String cloudAdminName);

}
