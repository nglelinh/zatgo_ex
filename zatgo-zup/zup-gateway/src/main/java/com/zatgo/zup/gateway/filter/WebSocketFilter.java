package com.zatgo.zup.gateway.filter;

import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.gateway.filter.whitelist.AppTokenList;
import com.zatgo.zup.gateway.utils.AuthTokenUtil;
import com.zatgo.zup.gateway.utils.FilterResultUtil;
import com.zatgo.zup.gateway.utils.WhiteListUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * Created by 46041 on 2018/9/6.
 */
@Component
public class WebSocketFilter implements GlobalFilter, Ordered {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketFilter.class);

    @Autowired
    private AuthTokenUtil authTokenUtil;

    @Override
    public int getOrder() {
        return 2;
    }


    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, GatewayFilterChain gatewayFilterChain) {
        ServerHttpRequest request = serverWebExchange.getRequest();
        String requestURI = request.getURI().getPath();

        //只校验在白名单中的websocket
        if(FilterResultUtil.isWebSocket(requestURI) && 
        		(WhiteListUtils.isInCloudUserAppAuthInfList(request) 
				|| WhiteListUtils.isInCloudUserAdminAuthInfList(request))){
            try {
                //检验token
                MultiValueMap<String, String> queryParams = request.getQueryParams();
                if (!queryParams.containsKey("token")) {
                    return FilterResultUtil.getFailResult(serverWebExchange, BusinessExceptionCode.TOKEN_VERIFY_FAIL);
                }
                String token = queryParams.getFirst("token");
                if (authTokenUtil.loginVerify(token)) {//通过
                    //校验通过，则刷新token过期时间
                    authTokenUtil.refreshLoginExpire(token);
                }
            } catch (Exception e){
                logger.error("", e);
                return FilterResultUtil.getFailResult(serverWebExchange);
            }
        }
        return FilterResultUtil.getSuccessResult(serverWebExchange, gatewayFilterChain);
    }
}
