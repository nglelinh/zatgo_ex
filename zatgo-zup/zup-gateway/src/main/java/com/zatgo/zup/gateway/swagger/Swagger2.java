package com.zatgo.zup.gateway.swagger;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
//@EnableSwagger2
public class Swagger2 {
	@Bean
	public Docket createRestApi() {
		//添加head参数start  
        List<Parameter> pars = new ArrayList<Parameter>();  
        
        ParameterBuilder tokenPar = new ParameterBuilder();  
        tokenPar.name("token").description("令牌").modelRef(new ModelRef("string")).parameterType("header").required(false).build(); 
        pars.add(tokenPar.build()); 
        
//        ParameterBuilder mwPar = new ParameterBuilder();  
//        mwPar.name("mnemonicWord").description("助记词").modelRef(new ModelRef("string")).parameterType("header").required(false).build(); 
//        pars.add(mwPar.build()); 
        
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.zatgo.zup.gateway.controller"))
				.paths(PathSelectors.any())
				.build()
				.globalOperationParameters(pars);
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
								.title("分布式统一支付平台(ZUP)")
								.description("平台接口文档说明")
								.termsOfServiceUrl("www.zatgo.net")
								.version("1.0")
								.build();
	}




}
