//package com.zatgo.zup.gateway.filter;
//
//import java.io.UnsupportedEncodingException;
//import java.nio.CharBuffer;
//import java.nio.charset.StandardCharsets;
//import java.util.HashMap;
//import java.util.List;
//import java.util.concurrent.atomic.AtomicReference;
//import java.util.function.Function;
//import java.util.stream.Collectors;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.core.Ordered;
//import org.springframework.core.io.buffer.DataBuffer;
//import org.springframework.core.io.buffer.DataBufferUtils;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
//import org.springframework.stereotype.Component;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.server.ServerWebExchange;
//
//import com.zatgo.zup.common.exception.BusinessExceptionCode;
//import com.zatgo.zup.gateway.filter.whitelist.FlyByOpenApiPublicAccessList;
//import com.zatgo.zup.gateway.model.FlybyOpenApiCallBackMethod;
//import com.zatgo.zup.gateway.utils.FilterResultUtil;
//import com.zatgo.zup.gateway.utils.RequestBodyUtil;
//
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//
///**
// * flyby-openapi专用签名filter
// *
// * @author Administrator
// *
// */
//@Component
//public class FlybySignFilter implements GlobalFilter, Ordered {
//
//	@Value("${system.checkSign}")
//	private boolean checkSign;
//
//	@Autowired
//	private RequestBodyUtil requestBodyUtil;
//
//	@Autowired
//	private FlybyOpenApiCallBackMethod callBackMethod;
//
//	@Override
//	public int getOrder() {
//		return 13;
//	}
//
//	@Override
//	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//		ServerHttpRequest request = exchange.getRequest();
//		String requestURI = request.getURI().getPath();
//
//		if (!checkSign || !FlyByOpenApiPublicAccessList.isInPublicAccessList(requestURI)) {
//			return FilterResultUtil.getSuccessResult(exchange, chain);
//		}
//
//		ServerHttpRequestDecorator requestDecorator = requestBodyUtil.getServerHttpRequestDecorator(exchange,
//				callBackMethod);
//
//		AtomicReference<String> bodyRef = new AtomicReference<>();
//		Flux<DataBuffer> flux = requestDecorator.getBody();
//		List<DataBuffer> buffers = flux.toStream().collect(Collectors.toList());
//		for (DataBuffer dataBuffer : buffers) {
//			CharBuffer charBuffer = StandardCharsets.UTF_8.decode(dataBuffer.asByteBuffer());
//            DataBufferUtils.release(dataBuffer);
//            bodyRef.set(charBuffer.toString());
//		}
//        //获取request body
//        String bodyStr = bodyRef.get();
//        boolean res = callBackMethod.callBack(bodyStr, null, requestDecorator);
//        if(!res) {
//        	return FilterResultUtil.getFailResult(exchange, BusinessExceptionCode.SIGN_ERROR);
//        }
//
//
//		ServerWebExchange swe = exchange.mutate().request(requestDecorator).build();
//		return FilterResultUtil.getSuccessResult(swe, chain);
//
//	}
//
//}
