package com.zatgo.zup.gateway.utils;

import com.zatgo.zup.common.utils.HttpServerUtil;
import com.zatgo.zup.gateway.filter.whitelist.AdminValidateList;
import com.zatgo.zup.gateway.filter.whitelist.AppTokenList;
import com.zatgo.zup.gateway.filter.whitelist.PublicAccessList;
import com.zatgo.zup.gateway.filter.whitelist.SignAccessList;
import com.zatgo.zup.gateway.model.SystemInterfaceData;
import org.springframework.http.server.reactive.ServerHttpRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WhiteListUtils {

	/**
	 * 当前访问接口是否为云用户的客户接口
	 * @param url
	 * @return
	 */
	public static boolean isInCloudUserAppAuthInfList(ServerHttpRequest request) {
		String requestURI = request.getURI().getPath();
		
		if(AppTokenList.isInWhiteList(requestURI)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 当前访问接口是否为云用户后台管理员接口
	 * @param url
	 * @return
	 */
	public static boolean isInCloudUserAdminAuthInfList(ServerHttpRequest request) {
		String requestURI = request.getURI().getPath();
		
		if(AdminValidateList.isInWhiteList(requestURI)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 当前访问接口是否为业务签名接口
	 * @param url
	 * @return
	 */
	public static boolean isInSignInfList(ServerHttpRequest request) {
		String requestURI = request.getURI().getPath();
		
		if(SignAccessList.isInSignAccessList(requestURI)) {
			return true;
		}
		
		return false;
	}
	
	
	public static boolean isAppSignInfList(ServerHttpRequest request) {
		String requestURI = request.getURI().getPath();
		
		if(AppTokenList.isInAppSignWhiteList(requestURI)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 当前访问接口是否为外部接口
	 * @param requestURI
	 * @param ips 内部接口可信IP列表访问
	 * @return
	 */
	public static boolean isCanAccessInf(ServerHttpRequest request, SystemInterfaceData data) {
		String requestURI = request.getURI().getPath();
		
		if(AppTokenList.isInWhiteList(requestURI)) {
			return true;
		}
		
		if(PublicAccessList.isInPublicAccessList(requestURI)) {
			return true;
		}
		
		if(SignAccessList.isInSignAccessList(requestURI)) {
			return true;
		}
		
		if(data.isAdminGateway() == true && AdminValidateList.isInWhiteList(requestURI)) {
			return true;
		}

		if (isInner(getRemoteIP(request),data)){
			return true;
		}

		//内部服务调用内部接口校验
		List<String> allIps = new ArrayList<>();
		List<String> directAccessInternalInterfaceIPs = data.getDirectAccessInternalInterfaceIPs();
		if(directAccessInternalInterfaceIPs != null) {
			for(String ip:directAccessInternalInterfaceIPs) {
				allIps.add(ip);
			}
		}
		if(allIps != null && allIps.size() > 0 && allIps.contains(getRemoteIP(request))) {
			return true;
		}

		return false;
	}


	private static boolean isInner(String ip,SystemInterfaceData data)
	{
		//TODO 待完善
//		String reg = "^(127\\.0\\.0\\.1)|(localhost)|(10\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})|(172\\.((1[6-9])|(2\\d)|(3[01]))\\.\\d{1,3}\\.\\d{1,3})|(192\\.168\\.\\d{1,3}\\.\\d{1,3})$";
//		Pattern p = Pattern.compile(reg);
//		Matcher matcher = p.matcher(ip);
//		return matcher.find();
		return true;
	}

	public static String getRemoteIP(ServerHttpRequest request) {
		String ip = HttpServerUtil.getHeader(request, "x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = HttpServerUtil.getHeader(request, "Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = HttpServerUtil.getHeader(request, "WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddress().getHostString();
		}
		if (ip != null && ip.length() > 0) {
			String[] ipArray = ip.split(",");
			if (ipArray != null && ipArray.length > 1) {
				return ipArray[0];
			}
			return ip;
		}

		return "";
	}
	
	
	
	
}
