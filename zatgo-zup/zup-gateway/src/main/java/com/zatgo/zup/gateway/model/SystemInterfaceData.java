package com.zatgo.zup.gateway.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "system.interface")
public class SystemInterfaceData {
	
	private Logger logger = LoggerFactory.getLogger(SystemInterfaceData.class);
	
	private List<String> credibleips;
	
	private List<String> credibleDomainNames;
	
	@Value("${system.interface.isVerifyCredible:true}")
	private boolean isVerifyCredible;
	
	@Value("${system.interface.isAdminGateway:false}")
	private boolean isAdminGateway;
	
	private long webSignExpireMinute;

	private List<String> directAccessInternalInterfaceIPs;

	public long getWebSignExpireMinute() {
		return webSignExpireMinute;
	}

	public void setWebSignExpireMinute(long webSignExpireMinute) {
		this.webSignExpireMinute = webSignExpireMinute;
	}

	public boolean isVerifyCredible() {
		return isVerifyCredible;
	}

	public void setVerifyCredible(boolean isVerifyCredible) {
		this.isVerifyCredible = isVerifyCredible;
	}

	public List<String> getCredibleDomainNames() {
		return credibleDomainNames;
	}

	public void setCredibleDomainNames(List<String> credibleDomainNames) {
		this.credibleDomainNames = credibleDomainNames;
	}

	public List<String> getCredibleips() {
		return credibleips;
	}

	public void setCredibleips(List<String> credibleips) {
		this.credibleips = credibleips;
	}

	public boolean isAdminGateway() {
		return isAdminGateway;
	}

	public void setAdminGateway(boolean isAdminGateway) {
		this.isAdminGateway = isAdminGateway;
	}

	public List<String> getDirectAccessInternalInterfaceIPs() {
		return directAccessInternalInterfaceIPs;
	}

	public void setDirectAccessInternalInterfaceIPs(List<String> directAccessInternalInterfaceIPs) {
		this.directAccessInternalInterfaceIPs = directAccessInternalInterfaceIPs;
	}
}
