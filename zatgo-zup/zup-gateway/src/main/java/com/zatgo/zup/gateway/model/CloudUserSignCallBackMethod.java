package com.zatgo.zup.gateway.model;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CloudUserApi;
import com.zatgo.zup.common.utils.HttpServerUtil;
import com.zatgo.zup.common.utils.SignUtils;
import com.zatgo.zup.gateway.utils.CheckCredibleSourceUtil;
import com.zatgo.zup.gateway.utils.FilterResultUtil;

@Component
public class CloudUserSignCallBackMethod implements StringCallBackMethod{

	private static final Logger logger = LoggerFactory.getLogger(CloudUserSignCallBackMethod.class);
	
	@Autowired
    private FilterConstant filterConstant;
	
	@Autowired
    private CheckCredibleSourceUtil checkCredibleSourceUtil;
	
	@Override
	public boolean callBack(String json,String appId,ServerHttpRequest request) {
		String cloudUserId = request.getQueryParams().getFirst(filterConstant.CLOUD_USER_ID_KEY);
        if (StringUtils.isEmpty(cloudUserId))
            cloudUserId = filterConstant.systemCloudUserId;
        
        //验证签名
        if (!verify(request, json, cloudUserId)) {//通过
        	throw new BusinessException(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
        }
		return false;
	}
	
	/**
     * 从请求header中获取AppInfo
     * @return
     */
    private AppAuthInfo getAppInfo(ServerHttpRequest request){
        String appId = HttpServerUtil.getHeader(request, "appId");
        String appType = HttpServerUtil.getHeader(request, "appType");
        String appVersion = HttpServerUtil.getHeader(request, "appVersion");
        String ts = HttpServerUtil.getHeader(request, "ts");
        String token = HttpServerUtil.getHeader(request, "token");
        String sign = HttpServerUtil.getHeader(request, "sign");

        if(StringUtils.isEmpty(appId)
                ||StringUtils.isEmpty(appType)
                ||StringUtils.isEmpty(appVersion)
                ||StringUtils.isEmpty(ts)){
            throw new BusinessException(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
        }
        return new AppAuthInfo(appId,appType,appVersion,ts,token,sign);
    }

    /**
     * 鉴权验证
     * @param request
     * @param params
     */
    private boolean verify(ServerHttpRequest request,String paramJson, String cloudUserId) {
    	JSONObject params = null;
    	if(paramJson != null && paramJson.length() > 0) {
    		params = JSONObject.parseObject(paramJson);
    	}else {
    		params = new JSONObject();
    	}
    	
    	
        AppAuthInfo appInfo = getAppInfo(request);

        //整理签名内容
        JSONObject jsonParams = (JSONObject)JSONObject.toJSON(appInfo);
        if(params!=null){
            Iterator paramsIter = params.keySet().iterator();
            while(paramsIter.hasNext()){
                Object obj = paramsIter.next();
                if(obj!=null){
                    try {
                        JSONObject json = (JSONObject)JSONObject.toJSON(obj);
                        jsonParams.putAll(json);
                    } catch (Exception e) {
                        logger.error("",e);
                    }
                }
            }
        }

        CloudUserApi cloudUserApi = checkCredibleSourceUtil.findCloudUserApi(cloudUserId);
        String publicKey = cloudUserApi == null ? null : cloudUserApi.getPublicKey();
        /**
         * 验签
         */
        return SignUtils.signCheck(params, appInfo.getSign(), publicKey);

    }

}
