package com.zatgo.zup.gateway.filter.whitelist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 签名接口校验
 * @Title
 * @author lincm
 * @date 2018年4月3日 下午3:17:52
 */
public class PublicAccessList {
	
	private static final Logger logger = LoggerFactory.getLogger(PublicAccessList.class);
	
	//注册前操作
	public final static String USER_EXIST  = "/user/exist";
	public final static String USER_FORGET_LOGIN_PASSWORD  = "/user/forgetLoginPassword";
	public final static String SYSTEM_COUNTRY_INFO  = "/auth/country/info";
	
	//网关系统
	private static final String AUTH_WECHAT_LOGIN = "/auth/weixinLogin";
	private static final String AUTH_LOGIN = "/auth/login";
	private static final String AUTH_LOGOUT = "/auth/logout";
	private static final String AUTH_SENDPHONEAUTHCODE = "/auth/sendPhoneAuthCode";
	private static final String AUTH_SENDEMAILAUTHCODE = "/auth/sendEmailAuthCode";
	private static final String AUTH_VERIFYAUTHCODE = "/auth/verifyAuthCode";
	private static final String AUTH_REGISTER = "/auth/register";
	private static final String ADMIN_LOGIN = "/auth/login/admin";

	//API文档
	private static final String SWAGGER_DOC_WALLET = "/wallet/v2/api-docs";
	private static final String SWAGGER_DOC_PAY = "/pay/v2/api-docs";
	private static final String SWAGGER_DOC_MERCHANT = "/user/v2/api-docs";
	private static final String SWAGGER_DOC_MINING = "/mining/v2/api-docs";
	private static final String SWAGGER_DOC_EXCHANGE = "/exchange/v2/api-docs";
	private static final String SWAGGER_DOC_SUANLI = "/suanli/v2/api-docs";
	private static final String SWAGGER_DOC_ACTIVITY = "/activity/v2/api-docs";
	private static final String SWAGGER_DOC_COUPONS = "/coupons/v2/api-docs";

	private static final String USER_SYSTEM_APP_VERSION = "/user/system/app/version";
	private static final String USER_SYSTEM_CAROUSELIMAGE_LIST = "/user/system/carouselImage/list";

	// 蒜粒
    private static final String SUANLI_TAG_LIST = "/suanli/tag/list";
    private static final String SUANLI_CATEGORY_CITY = "/suanli/category/city/list";
    private static final String SUANLI_CATEGORY_CATALOG_LIST = "/suanli/category/catalog/list";
    private static final String SUANLI_CATEGORY_STAGE_LIST = "/suanli/category/stage/list";
    private static final String SUANLI_CAMPAIGN_LIST = "/suanli/campaign/list";
    private static final String SUANLI_CAMPAIGN_DETAIL = "/suanli/campaign/detail";
    private static final String SUANLI_CAMPAIGN_REGISTRATION = "/suanli/campaign/registration";
    private static final String SUANLI_CATEGORY_CATALOG_BASE_LIST = "/suanli/category/catalog/base/list";
    private static final String SUANLI_INVESTOR_LIST = "/suanli/investor/list";
    private static final String SUANLI_INVESTOR_DETAIL = "/suanli/investor/detail";
    private static final String SUANLI_PROJECT_LIST = "/suanli/project/list";
    private static final String SUANLI_PROJECT_DETAIL = "/suanli/project/detail";
    
    private static final String PAY_COIN_LIST = "/pay/coin/list";

	private static final String NEED_MESSAGE_ADD = "/suanli/message/add";

	private static final String COMPANY_EXAMPLE_FRONT_LIST = "/suanli/company/example/front/list";
	private static final String COMPANY_EXAMPLE_DETAIL = "/suanli/company/example/detail";


	//微信授权
	private static final String WX_AUTH_USER_GET_USER_OPENID = "/user/wx/auth/user/getUserOpenid";

	private static final String WX_AUTH_LOGIN = "/user/wx/auth/login";

	private static final String WX_AUTH_CONFIG = "/user/wx/auth/config";

    //交易所
    private static final String EXCHANGE_COIN_PAIRS = "/exchange/appapi/coinPairs";
    private static final String EXCHANGE_COIN_RATE = "/exchange/appapi/rate";
    private static final String EXCHANGE_WS = "/ws/exchange/appapi";

	//搜索引擎
	private static final String SEARCH_IMAGE = "/search/image/scenicSpot";
	private static final String SEARCH_SEARCH = "/search/ScenicSpot";

	private static final String PRODUCT_SEARCH = "/trade/api/esProduct";
	private static final String PRODUCT = "/trade/api/pmsProduct";
	private static final String PRODUCT_FLIGHT = "/trade/api/pmsProduct/flight";
	private static final String PRODUCT_SMSADVERTISE = "/trade/api/smsAdvertise";
	private static final String PRODUCT_SMSSUBJECT = "/trade/api/smsSubject";
	private static final String PRODUCT_THIRD = "/trade/api/third";
	private static final String PRODUCT_WX_MSG = "/trade/api/wx/msg";
	private static final String PRODUCT_WECHAT = "/trade/api/wechat";
	private static final String PRODUCT_PARAMS = "/trade/api/params";
	private static final String PRODUCT_ARTICLE = "/trade/api/pmsArticle";








	private static final String FLYBY_OPEN_API = "/openapi";

	//活动
	private static final String ACTIVITY_USER = "/activity/user";
	private static final String ACTIVITY_ADMIN = "/activity/admin";
	private static final String ACTIVITY_QRCODE = "/activity/productActivity";
	private static final String ACTIVITY_WX = "/activity/wx/msg/token";
	private static final String ACTIVITY_COMMON = "/activity/productList";

	private static final String WECHAT_WX = "/wechat/wx/msg/token";

	private static final String YKB_WECHAT_PAY_NOTIFY = "/wechat/weixinpay/callback/pay";



	//flyby-flight-corpapi
	private static final String FLYBY_CORP_API_FLIGHT = "/corpapi/flight";
	private static final String FLYBY_CORP_API_TEST = "/corpapi/test";

	//flyby-tmc-corpapi
	private static final String FLYBY_CORP_API_CORP = "/corpapi/corp";

	//flyby-flight-channel  回调
	private static final String FLYBY_CHANNEL_CALLBACK = "/flight/channel/callback";

	private static final String  FLYBY_VAS_HAPI_CALLBACK = "/vas/flight/hapi/callback";
	private static final String FLYBY_VAS_MO_CALLBACK = "/tmc/notice/lxhulian/callback/MO";
	private static final String FLYBY_VAS_SANTI_UPSMS_CALLBACK = "/tmc/notice/santi/callback/upsms";
	private static final String FLYBY_VAS_SANTI_CHECK_CALLBACK = "/tmc/notice/santi/callback/check";


	private static List<String> publicList = null;
	private static Lock lock = new ReentrantLock();
	
	/**
	 * 获取白名单列表
	 * @return
	 */
	public static List<String> getPublicList(){
		if(publicList == null) {
			lock.lock();
			try {
				if(publicList == null) {
					publicList = new ArrayList<>();

					publicList.add(SWAGGER_DOC_WALLET);
					publicList.add(SWAGGER_DOC_PAY);
					publicList.add(SWAGGER_DOC_MERCHANT);
					publicList.add(SWAGGER_DOC_MINING);
					publicList.add(SWAGGER_DOC_SUANLI);
					publicList.add(SWAGGER_DOC_COUPONS);
					publicList.add(SWAGGER_DOC_EXCHANGE);
					publicList.add(SWAGGER_DOC_ACTIVITY);

					publicList.add(USER_EXIST);
					publicList.add(USER_FORGET_LOGIN_PASSWORD);

					publicList.add(USER_SYSTEM_APP_VERSION);
					publicList.add(USER_SYSTEM_CAROUSELIMAGE_LIST);
					publicList.add(SWAGGER_DOC_EXCHANGE);

					publicList.add(SYSTEM_COUNTRY_INFO);
					
					publicList.add(SUANLI_TAG_LIST);
					publicList.add(SUANLI_CATEGORY_CITY);
					publicList.add(SUANLI_CATEGORY_CATALOG_LIST);
					publicList.add(SUANLI_CATEGORY_STAGE_LIST);
					publicList.add(SUANLI_CAMPAIGN_REGISTRATION);
					publicList.add(SUANLI_CAMPAIGN_LIST);
					publicList.add(SUANLI_CAMPAIGN_DETAIL);
					publicList.add(SUANLI_CATEGORY_CATALOG_BASE_LIST);
					publicList.add(SUANLI_INVESTOR_LIST);
					publicList.add(SUANLI_INVESTOR_DETAIL);
					publicList.add(SUANLI_PROJECT_LIST);
					publicList.add(SUANLI_PROJECT_DETAIL);
					publicList.add(NEED_MESSAGE_ADD);
					publicList.add(COMPANY_EXAMPLE_FRONT_LIST);
					publicList.add(COMPANY_EXAMPLE_DETAIL);

					publicList.add(PAY_COIN_LIST);

					publicList.add(AUTH_LOGIN);
					publicList.add(AUTH_LOGOUT);
					publicList.add(AUTH_SENDPHONEAUTHCODE);
					publicList.add(AUTH_SENDEMAILAUTHCODE);
					publicList.add(AUTH_VERIFYAUTHCODE);
					publicList.add(AUTH_REGISTER);
					publicList.add(AUTH_WECHAT_LOGIN);
					
					publicList.add(ADMIN_LOGIN);
					publicList.add(SYSTEM_COUNTRY_INFO);


                    publicList.add(WX_AUTH_USER_GET_USER_OPENID);
                    publicList.add(WX_AUTH_CONFIG);
                    publicList.add(WX_AUTH_LOGIN);

					publicList.add(EXCHANGE_COIN_PAIRS);
					publicList.add(EXCHANGE_COIN_RATE);
					publicList.add(EXCHANGE_WS);

					publicList.add(SEARCH_IMAGE);
					publicList.add(SEARCH_SEARCH);

					publicList.add(ACTIVITY_USER);
					publicList.add(ACTIVITY_ADMIN);
					publicList.add(ACTIVITY_QRCODE);
					publicList.add(ACTIVITY_WX);
					publicList.add(ACTIVITY_COMMON);

					publicList.add(WECHAT_WX);
					publicList.add(YKB_WECHAT_PAY_NOTIFY);

					publicList.add(PRODUCT_SEARCH);
					publicList.add(PRODUCT);
					publicList.add(PRODUCT_FLIGHT);
					publicList.add(PRODUCT_SMSADVERTISE);
					publicList.add(PRODUCT_SMSSUBJECT);
					publicList.add(PRODUCT_THIRD);
					publicList.add(PRODUCT_WX_MSG);
					publicList.add(PRODUCT_WECHAT);
					publicList.add(PRODUCT_PARAMS);
					publicList.add(PRODUCT_ARTICLE);

					publicList.add(FLYBY_CORP_API_FLIGHT);
					publicList.add(FLYBY_CORP_API_CORP);
					publicList.add(FLYBY_CHANNEL_CALLBACK);
					publicList.add(FLYBY_CORP_API_TEST);

					publicList.add(FLYBY_VAS_HAPI_CALLBACK);
					publicList.add(FLYBY_VAS_MO_CALLBACK);
					publicList.add(FLYBY_VAS_SANTI_UPSMS_CALLBACK);
					publicList.add(FLYBY_VAS_SANTI_CHECK_CALLBACK);
				}
			}catch(Exception e) {
				logger.error("",e);
			}finally {
				lock.unlock();
			}
		}
		
		return publicList;
	}
	
	/**
	 * 判断是否为签名校验
	 * @param url
	 * @return
	 */
	public static boolean isInPublicAccessList(String url) {
		List<String> publicList = getPublicList();
		for(String signAccessUrl:publicList) {
			if(url.startsWith(signAccessUrl)) {
				return true;
			}
		}
		return false;
	}
}
