package com.zatgo.zup.gateway.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.model.CloudUserApi;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.gateway.model.SystemInterfaceData;
import com.zatgo.zup.gateway.remoteservice.UserRemoteService;

/**
 * Created by 46041 on 2018/9/17.
 */

@Component
public class CheckCredibleSourceUtil {

    private static final Logger logger = LoggerFactory.getLogger(CheckCredibleSourceUtil.class);

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private UserRemoteService userRemoteService;
    
    @Autowired
    private SystemInterfaceData data;

    /**
     * 校验IP
     * @return
     */
    public boolean checkCredibleSource(String ip,String domainName, String cloudUserId){
        return true;
        //TODO 先注释点，等待后期完善
//    	if(!data.isVerifyCredible()) {
//    		return true;
//    	}
//
//    	if(verifyCredibleips(ip,cloudUserId)
//    			|| verifyCredibleDomainNames(domainName, cloudUserId)) {
//    		return true;
//    	}else {
//    		return false;
//    	}
    }

    
    /**
	 * 校验域名
	 * @param domain
	 * @return
	 */
	private boolean verifyCredibleDomainNames(String domainName, String cloudUserId) {
		List<String> credibleDomainNames = data.getCredibleDomainNames();
		if(credibleDomainNames != null && credibleDomainNames.size() > 0) {
			for(String credibleDomainName:credibleDomainNames) {
				if(domainName != null 
						&& credibleDomainName != null 
						&& domainName.startsWith(credibleDomainName)) {
					return true;
				}
			}
			return false;
		}else {
			return false;
		}
	}

    /**
	 * 校验IP
	 * @param ip
	 * @return
	 */
	private boolean verifyCredibleips(String ip,String cloudUserId) {
		List<String> allIps = new ArrayList<>();
    	
    	//云用户自定义的IP
        CloudUserApi cloudUserApi = findCloudUserApi(cloudUserId);
        String bindIps = cloudUserApi.getBindIps();//多个ip用逗号隔开
        if(bindIps != null) {
        	String[] bindIpArray = bindIps.split(",");
        	for(String bindIp:bindIpArray) {
        		allIps.add(bindIp);
        	}
        }
        
        //平台自己部署的配置
        List<String> credibleIps = data.getCredibleips();
        if(credibleIps != null) {
        	for(String credibleIp:credibleIps) {
        		allIps.add(credibleIp);
        	}
        }
        
		if(allIps != null && allIps.size() > 0) {
			return allIps.contains(ip);
		}else {
			return false;
		}
	}
	
	public CloudUserApi findCloudUserApi(String cloudUserId){
        //获取用户
        CloudUserApi cloudUserApi = null;
        String key = RedisKeyConstants.CLOUD_USER_KEY + cloudUserId;
        if (redisUtils.hasKey(key)){
            cloudUserApi = redisUtils.get(key, CloudUserApi.class);
        } else {
            //初始化
            cloudUserApi = getCloudUserApi(cloudUserId);
            redisUtils.put(key, cloudUserApi);
        }
        return cloudUserApi;
    }

    private CloudUserApi getCloudUserApi(String cloudUserId){
        CloudUserApi cloudUserApi = null;
        try {
            ResponseData<CloudUserApi> pageInfoResponseData = userRemoteService.selectUserApiDetail(cloudUserId);
            if (pageInfoResponseData.isSuccessful()){
                CloudUserApi res = pageInfoResponseData.getData();
                if (res != null){
                    cloudUserApi = res;
                } else {
                    cloudUserApi = new CloudUserApi();
                    cloudUserApi.setBindIps("-11111");
                    cloudUserApi.setPublicKey("-22222");
                }
            }
        } catch (Exception e){
            logger.error("", e);
            throw e;
        }
        return cloudUserApi;
    }
}
