package com.zatgo.zup.gateway.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wallet.WalletAccountData;

@FeignClient("zup-wallet")
public interface AccountRemoteService {

	@RequestMapping(value = "/wallet/account/innerCreate",method = RequestMethod.POST )
	@ResponseBody
	public ResponseData<WalletAccountData> innerCreateAccount(@RequestBody CreateAccountParams params);
}
