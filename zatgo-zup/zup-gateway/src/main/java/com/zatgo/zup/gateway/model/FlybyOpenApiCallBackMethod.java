package com.zatgo.zup.gateway.model;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.utils.BeanToMapUtils;
import com.zatgo.zup.common.utils.SignUtils;
import com.zatgo.zup.gateway.remoteservice.FlybyRemoteService;

@Component
public class FlybyOpenApiCallBackMethod implements StringCallBackMethod {

	private static final Logger logger = LoggerFactory.getLogger(FlybyOpenApiCallBackMethod.class);

	@Autowired
	private FlybyRemoteService flybyRemoteService;

	@Override
	public boolean callBack(String json, String appId, ServerHttpRequest request) {

		HttpMethod submitMehtod = request.getMethod();
		if (submitMehtod.equals(HttpMethod.POST)) {
			JSONObject jsonObject = JSON.parseObject(json);
			return verify(BeanToMapUtils.mapToStringMap(jsonObject.getInnerMap()));
		} else if (submitMehtod.equals(HttpMethod.GET)) {
			return verify(request.getQueryParams().toSingleValueMap());
		}

		return false;
	}

	private boolean verify(Map<String, String> params) {

		String sign = params.get("sign");
		if (StringUtils.isBlank(sign)) {
			logger.error("sign is blank：{}", sign);
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "sign不能为空");
			// return false;
		}
		String tmcCode = params.get("tmcCode");
		if (StringUtils.isBlank(tmcCode)) {
			logger.error("tmcCode is blank：{}", tmcCode);
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "tmcCode不能为空");
			// return false;
		}

		ResponseData<String> responseData = flybyRemoteService.getTmcRsaPublicKey(tmcCode);
		String publicKey = responseData.getData();
		if (StringUtils.isEmpty(publicKey)) {
			// throw new
			// BusinessException(BusinessExceptionCode.OPEN_API_PUBLIC_KEY_NOT_SET);
			logger.error("tmc public key is empty:{}", publicKey);
			return false;
		}

		boolean res = SignUtils.signCheck(params, sign, publicKey);
		if (!res) {
			// throw new
			// BusinessException(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
			logger.error("签名校验失败，tmcCode：{}，sign：{}，params：{}", tmcCode, sign, params);
			return false;
		}

		return true;
	}

}
