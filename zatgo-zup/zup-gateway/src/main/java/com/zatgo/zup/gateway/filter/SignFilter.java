package com.zatgo.zup.gateway.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.zatgo.zup.gateway.filter.whitelist.SignAccessList;
import com.zatgo.zup.gateway.model.ExternalSignCallBackMethod;
import com.zatgo.zup.gateway.utils.FilterResultUtil;
import com.zatgo.zup.gateway.utils.RequestBodyUtil;

import reactor.core.publisher.Mono;

/**
 * Created by 46041 on 2018/9/18.
 */

@Component
public class SignFilter implements GlobalFilter, Ordered {

    private static final Logger logger = LoggerFactory.getLogger(SignFilter.class);

    @Autowired
    private ExternalSignCallBackMethod externalSignCallBackMethod;
    
    @Autowired
    private RequestBodyUtil requestBodyUtil;
    

    @Override
    public int getOrder() {
        return 2;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String requestURI = request.getURI().getPath();
        
//        if (SignAccessList.isInSignAccessList(requestURI)) {
//        	ServerHttpRequestDecorator requestDecorator =
//        			requestBodyUtil.getServerHttpRequestDecorator(exchange,externalSignCallBackMethod);
//
//            ServerWebExchange swe = exchange.mutate().request(requestDecorator).build();
//            return FilterResultUtil.getSuccessResult(swe, chain);
//        }

        return FilterResultUtil.getSuccessResult(exchange, chain);

    }
}
