package com.zatgo.zup.gateway.filter;

import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CloudUserApi;
import com.zatgo.zup.common.utils.HttpServerUtil;
import com.zatgo.zup.common.utils.MD5;
import com.zatgo.zup.gateway.model.CloudUserSignCallBackMethod;
import com.zatgo.zup.gateway.model.FilterConstant;
import com.zatgo.zup.gateway.model.SystemInterfaceData;
import com.zatgo.zup.gateway.utils.CheckCredibleSourceUtil;
import com.zatgo.zup.gateway.utils.FilterResultUtil;
import com.zatgo.zup.gateway.utils.RequestBodyUtil;
import com.zatgo.zup.gateway.utils.WhiteListUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * Created by 46041 on 2018/9/5.
 */

@Component
public class AppAccessFilter implements GlobalFilter, Ordered {

    private static final Logger logger = LoggerFactory.getLogger(AppAccessFilter.class);

    @Autowired
    private CheckCredibleSourceUtil checkCredibleSourceUtil;
    @Autowired
    private FilterConstant filterConstant;
    
    @Autowired
    private SystemInterfaceData data;
    
    @Autowired
    private RequestBodyUtil requestBodyUtil;
    
    @Autowired
    private CloudUserSignCallBackMethod cloudUserSignCallBackMethod;
    
    @Autowired
    private RedisTemplate redisTemplate;

    public AppAccessFilter() {
        System.out.println("=============================创建AppAccessFilter==================================");
    }

    @Override
    public int getOrder() {
        return -1;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, GatewayFilterChain gatewayFilterChain){
        ServerHttpRequest request = serverWebExchange.getRequest();
        String requestURI = request.getURI().getPath();
        String remoteIP = WhiteListUtils.getRemoteIP(request);
        String originDomainName = request.getHeaders().getOrigin();
        ServerHttpRequest host = serverWebExchange.getRequest().mutate().header("client-ip", remoteIP).build();
        serverWebExchange = serverWebExchange.mutate().request(host).build();
        
        //云用户ID
        String cloudUserId = request.getQueryParams().getFirst(filterConstant.CLOUD_USER_ID_KEY);
        if (StringUtils.isEmpty(cloudUserId))
            cloudUserId = filterConstant.systemCloudUserId;

        //校验外网可访问的接口
        if(!WhiteListUtils.isCanAccessInf(request,data)) {
        	logger.error("内部接口访问拒绝：" + request.getURI().getPath() + " ip来源：" + WhiteListUtils.getRemoteIP(request));
        	return FilterResultUtil.getFailResult(serverWebExchange, BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
        }

        //校验可信来源
    	if(!checkCredibleSourceUtil.checkCredibleSource(remoteIP, originDomainName, cloudUserId)) {
    		if(WhiteListUtils.isAppSignInfList(request)) {
    			String appSign = HttpServerUtil.getHeader(request, "appSign");
    			String webSign = HttpServerUtil.getHeader(request, "webSign");
    			CloudUserApi cloudUserApi = checkCredibleSourceUtil.findCloudUserApi(cloudUserId);
    	        String publicKey = cloudUserApi == null ? null : cloudUserApi.getPublicKey();
    	        
    	        boolean isCheckPass = true; //是否验证通过
    	        if(appSign == null && webSign == null) {
    	        	logger.error("appSign == null && webSign == null");
    	        	isCheckPass = false;
    	        }
    	        if(appSign != null && !appSign.equals(publicKey)) {
    	        	logger.error("appSign != null && !appSign.equals(publicKey)");
    	        	isCheckPass = false;
    	        }
    	        if(webSign != null) {
    	        	String[] webSigns = webSign.split("_");
    	        	if(webSigns.length == 2) {
    	        		String compMd5 = MD5.GetMD5Code(webSigns[0] + publicKey);
    	        		if(compMd5.equalsIgnoreCase(webSigns[1])) {
    	        			Long currentDate = System.currentTimeMillis();
    	        			Long clientDate = Long.valueOf(webSigns[0]);
    	        			if(data.getWebSignExpireMinute() != 0
    	        					&& ((currentDate - clientDate) < (data.getWebSignExpireMinute() * 60 * 1000))) {
    	        				isCheckPass = true;
    	        			}else {
    	        				logger.error("WebSignExpireMinute=" + data.getWebSignExpireMinute()
    	        				+ " currentDate=" + currentDate
    	        				+ " clientDate=" + clientDate);
    	        				isCheckPass = false;
    	        			}

    	        		}else {
    	        			logger.error("compMd5.equalsIgnoreCase(webSigns[1])");
    	        			isCheckPass = false;
    	        		}
    	        	}else {
    	        		logger.error("webSigns.length != 2");
    	        		isCheckPass = false;
    	        	}

    	        }

    	        if(!isCheckPass) {
    	        	logger.error("remoteIP:" + remoteIP + " originDomainName=" + originDomainName + " not auth access interface");
    	        	return FilterResultUtil.getFailResult(serverWebExchange,BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
    	        }
    		}
    	}


        return FilterResultUtil.getSuccessResult(serverWebExchange, gatewayFilterChain);
    }


    public static void  main(String[] args) {
    	String compMd5 = MD5.GetMD5Code("1547628554315" +  "7c195566331c4cbd86ef38d5e3a845a1");
    	System.out.println(compMd5);
    }

}
