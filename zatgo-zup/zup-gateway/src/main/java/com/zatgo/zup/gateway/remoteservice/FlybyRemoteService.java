package com.zatgo.zup.gateway.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.zatgo.zup.common.model.ResponseData;

@FeignClient("flyby-openapi")
public interface FlybyRemoteService {

	@GetMapping("/openapi/tmc/acc/rsaPublicKey")
	ResponseData<String> getTmcRsaPublicKey(@RequestParam("tmcCode") String tmcCode);
}
