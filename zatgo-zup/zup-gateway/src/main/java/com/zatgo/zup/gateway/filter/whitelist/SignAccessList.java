package com.zatgo.zup.gateway.filter.whitelist;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 签名接口校验
 * @Title
 * @author lincm
 * @date 2018年4月3日 下午3:17:52
 */
public class SignAccessList {
	
	private static final Logger logger = LoggerFactory.getLogger(SignAccessList.class);
	//支付

	private static final String PAY_REFUND = "/pay/orderRefund";
	private static final String PREPAY_SIGN = "/pay/sign/createOrder";
	private static final String PREPAY_ORDER_INFO = "/pay/orders/prepay";
	private static final String PREPAYID_CHECK = "/pay/orders/check/prepay";
	private static final String SIGN_PAY = "/pay/sign/orderpay";
	private static final String ORDER_QUERY = "/pay/orders/sign/orderquery/tradeno";
	private static final String ORDER_QUERY_PAGE = "/pay/orders/sign/orderquery/page";
	private static final String MARKETPRICE = "/pay/market/sign/price";
	
	private static final String PAY_CALLBACK_WXPAY = "/pay/callback/wxpay";

	private static List<String> checkoutSignAccessList = null;
	private static List<String> wxSignAccessList = null;
	
	private static Lock checkoutLock = new ReentrantLock();
	private static Lock wxLock = new ReentrantLock();
	
	/**
	 * 获取收银台白名单列表
	 * @return
	 */
	public static List<String> getCheckoutSignAccessList(){
		if(checkoutSignAccessList == null) {
			checkoutLock.lock();
			try {
				if(checkoutSignAccessList == null) {
					checkoutSignAccessList = new ArrayList<>();
					checkoutSignAccessList.add(PAY_REFUND);
					checkoutSignAccessList.add(PREPAY_SIGN);
					checkoutSignAccessList.add(PREPAY_ORDER_INFO);
					checkoutSignAccessList.add(PREPAYID_CHECK);
					checkoutSignAccessList.add(SIGN_PAY);
					checkoutSignAccessList.add(ORDER_QUERY);
					checkoutSignAccessList.add(ORDER_QUERY_PAGE);
					checkoutSignAccessList.add(MARKETPRICE);
					
					checkoutSignAccessList.add(PAY_CALLBACK_WXPAY);
				}
			}catch(Exception e) {
				logger.error("",e);
			}finally {
				checkoutLock.unlock();
			}
		}
		return checkoutSignAccessList;
	}
	
	/**
	 * 微信回调白名单
	 * @return
	 */
	public static List<String> getWXSignAccessList(){
		if(wxSignAccessList == null) {
			wxLock.lock();
			try {
				if(wxSignAccessList == null) {
					wxSignAccessList = new ArrayList<>();
				}
			}catch(Exception e) {
				logger.error("",e);
			}finally {
				wxLock.unlock();
			}
		}
		return wxSignAccessList;
	}
	
	/**
	 * 判断是否为签名校验接口
	 * @param url
	 * @return
	 */
	public static boolean isInSignAccessList(String url) {
		List<String> checkoutSignAccessList = getCheckoutSignAccessList();
		for(String checkoutSignAccessUrl:checkoutSignAccessList) {
			if(url.startsWith(checkoutSignAccessUrl)) {
				return true;
			}
		}
		List<String> wxSignAccessList = getWXSignAccessList();
		for(String wxSignAccessUrl:wxSignAccessList) {
			if(url.startsWith(wxSignAccessUrl)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 判断是否为收银台业务接口
	 * @param url
	 * @return
	 */
	public static boolean isInCheckoutSignAccessList(String url) {
		List<String> checkoutSignAccessList = getCheckoutSignAccessList();
		for(String signAccessUrl:checkoutSignAccessList) {
			if(url.startsWith(signAccessUrl)) {
				return true;
			}
		}
		return false;
	}
}
