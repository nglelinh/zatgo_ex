package com.zatgo.zup.gateway.filter;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;

import com.aliyun.openservices.shade.com.alibaba.fastjson.JSON;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.RequestData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.utils.HttpServerUtil;
import com.zatgo.zup.gateway.model.FilterConstant;
import com.zatgo.zup.gateway.remoteservice.PresentRemoteService;
import com.zatgo.zup.gateway.remoteservice.UserRemoteService;
import com.zatgo.zup.gateway.utils.AuthTokenUtil;
import com.zatgo.zup.gateway.utils.FilterResultUtil;
import com.zatgo.zup.gateway.utils.WhiteListUtils;

import reactor.core.publisher.Mono;

@Component
public class UserAccessFilter implements GlobalFilter, Ordered {

	private static final Logger logger = LoggerFactory.getLogger(UserAccessFilter.class);

	@Autowired
	private UserRemoteService userRemoteService;
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private AuthTokenUtil authTokenUtil;
	@Autowired
	private PresentRemoteService presentRemoteService;
	@Autowired
	private FilterConstant filterConstant;

	public UserAccessFilter() {
		System.out.println("=============================创建UserAccessFilter==================================");
	}

	@Override
	public int getOrder() {
		return 1;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange serverWebExchange, GatewayFilterChain gatewayFilterChain) {
		setDefaultCloudUserId(serverWebExchange);
		ServerHttpRequest request = serverWebExchange.getRequest();
		
		String requestURI = request.getURI().getPath();
		String token = HttpServerUtil.getHeader(request, "token");
		String remoteIP = WhiteListUtils.getRemoteIP(request);
		String deviceNo = HttpServerUtil.getHeader(request, "deviceNo");
		
		//检查同源多用户操作
		if(!checkTokenIsAbnormal(remoteIP, deviceNo, token)) {
			return FilterResultUtil.getFailResult(serverWebExchange, BusinessExceptionCode.VISIT_TOO_FREQUENTLY);
		}

		//验token
		if ((WhiteListUtils.isInCloudUserAppAuthInfList(request) 
				|| WhiteListUtils.isInCloudUserAdminAuthInfList(request))
				&& !FilterResultUtil.isWebSocket(requestURI)) {
			try {
				
				//只有后台管理员才可以访问对应的接口
				if (WhiteListUtils.isInCloudUserAdminAuthInfList(request)) {
					AuthUserInfo user = authTokenUtil.getAuthUserInfo(token);
					if (!user.getIsAdmin()) {
						logger.error("接口访问拒绝：" + request.getURI().getPath() + " ip来源：" + WhiteListUtils.getRemoteIP(request) + " 用户ID:" + user.getUserId());
						return FilterResultUtil.getFailResult(serverWebExchange, BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
					}
				}
				
				String mnemonicWord = HttpServerUtil.getHeader(request, "mnemonicWord");
				//两种模式：用户名密码登入-助记词不需要登入
				if (mnemonicWord != null && !mnemonicWord.trim().equals("")) {
					RequestData<String> params = new RequestData<>();
					params.setData(mnemonicWord);
					ResponseData<UserData> userData = userRemoteService.getUserByMW(params);
					if (!userData.isSuccessful() || userData.getData() == null) {
						return FilterResultUtil.getFailResult(serverWebExchange);
					} else {
						//生成一个全局的token,为后面的子系统使用
						//presentCoin(token);
						return FilterResultUtil.getSuccessResult(serverWebExchange, gatewayFilterChain, userData, redisTemplate);
					}

				} else {
					//token验证
					if (!authTokenUtil.loginVerify(token)) {//通过
						logger.error("token验证不通过，token：" + token + " 访问接口："+ request.getURI().getPath() + " ip来源：" + remoteIP);
						return FilterResultUtil.getFailResult(serverWebExchange, BusinessExceptionCode.TOKEN_VERIFY_FAIL);
					}
					//校验通过，则刷新token过期时间
					authTokenUtil.refreshLoginExpire(token);
					//presentCoin(token);  这个会影响性能，赠送的时候，手动触发
				}
			} catch (Exception e){
				logger.error("", e);
				FilterResultUtil.getFailResult(serverWebExchange);
			}
		}

		return FilterResultUtil.getSuccessResult(serverWebExchange, gatewayFilterChain);
	}


	private void presentCoin(String userToken) {
		//登入触发赠送
		String loginUserName = null;
		try {
			AuthUserInfo authUserInfo = authTokenUtil.getAuthUserInfo(userToken);
			loginUserName = authUserInfo.getUserName();
			ResponseData<Object> returnObj = presentRemoteService.present(loginUserName, authUserInfo.getCloudUserId(), new RequestData<>());
			if(!returnObj.isSuccessful()) {
				logger.error(loginUserName + "-赠送失败：" + returnObj.getMessage());
			}
		}catch(Exception e) {
			logger.error(loginUserName + "-赠送失败:",e);
		}
	}

	private void setDefaultCloudUserId(ServerWebExchange serverWebExchange){
		ServerHttpRequest request = serverWebExchange.getRequest();
		HttpHeaders headers = request.getHeaders();
		List<String> accessControlRequestHeaders = headers.getAccessControlRequestHeaders();
		if (!CollectionUtils.isEmpty(accessControlRequestHeaders)){
			for (String str : accessControlRequestHeaders){
				if (filterConstant.CLOUD_USER_ID_KEY.equals(str)){
					return;
				}
			}
		}
		request = request.mutate().header(filterConstant.CLOUD_USER_ID_KEY, filterConstant.systemCloudUserId).build();
	}
	
	
	/**
     * 检测设备是否异常(token)
     * @param request
     * @return
     */
    public boolean checkTokenIsAbnormal(String clientIp,String deviceNo,String token) {
    	if(token == null) {
    		return true;
    	}
    	
    	AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
    	if(authUserInfo == null) {
    		return true;
    	}
    	
    	String key = null;
    	if(deviceNo != null) {
    		key = RedisKeyConstants.GATEWAY_LIMIT_DEVICE_NO_TOKEN_PRE + deviceNo;
    		
    		Object deviceNoList = redisTemplate.opsForHash().keys(key);
        	if(deviceNoList != null) {
        		Set deviceNoHash = (Set)deviceNoList;
            	if(!deviceNoHash.contains(authUserInfo.getUserName()+authUserInfo.getAdminName()) && deviceNoHash.size() >= 3) {
            		logger.error("deviceNoHash=" + JSON.toJSONString(deviceNoHash));
            		logger.error("token: clientIp=" + clientIp + " deviceNo=" + deviceNo + " token=" + token + " userNameAndAdminName=" + authUserInfo.getUserName()+authUserInfo.getAdminName());
            		return false;
            	}
        	}
        	
    		redisTemplate.opsForHash().put(key, authUserInfo.getUserName()+authUserInfo.getAdminName(), null);
    		redisTemplate.expire(key, 1*24*60*1000,TimeUnit.SECONDS);
        	
    	}

    	return true;
    }

}
