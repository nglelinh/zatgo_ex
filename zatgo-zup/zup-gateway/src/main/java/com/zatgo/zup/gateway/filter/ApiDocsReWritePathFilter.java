package com.zatgo.zup.gateway.filter;

import java.lang.reflect.Field;
import java.net.URI;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.NettyWriteResponseFilter;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.NettyDataBufferFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.zatgo.zup.gateway.filter.whitelist.PublicAccessList;
import com.zatgo.zup.gateway.utils.FilterResultUtil;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by 46041 on 2018/9/5.
 */
@Component
public class ApiDocsReWritePathFilter implements GlobalFilter, Ordered {
    private static final Logger logger = LoggerFactory.getLogger(UserAccessFilter.class);

    public ApiDocsReWritePathFilter() {
        System.out.println("=============================创建ApiDocsFilter==================================");
    }

    @Override
    public int getOrder() {
    	return 10102;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, GatewayFilterChain gatewayFilterChain) {
        ServerHttpRequest request = serverWebExchange.getRequest();
        String requestURI = request.getURI().getPath();

        //判断是否为访问swagger文档URL
        if(PublicAccessList.isInPublicAccessList(requestURI) && requestURI.contains("/v2/api-docs")){
            modifyUrl(serverWebExchange);
        }

        return FilterResultUtil.getSuccessResult(serverWebExchange, gatewayFilterChain);
    }

    /**
     * 删除swagger的前缀
     * @param serverWebExchange
     */
    private void modifyUrl(ServerWebExchange serverWebExchange){
        URI uri = (URI) serverWebExchange.getAttributes().get(ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR);
        Class<? extends URI> aClass = uri.getClass();
        try {
            String url = "http://";
            Field field = aClass.getDeclaredField("authority");
            field.setAccessible(true);
            String value = (String) field.get(uri);
            url += value;
            field = aClass.getDeclaredField("path");
            field.setAccessible(true);
            value = (String) field.get(uri);
            value = value.replaceFirst("^\\/[A-z0-9]*\\/", "\\/");
            url += value;
            serverWebExchange.getAttributes().put(ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR, new URI(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
