package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.DiggerNewsGonggao;

public interface DiggerNewsGonggaoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DiggerNewsGonggao record);

    int insertSelective(DiggerNewsGonggao record);

    DiggerNewsGonggao selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DiggerNewsGonggao record);

    int updateByPrimaryKeyWithBLOBs(DiggerNewsGonggao record);

    int updateByPrimaryKey(DiggerNewsGonggao record);
}