package com.zatgo.zup.suanli.mapper;

import com.zatgo.zup.suanli.entity.DiggerNewsKuaixun;

public interface DiggerNewsKuaixunMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DiggerNewsKuaixun record);

    int insertSelective(DiggerNewsKuaixun record);

    DiggerNewsKuaixun selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DiggerNewsKuaixun record);

    int updateByPrimaryKeyWithBLOBs(DiggerNewsKuaixun record);

    int updateByPrimaryKey(DiggerNewsKuaixun record);
}