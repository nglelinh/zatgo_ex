package com.zatgo.zup.search;

//import com.zatgo.zup.search.utils.HbaseUtil;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.search.model.TestModel;
import com.zatgo.zup.search.model.xiecheng.ScenicSpot;
import com.zatgo.zup.search.repository.ScenicSpotRepository;
import com.zatgo.zup.search.repository.TestRepository;
import com.zatgo.zup.search.service.HBaseService;
import com.zatgo.zup.search.utils.DateUtil;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.profile.ProfileShardResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZupTourismSearchApplication .class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class ZupTourismSearchApplicationTests {

//
//	@Autowired
//	private HbaseUtil hbaseUtil;
//

	@Autowired
	private HBaseService hbaseService;

	@Autowired
	private TestRepository testRepository;
	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;
	@Autowired
	private ScenicSpotRepository scenicSpotRepository;
	@Autowired
	private HBaseService hBaseService;

	/**
	 * 测试删除、创建表
	 */
	@Test
	public void testCreateTable() {
//
//		//删除表
//		hbaseService.deleteTable("ScenicSpot");
//
//		//创建表
////		hbaseService.createTableBySplitKeys("ScenicSpot", Arrays.asList("f","back"),hbaseService.getSplitKeys(null));
//		Set<String> set = new HashSet<>();
//		set.add("info");
//		hbaseService.creatTable("ScenicSpot", set);

		//插入三条数据
//		new String[]{"project_id","varName","coefs","pvalues","tvalues","create_time"},new String[]{"40866","mob_3","0.9416","0.0000","12.2293","null"}
//		JSONObject table = new JSONObject();
//		table.put("project_id", 40866);
//		table.put("varName", "mob_3");
//		table.put("coefs", 0.9416);
//		table.put("pvalues", 0.0000);
//		table.put("tvalues", 12.2293);
//		table.put("create_time", new Date());
//		hbaseService.putData("test_base","66804_000004","f", table);
////		hbaseService.putData("test_base","66804_000002","f",new String[]{"project_id","varName","coefs","pvalues","tvalues","create_time"},new String[]{"40866","idno_prov","0.9317","0.0000","9.8679","null"});
////		hbaseService.putData("test_base","66804_000003","f",new String[]{"project_id","varName","coefs","pvalues","tvalues","create_time"},new String[]{"40866","education","0.8984","0.0000","25.5649","null"});
//
//		//查询数据
//		//1. 根据rowKey查询
//		JSONObject test_base = hbaseService.getRowData("ScenicSpot", "info", JSONObject.class);
//		System.out.println("+++++++++++根据rowKey查询+++++++++++");
//		System.out.println(test_base.toJSONString());
//
//		//精确查询某个单元格的数据
//		String str1 = hbaseService.getColumnValue("test_base","66804_000002","f","varName");
//		System.out.println("+++++++++++精确查询某个单元格的数据+++++++++++");
//		System.out.println(str1);
//		System.out.println();
//
//		//2. 遍历查询
//		Map<String,Map<String,String>> result2 = hbaseService.getResultScanner("Image");
//		System.out.println("+++++++++++遍历查询+++++++++++");
//		result2.forEach((k,value) -> {
//			System.out.println(k + "---" + value);
//		});
	}

	@Test
	public void test(){
//		TestModel model = new TestModel();
//		model.setContent("这是测试内容");
//		model.setId("这个是测试用key");
//		model.setValue("这个是测试用的value");
//		testRepository.save(model);


//		Optional<TestModel> req = testRepository.findById("这个是测试用key");
//		TestModel model1 = req.get();
//		System.out.println(model1);
//		NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
//		builder.withQuery(QueryBuilders.multiMatchQuery("功夫熊猫", "name"));
//		NativeSearchQuery build = builder.build();
//		elasticsearchTemplate.query(build, response -> {
//			Map<String, ProfileShardResult> profileResults = response.getProfileResults();
//			System.out.println(response);
//			return null;
//		});

//		QueryBuilder orderQuery = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("bathNumber", "2019-04-18 16:00"));
////		Page<ScenicSpot> search = scenicSpotRepository.search(orderQuery, PageRequest.of(0, 10));
//		Iterable<ScenicSpot> search = scenicSpotRepository.search(orderQuery);
////		List<ScenicSpot> content = search.getContent();
//		System.out.println(search);


//		Page<TestModel> req1 = testRepository.findTestModelByContent("内");
//		List<TestModel> content = req1.getContent();
//		System.out.println(content);

//		String hour = DateUtil.getHour(new Date(), -3);
//		String time = hour + ":43";
//		Map<String, Map<String, String>> bathNumber = hBaseService.getResultScannerColumnValueFilter("ScenicSpot", time);
//		System.out.println(bathNumber);
	}


}
