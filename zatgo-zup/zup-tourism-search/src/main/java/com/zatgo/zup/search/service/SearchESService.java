package com.zatgo.zup.search.service;

import com.zatgo.zup.search.model.xiecheng.ScenicSpot;
import org.springframework.data.domain.Page;

/**
 * Created by 46041 on 2019/4/18.
 */

public interface SearchESService {

    Page<ScenicSpot> search(String keyword, Integer pageSize, Integer pageNo);
}
