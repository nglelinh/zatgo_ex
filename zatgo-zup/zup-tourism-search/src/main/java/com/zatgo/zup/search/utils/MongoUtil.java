package com.zatgo.zup.search.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2018/10/10.
 */

@Component
public class MongoUtil {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 保存
     * @param object
     */
    public void insert(Object object, String name){
        mongoTemplate.insert(object, name);
    }

    public void insert(Collection<? extends Object> List, String name){
        mongoTemplate.insert(List, name);
    }

    /**
     * 查询
     * @param map
     */
    public <T> List<T> getDataByListQuery(Map<String, Collection<String>> map, Class<T> claxx, String name){
        Query query = new Query();
        Criteria criteria = new Criteria();
        if (map != null && !map.isEmpty()){
            for (Map.Entry<String, Collection<String>> m : map.entrySet()){
                String key = m.getKey();
                Collection<String> value = m.getValue();
                criteria.and(key).in(value);
            }
        }
        query.addCriteria(criteria);
        return mongoTemplate.find(query, claxx, name);
    }

    public <T> List<T> getData(Map<String, Object> map, Class<T> claxx, String name){
        Query query = new Query();
        Criteria criteria = new Criteria();
        if (map != null && !map.isEmpty()){
            for (Map.Entry<String, Object> m : map.entrySet()){
                String key = m.getKey();
                Object val = m.getValue();
                criteria.and(key).is(val);
            }
        }
        query.addCriteria(criteria);
        return mongoTemplate.find(query, claxx, name);
    }

    public <T> List<T> getData(Query query, Class<T> claxx, String name){
        return mongoTemplate.find(query, claxx, name);
    }

    public <T> List<T> getDataNotInQuery(Map<String, Object> map, Class<T> claxx, String name){
        Query query = new Query();
        Criteria criteria = new Criteria();
        if (map != null && !map.isEmpty()){
            for (Map.Entry<String, Object> m : map.entrySet()){
                String key = m.getKey();
                Object val = m.getValue();
                criteria.and(key).ne(val);
            }
        }
        query.addCriteria(criteria);
        return mongoTemplate.find(query, claxx, name);
    }


    /**
     * 根据类型删除数据
     * @param map
     */
    public void delte(Map<String, Object> map, String name){
        if (map == null || map.isEmpty())
            return;
        Query query = new Query();
        Criteria criteria = new Criteria();
        for (Map.Entry<String, Object> m : map.entrySet()){
            String key = m.getKey();
            Object val = m.getValue();
            criteria.and(key).is(val);
        }
        query.addCriteria(criteria);
        mongoTemplate.remove(query, name);
    }

    public void delte(Query query, String name){
        mongoTemplate.remove(query, name);
    }

    /**
     * 更新数据
     * @param map
     */
    public void update(Map<String, Object> map, String name, Update update){
        if (map == null || map.isEmpty())
            return;
        Query query = new Query();
        Criteria criteria = new Criteria();
        for (Map.Entry<String, Object> m : map.entrySet()){
            String key = m.getKey();
            Object val = m.getValue();
            criteria.and(key).is(val);
        }
        query.addCriteria(criteria);
        mongoTemplate.updateMulti(query, update, name);
    }

    public void update(Query query, String name, Update update){
        mongoTemplate.updateMulti(query, update, name);
    }

    /**
     * 插入更新数据
     * @param map
     */
    public void updateInsert(Map<String, Object> map, String name, Update update){
        if (map == null || map.isEmpty())
            return;
        Query query = new Query();
        Criteria criteria = new Criteria();
        for (Map.Entry<String, Object> m : map.entrySet()){
            String key = m.getKey();
            Object val = m.getValue();
            criteria.and(key).is(val);
        }
        query.addCriteria(criteria);
        mongoTemplate.upsert(query, update, name);
    }

    public void updateInsert(Query query, String name, Update update){
        mongoTemplate.upsert(query, update, name);
    }

    /**
     * 批量执行更新
     * @param queryUpdateMap
     * @param collectionName
     */
    public void batchExecuteUpdate(Map<Query, Update> queryUpdateMap, String collectionName){
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, collectionName);
        for (Map.Entry<Query, Update> map : queryUpdateMap.entrySet()){
            Query key = map.getKey();
            Update value = map.getValue();
            bulkOperations.updateOne(key, value);
        }
        bulkOperations.execute();
    }

    /**
     * 批量执行insert
     * @param list
     * @param collectionName
     */
    public void batchExecuteInsert(List list, String collectionName){
        if (CollectionUtils.isEmpty(list))
            return;
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, collectionName);
        bulkOperations.insert(list);
        bulkOperations.execute();
    }

}
