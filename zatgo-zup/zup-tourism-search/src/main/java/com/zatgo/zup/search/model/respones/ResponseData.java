package com.zatgo.zup.search.model.respones;


import com.zatgo.zup.search.model.common.BusinessExceptionCode;

import java.io.Serializable;

public class ResponseData<T>  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String code;
	private String message;
	private T data;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
	public boolean isSuccessful() {
		if(code != null && (code.equals(BusinessExceptionCode.SUCCESS_CODE) || code.equals(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING))) {
			return true;
		}else {
			return false;
		}
	}
	
}
