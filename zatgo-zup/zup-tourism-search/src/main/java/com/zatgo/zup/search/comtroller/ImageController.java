package com.zatgo.zup.search.comtroller;

import com.zatgo.zup.search.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by 46041 on 2019/4/19.
 */

@RestController
@RequestMapping("/search/image")
public class ImageController {

    @Autowired
    private ImageService imageService;


    @RequestMapping(value = "/scenicSpot/{imgKey}")
    public void search(@PathVariable("imgKey") String imgKey, HttpServletResponse response){
        imageService.getImage(response, imgKey);
    }
}
