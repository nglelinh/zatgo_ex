package com.zatgo.zup.search.repository;

import com.zatgo.zup.search.model.xiecheng.ScenicSpot;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2019/4/18.
 */

@Component
public interface ScenicSpotRepository extends ElasticsearchRepository<ScenicSpot, String> {

}
