package com.zatgo.zup.search.task;

import com.zatgo.zup.search.service.SaveDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2019/4/19.
 */

@Component
public class ScenicSpotsTask {

    private static final Logger logger = LoggerFactory.getLogger(ScenicSpotsTask.class);


    @Autowired
    private SaveDataService saveDataService;

    @Scheduled(cron = "0 */1 * * * ?")
    public void updateSymbolRate(){
        logger.info("================================================ sync data start ==============================================");
        saveDataService.syncData();
        logger.info("================================================ sync data end ==============================================");
    }
}
