package com.zatgo.zup.search.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Created by 46041 on 2019/4/16.
 */

@Document(indexName = "tm",type = "tm")
public class TestModel {

    private static final long serialVersionUID = 4564729518133694581L;

    @Id
    private String id;


    private String value;

    private String content;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
