package com.zatgo.zup.search.service;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by 46041 on 2019/4/19.
 */
public interface ImageService {


    void getImage(HttpServletResponse response, String imgKey);
}
