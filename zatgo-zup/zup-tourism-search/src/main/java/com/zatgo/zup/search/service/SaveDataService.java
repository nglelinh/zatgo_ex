package com.zatgo.zup.search.service;

import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.common.model.SaveImageRequest;

/**
 * Created by 46041 on 2019/4/17.
 */
public interface SaveDataService {


    void save(SaveDataRequest request);

    void syncData();

    String saveImage(SaveImageRequest request);
}
