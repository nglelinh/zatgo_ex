package com.zatgo.zup.search.conf;

import com.zatgo.zup.search.service.HBaseService;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2019/4/12.
 */


@Component
public class HBaseConfig {


    @Value("${HBase.nodes}")
    private String nodes;

    @Value("${HBase.port}")
    private String port;

    @Value("${HBase.maxsize}")
    private String maxsize;

    @Bean
    public HBaseService getHbaseService() {
        org.apache.hadoop.conf.Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", nodes);
        conf.set("hbase.client.keyvalue.maxsize", maxsize);
        conf.set("hbase.zookeeper.property.clientPort", port);

        return new HBaseService(conf);
    }
}
