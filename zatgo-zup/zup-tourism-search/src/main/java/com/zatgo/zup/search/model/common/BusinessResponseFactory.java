package com.zatgo.zup.search.model.common;


import com.zatgo.zup.search.model.respones.ResponseData;

public class BusinessResponseFactory {
    
	/**
	 * system error
	 * @return
	 */
    public static ResponseData<Object> createSystemError() {
    	ResponseData<Object> response = new ResponseData<>();
    	response.setCode(BusinessExceptionCode.SYSTEM_ERROR);
    	response.setMessage(BusinessExceptionCode.getMessage(BusinessExceptionCode.SYSTEM_ERROR));
    	response.setData(null);
        return response;
    }

    /**
     * business error
     * @param errorCode
     * @param message
     * @return
     */
    public static ResponseData createBusinessError(String errorCode,String message){
    	ResponseData<Object> response = new ResponseData<>();
    	response.setCode(errorCode);
    	response.setMessage(message);
    	response.setData(null);
        return response;
    }
    
    /**
     * business error
     * @param errorCode
     * @return
     */
    public static ResponseData createBusinessError(String errorCode){
    	ResponseData<Object> response = new ResponseData<>();
    	response.setCode(errorCode);
    	response.setMessage(BusinessExceptionCode.getMessage(errorCode));
    	response.setData(null);
        return response;
    }
    
    
    /**
     * success
     * @param businessData
     * @return
     */
    public static <T> ResponseData<T> createSuccess(T businessData){
    	ResponseData<T> response = new ResponseData<>();
    	response.setCode(BusinessExceptionCode.SUCCESS_CODE);
    	response.setMessage(BusinessExceptionCode.getMessage(BusinessExceptionCode.SUCCESS_CODE));
    	response.setData(businessData);
        return response;
    }
}
