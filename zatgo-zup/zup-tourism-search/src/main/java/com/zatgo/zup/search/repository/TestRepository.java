package com.zatgo.zup.search.repository;

import com.zatgo.zup.search.model.TestModel;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2019/4/16.
 */

@Component
public interface TestRepository extends ElasticsearchRepository<TestModel, String> {

//    Page<TestModel> findTestModelByContent(String content);
}
