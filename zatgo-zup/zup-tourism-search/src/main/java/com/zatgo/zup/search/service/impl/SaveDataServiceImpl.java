package com.zatgo.zup.search.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.jcraft.jsch.jce.MD5;
import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.common.model.SaveImageRequest;
import com.zatgo.zup.search.model.common.HbaseConstant;
import com.zatgo.zup.search.model.common.MongoConstant;
import com.zatgo.zup.search.model.xiecheng.ScenicSpot;
import com.zatgo.zup.search.model.xiecheng.ScenicSpotMongo;
import com.zatgo.zup.search.repository.ScenicSpotRepository;
import com.zatgo.zup.search.service.HBaseService;
import com.zatgo.zup.search.service.SaveDataService;
import com.zatgo.zup.search.utils.DateUtil;
import com.zatgo.zup.search.utils.MD5Util;
import com.zatgo.zup.search.utils.MongoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by 46041 on 2019/4/17.
 */

@Service("saveDataService")
public class SaveDataServiceImpl implements SaveDataService {

    private static final Logger logger = LoggerFactory.getLogger(SaveDataServiceImpl.class);

    @Autowired
    private HBaseService hBaseService;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private ScenicSpotRepository scenicSpotRepository;
    @Autowired
    private MongoUtil mongoUtil;


    @Value("${data.image.scenicSpot.prefix}")
    private String urlPrefix;

    @Override
    public void save(SaveDataRequest request) {
        JSONObject data = request.getData();
        hBaseService.putData(request.getTableName(), request.getRowKey(), request.getFamilyName(), data);
        mongoUtil.insert(data, MongoConstant.SCENIC_SPOT);
    }

    @Override
    public String saveImage(SaveImageRequest request) {
        String url = null;
        String image = request.getData();
        if (!StringUtils.isEmpty(image)){
            String rowKey = request.getRowKey();
            if (StringUtils.isEmpty(rowKey)){
                rowKey = MD5Util.MD5(image);
            }
            JSONObject data = new JSONObject();
            data.put(request.getImageValueKey(), image);
            hBaseService.putData(request.getTableName(), rowKey, request.getFamilyName(), data);
            url = urlPrefix + rowKey;
        }
        return url;
    }

    @Override
    public void syncData() {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.SEARCH_KEY).lt(getNowTimeStamp());
        query.addCriteria(criteria);
        List<ScenicSpotMongo> data = mongoUtil.getData(query, ScenicSpotMongo.class, MongoConstant.SCENIC_SPOT);
        List<ScenicSpot> scenicSpots = new ArrayList<>(data.size());
        if (!CollectionUtils.isEmpty(data)){
            Set<Long> set = new HashSet<>();
            for (ScenicSpotMongo ssm : data){
                ScenicSpot ss = new ScenicSpot();
                BeanUtils.copyProperties(ssm, ss);
                setSearchKey(ss);
                scenicSpots.add(ss);
                set.add(ss.getTimeStamp());
            }
            scenicSpotRepository.saveAll(scenicSpots);
            Query deleteQuery = new Query();
            Criteria deleteCriteria = new Criteria();
            deleteCriteria.and(MongoConstant.SEARCH_KEY).in(set);
            deleteQuery.addCriteria(deleteCriteria);
            mongoUtil.delte(deleteQuery, MongoConstant.SCENIC_SPOT);
        }
    }


    private void setSearchKey(ScenicSpot ss){
        StringBuffer sb = new StringBuffer();
        String province = ss.getProvince();
        if (StringUtils.isEmpty(province)){
            sb.append(ss.getCountry());
            sb.append("_");
            sb.append(province);
            sb.append("_");
            sb.append(ss.getCity());
            sb.append("_");
            sb.append(ss.getArea());
            sb.append("_");
        }
        sb.append(ss.getName());
        sb.append("_");
        sb.append(ss.getAddr());
        sb.append("_");
        sb.append(ss.getIntroduction());
        sb.append("_");
        sb.append(ss.getRemind());
        ss.setSearchKey(sb.toString());
    }

    private static Long getNowTimeStamp(){
        return Long.valueOf(System.currentTimeMillis() / 1000 + "000000000");
    }
}
