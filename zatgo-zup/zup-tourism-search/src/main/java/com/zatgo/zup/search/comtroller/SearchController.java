package com.zatgo.zup.search.comtroller;

import com.zatgo.zup.search.model.common.BusinessResponseFactory;
import com.zatgo.zup.search.model.respones.ResponseData;
import com.zatgo.zup.search.model.xiecheng.ScenicSpot;
import com.zatgo.zup.search.service.SearchESService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 46041 on 2019/4/18.
 */

@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchESService searchService;

    @GetMapping("/ScenicSpot/{keyword}")
    public ResponseData search(@PathVariable("keyword") String keyword,
                               @RequestParam("pageSize") Integer pageSize,
                               @RequestParam("pageNo") Integer pageNo){
        Page<ScenicSpot> search = searchService.search(keyword, pageSize, pageNo);
        List<ScenicSpot> list = search.getContent();
        for (ScenicSpot ss : list){
            ss.setSearchKey(null);
        }
        return BusinessResponseFactory.createSuccess(search);
    }
}
