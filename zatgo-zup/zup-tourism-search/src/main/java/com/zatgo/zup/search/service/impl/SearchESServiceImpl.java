package com.zatgo.zup.search.service.impl;

import com.zatgo.zup.search.model.xiecheng.ScenicSpot;
import com.zatgo.zup.search.repository.ScenicSpotRepository;
import com.zatgo.zup.search.service.SearchESService;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by 46041 on 2019/4/18.
 */

@Service("searchESService")
public class SearchESServiceImpl implements SearchESService {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private ScenicSpotRepository scenicSpotRepository;

    @Override
    public Page<ScenicSpot> search(String keyword, Integer pageSize, Integer pageNo) {
        QueryBuilder orderQuery = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("searchKey", keyword));
        return scenicSpotRepository.search(orderQuery, PageRequest.of(pageNo, pageSize));
    }
}
