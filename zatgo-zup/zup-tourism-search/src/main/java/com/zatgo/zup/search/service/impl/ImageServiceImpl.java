package com.zatgo.zup.search.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.search.model.common.HbaseConstant;
import com.zatgo.zup.search.service.HBaseService;
import com.zatgo.zup.search.service.ImageService;
import com.zatgo.zup.search.utils.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by 46041 on 2019/4/19.
 */

@Service("imageService")
public class ImageServiceImpl implements ImageService {

    private static final Logger logger = LoggerFactory.getLogger(ImageServiceImpl.class);

    @Autowired
    private HBaseService hBaseService;

    @Override
    public void getImage(HttpServletResponse response, String imgKey) {
        JSONObject img = hBaseService.getRowData(HbaseConstant.ScenicSpotImageTableName, imgKey, JSONObject.class);
        if (img != null){
            String imgString = img.getString(HbaseConstant.ScenicSpotImageValueKey);
            if (!StringUtils.isEmpty(imgString)){
                try{
                    response.setContentType("image/jpeg");
                    byte[] decode = Base64.decode(imgString);
                    ServletOutputStream out = response.getOutputStream();
                    out.write(decode);
                    out.flush();
                } catch (Exception e){
                    logger.error("", e);
                }
            }
        }
    }
}
