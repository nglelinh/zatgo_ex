#!/bin/bash
sed -i "s/<SKY_ES>/$SKY_ES/g" /src/docker/agent/config/agent.config
server_name=`cat /tmp/server_name.txt`
java -javaagent:/src/docker/agent/skywalking-agent.jar -jar $JVM_OPT /data/webapps/${server_name}/${server_name}.jar --eureka.client.serviceUrl.defaultZone=$REGISTER_ADDR $SP_ARGS
