#!/bin/bash
echo "generate Dockerfile"

PROFILE=$1

array_profile=("dev", "prod")
if [[ ! " ${array_profile[@]} " =~ "$PROFILE" ]]; then
    # whatever you want to do when arr doesn't contain value
    echo "PROFILE is allowed only [${array_profile[@]}]\n\
    dev = It does not setting APMs\n\
    prod = It does setting APMs"
    exit
fi

array=("zup-wechat" "zup-trade-api" "zup-trade-admin" "zup-suanli" "zup-register" "zup-merchant" "zup-gateway" "zup-config" "zup-activity")

for var in ${array[@]}
do
	echo "generate $var"
	sed "s/\$server_name/$var/g;s/\$profile/prod/g" Dockerfile >  "Dockerfile_$var"_bk

	line=$(grep -n 'Insert setting APM if environment is production.' "Dockerfile_$var"_bk | cut -d: -f1)

	if [ "$PROFILE" == "prod" ]; then
	    sed '/Insert setting APM if environment is production./ r Dockerfile_APM' "Dockerfile_$var"_bk > "Dockerfile_$var"
	else
	    cp  "Dockerfile_$var"_bk "Dockerfile_$var"
	fi
	rm "Dockerfile_$var"_bk
done
echo "-------------------------------------------------------------------------------"
for docker in ${array[@]}
do
echo "docker build . -t $docker             -f ./docker/Dockerfile_${docker}"
done
echo "-------------------------------------------------------------------------------"