package com.ykb.mall.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ThirdPayRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ThirdPayRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRecordIdIsNull() {
            addCriterion("record_id is null");
            return (Criteria) this;
        }

        public Criteria andRecordIdIsNotNull() {
            addCriterion("record_id is not null");
            return (Criteria) this;
        }

        public Criteria andRecordIdEqualTo(Long value) {
            addCriterion("record_id =", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotEqualTo(Long value) {
            addCriterion("record_id <>", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThan(Long value) {
            addCriterion("record_id >", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThanOrEqualTo(Long value) {
            addCriterion("record_id >=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThan(Long value) {
            addCriterion("record_id <", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThanOrEqualTo(Long value) {
            addCriterion("record_id <=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdIn(List<Long> values) {
            addCriterion("record_id in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotIn(List<Long> values) {
            addCriterion("record_id not in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdBetween(Long value1, Long value2) {
            addCriterion("record_id between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotBetween(Long value1, Long value2) {
            addCriterion("record_id not between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNull() {
            addCriterion("pay_type is null");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNotNull() {
            addCriterion("pay_type is not null");
            return (Criteria) this;
        }

        public Criteria andPayTypeEqualTo(Byte value) {
            addCriterion("pay_type =", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotEqualTo(Byte value) {
            addCriterion("pay_type <>", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThan(Byte value) {
            addCriterion("pay_type >", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("pay_type >=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThan(Byte value) {
            addCriterion("pay_type <", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThanOrEqualTo(Byte value) {
            addCriterion("pay_type <=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeIn(List<Byte> values) {
            addCriterion("pay_type in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotIn(List<Byte> values) {
            addCriterion("pay_type not in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeBetween(Byte value1, Byte value2) {
            addCriterion("pay_type between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("pay_type not between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowIsNull() {
            addCriterion("money_flow is null");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowIsNotNull() {
            addCriterion("money_flow is not null");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowEqualTo(Byte value) {
            addCriterion("money_flow =", value, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowNotEqualTo(Byte value) {
            addCriterion("money_flow <>", value, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowGreaterThan(Byte value) {
            addCriterion("money_flow >", value, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowGreaterThanOrEqualTo(Byte value) {
            addCriterion("money_flow >=", value, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowLessThan(Byte value) {
            addCriterion("money_flow <", value, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowLessThanOrEqualTo(Byte value) {
            addCriterion("money_flow <=", value, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowIn(List<Byte> values) {
            addCriterion("money_flow in", values, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowNotIn(List<Byte> values) {
            addCriterion("money_flow not in", values, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowBetween(Byte value1, Byte value2) {
            addCriterion("money_flow between", value1, value2, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyFlowNotBetween(Byte value1, Byte value2) {
            addCriterion("money_flow not between", value1, value2, "moneyFlow");
            return (Criteria) this;
        }

        public Criteria andMoneyIsNull() {
            addCriterion("money is null");
            return (Criteria) this;
        }

        public Criteria andMoneyIsNotNull() {
            addCriterion("money is not null");
            return (Criteria) this;
        }

        public Criteria andMoneyEqualTo(BigDecimal value) {
            addCriterion("money =", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotEqualTo(BigDecimal value) {
            addCriterion("money <>", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyGreaterThan(BigDecimal value) {
            addCriterion("money >", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("money >=", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyLessThan(BigDecimal value) {
            addCriterion("money <", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("money <=", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyIn(List<BigDecimal> values) {
            addCriterion("money in", values, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotIn(List<BigDecimal> values) {
            addCriterion("money not in", values, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("money between", value1, value2, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("money not between", value1, value2, "money");
            return (Criteria) this;
        }

        public Criteria andRecordTimeIsNull() {
            addCriterion("record_time is null");
            return (Criteria) this;
        }

        public Criteria andRecordTimeIsNotNull() {
            addCriterion("record_time is not null");
            return (Criteria) this;
        }

        public Criteria andRecordTimeEqualTo(Date value) {
            addCriterion("record_time =", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeNotEqualTo(Date value) {
            addCriterion("record_time <>", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeGreaterThan(Date value) {
            addCriterion("record_time >", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("record_time >=", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeLessThan(Date value) {
            addCriterion("record_time <", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeLessThanOrEqualTo(Date value) {
            addCriterion("record_time <=", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeIn(List<Date> values) {
            addCriterion("record_time in", values, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeNotIn(List<Date> values) {
            addCriterion("record_time not in", values, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeBetween(Date value1, Date value2) {
            addCriterion("record_time between", value1, value2, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeNotBetween(Date value1, Date value2) {
            addCriterion("record_time not between", value1, value2, "recordTime");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberIsNull() {
            addCriterion("third_business_number is null");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberIsNotNull() {
            addCriterion("third_business_number is not null");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberEqualTo(String value) {
            addCriterion("third_business_number =", value, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberNotEqualTo(String value) {
            addCriterion("third_business_number <>", value, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberGreaterThan(String value) {
            addCriterion("third_business_number >", value, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberGreaterThanOrEqualTo(String value) {
            addCriterion("third_business_number >=", value, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberLessThan(String value) {
            addCriterion("third_business_number <", value, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberLessThanOrEqualTo(String value) {
            addCriterion("third_business_number <=", value, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberLike(String value) {
            addCriterion("third_business_number like", value, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberNotLike(String value) {
            addCriterion("third_business_number not like", value, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberIn(List<String> values) {
            addCriterion("third_business_number in", values, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberNotIn(List<String> values) {
            addCriterion("third_business_number not in", values, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberBetween(String value1, String value2) {
            addCriterion("third_business_number between", value1, value2, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andThirdBusinessNumberNotBetween(String value1, String value2) {
            addCriterion("third_business_number not between", value1, value2, "thirdBusinessNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberIsNull() {
            addCriterion("refund_number is null");
            return (Criteria) this;
        }

        public Criteria andRefundNumberIsNotNull() {
            addCriterion("refund_number is not null");
            return (Criteria) this;
        }

        public Criteria andRefundNumberEqualTo(String value) {
            addCriterion("refund_number =", value, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberNotEqualTo(String value) {
            addCriterion("refund_number <>", value, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberGreaterThan(String value) {
            addCriterion("refund_number >", value, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberGreaterThanOrEqualTo(String value) {
            addCriterion("refund_number >=", value, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberLessThan(String value) {
            addCriterion("refund_number <", value, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberLessThanOrEqualTo(String value) {
            addCriterion("refund_number <=", value, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberLike(String value) {
            addCriterion("refund_number like", value, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberNotLike(String value) {
            addCriterion("refund_number not like", value, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberIn(List<String> values) {
            addCriterion("refund_number in", values, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberNotIn(List<String> values) {
            addCriterion("refund_number not in", values, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberBetween(String value1, String value2) {
            addCriterion("refund_number between", value1, value2, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andRefundNumberNotBetween(String value1, String value2) {
            addCriterion("refund_number not between", value1, value2, "refundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberIsNull() {
            addCriterion("third_refund_number is null");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberIsNotNull() {
            addCriterion("third_refund_number is not null");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberEqualTo(String value) {
            addCriterion("third_refund_number =", value, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberNotEqualTo(String value) {
            addCriterion("third_refund_number <>", value, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberGreaterThan(String value) {
            addCriterion("third_refund_number >", value, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberGreaterThanOrEqualTo(String value) {
            addCriterion("third_refund_number >=", value, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberLessThan(String value) {
            addCriterion("third_refund_number <", value, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberLessThanOrEqualTo(String value) {
            addCriterion("third_refund_number <=", value, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberLike(String value) {
            addCriterion("third_refund_number like", value, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberNotLike(String value) {
            addCriterion("third_refund_number not like", value, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberIn(List<String> values) {
            addCriterion("third_refund_number in", values, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberNotIn(List<String> values) {
            addCriterion("third_refund_number not in", values, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberBetween(String value1, String value2) {
            addCriterion("third_refund_number between", value1, value2, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andThirdRefundNumberNotBetween(String value1, String value2) {
            addCriterion("third_refund_number not between", value1, value2, "thirdRefundNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIsNull() {
            addCriterion("order_number is null");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIsNotNull() {
            addCriterion("order_number is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNumberEqualTo(String value) {
            addCriterion("order_number =", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotEqualTo(String value) {
            addCriterion("order_number <>", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberGreaterThan(String value) {
            addCriterion("order_number >", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberGreaterThanOrEqualTo(String value) {
            addCriterion("order_number >=", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLessThan(String value) {
            addCriterion("order_number <", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLessThanOrEqualTo(String value) {
            addCriterion("order_number <=", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLike(String value) {
            addCriterion("order_number like", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotLike(String value) {
            addCriterion("order_number not like", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIn(List<String> values) {
            addCriterion("order_number in", values, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotIn(List<String> values) {
            addCriterion("order_number not in", values, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberBetween(String value1, String value2) {
            addCriterion("order_number between", value1, value2, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotBetween(String value1, String value2) {
            addCriterion("order_number not between", value1, value2, "orderNumber");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}