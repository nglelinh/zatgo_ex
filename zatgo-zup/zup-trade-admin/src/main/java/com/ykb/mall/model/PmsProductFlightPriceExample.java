package com.ykb.mall.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PmsProductFlightPriceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PmsProductFlightPriceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andFlightIdIsNull() {
            addCriterion("flight_id is null");
            return (Criteria) this;
        }

        public Criteria andFlightIdIsNotNull() {
            addCriterion("flight_id is not null");
            return (Criteria) this;
        }

        public Criteria andFlightIdEqualTo(Long value) {
            addCriterion("flight_id =", value, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdNotEqualTo(Long value) {
            addCriterion("flight_id <>", value, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdGreaterThan(Long value) {
            addCriterion("flight_id >", value, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdGreaterThanOrEqualTo(Long value) {
            addCriterion("flight_id >=", value, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdLessThan(Long value) {
            addCriterion("flight_id <", value, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdLessThanOrEqualTo(Long value) {
            addCriterion("flight_id <=", value, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdIn(List<Long> values) {
            addCriterion("flight_id in", values, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdNotIn(List<Long> values) {
            addCriterion("flight_id not in", values, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdBetween(Long value1, Long value2) {
            addCriterion("flight_id between", value1, value2, "flightId");
            return (Criteria) this;
        }

        public Criteria andFlightIdNotBetween(Long value1, Long value2) {
            addCriterion("flight_id not between", value1, value2, "flightId");
            return (Criteria) this;
        }

        public Criteria andPriceNameIsNull() {
            addCriterion("price_name is null");
            return (Criteria) this;
        }

        public Criteria andPriceNameIsNotNull() {
            addCriterion("price_name is not null");
            return (Criteria) this;
        }

        public Criteria andPriceNameEqualTo(String value) {
            addCriterion("price_name =", value, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameNotEqualTo(String value) {
            addCriterion("price_name <>", value, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameGreaterThan(String value) {
            addCriterion("price_name >", value, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameGreaterThanOrEqualTo(String value) {
            addCriterion("price_name >=", value, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameLessThan(String value) {
            addCriterion("price_name <", value, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameLessThanOrEqualTo(String value) {
            addCriterion("price_name <=", value, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameLike(String value) {
            addCriterion("price_name like", value, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameNotLike(String value) {
            addCriterion("price_name not like", value, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameIn(List<String> values) {
            addCriterion("price_name in", values, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameNotIn(List<String> values) {
            addCriterion("price_name not in", values, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameBetween(String value1, String value2) {
            addCriterion("price_name between", value1, value2, "priceName");
            return (Criteria) this;
        }

        public Criteria andPriceNameNotBetween(String value1, String value2) {
            addCriterion("price_name not between", value1, value2, "priceName");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceIsNull() {
            addCriterion("supply_price is null");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceIsNotNull() {
            addCriterion("supply_price is not null");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceEqualTo(BigDecimal value) {
            addCriterion("supply_price =", value, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceNotEqualTo(BigDecimal value) {
            addCriterion("supply_price <>", value, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceGreaterThan(BigDecimal value) {
            addCriterion("supply_price >", value, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("supply_price >=", value, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceLessThan(BigDecimal value) {
            addCriterion("supply_price <", value, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("supply_price <=", value, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceIn(List<BigDecimal> values) {
            addCriterion("supply_price in", values, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceNotIn(List<BigDecimal> values) {
            addCriterion("supply_price not in", values, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("supply_price between", value1, value2, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andSupplyPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("supply_price not between", value1, value2, "supplyPrice");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleIsNull() {
            addCriterion("max_people is null");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleIsNotNull() {
            addCriterion("max_people is not null");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleEqualTo(Integer value) {
            addCriterion("max_people =", value, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleNotEqualTo(Integer value) {
            addCriterion("max_people <>", value, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleGreaterThan(Integer value) {
            addCriterion("max_people >", value, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleGreaterThanOrEqualTo(Integer value) {
            addCriterion("max_people >=", value, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleLessThan(Integer value) {
            addCriterion("max_people <", value, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleLessThanOrEqualTo(Integer value) {
            addCriterion("max_people <=", value, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleIn(List<Integer> values) {
            addCriterion("max_people in", values, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleNotIn(List<Integer> values) {
            addCriterion("max_people not in", values, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleBetween(Integer value1, Integer value2) {
            addCriterion("max_people between", value1, value2, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andMaxPeopleNotBetween(Integer value1, Integer value2) {
            addCriterion("max_people not between", value1, value2, "maxPeople");
            return (Criteria) this;
        }

        public Criteria andFirstShowIsNull() {
            addCriterion("first_show is null");
            return (Criteria) this;
        }

        public Criteria andFirstShowIsNotNull() {
            addCriterion("first_show is not null");
            return (Criteria) this;
        }

        public Criteria andFirstShowEqualTo(Byte value) {
            addCriterion("first_show =", value, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowNotEqualTo(Byte value) {
            addCriterion("first_show <>", value, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowGreaterThan(Byte value) {
            addCriterion("first_show >", value, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowGreaterThanOrEqualTo(Byte value) {
            addCriterion("first_show >=", value, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowLessThan(Byte value) {
            addCriterion("first_show <", value, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowLessThanOrEqualTo(Byte value) {
            addCriterion("first_show <=", value, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowIn(List<Byte> values) {
            addCriterion("first_show in", values, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowNotIn(List<Byte> values) {
            addCriterion("first_show not in", values, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowBetween(Byte value1, Byte value2) {
            addCriterion("first_show between", value1, value2, "firstShow");
            return (Criteria) this;
        }

        public Criteria andFirstShowNotBetween(Byte value1, Byte value2) {
            addCriterion("first_show not between", value1, value2, "firstShow");
            return (Criteria) this;
        }

        public Criteria andShowStatusIsNull() {
            addCriterion("show_status is null");
            return (Criteria) this;
        }

        public Criteria andShowStatusIsNotNull() {
            addCriterion("show_status is not null");
            return (Criteria) this;
        }

        public Criteria andShowStatusEqualTo(Byte value) {
            addCriterion("show_status =", value, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusNotEqualTo(Byte value) {
            addCriterion("show_status <>", value, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusGreaterThan(Byte value) {
            addCriterion("show_status >", value, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("show_status >=", value, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusLessThan(Byte value) {
            addCriterion("show_status <", value, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusLessThanOrEqualTo(Byte value) {
            addCriterion("show_status <=", value, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusIn(List<Byte> values) {
            addCriterion("show_status in", values, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusNotIn(List<Byte> values) {
            addCriterion("show_status not in", values, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusBetween(Byte value1, Byte value2) {
            addCriterion("show_status between", value1, value2, "showStatus");
            return (Criteria) this;
        }

        public Criteria andShowStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("show_status not between", value1, value2, "showStatus");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameIsNull() {
            addCriterion("shipping_space_name is null");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameIsNotNull() {
            addCriterion("shipping_space_name is not null");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameEqualTo(String value) {
            addCriterion("shipping_space_name =", value, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameNotEqualTo(String value) {
            addCriterion("shipping_space_name <>", value, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameGreaterThan(String value) {
            addCriterion("shipping_space_name >", value, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameGreaterThanOrEqualTo(String value) {
            addCriterion("shipping_space_name >=", value, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameLessThan(String value) {
            addCriterion("shipping_space_name <", value, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameLessThanOrEqualTo(String value) {
            addCriterion("shipping_space_name <=", value, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameLike(String value) {
            addCriterion("shipping_space_name like", value, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameNotLike(String value) {
            addCriterion("shipping_space_name not like", value, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameIn(List<String> values) {
            addCriterion("shipping_space_name in", values, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameNotIn(List<String> values) {
            addCriterion("shipping_space_name not in", values, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameBetween(String value1, String value2) {
            addCriterion("shipping_space_name between", value1, value2, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andShippingSpaceNameNotBetween(String value1, String value2) {
            addCriterion("shipping_space_name not between", value1, value2, "shippingSpaceName");
            return (Criteria) this;
        }

        public Criteria andFlightDateIsNull() {
            addCriterion("flight_date is null");
            return (Criteria) this;
        }

        public Criteria andFlightDateIsNotNull() {
            addCriterion("flight_date is not null");
            return (Criteria) this;
        }

        public Criteria andFlightDateEqualTo(Date value) {
            addCriterion("flight_date =", value, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateNotEqualTo(Date value) {
            addCriterion("flight_date <>", value, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateGreaterThan(Date value) {
            addCriterion("flight_date >", value, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateGreaterThanOrEqualTo(Date value) {
            addCriterion("flight_date >=", value, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateLessThan(Date value) {
            addCriterion("flight_date <", value, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateLessThanOrEqualTo(Date value) {
            addCriterion("flight_date <=", value, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateIn(List<Date> values) {
            addCriterion("flight_date in", values, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateNotIn(List<Date> values) {
            addCriterion("flight_date not in", values, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateBetween(Date value1, Date value2) {
            addCriterion("flight_date between", value1, value2, "flightDate");
            return (Criteria) this;
        }

        public Criteria andFlightDateNotBetween(Date value1, Date value2) {
            addCriterion("flight_date not between", value1, value2, "flightDate");
            return (Criteria) this;
        }

        public Criteria andStockIsNull() {
            addCriterion("stock is null");
            return (Criteria) this;
        }

        public Criteria andStockIsNotNull() {
            addCriterion("stock is not null");
            return (Criteria) this;
        }

        public Criteria andStockEqualTo(Integer value) {
            addCriterion("stock =", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotEqualTo(Integer value) {
            addCriterion("stock <>", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThan(Integer value) {
            addCriterion("stock >", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThanOrEqualTo(Integer value) {
            addCriterion("stock >=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThan(Integer value) {
            addCriterion("stock <", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThanOrEqualTo(Integer value) {
            addCriterion("stock <=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockIn(List<Integer> values) {
            addCriterion("stock in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotIn(List<Integer> values) {
            addCriterion("stock not in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockBetween(Integer value1, Integer value2) {
            addCriterion("stock between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotBetween(Integer value1, Integer value2) {
            addCriterion("stock not between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceIsNull() {
            addCriterion("purchase_price is null");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceIsNotNull() {
            addCriterion("purchase_price is not null");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceEqualTo(BigDecimal value) {
            addCriterion("purchase_price =", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceNotEqualTo(BigDecimal value) {
            addCriterion("purchase_price <>", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceGreaterThan(BigDecimal value) {
            addCriterion("purchase_price >", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("purchase_price >=", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceLessThan(BigDecimal value) {
            addCriterion("purchase_price <", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("purchase_price <=", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceIn(List<BigDecimal> values) {
            addCriterion("purchase_price in", values, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceNotIn(List<BigDecimal> values) {
            addCriterion("purchase_price not in", values, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchase_price between", value1, value2, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchase_price not between", value1, value2, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceIsNull() {
            addCriterion("child_price is null");
            return (Criteria) this;
        }

        public Criteria andChildPriceIsNotNull() {
            addCriterion("child_price is not null");
            return (Criteria) this;
        }

        public Criteria andChildPriceEqualTo(BigDecimal value) {
            addCriterion("child_price =", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceNotEqualTo(BigDecimal value) {
            addCriterion("child_price <>", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceGreaterThan(BigDecimal value) {
            addCriterion("child_price >", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("child_price >=", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceLessThan(BigDecimal value) {
            addCriterion("child_price <", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("child_price <=", value, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceIn(List<BigDecimal> values) {
            addCriterion("child_price in", values, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceNotIn(List<BigDecimal> values) {
            addCriterion("child_price not in", values, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("child_price between", value1, value2, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("child_price not between", value1, value2, "childPrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceIsNull() {
            addCriterion("child_purchase_price is null");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceIsNotNull() {
            addCriterion("child_purchase_price is not null");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceEqualTo(BigDecimal value) {
            addCriterion("child_purchase_price =", value, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceNotEqualTo(BigDecimal value) {
            addCriterion("child_purchase_price <>", value, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceGreaterThan(BigDecimal value) {
            addCriterion("child_purchase_price >", value, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("child_purchase_price >=", value, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceLessThan(BigDecimal value) {
            addCriterion("child_purchase_price <", value, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("child_purchase_price <=", value, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceIn(List<BigDecimal> values) {
            addCriterion("child_purchase_price in", values, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceNotIn(List<BigDecimal> values) {
            addCriterion("child_purchase_price not in", values, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("child_purchase_price between", value1, value2, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andChildPurchasePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("child_purchase_price not between", value1, value2, "childPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataIsNull() {
            addCriterion("return_flight_data is null");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataIsNotNull() {
            addCriterion("return_flight_data is not null");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataEqualTo(Date value) {
            addCriterion("return_flight_data =", value, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataNotEqualTo(Date value) {
            addCriterion("return_flight_data <>", value, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataGreaterThan(Date value) {
            addCriterion("return_flight_data >", value, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataGreaterThanOrEqualTo(Date value) {
            addCriterion("return_flight_data >=", value, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataLessThan(Date value) {
            addCriterion("return_flight_data <", value, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataLessThanOrEqualTo(Date value) {
            addCriterion("return_flight_data <=", value, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataIn(List<Date> values) {
            addCriterion("return_flight_data in", values, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataNotIn(List<Date> values) {
            addCriterion("return_flight_data not in", values, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataBetween(Date value1, Date value2) {
            addCriterion("return_flight_data between", value1, value2, "returnFlightData");
            return (Criteria) this;
        }

        public Criteria andReturnFlightDataNotBetween(Date value1, Date value2) {
            addCriterion("return_flight_data not between", value1, value2, "returnFlightData");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}