package com.ykb.mall.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PmsProductFlightExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PmsProductFlightExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCTime(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value.getTime()), property);
        }

        protected void addCriterionForJDBCTime(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Time> timeList = new ArrayList<java.sql.Time>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                timeList.add(new java.sql.Time(iter.next().getTime()));
            }
            addCriterion(condition, timeList, property);
        }

        protected void addCriterionForJDBCTime(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value1.getTime()), new java.sql.Time(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(Long value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(Long value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(Long value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(Long value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(Long value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(Long value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<Long> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<Long> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(Long value1, Long value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(Long value1, Long value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberIsNull() {
            addCriterion("trip_flight_number is null");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberIsNotNull() {
            addCriterion("trip_flight_number is not null");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberEqualTo(String value) {
            addCriterion("trip_flight_number =", value, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberNotEqualTo(String value) {
            addCriterion("trip_flight_number <>", value, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberGreaterThan(String value) {
            addCriterion("trip_flight_number >", value, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberGreaterThanOrEqualTo(String value) {
            addCriterion("trip_flight_number >=", value, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberLessThan(String value) {
            addCriterion("trip_flight_number <", value, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberLessThanOrEqualTo(String value) {
            addCriterion("trip_flight_number <=", value, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberLike(String value) {
            addCriterion("trip_flight_number like", value, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberNotLike(String value) {
            addCriterion("trip_flight_number not like", value, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberIn(List<String> values) {
            addCriterion("trip_flight_number in", values, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberNotIn(List<String> values) {
            addCriterion("trip_flight_number not in", values, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberBetween(String value1, String value2) {
            addCriterion("trip_flight_number between", value1, value2, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripFlightNumberNotBetween(String value1, String value2) {
            addCriterion("trip_flight_number not between", value1, value2, "tripFlightNumber");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyIsNull() {
            addCriterion("trip_airline_company is null");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyIsNotNull() {
            addCriterion("trip_airline_company is not null");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyEqualTo(String value) {
            addCriterion("trip_airline_company =", value, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyNotEqualTo(String value) {
            addCriterion("trip_airline_company <>", value, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyGreaterThan(String value) {
            addCriterion("trip_airline_company >", value, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("trip_airline_company >=", value, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyLessThan(String value) {
            addCriterion("trip_airline_company <", value, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyLessThanOrEqualTo(String value) {
            addCriterion("trip_airline_company <=", value, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyLike(String value) {
            addCriterion("trip_airline_company like", value, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyNotLike(String value) {
            addCriterion("trip_airline_company not like", value, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyIn(List<String> values) {
            addCriterion("trip_airline_company in", values, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyNotIn(List<String> values) {
            addCriterion("trip_airline_company not in", values, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyBetween(String value1, String value2) {
            addCriterion("trip_airline_company between", value1, value2, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andTripAirlineCompanyNotBetween(String value1, String value2) {
            addCriterion("trip_airline_company not between", value1, value2, "tripAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberIsNull() {
            addCriterion("return_flight_number is null");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberIsNotNull() {
            addCriterion("return_flight_number is not null");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberEqualTo(String value) {
            addCriterion("return_flight_number =", value, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberNotEqualTo(String value) {
            addCriterion("return_flight_number <>", value, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberGreaterThan(String value) {
            addCriterion("return_flight_number >", value, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberGreaterThanOrEqualTo(String value) {
            addCriterion("return_flight_number >=", value, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberLessThan(String value) {
            addCriterion("return_flight_number <", value, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberLessThanOrEqualTo(String value) {
            addCriterion("return_flight_number <=", value, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberLike(String value) {
            addCriterion("return_flight_number like", value, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberNotLike(String value) {
            addCriterion("return_flight_number not like", value, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberIn(List<String> values) {
            addCriterion("return_flight_number in", values, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberNotIn(List<String> values) {
            addCriterion("return_flight_number not in", values, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberBetween(String value1, String value2) {
            addCriterion("return_flight_number between", value1, value2, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnFlightNumberNotBetween(String value1, String value2) {
            addCriterion("return_flight_number not between", value1, value2, "returnFlightNumber");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyIsNull() {
            addCriterion("return_airline_company is null");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyIsNotNull() {
            addCriterion("return_airline_company is not null");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyEqualTo(String value) {
            addCriterion("return_airline_company =", value, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyNotEqualTo(String value) {
            addCriterion("return_airline_company <>", value, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyGreaterThan(String value) {
            addCriterion("return_airline_company >", value, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("return_airline_company >=", value, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyLessThan(String value) {
            addCriterion("return_airline_company <", value, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyLessThanOrEqualTo(String value) {
            addCriterion("return_airline_company <=", value, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyLike(String value) {
            addCriterion("return_airline_company like", value, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyNotLike(String value) {
            addCriterion("return_airline_company not like", value, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyIn(List<String> values) {
            addCriterion("return_airline_company in", values, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyNotIn(List<String> values) {
            addCriterion("return_airline_company not in", values, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyBetween(String value1, String value2) {
            addCriterion("return_airline_company between", value1, value2, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andReturnAirlineCompanyNotBetween(String value1, String value2) {
            addCriterion("return_airline_company not between", value1, value2, "returnAirlineCompany");
            return (Criteria) this;
        }

        public Criteria andStockLimitIsNull() {
            addCriterion("stock_limit is null");
            return (Criteria) this;
        }

        public Criteria andStockLimitIsNotNull() {
            addCriterion("stock_limit is not null");
            return (Criteria) this;
        }

        public Criteria andStockLimitEqualTo(Byte value) {
            addCriterion("stock_limit =", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitNotEqualTo(Byte value) {
            addCriterion("stock_limit <>", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitGreaterThan(Byte value) {
            addCriterion("stock_limit >", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitGreaterThanOrEqualTo(Byte value) {
            addCriterion("stock_limit >=", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitLessThan(Byte value) {
            addCriterion("stock_limit <", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitLessThanOrEqualTo(Byte value) {
            addCriterion("stock_limit <=", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitIn(List<Byte> values) {
            addCriterion("stock_limit in", values, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitNotIn(List<Byte> values) {
            addCriterion("stock_limit not in", values, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitBetween(Byte value1, Byte value2) {
            addCriterion("stock_limit between", value1, value2, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitNotBetween(Byte value1, Byte value2) {
            addCriterion("stock_limit not between", value1, value2, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockIsNull() {
            addCriterion("stock is null");
            return (Criteria) this;
        }

        public Criteria andStockIsNotNull() {
            addCriterion("stock is not null");
            return (Criteria) this;
        }

        public Criteria andStockEqualTo(Integer value) {
            addCriterion("stock =", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotEqualTo(Integer value) {
            addCriterion("stock <>", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThan(Integer value) {
            addCriterion("stock >", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThanOrEqualTo(Integer value) {
            addCriterion("stock >=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThan(Integer value) {
            addCriterion("stock <", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThanOrEqualTo(Integer value) {
            addCriterion("stock <=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockIn(List<Integer> values) {
            addCriterion("stock in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotIn(List<Integer> values) {
            addCriterion("stock not in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockBetween(Integer value1, Integer value2) {
            addCriterion("stock between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotBetween(Integer value1, Integer value2) {
            addCriterion("stock not between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameIsNull() {
            addCriterion("trip_departure_city_name is null");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameIsNotNull() {
            addCriterion("trip_departure_city_name is not null");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameEqualTo(String value) {
            addCriterion("trip_departure_city_name =", value, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameNotEqualTo(String value) {
            addCriterion("trip_departure_city_name <>", value, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameGreaterThan(String value) {
            addCriterion("trip_departure_city_name >", value, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("trip_departure_city_name >=", value, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameLessThan(String value) {
            addCriterion("trip_departure_city_name <", value, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameLessThanOrEqualTo(String value) {
            addCriterion("trip_departure_city_name <=", value, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameLike(String value) {
            addCriterion("trip_departure_city_name like", value, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameNotLike(String value) {
            addCriterion("trip_departure_city_name not like", value, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameIn(List<String> values) {
            addCriterion("trip_departure_city_name in", values, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameNotIn(List<String> values) {
            addCriterion("trip_departure_city_name not in", values, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameBetween(String value1, String value2) {
            addCriterion("trip_departure_city_name between", value1, value2, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureCityNameNotBetween(String value1, String value2) {
            addCriterion("trip_departure_city_name not between", value1, value2, "tripDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameIsNull() {
            addCriterion("trip_departure_airport_name is null");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameIsNotNull() {
            addCriterion("trip_departure_airport_name is not null");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameEqualTo(String value) {
            addCriterion("trip_departure_airport_name =", value, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameNotEqualTo(String value) {
            addCriterion("trip_departure_airport_name <>", value, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameGreaterThan(String value) {
            addCriterion("trip_departure_airport_name >", value, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameGreaterThanOrEqualTo(String value) {
            addCriterion("trip_departure_airport_name >=", value, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameLessThan(String value) {
            addCriterion("trip_departure_airport_name <", value, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameLessThanOrEqualTo(String value) {
            addCriterion("trip_departure_airport_name <=", value, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameLike(String value) {
            addCriterion("trip_departure_airport_name like", value, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameNotLike(String value) {
            addCriterion("trip_departure_airport_name not like", value, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameIn(List<String> values) {
            addCriterion("trip_departure_airport_name in", values, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameNotIn(List<String> values) {
            addCriterion("trip_departure_airport_name not in", values, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameBetween(String value1, String value2) {
            addCriterion("trip_departure_airport_name between", value1, value2, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureAirportNameNotBetween(String value1, String value2) {
            addCriterion("trip_departure_airport_name not between", value1, value2, "tripDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalIsNull() {
            addCriterion("trip_departure_terminal is null");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalIsNotNull() {
            addCriterion("trip_departure_terminal is not null");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalEqualTo(String value) {
            addCriterion("trip_departure_terminal =", value, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalNotEqualTo(String value) {
            addCriterion("trip_departure_terminal <>", value, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalGreaterThan(String value) {
            addCriterion("trip_departure_terminal >", value, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalGreaterThanOrEqualTo(String value) {
            addCriterion("trip_departure_terminal >=", value, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalLessThan(String value) {
            addCriterion("trip_departure_terminal <", value, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalLessThanOrEqualTo(String value) {
            addCriterion("trip_departure_terminal <=", value, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalLike(String value) {
            addCriterion("trip_departure_terminal like", value, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalNotLike(String value) {
            addCriterion("trip_departure_terminal not like", value, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalIn(List<String> values) {
            addCriterion("trip_departure_terminal in", values, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalNotIn(List<String> values) {
            addCriterion("trip_departure_terminal not in", values, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalBetween(String value1, String value2) {
            addCriterion("trip_departure_terminal between", value1, value2, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTerminalNotBetween(String value1, String value2) {
            addCriterion("trip_departure_terminal not between", value1, value2, "tripDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameIsNull() {
            addCriterion("trip_destination_city_name is null");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameIsNotNull() {
            addCriterion("trip_destination_city_name is not null");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameEqualTo(String value) {
            addCriterion("trip_destination_city_name =", value, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameNotEqualTo(String value) {
            addCriterion("trip_destination_city_name <>", value, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameGreaterThan(String value) {
            addCriterion("trip_destination_city_name >", value, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("trip_destination_city_name >=", value, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameLessThan(String value) {
            addCriterion("trip_destination_city_name <", value, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameLessThanOrEqualTo(String value) {
            addCriterion("trip_destination_city_name <=", value, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameLike(String value) {
            addCriterion("trip_destination_city_name like", value, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameNotLike(String value) {
            addCriterion("trip_destination_city_name not like", value, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameIn(List<String> values) {
            addCriterion("trip_destination_city_name in", values, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameNotIn(List<String> values) {
            addCriterion("trip_destination_city_name not in", values, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameBetween(String value1, String value2) {
            addCriterion("trip_destination_city_name between", value1, value2, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationCityNameNotBetween(String value1, String value2) {
            addCriterion("trip_destination_city_name not between", value1, value2, "tripDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameIsNull() {
            addCriterion("trip_destination_airport_name is null");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameIsNotNull() {
            addCriterion("trip_destination_airport_name is not null");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameEqualTo(String value) {
            addCriterion("trip_destination_airport_name =", value, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameNotEqualTo(String value) {
            addCriterion("trip_destination_airport_name <>", value, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameGreaterThan(String value) {
            addCriterion("trip_destination_airport_name >", value, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameGreaterThanOrEqualTo(String value) {
            addCriterion("trip_destination_airport_name >=", value, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameLessThan(String value) {
            addCriterion("trip_destination_airport_name <", value, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameLessThanOrEqualTo(String value) {
            addCriterion("trip_destination_airport_name <=", value, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameLike(String value) {
            addCriterion("trip_destination_airport_name like", value, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameNotLike(String value) {
            addCriterion("trip_destination_airport_name not like", value, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameIn(List<String> values) {
            addCriterion("trip_destination_airport_name in", values, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameNotIn(List<String> values) {
            addCriterion("trip_destination_airport_name not in", values, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameBetween(String value1, String value2) {
            addCriterion("trip_destination_airport_name between", value1, value2, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationAirportNameNotBetween(String value1, String value2) {
            addCriterion("trip_destination_airport_name not between", value1, value2, "tripDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalIsNull() {
            addCriterion("trip_destination_terminal is null");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalIsNotNull() {
            addCriterion("trip_destination_terminal is not null");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalEqualTo(String value) {
            addCriterion("trip_destination_terminal =", value, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalNotEqualTo(String value) {
            addCriterion("trip_destination_terminal <>", value, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalGreaterThan(String value) {
            addCriterion("trip_destination_terminal >", value, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalGreaterThanOrEqualTo(String value) {
            addCriterion("trip_destination_terminal >=", value, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalLessThan(String value) {
            addCriterion("trip_destination_terminal <", value, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalLessThanOrEqualTo(String value) {
            addCriterion("trip_destination_terminal <=", value, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalLike(String value) {
            addCriterion("trip_destination_terminal like", value, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalNotLike(String value) {
            addCriterion("trip_destination_terminal not like", value, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalIn(List<String> values) {
            addCriterion("trip_destination_terminal in", values, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalNotIn(List<String> values) {
            addCriterion("trip_destination_terminal not in", values, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalBetween(String value1, String value2) {
            addCriterion("trip_destination_terminal between", value1, value2, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTerminalNotBetween(String value1, String value2) {
            addCriterion("trip_destination_terminal not between", value1, value2, "tripDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeIsNull() {
            addCriterion("trip_plane_type is null");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeIsNotNull() {
            addCriterion("trip_plane_type is not null");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeEqualTo(String value) {
            addCriterion("trip_plane_type =", value, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeNotEqualTo(String value) {
            addCriterion("trip_plane_type <>", value, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeGreaterThan(String value) {
            addCriterion("trip_plane_type >", value, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeGreaterThanOrEqualTo(String value) {
            addCriterion("trip_plane_type >=", value, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeLessThan(String value) {
            addCriterion("trip_plane_type <", value, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeLessThanOrEqualTo(String value) {
            addCriterion("trip_plane_type <=", value, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeLike(String value) {
            addCriterion("trip_plane_type like", value, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeNotLike(String value) {
            addCriterion("trip_plane_type not like", value, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeIn(List<String> values) {
            addCriterion("trip_plane_type in", values, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeNotIn(List<String> values) {
            addCriterion("trip_plane_type not in", values, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeBetween(String value1, String value2) {
            addCriterion("trip_plane_type between", value1, value2, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripPlaneTypeNotBetween(String value1, String value2) {
            addCriterion("trip_plane_type not between", value1, value2, "tripPlaneType");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeIsNull() {
            addCriterion("trip_departure_time is null");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeIsNotNull() {
            addCriterion("trip_departure_time is not null");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeEqualTo(Date value) {
            addCriterionForJDBCTime("trip_departure_time =", value, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeNotEqualTo(Date value) {
            addCriterionForJDBCTime("trip_departure_time <>", value, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeGreaterThan(Date value) {
            addCriterionForJDBCTime("trip_departure_time >", value, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("trip_departure_time >=", value, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeLessThan(Date value) {
            addCriterionForJDBCTime("trip_departure_time <", value, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("trip_departure_time <=", value, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeIn(List<Date> values) {
            addCriterionForJDBCTime("trip_departure_time in", values, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeNotIn(List<Date> values) {
            addCriterionForJDBCTime("trip_departure_time not in", values, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("trip_departure_time between", value1, value2, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDepartureTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("trip_departure_time not between", value1, value2, "tripDepartureTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeIsNull() {
            addCriterion("trip_destination_time is null");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeIsNotNull() {
            addCriterion("trip_destination_time is not null");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeEqualTo(Date value) {
            addCriterionForJDBCTime("trip_destination_time =", value, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeNotEqualTo(Date value) {
            addCriterionForJDBCTime("trip_destination_time <>", value, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeGreaterThan(Date value) {
            addCriterionForJDBCTime("trip_destination_time >", value, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("trip_destination_time >=", value, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeLessThan(Date value) {
            addCriterionForJDBCTime("trip_destination_time <", value, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("trip_destination_time <=", value, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeIn(List<Date> values) {
            addCriterionForJDBCTime("trip_destination_time in", values, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeNotIn(List<Date> values) {
            addCriterionForJDBCTime("trip_destination_time not in", values, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("trip_destination_time between", value1, value2, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripDestinationTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("trip_destination_time not between", value1, value2, "tripDestinationTime");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeIsNull() {
            addCriterion("trip_flight_type is null");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeIsNotNull() {
            addCriterion("trip_flight_type is not null");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeEqualTo(Byte value) {
            addCriterion("trip_flight_type =", value, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeNotEqualTo(Byte value) {
            addCriterion("trip_flight_type <>", value, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeGreaterThan(Byte value) {
            addCriterion("trip_flight_type >", value, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("trip_flight_type >=", value, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeLessThan(Byte value) {
            addCriterion("trip_flight_type <", value, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeLessThanOrEqualTo(Byte value) {
            addCriterion("trip_flight_type <=", value, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeIn(List<Byte> values) {
            addCriterion("trip_flight_type in", values, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeNotIn(List<Byte> values) {
            addCriterion("trip_flight_type not in", values, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeBetween(Byte value1, Byte value2) {
            addCriterion("trip_flight_type between", value1, value2, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripFlightTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("trip_flight_type not between", value1, value2, "tripFlightType");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferIsNull() {
            addCriterion("trip_is_transfer is null");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferIsNotNull() {
            addCriterion("trip_is_transfer is not null");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferEqualTo(Byte value) {
            addCriterion("trip_is_transfer =", value, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferNotEqualTo(Byte value) {
            addCriterion("trip_is_transfer <>", value, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferGreaterThan(Byte value) {
            addCriterion("trip_is_transfer >", value, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferGreaterThanOrEqualTo(Byte value) {
            addCriterion("trip_is_transfer >=", value, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferLessThan(Byte value) {
            addCriterion("trip_is_transfer <", value, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferLessThanOrEqualTo(Byte value) {
            addCriterion("trip_is_transfer <=", value, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferIn(List<Byte> values) {
            addCriterion("trip_is_transfer in", values, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferNotIn(List<Byte> values) {
            addCriterion("trip_is_transfer not in", values, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferBetween(Byte value1, Byte value2) {
            addCriterion("trip_is_transfer between", value1, value2, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripIsTransferNotBetween(Byte value1, Byte value2) {
            addCriterion("trip_is_transfer not between", value1, value2, "tripIsTransfer");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeIsNull() {
            addCriterion("trip_cost_time is null");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeIsNotNull() {
            addCriterion("trip_cost_time is not null");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeEqualTo(String value) {
            addCriterion("trip_cost_time =", value, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeNotEqualTo(String value) {
            addCriterion("trip_cost_time <>", value, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeGreaterThan(String value) {
            addCriterion("trip_cost_time >", value, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeGreaterThanOrEqualTo(String value) {
            addCriterion("trip_cost_time >=", value, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeLessThan(String value) {
            addCriterion("trip_cost_time <", value, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeLessThanOrEqualTo(String value) {
            addCriterion("trip_cost_time <=", value, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeLike(String value) {
            addCriterion("trip_cost_time like", value, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeNotLike(String value) {
            addCriterion("trip_cost_time not like", value, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeIn(List<String> values) {
            addCriterion("trip_cost_time in", values, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeNotIn(List<String> values) {
            addCriterion("trip_cost_time not in", values, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeBetween(String value1, String value2) {
            addCriterion("trip_cost_time between", value1, value2, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andTripCostTimeNotBetween(String value1, String value2) {
            addCriterion("trip_cost_time not between", value1, value2, "tripCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameIsNull() {
            addCriterion("return_departure_city_name is null");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameIsNotNull() {
            addCriterion("return_departure_city_name is not null");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameEqualTo(String value) {
            addCriterion("return_departure_city_name =", value, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameNotEqualTo(String value) {
            addCriterion("return_departure_city_name <>", value, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameGreaterThan(String value) {
            addCriterion("return_departure_city_name >", value, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("return_departure_city_name >=", value, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameLessThan(String value) {
            addCriterion("return_departure_city_name <", value, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameLessThanOrEqualTo(String value) {
            addCriterion("return_departure_city_name <=", value, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameLike(String value) {
            addCriterion("return_departure_city_name like", value, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameNotLike(String value) {
            addCriterion("return_departure_city_name not like", value, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameIn(List<String> values) {
            addCriterion("return_departure_city_name in", values, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameNotIn(List<String> values) {
            addCriterion("return_departure_city_name not in", values, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameBetween(String value1, String value2) {
            addCriterion("return_departure_city_name between", value1, value2, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureCityNameNotBetween(String value1, String value2) {
            addCriterion("return_departure_city_name not between", value1, value2, "returnDepartureCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameIsNull() {
            addCriterion("return_departure_airport_name is null");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameIsNotNull() {
            addCriterion("return_departure_airport_name is not null");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameEqualTo(String value) {
            addCriterion("return_departure_airport_name =", value, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameNotEqualTo(String value) {
            addCriterion("return_departure_airport_name <>", value, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameGreaterThan(String value) {
            addCriterion("return_departure_airport_name >", value, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameGreaterThanOrEqualTo(String value) {
            addCriterion("return_departure_airport_name >=", value, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameLessThan(String value) {
            addCriterion("return_departure_airport_name <", value, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameLessThanOrEqualTo(String value) {
            addCriterion("return_departure_airport_name <=", value, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameLike(String value) {
            addCriterion("return_departure_airport_name like", value, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameNotLike(String value) {
            addCriterion("return_departure_airport_name not like", value, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameIn(List<String> values) {
            addCriterion("return_departure_airport_name in", values, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameNotIn(List<String> values) {
            addCriterion("return_departure_airport_name not in", values, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameBetween(String value1, String value2) {
            addCriterion("return_departure_airport_name between", value1, value2, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureAirportNameNotBetween(String value1, String value2) {
            addCriterion("return_departure_airport_name not between", value1, value2, "returnDepartureAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalIsNull() {
            addCriterion("return_departure_terminal is null");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalIsNotNull() {
            addCriterion("return_departure_terminal is not null");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalEqualTo(String value) {
            addCriterion("return_departure_terminal =", value, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalNotEqualTo(String value) {
            addCriterion("return_departure_terminal <>", value, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalGreaterThan(String value) {
            addCriterion("return_departure_terminal >", value, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalGreaterThanOrEqualTo(String value) {
            addCriterion("return_departure_terminal >=", value, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalLessThan(String value) {
            addCriterion("return_departure_terminal <", value, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalLessThanOrEqualTo(String value) {
            addCriterion("return_departure_terminal <=", value, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalLike(String value) {
            addCriterion("return_departure_terminal like", value, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalNotLike(String value) {
            addCriterion("return_departure_terminal not like", value, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalIn(List<String> values) {
            addCriterion("return_departure_terminal in", values, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalNotIn(List<String> values) {
            addCriterion("return_departure_terminal not in", values, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalBetween(String value1, String value2) {
            addCriterion("return_departure_terminal between", value1, value2, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTerminalNotBetween(String value1, String value2) {
            addCriterion("return_departure_terminal not between", value1, value2, "returnDepartureTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameIsNull() {
            addCriterion("return_destination_city_name is null");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameIsNotNull() {
            addCriterion("return_destination_city_name is not null");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameEqualTo(String value) {
            addCriterion("return_destination_city_name =", value, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameNotEqualTo(String value) {
            addCriterion("return_destination_city_name <>", value, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameGreaterThan(String value) {
            addCriterion("return_destination_city_name >", value, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("return_destination_city_name >=", value, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameLessThan(String value) {
            addCriterion("return_destination_city_name <", value, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameLessThanOrEqualTo(String value) {
            addCriterion("return_destination_city_name <=", value, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameLike(String value) {
            addCriterion("return_destination_city_name like", value, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameNotLike(String value) {
            addCriterion("return_destination_city_name not like", value, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameIn(List<String> values) {
            addCriterion("return_destination_city_name in", values, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameNotIn(List<String> values) {
            addCriterion("return_destination_city_name not in", values, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameBetween(String value1, String value2) {
            addCriterion("return_destination_city_name between", value1, value2, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationCityNameNotBetween(String value1, String value2) {
            addCriterion("return_destination_city_name not between", value1, value2, "returnDestinationCityName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameIsNull() {
            addCriterion("return_destination_airport_name is null");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameIsNotNull() {
            addCriterion("return_destination_airport_name is not null");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameEqualTo(String value) {
            addCriterion("return_destination_airport_name =", value, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameNotEqualTo(String value) {
            addCriterion("return_destination_airport_name <>", value, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameGreaterThan(String value) {
            addCriterion("return_destination_airport_name >", value, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameGreaterThanOrEqualTo(String value) {
            addCriterion("return_destination_airport_name >=", value, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameLessThan(String value) {
            addCriterion("return_destination_airport_name <", value, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameLessThanOrEqualTo(String value) {
            addCriterion("return_destination_airport_name <=", value, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameLike(String value) {
            addCriterion("return_destination_airport_name like", value, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameNotLike(String value) {
            addCriterion("return_destination_airport_name not like", value, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameIn(List<String> values) {
            addCriterion("return_destination_airport_name in", values, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameNotIn(List<String> values) {
            addCriterion("return_destination_airport_name not in", values, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameBetween(String value1, String value2) {
            addCriterion("return_destination_airport_name between", value1, value2, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationAirportNameNotBetween(String value1, String value2) {
            addCriterion("return_destination_airport_name not between", value1, value2, "returnDestinationAirportName");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalIsNull() {
            addCriterion("return_destination_terminal is null");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalIsNotNull() {
            addCriterion("return_destination_terminal is not null");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalEqualTo(String value) {
            addCriterion("return_destination_terminal =", value, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalNotEqualTo(String value) {
            addCriterion("return_destination_terminal <>", value, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalGreaterThan(String value) {
            addCriterion("return_destination_terminal >", value, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalGreaterThanOrEqualTo(String value) {
            addCriterion("return_destination_terminal >=", value, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalLessThan(String value) {
            addCriterion("return_destination_terminal <", value, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalLessThanOrEqualTo(String value) {
            addCriterion("return_destination_terminal <=", value, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalLike(String value) {
            addCriterion("return_destination_terminal like", value, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalNotLike(String value) {
            addCriterion("return_destination_terminal not like", value, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalIn(List<String> values) {
            addCriterion("return_destination_terminal in", values, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalNotIn(List<String> values) {
            addCriterion("return_destination_terminal not in", values, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalBetween(String value1, String value2) {
            addCriterion("return_destination_terminal between", value1, value2, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTerminalNotBetween(String value1, String value2) {
            addCriterion("return_destination_terminal not between", value1, value2, "returnDestinationTerminal");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeIsNull() {
            addCriterion("return_plane_type is null");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeIsNotNull() {
            addCriterion("return_plane_type is not null");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeEqualTo(String value) {
            addCriterion("return_plane_type =", value, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeNotEqualTo(String value) {
            addCriterion("return_plane_type <>", value, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeGreaterThan(String value) {
            addCriterion("return_plane_type >", value, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeGreaterThanOrEqualTo(String value) {
            addCriterion("return_plane_type >=", value, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeLessThan(String value) {
            addCriterion("return_plane_type <", value, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeLessThanOrEqualTo(String value) {
            addCriterion("return_plane_type <=", value, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeLike(String value) {
            addCriterion("return_plane_type like", value, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeNotLike(String value) {
            addCriterion("return_plane_type not like", value, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeIn(List<String> values) {
            addCriterion("return_plane_type in", values, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeNotIn(List<String> values) {
            addCriterion("return_plane_type not in", values, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeBetween(String value1, String value2) {
            addCriterion("return_plane_type between", value1, value2, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnPlaneTypeNotBetween(String value1, String value2) {
            addCriterion("return_plane_type not between", value1, value2, "returnPlaneType");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeIsNull() {
            addCriterion("return_departure_time is null");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeIsNotNull() {
            addCriterion("return_departure_time is not null");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeEqualTo(Date value) {
            addCriterionForJDBCTime("return_departure_time =", value, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeNotEqualTo(Date value) {
            addCriterionForJDBCTime("return_departure_time <>", value, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeGreaterThan(Date value) {
            addCriterionForJDBCTime("return_departure_time >", value, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("return_departure_time >=", value, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeLessThan(Date value) {
            addCriterionForJDBCTime("return_departure_time <", value, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("return_departure_time <=", value, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeIn(List<Date> values) {
            addCriterionForJDBCTime("return_departure_time in", values, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeNotIn(List<Date> values) {
            addCriterionForJDBCTime("return_departure_time not in", values, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("return_departure_time between", value1, value2, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDepartureTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("return_departure_time not between", value1, value2, "returnDepartureTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeIsNull() {
            addCriterion("return_destination_time is null");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeIsNotNull() {
            addCriterion("return_destination_time is not null");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeEqualTo(Date value) {
            addCriterionForJDBCTime("return_destination_time =", value, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeNotEqualTo(Date value) {
            addCriterionForJDBCTime("return_destination_time <>", value, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeGreaterThan(Date value) {
            addCriterionForJDBCTime("return_destination_time >", value, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("return_destination_time >=", value, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeLessThan(Date value) {
            addCriterionForJDBCTime("return_destination_time <", value, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("return_destination_time <=", value, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeIn(List<Date> values) {
            addCriterionForJDBCTime("return_destination_time in", values, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeNotIn(List<Date> values) {
            addCriterionForJDBCTime("return_destination_time not in", values, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("return_destination_time between", value1, value2, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnDestinationTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("return_destination_time not between", value1, value2, "returnDestinationTime");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeIsNull() {
            addCriterion("return_flight_type is null");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeIsNotNull() {
            addCriterion("return_flight_type is not null");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeEqualTo(Byte value) {
            addCriterion("return_flight_type =", value, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeNotEqualTo(Byte value) {
            addCriterion("return_flight_type <>", value, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeGreaterThan(Byte value) {
            addCriterion("return_flight_type >", value, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("return_flight_type >=", value, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeLessThan(Byte value) {
            addCriterion("return_flight_type <", value, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeLessThanOrEqualTo(Byte value) {
            addCriterion("return_flight_type <=", value, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeIn(List<Byte> values) {
            addCriterion("return_flight_type in", values, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeNotIn(List<Byte> values) {
            addCriterion("return_flight_type not in", values, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeBetween(Byte value1, Byte value2) {
            addCriterion("return_flight_type between", value1, value2, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnFlightTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("return_flight_type not between", value1, value2, "returnFlightType");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferIsNull() {
            addCriterion("return_is_transfer is null");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferIsNotNull() {
            addCriterion("return_is_transfer is not null");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferEqualTo(Byte value) {
            addCriterion("return_is_transfer =", value, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferNotEqualTo(Byte value) {
            addCriterion("return_is_transfer <>", value, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferGreaterThan(Byte value) {
            addCriterion("return_is_transfer >", value, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferGreaterThanOrEqualTo(Byte value) {
            addCriterion("return_is_transfer >=", value, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferLessThan(Byte value) {
            addCriterion("return_is_transfer <", value, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferLessThanOrEqualTo(Byte value) {
            addCriterion("return_is_transfer <=", value, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferIn(List<Byte> values) {
            addCriterion("return_is_transfer in", values, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferNotIn(List<Byte> values) {
            addCriterion("return_is_transfer not in", values, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferBetween(Byte value1, Byte value2) {
            addCriterion("return_is_transfer between", value1, value2, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnIsTransferNotBetween(Byte value1, Byte value2) {
            addCriterion("return_is_transfer not between", value1, value2, "returnIsTransfer");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeIsNull() {
            addCriterion("return_cost_time is null");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeIsNotNull() {
            addCriterion("return_cost_time is not null");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeEqualTo(String value) {
            addCriterion("return_cost_time =", value, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeNotEqualTo(String value) {
            addCriterion("return_cost_time <>", value, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeGreaterThan(String value) {
            addCriterion("return_cost_time >", value, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeGreaterThanOrEqualTo(String value) {
            addCriterion("return_cost_time >=", value, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeLessThan(String value) {
            addCriterion("return_cost_time <", value, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeLessThanOrEqualTo(String value) {
            addCriterion("return_cost_time <=", value, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeLike(String value) {
            addCriterion("return_cost_time like", value, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeNotLike(String value) {
            addCriterion("return_cost_time not like", value, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeIn(List<String> values) {
            addCriterion("return_cost_time in", values, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeNotIn(List<String> values) {
            addCriterion("return_cost_time not in", values, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeBetween(String value1, String value2) {
            addCriterion("return_cost_time between", value1, value2, "returnCostTime");
            return (Criteria) this;
        }

        public Criteria andReturnCostTimeNotBetween(String value1, String value2) {
            addCriterion("return_cost_time not between", value1, value2, "returnCostTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}