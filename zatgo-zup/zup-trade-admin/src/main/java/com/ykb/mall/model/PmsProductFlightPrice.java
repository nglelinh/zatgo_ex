package com.ykb.mall.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PmsProductFlightPrice implements Serializable {
    private Long id;

    private Long flightId;

    /**
     * 价格名称
     *
     * @mbggenerated
     */
    private String priceName;

    /**
     * 供应价
     *
     * @mbggenerated
     */
    private BigDecimal supplyPrice;

    /**
     * 人数限制
     *
     * @mbggenerated
     */
    private Integer maxPeople;

    /**
     * 是否优先展示价格（0 - 否，1 - 是）
     *
     * @mbggenerated
     */
    private Byte firstShow;

    /**
     * 展示状态（0 - 不展示，1 - 展示）
     *
     * @mbggenerated
     */
    private Byte showStatus;

    private Date createTime;

    /**
     * 舱位
     *
     * @mbggenerated
     */
    private String shippingSpaceName;

    /**
     * 航班日期
     *
     * @mbggenerated
     */
    private Date flightDate;

    /**
     * 0
     *
     * @mbggenerated
     */
    private Integer stock;

    /**
     * 采购价
     *
     * @mbggenerated
     */
    private BigDecimal purchasePrice;

    /**
     * 儿童价
     *
     * @mbggenerated
     */
    private BigDecimal childPrice;

    /**
     * 儿童采购价
     *
     * @mbggenerated
     */
    private BigDecimal childPurchasePrice;

    private Date returnFlightData;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    public String getPriceName() {
        return priceName;
    }

    public void setPriceName(String priceName) {
        this.priceName = priceName;
    }

    public BigDecimal getSupplyPrice() {
        return supplyPrice;
    }

    public void setSupplyPrice(BigDecimal supplyPrice) {
        this.supplyPrice = supplyPrice;
    }

    public Integer getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(Integer maxPeople) {
        this.maxPeople = maxPeople;
    }

    public Byte getFirstShow() {
        return firstShow;
    }

    public void setFirstShow(Byte firstShow) {
        this.firstShow = firstShow;
    }

    public Byte getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(Byte showStatus) {
        this.showStatus = showStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getShippingSpaceName() {
        return shippingSpaceName;
    }

    public void setShippingSpaceName(String shippingSpaceName) {
        this.shippingSpaceName = shippingSpaceName;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public BigDecimal getChildPrice() {
        return childPrice;
    }

    public void setChildPrice(BigDecimal childPrice) {
        this.childPrice = childPrice;
    }

    public BigDecimal getChildPurchasePrice() {
        return childPurchasePrice;
    }

    public void setChildPurchasePrice(BigDecimal childPurchasePrice) {
        this.childPurchasePrice = childPurchasePrice;
    }

    public Date getReturnFlightData() {
        return returnFlightData;
    }

    public void setReturnFlightData(Date returnFlightData) {
        this.returnFlightData = returnFlightData;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", flightId=").append(flightId);
        sb.append(", priceName=").append(priceName);
        sb.append(", supplyPrice=").append(supplyPrice);
        sb.append(", maxPeople=").append(maxPeople);
        sb.append(", firstShow=").append(firstShow);
        sb.append(", showStatus=").append(showStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", shippingSpaceName=").append(shippingSpaceName);
        sb.append(", flightDate=").append(flightDate);
        sb.append(", stock=").append(stock);
        sb.append(", purchasePrice=").append(purchasePrice);
        sb.append(", childPrice=").append(childPrice);
        sb.append(", childPurchasePrice=").append(childPurchasePrice);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}