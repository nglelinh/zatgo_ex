package com.ykb.mall.model;

import java.io.Serializable;
import java.util.Date;

public class MessageTemplate implements Serializable {
    /**
     * 模板编号
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 模板编码
     *
     * @mbggenerated
     */
    private String templateCode;

    /**
     * 模板类型，1-邮件模板；2-站内消息模板；3-短信模板；4-APP推送
     *
     * @mbggenerated
     */
    private Byte templateType;

    /**
     * 模板名称
     *
     * @mbggenerated
     */
    private String templateName;

    /**
     * 模板内容，参数用 #code# 替换
     *
     * @mbggenerated
     */
    private String templateContent;

    /**
     * 模板归属，1-后台管理员；2-用户
     *
     * @mbggenerated
     */
    private Byte templateTarget;

    /**
     * 业务类型：0 - 系统，1 - 订单
     *
     * @mbggenerated
     */
    private Byte businessType;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 更新时间
     *
     * @mbggenerated
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public Byte getTemplateType() {
        return templateType;
    }

    public void setTemplateType(Byte templateType) {
        this.templateType = templateType;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }

    public Byte getTemplateTarget() {
        return templateTarget;
    }

    public void setTemplateTarget(Byte templateTarget) {
        this.templateTarget = templateTarget;
    }

    public Byte getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Byte businessType) {
        this.businessType = businessType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", templateCode=").append(templateCode);
        sb.append(", templateType=").append(templateType);
        sb.append(", templateName=").append(templateName);
        sb.append(", templateContent=").append(templateContent);
        sb.append(", templateTarget=").append(templateTarget);
        sb.append(", businessType=").append(businessType);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}