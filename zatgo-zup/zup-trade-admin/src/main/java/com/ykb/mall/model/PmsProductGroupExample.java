package com.ykb.mall.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PmsProductGroupExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PmsProductGroupExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGroupNumberIsNull() {
            addCriterion("group_number is null");
            return (Criteria) this;
        }

        public Criteria andGroupNumberIsNotNull() {
            addCriterion("group_number is not null");
            return (Criteria) this;
        }

        public Criteria andGroupNumberEqualTo(String value) {
            addCriterion("group_number =", value, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberNotEqualTo(String value) {
            addCriterion("group_number <>", value, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberGreaterThan(String value) {
            addCriterion("group_number >", value, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberGreaterThanOrEqualTo(String value) {
            addCriterion("group_number >=", value, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberLessThan(String value) {
            addCriterion("group_number <", value, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberLessThanOrEqualTo(String value) {
            addCriterion("group_number <=", value, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberLike(String value) {
            addCriterion("group_number like", value, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberNotLike(String value) {
            addCriterion("group_number not like", value, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberIn(List<String> values) {
            addCriterion("group_number in", values, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberNotIn(List<String> values) {
            addCriterion("group_number not in", values, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberBetween(String value1, String value2) {
            addCriterion("group_number between", value1, value2, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andGroupNumberNotBetween(String value1, String value2) {
            addCriterion("group_number not between", value1, value2, "groupNumber");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(Long value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(Long value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(Long value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(Long value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(Long value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(Long value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<Long> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<Long> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(Long value1, Long value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(Long value1, Long value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andDateIdIsNull() {
            addCriterion("date_id is null");
            return (Criteria) this;
        }

        public Criteria andDateIdIsNotNull() {
            addCriterion("date_id is not null");
            return (Criteria) this;
        }

        public Criteria andDateIdEqualTo(Long value) {
            addCriterion("date_id =", value, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdNotEqualTo(Long value) {
            addCriterion("date_id <>", value, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdGreaterThan(Long value) {
            addCriterion("date_id >", value, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdGreaterThanOrEqualTo(Long value) {
            addCriterion("date_id >=", value, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdLessThan(Long value) {
            addCriterion("date_id <", value, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdLessThanOrEqualTo(Long value) {
            addCriterion("date_id <=", value, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdIn(List<Long> values) {
            addCriterion("date_id in", values, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdNotIn(List<Long> values) {
            addCriterion("date_id not in", values, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdBetween(Long value1, Long value2) {
            addCriterion("date_id between", value1, value2, "dateId");
            return (Criteria) this;
        }

        public Criteria andDateIdNotBetween(Long value1, Long value2) {
            addCriterion("date_id not between", value1, value2, "dateId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSecondNameIsNull() {
            addCriterion("second_name is null");
            return (Criteria) this;
        }

        public Criteria andSecondNameIsNotNull() {
            addCriterion("second_name is not null");
            return (Criteria) this;
        }

        public Criteria andSecondNameEqualTo(String value) {
            addCriterion("second_name =", value, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameNotEqualTo(String value) {
            addCriterion("second_name <>", value, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameGreaterThan(String value) {
            addCriterion("second_name >", value, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameGreaterThanOrEqualTo(String value) {
            addCriterion("second_name >=", value, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameLessThan(String value) {
            addCriterion("second_name <", value, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameLessThanOrEqualTo(String value) {
            addCriterion("second_name <=", value, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameLike(String value) {
            addCriterion("second_name like", value, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameNotLike(String value) {
            addCriterion("second_name not like", value, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameIn(List<String> values) {
            addCriterion("second_name in", values, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameNotIn(List<String> values) {
            addCriterion("second_name not in", values, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameBetween(String value1, String value2) {
            addCriterion("second_name between", value1, value2, "secondName");
            return (Criteria) this;
        }

        public Criteria andSecondNameNotBetween(String value1, String value2) {
            addCriterion("second_name not between", value1, value2, "secondName");
            return (Criteria) this;
        }

        public Criteria andStockLimitIsNull() {
            addCriterion("stock_limit is null");
            return (Criteria) this;
        }

        public Criteria andStockLimitIsNotNull() {
            addCriterion("stock_limit is not null");
            return (Criteria) this;
        }

        public Criteria andStockLimitEqualTo(Byte value) {
            addCriterion("stock_limit =", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitNotEqualTo(Byte value) {
            addCriterion("stock_limit <>", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitGreaterThan(Byte value) {
            addCriterion("stock_limit >", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitGreaterThanOrEqualTo(Byte value) {
            addCriterion("stock_limit >=", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitLessThan(Byte value) {
            addCriterion("stock_limit <", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitLessThanOrEqualTo(Byte value) {
            addCriterion("stock_limit <=", value, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitIn(List<Byte> values) {
            addCriterion("stock_limit in", values, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitNotIn(List<Byte> values) {
            addCriterion("stock_limit not in", values, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitBetween(Byte value1, Byte value2) {
            addCriterion("stock_limit between", value1, value2, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockLimitNotBetween(Byte value1, Byte value2) {
            addCriterion("stock_limit not between", value1, value2, "stockLimit");
            return (Criteria) this;
        }

        public Criteria andStockIsNull() {
            addCriterion("stock is null");
            return (Criteria) this;
        }

        public Criteria andStockIsNotNull() {
            addCriterion("stock is not null");
            return (Criteria) this;
        }

        public Criteria andStockEqualTo(Integer value) {
            addCriterion("stock =", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotEqualTo(Integer value) {
            addCriterion("stock <>", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThan(Integer value) {
            addCriterion("stock >", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThanOrEqualTo(Integer value) {
            addCriterion("stock >=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThan(Integer value) {
            addCriterion("stock <", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThanOrEqualTo(Integer value) {
            addCriterion("stock <=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockIn(List<Integer> values) {
            addCriterion("stock in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotIn(List<Integer> values) {
            addCriterion("stock not in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockBetween(Integer value1, Integer value2) {
            addCriterion("stock between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotBetween(Integer value1, Integer value2) {
            addCriterion("stock not between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andMinPeopleIsNull() {
            addCriterion("min_people is null");
            return (Criteria) this;
        }

        public Criteria andMinPeopleIsNotNull() {
            addCriterion("min_people is not null");
            return (Criteria) this;
        }

        public Criteria andMinPeopleEqualTo(Integer value) {
            addCriterion("min_people =", value, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleNotEqualTo(Integer value) {
            addCriterion("min_people <>", value, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleGreaterThan(Integer value) {
            addCriterion("min_people >", value, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleGreaterThanOrEqualTo(Integer value) {
            addCriterion("min_people >=", value, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleLessThan(Integer value) {
            addCriterion("min_people <", value, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleLessThanOrEqualTo(Integer value) {
            addCriterion("min_people <=", value, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleIn(List<Integer> values) {
            addCriterion("min_people in", values, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleNotIn(List<Integer> values) {
            addCriterion("min_people not in", values, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleBetween(Integer value1, Integer value2) {
            addCriterion("min_people between", value1, value2, "minPeople");
            return (Criteria) this;
        }

        public Criteria andMinPeopleNotBetween(Integer value1, Integer value2) {
            addCriterion("min_people not between", value1, value2, "minPeople");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayIsNull() {
            addCriterion("enroll_end_day is null");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayIsNotNull() {
            addCriterion("enroll_end_day is not null");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayEqualTo(Integer value) {
            addCriterion("enroll_end_day =", value, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayNotEqualTo(Integer value) {
            addCriterion("enroll_end_day <>", value, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayGreaterThan(Integer value) {
            addCriterion("enroll_end_day >", value, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayGreaterThanOrEqualTo(Integer value) {
            addCriterion("enroll_end_day >=", value, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayLessThan(Integer value) {
            addCriterion("enroll_end_day <", value, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayLessThanOrEqualTo(Integer value) {
            addCriterion("enroll_end_day <=", value, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayIn(List<Integer> values) {
            addCriterion("enroll_end_day in", values, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayNotIn(List<Integer> values) {
            addCriterion("enroll_end_day not in", values, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayBetween(Integer value1, Integer value2) {
            addCriterion("enroll_end_day between", value1, value2, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndDayNotBetween(Integer value1, Integer value2) {
            addCriterion("enroll_end_day not between", value1, value2, "enrollEndDay");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeIsNull() {
            addCriterion("enroll_end_time is null");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeIsNotNull() {
            addCriterion("enroll_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeEqualTo(Date value) {
            addCriterion("enroll_end_time =", value, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeNotEqualTo(Date value) {
            addCriterion("enroll_end_time <>", value, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeGreaterThan(Date value) {
            addCriterion("enroll_end_time >", value, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("enroll_end_time >=", value, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeLessThan(Date value) {
            addCriterion("enroll_end_time <", value, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("enroll_end_time <=", value, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeIn(List<Date> values) {
            addCriterion("enroll_end_time in", values, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeNotIn(List<Date> values) {
            addCriterion("enroll_end_time not in", values, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeBetween(Date value1, Date value2) {
            addCriterion("enroll_end_time between", value1, value2, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("enroll_end_time not between", value1, value2, "enrollEndTime");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendIsNull() {
            addCriterion("enroll_in_weekend is null");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendIsNotNull() {
            addCriterion("enroll_in_weekend is not null");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendEqualTo(Byte value) {
            addCriterion("enroll_in_weekend =", value, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendNotEqualTo(Byte value) {
            addCriterion("enroll_in_weekend <>", value, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendGreaterThan(Byte value) {
            addCriterion("enroll_in_weekend >", value, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendGreaterThanOrEqualTo(Byte value) {
            addCriterion("enroll_in_weekend >=", value, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendLessThan(Byte value) {
            addCriterion("enroll_in_weekend <", value, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendLessThanOrEqualTo(Byte value) {
            addCriterion("enroll_in_weekend <=", value, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendIn(List<Byte> values) {
            addCriterion("enroll_in_weekend in", values, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendNotIn(List<Byte> values) {
            addCriterion("enroll_in_weekend not in", values, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendBetween(Byte value1, Byte value2) {
            addCriterion("enroll_in_weekend between", value1, value2, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andEnrollInWeekendNotBetween(Byte value1, Byte value2) {
            addCriterion("enroll_in_weekend not between", value1, value2, "enrollInWeekend");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeIsNull() {
            addCriterion("collection_time is null");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeIsNotNull() {
            addCriterion("collection_time is not null");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeEqualTo(Date value) {
            addCriterion("collection_time =", value, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeNotEqualTo(Date value) {
            addCriterion("collection_time <>", value, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeGreaterThan(Date value) {
            addCriterion("collection_time >", value, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("collection_time >=", value, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeLessThan(Date value) {
            addCriterion("collection_time <", value, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeLessThanOrEqualTo(Date value) {
            addCriterion("collection_time <=", value, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeIn(List<Date> values) {
            addCriterion("collection_time in", values, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeNotIn(List<Date> values) {
            addCriterion("collection_time not in", values, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeBetween(Date value1, Date value2) {
            addCriterion("collection_time between", value1, value2, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andCollectionTimeNotBetween(Date value1, Date value2) {
            addCriterion("collection_time not between", value1, value2, "collectionTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeIsNull() {
            addCriterion("trip_end_time is null");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeIsNotNull() {
            addCriterion("trip_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeEqualTo(Date value) {
            addCriterion("trip_end_time =", value, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeNotEqualTo(Date value) {
            addCriterion("trip_end_time <>", value, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeGreaterThan(Date value) {
            addCriterion("trip_end_time >", value, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("trip_end_time >=", value, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeLessThan(Date value) {
            addCriterion("trip_end_time <", value, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("trip_end_time <=", value, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeIn(List<Date> values) {
            addCriterion("trip_end_time in", values, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeNotIn(List<Date> values) {
            addCriterion("trip_end_time not in", values, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeBetween(Date value1, Date value2) {
            addCriterion("trip_end_time between", value1, value2, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTripEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("trip_end_time not between", value1, value2, "tripEndTime");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelIsNull() {
            addCriterion("transport_urgent_tel is null");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelIsNotNull() {
            addCriterion("transport_urgent_tel is not null");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelEqualTo(String value) {
            addCriterion("transport_urgent_tel =", value, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelNotEqualTo(String value) {
            addCriterion("transport_urgent_tel <>", value, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelGreaterThan(String value) {
            addCriterion("transport_urgent_tel >", value, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelGreaterThanOrEqualTo(String value) {
            addCriterion("transport_urgent_tel >=", value, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelLessThan(String value) {
            addCriterion("transport_urgent_tel <", value, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelLessThanOrEqualTo(String value) {
            addCriterion("transport_urgent_tel <=", value, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelLike(String value) {
            addCriterion("transport_urgent_tel like", value, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelNotLike(String value) {
            addCriterion("transport_urgent_tel not like", value, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelIn(List<String> values) {
            addCriterion("transport_urgent_tel in", values, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelNotIn(List<String> values) {
            addCriterion("transport_urgent_tel not in", values, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelBetween(String value1, String value2) {
            addCriterion("transport_urgent_tel between", value1, value2, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andTransportUrgentTelNotBetween(String value1, String value2) {
            addCriterion("transport_urgent_tel not between", value1, value2, "transportUrgentTel");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceIsNull() {
            addCriterion("meet_place is null");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceIsNotNull() {
            addCriterion("meet_place is not null");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceEqualTo(String value) {
            addCriterion("meet_place =", value, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceNotEqualTo(String value) {
            addCriterion("meet_place <>", value, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceGreaterThan(String value) {
            addCriterion("meet_place >", value, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceGreaterThanOrEqualTo(String value) {
            addCriterion("meet_place >=", value, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceLessThan(String value) {
            addCriterion("meet_place <", value, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceLessThanOrEqualTo(String value) {
            addCriterion("meet_place <=", value, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceLike(String value) {
            addCriterion("meet_place like", value, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceNotLike(String value) {
            addCriterion("meet_place not like", value, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceIn(List<String> values) {
            addCriterion("meet_place in", values, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceNotIn(List<String> values) {
            addCriterion("meet_place not in", values, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceBetween(String value1, String value2) {
            addCriterion("meet_place between", value1, value2, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetPlaceNotBetween(String value1, String value2) {
            addCriterion("meet_place not between", value1, value2, "meetPlace");
            return (Criteria) this;
        }

        public Criteria andMeetSignIsNull() {
            addCriterion("meet_sign is null");
            return (Criteria) this;
        }

        public Criteria andMeetSignIsNotNull() {
            addCriterion("meet_sign is not null");
            return (Criteria) this;
        }

        public Criteria andMeetSignEqualTo(String value) {
            addCriterion("meet_sign =", value, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignNotEqualTo(String value) {
            addCriterion("meet_sign <>", value, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignGreaterThan(String value) {
            addCriterion("meet_sign >", value, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignGreaterThanOrEqualTo(String value) {
            addCriterion("meet_sign >=", value, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignLessThan(String value) {
            addCriterion("meet_sign <", value, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignLessThanOrEqualTo(String value) {
            addCriterion("meet_sign <=", value, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignLike(String value) {
            addCriterion("meet_sign like", value, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignNotLike(String value) {
            addCriterion("meet_sign not like", value, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignIn(List<String> values) {
            addCriterion("meet_sign in", values, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignNotIn(List<String> values) {
            addCriterion("meet_sign not in", values, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignBetween(String value1, String value2) {
            addCriterion("meet_sign between", value1, value2, "meetSign");
            return (Criteria) this;
        }

        public Criteria andMeetSignNotBetween(String value1, String value2) {
            addCriterion("meet_sign not between", value1, value2, "meetSign");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameIsNull() {
            addCriterion("local_guide_name is null");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameIsNotNull() {
            addCriterion("local_guide_name is not null");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameEqualTo(String value) {
            addCriterion("local_guide_name =", value, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameNotEqualTo(String value) {
            addCriterion("local_guide_name <>", value, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameGreaterThan(String value) {
            addCriterion("local_guide_name >", value, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameGreaterThanOrEqualTo(String value) {
            addCriterion("local_guide_name >=", value, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameLessThan(String value) {
            addCriterion("local_guide_name <", value, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameLessThanOrEqualTo(String value) {
            addCriterion("local_guide_name <=", value, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameLike(String value) {
            addCriterion("local_guide_name like", value, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameNotLike(String value) {
            addCriterion("local_guide_name not like", value, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameIn(List<String> values) {
            addCriterion("local_guide_name in", values, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameNotIn(List<String> values) {
            addCriterion("local_guide_name not in", values, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameBetween(String value1, String value2) {
            addCriterion("local_guide_name between", value1, value2, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuideNameNotBetween(String value1, String value2) {
            addCriterion("local_guide_name not between", value1, value2, "localGuideName");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneIsNull() {
            addCriterion("local_guide_phone is null");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneIsNotNull() {
            addCriterion("local_guide_phone is not null");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneEqualTo(String value) {
            addCriterion("local_guide_phone =", value, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneNotEqualTo(String value) {
            addCriterion("local_guide_phone <>", value, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneGreaterThan(String value) {
            addCriterion("local_guide_phone >", value, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneGreaterThanOrEqualTo(String value) {
            addCriterion("local_guide_phone >=", value, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneLessThan(String value) {
            addCriterion("local_guide_phone <", value, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneLessThanOrEqualTo(String value) {
            addCriterion("local_guide_phone <=", value, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneLike(String value) {
            addCriterion("local_guide_phone like", value, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneNotLike(String value) {
            addCriterion("local_guide_phone not like", value, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneIn(List<String> values) {
            addCriterion("local_guide_phone in", values, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneNotIn(List<String> values) {
            addCriterion("local_guide_phone not in", values, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneBetween(String value1, String value2) {
            addCriterion("local_guide_phone between", value1, value2, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andLocalGuidePhoneNotBetween(String value1, String value2) {
            addCriterion("local_guide_phone not between", value1, value2, "localGuidePhone");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIsNull() {
            addCriterion("delete_status is null");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIsNotNull() {
            addCriterion("delete_status is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusEqualTo(Byte value) {
            addCriterion("delete_status =", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotEqualTo(Byte value) {
            addCriterion("delete_status <>", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusGreaterThan(Byte value) {
            addCriterion("delete_status >", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("delete_status >=", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusLessThan(Byte value) {
            addCriterion("delete_status <", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusLessThanOrEqualTo(Byte value) {
            addCriterion("delete_status <=", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIn(List<Byte> values) {
            addCriterion("delete_status in", values, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotIn(List<Byte> values) {
            addCriterion("delete_status not in", values, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusBetween(Byte value1, Byte value2) {
            addCriterion("delete_status between", value1, value2, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("delete_status not between", value1, value2, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}