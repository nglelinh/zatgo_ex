package com.zatgo.zup.trade.admin.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.ykb.mall.common.model.OmsOrder;
import com.zatgo.zup.trade.admin.dto.OmsCloseParam;
import com.zatgo.zup.trade.admin.dto.OmsHandleFinishParam;
import com.zatgo.zup.trade.admin.dto.OmsHasCompleted;
import com.zatgo.zup.trade.admin.dto.OmsMoneyInfoParam;
import com.zatgo.zup.trade.admin.dto.OmsOrderDeliveryParam;
import com.zatgo.zup.trade.admin.dto.OmsOrderDetail;
import com.zatgo.zup.trade.admin.dto.OmsOrderQueryParam;
import com.zatgo.zup.trade.admin.dto.OmsPaymentConfirmParam;
import com.zatgo.zup.trade.admin.dto.OmsReceiverInfoParam;
import com.zatgo.zup.trade.admin.dto.OmsRefundInfoParam;
import com.zatgo.zup.trade.admin.dto.OmsSignParam;

/**
 * 订单管理Service
 * Created by chen on 2018/10/11.
 */
public interface OmsOrderService {
    /**
     * 订单查询
     */
    List<OmsOrder> list(OmsOrderQueryParam queryParam, Integer pageSize, Integer pageNum);

    /**
     * 批量发货
     */
    @Transactional
    int delivery(List<OmsOrderDeliveryParam> deliveryParamList);

    /**
     * 批量关闭订单
     */
    @Transactional
    int close(List<Long> ids, String note);

    /**
     * 批量删除订单
     */
    int delete(List<Long> ids);

    /**
     * 获取指定订单详情
     */
    OmsOrderDetail detail(Long id);

    /**
     * 修改订单收货人信息
     */
    @Transactional
    int updateReceiverInfo(OmsReceiverInfoParam receiverInfoParam);

    /**
     * 修改订单费用信息
     */
    @Transactional
    int updateMoneyInfo(OmsMoneyInfoParam moneyInfoParam);

    /**
     * 修改订单备注
     */
    @Transactional
    int updateNote(Long id, String note, Integer status);

    /**
     * 确认订单支付状态
     * @param param
     * @return
     */
    @Transactional
    int confirmPayment(OmsPaymentConfirmParam param);

    /**
     * 创建订单历史操作记录
     * @param orderId
     * @param orderStatus
     * @param note
     * @return
     */
    @Transactional
    int createOrderOperateHistory(Long orderId, Integer orderStatus, String note);

    /**
     * 材料办理完成
     * @param param
     * @return
     */
    @Transactional
    int handleFinish(OmsHandleFinishParam param);

    /**
     * 协议签署完成
     * @param param
     * @return
     */
    @Transactional
    int signAgreement(OmsSignParam param);

    /**
     * 已完成订单
     * @param param
     * @return
     */
    @Transactional
    int completeOrder(OmsHasCompleted param);

    /**
     * 关闭订单
     * @param param
     * @return
     */
    @Transactional
    int closeOmsOrder(OmsCloseParam param);

    /**
     * 退款订单金额
     * @param param
     * @return
     */
    @Transactional
    int refundOmsPrice(OmsRefundInfoParam param);
}
