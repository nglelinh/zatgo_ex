package com.zatgo.zup.trade.admin.dto;

import java.util.Date;
import java.util.List;

import com.ykb.mall.common.model.CmsPrefrenceAreaProductRelation;
import com.ykb.mall.common.model.CmsSubjectProductRelation;
import com.ykb.mall.common.model.PmsProductAttributeValue;
import com.ykb.mall.common.model.PmsProductRelation;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("文章录入")
public class PmsProductArticleParam {
	
	@ApiModelProperty("分类编号")
	private Long productCategoryId;

	@ApiModelProperty("属性编号")
    private Long productAttributeCategoryId;
	
	@ApiModelProperty(value = "业务类型（1-旅游；2-机票；3-酒店；4-文章）", hidden = true)
	private Byte businessType;
	
	@ApiModelProperty("标题")
	private String name;
	
	@ApiModelProperty("排序")
	private Integer sort;
	
	@ApiModelProperty("副标题")
	private String subTitle;
	
	@ApiModelProperty("内容")
	private String detailDesc;
	
	@ApiModelProperty("作者")
	private String author;
	
	@ApiModelProperty("作者头像")
	private String authorIcon;
	
	@ApiModelProperty("是否发布0->下架；1->上架")
	private Integer publishStatus;
	
	@ApiModelProperty("发布时间")
	private Date publishTime;
	
	@ApiModelProperty("封面")
	private String pic;

	@ApiModelProperty("标签")
	private String tags;
	
	@ApiModelProperty("图册")
	private String albumPics;
	
	@ApiModelProperty("新品状态:0->不是新品；1->新品")
	private Integer newStatus;
	
	@ApiModelProperty("推荐状态；0->不推荐；1->推荐")
	private Integer recommandStatus;
	
	@ApiModelProperty("删除状态：0->未删除；1->已删除")
	private Integer deleteStatus;
	
	@ApiModelProperty("点赞数")
	private Integer likeNum;
	
	@ApiModelProperty("浏览数")
	private Integer watchNum;
	
	@ApiModelProperty("商品参数及自定义规格属性")
    private List<PmsProductAttributeValue> productAttributeValueList;
	
	@ApiModelProperty("专题和商品关系")
    private List<CmsSubjectProductRelation> subjectProductRelationList;
	
    @ApiModelProperty("优选专区和商品的关系")
    private List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList;
    
    @ApiModelProperty("商品和商品的关系")
    private List<PmsProductRelation> pmsProductRelationList;
    
    @ApiModelProperty("地址")
    private String address;
    
    @ApiModelProperty("职位")
    private String position;

	public Long getProductCategoryId() {
		return productCategoryId;
	}

	public void setProductCategoryId(Long productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public Long getProductAttributeCategoryId() {
		return productAttributeCategoryId;
	}

	public void setProductAttributeCategoryId(Long productAttributeCategoryId) {
		this.productAttributeCategoryId = productAttributeCategoryId;
	}

	public Byte getBusinessType() {
		return businessType;
	}

	public void setBusinessType(Byte businessType) {
		this.businessType = businessType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getDetailDesc() {
		return detailDesc;
	}

	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthorIcon() {
		return authorIcon;
	}

	public void setAuthorIcon(String authorIcon) {
		this.authorIcon = authorIcon;
	}

	public Integer getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(Integer publishStatus) {
		this.publishStatus = publishStatus;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	public String getAlbumPics() {
		return albumPics;
	}

	public void setAlbumPics(String albumPics) {
		this.albumPics = albumPics;
	}

	public Integer getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(Integer newStatus) {
		this.newStatus = newStatus;
	}

	public Integer getRecommandStatus() {
		return recommandStatus;
	}

	public void setRecommandStatus(Integer recommandStatus) {
		this.recommandStatus = recommandStatus;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public Integer getLikeNum() {
		return likeNum;
	}

	public void setLikeNum(Integer likeNum) {
		this.likeNum = likeNum;
	}

	public Integer getWatchNum() {
		return watchNum;
	}

	public void setWatchNum(Integer watchNum) {
		this.watchNum = watchNum;
	}

	public List<PmsProductAttributeValue> getProductAttributeValueList() {
		return productAttributeValueList;
	}

	public void setProductAttributeValueList(List<PmsProductAttributeValue> productAttributeValueList) {
		this.productAttributeValueList = productAttributeValueList;
	}

	public List<CmsSubjectProductRelation> getSubjectProductRelationList() {
		return subjectProductRelationList;
	}

	public void setSubjectProductRelationList(List<CmsSubjectProductRelation> subjectProductRelationList) {
		this.subjectProductRelationList = subjectProductRelationList;
	}

	public List<CmsPrefrenceAreaProductRelation> getPrefrenceAreaProductRelationList() {
		return prefrenceAreaProductRelationList;
	}

	public void setPrefrenceAreaProductRelationList(
			List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList) {
		this.prefrenceAreaProductRelationList = prefrenceAreaProductRelationList;
	}

	public List<PmsProductRelation> getPmsProductRelationList() {
		return pmsProductRelationList;
	}

	public void setPmsProductRelationList(List<PmsProductRelation> pmsProductRelationList) {
		this.pmsProductRelationList = pmsProductRelationList;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}
}
