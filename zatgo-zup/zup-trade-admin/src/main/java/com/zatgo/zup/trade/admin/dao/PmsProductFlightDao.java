package com.zatgo.zup.trade.admin.dao;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.trade.admin.dto.PmsProductFlightResult;

public interface PmsProductFlightDao {
	
	PmsProductFlightResult details(@Param("id") Long id);

}
