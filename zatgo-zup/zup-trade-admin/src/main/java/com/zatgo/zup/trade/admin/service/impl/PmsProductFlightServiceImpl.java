package com.zatgo.zup.trade.admin.service.impl;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.ykb.mall.common.model.CmsSubjectProductRelationExample;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductExample;
import com.ykb.mall.common.model.PmsProductFlight;
import com.ykb.mall.common.model.PmsProductFlightExample;
import com.ykb.mall.common.model.PmsProductFlightPrice;
import com.ykb.mall.common.model.PmsProductFlightPriceExample;
import com.ykb.mall.common.model.PmsProductRelation;
import com.ykb.mall.common.model.PmsProductRelationExample;
import com.zatgo.zup.trade.admin.mapper.*;
import com.ykb.mall.model.*;
import com.zatgo.zup.trade.admin.dao.CmsSubjectProductRelationDao;
import com.zatgo.zup.trade.admin.dao.PmsProductFlightDao;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsProductFlightParams;
import com.zatgo.zup.trade.admin.dto.PmsProductFlightPriceParams;
import com.zatgo.zup.trade.admin.service.PmsProductFlightService;

@Service
public class PmsProductFlightServiceImpl implements PmsProductFlightService {

	@Autowired
	private PmsProductMapper pmsProductMapper;
	@Autowired
	private PmsProductFlightMapper pmsProductFlightMapper;
	@Autowired
	private PmsProductFlightPriceMapper pmsProductFlightPriceMapper;
	@Autowired
	private PmsProductFlightDao pmsProductFlightDao;
	@Autowired
	private CmsSubjectProductRelationDao subjectProductRelationDao;
	@Autowired
	private PmsProductRelationMapper pmsProductRelationMapper;
	@Autowired
	private CmsSubjectProductRelationMapper subjectProductRelationMapper;

	@Transactional
	@Override
	public CommonResult create(PmsProductFlightParams params) {

		PmsProduct product = params;
		pmsProductMapper.insertSelective(product);

		Long productId = product.getId();

		PmsProductFlight flight = new PmsProductFlight();
		BeanUtils.copyProperties(params, flight);
		flight.setProductId(productId);
		flight.setCreateTime(new Date());
		flight.setUpdateTime(new Date());
		pmsProductFlightMapper.insertSelective(flight);

		// 关联专题
		relateAndInsertList(subjectProductRelationDao, params.getSubjectProductRelationList(), productId);
		List<PmsProductRelation> pmsProductRelationList = params.getPmsProductRelationList();
		if (!CollectionUtils.isEmpty(pmsProductRelationList)) {
			for (PmsProductRelation relation : pmsProductRelationList) {
				relation.setProductId(productId);
				pmsProductRelationMapper.insertSelective(relation);
			}
		}

		return new CommonResult().success();
	}

	@Override
	public CommonResult details(Long id) {
		return new CommonResult().success(pmsProductFlightDao.details(id));
	}

	@Transactional
	@Override
	public CommonResult update(PmsProductFlightParams params) {

		Long productId = params.getId();
		PmsProduct product = params;
		PmsProductExample productExample = new PmsProductExample();
		productExample.createCriteria().andIdEqualTo(productId);
		pmsProductMapper.updateByExampleSelective(product, productExample);

		PmsProductFlight flight = new PmsProductFlight();
		BeanUtils.copyProperties(params, flight);
		flight.setUpdateTime(new Date());

		PmsProductFlightExample example = new PmsProductFlightExample();
		PmsProductFlightExample.Criteria criteria = example.createCriteria();
		criteria.andIdEqualTo(params.getFlightId());

		pmsProductFlightMapper.updateByExampleSelective(flight, example);

		// 关联专题
		CmsSubjectProductRelationExample subjectProductRelationExample = new CmsSubjectProductRelationExample();
		subjectProductRelationExample.createCriteria().andProductIdEqualTo(productId);
		subjectProductRelationMapper.deleteByExample(subjectProductRelationExample);
		relateAndInsertList(subjectProductRelationDao, params.getSubjectProductRelationList(), productId);

		PmsProductRelationExample pmsProductRelationExample = new PmsProductRelationExample();
		pmsProductRelationExample.createCriteria().andProductIdEqualTo(productId);
		pmsProductRelationMapper.deleteByExample(pmsProductRelationExample);
		List<PmsProductRelation> pmsProductRelationList = params.getPmsProductRelationList();
		if (!CollectionUtils.isEmpty(pmsProductRelationList)) {
			for (PmsProductRelation relation : pmsProductRelationList) {
				relation.setProductId(productId);
				pmsProductRelationMapper.insertSelective(relation);
			}
		}

		return new CommonResult().success();
	}

	@Override
	public int deleteProductFlight(Long flightId) {
		PmsProductFlightExample example = new PmsProductFlightExample();
		PmsProductFlightExample.Criteria criteria = example.createCriteria();
		criteria.andIdEqualTo(flightId);

		PmsProductFlight record = new PmsProductFlight();
		// record.setDeleteStatus(YesOrNo.YES.getCode());
		return pmsProductFlightMapper.updateByExampleSelective(record, example);
	}

	private <T> void relateAndInsertList(Object dao, List<T> dataList, Long productId) {
		try {
			if (CollectionUtils.isEmpty(dataList))
				return;
			for (Object item : dataList) {
				Method setId = item.getClass().getMethod("setId", Long.class);
				setId.invoke(item, (Long) null);
				Method setProductId = item.getClass().getMethod("setProductId", Long.class);
				setProductId.invoke(item, productId);
			}
			Method insertList = dao.getClass().getMethod("insertList", List.class);
			insertList.invoke(dao, dataList);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public CommonResult flightPriceList(Long flightId) {
		PmsProductFlightPriceExample example = new PmsProductFlightPriceExample();
		example.createCriteria().andFlightIdEqualTo(flightId).andShowStatusEqualTo(YesOrNo.YES.getCode());
		List<PmsProductFlightPrice> flightPrices = pmsProductFlightPriceMapper.selectByExample(example);
		return new CommonResult().success(flightPrices);
	}

	@Transactional
	@Override
	public CommonResult createFlightPrice(Long flightId, List<PmsProductFlightPriceParams> params) {
		if (CollectionUtils.isEmpty(params)) {
			return new CommonResult().failed("价格信息丢失");
		}

		for (PmsProductFlightPriceParams priceParams : params) {
			PmsProductFlightPrice price = new PmsProductFlightPrice();
			BeanUtils.copyProperties(priceParams, price);
			price.setFlightId(flightId);
			price.setCreateTime(new Date());
			pmsProductFlightPriceMapper.insertSelective(price);
		}

		return new CommonResult().success();
	}

	@Transactional
	@Override
	public CommonResult updateFlightPrice(List<PmsProductFlightPriceParams> params) {
		
		if(CollectionUtils.isEmpty(params)) {
			return new CommonResult().failed("价格信息丢失");
		}
		
		for (PmsProductFlightPriceParams priceParams : params) {
			PmsProductFlightPrice price = new PmsProductFlightPrice();
			BeanUtils.copyProperties(priceParams, price);
			
			Long id = priceParams.getId();
			if(id == null) {
				price.setCreateTime(new Date());
				pmsProductFlightPriceMapper.insertSelective(price);
			} else {
				PmsProductFlightPriceExample example = new PmsProductFlightPriceExample();
				example.createCriteria().andIdEqualTo(id);
				pmsProductFlightPriceMapper.updateByExampleSelective(price, example);
			}
			
		}
		
		return new CommonResult().success();
	}

}
