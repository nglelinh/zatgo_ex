package com.zatgo.zup.trade.admin.dto;

import java.util.List;

import com.ykb.mall.common.model.PmsProductGroup;
import com.ykb.mall.common.model.PmsProductGroupPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/19 16:26
 */
@ApiModel("班期详细信息")
public class PmsProductGroupInfo extends PmsProductGroup {

    @ApiModelProperty("班期交通信息")
    private List<PmsProductGroupTransportInfo> transportInfo;

    @ApiModelProperty("班期价格")
    private List<PmsProductGroupPrice> priceInfo;

    @ApiModelProperty("报名截止时间（HH:mm格式）")
    private String enrollEndTimeParam;

    @ApiModelProperty("出团集合时间（HH:mm格式）")
    private String collectionTimeParam;

    @ApiModelProperty("行程结束时间（HH:mm格式）")
    private String tripEndTimeParam;

    public List<PmsProductGroupTransportInfo> getTransportInfo() {
        return transportInfo;
    }

    public void setTransportInfo(List<PmsProductGroupTransportInfo> transportInfo) {
        this.transportInfo = transportInfo;
    }

    public List<PmsProductGroupPrice> getPriceInfo() {
        return priceInfo;
    }

    public void setPriceInfo(List<PmsProductGroupPrice> priceInfo) {
        this.priceInfo = priceInfo;
    }

    public String getEnrollEndTimeParam() {
        return enrollEndTimeParam;
    }

    public void setEnrollEndTimeParam(String enrollEndTimeParam) {
        this.enrollEndTimeParam = enrollEndTimeParam;
    }

    public String getCollectionTimeParam() {
        return collectionTimeParam;
    }

    public void setCollectionTimeParam(String collectionTimeParam) {
        this.collectionTimeParam = collectionTimeParam;
    }

    public String getTripEndTimeParam() {
        return tripEndTimeParam;
    }

    public void setTripEndTimeParam(String tripEndTimeParam) {
        this.tripEndTimeParam = tripEndTimeParam;
    }
}