package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ykb.mall.common.model.OmsOrder;
import com.zatgo.zup.trade.admin.dto.OmsOrderDeliveryParam;
import com.zatgo.zup.trade.admin.dto.OmsOrderDetail;
import com.zatgo.zup.trade.admin.dto.OmsOrderQueryParam;

/**
 * 订单自定义查询Dao
 * Created by chen on 2018/10/12.
 */
public interface OmsOrderDao {
    /**
     * 条件查询订单
     */
    List<OmsOrder> getList(@Param("queryParam") OmsOrderQueryParam queryParam);

    /**
     * 批量发货
     */
    int delivery(@Param("list") List<OmsOrderDeliveryParam> deliveryParamList);

    /**
     * 获取订单详情
     */
    OmsOrderDetail getDetail(@Param("id") Long id);
}
