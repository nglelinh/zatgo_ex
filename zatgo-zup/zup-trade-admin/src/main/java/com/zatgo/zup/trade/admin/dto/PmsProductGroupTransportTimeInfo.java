package com.zatgo.zup.trade.admin.dto;

import com.ykb.mall.common.model.PmsProductGroupTransportTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/19 16:41
 */
@ApiModel("班期交通时间")
public class PmsProductGroupTransportTimeInfo extends PmsProductGroupTransportTime {

    @ApiModelProperty("出发时间（HH:mm格式）")
    private String departureTimeParam;

    @ApiModelProperty("抵达时间（HH:mm格式）")
    private String destinationTimeParam;

    public String getDepartureTimeParam() {
        return departureTimeParam;
    }

    public void setDepartureTimeParam(String departureTimeParam) {
        this.departureTimeParam = departureTimeParam;
    }

    public String getDestinationTimeParam() {
        return destinationTimeParam;
    }

    public void setDestinationTimeParam(String destinationTimeParam) {
        this.destinationTimeParam = destinationTimeParam;
    }
}