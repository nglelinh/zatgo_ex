package com.zatgo.zup.trade.admin.dto;

import com.ykb.mall.common.model.SmsFlashPromotionSession;




/**
 * 包含商品数量的场次信息
 * Created by chen on 2018/11/19.
 */
public class SmsFlashPromotionSessionDetail extends SmsFlashPromotionSession {
    private Integer productCount;

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }
}
