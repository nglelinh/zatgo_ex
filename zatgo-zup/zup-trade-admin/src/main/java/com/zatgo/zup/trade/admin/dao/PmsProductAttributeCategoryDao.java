package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import com.zatgo.zup.trade.admin.dto.PmsProductAttributeCategoryItem;

/**
 * 自定义商品属性分类Dao
 * Created by chen on 2018/5/24.
 */
public interface PmsProductAttributeCategoryDao {
    List<PmsProductAttributeCategoryItem> getListWithAttr();
}
