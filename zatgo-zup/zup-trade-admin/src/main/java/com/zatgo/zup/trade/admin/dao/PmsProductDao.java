package com.zatgo.zup.trade.admin.dao;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.trade.admin.dto.PmsProductResult;


/**
 * 商品自定义Dao
 * Created by chen on 2018/4/26.
 */
public interface PmsProductDao {
    /**
     * 获取商品编辑信息
     */
    PmsProductResult getUpdateInfo(@Param("id") Long id);
}
