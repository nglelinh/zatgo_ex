package com.zatgo.zup.trade.admin.dto;

import com.ykb.mall.common.model.CmsSubject;

public class CmsSubjectData extends CmsSubject {
	
	private Integer sort;

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

}
