package com.zatgo.zup.trade.admin.dto;

import java.util.List;

import com.ykb.mall.common.model.UmsPermission;




/**
 * Created by chen on 2018/9/30.
 */
public class UmsPermissionNode extends UmsPermission {
    private List<UmsPermissionNode> children;

    public List<UmsPermissionNode> getChildren() {
        return children;
    }

    public void setChildren(List<UmsPermissionNode> children) {
        this.children = children;
    }
}
