package com.zatgo.zup.trade.admin.dto;

import com.ykb.mall.common.model.PageParams;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/22 16:41
 */
@ApiModel("班期列表查询")
public class PmsProductGroupListParam extends PageParams {

    @ApiModelProperty("产品分类编号")
    private Long productCategoryId;

    @ApiModelProperty("产品名称")
    private String productName;

    @ApiModelProperty("产品Id")
    private String productId;

    @ApiModelProperty("行程天数")
    private Integer tripDays;

    @ApiModelProperty("班期名称")
    private String groupName;

    @ApiModelProperty("班期状态（0 -待发布，1 - 已上架，2 - 已下架）")
    private Byte groupStatus;

    @ApiModelProperty("班期编码")
    private String groupNumber;

    @ApiModelProperty("创建开始时间")
    private String createStartDate;

    @ApiModelProperty("创建结束时间")
    private String createEndDate;

    @ApiModelProperty("编辑开始时间")
    private String updateStartDate;

    @ApiModelProperty("编辑结束时间")
    private String updateEndDate;

    @ApiModelProperty("出团开始时间")
    private String tripStartDate;

    @ApiModelProperty("开团结束时间")
    private String tripEndDate;
    
    @ApiModelProperty(value = "是否删除", hidden = true)
    private Byte deleteStatus;

    public Long getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getTripDays() {
        return tripDays;
    }

    public void setTripDays(Integer tripDays) {
        this.tripDays = tripDays;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Byte getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(Byte groupStatus) {
        this.groupStatus = groupStatus;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getCreateStartDate() {
        return createStartDate;
    }

    public void setCreateStartDate(String createStartDate) {
        this.createStartDate = createStartDate;
    }

    public String getCreateEndDate() {
        return createEndDate;
    }

    public void setCreateEndDate(String createEndDate) {
        this.createEndDate = createEndDate;
    }

    public String getUpdateStartDate() {
        return updateStartDate;
    }

    public void setUpdateStartDate(String updateStartDate) {
        this.updateStartDate = updateStartDate;
    }

    public String getUpdateEndDate() {
        return updateEndDate;
    }

    public void setUpdateEndDate(String updateEndDate) {
        this.updateEndDate = updateEndDate;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public Byte getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Byte deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}