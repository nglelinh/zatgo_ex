package com.zatgo.zup.trade.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ykb.mall.common.model.OmsOrderSetting;
import com.zatgo.zup.trade.admin.mapper.OmsOrderSettingMapper;
import com.zatgo.zup.trade.admin.service.OmsOrderSettingService;

/**
 * 订单设置管理Service实现类
 * Created by chen on 2018/10/16.
 */
@Service
public class OmsOrderSettingServiceImpl implements OmsOrderSettingService {
    @Autowired
    private OmsOrderSettingMapper orderSettingMapper;

    @Override
    public OmsOrderSetting getItem(Long id) {
        return orderSettingMapper.selectByPrimaryKey(id);
    }

    @Override
    public int update(Long id, OmsOrderSetting orderSetting) {
        orderSetting.setId(id);
        return orderSettingMapper.updateByPrimaryKey(orderSetting);
    }
}
