package com.zatgo.zup.trade.admin.controller;

import com.alibaba.fastjson.JSONObject;
import com.ykb.mall.common.model.SmsHomeRecommendSubject;
import com.zatgo.zup.common.model.SpecialProductListRequest;
import com.zatgo.zup.common.model.SpecialproductListResponse;
import com.zatgo.zup.common.model.UpdateSpecialProductRequest;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.service.SpecialProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 46041 on 2019/7/18.
 */


@Controller
@Api(tags = "SpecialProductController", description = "特殊商品")
@RequestMapping("/trade/admin/special")
public class SpecialProductController {

    @Autowired
    private SpecialProductService specialProductService;


    @ApiOperation("查询")
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    @ResponseBody
    public Object create(@RequestBody SpecialProductListRequest request) {
        JSONObject res = specialProductService.list(request);
        return new CommonResult().success(res);
    }


    @ApiOperation("删除")
    @RequestMapping(value = "/get/del", method = RequestMethod.POST)
    @ResponseBody
    public Object delete(@RequestBody UpdateSpecialProductRequest request) {
        specialProductService.update(request);
        return new CommonResult().success();
    }

    @ApiOperation("新增")
    @RequestMapping(value = "/get/add", method = RequestMethod.POST)
    @ResponseBody
    public Object add(@RequestBody UpdateSpecialProductRequest request) {
        specialProductService.update(request);
        return new CommonResult().success();
    }

}
