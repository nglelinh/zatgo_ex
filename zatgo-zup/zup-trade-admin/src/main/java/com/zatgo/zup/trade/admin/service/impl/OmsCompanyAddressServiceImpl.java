package com.zatgo.zup.trade.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ykb.mall.common.model.OmsCompanyAddress;
import com.ykb.mall.common.model.OmsCompanyAddressExample;
import com.zatgo.zup.trade.admin.mapper.OmsCompanyAddressMapper;
import com.zatgo.zup.trade.admin.service.OmsCompanyAddressService;

/**
 * 收货地址管理Service实现类
 * Created by chen on 2018/10/18.
 */
@Service
public class OmsCompanyAddressServiceImpl implements OmsCompanyAddressService {
    @Autowired
    private OmsCompanyAddressMapper companyAddressMapper;
    @Override
    public List<OmsCompanyAddress> list() {
        return companyAddressMapper.selectByExample(new OmsCompanyAddressExample());
    }
}
