package com.zatgo.zup.trade.admin.service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductRelation;
import com.zatgo.zup.trade.admin.dto.PmsProductParam;
import com.zatgo.zup.trade.admin.dto.PmsProductQueryParam;
import com.zatgo.zup.trade.admin.dto.PmsProductResult;

/**
 * 商品管理Service
 * Created by chen on 2018/4/26.
 */
public interface PmsProductService {
    /**
     * 创建商品
     */
    @Transactional(isolation = Isolation.DEFAULT,propagation = Propagation.REQUIRED)
    int create(PmsProductParam productParam);

    /**
     * 根据商品编号获取更新信息
     */
    PmsProductResult getUpdateInfo(Long id);

    /**
     * 更新商品
     */
    @Transactional
    int update(Long id, PmsProductParam productParam);

    /**
     * 分页查询产品
     */
    List<PmsProduct> list(PmsProductQueryParam productQueryParam, Integer pageSize, Integer pageNum);

    /**
     * 批量修改审核状态
     * @param ids 产品id
     * @param verifyStatus 审核状态
     * @param detail 审核详情
     */
    @Transactional
    int updateVerifyStatus(List<Long> ids, Integer verifyStatus, String detail);

    /**
     * 批量修改商品上架状态
     */
    int updatePublishStatus(List<Long> ids, Integer publishStatus);

    /**
     * 批量修改商品推荐状态
     */
    int updateRecommendStatus(List<Long> ids, Integer recommendStatus);

    /**
     * 批量修改新品状态
     */
    int updateNewStatus(List<Long> ids, Integer newStatus);

    /**
     * 批量删除商品
     */
    int updateDeleteStatus(List<Long> ids, Integer deleteStatus);

    /**
     * 根据商品名称或者货号模糊查询
     */
    List<PmsProduct> list(String keyword, Byte businessType);

    /**
     * 根据商品名称或者货号模糊查询
     */
    List<PmsProduct> listByIds(List<Long> ids);

    
    /**
     * 商品
     * @return
     */
    Map<Byte, List<PmsProduct>> getRelationList();
    
    /**
     * 关联产品
     * @param productId
     * @return
     */
    Map<Byte, List<PmsProductRelation>> getRelationProductList(Long productId, Byte relationType);
    
}
