package com.zatgo.zup.trade.admin.service;

import org.springframework.web.multipart.MultipartFile;

import com.zatgo.zup.trade.admin.dto.CommonResult;

public interface ExcelImportService {
	
	CommonResult excelImportHotel(MultipartFile file);

}
