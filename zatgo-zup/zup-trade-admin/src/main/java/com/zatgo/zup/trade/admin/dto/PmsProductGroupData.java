package com.zatgo.zup.trade.admin.dto;

import java.util.Date;
import java.util.List;

import com.ykb.mall.common.model.PmsProductGroup;
import com.ykb.mall.common.model.PmsProductGroupPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/22 11:03
 */
@ApiModel("班期信息")
public class PmsProductGroupData extends PmsProductGroup {

    @ApiModelProperty("出行时间")
    private Date date;

    @ApiModelProperty("班期价格")
    private List<PmsProductGroupPrice> prices;

    @ApiModelProperty("班期交通")
    private List<PmsProductGroupTransportInfo> transports;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<PmsProductGroupPrice> getPrices() {
        return prices;
    }

    public void setPrices(List<PmsProductGroupPrice> prices) {
        this.prices = prices;
    }

    public List<PmsProductGroupTransportInfo> getTransports() {
        return transports;
    }

    public void setTransports(List<PmsProductGroupTransportInfo> transports) {
        this.transports = transports;
    }
}