package com.zatgo.zup.trade.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.zatgo.zup.common.model.SyncProductParams;
import com.zatgo.zup.common.model.SyncProductRequest;
import com.zatgo.zup.trade.admin.remoteService.ActivityRemoteService;
import com.zatgo.zup.trade.admin.util.HttpUtil;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.model.PmsProduct;
import com.zatgo.zup.trade.admin.dto.PmsProductSyncParam;
import com.zatgo.zup.trade.admin.service.PmsProductSyncService;

/**
 * Created by 46041 on 2019/5/13.
 */

@Service
public class PmsProductSyncServiceImpl implements PmsProductSyncService {

    private static final Logger logger = LoggerFactory.getLogger(PmsProductServiceImpl.class);

    private static final String cloudUserId = "8ce72a140c57480296cd48412677081f";

    @Autowired
    private ActivityRemoteService activityRemoteService;

    @Override
    public Map<Long, Boolean> sync(List<PmsProduct> pmsProducts) {
        Map<String, Object> map = new HashedMap();
        List<SyncProductParams> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(pmsProducts)){
            pmsProducts.forEach(goods -> {
                SyncProductParams product = new SyncProductParams();
                product.setProductId(goods.getId() + "");
                product.setProductName(goods.getName());
                product.setProductMoney(goods.getPrice());
                product.setPromotionPrice(goods.getPromotionPrice());
                product.setProductImgAbbrUrl(goods.getPic());
                list.add(product);
            });
            SyncProductRequest request = new SyncProductRequest();
            request.setCloudUserId(cloudUserId);
            request.setProducts(list);
            activityRemoteService.syncProduct(request);
//            try {
//                HttpUtil.doPost(thirdApi + "/activity/syncProduct", JSONObject.toJSONString(map));
//            } catch (Exception e) {
//                logger.error("", e);
//                throw new BusinessException(e);
//            }
        }
        return null;
    }
}
