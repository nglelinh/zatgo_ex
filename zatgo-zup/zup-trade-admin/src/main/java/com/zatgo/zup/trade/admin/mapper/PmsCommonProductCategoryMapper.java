package com.zatgo.zup.trade.admin.mapper;

import java.util.List;

import com.ykb.mall.common.model.PmsCommonProductCategory;
import com.ykb.mall.common.model.PmsCommonProductCategoryExample;
import org.apache.ibatis.annotations.Param;

public interface PmsCommonProductCategoryMapper {
    int countByExample(PmsCommonProductCategoryExample example);

    int deleteByExample(PmsCommonProductCategoryExample example);

    int deleteByPrimaryKey(String id);

    int insert(PmsCommonProductCategory record);

    int insertSelective(PmsCommonProductCategory record);

    List<PmsCommonProductCategory> selectByExample(PmsCommonProductCategoryExample example);

    PmsCommonProductCategory selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") PmsCommonProductCategory record, @Param("example") PmsCommonProductCategoryExample example);

    int updateByExample(@Param("record") PmsCommonProductCategory record, @Param("example") PmsCommonProductCategoryExample example);

    int updateByPrimaryKeySelective(PmsCommonProductCategory record);

    int updateByPrimaryKey(PmsCommonProductCategory record);
}