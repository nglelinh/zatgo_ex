package com.zatgo.zup.trade.admin.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.github.pagehelper.PageHelper;
import com.ykb.mall.common.enumType.BusinessEnum.BusinessNumType;
import com.ykb.mall.common.enumType.BusinessEnum.TransportType;
import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductExample;
import com.ykb.mall.common.model.PmsProductGroup;
import com.ykb.mall.common.model.PmsProductGroupDate;
import com.ykb.mall.common.model.PmsProductGroupDateExample;
import com.ykb.mall.common.model.PmsProductGroupPrice;
import com.ykb.mall.common.model.PmsProductGroupTransport;
import com.ykb.mall.common.model.PmsProductGroupTransportTime;
import com.ykb.mall.common.utils.DateTimeUtils;
import com.zatgo.zup.trade.admin.mapper.*;
import com.ykb.mall.model.*;
import com.zatgo.zup.trade.admin.dao.PmsProductGroupDao;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupBasicInfo;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupData;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupInfo;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupListParam;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupParam;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupTransportInfo;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupTransportTimeInfo;
import com.zatgo.zup.trade.admin.service.PmsProductGroupService;
import com.zatgo.zup.trade.admin.service.RedisService;

/**
 * 产品班期管理
 *
 * @Author wangyucong
 * @Date 2019/3/19 17:07
 */
@Service
public class PmsProductGroupServiceImpl implements PmsProductGroupService {

//    @Value("${redis.key.prefix.businessNum}")
    private String REDIS_KEY_PREFIX_BUSINESS_NUM = "REDIS_KEY_PREFIX_BUSINESS_NUM";

    @Autowired
    private RedisService redisService;

    @Resource
    private PmsProductGroupDateMapper pmsProductGroupDateMapper;

    @Resource
    private PmsProductMapper pmsProductMapper;

    @Resource
    private PmsProductGroupMapper pmsProductGroupMapper;

    @Resource
    private PmsProductGroupTransportMapper pmsProductGroupTransportMapper;

    @Resource
    private PmsProductGroupTransportTimeMapper pmsProductGroupTransportTimeMapper;

    @Resource
    private PmsProductGroupPriceMapper pmsProductGroupPriceMapper;

    @Resource
    private PmsProductGroupDao pmsProductGroupDao;

    /**
     * 新增产品班期
     *
     * @param param
     * @return
     */
    @Override
    public int createProductGroup(PmsProductGroupParam param) {
        // 校验入参
        checkGroupParam(param);

        PmsProductGroupInfo groupParam = param.getGroupInfo();
        PmsProduct product = pmsProductMapper.selectByPrimaryKey(groupParam.getProductId());
        if (null == product) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "未找到对应的产品");
        }
        // 产品出行时间，天/夜
        if (null == product.getDayNight()) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "产品未设置出行天数");
        }
        String[] dayAndNight = product.getDayNight().split(",");
        Integer days = Integer.valueOf(dayAndNight[0]);
        Integer nights = Integer.valueOf(dayAndNight[1]);

        Calendar calendar = Calendar.getInstance();
        // 遍历此班期所有出行时间
        for (String date : param.getDateList()) {
            PmsProductGroupDate groupDate = null;
            // 出行时间
            Date startTime = DateTimeUtils.parseStringToDate(date, DateTimeUtils.PATTEN_YYYY_MM_DD);
            PmsProductGroupDateExample example = new PmsProductGroupDateExample();
            example.createCriteria().andDateEqualTo(startTime);
            List<PmsProductGroupDate> dates = pmsProductGroupDateMapper.selectByExample(example);
            if (CollectionUtils.isEmpty(dates)) {
                groupDate = new PmsProductGroupDate();
                groupDate.setDate(startTime);
                groupDate.setCreateTime(new Date());
                // 新增班期出行日期
                pmsProductGroupDateMapper.insertSelective(groupDate);
            } else {
                groupDate = dates.get(0);
            }

            PmsProductGroup group = groupParam;
            group.setGroupNumber(generateBusinessNum(BusinessNumType.PRODUCT_GROUP.getCode()));
            group.setDateId(groupDate.getId());
            group.setCreateTime(new Date());
            group.setUpdateTime(new Date());
            getGroupGeneralInfo(group, groupParam, calendar, date, nights);
            // 新增班期
            pmsProductGroupMapper.insertSelective(group);

            List<PmsProductGroupTransportInfo> transportInfoList = groupParam.getTransportInfo();
            if (!CollectionUtils.isEmpty(transportInfoList)) {
                for (PmsProductGroupTransportInfo transportInfo : transportInfoList) {
                    PmsProductGroupTransport transport = transportInfo;
                    transport.setGroupId(group.getId());
                    transport.setCreateTime(new Date());
                    pmsProductGroupTransportMapper.insertSelective(transport);

                    List<PmsProductGroupTransportTimeInfo> timeInfoList = transportInfo.getTimeInfo();
                    if (CollectionUtils.isEmpty(timeInfoList)) {
                        throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入接送时间");
                    }
                    for (PmsProductGroupTransportTimeInfo timeInfo : timeInfoList) {
                        PmsProductGroupTransportTime time = new PmsProductGroupTransportTime();
                        time.setTransportId(transport.getId());
                        time.setCreateTime(new Date());
                        getGroupTimeGeneralInfo(time, timeInfo, transport, calendar, date, nights);
                        pmsProductGroupTransportTimeMapper.insertSelective(time);
                    }
                }
            }
            List<PmsProductGroupPrice> priceList = groupParam.getPriceInfo();
            if (CollectionUtils.isEmpty(priceList)) {
                throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入班期具体价格");
            }
            int stock = 0;
            for (PmsProductGroupPrice price : priceList) {
                Integer maxPeople = price.getMaxPeople();
                stock += maxPeople == null ? 0 : maxPeople.intValue();
                price.setGroupId(group.getId());
                price.setCreateTime(new Date());
                pmsProductGroupPriceMapper.insertSelective(price);
            }
            group.setStock(stock);
            pmsProductGroupMapper.updateByPrimaryKeySelective(group);
        }
        return 1;
    }

    /**
     * 更新产品班期
     *
     * @param param
     * @return
     */
    @Override
    public int updateProductGroup(PmsProductGroupParam param) {
        PmsProductGroupInfo groupParam = param.getGroupInfo();
        PmsProduct product = pmsProductMapper.selectByPrimaryKey(groupParam.getProductId());
        if (null == product) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "未找到对应的产品");
        }
        // 产品出行时间，天/夜
        if (null == product.getDayNight()) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "产品未设置出行天数");
        }
        String[] dayAndNight = product.getDayNight().split(",");
        Integer days = Integer.valueOf(dayAndNight[0]);
        Integer nights = Integer.valueOf(dayAndNight[1]);

        Calendar calendar = Calendar.getInstance();
        String date = param.getDateList().get(0);
        PmsProductGroupDate groupDate = null;
        // 出行时间
        Date startTime = DateTimeUtils.parseStringToDate(date, DateTimeUtils.PATTEN_YYYY_MM_DD);
        PmsProductGroupDateExample example = new PmsProductGroupDateExample();
        example.createCriteria().andDateEqualTo(startTime);
        List<PmsProductGroupDate> dates = pmsProductGroupDateMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(dates)) {
            groupDate = new PmsProductGroupDate();
            groupDate.setDate(startTime);
            groupDate.setCreateTime(new Date());
            // 新增班期出行日期
            pmsProductGroupDateMapper.insertSelective(groupDate);
        } else {
            groupDate = dates.get(0);
        }
        PmsProductGroup group = groupParam;
        group.setDateId(groupDate.getId());
        group.setUpdateTime(new Date());
        getGroupGeneralInfo(group, groupParam, calendar, date, nights);
        // 更新班期
        pmsProductGroupMapper.updateByPrimaryKeySelective(group);

        List<PmsProductGroupTransportInfo> transportInfoList = groupParam.getTransportInfo();
        if (!CollectionUtils.isEmpty(transportInfoList)) {
            for (PmsProductGroupTransportInfo transportInfo : transportInfoList) {
                PmsProductGroupTransport transport = transportInfo;
                if (null != transport.getId()) {
                    pmsProductGroupTransportMapper.updateByPrimaryKeySelective(transport);
                } else {
                    transport.setGroupId(group.getId());
                    transport.setCreateTime(new Date());
                    pmsProductGroupTransportMapper.insertSelective(transport);
                }

                List<PmsProductGroupTransportTimeInfo> timeInfoList = transportInfo.getTimeInfo();
                if (CollectionUtils.isEmpty(timeInfoList)) {
                    throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入接送时间");
                }
                for (PmsProductGroupTransportTimeInfo timeInfo : timeInfoList) {
                    PmsProductGroupTransportTime time = timeInfo;
                    getGroupTimeGeneralInfo(time, timeInfo, transport, calendar, date, nights);
                    if (null != time.getId()) {
                        pmsProductGroupTransportTimeMapper.updateByPrimaryKeySelective(time);
                    } else {
                        time.setTransportId(transport.getId());
                        time.setCreateTime(new Date());
                        pmsProductGroupTransportTimeMapper.insertSelective(time);
                    }
                }
            }
        }
        List<PmsProductGroupPrice> priceList = groupParam.getPriceInfo();
        if (CollectionUtils.isEmpty(priceList)) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入班期具体价格");
        }
        int stock = 0;
        for (PmsProductGroupPrice price : priceList) {
            Integer maxPeople = price.getMaxPeople();
            stock += maxPeople == null ? 0 : maxPeople.intValue();
            if (null != price.getId()) {
                pmsProductGroupPriceMapper.updateByPrimaryKeySelective(price);
            } else {
                price.setGroupId(group.getId());
                price.setCreateTime(new Date());
                pmsProductGroupPriceMapper.insertSelective(price);
            }
        }
        group.setStock(stock);
        // 更新班期
        pmsProductGroupMapper.updateByPrimaryKeySelective(group);
        return 1;
    }

    /**
     * 查询班期详情
     *
     * @param groupId
     * @return
     */
    @Override
    public PmsProductGroupData selectGroupDetail(Long groupId) {
        return pmsProductGroupDao.selectGroupDetail(groupId);
    }

    /**
     * 查询班期列表
     *
     * @param param
     * @return
     */
    @Override
    public List<PmsProductGroupBasicInfo> selectGroupList(PmsProductGroupListParam param) {
    	PageHelper.startPage(param.getPageNum(), param.getPageSize());
    	param.setDeleteStatus(YesOrNo.NO.getCode());
        return pmsProductGroupDao.selectGroupList(param);
    }

    /**
     * 查询产品的班期日期
     *
     * @param productId
     * @return
     */
    @Override
    public List<PmsProductGroupDate> selectProductDateList(Long productId) {
        return pmsProductGroupDao.selectProductDateList(productId);
    }

    /**
     * 删除班期
     *
     * @param groupId
     * @return
     */
    @Override
    public int deleteProductGroup(Long groupId) {
        return pmsProductGroupDao.deleteProductGroup(groupId);
    }

    /**
     * 更新班期库存
     *
     * @param param
     * @return
     */
    @Override
    public int updateGroupStock(PmsProductGroupInfo param) {
        PmsProductGroup group = new PmsProductGroupBasicInfo();
        group.setId(param.getId());
        group.setStock(param.getStock());
        group.setStockLimit(param.getStockLimit());
        return pmsProductGroupMapper.updateByPrimaryKeySelective(group);
    }

    /**
     * 拼装班期交通时间
     *
     * @param time
     * @param timeInfo
     * @param transport
     * @param calendar
     * @param date
     * @param nights
     */
    private void getGroupTimeGeneralInfo(PmsProductGroupTransportTime time, PmsProductGroupTransportTimeInfo timeInfo, PmsProductGroupTransport transport,
                                         Calendar calendar, String date, Integer nights) {
        Date departureTime = null;
        Date destinationTime = null;
        if (TransportType.PICK.getCode().equals(transport.getType())) {
            departureTime = DateTimeUtils.parseStringToDate(date + " " + timeInfo.getDepartureTimeParam(), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM);
            destinationTime = DateTimeUtils.parseStringToDate(date + " " + timeInfo.getDestinationTimeParam(), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM);
        } else if (TransportType.DELIVER.getCode().equals(transport.getType())) {
            Date departureTimeTemp = DateTimeUtils.parseStringToDate(date + " " + timeInfo.getDepartureTimeParam(), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM);
            Date destinationTimeTemp = DateTimeUtils.parseStringToDate(date + " " + timeInfo.getDestinationTimeParam(), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM);
            calendar.setTime(departureTimeTemp);
            calendar.add(Calendar.DATE, nights);
            departureTime = calendar.getTime();
            calendar.setTime(destinationTimeTemp);
            calendar.add(Calendar.DATE, nights);
            destinationTime = calendar.getTime();
        }
        time.setDepartureTime(departureTime);
        time.setDestinationTime(destinationTime);
    }

    /**
     * 组装班期信息
     *
     * @param group
     * @param groupParam
     * @param calendar
     * @param date
     * @param nights
     */
    private void getGroupGeneralInfo(PmsProductGroup group, PmsProductGroupInfo groupParam, Calendar calendar, String date, Integer nights) {
        // 计算报名截止时间
        Date tempTime = DateTimeUtils.parseStringToDate(date + " " + groupParam.getEnrollEndTimeParam(), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM);
        if (YesOrNo.YES.getCode().equals(groupParam.getEnrollInWeekend())) {
            calendar.setTime(tempTime);
            calendar.add(Calendar.DATE, 0 - groupParam.getEnrollEndDay());
            group.setEnrollEndTime(calendar.getTime());
        } else {
            group.setEnrollEndTime(getEnrollEndTime(calendar, tempTime, groupParam.getEnrollEndDay()));
        }
        // 计算集合时间
        Date collectionTime = DateTimeUtils.parseStringToDate(date + " " + groupParam.getCollectionTimeParam(), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM);
        group.setCollectionTime(collectionTime);
        // 计算行程结束时间
        Date tempTripTime = DateTimeUtils.parseStringToDate(date + " " + groupParam.getTripEndTimeParam(), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM);
        calendar.setTime(tempTripTime);
        calendar.add(Calendar.DATE, nights);
        group.setTripEndTime(calendar.getTime());
    }

    /**
     * 跳过周末获取报名截止时间
     * @param calendar
     * @param date
     * @param num
     * @return
     */
    private Date getEnrollEndTime(Calendar calendar, Date date, Integer num){
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);
        num--;
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        date = calendar.getTime();
        if (0 < num || 1 == dayOfWeek || 7 == dayOfWeek) {
            // 若是周末，则天数无效
            if (1 == dayOfWeek || 7 == dayOfWeek) {
                num++;
            }
            return getEnrollEndTime(calendar, date, num);
        }
        return date;
    }

    /**
     * 校验新增班期入参
     *
     * @param param
     */
    private void checkGroupParam(PmsProductGroupParam param) {
        if (CollectionUtils.isEmpty(param.getDateList())) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请选择班期出行日期");
        }
        if (StringUtils.isEmpty(param.getGroupInfo().getName())) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入班期名称");
        }
        if (null == param.getGroupInfo().getMinPeople()) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入最小参团人数");
        }
        if (null == param.getGroupInfo().getEnrollEndDay()) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入报名截止天数");
        }
        if (StringUtils.isEmpty(param.getGroupInfo().getEnrollEndTimeParam())) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入报名截止时间");
        }
        if (StringUtils.isEmpty(param.getGroupInfo().getTransportUrgentTel())) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "请输入接送紧急联络电话");
        }
    }

    /**
     * 查询可选产品列表
     *
     * @return
     */
    @Override
    public List<PmsProduct> selectProductList(Byte businessType) {
        PmsProductExample example = new PmsProductExample();
        PmsProductExample.Criteria criteria = example.createCriteria();
        criteria.andDeleteStatusEqualTo(0);
        criteria.andPublishStatusEqualTo(1);
        criteria.andBusinessTypeEqualTo(businessType);
        return pmsProductMapper.selectByExample(example);
    }

    /**
     * 生成18位业务编码:8位日期+2位业务类型+4位随机数+6位以上自增id
     */
    public String generateBusinessNum(Byte businessType) {
        if (null == businessType) {
            throw new BusinessException(BusinessExceptionCode.CREATE_GROUP_ERROR, "生成业务编码失败");
        }
        StringBuilder sb = new StringBuilder();
        String date = DateTimeUtils.parseDateToString(new Date(), DateTimeUtils.PATTEN_YYYYMMDD);
        String key = REDIS_KEY_PREFIX_BUSINESS_NUM + date;
        Long increment = redisService.increment(key, 1);
        sb.append(date);
        sb.append(String.format("%02d", businessType));
        sb.append(Math.round(Math.random() * 1000));
        String incrementStr = increment.toString();
        if (incrementStr.length() <= 6) {
            sb.append(String.format("%06d", increment));
        } else {
            sb.append(incrementStr);
        }
        return sb.toString();
    }

    @Override
    public int deleteProductGroupList(String ids) {
        int i = 0;
        if (!StringUtils.isEmpty(ids)){
            i = pmsProductGroupDao.deleteProductGroupList(ids);
        }
        return i;
    }
}