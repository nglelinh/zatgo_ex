package com.zatgo.zup.trade.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ykb.mall.common.model.CmsPrefrenceArea;
import com.ykb.mall.common.model.CmsPrefrenceAreaExample;
import com.zatgo.zup.trade.admin.mapper.CmsPrefrenceAreaMapper;
import com.zatgo.zup.trade.admin.service.CmsPrefrenceAreaService;

/**
 * 商品优选Service实现类
 * Created by chen on 2018/6/1.
 */
@Service
public class CmsPrefrenceAreaServiceImpl implements CmsPrefrenceAreaService {
    @Autowired
    private CmsPrefrenceAreaMapper prefrenceAreaMapper;

    @Override
    public List<CmsPrefrenceArea> listAll() {
        return prefrenceAreaMapper.selectByExample(new CmsPrefrenceAreaExample());
    }
}
