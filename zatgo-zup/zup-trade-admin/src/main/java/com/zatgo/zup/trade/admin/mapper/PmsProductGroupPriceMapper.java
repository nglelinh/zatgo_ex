package com.zatgo.zup.trade.admin.mapper;

import com.ykb.mall.common.model.PmsProductGroupPrice;
import com.ykb.mall.common.model.PmsProductGroupPriceExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductGroupPriceMapper {
    int countByExample(PmsProductGroupPriceExample example);

    int deleteByExample(PmsProductGroupPriceExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductGroupPrice record);

    int insertSelective(PmsProductGroupPrice record);

    List<PmsProductGroupPrice> selectByExample(PmsProductGroupPriceExample example);

    PmsProductGroupPrice selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductGroupPrice record, @Param("example") PmsProductGroupPriceExample example);

    int updateByExample(@Param("record") PmsProductGroupPrice record, @Param("example") PmsProductGroupPriceExample example);

    int updateByPrimaryKeySelective(PmsProductGroupPrice record);

    int updateByPrimaryKey(PmsProductGroupPrice record);

    int updateStock(@Param("id")Long id, @Param("num") Integer num);
}