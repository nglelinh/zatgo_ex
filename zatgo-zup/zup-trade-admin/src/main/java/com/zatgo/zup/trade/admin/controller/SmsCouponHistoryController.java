package com.zatgo.zup.trade.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ykb.mall.common.model.SmsCouponHistory;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.service.SmsCouponHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 优惠券领取记录管理Controller
 * Created by chen on 2018/11/6.
 */
@Controller
@Api(tags = "SmsCouponHistoryController",description = "优惠券领取记录管理")
@RequestMapping("/trade/admin/couponHistory")
public class SmsCouponHistoryController {
    @Autowired
    private SmsCouponHistoryService historyService;
    @ApiOperation("根据优惠券id，使用状态，订单编号分页获取领取记录")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ResponseBody
    public Object list(@RequestParam(value = "couponId",required = false) Long couponId,
                       @RequestParam(value = "useStatus",required = false) Integer useStatus,
                       @RequestParam(value = "orderSn",required = false) String orderSn,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum){
        List<SmsCouponHistory> historyList = historyService.list(couponId,useStatus,orderSn,pageSize,pageNum);
        return new CommonResult().pageSuccess(historyList);
    }
}
