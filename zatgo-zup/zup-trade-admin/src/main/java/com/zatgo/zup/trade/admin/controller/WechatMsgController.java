package com.zatgo.zup.trade.admin.controller;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.remoteService.UserRemoteService;
import com.zatgo.zup.trade.admin.remoteService.WechatRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trade/admin/wx/msg")
public class WechatMsgController {

	@Autowired
	private UserRemoteService userRemoteService;

	@Autowired
	private WechatRemoteService wechatRemoteService;

	@GetMapping("/support/{userId}")
	public CommonResult sendWechatSupportMsg(@PathVariable Long userId) {
		ResponseData<UserData> userById = userRemoteService.getUserById(userId + "");
		if(userById == null || !userById.isSuccessful())
			throw new BusinessException();
		return wechatRemoteService.sendWechatSupportMsg(userById.getData().getWechatOpenid());
	}

	@GetMapping("/support/success/{userId}/{id}")
	public CommonResult sendWechatSupportSuccessMsg(@PathVariable Long userId, @PathVariable String id) {
		ResponseData<UserData> userById = userRemoteService.getUserById(userId + "");
		if(userById == null || !userById.isSuccessful())
			throw new BusinessException();
		return wechatRemoteService.sendWechatSupportSuccessMsg(userById.getData().getWechatOpenid(), id);
	}

}
