package com.zatgo.zup.trade.admin.service;

import java.util.List;
import java.util.Map;

import com.ykb.mall.common.model.PmsProduct;

/**
 * Created by 46041 on 2019/5/13.
 */
public interface PmsProductSyncService {


    Map<Long,Boolean> sync(List<PmsProduct> pmsProducts);
}
