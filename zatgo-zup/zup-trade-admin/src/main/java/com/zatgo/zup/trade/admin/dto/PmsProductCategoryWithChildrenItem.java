package com.zatgo.zup.trade.admin.dto;

import java.util.List;

import com.ykb.mall.common.model.PmsProductCategory;

/**
 * Created by chen on 2018/5/25.
 */
public class PmsProductCategoryWithChildrenItem extends PmsProductCategory {
    private List<PmsProductCategory> children;

    public List<PmsProductCategory> getChildren() {
        return children;
    }

    public void setChildren(List<PmsProductCategory> children) {
        this.children = children;
    }
}
