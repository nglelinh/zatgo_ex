package com.zatgo.zup.trade.admin.dto;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/7 18:17
 */
@ApiModel("订单退款金额")
public class OmsRefundInfoParam {

    @ApiModelProperty(value = "订单编号", required = true)
    private Long orderId;

    @ApiModelProperty(value = "退款金额", required = true)
    private BigDecimal refundPrice;

    @ApiModelProperty(value = "备注")
    private String note;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(BigDecimal refundPrice) {
        this.refundPrice = refundPrice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}