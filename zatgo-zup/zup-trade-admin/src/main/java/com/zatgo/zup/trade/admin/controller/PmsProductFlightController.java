package com.zatgo.zup.trade.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsProductFlightParams;
import com.zatgo.zup.trade.admin.dto.PmsProductFlightPriceParams;
import com.zatgo.zup.trade.admin.service.PmsProductFlightService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "PmsProductFlightController", description = "机票管理")
@RequestMapping("/trade/admin/product/flight")
public class PmsProductFlightController {

	@Autowired
	private PmsProductFlightService pmsProductFlightService;

	@ApiOperation("创建机票")
	@PostMapping(value = "/create")
	public CommonResult create(@RequestBody PmsProductFlightParams params) {
		return pmsProductFlightService.create(params);
	}

	@ApiOperation("机票详情")
	@PostMapping(value = "/details/{id}")
	public CommonResult details(@PathVariable Long id) {
		return pmsProductFlightService.details(id);
	}

	@ApiOperation("更新机票")
	@PostMapping(value = "/update")
	public CommonResult update(@RequestBody PmsProductFlightParams params) {
		return pmsProductFlightService.update(params);
	}

	@ApiOperation("机票价格")
	@GetMapping(value = "/price/list/{flightId}")
	public CommonResult flightPriceList(@PathVariable Long flightId) {
		return pmsProductFlightService.flightPriceList(flightId);
	}

	@ApiOperation("创建机票价格")
	@PostMapping(value = "/create/price/{flightId}")
	public CommonResult createPrice(@PathVariable Long flightId,
			@RequestBody List<PmsProductFlightPriceParams> params) {
		return pmsProductFlightService.createFlightPrice(flightId, params);
	}

	@ApiOperation("修改机票价格")
	@PostMapping(value = "/update/price")
	public CommonResult createPrice(@RequestBody List<PmsProductFlightPriceParams> params) {
		return pmsProductFlightService.updateFlightPrice(params);
	}

}
