package com.zatgo.zup.trade.admin.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.github.pagehelper.PageHelper;
import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.CmsSubject;
import com.ykb.mall.common.model.CmsSubjectExample;
import com.ykb.mall.common.model.CmsSubjectProductRelationExample;
import com.ykb.mall.common.model.SmsHomeRecommendSubject;
import com.ykb.mall.common.model.SmsHomeRecommendSubjectExample;
import com.zatgo.zup.trade.admin.mapper.CmsSubjectMapper;
import com.zatgo.zup.trade.admin.mapper.CmsSubjectProductRelationMapper;
import com.zatgo.zup.trade.admin.mapper.SmsHomeRecommendSubjectMapper;
import com.zatgo.zup.trade.admin.dao.CmsSubjectDao;
import com.zatgo.zup.trade.admin.dto.CmsSubjectData;
import com.zatgo.zup.trade.admin.dto.CmsSubjectParams;
import com.zatgo.zup.trade.admin.service.CmsSubjectService;

/**
 * 商品专题Service实现类
 * Created by chen on 2018/6/1.
 */
@Service
public class CmsSubjectServiceImpl implements CmsSubjectService {
    @Autowired
    private CmsSubjectMapper subjectMapper;
    @Autowired
    private CmsSubjectProductRelationMapper cmsSubjectProductRelationMapper;
    @Autowired
    private SmsHomeRecommendSubjectMapper smsHomeRecommendSubjectMapper;
    @Autowired
    private CmsSubjectDao cmsSubjectDao;

    @Override
    public List<CmsSubject> listAll() {
        return subjectMapper.selectByExample(new CmsSubjectExample());
    }

    @Override
    public List<CmsSubjectData> list(String keyword, Integer pageNum, Integer pageSize, Byte businessType) {
        PageHelper.startPage(pageNum, pageSize);
        return cmsSubjectDao.selectCmsSubjectList(keyword, businessType);
    }

    @Transactional
	@Override
	public void create(CmsSubjectParams params) {
    	
    	String title = params.getTitle();
    	if(StringUtils.isEmpty(title)) {
    		throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "必须填写专题标题");
    	}
    	
		if (params.getShowStatus() == null) {
			params.setShowStatus(YesOrNo.NO.getCode().intValue());
		}
		if (params.getRecommendStatus() == null) {
			params.setRecommendStatus(YesOrNo.NO.getCode().intValue());
		}

		CmsSubject subject = new CmsSubject();
		BeanUtils.copyProperties(params, subject);
		subject.setCreateTime(new Date());
		subjectMapper.insertSelective(subject);

		if (null != params.getRecommendStatus() && YesOrNo.YES.getCode().intValue() == params.getRecommendStatus()) {
			SmsHomeRecommendSubject recommendSubject = new SmsHomeRecommendSubject();
			recommendSubject.setRecommendStatus(YesOrNo.YES.getCode().intValue());
			recommendSubject.setSort(params.getSort() == null ? 0 : params.getSort());
			recommendSubject.setSubjectId(subject.getId());
			recommendSubject.setSubjectName(params.getTitle());
			smsHomeRecommendSubjectMapper.insertSelective(recommendSubject);
		}
	}

    @Transactional
	@Override
	public void update(CmsSubjectParams params) {
		if (params.getId() == null) {
			return;
		}

		CmsSubject subject = new CmsSubject();
		BeanUtils.copyProperties(params, subject);

		CmsSubjectExample example = new CmsSubjectExample();
		example.createCriteria().andIdEqualTo(params.getId());

		subjectMapper.updateByExampleSelective(subject, example);

		if (null != params.getRecommendStatus()) {
			SmsHomeRecommendSubjectExample subjectExample = new SmsHomeRecommendSubjectExample();
			subjectExample.createCriteria().andSubjectIdEqualTo(params.getId());
			if (YesOrNo.NO.getCode().intValue() == params.getRecommendStatus()) {
				smsHomeRecommendSubjectMapper.deleteByExample(subjectExample);
			} else if (YesOrNo.YES.getCode().intValue() == params.getRecommendStatus()) {
				List<SmsHomeRecommendSubject> subjects = smsHomeRecommendSubjectMapper.selectByExample(subjectExample);
				if (CollectionUtils.isEmpty(subjects)) {
					SmsHomeRecommendSubject recommendSubject = new SmsHomeRecommendSubject();
					recommendSubject.setRecommendStatus(YesOrNo.YES.getCode().intValue());
					recommendSubject.setSort(params.getSort() == null ? 0 : params.getSort());
					recommendSubject.setSubjectId(params.getId());
					recommendSubject.setSubjectName(params.getTitle());
					smsHomeRecommendSubjectMapper.insertSelective(recommendSubject);
				}else {
					SmsHomeRecommendSubject recommendSubject = subjects.get(0);
					recommendSubject.setRecommendStatus(YesOrNo.YES.getCode().intValue());
					recommendSubject.setSort(params.getSort() == null ? 0 : params.getSort());
					recommendSubject.setSubjectId(params.getId());
					recommendSubject.setSubjectName(params.getTitle());
					smsHomeRecommendSubjectMapper.updateByPrimaryKeySelective(recommendSubject);
				}
			}
		}
	}

	@Transactional
	@Override
	public void delete(Long id) {
		CmsSubjectProductRelationExample relationExample = new CmsSubjectProductRelationExample();
		relationExample.createCriteria().andSubjectIdEqualTo(id);
		
		SmsHomeRecommendSubjectExample subjectExample = new SmsHomeRecommendSubjectExample();
		subjectExample.createCriteria().andSubjectIdEqualTo(id);
		smsHomeRecommendSubjectMapper.deleteByExample(subjectExample);
		
		cmsSubjectProductRelationMapper.deleteByExample(relationExample);
		subjectMapper.deleteByPrimaryKey(id);
		
	}
}
