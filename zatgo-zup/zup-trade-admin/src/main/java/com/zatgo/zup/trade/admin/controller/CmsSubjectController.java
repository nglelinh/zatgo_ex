package com.zatgo.zup.trade.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ykb.mall.common.model.CmsSubject;
import com.zatgo.zup.trade.admin.dto.CmsSubjectData;
import com.zatgo.zup.trade.admin.dto.CmsSubjectParams;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.service.CmsSubjectService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 商品专题Controller
 * Created by chen on 2018/6/1.
 */
@Controller
@Api(tags = "CmsSubjectController", description = "商品专题管理")
//@RequestMapping("/ykbAdminBase/subject")
@RequestMapping("/trade/admin/subject")
public class CmsSubjectController {
    @Autowired
    private CmsSubjectService subjectService;

    @ApiOperation("获取全部商品专题")
    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    @ResponseBody
    public Object listAll() {
        List<CmsSubject> subjectList = subjectService.listAll();
        return new CommonResult().success(subjectList);
    }

    @ApiOperation(value = "根据专题名称分页获取专题")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object getList(@RequestParam(value = "keyword", required = false) String keyword,
                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                          @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                          @RequestParam(value = "businessType", required = false) Byte businessType) {
        List<CmsSubjectData> subjectList = subjectService.list(keyword, pageNum, pageSize, businessType);
        return new CommonResult().pageSuccess(subjectList);
    }
    
    @ApiOperation(value = "创建专题")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public Object create(@RequestBody CmsSubjectParams params) {
        subjectService.create(params);
        return new CommonResult().success();
    }
    
    @ApiOperation(value = "更新专题")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public Object update(@RequestBody CmsSubjectParams params) {
        subjectService.update(params);
        return new CommonResult().success();
    }
    
    @ApiOperation(value = "删除专题")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object delete(@PathVariable Long id) {
        subjectService.delete(id);
        return new CommonResult().success();
    }
}
