package com.zatgo.zup.trade.admin.service;

import java.util.List;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsProductFlightParams;
import com.zatgo.zup.trade.admin.dto.PmsProductFlightPriceParams;

public interface PmsProductFlightService {

	/**
	 * 创建机票价格
	 * @param params
	 * @return
	 */
	CommonResult create(PmsProductFlightParams params);
	
	/**
	 * 机票详情
	 * @param id
	 * @return
	 */
	CommonResult details(Long id);
	
	/**
	 * 更新机票
	 * @param params
	 * @return
	 */
	CommonResult update(PmsProductFlightParams params);
	
	/**
	 * 删除机票班期
	 * @param flightId
	 */
	int deleteProductFlight(Long flightId);
	
	/**
	 * 机票价格
	 * @param flightId
	 * @return
	 */
	CommonResult flightPriceList(Long flightId);
	
	/**
	 * 创建机票价格
	 * @param flightId
	 * @return
	 */
	CommonResult createFlightPrice(Long flightId, List<PmsProductFlightPriceParams> params);
	
	/**
	 * 修改机票价格
	 * @param params
	 * @return
	 */
	CommonResult updateFlightPrice(List<PmsProductFlightPriceParams> params);
	
}
