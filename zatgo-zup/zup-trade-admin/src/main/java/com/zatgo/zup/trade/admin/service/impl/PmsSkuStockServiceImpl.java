package com.zatgo.zup.trade.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ykb.mall.common.model.PmsSkuStock;
import com.ykb.mall.common.model.PmsSkuStockExample;
import com.zatgo.zup.trade.admin.mapper.PmsSkuStockMapper;
import com.zatgo.zup.trade.admin.dao.PmsSkuStockDao;
import com.zatgo.zup.trade.admin.service.PmsSkuStockService;

/**
 * 商品sku库存管理Service实现类
 * Created by chen on 2018/4/27.
 */
@Service
public class PmsSkuStockServiceImpl implements PmsSkuStockService {
    @Autowired
    private PmsSkuStockMapper skuStockMapper;
    @Autowired
    private PmsSkuStockDao skuStockDao;

    @Override
    public List<PmsSkuStock> getList(Long pid, String keyword) {
        PmsSkuStockExample example = new PmsSkuStockExample();
        PmsSkuStockExample.Criteria criteria = example.createCriteria().andProductIdEqualTo(pid);
        if (!StringUtils.isEmpty(keyword)) {
            criteria.andSkuCodeLike("%" + keyword + "%");
        }
        return skuStockMapper.selectByExample(example);
    }

    @Override
    public int update(Long pid, List<PmsSkuStock> skuStockList) {
        return skuStockDao.replaceList(skuStockList);
    }
}
