package com.zatgo.zup.trade.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/7 17:09
 */
@ApiModel("订单材料办理完成")
public class OmsHandleFinishParam {

    @ApiModelProperty(value = "订单编号", required = true)
    private Long orderId;

    @ApiModelProperty(value = "备注")
    private String note;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}