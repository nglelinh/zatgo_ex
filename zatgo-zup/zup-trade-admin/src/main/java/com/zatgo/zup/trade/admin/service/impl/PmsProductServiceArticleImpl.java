package com.zatgo.zup.trade.admin.service.impl;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.ykb.mall.common.enumType.BusinessEnum.BusinessType;
import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.ykb.mall.common.model.CmsPrefrenceAreaProductRelationExample;
import com.ykb.mall.common.model.CmsSubjectProductRelationExample;
import com.ykb.mall.common.model.PmsMemberPrice;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductArticle;
import com.ykb.mall.common.model.PmsProductArticleExample;
import com.ykb.mall.common.model.PmsProductAttributeValue;
import com.ykb.mall.common.model.PmsProductAttributeValueExample;
import com.ykb.mall.common.model.PmsProductFullReduction;
import com.ykb.mall.common.model.PmsProductLadder;
import com.ykb.mall.common.model.PmsProductRelation;
import com.ykb.mall.common.model.PmsProductRelationExample;
import com.ykb.mall.common.model.PmsSkuStock;
import com.zatgo.zup.trade.admin.mapper.*;
import com.ykb.mall.model.*;
import com.zatgo.zup.trade.admin.dao.CmsPrefrenceAreaProductRelationDao;
import com.zatgo.zup.trade.admin.dao.CmsSubjectProductRelationDao;
import com.zatgo.zup.trade.admin.dao.PmsMemberPriceDao;
import com.zatgo.zup.trade.admin.dao.PmsProductArticleDao;
import com.zatgo.zup.trade.admin.dao.PmsProductAttributeValueDao;
import com.zatgo.zup.trade.admin.dao.PmsProductFullReductionDao;
import com.zatgo.zup.trade.admin.dao.PmsProductLadderDao;
import com.zatgo.zup.trade.admin.dao.PmsSkuStockDao;
import com.zatgo.zup.trade.admin.dto.PmsProductArticleParam;
import com.zatgo.zup.trade.admin.dto.PmsProductArticleResult;
import com.zatgo.zup.trade.admin.dto.PmsProductParam;
import com.zatgo.zup.trade.admin.service.PmsProductArticleService;

/**
 * 商品管理Service实现类
 * Created by chen on 2018/4/26.
 */
@Service
public class PmsProductServiceArticleImpl implements PmsProductArticleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PmsProductServiceArticleImpl.class);
    @Autowired
    private PmsProductMapper productMapper;
    @Autowired
    private PmsMemberPriceDao memberPriceDao;
    @Autowired
    private PmsProductLadderDao productLadderDao;
    @Autowired
    private PmsProductFullReductionDao productFullReductionDao;
    @Autowired
    private PmsSkuStockDao skuStockDao;
    @Autowired
    private PmsProductAttributeValueDao productAttributeValueDao;
    @Autowired
    private PmsProductAttributeValueMapper productAttributeValueMapper;
    @Autowired
    private CmsSubjectProductRelationDao subjectProductRelationDao;
    @Autowired
    private CmsSubjectProductRelationMapper subjectProductRelationMapper;
    @Autowired
    private CmsPrefrenceAreaProductRelationDao prefrenceAreaProductRelationDao;
    @Autowired
    private CmsPrefrenceAreaProductRelationMapper prefrenceAreaProductRelationMapper;
    @Autowired
    private PmsProductArticleMapper pmsProductArticleMapper;
    @Autowired
    private PmsProductArticleDao articleDao;
    @Autowired
    private PmsProductRelationMapper pmsProductRelationMapper;

    @Override
    public int create(PmsProductArticleParam articleParam) {
        int count;
        articleParam.setBusinessType(BusinessType.ARTICLE.getCode());
        //创建商品
        PmsProduct product = new PmsProduct();
        BeanUtils.copyProperties(articleParam, product);
        product.setProductSn("");
        product.setDeleteStatus(YesOrNo.NO.getCode().intValue());
        if(product.getNewStatus() == null) {
        	product.setNewStatus(YesOrNo.NO.getCode().intValue());
        }
        if(product.getRecommandStatus() == null) {
        	product.setRecommandStatus(YesOrNo.NO.getCode().intValue());
        }
        
        productMapper.insertSelective(product);
        //根据促销类型设置价格：、阶梯价格、满减价格
        Long productId = product.getId();
        
        PmsProductArticle article = new PmsProductArticle();
        BeanUtils.copyProperties(articleParam, article);
        article.setProductId(productId);
        article.setUpdateTime(new Date());
        pmsProductArticleMapper.insertSelective(article);
        
        //添加商品参数,添加自定义商品规格
        relateAndInsertList(productAttributeValueDao, articleParam.getProductAttributeValueList(), productId);
        //关联专题
        relateAndInsertList(subjectProductRelationDao, articleParam.getSubjectProductRelationList(), productId);
        //关联优选
        relateAndInsertList(prefrenceAreaProductRelationDao, articleParam.getPrefrenceAreaProductRelationList(), productId);
        
        List<PmsProductRelation> pmsProductRelationList = articleParam.getPmsProductRelationList();
        if(!CollectionUtils.isEmpty(pmsProductRelationList)) {
        	for (PmsProductRelation relation : pmsProductRelationList) {
        		relation.setProductId(productId);
        		relation.setRelationType(relation.getRelationType());
        		pmsProductRelationMapper.insertSelective(relation);
			}
        }
        count = 1;
        return count;
    }

    /*private void handleSkuStockCode(List<PmsSkuStock> skuStockList, Long productId) {
        if(CollectionUtils.isEmpty(skuStockList))return;
        for(int i=0;i<skuStockList.size();i++){
            PmsSkuStock skuStock = skuStockList.get(i);
            if(StringUtils.isEmpty(skuStock.getSkuCode())){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                StringBuilder sb = new StringBuilder();
                //日期
                sb.append(sdf.format(new Date()));
                //四位商品id
                sb.append(String.format("%04d", productId));
                //三位索引id
                sb.append(String.format("%03d", i+1));
                skuStock.setSkuCode(sb.toString());
            }
        }
    }*/

    @Override
    public PmsProductArticleResult getUpdateInfo(Long id) {
        return articleDao.getUpdateInfo(id);
    }

    @Override
    public int update(Long id, PmsProductArticleParam articleParam) {
        int count;
        //更新商品信息
        PmsProduct product = new PmsProduct();
        BeanUtils.copyProperties(articleParam, product);
        product.setId(id);
        productMapper.updateByPrimaryKeySelective(product);
        
        PmsProductArticle article = new PmsProductArticle();
        BeanUtils.copyProperties(articleParam, article);
        article.setUpdateTime(new Date());
        PmsProductArticleExample example = new PmsProductArticleExample();
        PmsProductArticleExample.Criteria criteria = example.createCriteria();
        criteria.andProductIdEqualTo(id);
        pmsProductArticleMapper.updateByExampleSelective(article, example);
//        pmsProductArticleMapper.updateByExampleSelective(record, example);
        
        /*//会员价格
        PmsMemberPriceExample pmsMemberPriceExample = new PmsMemberPriceExample();
        pmsMemberPriceExample.createCriteria().andProductIdEqualTo(id);
        memberPriceMapper.deleteByExample(pmsMemberPriceExample);
        relateAndInsertList(memberPriceDao, productParam.getMemberPriceList(), id);
        //阶梯价格
        PmsProductLadderExample ladderExample = new PmsProductLadderExample();
        ladderExample.createCriteria().andProductIdEqualTo(id);
        productLadderMapper.deleteByExample(ladderExample);
        relateAndInsertList(productLadderDao, productParam.getProductLadderList(), id);
        //满减价格
        PmsProductFullReductionExample fullReductionExample = new PmsProductFullReductionExample();
        fullReductionExample.createCriteria().andProductIdEqualTo(id);
        productFullReductionMapper.deleteByExample(fullReductionExample);
        relateAndInsertList(productFullReductionDao, productParam.getProductFullReductionList(), id);
        //修改sku库存信息
        PmsSkuStockExample skuStockExample = new PmsSkuStockExample();
        skuStockExample.createCriteria().andProductIdEqualTo(id);
        skuStockMapper.deleteByExample(skuStockExample);
        handleSkuStockCode(productParam.getSkuStockList(),id);
        relateAndInsertList(skuStockDao, productParam.getSkuStockList(), id);*/
        //修改商品参数,添加自定义商品规格
        PmsProductAttributeValueExample productAttributeValueExample = new PmsProductAttributeValueExample();
        productAttributeValueExample.createCriteria().andProductIdEqualTo(id);
        productAttributeValueMapper.deleteByExample(productAttributeValueExample);
        relateAndInsertList(productAttributeValueDao, articleParam.getProductAttributeValueList(), id);
        //关联专题
        CmsSubjectProductRelationExample subjectProductRelationExample = new CmsSubjectProductRelationExample();
        subjectProductRelationExample.createCriteria().andProductIdEqualTo(id);
        subjectProductRelationMapper.deleteByExample(subjectProductRelationExample);
        relateAndInsertList(subjectProductRelationDao, articleParam.getSubjectProductRelationList(), id);
        //关联优选
        CmsPrefrenceAreaProductRelationExample prefrenceAreaExample = new CmsPrefrenceAreaProductRelationExample();
        prefrenceAreaExample.createCriteria().andProductIdEqualTo(id);
        prefrenceAreaProductRelationMapper.deleteByExample(prefrenceAreaExample);
        relateAndInsertList(prefrenceAreaProductRelationDao, articleParam.getPrefrenceAreaProductRelationList(), id);
        
        PmsProductRelationExample pmsProductRelationExample = new PmsProductRelationExample();
        pmsProductRelationExample.createCriteria().andProductIdEqualTo(id);
        pmsProductRelationMapper.deleteByExample(pmsProductRelationExample);
        List<PmsProductRelation> pmsProductRelationList = articleParam.getPmsProductRelationList();
        if(!CollectionUtils.isEmpty(pmsProductRelationList)) {
        	for (PmsProductRelation relation : pmsProductRelationList) {
        		relation.setProductId(id);
        		relation.setRelationType(relation.getRelationType());
        		pmsProductRelationMapper.insertSelective(relation);
			}
        }
        count = 1;
        return count;
    }

    /**
     * @deprecated 旧版创建
     */
    public int createOld(PmsProductParam productParam) {
        int count;
        //创建商品
        PmsProduct product = productParam;
        product.setId(null);
        productMapper.insertSelective(product);
        //根据促销类型设置价格：、阶梯价格、满减价格
        Long productId = product.getId();
        //会员价格
        List<PmsMemberPrice> memberPriceList = productParam.getMemberPriceList();
        if (!CollectionUtils.isEmpty(memberPriceList)) {
            for (PmsMemberPrice pmsMemberPrice : memberPriceList) {
                pmsMemberPrice.setId(null);
                pmsMemberPrice.setProductId(productId);
            }
            memberPriceDao.insertList(memberPriceList);
        }
        //阶梯价格
        List<PmsProductLadder> productLadderList = productParam.getProductLadderList();
        if (!CollectionUtils.isEmpty(productLadderList)) {
            for (PmsProductLadder productLadder : productLadderList) {
                productLadder.setId(null);
                productLadder.setProductId(productId);
            }
            productLadderDao.insertList(productLadderList);
        }
        //满减价格
        List<PmsProductFullReduction> productFullReductionList = productParam.getProductFullReductionList();
        if (!CollectionUtils.isEmpty(productFullReductionList)) {
            for (PmsProductFullReduction productFullReduction : productFullReductionList) {
                productFullReduction.setId(null);
                productFullReduction.setProductId(productId);
            }
            productFullReductionDao.insertList(productFullReductionList);
        }
        //添加sku库存信息
        List<PmsSkuStock> skuStockList = productParam.getSkuStockList();
        if (!CollectionUtils.isEmpty(skuStockList)) {
            for (PmsSkuStock skuStock : skuStockList) {
                skuStock.setId(null);
                skuStock.setProductId(productId);
            }
            skuStockDao.insertList(skuStockList);
        }
        //添加商品参数,添加自定义商品规格
        List<PmsProductAttributeValue> productAttributeValueList = productParam.getProductAttributeValueList();
        if (!CollectionUtils.isEmpty(productAttributeValueList)) {
            for (PmsProductAttributeValue productAttributeValue : productAttributeValueList) {
                productAttributeValue.setId(null);
                productAttributeValue.setProductId(productId);
            }
            productAttributeValueDao.insertList(productAttributeValueList);
        }
        //关联专题
        relateAndInsertList(subjectProductRelationDao, productParam.getSubjectProductRelationList(), productId);
        //关联优选
        relateAndInsertList(prefrenceAreaProductRelationDao, productParam.getPrefrenceAreaProductRelationList(), productId);
        count = 1;
        return count;
    }

    /**
     * 建立和插入关系表操作
     * @param <T>
     *
     * @param dao       可以操作的dao
     * @param dataList  要插入的数据
     * @param productId 建立关系的id
     */
    private <T> void relateAndInsertList(Object dao, List<T> dataList, Long productId) {
        try {
            if (CollectionUtils.isEmpty(dataList)) return;
            for (Object item : dataList) {
                Method setId = item.getClass().getMethod("setId", Long.class);
                setId.invoke(item, (Long) null);
                Method setProductId = item.getClass().getMethod("setProductId", Long.class);
                setProductId.invoke(item, productId);
            }
            Method insertList = dao.getClass().getMethod("insertList", List.class);
            insertList.invoke(dao, dataList);
        } catch (Exception e) {
            LOGGER.error("创建产品出错:{}",e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
