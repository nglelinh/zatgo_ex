package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ykb.mall.common.model.PmsProductLadder;

/**
 * 自定义会员阶梯价格Dao
 * Created by chen on 2018/4/26.
 */
public interface PmsProductLadderDao {
    int insertList(@Param("list") List<PmsProductLadder> productLadderList);
}
