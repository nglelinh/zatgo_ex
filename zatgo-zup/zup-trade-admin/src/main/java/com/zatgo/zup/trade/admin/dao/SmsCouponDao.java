package com.zatgo.zup.trade.admin.dao;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.trade.admin.dto.SmsCouponParam;

/**
 * 优惠券管理自定义查询Dao
 * Created by chen on 2018/8/29.
 */
public interface SmsCouponDao {
    SmsCouponParam getItem(@Param("id") Long id);
}
