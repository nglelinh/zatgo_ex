package com.zatgo.zup.trade.admin.service;

import java.util.List;

import com.ykb.mall.common.model.CmsSubject;
import com.zatgo.zup.trade.admin.dto.CmsSubjectData;
import com.zatgo.zup.trade.admin.dto.CmsSubjectParams;

/**
 * 商品专题Service
 * Created by chen on 2018/6/1.
 */
public interface CmsSubjectService {
    /**
     * 查询所有专题
     */
    List<CmsSubject> listAll();

    /**
     * 分页查询专题
     */
    List<CmsSubjectData> list(String keyword, Integer pageNum, Integer pageSize, Byte businessType);
    
    /**
     * 创建专题
     * @param params
     */
    void create(CmsSubjectParams params);
    
    /**
     * 更新专题
     * @param params
     */
    void update(CmsSubjectParams params);
    
    /**
     * 删除专题
     * @param id
     */
    void delete(Long id);
}
