package com.zatgo.zup.trade.admin.dto;

import com.ykb.mall.common.model.OmsCompanyAddress;
import com.ykb.mall.common.model.OmsOrderReturnApply;




/**
 * 申请信息封装
 * Created by chen on 2018/10/18.
 */
public class OmsOrderReturnApplyResult extends OmsOrderReturnApply {
    private OmsCompanyAddress companyAddress;

    public OmsCompanyAddress getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(OmsCompanyAddress companyAddress) {
        this.companyAddress = companyAddress;
    }
}
