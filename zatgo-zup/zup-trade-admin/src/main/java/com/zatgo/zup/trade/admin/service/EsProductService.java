package com.zatgo.zup.trade.admin.service;

/**
 * @Author wangyucong
 * @Date 2019/2/26 17:58
 */
public interface EsProductService {
    /**
     * 从数据库中导入所有商品到ES
     */
    int importAll();
}
