package com.zatgo.zup.trade.admin.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/19 16:10
 */
@ApiModel("班期信息提交")
public class PmsProductGroupParam {

    @ApiModelProperty("班期出行时间（yyyy-MM-dd格式）")
    private List<String> dateList;

    @ApiModelProperty("班期详细信息")
    private PmsProductGroupInfo groupInfo;

    public List<String> getDateList() {
        return dateList;
    }

    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }

    public PmsProductGroupInfo getGroupInfo() {
        return groupInfo;
    }

    public void setGroupInfo(PmsProductGroupInfo groupInfo) {
        this.groupInfo = groupInfo;
    }
}