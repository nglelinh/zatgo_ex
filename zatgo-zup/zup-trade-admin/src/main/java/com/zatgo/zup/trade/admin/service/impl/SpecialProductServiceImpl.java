package com.zatgo.zup.trade.admin.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductExample;
import com.zatgo.zup.common.model.SpecialProductListRequest;
import com.zatgo.zup.common.model.SpecialproductListResponse;
import com.zatgo.zup.common.model.UpdateSpecialProductRequest;
import com.zatgo.zup.trade.admin.mapper.PmsProductMapper;
import com.zatgo.zup.trade.admin.service.SpecialProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 46041 on 2019/7/18.
 */

@Service("specialProductService")
public class SpecialProductServiceImpl implements SpecialProductService {

    @Autowired
    private PmsProductMapper pmsProductMapper;

    @Override
    public JSONObject list(SpecialProductListRequest request) {
        //1-旅游；2-机票；3-酒店；4-文章;5-门票；6其他
        Integer businessType = request.getBusinessType();
        Integer pageNo = request.getPageNum();
        Integer pageSize = request.getPageSize();
        //1推荐2特惠3新品
        Integer type = request.getType();
        Boolean special = request.getSpecial();
        String productName = request.getProductName();
        PmsProductExample example = new PmsProductExample();
        PmsProductExample.Criteria criteria = example.createCriteria();
        if (businessType != null){
            criteria.andBusinessTypeEqualTo(new Byte(businessType + ""));
        }
        if (type != null){
            if (type.intValue() == 1){
                criteria.andRecommandStatusEqualTo(special ? 1 : 0);
            }
            if (type.intValue() == 2){
                criteria.andDiscountStatusEqualTo(special ? 1 : 0);
            }
            if (type.intValue() == 3){
                criteria.andNewStatusEqualTo(special ? 1 : 0);
            }
        }
        if (!StringUtils.isEmpty(productName)){
            criteria.andNameLike("%" + productName + "%");
        }
        PageHelper.startPage(pageNo, pageSize);
        List<PmsProduct> pmsProducts = pmsProductMapper.selectByExample(example);
        PageInfo<PmsProduct> pmsProductPageInfo = new PageInfo<>(pmsProducts);
        List<SpecialproductListResponse> list = new ArrayList<>();
        JSONObject r = new JSONObject();
        if (!CollectionUtils.isEmpty(pmsProducts)){
            for (PmsProduct pmsProduct : pmsProducts) {
                SpecialproductListResponse res = new SpecialproductListResponse();
                res.setImg(pmsProduct.getPic());
                res.setProductId(pmsProduct.getId() + "");
                res.setProductName(pmsProduct.getName());
                res.setSort(pmsProduct.getSort());
                res.setSpecial(false);
                if (type.intValue() == 1){
                    res.setSpecial(pmsProduct.getRecommandStatus().intValue() == 1);
                }
                if (type.intValue() == 2){
                    res.setSpecial(pmsProduct.getDiscountStatus().intValue() == 1);
                }
                if (type.intValue() == 3){
                    res.setSpecial(pmsProduct.getNewStatus().intValue() == 1);
                }
                res.setNote(pmsProduct.getNote());
                list.add(res);
            }
        }
        r.put("list", list);
        r.put("pageSzie", request.getPageSize());
        r.put("pageNum", request.getPageNum());
        r.put("totalPage", pmsProductPageInfo.getPages());
        r.put("total", pmsProductPageInfo.getTotal());
        return r;
    }

    @Override
    public void update(UpdateSpecialProductRequest request) {

        List<Long> ids = request.getIds();
        Integer businessType = request.getBusinessType();
        Boolean special = request.getSpecial();
        //1推荐2特惠3新品
        Integer type = request.getType();
        if (!CollectionUtils.isEmpty(ids)){
            PmsProduct pmsProduct = new PmsProduct();
            if (type != null){
                if (type.intValue() == 1){
                    pmsProduct.setRecommandStatus(special ? 1 : 0);
                }
                if (type.intValue() == 2){
                    pmsProduct.setDiscountStatus(special ? 1 : 0);
                }
                if (type.intValue() == 3){
                    pmsProduct.setNewStatus(special ? 1 : 0);
                }
                if (businessType != null){
                    pmsProduct.setBusinessType(new Byte(businessType + ""));
                }
                pmsProduct.setNote(request.getNote());
            }
            PmsProductExample example = new PmsProductExample();
            example.createCriteria().andIdIn(ids);
            pmsProductMapper.updateByExampleSelective(pmsProduct, example);
        }
    }
}
