package com.zatgo.zup.trade.admin.dto;

import io.swagger.annotations.ApiModel;

@ApiModel("航班详情")
public class PmsProductFlightResult extends PmsProductFlightParams {
	
	private Long cateParentId;

	public Long getCateParentId() {
		return cateParentId;
	}

	public void setCateParentId(Long cateParentId) {
		this.cateParentId = cateParentId;
	}

}
