package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ykb.mall.common.model.PmsProductRelation;

public interface PmsProductRelationDao {
	int insertList(@Param("list") List<PmsProductRelation> pmsProductRelationList);
}
