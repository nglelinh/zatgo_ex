package com.zatgo.zup.trade.admin.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.ykb.mall.common.enumType.BusinessEnum;
import com.ykb.mall.common.enumType.BusinessEnum.OrderStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.*;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.WechatRefundRequest;
import com.zatgo.zup.trade.admin.dao.OmsOrderDao;
import com.zatgo.zup.trade.admin.dao.OmsOrderOperateHistoryDao;
import com.zatgo.zup.trade.admin.dto.*;
import com.zatgo.zup.trade.admin.mapper.*;
import com.zatgo.zup.trade.admin.remoteService.ActivityRemoteService;
import com.zatgo.zup.trade.admin.remoteService.WechatRemoteService;
import com.zatgo.zup.trade.admin.service.OmsOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单管理Service实现类
 * Created by chen on 2018/10/11.
 */
@Service
public class OmsOrderServiceImpl implements OmsOrderService {

    private static final Logger logger = LoggerFactory.getLogger(OmsOrderServiceImpl.class);

    @Autowired
    private OmsOrderMapper orderMapper;
    @Autowired
    private OmsOrderDao orderDao;
    @Autowired
    private OmsOrderOperateHistoryDao orderOperateHistoryDao;
    @Autowired
    private OmsOrderOperateHistoryMapper orderOperateHistoryMapper;
    @Autowired
    private WechatRemoteService wechatRemoteService;
    @Autowired
    private ActivityRemoteService activityRemoteService;
    @Autowired
    private PmsProductGroupPriceMapper pmsProductGroupPriceMapper;
    @Autowired
    private PmsProductHotelMapper pmsProductHotelMapper;
    @Autowired
    private PmsProductGroupMapper pmsProductGroupMapper;
    @Autowired
    private PmsProductFlightPriceMapper pmsProductFlightPriceMapper;
    @Autowired
    private PmsCommonProductSkuMapper pmsCommonProductSkuMapper;
    @Autowired
    private OmsOrderItemMapper omsOrderItemMapper;


//    @Value("${system.pay.isOnlineRefund}")
//    private Boolean isOnlineRefund;
//    @Value("${third.activity.api}")
//    private String thirdApiUrl;
    @Value("${system.cloudUserId}")
    private String defaultCloudUserId;

    @Override
    public List<OmsOrder> list(OmsOrderQueryParam queryParam, Integer pageSize, Integer pageNum) {
        String productName = queryParam.getProductName();
        if (!StringUtils.isEmpty(productName)){
            queryParam.setProductName("%" + productName + "%");
        }
        PageHelper.startPage(pageNum, pageSize);
        List<OmsOrder> list = orderDao.getList(queryParam);
        StringBuffer sb = new StringBuffer();
        for (OmsOrder order : list) {
            String activityId = order.getActivityId();
            if (!StringUtils.isEmpty(activityId)){
                sb.append("," + activityId);
            }
        }
        String ids = sb.toString().replaceFirst(",", "");
        if (!StringUtils.isEmpty(ids)){
            try{
                ResponseData<Map<String, Long>> payTime = activityRemoteService.getPayTime(ids);
                if (payTime != null && payTime.isSuccessful()){
                    Map<String, Long> data = payTime.getData();
                    for (OmsOrder order : list) {
                        String activityId = order.getActivityId();
                        if (!StringUtils.isEmpty(activityId)){
                            Long timeOut = data.get(activityId);
                            order.setPayTime(timeOut);
                        }
                    }
                }
            } catch (Exception e){
                logger.error("", e);
            }
        }
        return list;
    }

    @Override
    public int delivery(List<OmsOrderDeliveryParam> deliveryParamList) {
        //批量发货
        int count = orderDao.delivery(deliveryParamList);
        //添加操作记录
        List<OmsOrderOperateHistory> operateHistoryList = deliveryParamList.stream()
                .map(omsOrderDeliveryParam -> {
                    OmsOrderOperateHistory history = new OmsOrderOperateHistory();
                    history.setOrderId(omsOrderDeliveryParam.getOrderId());
                    history.setCreateTime(new Date());
                    history.setOperateMan("后台管理员");
                    history.setOrderStatus(2);
                    history.setNote("完成发货");
                    return history;
                }).collect(Collectors.toList());
        orderOperateHistoryDao.insertList(operateHistoryList);
        return count;
    }

    @Override
    public int close(List<Long> ids, String note) {
        OmsOrder record = new OmsOrder();
        record.setStatus(4);
        OmsOrderExample example = new OmsOrderExample();
        example.createCriteria().andDeleteStatusEqualTo(0).andIdIn(ids);
        int count = orderMapper.updateByExampleSelective(record, example);
        List<OmsOrderOperateHistory> historyList = ids.stream().map(orderId -> {
            OmsOrderOperateHistory history = new OmsOrderOperateHistory();
            history.setOrderId(orderId);
            history.setCreateTime(new Date());
            history.setOperateMan("后台管理员");
            history.setOrderStatus(4);
            history.setNote("订单关闭:"+note);
            return history;
        }).collect(Collectors.toList());
        orderOperateHistoryDao.insertList(historyList);
        return count;
    }

    @Override
    public int delete(List<Long> ids) {
        OmsOrder record = new OmsOrder();
        record.setDeleteStatus(1);
        OmsOrderExample example = new OmsOrderExample();
        example.createCriteria().andDeleteStatusEqualTo(0).andIdIn(ids);
        return orderMapper.updateByExampleSelective(record, example);
    }

    @Override
    public OmsOrderDetail detail(Long id) {
        return orderDao.getDetail(id);
    }

    @Override
    public int updateReceiverInfo(OmsReceiverInfoParam receiverInfoParam) {
        OmsOrder order = new OmsOrder();
        order.setId(receiverInfoParam.getOrderId());
        order.setReceiverName(receiverInfoParam.getReceiverName());
        order.setReceiverPhone(receiverInfoParam.getReceiverPhone());
        order.setReceiverPostCode(receiverInfoParam.getReceiverPostCode());
        order.setReceiverDetailAddress(receiverInfoParam.getReceiverDetailAddress());
        order.setReceiverProvince(receiverInfoParam.getReceiverProvince());
        order.setReceiverCity(receiverInfoParam.getReceiverCity());
        order.setReceiverRegion(receiverInfoParam.getReceiverRegion());
        order.setModifyTime(new Date());
        int count = orderMapper.updateByPrimaryKeySelective(order);
        // 插入操作记录
        createOrderOperateHistory(receiverInfoParam.getOrderId(), receiverInfoParam.getStatus(), "修改收货人信息");
        return count;
    }

    @Override
    public int updateMoneyInfo(OmsMoneyInfoParam moneyInfoParam) {
        OmsOrder order = new OmsOrder();
        order.setId(moneyInfoParam.getOrderId());
        order.setFreightAmount(moneyInfoParam.getFreightAmount());
        order.setDiscountAmount(moneyInfoParam.getDiscountAmount());
        order.setModifyTime(new Date());
        int count = orderMapper.updateByPrimaryKeySelective(order);
        // 插入操作记录
        createOrderOperateHistory(moneyInfoParam.getOrderId(), moneyInfoParam.getStatus(), "修改费用信息");
        return count;
    }

    @Override
    public int updateNote(Long id, String note, Integer status) {
        OmsOrder order = new OmsOrder();
        order.setId(id);
        order.setNote(note);
        order.setModifyTime(new Date());
        int count = orderMapper.updateByPrimaryKeySelective(order);
        // 插入操作记录
        createOrderOperateHistory(id, status, "修改备注信息：" + note);
        return count;
    }

    /**
     * 确认订单支付状态
     * @param param
     * @return
     */
    @Override
    public int confirmPayment(OmsPaymentConfirmParam param) {
        OmsOrder originalOrder = orderMapper.selectByPrimaryKey(param.getOrderId());
        if (null == originalOrder) {
            return 0;
        }

        int orderStatus = originalOrder.getStatus();
        String note = "";
        OmsOrder order = new OmsOrder();
        Long orderId = param.getOrderId();
        order.setId(orderId);
        // 确认收款
        if (0 == originalOrder.getStatus()) {
            order.setStatus(1);
            order.setPaymentTime(new Date());
            note += "订单确认收款，";
        }
        // 未完全支付
        if ((originalOrder.getPayAmount().subtract(originalOrder.getPaidAmount())).compareTo(param.getPaidMoney()) > 0) {
            note += "部分支付，本次支付：" + param.getPaidMoney().toString() + "元，还需支付："
                    + originalOrder.getPayAmount().subtract(originalOrder.getPaidAmount().add(param.getPaidMoney())).stripTrailingZeros().toPlainString() + "元";
        } else {
            note += "支付完成，本次支付：" + param.getPaidMoney().toPlainString() + "元";
            if (originalOrder.getIsActivityOrder()){
                ResponseData<String> responseData = activityRemoteService.selectActivityIdByOrderId(orderId + "", defaultCloudUserId);
                if (responseData == null || !responseData.isSuccessful() || StringUtils.isEmpty(responseData.getData())){
                    logger.info("订单信息异常_orderId: " + orderId);
                    throw new BusinessException("", "订单信息异常");
                }
                String activityId = responseData.getData();
                ResponseData<Boolean> res = activityRemoteService.orderCallBack(activityId, originalOrder.getMemberId(), orderId + "", true);
                if (res == null || !res.isSuccessful()){
                    logger.info("返回信息 ============== activityId: " + activityId + "_userId:" + originalOrder.getMemberId() +  "_orderId: " + orderId + "_isSuccess :" + true);
                    throw new BusinessException("", "支付回调通知失败");
                }
            }
        }
        order.setPaidAmount(originalOrder.getPaidAmount().add(param.getPaidMoney()));
        order.setPayType(3);
        int count = orderMapper.updateByPrimaryKeySelective(order);

        if (!StringUtils.isEmpty(param.getNote())) {
            note += "，备注：" + param.getNote();
        }
        // 新增历史操作记录
        createOrderOperateHistory(param.getOrderId(), order.getStatus() == null ? orderStatus : order.getStatus(), note);
        return count;
    }

    /**
     * 创建订单历史操作记录
     * @param orderId
     * @param orderStatus
     * @param note
     * @return
     */
    @Override
    public int createOrderOperateHistory(Long orderId, Integer orderStatus, String note) {
        OmsOrderOperateHistory history = new OmsOrderOperateHistory();
        history.setOrderId(orderId);
        history.setCreateTime(new Date());
        history.setOperateMan("后台管理员");
        history.setOrderStatus(null == orderStatus ? 0 : orderStatus);
        history.setNote(null == note ? "" : note);
        return orderOperateHistoryMapper.insert(history);
    }

    /**
     * 材料办理完成
     * @param param
     * @return
     */
    @Override
    public int handleFinish(OmsHandleFinishParam param) {
        OmsOrder omsOrder = new OmsOrder();
        omsOrder.setId(param.getOrderId());
        omsOrder.setStatus(2);
        omsOrder.setHandleFinishTime(new Date());
        int result =  orderMapper.updateByPrimaryKeySelective(omsOrder);

        String note = "材料办理完成";
        if (null != param.getNote()) {
            note += "，备注：" + param.getNote();
        }
        // 新增历史操作记录
        createOrderOperateHistory(param.getOrderId(), omsOrder.getStatus(), note);
        return result;
    }

    /**
     * 协议签署完成
     * @param param
     * @return
     */
    @Override
    public int signAgreement(OmsSignParam param) {
        OmsOrder omsOrder = new OmsOrder();
        omsOrder.setId(param.getOrderId());
        omsOrder.setStatus(3);
        omsOrder.setSignTime(new Date());
        omsOrder.setConfirmStatus(1);
        int result =  orderMapper.updateByPrimaryKeySelective(omsOrder);

        String note = "协议签署完成";
        if (null != param.getNote()) {
            note += "，备注：" + param.getNote();
        }
        // 新增历史操作记录
        createOrderOperateHistory(param.getOrderId(), omsOrder.getStatus(), note);
        return result;
    }

    /**
     * 已完成订单
     * @param param
     * @return
     */
    @Override
    public int completeOrder(OmsHasCompleted param) {
        OmsOrder omsOrder = new OmsOrder();
        omsOrder.setId(param.getOrderId());
        omsOrder.setStatus(6);
        int result =  orderMapper.updateByPrimaryKeySelective(omsOrder);

        String note = "订单已完成";
        if (null != param.getNote()) {
            note += "，备注：" + param.getNote();
        }
        // 新增历史操作记录
        createOrderOperateHistory(param.getOrderId(), omsOrder.getStatus(), note);
        return result;
    }

    /**
     * 关闭订单
     * @param param
     * @return
     */
    @Override
    public int closeOmsOrder(OmsCloseParam param) {
        OmsOrder omsOrder = new OmsOrder();
        omsOrder.setId(param.getOrderId());
        omsOrder.setStatus(4);
        int result =  orderMapper.updateByPrimaryKeySelective(omsOrder);

        String note = "订单关闭";
        if (null != param.getNote()) {
            note += "，备注：" + param.getNote();
        }
        // 新增历史操作记录
        createOrderOperateHistory(param.getOrderId(), omsOrder.getStatus(), note);
        return result;
    }

    /**
     * 退款订单金额
     * @param param
     * @return
     */
    @Override
    @Transactional
    public int refundOmsPrice(OmsRefundInfoParam param) {
        OmsOrder omsOrder = orderMapper.selectByPrimaryKey(param.getOrderId());
        if (null == omsOrder) {
            return 0;
        }
        Integer status = omsOrder.getStatus();
        if (status.intValue() == 0 || status.intValue() == 4 || status.intValue() == 5)
            throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "当前订单状态无法退款");
        Byte businessType = omsOrder.getBusinessType();
        OmsOrder record = new OmsOrder();
        record.setId(param.getOrderId());
        record.setStatus(OrderStatus.refund.getCode());
        orderMapper.updateByPrimaryKeySelective(record);

        OmsOrderItemExample itemExample = new OmsOrderItemExample();
        itemExample.createCriteria().andOrderIdEqualTo(omsOrder.getId());
        //查出所有orderItem
        List<OmsOrderItem> omsOrderItems = omsOrderItemMapper.selectByExample(itemExample);
        OmsOrderItem omsOrderItem = omsOrderItems.get(0);
        String sp3 = omsOrderItem.getSp3();

        String note = "订单退款，退款金额：" + param.getRefundPrice().toString();
        if (null != param.getNote()) {
            note += "，备注：" + param.getNote();
        }
        // 新增历史操作记录
        createOrderOperateHistory(param.getOrderId(), OrderStatus.refund.getCode(), note);

        //线下的直接改状态
        if (!new Byte("3").equals(omsOrder.getPayType())){
            try {
                WechatRefundRequest request = new WechatRefundRequest();
                request.setOrderNumber(omsOrder.getOrderSn());
                request.setRefundAmount(param.getRefundPrice());
                wechatRemoteService.weixinRefund(request);
            } catch (Exception e) {
                logger.error("微信退款失败：", e);
                throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR, "微信退款失败");
            }
        }

        //旅游
        if (BusinessEnum.BusinessType.TRAVEL.getCode().equals(businessType)){
            addTravelStock(sp3, omsOrderItem);
        }
        //机票
        if (BusinessEnum.BusinessType.FLIGHT.getCode().equals(businessType)){
            addFlightStock(sp3, omsOrderItem);
        }
        //酒店
        if (BusinessEnum.BusinessType.HOTEL.getCode().equals(businessType)){
            addHotelStock(sp3, omsOrderItem);
        }
        //门票、普通商品
        if (BusinessEnum.BusinessType.COMMON.getCode().equals(businessType) || BusinessEnum.BusinessType.OTHER.getCode().equals(businessType)){
            addCommonStock(sp3, omsOrderItem);
        }

        //是活动的单子,通知活动
        Boolean isActivityOrder = omsOrder.getIsActivityOrder();
        if (isActivityOrder != null && isActivityOrder){
            try {
                ResponseData<Boolean> booleanResponseData = activityRemoteService.weixinRefundCallBack(omsOrder.getId() + "", defaultCloudUserId);
                if (booleanResponseData == null || !booleanResponseData.isSuccessful()){
                    logger.info("返回信息 ============== " + "_orderId: " + omsOrder.getId());
                    throw new BusinessException("", "支付回调通知失败");
                }
            } catch (Exception e){
                logger.error("", e);
                logger.info("返回信息 ============== " + "_orderId: " + omsOrder.getId());
                throw new BusinessException("", "支付回调通知失败");
            }
        }
        return 1;
    }

    private void addCommonStock(String sp3, OmsOrderItem omsOrderItem) {
        CommonExtInfo commonExtInfo = JSONObject.parseObject(sp3, CommonExtInfo.class);
        String skuId = commonExtInfo.getSkuId();
        Integer adultNum = omsOrderItem.getAdultNum();
        Integer childNum = omsOrderItem.getChildNum();
        pmsCommonProductSkuMapper.updateStock(skuId, adultNum + childNum);
    }


    private void addHotelStock(String sp3, OmsOrderItem omsOrderItem){
        HotelExtInfo hotelExtInfo = JSONObject.parseObject(sp3, HotelExtInfo.class);
        List<Long> prices = hotelExtInfo.getPrices();
        Integer adultNum = omsOrderItem.getAdultNum();
        Integer childNum = omsOrderItem.getChildNum();
        for (Long price : prices) {
            pmsProductHotelMapper.updateStock(price, adultNum + childNum);
        }
    }

    private void addFlightStock(String sp3, OmsOrderItem omsOrderItem){
        FlightExtInfo flightExtInfo = JSONObject.parseObject(sp3, FlightExtInfo.class);
        List<Long> prices = flightExtInfo.getPrices();
        Integer adultNum = omsOrderItem.getAdultNum();
        Integer childNum = omsOrderItem.getChildNum();
        for (Long price : prices) {
            pmsProductFlightPriceMapper.updateStock(price, adultNum + childNum);
        }

    }

    private void addTravelStock(String sp3, OmsOrderItem omsOrderItem){
        TravelExtInfo extInfo = JSONObject.parseObject(sp3, TravelExtInfo.class);
        List<Long> prices = extInfo.getPrices();
        PmsProductGroupPriceExample priceExample = new PmsProductGroupPriceExample();
        priceExample.createCriteria().andIdIn(prices);
        List<PmsProductGroupPrice> pmsProductGroupPrices = pmsProductGroupPriceMapper.selectByExample(priceExample);
        Long groupId = null;
        Integer adultNum = omsOrderItem.getAdultNum();
        Integer childNum = omsOrderItem.getChildNum();
        for (PmsProductGroupPrice ppgp : pmsProductGroupPrices) {
            Byte priceType = ppgp.getPriceType();
            Long priceId = ppgp.getId();
            groupId = ppgp.getGroupId();
            if (BusinessEnum.PriceType.ADULT.getCode().equals(priceType)){
                pmsProductGroupPriceMapper.updateStock(priceId, adultNum);
            }
            if (BusinessEnum.PriceType.CHILD.getCode().equals(priceType)){
                pmsProductGroupPriceMapper.updateStock(priceId, childNum);
            }
        }
        //加库存
        pmsProductGroupMapper.updateStock(groupId, adultNum + childNum);
    }
}
