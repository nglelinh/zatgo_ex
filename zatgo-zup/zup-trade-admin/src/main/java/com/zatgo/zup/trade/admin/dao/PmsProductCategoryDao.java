package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import com.zatgo.zup.trade.admin.dto.PmsProductCategoryWithChildrenItem;

/**
 * 商品分类自定义Dao
 * Created by chen on 2018/5/25.
 */
public interface PmsProductCategoryDao {
    List<PmsProductCategoryWithChildrenItem> listWithChildren();

    List<PmsProductCategoryWithChildrenItem> commonListWithChildren();
}
