//package com.zatgo.zup.trade.admin.component;
//
//import java.io.IOException;
//import java.nio.file.AccessDeniedException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.security.web.access.AccessDeniedHandler;
//import org.springframework.stereotype.Component;
//
//import com.zatgo.zup.trade.admin.dto.CommonResult;
//
///**
// * 当访问接口没有权限时，自定义的返回结果
// * Created by chen on 2018/4/26.
// */
//@Component
//public class RestfulAccessDeniedHandler implements AccessDeniedHandler{
//    @Override
//    public void handle(HttpServletRequest request,
//                       HttpServletResponse response,
//                       AccessDeniedException e) throws IOException, ServletException {
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json");
//        response.getWriter().println(JsonUtil.objectToJson(new CommonResult().forbidden(e.getMessage())));
//        response.getWriter().flush();
//    }
//}
