package com.zatgo.zup.trade.admin.mapper;

import com.ykb.mall.common.model.UmsMemberPassenger;
import com.ykb.mall.common.model.UmsMemberPassengerExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsMemberPassengerMapper {
    int countByExample(UmsMemberPassengerExample example);

    int deleteByExample(UmsMemberPassengerExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsMemberPassenger record);

    int insertSelective(UmsMemberPassenger record);

    List<UmsMemberPassenger> selectByExample(UmsMemberPassengerExample example);

    UmsMemberPassenger selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsMemberPassenger record, @Param("example") UmsMemberPassengerExample example);

    int updateByExample(@Param("record") UmsMemberPassenger record, @Param("example") UmsMemberPassengerExample example);

    int updateByPrimaryKeySelective(UmsMemberPassenger record);

    int updateByPrimaryKey(UmsMemberPassenger record);
}