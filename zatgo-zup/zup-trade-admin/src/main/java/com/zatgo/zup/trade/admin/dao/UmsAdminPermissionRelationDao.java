package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import com.ykb.mall.model.UmsAdminPermissionRelation;
import org.apache.ibatis.annotations.Param;


/**
 * 用户权限自定义Dao
 * Created by chen on 2018/10/8.
 */
public interface UmsAdminPermissionRelationDao {
    int insertList(@Param("list") List<UmsAdminPermissionRelation> list);
}
