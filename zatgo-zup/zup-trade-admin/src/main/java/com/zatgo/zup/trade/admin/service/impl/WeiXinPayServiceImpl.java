//package com.zatgo.zup.trade.admin.service.impl;
//
//import java.math.BigDecimal;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.alibaba.fastjson.JSONObject;
//import com.ykb.mall.common.enumType.BusinessEnum.OrderStatus;
//import com.ykb.mall.common.enumType.BusinessEnum.PayType;
//import com.zatgo.zup.common.exception.BusinessException;
//import com.ykb.mall.common.exception.BusinessExceptionCode;
//import com.ykb.mall.common.utils.UUIDUtil;
//import com.ykb.mall.common.wxpay.sdk.WXPay;
//import com.ykb.mall.common.wxpay.sdk.WeiXinPayConfig;
//import com.zatgo.zup.common.redis.WXPayConstants;
//import com.zatgo.zup.trade.admin.dao.PortalOrderDao;
//import com.zatgo.zup.trade.admin.service.ThirdPayRecordService;
//import com.zatgo.zup.trade.admin.service.WeiXinPayService;
//
//@Service
//public class WeiXinPayServiceImpl implements WeiXinPayService {
//
//	private static final Logger logger = LoggerFactory.getLogger(WeiXinPayServiceImpl.class);
//
//	private static final String SCENE_INFO = "{\"h5_info\":{\"type\":\"Wap\",\"wap_url\":\"%s\",\"wap_name\":\"%s\"}}";
//
//
//	@Autowired
//	private WeiXinPayConfig weiXinPayConfig;
//	//
//	// @Autowired
//	// private WeiXinPublicPayConfig wxPayPublicConfig;
//
//	@Autowired
//	private ThirdPayRecordService thirdPayRecordService;
//	@Autowired
//	private PortalOrderDao portalOrderDao;
//
//
//	@Override
//	public void weixinRefund(String orderNumber, BigDecimal refundAmount) throws Exception {
//		PortalOrderBaseInfo order = portalOrderDao.selectOrderByOrderSn(orderNumber);
//		if (null == order) {
//			throw new BusinessException(BusinessExceptionCode.EX_ORDER_NOT_EXIST);
//		}
//
//		if (OrderStatus.wait_pay.getCode() == order.getStatus()) {
//			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "订单未付款");
//		}
//
//		if (order.getPayPrice().compareTo(refundAmount) == -1) {
//			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "退款金额大于订单金额");
//		}
//
//		Map<String, String> reqData = new HashMap<>();
//		reqData.put(WXPayConstants.FIELD_OUT_TRADE_NO, order.getOrderSn());
//		reqData.put(WXPayConstants.FIELD_OUT_REFUND_NO, order.getOrderSn() + "-" + UUIDUtil.getRandomUserName());
//		reqData.put(WXPayConstants.FIELD_TOTAL_FEE,
//				order.getPayPrice().multiply(new BigDecimal("100")).stripTrailingZeros().toPlainString());
//		reqData.put(WXPayConstants.FIELD_REFUND_FEE, refundAmount.multiply(new BigDecimal("100")).stripTrailingZeros().toPlainString());
//		WXPay wxPay = new WXPay(weiXinPayConfig, "");
//
//		Map<String, String> respData = wxPay.refund(reqData);
//		if (!WXPayConstants.SUCCESS.equalsIgnoreCase(respData.get(WXPayConstants.FIELD_RETURN_CODE))) {
//			logger.error("退款入参", JSONObject.toJSONString(reqData));
//			logger.error("调用微信退款接口失败：{}", respData);
//			throw new BusinessException(BusinessExceptionCode.HTTP_FAIL_CODE, "调用微信退款接口失败");
//		}
//
//		if (!WXPayConstants.OK.equalsIgnoreCase(respData.get(WXPayConstants.FIELD_RETURN_MSG))) {
//			logger.error("退款入参", JSONObject.toJSONString(reqData));
//			logger.error("微信退款接口退款失败：{}", respData);
//			throw new BusinessException(BusinessExceptionCode.HTTP_FAIL_CODE, "微信退款接口退款失败");
//		}
//
//		// 保存退款记录
//		thirdPayRecordService.insertRefundRecord(orderNumber, reqData.get(WXPayConstants.FIELD_OUT_REFUND_NO),
//				PayType.weixinPay, refundAmount, reqData.get(WXPayConstants.FIELD_TRANSACTION_ID),
//				reqData.get(WXPayConstants.FIELD_REFUND_ID));
//	}
//
//}
