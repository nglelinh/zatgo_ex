package com.zatgo.zup.trade.admin.service;

import javax.servlet.http.HttpServletRequest;

import com.zatgo.zup.trade.admin.dto.OssCallbackResult;
import com.zatgo.zup.trade.admin.dto.OssPolicyResult;

/**
 * oss上传管理Service
 * Created by chen on 2018/5/17.
 */
public interface OssService {
    OssPolicyResult policy();
    OssCallbackResult callback(HttpServletRequest request);
}
