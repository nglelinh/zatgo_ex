package com.zatgo.zup.trade.admin.mapper;

import com.ykb.mall.common.model.PmsProductGroup;
import com.ykb.mall.common.model.PmsProductGroupExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductGroupMapper {
    int countByExample(PmsProductGroupExample example);

    int deleteByExample(PmsProductGroupExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductGroup record);

    int insertSelective(PmsProductGroup record);

    List<PmsProductGroup> selectByExample(PmsProductGroupExample example);

    PmsProductGroup selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductGroup record, @Param("example") PmsProductGroupExample example);

    int updateByExample(@Param("record") PmsProductGroup record, @Param("example") PmsProductGroupExample example);

    int updateByPrimaryKeySelective(PmsProductGroup record);

    int updateByPrimaryKey(PmsProductGroup record);

    int updateStock(@Param("groupId") Long groupId,@Param("number") Integer number);
}