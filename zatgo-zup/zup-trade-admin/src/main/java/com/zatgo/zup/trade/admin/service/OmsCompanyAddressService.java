package com.zatgo.zup.trade.admin.service;

import java.util.List;

import com.ykb.mall.common.model.OmsCompanyAddress;

/**
 * 收货地址管Service
 * Created by chen on 2018/10/18.
 */
public interface OmsCompanyAddressService {
    /**
     * 获取全部收货地址
     */
    List<OmsCompanyAddress> list();
}
