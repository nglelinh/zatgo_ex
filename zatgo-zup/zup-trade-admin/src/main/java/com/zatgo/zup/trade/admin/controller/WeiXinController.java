package com.zatgo.zup.trade.admin.controller;

import com.zatgo.zup.trade.admin.remoteService.WechatRemoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wangyucong
 * @Date 2019/4/8 13:54
 */
@RestController
@RequestMapping("/trade/admin/wechat")
@Api(tags = "WeiXinController", description = "微信管理")
public class WeiXinController {

    @Autowired
    private WechatRemoteService wechatRemoteService;

    @GetMapping("/jsapiConfig")
    @ApiOperation("获取微信接口配置")
    public Object selectJsapiConfig(@RequestParam("url") String url){
        return wechatRemoteService.selectJsapiConfig(url);
    }
}