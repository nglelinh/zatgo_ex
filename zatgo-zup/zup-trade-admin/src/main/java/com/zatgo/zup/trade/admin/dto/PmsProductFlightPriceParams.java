package com.zatgo.zup.trade.admin.dto;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("航班价格")
public class PmsProductFlightPriceParams {
	
	@ApiModelProperty("价格id")
	private Long id;
	
	@ApiModelProperty(hidden = true)
	private Long flightId;
	
	@ApiModelProperty("价格名称")
	private String priceName;
	
	@ApiModelProperty("供应价")
	private BigDecimal supplyPrice;
	
	@ApiModelProperty("人数限制")
	private Integer maxPeople;
	
	@ApiModelProperty("是否优先展示价格（0 - 否，1 - 是）")
	private Byte firstShow;
	
	@ApiModelProperty("展示状态（0 - 不展示，1 - 展示）")
	private Byte showStatus;
	
	@ApiModelProperty("舱位")
	private String shippingSpaceName;
	
	@ApiModelProperty("航班日期")
	private Date flightDate;
	
	@ApiModelProperty("库存")
	private Integer stock;
	
	@ApiModelProperty("采购价")
	private BigDecimal purchasePrice;
	
	@ApiModelProperty("儿童价")
	private BigDecimal childPrice;
	
	@ApiModelProperty("儿童采购价")
	private BigDecimal childPurchasePrice;

	@ApiModelProperty("返回航班日期")
	private Date returnFlightData;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFlightId() {
		return flightId;
	}

	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}

	public String getPriceName() {
		return priceName;
	}

	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}

	public BigDecimal getSupplyPrice() {
		return supplyPrice;
	}

	public void setSupplyPrice(BigDecimal supplyPrice) {
		this.supplyPrice = supplyPrice;
	}

	public Integer getMaxPeople() {
		return maxPeople;
	}

	public void setMaxPeople(Integer maxPeople) {
		this.maxPeople = maxPeople;
	}

	public Byte getFirstShow() {
		return firstShow;
	}

	public void setFirstShow(Byte firstShow) {
		this.firstShow = firstShow;
	}

	public Byte getShowStatus() {
		return showStatus;
	}

	public void setShowStatus(Byte showStatus) {
		this.showStatus = showStatus;
	}

	public String getShippingSpaceName() {
		return shippingSpaceName;
	}

	public void setShippingSpaceName(String shippingSpaceName) {
		this.shippingSpaceName = shippingSpaceName;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public BigDecimal getChildPrice() {
		return childPrice;
	}

	public void setChildPrice(BigDecimal childPrice) {
		this.childPrice = childPrice;
	}

	public BigDecimal getChildPurchasePrice() {
		return childPurchasePrice;
	}

	public void setChildPurchasePrice(BigDecimal childPurchasePrice) {
		this.childPurchasePrice = childPurchasePrice;
	}

	public Date getReturnFlightData() {
		return returnFlightData;
	}

	public void setReturnFlightData(Date returnFlightData) {
		this.returnFlightData = returnFlightData;
	}
}
