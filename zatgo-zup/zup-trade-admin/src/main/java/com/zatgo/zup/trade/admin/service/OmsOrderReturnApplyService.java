package com.zatgo.zup.trade.admin.service;

import java.util.List;

import com.ykb.mall.common.model.OmsOrderReturnApply;
import com.zatgo.zup.trade.admin.dto.OmsOrderReturnApplyResult;
import com.zatgo.zup.trade.admin.dto.OmsReturnApplyQueryParam;
import com.zatgo.zup.trade.admin.dto.OmsUpdateStatusParam;

/**
 * 退货申请管理Service
 * Created by chen on 2018/10/18.
 */
public interface OmsOrderReturnApplyService {
    /**
     * 分页查询申请
     */
    List<OmsOrderReturnApply> list(OmsReturnApplyQueryParam queryParam, Integer pageSize, Integer pageNum);

    /**
     * 批量删除申请
     */
    int delete(List<Long> ids);

    /**
     * 修改申请状态
     */
    int updateStatus(Long id, OmsUpdateStatusParam statusParam);

    /**
     * 获取指定申请详情
     */
    OmsOrderReturnApplyResult getItem(Long id);
}
