package com.zatgo.zup.trade.admin.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.ykb.mall.common.model.PmsProductHotel;
import com.ykb.mall.common.model.PmsProductHotelExample;
import com.ykb.mall.common.utils.DateTimeUtils;
import com.zatgo.zup.trade.admin.mapper.PmsProductHotelMapper;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsHotelData;
import com.zatgo.zup.trade.admin.dto.PmsProductHotelParams;
import com.zatgo.zup.trade.admin.service.PmsProductHotelService;

@Service
public class PmsProductHotelServiceImpl implements PmsProductHotelService {

	@Autowired
	private PmsProductHotelMapper pmsProductHotelMapper;

	@Transactional
	@Override
	public CommonResult createHotelPrice(PmsProductHotelParams params) {
		Long productId = params.getProductId();
		if (productId == null) {
			return new CommonResult().failed("产品编号丢失");
		}

		Map<String, List<PmsHotelData>> prices = params.getPrices();
		if (CollectionUtils.isEmpty(prices)) {
			return new CommonResult().failed("产品报价丢失");
		}

		Set<Entry<String, List<PmsHotelData>>> entries = prices.entrySet();
		for (Entry<String, List<PmsHotelData>> entry : entries) {
			List<PmsHotelData> hotelDatas = entry.getValue();
			for (PmsHotelData pmsHotelData : hotelDatas) {
				PmsProductHotel hotel = new PmsProductHotel();
				BeanUtils.copyProperties(pmsHotelData, hotel);
				hotel.setProductId(productId);
				hotel.setCreateTime(new Date());

				PmsProductHotelExample example = new PmsProductHotelExample();
				PmsProductHotelExample.Criteria criteria = example.createCriteria();
				Date arrivalDate = DateTimeUtils.parseStringToDate(pmsHotelData.getArrivalDate(),
						DateTimeUtils.PATTEN_YYYY_MM_DD);
				criteria.andArrivalDateEqualTo(arrivalDate);
				criteria.andRoomTypeEqualTo(pmsHotelData.getRoomType());
				criteria.andProductIdEqualTo(productId);
				List<PmsProductHotel> hotels = pmsProductHotelMapper.selectByExample(example);

				if (CollectionUtils.isEmpty(hotels)) {
					hotel.setArrivalDate(DateTimeUtils.parseStringToDate(pmsHotelData.getArrivalDate(),
							DateTimeUtils.PATTEN_YYYY_MM_DD));

					pmsProductHotelMapper.insertSelective(hotel);
				} else {
					criteria.andIdEqualTo(hotels.get(0).getId());
					pmsProductHotelMapper.updateByExampleSelective(hotel, example);
				}

			}
		}

		return new CommonResult().success();
	}

	@Override
	public CommonResult hotelPriceList(Long productId) {
		PmsProductHotelExample example = new PmsProductHotelExample();
		PmsProductHotelExample.Criteria criteria = example.createCriteria();
		criteria.andProductIdEqualTo(productId);

		LinkedHashMap<String, List<PmsHotelData>> prices = new LinkedHashMap<>();

		List<PmsProductHotel> hotels = pmsProductHotelMapper.selectByExample(example);
		if (!CollectionUtils.isEmpty(hotels)) {
			for (PmsProductHotel hotel : hotels) {
				String arrivalDate = DateTimeUtils.parseDateToString(hotel.getArrivalDate(),
						DateTimeUtils.PATTEN_YYYY_MM_DD);
				PmsHotelData hotelData = new PmsHotelData();
				BeanUtils.copyProperties(hotel, hotelData);
				hotelData.setArrivalDate(arrivalDate);
				List<PmsHotelData> hotelDatas = prices.get(arrivalDate);
				if (hotelDatas == null) {
					hotelDatas = new ArrayList<PmsHotelData>();
					prices.put(arrivalDate, hotelDatas);
				}

				hotelDatas.add(hotelData);
			}
		}
		return new CommonResult().success(prices);
	}

	@Override
	public CommonResult updateHotelPrice(PmsHotelData prams) {
		PmsProductHotel hotel = new PmsProductHotel();
		BeanUtils.copyProperties(prams, hotel);
		if (StringUtils.isNotEmpty(prams.getArrivalDate())) {
			hotel.setArrivalDate(
					DateTimeUtils.parseStringToDate(prams.getArrivalDate(), DateTimeUtils.PATTEN_YYYY_MM_DD));
		}

		PmsProductHotelExample example = new PmsProductHotelExample();
		PmsProductHotelExample.Criteria criteria = example.createCriteria();
		criteria.andIdEqualTo(prams.getId());

		pmsProductHotelMapper.updateByExampleSelective(hotel, example);
		return new CommonResult().success();
	}

	@Override
	public CommonResult updateHotelStatus(Long productId, Byte status) {
		
		PmsProductHotelExample example = new PmsProductHotelExample();
		PmsProductHotelExample.Criteria criteria = example.createCriteria();
		criteria.andProductIdEqualTo(productId);
		
		PmsProductHotel hotel = new PmsProductHotel();
		hotel.setStatus(status);
		pmsProductHotelMapper.updateByExampleSelective(hotel, example);
		return new CommonResult().success();
	}

}
