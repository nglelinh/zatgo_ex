package com.zatgo.zup.trade.admin.controller;

import com.ykb.mall.common.model.PmsCommonProductCategory;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.service.PmsCommonProductCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品分类模块Controller
 * Created by chen on 2018/4/26.
 */
@Controller
@Api(tags = "PmsProductCategoryController", description = "商品分类管理")
@RequestMapping("/trade/admin/common/productCategory")
public class PmsCommonProductCategoryController {
    @Autowired
    private PmsCommonProductCategoryService pmsCommonProductCategoryService;

    @ApiOperation("添加产品分类")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    //@PreAuthorize("hasAuthority('pms:productCategory:create')")
    public Object create(@Validated @RequestBody PmsCommonProductCategory productCategoryParam,
                         BindingResult result) {
        int count = pmsCommonProductCategoryService.create(productCategoryParam);
        if (count > 0) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation("修改商品分类")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    //@PreAuthorize("hasAuthority('pms:productCategory:update')")
    public Object update(@RequestBody PmsCommonProductCategory productCategoryParam) {
        int count = pmsCommonProductCategoryService.update(productCategoryParam);
        if (count > 0) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation("获取某节点下的所有子节点")
    @RequestMapping(value = "/getChild", method = RequestMethod.GET)
    @ResponseBody
    //@PreAuthorize("hasAuthority('pms:productCategory:read')")
    public Object getItem(@RequestParam(value = "id", required = false) String id) {
        List<PmsCommonProductCategory> list = pmsCommonProductCategoryService.getItem(id);
        return new CommonResult().success(list);
    }

    @ApiOperation("根据id获取类目信息")
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object detail(@PathVariable(value = "id") String id) {
        PmsCommonProductCategory cat = pmsCommonProductCategoryService.detail(id);
        return new CommonResult().success(cat);
    }

    @ApiOperation("删除类目（必须不存在子节点，没有被商品使用，否则删除失败）")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    //@PreAuthorize("hasAuthority('pms:productCategory:delete')")
    public Object delete(@PathVariable("id") String id) {
        pmsCommonProductCategoryService.delete(id);
        return new CommonResult().success();
    }
}
