package com.zatgo.zup.trade.admin.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductGroupDate;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupBasicInfo;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupData;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupInfo;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupListParam;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupParam;

/**
 * 产品班期管理
 * @Author wangyucong
 * @Date 2019/3/19 17:04
 */
public interface PmsProductGroupService {

    /**
     * 新增产品班期
     * @param param
     * @return
     */
    @Transactional
    int createProductGroup(PmsProductGroupParam param);

    /**
     * 更新产品班期
     * @param param
     * @return
     */
    @Transactional
    int updateProductGroup(PmsProductGroupParam param);

    /**
     * 查询班期详情
     * @param groupId
     * @return
     */
    PmsProductGroupData selectGroupDetail(Long groupId);

    /**
     * 查询班期列表
     * @param param
     * @return
     */
    List<PmsProductGroupBasicInfo> selectGroupList(PmsProductGroupListParam param);

    /**
     * 查询产品的班期日期
     * @param productId
     * @return
     */
    List<PmsProductGroupDate> selectProductDateList(Long productId);

    /**
     * 删除班期
     * @param groupId
     * @return
     */
    int deleteProductGroup(Long groupId);

    /**
     * 更新班期库存
     * @param param
     * @return
     */
    int updateGroupStock(PmsProductGroupInfo param);

    /**
     * 查询可选产品列表
     * @return
     */
    List<PmsProduct> selectProductList(Byte businessType);

    /**
     * 生成业务编码
     * @param businessType
     * @return
     */
    String generateBusinessNum(Byte businessType);

    int deleteProductGroupList(String ids);
}