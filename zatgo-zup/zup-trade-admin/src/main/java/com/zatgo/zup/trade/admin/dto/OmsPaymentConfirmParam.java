package com.zatgo.zup.trade.admin.dto;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * 确认订单支付状态
 * @Author wangyucong
 * @Date 2019/3/4 14:56
 */
@ApiModel("订单支付状态确认")
public class OmsPaymentConfirmParam {

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    private Long orderId;

    /**
     * 支付金额
     */
    @ApiModelProperty(value = "支付金额", required = true)
    private BigDecimal paidMoney;

    /**
     * 支付备注
     */
    @ApiModelProperty(value = "支付备注", required = false)
    private String note;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getPaidMoney() {
        return paidMoney;
    }

    public void setPaidMoney(BigDecimal paidMoney) {
        this.paidMoney = paidMoney;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}