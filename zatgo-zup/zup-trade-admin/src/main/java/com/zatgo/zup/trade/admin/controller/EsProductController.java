package com.zatgo.zup.trade.admin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.service.EsProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Author wangyucong
 * @Date 2019/2/27 9:51
 */
@RestController
@Api(tags = "EsProductController", description = "ES产品管理")
@RequestMapping("/trade/admin/esProduct")
public class EsProductController {

    private static final Logger logger = LoggerFactory.getLogger(EsProductController.class);

    @Autowired
    private EsProductService esProductService;

    @ApiOperation(value = "导入所有数据库中商品到ES")
    @RequestMapping(value = "/importAll", method = RequestMethod.GET)
    public Object importAll(){
        int count = esProductService.importAll();
        return new CommonResult().success(count);
    }
}