package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.trade.admin.dto.CmsSubjectData;

public interface CmsSubjectDao {
	
	List<CmsSubjectData> selectCmsSubjectList(@Param("keyword") String keyword, @Param("businessType") Byte businessType);

}
