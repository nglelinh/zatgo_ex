package com.zatgo.zup.trade.admin.service.impl;

import com.github.pagehelper.PageHelper;
import com.ykb.mall.common.enumType.BusinessEnum;
import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.*;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.trade.admin.dao.*;
import com.zatgo.zup.trade.admin.dto.PmsProductParam;
import com.zatgo.zup.trade.admin.dto.PmsProductQueryParam;
import com.zatgo.zup.trade.admin.dto.PmsProductResult;
import com.zatgo.zup.trade.admin.mapper.*;
import com.zatgo.zup.trade.admin.service.PmsProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 商品管理Service实现类
 * Created by chen on 2018/4/26.
 */
@Service
public class PmsProductServiceImpl implements PmsProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PmsProductServiceImpl.class);
    @Autowired
    private PmsProductMapper productMapper;
    @Autowired
    private PmsMemberPriceDao memberPriceDao;
    @Autowired
    private PmsMemberPriceMapper memberPriceMapper;
    @Autowired
    private PmsProductLadderDao productLadderDao;
    @Autowired
    private PmsProductLadderMapper productLadderMapper;
    @Autowired
    private PmsProductFullReductionDao productFullReductionDao;
    @Autowired
    private PmsProductFullReductionMapper productFullReductionMapper;
    @Autowired
    private PmsSkuStockDao skuStockDao;
    @Autowired
    private PmsSkuStockMapper skuStockMapper;
    @Autowired
    private PmsProductAttributeValueDao productAttributeValueDao;
    @Autowired
    private PmsProductAttributeValueMapper productAttributeValueMapper;
    @Autowired
    private CmsSubjectProductRelationDao subjectProductRelationDao;
    @Autowired
    private CmsSubjectProductRelationMapper subjectProductRelationMapper;
    @Autowired
    private CmsPrefrenceAreaProductRelationDao prefrenceAreaProductRelationDao;
    @Autowired
    private CmsPrefrenceAreaProductRelationMapper prefrenceAreaProductRelationMapper;
    @Autowired
    private PmsProductDao productDao;
    @Autowired
    private PmsProductVertifyRecordDao productVertifyRecordDao;
    @Autowired
    private PmsProductRelationDao pmsProductRelationDao;
    @Autowired
    private PmsProductRelationMapper pmsProductRelationMapper;
    @Autowired
    private PmsProductCategoryMapper pmsProductCategoryMapper;
    @Autowired
    private PmsProductFlightMapper pmsProductFlightMapper;
    @Autowired
    private PmsCommonProductSkuMapper pmsCommonProductSkuMapper;
    @Autowired
    private PmsCommonProductCategoryMapper pmsCommonProductCategoryMapper;
    @Autowired
    private ProductActivityArticleMapper productActivityArticleMapper;

    @Override
    @Transactional
    public int create(PmsProductParam productParam) {
        int count;
        if (!StringUtils.isEmpty(productParam.getDayNight()) && !productParam.getDayNight().contains(",")) {
            throw new BusinessException(BusinessExceptionCode.ERROR_AMOUNT_OF_MONEY, "产品出行时间设置出错");
        }
        String activityId = productParam.getActivityId();
        Byte activityType = productParam.getActivityType();
        //创建商品
        PmsProduct product = productParam;
        Integer newStatus = productParam.getNewStatus();
        Integer discountStatus = productParam.getDiscountStatus();
        Integer recommandStatus = productParam.getRecommandStatus();
        String activityName = productParam.getActivityName();
        product.setNewStatus(newStatus == null ? 0 : newStatus);
        product.setDiscountStatus(discountStatus == null ? 0 : discountStatus);
        product.setRecommandStatus(recommandStatus == null ? 0 : recommandStatus);
        product.setId(null);
        productMapper.insertSelective(product);
        //根据促销类型设置价格：、阶梯价格、满减价格
        Long productId = product.getId();
        //会员价格
        relateAndInsertList(memberPriceDao, productParam.getMemberPriceList(), productId);
        //阶梯价格
        relateAndInsertList(productLadderDao, productParam.getProductLadderList(), productId);
        //满减价格
        relateAndInsertList(productFullReductionDao, productParam.getProductFullReductionList(), productId);
        //处理sku的编码
        handleSkuStockCode(productParam.getSkuStockList(),productId);
        //添加sku库存信息
        relateAndInsertList(skuStockDao, productParam.getSkuStockList(), productId);
        //保存sku
        saveSku(productParam.getSkuList(), productId);
        //添加商品参数,添加自定义商品规格
        relateAndInsertList(productAttributeValueDao, productParam.getProductAttributeValueList(), productId);
        //关联专题
        relateAndInsertList(subjectProductRelationDao, productParam.getSubjectProductRelationList(), productId);
        //关联优选
        relateAndInsertList(prefrenceAreaProductRelationDao, productParam.getPrefrenceAreaProductRelationList(), productId);
        //文章关联活动
        if (BusinessEnum.BusinessType.ARTICLE.getCode().equals(productParam.getBusinessType())){
            saveActivityArticle(activityId, productId + "", activityType, activityName);
        }
        // 产品关联
//        relateAndInsertList(pmsProductRelationDao, productParam.getPmsProductRelationList(), productId);
        List<PmsProductRelation> pmsProductRelationList = productParam.getPmsProductRelationList();
        if(!CollectionUtils.isEmpty(pmsProductRelationList)) {
        	for (PmsProductRelation relation : pmsProductRelationList) {
        		relation.setProductId(productId);
        		pmsProductRelationMapper.insertSelective(relation);
			}
        }
        count = 1;
        return count;
    }

    private void saveSku(List<PmsCommonProductSku> skuList, Long productId) {
        if (CollectionUtils.isEmpty(skuList))
            return;
        Date date = new Date();
        for (PmsCommonProductSku pmsCommonProductSku : skuList) {
            pmsCommonProductSku.setId(UUIDUtils.getUuid());
            pmsCommonProductSku.setCreateTime(date);
            pmsCommonProductSku.setUpdateTime(date);
            pmsCommonProductSku.setProductId(productId + "");
            pmsCommonProductSkuMapper.insertSelective(pmsCommonProductSku);
        }
    }


    private void handleSkuStockCode(List<PmsSkuStock> skuStockList, Long productId) {
        if(CollectionUtils.isEmpty(skuStockList))return;
        for(int i=0;i<skuStockList.size();i++){
            PmsSkuStock skuStock = skuStockList.get(i);
            if(StringUtils.isEmpty(skuStock.getSkuCode())){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                StringBuilder sb = new StringBuilder();
                //日期
                sb.append(sdf.format(new Date()));
                //四位商品id
                sb.append(String.format("%04d", productId));
                //三位索引id
                sb.append(String.format("%03d", i+1));
                skuStock.setSkuCode(sb.toString());
            }
        }
    }

    @Override
    public PmsProductResult getUpdateInfo(Long id) {
        PmsProductResult updateInfo = productDao.getUpdateInfo(id);
        PmsCommonProductSkuExample example = new PmsCommonProductSkuExample();
        example.createCriteria().andProductIdEqualTo(id + "");
        List<PmsCommonProductSku> pmsCommonProductSkus = pmsCommonProductSkuMapper.selectByExample(example);
        updateInfo.setSkuList(pmsCommonProductSkus);
        return updateInfo;
    }

    @Override
    public int update(Long id, PmsProductParam productParam) {
        int count;
        if (!StringUtils.isEmpty(productParam.getDayNight()) && !productParam.getDayNight().contains(",")) {
            throw new BusinessException(BusinessExceptionCode.ERROR_AMOUNT_OF_MONEY, "产品出行时间设置出错");
        }
        
        if(productParam.getProductCategoryId() != null && !BusinessEnum.BusinessType.COMMON.getCode().equals(productParam.getBusinessType())) {
        	PmsProductCategory category = pmsProductCategoryMapper.selectByPrimaryKey(productParam.getProductCategoryId());
        	if(category != null) {
        		productParam.setProductCategoryName(category.getName());
        	}
        }

        if(productParam.getProductCategoryId() != null && BusinessEnum.BusinessType.COMMON.getCode().equals(productParam.getBusinessType())) {
            PmsCommonProductCategory pmsCommonProductCategory = pmsCommonProductCategoryMapper.selectByPrimaryKey(productParam.getProductCommonCategoryId());
            if(pmsCommonProductCategory != null) {
                productParam.setProductCategoryName(pmsCommonProductCategory.getName());
            }
        }

        String activityId = productParam.getActivityId();
        Byte activityType = productParam.getActivityType();
        String activityName = productParam.getActivityName();
        //更新商品信息
        PmsProduct product = productParam;
        product.setId(id);
        productMapper.updateByPrimaryKeySelective(product);
        //会员价格
        PmsMemberPriceExample pmsMemberPriceExample = new PmsMemberPriceExample();
        pmsMemberPriceExample.createCriteria().andProductIdEqualTo(id);
        memberPriceMapper.deleteByExample(pmsMemberPriceExample);
        relateAndInsertList(memberPriceDao, productParam.getMemberPriceList(), id);
        //阶梯价格
        PmsProductLadderExample ladderExample = new PmsProductLadderExample();
        ladderExample.createCriteria().andProductIdEqualTo(id);
        productLadderMapper.deleteByExample(ladderExample);
        relateAndInsertList(productLadderDao, productParam.getProductLadderList(), id);
        //满减价格
        PmsProductFullReductionExample fullReductionExample = new PmsProductFullReductionExample();
        fullReductionExample.createCriteria().andProductIdEqualTo(id);
        productFullReductionMapper.deleteByExample(fullReductionExample);
        relateAndInsertList(productFullReductionDao, productParam.getProductFullReductionList(), id);
        //修改sku库存信息
        PmsSkuStockExample skuStockExample = new PmsSkuStockExample();
        skuStockExample.createCriteria().andProductIdEqualTo(id);
        skuStockMapper.deleteByExample(skuStockExample);
        handleSkuStockCode(productParam.getSkuStockList(),id);
        relateAndInsertList(skuStockDao, productParam.getSkuStockList(), id);
        //修改商品参数,添加自定义商品规格
        PmsProductAttributeValueExample productAttributeValueExample = new PmsProductAttributeValueExample();
        productAttributeValueExample.createCriteria().andProductIdEqualTo(id);
        productAttributeValueMapper.deleteByExample(productAttributeValueExample);
        relateAndInsertList(productAttributeValueDao, productParam.getProductAttributeValueList(), id);
        //关联专题
        CmsSubjectProductRelationExample subjectProductRelationExample = new CmsSubjectProductRelationExample();
        subjectProductRelationExample.createCriteria().andProductIdEqualTo(id);
        subjectProductRelationMapper.deleteByExample(subjectProductRelationExample);
        relateAndInsertList(subjectProductRelationDao, productParam.getSubjectProductRelationList(), id);
        //关联优选
        CmsPrefrenceAreaProductRelationExample prefrenceAreaExample = new CmsPrefrenceAreaProductRelationExample();
        prefrenceAreaExample.createCriteria().andProductIdEqualTo(id);
        prefrenceAreaProductRelationMapper.deleteByExample(prefrenceAreaExample);
        relateAndInsertList(prefrenceAreaProductRelationDao, productParam.getPrefrenceAreaProductRelationList(), id);
        
        PmsProductRelationExample pmsProductRelationExample = new PmsProductRelationExample();
        pmsProductRelationExample.createCriteria().andProductIdEqualTo(id);
        pmsProductRelationMapper.deleteByExample(pmsProductRelationExample);
        //文章关联活动
        if (BusinessEnum.BusinessType.ARTICLE.getCode().equals(productParam.getBusinessType())){
            saveActivityArticle(activityId, id + "", activityType, activityName);
        }

        //修改sku
        updateSkuInfo(productParam.getSkuList(), id);
//        relateAndInsertList(pmsProductRelationDao, productParam.getPmsProductRelationList(), id);
        List<PmsProductRelation> pmsProductRelationList = productParam.getPmsProductRelationList();
        if(!CollectionUtils.isEmpty(pmsProductRelationList)) {
        	for (PmsProductRelation relation : pmsProductRelationList) {
        		relation.setProductId(id);
        		pmsProductRelationMapper.insertSelective(relation);
			}
        }
        count = 1;
        return count;
    }

    /**
     * 分页查询产品
     * @param productQueryParam
     * @param pageSize
     * @param pageNum
     * @return
     */
    @Override
    public List<PmsProduct> list(PmsProductQueryParam productQueryParam, Integer pageSize, Integer pageNum) {
        Byte businessType = productQueryParam.getBusinessType();
        Integer type = productQueryParam.getType();
        Boolean special = productQueryParam.getSpecial();
        if (null == businessType) {
            businessType = BusinessEnum.BusinessType.TRAVEL.getCode();
        }
        PageHelper.startPage(pageNum, pageSize);
        PmsProductExample productExample = new PmsProductExample();
        productExample.setOrderByClause("id desc");
        PmsProductExample.Criteria criteria = productExample.createCriteria();
        criteria.andDeleteStatusEqualTo(0);
        criteria.andBusinessTypeEqualTo(businessType);
        if (type != null){
            if (special == null){
                special = false;
            }
            if (type.intValue() == 1){
                criteria.andRecommandStatusEqualTo(special ? 1 : 0);
            }
            if (type.intValue() == 2){
                criteria.andDiscountStatusEqualTo(special ? 1 : 0);
            }
            if (type.intValue() == 3){
                criteria.andNewStatusEqualTo(special ? 1 : 0);
            }
        }
        if (productQueryParam.getPublishStatus() != null) {
            criteria.andPublishStatusEqualTo(productQueryParam.getPublishStatus());
        }
        if (productQueryParam.getVerifyStatus() != null) {
            criteria.andVerifyStatusEqualTo(productQueryParam.getVerifyStatus());
        }
        if (!StringUtils.isEmpty(productQueryParam.getKeyword())) {
            criteria.andNameLike("%" + productQueryParam.getKeyword() + "%");
        }
        if (!StringUtils.isEmpty(productQueryParam.getProductSn())) {
            criteria.andProductSnEqualTo(productQueryParam.getProductSn());
        }
        if (productQueryParam.getBrandId() != null) {
            criteria.andBrandIdEqualTo(productQueryParam.getBrandId());
        }
        if (productQueryParam.getProductCategoryId() != null) {
            criteria.andProductCategoryIdEqualTo(productQueryParam.getProductCategoryId());
        }
        List<PmsProduct> pmsProducts = productMapper.selectByExample(productExample);
        List<Long> ids = new ArrayList<>();
        for (PmsProduct pmsProduct : pmsProducts) {
            Long id = pmsProduct.getId();
            ids.add(id);
        }
       if (!CollectionUtils.isEmpty(ids)){
           PmsProductFlightExample example = new PmsProductFlightExample();
           example.createCriteria().andProductIdIn(ids);
           List<PmsProductFlight> pmsProductFlights = pmsProductFlightMapper.selectByExample(example);
           Map<Long, PmsProductFlight> map = new HashMap<>();
           for (PmsProductFlight pmsProductFlight : pmsProductFlights) {
               Long productId = pmsProductFlight.getProductId();
               map.put(productId, pmsProductFlight);
           }
           for (PmsProduct pmsProduct : pmsProducts) {
               Long id = pmsProduct.getId();
               PmsProductFlight pmsProductFlight = map.get(id);
               if (pmsProductFlight != null){
                   pmsProduct.setFlightId(pmsProductFlight.getId());
               }
           }
       }
        return pmsProducts;
    }

    @Override
    public int updateVerifyStatus(List<Long> ids, Integer verifyStatus, String detail) {
        PmsProduct product = new PmsProduct();
        product.setVerifyStatus(verifyStatus);
        PmsProductExample example = new PmsProductExample();
        example.createCriteria().andIdIn(ids);
        List<PmsProductVertifyRecord> list = new ArrayList<>();
        int count = productMapper.updateByExampleSelective(product, example);
        //修改完审核状态后插入审核记录
        for (Long id : ids) {
            PmsProductVertifyRecord record = new PmsProductVertifyRecord();
            record.setProductId(id);
            record.setCreateTime(new Date());
            record.setDetail(detail);
            record.setStatus(verifyStatus);
            record.setVertifyMan("test");
            list.add(record);
        }
        productVertifyRecordDao.insertList(list);
        return count;
    }

    @Override
    public int updatePublishStatus(List<Long> ids, Integer publishStatus) {
        PmsProduct record = new PmsProduct();
        record.setPublishStatus(publishStatus);
        PmsProductExample example = new PmsProductExample();
        example.createCriteria().andIdIn(ids);
        return productMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateRecommendStatus(List<Long> ids, Integer recommendStatus) {
        PmsProduct record = new PmsProduct();
        record.setRecommandStatus(recommendStatus);
        PmsProductExample example = new PmsProductExample();
        example.createCriteria().andIdIn(ids);
        return productMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateNewStatus(List<Long> ids, Integer newStatus) {
        PmsProduct record = new PmsProduct();
        record.setNewStatus(newStatus);
        PmsProductExample example = new PmsProductExample();
        example.createCriteria().andIdIn(ids);
        return productMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateDeleteStatus(List<Long> ids, Integer deleteStatus) {
        PmsProduct record = new PmsProduct();
        record.setDeleteStatus(deleteStatus);
        PmsProductExample example = new PmsProductExample();
        example.createCriteria().andIdIn(ids);
        return productMapper.updateByExampleSelective(record, example);
    }

    @Override
    public List<PmsProduct> list(String keyword, Byte businessType) {
        PmsProductExample productExample = new PmsProductExample();
        PmsProductExample.Criteria criteria = productExample.createCriteria();
        criteria.andDeleteStatusEqualTo(0);
        criteria.andBusinessTypeEqualTo(businessType);
        if(!StringUtils.isEmpty(keyword)){
            criteria.andNameLike("%" + keyword + "%");
            productExample.or().andDeleteStatusEqualTo(0).andProductSnLike("%" + keyword + "%");
        }
        return productMapper.selectByExample(productExample);
    }

    @Override
    public List<PmsProduct> listByIds(List<Long> ids) {
        PmsProductExample productExample = new PmsProductExample();
        PmsProductExample.Criteria criteria = productExample.createCriteria();
        criteria.andDeleteStatusEqualTo(0);
        criteria.andIdIn(ids);
        criteria.andPublishStatusEqualTo(1);
        return productMapper.selectByExample(productExample);
    }

    /**
     * @deprecated 旧版创建
     */
    public int createOld(PmsProductParam productParam) {
        int count;
        //创建商品
        PmsProduct product = productParam;
        product.setId(null);
        productMapper.insertSelective(product);
        //根据促销类型设置价格：、阶梯价格、满减价格
        Long productId = product.getId();
        //会员价格
        List<PmsMemberPrice> memberPriceList = productParam.getMemberPriceList();
        if (!CollectionUtils.isEmpty(memberPriceList)) {
            for (PmsMemberPrice pmsMemberPrice : memberPriceList) {
                pmsMemberPrice.setId(null);
                pmsMemberPrice.setProductId(productId);
            }
            memberPriceDao.insertList(memberPriceList);
        }
        //阶梯价格
        List<PmsProductLadder> productLadderList = productParam.getProductLadderList();
        if (!CollectionUtils.isEmpty(productLadderList)) {
            for (PmsProductLadder productLadder : productLadderList) {
                productLadder.setId(null);
                productLadder.setProductId(productId);
            }
            productLadderDao.insertList(productLadderList);
        }
        //满减价格
        List<PmsProductFullReduction> productFullReductionList = productParam.getProductFullReductionList();
        if (!CollectionUtils.isEmpty(productFullReductionList)) {
            for (PmsProductFullReduction productFullReduction : productFullReductionList) {
                productFullReduction.setId(null);
                productFullReduction.setProductId(productId);
            }
            productFullReductionDao.insertList(productFullReductionList);
        }
        //添加sku库存信息
        List<PmsSkuStock> skuStockList = productParam.getSkuStockList();
        if (!CollectionUtils.isEmpty(skuStockList)) {
            for (PmsSkuStock skuStock : skuStockList) {
                skuStock.setId(null);
                skuStock.setProductId(productId);
            }
            skuStockDao.insertList(skuStockList);
        }
        //添加商品参数,添加自定义商品规格
        List<PmsProductAttributeValue> productAttributeValueList = productParam.getProductAttributeValueList();
        if (!CollectionUtils.isEmpty(productAttributeValueList)) {
            for (PmsProductAttributeValue productAttributeValue : productAttributeValueList) {
                productAttributeValue.setId(null);
                productAttributeValue.setProductId(productId);
            }
            productAttributeValueDao.insertList(productAttributeValueList);
        }
        //关联专题
        relateAndInsertList(subjectProductRelationDao, productParam.getSubjectProductRelationList(), productId);
        //关联优选
        relateAndInsertList(prefrenceAreaProductRelationDao, productParam.getPrefrenceAreaProductRelationList(), productId);
        count = 1;
        return count;
    }

    /**
     * 建立和插入关系表操作
     *
     * @param dao       可以操作的dao
     * @param dataList  要插入的数据
     * @param productId 建立关系的id
     */
    private void relateAndInsertList(Object dao, List dataList, Long productId) {
        try {
            if (CollectionUtils.isEmpty(dataList)) return;
            for (Object item : dataList) {
                Method setId = item.getClass().getMethod("setId", Long.class);
                setId.invoke(item, (Long) null);
                Method setProductId = item.getClass().getMethod("setProductId", Long.class);
                setProductId.invoke(item, productId);
            }
            Method insertList = dao.getClass().getMethod("insertList", List.class);
            insertList.invoke(dao, dataList);
        } catch (Exception e) {
            LOGGER.error("创建产品出错:{}",e);
            throw new RuntimeException(e.getMessage());
        }
    }

	@Override
	public Map<Byte, List<PmsProduct>> getRelationList() {
		
		Map<Byte, List<PmsProduct>> data = new HashMap<>();
		
		PmsProductExample example = new PmsProductExample();
		example.setOrderByClause(" id desc ");
		
		PmsProductExample.Criteria criteria = example.createCriteria();
		criteria.andDeleteStatusEqualTo(YesOrNo.NO.getCode().intValue());
		
		List<PmsProduct> list = productMapper.selectByExample(example);
		if(CollectionUtils.isEmpty(list)) {
			return data;
		}
		
		for (PmsProduct pmsProduct : list) {
			List<PmsProduct> products = data.get(pmsProduct.getBusinessType());
			if(products == null) {
				products = new ArrayList<>();
				data.put(pmsProduct.getBusinessType(), products);
			}
			
			products.add(pmsProduct);
		}
		
		return data;
	}

	@Override
	public Map<Byte, List<PmsProductRelation>> getRelationProductList(Long productId, Byte relationType) {
		
		Map<Byte, List<PmsProductRelation>> data = new HashMap<>();
		PmsProductRelationExample example = new PmsProductRelationExample();
		example.createCriteria().andProductIdEqualTo(productId).andRelationTypeEqualTo(relationType);
		List<PmsProductRelation> relations = pmsProductRelationMapper.selectByExample(example);
		if(CollectionUtils.isEmpty(relations)) {
			return data;
		}
		
		for (PmsProductRelation relation : relations) {
			List<PmsProductRelation> _relations = data.get(relation.getBusinessType());
			if(_relations == null) {
				_relations = new ArrayList<>();
				data.put(relation.getBusinessType(), _relations);
			}
			
			_relations.add(relation);
		}
		
		return data;
	}

	private void updateSkuInfo(List<PmsCommonProductSku> list, Long id){
        if (CollectionUtils.isEmpty(list))
            return;
        PmsCommonProductSku pmsCommonProductSku = list.get(0);
        if (id == null)
            return;
        PmsCommonProductSkuExample example = new PmsCommonProductSkuExample();
        example.createCriteria().andProductIdEqualTo(id + "");
        pmsCommonProductSkuMapper.deleteByExample(example);
        Date date = new Date();
        for (PmsCommonProductSku commonProductSku : list) {
            commonProductSku.setCreateTime(date);
            commonProductSku.setUpdateTime(date);
            commonProductSku.setId(UUIDUtils.getUuid());
            commonProductSku.setProductId(id + "");
            pmsCommonProductSkuMapper.insertSelective(commonProductSku);
        }
    }

    private void saveActivityArticle(String activityId, String productId, Byte activityType, String activityName){
        if (StringUtils.isEmpty(activityId) || StringUtils.isEmpty(productId))
            return;
        ProductActivityArticleExample example = new ProductActivityArticleExample();
        example.createCriteria().andActivityIdEqualTo(activityId).andProductIdEqualTo(productId);
        List<ProductActivityArticle> productActivityArticles = productActivityArticleMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(productActivityArticles)){
            ProductActivityArticle paa = new ProductActivityArticle();
            paa.setActivityId(activityId);
            paa.setCreateTime(new Date());
            paa.setId(UUIDUtils.getUuid());
            paa.setProductId(productId);
            paa.setType(activityType);
            paa.setActivityName(activityName);
            productActivityArticleMapper.insertSelective(paa);
        } else {
            ProductActivityArticle productActivityArticle = productActivityArticles.get(0);
            productActivityArticle.setActivityId(activityId);
            productActivityArticle.setType(activityType);
            productActivityArticle.setActivityName(activityName);
            productActivityArticleMapper.updateByPrimaryKeySelective(productActivityArticle);
        }
    }

}
