package com.zatgo.zup.trade.admin.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.OssCallbackResult;
import com.zatgo.zup.trade.admin.dto.OssPolicyResult;
import com.zatgo.zup.trade.admin.service.impl.OssServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Oss相关操作接口
 * Created by chen on 2018/4/26.
 */
@Controller
@Api(tags = "OssController",description = "Oss管理")
@RequestMapping("/trade/admin/aliyun/oss")
public class OssController {
	@Autowired
	private OssServiceImpl ossService;

	@ApiOperation(value = "oss上传签名生成")
	@RequestMapping(value = "/policy",method = RequestMethod.GET)
	@ResponseBody
	public Object policy() {
		OssPolicyResult result = ossService.policy();
		return new CommonResult().success(result);
	}

	@ApiOperation(value = "oss上传成功回调")
	@RequestMapping(value = "callback",method = RequestMethod.POST)
	@ResponseBody
	public Object callback(HttpServletRequest request) {
		OssCallbackResult ossCallbackResult = ossService.callback(request);
		return new CommonResult().success(ossCallbackResult);
	}

}
