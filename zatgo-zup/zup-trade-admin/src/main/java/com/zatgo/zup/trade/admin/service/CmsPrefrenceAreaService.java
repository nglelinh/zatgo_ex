package com.zatgo.zup.trade.admin.service;

import java.util.List;

import com.ykb.mall.common.model.CmsPrefrenceArea;

/**
 * 优选专区Service
 * Created by chen on 2018/6/1.
 */
public interface CmsPrefrenceAreaService {
    List<CmsPrefrenceArea> listAll();
}
