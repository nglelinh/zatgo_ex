package com.zatgo.zup.trade.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ykb.mall.common.model.UmsAdmin;
import com.ykb.mall.common.model.UmsMemberWeixin;
import com.ykb.mall.common.model.UmsMemberWeixinExample;
import com.zatgo.zup.trade.admin.mapper.UmsMemberWeixinMapper;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.service.UmsAdminService;
//import com.zatgo.zup.trade.admin.util.JwtTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by 46041 on 2019/5/15.
 */

@RestController
@Api(tags = "ThirdGetDataController", description = "三方获取数据")
@RequestMapping("/trade/admin/third")
public class ThirdGetDataController {

    private static final Logger logger = LoggerFactory.getLogger(ThirdGetDataController.class);

//    @Autowired
//    private JwtTokenUtil jwtTokenUtil;
//    @Autowired
//    private UmsAdminService adminService;
//    @Autowired
//    private UmsMemberWeixinMapper memberWeixinMapper;
    @Autowired
    private UmsMemberWeixinMapper umsMemberWeixinMapper;


//    @Value("${jwt.tokenHead}")
//    private String tokenHead;
//
//    @Value("${jwt.tokenHeader}")
//    private String tokenHeader;



    @ApiOperation("根据微信openId获取用户信息")
    @RequestMapping(value = "/get/userInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object getUserInfo(@RequestParam("wxOpenId") String wxOpenId){
        UmsMemberWeixinExample example = new UmsMemberWeixinExample();
        example.createCriteria().andOpenIdEqualTo(wxOpenId);
        List<UmsMemberWeixin> umsMemberWeixins = umsMemberWeixinMapper.selectByExample(example);
        return new CommonResult().success(CollectionUtils.isEmpty(umsMemberWeixins) ? null : umsMemberWeixins.get(0));
    }


//    @PostMapping("/login")
//    @ApiOperation("三方登陆")
//    public Object thirdLogin(HttpServletRequest request){
//        CommonResult result = new CommonResult();
//        String token = request.getHeader(tokenHeader);
//        try{
//            if (StringUtils.isEmpty(token) || !token.startsWith(this.tokenHead)) {
//                return new CommonResult().failed("token不可为空");
//            }
//            String authToken = token.substring(this.tokenHead.length());
//            String username = jwtTokenUtil.getUserNameFromToken(authToken);
//            UmsAdmin member = adminService.getAdminByUsername(username);
//
//            ThirdLoginInfo info = new ThirdLoginInfo();
//            info.setUserId(member.getId() + "");
//            info.setUserName(member.getUsername());
//            info.setNickName(member.getNickName());
//            info.setIcon(member.getIcon());
//            result.success(info);
//        } catch (Exception e){
//            logger.error("", e);
//            result.failed("未知异常");
//        }
//        return result;
//    }
}
