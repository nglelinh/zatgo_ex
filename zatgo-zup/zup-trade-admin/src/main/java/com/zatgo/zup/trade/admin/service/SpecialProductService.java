package com.zatgo.zup.trade.admin.service;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.model.SpecialProductListRequest;
import com.zatgo.zup.common.model.SpecialproductListResponse;
import com.zatgo.zup.common.model.UpdateSpecialProductRequest;

import java.util.List;

/**
 * Created by 46041 on 2019/7/18.
 */
public interface SpecialProductService {
    JSONObject list(SpecialProductListRequest request);

    void update(UpdateSpecialProductRequest request);

}
