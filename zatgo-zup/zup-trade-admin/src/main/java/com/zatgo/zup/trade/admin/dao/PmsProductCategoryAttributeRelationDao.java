package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ykb.mall.common.model.PmsProductCategoryAttributeRelation;

/**
 * 自定义商品分类和属性关系Dao
 * Created by chen on 2018/5/23.
 */
public interface PmsProductCategoryAttributeRelationDao {
    int insertList(@Param("list") List<PmsProductCategoryAttributeRelation> productCategoryAttributeRelationList);

    int insertCommonProductCategoryList(@Param("list") List<PmsProductCategoryAttributeRelation> productCategoryAttributeRelationList);
}
