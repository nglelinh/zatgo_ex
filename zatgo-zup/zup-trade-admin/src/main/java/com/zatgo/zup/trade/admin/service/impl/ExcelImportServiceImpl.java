package com.zatgo.zup.trade.admin.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsHotelData;
import com.zatgo.zup.trade.admin.service.ExcelImportService;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.ykb.mall.common.utils.DateTimeUtils;

@Service
public class ExcelImportServiceImpl implements ExcelImportService {

    private static final Logger LOG = LoggerFactory.getLogger(ExcelImportServiceImpl.class);

    private static final String[] COLUMNS = { "hotelName", "roomType", "guestNumber", "startDate", "endDate", "weekdayPrice",
            "weekendPrice", "stock" };

    @Override
    public CommonResult excelImportHotel(MultipartFile file) {
        Map<String, List<PmsHotelData>> priceDatas = new LinkedHashMap<>();
        try {
            Workbook workbook = readExcel(file);
            // 用来存放表中数据
            List<Map<String, String>> list = new ArrayList<Map<String, String>>();
            // 获取第一个sheet
            Sheet sheet = workbook.getSheetAt(0);
            // 获取最大行数
            int rownum = sheet.getPhysicalNumberOfRows();
            // 获取第一行
            Row row = sheet.getRow(0);
            // 获取最大列数
            int colnum = row.getPhysicalNumberOfCells();

            for (int i = 1; i < rownum; i++) {
                Map<String, String> map = new LinkedHashMap<String, String>();
                row = sheet.getRow(i);
                if (row != null) {
                    for (int j = 0; j < colnum; j++) {
                        String cellData = (String) getCellFormatValue(row.getCell(j));
                        map.put(COLUMNS[j], cellData);
                    }
                } else {
                    break;
                }
                list.add(map);
            }

            for (Map<String, String> data : list) {
                DateTime startDate = DateTime.parse(data.get("startDate"));
                int diff = 0;
                String endDate = data.get("endDate");

                if(StringUtils.isNotBlank(endDate)) {
                    diff = Days.daysBetween(startDate, DateTime.parse(endDate)).getDays();
                }

                for (int i = 0; i <= diff; i++) {
                    DateTime arrivalDate = startDate.plusDays(i);
                    int week = arrivalDate.getDayOfWeek();

                    PmsHotelData priceData = new PmsHotelData();
                    if (week < 6 || StringUtils.isEmpty(data.get("weekendPrice"))) {
                        priceData.setSalePrice(new BigDecimal(data.get("weekdayPrice")));
                    } else {
                        priceData.setSalePrice(new BigDecimal(data.get("weekendPrice")));
                    }

                    String arrivalDateStr = arrivalDate.toString(DateTimeUtils.PATTEN_YYYY_MM_DD);
                    priceData.setHotelName(data.get("hotelName"));
                    priceData.setRoomType(data.get("roomType"));
                    priceData.setGuestNumber(Integer.valueOf(data.get("guestNumber")));
                    priceData.setArrivalDate(arrivalDateStr);
                    priceData.setStatus((byte)0);
                    priceData.setDeleteStatus(YesOrNo.NO.getCode());
                    priceData.setStock(Integer.valueOf(data.get("stock")));
                    List<PmsHotelData> priceList = priceDatas.get(arrivalDateStr);
                    if(priceList == null) {
                        priceList = new ArrayList<>();
                        priceDatas.put(arrivalDateStr, priceList);
                    }

                    priceList.add(priceData);
                }
            }

        } catch (IOException e) {
            LOG.error("解析Excel失败：", e);
        }
        return new CommonResult().success(priceDatas);
    }

    public Workbook readExcel(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));

        if (".xls".equals(suffix)) {
            return new HSSFWorkbook(file.getInputStream());
        } else if (".xlsx".equals(suffix)) {
            return new XSSFWorkbook(file.getInputStream());
        } else {
            return null;
        }
    }

    public static Object getCellFormatValue(Cell cell) {
        Object cellValue = null;
        if (cell != null) {
            // 判断cell类型
            switch (cell.getCellType()) {
                case NUMERIC: {
                    cellValue = BigDecimal.valueOf(cell.getNumericCellValue()).stripTrailingZeros().toPlainString();
                    break;
                }
                case FORMULA: {
                    // 判断cell是否为日期格式
                    if (DateUtil.isCellDateFormatted(cell)) {
                        // 转换为日期格式YYYY-mm-dd
                        cellValue = cell.getDateCellValue();
                    } else {
                        // 数字
                        cellValue = String.valueOf(cell.getNumericCellValue());
                    }
                    break;
                }
                case STRING: {
                    cellValue = cell.getRichStringCellValue().getString();
                    break;
                }
                default:
                    cellValue = "";
            }
        } else {
            cellValue = "";
        }
        return cellValue;
    }

}
