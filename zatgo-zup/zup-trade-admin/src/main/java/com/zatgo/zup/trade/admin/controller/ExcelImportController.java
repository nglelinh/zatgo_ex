package com.zatgo.zup.trade.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.service.ExcelImportService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "ExcelImportController", description = "Excel导入")
@RequestMapping("/trade/admin/excel")
public class ExcelImportController {
	
	@Autowired
	private ExcelImportService excelImportService;
	
	@ApiOperation("导入酒店信息")
	@PostMapping(value = "/hotel")
	public CommonResult excelImportHotel(MultipartFile file) {
		
		return excelImportService.excelImportHotel(file);
	}

}
