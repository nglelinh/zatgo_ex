package com.zatgo.zup.trade.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsProductArticleParam;
import com.zatgo.zup.trade.admin.dto.PmsProductArticleResult;
import com.zatgo.zup.trade.admin.service.PmsProductArticleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 文章管理Controller
 * Created by chen on 2018/4/26.
 */
@Controller
@Api(tags = "PmsProductArticleController", description = "文章管理")
@RequestMapping("/trade/admin/article")
public class PmsProductArticleController {
    @Autowired
    private PmsProductArticleService articleService;

    @ApiOperation("创建文章")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    //@PreAuthorize("hasAuthority('pms:product:create')")
    public Object create(@RequestBody PmsProductArticleParam articleParam, BindingResult bindingResult) {
        int count = articleService.create(articleParam);
        if (count > 0) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation("获取文章详情")
    @RequestMapping(value = "/updateInfo/{id}", method = RequestMethod.GET)
    @ResponseBody
    //@PreAuthorize("hasAuthority('pms:product:read')")
    public Object getUpdateInfo(@PathVariable Long id) {
    	PmsProductArticleResult articleResult = articleService.getUpdateInfo(id);
        return new CommonResult().success(articleResult);
    }

    @ApiOperation("更新商品")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    //@PreAuthorize("hasAuthority('pms:product:update')")
    public Object update(@PathVariable Long id, @RequestBody PmsProductArticleParam articleParam, BindingResult bindingResult) {
        int count = articleService.update(id, articleParam);
        if (count > 0) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }
}
