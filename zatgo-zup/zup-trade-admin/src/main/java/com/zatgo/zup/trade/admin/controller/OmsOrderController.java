package com.zatgo.zup.trade.admin.controller;

import com.ykb.mall.common.model.OmsOrder;
import com.zatgo.zup.common.model.wx.WechatRefundRequest;
import com.zatgo.zup.trade.admin.dto.*;
import com.zatgo.zup.trade.admin.remoteService.WechatRemoteService;
import com.zatgo.zup.trade.admin.service.OmsOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单管理Controller
 * Created by chen on 2018/10/11.
 */
@Controller
@Api(tags = "OmsOrderController", description = "订单管理")
@RequestMapping("/trade/admin/order")
public class OmsOrderController {

    private static final Logger logger = LoggerFactory.getLogger(OmsOrderController.class);


    @Autowired
    private OmsOrderService orderService;
    @Autowired
    private WechatRemoteService wechatRemoteService;

    @ApiOperation("查询订单")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(OmsOrderQueryParam queryParam,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        List<OmsOrder> orderList = orderService.list(queryParam, pageSize, pageNum);
        return new CommonResult().pageSuccess(orderList);
    }

    @ApiOperation("批量发货")
    @RequestMapping(value = "/update/delivery", method = RequestMethod.POST)
    @ResponseBody
    public Object delivery(@RequestBody List<OmsOrderDeliveryParam> deliveryParamList) {
        int count = orderService.delivery(deliveryParamList);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("批量关闭订单")
    @RequestMapping(value = "/update/close", method = RequestMethod.POST)
    @ResponseBody
    public Object close(@RequestParam("ids") List<Long> ids, @RequestParam String note) {
        int count = orderService.close(ids,note);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("批量删除订单")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public Object delete(@RequestParam("ids") List<Long> ids) {
        int count = orderService.delete(ids);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("获取订单详情:订单信息、商品信息、操作记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object detail(@PathVariable Long id) {
        OmsOrderDetail orderDetailResult = orderService.detail(id);
        return new CommonResult().success(orderDetailResult);
    }

    @ApiOperation("修改收货人信息")
    @RequestMapping(value = "/update/receiverInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateReceiverInfo(@RequestBody OmsReceiverInfoParam receiverInfoParam) {
        int count = orderService.updateReceiverInfo(receiverInfoParam);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("修改订单费用信息")
    @RequestMapping(value = "/update/moneyInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateReceiverInfo(@RequestBody OmsMoneyInfoParam moneyInfoParam) {
        int count = orderService.updateMoneyInfo(moneyInfoParam);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("备注订单")
    @RequestMapping(value = "/update/note", method = RequestMethod.POST)
    @ResponseBody
    public Object updateNote(@RequestParam("id") Long id,
                             @RequestParam("note") String note,
                             @RequestParam("status") Integer status) {
        int count = orderService.updateNote(id,note,status);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("确认订单支付状态")
    @RequestMapping(value = "/confirm/payment", method = RequestMethod.POST)
    @ResponseBody
    public Object confirmPayment(@RequestBody OmsPaymentConfirmParam param) {
        int count = orderService.confirmPayment(param);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("材料办理完成")
    @RequestMapping(value = "/handle/finish", method = RequestMethod.POST)
    @ResponseBody
    public Object handleFinish(@RequestBody OmsHandleFinishParam param) {
        int count = orderService.handleFinish(param);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("协议签署完成")
    @RequestMapping(value = "/sign/agreement", method = RequestMethod.POST)
    @ResponseBody
    public Object signAgreement(@RequestBody OmsSignParam param) {
        int count = orderService.signAgreement(param);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("已完成订单")
    @RequestMapping(value = "/complete", method = RequestMethod.POST)
    @ResponseBody
    public Object completeOrder(@RequestBody OmsHasCompleted param) {
        int count = orderService.completeOrder(param);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("订单关闭")
    @RequestMapping(value = "/close", method = RequestMethod.POST)
    @ResponseBody
    public Object closeOmsOrder(@RequestBody OmsCloseParam param) {
        int count = orderService.closeOmsOrder(param);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("订单金额退款")
    @RequestMapping(value = "/refund/price", method = RequestMethod.POST)
    @ResponseBody
    public Object refundOmsPrice(@RequestBody OmsRefundInfoParam param) {
        int count = orderService.refundOmsPrice(param);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("微信退款")
    @RequestMapping(value = "/wx/refund/price", method = RequestMethod.POST)
    @ResponseBody
    public Object wxRefundOmsPrice(@RequestBody OmsRefundInfoParam param) {
        try {
            WechatRefundRequest request = new WechatRefundRequest();
            request.setRefundAmount(param.getRefundPrice());
            request.setOrderNumber(param.getOrderId() + "");
            wechatRemoteService.weixinRefund(request);
            return new CommonResult().success();
        } catch (Exception e) {
            logger.error("", e);
            return new CommonResult().failed();
        }
    }
}
