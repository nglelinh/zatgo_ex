package com.zatgo.zup.trade.admin.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.zatgo.zup.trade.admin.dto.EsProduct;

/**
 * @Author wangyucong
 * @Date 2019/2/26 17:07
 */
public interface EsProductRepository extends ElasticsearchRepository<EsProduct, Long> {
}
