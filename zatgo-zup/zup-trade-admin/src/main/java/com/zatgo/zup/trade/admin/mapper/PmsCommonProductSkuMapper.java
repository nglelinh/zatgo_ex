package com.zatgo.zup.trade.admin.mapper;

import java.util.List;

import com.ykb.mall.common.model.PmsCommonProductSku;
import com.ykb.mall.common.model.PmsCommonProductSkuExample;
import org.apache.ibatis.annotations.Param;

public interface PmsCommonProductSkuMapper {
    int countByExample(PmsCommonProductSkuExample example);

    int deleteByExample(PmsCommonProductSkuExample example);

    int deleteByPrimaryKey(String id);

    int insert(PmsCommonProductSku record);

    int insertSelective(PmsCommonProductSku record);

    List<PmsCommonProductSku> selectByExample(PmsCommonProductSkuExample example);

    PmsCommonProductSku selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") PmsCommonProductSku record, @Param("example") PmsCommonProductSkuExample example);

    int updateByExample(@Param("record") PmsCommonProductSku record, @Param("example") PmsCommonProductSkuExample example);

    int updateByPrimaryKeySelective(PmsCommonProductSku record);

    int updateByPrimaryKey(PmsCommonProductSku record);

    int updateStock(@Param("skuId") String skuId,@Param("num") Integer num);
}