package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.trade.admin.dto.SmsFlashPromotionProduct;

/**
 * 限时购商品关联自定义Dao
 * Created by chen on 2018/11/16.
 */
public interface SmsFlashPromotionProductRelationDao {
    /**
     * 获取限时购及相关商品信息
     */
    List<SmsFlashPromotionProduct> getList(@Param("flashPromotionId") Long flashPromotionId, @Param("flashPromotionSessionId") Long flashPromotionSessionId);
}
