package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ykb.mall.common.model.PmsProductGroupDate;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupBasicInfo;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupData;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupListParam;

/**
 * @Author wangyucong
 * @Date 2019/3/22 11:25
 */
public interface PmsProductGroupDao {

    /**
     * 班期详情
     * @param groupId
     * @return
     */
    PmsProductGroupData selectGroupDetail(@Param("groupId") Long groupId);

    /**
     * 班期列表
     * @param param
     * @return
     */
    List<PmsProductGroupBasicInfo> selectGroupList(PmsProductGroupListParam param);

    /**
     * 查询产品下的班期时间
     * @param productId
     * @return
     */
    @Select("select d.* from pms_product_group g left join pms_product_group_date d on d.id = g.date_id where g.delete_status = 0 and g.product_id = #{productId} group by d.id")
    List<PmsProductGroupDate> selectProductDateList(@Param("productId") Long productId);

    /**
     * 删除班期
     * @param groupId
     * @return
     */
    @Update("update pms_product_group set delete_status = 1 where id = #{groupId}")
    int deleteProductGroup(@Param("groupId") Long groupId);

    @Update("update pms_product_group set delete_status = 1 where id in (${ids})")
    int deleteProductGroupList(@Param("ids") String ids);
}