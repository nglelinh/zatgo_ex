package com.zatgo.zup.trade.admin.service;

import java.math.BigDecimal;

import com.ykb.mall.common.enumType.BusinessEnum.PayType;

public interface ThirdPayRecordService {

	/**
	 * 保存第三方支付记录
	 * 
	 * @param orderNumber
	 * @param payType
	 * @param money
	 * @param thirdBusinessNumber
	 */
	public void insertPaymentRecord(String orderNumber, PayType payType, BigDecimal money, String thirdBusinessNumber);

	public void insertRefundRecord(String orderNumber, String refundNumber, PayType payType, BigDecimal money,
                                   String thirdBusinessNumber, String thirdRefundNumber);

}
