package com.zatgo.zup.trade.admin.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.zatgo.zup.trade.admin.service.EsProductService;

/**
 * @Author wangyucong
 * @Date 2019/3/8 17:16
 */
@Component
public class UpdateEsProductTask {

    private Logger logger = LoggerFactory.getLogger(UpdateEsProductTask.class);

    @Autowired
    private EsProductService esProductService;

    @Scheduled(cron = "0 0 0 1/1 * ?")
    private void importEsProduct(){
        logger.info("UpdateEsProductTask start...");
        int count = 0;
        try {
            count = esProductService.importAll();
        } catch (Exception e) {
            logger.error("UpdateEsProductTask error", e);
        }
        logger.info("UpdateEsProductTask end,import number : {}", count);
    }
}