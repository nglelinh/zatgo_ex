package com.zatgo.zup.trade.admin.service.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.zatgo.zup.trade.admin.dao.EsProductDao;
import com.zatgo.zup.trade.admin.dto.EsProduct;
import com.zatgo.zup.trade.admin.repository.EsProductRepository;
import com.zatgo.zup.trade.admin.service.EsProductService;

/**
 * @Author wangyucong
 * @Date 2019/2/26 17:58
 */
@Service
public class EsProductServiceImpl implements EsProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EsProductServiceImpl.class);

    @Autowired
    private EsProductDao productDao;
    @Autowired
    private EsProductRepository productRepository;

    /**
     * 从数据库中导入所有商品到ES
     */
    @Override
    public int importAll() {
        List<EsProduct> esProductList = productDao.getAllEsProductList(null);
        int result = 0;
        if (!CollectionUtils.isEmpty(esProductList)) {
            productRepository.deleteAll();
            Iterable<EsProduct> esProductIterable = productRepository.saveAll(esProductList);
            Iterator<EsProduct> iterator = esProductIterable.iterator();
            while (iterator.hasNext()) {
                result++;
                iterator.next();
            }
        }
        return result;
    }
}