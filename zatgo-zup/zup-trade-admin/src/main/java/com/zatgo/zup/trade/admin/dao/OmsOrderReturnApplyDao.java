package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ykb.mall.common.model.OmsOrderReturnApply;
import com.zatgo.zup.trade.admin.dto.OmsOrderReturnApplyResult;
import com.zatgo.zup.trade.admin.dto.OmsReturnApplyQueryParam;

/**
 * 订单退货申请自定义Dao
 * Created by chen on 2018/10/18.
 */
public interface OmsOrderReturnApplyDao {
    /**
     * 查询申请列表
     */
    List<OmsOrderReturnApply> getList(@Param("queryParam") OmsReturnApplyQueryParam queryParam);

    /**
     * 获取申请详情
     */
    OmsOrderReturnApplyResult getDetail(@Param("id") Long id);
}
