package com.zatgo.zup.trade.admin.dto;

import java.util.Date;

import com.ykb.mall.common.model.PmsProductGroup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/22 16:01
 */
@ApiModel("产品班期基本信息（列表用）")
public class PmsProductGroupBasicInfo extends PmsProductGroup {

    @ApiModelProperty("出团日期")
    private Date tripTime;

    @ApiModelProperty("天/晚（天晚用英文逗号分隔）")
    private String dayNight;

    @ApiModelProperty("产品名称")
    private String productName;

    public Date getTripTime() {
        return tripTime;
    }

    public void setTripTime(Date tripTime) {
        this.tripTime = tripTime;
    }

    public String getDayNight() {
        return dayNight;
    }

    public void setDayNight(String dayNight) {
        this.dayNight = dayNight;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}