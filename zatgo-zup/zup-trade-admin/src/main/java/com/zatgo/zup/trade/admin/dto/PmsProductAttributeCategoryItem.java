package com.zatgo.zup.trade.admin.dto;

import java.util.List;

import com.ykb.mall.common.model.PmsProductAttribute;
import com.ykb.mall.common.model.PmsProductAttributeCategory;

/**
 * 包含有分类下属性的dto
 * Created by chen on 2018/5/24.
 */
public class PmsProductAttributeCategoryItem extends PmsProductAttributeCategory {
    private List<PmsProductAttribute> productAttributeList;

    public List<PmsProductAttribute> getProductAttributeList() {
        return productAttributeList;
    }

    public void setProductAttributeList(List<PmsProductAttribute> productAttributeList) {
        this.productAttributeList = productAttributeList;
    }
}
