package com.zatgo.zup.trade.admin.remoteService;

import com.zatgo.zup.common.model.wx.WechatRefundRequest;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2019/6/5.
 */

@FeignClient("zup-wechat")
public interface WechatRemoteService {


    @PostMapping(value = "/wechat/weixinRefund")
    @ResponseBody
    String weixinRefund(@RequestBody WechatRefundRequest request);


    @GetMapping(value = "/wechat/wx/msg/support/{openId}")
    @ResponseBody
    CommonResult sendWechatSupportMsg(@PathVariable("openId") String openId);

    @GetMapping(value = "wechat/wx/msg/support/success/{userId}/{id}")
    @ResponseBody
    CommonResult sendWechatSupportSuccessMsg(@PathVariable("openId") String openId, @PathVariable("id") String id);

    @GetMapping(value = "/wechat/jsapiConfig")
    @ResponseBody
    CommonResult selectJsapiConfig(@RequestParam("url") String url);

    @GetMapping(value = "/wechat/wx/pay/params/{orderId}/{wxPayType}/{wxOpenId}")
    @ResponseBody
    CommonResult getOrderPayWeixinParams(@PathVariable("orderId") Long orderId,
                                         @PathVariable("wxPayType") Integer wxPayType,
                                         @PathVariable("wxOpenId") String wxOpenId);
}
