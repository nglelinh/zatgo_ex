package com.zatgo.zup.trade.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("专题")
public class CmsSubjectParams {
	
	@ApiModelProperty("")
	private Long id;

	@ApiModelProperty(value = "标题", required = true)
    private String title;

	@ApiModelProperty(value = "封面", required = true)
    private String pic;

	@ApiModelProperty(value = "是否推荐", required = true)
    private Integer recommendStatus;

	@ApiModelProperty(value = "相册", required = false)
    private String albumPics;

	@ApiModelProperty(value = "描述", required = false)
    private String description;

	@ApiModelProperty(value = "显示状态", required = true)
    private Integer showStatus;

	@ApiModelProperty(value = "内容", required = false)
    private String content;
	
	@ApiModelProperty(value = "推荐排序", required = false)
	private Integer sort;

	@ApiModelProperty(value = "业务类型 1-旅游；2-机票；3-酒店；4-文章;5-门票；6其他", required = true)
	private Byte businessType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getRecommendStatus() {
		return recommendStatus;
	}

	public void setRecommendStatus(Integer recommendStatus) {
		this.recommendStatus = recommendStatus;
	}

	public String getAlbumPics() {
		return albumPics;
	}

	public void setAlbumPics(String albumPics) {
		this.albumPics = albumPics;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getShowStatus() {
		return showStatus;
	}

	public void setShowStatus(Integer showStatus) {
		this.showStatus = showStatus;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Byte getBusinessType() {
		return businessType;
	}

	public void setBusinessType(Byte businessType) {
		this.businessType = businessType;
	}
}
