package com.zatgo.zup.trade.admin.dto;

import java.util.List;

import com.ykb.mall.common.model.OmsOrder;
import com.ykb.mall.common.model.OmsOrderItem;
import com.ykb.mall.common.model.OmsOrderOperateHistory;




/**
 * 订单详情信息
 * Created by chen on 2018/10/11.
 */
public class OmsOrderDetail extends OmsOrder {
    private List<OmsOrderItem> orderItemList;
    private List<OmsOrderOperateHistory> historyList;

    public List<OmsOrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OmsOrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public List<OmsOrderOperateHistory> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<OmsOrderOperateHistory> historyList) {
        this.historyList = historyList;
    }
}
