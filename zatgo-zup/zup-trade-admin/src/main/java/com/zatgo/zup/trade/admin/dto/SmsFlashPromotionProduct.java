package com.zatgo.zup.trade.admin.dto;

import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.SmsFlashPromotionProductRelation;




/**
 * 限时购及商品信息封装
 * Created by chen on 2018/11/16.
 */
public class SmsFlashPromotionProduct extends SmsFlashPromotionProductRelation{
    private PmsProduct product;

    public PmsProduct getProduct() {
        return product;
    }

    public void setProduct(PmsProduct product) {
        this.product = product;
    }
}
