package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ykb.mall.common.model.SmsCouponProductRelation;

/**
 * 优惠券和产品关系自定义Dao
 * Created by chen on 2018/8/28.
 */
public interface SmsCouponProductRelationDao {
    int insertList(@Param("list") List<SmsCouponProductRelation> productRelationList);
}
