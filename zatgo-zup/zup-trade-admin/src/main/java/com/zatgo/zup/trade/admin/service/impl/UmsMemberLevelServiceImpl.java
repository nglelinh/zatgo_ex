package com.zatgo.zup.trade.admin.service.impl;

import com.zatgo.zup.trade.admin.mapper.UmsMemberLevelMapper;
import com.ykb.mall.model.UmsMemberLevel;
import com.ykb.mall.model.UmsMemberLevelExample;
import com.zatgo.zup.trade.admin.service.UmsMemberLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员等级管理Service实现类
 * Created by chen on 2018/4/26.
 */
@Service
public class UmsMemberLevelServiceImpl implements UmsMemberLevelService {
    @Autowired
    private UmsMemberLevelMapper memberLevelMapper;
    @Override
    public List<UmsMemberLevel> list(Integer defaultStatus) {
        UmsMemberLevelExample example = new UmsMemberLevelExample();
        example.createCriteria().andDefaultStatusEqualTo(defaultStatus);
        return memberLevelMapper.selectByExample(example);
    }
}
