package com.zatgo.zup.trade.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsHotelData;
import com.zatgo.zup.trade.admin.dto.PmsProductHotelParams;
import com.zatgo.zup.trade.admin.service.PmsProductHotelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "PmsProductHotelController", description = "酒店管理")
@RequestMapping("/trade/admin/product/hotel")
public class PmsProductHotelController {
	
	@Autowired
	private PmsProductHotelService pmsProductHotelService;
	
	@ApiOperation("酒店产品报价")
	@PostMapping("/create")
	public CommonResult createHotelPrice(@RequestBody PmsProductHotelParams params) {
		return pmsProductHotelService.createHotelPrice(params);
	}
	
	@ApiOperation("酒店产品列表")
	@GetMapping("/list/{productId}")
	public CommonResult hotelPriceList(@PathVariable Long productId) {
		return pmsProductHotelService.hotelPriceList(productId);
	}
	
	@ApiOperation("更新酒店产品")
	@PostMapping("/update")
	public CommonResult updateHotelPrice(@RequestBody PmsHotelData prams) {
		return pmsProductHotelService.updateHotelPrice(prams);
	}
	
	@ApiOperation("批量上下架")
	@PostMapping("/update/status/{productId}/{status}")
	public CommonResult updateHotelStatus(@PathVariable Long productId, @ApiParam("1-上架；2-下架") @PathVariable Byte status) {
		return pmsProductHotelService.updateHotelStatus(productId, status);
	}

}
