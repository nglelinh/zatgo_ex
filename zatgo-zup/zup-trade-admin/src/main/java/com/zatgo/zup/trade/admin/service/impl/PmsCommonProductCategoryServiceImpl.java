package com.zatgo.zup.trade.admin.service.impl;

import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.model.PmsCommonProductCategory;
import com.ykb.mall.common.model.PmsCommonProductCategoryExample;
import com.ykb.mall.common.model.PmsProductExample;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.trade.admin.mapper.PmsCommonProductCategoryMapper;
import com.zatgo.zup.trade.admin.mapper.PmsProductMapper;
import com.zatgo.zup.trade.admin.service.PmsCommonProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * PmsProductCategoryService实现类
 * Created by chen on 2018/4/26.
 */
@Service
public class PmsCommonProductCategoryServiceImpl implements PmsCommonProductCategoryService {
    @Autowired
    private PmsCommonProductCategoryMapper pmsCommonProductCategoryMapper;
    @Autowired
    private PmsProductMapper pmsProductMapper;

    @Override
    @Transactional
    public int create(PmsCommonProductCategory pmsProductCategoryParam) {
        String parentId = pmsProductCategoryParam.getParentId();
        //判断是否存在子节点，如果没有，需修改子节点的hasChild
        if (!StringUtils.isEmpty(parentId)){
            PmsCommonProductCategoryExample example = new PmsCommonProductCategoryExample();
            example.createCriteria().andParentIdEqualTo(parentId);
            int i = pmsCommonProductCategoryMapper.countByExample(example);
            if (i == 0){
                PmsCommonProductCategory pcpc = new PmsCommonProductCategory();
                pcpc.setId(parentId);
                pcpc.setHasChild(true);
                pmsCommonProductCategoryMapper.updateByPrimaryKeySelective(pcpc);
            }
        }
        pmsProductCategoryParam.setId(UUIDUtils.getUuid());
        int count = pmsCommonProductCategoryMapper.insertSelective(pmsProductCategoryParam);
        return count;
    }

    @Override
    @Transactional
    public int update(PmsCommonProductCategory pmsCommonProductCategory) {
        String id = pmsCommonProductCategory.getId();
        String parentId = pmsCommonProductCategory.getParentId();
        PmsCommonProductCategory oldCat = pmsCommonProductCategoryMapper.selectByPrimaryKey(id);
        //判断是否存在子节点，如果没有，修改原父节点的hasChild
        String oldParentId = oldCat.getParentId();
        PmsCommonProductCategory pcpc = new PmsCommonProductCategory();
        if (!StringUtils.isEmpty(oldParentId)){
            PmsCommonProductCategoryExample example = new PmsCommonProductCategoryExample();
            example.createCriteria().andParentIdEqualTo(oldParentId);
            int i = pmsCommonProductCategoryMapper.countByExample(example);
            pcpc.setId(parentId);
            pcpc.setHasChild(i > 0);
            pmsCommonProductCategoryMapper.updateByPrimaryKeySelective(pcpc);
        }
        //判断是否存在子节点，如果没有，需修改新父节点的hasChild
        if (!StringUtils.isEmpty(parentId)){
            pcpc.setId(parentId);
            pcpc.setHasChild(true);
            pmsCommonProductCategoryMapper.updateByPrimaryKeySelective(pcpc);
        }
        return pmsCommonProductCategoryMapper.updateByPrimaryKeySelective(pmsCommonProductCategory);
    }

    @Transactional
    @Override
    public int delete(String id) {
        PmsCommonProductCategory pmsCommonProductCategory = pmsCommonProductCategoryMapper.selectByPrimaryKey(id);
        if (pmsCommonProductCategory.getHasChild()){
            throw new BusinessException(BusinessExceptionCode.HAS_CHILD_NODE);
        }
        PmsProductExample example = new PmsProductExample();
        example.createCriteria().andProductCommonCategoryIdEqualTo(id);
        int i = pmsProductMapper.countByExample(example);
        if (i > 0)
            throw new BusinessException(BusinessExceptionCode.CATEGOR_HAS_PRODUCT);
        return pmsCommonProductCategoryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<PmsCommonProductCategory> getItem(String id) {
        PmsCommonProductCategoryExample example = new PmsCommonProductCategoryExample();
        PmsCommonProductCategoryExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause("sort asc");
        if (StringUtils.isEmpty(id)){
            criteria.andParentIdIsNull();
        }
        if (!StringUtils.isEmpty(id)){
            criteria.andParentIdEqualTo(id);
        }
        return pmsCommonProductCategoryMapper.selectByExample(example);
    }

    @Override
    public PmsCommonProductCategory detail(String id) {
        return pmsCommonProductCategoryMapper.selectByPrimaryKey(id);
    }
}
