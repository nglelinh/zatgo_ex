package com.zatgo.zup.trade.admin.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.ykb.mall.common.model.PmsBrand;
import com.zatgo.zup.trade.admin.dto.PmsBrandParam;

/**
 * 商品品牌Service
 * Created by chen on 2018/4/26.
 */
public interface PmsBrandService {
    List<PmsBrand> listAllBrand();

    int createBrand(PmsBrandParam pmsBrandParam);
    @Transactional
    int updateBrand(Long id, PmsBrandParam pmsBrandParam);

    int deleteBrand(Long id);

    int deleteBrand(List<Long> ids);

    List<PmsBrand> listBrand(String keyword, int pageNum, int pageSize);

    PmsBrand getBrand(Long id);

    int updateShowStatus(List<Long> ids, Integer showStatus);

    int updateFactoryStatus(List<Long> ids, Integer factoryStatus);
}
