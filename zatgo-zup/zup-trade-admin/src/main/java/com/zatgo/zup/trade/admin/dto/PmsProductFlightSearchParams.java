package com.zatgo.zup.trade.admin.dto;

import java.util.Date;

import com.ykb.mall.common.model.PageParams;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("航班列表查询参数")
public class PmsProductFlightSearchParams extends PageParams {
	
	@ApiModelProperty("航班编号")
	private String flightNumber;
	
	@ApiModelProperty("航空公司")
	private String airlineCompany;
	
	@ApiModelProperty("产品编号")
	private Long productId;
	
	@ApiModelProperty("库存是否限量（0 - 否，1 - 是）")
	private Byte stockLimit;
	
	@ApiModelProperty("状态（0 -待发布，1 - 已上架，2 - 已下架）")
	private Byte status;
	
	@ApiModelProperty("出发城市名称")
	private String departureCityName;
	
	@ApiModelProperty("出发机场")
	private String departureAirportName;
	
	@ApiModelProperty("出发航站楼")
	private String departureTerminal;
	
	@ApiModelProperty("到达城市名称")
	private String destinationCityName;
	
	@ApiModelProperty("到达机场名称")
	private String destinationAirportName;
	
	@ApiModelProperty("到达航站楼")
	private String destinationTerminal;
	
	@ApiModelProperty("机型")
	private String planeType;
	
	@ApiModelProperty("起始创建时间")
	private Date createStringTime;
	
	@ApiModelProperty("结束创建时间")
	private Date createEndTime;
	
	@ApiModelProperty("起始编辑时间")
	private Date updateStringTime;
	
	@ApiModelProperty("结束编辑时间")
	private Date updateEndTime;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAirlineCompany() {
		return airlineCompany;
	}

	public void setAirlineCompany(String airlineCompany) {
		this.airlineCompany = airlineCompany;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Byte getStockLimit() {
		return stockLimit;
	}

	public void setStockLimit(Byte stockLimit) {
		this.stockLimit = stockLimit;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getDepartureCityName() {
		return departureCityName;
	}

	public void setDepartureCityName(String departureCityName) {
		this.departureCityName = departureCityName;
	}

	public String getDepartureAirportName() {
		return departureAirportName;
	}

	public void setDepartureAirportName(String departureAirportName) {
		this.departureAirportName = departureAirportName;
	}

	public String getDepartureTerminal() {
		return departureTerminal;
	}

	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}

	public String getDestinationCityName() {
		return destinationCityName;
	}

	public void setDestinationCityName(String destinationCityName) {
		this.destinationCityName = destinationCityName;
	}

	public String getDestinationAirportName() {
		return destinationAirportName;
	}

	public void setDestinationAirportName(String destinationAirportName) {
		this.destinationAirportName = destinationAirportName;
	}

	public String getDestinationTerminal() {
		return destinationTerminal;
	}

	public void setDestinationTerminal(String destinationTerminal) {
		this.destinationTerminal = destinationTerminal;
	}

	public String getPlaneType() {
		return planeType;
	}

	public void setPlaneType(String planeType) {
		this.planeType = planeType;
	}

	public Date getCreateStringTime() {
		return createStringTime;
	}

	public void setCreateStringTime(Date createStringTime) {
		this.createStringTime = createStringTime;
	}

	public Date getCreateEndTime() {
		return createEndTime;
	}

	public void setCreateEndTime(Date createEndTime) {
		this.createEndTime = createEndTime;
	}

	public Date getUpdateStringTime() {
		return updateStringTime;
	}

	public void setUpdateStringTime(Date updateStringTime) {
		this.updateStringTime = updateStringTime;
	}

	public Date getUpdateEndTime() {
		return updateEndTime;
	}

	public void setUpdateEndTime(Date updateEndTime) {
		this.updateEndTime = updateEndTime;
	}

}
