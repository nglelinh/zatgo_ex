package com.zatgo.zup.trade.admin.mapper;

import com.ykb.mall.common.model.PmsProductRelation;
import com.ykb.mall.common.model.PmsProductRelationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductRelationMapper {
    int countByExample(PmsProductRelationExample example);

    int deleteByExample(PmsProductRelationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductRelation record);

    int insertSelective(PmsProductRelation record);

    List<PmsProductRelation> selectByExample(PmsProductRelationExample example);

    PmsProductRelation selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductRelation record, @Param("example") PmsProductRelationExample example);

    int updateByExample(@Param("record") PmsProductRelation record, @Param("example") PmsProductRelationExample example);

    int updateByPrimaryKeySelective(PmsProductRelation record);

    int updateByPrimaryKey(PmsProductRelation record);
}