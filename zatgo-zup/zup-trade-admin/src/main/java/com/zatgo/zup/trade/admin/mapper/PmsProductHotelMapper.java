package com.zatgo.zup.trade.admin.mapper;

import com.ykb.mall.common.model.PmsProductHotel;
import com.ykb.mall.common.model.PmsProductHotelExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductHotelMapper {
    int countByExample(PmsProductHotelExample example);

    int deleteByExample(PmsProductHotelExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductHotel record);

    int insertSelective(PmsProductHotel record);

    List<PmsProductHotel> selectByExample(PmsProductHotelExample example);

    PmsProductHotel selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductHotel record, @Param("example") PmsProductHotelExample example);

    int updateByExample(@Param("record") PmsProductHotel record, @Param("example") PmsProductHotelExample example);

    int updateByPrimaryKeySelective(PmsProductHotel record);

    int updateByPrimaryKey(PmsProductHotel record);

    int updateStock(@Param("id") Long id,@Param("num") Integer num);
}