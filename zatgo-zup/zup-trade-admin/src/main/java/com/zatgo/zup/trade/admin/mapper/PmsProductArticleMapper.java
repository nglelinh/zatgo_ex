package com.zatgo.zup.trade.admin.mapper;

import com.ykb.mall.common.model.PmsProductArticle;
import com.ykb.mall.common.model.PmsProductArticleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductArticleMapper {
    int countByExample(PmsProductArticleExample example);

    int deleteByExample(PmsProductArticleExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductArticle record);

    int insertSelective(PmsProductArticle record);

    List<PmsProductArticle> selectByExample(PmsProductArticleExample example);

    PmsProductArticle selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductArticle record, @Param("example") PmsProductArticleExample example);

    int updateByExample(@Param("record") PmsProductArticle record, @Param("example") PmsProductArticleExample example);

    int updateByPrimaryKeySelective(PmsProductArticle record);

    int updateByPrimaryKey(PmsProductArticle record);
}