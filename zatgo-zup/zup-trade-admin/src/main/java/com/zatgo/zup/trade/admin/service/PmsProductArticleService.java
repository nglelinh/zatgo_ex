package com.zatgo.zup.trade.admin.service;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.zatgo.zup.trade.admin.dto.PmsProductArticleParam;
import com.zatgo.zup.trade.admin.dto.PmsProductArticleResult;

/**
 * 商品管理Service
 * Created by chen on 2018/4/26.
 */
public interface PmsProductArticleService {
    /**
     * 创建文章
     */
    @Transactional(isolation = Isolation.DEFAULT,propagation = Propagation.REQUIRED)
    int create(PmsProductArticleParam articleParam);

    /**
     * 根据商品编号获取更新信息
     */
    PmsProductArticleResult getUpdateInfo(Long id);

    /**
     * 更新商品
     */
    @Transactional
    int update(Long id, PmsProductArticleParam articleParam);

}
