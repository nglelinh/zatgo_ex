package com.zatgo.zup.trade.admin.dto;

import java.util.List;

import com.ykb.mall.common.model.PmsProductGroupTransport;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * @Author wangyucong
 * @Date 2019/3/19 16:30
 */
@ApiModel("班期交通信息")
public class PmsProductGroupTransportInfo extends PmsProductGroupTransport {

    @ApiModelProperty("交通时间")
    private List<PmsProductGroupTransportTimeInfo> timeInfo;

    public List<PmsProductGroupTransportTimeInfo> getTimeInfo() {
        return timeInfo;
    }

    public void setTimeInfo(List<PmsProductGroupTransportTimeInfo> timeInfo) {
        this.timeInfo = timeInfo;
    }
}