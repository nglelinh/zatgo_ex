package com.zatgo.zup.trade.admin.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.ykb.mall.common.model.PmsProductCategory;
import com.zatgo.zup.trade.admin.dto.PmsProductCategoryParam;
import com.zatgo.zup.trade.admin.dto.PmsProductCategoryWithChildrenItem;

/**
 * 产品分类Service
 * Created by chen on 2018/4/26.
 */
public interface PmsProductCategoryService {
    @Transactional
    int create(PmsProductCategoryParam pmsProductCategoryParam);

    @Transactional
    int update(Long id, PmsProductCategoryParam pmsProductCategoryParam);

    List<PmsProductCategory> getList(Long parentId, Integer pageSize, Integer pageNum);

    int delete(Long id);

    PmsProductCategory getItem(Long id);

    int updateNavStatus(List<Long> ids, Integer navStatus);

    int updateShowStatus(List<Long> ids, Integer showStatus);

    List<PmsProductCategoryWithChildrenItem> listWithChildren();
}
