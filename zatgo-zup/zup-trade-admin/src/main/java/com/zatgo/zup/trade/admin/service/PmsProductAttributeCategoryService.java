package com.zatgo.zup.trade.admin.service;

import java.util.List;

import com.ykb.mall.common.model.PmsProductAttributeCategory;
import com.zatgo.zup.trade.admin.dto.PmsProductAttributeCategoryItem;

/**
 * 商品属性分类Service
 * Created by chen on 2018/4/26.
 */
public interface PmsProductAttributeCategoryService {
    int create(String name);

    int update(Long id, String name);

    int delete(Long id);

    PmsProductAttributeCategory getItem(Long id);

    List<PmsProductAttributeCategory> getList(Integer pageSize, Integer pageNum);

    List<PmsProductAttributeCategoryItem> getListWithAttr();
}
