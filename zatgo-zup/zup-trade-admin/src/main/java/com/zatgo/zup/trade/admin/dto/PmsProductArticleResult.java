package com.zatgo.zup.trade.admin.dto;

import java.util.Date;
import java.util.List;

import com.ykb.mall.common.model.CmsPrefrenceAreaProductRelation;
import com.ykb.mall.common.model.CmsSubjectProductRelation;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductAttributeValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("文章信息")
public class PmsProductArticleResult extends PmsProduct {
	
	@ApiModelProperty("商品所选分类的父id")
	private Long cateParentId;
	
	@ApiModelProperty("作者")
	private String author;
	
	@ApiModelProperty("作者头像")
	private String authorIcon;
	
	@ApiModelProperty("是否发布0->下架；1->上架")
	private Integer publishStatus;
	
	@ApiModelProperty("发布时间")
	private Date publishTime;
	
	@ApiModelProperty("职位")
	private String position;
	
	@ApiModelProperty("商品参数及自定义规格属性")
    private List<PmsProductAttributeValue> productAttributeValueList;
    @ApiModelProperty("专题和商品关系")
    private List<CmsSubjectProductRelation> subjectProductRelationList;
    @ApiModelProperty("优选专区和商品的关系")
    private List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList;

	public Long getCateParentId() {
		return cateParentId;
	}

	public void setCateParentId(Long cateParentId) {
		this.cateParentId = cateParentId;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthorIcon() {
		return authorIcon;
	}

	public void setAuthorIcon(String authorIcon) {
		this.authorIcon = authorIcon;
	}

	public Integer getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(Integer publishStatus) {
		this.publishStatus = publishStatus;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	public List<PmsProductAttributeValue> getProductAttributeValueList() {
		return productAttributeValueList;
	}

	public void setProductAttributeValueList(List<PmsProductAttributeValue> productAttributeValueList) {
		this.productAttributeValueList = productAttributeValueList;
	}

	public List<CmsSubjectProductRelation> getSubjectProductRelationList() {
		return subjectProductRelationList;
	}

	public void setSubjectProductRelationList(List<CmsSubjectProductRelation> subjectProductRelationList) {
		this.subjectProductRelationList = subjectProductRelationList;
	}

	public List<CmsPrefrenceAreaProductRelation> getPrefrenceAreaProductRelationList() {
		return prefrenceAreaProductRelationList;
	}

	public void setPrefrenceAreaProductRelationList(
			List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList) {
		this.prefrenceAreaProductRelationList = prefrenceAreaProductRelationList;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

}
