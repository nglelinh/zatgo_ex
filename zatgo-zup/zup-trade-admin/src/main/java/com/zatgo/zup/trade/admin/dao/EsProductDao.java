package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zatgo.zup.trade.admin.dto.EsProduct;

/**
 * 搜索系统中的商品管理自定义Dao
 * Created by chen on 2018/6/19.
 */
public interface EsProductDao {
    List<EsProduct> getAllEsProductList(@Param("id") Long id);
}
