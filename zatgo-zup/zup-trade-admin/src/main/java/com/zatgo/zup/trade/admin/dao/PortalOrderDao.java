package com.zatgo.zup.trade.admin.dao;

import com.ykb.mall.common.model.PortalOrderBaseInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 前台订单自定义Dao
 * Created by chen on 2018/9/4.
 */
public interface PortalOrderDao {

    PortalOrderBaseInfo selectOrderByOrderSn(@Param("orderSn") String orderSn);

}
