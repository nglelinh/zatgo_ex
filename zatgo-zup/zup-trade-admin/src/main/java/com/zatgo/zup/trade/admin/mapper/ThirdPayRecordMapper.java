package com.zatgo.zup.trade.admin.mapper;

import com.ykb.mall.common.model.ThirdPayRecord;
import com.ykb.mall.common.model.ThirdPayRecordExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ThirdPayRecordMapper {
    int countByExample(ThirdPayRecordExample example);

    int deleteByExample(ThirdPayRecordExample example);

    int deleteByPrimaryKey(Long recordId);

    int insert(ThirdPayRecord record);

    int insertSelective(ThirdPayRecord record);

    List<ThirdPayRecord> selectByExample(ThirdPayRecordExample example);

    ThirdPayRecord selectByPrimaryKey(Long recordId);

    int updateByExampleSelective(@Param("record") ThirdPayRecord record, @Param("example") ThirdPayRecordExample example);

    int updateByExample(@Param("record") ThirdPayRecord record, @Param("example") ThirdPayRecordExample example);

    int updateByPrimaryKeySelective(ThirdPayRecord record);

    int updateByPrimaryKey(ThirdPayRecord record);
}