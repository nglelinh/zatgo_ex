package com.zatgo.zup.trade.admin.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ykb.mall.common.model.PmsProduct;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.service.PmsProductService;
import com.zatgo.zup.trade.admin.service.PmsProductSyncService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 商品同步管理
 * Created by 46041 on 2019/5/13.
 */

@Controller
@Api(tags = "PmsProductSyncController", description = "商品同步")
@RequestMapping("/trade/admin/product/sync")
public class PmsProductSyncController {

    private static final Logger logger = LoggerFactory.getLogger(PmsProductSyncController.class);

    @Autowired
    private PmsProductService productService;
    @Autowired
    private PmsProductSyncService productSyncService;


    @ApiOperation("同步")
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Object sync(@RequestBody List<Long> ids) {
        List<PmsProduct> pmsProducts = productService.listByIds(ids);
        if (CollectionUtils.isEmpty(pmsProducts))
            return new CommonResult().failed("未查询到需要同步的商品");
        try {
            Map<Long, Boolean> res = productSyncService.sync(pmsProducts);
        } catch (Exception e){
            logger.error("", e);
            return new CommonResult().failed("同步失败");
        }
        return new CommonResult().success();
    }
}
