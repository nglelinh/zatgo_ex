package com.zatgo.zup.trade.admin.remoteService;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SyncProductRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by 46041 on 2019/6/5.
 */

@FeignClient("zup-activity")
public interface ActivityRemoteService {

    @PostMapping(value = "/activity/internal/weixinRefundCallBack")
    @ResponseBody
    ResponseData<Boolean> weixinRefundCallBack(@RequestParam("orderId") String orderId,
                                               @RequestParam("cloudUserId") String cloudUserId);

    @GetMapping(value = "/activity/internal/selectActivityIdByOrderId")
    @ResponseBody
    ResponseData<String> selectActivityIdByOrderId(@RequestParam("orderId") String orderId,
                                                   @RequestParam("cloudUserId") String cloudUserId);

    @PostMapping(value = "/activity/internal/getActivityOrderPrice")
    @ResponseBody
    ResponseData<BigDecimal> getActivityOrderPrice(@RequestParam("activityId") String activityId,
                                                   @RequestParam("userId") String userId);

    @PostMapping(value = "/activity/syncProduct")
    @ResponseBody
    ResponseData<BigDecimal> syncProduct(@RequestBody SyncProductRequest params);

    @PostMapping(value = "/activity/internal/orderCallBack")
    @ResponseBody
    ResponseData<Boolean> orderCallBack(@RequestParam("activityId") String activityId,
                                        @RequestParam("userId") String userId,
                                        @RequestParam("orderId") String orderId,
                                        @RequestParam("isSuccess") Boolean isSuccess);


    @RequestMapping(value = "/activity/getPayTime",method = RequestMethod.GET)
    @ResponseBody
    ResponseData<Map<String, Long>> getPayTime(@RequestParam("ids") String ids);

}
