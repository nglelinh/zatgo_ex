package com.zatgo.zup.trade.admin.mapper;

import com.ykb.mall.common.model.PmsProductFlightPrice;
import com.ykb.mall.common.model.PmsProductFlightPriceExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductFlightPriceMapper {
    int countByExample(PmsProductFlightPriceExample example);

    int deleteByExample(PmsProductFlightPriceExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductFlightPrice record);

    int insertSelective(PmsProductFlightPrice record);

    List<PmsProductFlightPrice> selectByExample(PmsProductFlightPriceExample example);

    PmsProductFlightPrice selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductFlightPrice record, @Param("example") PmsProductFlightPriceExample example);

    int updateByExample(@Param("record") PmsProductFlightPrice record, @Param("example") PmsProductFlightPriceExample example);

    int updateByPrimaryKeySelective(PmsProductFlightPrice record);

    int updateByPrimaryKey(PmsProductFlightPrice record);

    int updateStock(@Param("id") Long id,@Param("num") Integer num);
}