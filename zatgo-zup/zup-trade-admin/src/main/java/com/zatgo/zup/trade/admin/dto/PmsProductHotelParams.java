package com.zatgo.zup.trade.admin.dto;

import java.util.LinkedHashMap;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("酒店产品信息")
public class PmsProductHotelParams {
	
	@ApiModelProperty("产品编号")
	private Long productId;
	
	@ApiModelProperty("产品报价")
	private LinkedHashMap<String, List<PmsHotelData>> prices;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public LinkedHashMap<String, List<PmsHotelData>> getPrices() {
		return prices;
	}

	public void setPrices(LinkedHashMap<String, List<PmsHotelData>> prices) {
		this.prices = prices;
	}

}
