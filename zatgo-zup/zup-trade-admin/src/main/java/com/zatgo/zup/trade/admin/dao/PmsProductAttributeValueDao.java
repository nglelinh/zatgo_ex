package com.zatgo.zup.trade.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ykb.mall.common.model.PmsProductAttributeValue;

/**
 * 商品参数，商品自定义规格属性Dao
 * Created by chen on 2018/4/26.
 */
public interface PmsProductAttributeValueDao {
    int insertList(@Param("list") List<PmsProductAttributeValue> productAttributeValueList);
}
