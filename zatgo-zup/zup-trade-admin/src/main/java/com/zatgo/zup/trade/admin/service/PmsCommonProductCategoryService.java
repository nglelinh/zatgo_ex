package com.zatgo.zup.trade.admin.service;

import com.ykb.mall.common.model.PmsCommonProductCategory;
import com.ykb.mall.common.model.PmsProductCategory;
import com.zatgo.zup.trade.admin.dto.PmsProductCategoryParam;
import com.zatgo.zup.trade.admin.dto.PmsProductCategoryWithChildrenItem;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 产品分类Service
 * Created by chen on 2018/4/26.
 */
public interface PmsCommonProductCategoryService {
    @Transactional
    int create(PmsCommonProductCategory pmsProductCategoryParam);

    @Transactional
    int update(PmsCommonProductCategory pmsProductCategoryParam);

    int delete(String id);

    List<PmsCommonProductCategory> getItem(String id);

    PmsCommonProductCategory detail(String id);
}
