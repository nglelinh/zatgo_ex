package com.zatgo.zup.trade.admin.entity;

/**
 * Created by 46041 on 2019/5/14.
 */
public class ThirdLoginInfo {

    private String userId;

    private String userName;

    private String nickName;

    private ThirdWxInfo wxInfo;

    private String icon;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ThirdWxInfo getWxInfo() {
        return wxInfo;
    }

    public void setWxInfo(ThirdWxInfo wxInfo) {
        this.wxInfo = wxInfo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
