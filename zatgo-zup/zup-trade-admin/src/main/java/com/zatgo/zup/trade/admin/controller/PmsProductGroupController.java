package com.zatgo.zup.trade.admin.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.model.PmsProduct;
import com.ykb.mall.common.model.PmsProductGroupDate;
import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupBasicInfo;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupData;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupInfo;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupListParam;
import com.zatgo.zup.trade.admin.dto.PmsProductGroupParam;
import com.zatgo.zup.trade.admin.service.PmsProductGroupService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Author wangyucong
 * @Date 2019/3/21 15:14
 */
@RestController
@Api(tags = "PmsProductGroupController", description = "班期管理")
@RequestMapping("/trade/admin/pmsProductGroup")
public class PmsProductGroupController {

    @Resource
    private PmsProductGroupService pmsProductGroupService;

    @ApiOperation(value = "新增班期")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Object createProductGroup(@RequestBody PmsProductGroupParam param){
        int result = 0;
        try {
            result = pmsProductGroupService.createProductGroup(param);
        } catch (BusinessException e) {
            return new CommonResult().failed(e.getMessage());
        }
        if (result > 0) {
            return new CommonResult().success();
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation(value = "更新班期")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Object updateProductGroup(@RequestBody PmsProductGroupParam param){
        int result = 0;
        try {
            result = pmsProductGroupService.updateProductGroup(param);
        } catch (BusinessException e) {
            return new CommonResult().failed(e.getMessage());
        }
        if (result > 0) {
            return new CommonResult().success();
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation(value = "查询班期详情")
    @RequestMapping(value = "/detail/{groupId}", method = RequestMethod.GET)
    public Object selectGroupDetail(@PathVariable Long groupId){
        PmsProductGroupData data = pmsProductGroupService.selectGroupDetail(groupId);
        if (null != data) {
            return new CommonResult().success(data);
        } else {
            return new CommonResult().failed("未查询到详情信息");
        }
    }

    @ApiOperation(value = "查询班期列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Object selectGroupList(PmsProductGroupListParam param){
        List<PmsProductGroupBasicInfo> list = pmsProductGroupService.selectGroupList(param);
        if (!CollectionUtils.isEmpty(list)) {
            return new CommonResult().pageSuccess(list);
        } else {
            return new CommonResult().success("未查询到班期记录");
        }
    }

    @ApiOperation(value = "查询产品日期信息")
    @RequestMapping(value = "/date/list/{productId}", method = RequestMethod.GET)
    public Object selectProductDateList(@PathVariable Long productId){
        List<PmsProductGroupDate> dateList = pmsProductGroupService.selectProductDateList(productId);
        if (!CollectionUtils.isEmpty(dateList)) {
            return new CommonResult().success(dateList);
        } else {
            return new CommonResult().success("未查询到产品日期信息");
        }
    }

    @ApiOperation(value = "删除班期")
    @RequestMapping(value = "/delete/{groupId}", method = RequestMethod.POST)
    public Object deleteProductGroup(@PathVariable Long groupId){
        int result = pmsProductGroupService.deleteProductGroup(groupId);
        if (result > 0) {
            return new CommonResult().success();
        } else {
            return new CommonResult().failed("删除班期失败");
        }
    }

    @ApiOperation(value = "批量删除班期")
    @RequestMapping(value = "/delete/group", method = RequestMethod.POST)
    public Object deleteProductGroupList(@RequestParam("ids") String ids){
        int result = pmsProductGroupService.deleteProductGroupList(ids);
        if (result > 0) {
            return new CommonResult().success();
        } else {
            return new CommonResult().failed("删除班期失败");
        }
    }

    @ApiOperation(value = "更新班期库存")
    @RequestMapping(value = "/update/stock", method = RequestMethod.POST)
    public Object updateGroupStock(@RequestBody PmsProductGroupInfo param) {
        int result = pmsProductGroupService.updateGroupStock(param);
        if (result > 0) {
            return new CommonResult().success();
        } else {
            return new CommonResult().failed("更新库存失败");
        }
    }

    @ApiOperation(value = "查询可选产品列表")
    @RequestMapping(value = "/product/list", method = RequestMethod.GET)
    public Object selectProductList(@RequestParam(required = false, defaultValue = "1") Byte businessType){
        List<PmsProduct> list = pmsProductGroupService.selectProductList(businessType);
        if (!CollectionUtils.isEmpty(list)) {
            return new CommonResult().success(list);
        } else {
            return new CommonResult().success("未查询到产品信息");
        }
    }
}