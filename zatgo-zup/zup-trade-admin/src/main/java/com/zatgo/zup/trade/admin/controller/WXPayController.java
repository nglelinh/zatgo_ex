package com.zatgo.zup.trade.admin.controller;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.remoteService.WechatRemoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "WXPayController", description = "微信支付")
@RestController
@RequestMapping(value = "/trade/admin/wx/pay")
public class WXPayController {

	@Autowired
	private WechatRemoteService wechatRemoteService;

	@ApiOperation("获取订单微信支付参数")
	@GetMapping(value = "/params/{orderId}/{wxPayType}/{wxOpenId}")
	public CommonResult getOrderPayWeixinParams(@PathVariable Long orderId, @PathVariable Integer wxPayType, @PathVariable String wxOpenId) {
		return wechatRemoteService.getOrderPayWeixinParams(orderId, wxPayType, wxOpenId);
	}

}
