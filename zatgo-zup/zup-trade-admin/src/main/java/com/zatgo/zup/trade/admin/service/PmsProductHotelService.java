package com.zatgo.zup.trade.admin.service;

import com.zatgo.zup.trade.admin.dto.CommonResult;
import com.zatgo.zup.trade.admin.dto.PmsHotelData;
import com.zatgo.zup.trade.admin.dto.PmsProductHotelParams;

public interface PmsProductHotelService {
	
	/**
	 * 保存酒店产品
	 * @param params
	 */
	CommonResult createHotelPrice(PmsProductHotelParams params);
	
	/**
	 * 酒店产品
	 * @return
	 */
	CommonResult hotelPriceList(Long productId);
	
	/**
	 * 更新酒店产品
	 * @param prams
	 */
	CommonResult updateHotelPrice(PmsHotelData prams);
	
	/**
	 * 批量上下架
	 * @param productId
	 * @param status
	 * @return
	 */
	CommonResult updateHotelStatus(Long productId, Byte status);

}
