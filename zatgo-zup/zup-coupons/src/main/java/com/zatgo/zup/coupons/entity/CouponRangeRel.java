package com.zatgo.zup.coupons.entity;

public class CouponRangeRel {
    private String rangeId;

    private String couponsId;

    private String rangeCategory;

    private String rangeCode;

    public String getRangeId() {
        return rangeId;
    }

    public void setRangeId(String rangeId) {
        this.rangeId = rangeId == null ? null : rangeId.trim();
    }

    public String getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(String couponsId) {
        this.couponsId = couponsId == null ? null : couponsId.trim();
    }

    public String getRangeCategory() {
        return rangeCategory;
    }

    public void setRangeCategory(String rangeCategory) {
        this.rangeCategory = rangeCategory == null ? null : rangeCategory.trim();
    }

    public String getRangeCode() {
        return rangeCode;
    }

    public void setRangeCode(String rangeCode) {
        this.rangeCode = rangeCode == null ? null : rangeCode.trim();
    }
}