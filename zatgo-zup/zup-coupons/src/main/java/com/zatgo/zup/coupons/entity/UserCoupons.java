package com.zatgo.zup.coupons.entity;

import java.util.Date;

public class UserCoupons {
    private String userCouponsId;

    private String couponsId;

    private String userId;

    private Date getDate;

    private Date validStartDate;

    private Date validEndDate;

    private Byte status;

    public String getUserCouponsId() {
        return userCouponsId;
    }

    public void setUserCouponsId(String userCouponsId) {
        this.userCouponsId = userCouponsId == null ? null : userCouponsId.trim();
    }

    public String getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(String couponsId) {
        this.couponsId = couponsId == null ? null : couponsId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Date getGetDate() {
        return getDate;
    }

    public void setGetDate(Date getDate) {
        this.getDate = getDate;
    }

    public Date getValidStartDate() {
        return validStartDate;
    }

    public void setValidStartDate(Date validStartDate) {
        this.validStartDate = validStartDate;
    }

    public Date getValidEndDate() {
        return validEndDate;
    }

    public void setValidEndDate(Date validEndDate) {
        this.validEndDate = validEndDate;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}