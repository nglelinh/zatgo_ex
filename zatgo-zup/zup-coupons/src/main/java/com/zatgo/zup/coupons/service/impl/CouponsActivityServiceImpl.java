package com.zatgo.zup.coupons.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.coupons.entity.Coupons;
import com.zatgo.zup.coupons.entity.CouponsActivity;
import com.zatgo.zup.coupons.model.DiscountEventSearchParams;
import com.zatgo.zup.coupons.service.CouponsActivityService;

@Service
public class CouponsActivityServiceImpl implements CouponsActivityService {

	@Override
	public Boolean addActivity(AuthUserInfo adminUser, CouponsActivity params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean updateActivity(AuthUserInfo adminUser, CouponsActivity params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getActivityByIdAndAdmin(AuthUserInfo adminUser, String activityId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PageInfo<Coupons> getAllActivityListByParams(AuthUserInfo adminUser, DiscountEventSearchParams params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Coupons> getCouponsByActivity(AuthUserInfo adminUser, String activityId) {
		// TODO Auto-generated method stub
		return null;
	}

}
