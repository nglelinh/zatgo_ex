package com.zatgo.zup.coupons.model;

import io.swagger.annotations.ApiModelProperty;

public class UserCouponsSearchParams {
	
	@ApiModelProperty(value = "状态：0=当前有效的，1=过期未已使用,2=未来有效的优惠券",required = false)
	private Byte status;
	
	@ApiModelProperty(value = "页码",required = false)
	private int pageNo;
	
	@ApiModelProperty(value = "每页条数",required = false)
	private int pageSize;

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
