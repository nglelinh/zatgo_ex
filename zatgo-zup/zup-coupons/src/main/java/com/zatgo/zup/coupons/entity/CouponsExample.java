package com.zatgo.zup.coupons.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CouponsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CouponsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCouponsIdIsNull() {
            addCriterion("coupons_id is null");
            return (Criteria) this;
        }

        public Criteria andCouponsIdIsNotNull() {
            addCriterion("coupons_id is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsIdEqualTo(String value) {
            addCriterion("coupons_id =", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotEqualTo(String value) {
            addCriterion("coupons_id <>", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdGreaterThan(String value) {
            addCriterion("coupons_id >", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_id >=", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLessThan(String value) {
            addCriterion("coupons_id <", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLessThanOrEqualTo(String value) {
            addCriterion("coupons_id <=", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLike(String value) {
            addCriterion("coupons_id like", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotLike(String value) {
            addCriterion("coupons_id not like", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdIn(List<String> values) {
            addCriterion("coupons_id in", values, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotIn(List<String> values) {
            addCriterion("coupons_id not in", values, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdBetween(String value1, String value2) {
            addCriterion("coupons_id between", value1, value2, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotBetween(String value1, String value2) {
            addCriterion("coupons_id not between", value1, value2, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCouponsNameIsNull() {
            addCriterion("coupons_name is null");
            return (Criteria) this;
        }

        public Criteria andCouponsNameIsNotNull() {
            addCriterion("coupons_name is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsNameEqualTo(String value) {
            addCriterion("coupons_name =", value, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameNotEqualTo(String value) {
            addCriterion("coupons_name <>", value, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameGreaterThan(String value) {
            addCriterion("coupons_name >", value, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_name >=", value, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameLessThan(String value) {
            addCriterion("coupons_name <", value, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameLessThanOrEqualTo(String value) {
            addCriterion("coupons_name <=", value, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameLike(String value) {
            addCriterion("coupons_name like", value, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameNotLike(String value) {
            addCriterion("coupons_name not like", value, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameIn(List<String> values) {
            addCriterion("coupons_name in", values, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameNotIn(List<String> values) {
            addCriterion("coupons_name not in", values, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameBetween(String value1, String value2) {
            addCriterion("coupons_name between", value1, value2, "couponsName");
            return (Criteria) this;
        }

        public Criteria andCouponsNameNotBetween(String value1, String value2) {
            addCriterion("coupons_name not between", value1, value2, "couponsName");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalIsNull() {
            addCriterion("distribution_money_total is null");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalIsNotNull() {
            addCriterion("distribution_money_total is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalEqualTo(BigDecimal value) {
            addCriterion("distribution_money_total =", value, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalNotEqualTo(BigDecimal value) {
            addCriterion("distribution_money_total <>", value, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalGreaterThan(BigDecimal value) {
            addCriterion("distribution_money_total >", value, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("distribution_money_total >=", value, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalLessThan(BigDecimal value) {
            addCriterion("distribution_money_total <", value, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("distribution_money_total <=", value, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalIn(List<BigDecimal> values) {
            addCriterion("distribution_money_total in", values, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalNotIn(List<BigDecimal> values) {
            addCriterion("distribution_money_total not in", values, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("distribution_money_total between", value1, value2, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionMoneyTotalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("distribution_money_total not between", value1, value2, "distributionMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalIsNull() {
            addCriterion("distribution_num_total is null");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalIsNotNull() {
            addCriterion("distribution_num_total is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalEqualTo(Long value) {
            addCriterion("distribution_num_total =", value, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalNotEqualTo(Long value) {
            addCriterion("distribution_num_total <>", value, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalGreaterThan(Long value) {
            addCriterion("distribution_num_total >", value, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalGreaterThanOrEqualTo(Long value) {
            addCriterion("distribution_num_total >=", value, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalLessThan(Long value) {
            addCriterion("distribution_num_total <", value, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalLessThanOrEqualTo(Long value) {
            addCriterion("distribution_num_total <=", value, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalIn(List<Long> values) {
            addCriterion("distribution_num_total in", values, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalNotIn(List<Long> values) {
            addCriterion("distribution_num_total not in", values, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalBetween(Long value1, Long value2) {
            addCriterion("distribution_num_total between", value1, value2, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andDistributionNumTotalNotBetween(Long value1, Long value2) {
            addCriterion("distribution_num_total not between", value1, value2, "distributionNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalIsNull() {
            addCriterion("assigned_money_total is null");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalIsNotNull() {
            addCriterion("assigned_money_total is not null");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalEqualTo(BigDecimal value) {
            addCriterion("assigned_money_total =", value, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalNotEqualTo(BigDecimal value) {
            addCriterion("assigned_money_total <>", value, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalGreaterThan(BigDecimal value) {
            addCriterion("assigned_money_total >", value, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("assigned_money_total >=", value, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalLessThan(BigDecimal value) {
            addCriterion("assigned_money_total <", value, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("assigned_money_total <=", value, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalIn(List<BigDecimal> values) {
            addCriterion("assigned_money_total in", values, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalNotIn(List<BigDecimal> values) {
            addCriterion("assigned_money_total not in", values, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("assigned_money_total between", value1, value2, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedMoneyTotalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("assigned_money_total not between", value1, value2, "assignedMoneyTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalIsNull() {
            addCriterion("assigned_num_total is null");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalIsNotNull() {
            addCriterion("assigned_num_total is not null");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalEqualTo(Long value) {
            addCriterion("assigned_num_total =", value, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalNotEqualTo(Long value) {
            addCriterion("assigned_num_total <>", value, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalGreaterThan(Long value) {
            addCriterion("assigned_num_total >", value, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalGreaterThanOrEqualTo(Long value) {
            addCriterion("assigned_num_total >=", value, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalLessThan(Long value) {
            addCriterion("assigned_num_total <", value, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalLessThanOrEqualTo(Long value) {
            addCriterion("assigned_num_total <=", value, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalIn(List<Long> values) {
            addCriterion("assigned_num_total in", values, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalNotIn(List<Long> values) {
            addCriterion("assigned_num_total not in", values, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalBetween(Long value1, Long value2) {
            addCriterion("assigned_num_total between", value1, value2, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andAssignedNumTotalNotBetween(Long value1, Long value2) {
            addCriterion("assigned_num_total not between", value1, value2, "assignedNumTotal");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeIsNull() {
            addCriterion("discount_type is null");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeIsNotNull() {
            addCriterion("discount_type is not null");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeEqualTo(Byte value) {
            addCriterion("discount_type =", value, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeNotEqualTo(Byte value) {
            addCriterion("discount_type <>", value, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeGreaterThan(Byte value) {
            addCriterion("discount_type >", value, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("discount_type >=", value, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeLessThan(Byte value) {
            addCriterion("discount_type <", value, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeLessThanOrEqualTo(Byte value) {
            addCriterion("discount_type <=", value, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeIn(List<Byte> values) {
            addCriterion("discount_type in", values, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeNotIn(List<Byte> values) {
            addCriterion("discount_type not in", values, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeBetween(Byte value1, Byte value2) {
            addCriterion("discount_type between", value1, value2, "discountType");
            return (Criteria) this;
        }

        public Criteria andDiscountTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("discount_type not between", value1, value2, "discountType");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceIsNull() {
            addCriterion("coupons_price is null");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceIsNotNull() {
            addCriterion("coupons_price is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceEqualTo(BigDecimal value) {
            addCriterion("coupons_price =", value, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceNotEqualTo(BigDecimal value) {
            addCriterion("coupons_price <>", value, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceGreaterThan(BigDecimal value) {
            addCriterion("coupons_price >", value, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coupons_price >=", value, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceLessThan(BigDecimal value) {
            addCriterion("coupons_price <", value, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coupons_price <=", value, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceIn(List<BigDecimal> values) {
            addCriterion("coupons_price in", values, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceNotIn(List<BigDecimal> values) {
            addCriterion("coupons_price not in", values, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coupons_price between", value1, value2, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andCouponsPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coupons_price not between", value1, value2, "couponsPrice");
            return (Criteria) this;
        }

        public Criteria andHowManyFullIsNull() {
            addCriterion("how_many_full is null");
            return (Criteria) this;
        }

        public Criteria andHowManyFullIsNotNull() {
            addCriterion("how_many_full is not null");
            return (Criteria) this;
        }

        public Criteria andHowManyFullEqualTo(BigDecimal value) {
            addCriterion("how_many_full =", value, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullNotEqualTo(BigDecimal value) {
            addCriterion("how_many_full <>", value, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullGreaterThan(BigDecimal value) {
            addCriterion("how_many_full >", value, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("how_many_full >=", value, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullLessThan(BigDecimal value) {
            addCriterion("how_many_full <", value, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullLessThanOrEqualTo(BigDecimal value) {
            addCriterion("how_many_full <=", value, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullIn(List<BigDecimal> values) {
            addCriterion("how_many_full in", values, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullNotIn(List<BigDecimal> values) {
            addCriterion("how_many_full not in", values, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("how_many_full between", value1, value2, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andHowManyFullNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("how_many_full not between", value1, value2, "howManyFull");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNull() {
            addCriterion("user_type is null");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNotNull() {
            addCriterion("user_type is not null");
            return (Criteria) this;
        }

        public Criteria andUserTypeEqualTo(Byte value) {
            addCriterion("user_type =", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotEqualTo(Byte value) {
            addCriterion("user_type <>", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThan(Byte value) {
            addCriterion("user_type >", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("user_type >=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThan(Byte value) {
            addCriterion("user_type <", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThanOrEqualTo(Byte value) {
            addCriterion("user_type <=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeIn(List<Byte> values) {
            addCriterion("user_type in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotIn(List<Byte> values) {
            addCriterion("user_type not in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeBetween(Byte value1, Byte value2) {
            addCriterion("user_type between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("user_type not between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andMemberLevelIsNull() {
            addCriterion("member_level is null");
            return (Criteria) this;
        }

        public Criteria andMemberLevelIsNotNull() {
            addCriterion("member_level is not null");
            return (Criteria) this;
        }

        public Criteria andMemberLevelEqualTo(Byte value) {
            addCriterion("member_level =", value, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelNotEqualTo(Byte value) {
            addCriterion("member_level <>", value, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelGreaterThan(Byte value) {
            addCriterion("member_level >", value, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelGreaterThanOrEqualTo(Byte value) {
            addCriterion("member_level >=", value, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelLessThan(Byte value) {
            addCriterion("member_level <", value, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelLessThanOrEqualTo(Byte value) {
            addCriterion("member_level <=", value, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelIn(List<Byte> values) {
            addCriterion("member_level in", values, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelNotIn(List<Byte> values) {
            addCriterion("member_level not in", values, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelBetween(Byte value1, Byte value2) {
            addCriterion("member_level between", value1, value2, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andMemberLevelNotBetween(Byte value1, Byte value2) {
            addCriterion("member_level not between", value1, value2, "memberLevel");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitIsNull() {
            addCriterion("each_get_limit is null");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitIsNotNull() {
            addCriterion("each_get_limit is not null");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitEqualTo(Integer value) {
            addCriterion("each_get_limit =", value, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitNotEqualTo(Integer value) {
            addCriterion("each_get_limit <>", value, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitGreaterThan(Integer value) {
            addCriterion("each_get_limit >", value, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitGreaterThanOrEqualTo(Integer value) {
            addCriterion("each_get_limit >=", value, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitLessThan(Integer value) {
            addCriterion("each_get_limit <", value, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitLessThanOrEqualTo(Integer value) {
            addCriterion("each_get_limit <=", value, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitIn(List<Integer> values) {
            addCriterion("each_get_limit in", values, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitNotIn(List<Integer> values) {
            addCriterion("each_get_limit not in", values, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitBetween(Integer value1, Integer value2) {
            addCriterion("each_get_limit between", value1, value2, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andEachGetLimitNotBetween(Integer value1, Integer value2) {
            addCriterion("each_get_limit not between", value1, value2, "eachGetLimit");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageIsNull() {
            addCriterion("is_overlay_usage is null");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageIsNotNull() {
            addCriterion("is_overlay_usage is not null");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageEqualTo(Boolean value) {
            addCriterion("is_overlay_usage =", value, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageNotEqualTo(Boolean value) {
            addCriterion("is_overlay_usage <>", value, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageGreaterThan(Boolean value) {
            addCriterion("is_overlay_usage >", value, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_overlay_usage >=", value, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageLessThan(Boolean value) {
            addCriterion("is_overlay_usage <", value, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageLessThanOrEqualTo(Boolean value) {
            addCriterion("is_overlay_usage <=", value, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageIn(List<Boolean> values) {
            addCriterion("is_overlay_usage in", values, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageNotIn(List<Boolean> values) {
            addCriterion("is_overlay_usage not in", values, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageBetween(Boolean value1, Boolean value2) {
            addCriterion("is_overlay_usage between", value1, value2, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andIsOverlayUsageNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_overlay_usage not between", value1, value2, "isOverlayUsage");
            return (Criteria) this;
        }

        public Criteria andValidityTypeIsNull() {
            addCriterion("validity_type is null");
            return (Criteria) this;
        }

        public Criteria andValidityTypeIsNotNull() {
            addCriterion("validity_type is not null");
            return (Criteria) this;
        }

        public Criteria andValidityTypeEqualTo(Byte value) {
            addCriterion("validity_type =", value, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeNotEqualTo(Byte value) {
            addCriterion("validity_type <>", value, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeGreaterThan(Byte value) {
            addCriterion("validity_type >", value, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("validity_type >=", value, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeLessThan(Byte value) {
            addCriterion("validity_type <", value, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeLessThanOrEqualTo(Byte value) {
            addCriterion("validity_type <=", value, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeIn(List<Byte> values) {
            addCriterion("validity_type in", values, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeNotIn(List<Byte> values) {
            addCriterion("validity_type not in", values, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeBetween(Byte value1, Byte value2) {
            addCriterion("validity_type between", value1, value2, "validityType");
            return (Criteria) this;
        }

        public Criteria andValidityTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("validity_type not between", value1, value2, "validityType");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateIsNull() {
            addCriterion("absolute_valid_date is null");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateIsNotNull() {
            addCriterion("absolute_valid_date is not null");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateEqualTo(Date value) {
            addCriterion("absolute_valid_date =", value, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateNotEqualTo(Date value) {
            addCriterion("absolute_valid_date <>", value, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateGreaterThan(Date value) {
            addCriterion("absolute_valid_date >", value, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateGreaterThanOrEqualTo(Date value) {
            addCriterion("absolute_valid_date >=", value, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateLessThan(Date value) {
            addCriterion("absolute_valid_date <", value, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateLessThanOrEqualTo(Date value) {
            addCriterion("absolute_valid_date <=", value, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateIn(List<Date> values) {
            addCriterion("absolute_valid_date in", values, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateNotIn(List<Date> values) {
            addCriterion("absolute_valid_date not in", values, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateBetween(Date value1, Date value2) {
            addCriterion("absolute_valid_date between", value1, value2, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteValidDateNotBetween(Date value1, Date value2) {
            addCriterion("absolute_valid_date not between", value1, value2, "absoluteValidDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateIsNull() {
            addCriterion("absolute_expire_date is null");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateIsNotNull() {
            addCriterion("absolute_expire_date is not null");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateEqualTo(Date value) {
            addCriterion("absolute_expire_date =", value, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateNotEqualTo(Date value) {
            addCriterion("absolute_expire_date <>", value, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateGreaterThan(Date value) {
            addCriterion("absolute_expire_date >", value, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateGreaterThanOrEqualTo(Date value) {
            addCriterion("absolute_expire_date >=", value, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateLessThan(Date value) {
            addCriterion("absolute_expire_date <", value, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateLessThanOrEqualTo(Date value) {
            addCriterion("absolute_expire_date <=", value, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateIn(List<Date> values) {
            addCriterion("absolute_expire_date in", values, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateNotIn(List<Date> values) {
            addCriterion("absolute_expire_date not in", values, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateBetween(Date value1, Date value2) {
            addCriterion("absolute_expire_date between", value1, value2, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andAbsoluteExpireDateNotBetween(Date value1, Date value2) {
            addCriterion("absolute_expire_date not between", value1, value2, "absoluteExpireDate");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayIsNull() {
            addCriterion("relative_valid_day is null");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayIsNotNull() {
            addCriterion("relative_valid_day is not null");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayEqualTo(Integer value) {
            addCriterion("relative_valid_day =", value, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayNotEqualTo(Integer value) {
            addCriterion("relative_valid_day <>", value, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayGreaterThan(Integer value) {
            addCriterion("relative_valid_day >", value, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayGreaterThanOrEqualTo(Integer value) {
            addCriterion("relative_valid_day >=", value, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayLessThan(Integer value) {
            addCriterion("relative_valid_day <", value, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayLessThanOrEqualTo(Integer value) {
            addCriterion("relative_valid_day <=", value, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayIn(List<Integer> values) {
            addCriterion("relative_valid_day in", values, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayNotIn(List<Integer> values) {
            addCriterion("relative_valid_day not in", values, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayBetween(Integer value1, Integer value2) {
            addCriterion("relative_valid_day between", value1, value2, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andRelativeValidDayNotBetween(Integer value1, Integer value2) {
            addCriterion("relative_valid_day not between", value1, value2, "relativeValidDay");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(String value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(String value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(String value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(String value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(String value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(String value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLike(String value) {
            addCriterion("create_date like", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotLike(String value) {
            addCriterion("create_date not like", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<String> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<String> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(String value1, String value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(String value1, String value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(String value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(String value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(String value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(String value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(String value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(String value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLike(String value) {
            addCriterion("update_date like", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotLike(String value) {
            addCriterion("update_date not like", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<String> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<String> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(String value1, String value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(String value1, String value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIsNull() {
            addCriterion("online_date is null");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIsNotNull() {
            addCriterion("online_date is not null");
            return (Criteria) this;
        }

        public Criteria andOnlineDateEqualTo(String value) {
            addCriterion("online_date =", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotEqualTo(String value) {
            addCriterion("online_date <>", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateGreaterThan(String value) {
            addCriterion("online_date >", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateGreaterThanOrEqualTo(String value) {
            addCriterion("online_date >=", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateLessThan(String value) {
            addCriterion("online_date <", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateLessThanOrEqualTo(String value) {
            addCriterion("online_date <=", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateLike(String value) {
            addCriterion("online_date like", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotLike(String value) {
            addCriterion("online_date not like", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIn(List<String> values) {
            addCriterion("online_date in", values, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotIn(List<String> values) {
            addCriterion("online_date not in", values, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateBetween(String value1, String value2) {
            addCriterion("online_date between", value1, value2, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotBetween(String value1, String value2) {
            addCriterion("online_date not between", value1, value2, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIsNull() {
            addCriterion("offline_date is null");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIsNotNull() {
            addCriterion("offline_date is not null");
            return (Criteria) this;
        }

        public Criteria andOfflineDateEqualTo(String value) {
            addCriterion("offline_date =", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotEqualTo(String value) {
            addCriterion("offline_date <>", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateGreaterThan(String value) {
            addCriterion("offline_date >", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateGreaterThanOrEqualTo(String value) {
            addCriterion("offline_date >=", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateLessThan(String value) {
            addCriterion("offline_date <", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateLessThanOrEqualTo(String value) {
            addCriterion("offline_date <=", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateLike(String value) {
            addCriterion("offline_date like", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotLike(String value) {
            addCriterion("offline_date not like", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIn(List<String> values) {
            addCriterion("offline_date in", values, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotIn(List<String> values) {
            addCriterion("offline_date not in", values, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateBetween(String value1, String value2) {
            addCriterion("offline_date between", value1, value2, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotBetween(String value1, String value2) {
            addCriterion("offline_date not between", value1, value2, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNull() {
            addCriterion("operator_name is null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNotNull() {
            addCriterion("operator_name is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameEqualTo(String value) {
            addCriterion("operator_name =", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotEqualTo(String value) {
            addCriterion("operator_name <>", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThan(String value) {
            addCriterion("operator_name >", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThanOrEqualTo(String value) {
            addCriterion("operator_name >=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThan(String value) {
            addCriterion("operator_name <", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThanOrEqualTo(String value) {
            addCriterion("operator_name <=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLike(String value) {
            addCriterion("operator_name like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotLike(String value) {
            addCriterion("operator_name not like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIn(List<String> values) {
            addCriterion("operator_name in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotIn(List<String> values) {
            addCriterion("operator_name not in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameBetween(String value1, String value2) {
            addCriterion("operator_name between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotBetween(String value1, String value2) {
            addCriterion("operator_name not between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeIsNull() {
            addCriterion("operator_code is null");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeIsNotNull() {
            addCriterion("operator_code is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeEqualTo(String value) {
            addCriterion("operator_code =", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeNotEqualTo(String value) {
            addCriterion("operator_code <>", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeGreaterThan(String value) {
            addCriterion("operator_code >", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeGreaterThanOrEqualTo(String value) {
            addCriterion("operator_code >=", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeLessThan(String value) {
            addCriterion("operator_code <", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeLessThanOrEqualTo(String value) {
            addCriterion("operator_code <=", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeLike(String value) {
            addCriterion("operator_code like", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeNotLike(String value) {
            addCriterion("operator_code not like", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeIn(List<String> values) {
            addCriterion("operator_code in", values, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeNotIn(List<String> values) {
            addCriterion("operator_code not in", values, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeBetween(String value1, String value2) {
            addCriterion("operator_code between", value1, value2, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeNotBetween(String value1, String value2) {
            addCriterion("operator_code not between", value1, value2, "operatorCode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}