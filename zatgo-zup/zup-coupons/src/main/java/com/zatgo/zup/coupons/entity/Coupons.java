package com.zatgo.zup.coupons.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Coupons {
    private String couponsId;

    private String cloudUserId;

    private String couponsName;

    private BigDecimal distributionMoneyTotal;

    private Long distributionNumTotal;

    private BigDecimal assignedMoneyTotal;

    private Long assignedNumTotal;

    private Byte discountType;

    private BigDecimal couponsPrice;

    private BigDecimal howManyFull;

    private Byte userType;

    private Byte memberLevel;

    private Integer eachGetLimit;

    private Boolean isOverlayUsage;

    private Byte validityType;

    private Date absoluteValidDate;

    private Date absoluteExpireDate;

    private Integer relativeValidDay;

    private Byte status;

    private String createDate;

    private String updateDate;

    private String onlineDate;

    private String offlineDate;

    private String operatorName;

    private String operatorCode;

    private String usageExplain;

    public String getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(String couponsId) {
        this.couponsId = couponsId == null ? null : couponsId.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getCouponsName() {
        return couponsName;
    }

    public void setCouponsName(String couponsName) {
        this.couponsName = couponsName == null ? null : couponsName.trim();
    }

    public BigDecimal getDistributionMoneyTotal() {
        return distributionMoneyTotal;
    }

    public void setDistributionMoneyTotal(BigDecimal distributionMoneyTotal) {
        this.distributionMoneyTotal = distributionMoneyTotal;
    }

    public Long getDistributionNumTotal() {
        return distributionNumTotal;
    }

    public void setDistributionNumTotal(Long distributionNumTotal) {
        this.distributionNumTotal = distributionNumTotal;
    }

    public BigDecimal getAssignedMoneyTotal() {
        return assignedMoneyTotal;
    }

    public void setAssignedMoneyTotal(BigDecimal assignedMoneyTotal) {
        this.assignedMoneyTotal = assignedMoneyTotal;
    }

    public Long getAssignedNumTotal() {
        return assignedNumTotal;
    }

    public void setAssignedNumTotal(Long assignedNumTotal) {
        this.assignedNumTotal = assignedNumTotal;
    }

    public Byte getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Byte discountType) {
        this.discountType = discountType;
    }

    public BigDecimal getCouponsPrice() {
        return couponsPrice;
    }

    public void setCouponsPrice(BigDecimal couponsPrice) {
        this.couponsPrice = couponsPrice;
    }

    public BigDecimal getHowManyFull() {
        return howManyFull;
    }

    public void setHowManyFull(BigDecimal howManyFull) {
        this.howManyFull = howManyFull;
    }

    public Byte getUserType() {
        return userType;
    }

    public void setUserType(Byte userType) {
        this.userType = userType;
    }

    public Byte getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(Byte memberLevel) {
        this.memberLevel = memberLevel;
    }

    public Integer getEachGetLimit() {
        return eachGetLimit;
    }

    public void setEachGetLimit(Integer eachGetLimit) {
        this.eachGetLimit = eachGetLimit;
    }

    public Boolean getIsOverlayUsage() {
        return isOverlayUsage;
    }

    public void setIsOverlayUsage(Boolean isOverlayUsage) {
        this.isOverlayUsage = isOverlayUsage;
    }

    public Byte getValidityType() {
        return validityType;
    }

    public void setValidityType(Byte validityType) {
        this.validityType = validityType;
    }

    public Date getAbsoluteValidDate() {
        return absoluteValidDate;
    }

    public void setAbsoluteValidDate(Date absoluteValidDate) {
        this.absoluteValidDate = absoluteValidDate;
    }

    public Date getAbsoluteExpireDate() {
        return absoluteExpireDate;
    }

    public void setAbsoluteExpireDate(Date absoluteExpireDate) {
        this.absoluteExpireDate = absoluteExpireDate;
    }

    public Integer getRelativeValidDay() {
        return relativeValidDay;
    }

    public void setRelativeValidDay(Integer relativeValidDay) {
        this.relativeValidDay = relativeValidDay;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? null : updateDate.trim();
    }

    public String getOnlineDate() {
        return onlineDate;
    }

    public void setOnlineDate(String onlineDate) {
        this.onlineDate = onlineDate == null ? null : onlineDate.trim();
    }

    public String getOfflineDate() {
        return offlineDate;
    }

    public void setOfflineDate(String offlineDate) {
        this.offlineDate = offlineDate == null ? null : offlineDate.trim();
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode == null ? null : operatorCode.trim();
    }

    public String getUsageExplain() {
        return usageExplain;
    }

    public void setUsageExplain(String usageExplain) {
        this.usageExplain = usageExplain == null ? null : usageExplain.trim();
    }
}