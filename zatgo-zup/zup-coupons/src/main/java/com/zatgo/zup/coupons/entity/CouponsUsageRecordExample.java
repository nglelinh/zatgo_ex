package com.zatgo.zup.coupons.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CouponsUsageRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CouponsUsageRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCouponsUsageIdIsNull() {
            addCriterion("coupons_usage_id is null");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdIsNotNull() {
            addCriterion("coupons_usage_id is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdEqualTo(String value) {
            addCriterion("coupons_usage_id =", value, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdNotEqualTo(String value) {
            addCriterion("coupons_usage_id <>", value, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdGreaterThan(String value) {
            addCriterion("coupons_usage_id >", value, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_usage_id >=", value, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdLessThan(String value) {
            addCriterion("coupons_usage_id <", value, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdLessThanOrEqualTo(String value) {
            addCriterion("coupons_usage_id <=", value, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdLike(String value) {
            addCriterion("coupons_usage_id like", value, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdNotLike(String value) {
            addCriterion("coupons_usage_id not like", value, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdIn(List<String> values) {
            addCriterion("coupons_usage_id in", values, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdNotIn(List<String> values) {
            addCriterion("coupons_usage_id not in", values, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdBetween(String value1, String value2) {
            addCriterion("coupons_usage_id between", value1, value2, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsUsageIdNotBetween(String value1, String value2) {
            addCriterion("coupons_usage_id not between", value1, value2, "couponsUsageId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdIsNull() {
            addCriterion("coupons_order_id is null");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdIsNotNull() {
            addCriterion("coupons_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdEqualTo(String value) {
            addCriterion("coupons_order_id =", value, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdNotEqualTo(String value) {
            addCriterion("coupons_order_id <>", value, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdGreaterThan(String value) {
            addCriterion("coupons_order_id >", value, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_order_id >=", value, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdLessThan(String value) {
            addCriterion("coupons_order_id <", value, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdLessThanOrEqualTo(String value) {
            addCriterion("coupons_order_id <=", value, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdLike(String value) {
            addCriterion("coupons_order_id like", value, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdNotLike(String value) {
            addCriterion("coupons_order_id not like", value, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdIn(List<String> values) {
            addCriterion("coupons_order_id in", values, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdNotIn(List<String> values) {
            addCriterion("coupons_order_id not in", values, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdBetween(String value1, String value2) {
            addCriterion("coupons_order_id between", value1, value2, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andCouponsOrderIdNotBetween(String value1, String value2) {
            addCriterion("coupons_order_id not between", value1, value2, "couponsOrderId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdIsNull() {
            addCriterion("user_coupons_id is null");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdIsNotNull() {
            addCriterion("user_coupons_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdEqualTo(String value) {
            addCriterion("user_coupons_id =", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdNotEqualTo(String value) {
            addCriterion("user_coupons_id <>", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdGreaterThan(String value) {
            addCriterion("user_coupons_id >", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_coupons_id >=", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdLessThan(String value) {
            addCriterion("user_coupons_id <", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdLessThanOrEqualTo(String value) {
            addCriterion("user_coupons_id <=", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdLike(String value) {
            addCriterion("user_coupons_id like", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdNotLike(String value) {
            addCriterion("user_coupons_id not like", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdIn(List<String> values) {
            addCriterion("user_coupons_id in", values, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdNotIn(List<String> values) {
            addCriterion("user_coupons_id not in", values, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdBetween(String value1, String value2) {
            addCriterion("user_coupons_id between", value1, value2, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdNotBetween(String value1, String value2) {
            addCriterion("user_coupons_id not between", value1, value2, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUsageDateIsNull() {
            addCriterion("usage_date is null");
            return (Criteria) this;
        }

        public Criteria andUsageDateIsNotNull() {
            addCriterion("usage_date is not null");
            return (Criteria) this;
        }

        public Criteria andUsageDateEqualTo(Date value) {
            addCriterion("usage_date =", value, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateNotEqualTo(Date value) {
            addCriterion("usage_date <>", value, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateGreaterThan(Date value) {
            addCriterion("usage_date >", value, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateGreaterThanOrEqualTo(Date value) {
            addCriterion("usage_date >=", value, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateLessThan(Date value) {
            addCriterion("usage_date <", value, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateLessThanOrEqualTo(Date value) {
            addCriterion("usage_date <=", value, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateIn(List<Date> values) {
            addCriterion("usage_date in", values, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateNotIn(List<Date> values) {
            addCriterion("usage_date not in", values, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateBetween(Date value1, Date value2) {
            addCriterion("usage_date between", value1, value2, "usageDate");
            return (Criteria) this;
        }

        public Criteria andUsageDateNotBetween(Date value1, Date value2) {
            addCriterion("usage_date not between", value1, value2, "usageDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}