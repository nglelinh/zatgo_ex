package com.zatgo.zup.coupons.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;
import com.zatgo.zup.coupons.entity.CouponsOrderAbbr;
import com.zatgo.zup.coupons.entity.UserCoupons;
import com.zatgo.zup.coupons.entity.UserCouponsExample;

public interface UserCouponsMapper {
    int countByExample(UserCouponsExample example);

    int deleteByExample(UserCouponsExample example);

    int deleteByPrimaryKey(String userCouponsId);

    int insert(UserCoupons record);

    int insertSelective(UserCoupons record);

    List<UserCoupons> selectByExample(UserCouponsExample example);

    UserCoupons selectByPrimaryKey(String userCouponsId);

    int updateByExampleSelective(@Param("record") UserCoupons record, @Param("example") UserCouponsExample example);

    int updateByExample(@Param("record") UserCoupons record, @Param("example") UserCouponsExample example);

    int updateByPrimaryKeySelective(UserCoupons record);

    int updateByPrimaryKey(UserCoupons record);
    
    /**
     * 查询用户有效的优惠券
     * @param cloudUserId
     * @param userId
     * @param status 0=有效的，1=已使用的,2=未使用过期的
     * @return
     */
    Page<UserCouponsModel> getUserCouponsListByPage(@Param("cloudUserId") String cloudUserId, @Param("userId") String userId, @Param("status") Byte status);
    
    /**
     * 根据业务订单号查询已使用的优惠券
     * @param cloudUserId
     * @param busiOrderId
     * @return
     */
    List<UserCouponsModel> selectusedUserCouponsByBusiOrderId(@Param("cloudUserId") String cloudUserId,@Param("busiOrderId") String busiOrderId);
}