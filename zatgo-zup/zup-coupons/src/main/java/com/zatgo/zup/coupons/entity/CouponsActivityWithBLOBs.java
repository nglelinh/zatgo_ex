package com.zatgo.zup.coupons.entity;

public class CouponsActivityWithBLOBs extends CouponsActivity {
    private String activityRule;

    private String shareContent;

    public String getActivityRule() {
        return activityRule;
    }

    public void setActivityRule(String activityRule) {
        this.activityRule = activityRule == null ? null : activityRule.trim();
    }

    public String getShareContent() {
        return shareContent;
    }

    public void setShareContent(String shareContent) {
        this.shareContent = shareContent == null ? null : shareContent.trim();
    }
}