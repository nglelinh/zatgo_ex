package com.zatgo.zup.coupons.entity;

import java.util.Date;

public class CouponsUsageRecord {
    private String couponsUsageId;

    private String couponsOrderId;

    private String userCouponsId;

    private Date usageDate;

    public String getCouponsUsageId() {
        return couponsUsageId;
    }

    public void setCouponsUsageId(String couponsUsageId) {
        this.couponsUsageId = couponsUsageId == null ? null : couponsUsageId.trim();
    }

    public String getCouponsOrderId() {
        return couponsOrderId;
    }

    public void setCouponsOrderId(String couponsOrderId) {
        this.couponsOrderId = couponsOrderId == null ? null : couponsOrderId.trim();
    }

    public String getUserCouponsId() {
        return userCouponsId;
    }

    public void setUserCouponsId(String userCouponsId) {
        this.userCouponsId = userCouponsId == null ? null : userCouponsId.trim();
    }

    public Date getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(Date usageDate) {
        this.usageDate = usageDate;
    }
}