package com.zatgo.zup.coupons.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.coupons.entity.Coupons;
import com.zatgo.zup.coupons.model.CouponsSearchParams;
import com.zatgo.zup.coupons.service.CouponsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/coupons/admin",description = "优惠券维护接口")
@RestController
@RequestMapping(value = "/coupons/admin")
public class AdminCouponsController extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(AdminCouponsController.class);
	
	@Autowired
	private CouponsService couponsService;

	@RequestMapping(value = "/addCoupons",method = RequestMethod.POST)
	@ApiOperation(value = "新增优惠券", notes = "新增优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> addCoupons(@RequestBody Coupons params){
		AuthUserInfo adminUser = getAdminInfo();
		couponsService.addCoupons(adminUser, params);
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@RequestMapping(value = "/updateCoupons",method = RequestMethod.POST)
	@ApiOperation(value = "修改优惠券", notes = "修改优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> updateCoupons(@RequestBody Coupons params){
		AuthUserInfo adminUser = getAdminInfo();
		couponsService.updateCoupons(adminUser, params);
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@RequestMapping(value = "/getCoupons/{couponsId}",method = RequestMethod.GET)
	@ApiOperation(value = "查询优惠券", notes = "查询优惠券", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Coupons> getCouponsByAdmin(@PathVariable("couponsId") String couponsId){
		AuthUserInfo adminUser = getAdminInfo();
		Coupons coupons = couponsService.getCouponsById(adminUser.getCloudUserId(), couponsId);
		return BusinessResponseFactory.createSuccess(coupons);
	}
	
	@RequestMapping(value = "/getAllCoupons",method = RequestMethod.POST)
	@ApiOperation(value = "查询优惠券列表", notes = "查询优惠券列表", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<PageInfo<Coupons>> getAllCouponsListByParams(@RequestBody CouponsSearchParams params){
		AuthUserInfo adminUser = getAdminInfo();
		PageInfo<Coupons> couponsList = couponsService.getAllCouponsListByParams(adminUser, params);
		return BusinessResponseFactory.createSuccess(couponsList);
	}

}
