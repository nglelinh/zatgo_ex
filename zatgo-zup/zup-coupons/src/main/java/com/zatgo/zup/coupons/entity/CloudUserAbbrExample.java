package com.zatgo.zup.coupons.entity;

import java.util.ArrayList;
import java.util.List;

public class CloudUserAbbrExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CloudUserAbbrExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdIsNull() {
            addCriterion("weixin_official_acct_app_id is null");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdIsNotNull() {
            addCriterion("weixin_official_acct_app_id is not null");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdEqualTo(String value) {
            addCriterion("weixin_official_acct_app_id =", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdNotEqualTo(String value) {
            addCriterion("weixin_official_acct_app_id <>", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdGreaterThan(String value) {
            addCriterion("weixin_official_acct_app_id >", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdGreaterThanOrEqualTo(String value) {
            addCriterion("weixin_official_acct_app_id >=", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdLessThan(String value) {
            addCriterion("weixin_official_acct_app_id <", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdLessThanOrEqualTo(String value) {
            addCriterion("weixin_official_acct_app_id <=", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdLike(String value) {
            addCriterion("weixin_official_acct_app_id like", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdNotLike(String value) {
            addCriterion("weixin_official_acct_app_id not like", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdIn(List<String> values) {
            addCriterion("weixin_official_acct_app_id in", values, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdNotIn(List<String> values) {
            addCriterion("weixin_official_acct_app_id not in", values, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdBetween(String value1, String value2) {
            addCriterion("weixin_official_acct_app_id between", value1, value2, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdNotBetween(String value1, String value2) {
            addCriterion("weixin_official_acct_app_id not between", value1, value2, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyIsNull() {
            addCriterion("weixin_official_acct_key is null");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyIsNotNull() {
            addCriterion("weixin_official_acct_key is not null");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyEqualTo(String value) {
            addCriterion("weixin_official_acct_key =", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyNotEqualTo(String value) {
            addCriterion("weixin_official_acct_key <>", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyGreaterThan(String value) {
            addCriterion("weixin_official_acct_key >", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyGreaterThanOrEqualTo(String value) {
            addCriterion("weixin_official_acct_key >=", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyLessThan(String value) {
            addCriterion("weixin_official_acct_key <", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyLessThanOrEqualTo(String value) {
            addCriterion("weixin_official_acct_key <=", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyLike(String value) {
            addCriterion("weixin_official_acct_key like", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyNotLike(String value) {
            addCriterion("weixin_official_acct_key not like", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyIn(List<String> values) {
            addCriterion("weixin_official_acct_key in", values, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyNotIn(List<String> values) {
            addCriterion("weixin_official_acct_key not in", values, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyBetween(String value1, String value2) {
            addCriterion("weixin_official_acct_key between", value1, value2, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyNotBetween(String value1, String value2) {
            addCriterion("weixin_official_acct_key not between", value1, value2, "weixinOfficialAcctKey");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}