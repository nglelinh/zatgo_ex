package com.zatgo.zup.coupons.entity;

import java.util.Date;

public class UserCouponsGetRecord {
    private String couponsGetId;

    private String userId;

    private String couponsId;

    private String userCouponsId;

    private String couponsActivityId;

    private Date getDate;

    public String getCouponsGetId() {
        return couponsGetId;
    }

    public void setCouponsGetId(String couponsGetId) {
        this.couponsGetId = couponsGetId == null ? null : couponsGetId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(String couponsId) {
        this.couponsId = couponsId == null ? null : couponsId.trim();
    }

    public String getUserCouponsId() {
        return userCouponsId;
    }

    public void setUserCouponsId(String userCouponsId) {
        this.userCouponsId = userCouponsId == null ? null : userCouponsId.trim();
    }

    public String getCouponsActivityId() {
        return couponsActivityId;
    }

    public void setCouponsActivityId(String couponsActivityId) {
        this.couponsActivityId = couponsActivityId == null ? null : couponsActivityId.trim();
    }

    public Date getGetDate() {
        return getDate;
    }

    public void setGetDate(Date getDate) {
        this.getDate = getDate;
    }
}