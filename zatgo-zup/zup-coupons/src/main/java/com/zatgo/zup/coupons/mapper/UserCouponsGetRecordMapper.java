package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.UserCouponsGetRecord;
import com.zatgo.zup.coupons.entity.UserCouponsGetRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserCouponsGetRecordMapper {
    int countByExample(UserCouponsGetRecordExample example);

    int deleteByExample(UserCouponsGetRecordExample example);

    int deleteByPrimaryKey(String userCouponsGetId);

    int insert(UserCouponsGetRecord record);

    int insertSelective(UserCouponsGetRecord record);

    List<UserCouponsGetRecord> selectByExample(UserCouponsGetRecordExample example);

    UserCouponsGetRecord selectByPrimaryKey(String userCouponsGetId);

    int updateByExampleSelective(@Param("record") UserCouponsGetRecord record, @Param("example") UserCouponsGetRecordExample example);

    int updateByExample(@Param("record") UserCouponsGetRecord record, @Param("example") UserCouponsGetRecordExample example);

    int updateByPrimaryKeySelective(UserCouponsGetRecord record);

    int updateByPrimaryKey(UserCouponsGetRecord record);
}