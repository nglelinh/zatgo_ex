package com.zatgo.zup.coupons.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.payment.PrepayInfo;

@FeignClient("zup-payment-web")
public interface OrderRemoteService {

	@RequestMapping(value = "/pay/orders/prepay/{id}",method = RequestMethod.GET )
	@ResponseBody
	public ResponseData<PrepayInfo> selectOrderByPrePayId(@PathVariable("id") String prePayId);
}
