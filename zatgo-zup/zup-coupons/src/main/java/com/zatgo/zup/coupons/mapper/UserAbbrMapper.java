package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.UserAbbr;
import com.zatgo.zup.coupons.entity.UserAbbrExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserAbbrMapper {
    int countByExample(UserAbbrExample example);

    int deleteByExample(UserAbbrExample example);

    int deleteByPrimaryKey(String userId);

    int insert(UserAbbr record);

    int insertSelective(UserAbbr record);

    List<UserAbbr> selectByExample(UserAbbrExample example);

    UserAbbr selectByPrimaryKey(String userId);

    int updateByExampleSelective(@Param("record") UserAbbr record, @Param("example") UserAbbrExample example);

    int updateByExample(@Param("record") UserAbbr record, @Param("example") UserAbbrExample example);

    int updateByPrimaryKeySelective(UserAbbr record);

    int updateByPrimaryKey(UserAbbr record);
}