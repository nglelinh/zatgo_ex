package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.CouponRangeRel;
import com.zatgo.zup.coupons.entity.CouponRangeRelExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CouponRangeRelMapper {
    int countByExample(CouponRangeRelExample example);

    int deleteByExample(CouponRangeRelExample example);

    int deleteByPrimaryKey(String rangeId);

    int insert(CouponRangeRel record);

    int insertSelective(CouponRangeRel record);

    List<CouponRangeRel> selectByExample(CouponRangeRelExample example);

    CouponRangeRel selectByPrimaryKey(String rangeId);

    int updateByExampleSelective(@Param("record") CouponRangeRel record, @Param("example") CouponRangeRelExample example);

    int updateByExample(@Param("record") CouponRangeRel record, @Param("example") CouponRangeRelExample example);

    int updateByPrimaryKeySelective(CouponRangeRel record);

    int updateByPrimaryKey(CouponRangeRel record);
}