package com.zatgo.zup.coupons.entity;

import java.util.Date;

public class CouponsActivity {
    private String couponsActivityId;

    private String cloudUserId;

    private String triggerBusinessType;

    private String triggerBusinessCode;

    private Byte getType;

    private String activityName;

    private Date activityStartDate;

    private Date activityEndDate;

    private String activityPictureUrl;

    private Byte activityStatus;

    private Boolean isAllowShare;

    private String shareTitle;

    private String sharePictureUrl;

    private String operatorName;

    private String operatorCode;

    private Date createDate;

    private Date updateDate;

    private Date onlineDate;

    private Date offlineDate;

    public String getCouponsActivityId() {
        return couponsActivityId;
    }

    public void setCouponsActivityId(String couponsActivityId) {
        this.couponsActivityId = couponsActivityId == null ? null : couponsActivityId.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getTriggerBusinessType() {
        return triggerBusinessType;
    }

    public void setTriggerBusinessType(String triggerBusinessType) {
        this.triggerBusinessType = triggerBusinessType == null ? null : triggerBusinessType.trim();
    }

    public String getTriggerBusinessCode() {
        return triggerBusinessCode;
    }

    public void setTriggerBusinessCode(String triggerBusinessCode) {
        this.triggerBusinessCode = triggerBusinessCode == null ? null : triggerBusinessCode.trim();
    }

    public Byte getGetType() {
        return getType;
    }

    public void setGetType(Byte getType) {
        this.getType = getType;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName == null ? null : activityName.trim();
    }

    public Date getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(Date activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public Date getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(Date activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getActivityPictureUrl() {
        return activityPictureUrl;
    }

    public void setActivityPictureUrl(String activityPictureUrl) {
        this.activityPictureUrl = activityPictureUrl == null ? null : activityPictureUrl.trim();
    }

    public Byte getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Byte activityStatus) {
        this.activityStatus = activityStatus;
    }

    public Boolean getIsAllowShare() {
        return isAllowShare;
    }

    public void setIsAllowShare(Boolean isAllowShare) {
        this.isAllowShare = isAllowShare;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle == null ? null : shareTitle.trim();
    }

    public String getSharePictureUrl() {
        return sharePictureUrl;
    }

    public void setSharePictureUrl(String sharePictureUrl) {
        this.sharePictureUrl = sharePictureUrl == null ? null : sharePictureUrl.trim();
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode == null ? null : operatorCode.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getOnlineDate() {
        return onlineDate;
    }

    public void setOnlineDate(Date onlineDate) {
        this.onlineDate = onlineDate;
    }

    public Date getOfflineDate() {
        return offlineDate;
    }

    public void setOfflineDate(Date offlineDate) {
        this.offlineDate = offlineDate;
    }
}