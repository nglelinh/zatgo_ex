package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.CouponsUsageRecord;
import com.zatgo.zup.coupons.entity.CouponsUsageRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CouponsUsageRecordMapper {
    int countByExample(CouponsUsageRecordExample example);

    int deleteByExample(CouponsUsageRecordExample example);

    int deleteByPrimaryKey(String couponsUsageId);

    int insert(CouponsUsageRecord record);

    int insertSelective(CouponsUsageRecord record);

    List<CouponsUsageRecord> selectByExample(CouponsUsageRecordExample example);

    CouponsUsageRecord selectByPrimaryKey(String couponsUsageId);

    int updateByExampleSelective(@Param("record") CouponsUsageRecord record, @Param("example") CouponsUsageRecordExample example);

    int updateByExample(@Param("record") CouponsUsageRecord record, @Param("example") CouponsUsageRecordExample example);

    int updateByPrimaryKeySelective(CouponsUsageRecord record);

    int updateByPrimaryKey(CouponsUsageRecord record);
}