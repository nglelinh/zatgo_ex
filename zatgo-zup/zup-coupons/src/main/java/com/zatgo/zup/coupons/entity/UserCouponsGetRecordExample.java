package com.zatgo.zup.coupons.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserCouponsGetRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserCouponsGetRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCouponsGetIdIsNull() {
            addCriterion("coupons_get_id is null");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdIsNotNull() {
            addCriterion("coupons_get_id is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdEqualTo(String value) {
            addCriterion("coupons_get_id =", value, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdNotEqualTo(String value) {
            addCriterion("coupons_get_id <>", value, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdGreaterThan(String value) {
            addCriterion("coupons_get_id >", value, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_get_id >=", value, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdLessThan(String value) {
            addCriterion("coupons_get_id <", value, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdLessThanOrEqualTo(String value) {
            addCriterion("coupons_get_id <=", value, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdLike(String value) {
            addCriterion("coupons_get_id like", value, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdNotLike(String value) {
            addCriterion("coupons_get_id not like", value, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdIn(List<String> values) {
            addCriterion("coupons_get_id in", values, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdNotIn(List<String> values) {
            addCriterion("coupons_get_id not in", values, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdBetween(String value1, String value2) {
            addCriterion("coupons_get_id between", value1, value2, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andCouponsGetIdNotBetween(String value1, String value2) {
            addCriterion("coupons_get_id not between", value1, value2, "couponsGetId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdIsNull() {
            addCriterion("coupons_id is null");
            return (Criteria) this;
        }

        public Criteria andCouponsIdIsNotNull() {
            addCriterion("coupons_id is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsIdEqualTo(String value) {
            addCriterion("coupons_id =", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotEqualTo(String value) {
            addCriterion("coupons_id <>", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdGreaterThan(String value) {
            addCriterion("coupons_id >", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_id >=", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLessThan(String value) {
            addCriterion("coupons_id <", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLessThanOrEqualTo(String value) {
            addCriterion("coupons_id <=", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLike(String value) {
            addCriterion("coupons_id like", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotLike(String value) {
            addCriterion("coupons_id not like", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdIn(List<String> values) {
            addCriterion("coupons_id in", values, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotIn(List<String> values) {
            addCriterion("coupons_id not in", values, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdBetween(String value1, String value2) {
            addCriterion("coupons_id between", value1, value2, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotBetween(String value1, String value2) {
            addCriterion("coupons_id not between", value1, value2, "couponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdIsNull() {
            addCriterion("user_coupons_id is null");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdIsNotNull() {
            addCriterion("user_coupons_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdEqualTo(String value) {
            addCriterion("user_coupons_id =", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdNotEqualTo(String value) {
            addCriterion("user_coupons_id <>", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdGreaterThan(String value) {
            addCriterion("user_coupons_id >", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_coupons_id >=", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdLessThan(String value) {
            addCriterion("user_coupons_id <", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdLessThanOrEqualTo(String value) {
            addCriterion("user_coupons_id <=", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdLike(String value) {
            addCriterion("user_coupons_id like", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdNotLike(String value) {
            addCriterion("user_coupons_id not like", value, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdIn(List<String> values) {
            addCriterion("user_coupons_id in", values, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdNotIn(List<String> values) {
            addCriterion("user_coupons_id not in", values, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdBetween(String value1, String value2) {
            addCriterion("user_coupons_id between", value1, value2, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andUserCouponsIdNotBetween(String value1, String value2) {
            addCriterion("user_coupons_id not between", value1, value2, "userCouponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdIsNull() {
            addCriterion("coupons_activity_id is null");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdIsNotNull() {
            addCriterion("coupons_activity_id is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdEqualTo(String value) {
            addCriterion("coupons_activity_id =", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdNotEqualTo(String value) {
            addCriterion("coupons_activity_id <>", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdGreaterThan(String value) {
            addCriterion("coupons_activity_id >", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_activity_id >=", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdLessThan(String value) {
            addCriterion("coupons_activity_id <", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdLessThanOrEqualTo(String value) {
            addCriterion("coupons_activity_id <=", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdLike(String value) {
            addCriterion("coupons_activity_id like", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdNotLike(String value) {
            addCriterion("coupons_activity_id not like", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdIn(List<String> values) {
            addCriterion("coupons_activity_id in", values, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdNotIn(List<String> values) {
            addCriterion("coupons_activity_id not in", values, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdBetween(String value1, String value2) {
            addCriterion("coupons_activity_id between", value1, value2, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdNotBetween(String value1, String value2) {
            addCriterion("coupons_activity_id not between", value1, value2, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andGetDateIsNull() {
            addCriterion("get_date is null");
            return (Criteria) this;
        }

        public Criteria andGetDateIsNotNull() {
            addCriterion("get_date is not null");
            return (Criteria) this;
        }

        public Criteria andGetDateEqualTo(Date value) {
            addCriterion("get_date =", value, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateNotEqualTo(Date value) {
            addCriterion("get_date <>", value, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateGreaterThan(Date value) {
            addCriterion("get_date >", value, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateGreaterThanOrEqualTo(Date value) {
            addCriterion("get_date >=", value, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateLessThan(Date value) {
            addCriterion("get_date <", value, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateLessThanOrEqualTo(Date value) {
            addCriterion("get_date <=", value, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateIn(List<Date> values) {
            addCriterion("get_date in", values, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateNotIn(List<Date> values) {
            addCriterion("get_date not in", values, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateBetween(Date value1, Date value2) {
            addCriterion("get_date between", value1, value2, "getDate");
            return (Criteria) this;
        }

        public Criteria andGetDateNotBetween(Date value1, Date value2) {
            addCriterion("get_date not between", value1, value2, "getDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}