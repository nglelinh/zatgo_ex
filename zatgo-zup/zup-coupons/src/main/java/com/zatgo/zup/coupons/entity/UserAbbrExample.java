package com.zatgo.zup.coupons.entity;

import java.util.ArrayList;
import java.util.List;

public class UserAbbrExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserAbbrExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdIsNull() {
            addCriterion("busi_user_id is null");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdIsNotNull() {
            addCriterion("busi_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdEqualTo(String value) {
            addCriterion("busi_user_id =", value, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdNotEqualTo(String value) {
            addCriterion("busi_user_id <>", value, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdGreaterThan(String value) {
            addCriterion("busi_user_id >", value, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("busi_user_id >=", value, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdLessThan(String value) {
            addCriterion("busi_user_id <", value, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdLessThanOrEqualTo(String value) {
            addCriterion("busi_user_id <=", value, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdLike(String value) {
            addCriterion("busi_user_id like", value, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdNotLike(String value) {
            addCriterion("busi_user_id not like", value, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdIn(List<String> values) {
            addCriterion("busi_user_id in", values, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdNotIn(List<String> values) {
            addCriterion("busi_user_id not in", values, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdBetween(String value1, String value2) {
            addCriterion("busi_user_id between", value1, value2, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andBusiUserIdNotBetween(String value1, String value2) {
            addCriterion("busi_user_id not between", value1, value2, "busiUserId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdIsNull() {
            addCriterion("weixin_open_id is null");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdIsNotNull() {
            addCriterion("weixin_open_id is not null");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdEqualTo(String value) {
            addCriterion("weixin_open_id =", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdNotEqualTo(String value) {
            addCriterion("weixin_open_id <>", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdGreaterThan(String value) {
            addCriterion("weixin_open_id >", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdGreaterThanOrEqualTo(String value) {
            addCriterion("weixin_open_id >=", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdLessThan(String value) {
            addCriterion("weixin_open_id <", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdLessThanOrEqualTo(String value) {
            addCriterion("weixin_open_id <=", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdLike(String value) {
            addCriterion("weixin_open_id like", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdNotLike(String value) {
            addCriterion("weixin_open_id not like", value, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdIn(List<String> values) {
            addCriterion("weixin_open_id in", values, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdNotIn(List<String> values) {
            addCriterion("weixin_open_id not in", values, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdBetween(String value1, String value2) {
            addCriterion("weixin_open_id between", value1, value2, "weixinOpenId");
            return (Criteria) this;
        }

        public Criteria andWeixinOpenIdNotBetween(String value1, String value2) {
            addCriterion("weixin_open_id not between", value1, value2, "weixinOpenId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}