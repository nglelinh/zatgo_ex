package com.zatgo.zup.coupons.entity;

public class UserAbbr {
    private String userId;

    private String cloudUserId;

    private String busiUserId;

    private String weixinOpenId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getBusiUserId() {
        return busiUserId;
    }

    public void setBusiUserId(String busiUserId) {
        this.busiUserId = busiUserId == null ? null : busiUserId.trim();
    }

    public String getWeixinOpenId() {
        return weixinOpenId;
    }

    public void setWeixinOpenId(String weixinOpenId) {
        this.weixinOpenId = weixinOpenId == null ? null : weixinOpenId.trim();
    }
}