package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.CouponsActivityRel;
import com.zatgo.zup.coupons.entity.CouponsActivityRelExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CouponsActivityRelMapper {
    int countByExample(CouponsActivityRelExample example);

    int deleteByExample(CouponsActivityRelExample example);

    int deleteByPrimaryKey(String relId);

    int insert(CouponsActivityRel record);

    int insertSelective(CouponsActivityRel record);

    List<CouponsActivityRel> selectByExample(CouponsActivityRelExample example);

    CouponsActivityRel selectByPrimaryKey(String relId);

    int updateByExampleSelective(@Param("record") CouponsActivityRel record, @Param("example") CouponsActivityRelExample example);

    int updateByExample(@Param("record") CouponsActivityRel record, @Param("example") CouponsActivityRelExample example);

    int updateByPrimaryKeySelective(CouponsActivityRel record);

    int updateByPrimaryKey(CouponsActivityRel record);
}