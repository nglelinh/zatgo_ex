package com.zatgo.zup.coupons.model;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class DiscountEventSearchParams {

	@ApiModelProperty(value = "状态：0=录入\r\n" + 
			"1=上线\r\n" + 
			"2=下线",required = false)
	private Byte eventStatus;
	
	@ApiModelProperty(value = "领取方式：0=用户主动领取\r\n" + 
			"1=系统自动触发",required = false)
	private Byte getType;
	
	@ApiModelProperty(value = "活动开始时间",required = false)
	private Date eventStartDate;
	
	@ApiModelProperty(value = "活动结束时间",required = false)
	private Date eventEndDate;
	
	@ApiModelProperty(value = "活动名称",required = false)
	private String eventName;
	
	@ApiModelProperty(value = "页码",required = false)
	private int pageNo;
	
	@ApiModelProperty(value = "每页条数",required = false)
	private int pageSize;

	public Byte getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(Byte eventStatus) {
		this.eventStatus = eventStatus;
	}

	public Byte getGetType() {
		return getType;
	}

	public void setGetType(Byte getType) {
		this.getType = getType;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
