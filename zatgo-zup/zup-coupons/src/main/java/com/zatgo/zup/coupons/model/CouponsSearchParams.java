package com.zatgo.zup.coupons.model;

import io.swagger.annotations.ApiModelProperty;

public class CouponsSearchParams {

	@ApiModelProperty(value = "优惠券状态：0=录入\r\n" + 
			"1=上线\r\n" + 
			"2=下线",required = false)
	private Byte status;
	
	@ApiModelProperty(value = "优惠形式：0=指定金额\r\n" + 
			"1=折扣",required = false)
	private Byte discountType;
	
	@ApiModelProperty(value = "有效期类型：0=固定日期\r\n" + 
			"1=领到券当日开始N天内有效\r\n" + 
			"2=领到券次日开始N天内有效",required = false)
	private Byte validityType;
	
	@ApiModelProperty(value = "页码",required = false)
	private int pageNo;
	
	@ApiModelProperty(value = "每页条数",required = false)
	private int pageSize;

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Byte getDiscountType() {
		return discountType;
	}

	public void setDiscountType(Byte discountType) {
		this.discountType = discountType;
	}

	public Byte getValidityType() {
		return validityType;
	}

	public void setValidityType(Byte validityType) {
		this.validityType = validityType;
	}
	
	
}
