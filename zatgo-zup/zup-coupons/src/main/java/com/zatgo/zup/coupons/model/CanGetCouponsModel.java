package com.zatgo.zup.coupons.model;

import com.zatgo.zup.coupons.entity.Coupons;

import io.swagger.annotations.ApiModelProperty;

public class CanGetCouponsModel extends Coupons {

	@ApiModelProperty(value = "当前优惠券还能领取的数量",required = false)
	private Integer canGetNum;
	
	@ApiModelProperty(value = "当前优惠券已领取的数量",required = false)
	private Integer gotNum;

	public Integer getCanGetNum() {
		return canGetNum;
	}

	public void setCanGetNum(Integer canGetNum) {
		this.canGetNum = canGetNum;
	}

	public Integer getGotNum() {
		return gotNum;
	}

	public void setGotNum(Integer gotNum) {
		this.gotNum = gotNum;
	}
	
	
}
