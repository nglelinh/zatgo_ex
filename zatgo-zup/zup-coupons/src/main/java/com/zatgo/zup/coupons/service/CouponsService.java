package com.zatgo.zup.coupons.service;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.coupons.entity.Coupons;
import com.zatgo.zup.coupons.model.CouponsSearchParams;

public interface CouponsService {

	/**
	 * 新增优惠券
	 * @param adminUser
	 * @param params
	 * @return
	 */
	public Boolean addCoupons(AuthUserInfo adminUser,Coupons params);

	/**
	 * 修改优惠券
	 * @param adminUser
	 * @param params
	 * @return
	 */
	public Boolean updateCoupons(AuthUserInfo adminUser,Coupons params);

	/**
	 * 查询优惠券
	 * @param cloudUserId
	 * @param couponsId
	 * @return
	 */
	public Coupons getCouponsById(String cloudUserId,String couponsId);

	/**
	 * 查询优惠券列表
	 * @param adminUser
	 * @param params
	 * @return
	 */
	public PageInfo<Coupons> getAllCouponsListByParams(AuthUserInfo adminUser,CouponsSearchParams params);

}
