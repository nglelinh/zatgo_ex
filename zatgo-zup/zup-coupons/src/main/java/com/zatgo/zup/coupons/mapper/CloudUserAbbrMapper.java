package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.CloudUserAbbr;
import com.zatgo.zup.coupons.entity.CloudUserAbbrExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CloudUserAbbrMapper {
    int countByExample(CloudUserAbbrExample example);

    int deleteByExample(CloudUserAbbrExample example);

    int deleteByPrimaryKey(String cloudUserId);

    int insert(CloudUserAbbr record);

    int insertSelective(CloudUserAbbr record);

    List<CloudUserAbbr> selectByExample(CloudUserAbbrExample example);

    int updateByExampleSelective(@Param("record") CloudUserAbbr record, @Param("example") CloudUserAbbrExample example);

    int updateByExample(@Param("record") CloudUserAbbr record, @Param("example") CloudUserAbbrExample example);
}