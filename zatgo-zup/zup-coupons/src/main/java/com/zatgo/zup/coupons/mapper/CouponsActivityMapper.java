package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.CouponsActivity;
import com.zatgo.zup.coupons.entity.CouponsActivityExample;
import com.zatgo.zup.coupons.entity.CouponsActivityWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CouponsActivityMapper {
    int countByExample(CouponsActivityExample example);

    int deleteByExample(CouponsActivityExample example);

    int deleteByPrimaryKey(String couponsActivityId);

    int insert(CouponsActivityWithBLOBs record);

    int insertSelective(CouponsActivityWithBLOBs record);

    List<CouponsActivityWithBLOBs> selectByExampleWithBLOBs(CouponsActivityExample example);

    List<CouponsActivity> selectByExample(CouponsActivityExample example);

    CouponsActivityWithBLOBs selectByPrimaryKey(String couponsActivityId);

    int updateByExampleSelective(@Param("record") CouponsActivityWithBLOBs record, @Param("example") CouponsActivityExample example);

    int updateByExampleWithBLOBs(@Param("record") CouponsActivityWithBLOBs record, @Param("example") CouponsActivityExample example);

    int updateByExample(@Param("record") CouponsActivity record, @Param("example") CouponsActivityExample example);

    int updateByPrimaryKeySelective(CouponsActivityWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(CouponsActivityWithBLOBs record);

    int updateByPrimaryKey(CouponsActivity record);
    
    List<CouponsActivity> getValidCouponsActivityByCouponsId(@Param("couponsId") String couponsId);
    
    List<CouponsActivity> getValidCouponsActivityByTriggerBusiness(
    		@Param("cloudUserId") String cloudUserId,
    		@Param("triggerBusinessType") String triggerBusinessType
    		,@Param("triggerBusinessCode") String triggerBusinessCode);
}