package com.zatgo.zup.coupons.service;

import java.util.List;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.coupons.entity.Coupons;
import com.zatgo.zup.coupons.entity.CouponsActivity;
import com.zatgo.zup.coupons.model.DiscountEventSearchParams;

public interface CouponsActivityService {

	public Boolean addActivity(AuthUserInfo adminUser,CouponsActivity params);
	
	public Boolean updateActivity(AuthUserInfo adminUser,CouponsActivity params);
	
	public Boolean getActivityByIdAndAdmin(AuthUserInfo adminUser,String activityId);
	
	public PageInfo<Coupons> getAllActivityListByParams(AuthUserInfo adminUser,DiscountEventSearchParams params);

	public List<Coupons> getCouponsByActivity(AuthUserInfo adminUser,String activityId);
}
