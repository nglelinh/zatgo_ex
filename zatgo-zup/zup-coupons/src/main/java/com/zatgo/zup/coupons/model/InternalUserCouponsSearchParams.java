package com.zatgo.zup.coupons.model;

import io.swagger.annotations.ApiModelProperty;

public class InternalUserCouponsSearchParams {

	@ApiModelProperty(value = "云商户ID",required = false)
	private String cloudUserId;
	
	@ApiModelProperty(value = "用户ID",required = false)
	private String userId;
	
	@ApiModelProperty(value = "状态：0=有效的，1=已使用的,2=过期未使用的",required = false)
	private Byte status;
	
	@ApiModelProperty(value = "页码",required = false)
	private int pageNo;
	
	@ApiModelProperty(value = "每页条数",required = false)
	private int pageSize;

	public String getCloudUserId() {
		return cloudUserId;
	}

	public void setCloudUserId(String cloudUserId) {
		this.cloudUserId = cloudUserId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
