package com.zatgo.zup.coupons.model;

import io.swagger.annotations.ApiModelProperty;

public class UsageCouponsOrderParams {

	@ApiModelProperty(value = "订单ID",required = true)
	private String prePayId;

	public String getPrePayId() {
		return prePayId;
	}

	public void setPrePayId(String prePayId) {
		this.prePayId = prePayId;
	}

	
	
	
}
