package com.zatgo.zup.coupons.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CouponsActivityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CouponsActivityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCouponsActivityIdIsNull() {
            addCriterion("coupons_activity_id is null");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdIsNotNull() {
            addCriterion("coupons_activity_id is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdEqualTo(String value) {
            addCriterion("coupons_activity_id =", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdNotEqualTo(String value) {
            addCriterion("coupons_activity_id <>", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdGreaterThan(String value) {
            addCriterion("coupons_activity_id >", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_activity_id >=", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdLessThan(String value) {
            addCriterion("coupons_activity_id <", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdLessThanOrEqualTo(String value) {
            addCriterion("coupons_activity_id <=", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdLike(String value) {
            addCriterion("coupons_activity_id like", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdNotLike(String value) {
            addCriterion("coupons_activity_id not like", value, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdIn(List<String> values) {
            addCriterion("coupons_activity_id in", values, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdNotIn(List<String> values) {
            addCriterion("coupons_activity_id not in", values, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdBetween(String value1, String value2) {
            addCriterion("coupons_activity_id between", value1, value2, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCouponsActivityIdNotBetween(String value1, String value2) {
            addCriterion("coupons_activity_id not between", value1, value2, "couponsActivityId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeIsNull() {
            addCriterion("trigger_business_type is null");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeIsNotNull() {
            addCriterion("trigger_business_type is not null");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeEqualTo(String value) {
            addCriterion("trigger_business_type =", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeNotEqualTo(String value) {
            addCriterion("trigger_business_type <>", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeGreaterThan(String value) {
            addCriterion("trigger_business_type >", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeGreaterThanOrEqualTo(String value) {
            addCriterion("trigger_business_type >=", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeLessThan(String value) {
            addCriterion("trigger_business_type <", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeLessThanOrEqualTo(String value) {
            addCriterion("trigger_business_type <=", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeLike(String value) {
            addCriterion("trigger_business_type like", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeNotLike(String value) {
            addCriterion("trigger_business_type not like", value, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeIn(List<String> values) {
            addCriterion("trigger_business_type in", values, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeNotIn(List<String> values) {
            addCriterion("trigger_business_type not in", values, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeBetween(String value1, String value2) {
            addCriterion("trigger_business_type between", value1, value2, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessTypeNotBetween(String value1, String value2) {
            addCriterion("trigger_business_type not between", value1, value2, "triggerBusinessType");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeIsNull() {
            addCriterion("trigger_business_code is null");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeIsNotNull() {
            addCriterion("trigger_business_code is not null");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeEqualTo(String value) {
            addCriterion("trigger_business_code =", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeNotEqualTo(String value) {
            addCriterion("trigger_business_code <>", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeGreaterThan(String value) {
            addCriterion("trigger_business_code >", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeGreaterThanOrEqualTo(String value) {
            addCriterion("trigger_business_code >=", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeLessThan(String value) {
            addCriterion("trigger_business_code <", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeLessThanOrEqualTo(String value) {
            addCriterion("trigger_business_code <=", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeLike(String value) {
            addCriterion("trigger_business_code like", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeNotLike(String value) {
            addCriterion("trigger_business_code not like", value, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeIn(List<String> values) {
            addCriterion("trigger_business_code in", values, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeNotIn(List<String> values) {
            addCriterion("trigger_business_code not in", values, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeBetween(String value1, String value2) {
            addCriterion("trigger_business_code between", value1, value2, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andTriggerBusinessCodeNotBetween(String value1, String value2) {
            addCriterion("trigger_business_code not between", value1, value2, "triggerBusinessCode");
            return (Criteria) this;
        }

        public Criteria andGetTypeIsNull() {
            addCriterion("get_type is null");
            return (Criteria) this;
        }

        public Criteria andGetTypeIsNotNull() {
            addCriterion("get_type is not null");
            return (Criteria) this;
        }

        public Criteria andGetTypeEqualTo(Byte value) {
            addCriterion("get_type =", value, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeNotEqualTo(Byte value) {
            addCriterion("get_type <>", value, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeGreaterThan(Byte value) {
            addCriterion("get_type >", value, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("get_type >=", value, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeLessThan(Byte value) {
            addCriterion("get_type <", value, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeLessThanOrEqualTo(Byte value) {
            addCriterion("get_type <=", value, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeIn(List<Byte> values) {
            addCriterion("get_type in", values, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeNotIn(List<Byte> values) {
            addCriterion("get_type not in", values, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeBetween(Byte value1, Byte value2) {
            addCriterion("get_type between", value1, value2, "getType");
            return (Criteria) this;
        }

        public Criteria andGetTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("get_type not between", value1, value2, "getType");
            return (Criteria) this;
        }

        public Criteria andActivityNameIsNull() {
            addCriterion("activity_name is null");
            return (Criteria) this;
        }

        public Criteria andActivityNameIsNotNull() {
            addCriterion("activity_name is not null");
            return (Criteria) this;
        }

        public Criteria andActivityNameEqualTo(String value) {
            addCriterion("activity_name =", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotEqualTo(String value) {
            addCriterion("activity_name <>", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameGreaterThan(String value) {
            addCriterion("activity_name >", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameGreaterThanOrEqualTo(String value) {
            addCriterion("activity_name >=", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLessThan(String value) {
            addCriterion("activity_name <", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLessThanOrEqualTo(String value) {
            addCriterion("activity_name <=", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLike(String value) {
            addCriterion("activity_name like", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotLike(String value) {
            addCriterion("activity_name not like", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameIn(List<String> values) {
            addCriterion("activity_name in", values, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotIn(List<String> values) {
            addCriterion("activity_name not in", values, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameBetween(String value1, String value2) {
            addCriterion("activity_name between", value1, value2, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotBetween(String value1, String value2) {
            addCriterion("activity_name not between", value1, value2, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIsNull() {
            addCriterion("activity_start_date is null");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIsNotNull() {
            addCriterion("activity_start_date is not null");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateEqualTo(Date value) {
            addCriterion("activity_start_date =", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotEqualTo(Date value) {
            addCriterion("activity_start_date <>", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateGreaterThan(Date value) {
            addCriterion("activity_start_date >", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("activity_start_date >=", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateLessThan(Date value) {
            addCriterion("activity_start_date <", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateLessThanOrEqualTo(Date value) {
            addCriterion("activity_start_date <=", value, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateIn(List<Date> values) {
            addCriterion("activity_start_date in", values, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotIn(List<Date> values) {
            addCriterion("activity_start_date not in", values, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateBetween(Date value1, Date value2) {
            addCriterion("activity_start_date between", value1, value2, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityStartDateNotBetween(Date value1, Date value2) {
            addCriterion("activity_start_date not between", value1, value2, "activityStartDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIsNull() {
            addCriterion("activity_end_date is null");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIsNotNull() {
            addCriterion("activity_end_date is not null");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateEqualTo(Date value) {
            addCriterion("activity_end_date =", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotEqualTo(Date value) {
            addCriterion("activity_end_date <>", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateGreaterThan(Date value) {
            addCriterion("activity_end_date >", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("activity_end_date >=", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateLessThan(Date value) {
            addCriterion("activity_end_date <", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateLessThanOrEqualTo(Date value) {
            addCriterion("activity_end_date <=", value, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateIn(List<Date> values) {
            addCriterion("activity_end_date in", values, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotIn(List<Date> values) {
            addCriterion("activity_end_date not in", values, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateBetween(Date value1, Date value2) {
            addCriterion("activity_end_date between", value1, value2, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityEndDateNotBetween(Date value1, Date value2) {
            addCriterion("activity_end_date not between", value1, value2, "activityEndDate");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlIsNull() {
            addCriterion("activity_picture_url is null");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlIsNotNull() {
            addCriterion("activity_picture_url is not null");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlEqualTo(String value) {
            addCriterion("activity_picture_url =", value, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlNotEqualTo(String value) {
            addCriterion("activity_picture_url <>", value, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlGreaterThan(String value) {
            addCriterion("activity_picture_url >", value, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlGreaterThanOrEqualTo(String value) {
            addCriterion("activity_picture_url >=", value, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlLessThan(String value) {
            addCriterion("activity_picture_url <", value, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlLessThanOrEqualTo(String value) {
            addCriterion("activity_picture_url <=", value, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlLike(String value) {
            addCriterion("activity_picture_url like", value, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlNotLike(String value) {
            addCriterion("activity_picture_url not like", value, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlIn(List<String> values) {
            addCriterion("activity_picture_url in", values, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlNotIn(List<String> values) {
            addCriterion("activity_picture_url not in", values, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlBetween(String value1, String value2) {
            addCriterion("activity_picture_url between", value1, value2, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityPictureUrlNotBetween(String value1, String value2) {
            addCriterion("activity_picture_url not between", value1, value2, "activityPictureUrl");
            return (Criteria) this;
        }

        public Criteria andActivityStatusIsNull() {
            addCriterion("activity_status is null");
            return (Criteria) this;
        }

        public Criteria andActivityStatusIsNotNull() {
            addCriterion("activity_status is not null");
            return (Criteria) this;
        }

        public Criteria andActivityStatusEqualTo(Byte value) {
            addCriterion("activity_status =", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusNotEqualTo(Byte value) {
            addCriterion("activity_status <>", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusGreaterThan(Byte value) {
            addCriterion("activity_status >", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("activity_status >=", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusLessThan(Byte value) {
            addCriterion("activity_status <", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusLessThanOrEqualTo(Byte value) {
            addCriterion("activity_status <=", value, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusIn(List<Byte> values) {
            addCriterion("activity_status in", values, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusNotIn(List<Byte> values) {
            addCriterion("activity_status not in", values, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusBetween(Byte value1, Byte value2) {
            addCriterion("activity_status between", value1, value2, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andActivityStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("activity_status not between", value1, value2, "activityStatus");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareIsNull() {
            addCriterion("is_allow_share is null");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareIsNotNull() {
            addCriterion("is_allow_share is not null");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareEqualTo(Boolean value) {
            addCriterion("is_allow_share =", value, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareNotEqualTo(Boolean value) {
            addCriterion("is_allow_share <>", value, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareGreaterThan(Boolean value) {
            addCriterion("is_allow_share >", value, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_allow_share >=", value, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareLessThan(Boolean value) {
            addCriterion("is_allow_share <", value, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareLessThanOrEqualTo(Boolean value) {
            addCriterion("is_allow_share <=", value, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareIn(List<Boolean> values) {
            addCriterion("is_allow_share in", values, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareNotIn(List<Boolean> values) {
            addCriterion("is_allow_share not in", values, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareBetween(Boolean value1, Boolean value2) {
            addCriterion("is_allow_share between", value1, value2, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andIsAllowShareNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_allow_share not between", value1, value2, "isAllowShare");
            return (Criteria) this;
        }

        public Criteria andShareTitleIsNull() {
            addCriterion("share_title is null");
            return (Criteria) this;
        }

        public Criteria andShareTitleIsNotNull() {
            addCriterion("share_title is not null");
            return (Criteria) this;
        }

        public Criteria andShareTitleEqualTo(String value) {
            addCriterion("share_title =", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotEqualTo(String value) {
            addCriterion("share_title <>", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleGreaterThan(String value) {
            addCriterion("share_title >", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleGreaterThanOrEqualTo(String value) {
            addCriterion("share_title >=", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLessThan(String value) {
            addCriterion("share_title <", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLessThanOrEqualTo(String value) {
            addCriterion("share_title <=", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleLike(String value) {
            addCriterion("share_title like", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotLike(String value) {
            addCriterion("share_title not like", value, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleIn(List<String> values) {
            addCriterion("share_title in", values, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotIn(List<String> values) {
            addCriterion("share_title not in", values, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleBetween(String value1, String value2) {
            addCriterion("share_title between", value1, value2, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andShareTitleNotBetween(String value1, String value2) {
            addCriterion("share_title not between", value1, value2, "shareTitle");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlIsNull() {
            addCriterion("share_picture_url is null");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlIsNotNull() {
            addCriterion("share_picture_url is not null");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlEqualTo(String value) {
            addCriterion("share_picture_url =", value, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlNotEqualTo(String value) {
            addCriterion("share_picture_url <>", value, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlGreaterThan(String value) {
            addCriterion("share_picture_url >", value, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlGreaterThanOrEqualTo(String value) {
            addCriterion("share_picture_url >=", value, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlLessThan(String value) {
            addCriterion("share_picture_url <", value, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlLessThanOrEqualTo(String value) {
            addCriterion("share_picture_url <=", value, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlLike(String value) {
            addCriterion("share_picture_url like", value, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlNotLike(String value) {
            addCriterion("share_picture_url not like", value, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlIn(List<String> values) {
            addCriterion("share_picture_url in", values, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlNotIn(List<String> values) {
            addCriterion("share_picture_url not in", values, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlBetween(String value1, String value2) {
            addCriterion("share_picture_url between", value1, value2, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andSharePictureUrlNotBetween(String value1, String value2) {
            addCriterion("share_picture_url not between", value1, value2, "sharePictureUrl");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNull() {
            addCriterion("operator_name is null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNotNull() {
            addCriterion("operator_name is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameEqualTo(String value) {
            addCriterion("operator_name =", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotEqualTo(String value) {
            addCriterion("operator_name <>", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThan(String value) {
            addCriterion("operator_name >", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThanOrEqualTo(String value) {
            addCriterion("operator_name >=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThan(String value) {
            addCriterion("operator_name <", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThanOrEqualTo(String value) {
            addCriterion("operator_name <=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLike(String value) {
            addCriterion("operator_name like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotLike(String value) {
            addCriterion("operator_name not like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIn(List<String> values) {
            addCriterion("operator_name in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotIn(List<String> values) {
            addCriterion("operator_name not in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameBetween(String value1, String value2) {
            addCriterion("operator_name between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotBetween(String value1, String value2) {
            addCriterion("operator_name not between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeIsNull() {
            addCriterion("operator_code is null");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeIsNotNull() {
            addCriterion("operator_code is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeEqualTo(String value) {
            addCriterion("operator_code =", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeNotEqualTo(String value) {
            addCriterion("operator_code <>", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeGreaterThan(String value) {
            addCriterion("operator_code >", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeGreaterThanOrEqualTo(String value) {
            addCriterion("operator_code >=", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeLessThan(String value) {
            addCriterion("operator_code <", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeLessThanOrEqualTo(String value) {
            addCriterion("operator_code <=", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeLike(String value) {
            addCriterion("operator_code like", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeNotLike(String value) {
            addCriterion("operator_code not like", value, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeIn(List<String> values) {
            addCriterion("operator_code in", values, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeNotIn(List<String> values) {
            addCriterion("operator_code not in", values, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeBetween(String value1, String value2) {
            addCriterion("operator_code between", value1, value2, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andOperatorCodeNotBetween(String value1, String value2) {
            addCriterion("operator_code not between", value1, value2, "operatorCode");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIsNull() {
            addCriterion("online_date is null");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIsNotNull() {
            addCriterion("online_date is not null");
            return (Criteria) this;
        }

        public Criteria andOnlineDateEqualTo(Date value) {
            addCriterion("online_date =", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotEqualTo(Date value) {
            addCriterion("online_date <>", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateGreaterThan(Date value) {
            addCriterion("online_date >", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateGreaterThanOrEqualTo(Date value) {
            addCriterion("online_date >=", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateLessThan(Date value) {
            addCriterion("online_date <", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateLessThanOrEqualTo(Date value) {
            addCriterion("online_date <=", value, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateIn(List<Date> values) {
            addCriterion("online_date in", values, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotIn(List<Date> values) {
            addCriterion("online_date not in", values, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateBetween(Date value1, Date value2) {
            addCriterion("online_date between", value1, value2, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOnlineDateNotBetween(Date value1, Date value2) {
            addCriterion("online_date not between", value1, value2, "onlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIsNull() {
            addCriterion("offline_date is null");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIsNotNull() {
            addCriterion("offline_date is not null");
            return (Criteria) this;
        }

        public Criteria andOfflineDateEqualTo(Date value) {
            addCriterion("offline_date =", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotEqualTo(Date value) {
            addCriterion("offline_date <>", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateGreaterThan(Date value) {
            addCriterion("offline_date >", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateGreaterThanOrEqualTo(Date value) {
            addCriterion("offline_date >=", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateLessThan(Date value) {
            addCriterion("offline_date <", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateLessThanOrEqualTo(Date value) {
            addCriterion("offline_date <=", value, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateIn(List<Date> values) {
            addCriterion("offline_date in", values, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotIn(List<Date> values) {
            addCriterion("offline_date not in", values, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateBetween(Date value1, Date value2) {
            addCriterion("offline_date between", value1, value2, "offlineDate");
            return (Criteria) this;
        }

        public Criteria andOfflineDateNotBetween(Date value1, Date value2) {
            addCriterion("offline_date not between", value1, value2, "offlineDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}