package com.zatgo.zup.coupons.entity;

public class CouponsActivityRel {
    private String relId;

    private String couponsId;

    private String couponsActivityId;

    public String getRelId() {
        return relId;
    }

    public void setRelId(String relId) {
        this.relId = relId == null ? null : relId.trim();
    }

    public String getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(String couponsId) {
        this.couponsId = couponsId == null ? null : couponsId.trim();
    }

    public String getCouponsActivityId() {
        return couponsActivityId;
    }

    public void setCouponsActivityId(String couponsActivityId) {
        this.couponsActivityId = couponsActivityId == null ? null : couponsActivityId.trim();
    }
}