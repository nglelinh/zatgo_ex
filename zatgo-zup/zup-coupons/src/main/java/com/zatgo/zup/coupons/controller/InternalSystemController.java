package com.zatgo.zup.coupons.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.coupons.InternalUsageCouponsOrderParams;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.coupons.model.CanGetCouponsModel;
import com.zatgo.zup.coupons.model.InternalUserCouponsSearchParams;
import com.zatgo.zup.coupons.service.UserCouponsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/coupons/user/internal",description = "系统内部用户优惠券接口")
@RestController
@RequestMapping(value = "/coupons/user/internal")
public class InternalSystemController {

	private static final Logger logger = LoggerFactory.getLogger(InternalSystemController.class);
	
	@Autowired
	private UserCouponsService userCouponsService;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@RequestMapping(value = "/getUserCouponsList",method = RequestMethod.POST)
	@ApiOperation(value = "查询用户的优惠券", notes = "查询用户的优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<PageInfo<UserCouponsModel>> getUserCouponsList(@RequestBody InternalUserCouponsSearchParams params){
		PageInfo<UserCouponsModel> pageInfoModel = userCouponsService.getUserCouponsList(params);
		return BusinessResponseFactory.createSuccess(pageInfoModel);
	}
	
	@RequestMapping(value = "/getUserCouponsByOrder",method = RequestMethod.POST)
	@ApiOperation(value = "查询指定订单可用的优惠券", notes = "查询指定订单可用的优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<UserCouponsModel>> getUserCouponsByOrder(@RequestBody InternalUsageCouponsOrderParams params){
		List<UserCouponsModel> models = userCouponsService.getUserCouponsByOrder(params);
		return BusinessResponseFactory.createSuccess(models);
	}
	
	@RequestMapping(value = "/getUsedUserCouponsByOrder/{cloudUserId}/{orderId}",method = RequestMethod.GET)
	@ApiOperation(value = "查询指定订单已使用的优惠券", notes = "查询指定订单已使用的优惠券", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<UserCouponsModel>> getUsedUserCouponsByOrder(@PathVariable("cloudUserId") String cloudUserId,@PathVariable("orderId") String orderId){
		List<UserCouponsModel> models = userCouponsService.getUsedUserCouponsByOrder(cloudUserId,orderId);
		return BusinessResponseFactory.createSuccess(models);
	}
	
	@RequestMapping(value = "/usageCoupons",method = RequestMethod.POST)
	@ApiOperation(value = "用户使用优惠券", notes = "用户使用优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<UserCouponsModel>> usageCoupons(@RequestBody InternalUsageCouponsOrderParams params){
		String redisLockKey = RedisKeyConstants.COUPONS_USER_ID_PRE + params.getUserId();
		redisLockUtils.lock(redisLockKey);
		try {
			List<UserCouponsModel> list = userCouponsService.usageCoupons(params);
			
			return BusinessResponseFactory.createSuccess(list);
		}finally {
			redisLockUtils.releaseLock(redisLockKey);
		}
	}
	
	
	@RequestMapping(value = "/checkUsageCoupons",method = RequestMethod.POST)
	@ApiOperation(value = "验证用户是否可以使用当前优惠券", notes = "验证用户是否可以使用当前优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<UserCouponsModel>> checkUsageCoupons(@RequestBody InternalUsageCouponsOrderParams params){
		List<UserCouponsModel> list = userCouponsService.checkUsageCoupons(params);
		return BusinessResponseFactory.createSuccess(list);
	}

	
	@RequestMapping(value = "/cancelUsageCoupons",method = RequestMethod.POST)
	@ApiOperation(value = "根据订单退还用户使用的优惠券", notes = "根据订单退还用户使用的优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> cancelUsageCoupons(@RequestBody InternalUsageCouponsOrderParams params){
		String redisLockKey = RedisKeyConstants.COUPONS_USER_ID_PRE + params.getUserId();
		redisLockUtils.lock(redisLockKey);
		try {
			userCouponsService.cancelUsageCoupons(params);
		}finally {
			redisLockUtils.releaseLock(redisLockKey);
		}
		
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@RequestMapping(value = "/userManualGetCoupons/{cloudUserId}/{userId}/{couponsId}",method = RequestMethod.POST)
	@ApiOperation(value = "用户手动领取优惠券", notes = "管理员-用户手动领取优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<UserCouponsModel> userManualGetCoupons(@PathVariable("cloudUserId") String cloudUserId,@PathVariable("userId") String userId,@PathVariable("couponsId") String couponsId){
		
		String redisLockKey = RedisKeyConstants.COUPONS_USER_ID_PRE + userId;
		redisLockUtils.lock(redisLockKey);
		try {
			UserCouponsModel  model = userCouponsService.userManualGetCoupons(cloudUserId,userId,couponsId);
			return BusinessResponseFactory.createSuccess(model);
		}finally {
			redisLockUtils.releaseLock(redisLockKey);
		}
		
	}
	
	
	@RequestMapping(value = "/triggerGetCoupons/{cloudUserId}/{userId}/{triggerBusinessType}/{triggerBusinessCode}",method = RequestMethod.POST)
	@ApiOperation(value = "系统触发发放优惠券", notes = "系统触发发放优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<UserCouponsModel>> triggerGetCoupons(
			@PathVariable("cloudUserId") String cloudUserId, 
			@PathVariable("userId") String userId, 
			@PathVariable("triggerBusinessType") String triggerBusinessType,
			@PathVariable("triggerBusinessCode") String triggerBusinessCode){
		
		String redisLockKey = RedisKeyConstants.COUPONS_USER_ID_PRE + userId;
		redisLockUtils.lock(redisLockKey);
		try {
			List<UserCouponsModel>  models = userCouponsService.triggerGetCoupons(
					cloudUserId,userId,triggerBusinessType,triggerBusinessCode);
			return BusinessResponseFactory.createSuccess(models);
		}finally {
			redisLockUtils.releaseLock(redisLockKey);
		}
		
		
	}
	
	@RequestMapping(value = "/canManualGetCouponsList/{cloudUserId}/{userId}",method = RequestMethod.POST)
	@ApiOperation(value = "获取可手动领取的优惠券", notes = "获取可手动领取的优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<CanGetCouponsModel>> getCanManualGetCouponsListByUser(
			@PathVariable("cloudUserId") String cloudUserId, 
			@PathVariable("userId") String userId){
		List<CanGetCouponsModel> couponsList = userCouponsService.getCanManualGetCouponsListByUser(cloudUserId, userId);
		return BusinessResponseFactory.createSuccess(couponsList);
	}
	
	
	@RequestMapping(value = "/getUserCoupons/{cloudUserId}/{userCouponsId}",method = RequestMethod.GET)
	@ApiOperation(value = "根据主键查询用户优惠券", notes = "根据主键查询用户优惠券", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<UserCouponsModel> getCouponsById(@PathVariable("cloudUserId") String cloudUserId, 
			@PathVariable("userCouponsId") String userCouponsId){
		UserCouponsModel model = userCouponsService.getUserCouponsById(cloudUserId, userCouponsId);
		return BusinessResponseFactory.createSuccess(model);
	}
	
	
}
