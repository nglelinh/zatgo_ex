package com.zatgo.zup.coupons.entity;

import java.util.ArrayList;
import java.util.List;

public class CouponRangeRelExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CouponRangeRelExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRangeIdIsNull() {
            addCriterion("range_id is null");
            return (Criteria) this;
        }

        public Criteria andRangeIdIsNotNull() {
            addCriterion("range_id is not null");
            return (Criteria) this;
        }

        public Criteria andRangeIdEqualTo(String value) {
            addCriterion("range_id =", value, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdNotEqualTo(String value) {
            addCriterion("range_id <>", value, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdGreaterThan(String value) {
            addCriterion("range_id >", value, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdGreaterThanOrEqualTo(String value) {
            addCriterion("range_id >=", value, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdLessThan(String value) {
            addCriterion("range_id <", value, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdLessThanOrEqualTo(String value) {
            addCriterion("range_id <=", value, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdLike(String value) {
            addCriterion("range_id like", value, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdNotLike(String value) {
            addCriterion("range_id not like", value, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdIn(List<String> values) {
            addCriterion("range_id in", values, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdNotIn(List<String> values) {
            addCriterion("range_id not in", values, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdBetween(String value1, String value2) {
            addCriterion("range_id between", value1, value2, "rangeId");
            return (Criteria) this;
        }

        public Criteria andRangeIdNotBetween(String value1, String value2) {
            addCriterion("range_id not between", value1, value2, "rangeId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdIsNull() {
            addCriterion("coupons_id is null");
            return (Criteria) this;
        }

        public Criteria andCouponsIdIsNotNull() {
            addCriterion("coupons_id is not null");
            return (Criteria) this;
        }

        public Criteria andCouponsIdEqualTo(String value) {
            addCriterion("coupons_id =", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotEqualTo(String value) {
            addCriterion("coupons_id <>", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdGreaterThan(String value) {
            addCriterion("coupons_id >", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdGreaterThanOrEqualTo(String value) {
            addCriterion("coupons_id >=", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLessThan(String value) {
            addCriterion("coupons_id <", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLessThanOrEqualTo(String value) {
            addCriterion("coupons_id <=", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdLike(String value) {
            addCriterion("coupons_id like", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotLike(String value) {
            addCriterion("coupons_id not like", value, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdIn(List<String> values) {
            addCriterion("coupons_id in", values, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotIn(List<String> values) {
            addCriterion("coupons_id not in", values, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdBetween(String value1, String value2) {
            addCriterion("coupons_id between", value1, value2, "couponsId");
            return (Criteria) this;
        }

        public Criteria andCouponsIdNotBetween(String value1, String value2) {
            addCriterion("coupons_id not between", value1, value2, "couponsId");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryIsNull() {
            addCriterion("range_category is null");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryIsNotNull() {
            addCriterion("range_category is not null");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryEqualTo(String value) {
            addCriterion("range_category =", value, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryNotEqualTo(String value) {
            addCriterion("range_category <>", value, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryGreaterThan(String value) {
            addCriterion("range_category >", value, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryGreaterThanOrEqualTo(String value) {
            addCriterion("range_category >=", value, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryLessThan(String value) {
            addCriterion("range_category <", value, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryLessThanOrEqualTo(String value) {
            addCriterion("range_category <=", value, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryLike(String value) {
            addCriterion("range_category like", value, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryNotLike(String value) {
            addCriterion("range_category not like", value, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryIn(List<String> values) {
            addCriterion("range_category in", values, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryNotIn(List<String> values) {
            addCriterion("range_category not in", values, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryBetween(String value1, String value2) {
            addCriterion("range_category between", value1, value2, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCategoryNotBetween(String value1, String value2) {
            addCriterion("range_category not between", value1, value2, "rangeCategory");
            return (Criteria) this;
        }

        public Criteria andRangeCodeIsNull() {
            addCriterion("range_code is null");
            return (Criteria) this;
        }

        public Criteria andRangeCodeIsNotNull() {
            addCriterion("range_code is not null");
            return (Criteria) this;
        }

        public Criteria andRangeCodeEqualTo(String value) {
            addCriterion("range_code =", value, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeNotEqualTo(String value) {
            addCriterion("range_code <>", value, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeGreaterThan(String value) {
            addCriterion("range_code >", value, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("range_code >=", value, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeLessThan(String value) {
            addCriterion("range_code <", value, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeLessThanOrEqualTo(String value) {
            addCriterion("range_code <=", value, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeLike(String value) {
            addCriterion("range_code like", value, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeNotLike(String value) {
            addCriterion("range_code not like", value, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeIn(List<String> values) {
            addCriterion("range_code in", values, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeNotIn(List<String> values) {
            addCriterion("range_code not in", values, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeBetween(String value1, String value2) {
            addCriterion("range_code between", value1, value2, "rangeCode");
            return (Criteria) this;
        }

        public Criteria andRangeCodeNotBetween(String value1, String value2) {
            addCriterion("range_code not between", value1, value2, "rangeCode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}