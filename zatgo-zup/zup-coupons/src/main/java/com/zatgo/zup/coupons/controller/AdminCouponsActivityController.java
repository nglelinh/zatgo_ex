package com.zatgo.zup.coupons.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.coupons.entity.Coupons;
import com.zatgo.zup.coupons.entity.CouponsActivity;
import com.zatgo.zup.coupons.model.DiscountEventSearchParams;
import com.zatgo.zup.coupons.service.CouponsActivityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/coupons/activity/admin",description = "优惠券活动接口")
@RestController
@RequestMapping(value = "/coupons/activity/admin")
public class AdminCouponsActivityController extends BaseController{
	
	@Autowired
	private CouponsActivityService couponsActivityService;

	private static final Logger logger = LoggerFactory.getLogger(AdminCouponsActivityController.class);

	@RequestMapping(value = "/addActivity",method = RequestMethod.POST)
	@ApiOperation(value = "新增优惠活动", notes = "新增优惠活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> addActivity(@RequestBody CouponsActivity params){
		AuthUserInfo adminUser = getAdminInfo();
		couponsActivityService.addActivity(adminUser, params);
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@RequestMapping(value = "/updateActivity",method = RequestMethod.POST)
	@ApiOperation(value = "修改优惠活动", notes = "修改优惠活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> updateActivity(@RequestBody CouponsActivity params){
		AuthUserInfo adminUser = getAdminInfo();
		couponsActivityService.updateActivity(adminUser, params);
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@RequestMapping(value = "/getActivity/{activityId}",method = RequestMethod.GET)
	@ApiOperation(value = "查询优惠活动", notes = "查询优惠活动", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<Boolean> getActivityByIdAndAdmin(@PathVariable("activityId") String activityId){
		AuthUserInfo adminUser = getAdminInfo();
		couponsActivityService.getActivityByIdAndAdmin(adminUser, activityId);
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@RequestMapping(value = "/getAllActivity",method = RequestMethod.POST)
	@ApiOperation(value = "查询优惠活动列表", notes = "查询优惠活动列表", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<PageInfo<Coupons>> getAllActivityListByParams(@RequestBody DiscountEventSearchParams params){
		AuthUserInfo adminUser = getAdminInfo();
		PageInfo<Coupons>  couponsList = couponsActivityService.getAllActivityListByParams(adminUser, params);
		return BusinessResponseFactory.createSuccess(couponsList);
	}
	
	@RequestMapping(value = "/getCouponsByActivity/{activityId}",method = RequestMethod.GET)
	@ApiOperation(value = "查询优惠活动列表", notes = "查询优惠活动列表", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<Coupons>> getCouponsByActivity(@PathVariable("activityId") String activityId){
		AuthUserInfo adminUser = getAdminInfo();
		List<Coupons>  couponsList = couponsActivityService.getCouponsByActivity(adminUser, activityId);
		return BusinessResponseFactory.createSuccess(couponsList);
	}

}
