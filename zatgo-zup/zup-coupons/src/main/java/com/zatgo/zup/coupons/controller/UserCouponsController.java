package com.zatgo.zup.coupons.controller;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.coupons.InternalUsageCouponsOrderParams;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;
import com.zatgo.zup.common.model.payment.PrepayInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.coupons.model.CanGetCouponsModel;
import com.zatgo.zup.coupons.model.InternalUserCouponsSearchParams;
import com.zatgo.zup.coupons.model.UsageCouponsOrderParams;
import com.zatgo.zup.coupons.model.UserCouponsSearchParams;
import com.zatgo.zup.coupons.remoteservice.OrderRemoteService;
import com.zatgo.zup.coupons.service.UserCouponsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/coupons/user",description = "用户优惠券接口")
@RestController
@RequestMapping(value = "/coupons/user")
public class UserCouponsController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(UserCouponsController.class);
	
	@Autowired
	private UserCouponsService userCouponsService;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@Autowired
	private OrderRemoteService orderRemoteService;

	@RequestMapping(value = "/getUserCouponsList",method = RequestMethod.POST)
	@ApiOperation(value = "查询用户的优惠券", notes = "查询用户的优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<PageInfo<UserCouponsModel>> getUserCouponsList(@RequestBody UserCouponsSearchParams params){
		AuthUserInfo userInfo = getUserInfo();
		InternalUserCouponsSearchParams internalParams = new InternalUserCouponsSearchParams();
		try {
			BeanUtils.copyProperties(internalParams, params);
			internalParams.setCloudUserId(userInfo.getCloudUserId());
			internalParams.setUserId(userInfo.getUserId());
		} catch (Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
		PageInfo<UserCouponsModel> pageInfoModel = userCouponsService.getUserCouponsList(internalParams);
		return BusinessResponseFactory.createSuccess(pageInfoModel);
	}
	
	@RequestMapping(value = "/getUserCouponsByOrder",method = RequestMethod.POST)
	@ApiOperation(value = "查询指定订单可用的优惠券", notes = "查询指定订单可用的优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<UserCouponsModel>> getUserCouponsByOrder(@RequestBody UsageCouponsOrderParams params){
		AuthUserInfo userInfo = getUserInfo();
		InternalUsageCouponsOrderParams internalParams = new InternalUsageCouponsOrderParams();
		ResponseData<PrepayInfo> data = orderRemoteService.selectOrderByPrePayId(params.getPrePayId());
		if(data.getData() == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		internalParams.setCloudUserId(userInfo.getCloudUserId());
		internalParams.setUserId(userInfo.getUserId());
		internalParams.setBusinessType(null);
		internalParams.setOrderId(data.getData().getOrder().getOrderId());
		internalParams.setOrderMoney(data.getData().getOrder().getOriginalPrice());
		internalParams.setSubBusinessCode(data.getData().getOrder().getOrderName());
		internalParams.setUserLevel(null);
		internalParams.setUserType(Byte.valueOf("2"));

		List<UserCouponsModel> models = userCouponsService.getUserCouponsByOrder(internalParams);
		return BusinessResponseFactory.createSuccess(models);
	}

	@RequestMapping(value = "/userManualGetCoupons/{couponsId}",method = RequestMethod.POST)
	@ApiOperation(value = "用户手动领取优惠券", notes = "管理员-用户手动领取优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<UserCouponsModel> userManualGetCoupons(@PathVariable("couponsId") String couponsId){
		AuthUserInfo userInfo = getUserInfo();
		
		String redisLockKey = RedisKeyConstants.COUPONS_USER_ID_PRE + userInfo.getUserId();
		redisLockUtils.lock(redisLockKey);
		try {
			UserCouponsModel  model = userCouponsService.userManualGetCoupons(
					userInfo.getCloudUserId(),userInfo.getUserId(),couponsId);
			
			return BusinessResponseFactory.createSuccess(model);
		}finally {
			redisLockUtils.releaseLock(redisLockKey);
		}
	}
	
	@RequestMapping(value = "/canManualGetCouponsList",method = RequestMethod.POST)
	@ApiOperation(value = "获取当前用户可手动领取的优惠券", notes = "获取当前用户可手动领取的优惠券", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseData<List<CanGetCouponsModel>> getCanManualGetCouponsListByUser(){
		AuthUserInfo userInfo = getUserInfo();
		List<CanGetCouponsModel> couponsList = userCouponsService.getCanManualGetCouponsListByUser(
				userInfo.getCloudUserId(), userInfo.getUserId());
		return BusinessResponseFactory.createSuccess(couponsList);
	}
}
