package com.zatgo.zup.coupons.entity;

import java.math.BigDecimal;
import java.util.Date;

public class CouponsOrderAbbr {
    private String couponsOrderId;

    private String busiOrderId;

    private String userId;

    private Byte userType;

    private Byte userLevel;

    private BigDecimal orderMoney;

    private Date orderDate;

    private Byte status;

    public String getCouponsOrderId() {
        return couponsOrderId;
    }

    public void setCouponsOrderId(String couponsOrderId) {
        this.couponsOrderId = couponsOrderId == null ? null : couponsOrderId.trim();
    }

    public String getBusiOrderId() {
        return busiOrderId;
    }

    public void setBusiOrderId(String busiOrderId) {
        this.busiOrderId = busiOrderId == null ? null : busiOrderId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Byte getUserType() {
        return userType;
    }

    public void setUserType(Byte userType) {
        this.userType = userType;
    }

    public Byte getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Byte userLevel) {
        this.userLevel = userLevel;
    }

    public BigDecimal getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(BigDecimal orderMoney) {
        this.orderMoney = orderMoney;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}