package com.zatgo.zup.coupons.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.enumtype.CouponsEnum;
import com.zatgo.zup.common.enumtype.CouponsEnum.CouponsActivityGetType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.coupons.InternalUsageCouponsOrderParams;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.coupons.entity.CouponRangeRel;
import com.zatgo.zup.coupons.entity.CouponRangeRelExample;
import com.zatgo.zup.coupons.entity.Coupons;
import com.zatgo.zup.coupons.entity.CouponsActivity;
import com.zatgo.zup.coupons.entity.CouponsActivityExample;
import com.zatgo.zup.coupons.entity.CouponsOrderAbbr;
import com.zatgo.zup.coupons.entity.CouponsOrderAbbrExample;
import com.zatgo.zup.coupons.entity.CouponsUsageRecord;
import com.zatgo.zup.coupons.entity.CouponsUsageRecordExample;
import com.zatgo.zup.coupons.entity.UserAbbr;
import com.zatgo.zup.coupons.entity.UserAbbrExample;
import com.zatgo.zup.coupons.entity.UserCoupons;
import com.zatgo.zup.coupons.entity.UserCouponsExample;
import com.zatgo.zup.coupons.entity.UserCouponsGetRecord;
import com.zatgo.zup.coupons.mapper.CouponRangeRelMapper;
import com.zatgo.zup.coupons.mapper.CouponsActivityMapper;
import com.zatgo.zup.coupons.mapper.CouponsMapper;
import com.zatgo.zup.coupons.mapper.CouponsOrderAbbrMapper;
import com.zatgo.zup.coupons.mapper.CouponsUsageRecordMapper;
import com.zatgo.zup.coupons.mapper.UserAbbrMapper;
import com.zatgo.zup.coupons.mapper.UserCouponsGetRecordMapper;
import com.zatgo.zup.coupons.mapper.UserCouponsMapper;
import com.zatgo.zup.coupons.model.CanGetCouponsModel;
import com.zatgo.zup.coupons.model.InternalUserCouponsSearchParams;
import com.zatgo.zup.coupons.service.UserCouponsService;

@Service
public class UserCouponsServiceImpl implements UserCouponsService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserCouponsServiceImpl.class);

	@Autowired
	private CouponsActivityMapper couponsActivityMapper;
	
	@Autowired
	private CouponsMapper couponsMapper;
	
	@Autowired
	private UserCouponsMapper userCouponsMapper;
	
	@Autowired
	private CouponRangeRelMapper couponRangeRelMapper;
	
	@Autowired
	private CouponsUsageRecordMapper couponsUsageRecordMapper;
	
	@Autowired
	private UserCouponsGetRecordMapper userCouponsGetRecordMapper;
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@Autowired
	private CouponsOrderAbbrMapper couponsOrderAbbrMapper;
	
	@Autowired
	private UserAbbrMapper userAbbrMapper;

	
	@Override
	public PageInfo<UserCouponsModel> getUserCouponsList(InternalUserCouponsSearchParams params) {
		PageHelper.startPage(Integer.valueOf(params.getPageNo()), Integer.valueOf(params.getPageSize()));
		
		Page<UserCouponsModel> pageData = userCouponsMapper.getUserCouponsListByPage(
				params.getCloudUserId(), params.getUserId(), params.getStatus());
		
		PageInfo<UserCouponsModel> pageInfoOrder =
				new PageInfo<UserCouponsModel>(pageData.getPageNum(),pageData.getPageSize(),pageData.getTotal(),pageData.getResult());

		PageHelper.clearPage();
		return pageInfoOrder;
	}

	@Override
	public List<UserCouponsModel> getUserCouponsByOrder(InternalUsageCouponsOrderParams params) {
		PageHelper.startPage(1, 9999);
		
		//查询当前有效的优惠券
		Page<UserCouponsModel> pageData = userCouponsMapper.getUserCouponsListByPage(
				params.getCloudUserId(), params.getUserId(), Byte.valueOf("0"));
		
		//过滤掉当前订单不满足的优惠券
		List<UserCouponsModel> validList = new ArrayList<>();
		for(UserCouponsModel model:pageData.getResult()) {
			List<String> userCouponsIds = new ArrayList<>();
			userCouponsIds.add(model.getUserCouponsId());
			params.setUserCouponsIds(userCouponsIds);
			
			try {
				checkUsageCoupons(params);
				validList.add(model);
			}catch(BusinessException e) {
				logger.info("",e);
			}catch(Exception e) {
				logger.error("",e);
			}
			
			
		}
		
		PageHelper.clearPage();
		return validList;
	}
	
	@Override
	public List<UserCouponsModel> checkUsageCoupons(InternalUsageCouponsOrderParams params) {
		Integer notCanOverlayUsageNum = 0;
		
		List<UserCouponsModel> models = new ArrayList<>();
		for(String userCouponsId:params.getUserCouponsIds()) {
			
			//验证当前优惠券是否是本人的
			UserCoupons userCoupons = userCouponsMapper.selectByPrimaryKey(userCouponsId);
			if(userCoupons == null || !userCoupons.getUserId().equals(params.getUserId())) {
				throw new BusinessException(BusinessExceptionCode.RECORD_NOT_EXIST);
			}
			
			//查询优惠券信息
			Coupons coupons = couponsMapper.selectByPrimaryKey(userCoupons.getCouponsId());
			Byte userType = params.getUserType();
			Byte userLevel = params.getUserLevel();
			BigDecimal money = params.getOrderMoney();
			
			//判断时间是否有效
			if(userCoupons.getValidStartDate().compareTo(new Date()) > 0
					|| userCoupons.getValidEndDate().compareTo(new Date()) < 0) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"不在有效的时间范围内");
			}
			
			//当前优惠券是否已使用
			if(!userCoupons.getStatus().equals(CouponsEnum.UserCouponsStatus.unused.getCode())) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"已使用");
			}
			
			//验证当前优惠券是否可用-判断用户类型
			if(!coupons.getUserType().equals(CouponsEnum.CouponsUserType.allUser.getCode()) 
					&& !coupons.getUserType().equals(userType)) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"用户类型不满足");
			}
			
			//验证当前优惠券是否可用-判断会员等级
			//TODO
			
			//验证当前优惠券是否可用-使用门槛（满多少金额使用）
			if(coupons.getHowManyFull() != null && money.compareTo(coupons.getHowManyFull()) < 0) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"满多少金额使用不满足");
			}
			
			//统计不可重叠数量
			if(!coupons.getIsOverlayUsage()) {
				notCanOverlayUsageNum++;
			}
			
			//范围类型验证
			CouponRangeRelExample example = new CouponRangeRelExample();
			example.createCriteria().andCouponsIdEqualTo(coupons.getCouponsId());
			List<CouponRangeRel>  rangeRels = couponRangeRelMapper.selectByExample(example);
			if(rangeRels.size() > 0) {
				Boolean isValid = false;
				for(CouponRangeRel rel:rangeRels) {
					if((rel.getRangeCategory() == null || rel.getRangeCategory().equals(params.getBusinessType())) 
							&& (rel.getRangeCode() == null || rel.getRangeCode().equals(params.getSubBusinessCode()))) {
						isValid = true;
						break;
					}
				}
				
				if(!isValid) {
					throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"范围类型验证不满足");
				}
			}
			
			UserCouponsModel model = getUserCouponsById(params.getCloudUserId(), userCouponsId);
			models.add(model);
		}
		
		//是否可叠加使用验证
		if(notCanOverlayUsageNum.intValue() >= 2) {
			throw new BusinessException(BusinessExceptionCode.COUPONS_NOT_OVERLAY_ERROR);
		}
		
		return models;

	}

	@Override
	@Transactional
	public List<UserCouponsModel> usageCoupons(InternalUsageCouponsOrderParams params) {
		CouponsOrderAbbrExample example = new CouponsOrderAbbrExample();
		example.createCriteria().andBusiOrderIdEqualTo(params.getOrderId()).andUserIdEqualTo(params.getUserId());
		List<CouponsOrderAbbr>  orderList = couponsOrderAbbrMapper.selectByExample(example);
		if(orderList.size() > 0) {
			throw new BusinessException(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING);
		}
		
		//验证优惠券的有效性
		checkUsageCoupons(params);
		
		UserAbbrExample exampleUser = new UserAbbrExample();
		exampleUser.createCriteria().andBusiUserIdEqualTo(params.getUserId())
		.andCloudUserIdEqualTo(params.getCloudUserId());
		List<UserAbbr> userAbbrs = userAbbrMapper.selectByExample(exampleUser);
		
		UserAbbr userAbbr = null;
		if(userAbbrs.size() == 0) {
			userAbbr = new UserAbbr();
			userAbbr.setBusiUserId(params.getUserId());
			userAbbr.setCloudUserId(params.getCloudUserId());
			userAbbr.setUserId(UUIDUtils.getUuid());
			userAbbrMapper.insertSelective(userAbbr);
		}else {
			userAbbr = userAbbrs.get(0);
		}
		
		String orderId = UUIDUtils.getUuid();
		CouponsOrderAbbr order = new CouponsOrderAbbr();
		order.setBusiOrderId(params.getOrderId());
		order.setCouponsOrderId(orderId);
		order.setOrderDate(new Date());
		order.setOrderMoney(params.getOrderMoney());
		order.setStatus(Byte.valueOf("0"));
		order.setUserId(userAbbr.getUserId());
		order.setUserLevel(params.getUserLevel());
		order.setUserType(params.getUserType());
		couponsOrderAbbrMapper.insertSelective(order);
		
		List<UserCouponsModel> models = new ArrayList<>();
		for(String userCouponsId:params.getUserCouponsIds()) {
			CouponsUsageRecord record = new CouponsUsageRecord();
			record.setCouponsOrderId(orderId);
			record.setCouponsUsageId(UUIDUtils.getUuid());
			record.setUsageDate(new Date());
			record.setUserCouponsId(userCouponsId);
			couponsUsageRecordMapper.insert(record);

			UserCoupons updateUserCoupons = new UserCoupons();
			updateUserCoupons.setUserCouponsId(userCouponsId);
			updateUserCoupons.setStatus(CouponsEnum.UserCouponsStatus.used.getCode());
			userCouponsMapper.updateByPrimaryKeySelective(updateUserCoupons);
			
			UserCouponsModel model = getUserCouponsById(params.getCloudUserId(), userCouponsId);
			models.add(model);
		}	
		return models;
	}
	
	/**
	 * 缓存获取优惠券
	 * @param couponsId
	 * @return
	 */
	private Coupons getCacheCoupons(String couponsId) {
		String couponsCacheKey = RedisKeyConstants.COUPONS_CACHE_KEY;
		Coupons coupons = (Coupons)redisTemplate.opsForHash().get(couponsCacheKey, couponsId);
		if(coupons == null) {
			String lockKey = couponsCacheKey + couponsId;
			redisLockUtils.lock(lockKey);
			try {
				coupons = (Coupons)redisTemplate.opsForHash().get(couponsCacheKey, couponsId);
				if(coupons == null) {
					coupons = couponsMapper.selectByPrimaryKey(couponsId);
					redisTemplate.opsForHash().put(couponsCacheKey, couponsId, coupons);
				}
			}finally {
				redisLockUtils.releaseLock(lockKey);
			}
		}
		
		return coupons;
	}
	
	/**
	 * 扣减优惠券的总发行量
	 * @param couponsId
	 */
	private void deductionCouponsDistributionTotalMoney(String couponsId) {
		String lockKey = RedisKeyConstants.COUPONS_CACHE_UPDATE_PRO + couponsId;
		Coupons coupons = null;
		redisLockUtils.lock(lockKey);
		try {
			coupons = getCacheCoupons(couponsId);
			
			BigDecimal assignedMoney = 
					coupons.getAssignedMoneyTotal() == null?BigDecimal.ZERO:coupons.getAssignedMoneyTotal();
			Long assignedNum = 
					coupons.getAssignedNumTotal() == null ? 0:coupons.getAssignedNumTotal();
			
			BigDecimal couponsPrice = coupons.getDiscountType()
					.equals(CouponsEnum.CouponsDiscountType.fixedMoney.getCode())?
							coupons.getCouponsPrice():BigDecimal.ZERO;
			
			//发行总金额限制
			if(coupons.getDistributionMoneyTotal() != null) {
				BigDecimal balanceMoney = coupons.getDistributionMoneyTotal()
						.subtract(assignedMoney);
				if(balanceMoney.subtract(couponsPrice).compareTo(BigDecimal.ZERO) < 0) {
					throw new BusinessException(BusinessExceptionCode.COUPONS_CURRENT_ACTIVITY_END_ERROR);
				}
			}
			
			//发行总张数限制
			if(coupons.getDistributionNumTotal() != null) {
				Long balanceNum = coupons.getDistributionNumTotal()
						.longValue() - assignedNum;
				if(balanceNum < 0) {
					throw new BusinessException(BusinessExceptionCode.COUPONS_CURRENT_ACTIVITY_END_ERROR);
				}
			}
			
			//更新缓存
			coupons.setAssignedMoneyTotal(assignedMoney.add(couponsPrice));
			coupons.setAssignedNumTotal(assignedNum + 1);
			String couponsCacheKey = RedisKeyConstants.COUPONS_CACHE_KEY;
			redisTemplate.opsForHash().put(couponsCacheKey, couponsId, coupons);
			
		}finally {
			redisLockUtils.releaseLock(lockKey);
		}
		
		//更新数据库
		if(coupons != null) {
			couponsMapper.updateByPrimaryKeyWithBLOBs(coupons);
		}
		
	}

	@Override
	@Transactional
	public UserCouponsModel userManualGetCoupons(String cloudUserId, String userId, String couponsId) {
		

		List<CouponsActivity> activitys = couponsActivityMapper.getValidCouponsActivityByCouponsId(couponsId);
		
		//验证用户是否有权限领取当前优惠券
		CouponsActivity couponsActivity = null;
		for(CouponsActivity activity:activitys) {
			if(activity.getGetType().equals(CouponsActivityGetType.userGet.getCode())) {
				couponsActivity = activity;
				break;
			}
		}
		if(couponsActivity == null) {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"活动不存在");
		}
		
		
		return doGetCoupons(cloudUserId, userId, couponsId,couponsActivity.getCouponsActivityId());
	}
	
	/**
	 * 领取优惠券
	 * @param cloudUserId
	 * @param userId
	 * @param couponsId
	 */
	private UserCouponsModel doGetCoupons(String cloudUserId,String userId,String couponsId,String activityId) {
		Coupons coupons = getCacheCoupons(couponsId);
		
		//限制领取数量
		if(coupons.getEachGetLimit() != null) {
			UserCouponsExample example = new UserCouponsExample();
			example.createCriteria().andCouponsIdEqualTo(couponsId).andUserIdEqualTo(userId);
			int getCouponsNum = userCouponsMapper.countByExample(example);
			Integer eachGetLimit = coupons.getEachGetLimit();
			if (eachGetLimit == null)
				eachGetLimit = -1;
			if(eachGetLimit > 1 && getCouponsNum >= eachGetLimit.intValue()) {
				throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"当前优惠券不能多次领取");
			}
		}
		
		//计算时间
		Date validEndDate = null;
		Date validStartDate = null;
		if(coupons.getValidityType().equals(CouponsEnum.CouponsValidityType.fixedDate.getCode())) {
			validEndDate = coupons.getAbsoluteExpireDate();
			validStartDate = coupons.getAbsoluteValidDate();
		}else if(coupons.getValidityType().equals(CouponsEnum.CouponsValidityType.currentStart.getCode())) {
			validEndDate = DateUtils.addDays(new Date(), coupons.getRelativeValidDay());
			validStartDate = new Date();
		}else if(coupons.getValidityType().equals(CouponsEnum.CouponsValidityType.tomorrowStart.getCode())) {
			validEndDate = DateUtils.addDays(new Date(), coupons.getRelativeValidDay() + 1);
			validStartDate = DateUtils.addDays(new Date(), 1);
		}else {
			throw new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"无效的优惠券有效期类型");
		}
		
		
		//修改总的发行量
		deductionCouponsDistributionTotalMoney(couponsId);
		
		//用户优惠券
		String userCouponsId = UUIDUtils.getUuid();
		UserCoupons userCoupons = new UserCoupons();
		userCoupons.setCouponsId(couponsId);
		userCoupons.setGetDate(new Date());
		userCoupons.setStatus(CouponsEnum.UserCouponsStatus.unused.getCode());
		userCoupons.setUserCouponsId(userCouponsId);
		userCoupons.setUserId(userId);
		userCoupons.setValidEndDate(validEndDate);
		userCoupons.setValidStartDate(validStartDate);
		userCouponsMapper.insert(userCoupons);
		
		//领取记录
		UserCouponsGetRecord record = new UserCouponsGetRecord();
		record.setCouponsId(couponsId);
		record.setGetDate(new Date());
		record.setCouponsGetId(UUIDUtils.getUuid());
		record.setCouponsActivityId(activityId);
		record.setUserCouponsId(userCouponsId);
		userCouponsGetRecordMapper.insert(record);
		
		//组装返回信息
		UserCouponsModel model = new UserCouponsModel();
		try {
			BeanUtils.copyProperties(model, coupons);
			BeanUtils.copyProperties(model, userCoupons);
		}catch(Exception e) {
			throw new BusinessException();
		}
		return model;
	}

	@Override
	@Transactional
	public List<UserCouponsModel> triggerGetCoupons(String cloudUserId, String userId, String triggerBusinessType,
			String triggerBusinessCode) {
		List<CouponsActivity> activitys = couponsActivityMapper.getValidCouponsActivityByTriggerBusiness(cloudUserId,triggerBusinessType, triggerBusinessCode);
		
		List<UserCouponsModel> userCouponsModels = new ArrayList<>();
		for(CouponsActivity activity:activitys) {
			List<Coupons> couponsList = couponsMapper.selectCouponsByActivityId(activity.getCouponsActivityId());
			for(Coupons coupons:couponsList) {
				try {
					UserCouponsModel model = doGetCoupons(cloudUserId, userId, coupons.getCouponsId(), activity.getCouponsActivityId());
					userCouponsModels.add(model);
				}catch(BusinessException e) {
					logger.error("自动业务触发赠送优惠券：",e);
				}catch(Exception e) {
					logger.error("自动业务触发赠送优惠券：",e);
					throw new BusinessException();
				}
				
			}
		}
		
		return userCouponsModels;
	}

	@Override
	public List<CanGetCouponsModel> getCanManualGetCouponsListByUser(String cloudUserId, String userId) {
		
		//查询有效的手动领取活动
		CouponsActivityExample example = new CouponsActivityExample();
		example.createCriteria().andCloudUserIdEqualTo(cloudUserId)
		.andGetTypeEqualTo(CouponsActivityGetType.userGet.getCode())
		.andActivityStatusEqualTo(CouponsEnum.CouponsActivityStatus.online.getCode())
		.andActivityEndDateGreaterThanOrEqualTo(new Date());
		List<CouponsActivity> activitys = couponsActivityMapper.selectByExample(example);
		
		//计算用户已领取的数量
		List<CanGetCouponsModel> returnList = new ArrayList<>();
		for(CouponsActivity couponsActivity:activitys) {
			List<Coupons> list = couponsMapper.selectCouponsByActivityId(couponsActivity.getCouponsActivityId());
			for(Coupons coupons:list) {
				CanGetCouponsModel model = new CanGetCouponsModel();
				try {
					BeanUtils.copyProperties(model, coupons);
				} catch (Exception e) {
					logger.error("",e);
					continue;
				}
				
				//已领取数量
				UserCouponsExample userCouponsExample = new UserCouponsExample();
				userCouponsExample.createCriteria().andCouponsIdEqualTo(coupons.getCouponsId()).andUserIdEqualTo(userId);
				int getCouponsNum = userCouponsMapper.countByExample(userCouponsExample);
				model.setGotNum(getCouponsNum);
				
				//还可以领取数量
				if(coupons.getEachGetLimit() != null) {
					model.setCanGetNum(coupons.getEachGetLimit().intValue());
				}
				returnList.add(model);
			}
		}
		return returnList;
	}

	@Override
	public Boolean cancelUsageCoupons(InternalUsageCouponsOrderParams params) {
		CouponsOrderAbbrExample example = new CouponsOrderAbbrExample();
		example.createCriteria().andCouponsOrderIdEqualTo(params.getOrderId()).andUserIdEqualTo(params.getUserId());
		List<CouponsOrderAbbr>  orderList = couponsOrderAbbrMapper.selectByExample(example);
		
		if(orderList.size() == 0) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		
		CouponsOrderAbbr order = orderList.get(0);
		if(order.getStatus().equals(Byte.valueOf("1"))) {
			throw new BusinessException(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING);
		}
		
		//订单取消
		order.setStatus(Byte.valueOf("1"));
		couponsOrderAbbrMapper.updateByPrimaryKeySelective(order);
		
		//退还优惠券
		CouponsUsageRecordExample example1 = new CouponsUsageRecordExample();
		example1.createCriteria().andCouponsOrderIdEqualTo(order.getCouponsOrderId());
		List<CouponsUsageRecord> usageRecords = couponsUsageRecordMapper.selectByExample(example1);
		for(CouponsUsageRecord record:usageRecords) {
			UserCoupons userCoupons = new UserCoupons();
			userCoupons.setUserCouponsId(record.getUserCouponsId());
			userCoupons.setStatus(CouponsEnum.UserCouponsStatus.unused.getCode());
			userCouponsMapper.updateByPrimaryKeySelective(userCoupons);
		}
		
		return true;
	}

	@Override
	public UserCouponsModel getUserCouponsById(String cloudUserId, String userCouponsId) {
		UserCoupons userCoupons = userCouponsMapper.selectByPrimaryKey(userCouponsId);
		Coupons coupons = couponsMapper.selectByPrimaryKey(userCoupons.getCouponsId());
		
		UserCouponsModel model = new UserCouponsModel();
		try {
			BeanUtils.copyProperties(model, userCoupons);
			BeanUtils.copyProperties(model, coupons);
		}catch(Exception e) {
			logger.error("",e);
			throw new BusinessException();
		}
		return model;
	}

	@Override
	public List<UserCouponsModel> getUsedUserCouponsByOrder(String cloudUserId, String orderId) {
		List<UserCouponsModel> list = userCouponsMapper.selectusedUserCouponsByBusiOrderId(cloudUserId, orderId);
		return list;
	}

}
