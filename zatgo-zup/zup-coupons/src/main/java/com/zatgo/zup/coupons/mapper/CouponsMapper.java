package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.Coupons;
import com.zatgo.zup.coupons.entity.CouponsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CouponsMapper {
    int countByExample(CouponsExample example);

    int deleteByExample(CouponsExample example);

    int deleteByPrimaryKey(String couponsId);

    int insert(Coupons record);

    int insertSelective(Coupons record);

    List<Coupons> selectByExampleWithBLOBs(CouponsExample example);

    List<Coupons> selectByExample(CouponsExample example);

    Coupons selectByPrimaryKey(String couponsId);

    int updateByExampleSelective(@Param("record") Coupons record, @Param("example") CouponsExample example);

    int updateByExampleWithBLOBs(@Param("record") Coupons record, @Param("example") CouponsExample example);

    int updateByExample(@Param("record") Coupons record, @Param("example") CouponsExample example);

    int updateByPrimaryKeySelective(Coupons record);

    int updateByPrimaryKeyWithBLOBs(Coupons record);

    int updateByPrimaryKey(Coupons record);
    
    List<Coupons> selectCouponsByActivityId(@Param("activityId") String activityId);
}