package com.zatgo.zup.coupons.service;

import java.util.List;

import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.coupons.InternalUsageCouponsOrderParams;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;
import com.zatgo.zup.coupons.model.CanGetCouponsModel;
import com.zatgo.zup.coupons.model.InternalUserCouponsSearchParams;

public interface UserCouponsService {

	/**
	 * 查询用户的优惠券
	 * @param params
	 * @return
	 */
	public PageInfo<UserCouponsModel> getUserCouponsList(InternalUserCouponsSearchParams params);
	
	/**
	 * 根据主键查询用户的优惠券
	 * @param cloudUserId
	 * @param userCouponsId
	 * @return
	 */
	public UserCouponsModel getUserCouponsById(String cloudUserId,String userCouponsId);
	
	/**
	 * 查询指定订单可用的优惠券
	 * @param params
	 * @return
	 */
	public List<UserCouponsModel> getUserCouponsByOrder(InternalUsageCouponsOrderParams params);
	
	/**
	 * 查询指定订单已使用过的优惠券
	 * @param params
	 * @return
	 */
	public List<UserCouponsModel> getUsedUserCouponsByOrder(String cloudUserId,String orderId);
	
	
	/**
	 * 用户使用优惠券
	 * @param params
	 * @param userCouponsIds
	 * @return
	 */
	public List<UserCouponsModel> usageCoupons(InternalUsageCouponsOrderParams params);
	
	/**
	 * 根据订单退还用户使用的优惠券
	 * @param params
	 * @param userCouponsIds
	 * @return
	 */
	public Boolean cancelUsageCoupons(InternalUsageCouponsOrderParams params);
	
	/**
	 * 验证用户使用优惠券
	 * @param params
	 * @param userCouponsIds
	 * @return
	 */
	public List<UserCouponsModel> checkUsageCoupons(InternalUsageCouponsOrderParams params);
	
	/**
	 * 用户手动领取优惠券
	 * @param cloudUserId
	 * @param userId
	 * @param couponsId
	 * @return
	 */
	public UserCouponsModel userManualGetCoupons(String cloudUserId,String userId,String couponsId);

	/**
	 * 系统触发发放优惠券
	 * @param cloudUserId
	 * @param userId
	 * @param triggerBusinessType
	 * @param triggerBusinessCode
	 * @return
	 */
	public List<UserCouponsModel> triggerGetCoupons(String cloudUserId,String userId,
			String triggerBusinessType,String triggerBusinessCode);
	
	
	/**
	 * 根据 用户获取可手动领取的优惠券
	 * @param cloudUserId
	 * @param userId
	 * @return
	 */
	List<CanGetCouponsModel> getCanManualGetCouponsListByUser(String cloudUserId, 
			String userId);
	
}
