package com.zatgo.zup.coupons.mapper;

import com.zatgo.zup.coupons.entity.CouponsOrderAbbr;
import com.zatgo.zup.coupons.entity.CouponsOrderAbbrExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CouponsOrderAbbrMapper {
    int countByExample(CouponsOrderAbbrExample example);

    int deleteByExample(CouponsOrderAbbrExample example);

    int deleteByPrimaryKey(String couponsOrderId);

    int insert(CouponsOrderAbbr record);

    int insertSelective(CouponsOrderAbbr record);

    List<CouponsOrderAbbr> selectByExample(CouponsOrderAbbrExample example);

    CouponsOrderAbbr selectByPrimaryKey(String couponsOrderId);

    int updateByExampleSelective(@Param("record") CouponsOrderAbbr record, @Param("example") CouponsOrderAbbrExample example);

    int updateByExample(@Param("record") CouponsOrderAbbr record, @Param("example") CouponsOrderAbbrExample example);

    int updateByPrimaryKeySelective(CouponsOrderAbbr record);

    int updateByPrimaryKey(CouponsOrderAbbr record);

}