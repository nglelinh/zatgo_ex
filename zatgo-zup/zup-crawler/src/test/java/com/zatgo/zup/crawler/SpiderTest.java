package com.zatgo.zup.crawler;

import com.zatgo.zup.crawler.spider.xiecheng.XieChengSpider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by 46041 on 2019/4/18.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZupCrawlerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class SpiderTest {

    @Autowired
    private XieChengSpider spider;

    @Autowired
    private SpiderMain main;

    @Test
    public void Test(){
//        spider.start();
        main.start();
    }
}
