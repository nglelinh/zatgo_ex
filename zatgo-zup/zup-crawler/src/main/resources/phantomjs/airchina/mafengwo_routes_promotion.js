var page = require('webpage').create();
var system = require('system');

var url = system.args[1];

var paraObj = {};

page.settings.loadImages = false;  //为了提升加载速度，不加载图片
page.settings.resourceTimeout = 30000;//超过30秒放弃加载
page.settings.userAgent = "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0";


//此处是用来设置截图的参数。不截图没啥用
// page.viewportSize = {
//     width: 1280,
//     height: 800
// };

// block_urls = ['baidu.com'];//为了提升速度，屏蔽一些需要时间长的。比如百度广告

// page.onResourceRequested = function(requestData, request){
//     for(url in block_urls) {
//         if(requestData.url.indexOf(block_urls[url]) !== -1) {
//             request.abort();
//             //console.log(requestData.url + " aborted");
//             return;
//         }
//     }
// }



page.open(url, function(status) {
    if (status !== 'success') {
        console.log('9999');
        phantom.exit();
    } else {
        try {
            page.includeJs("http://code.jquery.com/jquery-1.6.1.min.js", function () {
                window.setTimeout(function() {
                    paraObj = page.content;
                    var msg = JSON.stringify(paraObj);
                    page.evaluate(function(_msg) {
                        try {
                            $.ajax({
                                url: 'http://127.0.0.1:8100/receive/data',
                                type: 'POST',
                                dataType: 'json',
                                contentType: 'application/json; charset=utf-8',
                                data: JSON.stringify({'a': _msg}),
                                success: function (response, xml) {
                                    //请求成功后执行的代码
                                },
                                error: function (status) {

                                }
                            });
                        } catch (e) {
                            console.log(e);
                            phantom.exit();
                        }
                    }, msg)
                }, 15000);
            });
        } catch (e){
            console.log(e);
            phantom.exit();
        }
    }


});


function parseRoutesData(){

    // $("#originCity").val("SHA");
    //
    // $("#flightMapSearchBtn").trigger("click");

    // $(".pg-next").click();

}

// var ajax={
//     get: function(url, fn) {
//         // XMLHttpRequest对象用于在后台与服务器交换数据
//         var xhr = new XMLHttpRequest();
//         xhr.open('GET', url, true);
//         xhr.onreadystatechange = function() {
//             // readyState == 4说明请求已完成
//             if (xhr.readyState == 4 && xhr.status == 200 || xhr.status == 304) {
//                 // 从服务器获得数据
//                 fn.call(this, xhr.responseText);
//             }
//         };
//         xhr.send();
//     },
//     // datat应为'a=a1&b=b1'这种字符串格式，在jq里如果data为对象会自动将对象转成这种字符串格式
//     post: function (url, data, fn) {
//         var xhr = new XMLHttpRequest();
//         xhr.open("POST", url, true);
//         // 添加http头，发送信息至服务器时内容编码类型
//         xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
//         xhr.onreadystatechange = function() {
//             if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 304)) {
//                 fn.call(this, xhr.responseText);
//             }
//         };
//         xhr.send(data);
//     }
// }
//
//
// function sendMsg() {
//     window.setTimeout(function() {
//         try {
//             $.ajax({
//                 url: 'http://127.0.0.1:8100/receive/data',
//                 type: 'POST',
//                 dataType: 'json',
//                 contentType: 'application/json',
//                 data: JSON.stringify({name: sadas_content}),
//                 success: function (response, xml) {
//                     //请求成功后执行的代码
//                 },
//                 error: function (status) {
//                     //失败后执行的代码
//                 }
//             });
//         } catch (e) {
//             console.log(e);
//             phantom.exit();
//         }
//     }, 5000);
// }
