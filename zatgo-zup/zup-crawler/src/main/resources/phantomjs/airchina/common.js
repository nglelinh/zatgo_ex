var page = require('webpage').create();
var system = require('system');

var url = system.args[1];
var param = system.args[2];

page.settings.loadImages = false;  //为了提升加载速度，不加载图片
page.settings.resourceTimeout = 30000;//超过30秒放弃加载
page.settings.userAgent = "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0";

//此处是用来设置截图的参数。不截图没啥用
// page.viewportSize = {
//     width: 1280,
//     height: 800
// };

// block_urls = ['baidu.com'];//为了提升速度，屏蔽一些需要时间长的。比如百度广告

// page.onResourceRequested = function(requestData, request){
//     for(url in block_urls) {
//         if(requestData.url.indexOf(block_urls[url]) !== -1) {
//             request.abort();
//             //console.log(requestData.url + " aborted");
//             return;
//         }
//     }
// }
page.onConsoleMessage = function(msg) {
    console.log(msg)
};

page.open(url, function(status) {
    if (status !== 'success') {
        console.log('9999');
        phantom.exit();
    } else {
        try{
            param = window.atob(param);
            page.includeJs("http://code.jquery.com/jquery-1.6.1.min.js", function () {
                //等待页面加载
                window.setTimeout(function() {
                    var params = JSON.parse(param);
                    //页面等待时间
                    var waitTime = params.waitTime;
                    //总等待时间
                    var waitTimeCount = 0;
                    //获取数据前置操作
                    var prefixClick = params.prefixClick;
                    if (prefixClick){
                        for (var h = 0; h < prefixClick.length; h++){
                            var prefix = prefixClick[h];
                            window.setTimeout(function() {
                                page.evaluate(function(par, click, getClickElement) {
                                    click (par.selector, par.index, getClickElement)
                                }, prefix, click, getClickElement);
                            }, waitTimeCount)
                            waitTimeCount += waitTime;
                        }
                    }

                    //获取数据
                    var getDataClick = params.getData;
                    var toNextClick = params.toNext;
                    var count = params.callTheNumber;
                    var noticeUrl = params.noticeUrl;
                    for (var i =0; i < count; i++){
                        //取数据主体
                        if (getDataClick){
                            for (var j = 0; j < getDataClick.length; j++){
                                var dataClick = getDataClick[j];
                                window.setTimeout(function() {
                                    page.evaluate(function(par, click, getClickElement) {
                                        click (par.selector, par.index, getClickElement)
                                    }, dataClick, click, getClickElement);
                                }, waitTimeCount);
                                waitTimeCount += waitTime;
                            }
                        }
                        //输出数据
                        window.setTimeout(function() {
                            page.evaluate(function(_post, noticeUrl) {
                                // _post(noticeUrl, $(":root").html());
                                console.log($(":root").html())
                            }, _post, noticeUrl);
                        }, waitTimeCount);
                        waitTimeCount += waitTime;

                        //翻页
                        if (toNextClick){
                            for (var k = 0; k < toNextClick.length; k++){
                                var toNext = toNextClick[k];
                                window.setTimeout(function() {
                                    page.evaluate(function (par, click, getClickElement) {
                                        click (par.selector, par.index, getClickElement)
                                    }, toNext, click, getClickElement);
                                }, waitTimeCount);
                                waitTimeCount += waitTime;
                            }
                        }
                    }

                    window.setTimeout(function() {
                        phantom.exit();
                    }, waitTimeCount);

                }, 5000);
            });


        } catch (e){
            var res = {'type':-1, 'data': e};
            console.log(JSON.stringify(res));
        }
    }


});







function click (selects, indexs, _getClickElement) {
    if (selects){
        var clickEvent = document.createEvent("HTMLEvents");
        clickEvent.initEvent("click",false,true);
        var doc = null;
        for (var i = 0; i < selects.length; i++){
            doc = _getClickElement(doc, selects[i], indexs[i], i + 1 == selects.length, i == 0);
        }
        doc.dispatchEvent(clickEvent);
    }
}

function getClickElement(doc, select, index, isLast, isFirst) {
    decode(select, "iso 8859-1")
    console.log('>>>>select:', select);
    if (isFirst){
        doc = $(select);
    } else {
        doc = doc.find(select);
    }

    if (index == -1){
        return doc;
    }

    if (isLast){
        return doc[index];
    } else {
        return doc.eq(index);
    }
}


function sleep(d){
    var t = Date.now();
    while(Date.now - t <= d);
}


function _post(url, data){
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: data,
        success: function (response, xml) {
            //请求成功后执行的代码
        },
        error: function (status) {

        }
    });
}









function endTest() {
    var clickEvent = document.createEvent("HTMLEvents");
    clickEvent.initEvent("click",false,true);
    $(".pg-next")[0].dispatchEvent(clickEvent);
    // $(".pg-next")[0].dispatchEvent(clickEvent);
    // $(".pg-next")[0].dispatchEvent(clickEvent);

    // $('.navbar-sub').children().children().addEvent();
    // var clickEvent = $('.navbar-sub').children().children().createEvent("HTMLEvents");
    // clickEvent.initEvent("click", false, false);
    // dom.dispatchEvent(evt);
    //
    // var e = document.createEvent('MouseEvents');
    // e.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    // var href = ;
    // href.dispatchEvent(e);
    // href.click();
}
