package com.zatgo.zup.crawler.proxy;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.crawler.common.DateTimeUtils;
import com.zatgo.zup.crawler.common.SingleTimerTools;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;


/**
 * 代理ip获取
 * @author hejiang
 *
 */
@Service
public class ProxyService {

	private Logger log = LoggerFactory.getLogger(getClass().getName());
	

	//线程默认休眠时间 10秒
	private static final String SLEEP_TIME = "10000";
	
	@Resource
	private OkHttpService httpService;
	
	//测试代理ip响应速度的url
	private static final String TEST_IP_URL = "http://www.sina.com.cn";
	//测试代理ip超时时间
	private static final int TEST_TIME_OUT = 3000;
	//无忧ip测试响应速度超时次数
	private static int WUYOU_IP_FAIL_NUMBER = 0;

	@Value("${proxy.wuyou.url}")
	private String WUYOU_URL;

	@Value("${proxy.zhima.url}")
	private String ZHIMA_URL;

	public String getWuyouProcyId() {
		String ipstr = httpService.getStr(WUYOU_URL, null);
		log.info("ip:"+ipstr);
		return ipstr;
	}

	/**
	 * 获取芝麻ip
	 * @return
	 */
	public String getZhimaProcyId() {
		String ipstr = httpService.getStr(ZHIMA_URL, null);
		if(StringUtils.isNotEmpty(ipstr)){
			log.info("获取芝麻代理ip="+ipstr);
			JSONObject json = JSON.parseObject(ipstr);
			if("0".equals(json.getString("code")) && "true".equals(json.getString("success"))){
				JSONArray ja = json.getJSONArray("data");
				JSONObject jo = ja.getJSONObject(0);
				String ip = jo.getString("ip");
				String port = jo.getString("port");
				Date expireTime = DateTimeUtils.parseStringToDate(jo.getString("expire_time"), DateTimeUtils.PATTEN_YYYY_MM_DD_HH_MM_SS);
				return ip+":"+port+","+(expireTime.getTime()-System.currentTimeMillis());
			}
		}
		return null;
	}
	
	/**
	 * 提取无忧代理ip
	 */
	public Map<String, String> getProxyIp(){
		try {
			String ipstr = this.getWuyouProcyId();
			log.info("ip:"+ipstr);
			String[] ips = ipstr.trim().split(",");
			Map<String, String> map = new HashMap<String, String>();
			map.put("ip", ips[0].split(":")[0]);
			map.put("port", ips[0].split(":")[1]);
			map.put("validTime", ips[1]);
			return map;
		} catch (Exception e) {
			log.error("获取代理ip失败", e);
			return null;
		}
	}

	public Map<String, String> getProxyZhimaIp(){
		try {
			String ipstr = this.getZhimaProcyId();
			log.info("ip:"+ipstr);
			String[] ips = ipstr.trim().split(",");
			Map<String, String> map = new HashMap<String, String>();
			map.put("ip", ips[0].split(":")[0]);
			map.put("port", ips[0].split(":")[1]);
			map.put("validTime", ips[1]);
			return map;
		} catch (Exception e) {
			log.error("获取芝麻代理ip失败", e);
			return null;
		}
	}


	/**
	 * 设置代理ip
	 */
	public void setSystemProxyIp(String ip, String port) {
		log.info("更新代理："+ip+":"+port);
		System.setProperty("proxyType", "4");
        System.setProperty("proxyPort", port);
        System.setProperty("proxyHost", ip);
        System.setProperty("proxySet", "true");
	}
	
	/**
	 * 代理ip自动替换任务
	 */
	public void initProxyIp(Long delayTime) {
		SingleTimerTools.getInstance().schedule(new TimerTask() {
			@Override
			public void run() {
				setProxyIp();
			}

			private void setProxyIp(){
				try {
					Map<String, String> map = null;
					if(WUYOU_IP_FAIL_NUMBER == 2){
						map = getProxyZhimaIp();
					}else{
						map = getProxyIp();
					}
					if(map != null && !map.isEmpty()){
						setSystemProxyIp(map.get("ip"), map.get("port"));
						//测试代理的响应时间
						Connection.Response rs = Jsoup.connect(TEST_IP_URL).timeout(TEST_TIME_OUT).execute();
						if(rs.statusCode() != 200){//响应不成功的情况换掉ip
							Thread.sleep(1000);//休眠后继续请求ip
							setProxyIp();
							return;
						}

						//ip有效时间
						String validTime = map.get("validTime");
						if(StringUtils.isEmpty(validTime)){
							validTime = SLEEP_TIME;
						}
						Long sleepTime = Long.parseLong(validTime);
						//线程休眠时间
						Thread.sleep(sleepTime - (sleepTime/5));
					}else{
						WUYOU_IP_FAIL_NUMBER++;
						//没有取到代理ip,休眠1秒后再次请求
						Thread.sleep(3000);
					}
					WUYOU_IP_FAIL_NUMBER = 0;
				} catch (SocketTimeoutException e){//测试ip响应速度请求超时，更新ip
					log.error("测试ip响应速度请求超时！", e);
					WUYOU_IP_FAIL_NUMBER++;
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						log.error("线程休眠失败", e);
						return;
					}
				} catch (Exception e) {
					log.error("代理ip自动替换任务失败", e);
					try {
						Thread.sleep(Long.parseLong(SLEEP_TIME));
					} catch (Exception e2) {
						log.error("线程休眠失败", e);
						return;
					}
				}
			}
			
		}, delayTime, 1);
	}

	/**
	 * 更新代理ip
	 */
	public boolean updateProxyIp() {
		Map<String, String> map = getProxyIp();
		if(map != null && !map.isEmpty()){
			setSystemProxyIp(map.get("ip"), map.get("port"));
			return true;
		}
		return false;
	}
	
	
}


