package com.zatgo.zup.crawler.remoteService;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.common.model.SaveImageRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created by 46041 on 2019/4/17.
 */

@FeignClient("zup-tourism-search")
public interface TourismSearchRemoteService {

    @PostMapping(value = "/data/save")
    void saveData(@RequestBody SaveDataRequest request);

    @PostMapping(value = "/data/save/image")
    ResponseData<String> saveImage(@RequestBody SaveImageRequest request);

}
