package com.zatgo.zup.crawler.utils.phantomjs.airchina;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.crawler.common.JsonUtils;
import com.zatgo.zup.crawler.model.promotion.PromotionData;
import com.zatgo.zup.crawler.utils.phantomjs.SeleniumTool;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chen on 2019/3/15.
 */
//@Component
public class AirchinaPromotionSelenium extends SeleniumTool {

    private final String JSON_CA_PROMOTION_CITY = "phantomjs/airchina/ca_promotion_city.json";
    private final String CA_PROMOTION_URL = "http://www.airchina.com.cn/cn/specials/routes_promotion.shtml";


    public List<PromotionData> getPromotions() {

        try {

            //首先获取出发城市列表
            String cityStr = JsonUtils.readJsonFromClassPath(JSON_CA_PROMOTION_CITY);
            if(StringUtils.isEmpty(cityStr))
                return null;

            // 让浏览器访问主页
            driver.get(CA_PROMOTION_URL);

            WebElement selectElement = new WebDriverWait(driver, 10).until(new ExpectedCondition< WebElement>(){
                @Override
                public WebElement apply(WebDriver webDriver) {
                    return webDriver.findElement(By.id("originCity"));
                }
            });

            WebElement searchButton = new WebDriverWait(driver, 10).until(new ExpectedCondition< WebElement>(){
                @Override
                public WebElement apply(WebDriver webDriver) {
                    return webDriver.findElement(By.id("flightMapSearchBtn"));
                }
            });

            Select select = new Select(selectElement);

            List<PromotionData> datas = new ArrayList<>();

            JSONArray cityArray = JSON.parseArray(cityStr);
            for(int i=0;i<cityArray.size();i++){
                JSONObject city = (JSONObject) cityArray.get(i);
                String cityCode = city.getString("code");
                List<PromotionData> promotions = getPromotionByCity(select,searchButton,cityCode);
                datas.addAll(promotions);
                Thread.sleep(2000);
            }

            return datas;

        } catch (Exception e) {
            logger.error("",e);
        } finally {
            //关闭并退出浏览器
            driver.close();
            driver.quit();
        }

        return null;

    }

    private List<PromotionData> getPromotionByCity(Select select, WebElement searchButton, String cityCode) {
        select.selectByValue(cityCode);

        searchButton.click();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            logger.error("",e);
        }

        WebElement tableHtml = new WebDriverWait(driver, 10).until(new ExpectedCondition< WebElement>(){
            @Override
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(By.id("html"));
            }
        });

        List<WebElement> trElements = tableHtml.findElements(By.xpath("table/tbody/tr"));

        List<PromotionData> datas = new ArrayList<>();

        trElements.forEach(webElement -> {

            try{
                List<WebElement> tdElements = webElement.findElements(By.className("tableTdSearch"));
                if(tdElements==null||tdElements.size()==0){
                    return;
                }
                String fromCity = tdElements.get(0).getText();
                String toCity = tdElements.get(1).getText();
                String price = tdElements.get(2).getText();
                String fromDate = tdElements.get(3).getText();
                String url = tdElements.get(4).getText();

                PromotionData data = new PromotionData();
                data.setFromCity(fromCity);
                data.setToCity(toCity);
                data.setRefPrice(price);
                data.setFromDate(fromDate);
                data.setUrl(url);

                datas.add(data);
            }catch (Exception e){
                logger.error("",e);
            }

        });


        return datas;
    }

}
