package com.zatgo.zup.crawler.spider.xiecheng;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.model.HbaseConstant;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.common.model.SaveImageRequest;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.crawler.annotation.Spider;
import com.zatgo.zup.crawler.model.enums.SpiderEnum;
import com.zatgo.zup.crawler.model.promotion.PageModel;
import com.zatgo.zup.crawler.model.xiecheng.ScenicSpot;
import com.zatgo.zup.crawler.model.xiecheng.SpiderParam;
import com.zatgo.zup.crawler.remoteService.TourismSearchRemoteService;
import com.zatgo.zup.crawler.spider.SpiderInterface;
import com.zatgo.zup.crawler.utils.jsoup.JsoupUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by 46041 on 2019/4/17.
 */

@Spider(value = SpiderEnum.ALIASTYPE)
public class XieChengSpider implements SpiderInterface {

    private static final Logger logger = LoggerFactory.getLogger(XieChengSpider.class);

    private static final String url = "https://you.ctrip.com/sight/";

    private static final String detail = "https://you.ctrip.com";

    private static final String gaodeAPI = "https://restapi.amap.com/v3/geocode/geo";

    private static final String gaodeKey = "9caa771d882f4609e9609208f08d8b0b";

    private static final Long cacheSaveTime = 30l;

    @Autowired
    private TourismSearchRemoteService tourismSearchRemoteService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisUtils redisUtils;


    public void spider(String pageUrl){
        Document detailHtml = JsoupUtil.get(pageUrl);
        String cityName = detailHtml.getElementsByClass("f_left").get(0).getElementsByTag("a").html();
        Elements listMod2 = detailHtml.getElementsByClass("list_mod2");
        for (int i = 0; i < listMod2.size(); i++){
            Element doc = listMod2.get(i);
            ScenicSpot ss = new ScenicSpot();
            String infoUrl = detail + doc.getElementsByTag("a").get(0).attr("href");
            //排名
            Elements sortElement = doc.getElementsByClass("rdetailbox").get(0).getElementsByTag("s");
            if (sortElement != null && sortElement.size() > 0){
                String sort = sortElement.get(0).html();
                ss.setSort(sort);
            }
            //地区
            String area = doc.getElementsByClass("ellipsis").get(0).html();
            if (!StringUtils.isEmpty(area)){
                area = area.replace("\u200E", "");
                area = area.trim();
                area = area.replaceAll(" ", "");
            }
            try {
                //图片
                String imgUrl = doc.getElementsByClass("leftimg").get(0).getElementsByTag("img").get(0).attr("src");
                String image = JsoupUtil.downloadImage(imgUrl);
                SaveImageRequest request = new SaveImageRequest();
                request.setTableName(HbaseConstant.ScenicSpotImageTableName);
                request.setFamilyName(HbaseConstant.ScenicSpotImageFamilyName);
                request.setImageValueKey(HbaseConstant.ScenicSpotImageValueKey);
                request.setData(image);
                ResponseData<String> responseData = tourismSearchRemoteService.saveImage(request);
                if (responseData != null && responseData.isSuccessful()){
                    ss.setImgs(responseData.getData());
                }
            } catch (Exception e){
                logger.error("", e);
            }
            //景点名称
            String name = detailHtml.getElementsByClass("rdetailbox").get(i).getElementsByTag("a").get(0).html();
            ss.setName(name);
            try {
                //省市区
                String api = gaodeAPI + "?address=" + name + "&city=" + cityName + "&key=" + gaodeKey;
                api = api.replace("\u200E", "");
                api = api.trim();
                api = api.replaceAll(" ", "");
                String s = JsoupUtil.httpGet(api);
                if (!StringUtils.isEmpty(s)){
                    JSONObject res = JSONObject.parseObject(s);
                    if (res.getInteger("status") == 1){
                        JSONArray geocodes = res.getJSONArray("geocodes");
                        if (geocodes != null && !geocodes.isEmpty()){
                            JSONObject resAddr = geocodes.getJSONObject(0);
                            ss.setCountry(resAddr.getString("country"));
                            ss.setProvince(resAddr.getString("province"));
                            ss.setCity(resAddr.getString("city"));
                            ss.setArea(resAddr.getString("district"));
                            String location = resAddr.getString("location");
                            if (!StringUtils.isEmpty(location)){
                                String[] split = location.split(",");
                                ss.setLongitude(split[0]);
                                ss.setLatitude(split[1]);
                            }
                        }
                        ss.setAddr(area);
                    }
                }
            } catch (Exception e){
                logger.error("", e);
            }
            Document document = JsoupUtil.get(infoUrl);
            //批号
            ss.setTimeStamp(SpiderParam.getTimeStamp());
            if (document != null){
                //评分
                Elements score = document.getElementsByClass("score");
                if (score != null && !score.isEmpty()){
                    Elements b = score.get(0).getElementsByTag("b");
                    if (b != null && !b.isEmpty()){
                        String sorce = b.get(0).html();
                        ss.setScore(sorce);
                    }
                }
                //简介
                Elements textStyle = document.getElementsByClass("text_style");
                try{
                    if (textStyle != null && !textStyle.isEmpty()){
                        String introduction = textStyle.get(1).html();
                        ss.setIntroduction(introduction);
                        //提示
                        if (textStyle.size() >= 6){
                            String remind = textStyle.get(3).html();
                            ss.setRemind(remind);
                        }
                    }
                } catch (Exception e){
                    logger.error("", e);
                }
            }
            try{
                SaveDataRequest request = new SaveDataRequest();
                request.setData(JSONObject.parseObject(JSONObject.toJSONString(ss)));
                request.setFamilyName(HbaseConstant.ScenicSpotFamilyName);
                String rowKey = ss.getCountry() + ss.getProvince() + ss.getCity() + ss.getName();
                request.setRowKey(rowKey);
                request.setTableName(HbaseConstant.ScenicSpotTableName);
                tourismSearchRemoteService.saveData(request);
            } catch (Exception e){
                logger.error("", e);
            }
        }

    }

    @Override
    public String defaultStart() {
        return null;
    }

    @Override
    public SaveDataRequest defaultGetInfo(String url) {
        return null;
    }

    public void init(){
        Long city = redisTemplate.opsForValue().increment(SpiderParam.cityKey, 0l);
        if (city == null || city.intValue() == 0){
            redisTemplate.opsForValue().increment(SpiderParam.cityKey, 1l);
        }
    }

    @Override
    public void aliasStart() {
        while (true){
            Long city = redisTemplate.opsForValue().increment(SpiderParam.cityKey, 0l);
            redisTemplate.expire(SpiderParam.cityKey, cacheSaveTime, TimeUnit.DAYS);
            Long pageNo = redisTemplate.opsForValue().increment(SpiderParam.getPageNoKey(city), 1l);
            redisTemplate.expire(SpiderParam.cityKey, cacheSaveTime, TimeUnit.DAYS);
            Integer totalPage = null;
            String totalPageKey = SpiderParam.getTotalPageKey(city);
            //有的没有总页数
            if (!redisUtils.hasKey(totalPageKey)){
                //没有的话，先获取总页数
                Document detailHtml = JsoupUtil.get(getUrl(null, city.intValue()));
                if (detailHtml == null){
                    redisUtils.put(SpiderParam.cityKey, city + 1l, cacheSaveTime, TimeUnit.DAYS);
                    continue;
                }
                String numpage = detailHtml.getElementsByClass("numpage").html();
                if (StringUtils.isEmpty(numpage)){
                    nextCity();
                    continue;
                }
                totalPage = Integer.valueOf(numpage);
                redisUtils.put(totalPageKey, totalPage, cacheSaveTime, TimeUnit.DAYS);
            } else {
                totalPage = redisUtils.get(totalPageKey, Integer.class);
                if (pageNo.intValue() > totalPage.intValue() * 10){
                    nextCity();
                    continue;
                }
                //看看当前页数是不是小于总页数
                if (pageNo.intValue() > totalPage)
                    continue;
                //看看当前页数是否满足翻页要求，如果满足，则翻页
                if (pageNo.intValue() >= totalPage){
                    nextCity();
                }
            }
            logger.info("城市：" + city.intValue() + "=== 页码：" + pageNo.intValue());
            try {
                spider(getUrl(pageNo.intValue(), city.intValue()));
            } catch (Exception e){
                logger.error("", e);
                try{
                    spider(getUrl(pageNo.intValue(), city.intValue()));
                } catch (Exception e1){
                    logger.error("", e1);
                }
            }
        }
    }

    @Override
    public List<String> jsEngineStart() {
        return null;
    }

    @Override
    public PageModel jsEngineGetInfo(String url) {
        return null;
    }

    @Override
    public void jsEngineCallBack(String pageInfo, String url) {

    }

    private void nextCity(){
        redisTemplate.opsForValue().increment(SpiderParam.cityKey, 1l);
    }

    private static String getUrl(Integer pageNo, int city){
        String u = null;
        if (pageNo != null){
            u = url + city + "/s0-p" + pageNo +".html#sightname";
        } else {
            u = url + city + ".html";
        }
        return u;
    }
}
