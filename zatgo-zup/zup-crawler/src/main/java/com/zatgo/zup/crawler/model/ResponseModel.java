package com.zatgo.zup.crawler.model;



import com.zatgo.zup.crawler.exception.BusinessCode;

import java.io.Serializable;

/**
 * Created by chen on 2018/9/11.
 */
public class ResponseModel implements Serializable{

    private String code;

    private String message;

    private Object data;

    /**
     * 成功返回数据
     * @param data
     */
    public ResponseModel(Object data){
        this.code = BusinessCode.SUCCESS;
        this.message = BusinessCode.getMessage(BusinessCode.SUCCESS);
        this.data = data;
    }

    public ResponseModel(String code){
        this.code = code;
        this.message = BusinessCode.getMessage(code);
    }

    public ResponseModel(String code,String message){
        this.code = code;
        this.message = message;
    }

    public ResponseModel(String code, String message, Object data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
