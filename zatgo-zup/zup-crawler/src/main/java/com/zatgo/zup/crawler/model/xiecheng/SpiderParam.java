package com.zatgo.zup.crawler.model.xiecheng;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by 46041 on 2019/4/18.
 */
public class SpiderParam {


    //========================================  携程  =============================================
    private static final String prefix = "xiecheng:spider:";

    public static final String cityKey = prefix + "Scenic_Spot:city";

    private static final String pageNoKey = prefix + "Scenic_Spot:pageNo:";

    private static final String totalPageKey = prefix + "Scenic_Spot:totalPage:";


    public static String getPageNoKey(Long city){
        return pageNoKey + city;
    }

    public static String getTotalPageKey(Long city){
        return totalPageKey + city;
    }

    public static Long getTimeStamp(){
        return Long.valueOf(System.currentTimeMillis() / 1000 + ((long)((Math.random()*9+1)* Math.pow(10,8)) + ""));
    }


    //========================================  携程  =============================================



    //========================================  马蜂窝  =============================================

    private static final String mfwPrefix = "mafw:spider:";

    public static final String mfwCountryKey = mfwPrefix + "Scenic_Spot:country";

    private static final String mfwPageNoKey = mfwPrefix + "Scenic_Spot:pageNo:";

    private static final String mfwTotalPageKey = mfwPrefix + "Scenic_Spot:totalPage:";

    private static final String mfwScenicListKey = mfwPrefix + "Scenic_Spot:scenicList:";

    public static final String lastPageKey = mfwPrefix + "Scenic_Spot:lastPage";

    public static final String pageListKey = mfwPrefix + "Scenic_Spot:pageList:";

    public static final String mfwcountryCityKey = mfwPrefix + "Scenic_Spot:countryKey:";


    public static String getMFWPageNoKey(Long country){
        return mfwPageNoKey + country;
    }

    public static String getMFWTotalPageKey(Long country){
        return mfwTotalPageKey + country;
    }

    public static String getMFWScenicListKey(Long country, Long pageNo){
        return mfwScenicListKey + country + ":" + pageNo;
    }


    //========================================  马蜂窝  =============================================

}
