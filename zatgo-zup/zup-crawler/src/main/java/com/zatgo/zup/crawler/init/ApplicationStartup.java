package com.zatgo.zup.crawler.init;

import javax.annotation.PostConstruct;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup {

	public static void main(String[] args) {
		WebDriver driver=getPhantomJSDriver();
        driver.get("https://vacations.ctrip.com/");
        WebElement webElement = driver.findElement(By.className("product_list"));
        String str = webElement.getAttribute("outerHTML");
        System.out.println(str);
        System.out.println(driver.getCurrentUrl());
	}
//	@PostConstruct
//	public void startup() {
//		WebDriver driver=getPhantomJSDriver();
//        driver.get("http://www.baidu.com");
//        WebElement element = driver.findElement(By.id("vac-103045-recommend-tab-周边当地游-杭州"));
//        System.out.println(element.getText());
//        System.out.println(driver.getCurrentUrl());
//	}
	
	public static PhantomJSDriver getPhantomJSDriver(){
        //设置必要参数
        DesiredCapabilities dcaps = new DesiredCapabilities();
        //ssl证书支持
        dcaps.setCapability("acceptSslCerts", true);
        //截屏支持
        dcaps.setCapability("takesScreenshot", false);
        //css搜索支持
        dcaps.setCapability("cssSelectorsEnabled", true);
        //js支持
        dcaps.setJavascriptEnabled(true);
        //驱动支持
        dcaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"D:\\green software\\phantomjs-2.1.1-windows\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");

        PhantomJSDriver driver = new PhantomJSDriver(dcaps);
        return  driver;
    }
}
