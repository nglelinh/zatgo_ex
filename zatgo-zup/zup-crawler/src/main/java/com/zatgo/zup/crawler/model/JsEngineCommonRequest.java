package com.zatgo.zup.crawler.model;

import com.zatgo.zup.crawler.model.promotion.PageModel;

import java.util.List;

/**
 * Created by 46041 on 2019/5/13.
 */
public class JsEngineCommonRequest {

    private List<String> urls;

    private PageModel pageModel;


    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public PageModel getPageModel() {
        return pageModel;
    }

    public void setPageModel(PageModel pageModel) {
        this.pageModel = pageModel;
    }

}
