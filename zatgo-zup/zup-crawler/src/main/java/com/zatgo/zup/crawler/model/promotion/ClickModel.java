package com.zatgo.zup.crawler.model.promotion;

import java.util.List;

/**
 * Created by 46041 on 2019/5/5.
 */
public class ClickModel {

    /**
     * jq选择器
     */
    private List<String> selector;
    /**
     * 索引
     */
    private List<Integer> index;

    public List<String> getSelector() {
        return selector;
    }

    public void setSelector(List<String> selector) {
        this.selector = selector;
    }

    public List<Integer> getIndex() {
        return index;
    }

    public void setIndex(List<Integer> index) {
        this.index = index;
    }
}
