package com.zatgo.zup.crawler.utils.phantomjs;

import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import com.zatgo.zup.crawler.model.promotion.ClickModel;
import com.zatgo.zup.crawler.model.promotion.PageModel;
import com.zatgo.zup.crawler.utils.jsoup.JsoupUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Phantomjs+JS 的方式调用
 * Created by chen on 2019/3/12.
 */
@Component
public class PhantomjsTool {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${phantomjs.exepath}")
    private String exePath;

    public String run(String url,String jsFile){
        try {
            ClassPathResource resource = new ClassPathResource(jsFile);
            String jsPath = resource.getFile().getAbsolutePath();
            return getParseredHtml(url,jsPath);
        } catch (IOException e) {
            logger.error("",e);
        }
        return null;
    }

    public String getParseredHtml(String url, String jsPath) {

        try{
            String cmd = "C:\\Users\\46041\\Desktop\\update\\zatgo-zup\\zatgo-zup\\zup-crawler\\src\\main\\resources\\exe\\phantomjs.exe" + " " + jsPath + " " + url;
            Runtime  rt= Runtime.getRuntime();
            Process p = rt.exec(cmd);
            InputStream is = p.getInputStream();

            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, StandardCharsets.UTF_8.name());
            String str = writer.toString();
            return str;
//            Document doc = Jsoup.parse(is,"UTF-8",url);
//            return doc;
        }catch(Exception e){
            logger.error("",e);
        }
        return null;
    }


    public static String getParseredHtml(String jsPath, PageModel param) {
        try{
            File file = new File(ResourceUtils.getURL("classpath:").getPath());
            String path = file.getPath();
            String exe = path + File.separatorChar + "exe" + File.separatorChar + "phantomjs.exe";
            String cmd =  exe + " " + jsPath + " " + param.getFirstUrl()
                    + " " + Base64.encode(JSONObject.toJSONString(param).getBytes()).replace("\n", "");
            Runtime  rt= Runtime.getRuntime();
            Process p = rt.exec(cmd);
            InputStream is = p.getInputStream();

            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, StandardCharsets.UTF_8.name());
            String str = writer.toString();
            return str;
//            Document doc = Jsoup.parse(is,"UTF-8",url);
//            return doc;
        }catch(Exception e){
//            logger.error("",e);
        }
        return null;
    }

    public static void main(String[] args) {
        PageModel pageModel = new PageModel();
        //初始页面
        pageModel.setFirstUrl("http://www.mafengwo.cn/travel-scenic-spot/mafengwo/11751.html");
        //翻3页
        pageModel.setCallTheNumber(2);
        //设置操作等待时间
        pageModel.setWaitTime(2000l);
        //获取数据前置操作
        List<ClickModel> prefixClickList = new ArrayList<>();
        ClickModel prefixClick = new ClickModel();
//        prefixClick.setSelector(Arrays.asList(".navbar-sub", "a"));
//        prefixClick.setIndex(Arrays.asList(0, 1));
        prefixClick.setSelector(Arrays.asList(".navbar-sub", "a[data-cs-p=景点]"));
        prefixClick.setIndex(Arrays.asList(0, 0));
        prefixClickList.add(prefixClick);
        pageModel.setPrefixClick(prefixClickList);
        //获取数据操作,如果不需要，则空着


        //获取完数据翻页
        List<ClickModel> toNextClickList = new ArrayList<>();
        ClickModel toNext = new ClickModel();
        toNext.setSelector(Arrays.asList(".pg-next"));
        toNext.setIndex(Arrays.asList(0));
        toNextClickList.add(toNext);
        pageModel.setToNext(toNextClickList);
        //数据回调地址
        pageModel.setNoticeUrl("http://127.0.0.1:8100/receive/data");
        String html = getParseredHtml("C:\\Users\\46041\\Desktop\\wk\\zatgo-zup\\zup-crawler\\src\\main\\resources\\phantomjs\\airchina\\common.js", pageModel);
        System.out.println(html);
    }

}
