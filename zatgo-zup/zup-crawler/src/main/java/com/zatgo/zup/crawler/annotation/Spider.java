package com.zatgo.zup.crawler.annotation;

import com.zatgo.zup.crawler.model.enums.SpiderEnum;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * Created by 46041 on 2019/4/28.
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Spider {

    SpiderEnum value() default SpiderEnum.DEFAULTTYPE;

    boolean isOpen() default false;
}
