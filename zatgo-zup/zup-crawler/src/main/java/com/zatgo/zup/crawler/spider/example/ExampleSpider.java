package com.zatgo.zup.crawler.spider.example;

import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.crawler.annotation.Spider;
import com.zatgo.zup.crawler.model.enums.SpiderEnum;
import com.zatgo.zup.crawler.model.promotion.ClickModel;
import com.zatgo.zup.crawler.model.promotion.PageModel;
import com.zatgo.zup.crawler.spider.SpiderInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 46041 on 2019/5/9.
 */

@Spider(value = SpiderEnum.JSENGINE, isOpen = false)
public class ExampleSpider implements SpiderInterface{


    @Override
    public String defaultStart() {
        return null;
    }

    @Override
    public SaveDataRequest defaultGetInfo(String url) {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public void aliasStart() {

    }

    @Override
    public List<String> jsEngineStart() {
        return Arrays.asList("http://www.mafengwo.cn/travel-scenic-spot/mafengwo/21536.html");
    }

    @Override
    public PageModel jsEngineGetInfo(String url) {
        PageModel pageModel = new PageModel();
        //初始页面
        pageModel.setFirstUrl(url);
        //翻3页
        pageModel.setCallTheNumber(5);
        //设置操作等待时间
        pageModel.setWaitTime(2000l);
        //获取数据前置操作
        List<ClickModel> prefixClickList = new ArrayList<>();
        ClickModel prefixClick = new ClickModel();
        prefixClick.setSelector(Arrays.asList(".navbar-sub", "a"));
        prefixClick.setIndex(Arrays.asList(0, 1));
        prefixClickList.add(prefixClick);
        pageModel.setPrefixClick(prefixClickList);
        //获取数据操作,如果不需要，则空着


        //获取完数据翻页
        List<ClickModel> toNextClickList = new ArrayList<>();
        ClickModel toNext = new ClickModel();
        toNext.setSelector(Arrays.asList(".pg-next"));
        toNext.setIndex(Arrays.asList(0));
        toNextClickList.add(toNext);
        pageModel.setToNext(toNextClickList);
        //数据回调地址
        pageModel.setNoticeUrl("http://127.0.0.1:8100/receive/data");

        return pageModel;
    }

    @Override
    public void jsEngineCallBack(String pageInfo, String url) {

    }

}
