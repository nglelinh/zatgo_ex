package com.zatgo.zup.crawler;

import com.zatgo.zup.crawler.annotation.Spider;
import com.zatgo.zup.crawler.model.enums.SpiderEnum;
import com.zatgo.zup.crawler.proxy.MainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Created by 46041 on 2019/4/28.
 */

@Component
public class SpiderMain {

    private static final Logger logger = LoggerFactory.getLogger(SpiderMain.class);

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private MainService mainService;



    @PostConstruct
    @Async
    public void start(){
        Map<String,Object> spiders = applicationContext.getBeansWithAnnotation(Spider.class);
        spiders.forEach((k, v) -> {
            try{
                Object bean = applicationContext.getBean(k);
                Class<?> claxx = bean.getClass();
                //看看有没有实现SpiderInterface接口
                Spider annotation = claxx.getAnnotation(Spider.class);
                SpiderEnum value = annotation.value();
                boolean open = annotation.isOpen();
                //默认类型
                if (SpiderEnum.DEFAULTTYPE.getCode().equals(value.getCode()) && open){
                    mainService.defaultStart(bean);
                }
                //自定义类型
                if (SpiderEnum.ALIASTYPE.getCode().equals(value.getCode()) && open){
                    mainService.aliasStart(bean);
                }
                //js引擎点击下一页
                if (SpiderEnum.JSENGINE.getCode().equals(value.getCode()) && open){
                    mainService.jsEngineStart(bean);
                }
            } catch (Exception e){
                logger.error("", e);
            }
        });
    }
}
