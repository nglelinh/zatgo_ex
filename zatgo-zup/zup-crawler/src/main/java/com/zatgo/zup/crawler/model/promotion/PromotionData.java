package com.zatgo.zup.crawler.model.promotion;


import com.zatgo.zup.crawler.model.BaseData;

/**
 * 促销信息
 * Created by chen on 2019/3/11.
 */
public class PromotionData extends BaseData {

    private String fromCity;
    private String toCity;
    private String refPrice;
    private String fromDate;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getRefPrice() {
        return refPrice;
    }

    public void setRefPrice(String refPrice) {
        this.refPrice = refPrice;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }
}
