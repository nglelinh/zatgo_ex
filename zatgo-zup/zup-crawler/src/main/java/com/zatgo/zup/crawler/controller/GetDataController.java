package com.zatgo.zup.crawler.controller;

import com.zatgo.zup.crawler.model.JsEngineCommonRequest;
import com.zatgo.zup.crawler.model.promotion.PageModel;
import com.zatgo.zup.crawler.spider.mafengwo.MaFengWoSpider;
import com.zatgo.zup.crawler.utils.phantomjs.PhantomjsTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;

/**
 * Created by 46041 on 2019/5/13.
 */


@RestController
@RequestMapping("/get")
public class GetDataController {

    private static final Logger logger = LoggerFactory.getLogger(MaFengWoSpider.class);

    @Autowired
    private PhantomjsTool phantomjsTool;


    @PostMapping("/common/jsEngine")
    public void saveData(@RequestBody PageModel pageModel){
        try{
            File file = new File(ResourceUtils.getURL("classpath:").getPath());
            String path = file.getPath();
            String res = phantomjsTool.getParseredHtml(path + File.separatorChar + "phantomjs"
                    + File.separatorChar + "airchina" + File.separatorChar + "common.js", pageModel);
        } catch (Exception e){
            logger.error("", e);
        }
    }
}
