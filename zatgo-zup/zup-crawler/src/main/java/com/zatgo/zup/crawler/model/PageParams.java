package com.zatgo.zup.crawler.model;

/**
 * Created by chen on 2018/11/7.
 */
public class PageParams extends BaseParams{

    private int page=1;

    private int pageSize=20;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
