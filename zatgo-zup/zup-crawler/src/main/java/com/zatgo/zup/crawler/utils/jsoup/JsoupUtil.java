package com.zatgo.zup.crawler.utils.jsoup;

import com.zatgo.zup.common.utils.Base64;
import com.zatgo.zup.crawler.proxy.ProxyService;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 46041 on 2019/4/17.
 */
public class JsoupUtil {

    private static final Map<String, String> header = new HashMap<>();

    private static final Logger logger = LoggerFactory.getLogger(JsoupUtil.class);

    @Autowired
    private ProxyService proxyService;

    static {
        header.put("Host", "https://you.ctrip.com/");
        header.put("User-Agent", "  Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36");
        header.put("Accept", "  text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        header.put("Accept-Language", "zh-cn,zh;q=0.5");
        header.put("Accept-Charset", "  GB2312,utf-8;q=0.7,*;q=0.7");
        header.put("Connection", "keep-alive");
        header.put("authority", "you.ctrip.com");
        header.put("accept-encoding", "gzip, deflate, br");
        header.put("cache-control", "no-cache");
        header.put("pragma", "no-cache");
//        header.put("Cookie", "mfw_uuid=5cbe755a-62e0-3e04-2817-33b1c946ca93; _r=baidu; _rp=a%3A2%3A%7Bs%3A1%3A%22p%22%3Bs%3A18%3A%22www.baidu.com%2Flink%22%3Bs%3A1%3A%22t%22%3Bi%3A1555985754%3B%7D; oad_n=a%3A5%3A%7Bs%3A5%3A%22refer%22%3Bs%3A21%3A%22https%3A%2F%2Fwww.baidu.com%22%3Bs%3A2%3A%22hp%22%3Bs%3A13%3A%22www.baidu.com%22%3Bs%3A3%3A%22oid%22%3Bi%3A1026%3Bs%3A2%3A%22dm%22%3Bs%3A15%3A%22www.mafengwo.cn%22%3Bs%3A2%3A%22ft%22%3Bs%3A19%3A%222019-04-23+10%3A15%3A54%22%3B%7D; __mfwothchid=referrer%7Cwww.baidu.com; uva=s%3A264%3A%22a%3A4%3A%7Bs%3A13%3A%22host_pre_time%22%3Bs%3A10%3A%222019-04-23%22%3Bs%3A2%3A%22lt%22%3Bi%3A1555985757%3Bs%3A10%3A%22last_refer%22%3Bs%3A137%3A%22https%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3D918rAC0URaaPzy_Ix-kCajZcDS5LGWwUO2WyrAvan6Z3EK5jycLfPwQ-MiM1F2pR%26wd%3D%26eqid%3D9525ec700000c28a000000065cbe7558%22%3Bs%3A5%3A%22rhost%22%3Bs%3A13%3A%22www.baidu.com%22%3B%7D%22%3B; __mfwurd=a%3A3%3A%7Bs%3A6%3A%22f_time%22%3Bi%3A1555985757%3Bs%3A9%3A%22f_rdomain%22%3Bs%3A13%3A%22www.baidu.com%22%3Bs%3A6%3A%22f_host%22%3Bs%3A3%3A%22www%22%3B%7D; __mfwuuid=5cbe755a-62e0-3e04-2817-33b1c946ca93; UM_distinctid=16a47fac29163-0c254aeb1557b3-e323069-1fa400-16a47fac2927fe; PHPSESSID=kffbnhr4m2quh6d2ac2olfmh44; __mfwlv=1556442584; __mfwvn=2; CNZZDATA30065558=cnzz_eid%3D481737055-1555985593-%26ntime%3D1556444593; __mfwlt=1556445298");
//        header.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    }


    /**
     * 固定Ip访问
     * @param url
     * @return
     */
    public static Document get(String url){
        try {
            return Jsoup.connect(url).timeout(50000).headers(header).get();
        } catch (Exception e) {
            if (e instanceof SocketTimeoutException){
                return get(url, 1);
            }
            logger.error("", e);
        }
        return null;
    }

    public static Document get(String url, int i){
        if (i > 3)
            return null;
        try {
            return Jsoup.connect(url).timeout(50000).headers(header).get();
        } catch (Exception e) {
            if (e instanceof SocketTimeoutException){
                logger.info(url + "访问超时，正在第" + i + "次重试");
                return get(url, ++i);
            }
            logger.error("", e);
        }
        return null;
    }

    public static Document post(String url){
        try {
            return Jsoup.connect(url).timeout(50000).headers(header).post();
        } catch (Exception e) {
            if (e instanceof SocketTimeoutException){
                return get(url, 1);
            }
            logger.error("", e);
        }
        return null;
    }

    public static String httpGet(String url){
        CloseableHttpClient httpCilent = HttpClients.createDefault();
        try {
            HttpGet httpGet = new HttpGet(url);
            return EntityUtils.toString(httpCilent.execute(httpGet).getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpCilent.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void main(String[] args) {
        String url = "https://restapi.amap.com/v3/geocode/geo?address=老北京动起来展馆&city=北京市朝阳区天辰东路7号 ，国家会议中心B4入口&key=9caa771d882f4609e9609208f08d8b0b";
        url = url.replaceAll(" ", "");
        String s = httpGet(url);
        System.out.println(s);
    }

    public static String downloadImage(String url){
        try {
            Connection.Response execute = Jsoup.connect(url).timeout(50000).headers(header).ignoreContentType(true).execute();
            byte[] bytes = execute.bodyAsBytes();
            return Base64.encode(bytes);
        } catch (IOException e) {
            logger.error("", e);
        }
        return null;
    }

    /**
     * 随机IP访问
     * @return
     */
    public Document randomIpGet(String url){
        proxyService.updateProxyIp();
        return get(url);
    }

}
