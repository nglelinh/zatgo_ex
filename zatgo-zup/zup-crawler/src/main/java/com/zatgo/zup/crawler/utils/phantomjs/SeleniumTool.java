package com.zatgo.zup.crawler.utils.phantomjs;

import com.zatgo.zup.crawler.proxy.ProxyService;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Selenium+PhantomJs 的方式调用
 * Created by chen on 2019/3/12.
 */
//@Component
public class SeleniumTool {

    public Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${phantomjs.exepath}")
    public String exePath;

    public DesiredCapabilities dcaps;
    public PhantomJSDriver driver;

    @Resource
    private ProxyService proxyService;

    public SeleniumTool(){
        //设置必要参数
        dcaps = new DesiredCapabilities();
        //驱动支持
        dcaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, exePath);

        initDriver();

    }

    public void initDriver(){
        //ssl证书支持
        dcaps.setCapability("acceptSslCerts", true);
        //截屏支持
        dcaps.setCapability("takesScreenshot", true);
        //css搜索支持
        dcaps.setCapability("cssSelectorsEnabled", true);
        //js支持
        dcaps.setJavascriptEnabled(true);
        dcaps.setCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
        dcaps.setCapability("phantomjs.page.settings.accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        dcaps.setCapability("phantomjs.page.settings.acceptEncoding", "gzip, deflate");
        dcaps.setCapability("phantomjs.page.settings.acceptLanguage", "zh-CN,zh;q=0.9,ja;q=0.8,en;q=0.7");

        dcaps.setCapability("phantomjs.page.customHeaders.User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
        dcaps.setCapability("phantomjs.page.customHeaders.Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        dcaps.setCapability("phantomjs.page.customHeaders.Accept-Encoding", "gzip, deflate");
        dcaps.setCapability("phantomjs.page.customHeaders.Accept-Language", "zh-CN,zh;q=0.9,ja;q=0.8,en;q=0.7");


        initProxy();

        if(driver==null){
            //创建无界面浏览器对象
            driver = new PhantomJSDriver(dcaps);
        }
    }

    private void initProxy() {

        Map<String,String> ipMap = proxyService.getProxyIp();
        String ipAndPort = ipMap.get("ip")+":"+ipMap.get("port");

        Proxy proxy = new Proxy();

        // 配置http、ftp、ssl代理（注：当前版本只支持所有的协议公用http协议，下述代码等同于只配置http）
        proxy.setHttpProxy(ipAndPort)
                .setFtpProxy(ipAndPort)
                .setSslProxy(ipAndPort);

        // 以下三行是为了避免localhost和selenium driver的也使用代理，务必要加，否则无法与iedriver通讯
        dcaps.setCapability(CapabilityType.ForSeleniumServer.AVOIDING_PROXY, true);
        dcaps.setCapability(CapabilityType.ForSeleniumServer.ONLY_PROXYING_SELENIUM_TRAFFIC, true);
        System.setProperty("http.nonProxyHosts", "localhost");

        dcaps.setCapability(CapabilityType.PROXY, proxy);
    }


    public void closeDriver(){
        if(driver!=null){
            driver.close();
            driver.quit();
        }
    }




}
