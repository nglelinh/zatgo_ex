package com.zatgo.zup.crawler.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;


/**
 * 单例的timer 
 * @author hejiang
 *
 */
public class SingleTimerTools {

	private static Logger log = LoggerFactory.getLogger(SingleTimerTools.class);
	
	private SingleTimerTools(){
		
    }  
  
	//单例模式
    private static Timer timer; 
  
    public static Timer getInstance() {  
    	try {
			if(timer == null){
				synchronized(SingleTimerTools.class){
					if(timer == null){
						timer = new Timer();
					}
				}
			}
		} catch (Exception e) {
			log.error("实例timer失败！", e);
		}
        return timer;  
    }  
}
