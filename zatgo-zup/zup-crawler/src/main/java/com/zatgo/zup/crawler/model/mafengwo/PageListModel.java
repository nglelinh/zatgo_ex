package com.zatgo.zup.crawler.model.mafengwo;

/**
 * Created by 46041 on 2019/4/30.
 */
public class PageListModel {

    private String url;

    private String countryName;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
