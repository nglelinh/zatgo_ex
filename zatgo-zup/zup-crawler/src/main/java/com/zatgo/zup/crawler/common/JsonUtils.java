package com.zatgo.zup.crawler.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.util.Scanner;

/**
 * Created by chen on 2019/3/11.
 */
public class JsonUtils {

    private static Logger logger = LoggerFactory.getLogger(JsonUtils.class);

    public static String readJsonFromClassPath(String path){
        try{
            ClassPathResource resource = new ClassPathResource(path);
            if (resource.exists()) {
                File file = resource.getFile();
                String jsonData = jsonRead(file);
                return jsonData;
            } else {
                logger.error("JsonUtils read error , path: "+path);
                return null;
            }
        }catch (Exception e){
            logger.error("JsonUtils read error , path: "+path);
            return null;
        }
    }

    private static String jsonRead(File file){
        Scanner scanner = null;
        StringBuilder buffer = new StringBuilder();
        try {
            scanner = new Scanner(file, "utf-8");
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine());
            }
        } catch (Exception e) {

        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return buffer.toString();
    }
}
