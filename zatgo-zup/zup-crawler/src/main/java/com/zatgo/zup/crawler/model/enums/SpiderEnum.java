package com.zatgo.zup.crawler.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 46041 on 2019/4/28.
 */
public enum SpiderEnum {

    ALIASTYPE("aliasType"),DEFAULTTYPE("defaultType"),JSENGINE("jsEngine");

    private String code;

    private static final Map<String, SpiderEnum> stringToEnum = new HashMap<String, SpiderEnum>();
    static {
        for(SpiderEnum enumType : values()) {
            stringToEnum.put(enumType.getCode(), enumType);
        }
    }

    public static SpiderEnum getEnumByType(String type) {
        return stringToEnum.get(type);
    }

    private SpiderEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
