package com.zatgo.zup.crawler.js;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by chen on 2019/3/11.
 */
public class JavaScriptEngine {

    private static Logger logger = LoggerFactory.getLogger(JavaScriptEngine.class);

    public static String invoke(String jsFileName,String functionName,String... params){
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        FileReader reader = null;
        try {
            ClassPathResource resource = new ClassPathResource(jsFileName);
            reader = new FileReader(resource.getFile());
            engine.eval(reader);
            if(engine instanceof Invocable) {
                Invocable invoke = (Invocable)engine;
                Object ret = invoke.invokeFunction(functionName, params);
                logger.info("Js invoke result: "+ret);
            }
        } catch (FileNotFoundException e) {
            logger.error("Cannot find js file with name "+jsFileName,e);
        } catch (NoSuchMethodException e) {
            logger.error("Cannot find js function with name "+functionName,e);
        } catch (IOException e) {
            logger.error("Js error with name "+jsFileName,e);
        } catch (ScriptException e) {
            logger.error("Js error with name "+jsFileName,e);
        } finally {
            if(reader!=null)
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("Js reader close error: "+jsFileName,e);
                }
        }
        return null;
    }

    public static void main(String[] args){
        JavaScriptEngine.invoke("phantomjs/airchina/ca_airchina.js","run");
    }
}
