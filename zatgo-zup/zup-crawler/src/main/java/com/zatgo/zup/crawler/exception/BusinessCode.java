package com.zatgo.zup.crawler.exception;

import java.util.HashMap;

/**
 * Created by chen on 2018/11/6.
 */
public class BusinessCode {

    public static final String SUCCESS = "0000";

    /*********错误代码***************/
    public static final String SYSTEM_ERROR = "9000";
    public static final String PARAM_NOT_NULL = "9001";
    public static final String OBJECT_NOT_FOUND = "9002";
    public static final String ACCOUNT_LOGIN_ERROR = "9003";
    public static final String TOKEN_ERROR = "9004";
    public static final String SYSTEM_SERVICE_TIMEOUT = "9005";

    public static final HashMap<String,String> SYS_MESSAGE = new HashMap<String,String>(){
        {
            put(SUCCESS,"SUCCESS");
            put(SYSTEM_ERROR,"SYSTEM ERROR");
            put(PARAM_NOT_NULL,"PARAM NOT NULL");
            put(OBJECT_NOT_FOUND,"OBJECT NOT FOUND");
            put(ACCOUNT_LOGIN_ERROR,"ERROR USERNAME OR PASSWORD");
            put(TOKEN_ERROR,"ERROR TOKEN");
            put(SYSTEM_SERVICE_TIMEOUT,"SYSTEM SERVICE TIMEOUT");
        }
    };

    public static String getMessage(String code){
        return SYS_MESSAGE.get(code);
    }


}
