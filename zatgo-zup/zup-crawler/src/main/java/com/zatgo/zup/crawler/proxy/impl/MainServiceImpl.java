package com.zatgo.zup.crawler.proxy.impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.crawler.SpiderMain;
import com.zatgo.zup.crawler.model.mafengwo.PageListModel;
import com.zatgo.zup.crawler.model.promotion.PageModel;
import com.zatgo.zup.crawler.model.xiecheng.SpiderParam;
import com.zatgo.zup.crawler.proxy.MainService;
import com.zatgo.zup.crawler.remoteService.TourismSearchRemoteService;
import com.zatgo.zup.crawler.utils.phantomjs.PhantomjsTool;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by 46041 on 2019/4/28.
 */

@Component
public class MainServiceImpl implements MainService {

    private static final Logger logger = LoggerFactory.getLogger(SpiderMain.class);

    @Autowired
    private TourismSearchRemoteService tourismSearchRemoteService;
    @Autowired
    private OkHttpService okHttpService;
    @Autowired
    private PhantomjsTool phantomjsTool;
    @Value("${phantomjs.exepath}")
    private String exePath;


    /**
     * 默认模式  即时抓取顺序调用start方法和getInfo方法 每次调用间隔1秒
     * @param o
     */
    @Async
    @Override
    public void defaultStart(Object o) {
        while (true){
            try {
                Class<?> claxx = o.getClass();
                Method start = claxx.getMethod("start");
                String url = (String) start.invoke(o);
                Method getInfo = claxx.getMethod("getInfo", String.class);
                Object res = getInfo.invoke(o, url);
                if (res != null){
                    SaveDataRequest request = JSONObject.parseObject(JSONObject.toJSONString(res), SaveDataRequest.class);
                    tourismSearchRemoteService.saveData(request);
                }
                Thread.sleep(1000);
            } catch (Exception e){
                logger.error("", e);
            }
        }
    }


    /**
     * 自定义模式
     * 需要自行存储数据
     * @param o
     */
    @Async
    @Override
    public void aliasStart(Object o) {
        try {
            Class<?> claxx = o.getClass();
            Method start = claxx.getMethod("aliasStart");
            start.invoke(o);
        } catch (Exception e){
            logger.error("", e);
        }
    }




    /**
     * js引擎模式
     * 需要自行存储数据
     * @param o
     */
    @Async
    @Override
    public void jsEngineStart(Object o) {
        try {
            Class<?> claxx = o.getClass();
            Method start = claxx.getMethod("jsEngineStart");
            Method getInfo = claxx.getMethod("jsEngineGetInfo", String.class);
            Method callBack = claxx.getMethod("jsEngineCallBack", String.class, String.class);
            List<String> urls = (List) start.invoke(o);
            for (String url : urls){
                try{
                    PageModel pageModel = (PageModel) getInfo.invoke(o, url);
                    File file = new File(ResourceUtils.getURL("classpath:").getPath());
                    String path = file.getPath();
                    String res = phantomjsTool.getParseredHtml(path + File.separatorChar + "phantomjs"
                            + File.separatorChar + "airchina" + File.separatorChar + "common.js", pageModel);
                    callBack.invoke(o, res, url);
                } catch (Exception e){
                    logger.error("", e);
                }
            }
        } catch (Exception e){
            logger.error("", e);
        }
    }
}
