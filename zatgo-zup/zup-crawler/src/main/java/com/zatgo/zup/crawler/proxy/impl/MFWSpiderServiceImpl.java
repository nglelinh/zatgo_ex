package com.zatgo.zup.crawler.proxy.impl;

import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.crawler.model.mafengwo.PageListModel;
import com.zatgo.zup.crawler.model.xiecheng.SpiderParam;
import com.zatgo.zup.crawler.proxy.MFWSpiderService;
import com.zatgo.zup.crawler.utils.jsoup.JsoupUtil;
import com.zatgo.zup.crawler.utils.phantomjs.PhantomjsTool;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 46041 on 2019/4/30.
 */

@Component
public class MFWSpiderServiceImpl implements MFWSpiderService{

    private static final Long cacheSaveTime = 30l;

    private static final String firstUrl = "http://www.mafengwo.cn/mdd/";

    private static final String secondUrl = "http://www.mafengwo.cn";

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private PhantomjsTool phantomjsTool;
    @Autowired
    private RedisUtils redisUtils;


    @Async
    @Override
    public void getPageList(){
        Long countryNumber = redisTemplate.opsForValue().increment(SpiderParam.mfwCountryKey, 0l);
        redisTemplate.expire(SpiderParam.cityKey, cacheSaveTime, TimeUnit.DAYS);
        Document document = JsoupUtil.get(firstUrl);
        //拿到国家列表
        Elements countryList = document.getElementsByClass("bd");
        Element country = countryList.get(countryNumber.intValue()).getElementsByTag("a").get(0);
        //国家名字
        String countryName = country.html().replaceFirst("<span.+</span>", "").trim();
        //获取国家的url
        String countryUrl = country.attr("href");
        Document secondUrlPage = JsoupUtil.get(secondUrl + countryUrl);
        String attr1 = secondUrlPage.getElementsByClass("crumb").get(0).getElementsByClass("clearfix").get(0).getElementsByTag("a").get(0).attr("href");
        String scenicListPage = phantomjsTool.getParseredHtml(secondUrl + attr1, "C:\\Users\\46041\\Desktop\\update\\zatgo-zup\\zatgo-zup\\zup-crawler\\src\\main\\resources\\phantomjs\\airchina\\mafengwo_routes_promotion.js");
    }


}
