package com.zatgo.zup.crawler.spider;

import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.crawler.model.promotion.PageModel;

import java.util.List;

/**
 * Created by 46041 on 2019/4/28.
 */
public interface SpiderInterface {

    /**
     * 获取目标页面前的所有步骤
     * @return
     */
    String defaultStart();

    /**
     * 获取页面数据，返回结果
     * @param url
     * @return
     */
    SaveDataRequest defaultGetInfo(String url);

    /**
     * 初始化参数
     */
    void init();

    /**
     * 自定义爬虫，自己调用存储
     */
    void aliasStart();

    /**
     * js引擎
     * @return
     */
    List<String> jsEngineStart();

    /**
     * js引擎获取页面数据
     * @return
     */
    PageModel jsEngineGetInfo(String url);

    /**
     * js引擎回调
     * @return
     */
    void jsEngineCallBack(String pageInfo, String url);
}
