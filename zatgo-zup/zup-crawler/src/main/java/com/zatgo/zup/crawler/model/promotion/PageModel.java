package com.zatgo.zup.crawler.model.promotion;

import java.util.List;

/**
 * Created by 46041 on 2019/5/5.
 */

public class PageModel {

    /**
     * 第一个页面必须要有url  必传
     */
    private String firstUrl;
    /**
     * 获取数据前的操作  可为空
     */
    private List<ClickModel> prefixClick;
    /**
     * 数据返回接口地址  必传
     */
    private String noticeUrl;
    /**
     * 调用次数  可为空
     */
    private Integer callTheNumber = 1;
    /**
     * 每个操作后的等待时间 单位毫秒
     */
    private Long waitTime = 2000l;

//=========================一下会被重复操作==================

    /**
     * 获取数据  可为空
     */
    private List<ClickModel> getData;
    /**
     * 翻页   可为空
     */
    private List<ClickModel> toNext;







    public String getFirstUrl() {
        return firstUrl;
    }

    public void setFirstUrl(String firstUrl) {
        this.firstUrl = firstUrl;
    }

    public List<ClickModel> getPrefixClick() {
        return prefixClick;
    }

    public void setPrefixClick(List<ClickModel> prefixClick) {
        this.prefixClick = prefixClick;
    }

    public String getNoticeUrl() {
        return noticeUrl;
    }

    public void setNoticeUrl(String noticeUrl) {
        this.noticeUrl = noticeUrl;
    }

    public Integer getCallTheNumber() {
        return callTheNumber;
    }

    public void setCallTheNumber(Integer callTheNumber) {
        this.callTheNumber = callTheNumber;
    }

    public List<ClickModel> getGetData() {
        return getData;
    }

    public void setGetData(List<ClickModel> getData) {
        this.getData = getData;
    }

    public List<ClickModel> getToNext() {
        return toNext;
    }

    public void setToNext(List<ClickModel> toNext) {
        this.toNext = toNext;
    }

    public Long getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(Long waitTime) {
        this.waitTime = waitTime;
    }
}
