package com.zatgo.zup.crawler.proxy;

/**
 * Created by 46041 on 2019/4/28.
 */
public interface MainService {

    void defaultStart(Object o);

    void aliasStart(Object o);

    void jsEngineStart(Object o);
}
