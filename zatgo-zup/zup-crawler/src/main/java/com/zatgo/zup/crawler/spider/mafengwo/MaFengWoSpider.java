package com.zatgo.zup.crawler.spider.mafengwo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.crawler.annotation.Spider;
import com.zatgo.zup.crawler.model.enums.SpiderEnum;
import com.zatgo.zup.crawler.model.promotion.ClickModel;
import com.zatgo.zup.crawler.model.promotion.PageModel;
import com.zatgo.zup.crawler.model.xiecheng.SpiderParam;
import com.zatgo.zup.crawler.proxy.MFWSpiderService;
import com.zatgo.zup.crawler.spider.SpiderInterface;
import com.zatgo.zup.crawler.utils.jsoup.JsoupUtil;
import com.zatgo.zup.crawler.utils.phantomjs.PhantomjsTool;
import org.apache.http.client.HttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by 46041 on 2019/4/28.
 */

@Spider(value = SpiderEnum.JSENGINE ,isOpen = true)
public class MaFengWoSpider implements SpiderInterface {

    private static final Logger logger = LoggerFactory.getLogger(MaFengWoSpider.class);

    private static final Long cacheSaveTime = 30l;

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private PhantomjsTool phantomjsTool;
    @Autowired
    private MFWSpiderService mfwSpiderService;
    @Autowired
    private OkHttpService okHttpService;


    @Override
    public String defaultStart() {
        return null;
    }

    @Override
    public SaveDataRequest defaultGetInfo(String url) {
        return null;
    }

    @Override
    public void init() {
//        redisTemplate.opsForValue().increment(SpiderParam.mfwCountryKey, 0l);
//        redisTemplate.expire(SpiderParam.cityKey, cacheSaveTime, TimeUnit.DAYS);
    }

    @Override
    public void aliasStart() {
//        mfwSpiderService.getPageList();


        String s = okHttpService.doGet("https://m.mafengwo.cn/jd/11698/gonglve.html?page=2&is_ajax=1");
        System.out.println(s);


//        Element element = detail.getElementsByClass("scenic-list").get(0);
//        String attr2 = element.getElementsByTag("a").get(0).attr("href");
//        Document document1 = JsoupUtil.get(secondUrl + attr2);
//        String countryName = country.html().replaceFirst("<span.+</span>", "").trim();
//        String imgUrl = document1.getElementsByClass("pic-big").get(0).getElementsByTag("img").attr("src");
//        String name = document1.getElementsByClass("title").get(0).getElementsByTag("h1").html();
//
//        System.out.println(countryName);
//        System.out.println(imgUrl);
//        System.out.println(name);

    }

    @Override
    public List<String> jsEngineStart() {
        return getUrlList();
    }

    @Override
    public PageModel jsEngineGetInfo(String url) {
        PageModel pageModel = new PageModel();
        //初始页面
        pageModel.setFirstUrl(url);
        //翻3页
        pageModel.setCallTheNumber(2);
        //设置操作等待时间
        pageModel.setWaitTime(2000l);
        //获取数据前置操作
        List<ClickModel> prefixClickList = new ArrayList<>();
        ClickModel prefixClick = new ClickModel();
//        prefixClick.setSelector(Arrays.asList(".navbar-sub", "a"));
//        prefixClick.setIndex(Arrays.asList(0, 1));
        prefixClick.setSelector(Arrays.asList(".navbar-sub", "a[data-cs-p=景点]"));
        prefixClick.setIndex(Arrays.asList(0, 0));
        prefixClickList.add(prefixClick);
        pageModel.setPrefixClick(prefixClickList);
        //获取数据操作,如果不需要，则空着


        //获取完数据翻页
        List<ClickModel> toNextClickList = new ArrayList<>();
        ClickModel toNext = new ClickModel();
        toNext.setSelector(Arrays.asList(".pg-next"));
        toNext.setIndex(Arrays.asList(0));
        toNextClickList.add(toNext);
        pageModel.setToNext(toNextClickList);
        //数据回调地址
        pageModel.setNoticeUrl("http://127.0.0.1:8100/receive/data");
        return pageModel;
    }

    @Override
    public void jsEngineCallBack(String pageInfo, String url) {
        String country = redisUtils.get(SpiderParam.pageListKey + url, String.class);
        Document detail = Jsoup.parse(pageInfo);
        Elements scenicList = detail.getElementsByClass("scenic-list");
        for (Element scenic : scenicList){
            Elements a = scenic.getElementsByTag("a");
            for (Element e : a){
                String title = e.attr("title");
                redisUtils.put(SpiderParam.mfwcountryCityKey + title, country, 30l, TimeUnit.DAYS);
                System.out.println("===============>>>>  " + country + ":" + title);
            }
        }
    }


    private List<String> getUrlList(){
        List<String> arr = new ArrayList<>();
        Set<String> keys = redisUtils.getKeys(SpiderParam.pageListKey + "*");
        if (keys != null && !keys.isEmpty()){
            keys.forEach(url -> {
                arr.add(url.replace(SpiderParam.pageListKey, ""));
            });
        } else {
            Document document = JsoupUtil.get("http://www.mafengwo.cn/mdd/");
            Element element = document.getElementsByClass("row-list").get(0);
            Elements a = element.getElementsByTag("a");
            for (int i = 1; i < a.size(); i++) {
                Element e = a.get(i);
                String url = "http://www.mafengwo.cn" + e.attr("href");
                String country = e.html().replaceFirst("<span.+</span>", "").trim();
                arr.add(url);
                redisUtils.put(SpiderParam.pageListKey +url, country);
            }
        }
        return arr;
    }


    public static void main(String[] args) {
//        Document document = JsoupUtil.get("http://www.mafengwo.cn/mdd/");
//        Element element = document.getElementsByClass("row-list").get(0);
//        Elements a = element.getElementsByTag("a");
//        for (Element e : a){
//            System.out.println("http://www.mafengwo.cn/" + e.attr("href"));
//            System.out.println(e.html().replaceFirst("<span.+</span>", "").trim());
//        }


//
//        Elements bd = document.getElementsByClass("bd");
//        String attr = bd.get(0).getElementsByTag("a").get(0).attr("href");
//        Document secondUrlPage = JsoupUtil.get(secondUrl + attr);
//        String attr1 = secondUrlPage.getElementsByClass("crumb").get(0).getElementsByClass("clearfix").get(0).getElementsByTag("a").get(0).attr("href");
//        Document detail = JsoupUtil.get(secondUrl + attr1);
//        Element element = detail.getElementsByClass("scenic-list").get(0);
//        String attr2 = element.getElementsByTag("a").get(0).attr("href");
//        System.out.println(JsoupUtil.get(secondUrl + attr2));


//        System.out.println(JsoupUtil.get("http://www.mafengwo.cn/poi/6933436.html"));
    }

}
