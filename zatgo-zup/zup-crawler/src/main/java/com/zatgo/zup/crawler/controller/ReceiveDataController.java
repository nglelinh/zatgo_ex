package com.zatgo.zup.crawler.controller;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.model.HbaseConstant;
import com.zatgo.zup.common.model.SaveDataRequest;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.crawler.model.xiecheng.ScenicSpot;
import com.zatgo.zup.crawler.model.xiecheng.SpiderParam;
import com.zatgo.zup.crawler.remoteService.TourismSearchRemoteService;
import com.zatgo.zup.crawler.utils.jsoup.JsoupUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 46041 on 2019/4/30.
 */

@RestController
@RequestMapping("/receive")
public class ReceiveDataController {

    private static final Logger logger = LoggerFactory.getLogger(ReceiveDataController.class);

    private static final Long cacheSaveTime = 30l;

    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private TourismSearchRemoteService tourismSearchRemoteService;


    @PostMapping("/data")
    public void saveData(@RequestBody String scenicListPage){

        Document detail = Jsoup.parse(scenicListPage);
        Elements scenicList = detail.getElementsByClass("scenic-list");
        Element list = scenicList.get(0);
        Elements a = list.getElementsByTag("a");
        for (Element element : a){
            try {
                ScenicSpot ss = new ScenicSpot();
                String title = element.attr("title");
                ss.setName(title);
                String url = "http://www.mafengwo.cn" + element.attr("href");
                Document document = JsoupUtil.get(url);
                String introduction = null;
                try{
                    introduction = document.getElementsByClass("summary").get(0).html();
                    ss.setIntroduction(introduction);
                } catch (Exception e){
                    logger.error("", e);
                }
                String imgurl = null;
                try{
                    imgurl = document.getElementsByClass("pic-big").get(0).getElementsByTag("img").attr("src");
                    ss.setImgs(JsoupUtil.downloadImage(imgurl));
                } catch (Exception e){
                    logger.error("", e);
                }
                ss.setTimeStamp(SpiderParam.getTimeStamp());
                ss.setCountry(redisUtils.get(SpiderParam.mfwcountryCityKey + title, String.class));

                try{
                    SaveDataRequest request = new SaveDataRequest();
                    request.setData(JSONObject.parseObject(JSONObject.toJSONString(ss)));
                    request.setFamilyName(HbaseConstant.ScenicSpotFamilyName);
                    String rowKey = ss.getCountry() + ss.getName();
                    request.setRowKey(rowKey);
                    request.setTableName(HbaseConstant.ScenicSpotTableName);
                    tourismSearchRemoteService.saveData(request);
                } catch (Exception e){
                    logger.error("", e);
                }
            } catch (Exception e){
                logger.error("", e);
            }
        }

    }

    private Long getPage(String source){
        String reg = "<span>\\d+</span>";
        Matcher m = Pattern.compile(reg).matcher(source);
        String page = "";
        while (m.find()) {
            page = m.group(0);
            page = page.replace("<span>", "");
            page = page.replace("</span>", "");
            break;
        }
        return Long.valueOf(page);
    }
}
