package com.zatgo.zup.register;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ZupRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZupRegisterApplication.class, args);
	}
}
