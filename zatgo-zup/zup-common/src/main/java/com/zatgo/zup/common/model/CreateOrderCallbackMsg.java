package com.zatgo.zup.common.model;

import java.io.Serializable;

/**
 * Created by 46041 on 2018/9/25.
 */
public class CreateOrderCallbackMsg<T> implements Serializable {

    private Integer count;

    private T result;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
