package com.zatgo.zup.common.sms.santi;

/**
 * 三体发送短信请求参数
 * @author hejiang
 *
 */
public class SantiSmsParams {

	private String appId;//	必填参数。应用ID号
	private String modeId;//	必填参数。模板ID，需后台审核通过，发送内容加企业签名和变量内容必须小于670个字符。
	private String vars;//	选填参数。若modeId有变量内容则必填。变量内容：“123|十”英文竖线分割，内容必须小于670个字符。 UTF-8编码，若vars包含超链接或者特殊字符，则需要对vars做urlencode编码
	private String mobile;//	必填参数。11位标准手机号码。若为多个号码用英文逗号分隔，最多支持500个手机号。
	private String sendTime;//	选填参数。发送时间。若不传，视为立即发送；若传值，则会在sendTime表示的时间点开始发送。格式为：yyyyMMddHHmmss如：20161123143022
	private String notifyUrl;//	选填参数。状态通知地址。若传值，则按照传入notifyUrl做状态通知。需作Urlencode后传入
	private String sign;//	必填参数。用户签名。(appKey+appId+ mobile)生成MD5转小写字母，appKey在平台获取。
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getModeId() {
		return modeId;
	}
	public void setModeId(String modeId) {
		this.modeId = modeId;
	}
	public String getVars() {
		return vars;
	}
	public void setVars(String vars) {
		this.vars = vars;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getSendTime() {
		return sendTime;
	}
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
}
