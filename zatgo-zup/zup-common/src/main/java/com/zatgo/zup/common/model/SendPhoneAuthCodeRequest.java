package com.zatgo.zup.common.model;


import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

/**
 * Created by 46041 on 2018/12/28.
 */
public class SendPhoneAuthCodeRequest {

    @ApiModelProperty(value="云用户ID",required=true)
    private String cloudUserId;

    @ApiModelProperty(value="手机号码",required=true)
    private String mobilePhone;

    @ApiModelProperty(value="阿里云返回数据",required=true)
    private AliyunValidateCode aliyunValidateCode;


    public String getCloudUserId() {
        if (StringUtils.isEmpty(cloudUserId)){
            cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
        }
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public AliyunValidateCode getAliyunValidateCode() {
        return aliyunValidateCode;
    }

    public void setAliyunValidateCode(AliyunValidateCode aliyunValidateCode) {
        this.aliyunValidateCode = aliyunValidateCode;
    }
}
