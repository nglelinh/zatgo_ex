package com.zatgo.zup.common.mq;



import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.exception.BusinessException;


@Component
public class MQProducer {
	private static final Logger logger = LoggerFactory.getLogger(MQProducer.class);
	
	@Value("${mq.type:rocketmq}")
	private String mqType;
	
	private static final String MQ_TYPE_ROCKETMQ = "rocketmq";
	
	private static final String MQ_TYPE_ALIYUNMQ = "aliyunmq";
	
	@Resource(name="RocketMQServiceImpl")
	private MQService rocketMQService;
	
	@Resource(name="AliYunMQServiceImpl")
	private MQService aliyunMQService;
	
	

	private MQService getMQService() {
		if(mqType.equals(MQ_TYPE_ROCKETMQ)) {
			return rocketMQService;
		}else if(mqType.equals(MQ_TYPE_ALIYUNMQ)) {
			return aliyunMQService;
		}else {
			throw new BusinessException();
		}
	}
	
	public void send(String topic, String tags, String json,String key){
		try {
			getMQService().sendMQmessage(topic, tags, json, key);
		} catch (UnsupportedEncodingException e) {
			logger.error("key=" +key + ":" + json,e);
		}
	}

	public void sendDelay(String topic, String tags, String json,String key,Long delay){
		try {
			getMQService().sendMQDelayMessage(topic, tags, json, key, delay);
		} catch (Exception e) {
			logger.error("key=" +key + ":" + json,e);
		}
	}

	public void sendOrderMessage(String topic, String tags, String json,String key,String shardingkey){
		try {
			getMQService().sendMQSeqMessage(topic, tags, json, key, shardingkey);
		} catch (Exception e) {
			logger.error("key=" +key + ":" + json,e);
		}
	}

}
