package com.zatgo.zup.common.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolUtils {

	private static final ExecutorService threadPool = Executors.newCachedThreadPool();

	private static final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(10);
	public static ExecutorService getThreadPool(){
		return threadPool;
	}

	public static ExecutorService getFixedThreadPool(){
		return fixedThreadPool;
	}
}
