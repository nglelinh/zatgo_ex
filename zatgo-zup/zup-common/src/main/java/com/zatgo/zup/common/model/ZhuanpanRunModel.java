package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "大转盘启动返回对象")
public class ZhuanpanRunModel {

    @ApiModelProperty(value="中奖结果，0-未中奖、1-中奖")
    private String result;

    @ApiModelProperty(value="奖品")
    private GamePrizeModel prize;

    @ApiModelProperty(value="今日剩余免费次数")
    private long todayFreeTimes;

    @ApiModelProperty(value="祝福语")
    private String blessings;

    @ApiModelProperty(value="任务记录ID")
    private String recordId;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public GamePrizeModel getPrize() {
        return prize;
    }

    public void setPrize(GamePrizeModel prize) {
        this.prize = prize;
    }

    public long getTodayFreeTimes() {
        return todayFreeTimes;
    }

    public void setTodayFreeTimes(long todayFreeTimes) {
        this.todayFreeTimes = todayFreeTimes;
    }

    public String getBlessings() {
        return blessings;
    }

    public void setBlessings(String blessings) {
        this.blessings = blessings;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }
}
