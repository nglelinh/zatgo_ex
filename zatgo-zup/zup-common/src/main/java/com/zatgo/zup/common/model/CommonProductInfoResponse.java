package com.zatgo.zup.common.model;

import com.ykb.mall.common.model.PmsCommonProductSku;
import com.ykb.mall.common.model.PmsProduct;

import java.util.List;

/**
 * Created by 46041 on 2019/7/17.
 */
public class CommonProductInfoResponse {

    private PmsProduct pmsProduct;

    private List<PmsCommonProductSku> list;

    public PmsProduct getPmsProduct() {
        return pmsProduct;
    }

    public void setPmsProduct(PmsProduct pmsProduct) {
        this.pmsProduct = pmsProduct;
    }

    public List<PmsCommonProductSku> getList() {
        return list;
    }

    public void setList(List<PmsCommonProductSku> list) {
        this.list = list;
    }
}
