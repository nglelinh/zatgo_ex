package com.zatgo.zup.common.enumtype;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class BusinessEnum {


	public enum ExtractStatus{

		confirming("0"),confirmed("1"),approvaling("2"),approvaled("3"),abnormal("4"),approvalfail("5");

		private String code;
		
		private static final Map<String, ExtractStatus> stringToEnum = new HashMap<String, ExtractStatus>();
	    static {
	        for(ExtractStatus enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static ExtractStatus getEnumByType(String type) {
	    	return stringToEnum.get(type);
	    }

		private ExtractStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
	}
	
	public enum DepositStatus{

		confirming("0"),confirmed("1"),fail("2");

		private String code;
		
		private static final Map<String, DepositStatus> stringToEnum = new HashMap<String, DepositStatus>();
	    static {
	        for(DepositStatus enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static DepositStatus getEnumByType(String type) {
	    	return stringToEnum.get(type);
	    }

		private DepositStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
	}
	
	public enum PaymentRecordType{

		orderPay(0),transfer(1),extract(2),deposit(3),exchange(4),orderRefund(5),exchangeLock(6),exchangeUnLock(7),
		present(8),mining(9),exchangeFee(10),task(11),gamePay(12),extractTranfer(13),
		fixedDepositSubsc(14),fixedDepositSubscYield(15);

		private Byte code;
		
		private static final Map<Byte, PaymentRecordType> stringToEnum = new HashMap<Byte, PaymentRecordType>();
	    static {
	        for(PaymentRecordType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static PaymentRecordType getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private PaymentRecordType(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum OrderStatus{

		PAYING(0),PAID(1),FAIL(2),REFUND(3), REFUND_FAIL(4),PAY_UNKOWN_ERROR(5),REFUND_UNKOWN_ERROR(6);

		private Byte code;
		
		private static final Map<Byte, OrderStatus> stringToEnum = new HashMap<Byte, OrderStatus>();
	    static {
	        for(OrderStatus enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static OrderStatus getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private OrderStatus(Integer  code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}
	
	public enum OrderType{
		order("0");
		
		private String code;
		
		private static final Map<String, OrderType> stringToEnum = new HashMap<String, OrderType>();
	    static {
	        for(OrderType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static OrderType getEnumByType(String type) {
	    	return stringToEnum.get(type);
	    }

		private OrderType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
	}
	
	public enum AccountType{
		wallet("0");
		
		private String code;
		
		private static final Map<String, AccountType> stringToEnum = new HashMap<String, AccountType>();
	    static {
	        for(AccountType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static AccountType getEnumByType(String type) {
	    	return stringToEnum.get(type);
	    }

		private AccountType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
	}

	public enum ResultCode {
		UNTREATED("UNTREATED"),SUCCESS("SUCCESS"), PROCESSING("PROCESSING"),FAIL("FAIL");

		private String code;
		
		private static final Map<String, ResultCode> stringToEnum = new HashMap<String, ResultCode>();
	    static {
	        for(ResultCode enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static ResultCode getEnumByType(String type) {
	    	return stringToEnum.get(type);
	    }

		private ResultCode(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
	}
	public enum PrimaryAccount{

		TRUE(1),FALSE(0);

		private Byte code;
		
		private static final Map<Byte, PrimaryAccount> stringToEnum = new HashMap<Byte, PrimaryAccount>();
	    static {
	        for(PrimaryAccount enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static PrimaryAccount getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private PrimaryAccount(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}


	public enum ReceiveType{

		transfer(0),deposit(1),refund(2);

		private Byte code;
		
		private static final Map<Byte, ReceiveType> stringToEnum = new HashMap<Byte, ReceiveType>();
	    static {
	        for(ReceiveType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static ReceiveType getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private ReceiveType(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}


	public enum CashPayType{

		WX(0),ALIPAY(1);

		private Byte code;
		
		private static final Map<Byte, CashPayType> stringToEnum = new HashMap<Byte, CashPayType>();
	    static {
	        for(CashPayType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 

	    public static CashPayType getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private CashPayType(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum MultiPay{

		FALSE(0),TRUE(1);

		private Byte code;
		
		private static final Map<Byte, MultiPay> stringToEnum = new HashMap<Byte, MultiPay>();
	    static {
	        for(MultiPay enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static MultiPay getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private MultiPay(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum ThirdPayStatus{

		FAIL(0),SUCCESS(1);

		private Byte code;
		
		private static final Map<Byte, ThirdPayStatus> stringToEnum = new HashMap<Byte, ThirdPayStatus>();
	    static {
	        for(ThirdPayStatus enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static ThirdPayStatus getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private ThirdPayStatus(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}
	
	public enum ThirdPayType{

		WEIXIN(0),Alipay(1);

		private Byte code;
		
		private static final Map<Byte, ThirdPayType> stringToEnum = new HashMap<Byte, ThirdPayType>();
	    static {
	        for(ThirdPayType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static ThirdPayType getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }


		private ThirdPayType(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum MoneyFlow{

		PAY(0),REFUND(1);

		private Byte code;
		
		private static final Map<Byte, MoneyFlow> stringToEnum = new HashMap<Byte, MoneyFlow>();
	    static {
	        for(MoneyFlow enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static MoneyFlow getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private MoneyFlow(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum ClientType{

		H5(0),APP(1);

		private Byte code;
		
		private static final Map<Byte, ClientType> stringToEnum = new HashMap<Byte, ClientType>();
	    static {
	        for(ClientType enumType : values()) {
	            stringToEnum.put(enumType.getCode(), enumType);
	        }
	    } 
	    
	    public static ClientType getEnumByType(Byte type) {
	    	return stringToEnum.get(type);
	    }

		private ClientType(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
		
		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum WxInfoType {
		IOS("IOS"), ANDROID("Android"), WAP("Wap");

		private String type;
		
		private static final Map<String, WxInfoType> stringToEnum = new HashMap<String, WxInfoType>();
	    static {
	        for(WxInfoType enumType : values()) {
	            stringToEnum.put(enumType.getType(), enumType);
	        }
	    } 
	    
	    public static WxInfoType getEnumByType(String type) {
	    	return stringToEnum.get(type);
	    }

		private WxInfoType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
		
		public void setType(String type) {
			this.type = type;
		}

	}


	public static Class[] getEnumClassList() {
		Class businessEnumClass = BusinessEnum.class;
		return businessEnumClass.getClasses();
	}
	
	/**
	 * 是否为系统自定义的枚举值
	 * @param cla
	 * @return
	 */
	public static boolean isBusinessEnum(Class cla) {
		Class[] a = getEnumClassList();
		for(int i = 0; i<= a.length - 1; i++) {
			if(cla.isAssignableFrom(a[i])) {
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		BusinessEnum.ClientType.values();
		System.out.println(BusinessEnum.ClientType.valueOf("").getCode());
	}


	public enum CoinScaleType{

		PERCENT(1),FIXED(0);

		private Byte code;

		private static final Map<Byte, CoinScaleType> stringToEnum = new HashMap<Byte, CoinScaleType>();
		static {
			for(CoinScaleType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static CoinScaleType getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}

		private CoinScaleType(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum FeeType{

		PERCENT(0),FIXED(1);

		private Byte code;

		private static final Map<Byte, FeeType> stringToEnum = new HashMap<Byte, FeeType>();
		static {
			for(FeeType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static FeeType getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}

		private FeeType(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum TxStatus{

		UNCONFIRMED(1),CONFIRMED(0);

		private Byte code;

		private static final Map<Byte, TxStatus> stringToEnum = new HashMap<Byte, TxStatus>();
		static {
			for(TxStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static TxStatus getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}

		private TxStatus(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum MerchantAppStatus{

		FALSE(0),TRUE(1);

		private Byte code;

		private static final Map<Byte, MerchantAppStatus> stringToEnum = new HashMap<Byte, MerchantAppStatus>();
		static {
			for(MerchantAppStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static MerchantAppStatus getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}

		private MerchantAppStatus(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum AddressStatus{

		UNUSED(0),USED(1);

		private Byte code;

		private static final Map<Byte, AddressStatus> stringToEnum = new HashMap<Byte, AddressStatus>();
		static {
			for(AddressStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static AddressStatus getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}

		private AddressStatus(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}
	
	/**
	 * 币对配置状态:1-开启;2-关闭
	 * @author Administrator
	 *
	 */
	public enum CoinPairConfigStatus{
		OPEN(1), CLOSE(2);
		
		private Byte code;
		
		private CoinPairConfigStatus(Integer code) {
			this.code = code.byteValue();
		}
		
		public Byte getCode() {
			return code;
		}
	}
	
	/**
	 * 交易更新余额操作枚举
	 * @author 1
	 *
	 */
	public enum LockBalanceOperateType{
		LOCK(1), UNLOCK(2);
		
		private Byte code;
		
		private static final Map<Byte, LockBalanceOperateType> stringToEnum = new HashMap<Byte, LockBalanceOperateType>();
		static {
			for(LockBalanceOperateType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}
		
		public static LockBalanceOperateType getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}
		
		private LockBalanceOperateType(Integer code) {
			this.code = code.byteValue();
		}
		
		public Byte getCode() {
			return code;
		}
	}
	
	public enum OssBucket {
		CAMPAIGN("campaign"), PROJECT("project"), INVESTOR("investor"), INSTITUTION("institution"), INFORMATION(
				"information"), USER("user"), TASK("task");

		private String name;

		private static final Map<String, OssBucket> stringToEnum = new HashMap<String, OssBucket>();
		static {
			for (OssBucket enumType : values()) {
				stringToEnum.put(enumType.getName(), enumType);
			}
		}

		public static OssBucket getEnumByType(String name) {
			if (StringUtils.isEmpty(name)) {
				return null;
			}
			return stringToEnum.get(name.toLowerCase());
		}

		private OssBucket(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
	
	public enum CarouselImageSiteType {
		BOOT_PAGE(0), INDEX_BANNER(1);

		private Byte type;

		private static final Map<Byte, CarouselImageSiteType> stringToEnum = new HashMap<Byte, CarouselImageSiteType>();
		static {
			for (CarouselImageSiteType type : values()) {
				stringToEnum.put(type.type, type);
			}
		}

		public static CarouselImageSiteType getEnumByType(String type) {
			if (StringUtils.isEmpty(type)) {
				return null;
			}
			return stringToEnum.get(new Byte(type));
		}

		private CarouselImageSiteType(Integer type) {
			this.type = type.byteValue();
		}

		public Byte getType() {
			return type.byteValue();
		}
	}

	/**
	 * 云用户管理账户类型
	 * @author 1
	 *
	 */
	public enum CloudManageUserType {
		DEPOSIT(0), EXCHANGE_FEE(1),PRESENT(2),MINING(3),GAME(4),EXTRACT(5),EXTRACT_APPROVE(6),EXTRACT_FEE(7),FIXED_TIME_DEPOSIT_SUBSC(8),FIXED_TIME_DEPOSIT_YIELD(9);

		private Byte type;

		private static final Map<Byte, CloudManageUserType> stringToEnum = new HashMap<Byte, CloudManageUserType>();
		static {
			for (CloudManageUserType type : values()) {
				stringToEnum.put(type.type, type);
			}
		}

		public static CloudManageUserType getEnumByType(String type) {
			if (StringUtils.isEmpty(type)) {
				return null;
			}
			return stringToEnum.get(new Byte(type));
		}

		private CloudManageUserType(Integer type) {
			this.type = type.byteValue();
		}

		public Byte getType() {
			return type.byteValue();

		}
	}

	/**
	 * 算力来源类型
	 * @author 1
	 *   0登陆 1 阅读 2购物 3关注 4 邀请好友
	 */
	public enum ComputePowerSourceType {
		LOGIN(0), READ(1),BUY(2),FOLLOW(3),INVITE(4),GAME(5),REGISTER(6);

		private Byte type;

		private static final Map<Byte, ComputePowerSourceType> stringToEnum = new HashMap<Byte, ComputePowerSourceType>();
		static {
			for (ComputePowerSourceType type : values()) {
				stringToEnum.put(type.type, type);
			}
		}

		public static ComputePowerSourceType getEnumByType(String type) {
			if (StringUtils.isEmpty(type)) {
				return null;
			}
			return stringToEnum.get(new Byte(type));
		}

		private ComputePowerSourceType(Integer type) {
			this.type = type.byteValue();
		}

		public Byte getType() {
			return type.byteValue();
		}
	}


	public enum ComputePowerSourceTypeName {
		LOGIN("每日登录", "0"), READ("资讯", "1"),BUY("购物", "2"),FOLLOW("关注", "3"),INVITE("邀请好友" , "4"),GAME("游戏中心","5"),REGISTER("注册","6");

		private String name;

		private Byte type;

		private static final Map<Byte, ComputePowerSourceTypeName> stringToEnum = new HashMap<Byte, ComputePowerSourceTypeName>();
		static {
			for (ComputePowerSourceTypeName type : values()) {
				stringToEnum.put(type.getType(), type);
			}
		}

		public String getName() {
			return name;
		}

		public Byte getType() {
			return type;
		}

		public static String getName(Byte type) {
			return stringToEnum.get(type).getName();
		}

		ComputePowerSourceTypeName(String name, String type) {
			this.name = name;
			this.type = new Byte(type);
		}
	}


	/**
	 * 算力类型
	 * @author 1
	 *   0 永久算力  1 临时算力
	 *
	 */
	public enum ComputePowerType {
		PERMANENT(0), TEMP(1);

		private Byte type;

		private static final Map<Byte, ComputePowerType> stringToEnum = new HashMap<Byte, ComputePowerType>();
		static {
			for (ComputePowerType type : values()) {
				stringToEnum.put(type.type, type);
			}
		}

		public static ComputePowerType getEnumByType(String type) {
			if (StringUtils.isEmpty(type)) {
				return null;
			}
			return stringToEnum.get(new Byte(type));
		}

		private ComputePowerType(Integer type) {
			this.type = type.byteValue();
		}

		public Byte getType() {
			return type.byteValue();
		}
	}

	/**
	 * 审批结果，0-不通过，1-通过
	 */
	public enum ApprovalResult{

		fail("0"),pass("1");

		private String code;

		private static final Map<String, ApprovalResult> stringToEnum = new HashMap<String, ApprovalResult>();
		static {
			for(ApprovalResult enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static ApprovalResult getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private ApprovalResult(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	/**
	 *  0算力 1币种
	 */
	public enum RewardType{

		COMPUTERPOWER(0),COIN(1);

		private Byte code;

		private static final Map<Byte, RewardType> stringToEnum = new HashMap<Byte, RewardType>();
		static {
			for(RewardType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static RewardType getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}

		private RewardType(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}
	
	/**
	 *  用户实名申请状态
	 */
	public enum UserCertificateStatus{

		applying("0"),approve("1"),approveFail("2");

		private String code;

		private static final Map<String, UserCertificateStatus> stringToEnum = new HashMap<String, UserCertificateStatus>();
		static {
			for(UserCertificateStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static UserCertificateStatus getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private UserCertificateStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}
	
	/**
	 *  用户认证等级
	 */
	public enum UserAuthLevel{

		unverified("0"),realNameAuth("1"),certificateAuth("2"),videoAuth("3");

		private String code;

		private static final Map<String, UserAuthLevel> stringToEnum = new HashMap<String, UserAuthLevel>();
		static {
			for(UserAuthLevel enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static UserAuthLevel getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private UserAuthLevel(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}
	
	
	/**
	 *  理财产品状态
	 */
	public enum FixedTimeDepositStatus{

		inputStatus("0"),releaseStatus("1");

		private String code;

		private static final Map<String, FixedTimeDepositStatus> stringToEnum = new HashMap<String, FixedTimeDepositStatus>();
		static {
			for(FixedTimeDepositStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static FixedTimeDepositStatus getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private FixedTimeDepositStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}
	
	/**
	 *  理财产品认购状态
	 */
	public enum FixedTimeDepositSubscStatus{

		subscSuccess("1"),ransomSuccess("2");

		private String code;

		private static final Map<String, FixedTimeDepositSubscStatus> stringToEnum = new HashMap<String, FixedTimeDepositSubscStatus>();
		static {
			for(FixedTimeDepositSubscStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static FixedTimeDepositSubscStatus getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private FixedTimeDepositSubscStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}


	public enum ActivityBargainStatus{

		Input(new Byte("0")),Online(new Byte("1")),Offline(new Byte("2"));

		private Byte code;

		private static final Map<Byte, ActivityBargainStatus> stringToEnum = new HashMap<Byte, ActivityBargainStatus>();
		static {
			for(ActivityBargainStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static ActivityBargainStatus getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}

		private ActivityBargainStatus(Byte code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum GroupBookingStatus{

		Input(new Byte("0")),Online(new Byte("1")),Offline(new Byte("2"));

		private Byte code;

		private static final Map<Byte, GroupBookingStatus> stringToEnum = new HashMap<Byte, GroupBookingStatus>();
		static {
			for(GroupBookingStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static GroupBookingStatus getEnumByType(Byte type) {
			return stringToEnum.get(type);
		}

		private GroupBookingStatus(Byte code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum GroupBookingOrderStatus{

		GroupBooking(new Byte("1")),GroupBookingSuccess(new Byte("2")),Refunding(new Byte("3")),RefundSuccess(new Byte("4")),Cancel(new Byte("5"));

		private Byte code;

		private static final Map<Byte, GroupBookingOrderStatus> stringToEnum = new HashMap<Byte, GroupBookingOrderStatus>();
		static {
			for(GroupBookingOrderStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static GroupBookingOrderStatus getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private GroupBookingOrderStatus(Byte code) {
			this.code = code;
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}

	public enum GroupBookingOrderPayStatus{

		WaitPay("0"),HasPay("1"),HasSend("2"),Done("3"),Cancel("4"),Refund("5");

		private String code;

		private static final Map<String, GroupBookingOrderPayStatus> stringToEnum = new HashMap<String, GroupBookingOrderPayStatus>();
		static {
			for(GroupBookingOrderPayStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static GroupBookingOrderPayStatus getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private GroupBookingOrderPayStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	public enum TaskRewardType{

		compute(new Byte("0")),coin(new Byte("1")),game(new Byte("2")),coupon(new Byte("3")),gameNumber(new Byte("4"));

		private Byte code;

		private static final Map<Byte, TaskRewardType> stringToEnum = new HashMap<Byte, TaskRewardType>();
		static {
			for(TaskRewardType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static TaskRewardType getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private TaskRewardType(Byte code) {
			this.code = code;
		}

		public Byte getCode() {
			return code;
		}

		public void setCode(Byte code) {
			this.code = code;
		}
	}
}
