package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class RefundRequestParams implements Serializable {

	@ApiModelProperty(value = "商家APP ID",required = true)
	private String appId;
	
	@ApiModelProperty(value = "退款订单号",required = true)
	private String orderId;

	@ApiModelProperty(value = "退款到用户ID",required = true)
	private String userId;

	@ApiModelProperty(value = "退款金额",required = true)
	private BigDecimal amount;

	@ApiModelProperty(value = "签名",required = true)
	private String sign;

	@ApiModelProperty(value = "附加信息",required = true)
	private String attach;

	@ApiModelProperty(value = "交易ID",required = true)
	private String outTradeId;

	@ApiModelProperty(value = "无用",required = true)
	private BigDecimal orderAmount;

	@ApiModelProperty(value = "退款回调地址",required = true)
	private String notifyUrl;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getOutTradeId() {
		return outTradeId;
	}

	public void setOutTradeId(String outTradeId) {
		this.outTradeId = outTradeId;
	}

	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
