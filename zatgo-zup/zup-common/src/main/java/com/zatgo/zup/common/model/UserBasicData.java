package com.zatgo.zup.common.model;

import java.io.Serializable;

public class UserBasicData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userId;

    private String userName;
    
    private String iconUrl;
    
    private String nickname;
    
    private String inviteCode;
    
    private String inviteImg;
    
    private Byte authLevel;

	private String wechatOpenId;

	public Byte getAuthLevel() {
		return authLevel;
	}

	public void setAuthLevel(Byte authLevel) {
		this.authLevel = authLevel;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	public String getInviteImg() {
		return inviteImg;
	}

	public void setInviteImg(String inviteImg) {
		this.inviteImg = inviteImg;
	}

	public String getWechatOpenId() {
		return wechatOpenId;
	}

	public void setWechatOpenId(String wechatOpenId) {
		this.wechatOpenId = wechatOpenId;
	}
}
