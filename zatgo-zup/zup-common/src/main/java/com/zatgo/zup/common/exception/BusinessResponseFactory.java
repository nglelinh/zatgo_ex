package com.zatgo.zup.common.exception;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.utils.SignUtils;

public class BusinessResponseFactory{
    
	/**
	 * system error
	 * @return
	 */
    public static ResponseData<Object> createSystemError() {
    	ResponseData<Object> response = new ResponseData<>();
    	response.setCode(BusinessExceptionCode.SYSTEM_ERROR);
    	response.setMessage(BusinessExceptionCode.getMessage(BusinessExceptionCode.SYSTEM_ERROR));
    	response.setData(null);
        return response;
    }

    /**
     * business error
     * @param errorCode
     * @param message
     * @return
     */
    public static ResponseData createBusinessError(String errorCode,String message){
    	ResponseData<Object> response = new ResponseData<>();
    	response.setCode(errorCode);
    	response.setMessage(message);
    	response.setData(null);
        return response;
    }
    
    /**
     * business error
     * @param errorCode
     * @return
     */
    public static ResponseData createBusinessError(String errorCode){
    	ResponseData<Object> response = new ResponseData<>();
    	response.setCode(errorCode);
    	response.setMessage(BusinessExceptionCode.getMessage(errorCode));
    	response.setData(null);
        return response;
    }
    
    
    /**
     * success
     * @param businessData
     * @return
     */
    public static <T> ResponseData<T> createSuccess(T businessData){
    	ResponseData<T> response = new ResponseData<>();
    	response.setCode(BusinessExceptionCode.SUCCESS_CODE);
    	response.setMessage(BusinessExceptionCode.getMessage(BusinessExceptionCode.SUCCESS_CODE));
    	response.setData(businessData);
        return response;
    }
    
    /**
     * success
     * @param businessData
     * @return
     */
    public static <T> ResponseData<T> createSuccess(T businessData, String sequence, String privateKey){
    	ResponseData<T> response = new ResponseData<>();
    	response.setCode(BusinessExceptionCode.SUCCESS_CODE);
    	response.setMessage(BusinessExceptionCode.getMessage(BusinessExceptionCode.SUCCESS_CODE));
    	response.setData(businessData);
    	response.setSequence(sequence);
    	response.setSign(SignUtils.sign256(response, privateKey));
        return response;
    }
}
