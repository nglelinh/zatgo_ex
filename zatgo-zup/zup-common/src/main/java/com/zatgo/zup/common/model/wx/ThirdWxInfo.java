package com.zatgo.zup.common.model.wx;

/**
 * Created by 46041 on 2019/5/14.
 */
public class ThirdWxInfo {

    private String openId;

    private String name;

    private String headUrl;

    private int gender;


    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }
}
