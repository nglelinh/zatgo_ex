package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class EthContractSignParams implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fromAddress;
	
	private String toAddress;
	
	private BigDecimal amount;
	
	private String password;
	
	private BigInteger mNonce;
	
	private BigInteger gasPrice;
	
	private BigInteger gasLimit;
	
	private String contractAddress;
	
	private BigInteger decimal;
	
	public BigInteger getDecimal() {
		return decimal;
	}

	public void setDecimal(BigInteger decimal) {
		this.decimal = decimal;
	}

	public String getContractAddress() {
		return contractAddress;
	}

	public void setContractAddress(String contractAddress) {
		this.contractAddress = contractAddress;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BigInteger getmNonce() {
		return mNonce;
	}

	public void setmNonce(BigInteger mNonce) {
		this.mNonce = mNonce;
	}

	public BigInteger getGasPrice() {
		return gasPrice;
	}

	public void setGasPrice(BigInteger gasPrice) {
		this.gasPrice = gasPrice;
	}

	public BigInteger getGasLimit() {
		return gasLimit;
	}

	public void setGasLimit(BigInteger gasLimit) {
		this.gasLimit = gasLimit;
	}
	
	
}
