package com.zatgo.zup.common.model;

/**
 * Created by 46041 on 2019/4/19.
 */
public class MongoConstant {

    /**
     * 景点
     */
    public static final String SCENIC_SPOT = "scenic_spot";

    public static final String SEARCH_KEY = "timeStamp";
}
