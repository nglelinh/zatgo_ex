package com.zatgo.zup.common.utils;

import java.beans.IntrospectionException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class MD5
{
	protected static Logger logger = LoggerFactory.getLogger(MD5.class);
    
    // 全局数组
    private final static String[] strDigits = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
            "e", "f" };

    // 返回形式为数字跟字符串
    private static String byteToArrayString(byte bByte)
    {
        int iRet = bByte;
        // System.out.println("iRet="+iRet);
        if (iRet < 0)
        {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return strDigits[iD1] + strDigits[iD2];
    }

    // 返回形式只为数字
    private static String byteToNum(byte bByte)
    {
        int iRet = bByte;
        System.out.println("iRet1=" + iRet);
        if (iRet < 0)
        {
            iRet += 256;
        }
        return String.valueOf(iRet);
    }

    // 转换字节数组为16进制字串
    private static String byteToString(byte[] bByte)
    {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++)
        {
            sBuffer.append(byteToArrayString(bByte[i]));
        }
        return sBuffer.toString();
    }

    /**
     * 小写MD5加密
     * @param strObj
     * @return
     */
    public static String GetMD5Code(String strObj)
    {
        String resultString = null;
        try
        {
            resultString = new String(strObj);
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.digest() 该函数返回值为存放哈希值结果的byte数组
            resultString = byteToString(md.digest(strObj.getBytes("UTF-8")));
        }
        catch(NoSuchAlgorithmException ex)
        {
        	logger.error("",ex);
        } catch (UnsupportedEncodingException e) {
        	logger.error("",e);
		}
        return resultString;
    }
    
    public static String encrypt(String salt, String password)
    {
        String usernameMD5 = DigestUtils.md5Hex(salt.trim());
        String passwordMD5 = DigestUtils.md5Hex(password.trim());
        return GetMD5Code(usernameMD5 + passwordMD5);
    }

    public static void main(String[] args)
    {
        System.out.println(MD5.GetMD5Code("23张三"));
    }
    
    /**
     * 大写md5加密
     * @param s
     * @return
     */
    public static String MD5(String s) {
        char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};       
        try {
            byte[] btInput = s.getBytes("utf-8");
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            mdInst.update(btInput);
            byte[] md = mdInst.digest();
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

	public static String GetMD5CodeNOUTF8(String strObj)
	{
		String resultString = null;
		try {
			resultString = new String(strObj);
			MessageDigest md = MessageDigest.getInstance("MD5");
			// md.digest() 该函数返回值为存放哈希值结果的byte数组
			resultString = byteToString(md.digest(strObj.getBytes()));
		} catch(NoSuchAlgorithmException ex) {
			logger.error("",ex);
		}
		return resultString;
	}

	public static String getMD5Sign(Object o){
		Map<String, String> map = null;
		try {
			map = BeanToMapUtils.beanToStringMap(o);
		} catch (Exception e) {
			logger.error("",e);

		}
		TreeMap<String, String> sortMap = new TreeMap<>(map);
		sortMap.remove("sign");
		StringBuilder signTemp = new StringBuilder();
		sortMap.forEach((k,v) -> {
			if(!StringUtils.isEmpty(v)){
				signTemp.append("&").append(k).append("=").append(v);
			}
		});
		String signStr = signTemp.substring(1);
		return MD5.GetMD5Code(signStr);
	}

	public static String getMD5Sign(TreeMap<String,String> map){
		StringBuilder signTemp = new StringBuilder();
		map.forEach((k,v) -> {
			if(!StringUtils.isEmpty(v)){
				signTemp.append("&").append(k).append("=").append(v);
			}
		});
		String signStr = signTemp.substring(1);
		return MD5.GetMD5Code(signStr);
	}
}
