package com.zatgo.zup.common.model;

import com.zatgo.zup.common.enumtype.BusinessEnum;

import java.io.Serializable;

public class MiningDepositInsertParams implements Serializable {

	private String amount;

	private String coinType;
	
	private String netWorkType;

	private String recordId;

	private String userId;

	private String userName;

	private BusinessEnum.PaymentRecordType type;


	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getNetWorkType() {
		return netWorkType;
	}

	public void setNetWorkType(String netWorkType) {
		this.netWorkType = netWorkType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BusinessEnum.PaymentRecordType getType() {
		return type;
	}

	public void setType(BusinessEnum.PaymentRecordType type) {
		this.type = type;
	}
}
