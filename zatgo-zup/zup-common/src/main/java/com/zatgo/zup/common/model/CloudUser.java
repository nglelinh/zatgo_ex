package com.zatgo.zup.common.model;

public class CloudUser {
    private String cloudUserId;

    private String cloudUserName;

    private String smsSign;

    private String weixinOfficialAcctAppId;

    private String weixinOfficialAcctKey;

    private String thirdAuthenticationUrl;

    private String thirdAuthenticationAdminUrl;

    private String thirdAuthenticationToken;

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getCloudUserName() {
        return cloudUserName;
    }

    public void setCloudUserName(String cloudUserName) {
        this.cloudUserName = cloudUserName == null ? null : cloudUserName.trim();
    }

    public String getSmsSign() {
        return smsSign;
    }

    public void setSmsSign(String smsSign) {
        this.smsSign = smsSign == null ? null : smsSign.trim();
    }

    public String getWeixinOfficialAcctAppId() {
        return weixinOfficialAcctAppId;
    }

    public void setWeixinOfficialAcctAppId(String weixinOfficialAcctAppId) {
        this.weixinOfficialAcctAppId = weixinOfficialAcctAppId;
    }

    public String getWeixinOfficialAcctKey() {
        return weixinOfficialAcctKey;
    }

    public void setWeixinOfficialAcctKey(String weixinOfficialAcctKey) {
        this.weixinOfficialAcctKey = weixinOfficialAcctKey;
    }

    public String getThirdAuthenticationUrl() {
        return thirdAuthenticationUrl;
    }

    public void setThirdAuthenticationUrl(String thirdAuthenticationUrl) {
        this.thirdAuthenticationUrl = thirdAuthenticationUrl;
    }

    public String getThirdAuthenticationAdminUrl() {
        return thirdAuthenticationAdminUrl;
    }

    public void setThirdAuthenticationAdminUrl(String thirdAuthenticationAdminUrl) {
        this.thirdAuthenticationAdminUrl = thirdAuthenticationAdminUrl;
    }

    public String getThirdAuthenticationToken() {
        return thirdAuthenticationToken;
    }

    public void setThirdAuthenticationToken(String thirdAuthenticationToken) {
        this.thirdAuthenticationToken = thirdAuthenticationToken;
    }
}