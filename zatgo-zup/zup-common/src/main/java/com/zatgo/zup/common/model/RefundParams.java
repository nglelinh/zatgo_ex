package com.zatgo.zup.common.model;

import java.awt.geom.RectangularShape;
import java.io.Serializable;
import java.math.BigDecimal;

public class RefundParams implements Serializable {

	private String refundUserId;

	private String receiveUserId;

	private BigDecimal amount;

	private String coinType;
	private String oderId;
	private BigDecimal orderMoney;
	private String refundUserName;
	private String receiveUserName;

	public String getRefundUserName() {
		return refundUserName;
	}

	public void setRefundUserName(String refundUserName) {
		this.refundUserName = refundUserName;
	}

	public String getReceiveUserName() {
		return receiveUserName;
	}

	public void setReceiveUserName(String receiveUserName) {
		this.receiveUserName = receiveUserName;
	}

	public BigDecimal getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(BigDecimal orderMoney) {
		this.orderMoney = orderMoney;
	}

	public String getOderId() {
		return oderId;
	}

	public void setOderId(String oderId) {
		this.oderId = oderId;
	}

	public String getRefundUserId() {
		return refundUserId;
	}

	public void setRefundUserId(String refundUserId) {
		this.refundUserId = refundUserId;
	}

	public String getReceiveUserId() {
		return receiveUserId;
	}

	public void setReceiveUserId(String receiveUserId) {
		this.receiveUserId = receiveUserId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}
}
