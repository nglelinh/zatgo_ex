package com.zatgo.zup.common.enumtype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class ExchangeEnum {

	// 挂单类型
	public enum EntryOrdersType {

		limitPriceEntrust("1"), marketPriceEntrust("2");

		private String code;

		private static final Map<String, EntryOrdersType> stringToEnum = new HashMap<String, EntryOrdersType>();
		static {
			for (EntryOrdersType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static EntryOrdersType getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private EntryOrdersType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	// 交易方向
	public enum ExchangeSide {

		BUY("BUY"), SELL("SELL");

		private String code;

		private static final Map<String, ExchangeSide> stringToEnum = new HashMap<String, ExchangeSide>();
		static {
			for (ExchangeSide enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static ExchangeSide getEnumByType(String type) {
			return stringToEnum.get(type.toUpperCase());
		}

		private ExchangeSide(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	// 订单状态
	public enum OrderStatus {

		INIT("0"), NEW("1"), FILLED("2"), PART_FILLED("3"), CANCELED("4"), PENDING_CANCEL("5"), EXCEPTION(
				"6"), UNCOMMITTED("7");

		private String code;

		private static final Map<String, OrderStatus> stringToEnum = new HashMap<String, OrderStatus>();
		static {
			for (OrderStatus enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static OrderStatus getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private OrderStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	// web socket type
	public enum WebSocketType {

		SUB("sub"), UNSUB("unsub"), REQ("req");

		private String code;

		private static final Map<String, WebSocketType> stringToEnum = new HashMap<String, WebSocketType>();
		static {
			for (WebSocketType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static WebSocketType getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private WebSocketType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	//// websocet订阅接口类型
	public enum WebSocketEvent {

	SUB("sub"), UNSUB("unsub"), REQ("req"), PING("ping"), PONG("pong");

		private String code;

		private static final Map<String, WebSocketEvent> stringToEnum = new HashMap<String, WebSocketEvent>();
		static {
			for (WebSocketEvent enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static WebSocketEvent getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private WebSocketEvent(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	// websocet订阅接口业务类型
	public enum WebSocketBusiType {

		KLINE("kline"), TICKER("ticker"), TRADE_TICKER("trade_ticker"), DEPTH_STEP("depth_step"), MARKET("market");

		private String code;

		private static final Map<String, WebSocketBusiType> stringToEnum = new HashMap<String, WebSocketBusiType>();
		static {
			for (WebSocketBusiType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static WebSocketBusiType getEnumByType(String type) {
			return stringToEnum.get(type.toLowerCase());
		}

		private WebSocketBusiType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	// 交易所类型
	public enum ExchangeType {

		Bytex("bytex"), Huobi("huobi"), Huobi_Hadax("huobi_hadax"),Bitshop("bitshop"),Zatgo("zatgo"),Gsboms("gsboms");

		private String code;

		private static final Map<String, ExchangeType> stringToEnum = new HashMap<String, ExchangeType>();
		static {
			for (ExchangeType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static ExchangeType getEnumByType(String type) {
			if (StringUtils.isEmpty(type)) {
				return null;
			}
			return stringToEnum.get(type.toLowerCase());
		}

		private ExchangeType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	// 交易所类型
	public enum LegalCoinType {

		cny("cny"), usd("usd");

		private String code;

		private static final Map<String, LegalCoinType> stringToEnum = new HashMap<String, LegalCoinType>();
		static {
			for (LegalCoinType enumType : values()) {
				stringToEnum.put(enumType.getCode(), enumType);
			}
		}

		public static LegalCoinType getEnumByType(String type) {
			return stringToEnum.get(type);
		}

		private LegalCoinType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	/**
	 * ex_order订单来源:1-WEB;2-APP;3-API
	 * 
	 * @author Administrator
	 *
	 */
	public enum OrderSource {
		WEB(1), APP(2), API(3);

		private Byte code;

		private OrderSource(Integer code) {
			this.code = code.byteValue();
		}

		public Byte getCode() {
			return code;
		}
	}

	/**
	 * bytex请求返回码：0-成功
	 * 
	 * @author Administrator
	 *
	 */
	public enum BytexRespCode {
		SUCCESS("0"), TRADE_BELOCK("9"), ORDER_NO_EXIST("22");

		private String code;

		private BytexRespCode(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	/**
	 * k线时长：1min,5min,15min,30min,60min,1day,1week,1month
	 * 
	 * @author Administrator
	 *
	 */
	public enum KlineTime {
		MIN("1"), MIN5("5"), MIN15("15"), MIN30("30"), MIN60("60"), DAY("1440"), WEEK("10080"), MONTH("43200");

		private String code;

		private KlineTime(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	/**
	 * 盘口深度：step0, step1, step2（合并深度0-2）；step0时，精度最高
	 * 
	 * @author Administrator
	 *
	 */
	public enum DepthStep {
		STEP0("step0"), STEP1("step1"), STEP2("step2");
		
		private static final Map<String, DepthStep> stringToEnum = new HashMap<String, DepthStep>();
		static {
			for (DepthStep depthStep : values()) {
				stringToEnum.put(depthStep.getCode(), depthStep);
			}
		}

		public static DepthStep getEnumByType(String type) {
			return stringToEnum.get(type.toLowerCase());
		}

		private String code;

		private DepthStep(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	/**
	 * 交易模式：1-直连；2-转发
	 * 
	 * @author Administrator
	 *
	 */
	public enum ExchangeMode {
		DIRECT("direct"), LOCAL("local");

		private String code;

		private static final Map<String, ExchangeMode> stringToEnum = new HashMap<String, ExchangeMode>();
		static {
			for (ExchangeMode exchangeMode : values()) {
				stringToEnum.put(exchangeMode.getCode(), exchangeMode);
			}
		}

		public static ExchangeMode getEnumByType(String type) {
			if (StringUtils.isEmpty(type)) {
				return null;
			}
			return stringToEnum.get(type.toLowerCase());
		}

		private ExchangeMode(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	public enum SymbolClientType {
		POLLING("polling"), WEBSOCKET("websocket");

		private static final Map<String, SymbolClientType> stringToEnum = new HashMap<String, SymbolClientType>();
		static {
			for (SymbolClientType symbolClientType : values()) {
				stringToEnum.put(symbolClientType.getType(), symbolClientType);
			}
		}

		public static SymbolClientType getEnumByType(String type) {
			if (StringUtils.isEmpty(type)) {
				return null;
			}
			return stringToEnum.get(type.toLowerCase());
		}

		private String type;

		private SymbolClientType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}
	
	public enum ExchangeClientType {
		POLLING("polling"), WEBSOCKET("websocket");

		private static final Map<String, ExchangeClientType> stringToEnum = new HashMap<String, ExchangeClientType>();
		static {
			for (ExchangeClientType exchangeClientType : values()) {
				stringToEnum.put(exchangeClientType.getType(), exchangeClientType);
			}
		}

		public static ExchangeClientType getEnumByType(String type) {
			if (StringUtils.isEmpty(type)) {
				return null;
			}
			return stringToEnum.get(type.toLowerCase());
		}

		private String type;

		private ExchangeClientType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}

	/**
	 * 火币计价币
	 * 
	 * @author Administrator
	 *
	 */
	public enum ExchangeQuoteCoin {
		USDT("usdt"), BTC("btc"), ETH("eth");

		private String coin;

		private ExchangeQuoteCoin(String coin) {
			this.coin = coin;
		}

		public String getCoin() {
			return coin;
		}

		public static List<String> getCoins() {
			ExchangeQuoteCoin[] _coins = values();
			List<String> coins = new ArrayList<String>();
			for (ExchangeQuoteCoin coin : _coins) {
				coins.add(coin.getCoin());
			}

			return coins;
		}
	}
	
	public enum WebSocketOperateType {
		FULL("full"), INCREMENT("increment"), DEL("del");
		
		private String type;
		
		private WebSocketOperateType(String type) {
			this.type = type;
		}
		
		public String getType() {
			return type;
		}
	}
	
	public enum BytexWebSocketEvent {
		REQ("req"), SUB("sub");
		
		private String event;
		
		private BytexWebSocketEvent(String event) {
			this.event = event;
		}
		
		public String getEvent() {
			return event;
		}
	}
}
