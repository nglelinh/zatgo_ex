package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@ApiModel(value = "订单状态")
public class OrderStatus implements Serializable{
	@ApiModelProperty(value = "第三方订单状态1已支付非1未支付",required =true )
	private Byte thirdPayStatus;
	@ApiModelProperty(value = "ZUP支付状态1已支付非1未支付",required =true )
	private Byte orderStatus;
	@ApiModelProperty(value = "代币金额",required =true )
	private BigDecimal token;
	@ApiModelProperty(value = "人民币金额",required =true )
	private BigDecimal cash;
	public Byte getThirdPayStatus() {
		return thirdPayStatus;
	}

	public void setThirdPayStatus(Byte thirdPayStatus) {
		this.thirdPayStatus = thirdPayStatus;
	}

	public Byte getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Byte orderStatus) {
		this.orderStatus = orderStatus;
	}

	public BigDecimal getToken() {
		return token;
	}

	public void setToken(BigDecimal token) {
		this.token = token;
	}

	public BigDecimal getCash() {
		return cash;
	}

	public void setCash(BigDecimal cash) {
		this.cash = cash;
	}
}
