package com.zatgo.zup.common.sms.santi;

public interface SantiService {

	/**
	 * 发送三体短信
	 * @param value 可变内容（***|***|***）
	 * @param phone 手机号码
	 * @param modelId 三体模板ID
	 * @param asyncCallbackUrl  异步回调url
	 * @return
	 */
	public SantiSmsData sendSms(String smsSign, String value, String phone, String modelId);
	
	/**
	 * 发送国际短信（不支持发送86）
	 * @param value 可变内容（***|***|***）
	 * @param dialingCode 国家区号
	 * @param phone 手机号码
	 * @param modelId 三体模板ID
	 * @return
	 */
	public SantiSmsData sendInternationalSMS(String smsSign, String value, String dialingCode, String phone, String modelId); 
}
