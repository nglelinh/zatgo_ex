package com.zatgo.zup.common.model.coupons;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class InternalUsageCouponsOrderParams {

	@ApiModelProperty(value = "订单ID",required = true)
	private String orderId;
	
	@ApiModelProperty(value = "云商户ID",required = true)
	private String cloudUserId;
	
	@ApiModelProperty(value = "用户ID",required = true)
	private String userId;

	@ApiModelProperty(value = "用户类型：1=新用户\r\n" + 
			"2=老用户",required = false)
	private Byte userType;
	
	@ApiModelProperty(value = "会员等级",required = false)
	private Byte userLevel;
	
	@ApiModelProperty(value = "自定义信息，对应优惠范围关联表的类型,业务类型：1=机票\r\n" + 
			"2=酒店\r\n" + 
			"3=火车\r\n" + 
			"4=用车\r\n" + 
			"5=挖矿\r\n" + 
			"6=游戏\r\n" + 
			"7=理财产品",required = true)
	private Byte businessType;
	
	@ApiModelProperty(value = "子业务Code,自定义信息，对应优惠范围关联表的code",required = false)
	private String subBusinessCode;
	
	@ApiModelProperty(value = "订单金额",required = true)
	private BigDecimal orderMoney;
	
	@ApiModelProperty(value = "用户优惠券列表",required = true)
	private List<String> userCouponsIds;
	
	public List<String> getUserCouponsIds() {
		return userCouponsIds;
	}

	public void setUserCouponsIds(List<String> userCouponsIds) {
		this.userCouponsIds = userCouponsIds;
	}

	public String getSubBusinessCode() {
		return subBusinessCode;
	}

	public void setSubBusinessCode(String subBusinessCode) {
		this.subBusinessCode = subBusinessCode;
	}

	public Byte getBusinessType() {
		return businessType;
	}

	public void setBusinessType(Byte businessType) {
		this.businessType = businessType;
	}

	public String getCloudUserId() {
		return cloudUserId;
	}

	public void setCloudUserId(String cloudUserId) {
		this.cloudUserId = cloudUserId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Byte getUserType() {
		return userType;
	}

	public void setUserType(Byte userType) {
		this.userType = userType;
	}

	public Byte getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(Byte userLevel) {
		this.userLevel = userLevel;
	}

	public BigDecimal getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(BigDecimal orderMoney) {
		this.orderMoney = orderMoney;
	}
	
	
}
