package com.zatgo.zup.common.mq.aliyunmq;

import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.openservices.ons.api.Consumer;
import com.zatgo.zup.common.mq.MQConsumerCallBack;
import com.zatgo.zup.common.mq.aliyunmq.listener.ZupNoticeListener;
import com.zatgo.zup.common.mq.aliyunmq.tool.AliyunMQClient;

@Component
public class AliYunMQConsumer {
	private static final Logger logger = LoggerFactory.getLogger(AliYunMQConsumer.class);
	
	@Autowired
	private AliyunMQClient aliyunMQClient;


	public void startConsumer(String consumerGroup,String topic,String tags,MQConsumerCallBack callBack,MessageModel model){
		Consumer zupNoticeConsumer = null;
		String consumerId = "CID_" + topic;
		if(model.equals(MessageModel.BROADCASTING)){
			zupNoticeConsumer = aliyunMQClient.getBroadCastConsumer(consumerId);
		} else {
			zupNoticeConsumer = aliyunMQClient.getClusteringConsumer(consumerId);
		}
		ZupNoticeListener zupNoticeListener = new ZupNoticeListener(callBack);
		zupNoticeConsumer.subscribe(topic,tags, zupNoticeListener);
		logger.info("zup 提醒消息消费者已启动 " + consumerId);
	}
}
