package com.zatgo.zup.common.sms.santi;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.common.utils.BeanToMapUtils;
import com.zatgo.zup.common.utils.MD5;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

@Service
public class SantiServiceImpl implements SantiService {

	private static final Logger logger = LoggerFactory.getLogger(SantiServiceImpl.class);
	
	@Value("${sms.santi.account.name:}")
	private String santiAccountName;
	@Value("${sms.santi.account.callbackurl:}")
	private String santiAccountCallBackUrl;
	@Value("${sms.santi.account.key:}")
	private String santiAccountKey;
	@Value("${sms.santi.url:}")
	private String apiInterfceUrl;
	@Value("${sms.santi.intlurl:}")
	private String intlUrl;
	
	@Resource
	private OkHttpService httpService;
	
	@Override
	public SantiSmsData sendSms(String smsSign, String value, String phone, String modelId) {
		SantiSmsParams params = new SantiSmsParams();
		params.setAppId(santiAccountName);
		params.setMobile(phone);
		params.setModeId(modelId);
		
		if(StringUtils.isEmpty(smsSign)) {
			smsSign = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_SMS_SIGN;
		}
		
		params.setVars(smsSign + "|" + value);
		params.setNotifyUrl(santiAccountCallBackUrl);
		
		StringBuilder sign = new StringBuilder(santiAccountKey).append(santiAccountName).append(phone);
		params.setSign(MD5.GetMD5Code(sign.toString()));
		
		HashMap<String, String> map = null;
		try {
			map = BeanToMapUtils.beanToHashMap(params);
		} catch (IllegalAccessException | InvocationTargetException
				| IntrospectionException e) {
			logger.error("bean转map失败！",e);
		}
		SantiSmsData data = httpService.get(apiInterfceUrl, map, SantiSmsData.class);
		if(data == null || !data.getCode().equals("0")) {
			logger.error(data.getCode() + "--" + data.getMsg());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		return data;
	}

	@Override
	public SantiSmsData sendInternationalSMS(String smsSign, String value, String dialingCode, String phone, String modelId) {
		if(dialingCode.startsWith("+")) {
			dialingCode = dialingCode.substring(1);
		}
		phone = dialingCode + phone;
		SantiSmsParams params = new SantiSmsParams();
		params.setAppId(santiAccountName);
		params.setMobile(phone);
		params.setModeId(modelId);
		
		if(StringUtils.isEmpty(smsSign)) {
			smsSign = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_SMS_SIGN;
		}
		
		params.setVars(smsSign + "|" + value);
		params.setNotifyUrl(santiAccountCallBackUrl);
		
		StringBuilder sign = new StringBuilder(santiAccountKey).append(santiAccountName).append(phone);
		params.setSign(MD5.GetMD5Code(sign.toString()));
		
		HashMap<String, String> map = null;
		try {
			map = BeanToMapUtils.beanToHashMap(params);
		} catch (IllegalAccessException | InvocationTargetException
				| IntrospectionException e) {
			logger.error("bean转map失败！",e);
		}
		
		SantiSmsData data = httpService.get(intlUrl, map, SantiSmsData.class);
		if(data == null || !data.getCode().equals("0")) {
			logger.error(data.getCode() + "--" + data.getMsg());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		
		Rets ret = data.getRets().get(0);
		if(!ret.getRespCode().equals("0")) {
			logger.error("国际短信发送失败：" + data.getCode() + "--" + data.getMsg());
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		
		return data;
	}

}
