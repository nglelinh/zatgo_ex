package com.zatgo.zup.common.sms.santi;

public class Rets {

	private String respCode;//	rets	int	可选	二级返回码。0为成功，其他响应码为失败
	private String mobile;//	rets	String	可选	手机号码
	private String sendId;//	rets	String	可选	订单号
	public String getRespCode() {
		return respCode;
	}
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getSendId() {
		return sendId;
	}
	public void setSendId(String sendId) {
		this.sendId = sendId;
	}
}
