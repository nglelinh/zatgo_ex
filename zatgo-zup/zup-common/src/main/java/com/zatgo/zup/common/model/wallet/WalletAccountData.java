package com.zatgo.zup.common.model.wallet;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class WalletAccountData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String accountId;

    private String userId;

    private String accountName;

    /**
     * wallet
            multisig
            
     */
    private String accountType;

    /**
     * BTC
            ETH
            QTUM
     */
    private String coinNetworkType;

    private String coinType;

    private String contractAddress;

    private String coinImage;

    private Byte isPrimary;

    private Date createDate;

    private Date updateDate;

    private BigDecimal balance;
    
    private BigDecimal lockBalance;


	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getContractAddress() {
		return contractAddress;
	}

	public void setContractAddress(String contractAddress) {
		this.contractAddress = contractAddress;
	}

	public String getCoinImage() {
		return coinImage;
	}

	public void setCoinImage(String coinImage) {
		this.coinImage = coinImage;
	}

	public Byte getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Byte isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getLockBalance() {
		return lockBalance;
	}

	public void setLockBalance(BigDecimal lockBalance) {
		this.lockBalance = lockBalance;
	}


}
