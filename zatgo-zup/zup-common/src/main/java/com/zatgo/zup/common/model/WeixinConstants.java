package com.zatgo.zup.common.model;

public class WeixinConstants {

	public final static String appid  = "appid";
	public final static String mch_id  = "mch_id";
	public final static String partnerid  = "partnerid";
	public final static String prepayid  = "prepayid";
	public final static String timestamp  = "timestamp";
	public final static String time_stamp  = "time_stamp";
	public final static String noncestr  = "noncestr";
	public final static String nonce_str  = "nonce_str";
	public final static String sign  = "sign";


	public final static String device_info  = "device_info";
	public final static String body  = "body";
	public final static String detail  = "detail";
	public final static String attach  = "attach";
	public final static String out_trade_no  = "out_trade_no";
	public final static String fee_type  = "fee_type";
	public final static String total_fee  = "total_fee";
	public final static String spbill_create_ip  = "spbill_create_ip";
	public final static String time_start  = "time_start";
	public final static String time_expire  = "time_expire";
	public final static String goods_tag  = "goods_tag";
	public final static String notify_url  = "notify_url";
	public final static String trade_type  = "trade_type";
	public final static String product_id  = "product_id";
	public final static String limit_pay  = "limit_pay";
	public final static String openid  = "openid";
	public final static String scene_info  = "scene_info";



	public final static String return_code  = "return_code";
	public final static String return_msg  = "return_msg";
	public final static String result_code  = "result_code";
	public final static String err_code  = "err_code";
	public final static String err_code_des  = "err_code_des";
	public final static String prepay_id  = "prepay_id";
	public final static String code_url  = "code_url";


	public final static String transaction_id  = "transaction_id";
	public final static String out_refund_no  = "out_refund_no";
	public final static String refund_fee  = "refund_fee";
	public final static String refund_fee_type  = "refund_fee_type";
	public final static String refund_desc  = "refund_desc";
	public final static String refund_account  = "refund_account";
	public final static String settlement_refund_fee  = "settlement_refund_fee";
	public final static String settlement_total_fee   = "settlement_total_fee";
	public final static String cash_fee  = "cash_fee";
	public final static String cash_fee_type   = "cash_fee_type";
	public final static String cash_refund_fee  = "cash_refund_fee";
	public final static String coupon_refund_fee   = "coupon_refund_fee";
	public final static String coupon_type_$n   = "coupon_type_$n";
	public final static String coupon_refund_fee_$n   = "coupon_refund_fee_$n";
	public final static String coupon_refund_count   = "coupon_refund_count";
	public final static String coupon_refund_id_$n   = "coupon_refund_id_$n";
	public final static String time_end   = "time_end";
	
	public final static String WX_APPID  = "wx_app_id";
	public final static String WX_MCHID  = "wx_mch_id";
	public final static String WX_KEY  = "wx_key";
	public final static String WX_KEYPATH  = "wx_key_path";
	public final static String WX_CODE_URL  = "weixin://wxpay/bizpayurl?";

	public enum TradeType {
		PUBLIC("JSAPI"), QR("NATIVE"), APP(""), H5("MWEB");

		private String type;

		private TradeType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}
	
}
