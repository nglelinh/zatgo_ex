package com.zatgo.zup.common.model;

/**
 * 游戏奖品
 * Created by chen on 2018/12/20.
 */
public class GamePrizeModel {

    /**
     * 奖品等级
     */
    private String level;

    /**
     * 奖品方式，1-固定数量奖励
     */
    private String type;

    /**
     * 奖品，1-代币，2-算力
     */
    private String prize;

    /**
     * 数值
     */
    private String value;
    /**
     * 如果 prize 是币的话 则不为null
     */
    private String coinNetworkType;
    /**
     * 如果 prize 是币的话 则不为null
     */
    private String coinType;

    private String triggerBusinessType;

    private String triggerBusinessCode;

    private String aliasName;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getTriggerBusinessType() {
        return triggerBusinessType;
    }

    public void setTriggerBusinessType(String triggerBusinessType) {
        this.triggerBusinessType = triggerBusinessType;
    }

    public String getTriggerBusinessCode() {
        return triggerBusinessCode;
    }

    public void setTriggerBusinessCode(String triggerBusinessCode) {
        this.triggerBusinessCode = triggerBusinessCode;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }
}
