package com.zatgo.zup.common.mq;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * 消息服务接口
 * @Title
 * @author lincm
 * @date 2018年5月8日 下午2:19:37
 */
public interface MQService {

	/**
     * 发送普通消息
     * @param topic  发送topic
     * @param tag  发送tag
     * @param body 发送的内容
     * @param key 代表业务的关键属性，最好全局唯一， 可以是订单号等等
     * @return sendResult 返回对象
     */
    public void sendMQmessage(String topic, String tag, String body, String key) throws UnsupportedEncodingException;

    /**
     * 发送顺序消息
     * @param topic 发送顺序消息的topic
     * @param tag 发送顺序消息的tag
     * @param body 发送的内容
     * @param key 消息的key
     * @param  shardingKey 顺序消息用来分区的key，全局顺序消息时可以传null
     * @return
     */
    public void sendMQSeqMessage(String topic, String tag, String body, String key, String shardingKey);

    /**
     * 发送延时消息
     * @param topic topic
     * @param tag tag
     * @param body 发送的内容
     * @param key key 代表消息的业务关键属性，
     * @param delayTime 延迟发送时间 单位毫秒
     * @return
     */
    public void sendMQDelayMessage(String topic, String tag, String body, String key, Long delayTime);

    /**
     * 发送定时消息
     * @param topic topic
     * @param tag tag
     * @param body 发送的内容
     * @param key key 代表消息的业务关键属性，
     * @param Timing 定时消息发送时间
     * @return
     */
    public void sendMQTimingMessage(String topic, String tag, String body, String key, Date Timing);

    /**
     * 发送事务型消息
     * @param topic
     * @param tag
     * @param body
     * @param key
     */
    public void sendMQTranscctionMessage(String topic, String tag, String body, String key);
}
