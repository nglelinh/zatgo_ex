package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class SignBTCTransactionParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<UnSpentBTCData> unSpentBtcs;
	
	private String password;
	
	private String toAddress;
	
	private long amount;
	
	private long fee;
	
	private String changeAddress;

	public List<UnSpentBTCData> getUnSpentBtcs() {
		return unSpentBtcs;
	}

	public void setUnSpentBtcs(List<UnSpentBTCData> unSpentBtcs) {
		this.unSpentBtcs = unSpentBtcs;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getFee() {
		return fee;
	}

	public void setFee(long fee) {
		this.fee = fee;
	}

	public String getChangeAddress() {
		return changeAddress;
	}

	public void setChangeAddress(String changeAddress) {
		this.changeAddress = changeAddress;
	}
	
	
}
