package com.zatgo.zup.common.model;

/**
 * Created by 46041 on 2019/7/18.
 */
public class SpecialProductListRequest {


    private Integer pageSize;

    private Integer pageNum;
    //1-旅游；2-机票；3-酒店；4-文章;5-门票；6其他
    private Integer businessType;
    //1推荐2特惠3新品
    private Integer type;

    private Boolean special;

    private String productName;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getSpecial() {
        return special;
    }

    public void setSpecial(Boolean special) {
        this.special = special;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
