package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
@ApiModel(value = "获取支付信息返回实体")
public class PayQRcodeInfo implements Serializable{
	@ApiModelProperty(value = "生成二维码字符串",required = false)
	private String code;
	@ApiModelProperty(value = "商家名称",required = true)
	private String name;
	@ApiModelProperty(value = "商家ID",required = true)
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
