package com.zatgo.zup.common.mq;

import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.mq.aliyunmq.AliYunMQConsumer;
import com.zatgo.zup.common.mq.rocketmq.RocketMQConsumer;

@Component
public class MQConsumer {
	private static final Logger logger = LoggerFactory.getLogger(MQConsumer.class);
	
	@Value("${mq.type:rocketmq}")
	private String mqType;
	
	private static final String MQ_TYPE_ROCKETMQ = "rocketmq";
	
	private static final String MQ_TYPE_ALIYUNMQ = "aliyunmq";
	
	@Autowired
	private RocketMQConsumer rocketMQConsumer;
	
	@Autowired
	private AliYunMQConsumer aliYunMQConsumer;
	
	public void startConsumer(String consumerGroup, String topic,String tags, MQConsumerCallBack callBack, MessageModel model){
		try {
			if(mqType.equals(MQ_TYPE_ROCKETMQ)) {
				rocketMQConsumer.startConsumer(consumerGroup, topic,tags, callBack,model);
			}else if(mqType.equals(MQ_TYPE_ALIYUNMQ)) {
				aliYunMQConsumer.startConsumer(consumerGroup, topic,tags, callBack,model);
			}else {
				throw new BusinessException();
			}
		} catch (Exception e) {
			logger.error("consumer error",e);
		}
	}


}

