package com.zatgo.zup.common.encrypt;

public class RSAKey {
	
	public static final String PRIVATE_KEY = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJxiHXHapduKZ3Fy"
			+"79LgKXKMySx/1vEZgweBdV+nEL7sDJ4ZMH6YerV5mv6mzyQFUfu335lE5KbxChTR"
			+"3Roiblyz5Jjk+GPCqZJHDUulAVU/jUZvye8Stdel+zb1pY0MgPwHl8yCU13f3AFa"
			+"9fTurJU/MNhCzojssRIr+CAhhuCdAgMBAAECgYBdMT8YzVLPrQAOJI90nG82u11b"
			+"bmeZ0Dk8bNKUiQu6qxKt2JcEqjteEDTkkmWYsq7zkdTnEp4sOjJmy9W2MFSRbYBT"
			+"DVtqdk1R5KyTtc1JDorPxSk7u5DBoWou7ECbEABKWLlRgelZFDky0+XG5Gvz5D+u"
			+"VcOrClh5hD20r31RgQJBAMxCQ/4sk//njprjf+zq6lDp8hzgbjb6ygy7cXRko0V/"
			+"lI839S3ljYvqNHR7LVyIuSvMa6s9UgJr3ghfhUqvNGECQQDD/zk1mHfyeon0lAjO"
			+"0eGLISriXXxX5Wp3fHr6170JVp10uA4OB926ldKBC7cmTp8bkal4fZqJKB7j2u8W"
			+"aVW9AkBVDS60sqk5wJivxBUreLHy2Uc4YPdjq0/gt1hJp/IYaURZ0mokH1auMOqA"
			+"qh28Ty4Tn74hgpoYagP9JpEhBQ1hAkAYhe6ajiEz7bElMQCJICmgRt4exKOyO7o+"
			+"iQy6JhQ4/uVknULk7ocHuYdFLjoUSPIPeQJDWTPkxWhYCiSwIS3hAkEAk5n7CVss"
			+"jZyVp/s+iEHTXMkLvsMf1bNQxUPmZ9tpzKv3I4kuIXiVtwkfUS0ujDbRFRUUexnP"
			+"t2ezBV+2Wy4m+g==";

	public static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCcYh1x2qXbimdx"
			+ "cu/S4ClyjMksf9bxGYMHgXVfpxC+7AyeGTB+mHq1eZr+ps8kBVH7t9+ZROSm8QoU0d0aIm5cs+SY5"
			+ "PhjwqmSRw1LpQFVP41Gb8nvErXXpfs29aWNDID8B5fMglNd39wBWvX07qyVPzDYQs6I7LESK/ggIY"
			+ "bgnQIDAQAB";

	

}
