package com.zatgo.zup.common.model.wx;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/6/10.
 */
public class WechatRefundRequest {

    private String orderNumber;
    private BigDecimal refundAmount;


    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }
}
