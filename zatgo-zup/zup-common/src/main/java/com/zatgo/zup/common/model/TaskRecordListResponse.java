package com.zatgo.zup.common.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 46041 on 2019/1/10.
 */
public class TaskRecordListResponse {

    private String id;

    private String taskId;

    private String taskName;

    private Date createDate;

    private BigDecimal coinNum;

    private String coinType;

    private String coinNetworkType;

    private Integer isForever;


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId == null ? null : taskId.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getCoinNum() {
        return coinNum;
    }

    public void setCoinNum(BigDecimal coinNum) {
        this.coinNum = coinNum;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType;
    }

    public Integer getIsForever() {
        return isForever;
    }

    public void setIsForever(Integer isForever) {
        this.isForever = isForever;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
