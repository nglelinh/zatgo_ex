package com.zatgo.zup.common.model.wx;

/**
 * Created by 46041 on 2019/5/14.
 */
public class ThirdLoginInfo {

    private String userId;

    private String userName;

    private ThirdWxInfo wxInfo;

    private String cloudUserWXAppId;

    private String cloudUserWXSecret;

    private String cloudUserId;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ThirdWxInfo getWxInfo() {
        return wxInfo;
    }

    public void setWxInfo(ThirdWxInfo wxInfo) {
        this.wxInfo = wxInfo;
    }

    public String getCloudUserWXAppId() {
        return cloudUserWXAppId;
    }

    public void setCloudUserWXAppId(String cloudUserWXAppId) {
        this.cloudUserWXAppId = cloudUserWXAppId;
    }

    public String getCloudUserWXSecret() {
        return cloudUserWXSecret;
    }

    public void setCloudUserWXSecret(String cloudUserWXSecret) {
        this.cloudUserWXSecret = cloudUserWXSecret;
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }
}
