package com.zatgo.zup.common.model;

import java.io.Serializable;

public class QtumBlock implements Serializable {
	private String hash;

	private Integer confirmations;
	private Integer strippedsize;
	private Integer size;
	private Integer weight;
	private Integer height;
	private String version;
	private String versionHex;
	private String merkleroot;
	private String hashStateRoot;
	private String hashUTXORoot;
	private String[] tx;
	private Integer time;
	private Integer mediantime;
	private Integer nonce;
	private String bits;
	private Double difficulty;
	private String chainwork;
	private String previousblockhash;
	private String nextblockhash;
	private String flags;
	private String proofhash;
	private String modifier;
	private String signature;

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Integer getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(Integer confirmations) {
		this.confirmations = confirmations;
	}

	public Integer getStrippedsize() {
		return strippedsize;
	}

	public void setStrippedsize(Integer strippedsize) {
		this.strippedsize = strippedsize;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersionHex() {
		return versionHex;
	}

	public void setVersionHex(String versionHex) {
		this.versionHex = versionHex;
	}

	public String getMerkleroot() {
		return merkleroot;
	}

	public void setMerkleroot(String merkleroot) {
		this.merkleroot = merkleroot;
	}

	public String getHashStateRoot() {
		return hashStateRoot;
	}

	public void setHashStateRoot(String hashStateRoot) {
		this.hashStateRoot = hashStateRoot;
	}

	public String getHashUTXORoot() {
		return hashUTXORoot;
	}

	public void setHashUTXORoot(String hashUTXORoot) {
		this.hashUTXORoot = hashUTXORoot;
	}

	public String[] getTx() {
		return tx;
	}

	public void setTx(String[] tx) {
		this.tx = tx;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public Integer getMediantime() {
		return mediantime;
	}

	public void setMediantime(Integer mediantime) {
		this.mediantime = mediantime;
	}

	public Integer getNonce() {
		return nonce;
	}

	public void setNonce(Integer nonce) {
		this.nonce = nonce;
	}

	public String getBits() {
		return bits;
	}

	public void setBits(String bits) {
		this.bits = bits;
	}

	public Double getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Double difficulty) {
		this.difficulty = difficulty;
	}

	public String getChainwork() {
		return chainwork;
	}

	public void setChainwork(String chainwork) {
		this.chainwork = chainwork;
	}

	public String getPreviousblockhash() {
		return previousblockhash;
	}

	public void setPreviousblockhash(String previousblockhash) {
		this.previousblockhash = previousblockhash;
	}

	public String getNextblockhash() {
		return nextblockhash;
	}

	public void setNextblockhash(String nextblockhash) {
		this.nextblockhash = nextblockhash;
	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public String getProofhash() {
		return proofhash;
	}

	public void setProofhash(String proofhash) {
		this.proofhash = proofhash;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
}
