package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value = "提币接口参数")
public class ExtractParams implements Serializable{
	@ApiModelProperty(value = "支付密码",required = true)
	private String passWord;
	@ApiModelProperty(value = "提币到账地址",required = true)
	private String toAddress;
	@ApiModelProperty(value = "金额",required = true)
	private BigDecimal amount;
	@ApiModelProperty(value = "币种",required = true)
	private String type;

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
