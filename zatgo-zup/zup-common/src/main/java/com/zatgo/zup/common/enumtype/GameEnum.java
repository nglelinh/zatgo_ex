package com.zatgo.zup.common.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chen on 2018/12/19.
 */
public class GameEnum {

    public enum GameResult{

        /**
         * 游戏结果，0-失败，1-胜利/中奖
         */
        LOSE("0"),WIN("1");

        private String code;

        private GameResult(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    public enum GameWinRule{

        /**
         * 1-累积达到次数后出一次奖；2-累积达到金额后出一次奖
         */
        BY_TIMES("1"),BY_AMOUNT("2");

        private String code;

        private GameWinRule(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

    }

    public enum GamePrizeStatus{
        /**
         * 0-未发放，1-发放中，2-已发放
         */
        NOT_SEND("0"),SENDING("1"),SEND_DONE("2");

        private String code;

        private GamePrizeStatus(String code) {
            this.code = code;
        }

        public Byte getCode() {
            return Byte.parseByte(code);
        }
    }

    public enum GamePrizeType{
        /**
         * 奖品类型，1-代币，2-算力,3-券
         */
        COIN("1"),SUANLI("2"),COUPON("3");

        private String code;

        private GamePrizeType(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

    }

}
