package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class QTxDetails implements Serializable {

	private String account;

	private String category;

	private BigDecimal amount;

	private Integer vout;

	private BigDecimal fee;

	private boolean abandoned;

	private String label;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getVout() {
		return vout;
	}

	public void setVout(Integer vout) {
		this.vout = vout;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public boolean isAbandoned() {
		return abandoned;
	}

	public void setAbandoned(boolean abandoned) {
		this.abandoned = abandoned;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
