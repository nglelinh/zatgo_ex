package com.zatgo.zup.common.model;


import java.io.Serializable;
import java.util.List;

public class QtumLogData implements Serializable {

	private String blockHash;

	private Integer blockNumber;

	private String transactionHash;

	private Integer transactionIndex;

	private String from;
	
	private String to;

	private Integer cumulativeGasUsed;

	private Integer gasUsed;

	private String contractAddress;

	private String excepted;

	private List<QtumTopicData> log;

	public String getBlockHash() {
		return blockHash;
	}

	public void setBlockHash(String blockHash) {
		this.blockHash = blockHash;
	}

	public Integer getBlockNumber() {
		return blockNumber;
	}

	public void setBlockNumber(Integer blockNumber) {
		this.blockNumber = blockNumber;
	}

	public String getTransactionHash() {
		return transactionHash;
	}

	public void setTransactionHash(String transactionHash) {
		this.transactionHash = transactionHash;
	}

	public Integer getTransactionIndex() {
		return transactionIndex;
	}

	public void setTransactionIndex(Integer transactionIndex) {
		this.transactionIndex = transactionIndex;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Integer getCumulativeGasUsed() {
		return cumulativeGasUsed;
	}

	public void setCumulativeGasUsed(Integer cumulativeGasUsed) {
		this.cumulativeGasUsed = cumulativeGasUsed;
	}

	public Integer getGasUsed() {
		return gasUsed;
	}

	public void setGasUsed(Integer gasUsed) {
		this.gasUsed = gasUsed;
	}

	public String getContractAddress() {
		return contractAddress;
	}

	public void setContractAddress(String contractAddress) {
		this.contractAddress = contractAddress;
	}

	public String getExcepted() {
		return excepted;
	}

	public void setExcepted(String excepted) {
		this.excepted = excepted;
	}

	public List<QtumTopicData> getLog() {
		return log;
	}

	public void setLog(List<QtumTopicData> log) {
		this.log = log;
	}
}
