package com.zatgo.zup.common.utils;

import java.util.ArrayList;
import java.util.List;

public class ArgsUtils {
	public static List<Object> asList(Object... items) {
		if (items == null)
			return null;
		List<Object> itemsList = new ArrayList<Object>(items.length);
		for (Object item : items) {
			itemsList.add(item);
		}
		return itemsList;
	}

}
