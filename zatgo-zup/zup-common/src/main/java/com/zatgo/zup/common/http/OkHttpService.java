package com.zatgo.zup.common.http;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;

import okhttp3.FormBody;
import okhttp3.FormBody.Builder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.web.client.RestTemplate;

@Component
public class OkHttpService {

	private static OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();

	private static OkHttpClient client;

	@Autowired
	private RestTemplate restTemplate;

	//默认编码集
	private static final String CHARSET = "utf-8";

	public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

	public static final MediaType MEDIA_TYPE_FROM = MediaType.parse("application/x-www-form-urlencoded");

	public static final MediaType MEDIA_TYPE_XML = MediaType.parse("application/xml");

	public static final MediaType MEDIA_TEXT_CSV = MediaType.parse("text/csv");

	private OkHttpClient getClient() {
		if (client == null) {
			client = httpBuilder.readTimeout(2, TimeUnit.MINUTES)
					.connectTimeout(2, TimeUnit.MINUTES).writeTimeout(2, TimeUnit.MINUTES) //设置超时
					.build();
		}
		return client;
	}

	private Logger logger = LoggerFactory.getLogger(Logger.class);

	/**
	 * 微信GET请求
	 * @param path
	 * @param clazz
	 * @param pathParams
	 * @return
	 */
	public <T> T wxGet(String path, HashMap<String, String> params, Class<T> clazz, String... pathParams){
		path = getUrl(path, params, pathParams);
		return JSON.parseObject(isoToUtf(doGet(path)), clazz);
	}

	public <T> T wxGetUtf8(String path, HashMap<String, String> params, Class<T> clazz, String... pathParams){
		path = getUrl(path, params, pathParams);
		String res = doGet(path);
		return JSON.parseObject(res, clazz);
	}

	/**
	 * JSON转JAVA
	 *
	 * @param json
	 * @param clazz
	 * @return
	 */
	private Object toJavaObject(String json, Class clazz) {
		return JSON.toJavaObject(JSON.parseObject(json), clazz);
	}

	/**
	 * 转JSONOBJECT
	 *
	 * @param json
	 * @return
	 */
	private JSONObject toJsonObject(String json) {
		return JSON.parseObject(json);
	}

	public String doPost(String url, Map<String, String> map) {
		RequestBody body = new FormBody.Builder()
				.add("apikey", map.get("apikey")).add("mobile", map.get("mobile")).add("text", map.get("text"))
				.build();
		Request request = new Request.Builder().url(url).post(body).build();
		try {
			Response response = getClient().newCall(request).execute();
			if (response.isSuccessful()) {
				return response.body().string();
			} else {
				//抛出业务异常
				throw (BusinessException) toJavaObject(response.body().string(), BusinessException.class);
			}
		} catch (IOException e) {
			//服务调用失败
			throw new BusinessException(BusinessExceptionCode.THIRD_PARTY_SERVICE_TIMEOUT);
		}
	}

	/**
	 * POST请求，带路径和参数
	 *
	 * @param path
	 * @param pathParams
	 * @return
	 */
	public JSONObject post(String path, Map<String, String> map, String... pathParams) {
		String retJson = doPost(path, map);
		return toJsonObject(retJson);
	}

	public <T> T post(String path, Object paramObj, Class<T> clazz) {
		ResponseEntity<String> response = restTemplate.postForEntity(path, paramObj, String.class);
		T responseBody = JSON.parseObject(response.getBody(), clazz);
		return responseBody;
	}

	/**
	 * 微信GET请求
	 * @param path
	 * @param clazz
	 * @param pathParams
	 * @return
	 */
	public <T> T wxGet(String path, Map<String, String> params, Class<T> clazz, String... pathParams){
		path = getUrl(path, params, pathParams);
		return JSON.parseObject(isoToUtf(doGet(path)), clazz);
	}

	/**
	 * 封装get请求url
	 *
	 * @param path
	 * @param params
	 * @param pathParams
	 * @return
	 */
	private String getUrl(String path, Map<String, String> params, String... pathParams) {
		path = createUrl(path, pathParams);
		StringBuffer sb = new StringBuffer(path);
		if (params != null) {
			Iterator<Map.Entry<String, String>> iter = params.entrySet().iterator();
			int index = 1;
			while (iter.hasNext()) {
				Map.Entry<String, String> entry = iter.next();
				String key = entry.getKey();
				String val = entry.getValue();
				if (index == 1) {
					//第一个
					sb.append("?");
				}
				sb.append(key).append("=").append(val).append("&");
				index++;
			}
			if (index > 1) {
				//至少有一个参数，则去掉最后的&符`号
				path = sb.substring(0, sb.length() - 1);
			}
		}
		return path;
	}

	/**
	 * post请求，返回String
	 *
	 * @param path
	 * @param pathParams
	 * @return
	 */
	public String postStr(String path, Map<String, String> map, String... pathParams) {
		return doPost(path, map);
	}

	/**
	 * GET请求
	 *
	 * @param url
	 * @return
	 */
	public String doGet(String url) {
		Request request = new Request.Builder().url(url).build();
		try {
			Response response = getClient().newCall(request).execute();
			if (response.isSuccessful()) {
				return response.body().string();
			} else {
				//抛出业务异常
				throw (BusinessException) toJavaObject(response.body().string(), BusinessException.class);
			}
		} catch (IOException e) {
			//服务调用失败
			throw new BusinessException(BusinessExceptionCode.THIRD_PARTY_SERVICE_TIMEOUT);
		}
	}

	public String doGet(String url,Map<String,String> headers) {
		Request.Builder builder = new Request.Builder().url(url);
		if(headers != null) {
			Iterator iterator = headers.entrySet().iterator();
			while(iterator.hasNext()) {
				Entry entry = (Entry)iterator.next();
				builder.addHeader(entry.getKey().toString(), entry.getValue().toString());
			}
		}
		Request request = builder.build();
		try {
			Response response = getClient().newCall(request).execute();
			return response.body().string();
		} catch (IOException e) {
			//服务调用失败
			throw new BusinessException(BusinessExceptionCode.THIRD_PARTY_SERVICE_TIMEOUT);
		}
	}



	public String postAdd(String path, Object paramObj) {
		String json = JSON.toJSONString(paramObj);
		return doPost(path, json);
	}

	public String postYiTu8(String path, String json) {

		return doPost(path, json);
	}

	public String doPost(String url, String json) {

		RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, json);
		Request request = new Request.Builder().url(url).post(body).build();
		try {
			Response response = getClient().newCall(request).execute();
			if (response.isSuccessful()) {
				return response.body().string();
			} else {
				//抛出业务异常
				logger.error("rsp msg:"+response.message()+"rsp cdoe"+response.code()+"rsp body:"+response.body().string());
				throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
			}
		} catch (IOException e) {
			//服务调用失败
			throw new BusinessException(BusinessExceptionCode.THIRD_PARTY_SERVICE_TIMEOUT);
		}
	}

	public String doPost(String url, Map<String,String> headers,String json) {

		RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, json);
		Request.Builder builder = new Request.Builder().url(url);
		if(headers != null) {
			Iterator iterator = headers.entrySet().iterator();
			while(iterator.hasNext()) {
				Entry entry = (Entry)iterator.next();
				builder.addHeader(entry.getKey().toString(), entry.getValue().toString());
			}
		}
		Request request = builder.post(body).build();
		try {
			Response response = getClient().newCall(request).execute();
			return response.body().string();
		} catch (IOException e) {
			//服务调用失败
			throw new BusinessException(BusinessExceptionCode.THIRD_PARTY_SERVICE_TIMEOUT);
		}
	}



	/**
	 * GET请求，带路径，获取字符串
	 *
	 * @param path
	 * @param pathParams
	 * @return
	 */
	public String getStr(String path, HashMap<String, String> params, String... pathParams) {
		path = getUrl(path, params, pathParams);
		String retJson = doGet(path);
		return retJson;
	}

	/**
	 * GET请求，不带参数，返回JAVA对象
	 *
	 * @param path
	 * @param clazz
	 * @param pathParams
	 * @return
	 */
	public <T> T get(String path, Class<T> clazz, String... pathParams) {
		return JSON.toJavaObject(get(path, pathParams), clazz);
	}

	public <T> List<T> getList(String path, Class<T> clazz, String... pathParams) {
		return JSON.parseArray(getList(path, pathParams), clazz);
	}

	/**
	 * GET请求，带参数，返回JAVA对象
	 *
	 * @param path
	 * @param clazz
	 * @param pathParams
	 * @return
	 */
	public <T> T get(String path, HashMap<String, String> params, Class<T> clazz, String... pathParams) {
		path = getUrl(path, params, pathParams);
//		logger.info(path);
		return JSON.toJavaObject(get(path), clazz);
	}


	/**
	 * GET请求，带路径，获取JSON对象
	 *
	 * @param path
	 * @param pathParams
	 * @return
	 */
	public JSONObject get(String path, String... pathParams) {
		String retJson = doGet(createUrl(path, pathParams));
		logger.info(retJson);
		return toJsonObject(retJson);
	}

	public String getList(String path, String... pathParams) {
		return doGet(createUrl(path, pathParams));
	}


	/**
	 * 封装get请求url
	 *
	 * @param path
	 * @param params
	 * @param pathParams
	 * @return
	 */
	private String getUrl(String path, HashMap<String, String> params, String... pathParams) {
		path = createUrl(path, pathParams);
		StringBuffer sb = new StringBuffer(path);
		if (params != null) {
			Iterator<Map.Entry<String, String>> iter = params.entrySet().iterator();
			int index = 1;
			while (iter.hasNext()) {
				Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
				String key = entry.getKey();
				String val = entry.getValue();
				if (index == 1) {
					//第一个
					sb.append("?");
				}
				sb.append(key).append("=").append(val).append("&");
				index++;
			}
			if (index > 1) {
				//至少有一个参数，则去掉最后的&符号
				path = sb.substring(0, sb.length() - 1);
			}
		}
		return path;
	}

	private String createUrl(String path, String... pathParams) {
		StringBuffer p = new StringBuffer(getServiceUrl(path));
		if (pathParams != null) {
			for (String param : pathParams) {
				p.append("/").append(param);
			}
		}
		return p.toString();
	}

	public String getServiceUrl(String path) {
		if (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}
		return path;
	}

	public String delete(String path, String... pathParams) {
		return doDelete(createUrl(path, pathParams));
	}

	public String doDelete(String url) {
		Request request = new Request.Builder().url(url).delete().build();
		try {
			Response response = getClient().newCall(request).execute();
			if (response.isSuccessful()) {
				return response.body().string();
			} else {
				//抛出业务异常
				throw (BusinessException) toJavaObject(response.body().string(), BusinessException.class);
			}
		} catch (IOException e) {
			//服务调用失败
			throw new BusinessException(BusinessExceptionCode.THIRD_PARTY_SERVICE_TIMEOUT);
		}
	}

	/**
	 * webservice请求
	 *
	 * @param url
	 * @param fb
	 * @return
	 * @throws Exception
	 */
	public String postXml(String url, FormBody fb) throws Exception {
		Request request = new Request.Builder().url(url).post(fb).build();
		Response response = getClient().newCall(request).execute();
		if (response.isSuccessful()) {
			return response.body().string();
		} else {
			return null;
		}
	}

	/**
	 * post listPage
	 */
	public <T> T postListPage(String path, Object paramObj, Class<T> clazz, String... pathParams) {
		String json = JSON.toJSONString(paramObj);
		String retJson = doPost(createUrl(path, pathParams), json);
		return JSON.toJavaObject(toJsonObject(retJson), clazz);
	}

	/**
	 * post list
	 */
	public <T> List<T> postList(String path, Object paramObj,
								Class<T> clazz, String... pathParams) {
		String json = JSON.toJSONString(paramObj);
		String retJson = doPost(createUrl(path, pathParams), json);
		return JSON.parseArray(retJson, clazz);
	}

	/**
	 * post return str
	 */
	public String postStr(String path, Object paramObj) {
		String json = JSON.toJSONString(paramObj);
		return doPost(path, json);
	}

	/**
	 * post obj
	 *
	 * @param path
	 * @param paramObj
	 * @param clazz
	 * @param pathParams
	 * @return
	 */
	public <T> T postObj(String path, Object paramObj, Class<T> clazz, String... pathParams) {
		String json = JSON.toJSONString(paramObj);
		String retJson = doPost(createUrl(path, pathParams), json);
		return JSON.toJavaObject(toJsonObject(retJson), clazz);
	}

	/**
	 * get请求成功或失败
	 * @param url
	 * @return
	 */
	public boolean getBoolean(String url) {
		Request request = new Request.Builder().url(url).build();
		try {
			Response response = getClient().newCall(request).execute();
			if (response.isSuccessful()) {
				logger.info(response.body().string());
				return true;
			}
		} catch (IOException e) {
			//服务调用失败
		}
		return false;
	}

	public String postXml(String url, String params) {
		RequestBody body = RequestBody.create(MEDIA_TYPE_XML, params);
		Request request = new Request.Builder().url(url).post(body).build();
		try {
			Response response = getClient().newCall(request).execute();
			if (response.isSuccessful()) {
				return response.body().string();
			} else {
				//抛出业务异常
				throw (BusinessException) toJavaObject(response.body().string(), BusinessException.class);
			}
		} catch (IOException e) {
			//服务调用失败
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
	}

	public String postFrom(String url, Map<String, String> params) {


		Builder formBuilder = new FormBody.Builder();

		if(MapUtils.isNotEmpty(params)) {
			params.forEach((key, value) -> {
				formBuilder.add(key, value);
			});
		}

		Request request = new Request.Builder().url(url).post(formBuilder.build()).build();

		try {
			Response response = getClient().newCall(request).execute();
			if (response.isSuccessful()) {
				return response.body().string();
			} else {
				//抛出业务异常
				throw (BusinessException) toJavaObject(response.body().string(), BusinessException.class);
			}
		} catch (IOException e) {
			//服务调用失败
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
	}

	private String isoToUtf(String data) {
		try {
			data = new String(data.getBytes(), "ISO-8859-1");
			data = new String(data.getBytes("ISO-8859-1"), "UTF-8");
			logger.info("数据编码 ISO-8859-1 转 UTF-8后，data:{}", data);
			return data;
		} catch (UnsupportedEncodingException e) {
			throw new BusinessException(BusinessExceptionCode.HTTP_FAIL_CODE, "数据编码 ISO-8859-1 转 UTF-8 时发生错误");
		}
	}
}