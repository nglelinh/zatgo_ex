package com.zatgo.zup.common.mq.rocketmq.tool;



import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.PreDestroy;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class RocketMQProducer {
	private static final Logger logger = LoggerFactory.getLogger(RocketMQProducer.class);
	
	private ConcurrentHashMap<String, DefaultMQProducer> producers = new ConcurrentHashMap<>();

	private ReentrantLock reentrantLock = new ReentrantLock();
	
	@Value("${mq.rocketmq.url:}")
	private String add;

	public DefaultMQProducer getProducer(String producerGroup) throws MQClientException {
		DefaultMQProducer producer = producers.get(producerGroup);
		if(producer != null) {
			return producer;
		}
		
		reentrantLock.lock();
		try {
			producer = producers.get(producerGroup);
			if(producer == null) {
				producer = new DefaultMQProducer(producerGroup);
				producer.setNamesrvAddr(add);
				producer.start();
				producers.put(producerGroup, producer);
				logger.info("RocketMQProducer producerGroup=" + producerGroup + " start");
			}
			return producer;
		}finally {
			reentrantLock.unlock();
		}
		
	}
	
	@PreDestroy
	public void close(){
		if(producers != null) {
			Iterator<Entry<String, DefaultMQProducer>> iterator = producers.entrySet().iterator();
			while(iterator.hasNext()) {
				Entry<String, DefaultMQProducer> entry = iterator.next();
				DefaultMQProducer producer = entry.getValue();
				producer.shutdown();
			}
		}
	}

}
