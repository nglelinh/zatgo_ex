package com.zatgo.zup.common.model;

public class AliyunValidateCode {

	private String csessionid;
	private String sig;
	private String token;
	private String scene;
	public String getCsessionid() {
		return csessionid;
	}
	public void setCsessionid(String csessionid) {
		this.csessionid = csessionid;
	}
	public String getSig() {
		return sig;
	}
	public void setSig(String sig) {
		this.sig = sig;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getScene() {
		return scene;
	}
	public void setScene(String scene) {
		this.scene = scene;
	}

	@Override
	public String toString() {
		return "AliyunValidateCode{" +
				"csessionid='" + csessionid + '\'' +
				", sig='" + sig + '\'' +
				", token='" + token + '\'' +
				", scene='" + scene + '\'' +
				'}';
	}
}
