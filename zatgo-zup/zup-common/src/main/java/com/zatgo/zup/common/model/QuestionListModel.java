package com.zatgo.zup.common.model;

import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/2/12.
 */

//题目列表
public class QuestionListModel {


    private String batchNumber;

    private List<QuestionModel> questions;

    private Long time;


    public class QuestionModel{
        //题目ID
        private String questionId;
        //题目
        private String topics;

        //题目类型 0判断题 1单选题 2多选题
        private String type;
        //选项
        private List<Option> options;
        //答题时间
        private Date endDate;
        //错误展示时间
        private Long showTime;




        public String getTopics() {
            return topics;
        }

        public void setTopics(String topics) {
            this.topics = topics;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public Long getShowTime() {
            return showTime;
        }

        public void setShowTime(Long showTime) {
            this.showTime = showTime;
        }
    }


    public class Option{
        //答案
        private String answer;
        //显示的答案
        private String title;



        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public List<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionModel> questions) {
        this.questions = questions;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
