package com.zatgo.zup.common.sms.santi;

import java.util.List;

/**
 * 三体发送短信返回参数
 * @Title
 * @author lincm
 * @date 2018年4月4日 下午3:00:03
 */
public class SantiSmsData {
	private String code;//int	必选	一级返回码。0为成功，其他响应码为失败
	private String msg;//String	必选	描述信息。
	private List<Rets> rets;//int	可选	发送详情列表，code=0时展示
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<Rets> getRets() {
		return rets;
	}
	public void setRets(List<Rets> rets) {
		this.rets = rets;
	}
}
