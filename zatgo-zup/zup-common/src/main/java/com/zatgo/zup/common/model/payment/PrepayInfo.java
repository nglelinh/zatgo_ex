package com.zatgo.zup.common.model.payment;
import java.io.Serializable;

import com.zatgo.zup.common.model.CheckoutOrderData;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value = "prepayId查询结果")
public class PrepayInfo implements Serializable {
	@ApiModelProperty(value = "订单相关",required = true)
	private CheckoutOrderData order;
	@ApiModelProperty(value = "支付金额比例",required = true)
	private PayMoney payMoney;


	public CheckoutOrderData getOrder() {
		return order;
	}

	public void setOrder(CheckoutOrderData order) {
		this.order = order;
	}

	public PayMoney getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(PayMoney payMoney) {
		this.payMoney = payMoney;
	}

}
