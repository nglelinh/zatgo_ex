package com.zatgo.zup.common.utils;

import org.bitcoinj.core.Base58;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContractParamsBuilder {

	private  String hashPattern = "0000000000000000000000000000000000000000000000000000000000000000"; //64b

	private  final int radix = 16;
	private  final String TYPE_INT = "int";
	private  final String TYPE_STRING = "string";
	private  final String TYPE_ADDRESS = "address";
	private  final String TYPE_BOOL = "bool";
	public final static String TYPE_UINT256  = "uint256";
	private  final String ARRAY_PARAMETER_CHECK_PATTERN = ".*?\\d+\\[\\d*\\]";
	private  final String ARRAY_PARAMETER_TYPE = "(.*?\\d+)\\[(\\d*)\\]";

	final int OP_PUSHDATA_1 = 1;
	final int OP_PUSHDATA_4 = 0x04;
	final int OP_PUSHDATA_8 = 8;
	final int OP_EXEC = 193;
	final int OP_EXEC_ASSIGN = 194;
	final int OP_EXEC_SPEND = 195;

	private  String  convertParameter(ContractMethodParameter parameter) {
		String _value = parameter.getValue();
		if (!parameterIsArray(parameter)) {
			if (parameter.getType().equals(TYPE_INT)) {
				return appendNumericPattern(convertToByteCode(new BigInteger(_value)));
			} else if(parameter.getType().equals(TYPE_UINT256)){
				return appendNumericPattern(convertToByteCode(new BigInteger(_value)));
			} else if (parameter.getType().contains(TYPE_STRING)) {
				return getStringOffset(parameter);
			} else if (parameter.getType().contains(TYPE_ADDRESS) && _value.length() == 34) {
				byte[] decode = Base58.decode(_value);
				String toHexString = Hex.toHexString(decode);
				String substring = toHexString.substring(2, 42);
				return appendAddressPattern(substring);
			} else if (parameter.getType().contains(TYPE_ADDRESS)) {
				return getStringOffset(parameter);
			} else if (parameter.getType().contains(TYPE_BOOL)) {
				return appendBoolean(_value);
			}
		} else {
			return getStringOffset(parameter);
		}
		return "";
	}

	private  boolean parameterIsArray(ContractMethodParameter contractMethodParameter) {
		Pattern p = Pattern.compile(ARRAY_PARAMETER_CHECK_PATTERN);
		Matcher m = p.matcher(contractMethodParameter.getType());
		return m.matches();
	}

	public  String appendNumericPattern(String _value) {
		return hashPattern.substring(0, hashPattern.length() - _value.length()) + _value;
	}

	private  String convertToByteCode(BigInteger _value) {
		return _value.toString(radix);
	}

	private  String convertToByteCode(Double _value) {
		return _value.toString(radix);
	}
	static long  paramsCount;
	static long  currStringOffset = 0;
	private  String getStringOffset(ContractMethodParameter parameter) {
		long currOffset = ((paramsCount + currStringOffset) * 32);
		currStringOffset = getStringHash(parameter.getValue()).length() / hashPattern.length() + 1;
		return appendNumericPattern(convertToByteCode(currOffset));
	}
	private  String getStringHash(String _value) {
		if (_value.length() <= hashPattern.length()) {
			return formNotFullString(_value);
		} else {
			int ost = _value.length() % hashPattern.length();
			return _value + hashPattern.substring(0, hashPattern.length() - ost);
		}
	}
	private  String convertToByteCode(long _value) {
		return Long.toString(_value, radix);
	}
	private  String formNotFullString(String _value) {
		return _value + hashPattern.substring(_value.length());
	}

	private  String appendAddressPattern(String _value) {
		return hashPattern.substring(_value.length()) + _value;
	}

	private  String appendBoolean(String parameter) {
		return Boolean.valueOf(parameter) ? appendNumericPattern("1") : appendNumericPattern("0");
	}

	public  String createAbiMethodParams(final String _methodName, final List<ContractMethodParameter> contractMethodParameterList){
		paramsCount  = contractMethodParameterList.size();
		String methodName = _methodName;
		String parameters = "";
		String abiParams = "";
		if(!CollectionUtils.isEmpty(contractMethodParameterList)){
			for (ContractMethodParameter parameter : contractMethodParameterList) {
				abiParams += convertParameter(parameter);
				parameters = parameters + parameter.getType() + ",";
			}
			methodName = methodName + "(" + parameters.substring(0, parameters.length() - 1) + ")";
		} else {
			methodName = methodName + "()";
		}
		Keccak keccak = new Keccak();
		String hashMethod = keccak.getHash(Hex.toHexString((methodName).getBytes()), Parameters.KECCAK_256).substring(0, 8);
		abiParams = hashMethod + abiParams;
		return abiParams;
	}

}
