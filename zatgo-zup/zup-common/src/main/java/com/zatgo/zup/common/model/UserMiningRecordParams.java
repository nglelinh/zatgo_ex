package com.zatgo.zup.common.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 46041 on 2018/8/6.
 */
public class UserMiningRecordParams {
    private String recordId;

    private String userId;

    private String coinNetworkType;

    private String coinType;

    private BigDecimal coinNum;

    private Date createDate;

    private Date timeParagraph;

    private String coinImage;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId == null ? null : recordId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType == null ? null : coinNetworkType.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public BigDecimal getCoinNum() {
        return coinNum;
    }

    public void setCoinNum(BigDecimal coinNum) {
        this.coinNum = coinNum;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getTimeParagraph() {
        return timeParagraph;
    }

    public void setTimeParagraph(Date timeParagraph) {
        this.timeParagraph = timeParagraph;
    }

    public String getCoinImage() {
        return coinImage;
    }

    public void setCoinImage(String coinImage) {
        this.coinImage = coinImage;
    }
}
