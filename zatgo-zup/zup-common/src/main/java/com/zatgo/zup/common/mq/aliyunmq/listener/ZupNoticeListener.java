package com.zatgo.zup.common.mq.aliyunmq.listener;

import java.io.UnsupportedEncodingException;

import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.zatgo.zup.common.mq.MQConsumerCallBack;


public class ZupNoticeListener implements MessageListener {

    private Logger logger = LoggerFactory.getLogger(getClass().getName());
    
    private MQConsumerCallBack callBack;

    public ZupNoticeListener(MQConsumerCallBack callBack) {
    	this.callBack = callBack;
    }
    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {

        String body = null;
        try {
            body = new String(message.getBody(), RemotingHelper.DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            logger.error("字节码转String失败！",e);
            return Action.ReconsumeLater;
        }
        logger.debug("ZupNoticeListener=" + body);
        
        try {
        	callBack.callBack(body);
        }catch(Exception e) {
        	logger.error("",body);
        }
        

        return Action.CommitMessage;

    }

}
