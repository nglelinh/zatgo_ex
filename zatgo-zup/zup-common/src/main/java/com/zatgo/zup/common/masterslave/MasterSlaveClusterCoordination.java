package com.zatgo.zup.common.masterslave;

public interface MasterSlaveClusterCoordination {

	public boolean isMasterExisted();
	
	public boolean tryToLeaderMaster();
	
	public boolean isCurrentMaster();
	
	public boolean unregisterCurrentProcessFromSlave();
	
	public boolean registerCurrentProcessToSlave();
}
