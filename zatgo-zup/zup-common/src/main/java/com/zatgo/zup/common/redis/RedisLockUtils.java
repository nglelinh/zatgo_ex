package com.zatgo.zup.common.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

@Component
public class RedisLockUtils {

	private Logger logger = LoggerFactory.getLogger(getClass().getName());
	
	@Resource
	private RedisTemplate redisTemplate;
	
	//锁超时时间1分钟
	private static final Long LOCK_TIME_OUT = 120000L;
	
	//默认锁获取超时时间 1分钟
	private static final Long DEFAULT_LOCK_GET_TIME_OUT = 120000L;
	
	//加锁阻塞等待时间
	private static final Long THREAD_SLEEP_TIME = 500L;
	
	//隔离各个线程内的局部变量，用于锁删除
	private static final ThreadLocal<HashMap<String, Boolean>> delLockFlag = new ThreadLocal<HashMap<String, Boolean>>(){
		@Override
        protected HashMap<String, Boolean> initialValue() {//初始化false,当获得锁时改为true
            return new HashMap<>();
        }
	};
	/**
	 *
	 * @param key 需要加锁的key 格式：类名+方法名称+变量
	 * @return
	 */
	public boolean lock(String key){
		Long timeOut = DEFAULT_LOCK_GET_TIME_OUT;
		logger.info("lock start ... "+key);
		//锁key
		String lockKey = RedisKeyConstants.getRedisKey(RedisKeyConstants.LOCK, key);
		try {
			while(timeOut >= 0){
				//锁到期时间
				long expires = currtTimeForRedis() + LOCK_TIME_OUT;
				
				Boolean success = redisTemplate.opsForValue().setIfAbsent(lockKey, expires);
//				Boolean success = (Boolean)redisTemplate.execute(new RedisCallback<Boolean>() {
//	                @Override
//	                public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
//	                    //JdkSerializationRedisSerializer jdkSerializer = new JdkSerializationRedisSerializer();
//
//	                	RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
//	                    byte[] value = serializer.serialize(String.valueOf(expires));
//	                    return connection.setNX(lockKey.getBytes(),value);
//	                }
//	            });

				if(success){
					logger.info("get lock success1!");
					delLockFlag.get().put(lockKey,true);
					redisTemplate.expire(lockKey, LOCK_TIME_OUT, TimeUnit.MILLISECONDS); 
					return true;
				}else{
					//获得当前锁主键的值,即过期时间
					Object lockValObj = redisTemplate.opsForValue().get(lockKey);
					Long lockVal = lockValObj == null ? null:Long.parseLong(lockValObj.toString());
					//锁已超时
					if(lockVal != null && lockVal < currtTimeForRedis()){
						//获取lock的旧的时间戳并且重写进当前执行此操作时的时间戳
						Long oldLockVal = (Long)redisTemplate.opsForValue().getAndSet(lockKey, expires);
						//比较两个时间戳的值，并发执行时，只有一个线程能拿到锁
						if(oldLockVal != null && lockVal.equals(oldLockVal)){
							delLockFlag.get().put(lockKey,true);
							redisTemplate.expire(lockKey, LOCK_TIME_OUT, TimeUnit.MILLISECONDS); //设置超时时间，释放内存
							logger.info("get lock success2!");
							return true;
						}
					}
				}
				timeOut -= THREAD_SLEEP_TIME;
				try {
					Thread.sleep(THREAD_SLEEP_TIME);
				} catch (InterruptedException e) {
					logger.error("线程阻塞失败!", e);
				}
			}
		} catch (Exception e) {
			logger.error("获取锁失败！", e);
		} finally{
			logger.info("lock end!");
		}
		return false;
	}


	public boolean taskLock(String key){
		logger.info("lock start ... "+key);
		//锁key
		String lockKey = RedisKeyConstants.getRedisKey(RedisKeyConstants.LOCK, key);
		try {
				//锁到期时间
				long expires = currtTimeForRedis() + LOCK_TIME_OUT;

				Boolean success = redisTemplate.opsForValue().setIfAbsent(lockKey, expires);

				if(success){
					logger.info("get lock success1!");
					delLockFlag.get().put(lockKey,true);
					redisTemplate.expire(lockKey, LOCK_TIME_OUT, TimeUnit.MILLISECONDS);
					return true;
				}else{
					//获得当前锁主键的值,即过期时间
					Object lockValObj = redisTemplate.opsForValue().get(lockKey);
					Long lockVal = lockValObj == null ? null:Long.parseLong(lockValObj.toString());
					//锁已超时
					if(lockVal != null && lockVal < currtTimeForRedis()){
						//获取lock的旧的时间戳并且重写进当前执行此操作时的时间戳
						Long oldLockVal = (Long)redisTemplate.opsForValue().getAndSet(lockKey, expires);
						//比较两个时间戳的值，并发执行时，只有一个线程能拿到锁
						if(oldLockVal != null && lockVal.equals(oldLockVal)){
							delLockFlag.get().put(lockKey,true);
							redisTemplate.expire(lockKey, LOCK_TIME_OUT, TimeUnit.MILLISECONDS); //设置超时时间，释放内存
							logger.info("get lock success2!");
							return true;
						}
					}
				}
		} catch (Exception e) {
			logger.error("获取锁失败！", e);
		} finally{
			logger.info("lock end!");
		}
		return false;
	}
	
	/**
	 * 解锁
	 * @param key 需要解锁的key 格式：类名+方法名称+变量
	 * @param timeOut 等待时间，超过此时间如果没有删除锁成功就不管了
	 * @return
	 */
	public void releaseLock(String key){
		logger.info("del lock start ... "+key);
		String lockKey = RedisKeyConstants.getRedisKey(RedisKeyConstants.LOCK, key);
		try {
			if(delLockFlag.get().get(lockKey)){
				redisTemplate.delete(lockKey);
				
				delLockFlag.get().put(lockKey, false);//修改成功改变变量的值
				logger.info("del lock end!");
			}
		} catch (Exception e) {
			logger.error("删除锁失败！", e);
		}
	}
	
	public void run(int i){
		lock("xxxxx");
		try {
			Thread.sleep(8000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		releaseLock("xxxxx");
	}
	
	/**
     * 多服务器集群，使用下面的方法，代替System.currentTimeMillis()，获取redis时间，避免多服务的时间不一致问题！！！
     *
     * @return
     */
    public Long currtTimeForRedis() {
        return (Long) redisTemplate.execute(new RedisCallback<Long>() {
            @Override
            public Long doInRedis(RedisConnection redisConnection) throws DataAccessException {
                return redisConnection.time();
            }
        });
    }

	public String getRedisKey(String prefix, String... _key) {
		StringBuffer key = new StringBuffer(prefix);
		for (String str : _key) {
			key.append("_").append(str);
		}
		return key.toString();
	}

	public String getRedisKey(Class<?> _calss, String _key) {
		StringBuffer key = new StringBuffer();
		key.append(_calss.getSimpleName());
		key.append(_calss.getMethods()[0].getName());
		key.append(_key);
		return key.toString();
	}
	
}
