package com.zatgo.zup.common.model.wallet;

import com.zatgo.zup.common.enumtype.BusinessEnum;

public class DepositRecordByCloudUserParams {

	private String key;
	
	private BusinessEnum.DepositStatus status;
	
	private String coinType;
	
	private int pageNo;
	
	private int pageSize;

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public BusinessEnum.DepositStatus getStatus() {
		return status;
	}

	public void setStatus(BusinessEnum.DepositStatus status) {
		this.status = status;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
