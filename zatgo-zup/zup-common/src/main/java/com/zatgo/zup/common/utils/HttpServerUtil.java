package com.zatgo.zup.common.utils;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by 46041 on 2018/9/4.
 */
public class HttpServerUtil {


    /**
     * 获取header中指定参数
     * @param serverRequest
     * @param headerName
     * @return
     */
    public static String getHeader(ServerHttpRequest serverRequest, String headerName){
        HttpHeaders headers = serverRequest.getHeaders();
        List<String> datas = headers.get(headerName);
        if(CollectionUtils.isEmpty(datas)) {
            return null;
        }
        return datas.get(0);
    }
}
