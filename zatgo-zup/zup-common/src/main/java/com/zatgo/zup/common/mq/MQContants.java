package com.zatgo.zup.common.mq;

public class MQContants {

	//-------------------------------topic--------------------------------
	public static final String TOPIC_PAY_CALLBACK = "zup_topic_pay_callback";
	
	public final static String TOPIC_REFUND_CALLBACK  = "zup_topic_refund_callback";
	
	public final static String TOPIC_COIN_RECEIVE = "zup_topic_coin_receive";
	
	public final static String TOPIC_ORDER_RECEIVE = "zup_topic_order_receive";
//	public final static String TOPIC_TRANSFER_RECEIVE = "zup_topic_transfer_receive";

	public static final String ZUP_ORDER_CREATE_TOPIC = "zup_order_create_topic";

	public static final String ZUP_TRANSACTION_NOTIFY_TOPIC = "zup_transaction_notify_topic";

	public static final String ZUP_SEND_TRANSACTION_TOPIC = "zup_send_transaction_topic";

	
	//----------------------------consumer group--------------------------
	public final static String CONSUMER_GROUP_PAY_CALLBACK  = "comsumer_group_pay_callback";
	
	public final static String CONSUMER_GROUP_REFUND_CALLBACK  = "comsumer_group_refund_callback";
	
	public final static String CONSUMER_GROUP_COIN_RECEIVE = "consumer_group_coin_receive";
	
	public final static String CONSUMER_GROUP_ORDER_RECEIVE = "consumer_group_order_receive";

	public final static String CONSUMER_GROUP_ORDER_CALLBACK  = "consumer_group_order_callback";

	public final static String CONSUMER_TRANSACTION_NOTIFY_CALLBACK  = "consumer_transaction_notify_callback";

	public final static String CONSUMER_SEND_TRANSACTION_CALLBACK  = "consumer_send_transaction_callback";
}
