package com.zatgo.zup.common.model;

import java.io.Serializable;

public class UnSpentQtumData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String txId;
	
	private Long vout;
	
	private long satoshis;
	
	private Integer height;
	
	private String scriptPubKey;

	public String getTxId() {
		return txId;
	}

	public void setTxId(String txId) {
		this.txId = txId;
	}

	public Long getVout() {
		return vout;
	}

	public void setVout(Long vout) {
		this.vout = vout;
	}

	public long getSatoshis() {
		return satoshis;
	}

	public void setSatoshis(long satoshis) {
		this.satoshis = satoshis;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getScriptPubKey() {
		return scriptPubKey;
	}

	public void setScriptPubKey(String scriptPubKey) {
		this.scriptPubKey = scriptPubKey;
	}
	
	
}
