package com.zatgo.zup.common.model;

/**
 * Created by 46041 on 2019/4/22.
 */
public class SaveImageRequest {

    private String tableName;

    private String familyName;

    private String rowKey;

    private String data;

    private String imageValueKey;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getImageValueKey() {
        return imageValueKey;
    }

    public void setImageValueKey(String imageValueKey) {
        this.imageValueKey = imageValueKey;
    }
}
