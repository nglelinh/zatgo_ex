package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by chen on 2018/6/25.
 */
public class AicoinMarketModel implements Serializable{

    /**
     * 交易所名词，coinegg
     */
    private String marketName;
    /**
     * 币对，用下划线分割，ZAT_BTC
     */
    private String pairs;

    /**
     * 币对汇率
     */
    private BigDecimal price;

    /**
     *  美元价格
     */
    private BigDecimal priceUsd;

    /**
     * 人民币价格
     */
    private BigDecimal priceCny;

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getPairs() {
        return pairs;
    }

    public void setPairs(String pairs) {
        this.pairs = pairs;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceUsd() {
        return priceUsd;
    }

    public void setPriceUsd(BigDecimal priceUsd) {
        this.priceUsd = priceUsd;
    }

    public BigDecimal getPriceCny() {
        return priceCny;
    }

    public void setPriceCny(BigDecimal priceCny) {
        this.priceCny = priceCny;
    }
}
