package com.zatgo.zup.common.model;

/**
 * Created by 46041 on 2019/1/10.
 */
public class TaskRecordListRequest {

    private String userId;

    private Integer pageNo;

    private Integer pageSize;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
