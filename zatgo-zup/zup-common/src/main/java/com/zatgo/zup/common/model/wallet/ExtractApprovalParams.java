package com.zatgo.zup.common.model.wallet;

import com.zatgo.zup.common.enumtype.BusinessEnum;

import io.swagger.annotations.ApiModelProperty;

public class ExtractApprovalParams {

	private String extractRecordId;
	
	private BusinessEnum.ApprovalResult status;
	
	@ApiModelProperty(value = "审批结果，用户可见",required = false)
	private String approveResultMemo;

	public String getApproveResultMemo() {
		return approveResultMemo;
	}

	public void setApproveResultMemo(String approveResultMemo) {
		this.approveResultMemo = approveResultMemo;
	}

	public String getExtractRecordId() {
		return extractRecordId;
	}

	public void setExtractRecordId(String extractRecordId) {
		this.extractRecordId = extractRecordId;
	}

	public BusinessEnum.ApprovalResult getStatus() {
		return status;
	}

	public void setStatus(BusinessEnum.ApprovalResult status) {
		this.status = status;
	}
	
	
}
