package com.zatgo.zup.common.enumtype;

public class SuanliEnum {

	public enum TagStatus {
		DELETE(0), USING(1);

		Byte status;

		private TagStatus(Integer status) {
			this.status = status.byteValue();
		}

		public Byte getStatus() {
			return status;
		}
	}

	public enum CatalogStatus {
		DELETE(0), USING(1);

		Byte status;

		private CatalogStatus(Integer status) {
			this.status = status.byteValue();
		}

		public Byte getStatus() {
			return status;
		}
	}

	public enum InvestorStatus {
		DELETE(0), USING(1);

		Byte status;

		private InvestorStatus(Integer status) {
			this.status = status.byteValue();
		}

		public Byte getStatus() {
			return status;
		}
	}
	
	public enum TagClassType {
		PROJECT(0), INVESTOR(1), INSTITUTION(2);
		
		private Byte type;
		
		private TagClassType (Integer type) {
			this.type = type.byteValue();
		}
		
		public Byte getType() {
			return type;
		}
	}
	
	public enum ProjectStatus {
		DELETE(0), USING(1);

		Byte status;

		private ProjectStatus(Integer status) {
			this.status = status.byteValue();
		}

		public Byte getStatus() {
			return status;
		}
	}
	
	public enum CampaignStatus {
		DELETE(0), USING(1);

		Byte status;

		private CampaignStatus(Integer status) {
			this.status = status.byteValue();
		}

		public Byte getStatus() {
			return status;
		}
	}
	
	public enum CompanyStatus {
		DELETE(0), USING(1);

		Byte status;

		private CompanyStatus(Integer status) {
			this.status = status.byteValue();
		}

		public Byte getStatus() {
			return status;
		}
	}
	
	public enum InstitutionStatus {
		DELETE(0), USING(1);
		
		Byte status;
		
		private InstitutionStatus(Integer status) {
			this.status = status.byteValue();
		}
		
		public Byte getStatus() {
			return status;
		}
	}
	
	public enum UserFavoriteType {
		PROJECT(0), INVESTOR(1), INSTITUTION(2), CAMPAIGN(3);
		
		private Byte type;
		
		private UserFavoriteType (Integer type) {
			this.type = type.byteValue();
		}
		
		public Byte getType() {
			return type;
		}
	}

}
