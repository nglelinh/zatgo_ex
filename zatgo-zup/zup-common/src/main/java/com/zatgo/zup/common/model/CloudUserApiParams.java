package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModelProperty;

public class CloudUserApiParams {
    @ApiModelProperty(value = "id",required = false)
    private String id;
    @ApiModelProperty(value = "云用户ID",required = true)
    private String cloudUserId;
    @ApiModelProperty(value = "备注",required = false)
    private String memo;
    @ApiModelProperty(value = "绑定IP",required = false)
    private String bindIps;
    @ApiModelProperty(value = "公钥",required = true)
    private String publicKey;
    @ApiModelProperty(value = "查询分页用起始页(查询时必传)",required = true)
    private Integer pageNo;
    @ApiModelProperty(value = "每页数量(查询时必传)",required = true)
    private Integer pageSize;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getBindIps() {
        return bindIps;
    }

    public void setBindIps(String bindIps) {
        this.bindIps = bindIps == null ? null : bindIps.trim();
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey == null ? null : publicKey.trim();
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}