package com.zatgo.zup.common.model.coupons;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class UserCouponsModel{
	
    private String cloudUserId;

    @ApiModelProperty(value = "优惠券名称",required = false)
    private String couponsName;

    @ApiModelProperty(value = "优惠形式：0=指定金额\r\n" + 
    		"1=折扣",required = false)
    private Byte discountType;

    @ApiModelProperty(value = "券面值（金额或折扣）",required = false)
    private BigDecimal couponsPrice;

    @ApiModelProperty(value = "使用门槛（满多少金额使用）",required = false)
    private BigDecimal howManyFull;

    @ApiModelProperty(value = "用户类型",required = false)
    private Byte userType;

    @ApiModelProperty(value = "会员等级",required = false)
    private Byte memberLevel;

    @ApiModelProperty(value = "每人限领",required = false)
    private Integer eachGetLimit;

    @ApiModelProperty(value = "是否可叠加使用",required = false)
    private Boolean isOverlayUsage;

    @ApiModelProperty(value = "使用说明",required = false)
    private String usageExplain;

	@ApiModelProperty(value = "用户优惠券ID",required = false)
	private String userCouponsId;
	
	@ApiModelProperty(value = "用户ID",required = false)
    private String userId;

	@ApiModelProperty(value = "用户优惠券领取时间",required = false)
    private Date getDate;

	@ApiModelProperty(value = "用户优惠券生效时间",required = false)
    private Date validStartDate;

	@ApiModelProperty(value = "用户优惠券失效时间",required = false)
    private Date validEndDate;

	@ApiModelProperty(value = "用户优惠券状态：0=未使用\r\n" + 
			"1=已使用",required = false)
    private Byte userCouponsStatus;

	public String getUserCouponsId() {
		return userCouponsId;
	}

	public void setUserCouponsId(String userCouponsId) {
		this.userCouponsId = userCouponsId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getGetDate() {
		return getDate;
	}

	public void setGetDate(Date getDate) {
		this.getDate = getDate;
	}

	public Date getValidStartDate() {
		return validStartDate;
	}

	public void setValidStartDate(Date validStartDate) {
		this.validStartDate = validStartDate;
	}

	public Date getValidEndDate() {
		return validEndDate;
	}

	public void setValidEndDate(Date validEndDate) {
		this.validEndDate = validEndDate;
	}

	public Byte getUserCouponsStatus() {
		return userCouponsStatus;
	}

	public void setUserCouponsStatus(Byte userCouponsStatus) {
		this.userCouponsStatus = userCouponsStatus;
	}


	public String getCloudUserId() {
		return cloudUserId;
	}

	public void setCloudUserId(String cloudUserId) {
		this.cloudUserId = cloudUserId;
	}

	public String getCouponsName() {
		return couponsName;
	}

	public void setCouponsName(String couponsName) {
		this.couponsName = couponsName;
	}

	public Byte getDiscountType() {
		return discountType;
	}

	public void setDiscountType(Byte discountType) {
		this.discountType = discountType;
	}

	public BigDecimal getCouponsPrice() {
		return couponsPrice;
	}

	public void setCouponsPrice(BigDecimal couponsPrice) {
		this.couponsPrice = couponsPrice;
	}

	public BigDecimal getHowManyFull() {
		return howManyFull;
	}

	public void setHowManyFull(BigDecimal howManyFull) {
		this.howManyFull = howManyFull;
	}

	public Byte getUserType() {
		return userType;
	}

	public void setUserType(Byte userType) {
		this.userType = userType;
	}

	public Byte getMemberLevel() {
		return memberLevel;
	}

	public void setMemberLevel(Byte memberLevel) {
		this.memberLevel = memberLevel;
	}

	public Integer getEachGetLimit() {
		return eachGetLimit;
	}

	public void setEachGetLimit(Integer eachGetLimit) {
		this.eachGetLimit = eachGetLimit;
	}

	public Boolean getIsOverlayUsage() {
		return isOverlayUsage;
	}

	public void setIsOverlayUsage(Boolean isOverlayUsage) {
		this.isOverlayUsage = isOverlayUsage;
	}

	public String getUsageExplain() {
		return usageExplain;
	}

	public void setUsageExplain(String usageExplain) {
		this.usageExplain = usageExplain;
	}
    
    
}
