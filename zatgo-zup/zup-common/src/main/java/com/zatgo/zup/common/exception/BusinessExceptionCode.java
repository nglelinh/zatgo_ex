package com.zatgo.zup.common.exception;

import java.util.HashMap;

public class BusinessExceptionCode {
	
	/**
	 * 成功码
	 */
	public static final String SUCCESS_CODE = "0000";
	
	/**
	 * 系统错误
	 */
	public static final String SYSTEM_ERROR = "9999";
	
	/**
	 * 自定义错误
	 */
	public static final String SYSTEM_CUSTOM_ERROR = "9998";
	
	/**
	 * 脏数据处理
	 */
	public final static String SYSTEM_DIRTY_DATA_ERROR = "9997";
	
	/**
	 * token校验异常
	 */
	public static final String TOKEN_VERIFY_EXCEPTION = "8003";
	
	/**
	 * token失效
	 */
	public static final String TOKEN_VERIFY_OVERDAY = "8004";
	
	/**
	 * 请求参数错误
	 */
	public static final String REQUEST_PARAM_ERROR = "1000";
	
	/**
	 * 用户不存在
	 */
	public static final String USER_IS_NOT_EXIST = "1001";
	
	/**
	 * 用户被锁，无法登入
	 */
	public static final String LOGIN_FAIL_LOCK = "1002";
	
	/**
	 * 密码不正确
	 */
	public static final String LOGIN_FAIL_PASSWORD_ERROR = "1003";
	
	/**
	 * TOKEN 校验失败
	 */
	public static final String TOKEN_VERIFY_FAIL = "1004";
	
	/**
	 * app 校验失败
	 */
	public static final String APP_AUTH_VALIDATE_SIGN_ERROR = "1005";

	/**
	 * 用户已存在
	 */
	public static final String USER_IS_EXIST = "1006";

	/**
	 * 实名认证失败
	 */
	public static final String REAL_NAME_AUTH_FIAL = "1007";
	
	/**
	 * 实名认证失败
	 */
	public static final String COIN_PAIR_CONFIG_CLOSE = "1008";

	/**
	 *  title重复
	 */
	public static final String TITLE_IS_EXIST = "10001";

	/**
	 *  缺少必要参数
	 */
	public static final String LACK_MUST_PARAMETER = "10002";
	/**
	 *  获取云用户管理员ID失败
	 */
	public static final String GET_CLOUD_USER_MANAGE_ID_ERROR = "10003";
	/**
	 *  获取云用户管理员已存在
	 */
	public static final String CLOUD_USER_MANAGE_IS_EXIST = "10004";
	/**
	 *  CLOUD_USER_ID为空
	 */
	public static final String CLOUD_USER_ID_IS_EMPTY = "10006";
	/**
	 *  三方token为空
	 */
	public static final String THIRD_TOKEN_IS_EMPTY = "10007";

	/**
	 * 云用户IP校验失败
	 */
	public static final String CLOUD_USER_CHECK_IP_ERROR = "10005";
	/**
	 *  三方服务调用失败
	 */
	public static final String INVOK_THIRD_SERVICE_FAIL = "10008";

	/**
	 *  三方服务调用失败
	 */
	public static final String GET_USER_INFO_FAIL = "10009";

	/**
	 * 支付被锁定
	 */
	public final static String PAY_PASSWORD_LOCK  = "1033";
	public final static String PAY_PASSWORD_ERROR  = "1009";
	public final static String BALANCE_NOT_ENOUGH  = "1010";
	public final static String WALLETACCOUNT_EXIST  = "1011";
	public final static String PAY_FAILED  = "1012";
	public final static String RECEIVE_USER_NOT_EXIST  = "1013";
	
	public final static String WALLETACCOUNT_NOT_EXIST  = "1014";
	
	/**
	 * 冻结金额不足
	 */
	public final static String LOCK_BALANCE_NOT_ENOUGH  = "1015";
	
	/**
	 * 冻结余额失败
	 */
	public final static String LOCK_BALANCE_FAIL = "1016";
	
	/**
	 * 解冻余额失败
	 */
	public final static String UNLOCK_BALANCE_FAIL = "1017";
	
	/**
	 * 扣除冻结余额失败
	 */
	public final static String DEDUCT_LOCK_BALANCE_FAIL = "1018";
	
	/**
	 * 冻结账户余额类型失配
	 */
	public final static String LOCK_BALANCE_TYPE_MISMATCH = "1019";
	
	/**
	 * 交易订单不存在
	 */
	public final static String EX_ORDER_NOT_EXIST = "1020";
	
	/**
	 * 交易订单不能取消
	 */
	public final static String EX_ORDER_CANNOT_CANCEL = "1021";
	
	/**
	 * 交易订单取消失败
	 */
	public final static String EX_ORDER_CANCEL_FAIL = "1022";
	
	public final static String EX_ORDER_CREATE_FAIL = "1023";
	
	/**
	 * ip非法
	 */
	public final static String IP_ILLEGAL = "1024";
	
	/**
	 * Api key error
	 */
	public final static String API_KEY_ERROR = "1025";
	
	/**
	 * Api key error
	 */
	public final static String SECRET_KEY_ERROR = "1026";
	
	public final static String USER_LOCK = "1027";
	
	public final static String ADD_EXCHANGE_BALANCE_FAIL = "1028";
	
	/**
	 * 重复操作账户余额
	 */
	public final static String REPEAT_OPERATION_BALANCE = "1029";
	
	/**
	 * 火币交易账户未找到
	 */
	public final static String HUOBI_ACCOUNT_NOT_FIND = "1030";
	
	/**
	 * 火币交易账户不可用
	 */
	public final static String HUOBI_ACCOUNT_STATE_UNUSABLE = "1031";
	
	/**
	 * 火币下单失败
	 */
	public final static String HUOBI_CREATE_ORDER_FAIL = "1032";

	/**
	 * 第三方调用超时
	 */
	public static final String THIRD_PARTY_SERVICE_TIMEOUT = "10014";
	
	/**
	 * 用户未登入
	 */
	public static final String USER_NOT_LOGIN = "10015";
	
	/**
	 * 收银台订单号重复
	 */
	public static final String CHECKOUT_ORDER_ID_DUPLICATE = "10016";
	
	public final static String DEPOSIT_ERROR  = "10017";
	
	/**
	 * 支付超时
	 */
	public final static String PAYMENT_TIMEOUT = "10018";
	public final static String ALREADYPAID  = "10019";
	public final static String AUTH_CODE_ERROR  = "10020";
	
	/**
	 * 用户权限错误
	 */
	public final static String USER_AUTH_ERROR  = "10021";

	/**
	 * 订单ID错误
	 */
	public final static String CHECKOUT_ORDER_ID_ERROR  = "2001";

	public final static String REFUND_AMOUNT_MORETHAN_ORDER_AMOUNT  = "2002";
	
	public final static String SIGN_ERROR  = "2003";
	
	public final static String REFUND_STATUS_ERROR = "2004";
	public final static String ERROR_AMOUNT_OF_MONEY = "2005";
	public final static String WXPAY_ERROR = "2006";
	
	/**
	 * 重复数据处理
	 */
	public final static String REPETITIVE_DATA_PROCESSING = "2007";
	
	/**
	 * 记录不存在
	 */
	public final static String RECORD_NOT_EXIST = "2008";
	
	/**
	 * 不支持的币种网络类型
	 */
	public final static String 	NOT_SUPPORT_COIN_NETWORK = "2009";
	public final static String 	ILLEGAL_PARAMS = "2010";
	//不支持提币
	public final static String 	NOT_SUPPORT_EXTRACT_COIN_TYPE = "20001";
	
	/**
	 * 不支持的币种
	 */
	public final static String 	NOT_EXIST_COIN_TYPE = "20002";
	
	/**
	 * 无权访问接口
	 */
	public final static String NOT_AUTH_ACCESS_INTERFACE = "2011";
	
	public final static String PAY_PROCESSING  = "2012";
	public final static String USER_CONFIG_ERROR  = "2013";
	public final static String LOWEST_EXRACT_ERROR  = "2014";
	public final static String INVALID_ADDRESS  = "2015";
	public final static String MERCHANT_SETTING_ERROR  = "2016";
	public final static String PAY_INFO_ERROR  = "2017";
	public final static String APP_NAME_DUPLICATE  = "2018";
	public static HashMap<String, String> EXCEPTION_MESSAGE = new HashMap<>();

	/**
	 * 调用微信服务错误
	 */
	public static final String WEIXIN_HTTP_ERROR = "60003";

	/**
	 * 提币金额异常
	 */
	public final static String EXTRACT_MONEY_ERROR = "50001";
	/**
	 * 提币的地址不存在
	 */
	public final static String EXTRACT_FROM_ADDRESS_NOT_EXIST = "50002";
	/**
	 * 签名错误
	 */
	public final static String GO_TO_SIGN_ERROR = "50003";

	/**
	 * 获取地址失败
	 */
	public final static String GET_ADDRESS_FAIL = "50004";

	/**
	 * 访问太频繁
	 */
	public final static String VISIT_TOO_FREQUENTLY = "50005";

	/**
	 * 服务调用失败
	 */
	public static final String HTTP_FAIL_CODE = "9997";

	/**
	 *  业务ID不能为空
	 */
	public final static String BUSINESS_TXID_NOT_IS_EMPTY = "50010";

	public final static String BUSINESS_GET_NONCE_FAIL = "50006";

	public final static String ORDER_NOT_MEET_CONDITION = "50007";

	public final static String ORDER_SEND_FAIL = "50008";

	public final static String NOTIFY_WALLET_FAIL = "50009";

	public final static String ORDER_ID_ERROR  = "50011";
	
	public final static String CHECKOUT_COIN_BALANCE_INSUFFICIENT = "50012";
	
	public final static String MERCHANT_EXCEPTION_USER = "50013";
	
	public final static String MERCHANT_EXCEPTION_INVITED_CODE = "50014";
	
	public final static String EXTRACT_APPROVE_STATUS_ERROR = "50015";

	public final static String ORDER_STATUS_ERROR  = "50016";


	

	/**************游戏相关*******************************/
	public static final String GAME_IS_NOT_EXIST = "60001";
	public static final String GAME_PARAMS_ERROR = "60002";
	public static final String GAME_ANSWER_ERROR = "60003";
	public static final String GAME_NOT_IN_AWARD_TIME = "60004";
	public static final String GAME_REPEAT_THE_AWARD = "60005";
	public static final String GAME_AWARD_HAS_BEEN_RECEIVED = "60006";
	
	
	/**************理财产品*******************************/
	public static final String FIXED_TIME_DEPOSIT_MIN_NUM_ERROR = "70001";
	public static final String FIXED_TIME_DEPOSIT_MAX_NUM_ERROR = "70002";
	public static final String FIXED_TIME_DEPOSIT_NOT_BALANCE_ERROR = "70003";
	
	/*************************优惠券******************************/
    public static final String COUPONS_CURRENT_ACTIVITY_END_ERROR = "80001";
    public static final String COUPONS_NOT_OVERLAY_ERROR = "80002";
	public static final String ACTIVITY_NOT_EXIST = "80003";
	public static final String ACTIVITY_PRODUCT_NOT_EXIST = "80004";
	public static final String ACTIVITY_ORDER_NOT_EXIST = "80005";
	public static final String OVER_ACTIVITY_NUMBER_LIMIT = "80006";
	public static final String YOU_CAN_NOT_CUT_AGAIN = "80007";
	public static final String GROUP_BOOKING_IS_OVER = "80008";
	public static final String OUT_OF_GROUP_BOOKING_PEOPLE_LIMIT = "80009";
	public static final String ORDER_NOT_EXIST = "80010";
	public static final String YOUR_OPENID_NEED_WECHAT_AUTHORIZATION = "80011";

	public static final String THIS_ACTIVITY_IS_UNDER_WAY = "80012";
	public static final String YOU_CAN_NOT_JOIN_AGAIN = "80013";

	public static final String ACTIVITY_OFFLINE = "800013";

	/*************************flyby open api******************************/
	public static final String OPEN_API_PUBLIC_KEY_NOT_SET = "90001";

	/**
	 * 微信获取openId失败
	 */
	public static final String GET_WECHAT_OPENID_FAIL = "90000";

	/**
	 * 获取微信二维码失败
	 */
	public static final String GET_WECHAT_QECODE_FAIL = "90001";

	/**
	 *  商品相关
	 */
	public static final String HAS_CHILD_NODE = "81000";

	public static final String CATEGOR_HAS_PRODUCT = "81001";

	static {
        EXCEPTION_MESSAGE.put(SYSTEM_ERROR,"System error，Please try again later");
        EXCEPTION_MESSAGE.put(SUCCESS_CODE, "The call is successful");
        EXCEPTION_MESSAGE.put(REQUEST_PARAM_ERROR, "request param error");
        EXCEPTION_MESSAGE.put(SYSTEM_DIRTY_DATA_ERROR, "dirty data ,please refresh data");
        EXCEPTION_MESSAGE.put(USER_IS_NOT_EXIST, "user is not exist");
        EXCEPTION_MESSAGE.put(LOGIN_FAIL_LOCK, "login error too many times, please operation later");
        EXCEPTION_MESSAGE.put(LOGIN_FAIL_PASSWORD_ERROR, "password error");
        EXCEPTION_MESSAGE.put(TOKEN_VERIFY_FAIL, "token verify fail");
        EXCEPTION_MESSAGE.put(APP_AUTH_VALIDATE_SIGN_ERROR, "sign verify fail");
        EXCEPTION_MESSAGE.put(USER_IS_EXIST, "user is exist");
        EXCEPTION_MESSAGE.put(REAL_NAME_AUTH_FIAL, "real name auth fail");
        EXCEPTION_MESSAGE.put(THIRD_PARTY_SERVICE_TIMEOUT, "third party service call timeout");
        EXCEPTION_MESSAGE.put(PAY_PASSWORD_LOCK, "pay password locked");
        EXCEPTION_MESSAGE.put(PAY_PASSWORD_ERROR, "pay password error");
        EXCEPTION_MESSAGE.put(BALANCE_NOT_ENOUGH, "balance not enough");
        EXCEPTION_MESSAGE.put(WALLETACCOUNT_EXIST, "wallet account exist");
        EXCEPTION_MESSAGE.put(PAY_FAILED, "pay failed");
        EXCEPTION_MESSAGE.put(RECEIVE_USER_NOT_EXIST, "receive user not exist");
        EXCEPTION_MESSAGE.put(USER_NOT_LOGIN, "user is not login");
        EXCEPTION_MESSAGE.put(CHECKOUT_ORDER_ID_DUPLICATE, "checkout order id is duplicate");
        EXCEPTION_MESSAGE.put(DEPOSIT_ERROR, "deposit error");
        EXCEPTION_MESSAGE.put(CHECKOUT_ORDER_ID_ERROR, "order id is not exist");
        EXCEPTION_MESSAGE.put(REFUND_AMOUNT_MORETHAN_ORDER_AMOUNT, "The refund amount is larger than the order amount.");
        EXCEPTION_MESSAGE.put(SIGN_ERROR, "sign error");
        EXCEPTION_MESSAGE.put(PAYMENT_TIMEOUT, "payment timeout");
        EXCEPTION_MESSAGE.put(REFUND_STATUS_ERROR, "refund status error");
        EXCEPTION_MESSAGE.put(ERROR_AMOUNT_OF_MONEY, "error amount of money");
        EXCEPTION_MESSAGE.put(WXPAY_ERROR, "wxpay error");
        EXCEPTION_MESSAGE.put(RECORD_NOT_EXIST, "record is not exist");
        EXCEPTION_MESSAGE.put(NOT_SUPPORT_COIN_NETWORK, "not support coin network");
        EXCEPTION_MESSAGE.put(ALREADYPAID, "order already paid");
        EXCEPTION_MESSAGE.put(ILLEGAL_PARAMS, "illegal params");
        EXCEPTION_MESSAGE.put(NOT_AUTH_ACCESS_INTERFACE, "not auth access interface");
        EXCEPTION_MESSAGE.put(PAY_PROCESSING, "order processing,wait for moment");
        EXCEPTION_MESSAGE.put(USER_CONFIG_ERROR, "config error");
        EXCEPTION_MESSAGE.put(AUTH_CODE_ERROR, "auth code error");
        EXCEPTION_MESSAGE.put(LOWEST_EXRACT_ERROR, "extract amount lower than limit");
        EXCEPTION_MESSAGE.put(INVALID_ADDRESS, "invalid address");
        EXCEPTION_MESSAGE.put(MERCHANT_SETTING_ERROR, "merchant setting error");
        EXCEPTION_MESSAGE.put(PAY_INFO_ERROR, "pay info error");
        EXCEPTION_MESSAGE.put(APP_NAME_DUPLICATE, "app name duplicate");
		EXCEPTION_MESSAGE.put(TITLE_IS_EXIST, "title is exist");
		EXCEPTION_MESSAGE.put(LACK_MUST_PARAMETER, "lack must parameter");
		EXCEPTION_MESSAGE.put(WALLETACCOUNT_NOT_EXIST, "wallet account not exist");
		EXCEPTION_MESSAGE.put(LOCK_BALANCE_FAIL, "wallet account lock balance fail");
		EXCEPTION_MESSAGE.put(LOCK_BALANCE_NOT_ENOUGH, "lock balance not enough");
		EXCEPTION_MESSAGE.put(UNLOCK_BALANCE_FAIL, "wallet account unlock balance fail");
		EXCEPTION_MESSAGE.put(DEDUCT_LOCK_BALANCE_FAIL, "wallet account deduct lock balance fail");
		EXCEPTION_MESSAGE.put(LOCK_BALANCE_TYPE_MISMATCH, "lock balance type mismatch");
		EXCEPTION_MESSAGE.put(COIN_PAIR_CONFIG_CLOSE, "coin pair no support for transactions");
		EXCEPTION_MESSAGE.put(EX_ORDER_NOT_EXIST, "exchange order not exist");
		EXCEPTION_MESSAGE.put(EX_ORDER_CANNOT_CANCEL, "exchange order cannot cancel");
		EXCEPTION_MESSAGE.put(EX_ORDER_CANCEL_FAIL, "exchange order cancel fail");
		EXCEPTION_MESSAGE.put(EX_ORDER_CREATE_FAIL, "exchange order create fail");
		EXCEPTION_MESSAGE.put(GET_CLOUD_USER_MANAGE_ID_ERROR, "get cloud manage userId fail");
		EXCEPTION_MESSAGE.put(NOT_SUPPORT_EXTRACT_COIN_TYPE, "not support extract coin type");
		EXCEPTION_MESSAGE.put(CLOUD_USER_MANAGE_IS_EXIST, "cloud user manage is exist");
		EXCEPTION_MESSAGE.put(IP_ILLEGAL, "ip illegal");
		EXCEPTION_MESSAGE.put(API_KEY_ERROR, "api key error");
		EXCEPTION_MESSAGE.put(SECRET_KEY_ERROR, "secrect key error");
		EXCEPTION_MESSAGE.put(USER_LOCK, "user lock");
		EXCEPTION_MESSAGE.put(CLOUD_USER_CHECK_IP_ERROR, "CLOUD_USER_CHECK_IP_ERROR");
		EXCEPTION_MESSAGE.put(ADD_EXCHANGE_BALANCE_FAIL, "add exchange balance fail");
		EXCEPTION_MESSAGE.put(REPEAT_OPERATION_BALANCE, "repeat operation balance");
		EXCEPTION_MESSAGE.put(HUOBI_ACCOUNT_NOT_FIND, "huobi account not find");
		EXCEPTION_MESSAGE.put(HUOBI_ACCOUNT_STATE_UNUSABLE, "huobi account state unusable");
		EXCEPTION_MESSAGE.put(HUOBI_CREATE_ORDER_FAIL, "huobi create order fail");
		EXCEPTION_MESSAGE.put(EXTRACT_MONEY_ERROR, "extract money error");
		EXCEPTION_MESSAGE.put(EXTRACT_FROM_ADDRESS_NOT_EXIST, "extract from address not exist");
		EXCEPTION_MESSAGE.put(GO_TO_SIGN_ERROR, "go to sign error");
		EXCEPTION_MESSAGE.put(USER_AUTH_ERROR, "user auth error");

		EXCEPTION_MESSAGE.put(GET_ADDRESS_FAIL, "get address fail");
		EXCEPTION_MESSAGE.put(NOT_EXIST_COIN_TYPE, "not exist coin type");
		EXCEPTION_MESSAGE.put(VISIT_TOO_FREQUENTLY, "visit too frequently");
        EXCEPTION_MESSAGE.put(BUSINESS_GET_NONCE_FAIL, "get nonce fail");
        EXCEPTION_MESSAGE.put(ORDER_NOT_MEET_CONDITION, "order not meet condition, confirmed greater than zero");
        EXCEPTION_MESSAGE.put(ORDER_SEND_FAIL, "order send fail");
        EXCEPTION_MESSAGE.put(NOTIFY_WALLET_FAIL, "notify wallet fail");
        EXCEPTION_MESSAGE.put(CHECKOUT_COIN_BALANCE_INSUFFICIENT, "当前币种流通量不足，不能进行支付");
        EXCEPTION_MESSAGE.put(GAME_IS_NOT_EXIST,"Game is not exist");
        EXCEPTION_MESSAGE.put(GAME_PARAMS_ERROR,"Game params error");
        EXCEPTION_MESSAGE.put(MERCHANT_EXCEPTION_USER, "异常用户，请联系平台管理员");
        EXCEPTION_MESSAGE.put(MERCHANT_EXCEPTION_INVITED_CODE, "invited code error");
        EXCEPTION_MESSAGE.put(EXTRACT_APPROVE_STATUS_ERROR, "EXTRACT APPROVE STATUS ERROR");
		EXCEPTION_MESSAGE.put(GAME_ANSWER_ERROR, "answered the question today");
		EXCEPTION_MESSAGE.put(GAME_NOT_IN_AWARD_TIME, "not in award time");
		EXCEPTION_MESSAGE.put(GAME_REPEAT_THE_AWARD, "repeat the award");
		EXCEPTION_MESSAGE.put(GAME_AWARD_HAS_BEEN_RECEIVED, "The award has been received");
		
		EXCEPTION_MESSAGE.put(FIXED_TIME_DEPOSIT_MIN_NUM_ERROR, "min subsc number error");
		EXCEPTION_MESSAGE.put(FIXED_TIME_DEPOSIT_MAX_NUM_ERROR, "max subsc number error");
		EXCEPTION_MESSAGE.put(FIXED_TIME_DEPOSIT_NOT_BALANCE_ERROR, "subsc balance not enough");
		
		EXCEPTION_MESSAGE.put(COUPONS_CURRENT_ACTIVITY_END_ERROR, "End of current coupons activity");
		EXCEPTION_MESSAGE.put(COUPONS_NOT_OVERLAY_ERROR, "current coupons not overlay usage");
		EXCEPTION_MESSAGE.put(ACTIVITY_NOT_EXIST, "activity not exist");
		EXCEPTION_MESSAGE.put(ACTIVITY_PRODUCT_NOT_EXIST, "activity product not exist");
		EXCEPTION_MESSAGE.put(ACTIVITY_ORDER_NOT_EXIST, "activity order not exist");
		EXCEPTION_MESSAGE.put(OVER_ACTIVITY_NUMBER_LIMIT, "over activity number limit");
		EXCEPTION_MESSAGE.put(YOU_CAN_NOT_CUT_AGAIN, "you can not cut again");
		EXCEPTION_MESSAGE.put(GROUP_BOOKING_IS_OVER, "group booking is over");
		EXCEPTION_MESSAGE.put(OUT_OF_GROUP_BOOKING_PEOPLE_LIMIT, "out of group booking people limit");
		EXCEPTION_MESSAGE.put(ORDER_NOT_EXIST, "order not exist");
		EXCEPTION_MESSAGE.put(YOUR_OPENID_NEED_WECHAT_AUTHORIZATION, "your openid need wechat authorization");

		EXCEPTION_MESSAGE.put(OPEN_API_PUBLIC_KEY_NOT_SET, "public api public key not set");
		
		EXCEPTION_MESSAGE.put(TOKEN_VERIFY_OVERDAY,"登陆信息过期，请重新登陆");
        EXCEPTION_MESSAGE.put(TOKEN_VERIFY_EXCEPTION,"登陆信息校验失败，请重新登陆");

		EXCEPTION_MESSAGE.put(HTTP_FAIL_CODE, "服务调用失败");
		EXCEPTION_MESSAGE.put(WEIXIN_HTTP_ERROR, "调用微信服务错误");
		EXCEPTION_MESSAGE.put(CLOUD_USER_ID_IS_EMPTY, "cloud_user_id不能为空");
		EXCEPTION_MESSAGE.put(THIRD_TOKEN_IS_EMPTY, "三方token不能为空");
		EXCEPTION_MESSAGE.put(INVOK_THIRD_SERVICE_FAIL, "三方服务调用失败");
		EXCEPTION_MESSAGE.put(GET_USER_INFO_FAIL, "获取用户信息失败");
		EXCEPTION_MESSAGE.put(THIS_ACTIVITY_IS_UNDER_WAY, "此活动正在进行");
		EXCEPTION_MESSAGE.put(GET_WECHAT_OPENID_FAIL, "get wechat openId fail");
		EXCEPTION_MESSAGE.put(GET_WECHAT_QECODE_FAIL, "get wechat qecode fail");
		EXCEPTION_MESSAGE.put(YOU_CAN_NOT_JOIN_AGAIN, "you can not join again");
		EXCEPTION_MESSAGE.put(ORDER_STATUS_ERROR, "order status error");

		EXCEPTION_MESSAGE.put(HAS_CHILD_NODE, "delete fail, has child node");
		EXCEPTION_MESSAGE.put(CATEGOR_HAS_PRODUCT, "categor has product, can not delete");
		EXCEPTION_MESSAGE.put(ACTIVITY_OFFLINE, "活动已下线");

    }

	public static String getMessage(String code) {
    	String message = EXCEPTION_MESSAGE.get(code);
    	if(message == null) {
    		message = "System error，Please try again later";
    	}
        return message;
    }

}
