package com.zatgo.zup.common.interceptor;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;


@ControllerAdvice
public class GlobalExceptionHandler
{
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class.getName());

    @ExceptionHandler(value = { Throwable.class })
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseData<Object> handleCommonException(Throwable e)
            throws IOException
    {
        logger.error("handleCommonException=", e.getCause(), e);
        return BusinessResponseFactory.createSystemError();
    }

    @ExceptionHandler(value = BusinessException.class)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public ResponseData<Object> handleBusinessException(BusinessException e)
            throws IOException {
        logger.error("handleBusinessException=" + e.getMessage(), e);
        return BusinessResponseFactory.createBusinessError(e.getCode(), e.getMessage());
    }
    
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public ResponseData<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
    	logger.error("handleMethodArgumentNotValidException={}", e.getMessage(), e);
        String message = e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(";"));
        return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REQUEST_PARAM_ERROR, message);
    }
    
    @ExceptionHandler(value = {BindException.class})
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public ResponseData<Object> handleBindException(BindException e) {
    	logger.error("handleBindException={}", e.getMessage(), e);
        String message = e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(";"));
        return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REQUEST_PARAM_ERROR, message);
    }

}