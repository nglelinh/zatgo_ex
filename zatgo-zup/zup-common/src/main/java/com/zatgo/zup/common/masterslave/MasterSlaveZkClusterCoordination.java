package com.zatgo.zup.common.masterslave;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException.NoNodeException;
import org.apache.zookeeper.KeeperException.NodeExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.curator.PathUtils;
import com.zatgo.zup.common.curator.ZkClusterCoordination;


public class MasterSlaveZkClusterCoordination implements MasterSlaveClusterCoordination {

	private static final Logger logger = LoggerFactory.getLogger(MasterSlaveZkClusterCoordination.class);
	
	@Autowired
	private ZkClusterCoordination zkClusterCoordination;
	
	private String hostPort;
	
	MasterSlaveZkClusterCoordination(){
		try {
			hostPort = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (UnknownHostException e) {
			logger.error("get host name error:",e);
			throw new RuntimeException(e);
		}
	}
	@Override
	public boolean isMasterExisted() {
		try{
			return zkClusterCoordination.isNodeExisted(MasterSlaveClusterConstant.MASTER_SUBTREE, false);
		}catch(Exception e){
			logger.error("",e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean tryToLeaderMaster() {
		try {
			if(!zkClusterCoordination.isNodeExisted(MasterSlaveClusterConstant.MASTER_SUBTREE, false)){
				zkClusterCoordination.mkdirs(PathUtils.getParentPath(MasterSlaveClusterConstant.MASTER_SUBTREE), CreateMode.PERSISTENT);
			}
			zkClusterCoordination.createNode(MasterSlaveClusterConstant.MASTER_SUBTREE, hostPort.getBytes(), CreateMode.EPHEMERAL);
			return true;
		} catch (NodeExistsException e) {
			boolean is = isMasterExisted();
			if(is != true){
				logger.error("",e);
				throw new RuntimeException(e);
			}
			
		}catch(Exception e){
			logger.error("",e);
			throw new RuntimeException(e);
		}
		return false;
	}

	@Override
	public boolean isCurrentMaster() {
		try {
			
			String masterHostPort =  new String(zkClusterCoordination.getData(MasterSlaveClusterConstant.MASTER_SUBTREE, false));
			
			if(masterHostPort.equals(hostPort)){
				return true;
			}else{
				return false;
			}
		}catch(NoNodeException e){
			return false;
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean unregisterCurrentProcessFromSlave() {
		try{
			try{
				zkClusterCoordination.deleteNode(MasterSlaveClusterConstant.SLAVE_SUBTREE + MasterSlaveClusterConstant.ZK_SEPERATOR + hostPort);
			}catch(NoNodeException e){
				return true;
			}
			
			return true;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean registerCurrentProcessToSlave() {
		try{
			if(!zkClusterCoordination.isNodeExisted(MasterSlaveClusterConstant.SLAVE_SUBTREE, false)){
				zkClusterCoordination.mkdirs(MasterSlaveClusterConstant.SLAVE_SUBTREE, CreateMode.PERSISTENT);
			}
			zkClusterCoordination.createNode(
					MasterSlaveClusterConstant.SLAVE_SUBTREE + 
					MasterSlaveClusterConstant.ZK_SEPERATOR + 
					hostPort, null, CreateMode.EPHEMERAL);
			
			return true;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

}
