package com.zatgo.zup.common.encrypt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zatgo.zup.common.enumtype.ClientTypeEnum;

/**
 * RSA私钥解密工具，服务端
 * @author chenlei
 *
 */
public class RSAServerUtil {
	
		protected static Logger logger = LoggerFactory.getLogger(RSAServerUtil.class);
		
		public static final String PRIVATE_KEY = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJxiHXHapduKZ3Fy"
				+"79LgKXKMySx/1vEZgweBdV+nEL7sDJ4ZMH6YerV5mv6mzyQFUfu335lE5KbxChTR"
				+"3Roiblyz5Jjk+GPCqZJHDUulAVU/jUZvye8Stdel+zb1pY0MgPwHl8yCU13f3AFa"
				+"9fTurJU/MNhCzojssRIr+CAhhuCdAgMBAAECgYBdMT8YzVLPrQAOJI90nG82u11b"
				+"bmeZ0Dk8bNKUiQu6qxKt2JcEqjteEDTkkmWYsq7zkdTnEp4sOjJmy9W2MFSRbYBT"
				+"DVtqdk1R5KyTtc1JDorPxSk7u5DBoWou7ECbEABKWLlRgelZFDky0+XG5Gvz5D+u"
				+"VcOrClh5hD20r31RgQJBAMxCQ/4sk//njprjf+zq6lDp8hzgbjb6ygy7cXRko0V/"
				+"lI839S3ljYvqNHR7LVyIuSvMa6s9UgJr3ghfhUqvNGECQQDD/zk1mHfyeon0lAjO"
				+"0eGLISriXXxX5Wp3fHr6170JVp10uA4OB926ldKBC7cmTp8bkal4fZqJKB7j2u8W"
				+"aVW9AkBVDS60sqk5wJivxBUreLHy2Uc4YPdjq0/gt1hJp/IYaURZ0mokH1auMOqA"
				+"qh28Ty4Tn74hgpoYagP9JpEhBQ1hAkAYhe6ajiEz7bElMQCJICmgRt4exKOyO7o+"
				+"iQy6JhQ4/uVknULk7ocHuYdFLjoUSPIPeQJDWTPkxWhYCiSwIS3hAkEAk5n7CVss"
				+"jZyVp/s+iEHTXMkLvsMf1bNQxUPmZ9tpzKv3I4kuIXiVtwkfUS0ujDbRFRUUexnP"
				+"t2ezBV+2Wy4m+g==";
		
	    /**  
	     * 私钥  
	     */    
	    private static RSAPrivateKey privateKey;    
	    
	    /**
	     * 载入私钥
	     */
	    static{
	    	try {
	    		if(privateKey==null){
	    			loadPrivateKey(PRIVATE_KEY);
	    		}
			} catch (Exception e) {
				logger.error("载入私钥失败",e);
			}
	    }
	    
	    
	    /**  
	     * 获取私钥  
	     * @return 当前的私钥对象  
	     */    
	    public RSAPrivateKey getPrivateKey() {    
	        return privateKey;    
	    }    
	    
	    /**  
	     * 从文件中加载私钥  
	     * @param keyFileName 私钥文件名  
	     * @return 是否成功  
	     * @throws Exception   
	     */    
	    public static void loadPrivateKey(InputStream in) throws Exception{    
	        try {    
	            BufferedReader br= new BufferedReader(new InputStreamReader(in));    
	            String readLine= null;    
	            StringBuilder sb= new StringBuilder();    
	            while((readLine= br.readLine())!=null){    
	                if(readLine.charAt(0)=='-'){    
	                    continue;    
	                }else{    
	                    sb.append(readLine);    
	                    sb.append('\r');    
	                }    
	            }    
	            loadPrivateKey(sb.toString());    
	        } catch (IOException e) {    
	            throw new Exception("私钥数据读取错误");    
	        } catch (NullPointerException e) {    
	            throw new Exception("私钥输入流为空");    
	        }    
	    }    
	    
	    public static void loadPrivateKey(String privateKeyStr) throws Exception{    
	        try {    
	        	Base64 base64Decoder= new Base64();    
	            byte[] buffer= base64Decoder.decodeBase64(privateKeyStr); 
	            PKCS8EncodedKeySpec keySpec= new PKCS8EncodedKeySpec(buffer);    
	            KeyFactory keyFactory= KeyFactory.getInstance("RSA");    
	            privateKey= (RSAPrivateKey) keyFactory.generatePrivate(keySpec);    
	        } catch (NoSuchAlgorithmException e) {    
	            throw new Exception("无此算法");    
	        } catch (InvalidKeySpecException e) {    
	            e.printStackTrace();  
	            throw new Exception("私钥非法");    
	        } catch (NullPointerException e) {    
	            throw new Exception("私钥数据为空");    
	        }    
	    }    
	    
	    /**  
	     * 解密过程  
	     * @param privateKey 私钥  
	     * @param cipherData 密文数据  
	     * @return 明文  
	     * @throws Exception 解密过程中的异常信息  
	     */    
	    public static byte[] decrypt(RSAPrivateKey privateKey, byte[] cipherData,ClientTypeEnum clientType) throws Exception{    
	        if (privateKey== null){    
	            throw new Exception("解密私钥为空, 请设置");    
	        }    
	        Cipher cipher= null;    
	        try {    
	        	if(ClientTypeEnum.PHONE.equals(clientType)){
	        		cipher= Cipher.getInstance("RSA",new BouncyCastleProvider());
	        	}else{
	        		cipher= Cipher.getInstance("RSA");
	        	}
	            cipher.init(Cipher.DECRYPT_MODE, privateKey);    
	            byte[] output= cipher.doFinal(cipherData);    
	            return output;    
	        } catch (NoSuchAlgorithmException e) {    
	            throw new Exception("无此解密算法");    
	        } catch (NoSuchPaddingException e) {    
	        	throw new Exception("RSA解密异常");  
	        }catch (InvalidKeyException e) {    
	            throw new Exception("解密私钥非法,请检查");    
	        } catch (IllegalBlockSizeException e) {    
	            throw new Exception("密文长度非法");    
	        } catch (BadPaddingException e) {    
	            throw new Exception("密文数据已损坏");    
	        }           
	    }    
	    
	    /**
	     * 解密入口
	     * @param cipherData
	     * @param clientType
	     * @return
	     * @throws Exception
	     */
	    public static String decrypt(String cipherData,ClientTypeEnum clientType) throws Exception{    
	    	byte[] plainText = decrypt(privateKey,Base64.decodeBase64(cipherData),clientType);  
	    	return new String(plainText);
	    	
	    } 
	    
//	    public static void main(String[] args){
//
//
//	    }
}
