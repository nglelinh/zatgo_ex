package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@ApiModel(value = "支付接口参数")
public class OrderPayParams implements Serializable {

	@ApiModelProperty(value = "收款用户",required = true)
	private String receiptUserId;
	@ApiModelProperty(value = "收款方名称",required = true)
	private String receiptUserName;
	@ApiModelProperty(value = "金额",required = true)
	private BigDecimal amount;
	@ApiModelProperty(value = "支付密码",required = true)
	private String payPassword;
	@ApiModelProperty(value = "币种网络类型",required = true,allowableValues = "QTUM,BTC,ETH")
	private String networkType;
	@ApiModelProperty(value = "币种类型",required = true,allowableValues = "ZAT,QTUM,BTC,ETH")
	private String coinType;
	@ApiModelProperty(value = "签名",required = true)
	private String sign;
	@ApiModelProperty(value = "付款用户名",required = false)
	private String payUserName;
	@ApiModelProperty(value = "人民币支付金额",required = false)
	private BigDecimal cashAmount;
	@ApiModelProperty(value = "第三方支付渠道0微信1支付宝",required = false)
	private Byte thirdPayType;
	@ApiModelProperty(value = "客户端类型0:h5， 1:app",required = true)
	private Byte clientType;
	@ApiModelProperty(value = "收银台订单号",required = false)
	private String checkoutOrderId;
	private String clientIp;
	@ApiModelProperty(value = "第三方系统中的人民币订单原价",required = true)
	private BigDecimal originalPrice;

	public BigDecimal getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}

	public String getReceiptUserName() {
		return receiptUserName;
	}

	public void setReceiptUserName(String receiptUserName) {
		this.receiptUserName = receiptUserName;
	}

	public Byte getClientType() {
		return clientType;
	}

	public void setClientType(Byte clientType) {
		this.clientType = clientType;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getCheckoutOrderId() {
		return checkoutOrderId;
	}

	public void setCheckoutOrderId(String checkoutOrderId) {
		this.checkoutOrderId = checkoutOrderId;
	}

	public Byte getThirdPayType() {
		return thirdPayType;
	}

	public void setThirdPayType(Byte thirdPayType) {
		this.thirdPayType = thirdPayType;
	}

	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public String getPayUserName() {
		return payUserName;
	}

	public void setPayUserName(String payUserName) {
		this.payUserName = payUserName;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getReceiptUserId() {
		return receiptUserId;
	}

	public void setReceiptUserId(String receiptUserId) {
		this.receiptUserId = receiptUserId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPayPassword() {
		return payPassword;
	}

	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}
}
