package com.zatgo.zup.common.masterslave;

public class MasterSlaveClusterConstant {
	
	public static final String ZK_SEPERATOR = "/";
	private static final String ZUP_MASTER_ROOT = "zup_master";
	private static final String ZUP_SLAVE_ROOT = "zup_slave";

	public static final String MASTER_SUBTREE;
	public static final String SLAVE_SUBTREE;
	
	static{
		MASTER_SUBTREE = ZK_SEPERATOR + ZUP_MASTER_ROOT;
		SLAVE_SUBTREE = ZK_SEPERATOR + ZUP_SLAVE_ROOT;
	}
}
