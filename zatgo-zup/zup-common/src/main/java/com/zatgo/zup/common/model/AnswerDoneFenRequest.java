package com.zatgo.zup.common.model;

import java.util.Date;

/**
 * Created by 46041 on 2019/2/22.
 */
public class AnswerDoneFenRequest {

    private Date startDate;

    private Date endDate;

    private String gameId;

    private Integer count;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
