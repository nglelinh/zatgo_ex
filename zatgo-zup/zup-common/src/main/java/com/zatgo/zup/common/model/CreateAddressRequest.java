package com.zatgo.zup.common.model;

/**
 * Created by 46041 on 2018/10/30.
 */
public class CreateAddressRequest {

    private Integer num;

    private String password;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
