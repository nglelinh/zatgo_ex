package com.zatgo.zup.common.model;

import java.io.Serializable;

public class UserData  implements Serializable {

	private String userId;

	private String userName;

	private String userPassword;

	private String userPayPassword;

	private String mnemonicWord;

	private String inviteCode;

	private String invitedCode;

	private String emailCountryCode;

	private String email;

	private String mobile;

	private String mobileCountryCode;

	private Byte registType;

	private String cloudUserId;

	private Byte isCloudManager;

	private String iconUrl;

	private String nickname;
	
	private Boolean isLockLogin;

    private Boolean isLockPay;

    private Boolean isLockWithdraw;

    private Byte userGrade;

    private String wechatOpenid;

	public Boolean getIsLockLogin() {
		return isLockLogin;
	}

	public void setIsLockLogin(Boolean isLockLogin) {
		this.isLockLogin = isLockLogin;
	}

	public Boolean getIsLockPay() {
		return isLockPay;
	}

	public void setIsLockPay(Boolean isLockPay) {
		this.isLockPay = isLockPay;
	}

	public Boolean getIsLockWithdraw() {
		return isLockWithdraw;
	}

	public void setIsLockWithdraw(Boolean isLockWithdraw) {
		this.isLockWithdraw = isLockWithdraw;
	}

	public Byte getUserGrade() {
		return userGrade;
	}

	public void setUserGrade(Byte userGrade) {
		this.userGrade = userGrade;
	}

	public String getWechatOpenid() {
		return wechatOpenid;
	}

	public void setWechatOpenid(String wechatOpenid) {
		this.wechatOpenid = wechatOpenid;
	}

	public String getMnemonicWord() {
		return mnemonicWord;
	}

	public void setMnemonicWord(String mnemonicWord) {
		this.mnemonicWord = mnemonicWord;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPayPassword() {
		return userPayPassword;
	}

	public void setUserPayPassword(String userPayPassword) {
		this.userPayPassword = userPayPassword;
	}

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	public String getInvitedCode() {
		return invitedCode;
	}

	public void setInvitedCode(String invitedCode) {
		this.invitedCode = invitedCode;
	}

	public String getEmailCountryCode() {
		return emailCountryCode;
	}

	public void setEmailCountryCode(String emailCountryCode) {
		this.emailCountryCode = emailCountryCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobileCountryCode() {
		return mobileCountryCode;
	}

	public void setMobileCountryCode(String mobileCountryCode) {
		this.mobileCountryCode = mobileCountryCode;
	}

	public Byte getRegistType() {
		return registType;
	}

	public void setRegistType(Byte registType) {
		this.registType = registType;
	}

	public String getCloudUserId() {
		return cloudUserId;
	}

	public void setCloudUserId(String cloudUserId) {
		this.cloudUserId = cloudUserId;
	}

	public Byte getIsCloudManager() {
		return isCloudManager;
	}

	public void setIsCloudManager(Byte isCloudManager) {
		this.isCloudManager = isCloudManager;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
}
