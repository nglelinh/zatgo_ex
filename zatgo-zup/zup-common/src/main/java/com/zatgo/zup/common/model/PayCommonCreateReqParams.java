package com.zatgo.zup.common.model;

import java.io.Serializable;

public class PayCommonCreateReqParams implements Serializable {

	private String appId;

	private String signCharset;

	private String signType;

	private String sign;

	private String timestamp;

	private String version;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getSignCharset() {
		return signCharset;
	}

	public void setSignCharset(String signCharset) {
		this.signCharset = signCharset;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
