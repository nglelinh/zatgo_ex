package com.zatgo.zup.common.model;



public class Commands {

	public final static String ADD_MULTI_SIG_ADDRESS = "addmultisigaddress";
	public final static String BACKUP_WALLET = "backupwallet";
	public final static String CREATE_MULTI_SIG  = "createmultisig";
	public final static String CREATE_RAW_TRANSACTION  = "createrawtransaction";
	public final static String DECODE_RAW_TRANSACTION  = "decoderawtransaction";
	public final static String DECODE_SCRIPT  = "decodescript";
	public final static String DUMP_PRIV_KEY  = "dumpprivkey";
	public final static String DUMP_WALLET  = "dumpwallet";
	public final static String ENCRYPT_WALLET  = "encryptwallet";
	public final static String ESTIMATE_FEE  = "estimatefee";
	public final static String GET_ACCOUNT  = "getaccount";
	public final static String GET_ACCOUNT_ADDRESS  = "getaccountaddress";
	public final static String GET_ADDRESSES_BY_ACCOUNT  = "getaddressesbyaccount";
	public final static String GET_BALANCE  = "getbalance";
	public final static String GET_BEST_BLOCK_HASH  = "getbestblockhash";
	public final static String GET_BLOCK = "getblock";
	public final static String GET_BLOCK_COUNT  = "getblockcount";
	public final static String GET_BLOCK_HASH  = "getblockhash";
	public final static String GET_DIFFICULTY  = "getdifficulty";
	public final static String GET_GENERATE  = "getgenerate";
	public final static String GET_HASHES_PER_SEC = "gethashespersec";
	public final static String GET_INFO  = "getinfo";
	public final static String GET_MEM_POOL_INFO  = "getmempoolinfo";
	public final static String GET_MINING_INFO  = "getmininginfo";
	public final static String GET_NEW_ADDRESS  = "getnewaddress";
	public final static String GET_PEER_INFO  = "getpeerinfo";
	public final static String GET_RAW_CHANGE_ADDRESS  = "getrawchangeaddress";
	public final static String GET_RAW_TRANSACTION = "getrawtransaction";
	public final static String GET_RECEIVED_BY_ACCOUNT  = "getreceivedbyaccount";
	public final static String GET_RECEIVED_BY_ADDRESS  = "getreceivedbyaddress";
	public final static String GET_TRANSACTION  = "gettransaction";
	public final static String GET_UNCONFIRMED_BALANCE  = "getunconfirmedbalance";
	public final static String GET_WALLET_INFO  = "getwalletinfo";
	public final static String IMPORT_ADDRESS  = "importaddress";
	public final static String IMPORT_PRIV_KEY = "importprivkey";
	public final static String IMPORT_WALLET  = "importwallet";
	public final static String KEY_POOL_REFILL  = "keypoolrefill";
	public final static String LIST_ACCOUNTS  = "listaccounts";
	public final static String LIST_ADDRESS_GROUPINGS  = "listaddressgroupings";
	public final static String LIST_LOCK_UNSPENT  = "listlockunspent";
	public final static String LIST_RECEIVED_BY_ACCOUNT  = "listreceivedbyaccount";
	public final static String LIST_RECEIVED_BY_ADDRESS  = "listreceivedbyaddress";
	public final static String LIST_SINCE_BLOCK  = "listsinceblock";
	public final static String LIST_TRANSACTIONS  = "listtransactions";
	public final static String LIST_UNSPENT  = "listunspent";
	public final static String LOCK_UNSPENT  = "lockunspent";
	public final static String MOVE = "move";
	public final static String PING  = "ping";
	public final static String SET_ACCOUNT  = "setaccount";
	public final static String SET_GENERATE  = "setgenerate";
	public final static String SET_TX_FEE  = "settxfee";
	public final static String SEND_FROM  = "sendfrom";
	public final static String SEND_MANY  = "sendmany";
	public final static String SEND_RAW_TRANSACTION  = "sendrawtransaction";
	public final static String SEND_TO_ADDRESS  = "sendtoaddress";
	public final static String SIGN_MESSAGE  = "signmessage";
	public final static String SIGN_RAW_TRANSACTION  = "signrawtransaction";
	public final static String STOP  = "stop";
	public final static String VALIDATE_ADDRESS  = "validateaddress";
	public final static String VERIFY_MESSAGE  = "verifymessage";
	public final static String WALLET_LOCK  = "walletlock";
	public final static String WALLET_PASSPHRASE  = "walletpassphrase";
	public final static String WALLET_PASSPHRASE_CHANGE  = "walletpassphrasechange";
	public final static String CALL_CONTRACT  = "callcontract";
	public final static String SENT_TO_CONTRACT  = "sendtocontract";
	public final static String SEARCH_LOGS  = "searchlogs";
	public final static String GET_HEX_ADDRESS  = "gethexaddress";
	public final static String FROM_HEX_ADDRESS  = "fromhexaddress";
	public final static String GET_TRANSACTION_RECEIPT = "gettransactionreceipt";


	//usdt
	public final static String GET_USDT_TRANSACTION = "omni_gettrade";

}