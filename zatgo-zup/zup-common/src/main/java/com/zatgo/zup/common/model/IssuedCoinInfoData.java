package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class IssuedCoinInfoData implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private String recordId;

    private String coinFullName;

    private String coinShortName;

    private String coinSymbol;

    private Integer coinDecimals;

    private Long issuedNumber;

    private String issuedWay;

    private Date issuedDatetime;

    private String coinNetworkType;

    private String coinType;

    private String coinImage;

    private BigDecimal marketPriceCny;

    /**
     * 0: 百分比
            1: 固定值
     */
    private Byte sysTradingFeeType;

    /**
     * 百分比：0.3------> 代表收取0.3%的手续费
            固定值：0.01---->代表收到0.01个币为手续费
     */
    private BigDecimal sysTradingFee;

    private BigDecimal minerTradingFee;

    private BigDecimal lowestExtractNumber;

    private Byte isAutoGatherCoin;

    private BigDecimal upperLimitAmount;
    
    private String walletMainAcct;

    private String walletExtractAddress;

    private String gatherMainAddress;

    private Boolean isCloseDeposit;

    private Boolean isCloseWithdraw;


    public String getWalletMainAcct() {
		return walletMainAcct;
	}

	public void setWalletMainAcct(String walletMainAcct) {
		this.walletMainAcct = walletMainAcct;
	}


	public String getWalletExtractAddress() {
		return walletExtractAddress;
	}

	public void setWalletExtractAddress(String walletExtractAddress) {
		this.walletExtractAddress = walletExtractAddress;
	}

	public String getGatherMainAddress() {
		return gatherMainAddress;
	}

	public void setGatherMainAddress(String gatherMainAddress) {
		this.gatherMainAddress = gatherMainAddress;
	}

	public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCoinFullName() {
        return coinFullName;
    }

    public void setCoinFullName(String coinFullName) {
        this.coinFullName = coinFullName;
    }

    public String getCoinShortName() {
        return coinShortName;
    }

    public void setCoinShortName(String coinShortName) {
        this.coinShortName = coinShortName;
    }

    public String getCoinSymbol() {
        return coinSymbol;
    }

    public void setCoinSymbol(String coinSymbol) {
        this.coinSymbol = coinSymbol;
    }

    public Integer getCoinDecimals() {
        return coinDecimals;
    }

    public void setCoinDecimals(Integer coinDecimals) {
        this.coinDecimals = coinDecimals;
    }

    public Long getIssuedNumber() {
        return issuedNumber;
    }

    public void setIssuedNumber(Long issuedNumber) {
        this.issuedNumber = issuedNumber;
    }

    public String getIssuedWay() {
        return issuedWay;
    }

    public void setIssuedWay(String issuedWay) {
        this.issuedWay = issuedWay;
    }

    public Date getIssuedDatetime() {
        return issuedDatetime;
    }

    public void setIssuedDatetime(Date issuedDatetime) {
        this.issuedDatetime = issuedDatetime;
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getCoinImage() {
        return coinImage;
    }

    public void setCoinImage(String coinImage) {
        this.coinImage = coinImage;
    }

    public BigDecimal getMarketPriceCny() {
        return marketPriceCny;
    }

    public void setMarketPriceCny(BigDecimal marketPriceCny) {
        this.marketPriceCny = marketPriceCny;
    }

    public Byte getSysTradingFeeType() {
        return sysTradingFeeType;
    }

    public void setSysTradingFeeType(Byte sysTradingFeeType) {
        this.sysTradingFeeType = sysTradingFeeType;
    }

    public BigDecimal getSysTradingFee() {
        return sysTradingFee;
    }

    public void setSysTradingFee(BigDecimal sysTradingFee) {
        this.sysTradingFee = sysTradingFee;
    }

    public BigDecimal getMinerTradingFee() {
        return minerTradingFee;
    }

    public void setMinerTradingFee(BigDecimal minerTradingFee) {
        this.minerTradingFee = minerTradingFee;
    }

    public BigDecimal getLowestExtractNumber() {
        return lowestExtractNumber;
    }

    public void setLowestExtractNumber(BigDecimal lowestExtractNumber) {
        this.lowestExtractNumber = lowestExtractNumber;
    }

    public Byte getIsAutoGatherCoin() {
        return isAutoGatherCoin;
    }

    public void setIsAutoGatherCoin(Byte isAutoGatherCoin) {
        this.isAutoGatherCoin = isAutoGatherCoin;
    }

    public BigDecimal getUpperLimitAmount() {
        return upperLimitAmount;
    }

    public void setUpperLimitAmount(BigDecimal upperLimitAmount) {
        this.upperLimitAmount = upperLimitAmount;
    }

    public Boolean getCloseDeposit() {
        return isCloseDeposit;
    }

    public void setCloseDeposit(Boolean closeDeposit) {
        isCloseDeposit = closeDeposit;
    }

    public Boolean getCloseWithdraw() {
        return isCloseWithdraw;
    }

    public void setCloseWithdraw(Boolean closeWithdraw) {
        isCloseWithdraw = closeWithdraw;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        IssuedCoinInfoData other = (IssuedCoinInfoData) that;
        return (this.getRecordId() == null ? other.getRecordId() == null : this.getRecordId().equals(other.getRecordId()))
            && (this.getCoinFullName() == null ? other.getCoinFullName() == null : this.getCoinFullName().equals(other.getCoinFullName()))
            && (this.getCoinShortName() == null ? other.getCoinShortName() == null : this.getCoinShortName().equals(other.getCoinShortName()))
            && (this.getCoinSymbol() == null ? other.getCoinSymbol() == null : this.getCoinSymbol().equals(other.getCoinSymbol()))
            && (this.getCoinDecimals() == null ? other.getCoinDecimals() == null : this.getCoinDecimals().equals(other.getCoinDecimals()))
            && (this.getIssuedNumber() == null ? other.getIssuedNumber() == null : this.getIssuedNumber().equals(other.getIssuedNumber()))
            && (this.getIssuedWay() == null ? other.getIssuedWay() == null : this.getIssuedWay().equals(other.getIssuedWay()))
            && (this.getIssuedDatetime() == null ? other.getIssuedDatetime() == null : this.getIssuedDatetime().equals(other.getIssuedDatetime()))
            && (this.getCoinNetworkType() == null ? other.getCoinNetworkType() == null : this.getCoinNetworkType().equals(other.getCoinNetworkType()))
            && (this.getCoinType() == null ? other.getCoinType() == null : this.getCoinType().equals(other.getCoinType()))
            && (this.getCoinImage() == null ? other.getCoinImage() == null : this.getCoinImage().equals(other.getCoinImage()))
            && (this.getMarketPriceCny() == null ? other.getMarketPriceCny() == null : this.getMarketPriceCny().equals(other.getMarketPriceCny()))
            && (this.getSysTradingFeeType() == null ? other.getSysTradingFeeType() == null : this.getSysTradingFeeType().equals(other.getSysTradingFeeType()))
            && (this.getSysTradingFee() == null ? other.getSysTradingFee() == null : this.getSysTradingFee().equals(other.getSysTradingFee()))
            && (this.getMinerTradingFee() == null ? other.getMinerTradingFee() == null : this.getMinerTradingFee().equals(other.getMinerTradingFee()))
            && (this.getLowestExtractNumber() == null ? other.getLowestExtractNumber() == null : this.getLowestExtractNumber().equals(other.getLowestExtractNumber()))
            && (this.getIsAutoGatherCoin() == null ? other.getIsAutoGatherCoin() == null : this.getIsAutoGatherCoin().equals(other.getIsAutoGatherCoin()))
            && (this.getUpperLimitAmount() == null ? other.getUpperLimitAmount() == null : this.getUpperLimitAmount().equals(other.getUpperLimitAmount()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRecordId() == null) ? 0 : getRecordId().hashCode());
        result = prime * result + ((getCoinFullName() == null) ? 0 : getCoinFullName().hashCode());
        result = prime * result + ((getCoinShortName() == null) ? 0 : getCoinShortName().hashCode());
        result = prime * result + ((getCoinSymbol() == null) ? 0 : getCoinSymbol().hashCode());
        result = prime * result + ((getCoinDecimals() == null) ? 0 : getCoinDecimals().hashCode());
        result = prime * result + ((getIssuedNumber() == null) ? 0 : getIssuedNumber().hashCode());
        result = prime * result + ((getIssuedWay() == null) ? 0 : getIssuedWay().hashCode());
        result = prime * result + ((getIssuedDatetime() == null) ? 0 : getIssuedDatetime().hashCode());
        result = prime * result + ((getCoinNetworkType() == null) ? 0 : getCoinNetworkType().hashCode());
        result = prime * result + ((getCoinType() == null) ? 0 : getCoinType().hashCode());
        result = prime * result + ((getCoinImage() == null) ? 0 : getCoinImage().hashCode());
        result = prime * result + ((getMarketPriceCny() == null) ? 0 : getMarketPriceCny().hashCode());
        result = prime * result + ((getSysTradingFeeType() == null) ? 0 : getSysTradingFeeType().hashCode());
        result = prime * result + ((getSysTradingFee() == null) ? 0 : getSysTradingFee().hashCode());
        result = prime * result + ((getMinerTradingFee() == null) ? 0 : getMinerTradingFee().hashCode());
        result = prime * result + ((getLowestExtractNumber() == null) ? 0 : getLowestExtractNumber().hashCode());
        result = prime * result + ((getIsAutoGatherCoin() == null) ? 0 : getIsAutoGatherCoin().hashCode());
        result = prime * result + ((getUpperLimitAmount() == null) ? 0 : getUpperLimitAmount().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", recordId=").append(recordId);
        sb.append(", coinFullName=").append(coinFullName);
        sb.append(", coinShortName=").append(coinShortName);
        sb.append(", coinSymbol=").append(coinSymbol);
        sb.append(", coinDecimals=").append(coinDecimals);
        sb.append(", issuedNumber=").append(issuedNumber);
        sb.append(", issuedWay=").append(issuedWay);
        sb.append(", issuedDatetime=").append(issuedDatetime);
        sb.append(", coinNetworkType=").append(coinNetworkType);
        sb.append(", coinType=").append(coinType);
        sb.append(", coinImage=").append(coinImage);
        sb.append(", marketPriceCny=").append(marketPriceCny);
        sb.append(", sysTradingFeeType=").append(sysTradingFeeType);
        sb.append(", sysTradingFee=").append(sysTradingFee);
        sb.append(", minerTradingFee=").append(minerTradingFee);
        sb.append(", lowestExtractNumber=").append(lowestExtractNumber);
        sb.append(", isAutoGatherCoin=").append(isAutoGatherCoin);
        sb.append(", upperLimitAmount=").append(upperLimitAmount);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}