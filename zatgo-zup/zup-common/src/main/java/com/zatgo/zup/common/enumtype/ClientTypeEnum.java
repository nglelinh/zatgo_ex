package com.zatgo.zup.common.enumtype;

/**
 * 0: 手机
 * 1：WEB网站
 * @Title
 * @author lincm
 * @date 2018年3月16日 下午3:23:41
 */
public enum ClientTypeEnum {

	PHONE(0),WEB(1);

    private Integer code;

    private ClientTypeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
