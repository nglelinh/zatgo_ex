package com.zatgo.zup.common.model;

/**
 * Created by 46041 on 2019/1/15.
 */
public class GetRewarRequest {

    private String gameId;

    private String coinType;
    /**
     * 是否免费
     */
    private Boolean isFree;

    private String userId;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public Boolean getFree() {
        return isFree;
    }

    public void setFree(Boolean free) {
        isFree = free;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
