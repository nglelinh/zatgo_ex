package com.zatgo.zup.common.model;

/**
 * Created by 46041 on 2018/9/20.
 */
public class CloudSystemConstant {

    /**
     * 默认的云ID
     */
    public static final String DEFAULT_SYSTEM_CLOUD_USER_ID = "65c2c3a074774f8090a1e4544a56632a";
    
    /**
     * 默认云短信签名
     */
    public static final String DEFAULT_SYSTEM_CLOUD_USER_SMS_SIGN = "【ZATGO】";
}
