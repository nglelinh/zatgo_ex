package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class QtumContractSignParams implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fromAddress;

	private String toAddressHex;
	
	private String toAddress;
	
	private BigDecimal amount;
	
	private String password;
	
	private BigInteger gasPrice;
	
	private BigInteger gasLimit;

	private BigInteger fee;
	
	private String contractAddress;
	
	private BigInteger decimal;

	private String changeAddress;

	private List<UnSpentQtumData> unSpentBtcs;
	
	public BigInteger getDecimal() {
		return decimal;
	}

	public void setDecimal(BigInteger decimal) {
		this.decimal = decimal;
	}

	public String getContractAddress() {
		return contractAddress;
	}

	public void setContractAddress(String contractAddress) {
		this.contractAddress = contractAddress;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BigInteger getGasPrice() {
		return gasPrice;
	}

	public void setGasPrice(BigInteger gasPrice) {
		this.gasPrice = gasPrice;
	}

	public BigInteger getGasLimit() {
		return gasLimit;
	}

	public void setGasLimit(BigInteger gasLimit) {
		this.gasLimit = gasLimit;
	}

	public String getChangeAddress() {
		return changeAddress;
	}

	public void setChangeAddress(String changeAddress) {
		this.changeAddress = changeAddress;
	}

	public List<UnSpentQtumData> getUnSpentBtcs() {
		return unSpentBtcs;
	}

	public void setUnSpentBtcs(List<UnSpentQtumData> unSpentBtcs) {
		this.unSpentBtcs = unSpentBtcs;
	}

	public String getToAddressHex() {
		return toAddressHex;
	}

	public void setToAddressHex(String toAddressHex) {
		this.toAddressHex = toAddressHex;
	}

	public BigInteger getFee() {
		return fee;
	}

	public void setFee(BigInteger fee) {
		this.fee = fee;
	}
}
