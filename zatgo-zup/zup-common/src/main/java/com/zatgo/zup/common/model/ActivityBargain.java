package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel(value = "活动")
public class ActivityBargain {


    @ApiModelProperty(value = "活动ID",required = true)
    private String bargainActivityId;
    @ApiModelProperty(value = "云商家ID",required = true)
    private String cloudUserId;
    @ApiModelProperty(value = "砍价商品ID",required = true)
    private String productId;
    @ApiModelProperty(value = "商品名称",required = true)
    private String productName;
    @ApiModelProperty(value = "商品缩略图",required = true)
    private String productImgAbbrUrl;
    @ApiModelProperty(value = "下单的url",required = true)
    private String addOrderUrl;
    @ApiModelProperty(value = "商品原价",required = true)
    private BigDecimal productMoney;
    @ApiModelProperty(value = "商品简介",required = true)
    private String productIntro;
    @ApiModelProperty(value = "活动价",required = true)
    private BigDecimal activityMoney;
    @ApiModelProperty(value = "起砍价",required = true)
    private BigDecimal activityStartMoney;
    @ApiModelProperty(value = "是否接触参与人数的限制",required = true)
    private Boolean isRemoveLimit;
    @ApiModelProperty(value = "低于活动价后，砍一刀的比例   砍一刀的钱 = 当前价格*bargain_rate",required = true)
    private BigDecimal bargainRate;
    @ApiModelProperty(value = "参与人数",required = true)
    private Integer joinCount;
    @ApiModelProperty(value = "活动商品描述",required = true)
    private String productDesc;
    @ApiModelProperty(value = "活动开始时间",required = true)
    private Date activityStartDate;
    @ApiModelProperty(value = "活动结束时间",required = true)
    private Date activityEndDate;
    @ApiModelProperty(value = "状态 0=录入\n" +
            "1=上线\n" +
            "2=下线",required = true)
    private Byte status;
    @ApiModelProperty(value = "创建时间",required = true)
    private Date createDate;
    @ApiModelProperty(value = "上线时间",required = true)
    private Date onlineDate;
    @ApiModelProperty(value = "下线时间",required = true)
    private Date offlineDate;
    @ApiModelProperty(value = "操作人姓名",required = true)
    private String operatorName;
    @ApiModelProperty(value = "操作人ID",required = true)
    private String operatorId;
    @ApiModelProperty(value = "支付类型   0 先砍后下单  1先下单再砍",required = true)
    private Byte payType;
    @ApiModelProperty(value = "分享的title",required = true)
    private String shareTitle;
    @ApiModelProperty(value = "分享的描述",required = true)
    private String shareDesc;
    @ApiModelProperty(value = "分享图片的url",required = true)
    private String shareImgUrl;
    @ApiModelProperty(value = "分享的类型",required = true)
    private String shareType;
    @ApiModelProperty(value = "分享的跳转链接",required = true)
    private String shareLink;
    @ApiModelProperty(value = "商品总库存",required = true)
    private Long productStockTotal;
    @ApiModelProperty(value = "商品当前库存",required = true)
    private Long productStock;
    @ApiModelProperty(value = "模式 0自动 1手动",required = true)
    private Byte model;
    @ApiModelProperty(value = "超时时间（分）",required = true)
    private Long timeOut;
    @ApiModelProperty(value = "活动图片",required = true)
    private String imgs;
    @ApiModelProperty(value = "活动名称",required = true)
    private String activityName;
    @ApiModelProperty(value = "活动介绍",required = true)
    private String activityInfo;
    @ApiModelProperty(value = "扩展属性",required = true)
    private String activityExtInfo;
    @ApiModelProperty(value = "活动标准",required = true)
    private Long activityCriterion;

    private Byte activityStatus;

    @ApiModelProperty(value = "微信群二维码url",required = true)
    private String wxGroupQrUrl;

    private String priceIds;


    public String getBargainActivityId() {
        return bargainActivityId;
    }

    public void setBargainActivityId(String bargainActivityId) {
        this.bargainActivityId = bargainActivityId == null ? null : bargainActivityId.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public String getProductImgAbbrUrl() {
        return productImgAbbrUrl;
    }

    public void setProductImgAbbrUrl(String productImgAbbrUrl) {
        this.productImgAbbrUrl = productImgAbbrUrl == null ? null : productImgAbbrUrl.trim();
    }

    public String getAddOrderUrl() {
        return addOrderUrl;
    }

    public void setAddOrderUrl(String addOrderUrl) {
        this.addOrderUrl = addOrderUrl == null ? null : addOrderUrl.trim();
    }

    public BigDecimal getProductMoney() {
        return productMoney;
    }

    public void setProductMoney(BigDecimal productMoney) {
        this.productMoney = productMoney;
    }

    public String getProductIntro() {
        return productIntro;
    }

    public void setProductIntro(String productIntro) {
        this.productIntro = productIntro == null ? null : productIntro.trim();
    }

    public BigDecimal getActivityMoney() {
        return activityMoney;
    }

    public void setActivityMoney(BigDecimal activityMoney) {
        this.activityMoney = activityMoney;
    }

    public BigDecimal getActivityStartMoney() {
        return activityStartMoney;
    }

    public void setActivityStartMoney(BigDecimal activityStartMoney) {
        this.activityStartMoney = activityStartMoney;
    }

    public Boolean getRemoveLimit() {
        return isRemoveLimit;
    }

    public void setRemoveLimit(Boolean removeLimit) {
        isRemoveLimit = removeLimit;
    }

    public BigDecimal getBargainRate() {
        return bargainRate;
    }

    public void setBargainRate(BigDecimal bargainRate) {
        this.bargainRate = bargainRate;
    }

    public Integer getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(Integer joinCount) {
        this.joinCount = joinCount;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc == null ? null : productDesc.trim();
    }

    public Date getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(Date activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public Date getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(Date activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getOnlineDate() {
        return onlineDate;
    }

    public void setOnlineDate(Date onlineDate) {
        this.onlineDate = onlineDate;
    }

    public Date getOfflineDate() {
        return offlineDate;
    }

    public void setOfflineDate(Date offlineDate) {
        this.offlineDate = offlineDate;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId == null ? null : operatorId.trim();
    }

    public Byte getPayType() {
        return payType;
    }

    public void setPayType(Byte payType) {
        this.payType = payType;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareDesc() {
        return shareDesc;
    }

    public void setShareDesc(String shareDesc) {
        this.shareDesc = shareDesc;
    }

    public String getShareImgUrl() {
        return shareImgUrl;
    }

    public void setShareImgUrl(String shareImgUrl) {
        this.shareImgUrl = shareImgUrl;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public Long getProductStockTotal() {
        return productStockTotal;
    }

    public void setProductStockTotal(Long productStockTotal) {
        this.productStockTotal = productStockTotal;
    }

    public Long getProductStock() {
        return productStock;
    }

    public void setProductStock(Long productStock) {
        this.productStock = productStock;
    }

    public Byte getModel() {
        return model;
    }

    public void setModel(Byte model) {
        this.model = model;
    }

    public Long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Long timeOut) {
        this.timeOut = timeOut;
    }

    public String getImgs() {
        return imgs;
    }

    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityInfo() {
        return activityInfo;
    }

    public void setActivityInfo(String activityInfo) {
        this.activityInfo = activityInfo;
    }

    public String getActivityExtInfo() {
        return activityExtInfo;
    }

    public void setActivityExtInfo(String activityExtInfo) {
        this.activityExtInfo = activityExtInfo;
    }

    public Long getActivityCriterion() {
        return activityCriterion;
    }

    public void setActivityCriterion(Long activityCriterion) {
        this.activityCriterion = activityCriterion;
    }

    public Byte getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Byte activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getWxGroupQrUrl() {
        return wxGroupQrUrl;
    }

    public void setWxGroupQrUrl(String wxGroupQrUrl) {
        this.wxGroupQrUrl = wxGroupQrUrl;
    }

    public String getPriceIds() {
        return priceIds;
    }

    public void setPriceIds(String priceIds) {
        this.priceIds = priceIds;
    }
}