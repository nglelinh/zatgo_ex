package com.zatgo.zup.common.utils;

import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;

public class SignUtils {
	public final static String PRIVATE_KEY  = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOlJgJ0fWy42g/XUvxPRwbOSbWBlEd3jVhlnh2VVgksxZjFKHEV0kWQD800fC8DNBJ904FuR4xcLkEZrlcZyb9IIiYt+f0aQs+p6k8d2bFo4XU9OT6khmsQrD+ZeRwiT7ZKCFg1pDBwfj5oiX8YdIkiMP+zrZt+rEko5qa+a6531AgMBAAECgYEAqCvAd4P/KxVJk5HGHPd894J7Lp4tfxM/Ihkr1bUKtEHsNWQDdV5O4Ran8dSLmHUy+/A5PEmytw4y91DfK6ubRyE4y/M3pr14I3F9c1dccv6qCd9TxICIzx+BLUvbN4czQ5EWxRx7+I94rpB8VOyssY6GEssSAXjw4knj6CSy+AECQQD70R13x2dg0pNZ6+61k+ePXb8MptvjzRHMkjpDvU/UpFzMqV3aYkkA/cdE+Va/qG63JWI4yuisq/x55nWRaXyBAkEA7SmWTDswM3Zvi8+BxK7c76FtZWnsn/CCGQsjhpVoXUH/sYKsdjro1yqZAjtAWOTTP2B1D2Tzkyl9F+FM2PY3dQJAaOGQBHNo+0q5VJqPsdEIgQ3qpLpam2auIyZEXjJbLZXHkPZ5wJtvDJqmhHPVTb45Xs6TOlRI1KJNZfg2v3nQAQJBALcLP/t6O0uc/57y+39oTxGMkDiTtpQDNwZJOJQpQLGpvMWuy5k9AnT/ZbT5MkLp6bmiovbMvco5XVlPBNb3wfkCQAlETmPDTy8D8XEguOzDAGyYAWM4x80eBH+CEOUo79PyboK42KVfVEYjR2fFGIyHyOSF5XJHS+QMoHAXa5vvsLk=";
	private static Logger logger = LoggerFactory.getLogger(SignUtils.class);

	private static final String ALGORITHM = "RSA";

	private static final String SIGN_ALGORITHMS = "SHA1WithRSA";

	private static final String SIGN_ALGORITHMS_256 = "SHA256withRsa";

	private static final String DEFAULT_CHARSET = "UTF-8";
	
	public static String sign256(Object object, String privateKey) {
		try {
			TreeMap<String,String> signMap = new TreeMap<>(BeanToMapUtils.beanToHashMap(object));
			StringBuffer sign = new StringBuffer();
			Set<Entry<String, String>> entries = signMap.entrySet();
			for (Entry<String, String> entry : entries) {
				sign.append("&").append(entry.getKey()).append("=").append(entry.getValue());
			}
			
			return sign256(sign.substring(1), privateKey);
		} catch (Exception e) {
			logger.error("生成签名失败：", e);
		}

		return null;
	}
	
	public static String sign256(Map<String, String> map, String privateKey) {
		try {
			TreeMap<String,String> signMap = new TreeMap<>(map);
			StringBuffer sign = new StringBuffer();
			Set<Entry<String, String>> entries = signMap.entrySet();
			for (Entry<String, String> entry : entries) {
				sign.append("&").append(entry.getKey()).append("=").append(entry.getValue());
			}
			
			return sign256(sign.substring(1), privateKey);
		} catch (Exception e) {
			logger.error("生成签名失败：", e);
		}

		return null;
	}
	
	public static String sign256(String content, String privateKey) {
		try {
			PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
					Base64.decode(privateKey));
			KeyFactory keyf = KeyFactory.getInstance(ALGORITHM);
			PrivateKey priKey = keyf.generatePrivate(priPKCS8);

			Signature signature = Signature.getInstance(SIGN_ALGORITHMS_256);

			signature.initSign(priKey);
			signature.update(content.getBytes(DEFAULT_CHARSET));

			byte[] signed = signature.sign();

			String sign = Base64.encode(signed);

			return sign;

		} catch (Exception e) {
			logger.error("SignUtils error!", e);
		}

		return null;
	}

	public static String sign(String content, String privateKey) {
		try {
			PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
					Base64.decode(privateKey));
			KeyFactory keyf = KeyFactory.getInstance(ALGORITHM);
			PrivateKey priKey = keyf.generatePrivate(priPKCS8);

			Signature signature = Signature.getInstance(SIGN_ALGORITHMS);

			signature.initSign(priKey);
			signature.update(content.getBytes(DEFAULT_CHARSET));

			byte[] signed = signature.sign();

			String sign = Base64.encode(signed);

			return sign;

		} catch (Exception e) {
			logger.error("SignUtils error!", e);
		}

		return null;
	}

	public static boolean signCheck256(String content, String sign,
									String publicKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			byte[] encodedKey = Base64.decode(publicKey);
			PublicKey pubKey = keyFactory
					.generatePublic(new X509EncodedKeySpec(encodedKey));

			Signature signature = Signature.getInstance(SIGN_ALGORITHMS_256);

			signature.initVerify(pubKey);
			signature.update(content.getBytes(DEFAULT_CHARSET));

			return signature.verify(Base64.decode(sign));

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean signCheck(String content, String sign,
			String publicKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			byte[] encodedKey = Base64.decode(publicKey);
			PublicKey pubKey = keyFactory
					.generatePublic(new X509EncodedKeySpec(encodedKey));

			Signature signature = Signature.getInstance(SIGN_ALGORITHMS);

			signature.initVerify(pubKey);
			signature.update(content.getBytes(DEFAULT_CHARSET));
			return signature.verify(Base64.decode(sign));

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static String sha1(String str) {
		if (str == null) {
			return null;
		}
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
			messageDigest.update(str.getBytes());
			return getFormattedText(messageDigest.digest());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5',  
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}; 
	private static String getFormattedText(byte[] bytes) {  
        int len = bytes.length;  
        StringBuilder buf = new StringBuilder(len * 2);  
        // 把密文转换成十六进制的字符串形式  
        for (int j = 0; j < len; j++) {  
            buf.append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);  
            buf.append(HEX_DIGITS[bytes[j] & 0x0f]);  
        }  
        return buf.toString();  
    }
	
	private static String byteArrayToHexString(byte[] b) {
		StringBuilder hs = new StringBuilder();
		String stmp;
		for (int n = 0; b != null && n < b.length; n++) {
			stmp = Integer.toHexString(b[n] & 0XFF);
			if (stmp.length() == 1)
				hs.append('0');
			hs.append(stmp);
		}
		return hs.toString();
	}

	/**
	 * sha256_HMAC加密
	 * 
	 * @param message
	 *            消息
	 * @param secret
	 *            秘钥
	 * @return 加密后字符串
	 */
	public static String sha256_HMAC(String message, String secret) {
		String hash = "";
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			byte[] bytes = sha256_HMAC.doFinal(message.getBytes());
			hash = byteArrayToHexString(bytes);
		} catch (Exception e) {
			logger.error("hmac_sha256加密失败：", e);
		}
		return hash;
	}
	public static Boolean signCheck(Object o,String sign,String pubKey) {
		Map<String,String> map = null;
		try {
			map = BeanToMapUtils.beanToStringMap(o);
		} catch (Exception e){
			logger.error("bean to map error");
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		}
		return signCheck(map,sign,pubKey);
	}
	
	public static Boolean signCheck(Map<String,String> map,String sign,String pubKey) {
		map.remove("sign");
		TreeMap<String, String> sortMap = new TreeMap<>(map);
		String md5Sign = MD5.getMD5Sign(sortMap);
		return SignUtils.signCheck256(md5Sign,sign,pubKey);
	}
	
	public static Boolean signCheck(JSONObject jsonParams,String sign,String pubKey) {
		HashMap<String, String> map = new HashMap<>();
        for (Map.Entry<String, Object> entry : jsonParams.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            //只对参数第一层做签名
            try {
                if(value instanceof JSONObject
                        || value instanceof JSONArray
                        || value == null
                        || String.valueOf(value).length() == 0){
                    continue;
                }
                map.put(key, value.toString());
            } catch (Exception e) {
                logger.error("",e);
            }

        }
        
		map.remove("sign");
		TreeMap<String, String> sortMap = new TreeMap<>(map);
		String md5Sign = MD5.getMD5Sign(sortMap);
		return SignUtils.signCheck256(md5Sign,sign,pubKey);
	}
//	public static void main(String[] args) {
//		String prkey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJxiHXHapduKZ3Fy79LgKXKMySx/1vEZgweBdV+nEL7sDJ4ZMH6YerV5mv6mzyQFUfu335lE5KbxChTR3Roiblyz5Jjk+GPCqZJHDUulAVU/jUZvye8Stdel+zb1pY0MgPwHl8yCU13f3AFa9fTurJU/MNhCzojssRIr+CAhhuCdAgMBAAECgYBdMT8YzVLPrQAOJI90nG82u11bbmeZ0Dk8bNKUiQu6qxKt2JcEqjteEDTkkmWYsq7zkdTnEp4sOjJmy9W2MFSRbYBTDVtqdk1R5KyTtc1JDorPxSk7u5DBoWou7ECbEABKWLlRgelZFDky0+XG5Gvz5D+uVcOrClh5hD20r31RgQJBAMxCQ/4sk//njprjf+zq6lDp8hzgbjb6ygy7cXRko0V/lI839S3ljYvqNHR7LVyIuSvMa6s9UgJr3ghfhUqvNGECQQDD/zk1mHfyeon0lAjO0eGLISriXXxX5Wp3fHr6170JVp10uA4OB926ldKBC7cmTp8bkal4fZqJKB7j2u8WaVW9AkBVDS60sqk5wJivxBUreLHy2Uc4YPdjq0/gt1hJp/IYaURZ0mokH1auMOqAqh28Ty4Tn74hgpoYagP9JpEhBQ1hAkAYhe6ajiEz7bElMQCJICmgRt4exKOyO7o+iQy6JhQ4/uVknULk7ocHuYdFLjoUSPIPeQJDWTPkxWhYCiSwIS3hAkEAk5n7CVssjZyVp/s+iEHTXMkLvsMf1bNQxUPmZ9tpzKv3I4kuIXiVtwkfUS0ujDbRFRUUexnPt2ezBV+2Wy4m+g==";
//		String pubkey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCcYh1x2qXbimdxcu/S4ClyjMksf9bxGYMHgXVfpxC+7AyeGTB+mHq1eZr+ps8kBVH7t9+ZROSm8QoU0d0aIm5cs+SY5PhjwqmSRw1LpQFVP41Gb8nvErXXpfs29aWNDID8B5fMglNd39wBWvX07qyVPzDYQs6I7LESK/ggIYbgnQIDAQAB";
//		String content = "amount=888&appid=cae4d08b-2123-4826-ae68-374c37e5b7bf&notifyUrl=http://work.flybycloud.com:8023/zat/pay/callback/flight&orderNumber=128056083004810";
//
//		Map<String, String> data = new HashMap<>();
//		data.put("orderNumber", "128056089006710");
//		data.put("amount", "888");
//		data.put("appid", "cae4d08b-2123-4826-ae68-374c37e5b7bf");
//		data.put("notifyUrl", "http://work.flybycloud.com:8023/zat/pay/callback/flight");
//		List<String> list = new ArrayList<String>(data.keySet());
//		list.sort((fir, sec) -> fir.compareTo(sec));
//		StringBuffer str = new StringBuffer();
//		list.stream().forEach(value -> str.append("&").append(value).append("=").append(data.get(value)));
//		String res = str.substring(1);
//		System.out.println(res);
//		data.put("sign", SignUtils.sign(MD5.GetMD5Code(res), prkey));
//
//		String sign1 = SignUtils.sign(MD5.GetMD5Code(res), prkey);
//
//		System.out.println(sign1);
//
//		String sign = sign(content, prkey);
//		boolean b = signCheck(content, sign, pubkey);
//		System.out.println(b);
//	}
}
