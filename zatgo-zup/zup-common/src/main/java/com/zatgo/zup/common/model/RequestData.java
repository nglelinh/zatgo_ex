package com.zatgo.zup.common.model;

import java.io.Serializable;

public class RequestData<T> implements Serializable {

	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	
}
