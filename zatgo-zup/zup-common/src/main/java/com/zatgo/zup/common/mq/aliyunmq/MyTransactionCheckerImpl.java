package com.zatgo.zup.common.mq.aliyunmq;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.transaction.LocalTransactionChecker;
import com.aliyun.openservices.ons.api.transaction.TransactionStatus;


@Service
public class MyTransactionCheckerImpl implements LocalTransactionChecker {

    private Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Override
    public TransactionStatus check(Message message) {
        return null;
    }
}
