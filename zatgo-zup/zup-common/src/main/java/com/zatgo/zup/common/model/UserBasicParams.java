package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户基础信息参数")
public class UserBasicParams {
	
	@ApiModelProperty(value = "头像地址", required = false)
	private String iconUrl;
	
	@ApiModelProperty(value = "昵称", required = false)
	private String nickname;

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

}
