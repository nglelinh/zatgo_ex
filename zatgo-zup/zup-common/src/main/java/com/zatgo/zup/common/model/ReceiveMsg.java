package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.zatgo.zup.common.enumtype.BusinessEnum;

public class ReceiveMsg implements Serializable {

	private String userId;

	private BigDecimal amount;
	
	private String netWorkType;
	
	private String coinType;

	private Byte type;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getNetWorkType() {
		return netWorkType;
	}

	public void setNetWorkType(String netWorkType) {
		this.netWorkType = netWorkType;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}
}
