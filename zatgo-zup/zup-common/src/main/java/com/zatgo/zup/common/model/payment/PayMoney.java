package com.zatgo.zup.common.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
@ApiModel(value = "支付金额比例相关")
public class PayMoney implements Serializable {
	@ApiModelProperty(value = "主币类型",required = true)
	private String mainCoinType;
	@ApiModelProperty(value = "次币类型",required = false)
	private String secondaryCoinType;
	@ApiModelProperty(value = "主币金额",required = true)
	private BigDecimal token;
	@ApiModelProperty(value = "次币金额",required = true)
	private BigDecimal cashMoney;
	@ApiModelProperty(value = "排序",required = true)
	private Byte sort;

	public String getMainCoinType() {
		return mainCoinType;
	}

	public void setMainCoinType(String mainCoinType) {
		this.mainCoinType = mainCoinType;
	}

	public String getSecondaryCoinType() {
		return secondaryCoinType;
	}

	public void setSecondaryCoinType(String secondaryCoinType) {
		this.secondaryCoinType = secondaryCoinType;
	}

	public BigDecimal getToken() {
		return token;
	}

	public void setToken(BigDecimal token) {
		this.token = token;
	}

	public BigDecimal getCashMoney() {
		return cashMoney;
	}

	public void setCashMoney(BigDecimal cashMoney) {
		this.cashMoney = cashMoney;
	}

	public Byte getSort() {
		return sort;
	}

	public void setSort(Byte sort) {
		this.sort = sort;
	}
}
