package com.zatgo.zup.common.curator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


public class ZkClusterConfig {
	
	@Value("spring.zookeeper.curator.connection.timeout:0")
	private Integer zkConnectionTimeout;
	@Value("spring.zookeeper.curator.session.timeout:0")
	private Integer zkSessionTimeout;
	@Value("spring.zookeeper.curator.retry.times:0")
	private Integer zkRetryTimes;
	@Value("spring.zookeeper.curator.retry.interval.floor:0")
	private Integer zkRetryIntervalFloor;
	@Value("spring.zookeeper.curator.retry.interval.ceiling:0")
	private Integer zkRetryIntervalCeiling;
	@Value("spring.curator.zookeeper.servers:")
	private List<String> zkServers;
	@Value("spring.curator.zookeeper.port:")
	private Integer zkPort;
	@Value("spring.curator.zookeeper.root:")
	private Integer zkRoot;
	
	public String getServerUrl() {
		List<String> serverPorts = new ArrayList<String>();
		for(String zkServer:zkServers){
			serverPorts.add(zkServer + ":" + zkPort);
		}
		String serverPortStr = StringUtils.join(serverPorts, ",") + zkPort;
		return serverPortStr;
	}

	public Integer getZkConnectionTimeout() {
		return zkConnectionTimeout;
	}

	public void setZkConnectionTimeout(Integer zkConnectionTimeout) {
		this.zkConnectionTimeout = zkConnectionTimeout;
	}

	public Integer getZkSessionTimeout() {
		return zkSessionTimeout;
	}

	public void setZkSessionTimeout(Integer zkSessionTimeout) {
		this.zkSessionTimeout = zkSessionTimeout;
	}

	public Integer getZkRetryTimes() {
		return zkRetryTimes;
	}

	public void setZkRetryTimes(Integer zkRetryTimes) {
		this.zkRetryTimes = zkRetryTimes;
	}

	public Integer getZkRetryIntervalFloor() {
		return zkRetryIntervalFloor;
	}

	public void setZkRetryInterval(Integer zkRetryIntervalFloor) {
		this.zkRetryIntervalFloor = zkRetryIntervalFloor;
	}

	public Integer getZkRetryIntervalCeiling() {
		return zkRetryIntervalCeiling;
	}

	public void setZkRetryIntervalCeiling(Integer zkRetryIntervalCeiling) {
		this.zkRetryIntervalCeiling = zkRetryIntervalCeiling;
	}
	
}
