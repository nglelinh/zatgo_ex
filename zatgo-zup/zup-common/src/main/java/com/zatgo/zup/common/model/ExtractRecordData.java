package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ExtractRecordData implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String extractId;

    private String accountId;

    private String extractAddress;

    private Date extractDate;

    private Date extractSuccessDate;

    private BigDecimal extractNumber;

    private String extractStatus;

    private String txHash;

    private String blockHash;
    
    private String approveReason;
    
    private String approveResult;
    
    private String coinType;
    
    private String coinNetworkType;

    private BigDecimal charge;

	public BigDecimal getCharge() {
		return charge;
	}

	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}

    public String getApproveReason() {
		return approveReason;
	}

	public void setApproveReason(String approveReason) {
		this.approveReason = approveReason;
	}

	public String getApproveResult() {
		return approveResult;
	}

	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}

	public String getExtractId() {
        return extractId;
    }

    public void setExtractId(String extractId) {
        this.extractId = extractId == null ? null : extractId.trim();
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId == null ? null : accountId.trim();
    }

    public String getExtractAddress() {
        return extractAddress;
    }

    public void setExtractAddress(String extractAddress) {
        this.extractAddress = extractAddress == null ? null : extractAddress.trim();
    }

    public Date getExtractDate() {
        return extractDate;
    }

    public void setExtractDate(Date extractDate) {
        this.extractDate = extractDate;
    }

    public Date getExtractSuccessDate() {
        return extractSuccessDate;
    }

    public void setExtractSuccessDate(Date extractSuccessDate) {
        this.extractSuccessDate = extractSuccessDate;
    }

    public BigDecimal getExtractNumber() {
        return extractNumber;
    }

    public void setExtractNumber(BigDecimal extractNumber) {
        this.extractNumber = extractNumber;
    }

    public String getExtractStatus() {
        return extractStatus;
    }

    public void setExtractStatus(String extractStatus) {
        this.extractStatus = extractStatus == null ? null : extractStatus.trim();
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash == null ? null : txHash.trim();
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash == null ? null : blockHash.trim();
    }
}