package com.zatgo.zup.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public class CouponsEnum {

		public enum CouponsUserType {

			allUser(0), newUser(1),oldUser(2);

			private Byte code;

			private static final Map<Byte, CouponsUserType> stringToEnum = new HashMap<Byte, CouponsUserType>();
			static {
				for (CouponsUserType enumType : values()) {
					stringToEnum.put(enumType.getCode(), enumType);
				}
			}

			public static CouponsUserType getEnumByType(Byte type) {
				return stringToEnum.get(type);
			}

			private CouponsUserType(Integer code) {
				this.code = code.byteValue();
			}

			public Byte getCode() {
				return code;
			}

			public void setCode(Byte code) {
				this.code = code;
			}
		}
		
		public enum UserCouponsStatus {

			unused(0), used(1);

			private Byte code;

			private static final Map<Byte, UserCouponsStatus> stringToEnum = new HashMap<Byte, UserCouponsStatus>();
			static {
				for (UserCouponsStatus enumType : values()) {
					stringToEnum.put(enumType.getCode(), enumType);
				}
			}

			public static UserCouponsStatus getEnumByType(Byte type) {
				return stringToEnum.get(type);
			}

			private UserCouponsStatus(Integer code) {
				this.code = code.byteValue();
			}

			public Byte getCode() {
				return code;
			}

			public void setCode(Byte code) {
				this.code = code;
			}
		}
		
		
		public enum CouponsActivityGetType {

			userGet(0), systemGet(1);

			private Byte code;

			private static final Map<Byte, CouponsActivityGetType> stringToEnum = new HashMap<Byte, CouponsActivityGetType>();
			static {
				for (CouponsActivityGetType enumType : values()) {
					stringToEnum.put(enumType.getCode(), enumType);
				}
			}

			public static CouponsActivityGetType getEnumByType(Byte type) {
				return stringToEnum.get(type);
			}

			private CouponsActivityGetType(Integer code) {
				this.code = code.byteValue();
			}

			public Byte getCode() {
				return code;
			}

			public void setCode(Byte code) {
				this.code = code;
			}
		}
		
		/**
		 * 优惠券有效期类型
		 * @author 1
		 *
		 */
		public enum CouponsValidityType {

			fixedDate(0), currentStart(1),tomorrowStart(2);

			private Byte code;

			private static final Map<Byte, CouponsValidityType> stringToEnum = new HashMap<Byte, CouponsValidityType>();
			static {
				for (CouponsValidityType enumType : values()) {
					stringToEnum.put(enumType.getCode(), enumType);
				}
			}

			public static CouponsValidityType getEnumByType(Byte type) {
				return stringToEnum.get(type);
			}

			private CouponsValidityType(Integer code) {
				this.code = code.byteValue();
			}

			public Byte getCode() {
				return code;
			}

			public void setCode(Byte code) {
				this.code = code;
			}
		}
		
		
		public enum CouponsDiscountType {

			fixedMoney(0), discount(1);

			private Byte code;

			private static final Map<Byte, CouponsDiscountType> stringToEnum = new HashMap<Byte, CouponsDiscountType>();
			static {
				for (CouponsDiscountType enumType : values()) {
					stringToEnum.put(enumType.getCode(), enumType);
				}
			}

			public static CouponsDiscountType getEnumByType(Byte type) {
				return stringToEnum.get(type);
			}

			private CouponsDiscountType(Integer code) {
				this.code = code.byteValue();
			}

			public Byte getCode() {
				return code;
			}

			public void setCode(Byte code) {
				this.code = code;
			}
		}
		
		
		public enum CouponsActivityStatus {

			input(0), online(1),offline(2);

			private Byte code;

			private static final Map<Byte, CouponsActivityStatus> stringToEnum = new HashMap<Byte, CouponsActivityStatus>();
			static {
				for (CouponsActivityStatus enumType : values()) {
					stringToEnum.put(enumType.getCode(), enumType);
				}
			}

			public static CouponsActivityStatus getEnumByType(Byte type) {
				return stringToEnum.get(type);
			}

			private CouponsActivityStatus(Integer code) {
				this.code = code.byteValue();
			}

			public Byte getCode() {
				return code;
			}

			public void setCode(Byte code) {
				this.code = code;
			}
		}
}
