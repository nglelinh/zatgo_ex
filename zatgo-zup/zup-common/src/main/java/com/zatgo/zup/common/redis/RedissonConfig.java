//package com.zatgo.zup.common.redis;
//
//import io.netty.channel.nio.NioEventLoopGroup;
//
//import org.redisson.Redisson;
//import org.redisson.api.RedissonClient;
//import org.redisson.config.Config;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class RedissonConfig {
//	@Value("${redisson.address}")
//	private String address;
//
//	@Value("${redisson.connectionMinimumIdleSize}")
//	private int connectionMinimumIdleSize = 10;
//
//	@Value("${redisson.idleConnectionTimeout}")
//	private int idleConnectionTimeout=10000;
//
//	@Value("${redisson.pingTimeout}")
//	private int pingTimeout=1000;
//
//	@Value("${redisson.connectTimeout}")
//	private int connectTimeout=10000;
//
//	@Value("${redisson.timeout}")
//	private int timeout=3000;
//
//	@Value("${redisson.retryAttempts}")
//	private int retryAttempts=3;
//
//	@Value("${redisson.retryInterval}")
//	private int retryInterval=1500;
//
//	@Value("${redisson.reconnectionTimeout}")
//	private int reconnectionTimeout=3000;
//
//	@Value("${redisson.failedAttempts}")
//	private int failedAttempts=3;
//
//	@Value("${redisson.password}")
//	private String password;
//
//	@Value("${redisson.subscriptionsPerConnection}")
//	private int subscriptionsPerConnection=5;
//
//	@Value("${redisson.subscriptionConnectionMinimumIdleSize}")
//	private int subscriptionConnectionMinimumIdleSize = 1;
//
//	@Value("${redisson.subscriptionConnectionPoolSize}")
//	private int subscriptionConnectionPoolSize = 50;
//
//	@Value("${redisson.connectionPoolSize}")
//	private int connectionPoolSize = 64;
//
//	@Value("${redisson.database}")
//	private int database = 0;
//
//	@Value("${redisson.dnsMonitoring}")
//	private boolean dnsMonitoring;
//
//	@Value("${redisson.dnsMonitoringInterval}")
//	private int dnsMonitoringInterval = 5000;
//
//
//	private int thread; //当前处理核数量 * 2
//
//	@Bean(destroyMethod = "shutdown")
//	RedissonClient redisson() throws Exception {
//		Config config = new Config();
//		config.useSingleServer().setAddress(address)
//				.setConnectionMinimumIdleSize(connectionMinimumIdleSize)
//				.setConnectionPoolSize(connectionPoolSize)
//				.setDatabase(database)
//				.setDnsMonitoring(dnsMonitoring)
//				.setDnsMonitoringInterval(dnsMonitoringInterval)
//				.setSubscriptionConnectionMinimumIdleSize(subscriptionConnectionMinimumIdleSize)
//				.setSubscriptionConnectionPoolSize(subscriptionConnectionPoolSize)
//				.setSubscriptionsPerConnection(subscriptionsPerConnection)
//				.setFailedAttempts(failedAttempts)
//				.setRetryAttempts(retryAttempts)
//				.setRetryInterval(retryInterval)
//				.setReconnectionTimeout(reconnectionTimeout)
//				.setTimeout(timeout)
//				.setConnectTimeout(connectTimeout)
//				.setIdleConnectionTimeout(idleConnectionTimeout)
//				.setPingTimeout(pingTimeout)
//				.setPassword(password);
//		config.setThreads(thread);
//		config.setEventLoopGroup(new NioEventLoopGroup());
//		config.setUseLinuxNativeEpoll(false);
//		return Redisson.create(config);
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public int getConnectionMinimumIdleSize() {
//		return connectionMinimumIdleSize;
//	}
//
//	public void setConnectionMinimumIdleSize(int connectionMinimumIdleSize) {
//		this.connectionMinimumIdleSize = connectionMinimumIdleSize;
//	}
//
//	public int getIdleConnectionTimeout() {
//		return idleConnectionTimeout;
//	}
//
//	public void setIdleConnectionTimeout(int idleConnectionTimeout) {
//		this.idleConnectionTimeout = idleConnectionTimeout;
//	}
//
//	public int getPingTimeout() {
//		return pingTimeout;
//	}
//
//	public void setPingTimeout(int pingTimeout) {
//		this.pingTimeout = pingTimeout;
//	}
//
//	public int getConnectTimeout() {
//		return connectTimeout;
//	}
//
//	public void setConnectTimeout(int connectTimeout) {
//		this.connectTimeout = connectTimeout;
//	}
//
//	public int getTimeout() {
//		return timeout;
//	}
//
//	public void setTimeout(int timeout) {
//		this.timeout = timeout;
//	}
//
//	public int getRetryAttempts() {
//		return retryAttempts;
//	}
//
//	public void setRetryAttempts(int retryAttempts) {
//		this.retryAttempts = retryAttempts;
//	}
//
//	public int getRetryInterval() {
//		return retryInterval;
//	}
//
//	public void setRetryInterval(int retryInterval) {
//		this.retryInterval = retryInterval;
//	}
//
//	public int getReconnectionTimeout() {
//		return reconnectionTimeout;
//	}
//
//	public void setReconnectionTimeout(int reconnectionTimeout) {
//		this.reconnectionTimeout = reconnectionTimeout;
//	}
//
//	public int getFailedAttempts() {
//		return failedAttempts;
//	}
//
//	public void setFailedAttempts(int failedAttempts) {
//		this.failedAttempts = failedAttempts;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public int getSubscriptionsPerConnection() {
//		return subscriptionsPerConnection;
//	}
//
//	public void setSubscriptionsPerConnection(int subscriptionsPerConnection) {
//		this.subscriptionsPerConnection = subscriptionsPerConnection;
//	}
//
//
//	public int getSubscriptionConnectionMinimumIdleSize() {
//		return subscriptionConnectionMinimumIdleSize;
//	}
//
//	public void setSubscriptionConnectionMinimumIdleSize(int subscriptionConnectionMinimumIdleSize) {
//		this.subscriptionConnectionMinimumIdleSize = subscriptionConnectionMinimumIdleSize;
//	}
//
//	public int getSubscriptionConnectionPoolSize() {
//		return subscriptionConnectionPoolSize;
//	}
//
//	public void setSubscriptionConnectionPoolSize(int subscriptionConnectionPoolSize) {
//		this.subscriptionConnectionPoolSize = subscriptionConnectionPoolSize;
//	}
//
//	public int getConnectionPoolSize() {
//		return connectionPoolSize;
//	}
//
//	public void setConnectionPoolSize(int connectionPoolSize) {
//		this.connectionPoolSize = connectionPoolSize;
//	}
//
//	public int getDatabase() {
//		return database;
//	}
//
//	public void setDatabase(int database) {
//		this.database = database;
//	}
//
//	public boolean isDnsMonitoring() {
//		return dnsMonitoring;
//	}
//
//	public void setDnsMonitoring(boolean dnsMonitoring) {
//		this.dnsMonitoring = dnsMonitoring;
//	}
//
//	public int getDnsMonitoringInterval() {
//		return dnsMonitoringInterval;
//	}
//
//	public void setDnsMonitoringInterval(int dnsMonitoringInterval) {
//		this.dnsMonitoringInterval = dnsMonitoringInterval;
//	}
//
//	public int getThread() {
//		return thread;
//	}
//
//	public void setThread(int thread) {
//		this.thread = thread;
//	}
//}
