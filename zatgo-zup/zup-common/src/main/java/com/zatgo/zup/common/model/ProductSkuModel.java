package com.zatgo.zup.common.model;

import java.util.List;

/**
 * Created by 46041 on 2019/7/15.
 */
public class ProductSkuModel {
    private String attribute;

    private List<String> value;

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }
}
