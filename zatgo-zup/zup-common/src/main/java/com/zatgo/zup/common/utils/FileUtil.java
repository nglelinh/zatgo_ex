package com.zatgo.zup.common.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class FileUtil {

	public static String[] readTextFile(String fileName,String encoding) {
		ArrayList<String> wordlist = new ArrayList<>();
		try {
            Enumeration<URL> resources = Thread.currentThread().getContextClassLoader().getResources(fileName);
			List<URL> urls = new ArrayList<URL>();
			while(resources.hasMoreElements()){
				urls.add(resources.nextElement());
			}
			
			if(urls.isEmpty()){
				throw new RuntimeException("Could not find file on classpath " + fileName);
			}
            if (!urls.isEmpty()) { //判断文件是否存在
                InputStreamReader read = new InputStreamReader(
                		urls.get(0).openStream(), encoding);//考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                int index = 0;
                while ((lineTxt = bufferedReader.readLine()) != null) {
                	wordlist.add(lineTxt);
                }
                read.close();
            } else {
                System.out.println("找不到指定的文件");
            }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        }
		
		String[] res =  new String[wordlist.size()];
		wordlist.toArray(res);
		return res;
    }
}
