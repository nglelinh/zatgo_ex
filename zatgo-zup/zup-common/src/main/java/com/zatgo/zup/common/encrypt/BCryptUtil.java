package com.zatgo.zup.common.encrypt;

import org.mindrot.jbcrypt.BCrypt;

import com.zatgo.zup.common.utils.MD5;

/**
 * 账号密码工具
 * @author chenlei
 *
 */
public class BCryptUtil {
	
	/**
	 * 加密
	 * @param str 需要加密的明文
	 * @return
	 */
	public static String encrypt(String salt,String pswStr){
		return BCrypt.hashpw(MD5.encrypt(salt, pswStr), BCrypt.gensalt(12));
	}
	
	/**
	 * 校验
	 * @param salt 加密盐（账号名）
	 * @param checkStr 需要校验的明文（密码）
	 * @param encryptStr 密文（数据库中密码）
	 * @return
	 */
	public static boolean check(String salt,String checkStr,String encryptStr){
		return BCrypt.checkpw(MD5.encrypt(salt, checkStr), encryptStr);
	}
	
	public static void main(String[] args){
		String en = BCryptUtil.encrypt("","1234");
		System.out.println(en);

		System.out.println(BCryptUtil.check("","1234", "$2a$12$mJMKtOtXtEV4pstDnkeLYuFjooSIFiG5rqnAsYwo0R/fdFcupZW3G"));
	}

}
