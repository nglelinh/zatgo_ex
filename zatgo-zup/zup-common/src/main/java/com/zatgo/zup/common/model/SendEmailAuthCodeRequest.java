package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by 46041 on 2018/12/28.
 */
public class SendEmailAuthCodeRequest {

    @ApiModelProperty(value="email",required=true)
    private String email;

    @ApiModelProperty(value="阿里云返回数据",required=true)
    private AliyunValidateCode aliyunValidateCode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AliyunValidateCode getAliyunValidateCode() {
        return aliyunValidateCode;
    }

    public void setAliyunValidateCode(AliyunValidateCode aliyunValidateCode) {
        this.aliyunValidateCode = aliyunValidateCode;
    }
}
