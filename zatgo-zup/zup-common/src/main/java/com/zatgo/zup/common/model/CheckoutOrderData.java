package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.model.coupons.UserCouponsModel;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author
 */
public class CheckoutOrderData implements Serializable {

	private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单编号",required = true)
    private String orderId;
	@ApiModelProperty(value = "付款人ID",required = true)
    private String payUserId;
	@ApiModelProperty(value = "收款人ID",required = true)
    private String receiptUserId;
	@ApiModelProperty(value = "第三方订单号",required = true)
    private String orderFlowCode;
	@ApiModelProperty(value = "主要币种支付状态",required = true)
    private Byte orderStatus;
	
    private String orderType;
	@ApiModelProperty(value = "订单描述",required = true)
    private String orderMemo;
	@ApiModelProperty(value = "订单标题",required = true)
    private String orderName;
	@ApiModelProperty(value = "主币种金额",required = true)
    private BigDecimal orderMoney;

    /**
     * BTC
            ETH
            QTUM
            ZAT
     */
    @ApiModelProperty(value = "主币类型",required = true)
    private String coinType;

    /**
     * BTC
            ETH
            QTUM
     */
    private String coinNetworkType;

    private String appId;

    private String checkoutMetadata;

    private Date createDate;

    private Date updateDate;

    private Date paymentDate;
	@ApiModelProperty(value = "次币金额",required = true)
    private BigDecimal cashAmount;
	@ApiModelProperty(value = "是否混合支付",required = true)
    private Byte isMultiPay;
	@ApiModelProperty(value = "第三方支付类型",required = true)
    private Byte thirdPayType;
	@ApiModelProperty(value = "次币支付状态",required = true)
    private Byte thirdPayStatus;
	@ApiModelProperty(value = "收款方名称",required = true)
    private String receiptUserName;

    /**
     * 代币在支付中所占比例，如1 表示代币在支付比例中为百分之1
     */
    private BigDecimal payCoinScale;

    /**
     * 币种人民币价格
     */
    private BigDecimal coinToCnyPrice;

    private Byte payCoinScaleType;

    private String payUserName;

    private String notifyUrl;

    private BigDecimal originalPrice;

    private String mixCoinType;

    private String mixCoinNetworkType;
    
    private BigDecimal serviceCharge;
    
    @ApiModelProperty(value = "优惠券支付状态：PAYING(0)\n" + 
    		"            PAID(1)\n" + 
    		"            FAIL(2)\n" + 
    		"            REFUND(3),\n" + 
    		"            REFUND_FAIL(4)\n" + 
    		"            ,PAY_UNKOWN_ERROR(5)\n" + 
    		"            REFUND_UNKOWN_ERROR(6)",required = true)
    private Byte usageCouponsStatus;
    
    //----------------------自定义数据------------------------------
    private BusinessEnum.OrderStatus orderStatusEnum;

    private BusinessEnum.ThirdPayType thirdPayTypeEnum;

    private BusinessEnum.OrderStatus thirdPayStatusEnum;
    
    private String coinTypeEnum;

    private String coinNetworkTypeEnum;
    
	private String mixCoinTypeEnum;

	private String mixCoinNetworkTypeEnum;
	
	private String couponsList;
	
	private List<UserCouponsModel> userCouponsList;


	public Byte getUsageCouponsStatus() {
		return usageCouponsStatus;
	}

	public void setUsageCouponsStatus(Byte usageCouponsStatus) {
		this.usageCouponsStatus = usageCouponsStatus;
	}


	public String getCouponsList() {
		return couponsList;
	}

	public void setCouponsList(String couponsList) {
		this.couponsList = couponsList;
	}

	public List<UserCouponsModel> getUserCouponsList() {
		return userCouponsList;
	}

	public void setUserCouponsList(List<UserCouponsModel> userCouponsList) {
		this.userCouponsList = userCouponsList;
	}

	public Byte getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Byte orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getCoinNetworkType() {
		return coinNetworkType;
	}

	public void setCoinNetworkType(String coinNetworkType) {
		this.coinNetworkType = coinNetworkType;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Byte getThirdPayType() {
		return thirdPayType;
	}

	public void setThirdPayType(Byte thirdPayType) {
		this.thirdPayType = thirdPayType;
	}

	public Byte getThirdPayStatus() {
		return thirdPayStatus;
	}

	public void setThirdPayStatus(Byte thirdPayStatus) {
		this.thirdPayStatus = thirdPayStatus;
	}

	public BigDecimal getPayCoinScale() {
		return payCoinScale;
	}

	public void setPayCoinScale(BigDecimal payCoinScale) {
		this.payCoinScale = payCoinScale;
	}

	public BigDecimal getCoinToCnyPrice() {
		return coinToCnyPrice;
	}

	public void setCoinToCnyPrice(BigDecimal coinToCnyPrice) {
		this.coinToCnyPrice = coinToCnyPrice;
	}

	public Byte getPayCoinScaleType() {
		return payCoinScaleType;
	}

	public void setPayCoinScaleType(Byte payCoinScaleType) {
		this.payCoinScaleType = payCoinScaleType;
	}

	public String getPayUserName() {
		return payUserName;
	}

	public void setPayUserName(String payUserName) {
		this.payUserName = payUserName;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public BigDecimal getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}

	public String getMixCoinType() {
		return mixCoinType;
	}

	public void setMixCoinType(String mixCoinType) {
		this.mixCoinType = mixCoinType;
	}

	public String getMixCoinNetworkType() {
		return mixCoinNetworkType;
	}

	public void setMixCoinNetworkType(String mixCoinNetworkType) {
		this.mixCoinNetworkType = mixCoinNetworkType;
	}

	public String getCoinTypeEnum() {
		return coinTypeEnum;
	}

	public void setCoinTypeEnum(String coinTypeEnum) {
		this.coinTypeEnum = coinTypeEnum;
	}

	public String getCoinNetworkTypeEnum() {
		return coinNetworkTypeEnum;
	}

	public void setCoinNetworkTypeEnum(String coinNetworkTypeEnum) {
		this.coinNetworkTypeEnum = coinNetworkTypeEnum;
	}

	public String getMixCoinTypeEnum() {
		return mixCoinTypeEnum;
	}

	public void setMixCoinTypeEnum(String mixCoinTypeEnum) {
		this.mixCoinTypeEnum = mixCoinTypeEnum;
	}

	public String getMixCoinNetworkTypeEnum() {
		return mixCoinNetworkTypeEnum;
	}

	public void setMixCoinNetworkTypeEnum(String mixCoinNetworkTypeEnum) {
		this.mixCoinNetworkTypeEnum = mixCoinNetworkTypeEnum;
	}

	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public BusinessEnum.OrderStatus getOrderStatusEnum() {
		return orderStatusEnum;
	}

	public void setOrderStatusEnum(BusinessEnum.OrderStatus orderStatusEnum) {
		this.orderStatusEnum = orderStatusEnum;
	}

	public BusinessEnum.ThirdPayType getThirdPayTypeEnum() {
		return thirdPayTypeEnum;
	}

	public void setThirdPayTypeEnum(BusinessEnum.ThirdPayType thirdPayTypeEnum) {
		this.thirdPayTypeEnum = thirdPayTypeEnum;
	}

	public BusinessEnum.OrderStatus getThirdPayStatusEnum() {
		return thirdPayStatusEnum;
	}

	public void setThirdPayStatusEnum(BusinessEnum.OrderStatus thirdPayStatusEnum) {
		this.thirdPayStatusEnum = thirdPayStatusEnum;
	}

	public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPayUserId() {
        return payUserId;
    }

    public void setPayUserId(String payUserId) {
        this.payUserId = payUserId;
    }

    public String getReceiptUserId() {
        return receiptUserId;
    }

    public void setReceiptUserId(String receiptUserId) {
        this.receiptUserId = receiptUserId;
    }

    public String getOrderFlowCode() {
        return orderFlowCode;
    }

    public void setOrderFlowCode(String orderFlowCode) {
        this.orderFlowCode = orderFlowCode;
    }

	public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderMemo() {
        return orderMemo;
    }

    public void setOrderMemo(String orderMemo) {
        this.orderMemo = orderMemo;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public BigDecimal getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(BigDecimal orderMoney) {
        this.orderMoney = orderMoney;
    }


    public String getCheckoutMetadata() {
        return checkoutMetadata;
    }

    public void setCheckoutMetadata(String checkoutMetadata) {
        this.checkoutMetadata = checkoutMetadata;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public BigDecimal getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(BigDecimal cashAmount) {
        this.cashAmount = cashAmount;
    }

    public Byte getIsMultiPay() {
        return isMultiPay;
    }

    public void setIsMultiPay(Byte isMultiPay) {
        this.isMultiPay = isMultiPay;
    }

	public String getReceiptUserName() {
		return receiptUserName;
	}

	public void setReceiptUserName(String receiptUserName) {
		this.receiptUserName = receiptUserName;
	}
	
	/**
	 * 订单优惠券使用信息转MAP
	 * @param couponsList
	 * @return
	 */
	public static Map<String,String> parseCouponsListToMap(String couponsList) {
		Map<String,String> map = new HashMap<String, String>();
		if(couponsList == null || couponsList.trim().equals("")) {
			return map;
		}
		
		String[] couponsArray = couponsList.split("#");
		for(String coupons:couponsArray) {
			String[] couponsKeyValue = coupons.split("\\|");
			map.put(couponsKeyValue[0], couponsKeyValue[1]);
		}
		return map;
	}
	
	/**
	 * 订单优惠券使用信息转LIST
	 * @param couponsList
	 * @return
	 */
	public static List<String> parseCouponsListToIdList(String couponsList) {
		List<String> list = new ArrayList<>();
		if(couponsList == null || couponsList.trim().equals("")) {
			return list ;
		}
		
		String[] couponsArray = couponsList.split("#");
		for(String coupons:couponsArray) {
			String[] couponsKeyValue = coupons.split("\\|");
			list.add(couponsKeyValue[0]);
		}
		return list;
	}
	
	/**
	 * 订单优惠券使用信息转字符串
	 * @param list
	 * @return
	 */
	public static String toCouponsList(List<UserCouponsModel> list) {
		if(list == null || list.size() == 0) {
			return "";
		}
		
		String couponsListStr = "";
		for(UserCouponsModel model:list) {
			if(!couponsListStr.equals("")) {
				couponsListStr = couponsListStr + "#";
			}
			
			couponsListStr = couponsListStr + model.getUserCouponsId() + "|" + model.getCouponsName() + "|" + model.getCouponsPrice();
		}
		return couponsListStr;
	}
}