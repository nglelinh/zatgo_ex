//package com.zatgo.zup.common.email;
//
//import com.zatgo.zup.common.exception.BusinessException;
//import com.ykb.mall.common.exception.BusinessExceptionCode;
//import com.ykb.mall.common.model.MessageTemplate;
//import com.ykb.mall.common.model.MessageTemplateExample;
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.JavaMailSenderImpl;
//import org.springframework.mail.javamail.MimeMessageHelper;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//import javax.mail.MessagingException;
//import javax.mail.internet.MimeMessage;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//import java.util.Properties;
//
//@Service("mailService")
//public class MailServiceImpl implements MailService {
//
//    protected Logger logger = LoggerFactory.getLogger(getClass().getName());
//
//    @Autowired
//    private JavaMailSender mailSender;
//
//    @Value("${system.mail.host:}")
//    private String SYSTEM_MAIL_HOST;
//
//    @Value("${system.mail.encoding:}")
//    private String SYSTEM_MAIL_ENCODING;
//
//    @Value("${system.mail.username:}")
//    private String SYSTEM_MAIL_USERNAME;
//
//    @Value("${system.mail.password:}")
//    private String SYSTEM_MAIL_PASSWORD;
//
//    @Value("${system.mail.timeout:}")
//    private String SYSTEM_MAIL_TIMEOUT;
//
//    @Value("${system.mail.from:}")
//    private String SYSTEM_MAIL_FROM;
//
//    @Value("system.mail.receiver:")
//    private String SYSTEM_NOTICE_MAIL_RECEIVER;
//
//    @Value("system.mail.cc:")
//    private String SYSTEM_NOTICE_MAIL_CC;
//
//    @Value("${mail.from}")
//    private String MAIL_FROM;
//
//    @Override
//    public void sendMail(String Subject, String text) {
//        try {
//
//            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//            mailSender.setDefaultEncoding(SYSTEM_MAIL_ENCODING);
//            mailSender.setHost(SYSTEM_MAIL_HOST);
//            mailSender.setUsername(SYSTEM_MAIL_USERNAME); // 根据自己的情况,设置username
//            mailSender.setPassword(SYSTEM_MAIL_PASSWORD); // 根据自己的情况, 设置password
//            Properties prop = new Properties();
//            prop.put("mail.smtp.auth", "true"); // 将这个参数设为true，让服务器进行认证,认证用户名和密码是否正确
//            prop.put("mail.smtp.timeout", SYSTEM_MAIL_TIMEOUT);
//            mailSender.setJavaMailProperties(prop);
//
//            // 建立邮件消息,发送简单邮件和html邮件的区别
//            MimeMessage mailMessage = mailSender.createMimeMessage();
//            MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage);
//
//            // 设置收件人，寄件人
//            String recever = SYSTEM_NOTICE_MAIL_RECEIVER;
//            messageHelper.setTo(StringUtils.split(recever, ","));
//            // 抄送
//            String cc = SYSTEM_NOTICE_MAIL_CC;
//            messageHelper.setCc(StringUtils.split(cc, ","));
//
//            messageHelper.setFrom(SYSTEM_MAIL_FROM);
//            messageHelper.setSubject(Subject);
//
//            // true 表示启动HTML格式的邮件
//            messageHelper.setText(text, true);
//
//            // 发送邮件
//            mailSender.send(mailMessage);
//
//        } catch (MessagingException ee) {
//            logger.error("发送提醒邮件失败！", ee);
//        } catch (Exception e) {
//            logger.error("发送提醒邮件失败！", e);
//        }
//    }
//
//    @Override
//    public void sendEmail(String templateCode, String receiver, Map<String, String> contentMap) {
//
//        throw new BusinessException();
////    	try {
////    		threadPoolTaskExecutor.execute(() -> {
////        		MessageTemplateExample example = new MessageTemplateExample();
////                example.createCriteria().andTemplateCodeEqualTo(templateCode);
////                List<MessageTemplate> templates = messageTemplateMapper.selectByExample(example);
////                if (CollectionUtils.isEmpty(templates)) {
////                    throw new BusinessException(BusinessExceptionCode.MESSAGE_TEMPLATE_ERROR, "未找到相应消息模板");
////                }
////                MessageTemplate template = templates.get(0);
////                String messageContent = messageService.getMessageContent(template.getTemplateContent(), contentMap);
////                if (StringUtils.isEmpty(messageContent)) {
////                    throw new BusinessException(BusinessExceptionCode.MESSAGE_TEMPLATE_ERROR, "消息内容为空");
////                }
////                // 发邮件
////                Byte result = sendHtmlMail(receiver, template.getTemplateName(), messageContent);
////                // 新增消息记录
////                messageService.createMessageRecord(template, receiver, messageContent, result, null);
////        	});
////		} catch (Exception e) {
////			logger.error("", e);
////		}
//
//    }
//
//
//    @Override
//    public void sendMailByUser(String Subject, String text, String userEmail) {
//        try {
//
//            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//            mailSender.setDefaultEncoding(SYSTEM_MAIL_ENCODING);
//            mailSender.setHost(SYSTEM_MAIL_HOST);
//            mailSender.setUsername(SYSTEM_MAIL_USERNAME); // 根据自己的情况,设置username
//            mailSender.setPassword(SYSTEM_MAIL_PASSWORD); // 根据自己的情况, 设置password
//            Properties prop = new Properties();
//            prop.put("mail.smtp.auth", "true"); // 将这个参数设为true，让服务器进行认证,认证用户名和密码是否正确
//            prop.put("mail.smtp.timeout", SYSTEM_MAIL_TIMEOUT);
//            mailSender.setJavaMailProperties(prop);
//
//            // 建立邮件消息,发送简单邮件和html邮件的区别
//            MimeMessage mailMessage = mailSender.createMimeMessage();
//            MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage);
//
//            // 设置收件人，寄件人
//            messageHelper.setTo(StringUtils.split(userEmail, ","));
//            messageHelper.setFrom(SYSTEM_MAIL_FROM);
//            messageHelper.setSubject(Subject);
//
//            // true 表示启动HTML格式的邮件
//            messageHelper.setText(text, true);
//
//            // 发送邮件
//            mailSender.send(mailMessage);
//
//        } catch (MessagingException ee) {
//            logger.error("发送提醒邮件失败！", ee);
//        } catch (Exception e) {
//            logger.error("发送提醒邮件失败！", e);
//        }
//    }
//
//    /**
//     * 发送邮件
//     *
//     * @param subject
//     * @param text
//     */
//    @Override
//    public Byte sendHtmlMail(String receiveEmail, String subject, String text) {
//        try {
//            MimeMessage mimeMessage = mailSender.createMimeMessage();
//            //是否发送的邮件是富文本（附件，图片，html等）
//            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
//            messageHelper.setFrom(MAIL_FROM);
//            messageHelper.setTo(receiveEmail);
//            messageHelper.setSubject(subject);
//            messageHelper.setText(text, true);//重点，默认为false，显示原始html代码，无效果
//            mailSender.send(mimeMessage);
//            return 1;
//        } catch (Exception e) {
//            logger.error("邮件发送失败，", e);
//            return 2;
//        }
//    }
//
//    @Override
//    public void sendMailByTemplate(String subject, String email, Map<String, String> data, String templateContent) {
//        //获取发送内容
//        String sendContent = replaceTemplateparams(data, templateContent);
//
//        sendMailByUser(subject, sendContent, email);
//    }
//
//    private String replaceTemplateparams(Map<String, String> data, String templateContent) {
//        for (Map.Entry<String, String> entry : data.entrySet()) {
//            templateContent = templateContent.replaceAll(entry.getKey(), entry.getValue());
//        }
//        return templateContent;
//    }
//
//}
