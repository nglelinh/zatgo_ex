package com.zatgo.zup.common.mq;

public interface MQConsumerCallBack {

	boolean callBack(String json);
}
