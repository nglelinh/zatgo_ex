package com.zatgo.zup.common.redis;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by 46041 on 2018/9/13.
 */
@Component
public class RedisUtils {

    @Autowired
    private RedisTemplate redisTemplate;

    //默认存储时间 1天
    private static final Long DEFAULT_SAVE_TIME = 1l;


    public void put(String k, Object value) {
        String v = JSONObject.toJSONString(value);
        redisTemplate.opsForValue().set(k, v, DEFAULT_SAVE_TIME, TimeUnit.DAYS);
    }

    public void putList(String k, Object value) {
        String v = JSONArray.toJSONString(value);
        redisTemplate.opsForValue().set(k, v, DEFAULT_SAVE_TIME, TimeUnit.DAYS);
    }

    public void putList(String k, Object value, long time, TimeUnit type) {
        String v = JSONArray.toJSONString(value);
        redisTemplate.opsForValue().set(k, v, time, type);
    }

    public Long getExpire(String k, TimeUnit unit){
        return redisTemplate.getExpire(k, unit);
    }

    public void put(String k, Object value, long time, TimeUnit type) {
        String v = JSONObject.toJSONString(value);
        redisTemplate.opsForValue().set(k, v, time, type);
    }

    public <T> T get (String k, Class<T> clazz) {
        if (hasKey(k)){
            String value = (String) redisTemplate.opsForValue().get(k);
            return JSONObject.parseObject(value, clazz);
        }
        return null;
    }

    public <T extends Collection> T getList (String k, Class<T> clazz) {
       if (hasKey(k)){
           String value = (String) redisTemplate.opsForValue().get(k);
           return JSONArray.parseObject(value, clazz);
       }
       return null;
    }

    public void removeKey(String k) {
        if (k.contains("*")){
            Set keys = redisTemplate.keys(k);
            redisTemplate.delete(keys);
        } else {
            redisTemplate.delete(k);
        }
    }

    public Object get (String k) {
        return redisTemplate.opsForValue().get(k);
    }

    public Set<String> getKeys(String k){
        return redisTemplate.keys(k);
    }

    public boolean hasKey (String k) {
        return redisTemplate.hasKey(k);
    }
    
    @Bean
	public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory)
	{
	        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class);
	        RedisSerializer<String> stringSerializer = new StringRedisSerializer();
	        ObjectMapper om = new ObjectMapper();
	        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
	        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
	        jackson2JsonRedisSerializer.setObjectMapper(om);
	        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
	        template.setConnectionFactory(redisConnectionFactory);
	        template.setKeySerializer(stringSerializer);
	        template.setValueSerializer(jackson2JsonRedisSerializer);
	        template.setHashKeySerializer(jackson2JsonRedisSerializer);
	        template.setHashValueSerializer(jackson2JsonRedisSerializer);
	        template.afterPropertiesSet();
	        return template;
	}
}
