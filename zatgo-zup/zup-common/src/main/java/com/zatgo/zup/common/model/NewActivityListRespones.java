package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 46041 on 2019/5/30.
 */

@ApiModel(value = "活动")
public class NewActivityListRespones {
    @ApiModelProperty(value = "活动ID",required = true)
    private String activityId;
    @ApiModelProperty(value = "活动名称",required = true)
    private String activityName;
    @ApiModelProperty(value = "活动开始时间",required = true)
    private Date activityCreateTime;
    @ApiModelProperty(value = "活动图片",required = true)
    private String activityImage;
    @ApiModelProperty(value = "活动类型 1-砍价 2-拼团",required = true)
    private String activityType;
    @ApiModelProperty(value = "加入活动的ID",required = true)
    private String activityJoinId;
    @ApiModelProperty(value = "团的ID",required = true)
    private String activityOrderId;
    @ApiModelProperty(value = "活动描述",required = true)
    private String activityDesc;
    @ApiModelProperty(value = "活动状态",required = true)
    private String activityStatus;

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Date getActivityCreateTime() {
        return activityCreateTime;
    }

    public void setActivityCreateTime(Date activityCreateTime) {
        this.activityCreateTime = activityCreateTime;
    }

    public String getActivityImage() {
        return activityImage;
    }

    public void setActivityImage(String activityImage) {
        this.activityImage = activityImage;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getActivityJoinId() {
        return activityJoinId;
    }

    public void setActivityJoinId(String activityJoinId) {
        this.activityJoinId = activityJoinId;
    }

    public String getActivityOrderId() {
        return activityOrderId;
    }

    public void setActivityOrderId(String activityOrderId) {
        this.activityOrderId = activityOrderId;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }
}
