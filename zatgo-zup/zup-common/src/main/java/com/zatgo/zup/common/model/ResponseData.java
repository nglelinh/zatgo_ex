package com.zatgo.zup.common.model;

import java.io.Serializable;

import com.zatgo.zup.common.exception.BusinessExceptionCode;

public class ResponseData<T>  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String code;
	private String message;
	private T data;
	private String sequence;
	private String sign;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public boolean isSuccessful() {
		if(code != null && (code.equals(BusinessExceptionCode.SUCCESS_CODE) || code.equals(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING))) {
			return true;
		}else {
			return false;
		}
	}
	
}
