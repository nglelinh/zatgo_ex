package com.zatgo.zup.common.mq.aliyunmq;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.Producer;
import com.aliyun.openservices.ons.api.SendResult;
import com.aliyun.openservices.ons.api.order.OrderProducer;
import com.aliyun.openservices.ons.api.transaction.LocalTransactionExecuter;
import com.aliyun.openservices.ons.api.transaction.TransactionProducer;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.mq.MQService;
import com.zatgo.zup.common.mq.aliyunmq.tool.AliyunMQConfig;
import com.zatgo.zup.common.mq.aliyunmq.tool.AliyunMQProducer;

@Component("AliYunMQServiceImpl")
public class AliYunMQServiceImpl implements MQService {

	private Logger logger = LoggerFactory.getLogger(getClass().getName());

    //延时/定时消息最大延迟时间，40天
    private static final Long MAX_DELAY_TIME = 40*24*60*60*1000L;

    //事物消息回查
    @Autowired
    private LocalTransactionExecuter localTransactionExecuter;
    
    @Autowired
    private AliyunMQConfig aliyunMQConfig;
    
    private String producerIdPrefix = "PID_";


    @Override
    public void sendMQSeqMessage(String topic, String tag, String body, String key, String shardingKey) {
        try{
            OrderProducer producer = AliyunMQProducer.getAliyunOrderProducer(producerIdPrefix + topic);
            //实例待发送的消息对象
            Message msg = new Message(topic, null, body.getBytes("utf-8"));
            // 设置代表消息的业务关键属性，请尽可能全局唯一。以方便您在无法正常收到消息情况下，可通过ONS Console查询消息并补发。
            if(StringUtils.isNotEmpty(key)){
                msg.setKey(key);
            }
            // 发送消息，只要不抛异常就是成功
            SendResult sendResult = producer.send(msg, shardingKey);
            logger.info(sendResult.toString());

        }catch(Exception e){
            logger.error("发送MQ信息失败，topic=" + topic +",tag=" + tag + ",message=" + body, e);
            throw new BusinessException();
        }
    }


    @Override
    public void sendMQDelayMessage(String topic, String tag, String body, String key, Long delayTime) {
        try{
            //校验延迟消息的时间是否超过40天
            if(delayTime > MAX_DELAY_TIME){
                logger.info("延迟消息最大延迟时间不可超过40天");
                throw new BusinessException();
            }
            Producer producer = AliyunMQProducer.getAliyunProducer(producerIdPrefix + topic);
            //实例待发送的消息对象
            Message msg = new Message(topic, null, body.getBytes("utf-8"));
            // 设置代表消息的业务关键属性，请尽可能全局唯一。以方便您在无法正常收到消息情况下，可通过ONS Console查询消息并补发。
            if(StringUtils.isNotEmpty(key)){
                msg.setKey(key);
            }
            //设置延迟消息发送时间
            Long sendTime = System.currentTimeMillis() + delayTime;
            msg.setStartDeliverTime(sendTime);
            logger.info("消息发送时间："+ new Date(sendTime));
            //发送消息，只要不抛异常就是成功
            SendResult sendResult = producer.send(msg);
            logger.info(sendResult.toString());
        }catch(Exception e){
            logger.error("发送MQ信息失败，topic=" + topic +",tag=" + tag + ",message=" + body, e);
            throw new BusinessException();
        }
    }


    @Override
    public void sendMQTimingMessage(String topic, String tag, String body, String key, Date Timing) {
        try{
            //校验定时消息的时间是否超过40天
            if(Timing.getTime()-System.currentTimeMillis() > MAX_DELAY_TIME){
                logger.info("延迟消息最大延迟时间不可超过40天");
                throw new BusinessException();
            }
            Producer producer = AliyunMQProducer.getAliyunProducer(producerIdPrefix + topic);
            //实例待发送的消息对象
            Message msg = new Message(topic, topic+tag, body.getBytes("utf-8"));
            // 设置代表消息的业务关键属性，请尽可能全局唯一。以方便您在无法正常收到消息情况下，可通过ONS Console查询消息并补发。
            if(StringUtils.isNotEmpty(key)){
                msg.setKey(key);
            }
            //设置定时消息发送时间
            msg.setStartDeliverTime(Timing.getTime());
            logger.info("消息发送时间："+ Timing);
            //发送消息，只要不抛异常就是成功
            SendResult sendResult = producer.send(msg);
            logger.info(sendResult.toString());
        }catch(Exception e){
            logger.error("发送MQ信息失败，topic=" + topic +",tag=" + tag + ",message=" + body, e);
            throw new BusinessException();
        }
    }

    @Override
    public void sendMQTranscctionMessage(String topic, String tag, String body, String key) {
        try{
            TransactionProducer producer = AliyunMQProducer.getAliyunTransactionProducer(producerIdPrefix + topic);
            //实例待发送的消息对象
            Message msg = new Message(topic, topic+tag, body.getBytes("utf-8"));
            SendResult sendResult = producer.send(msg, localTransactionExecuter, null);
        }catch(Exception e){
            logger.error("发送MQ事物信息失败，topic=" + topic +",tag=" + tag + ",message=" + body, e);
            throw new BusinessException();
        }
    }

    @Override
    public void sendMQmessage(String topic, String tag, String body, String key) throws UnsupportedEncodingException {
        try{
            Producer producer = AliyunMQProducer.getAliyunProducer(producerIdPrefix + topic);
            //实例待发送的消息对象
            Message msg = new Message(topic, topic+tag, body.getBytes("utf-8"));
            // 设置代表消息的业务关键属性，请尽可能全局唯一。以方便您在无法正常收到消息情况下，可通过ONS Console查询消息并补发。
            if(StringUtils.isNotEmpty(key)){
                msg.setKey(key);
            }
            //发送消息，只要不抛异常就是成功
            SendResult sendResult = producer.send(msg);
            logger.info(sendResult.toString());
        }catch(Exception e){
            logger.error("发送MQ信息失败，topic=" + topic +",tag=" + tag + ",message=" + body, e);
            throw new BusinessException();
        }
    }

}
