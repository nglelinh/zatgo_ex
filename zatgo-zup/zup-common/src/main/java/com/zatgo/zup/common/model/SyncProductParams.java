package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class SyncProductParams {

	@ApiModelProperty(value = "商品名",required = true)
	private String productName;
	
	@ApiModelProperty(value = "商品ID",required = true)
	private String productId;
	
	@ApiModelProperty(value = "商品缩略图URL",required = true)
	private String productImgAbbrUrl;
	
	@ApiModelProperty(value = "下单地址",required = true)
	private String addOrderUrl;
	
	@ApiModelProperty(value = "商品原始价格",required = true)
	private BigDecimal productMoney;

	@ApiModelProperty(value = "促销价",required = true)
	private BigDecimal promotionPrice;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductImgAbbrUrl() {
		return productImgAbbrUrl;
	}

	public void setProductImgAbbrUrl(String productImgAbbrUrl) {
		this.productImgAbbrUrl = productImgAbbrUrl;
	}

	public String getAddOrderUrl() {
		return addOrderUrl;
	}

	public void setAddOrderUrl(String addOrderUrl) {
		this.addOrderUrl = addOrderUrl;
	}

	public BigDecimal getProductMoney() {
		return productMoney;
	}

	public void setProductMoney(BigDecimal productMoney) {
		this.productMoney = productMoney;
	}

	public BigDecimal getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(BigDecimal promotionPrice) {
		this.promotionPrice = promotionPrice;
	}
}
