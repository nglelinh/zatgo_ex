package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by 46041 on 2018/9/18.
 */
public class WebSocketBaseParams{

    @ApiModelProperty(value = "appId", required = true)
    private String appId;

    @ApiModelProperty(value = "app类型", required = true)
    private String appType;

    @ApiModelProperty(value = "app版本", required = true)
    private String appVersion;

    @ApiModelProperty(value = "ts", required = true)
    private String ts;

    @ApiModelProperty(value = "登陆后返回的token", required = true)
    private String token;

    @ApiModelProperty(value = "签名", required = true)
    private String sign;

    @ApiModelProperty(value = "云用户Id", required = true)
    private String cloudUserId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }
}
