package com.zatgo.zup.common.redis;

public class RedisKeyConstants {

	private static final String BASE_PRE = "ZUP_";
	
//-----------------------------------redis key------------------------------------------------------------	
	/**
	 * 用户登入锁
	 */
    public static final String USER_LOGIN_LOCK_PRE = BASE_PRE + "USER_LOGIN_LOCK_";
	/**
	 *  订单任务取消锁
	 */
	public static final String GROUP_TASK_PRE = "CANCEL_ORDER_TASK_PRE";
	/**
	 * 砍价活动锁
	 */
	public static final String BARGAIN_TASK_PRE = "CANCEL_ORDER_TASK_PRE";
	/**
	 * 拼团活动锁
	 */
	public static final String CANCEL_ORDER_TASK_PRE = "CANCEL_ORDER_TASK_PRE";
    
    /**
     * 验证码
     */
	public static final String VERIFY_CODE_PRE = BASE_PRE + "VERIFY_CODE_";
	 /**
     * 验证码错误次数
     */
	public static final String VERIFY_CODE_ERROR_PRE = BASE_PRE + "VERIFY_CODE_ERROR_";
	
	/**
	 * auth token 前缀
	 */
	public static final String AUTH_TOKEN_PRE = BASE_PRE + "AUTH_TOKEN_PRE_";

	/**
	 * auth token 前缀
	 */
	public static final String WECHAT_AUTH_TOKEN_PRE = BASE_PRE + "WECHAT_AUTH_TOKEN_PRE:";
	/**
	 * 支付密码锁定
	 */
	public static final String USER_PAY_PASS_LOCK  = BASE_PRE + "USER_PAY_PASS_LOCK_";
	
	/**
	 * 支付回调
	 */
	public final static String NOTICE_PAY_CALLBACK_PRE = BASE_PRE + "NOTICE_PAY_CALLBACK_PRE_";
	
	/**
	 * 币价查询锁
	 */
	public static final String BUSI_MARKET_PRICE_QUERY_LOCK = BASE_PRE + "MARKET_PRICE_QUERY_LOCK_";
	
	/**
	 * 币价查询缓存数据
	 */
	public static final String BUSI_MARKET_PRICE_QUERY_DATA = BASE_PRE + "MARKET_PRICE_QUERY_DATA_";

	/**
	 *  云用户key值
	 */
	public static final String CLOUD_USER_KEY = BASE_PRE + "CLOUD:USER:API:";

	/**
	 *  云用户key值
	 */
	public static final String SYSTEM_COUNTRY_INFO = BASE_PRE + "SYSTEM:COUNTRY:INFO:";

	/**
	 * 交易机互斥key
	 */
	public static final String TRANSACTION_LIVE_KEY = BASE_PRE + "transaction_live_key_";
    /**
     * 挖矿排行key
     */
    public static final String COMPUTE_POWER_TOP_KEY = BASE_PRE +  "compute_power_top_key";
	/**
	 * 任务缓存key
	 */
	public static final String TASK_CACHE_KEY = BASE_PRE +  "task_cache_key";
	/**
	 * 任务缓存key
	 */
	public static final String GAME_REWARD_CARVE_UP = BASE_PRE +  "game_reward_carve_up_key";


//-----------------------------------锁值------------------------------------------------------------
	/**
	 * reids 锁key
	 */
	public static final String LOCK = BASE_PRE + "LOCK_";
	
	/**
	 * 充值业务
	 */
	public static final String BUSI_DEPOSIT = BASE_PRE + "DEPOSIT_";
	
	/**
	 * 提币业务
	 */
	public static final String BUSI_EXTRACT = BASE_PRE + "EXTRACT_";
	
	/**
	 * 账户
	 */
	public static final String BUSI_ACCOUNT = BASE_PRE + "ACCOUNT_";
	
	/**
	 * 用户
	 */
	public static final String BUSI_USER = BASE_PRE + "USER_";
	
	/**
	 * 地址
	 */
	public static final String BUSI_ADDRESS = BASE_PRE + "ADDRESS_";

	/**
	 * 挖矿
	 */
	public static final String MINING_LOCK = BASE_PRE + "MINING_";
	/**
	 * 任务
	 */
	public static final String TASK_LOCK = BASE_PRE + "TASK_";
	/**
	 * 答题
	 */
	public static final String TASK_ANSWER = BASE_PRE + "ANSWER_";
	/**
	 * 用户邀请码
	 */
	public static final String USER_INVITATION_CODE = BASE_PRE + "USER_INVITATION_CODE_";

	/**
	 * 交易机提币锁
	 */
	public static final String TRANSACTION_EXTRACT_LOCK = BASE_PRE + "TRANSACTION_EXTRACT_LOCK_";

    /**
     * 充值的时候新增代币表地址
     */
    public static final String TRANSACTION_ADD_ADDRESS_LOCK = BASE_PRE + "TRANSACTION_ADD_ADDRESS_LOCK_";
    /**
     * 修改地址余额的锁
     */
    public static final String UPDATE_ADDRESS_BALANCE_LOCK = BASE_PRE + "UPDATE_ADDRESS_BALANCE_LOCK";

	/**
	 * 减缓存余额锁
	 */
	public static final String UPDATE_CACHE_BALANCE_LOCK = BASE_PRE + "UPDATE_CACHE_BALANCE_LOCK";
	
	public static final String GET_CACHE_BALANCE_LOCK = BASE_PRE + "GET_CACHE_BALANCE_LOCK";

	/**
	 * QTUM修改地址余额的锁
	 */
	public static final String QTUM_UPDATE_ADDRESS_BALANCE_LOCK = BASE_PRE + "QTUM_UPDATE_ADDRESS_BALANCE_LOCK";

//	----------------------------------业务锁结束-----------------------------------------------
	
	/**
	 * 币赠送
	 */
	public static final String COIN_PRESENT = BASE_PRE + "COIN_PRESENT_";
	public static final String PREPAY_ = BASE_PRE + "PREPAY_";
	public static final String SIGN_DATA = BASE_PRE + "SIGN_DATA";
	
	public static final String COIN_TICKER = BASE_PRE + "COIN_TICKER_";
	
	public static final String COIN_RATE = BASE_PRE + "COIN_RATE";
	
	public static final String COIN_PAIR_CACHE = BASE_PRE + "COIN_PAIR_CACHE";
	
	public static final String CLOUD_USER_COIN_PAIR_CACHE = BASE_PRE + "CLOUD_USER_COIN_PAIR_CACHE";

	public static final String SYMBOL = BASE_PRE + "SYMBOL";

	public static final String EXCHANGE_TYPE = BASE_PRE + "EXCHANGE_TYPE";

	public static final String TRANSACTION_ADDRESS_LOCK = BASE_PRE + "transaction_address_lock:";
	
	public static final String PAYMENT_WEB_CHECKOUT_BALANCE = BASE_PRE + "PAYMENT_WEB_CHECKOUT_BALANCE";



	/***********游戏相关**********************************************/

	/**
	 * 游戏运行分布式锁，根据用户锁
	 */
	public static final String GAME_RUN_LOCK_PRE = "GAME_RUN_LOCK_";

	public static final String GAME_REWARD_LOCK_PRE = "GAME_REWARD_LOCK_";

	public static final String GAME_QUESTION_LOCK_PRE = "GAME_QUESTION_LOCK_";
	/**
	 * 用户当日游戏次数，按 key+用户ID+日期，失效时间24小时
	 */
	public static final String GAME_RUN_TIMES_PRE = "GAME_RUN_TIMES_";
	/**
	 * 用户永久免费游戏次数
	 */
	public static final String GAME_RUN_FREE_TIMES_PRE = "GAME_RUN_FREE_TIMES_";
	/**
	 * 游戏总运行次数
	 */
	public static final String GAME_TOTAL_TIMES_PRE = "GAME_TOTAL_TIMES_";
	/**
	 * 游戏总资金池，用户付费增加，发奖后减少
	 */
	public static final String GAME_TOTAL_AMOUNT_PRE = "GAME_TOTAL_AMOUNT_";
	/**
	 * 游戏黑名单
	 */
	public static final String GAME_BLACK_LIST_PRE = "GAME:BLICKLIST:";
	
	/**
	 * 同一IP不能使用多个token
	 */
	public static final String GATEWAY_LIMIT_IP_TOKEN_PRE = "GATEWAY_LIMIT:IP:TOKEN";
	
	/**
	 * 同一IP不能注册多个用户
	 */
	public static final String GATEWAY_LIMIT_IP_REGIST_PRE = "GATEWAY_LIMIT:IP:REGIST";
	
	/**
	 * 同一设备号不能使用多个token
	 */
	public static final String GATEWAY_LIMIT_DEVICE_NO_TOKEN_PRE = "GATEWAY_LIMIT:DEVICE_NO:TOKEN";
	
	/**
	 * 同一设备号不能注册多个用户
	 */
	public static final String GATEWAY_LIMIT_DEVICE_NO_REGIST_PRE = "GATEWAY_LIMIT:DEVICE_NO:REGIST";



	/********************************************************/


	/****************************活动相关****************************/

	public static final String BARGAIN_LOCK = "BARGAIN_LOCK_";

	public static final String BARGAIN_CREATE_USER_LOCK = "BARGAIN_CREATE_USER_LOCK_";

	public static final String BARGAIN_CANCEL_ORDER_LOCK = "BARGAIN_CANCEL_ORDER_LOCK_";

	public static final String BARGAIN_CREATE_ORDER_LOCK = "BARGAIN_CREATE_ORDER_LOCK_";

	public static final String CREATE_GROUP_BOOKING_ORDER_LOCK = "CREATE_GROUP_BOOKING_ORDER_LOCK_";

	public static final String JOIN_GROUP_BOOKING_LOCK = "JOIN_GROUP_BOOKING_LOCK_";

	public static final String CANCEL_GROUP_BOOKING_LOCK_ = "CANCEL_GROUP_BOOKING_LOCK_";



	/********************************************************/
	
	
	public static final String FIXED_TIME_DEPOSIT_PRO_PRE = BASE_PRE + "FIXED_TIME_DEPOSIT_PRO_";
	
	public static final String FIXED_TIME_DEPOSIT_USER_PRE = BASE_PRE + "FIXED_TIME_DEPOSIT_USER_";
	
	public static final String COUPONS_USER_ID_PRE = BASE_PRE + "COUPONS_USER_ID_";
	
	public static final String COUPONS_CACHE_KEY = BASE_PRE + "COUPONS_CACHE";
	
	public static final String COUPONS_CACHE_UPDATE_PRO = BASE_PRE + "COUPONS_CACHE_UPDATE_";
	
	public static final String PAYMENT_ORDER_PRO = BASE_PRE + "PAYMENT_ORDER_";



	public static String WECHAT_ACCESS_TOKEN = "WECHAT_ACCESS_TOKEN";

	public static String GET_WECHAT_ACCESS_TOKEN_LOCK = "GET_WECHAT_ACCESS_TOKEN_LOCK_";

	public static String WECHAT_ACCESS_TOKEN_BACKUP = "WECHAT_ACCESS_TOKEN_BACKUP";

	public static String WECHAT_JSAPI_TICKET = "WECHAT_JSAPI_TICKET";

	public static String WECHAT_JSAPI_TICKET_BACKUP = "WECHAT_JSAPI_TICKET_BACKUP";

	public static final String WECHAT_ACCESS_TOKEN_KEY = "WECHAT_ACCESS_TOKEN_KEY";

	/**
	 * 微信公众号二维码
	 */
	public static final String WECHAT_QR = "WECHAT_USER_QR:";


	/**************************flyby open api******************************/
	public static final String FLYBY_OPEN_API_RSA_KEY = "FLYBY_OPEN_API_RSA_KEY_";
	public static final String FLYBY_OPEN_API_RSA_PUBLIC_KEY = "FLYBY_OPEN_API_RSA_PUBLIC_KEY";
	public static final String FLYBY_OPEN_API_RSA_PRIVATE_KEY = "FLYBY_OPEN_API_RSA_PRIVATE_KEY";
	public static final String FLYBY_OPEN_API_RSA_KEY_LOCK = "FLYBY_OPEN_API_RSA_KEY_LOCK_";
	
	public static final String FLYBY_OPEN_API_SYNC_BATCH_USER_LOCK = "FLYBY_OPEN_API_SYNC_BATCH_USER_LOCK_";
	public static final String FLYBY_OPEN_API_SYNC_PRO_APPROVEINFO_LOCK = "FLYBY_OPEN_API_SYNC_PRO_APPROVEINFO_LOCK_";
	public static final String FLYBY_OPEN_API_SIGN_LOGIN_LOCK = "FLYBY_OPEN_API_SIGN_LOGIN_LOCK_";
	
	/**
	 * 表主键
	 */
	public static final String TABLE_PRIMARY_KEY = "TABLE_PRIMARY_KEY";

	/**
	 * 组装缓存key
	 * @param prefix 前缀 
	 * @param key key
	 * @return
	 */
	public static String getRedisKey(String prefix, String key) {
		return new StringBuilder(prefix).append(key).toString();
	}


}
