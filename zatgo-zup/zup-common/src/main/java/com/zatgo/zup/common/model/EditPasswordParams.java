package com.zatgo.zup.common.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "修改密码接口参数")
public class EditPasswordParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "原密码",required = true)
	private String oldPassword;
	
	@ApiModelProperty(value = "新密码",required = true)
	private String newPassword;
	
	@ApiModelProperty(value="助记词",required=false,hidden=true)
	private String mnemonicWord;
	
	public String getMnemonicWord() {
		return mnemonicWord;
	}
	public void setMnemonicWord(String mnemonicWord) {
		this.mnemonicWord = mnemonicWord;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	
}
