package com.zatgo.zup.common.masterslave;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FollowerRunnable implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(FollowerRunnable.class);
	
	
	private static MasterSlaveClusterCoordination coordination;
	
	public static volatile boolean isLeader = false;
	
	@Override
	public void run() {
		while(true){
			try{
				Thread.sleep(5000);
				boolean leaderSuccess = false;
				
				if(!coordination.isMasterExisted()){
					leaderSuccess = coordination.tryToLeaderMaster();
					if(leaderSuccess){
						logger.info("current process be leader to master!");
						continue;
					}else{
						logger.info("master has been restored,current master leader fail!");
					}
				}
				
				
				if(coordination.isCurrentMaster()){
					coordination.unregisterCurrentProcessFromSlave();
					isLeader = true;
					continue;
				}else{
					if(isLeader == true){
						logger.error("existed other master process,Edit current master process to slave!");
					}else{
						logger.debug("existed other master process,current process leader fail!");
					}
					isLeader = false;
					coordination.registerCurrentProcessToSlave();
					
				}
					

			}catch(Throwable e){
				logger.error("Unknow exception",e);
			}
		}

	}
}
