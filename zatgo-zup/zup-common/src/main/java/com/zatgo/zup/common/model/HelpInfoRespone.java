package com.zatgo.zup.common.model;

/**
 * Created by 46041 on 2019/5/23.
 */
public class HelpInfoRespone {

    /**
     * 被帮助人的用户昵称
     */
    private String userNick;
    /**
     * 是否帮助过了
     */
    private Boolean isHelp;
    /**
     * 用戶是否已关注
     */
    private Boolean isSubscribe;

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public Boolean getHelp() {
        return isHelp;
    }

    public void setHelp(Boolean help) {
        isHelp = help;
    }

    public Boolean getSubscribe() {
        return isSubscribe;
    }

    public void setSubscribe(Boolean subscribe) {
        isSubscribe = subscribe;
    }
}
