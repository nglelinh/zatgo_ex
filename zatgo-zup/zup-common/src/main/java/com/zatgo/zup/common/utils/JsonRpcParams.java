package com.zatgo.zup.common.utils;

public class JsonRpcParams {

	private String to;

	private String data;

	public JsonRpcParams(String to, String data) {
		this.to = to;
		this.data = data;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}


}
