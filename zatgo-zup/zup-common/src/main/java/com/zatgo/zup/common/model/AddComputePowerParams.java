package com.zatgo.zup.common.model;

import com.zatgo.zup.common.enumtype.BusinessEnum;

import java.util.Date;

/**
 * Created by 46041 on 2018/12/25.
 */
public class AddComputePowerParams {

    private String userId;

    private Long computeNum;

    private BusinessEnum.ComputePowerSourceType computePowerSourceType;

    private BusinessEnum.ComputePowerType computePowerType;

    private Date validStartDate;

    private Date validEndDate;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getComputeNum() {
        return computeNum;
    }

    public void setComputeNum(Long computeNum) {
        this.computeNum = computeNum;
    }

    public BusinessEnum.ComputePowerSourceType getComputePowerSourceType() {
        return computePowerSourceType;
    }

    public void setComputePowerSourceType(BusinessEnum.ComputePowerSourceType computePowerSourceType) {
        this.computePowerSourceType = computePowerSourceType;
    }

    public BusinessEnum.ComputePowerType getComputePowerType() {
        return computePowerType;
    }

    public void setComputePowerType(BusinessEnum.ComputePowerType computePowerType) {
        this.computePowerType = computePowerType;
    }

    public Date getValidStartDate() {
        return validStartDate;
    }

    public void setValidStartDate(Date validStartDate) {
        this.validStartDate = validStartDate;
    }

    public Date getValidEndDate() {
        return validEndDate;
    }

    public void setValidEndDate(Date validEndDate) {
        this.validEndDate = validEndDate;
    }
}
