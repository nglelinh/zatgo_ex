package com.zatgo.zup.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "支付接口参数")
public class PageInfo<T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "当前页码",required = true)
	private int pageNum;
    
	@ApiModelProperty(value = "每页条数",required = true)
    private int pageSize;
    
	@ApiModelProperty(value = "当前返回条数",required = true)
    private int size;

	@ApiModelProperty(value = "总条数",required = true)
    private long total;

	@ApiModelProperty(value = "数据集合",required = true)
    private List<T> list;

	@ApiModelProperty(value = "总页数",required = true)
	private int pages;
	
	public PageInfo(int pageNum,int pageSize,long total,List<T> list) {
		this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.size = list.size();
        this.total = total;
        this.list = list;

		this.pages = (int)total/pageSize;
		if(total%pageSize!=0)
			this.pages++;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

}
