package com.zatgo.zup.common.mq.rocketmq;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.UtilAll;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.mq.MQConsumerCallBack;

@Component
public class RocketMQConsumer {
	private static final Logger logger = LoggerFactory.getLogger(RocketMQConsumer.class);
	
	@Value("${mq.rocketmq.url:}")
	private String svrAddress;
	
	private static final String CONSUMER_GROUP = "ZUP_CONSUMER_GROUP";
	
	private ConcurrentHashMap<String, DefaultMQPushConsumer> consumers = new ConcurrentHashMap<>();
	
	public void startConsumer(String consumerGroup,String topic,String tags,MQConsumerCallBack callBack,MessageModel model){
		DefaultMQPushConsumer consumer = consumers.get(consumerGroup + "_" + topic);
		if(consumer != null) {
			logger.error("重复启动rocketmq consumer consumerGroup=" + consumerGroup + ",topic=" + topic);
			return;
		}
		
		try {
			consumer = new DefaultMQPushConsumer(consumerGroup);
			if(tags == null) {
				consumer.subscribe(topic,"*");
			}else {
				consumer.subscribe(topic,tags);
			}
			
			consumer.setNamesrvAddr(svrAddress);
			consumer.setMessageModel(model);
			consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_TIMESTAMP);
			consumer.setConsumeTimestamp(UtilAll.timeMillisToHumanString3(System.currentTimeMillis()));
			consumer.registerMessageListener(new MessageListenerConcurrently() {
				@Override
				public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
					MessageExt message = list.get(0);
					try {
						String json = new String(message.getBody(), RemotingHelper.DEFAULT_CHARSET);
						callBack.callBack(json);
					} catch (Exception e) {
						logger.error("message put error",e);
					} finally {
						return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
					}
				}
			});
			consumer.start();
		} catch (Exception e) {
			logger.error("consumer error",e);
		}
	}


	@PreDestroy
	public void close(){
		if(consumers != null) {
			Iterator<Entry<String, DefaultMQPushConsumer>> iterator = consumers.entrySet().iterator();
			while(iterator.hasNext()) {
				Entry<String, DefaultMQPushConsumer> entry = iterator.next();
				DefaultMQPushConsumer consumer = entry.getValue();
				consumer.shutdown();
			}
		}
	}














}
