package com.zatgo.zup.common.mq.aliyunmq.tool;


import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.openservices.ons.api.Consumer;
import com.aliyun.openservices.ons.api.ONSFactory;
import com.aliyun.openservices.ons.api.Producer;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.PropertyValueConst;
import com.aliyun.openservices.ons.api.order.OrderConsumer;
import com.aliyun.openservices.ons.api.order.OrderProducer;
import com.aliyun.openservices.ons.api.transaction.LocalTransactionChecker;
import com.aliyun.openservices.ons.api.transaction.TransactionProducer;

/**
 * aliyunmq client
 * @Title
 * @author lincm
 * @date 2018年5月8日 下午3:52:15
 */
@Component
public class AliyunMQClient {

    private Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    private AliyunMQConfig aliyunMQConfig;

    @Autowired
    private LocalTransactionChecker transactionCheckerImpl;

    /**
     * 获取aliyunmq的生产属性
     * @return
     */
    private Properties getProducerProperties(String producerId){
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ProducerId, producerId);
        properties.put(PropertyKeyConst.AccessKey, aliyunMQConfig.getAccessKey());
        properties.put(PropertyKeyConst.SecretKey, aliyunMQConfig.getSecretKey());
        properties.put(PropertyKeyConst.ONSAddr, aliyunMQConfig.getUrl());
        return properties;
    }

    /**
     * 实例阿里云MQ的producer对象
     * @return
     */
    public Producer getProducer(String producerId){
        Producer producer = ONSFactory.createProducer(getProducerProperties(producerId));
        return producer;
    }

    /**
     * 设置阿里云的分区顺序生产者属性
     * @return
     */
    private Properties getOrderProducerProperties(String producerId){
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ProducerId, producerId);
        properties.put(PropertyKeyConst.AccessKey, aliyunMQConfig.getAccessKey());
        properties.put(PropertyKeyConst.SecretKey, aliyunMQConfig.getSecretKey());
        properties.put(PropertyKeyConst.ONSAddr, aliyunMQConfig.getUrl());
        return properties;
    }
    /**
     * 实例阿里云MQ的orderProducer对象
     * @return
     */
    public OrderProducer getOrderProducer(String producerId){
        OrderProducer orderProducer = ONSFactory.createOrderProducer(getOrderProducerProperties(producerId));
        return orderProducer;
    }

    /**
     * 设置阿里云的分区顺序生产者属性
     * @return
     */
    private Properties getTransactionProducerProperties(String producerId){
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ProducerId, producerId);
        properties.put(PropertyKeyConst.AccessKey, aliyunMQConfig.getAccessKey());
        properties.put(PropertyKeyConst.SecretKey, aliyunMQConfig.getSecretKey());
        properties.put(PropertyKeyConst.ONSAddr, aliyunMQConfig.getUrl());
        return properties;
    }

    /**
     * 实例阿里云MQ的transactionProducer对象
     * @return
     */
    public TransactionProducer getTransactionProducer(String producerId){
        TransactionProducer transactionProducer = ONSFactory.createTransactionProducer(getTransactionProducerProperties(producerId),transactionCheckerImpl);
        return transactionProducer;
    }


    /**
     * 获取aliyunmq的消费属性
     * @param messageModel 订阅消息模式 PropertyValueConst.BROADCASTING-广播模式； 不传默认集群订阅模式
     * @param consumerId consumerId
     * @return
     */
    private Properties getConsumerProperties(String messageModel, String consumerId){
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ConsumerId, consumerId);
        properties.put(PropertyKeyConst.AccessKey, aliyunMQConfig.getAccessKey());
        properties.put(PropertyKeyConst.SecretKey, aliyunMQConfig.getSecretKey());
        properties.put(PropertyKeyConst.ONSAddr, aliyunMQConfig.getUrl());
        if(StringUtils.isNotEmpty(messageModel)){
            properties.put(PropertyKeyConst.MessageModel, messageModel);
        }
        properties.put(PropertyKeyConst.ConsumeThreadNums, aliyunMQConfig.getConsumerThreadNum());
        return properties;
    }

    /**
     * 实例阿里云MQ广播订阅模式的consumer对象
     * @return
     */
    public Consumer getBroadCastConsumer(String consumerId){
        Consumer consumer = ONSFactory.createConsumer(getConsumerProperties(PropertyValueConst.BROADCASTING, consumerId));
        if(!consumer.isStarted()){
            consumer.start();
            logger.info("aliyun mq BROADCASTING consumer is start");
        }
        return consumer;
    }

    /**
     * 实例阿里云MQ集群订阅模式的consumer对象
     * @return
     */
    public Consumer getClusteringConsumer(String consumerId){
        Consumer consumer = ONSFactory.createConsumer(getConsumerProperties(PropertyValueConst.CLUSTERING, consumerId));
        if(!consumer.isStarted()){
            consumer.start();
            logger.info("aliyun mq CLUSTERING consumer is start");
        }
        return consumer;
    }

    /**
     * 获取aliyunmq的顺序消费属性
     * @return
     */
    private Properties getOrderConsumerProperties(String consumerId){
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.ConsumerId, consumerId);
        properties.put(PropertyKeyConst.AccessKey, aliyunMQConfig.getAccessKey());
        properties.put(PropertyKeyConst.SecretKey, aliyunMQConfig.getSecretKey());
        properties.put(PropertyKeyConst.ONSAddr, aliyunMQConfig.getUrl());
        properties.put(PropertyKeyConst.SuspendTimeMillis, aliyunMQConfig.getSuspendTimeMillis());
        properties.put(PropertyKeyConst.MaxReconsumeTimes, aliyunMQConfig.getMaxReconsumeTimes());
        properties.put(PropertyKeyConst.ConsumeThreadNums, aliyunMQConfig.getConsumerThreadNum());
        return properties;
    }

    /**
     * 实例阿里云mq顺序消费对象
     * @return
     */
    public OrderConsumer getOrderConsumer(String consumerId){
        OrderConsumer consumer = ONSFactory.createOrderedConsumer(getOrderConsumerProperties(consumerId));
        if(!consumer.isStarted()){
            consumer.start();
            logger.info("aliyun mq seq consumer is start");
        }
        return consumer;
    }
}
