package com.zatgo.zup.common.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class QtumTransaction implements Serializable{

	private BigDecimal amount;
	
	private BigDecimal fee;

	private Integer confirmations;

	private String blockhash;

	private Integer blockindex;

	private Long blocktime;
	
	private String txid;
	
	private String[] walletconflicts;

	private Long time;

	private Long timereceived;

	private List<QTxDetails> details;

	private String hex;

	private String replaceable;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public Integer getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(Integer confirmations) {
		this.confirmations = confirmations;
	}

	public String getBlockhash() {
		return blockhash;
	}

	public void setBlockhash(String blockhash) {
		this.blockhash = blockhash;
	}

	public Integer getBlockindex() {
		return blockindex;
	}

	public void setBlockindex(Integer blockindex) {
		this.blockindex = blockindex;
	}

	public Long getBlocktime() {
		return blocktime;
	}

	public void setBlocktime(Long blocktime) {
		this.blocktime = blocktime;
	}

	public String getTxid() {
		return txid;
	}

	public void setTxid(String txid) {
		this.txid = txid;
	}

	public String[] getWalletconflicts() {
		return walletconflicts;
	}

	public void setWalletconflicts(String[] walletconflicts) {
		this.walletconflicts = walletconflicts;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Long getTimereceived() {
		return timereceived;
	}

	public void setTimereceived(Long timereceived) {
		this.timereceived = timereceived;
	}

	public List<QTxDetails> getDetails() {
		return details;
	}

	public void setDetails(List<QTxDetails> details) {
		this.details = details;
	}

	public String getHex() {
		return hex;
	}

	public void setHex(String hex) {
		this.hex = hex;
	}
}
