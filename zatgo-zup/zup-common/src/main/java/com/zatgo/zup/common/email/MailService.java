package com.zatgo.zup.common.email;

import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 邮件服务
 * @Title
 * @author lincm
 * @date 2018年4月4日 下午4:49:54
 */
public interface MailService {

	/**
	 * 发送邮件
	 * @param subject
	 * @param text
	 */
	Byte sendHtmlMail(String receiveEmail, String subject, String text);
	
	/**
	 * 发送系统邮件
	 * @param Subject
	 * @param text
	 */
	public void sendMail(String Subject,String text);

	/**
	 //     * 发送邮件
	 //     * @param templateCode
	 //     * @param receiver
	 //     * @param contentMap
	 //     */
    @Transactional
    void sendEmail(String templateCode, String receiver, Map<String, String> contentMap);

	/**
	 * 发送指定邮件
	 */
	public void sendMailByUser(String Subject,String text,String userEmail);

	/**
	 * 根据模板发送通知
	 * @param email
	 * @param data
	 * @param templateContext
	 */
	public void sendMailByTemplate(String subject,String email, Map<String, String> data, String templateContent);

}
