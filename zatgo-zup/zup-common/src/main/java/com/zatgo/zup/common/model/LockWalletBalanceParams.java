package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.zatgo.zup.common.enumtype.BusinessEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("冻结解冻钱包余额参数")
public class LockWalletBalanceParams  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(name = "用户id", required = true)
	private String userId;
	
	@ApiModelProperty(name = "用户名", required = true)
	private String userName;
	
	@ApiModelProperty(name = "订单id", required = true)
	private String orderId;
	
	@ApiModelProperty(value = "操作的冻结金额", required = true)
	private BigDecimal lockAmount;
	
	@ApiModelProperty(value = "币种类型", required = true)
	private String coinType;
	
	@ApiModelProperty(value = "更新余额类型", required = true)
	private BusinessEnum.LockBalanceOperateType lockBalanceType;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BusinessEnum.LockBalanceOperateType getLockBalanceType() {
		return lockBalanceType;
	}

	public void setLockBalanceType(BusinessEnum.LockBalanceOperateType lockBalanceType) {
		this.lockBalanceType = lockBalanceType;
	}

	public String getCoinType() {
		return coinType;
	}

	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getLockAmount() {
		return lockAmount;
	}

	public void setLockAmount(BigDecimal lockAmount) {
		this.lockAmount = lockAmount;
	}

}
