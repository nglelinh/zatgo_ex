package com.zatgo.zup.common.model;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by 46041 on 2019/4/22.
 */
public class SaveDataRequest {

    private String tableName;

    private String familyName;

    private String rowKey;

    private JSONObject data;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
