package com.zatgo.zup.common.model;

import com.ykb.mall.common.enumType.BusinessEnum;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/6/10.
 */
public class WechatRefundRecordRequest {


    private String orderNumber;
    private String refundNumber;
    private BusinessEnum.PayType payType;
    private BigDecimal money;
    private String thirdBusinessNumber;
    private String thirdRefundNumber;

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getRefundNumber() {
        return refundNumber;
    }

    public void setRefundNumber(String refundNumber) {
        this.refundNumber = refundNumber;
    }

    public BusinessEnum.PayType getPayType() {
        return payType;
    }

    public void setPayType(BusinessEnum.PayType payType) {
        this.payType = payType;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getThirdBusinessNumber() {
        return thirdBusinessNumber;
    }

    public void setThirdBusinessNumber(String thirdBusinessNumber) {
        this.thirdBusinessNumber = thirdBusinessNumber;
    }

    public String getThirdRefundNumber() {
        return thirdRefundNumber;
    }

    public void setThirdRefundNumber(String thirdRefundNumber) {
        this.thirdRefundNumber = thirdRefundNumber;
    }
}
