package com.zatgo.zup.common.model;

import java.util.List;

/**
 * Created by 46041 on 2019/2/14.
 */
public class AnswerRecordResult {

    private List<AnswerQuestionResultModel> list;

    private String date;


    public List<AnswerQuestionResultModel> getList() {
        return list;
    }

    public void setList(List<AnswerQuestionResultModel> list) {
        this.list = list;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
