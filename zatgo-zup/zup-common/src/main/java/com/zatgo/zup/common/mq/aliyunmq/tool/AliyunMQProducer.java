package com.zatgo.zup.common.mq.aliyunmq.tool;

import com.aliyun.openservices.ons.api.Producer;
import com.aliyun.openservices.ons.api.order.OrderProducer;
import com.aliyun.openservices.ons.api.transaction.TransactionProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 获取生产者类
 * @Title
 * @author lincm
 * @date 2018年5月8日 下午3:39:41
 */
@Component
public class AliyunMQProducer {

    private static Logger logger = LoggerFactory.getLogger(AliyunMQProducer.class);

    @Autowired
    private AliyunMQClient aliyunMQClient;

    private static AliyunMQProducer aliyunMQProducer;

    /**
     * 初始化变量的值
     */
    @PostConstruct
    public void init() {
        aliyunMQProducer = this;
        aliyunMQProducer.aliyunMQClient = this.aliyunMQClient;
    }

    private AliyunMQProducer(){};

    //普通发送消息对象
    private static volatile Producer producer;

    //顺序发送消息对象
    private static volatile OrderProducer orderProducer;

    //事物消息发送对象
    private static volatile TransactionProducer transactionProducer;

    //实例化普通发送消息对象
    public static Producer getAliyunProducer(String producerId) {
        // 对象实例化时与否判断（不使用同步代码块，instance不等于null时，直接返回对象，提高运行效率）
        if (producer == null) {
            synchronized (AliyunMQProducer.class) {
                //未初始化，则初始instance变量
                if (producer == null) {
                    producer = aliyunMQProducer.aliyunMQClient.getProducer(producerId);
                    //启动
                    producer.start();
                    logger.info("aliyun mq producer is start");
                }
            }
        }else {
            //已有对象判断是否启动，未启动先启动
            if(!producer.isStarted()){
                logger.info("aliyun mq producer is not start");
                producer.start();
            }
        }
        return producer;
    }

    //实例化普通发送消息对象
    public static OrderProducer getAliyunOrderProducer(String producerId) {
        // 对象实例化时与否判断（不使用同步代码块，instance不等于null时，直接返回对象，提高运行效率）
        if (orderProducer == null) {
            synchronized (AliyunMQProducer.class) {
                //未初始化，则初始instance变量
                if (orderProducer == null) {
                    orderProducer = aliyunMQProducer.aliyunMQClient.getOrderProducer(producerId);
                    //启动
                    orderProducer.start();
                    logger.info("aliyun mq subarea seq producer is start");
                }
            }
        }else {
            //已有对象判断是否启动，未启动先启动
            if(!orderProducer.isStarted()){
                logger.info("aliyun mq subarea seq producer is not start");
                orderProducer.start();
            }
        }
        return orderProducer;
    }

    //实例化事物送消息对象
    public static TransactionProducer getAliyunTransactionProducer(String producerId) {
        // 对象实例化时与否判断（不使用同步代码块，instance不等于null时，直接返回对象，提高运行效率）
        if (transactionProducer == null) {
            synchronized (AliyunMQProducer.class) {
                //未初始化，则初始instance变量
                if (transactionProducer == null) {
                    transactionProducer = aliyunMQProducer.aliyunMQClient.getTransactionProducer(producerId);
                    //启动
                    transactionProducer.start();
                    logger.info("aliyun mq subarea transaction is start");
                }
            }
        }else {
            //已有对象判断是否启动，未启动先启动
            if(!transactionProducer.isStarted()){
                logger.info("aliyun mq subarea transaction is not start");
                transactionProducer.start();
            }
        }
        return transactionProducer;
    }
}
