package com.zatgo.zup.common.model;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("交易更新账户金额入参")
public class ExchangeUpdateBalanceParams implements Serializable {

	private static final long serialVersionUID = 5679661688973652335L;
	
	@ApiModelProperty(name = "买用户id", required = false)
	private String buyUserId;
	
	@ApiModelProperty(name = "买用户名", required = false)
	private String buyUserName;
	
	@ApiModelProperty(name = "卖用户id", required = false)
	private String sellUserId;
	
	@ApiModelProperty(name = "卖用户名", required = false)
	private String sellUserName;
	
	@ApiModelProperty(name = "买订单id", required = true)
	private String buyOrderId;
	
	@ApiModelProperty(name = "卖订单id", required = true)
	private String sellOrderId;
	
	@ApiModelProperty(value = "基准币种类型", required = true)
	private String baseCoinType;
	
	@ApiModelProperty(value = "计价币种类型", required = true)
	private String quoteCoinType;
	
	@ApiModelProperty(value = "买方操作的冻结金额", required = true)
	private BigDecimal buyLockAmount;
	
	@ApiModelProperty(value = "卖方操作的冻结金额", required = true)
	private BigDecimal sellLockAmount;
	
	@ApiModelProperty(value = "买方手续费", required = true)
	private BigDecimal buyfee;
	
	@ApiModelProperty(value = "卖方手续费", required = true)
	private BigDecimal sellfee;
	
	@ApiModelProperty(value = "业务流水号", required = true)
	private String businessNumber;

	public BigDecimal getBuyfee() {
		return buyfee;
	}

	public void setBuyfee(BigDecimal buyfee) {
		this.buyfee = buyfee;
	}

	public BigDecimal getSellfee() {
		return sellfee;
	}

	public void setSellfee(BigDecimal sellfee) {
		this.sellfee = sellfee;
	}

	public String getSellUserId() {
		return sellUserId;
	}

	public void setSellUserId(String sellUserId) {
		this.sellUserId = sellUserId;
	}

	public String getBuyUserName() {
		return buyUserName;
	}

	public void setBuyUserName(String buyUserName) {
		this.buyUserName = buyUserName;
	}

	public String getSellUserName() {
		return sellUserName;
	}

	public void setSellUserName(String sellUserName) {
		this.sellUserName = sellUserName;
	}

	public String getBuyUserId() {
		return buyUserId;
	}

	public void setBuyUserId(String buyUserId) {
		this.buyUserId = buyUserId;
	}


	public String getBuyOrderId() {
		return buyOrderId;
	}

	public void setBuyOrderId(String buyOrderId) {
		this.buyOrderId = buyOrderId;
	}

	public String getSellOrderId() {
		return sellOrderId;
	}

	public void setSellOrderId(String sellOrderId) {
		this.sellOrderId = sellOrderId;
	}


	public String getBaseCoinType() {
		return baseCoinType;
	}

	public void setBaseCoinType(String baseCoinType) {
		this.baseCoinType = baseCoinType;
	}

	public String getQuoteCoinType() {
		return quoteCoinType;
	}

	public void setQuoteCoinType(String quoteCoinType) {
		this.quoteCoinType = quoteCoinType;
	}

	public BigDecimal getBuyLockAmount() {
		return buyLockAmount;
	}

	public void setBuyLockAmount(BigDecimal buyLockAmount) {
		this.buyLockAmount = buyLockAmount;
	}

	public BigDecimal getSellLockAmount() {
		return sellLockAmount;
	}

	public void setSellLockAmount(BigDecimal sellLockAmount) {
		this.sellLockAmount = sellLockAmount;
	}

	public String getBusinessNumber() {
		return businessNumber;
	}

	public void setBusinessNumber(String businessNumber) {
		this.businessNumber = businessNumber;
	}
	

}
