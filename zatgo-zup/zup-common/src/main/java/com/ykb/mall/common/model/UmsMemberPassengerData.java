package com.ykb.mall.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

@ApiModel("出行人信息")
public class UmsMemberPassengerData {
	
	@ApiModelProperty("出行人编号")
	private Long id;
	
	@ApiModelProperty("出行人姓名")
	private String name;
	
	@ApiModelProperty("出行人性别")
	private Byte sex;
	
	@ApiModelProperty("出行人生日")
	private Date birthday;
	
	@ApiModelProperty("联系电话")
	private String phone;
	
	@ApiModelProperty("出行人证件列表")
	private List<UmsMemberPassengerIdCardData> idCardList;
	
	private Byte isAdult;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Byte getSex() {
		return sex;
	}

	public void setSex(Byte sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<UmsMemberPassengerIdCardData> getIdCardList() {
		return idCardList;
	}

	public void setIdCardList(List<UmsMemberPassengerIdCardData> idCardList) {
		this.idCardList = idCardList;
	}

	public Byte getIsAdult() {
		return isAdult;
	}

	public void setIsAdult(Byte isAdult) {
		this.isAdult = isAdult;
	}

}
