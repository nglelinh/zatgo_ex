package com.ykb.mall.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("乘客信息")
public class PassengerData {
	
	@ApiModelProperty("乘客姓名")
	private String name;
	
	@ApiModelProperty("联系电话")
	private String phone;
	
	@ApiModelProperty("乘客证件类型")
	private String idcardType;
	
	@ApiModelProperty("乘客证件号")
	private String idcardCode;
	
	@ApiModelProperty("是否成人：1-是；2-否")
	private Byte isAdult;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdcardType() {
		return idcardType;
	}

	public void setIdcardType(String idcardType) {
		this.idcardType = idcardType;
	}

	public String getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(String idcardCode) {
		this.idcardCode = idcardCode;
	}

	public Byte getIsAdult() {
		return isAdult;
	}

	public void setIsAdult(Byte isAdult) {
		this.isAdult = isAdult;
	}

}
