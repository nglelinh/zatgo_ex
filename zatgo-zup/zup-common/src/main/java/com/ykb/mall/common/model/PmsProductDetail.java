package com.ykb.mall.common.model;

import com.ykb.mall.common.model.PmsProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/16 14:27
 */
@ApiModel("产品详情")
public class PmsProductDetail extends PmsProduct {

    @ApiModelProperty("产品属性详情")
    private List<ProductAttributeValue> attrValueList;

    public List<ProductAttributeValue> getAttrValueList() {
        return attrValueList;
    }

    public void setAttrValueList(List<ProductAttributeValue> attrValueList) {
        this.attrValueList = attrValueList;
    }
}