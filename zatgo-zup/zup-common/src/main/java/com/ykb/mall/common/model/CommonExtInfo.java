package com.ykb.mall.common.model;

/**
 * Created by 46041 on 2019/7/29.
 */
public class CommonExtInfo {

    private String skuAttr;

    private String skuId;

    public String getSkuAttr() {
        return skuAttr;
    }

    public void setSkuAttr(String skuAttr) {
        this.skuAttr = skuAttr;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }
}
