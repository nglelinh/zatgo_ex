package com.ykb.mall.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/27 16:04
 */
@ApiModel("个人订单详情")
public class PortalOrderDetailInfo extends PortalOrderBaseInfo {
    @ApiModelProperty("订单售价")
    private BigDecimal totalPrice;

    @ApiModelProperty("期望出行时间，json格式")
    private String desirableTimeStr;

    @ApiModelProperty("期望出行时间")
    private List<String> desirableDates;

    @ApiModelProperty("成人人数")
    private Integer adultNum;

    @ApiModelProperty("儿童人数")
    private Integer childNum;

    @ApiModelProperty("联系人名称")
    private String contactName;

    @ApiModelProperty("联系人手机号")
    private String contactPhone;

    @ApiModelProperty("成人价")
    private BigDecimal productPrice;

    @ApiModelProperty("儿童价")
    private BigDecimal productChildPrice;
    
    @ApiModelProperty("类型")
    private String productBrand;
    
    @ApiModelProperty("价格")
    private String prices;
}