package com.ykb.mall.common.model;

import com.alibaba.fastjson.annotation.JSONField;

public class WechatSendMessageData extends WechatErrData {
	
	@JSONField(name = "msgid")
	private String msgId;

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
}
