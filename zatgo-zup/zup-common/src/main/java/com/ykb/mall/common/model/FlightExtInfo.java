package com.ykb.mall.common.model;

import java.util.List;

/**
 * Created by 46041 on 2019/6/17.
 */
public class FlightExtInfo {

    private String fromAddr;

    private String toAddr;

    private String companyName;

    private String flight;

    private String startDate;

    private String startAirport;

    private String endAirport;

    private String shippingSpace;

    private Byte tripIsTransfer;

    private String returnFromAddr;

    private String returnToAddr;

    private String returnCompanyName;

    private String returnFlight;

    private String returnStartDate;

    private String returnStartAirport;

    private String returnEndAirport;

    private Byte returnTripIsTransfer;

    private List<Long> prices;

    public String getFromAddr() {
        return fromAddr;
    }

    public void setFromAddr(String fromAddr) {
        this.fromAddr = fromAddr;
    }

    public String getToAddr() {
        return toAddr;
    }

    public void setToAddr(String toAddr) {
        this.toAddr = toAddr;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getReturnFromAddr() {
        return returnFromAddr;
    }

    public void setReturnFromAddr(String returnFromAddr) {
        this.returnFromAddr = returnFromAddr;
    }

    public String getReturnToAddr() {
        return returnToAddr;
    }

    public void setReturnToAddr(String returnToAddr) {
        this.returnToAddr = returnToAddr;
    }

    public String getReturnCompanyName() {
        return returnCompanyName;
    }

    public void setReturnCompanyName(String returnCompanyName) {
        this.returnCompanyName = returnCompanyName;
    }

    public String getReturnFlight() {
        return returnFlight;
    }

    public void setReturnFlight(String returnFlight) {
        this.returnFlight = returnFlight;
    }

    public String getReturnStartDate() {
        return returnStartDate;
    }

    public void setReturnStartDate(String returnStartDate) {
        this.returnStartDate = returnStartDate;
    }

    public String getShippingSpace() {
        return shippingSpace;
    }

    public void setShippingSpace(String shippingSpace) {
        this.shippingSpace = shippingSpace;
    }

    public Byte getTripIsTransfer() {
        return tripIsTransfer;
    }

    public void setTripIsTransfer(Byte tripIsTransfer) {
        this.tripIsTransfer = tripIsTransfer;
    }

    public Byte getReturnTripIsTransfer() {
        return returnTripIsTransfer;
    }

    public void setReturnTripIsTransfer(Byte returnTripIsTransfer) {
        this.returnTripIsTransfer = returnTripIsTransfer;
    }

    public String getStartAirport() {
        return startAirport;
    }

    public void setStartAirport(String startAirport) {
        this.startAirport = startAirport;
    }

    public String getEndAirport() {
        return endAirport;
    }

    public void setEndAirport(String endAirport) {
        this.endAirport = endAirport;
    }

    public String getReturnStartAirport() {
        return returnStartAirport;
    }

    public void setReturnStartAirport(String returnStartAirport) {
        this.returnStartAirport = returnStartAirport;
    }

    public String getReturnEndAirport() {
        return returnEndAirport;
    }

    public void setReturnEndAirport(String returnEndAirport) {
        this.returnEndAirport = returnEndAirport;
    }

    public List<Long> getPrices() {
        return prices;
    }

    public void setPrices(List<Long> prices) {
        this.prices = prices;
    }
}
