package com.ykb.mall.common.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ThirdPayRecord implements Serializable {
    /**
     * 记录编号
     *
     * @mbggenerated
     */
    private Long recordId;

    /**
     * 支付方式，1->支付宝；2->微信
     *
     * @mbggenerated
     */
    private Byte payType;

    /**
     * 资金流向，1-支付；2-退款；
     *
     * @mbggenerated
     */
    private Byte moneyFlow;

    /**
     * 金额
     *
     * @mbggenerated
     */
    private BigDecimal money;

    /**
     * 支付时间
     *
     * @mbggenerated
     */
    private Date recordTime;

    /**
     * 第三方业务流水号，记录第三方支付的流水号
     *
     * @mbggenerated
     */
    private String thirdBusinessNumber;

    /**
     * 退款单号
     *
     * @mbggenerated
     */
    private String refundNumber;

    /**
     * 第三方退款单号
     *
     * @mbggenerated
     */
    private String thirdRefundNumber;

    /**
     * 业务单号
     *
     * @mbggenerated
     */
    private String orderNumber;

    private static final long serialVersionUID = 1L;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Byte getPayType() {
        return payType;
    }

    public void setPayType(Byte payType) {
        this.payType = payType;
    }

    public Byte getMoneyFlow() {
        return moneyFlow;
    }

    public void setMoneyFlow(Byte moneyFlow) {
        this.moneyFlow = moneyFlow;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public String getThirdBusinessNumber() {
        return thirdBusinessNumber;
    }

    public void setThirdBusinessNumber(String thirdBusinessNumber) {
        this.thirdBusinessNumber = thirdBusinessNumber;
    }

    public String getRefundNumber() {
        return refundNumber;
    }

    public void setRefundNumber(String refundNumber) {
        this.refundNumber = refundNumber;
    }

    public String getThirdRefundNumber() {
        return thirdRefundNumber;
    }

    public void setThirdRefundNumber(String thirdRefundNumber) {
        this.thirdRefundNumber = thirdRefundNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", recordId=").append(recordId);
        sb.append(", payType=").append(payType);
        sb.append(", moneyFlow=").append(moneyFlow);
        sb.append(", money=").append(money);
        sb.append(", recordTime=").append(recordTime);
        sb.append(", thirdBusinessNumber=").append(thirdBusinessNumber);
        sb.append(", refundNumber=").append(refundNumber);
        sb.append(", thirdRefundNumber=").append(thirdRefundNumber);
        sb.append(", orderNumber=").append(orderNumber);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}