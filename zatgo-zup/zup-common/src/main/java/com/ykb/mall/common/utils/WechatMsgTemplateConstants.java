package com.ykb.mall.common.utils;

/**
 * @Author wangyucong
 * @Date 2019/4/8 10:20
 */
public class WechatMsgTemplateConstants {
	
	public static final int WECHAT_MSG_SUPPORT_ID = 1;
	public static final int WECHAT_MSG_SUPPORT_SUCCESS_ID = 2;

}