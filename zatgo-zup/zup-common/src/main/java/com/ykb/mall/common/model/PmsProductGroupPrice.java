package com.ykb.mall.common.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PmsProductGroupPrice implements Serializable {
    /**
     * 班期价格编号
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 班期编号
     *
     * @mbggenerated
     */
    private Long groupId;

    /**
     * 价格类型（1 - 成人，2 - 儿童）
     *
     * @mbggenerated
     */
    private Byte priceType;

    /**
     * 价格名称
     *
     * @mbggenerated
     */
    private String priceName;

    /**
     * 供应价
     *
     * @mbggenerated
     */
    private BigDecimal supplyPrice;

    /**
     * 补房差
     *
     * @mbggenerated
     */
    private BigDecimal houseExtraPrice;

    /**
     * 人数限制
     *
     * @mbggenerated
     */
    private Integer maxPeople;

    /**
     * 是否优先展示价格（0 - 否，1 - 是）
     *
     * @mbggenerated
     */
    private Byte firstShow;

    /**
     * 展示状态（0 - 不展示，1 - 展示）
     *
     * @mbggenerated
     */
    private Byte showStatus;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Byte getPriceType() {
        return priceType;
    }

    public void setPriceType(Byte priceType) {
        this.priceType = priceType;
    }

    public String getPriceName() {
        return priceName;
    }

    public void setPriceName(String priceName) {
        this.priceName = priceName;
    }

    public BigDecimal getSupplyPrice() {
        return supplyPrice;
    }

    public void setSupplyPrice(BigDecimal supplyPrice) {
        this.supplyPrice = supplyPrice;
    }

    public BigDecimal getHouseExtraPrice() {
        return houseExtraPrice;
    }

    public void setHouseExtraPrice(BigDecimal houseExtraPrice) {
        this.houseExtraPrice = houseExtraPrice;
    }

    public Integer getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(Integer maxPeople) {
        this.maxPeople = maxPeople;
    }

    public Byte getFirstShow() {
        return firstShow;
    }

    public void setFirstShow(Byte firstShow) {
        this.firstShow = firstShow;
    }

    public Byte getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(Byte showStatus) {
        this.showStatus = showStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", groupId=").append(groupId);
        sb.append(", priceType=").append(priceType);
        sb.append(", priceName=").append(priceName);
        sb.append(", supplyPrice=").append(supplyPrice);
        sb.append(", houseExtraPrice=").append(houseExtraPrice);
        sb.append(", maxPeople=").append(maxPeople);
        sb.append(", firstShow=").append(firstShow);
        sb.append(", showStatus=").append(showStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}