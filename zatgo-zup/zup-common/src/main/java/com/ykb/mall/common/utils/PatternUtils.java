package com.ykb.mall.common.utils;

import org.springframework.util.StringUtils;

import java.util.regex.Pattern;

/**
 * 正则工具类
 * 
 * @author tiancongcong
 *
 */
public class PatternUtils {

	/**
	 * 手机号正则表达式 不对手机号做强校验，只需满足首位是1，后10位是数字即认为合法
	 */
	private static final String MOBILE_REGEX = "^[1][0-9]{10}$";

	/**
	 * 手机号校验
	 * 
	 * @param phone
	 * @return
	 */
	public static boolean isMobile(String phone) {

		if (!StringUtils.isEmpty(phone) && Pattern.matches(MOBILE_REGEX, phone)) {
			return true;
		}

		return false;
	}
	
	public static void main(String[] args) {
		System.out.println(isMobile("1566992778"));
		System.out.println(isMobile("15669927785"));
		System.out.println(isMobile("156699277855"));
		System.out.println(isMobile("1566992778p"));
	}

}
