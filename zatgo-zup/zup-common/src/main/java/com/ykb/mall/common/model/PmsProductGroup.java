package com.ykb.mall.common.model;

import java.io.Serializable;
import java.util.Date;

public class PmsProductGroup implements Serializable {
    /**
     * 班期编号
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 班期编码
     *
     * @mbggenerated
     */
    private String groupNumber;

    /**
     * 产品编号
     *
     * @mbggenerated
     */
    private Long productId;

    /**
     * 出行日期编号
     *
     * @mbggenerated
     */
    private Long dateId;

    /**
     * 主名称
     *
     * @mbggenerated
     */
    private String name;

    /**
     * 子名称
     *
     * @mbggenerated
     */
    private String secondName;

    /**
     * 库存是否限量（0 - 否，1 - 是）
     *
     * @mbggenerated
     */
    private Byte stockLimit;

    /**
     * 库存
     *
     * @mbggenerated
     */
    private Integer stock;

    /**
     * 最小成团人数
     *
     * @mbggenerated
     */
    private Integer minPeople;

    /**
     * 报名提前截止天数（0为截止至出团当日）
     *
     * @mbggenerated
     */
    private Integer enrollEndDay;

    /**
     * 报名截止时间
     *
     * @mbggenerated
     */
    private Date enrollEndTime;

    /**
     * 周末是否计算在内（0 - 否，1 - 是）
     *
     * @mbggenerated
     */
    private Byte enrollInWeekend;

    /**
     * 出团集合时间
     *
     * @mbggenerated
     */
    private Date collectionTime;

    /**
     * 行程结束时间
     *
     * @mbggenerated
     */
    private Date tripEndTime;

    /**
     * 接送紧急联络电话
     *
     * @mbggenerated
     */
    private String transportUrgentTel;

    /**
     * 接团地点
     *
     * @mbggenerated
     */
    private String meetPlace;

    /**
     * 接团标语
     *
     * @mbggenerated
     */
    private String meetSign;

    /**
     * 接地导游姓名
     *
     * @mbggenerated
     */
    private String localGuideName;

    /**
     * 接地导游手机号
     *
     * @mbggenerated
     */
    private String localGuidePhone;

    /**
     * 班期状态（0 -待发布，1 - 已发布，2 - 已下架）
     *
     * @mbggenerated
     */
    private Byte status;

    /**
     * 是否为删除状态（0 - 否，1 - 是）
     *
     * @mbggenerated
     */
    private Byte deleteStatus;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 更新时间
     *
     * @mbggenerated
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getDateId() {
        return dateId;
    }

    public void setDateId(Long dateId) {
        this.dateId = dateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Byte getStockLimit() {
        return stockLimit;
    }

    public void setStockLimit(Byte stockLimit) {
        this.stockLimit = stockLimit;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getMinPeople() {
        return minPeople;
    }

    public void setMinPeople(Integer minPeople) {
        this.minPeople = minPeople;
    }

    public Integer getEnrollEndDay() {
        return enrollEndDay;
    }

    public void setEnrollEndDay(Integer enrollEndDay) {
        this.enrollEndDay = enrollEndDay;
    }

    public Date getEnrollEndTime() {
        return enrollEndTime;
    }

    public void setEnrollEndTime(Date enrollEndTime) {
        this.enrollEndTime = enrollEndTime;
    }

    public Byte getEnrollInWeekend() {
        return enrollInWeekend;
    }

    public void setEnrollInWeekend(Byte enrollInWeekend) {
        this.enrollInWeekend = enrollInWeekend;
    }

    public Date getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(Date collectionTime) {
        this.collectionTime = collectionTime;
    }

    public Date getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(Date tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String getTransportUrgentTel() {
        return transportUrgentTel;
    }

    public void setTransportUrgentTel(String transportUrgentTel) {
        this.transportUrgentTel = transportUrgentTel;
    }

    public String getMeetPlace() {
        return meetPlace;
    }

    public void setMeetPlace(String meetPlace) {
        this.meetPlace = meetPlace;
    }

    public String getMeetSign() {
        return meetSign;
    }

    public void setMeetSign(String meetSign) {
        this.meetSign = meetSign;
    }

    public String getLocalGuideName() {
        return localGuideName;
    }

    public void setLocalGuideName(String localGuideName) {
        this.localGuideName = localGuideName;
    }

    public String getLocalGuidePhone() {
        return localGuidePhone;
    }

    public void setLocalGuidePhone(String localGuidePhone) {
        this.localGuidePhone = localGuidePhone;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Byte deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", groupNumber=").append(groupNumber);
        sb.append(", productId=").append(productId);
        sb.append(", dateId=").append(dateId);
        sb.append(", name=").append(name);
        sb.append(", secondName=").append(secondName);
        sb.append(", stockLimit=").append(stockLimit);
        sb.append(", stock=").append(stock);
        sb.append(", minPeople=").append(minPeople);
        sb.append(", enrollEndDay=").append(enrollEndDay);
        sb.append(", enrollEndTime=").append(enrollEndTime);
        sb.append(", enrollInWeekend=").append(enrollInWeekend);
        sb.append(", collectionTime=").append(collectionTime);
        sb.append(", tripEndTime=").append(tripEndTime);
        sb.append(", transportUrgentTel=").append(transportUrgentTel);
        sb.append(", meetPlace=").append(meetPlace);
        sb.append(", meetSign=").append(meetSign);
        sb.append(", localGuideName=").append(localGuideName);
        sb.append(", localGuidePhone=").append(localGuidePhone);
        sb.append(", status=").append(status);
        sb.append(", deleteStatus=").append(deleteStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}