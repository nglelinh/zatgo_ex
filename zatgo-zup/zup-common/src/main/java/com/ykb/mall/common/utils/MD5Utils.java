package com.ykb.mall.common.utils;

import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessException;
import org.springframework.util.DigestUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5工具类
 *
 * @author tiancongcong
 */
public class MD5Utils {

    /**
     * JDK MD5加密
     *
     * @param str
     * @return
     */
    public static String jdkMD5Encode(String str) {

        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(str.getBytes());
            byte s[] = m.digest();
            String result = "";
            for (int i = 0; i < s.length; i++) {
                result += Integer.toHexString((0x000000FF & s[i]) | 0xFFFFFF00).substring(6);
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
        }
    }

    /**
     * JDK MD5加密含salt
     *
     * @param str
     * @param salt
     * @return
     */
    public static String jdkMD5Encode(String str, String salt) {
        return jdkMD5Encode(new StringBuffer().append(str).append(salt).toString());
    }

    /**
     * spring MD5加密
     *
     * @param str
     * @return
     */
    public static String springMD5Encode(String str) {
        return DigestUtils.md5DigestAsHex(str.getBytes());
    }

    /**
     * spring MD5加密含salt
     *
     * @param str
     * @return
     */
    public static String springMD5Encode(String str, String salt) {
        return springMD5Encode(new StringBuffer().append(str).append(salt).toString());
    }

    public static void main(String[] args) {
        String i = jdkMD5Encode("test");
        String p = springMD5Encode("test");
        System.out.println(i);
        System.out.println(p);
        System.out.println(i.equals(p));

        i = jdkMD5Encode("test", "123");
        p = springMD5Encode("test", "123");
        System.out.println(i);
        System.out.println(p);
        System.out.println(i.equals(p));
    }

}
