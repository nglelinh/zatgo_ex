package com.ykb.mall.common.model;

import java.io.Serializable;
import java.util.Date;

public class PmsProductFlight implements Serializable {
    private Long id;

    /**
     * 产品编号
     *
     * @mbggenerated
     */
    private Long productId;

    /**
     * 去程航班编号
     *
     * @mbggenerated
     */
    private String tripFlightNumber;

    /**
     * 去程航空公司
     *
     * @mbggenerated
     */
    private String tripAirlineCompany;

    /**
     * 返程航班编号
     *
     * @mbggenerated
     */
    private String returnFlightNumber;

    /**
     * 返程航空公司
     *
     * @mbggenerated
     */
    private String returnAirlineCompany;

    /**
     * 库存是否限量（0 - 否，1 - 是）
     *
     * @mbggenerated
     */
    private Byte stockLimit;

    /**
     * 库存
     *
     * @mbggenerated
     */
    private Integer stock;

    private Date createTime;

    /**
     * 更新时间
     *
     * @mbggenerated
     */
    private Date updateTime;

    /**
     * 去程出发城市名称
     *
     * @mbggenerated
     */
    private String tripDepartureCityName;

    /**
     * 去程出发机场
     *
     * @mbggenerated
     */
    private String tripDepartureAirportName;

    /**
     * 去程出发航站楼
     *
     * @mbggenerated
     */
    private String tripDepartureTerminal;

    /**
     * 去程到达城市名称
     *
     * @mbggenerated
     */
    private String tripDestinationCityName;

    /**
     * 去程到达机场名称
     *
     * @mbggenerated
     */
    private String tripDestinationAirportName;

    /**
     * 去程到达航站楼
     *
     * @mbggenerated
     */
    private String tripDestinationTerminal;

    /**
     * 去程机型
     *
     * @mbggenerated
     */
    private String tripPlaneType;

    /**
     * 去程出发时间
     *
     * @mbggenerated
     */
    private Date tripDepartureTime;

    /**
     * 去程到达时间
     *
     * @mbggenerated
     */
    private Date tripDestinationTime;

    /**
     * 去程订单航程类型 1-单程；2-往返；3-联程
     *
     * @mbggenerated
     */
    private Byte tripFlightType;

    /**
     * 去程是否中转：1-直飞；2-中转
     *
     * @mbggenerated
     */
    private Byte tripIsTransfer;

    /**
     * 去程历时
     *
     * @mbggenerated
     */
    private String tripCostTime;

    /**
     * 返程出发城市名称
     *
     * @mbggenerated
     */
    private String returnDepartureCityName;

    /**
     * 返程出发机场
     *
     * @mbggenerated
     */
    private String returnDepartureAirportName;

    /**
     * 返程出发航站楼
     *
     * @mbggenerated
     */
    private String returnDepartureTerminal;

    /**
     * 返程到达城市名称
     *
     * @mbggenerated
     */
    private String returnDestinationCityName;

    /**
     * 返程到达机场名称
     *
     * @mbggenerated
     */
    private String returnDestinationAirportName;

    /**
     * 返程到达航站楼
     *
     * @mbggenerated
     */
    private String returnDestinationTerminal;

    /**
     * 返程机型
     *
     * @mbggenerated
     */
    private String returnPlaneType;

    /**
     * 返程出发时间
     *
     * @mbggenerated
     */
    private Date returnDepartureTime;

    /**
     * 返程到达时间
     *
     * @mbggenerated
     */
    private Date returnDestinationTime;

    /**
     * 返程订单航程类型 1-单程；2-往返；3-联程
     *
     * @mbggenerated
     */
    private Byte returnFlightType;

    /**
     * 返程是否中转：1-直飞；2-中转
     *
     * @mbggenerated
     */
    private Byte returnIsTransfer;

    /**
     * 返程历时
     *
     * @mbggenerated
     */
    private String returnCostTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getTripFlightNumber() {
        return tripFlightNumber;
    }

    public void setTripFlightNumber(String tripFlightNumber) {
        this.tripFlightNumber = tripFlightNumber;
    }

    public String getTripAirlineCompany() {
        return tripAirlineCompany;
    }

    public void setTripAirlineCompany(String tripAirlineCompany) {
        this.tripAirlineCompany = tripAirlineCompany;
    }

    public String getReturnFlightNumber() {
        return returnFlightNumber;
    }

    public void setReturnFlightNumber(String returnFlightNumber) {
        this.returnFlightNumber = returnFlightNumber;
    }

    public String getReturnAirlineCompany() {
        return returnAirlineCompany;
    }

    public void setReturnAirlineCompany(String returnAirlineCompany) {
        this.returnAirlineCompany = returnAirlineCompany;
    }

    public Byte getStockLimit() {
        return stockLimit;
    }

    public void setStockLimit(Byte stockLimit) {
        this.stockLimit = stockLimit;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getTripDepartureCityName() {
        return tripDepartureCityName;
    }

    public void setTripDepartureCityName(String tripDepartureCityName) {
        this.tripDepartureCityName = tripDepartureCityName;
    }

    public String getTripDepartureAirportName() {
        return tripDepartureAirportName;
    }

    public void setTripDepartureAirportName(String tripDepartureAirportName) {
        this.tripDepartureAirportName = tripDepartureAirportName;
    }

    public String getTripDepartureTerminal() {
        return tripDepartureTerminal;
    }

    public void setTripDepartureTerminal(String tripDepartureTerminal) {
        this.tripDepartureTerminal = tripDepartureTerminal;
    }

    public String getTripDestinationCityName() {
        return tripDestinationCityName;
    }

    public void setTripDestinationCityName(String tripDestinationCityName) {
        this.tripDestinationCityName = tripDestinationCityName;
    }

    public String getTripDestinationAirportName() {
        return tripDestinationAirportName;
    }

    public void setTripDestinationAirportName(String tripDestinationAirportName) {
        this.tripDestinationAirportName = tripDestinationAirportName;
    }

    public String getTripDestinationTerminal() {
        return tripDestinationTerminal;
    }

    public void setTripDestinationTerminal(String tripDestinationTerminal) {
        this.tripDestinationTerminal = tripDestinationTerminal;
    }

    public String getTripPlaneType() {
        return tripPlaneType;
    }

    public void setTripPlaneType(String tripPlaneType) {
        this.tripPlaneType = tripPlaneType;
    }

    public Date getTripDepartureTime() {
        return tripDepartureTime;
    }

    public void setTripDepartureTime(Date tripDepartureTime) {
        this.tripDepartureTime = tripDepartureTime;
    }

    public Date getTripDestinationTime() {
        return tripDestinationTime;
    }

    public void setTripDestinationTime(Date tripDestinationTime) {
        this.tripDestinationTime = tripDestinationTime;
    }

    public Byte getTripFlightType() {
        return tripFlightType;
    }

    public void setTripFlightType(Byte tripFlightType) {
        this.tripFlightType = tripFlightType;
    }

    public Byte getTripIsTransfer() {
        return tripIsTransfer;
    }

    public void setTripIsTransfer(Byte tripIsTransfer) {
        this.tripIsTransfer = tripIsTransfer;
    }

    public String getTripCostTime() {
        return tripCostTime;
    }

    public void setTripCostTime(String tripCostTime) {
        this.tripCostTime = tripCostTime;
    }

    public String getReturnDepartureCityName() {
        return returnDepartureCityName;
    }

    public void setReturnDepartureCityName(String returnDepartureCityName) {
        this.returnDepartureCityName = returnDepartureCityName;
    }

    public String getReturnDepartureAirportName() {
        return returnDepartureAirportName;
    }

    public void setReturnDepartureAirportName(String returnDepartureAirportName) {
        this.returnDepartureAirportName = returnDepartureAirportName;
    }

    public String getReturnDepartureTerminal() {
        return returnDepartureTerminal;
    }

    public void setReturnDepartureTerminal(String returnDepartureTerminal) {
        this.returnDepartureTerminal = returnDepartureTerminal;
    }

    public String getReturnDestinationCityName() {
        return returnDestinationCityName;
    }

    public void setReturnDestinationCityName(String returnDestinationCityName) {
        this.returnDestinationCityName = returnDestinationCityName;
    }

    public String getReturnDestinationAirportName() {
        return returnDestinationAirportName;
    }

    public void setReturnDestinationAirportName(String returnDestinationAirportName) {
        this.returnDestinationAirportName = returnDestinationAirportName;
    }

    public String getReturnDestinationTerminal() {
        return returnDestinationTerminal;
    }

    public void setReturnDestinationTerminal(String returnDestinationTerminal) {
        this.returnDestinationTerminal = returnDestinationTerminal;
    }

    public String getReturnPlaneType() {
        return returnPlaneType;
    }

    public void setReturnPlaneType(String returnPlaneType) {
        this.returnPlaneType = returnPlaneType;
    }

    public Date getReturnDepartureTime() {
        return returnDepartureTime;
    }

    public void setReturnDepartureTime(Date returnDepartureTime) {
        this.returnDepartureTime = returnDepartureTime;
    }

    public Date getReturnDestinationTime() {
        return returnDestinationTime;
    }

    public void setReturnDestinationTime(Date returnDestinationTime) {
        this.returnDestinationTime = returnDestinationTime;
    }

    public Byte getReturnFlightType() {
        return returnFlightType;
    }

    public void setReturnFlightType(Byte returnFlightType) {
        this.returnFlightType = returnFlightType;
    }

    public Byte getReturnIsTransfer() {
        return returnIsTransfer;
    }

    public void setReturnIsTransfer(Byte returnIsTransfer) {
        this.returnIsTransfer = returnIsTransfer;
    }

    public String getReturnCostTime() {
        return returnCostTime;
    }

    public void setReturnCostTime(String returnCostTime) {
        this.returnCostTime = returnCostTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", productId=").append(productId);
        sb.append(", tripFlightNumber=").append(tripFlightNumber);
        sb.append(", tripAirlineCompany=").append(tripAirlineCompany);
        sb.append(", returnFlightNumber=").append(returnFlightNumber);
        sb.append(", returnAirlineCompany=").append(returnAirlineCompany);
        sb.append(", stockLimit=").append(stockLimit);
        sb.append(", stock=").append(stock);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", tripDepartureCityName=").append(tripDepartureCityName);
        sb.append(", tripDepartureAirportName=").append(tripDepartureAirportName);
        sb.append(", tripDepartureTerminal=").append(tripDepartureTerminal);
        sb.append(", tripDestinationCityName=").append(tripDestinationCityName);
        sb.append(", tripDestinationAirportName=").append(tripDestinationAirportName);
        sb.append(", tripDestinationTerminal=").append(tripDestinationTerminal);
        sb.append(", tripPlaneType=").append(tripPlaneType);
        sb.append(", tripDepartureTime=").append(tripDepartureTime);
        sb.append(", tripDestinationTime=").append(tripDestinationTime);
        sb.append(", tripFlightType=").append(tripFlightType);
        sb.append(", tripIsTransfer=").append(tripIsTransfer);
        sb.append(", tripCostTime=").append(tripCostTime);
        sb.append(", returnDepartureCityName=").append(returnDepartureCityName);
        sb.append(", returnDepartureAirportName=").append(returnDepartureAirportName);
        sb.append(", returnDepartureTerminal=").append(returnDepartureTerminal);
        sb.append(", returnDestinationCityName=").append(returnDestinationCityName);
        sb.append(", returnDestinationAirportName=").append(returnDestinationAirportName);
        sb.append(", returnDestinationTerminal=").append(returnDestinationTerminal);
        sb.append(", returnPlaneType=").append(returnPlaneType);
        sb.append(", returnDepartureTime=").append(returnDepartureTime);
        sb.append(", returnDestinationTime=").append(returnDestinationTime);
        sb.append(", returnFlightType=").append(returnFlightType);
        sb.append(", returnIsTransfer=").append(returnIsTransfer);
        sb.append(", returnCostTime=").append(returnCostTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}