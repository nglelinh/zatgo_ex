package com.ykb.mall.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("微信支付参数")
public class WXPayData {
	
	@ApiModelProperty(value = "支付方式：1-h5;2-公众号", allowableValues = "1,2", dataType = "Byte")
	private Byte payType;
	
	@ApiModelProperty(value = "公众号支付参数：package")
	private String packageStr;
	
	@ApiModelProperty(value = "公众号支付参数：签名随机字符")
	private String nonceStr;
	
	@ApiModelProperty(value = "公众号支付参数：时间戳")
	private String timestamp;
	
	@ApiModelProperty(value = "公众号支付参数：签名类型")
	private String signType;
	
	@ApiModelProperty(value = "公众号支付参数：支付签名")
	private String paySign;
	
	@ApiModelProperty(value = "h5支付链接")
	private String codeUrl;

	public Byte getPayType() {
		return payType;
	}

	public void setPayType(Byte payType) {
		this.payType = payType;
	}

	public String getPackageStr() {
		return packageStr;
	}

	public void setPackageStr(String packageStr) {
		this.packageStr = packageStr;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getPaySign() {
		return paySign;
	}

	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

	public String getCodeUrl() {
		return codeUrl;
	}

	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}

}
