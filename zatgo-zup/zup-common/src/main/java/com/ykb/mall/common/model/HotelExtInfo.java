package com.ykb.mall.common.model;

import java.util.List;

/**
 * Created by 46041 on 2019/6/17.
 */
public class HotelExtInfo {

    private String hotelName;

    private String roomType;

    private String startDate;

    private String endDate;

    private List<Long> prices;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<Long> getPrices() {
        return prices;
    }

    public void setPrices(List<Long> prices) {
        this.prices = prices;
    }
}
