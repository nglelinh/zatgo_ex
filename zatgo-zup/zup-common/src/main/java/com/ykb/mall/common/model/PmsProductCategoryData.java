package com.ykb.mall.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @Author wangyucong
 * @Date 2019/2/15 16:28
 */
@ApiModel("产品类型")
public class PmsProductCategoryData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long parentId;

    @ApiModelProperty("产品类型名称")
    private String name;

    @ApiModelProperty("产品类型等级")
    private Integer level;

    @ApiModelProperty("产品类型排序")
    private Integer sort;

    @ApiModelProperty("描述")
    private String description;

    @ApiModelProperty("产品类型图标")
    private String icon;

    private List<PmsProductCategoryData> categoryChildList;
    
    @ApiModelProperty("图册")
    private String albumPics;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<PmsProductCategoryData> getCategoryChildList() {
        return categoryChildList;
    }

    public void setCategoryChildList(List<PmsProductCategoryData> categoryChildList) {
        this.categoryChildList = categoryChildList;
    }

	public String getAlbumPics() {
		return albumPics;
	}

	public void setAlbumPics(String albumPics) {
		this.albumPics = albumPics;
	}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}