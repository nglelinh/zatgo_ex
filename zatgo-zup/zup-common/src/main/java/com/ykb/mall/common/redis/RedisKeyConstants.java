package com.ykb.mall.common.redis;

/**
 * @Author wangyucong
 * @Date 2019/4/8 10:20
 */
    public class RedisKeyConstants {

    public static String WECHAT_ACCESS_TOKEN = "WECHAT_ACCESS_TOKEN";

    public static final String WECHAT_JSAPI_TICKET = "WECHAT_JSAPI_TICKET";

    public static final String WECHAT_ACCESS_TOKEN_LOCK = "WECHAT_ACCESS_TOKEN_LOCK";

    public static final String WECHAT_JSAPI_TICKET_LOCK = "WECHAT_JSAPI_TICKET_LOCK";
    
    /**
     * 微信支付通知锁
     */
    public static final String WECHAT_PAY_NOTIFY_LOCK = "WECHAT_PAY_NOTIFY_LOCK_%s";
    
    public static final String CREATE_OMS_ORDER = "CREATE_OMS_ORDER_";

    private static final String USER_TOKEN = "USER_TOKEN:";

    public static final String WECHAT_MSG_REPLY = "WECHAT_MSG_REPLY:%s_%s_%s";




    public static String getUserTokenKey(String userName){
        return USER_TOKEN + userName;
    }
}