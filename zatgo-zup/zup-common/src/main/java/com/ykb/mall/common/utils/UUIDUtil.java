package com.ykb.mall.common.utils;

import java.util.UUID;

/**
 * @Author wangyucong
 * @Date 2019/2/20 17:21
 */
public class UUIDUtil {

    public static String getRandomUserName() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}