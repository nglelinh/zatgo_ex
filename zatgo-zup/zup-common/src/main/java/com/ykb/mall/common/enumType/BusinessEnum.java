package com.ykb.mall.common.enumType;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author wangyucong
 * @Date 2019/2/20 16:16
 */
public class BusinessEnum {

    /**
     * 登录类型：1 - 登录；2 - 注册登录
     */
    public enum LoginType{
        LOGIN(1), REGISTER(2);

        private Byte code;

        LoginType(Integer code) {
            this.code = code.byteValue();
        }

        public Byte getCode() {
            return code;
        }
    }

    public enum GroupBookingOrderStatus{

        GroupBooking(new Byte("1")),GroupBookingSuccess(new Byte("2")),Refunding(new Byte("3")),RefundSuccess(new Byte("4")),Cancel(new Byte("5"));

        private Byte code;

        private static final Map<Byte, GroupBookingOrderStatus> stringToEnum = new HashMap<Byte, GroupBookingOrderStatus>();
        static {
            for(GroupBookingOrderStatus enumType : values()) {
                stringToEnum.put(enumType.getCode(), enumType);
            }
        }

        public static GroupBookingOrderStatus getEnumByType(String type) {
            return stringToEnum.get(type);
        }

        private GroupBookingOrderStatus(Byte code) {
            this.code = code;
        }

        public Byte getCode() {
            return code;
        }

        public void setCode(Byte code) {
            this.code = code;
        }
    }


    public enum GroupBookingOrderPayStatus{

        WaitPay("0"),HasPay("1"),HasSend("2"),Done("3"),Cancel("4"),Refund("5");

        private String code;

        private static final Map<String, GroupBookingOrderPayStatus> stringToEnum = new HashMap<String, GroupBookingOrderPayStatus>();
        static {
            for(GroupBookingOrderPayStatus enumType : values()) {
                stringToEnum.put(enumType.getCode(), enumType);
            }
        }

        public static GroupBookingOrderPayStatus getEnumByType(String type) {
            return stringToEnum.get(type);
        }

        private GroupBookingOrderPayStatus(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

    public enum GroupBookingStatus{

        Input(new Byte("0")),Online(new Byte("1")),Offline(new Byte("2"));

        private Byte code;

        private static final Map<Byte, GroupBookingStatus> stringToEnum = new HashMap<Byte, GroupBookingStatus>();
        static {
            for(GroupBookingStatus enumType : values()) {
                stringToEnum.put(enumType.getCode(), enumType);
            }
        }

        public static GroupBookingStatus getEnumByType(Byte type) {
            return stringToEnum.get(type);
        }

        private GroupBookingStatus(Byte code) {
            this.code = code.byteValue();
        }

        public Byte getCode() {
            return code;
        }

        public void setCode(Byte code) {
            this.code = code;
        }
    }

    public enum ActivityBargainStatus{

        Input(new Byte("0")),Online(new Byte("1")),Offline(new Byte("2"));

        private Byte code;

        private static final Map<Byte, ActivityBargainStatus> stringToEnum = new HashMap<Byte, ActivityBargainStatus>();
        static {
            for(ActivityBargainStatus enumType : values()) {
                stringToEnum.put(enumType.getCode(), enumType);
            }
        }

        public static ActivityBargainStatus getEnumByType(Byte type) {
            return stringToEnum.get(type);
        }

        private ActivityBargainStatus(Byte code) {
            this.code = code.byteValue();
        }

        public Byte getCode() {
            return code;
        }

        public void setCode(Byte code) {
            this.code = code;
        }
    }

    /**
     * 是否：0 - 否；1 - 是
     */
    public enum YesOrNo{
        NO(0), YES(1);

        private Byte code;

        YesOrNo(Integer code) {
            this.code = code.byteValue();
        }

        public Byte getCode() {
            return code;
        }
    }


    /**
     * 支付方式：0 - 未支付；1 - 支付宝；2 - 微信
     */
    public enum PayType{
        unpaid(0),aliPay(1),weixinPay(2);

        private Integer code;

        PayType(Integer code) {
            this.code = code;
        }

        public Integer getCode() {
            return code;
        }
    }

    /**
     * 订单来源：0 - PC订单；1 - APP订单；2 - 微信H5订单
     */
    public enum SourceType{
        pc(0),app(1),weixin_H5(2);

        private Integer code;

        SourceType(Integer code) {
            this.code = code;
        }

        public Integer getCode() {
            return code;
        }
    }

    /**
     * Oms订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单；6->已支付
     */
    public enum OrderStatus{
        wait_pay(0),wait_deliver(1),has_Delivered(2),done(3),closed(4),invalid(5), paid(6), refund(7);

        private Integer code;

        OrderStatus(Integer code) {
            this.code = code;
        }

        public Integer getCode() {
            return code;
        }
    }

    /**
     * 订单类型：0 - 正常订单；1 - 秒杀订单
     */
    public enum OrderType{
        ordinary(0),ms_order(1);

        private Integer code;

        OrderType(Integer code) {
            this.code = code;
        }

        public Integer getCode() {
            return code;
        }
    }

    /**
     * 业务类型：1 - 产品班期
     */
    public enum BusinessNumType{
        PRODUCT_GROUP(1);

        private Byte code;

        BusinessNumType(Integer code) {
            this.code = code.byteValue();
        }

        public Byte getCode() {
            return code;
        }
    }

    /**
     * 班期交通类型：1 - 接；2 - 送
     */
    public enum TransportType{
        PICK(1),DELIVER(2);

        private Byte code;

        TransportType(Integer code) {
            this.code = code.byteValue();
        }

        public Byte getCode() {
            return code;
        }
    }

    /**
     * 模板编码：1001 - 订单申请通知
     */
    public enum TemplateCode{
        APPLY_NOTICE(1001);

        private String code;

        TemplateCode(Integer code) {
            this.code = String.valueOf(code);
        }

        public String getCode() {
            return code;
        }
    }

    /**
     * 业务类型（1-旅游；2-机票；3-酒店；4-文章；5-门票；6-其他）
     */
    public enum BusinessType{
        TRAVEL(1), FLIGHT(2), HOTEL(3), ARTICLE(4),COMMON(5),OTHER(6);

        private Byte code;

        BusinessType(Integer code) {
            this.code = code.byteValue();
        }

        public Byte getCode() {
            return code;
        }
    }

    /**
     * 价格类型（1-成人票；2-儿童票；）
     */
    public enum PriceType{
        ADULT(1), CHILD(2);

        private Byte code;

        PriceType(Integer code) {
            this.code = code.byteValue();
        }

        public Byte getCode() {
            return code;
        }
    }

    /**
     * 创建订单时的类型（0-普通;1-砍价；2-拼团）
     */
    public enum CreateOrderType{
        ORDINARY("0"), BARGAIN("1"), GROUPBOOKING("2");

        private String code;

        CreateOrderType(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
    
    /**
     * 微信支付类型：1-h5;2-公众号
     */
    public enum WeiXinPayType{
    	H5(1), PUBLIC(2);
    	
    	private Integer code;
    	
    	WeiXinPayType(Integer code){
    		this.code = code;
    	}
    	
    	public Integer getCode() {
            return code;
        }
    }
    
    /**
     * 金额流向：1-支付；2-退款
     */
    public enum MoneyFlow{
    	PAYMENT(1), REFUND(2);
    	
    	private Byte code;
    	
    	MoneyFlow(Integer code){
    		this.code = code.byteValue();
    	}
    	
    	public Byte getCode() {
            return code;
        }
    }
    
    /**
     * 旅游产品类型:1-自由行，2-跟团游，3-周边游
     * @author Administrator
     *
     */
    public enum TravelType{
    	FREE_WALKER(1), PACKAGE_TOUR(2), TRAVEL_AROUND(3);
    	
    	private Byte code;
    	
    	TravelType(Integer code){
    		this.code = code.byteValue();
    	}
    	
    	public Byte getCode() {
            return code;
        }
    }
    
    /**
     * 证件类型，1-身份证；2-护照；3-军官证；4-港澳通行证；5-其他证件
     * @author Administrator
     *
     */
    public enum IdcardType{
    	IDENTITY_CARD(1), PASSPORT(2), CERTIFICATE_OF_OFFICERS(3), TRAFFIC_PERMIT(4), OTHER(5);
    	
    	private Byte code;
    	
    	IdcardType(Integer code){
    		this.code = code.byteValue();
    	}
    	
    	public Byte getCode() {
            return code;
        }
    }
    
    /**
     * 订单操作人
     * @author Administrator
     *
     */
    public enum OrderOperateMan {
    	USER("用户"), SYS("系统"), ADMIN("后台管理员");
    	
    	private String code;
    	
    	OrderOperateMan(String code){
    		this.code = code;
    	}
    	
    	public String getCode() {
            return code;
        }
    }
    
    public enum WechatMsgStatus{
    	SEND(0), SUCCESS(1), FAILED(2);
    	
    	private Byte code;
    	
    	WechatMsgStatus(Integer code){
    		this.code = code.byteValue();
    	}
    	
    	public Byte getCode() {
            return code;
        }
    }
    
    public static void main(String[] args) {
		String i = "traffic_permit";
		System.out.println(i.toUpperCase());
	}
}