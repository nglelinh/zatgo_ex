package com.ykb.mall.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("文章详情")
public class PmsProductArticleDetail extends PmsProductDetail {

	private static final long serialVersionUID = -368559366467543640L;
	
	@ApiModelProperty("作者")
	private String author;
	
	@ApiModelProperty("作者头像")
	private String authorIcon;
	
	@ApiModelProperty("发布时间")
	private Date publishTime;
	
	@ApiModelProperty("点赞数")
	private Integer likeNum;
	
	@ApiModelProperty("浏览数")
	private Integer watchNum;
	
	@ApiModelProperty("目的地")
	private String productCategoryName;
	
	@ApiModelProperty("职位")
	private String position;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthorIcon() {
		return authorIcon;
	}

	public void setAuthorIcon(String authorIcon) {
		this.authorIcon = authorIcon;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	public Integer getLikeNum() {
		return likeNum;
	}

	public void setLikeNum(Integer likeNum) {
		this.likeNum = likeNum;
	}

	public Integer getWatchNum() {
		return watchNum;
	}

	public void setWatchNum(Integer watchNum) {
		this.watchNum = watchNum;
	}

	public String getProductCategoryName() {
		return productCategoryName;
	}

	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

}
