package com.ykb.mall.common.model;

import java.io.Serializable;
import java.util.Date;

public class WechatMsgReply implements Serializable {
    private Long id;

    /**
     * 消息类型
     *
     * @mbggenerated
     */
    private String msgType;

    /**
     * 事件类型
     *
     * @mbggenerated
     */
    private String event;

    /**
     * 消息key
     *
     * @mbggenerated
     */
    private String key;

    /**
     * 消息回复
     *
     * @mbggenerated
     */
    private String reply;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", msgType=").append(msgType);
        sb.append(", event=").append(event);
        sb.append(", key=").append(key);
        sb.append(", reply=").append(reply);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}