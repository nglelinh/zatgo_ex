package com.ykb.mall.common.model;

import java.util.List;

/**
 * Created by 46041 on 2019/6/17.
 */
public class TravelExtInfo {

    private String startDate;

    private String endDate;

    private List<Long> prices;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<Long> getPrices() {
        return prices;
    }

    public void setPrices(List<Long> prices) {
        this.prices = prices;
    }
}
