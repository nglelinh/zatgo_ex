package com.ykb.mall.common.model;

import io.swagger.annotations.ApiModelProperty;

public class PageParams {
	
	@ApiModelProperty("页数")
	private Integer pageSize;
	
	@ApiModelProperty("数量")
	private Integer pageNum;

	public Integer getPageSize() {
		return pageSize == null ? 10 : pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNum() {
		return pageNum == null ? 1 : pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

}
