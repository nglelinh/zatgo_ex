package com.zatgo.zup.transaction.basic.entity.btc;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 46041 on 2018/10/10.
 */
public class Address implements Serializable{
    /**
     * 地址
     */
    private String address;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date updateDate;



    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
