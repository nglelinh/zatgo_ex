package com.zatgo.zup.transaction.basic.listener;

import com.zatgo.zup.transaction.basic.service.CollectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 46041 on 2019/2/28.
 */
public class CollectListener implements Runnable {


    private static final Logger logger = LoggerFactory.getLogger(CheckTransactionListener.class);

    private CollectService collectService;

    @Override
    public void run() {
        while (true){
            try {
                collectService.collect();
                Thread.sleep(10 * 1000l);
            } catch (Exception e){
                logger.error("", e);
                try {
                    Thread.sleep(10 * 1000l);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public CollectListener(CollectService collectService) {
        this.collectService = collectService;
    }
}
