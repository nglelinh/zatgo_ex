package com.zatgo.zup.transaction.basic.entity.usdt;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by 46041 on 2018/10/10.
 */
public class Vout implements Serializable {

    private String address;

    private BigDecimal value;

    private ScriptPubkey scriptPubKey;

    private Integer vout;

    private Boolean isSelfAddr;

    private String coinType;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public ScriptPubkey getScriptPubKey() {
        return scriptPubKey;
    }

    public void setScriptPubKey(ScriptPubkey scriptPubKey) {
        this.scriptPubKey = scriptPubKey;
    }

    public Integer getVout() {
        return vout;
    }

    public void setVout(Integer vout) {
        this.vout = vout;
    }

    public Boolean getSelfAddr() {
        return isSelfAddr;
    }

    public void setSelfAddr(Boolean selfAddr) {
        isSelfAddr = selfAddr;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }
}
