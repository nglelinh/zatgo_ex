package com.zatgo.zup.transaction.basic.entity.params;


import com.zatgo.zup.transaction.basic.entity.usdt.UTXO;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 46041 on 2018/10/16.
 */
public class InsertUsdtRecordParam {

    private String txid;

    private List<UTXO> payList;

    private String hex;

    private String recordId;

    private String toAddress;

    private String fromAddress;

    private BigDecimal amount;

    private BigDecimal fee;


    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public List<UTXO> getPayList() {
        return payList;
    }

    public void setPayList(List<UTXO> payList) {
        this.payList = payList;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
}
