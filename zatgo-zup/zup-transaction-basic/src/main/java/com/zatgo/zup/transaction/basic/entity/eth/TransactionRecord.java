package com.zatgo.zup.transaction.basic.entity.eth;

import com.alibaba.fastjson.JSONArray;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by 46041 on 2018/10/10.
 */
public class TransactionRecord implements Serializable {
    /**
     * 块上交易ID
     */
    private String txId;
    /**
     * 系统交易id
     */
    private String recordId;
    /**
     * 出账地址
     */
    private String fromAddress;
    /**
     * 入账地址
     */
    private String toAddress;
    /**
     * 1充值0提币
     */
    private Integer type;
    /**
     * 确认数
     */
    private Integer confirm;
    /**
     * 币种类型
     */
    private String coinType;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 区块高度
     */
    private BigInteger height;
    /**
     * 合约地址
     */
    private String contractAddress;
    /**
     * 金额
     */
    private BigDecimal amount;

    private BigDecimal gasLimit;

    private BigDecimal gasPrice;

    private JSONArray topics;
    /**
     *  针对手续费交易的操作，如果使用了该笔手续费，将值置为1
     */
    private Integer isUsed;


    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public BigInteger getHeight() {
        return height;
    }

    public void setHeight(BigInteger height) {
        this.height = height;
    }

    public String getContractAddress() {
        return contractAddress;
    }

    public void setContractAddress(String contractAddress) {
        this.contractAddress = contractAddress;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getGasLimit() {
        return gasLimit;
    }

    public void setGasLimit(BigDecimal gasLimit) {
        this.gasLimit = gasLimit;
    }

    public BigDecimal getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(BigDecimal gasPrice) {
        this.gasPrice = gasPrice;
    }

    public JSONArray getTopics() {
        return topics;
    }

    public void setTopics(JSONArray topics) {
        this.topics = topics;
    }

    public Integer getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(Integer isUsed) {
        this.isUsed = isUsed;
    }
}
