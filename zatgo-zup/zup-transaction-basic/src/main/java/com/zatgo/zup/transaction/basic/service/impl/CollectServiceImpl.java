package com.zatgo.zup.transaction.basic.service.impl;

import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.transaction.basic.conf.ContractConfig;
import com.zatgo.zup.transaction.basic.entity.SendRequest;
import com.zatgo.zup.transaction.basic.entity.account.TransfAddressBalance;
import com.zatgo.zup.transaction.basic.entity.account.TransfConfig;
import com.zatgo.zup.transaction.basic.entity.collect.TransfCollectRecord;
import com.zatgo.zup.transaction.basic.entity.collect.TransfCollectRecordExample;
import com.zatgo.zup.transaction.basic.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.basic.mapper.TransfCollectRecordMapper;
import com.zatgo.zup.transaction.basic.service.AccountService;
import com.zatgo.zup.transaction.basic.service.CoinService;
import com.zatgo.zup.transaction.basic.service.CollectService;
import com.zatgo.zup.transaction.basic.util.eth.EthUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/1/9.
 */

@Service("collectService")
public class CollectServiceImpl implements CollectService {

    private static final Logger logger = LoggerFactory.getLogger(CollectServiceImpl.class);


    @Autowired
    private ConfigurableApplicationContext applicationContext;
    @Autowired
    private AccountService accountService;
    @Autowired
    private ContractConfig contractConfig;
    @Autowired
    private TransfCollectRecordMapper transfCollectRecordMapper;
    @Autowired
    private EthUtil ethUtil;


    @Value("${transaction.type}")
    private String type;

    @Override
    public void collect() {
        CoinService coinService = (CoinService) applicationContext.getBean(type + "Service");
        List<TransfConfig> configs = contractConfig.getCollectList();
        for (TransfConfig config : configs) {
            try {
                String coinNetworkType = config.getCoinNetworkType();
                String coinType = config.getCoinType();
                BigDecimal collectLimit = config.getCollectLimit();
                String collectReceiveAddress = config.getCollectReceiveAddress();
                List<TransfAddressBalance> accounts = accountService.getNeedCollectAccount(collectLimit, coinNetworkType, coinType);
                for (TransfAddressBalance account : accounts) {
                    try {
                        String address = account.getAddress();
                        if(!collectReceiveAddress.equals(address) && !config.getSystemSendAddress().contains(address)){
                            send(account, coinService, config);
                        }
                    } catch (Exception e){
                        logger.error("", e);
                    }
                }
            } catch (Exception e){
                logger.error("", e);
            }
        }
    }



    private void send(TransfAddressBalance account, CoinService coinService, TransfConfig config){
        try{
            String coinType = account.getCoinType();
            String coinNetworkType = account.getCoinNetworkType();
            BigDecimal gasLimit = config.getGasLimit();
            BigDecimal gasPrice = config.getGasPrice();
            Date date = new Date();
            BigDecimal fee = null;
            if ("eth".equalsIgnoreCase(type)){
                fee = ethUtil.ethAmountFormate9(gasPrice.multiply(gasLimit));
            } else {
                fee = config.getFee();
            }
            //是主币
            if (!coinType.equalsIgnoreCase(coinNetworkType)) {
                TransfAddressBalance addressAccount = accountService.getAddressAccount(coinNetworkType, coinNetworkType, account.getAddress());
                //主币不够，需要打手续费
                if (addressAccount.getConfirmed().subtract(fee).compareTo(BigDecimal.ZERO) == -1){
                    if (hasSendFee(config.getFeeAddress(), account.getAddress())){
                        return;
                    }
                    SendRequest request = new SendRequest();
                    request.setCoinType(coinNetworkType);
                    request.setBusinessTxId(UUIDUtils.getUuid() + "_1");
                    request.setFee(fee);
                    request.setAmount(fee);
                    request.setToAddress(account.getAddress());
                    request.setFromAddress(config.getFeeAddress());
                    request.setGasLimit(gasLimit);
                    request.setGasPrice(gasPrice);
                    request.setTransfType(TransactionTypeEnum.COLLECT.getCode());
                    String txId = coinService.sendTransaction(request);

                    //保存记录发送手续记录
                    TransfCollectRecord record = new TransfCollectRecord();
                    record.setBusinessId(request.getBusinessTxId());
                    record.setCoinNetworkType(type);
                    record.setCoinType(request.getCoinType());
                    record.setCreateDate(date);
                    record.setFromAddress(request.getFromAddress());
                    record.setToAddress(request.getToAddress());
                    record.setTxId(txId);
                    record.setId(UUIDUtils.getUuid());
                    record.setType(new Byte("0"));
                    record.setAmount(request.getAmount());
                    record.setFee(fee);
                    transfCollectRecordMapper.insertSelective(record);
                    return;
                }
            }
            if (!hasCollect(account.getAddress(), config.getCollectReceiveAddress())){
                SendRequest request = new SendRequest();
                request.setCoinType(coinType);
                request.setBusinessTxId(UUIDUtils.getUuid());
                request.setFee(fee);
                if(coinType.equalsIgnoreCase(coinNetworkType)){
                    request.setAmount(account.getConfirmed().subtract(fee));
                } else {
                    request.setAmount(account.getConfirmed());
                }
                request.setToAddress(config.getCollectReceiveAddress());
                request.setFromAddress(account.getAddress());
                request.setGasLimit(config.getGasLimit());
                request.setGasPrice(config.getGasPrice());
                request.setTransfType(TransactionTypeEnum.COLLECT.getCode());
                String txId = coinService.sendTransaction(request);


                //保存收集记录
                TransfCollectRecord record = new TransfCollectRecord();
                record.setBusinessId(request.getBusinessTxId());
                record.setCoinNetworkType(type);
                record.setCoinType(request.getCoinType());
                record.setCreateDate(date);
                record.setFromAddress(request.getFromAddress());
                record.setToAddress(request.getToAddress());
                record.setTxId(txId);
                record.setId(UUIDUtils.getUuid());
                record.setType(new Byte("1"));
                record.setAmount(request.getAmount());
                record.setFee(fee);
                transfCollectRecordMapper.insertSelective(record);
            }
        } catch (Exception e){
            logger.error("", e);
        }
    }

    private boolean hasCollect(String fromAddress, String toAddress){
        TransfCollectRecordExample example = new TransfCollectRecordExample();
        TransfCollectRecordExample.Criteria criteria = example.createCriteria();
        criteria.andFromAddressEqualTo(fromAddress);
        criteria.andToAddressEqualTo(toAddress);
        criteria.andTypeEqualTo(new Byte("1"));
        criteria.andIsDoneEqualTo(false);
        int i = transfCollectRecordMapper.countByExample(example);
        return i > 0;
    }

    private boolean hasSendFee(String fromAddress, String toAddress){
        TransfCollectRecordExample example = new TransfCollectRecordExample();
        TransfCollectRecordExample.Criteria criteria = example.createCriteria();
        criteria.andFromAddressEqualTo(fromAddress);
        criteria.andToAddressEqualTo(toAddress);
        criteria.andTypeEqualTo(new Byte("0"));
        criteria.andIsDoneEqualTo(false);
        int i = transfCollectRecordMapper.countByExample(example);
        return i > 0;
    }

}
