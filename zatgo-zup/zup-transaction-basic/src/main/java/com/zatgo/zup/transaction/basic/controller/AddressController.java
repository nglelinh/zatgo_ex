package com.zatgo.zup.transaction.basic.controller;

import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.transaction.basic.service.CoinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by 46041 on 2019/1/29.
 */



@RestController
@RequestMapping("/address")
public class AddressController {

    private static final Logger logger = LoggerFactory.getLogger(AddressController.class);

    @Autowired
    private ConfigurableApplicationContext applicationContext;


    @GetMapping("/eth/add")
    public ResponseData<List<String>> addAddressEth(){
        try {
            CoinService coinService = (CoinService) applicationContext.getBean("ethService");
            coinService.addAddress();
            return BusinessResponseFactory.createSuccess(null);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
    }

    @GetMapping("/qtum/add")
    public ResponseData<List<String>> addAddressQtum(){
        try {
            CoinService coinService = (CoinService) applicationContext.getBean("qtumService");
            coinService.addAddress();
            return BusinessResponseFactory.createSuccess(null);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
    }

    @GetMapping("/btc/add")
    public ResponseData<List<String>> addAddressBtc(){
        try {
            CoinService coinService = (CoinService) applicationContext.getBean("btcService");
            coinService.addAddress();
            return BusinessResponseFactory.createSuccess(null);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
    }

    @GetMapping("/usdt/add")
    public ResponseData<List<String>> addAddressUsdt(){
        try {
            CoinService coinService = (CoinService) applicationContext.getBean("usdtService");
            coinService.addAddress();
            return BusinessResponseFactory.createSuccess(null);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
    }
}
