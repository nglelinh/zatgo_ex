package com.zatgo.zup.transaction.basic.mapper;


import com.zatgo.zup.transaction.basic.entity.account.TransfBalanceChangeRecord;
import org.springframework.stereotype.Repository;

@Repository
public interface TransfBalanceChangeRecordMapper extends BaseMapper<TransfBalanceChangeRecord>{
}