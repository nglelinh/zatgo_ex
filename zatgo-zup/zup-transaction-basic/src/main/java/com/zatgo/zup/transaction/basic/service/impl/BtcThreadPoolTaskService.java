package com.zatgo.zup.transaction.basic.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.transaction.basic.entity.BaseTransactionRecord;
import com.zatgo.zup.transaction.basic.entity.btc.Address;
import com.zatgo.zup.transaction.basic.entity.btc.TransactionRecord;
import com.zatgo.zup.transaction.basic.entity.btc.UTXO;
import com.zatgo.zup.transaction.basic.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.basic.service.ThreadPoolTaskService;
import com.zatgo.zup.transaction.basic.util.MongoUtil;
import com.zatgo.zup.transaction.basic.util.btc.BitcoinUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.Future;

/**
 * Created by 46041 on 2018/10/17.
 */

@Service("btcThreadPoolTaskService")
public class BtcThreadPoolTaskService implements ThreadPoolTaskService {

    private static final Logger logger = LoggerFactory.getLogger(BtcThreadPoolTaskService.class);

    @Autowired
    private BitcoinUtil bitcoinUtil;
    @Autowired
    private MongoUtil mongoUtil;
    @Value("${transaction.confirmed:}")
    private Integer confirmed;

    @Override
    @Async("threadPoolTask")
    public Future<ResponseData<Object>> tx(String txId, Integer maxValue){
        ResponseData<Object> result = null;
        try {
            TransactionRecord transaction = bitcoinUtil.getTransaction(txId, Integer.valueOf(maxValue + ""));
            //获取所有utxo
            List<UTXO> unSpentUTXO = bitcoinUtil.getUnSpentUTXO(transaction);
            //保存自己的utxo，返回是否保存成功
            saveDepositUTXOInfo(unSpentUTXO, transaction);
            //更新已经存在提币的交易
            result = BusinessResponseFactory.createSuccess(null);
        } catch (Exception e){
            logger.error("", e);
            result = BusinessResponseFactory.createBusinessError("-1", txId);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return new AsyncResult(result);
    }



//    @Override
//    @Async("scanExtractTxThreadPool")
//    public Future<ResponseData<Object>> scanExtractTx(String txId, Integer maxValue){
//        ResponseData<Object> result = null;
//        try {
//            TransactionRecord transaction = bitcoinUtil.getTransaction(txId, Integer.valueOf(maxValue + ""));
//            //获取所有utxo
//            List<UTXO> spentUTXO = bitcoinUtil.getSpentUTXO(transaction);
//            //更新utxo并查看是否为自己的交易
//            boolean isSelfTx = updateExtractUTXOInfo(spentUTXO, transaction.getConfirmed());
//            //将提币交易记录过滤掉
//            if (selfTransactionMap.containsKey(txId)){
//                TransactionRecord record = (TransactionRecord) selfTransactionMap.get(txId);
//                transaction.setVoutList(record.getVoutList());
//                transaction.setRecordId(record.getRecordId());
//                transaction.setFee(record.getFee());
//                Map<String, Object> map = new HashMap();
//                map.put(MongoConstant.TRANSACTION_CLASS_TXID, txId);
//                mongoUtil.delte(map, MongoConstant.TRANSACTION_CLASS);
//                mongoUtil.insert(transaction, MongoConstant.TRANSACTION_CLASS);
//            } else if (isSelfTx){
//                //如果是自己的提币交易
//                mongoUtil.insert(transaction, MongoConstant.TRANSACTION_CLASS);
//            }
//            result = BusinessResponseFactory.createSuccess(null);
//        } catch (Exception e){
//            logger.error("", e);
//            result = BusinessResponseFactory.createBusinessError("-1", txId);
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
//        return new AsyncResult(result);
//    }





    private void saveDepositUTXOInfo(List<UTXO> utxos, TransactionRecord transaction){
        if (CollectionUtils.isEmpty(utxos))
            return ;
        Map<String, Collection<String>> query = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (UTXO utxo : utxos){
            String address = utxo.getAddress();
            if (address != null){
                list.add(address);
            }
        }
        query.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, list);
        //查询是否为自己的地址
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        if (!CollectionUtils.isEmpty(addresses)){
            List<UTXO> utxoList = new ArrayList<>();
            int i = utxos.size() - 1;
            for ( ; i >= 0; i--){
                UTXO utxo = utxos.get(i);
                String utxoAddress = utxo.getAddress();
                if (!hasAddress(addresses, utxoAddress)){
                    continue;
                }
                Query utxoQuery = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
                criteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
                utxoQuery.addCriteria(criteria);
                List<UTXO> data = mongoUtil.getData(utxoQuery, UTXO.class, MongoConstant.UTXO_CLASS_NAME);
                //防止重复添加utxo
                if (CollectionUtils.isEmpty(data)){
                    utxoList.add(utxo);
                }
            }
            mongoUtil.batchExecuteInsert(utxoList, MongoConstant.UTXO_CLASS_NAME);
            if (getTransactionRecord(transaction.getTxId()) == null){
                mongoUtil.insert(transaction, MongoConstant.TRANSACTION_CLASS);
            }
        }
    }


//    /**
//     * 提币时候用
//     * @param utxos
//     * @param confirmed
//     * @return
//     */
//    private boolean updateExtractUTXOInfo(List<UTXO> utxos, Integer confirmed){
//        if (CollectionUtils.isEmpty(utxos))
//            return false;
//        Map<String, Collection<String>> query = new HashMap<>();
//        List<String> list = new ArrayList<>();
//        for (UTXO utxo : utxos){
//            String txId = utxo.getTxId();
//            if (!StringUtils.isEmpty(txId)){
//                list.add(txId);
//            }
//        }
//        query.put(MongoConstant.UTXO_CLASS_NAME_TXID, list);
//        //查询是否为自己的地址
//        List<UTXO> utxoList = mongoUtil.getDataByListQuery(query, UTXO.class, MongoConstant.UTXO_CLASS_NAME);
//        if (!CollectionUtils.isEmpty(utxoList)){
//            int i = utxos.size() - 1;
//            for ( ; i >= 0; i--){
//                UTXO utxo = utxos.get(i);
//                utxo = hasUTXO(utxoList, utxo);
//                if (utxo == null){
//                    continue;
//                }
//                Query utxoQuery = new Query();
//                Criteria utxoCriteria = new Criteria();
//                utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
//                utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
//                utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_STATUS).is(2);
//                utxoQuery.addCriteria(utxoCriteria);
//                List<UTXO> data = mongoUtil.getData(utxoQuery, UTXO.class, MongoConstant.UTXO_CLASS_NAME);
//                if (CollectionUtils.isEmpty(data)) {
//                    //更新地址余额
//                    Map addBalance = new HashMap();
//                    Update update = new Update();
//                    try {
//                        Address address = bitcoinUtil.getAddress(utxo.getAddress());
//                        if (confirmed >= this.confirmed) {
//                            Query updateUTXOQuery = new Query();
//                            Criteria criteria = new Criteria();
//                            criteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
//                            criteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
//                            updateUTXOQuery.addCriteria(criteria);
//                            Update updateUTXO = new Update();
//                            updateUTXO.set(MongoConstant.UTXO_CLASS_NAME_STATUS, 2);
//                            //更新utxo状态
//                            mongoUtil.update(updateUTXOQuery, MongoConstant.UTXO_CLASS_NAME, updateUTXO);
//                        }
//                    } catch (Exception e) {
//                        logger.error("", e);
//                    }
//                    addBalance.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, utxo.getAddress());
//                    mongoUtil.update(addBalance, MongoConstant.ADDRESS_CLASS_NAME, update);
//                }
//            }
//            return true;
//        }
//        return false;
//    }



    private boolean hasAddress(List<Address> addresses, String find){
        for (Address address : addresses){
            if (address.getAddress().equals(find)){
                return true;
            }
        }
        return false;
    }

//    private UTXO hasUTXO(List<UTXO> utxos, UTXO find){
//        if (find == null)
//            return null;
//        for (UTXO utxo : utxos){
//            if (utxo.getTxId().equals(find.getTxId()) && utxo.getVout().intValue() == find.getVout().intValue()){
//                return utxo;
//            }
//        }
//        return null;
//    }

    private TransactionRecord getTransactionRecord(String txId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.TRANSACTION_CLASS_TXID).is(txId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }
}
