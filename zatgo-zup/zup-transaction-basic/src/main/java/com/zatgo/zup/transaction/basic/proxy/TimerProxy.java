package com.zatgo.zup.transaction.basic.proxy;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.transaction.basic.conf.ContractConfig;
import com.zatgo.zup.transaction.basic.entity.account.TransfNoticeRecord;
import com.zatgo.zup.transaction.basic.entity.account.TransfNoticeRecordExample;
import com.zatgo.zup.transaction.basic.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.basic.entity.eth.Address;
import com.zatgo.zup.transaction.basic.mapper.TransfNoticeRecordMapper;
import com.zatgo.zup.transaction.basic.service.TimerService;
import com.zatgo.zup.transaction.basic.util.MongoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by 46041 on 2019/1/23.
 */

@Component
public class TimerProxy {

    private static final Logger logger = LoggerFactory.getLogger(TimerProxy.class);

    @Autowired
    private TransfNoticeRecordMapper transfNoticeRecordMapper;
    @Autowired
    private TimerService timerService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private ContractConfig contractConfig;


    public void transactionNoticeListen() {
        List<TransfNoticeRecord> notice = getNotice();
        if (!CollectionUtils.isEmpty(notice)){
            for (TransfNoticeRecord request : notice){
                String address = request.getAddress();
                String updateBalanceLock = RedisKeyConstants.UPDATE_ADDRESS_BALANCE_LOCK + address;
                redisLockUtils.lock(updateBalanceLock);
                try {
                    timerService.handlingNoticeRecord(request);
                }catch (Exception e){
                    logger.error("", e);
                }
                redisLockUtils.releaseLock(updateBalanceLock);
            }
        }
    }


    private List<TransfNoticeRecord> getNotice(){
        TransfNoticeRecordExample example = new TransfNoticeRecordExample();
        TransfNoticeRecordExample.Criteria criteria = example.createCriteria();
        criteria.andNoticeSuccessEqualTo(0).andSystemTypeEqualTo(contractConfig.type);
        List<TransfNoticeRecord> transfNoticeRecords = transfNoticeRecordMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(transfNoticeRecords)){
            List<String> addrs = new ArrayList<>();
            transfNoticeRecords.forEach(record -> {
                addrs.add(record.getAddress());
            });
            Map<String, Collection<String>> query = new HashMap<>();
            query.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, addrs);
            List<JSONObject> selfAddrs = mongoUtil.getDataByListQuery(query, JSONObject.class, MongoConstant.ADDRESS_CLASS_NAME);
            Set<String> set = new HashSet<>();
            selfAddrs.forEach(addr -> {
                try {
                    set.add(addr.getString(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS));
                } catch (Exception e) {
                    logger.error("", e);
                }
            });
            transfNoticeRecords.forEach((record -> {
                if (set.contains(record.getAddress())){
                    record.setSelfAddr(true);
                }
            }));
        }
        return transfNoticeRecords;
    }

    /**
     * 判断地址金额变更记录是否存在
     * @param noticeRecordId
     * @return
     */
    private boolean notExistUpdateBalanceRecord(String noticeRecordId){
        TransfNoticeRecord transfNoticeRecord = transfNoticeRecordMapper.selectByPrimaryKey(noticeRecordId);
        return transfNoticeRecord == null;
    }

}
