package com.zatgo.zup.transaction.basic.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.transaction.basic.entity.account.TransfNoticeRecord;
import com.zatgo.zup.transaction.basic.entity.collect.TransfCollectRecord;
import com.zatgo.zup.transaction.basic.entity.collect.TransfCollectRecordExample;
import com.zatgo.zup.transaction.basic.entity.constant.Constants;
import com.zatgo.zup.transaction.basic.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.basic.mapper.TransfCollectRecordMapper;
import com.zatgo.zup.transaction.basic.mapper.TransfNoticeRecordMapper;
import com.zatgo.zup.transaction.basic.remoteservice.WalletRemotService;
import com.zatgo.zup.transaction.basic.service.AccountService;
import com.zatgo.zup.transaction.basic.service.TimerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 46041 on 2019/1/23.
 */

@Service("timerService")
public class TimerServiceImpl implements TimerService {

    private static final Logger logger = LoggerFactory.getLogger(TimerServiceImpl.class);

    @Autowired
    private TransfNoticeRecordMapper transfNoticeRecordMapper;
    @Autowired
    private AccountService accountService;
    @Autowired
    private WalletRemotService walletRemotService;
    @Autowired
    private TransfCollectRecordMapper transfCollectRecordMapper;




    @Override
    @Transactional
    public void handlingNoticeRecord(TransfNoticeRecord record) {
        Integer isUsed = record.getIsUsed();
        String address = record.getAddress();
        String coinNetworkType = record.getCoinNetworkType();
        String coinType = record.getCoinType();
        BigDecimal amount = record.getAmount();
        String noticeId = record.getNoticeId();
        String txId = record.getTxId();
        BigDecimal realFee = record.getRealFee();
        int style = record.getStyle().intValue();
        //未被处理过的通知,先修改余额
        if (0 == isUsed.intValue()){
            boolean selfAddr = record.isSelfAddr();
            //修改状态
            TransfNoticeRecord transfNoticeRecord = new TransfNoticeRecord();
            transfNoticeRecord.setIsUsed(1);
            transfNoticeRecord.setNoticeId(noticeId);
            if (!selfAddr){
                transfNoticeRecord.setNoticeSuccess(1);
            }
            transfNoticeRecordMapper.updateByPrimaryKeySelective(transfNoticeRecord);
            //不是自己的地址直接return
            if (!selfAddr){
                return;
            }
            //1加钱
            if (1 == style){
                accountService.updateAddressConfirmed(coinNetworkType, coinType, amount, address, txId);
            }
        }
        //如果是收集，直接将消息标记为通知成功
        if (record.getType().intValue() == TransactionTypeEnum.COLLECT.getCode().intValue()
                || record.getType().intValue() == TransactionTypeEnum.RETURN.getCode().intValue()
                || record.getType().intValue() == TransactionTypeEnum.FEE.getCode().intValue()){
            TransfCollectRecordExample example = new TransfCollectRecordExample();
            example.createCriteria().andTxIdEqualTo(record.getTxId());
            TransfCollectRecord transfCollectRecord = new TransfCollectRecord();
            transfCollectRecord.setIsDone(true);
            transfCollectRecordMapper.updateByExampleSelective(transfCollectRecord, example);
            useNoticeRecord(noticeId);
            return;
        }
        //通知钱包
        Map<String, ?> res = null;
        Map<String, String> map = new HashMap();
        try {
            //如果是充值
            if (record.getType().intValue() == TransactionTypeEnum.DEPOSIT.getCode().intValue()){
                map.put("timestamp", record.getCreateTime().getTime() + "");
                map.put("symbol", record.getCoinType());
                map.put("txid", record.getTxId());
                map.put("amount", record.getAmount().toPlainString());
                map.put("address_to", record.getAddress());
                map.put("confirm", record.getConfirm() + "");
                map.put("is_mining", "1");
                res = walletRemotService.depositNotify(map);
                logger.info("钱包返回信息=====" + JSONObject.toJSONString(res));
                if (res != null && Constants.CODE_SUCCESS.equals(res.get("errno"))){
                    useNoticeRecord(noticeId);
                }
                return;
            }
            //如果是提币
            if (record.getType().intValue() == TransactionTypeEnum.EXTRACT.getCode().intValue()){
                map.put("trans_id", record.getTransId());
                map.put("symbol", record.getCoinType());
                map.put("address_to", record.getAddress());
                map.put("txid", record.getTxId());
                map.put("amount", record.getAmount().toPlainString());
                map.put("confirm", record.getConfirm() + "");
                map.put("real_fee", realFee.toPlainString());
                res = walletRemotService.withdrawNotify(map);
                logger.info("钱包返回信息=====" + JSONObject.toJSONString(res));
                if (res != null && Constants.CODE_SUCCESS.equals(res.get("errno"))){
                    useNoticeRecord(noticeId);
                }
            }
        } catch (Exception e){
            logger.error("", e);
        }

    }


    private void useNoticeRecord(String recordId){
        TransfNoticeRecord record = new TransfNoticeRecord();
        record.setNoticeId(recordId);
        record.setNoticeSuccess(1);
        transfNoticeRecordMapper.updateByPrimaryKeySelective(record);
    }


}
