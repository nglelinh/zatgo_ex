package com.zatgo.zup.transaction.basic.mapper;

import com.zatgo.zup.transaction.basic.entity.account.TransfConfig;
import org.springframework.stereotype.Repository;

@Repository
public interface TransfConfigMapper extends BaseMapper<TransfConfig>{
}