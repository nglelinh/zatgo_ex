package com.zatgo.zup.transaction.basic.entity.account;

import java.math.BigDecimal;
import java.util.Date;

public class TransfBalanceChangeRecord {
    private String id;

    private String address;

    private String coinNetworkType;

    private String coinType;

    private BigDecimal amount;

    private Date createTime;

    private Integer type;

    private String txId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType == null ? null : coinNetworkType.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}