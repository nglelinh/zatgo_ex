package com.zatgo.zup.transaction.basic.service.impl;

import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.transaction.basic.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.basic.entity.usdt.Address;
import com.zatgo.zup.transaction.basic.entity.usdt.TransactionRecord;
import com.zatgo.zup.transaction.basic.entity.usdt.UTXO;
import com.zatgo.zup.transaction.basic.service.ThreadPoolTaskService;
import com.zatgo.zup.transaction.basic.util.MongoUtil;
import com.zatgo.zup.transaction.basic.util.usdt.UsdtUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.Future;

/**
 * Created by 46041 on 2018/10/24.
 */

@Service("usdtThreadPoolTaskService")
public class UsdtThreadPoolTaskService implements ThreadPoolTaskService {
    private static final Logger logger = LoggerFactory.getLogger(UsdtThreadPoolTaskService.class);

    @Autowired
    private UsdtUtil usdtUtil;
    @Autowired
    private MongoUtil mongoUtil;
    @Value("${transaction.confirmed:}")
    private Integer confirmed;

    @Override
    @Async("threadPoolTask")
    public Future<ResponseData<Object>> tx(String txId, Integer maxValue){
        ResponseData<Object> result = null;
        try {
            TransactionRecord transaction = usdtUtil.getTransaction(txId, Integer.valueOf(maxValue + ""));
            //获取所有utxo
            List<UTXO> unSpentUTXO = usdtUtil.getUnSpentUTXO(transaction);
            //保存自己的utxo，返回是否保存成功
            saveDepositUTXOInfo(unSpentUTXO, transaction);
            result = BusinessResponseFactory.createSuccess(null);
        } catch (Exception e){
            logger.error("", e);
            result = BusinessResponseFactory.createBusinessError("-1", txId);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return new AsyncResult(result);
    }


    private void saveDepositUTXOInfo(List<UTXO> utxos, TransactionRecord transaction){
        if (CollectionUtils.isEmpty(utxos))
            return;
        Map<String, Collection<String>> query = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (UTXO utxo : utxos){
            String address = utxo.getAddress();
            if (address != null){
                list.add(address);
            }
        }
        query.put(MongoConstant.USDT_ADDRESS_CLASS_NAME_ADDRESS, list);
        //查询是否为自己的地址
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.USDT_ADDRESS_CLASS_NAME);
        if (!CollectionUtils.isEmpty(addresses)){
            List<UTXO> utxoList = new ArrayList<>();
            int i = utxos.size() - 1;
            for ( ; i >= 0; i--){
                UTXO utxo = utxos.get(i);
                String utxoAddress = utxo.getAddress();
                if (!hasAddress(addresses, utxoAddress)){
                    continue;
                }
                Query utxoQuery = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.USDT_UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
                criteria.and(MongoConstant.USDT_UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
                utxoQuery.addCriteria(criteria);
                List<UTXO> data = mongoUtil.getData(utxoQuery, UTXO.class, MongoConstant.USDT_UTXO_CLASS_NAME);
                //防止重复添加utxo
                if (CollectionUtils.isEmpty(data)){
                    utxoList.add(utxo);
                }
            }
            mongoUtil.batchExecuteInsert(utxoList, MongoConstant.USDT_UTXO_CLASS_NAME);
            if (getTransactionRecord(transaction.getTxId()) == null){
                mongoUtil.insert(transaction, MongoConstant.USDT_TRANSACTION_CLASS);
            }
        }
    }

    private boolean hasAddress(List<Address> addresses, String find){
        for (Address address : addresses){
            if (address.getAddress().equals(find)){
                return true;
            }
        }
        return false;
    }

    private TransactionRecord getTransactionRecord(String txId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.USDT_TRANSACTION_CLASS_TXID).is(txId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.USDT_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }
}
