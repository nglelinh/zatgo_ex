//package com.zatgo.zup.transaction.basic.conf;
//
//import com.alibaba.fastjson.JSONObject;
//import com.zatgo.zup.common.exception.BusinessException;
//import com.zatgo.zup.common.exception.BusinessExceptionCode;
//import com.zatgo.zup.transaction.basic.entity.eth.AddressTypeEnum;
//import com.zatgo.zup.transaction.basic.entity.constant.CoinConstant;
//import com.zatgo.zup.transaction.basic.entity.eth.Address;
//import com.zatgo.zup.transaction.basic.util.MongoUtil;
//import com.zatgo.zup.transaction.basic.util.eth.EthUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.stereotype.Component;
//import org.springframework.util.CollectionUtils;
//
//import javax.annotation.PostConstruct;
//import java.math.BigDecimal;
//import java.math.BigInteger;
//import java.util.*;
//
///**
// * Created by 46041 on 2018/10/25.
// */
//
//@Component
//@ConfigurationProperties(prefix="transaction.eth")
//public class EthAndContractConfig {
//
//    private List<Map<String, String>> config = new ArrayList<>();
//
//    private static Map<String, ContractConfig> contractConfig = null;
//
//    private static Map<String, ContractConfig> coinTypeConfig = null;
//    @Autowired
//    private EthUtil ethUtil;
//    @Autowired
//    private MongoUtil mongoUtil;
//
//
//    @Value("${transaction.eth.confirmed:}")
//    private Integer ethConfirmed;
//    @Value("${transaction.eth.gasprice:}")
//    private BigInteger gasprice;
//    @Value("${transaction.eth.collectlimit:}")
//    private BigDecimal collectlimit;
//    @Value("${transaction.type:}")
//    private String type;
//
//    public Map<String, ContractConfig> getContractConfig(){
//        if (contractConfig == null){
//            init();
//        }
//        return contractConfig;
//    }
//
//    public Map<String, ContractConfig> getCoinTypeConfig(){
//        if (coinTypeConfig == null){
//            init();
//        }
//        return coinTypeConfig;
//    }
//
//    @PostConstruct
//    private void init(){
//        if ("eth".equalsIgnoreCase(type)){
//            if (contractConfig == null){
//                if (!CollectionUtils.isEmpty(config)){
//                    contractConfig = new HashMap<>();
//                    coinTypeConfig = new HashMap<>();
//                    for (Map<String, String> conf : config){
//                        ContractConfig config = JSONObject.parseObject(JSONObject.toJSONString(conf), ContractConfig.class) ;
//                        config.setName(config.getName().toUpperCase());
//                        config.setContract("0x" + new BigInteger(config.getContract()).toString(16));
//                        contractConfig.put(config.getContract(), config);
//                        coinTypeConfig.put(config.getName(), config);
//                    }
//                }
//            }
//            putEthConfig();
//        }
//    }
//
//    private void putEthConfig(){
//        ContractConfig config = new ContractConfig();
//        config.setName("ETH");
//        config.setConfirmed(ethConfirmed);
//        config.setPrecision(CoinConstant.ETH_PRECISION);
//        contractConfig.put(config.getContract(), config);
//        coinTypeConfig.put(config.getName(), config);
//    }
//
//    private Address createTokenAddress(String coinType){
//        Address address = new Address();
//        Address eth = ethUtil.getSystemAddress("ETH");
//        if (eth == null){
//            List<String> addresses = ethUtil.getAddress(2);
//            if (!CollectionUtils.isEmpty(addresses)){
//                List<Address> list = new ArrayList<>(2);
//                eth = new Address();
//                eth.setCreateDate(new Date());
//                eth.setAddress(addresses.get(0));
//                eth.setType(0);
//                eth.setUnconfirmed(BigDecimal.ZERO.doubleValue());
//                eth.setConfirmed(BigDecimal.ZERO.doubleValue());
//                list.add(eth);
//                Address collectAddress = new Address();
//                collectAddress.setAddress(addresses.get(1));
//                collectAddress.setType(2);
//                collectAddress.setCreateDate(new Date());
//                collectAddress.setConfirmed(BigDecimal.ZERO.doubleValue());
//                collectAddress.setUnconfirmed(BigDecimal.ZERO.doubleValue());
//                list.add(collectAddress);
//                mongoUtil.insert(list, ethUtil.getTokenAddressName("ETH"));
//            } else {
//                throw new BusinessException(BusinessExceptionCode.GET_ADDRESS_FAIL);
//            }
//        }
//        address.setAddress(eth.getAddress());
//        address.setType(AddressTypeEnum.SYSTEM_ADDRESS.getCode());
//        address.setCreateDate(new Date());
//        address.setConfirmed(BigDecimal.ZERO.doubleValue());
//        address.setUnconfirmed(BigDecimal.ZERO.doubleValue());
//        mongoUtil.insert(address, ethUtil.getTokenAddressName(coinType));
//        return eth;
//    }
//
//    public List<Map<String, String>> getConfig() {
//        return config;
//    }
//
//    public void setConfig(List<Map<String, String>> config) {
//        this.config = config;
//    }
//}
