package com.zatgo.zup.transaction.basic.conf;

import com.zatgo.zup.transaction.basic.entity.account.TransfConfig;
import com.zatgo.zup.transaction.basic.entity.account.TransfConfigExample;
import com.zatgo.zup.transaction.basic.mapper.TransfConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 46041 on 2018/10/25.
 */

@Component
public class ContractConfig {

    @Autowired
    private TransfConfigMapper transfConfigMapper;


    @Value("${transaction.height}")
    public Integer height;
    @Value("${transaction.type}")
    public String type;
    @Value("${transaction.addressNum}")
    public Integer addressNum;



    private static Map<String, TransfConfig> coinTypeConfig = null;

    private static Map<String, TransfConfig> contractConfig = null;


    public Map<String, TransfConfig> getCoinTypeConfig(){
        if (coinTypeConfig == null || coinTypeConfig.isEmpty()){
            init();
        }
        return coinTypeConfig;
    }

    public Map<String, TransfConfig> getContractConfig(){
        if (contractConfig == null || contractConfig.isEmpty()){
            init();
        }
        return contractConfig;
    }

    public TransfConfig syncGetConfig(String netWorkType, String coinType){
        TransfConfigExample example = new TransfConfigExample();
        TransfConfigExample.Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(netWorkType)){
            criteria.andCoinNetworkTypeEqualTo(netWorkType.toUpperCase());
        }
        if (!StringUtils.isEmpty(coinType)){
            criteria.andCoinTypeEqualTo(coinType.toUpperCase());
        }
        List<TransfConfig> transfConfigs = transfConfigMapper.selectByExample(example);
        return CollectionUtils.isEmpty(transfConfigs) ? null : transfConfigs.get(0);
    }

    public List<TransfConfig> getCollectList(){
        Map<String, TransfConfig> contractConfig = getContractConfig();
        List<TransfConfig> list = new ArrayList<>();
        contractConfig.forEach((k, v) -> {
            if (v.getIsCollect()){
                list.add(v);
            }
        });
        return list;
    }

    private void init(){
        coinTypeConfig = new HashMap<>();
        contractConfig = new HashMap<>();
        TransfConfigExample example = new TransfConfigExample();
        example.createCriteria().andCoinNetworkTypeEqualTo(type.toUpperCase());
        List<TransfConfig> transfConfigs = transfConfigMapper.selectByExample(example);
        transfConfigs.forEach(transfConfig -> {
            String coinType = transfConfig.getCoinType();
            String contract = transfConfig.getContract();
            coinTypeConfig.put(coinType, transfConfig);
            contractConfig.put(contract, transfConfig);
        });
    }

}
