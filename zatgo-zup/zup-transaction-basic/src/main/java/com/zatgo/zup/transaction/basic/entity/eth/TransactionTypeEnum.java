package com.zatgo.zup.transaction.basic.entity.eth;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 46041 on 2018/10/29.
 */
public enum  TransactionTypeEnum {
    // 0提币 1充值 2收集 3 qtum代币的手续费退回
    EXTRACT(0), DEPOSIT(1), COLLECT(2), RETURN(3), FEE(4);

    private Integer code;

    private static final Map<Integer, TransactionTypeEnum> stringToEnum = new HashMap<Integer, TransactionTypeEnum>();
    static {
        for (TransactionTypeEnum enumType : values()) {
            stringToEnum.put(enumType.getCode(), enumType);
        }
    }

    TransactionTypeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
