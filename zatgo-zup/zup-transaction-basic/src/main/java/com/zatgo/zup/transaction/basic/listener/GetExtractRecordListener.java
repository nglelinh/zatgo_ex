package com.zatgo.zup.transaction.basic.listener;

import com.zatgo.zup.transaction.basic.service.CoinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 46041 on 2018/10/22.
 */
public class GetExtractRecordListener implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(GetExtractRecordListener.class);

    private CoinService coinService;


    @Override
    public void run() {
        while (true){

            try {
                coinService.GetBtcExtractRecord();
                Thread.sleep(10 * 1000l);
            } catch (Exception e) {
                logger.error("", e);
                try {
                    Thread.sleep(10 * 1000l);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public GetExtractRecordListener(CoinService coinService) {
        this.coinService = coinService;
    }
}
