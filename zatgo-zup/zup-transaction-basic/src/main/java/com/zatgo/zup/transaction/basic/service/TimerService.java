package com.zatgo.zup.transaction.basic.service;

import com.zatgo.zup.transaction.basic.entity.account.TransfNoticeRecord;

/**
 * Created by 46041 on 2019/1/23.
 */
public interface TimerService {


    /**
     *  处理通知信息
     */
    void handlingNoticeRecord(TransfNoticeRecord record);
}
