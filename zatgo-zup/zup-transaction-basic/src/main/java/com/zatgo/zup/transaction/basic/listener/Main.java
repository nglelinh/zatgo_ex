package com.zatgo.zup.transaction.basic.listener;

import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.transaction.basic.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.basic.proxy.TimerProxy;
import com.zatgo.zup.transaction.basic.service.CoinService;
import com.zatgo.zup.transaction.basic.service.CollectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * Created by 46041 on 2018/10/19.
 */

@Component
public class Main {

    private final Logger logger = LoggerFactory.getLogger(Main.class);

    private CoinService coinService;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private ConfigurableApplicationContext applicationContext;
    @Autowired
    private TimerProxy timerProxy;
    @Autowired
    private CollectService collectService;


    @Value("${transaction.type}")
    private String type;
    @Value("${password}")
    private String password;


    @PostConstruct
    public void init() {
        if (StringUtils.isEmpty(password)){
            logger.error("================未输入密码，无法启动===================");
            Runtime.getRuntime().halt(-1);
        }
        CoinConstant.SIGN_PASSWORD = password;
        String transactionLiveKey = RedisKeyConstants.TRANSACTION_LIVE_KEY + type;
        int count = 0;
        while (true){
            if (redisUtils.hasKey(transactionLiveKey)){
                count++;
            } else {
                break;
            }
            if (count >= 2){
                logger.error("================检测到同币种的交易机已启动，强制停止当前启动===================");
                Runtime.getRuntime().halt(-1);
            }
            try {
                Thread.sleep(5 * 1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //装载配置
        coinService = (CoinService) applicationContext.getBean(type + "Service");
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                logger.info("=============================开始启动扫描区块线程===================================");
                ScanBlockListener scanBlockListener = new ScanBlockListener(coinService);
                Thread scanBlock = new Thread(scanBlockListener);
                scanBlock.setDaemon(true);
                scanBlock.start();
                Daemon scanBlockDaemon = new Daemon(scanBlockListener, scanBlock);
                Thread sbDaemon = new Thread(scanBlockDaemon);
                sbDaemon.setDaemon(true);
                sbDaemon.start();
                logger.info("=============================启动扫描区块线程成功===================================");



                logger.info("=============================开始启动确认区块线程===================================");
                CheckTransactionListener checkTransactionListener = new CheckTransactionListener(coinService);
                Thread checkTransaction = new Thread(checkTransactionListener);
                checkTransaction.setDaemon(true);
                checkTransaction.start();
                Daemon checkTransactionDaemon = new Daemon(checkTransactionListener, checkTransaction);
                Thread ctDaemon = new Thread(checkTransactionDaemon);
                ctDaemon.setDaemon(true);
                ctDaemon.start();
                logger.info("=============================启动确认区块线程成功===================================");


                logger.info("=============================开始启动推送信息===================================");
                PushMessageListener pushMessageListener = new PushMessageListener(timerProxy);
                Thread pushMessage = new Thread(pushMessageListener);
                pushMessage.setDaemon(true);
                pushMessage.start();
                logger.info("=============================开始启动推送信息===================================");

                logger.info("=============================开始启动推送信息===================================");
                CollectListener collectListener = new CollectListener(collectService);
                Thread collect = new Thread(collectListener);
                collect.setDaemon(true);
                collect.start();
                logger.info("=============================开始启动推送信息===================================");
//////
//////
//////
                logger.info("=============================开始启动获取提币交易线程===================================");
                GetExtractRecordListener getBtcExtractRecordListener = new GetExtractRecordListener(coinService);
                Thread getBtcExtractRecordThread = new Thread(getBtcExtractRecordListener);
                getBtcExtractRecordThread.setDaemon(true);
                getBtcExtractRecordThread.start();
                Daemon getBtcExtractRecordDaemon = new Daemon(getBtcExtractRecordListener, getBtcExtractRecordThread);
                Thread gbeDaemon = new Thread(getBtcExtractRecordDaemon);
                gbeDaemon.setDaemon(true);
                gbeDaemon.start();
                logger.info("=============================启动获取提币交易线程成功===================================");
//
//
//
//

                while(true){
                    try {
                        redisUtils.put(transactionLiveKey, "1", 7l, TimeUnit.SECONDS);
                        Thread.sleep(5 * 1000l);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        Thread mainThread = new Thread(runnable);
        mainThread.start();
    }
}
