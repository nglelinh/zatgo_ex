package com.zatgo.zup.transaction.basic.entity.account;

import java.math.BigDecimal;

public class TransfConfig {
    private String id;

    private String coinNetworkType;

    private String coinType;

    private BigDecimal fee;

    private BigDecimal gasPrice;

    private BigDecimal gasLimit;

    private BigDecimal collectLimit;

    private Boolean isCollect;

    private String feeAddress;

    private String collectReceiveAddress;

    private Integer precision;

    private Integer confirmed;

    private String contract;

    private String systemSendAddress;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType == null ? null : coinNetworkType.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(BigDecimal gasPrice) {
        this.gasPrice = gasPrice;
    }

    public BigDecimal getGasLimit() {
        return gasLimit;
    }

    public void setGasLimit(BigDecimal gasLimit) {
        this.gasLimit = gasLimit;
    }

    public BigDecimal getCollectLimit() {
        return collectLimit;
    }

    public void setCollectLimit(BigDecimal collectLimit) {
        this.collectLimit = collectLimit;
    }

    public Boolean getIsCollect() {
        return isCollect;
    }

    public void setIsCollect(Boolean isCollect) {
        this.isCollect = isCollect;
    }

    public String getFeeAddress() {
        return feeAddress;
    }

    public void setFeeAddress(String feeAddress) {
        this.feeAddress = feeAddress == null ? null : feeAddress.trim();
    }

    public String getCollectReceiveAddress() {
        return collectReceiveAddress;
    }

    public void setCollectReceiveAddress(String collectReceiveAddress) {
        this.collectReceiveAddress = collectReceiveAddress == null ? null : collectReceiveAddress.trim();
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract == null ? null : contract.trim();
    }

    public String getSystemSendAddress() {
        return systemSendAddress;
    }

    public void setSystemSendAddress(String systemSendAddress) {
        this.systemSendAddress = systemSendAddress;
    }
}