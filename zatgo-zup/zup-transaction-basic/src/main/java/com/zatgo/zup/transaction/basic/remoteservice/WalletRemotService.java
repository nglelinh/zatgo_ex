package com.zatgo.zup.transaction.basic.remoteservice;

import com.alibaba.fastjson.JSONArray;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by 46041 on 2018/10/22.
 */

@FeignClient("zup-wallet")
public interface WalletRemotService {

    @RequestMapping(value = "/wallet/withdrawConsume", method = RequestMethod.POST)
    @ResponseBody
    Map<String, ? extends Object> withdrawConsume(@RequestBody Map<String, String> paramsMap);

    @RequestMapping(value = "/wallet/address/add", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<String> addAddresses(@RequestBody JSONArray array);

    @RequestMapping(value = "/wallet/address/remainderAddress", method = RequestMethod.POST)
    @ResponseBody
    ResponseData<Integer> remainderAddress(@RequestParam("type") String type);


    @RequestMapping(value = "/wallet/depositNotify", method = RequestMethod.POST)
    @ResponseBody
    Map<String, ? extends Object> depositNotify(@RequestBody Map<String, String> paramsMap);


    @RequestMapping(value = "/wallet/withdrawNotify", method = RequestMethod.POST)
    @ResponseBody
    Map<String, ? extends Object> withdrawNotify(@RequestBody Map<String, String> paramsMap);


}
