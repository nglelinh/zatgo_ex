package com.zatgo.zup.transaction.basic.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.encrypt.MD5Util;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CreateAddressRequest;
import com.zatgo.zup.common.model.EthContractSignParams;
import com.zatgo.zup.common.model.EthSignParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.transaction.basic.conf.ContractConfig;
import com.zatgo.zup.transaction.basic.entity.SendRequest;
import com.zatgo.zup.transaction.basic.entity.account.TransfBalanceChangeRecord;
import com.zatgo.zup.transaction.basic.entity.account.TransfBalanceChangeRecordExample;
import com.zatgo.zup.transaction.basic.entity.account.TransfConfig;
import com.zatgo.zup.transaction.basic.entity.account.TransfNoticeRecord;
import com.zatgo.zup.transaction.basic.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.basic.entity.constant.Constants;
import com.zatgo.zup.transaction.basic.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.basic.entity.eth.Address;
import com.zatgo.zup.transaction.basic.entity.eth.MainChain;
import com.zatgo.zup.transaction.basic.entity.eth.TransactionRecord;
import com.zatgo.zup.transaction.basic.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.basic.mapper.TransfBalanceChangeRecordMapper;
import com.zatgo.zup.transaction.basic.mapper.TransfNoticeRecordMapper;
import com.zatgo.zup.transaction.basic.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.basic.remoteservice.WalletRemotService;
import com.zatgo.zup.transaction.basic.service.AccountService;
import com.zatgo.zup.transaction.basic.service.CoinService;
import com.zatgo.zup.transaction.basic.util.MongoUtil;
import com.zatgo.zup.transaction.basic.util.eth.EthUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.web3j.protocol.core.methods.response.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.*;

/**
 * Created by 46041 on 2018/10/24.
 */

@Service("ethService")
public class EthService implements CoinService {

    private static final Logger logger = LoggerFactory.getLogger(EthService.class);

    @Autowired
    private EthUtil ethUtil;
    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private WalletRemotService walletRemotService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private SignRemotService signRemotService;
    @Autowired
    private TransfNoticeRecordMapper transfNoticeRecordMapper;
    @Autowired
    private ContractConfig contractConfig;
    @Autowired
    private AccountService accountService;
    @Autowired
    private TransfBalanceChangeRecordMapper transfBalanceChangeRecordMapper;

    @Override
    @Transactional
    public String sendTransaction(SendRequest sendRequest) {
        String txId = null;
        BigDecimal amount = sendRequest.getAmount();
        String businessTxId = sendRequest.getBusinessTxId();
        String coinType = sendRequest.getCoinType();
        String fromAddress = sendRequest.getFromAddress();
        String toAddress = sendRequest.getToAddress();
        BigDecimal gasLimit = sendRequest.getGasLimit();
        BigDecimal gasPrice = sendRequest.getGasPrice();
        Integer transfType = sendRequest.getTransfType();
        if (amount.compareTo(BigDecimal.ZERO) == -1){
            throw new BusinessException(BusinessExceptionCode.EXTRACT_MONEY_ERROR);
        }
        EthSendTransaction tx = null;
        TransactionRecord record = new TransactionRecord();
        String extractLock = RedisKeyConstants.TRANSACTION_EXTRACT_LOCK + fromAddress;
        redisLockUtils.lock(extractLock);
        try {
            if (StringUtils.isEmpty(businessTxId)){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BUSINESS_TXID_NOT_IS_EMPTY);
            }
            if (hasBusinessTxId(businessTxId) == null) {
                Address address = getAddress(fromAddress, coinType);
                if (address == null){
                    logger.error("转账失败的业务Id：" + businessTxId);
                    throw new BusinessException(BusinessExceptionCode.EXTRACT_FROM_ADDRESS_NOT_EXIST);
                }
                TransfConfig transfConfig = contractConfig.syncGetConfig(contractConfig.type, coinType);
                if (gasLimit == null)
                    gasLimit = transfConfig.getGasLimit();
                if (gasPrice == null)
                    gasPrice = transfConfig.getGasPrice();
                //判断地址是否被使用了，如果被使用需要排队
                if (canExtract(address.getAddress(), transfConfig)) {
                    BigInteger nonce = ethUtil.getNonce(fromAddress);
                    if (nonce == null) {
                        logger.error("转账失败的业务Id：" + businessTxId);
                        throw new BusinessException(BusinessExceptionCode.BUSINESS_GET_NONCE_FAIL);
                    }
                    BigDecimal fee = ethUtil.ethAmountFormate9(gasPrice.multiply(gasLimit)).negate();
                    accountService.updateBalanceExtract("ETH", coinType, amount.negate(), fee, fromAddress, businessTxId);
                    //签名
                    ResponseData<String> sign = null;
                    if ("ETH".equalsIgnoreCase(coinType)) {
                        EthSignParams params = new EthSignParams();
                        params.setAmount(amount);
                        params.setToAddress(toAddress);
                        params.setFromAddress(fromAddress);
                        params.setmNonce(nonce);
                        params.setPassword(CoinConstant.SIGN_PASSWORD);
                        params.setGasLimit(gasLimit.toBigInteger());
                        params.setGasPrice(ethUtil.ethAmountFormateSumN(gasPrice, new BigInteger("9")).toBigInteger());
                        sign = signRemotService.ethSign(params);
                        record.setGasLimit(gasLimit);
                        record.setGasPrice(gasPrice);
                    } else {
                        EthContractSignParams params = new EthContractSignParams();
                        params.setGasLimit(gasLimit.toBigInteger());
                        params.setGasPrice(ethUtil.ethAmountFormateSumN(gasPrice, new BigInteger("9")).toBigInteger());
                        params.setPassword(CoinConstant.SIGN_PASSWORD);
                        params.setmNonce(nonce);
                        params.setAmount(amount);
                        params.setContractAddress(transfConfig.getContract());
                        params.setFromAddress(fromAddress);
                        params.setToAddress(toAddress);
                        params.setDecimal(new BigInteger(transfConfig.getPrecision() + ""));
                        sign = signRemotService.ethSign(params);
                        record.setContractAddress(transfConfig.getContract());
                        record.setGasLimit(gasLimit);
                        record.setGasPrice(gasPrice);
                    }
                    if (!sign.isSuccessful()) {
                        throw new BusinessException(BusinessExceptionCode.SIGN_ERROR);
                    }
                    String signData = sign.getData();
                    record.setToAddress(toAddress);
                    record.setFromAddress(fromAddress);
                    record.setAmount(amount);
                    record.setConfirm(0);
                    record.setCoinType(coinType);
                    record.setType(transfType);
                    record.setRecordId(businessTxId);
                    record.setCreateDate(new Date());
                    mongoUtil.insert(record, MongoConstant.ETH_TRANSACTION_CLASS);
                    tx = ethUtil.sendTransaction(signData);
                    txId = tx.getTransactionHash();
                    if (txId == null) {
                        throw new BusinessException(BusinessExceptionCode.ORDER_SEND_FAIL);
                    }
                    updateTxId(txId, businessTxId);
                }
            }
        } catch (Exception e) {
            if (tx != null){
                logger.error(JSONObject.toJSONString(tx.getError()));
                logger.error(JSONObject.toJSONString(tx));
            }
            logger.error("", e);
            logger.error(JSONObject.toJSONString(record));
        } finally {
            redisLockUtils.releaseLock(extractLock);
        }
        return txId;
    }

    @Override
    public void scanBlock() throws IOException {
        //获取数据库中区块高度
        Integer maxValue = mongoUtil.getMaxValue("height", MongoConstant.MAIN_CHAIN_CLASS_NAME) + 1;
        maxValue = ethUtil.getMax(contractConfig.height, maxValue);
        BigInteger blockCount = ethUtil.getBlockCount();
        //如果已经扫描到最高的区块了，休息10秒继续扫描
        if (maxValue - 1 >= blockCount.intValue() - 6){
            try {
                logger.info("睡了10秒");
                Thread.sleep(10 * 1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return;
        }
        try {
            EthBlock block = ethUtil.getBlockTx(new BigInteger(maxValue + ""));
            EthBlock.Block blockInfo = block.getBlock();
            if (blockInfo != null){
                BigInteger number = blockInfo.getNumber();
                List<EthBlock.TransactionResult> blockTx = blockInfo.getTransactions();
                //获取其中自己的交易
                List<TransactionRecord> selfTransactionRecord = getSelfTransactionRecord(blockTx, number);
                if (!CollectionUtils.isEmpty(selfTransactionRecord)){
                    for (TransactionRecord record : selfTransactionRecord){
                        saveTransactionRecord(record);
                    }
                }
                MainChain mainChain = new MainChain();
                mainChain.setHeight(maxValue);
                mainChain.setHash(blockInfo.getHash());
                mongoUtil.insert(mainChain, MongoConstant.MAIN_CHAIN_CLASS_NAME);
            } else {
                logger.error(JSONObject.toJSONString(block.getError()));
            }
        } catch (Exception e) {
            logger.error("", e);
        }

    }

    @Override
    public void checkTransaction() throws IOException {
        List<TransactionRecord> unConfirmRecord = getUnConfirmRecord();
        if (!CollectionUtils.isEmpty(unConfirmRecord)){
            BigInteger blockCount = ethUtil.getBlockCount();
            unConfirmRecord.forEach(record -> {
                try {
                    EthTransaction transactionByHash = ethUtil.getTransactionByHash(record.getTxId());
                    if (transactionByHash != null){
                        updateTransaction(transactionByHash, record, blockCount);
                    }
                } catch (Exception e){
                    logger.error("", e);
                }
            });
        }
    }

    @Override
    public void GetBtcExtractRecord() {
        Map<String, TransfConfig> coinTypeConfig = contractConfig.getCoinTypeConfig();
        coinTypeConfig.forEach((k, v) -> {
            Map<String, String> map = new HashMap();
            String coinType = v.getCoinType();
            map.put("symbol", coinType);
            Map<String, ?> stringMap = walletRemotService.withdrawConsume(map);
            if (stringMap != null){
                String code = (String) stringMap.get("errno");
                if (Constants.CODE_SUCCESS.equals(code)){
                    List<Map> withdrawResults = (List<Map>) stringMap.get("data");
                    if (!CollectionUtils.isEmpty(withdrawResults)){
                        for (Map m : withdrawResults){
                            SendRequest request = new SendRequest();
                            request.setFromAddress((String) m.get("from_address"));
                            request.setToAddress((String) m.get("address_to"));
                            request.setAmount(new BigDecimal(m.get("amount") + ""));
                            request.setBusinessTxId(m.get("trans_id") + "");
                            request.setCoinType(v.getCoinType());
                            request.setTransfType(TransactionTypeEnum.EXTRACT.getCode());
                            sendTransaction(request);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void addAddress() {
        if (!CoinConstant.createAddressLockMap.containsKey(CoinConstant.ETH_ADDRESS_LOCK)){
            CoinConstant.createAddressLockMap.put(CoinConstant.ETH_ADDRESS_LOCK, "");
            try {
                CreateAddressRequest request = new CreateAddressRequest();
                List<String> data = new ArrayList<>();
                request.setPassword(CoinConstant.SIGN_PASSWORD);
                request.setNum(100);
                while (true){
                    try{
                        ResponseData<List<String>> newAddresses = signRemotService.getNewEthAddresses(request);
                        data.addAll(newAddresses.getData());
                        if (contractConfig.addressNum <= data.size()){
                            break;
                        }
                    } catch (Exception e){
                        logger.error("", e);
                    }
                }
                List<Address> addresses = new ArrayList<>(data.size());
                JSONArray array = new JSONArray();
                Date date = new Date();
                for (String a : data){
                    JSONObject object = new JSONObject();
                    object.put("address", a);
                    object.put("networkType", "ETH");
                    object.put("isUsed", 0);
                    object.put("create_time", date);
                    array.add(object);
                    Address address = new Address();
                    address.setCreateDate(date);
                    address.setAddress(a);
                    addresses.add(address);
                }
                ResponseData<String> responseData = walletRemotService.addAddresses(array);
                if (responseData.isSuccessful()){
                    mongoUtil.insert(addresses, MongoConstant.ADDRESS_CLASS_NAME);
                }
            } finally {
                CoinConstant.createAddressLockMap.remove(CoinConstant.ETH_ADDRESS_LOCK);
            }
        }
    }

    private boolean canExtract(String address, TransfConfig transfConfig){
        List<Integer> list = new ArrayList<>();
        list.add(TransactionTypeEnum.EXTRACT.getCode());
        list.add(TransactionTypeEnum.COLLECT.getCode());
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_FROMADDRESS).is(address);
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TYPE).in(list);
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_CONFIRM).lt(transfConfig.getConfirmed());
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data);
    }



    private void saveTransactionRecord(TransactionRecord record) throws ParseException {
        //先看看这笔交易是否已经存在，如果存在，不做任何操作
        try{
            Map<String, Object> query = new HashMap();
            query.put(MongoConstant.ETH_TRANSACTION_CLASS_TXID, record.getTxId());
            List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
            if (CollectionUtils.isEmpty(data)){
                mongoUtil.insert(record, MongoConstant.ETH_TRANSACTION_CLASS);
            }
        } catch (Exception e){
            logger.error("", e);
        }
    }

    private TransactionRecord getTransactionRecord(JSONObject transaction) throws IOException {
        if (transaction == null)
            return null;
        EthGetTransactionReceipt transactionReceiptSend = null;
        try{
            TransactionRecord record = new TransactionRecord();
            record.setType(TransactionTypeEnum.DEPOSIT.getCode());
            record.setCreateDate(new Date());
            record.setTxId(transaction.getString("hash"));
            record.setConfirm(0);
            record.setFromAddress(transaction.getString("from"));
            record.setToAddress(transaction.getString("to"));
            record.setGasPrice(ethUtil.ethAmountFormate9(transaction.getBigDecimal("gasPrice")));
            record.setGasLimit(transaction.getBigDecimal("gas"));

            String input = transaction.getString("input");
            String value = transaction.getString("value").replaceFirst("0x", "0");
            //如果是代币，value为0大概率是代币，还需进一步判断
            if (!"0x".equals(input)){
                Map<String, TransfConfig> configMap = contractConfig.getContractConfig();
                String contractAddress = record.getToAddress();
                if (!StringUtils.isEmpty(contractAddress) && configMap.containsKey(contractAddress)) {
                    TransfConfig config = configMap.get(contractAddress);
                    transactionReceiptSend = ethUtil.getTransactionReceipt(record.getTxId());
                    JSONObject transactionReceipt = JSONObject.parseObject(JSONObject.toJSONString(transactionReceiptSend.getTransactionReceipt().get()));
                    JSONArray logs = transactionReceipt.getJSONArray("logs");
                    //没有logs的认为不是erc20规范的，或者就是eth
                    //如果不是erc20规范的，地址不在地址表中，认为是eth也无妨
                    if (!logs.isEmpty()) {
                        JSONObject log = logs.getJSONObject(0);
                        record.setContractAddress(contractAddress);
                        String data = log.getString("data");
                        if ("0x".equals(data)) {
                            record.setAmount(ethUtil.ethAmountFormate18(new BigDecimal(0)));
                        } else {
                            String amount = data.replaceFirst("^0x0+", "");
                            if (StringUtils.isEmpty(amount)) {
                                record.setAmount(ethUtil.ethAmountFormate18(new BigDecimal(0)));
                            } else {
                                try {
                                    record.setAmount(ethUtil.ethAmountFormate18(new BigDecimal(new BigInteger(amount, 16))));
                                } catch (Exception e) {
                                    logger.error("", e);
                                }
                            }
                        }
                        JSONArray topics = log.getJSONArray("topics");
                        record.setTopics(topics);
                        record.setCoinType(config.getCoinType());
                        record.setAmount(ethUtil.ethAmountFormateSumN(record.getAmount(), new BigInteger("18").subtract(new BigInteger(config.getPrecision() + ""))));
                        if (topics != null) {
                            String toAddress = (String) topics.get(2);
                            record.setToAddress("0x" + toAddress.substring(toAddress.length() - 40, toAddress.length()));
                        }
                        return record;
                    }
                }
            }
            record.setAmount(ethUtil.ethAmountFormate18(new BigDecimal(value)));
            record.setContractAddress(null);
            record.setCoinType("ETH");
            return record;
        } catch (Exception e){
            logger.error("", e);
            logger.error(JSONObject.toJSONString(transactionReceiptSend.getError()));
            logger.error(JSONObject.toJSONString(transaction));
        }
        return null;
    }

    private List<TransactionRecord> getSelfTransactionRecord(List<EthBlock.TransactionResult> blockTx, BigInteger number) throws IOException {
        //所有交易
        Map<String, List<TransactionRecord>> map = new HashMap<>();
        //to地址
        Set<String> addresses = new HashSet<>();
        for (EthBlock.TransactionResult transaction : blockTx){
            try {
                JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(transaction.get()));
                TransactionRecord transactionRecord = getTransactionRecord(object);
                if (transactionRecord != null){
                    transactionRecord.setHeight(number);
                    //没有币种的认为不是我们支持的币种
                    //把支持币种的到账地址拿出来查询有没有我们的地址
                    if (!StringUtils.isEmpty(transactionRecord.getCoinType())){
                        String toAddress = transactionRecord.getToAddress();
                        if (map.containsKey(toAddress)){
                            List<TransactionRecord> records = map.get(toAddress);
                            records.add(transactionRecord);
                        } else {
                            ArrayList<TransactionRecord> records = new ArrayList<>();
                            records.add(transactionRecord);
                            map.put(toAddress, records);
                        }
                        addresses.add(toAddress);
                    }
                }
            } catch (Exception e){
                logger.error("", e);
            }
        }
        //在数据库中获取自己的地址
        Map<String, Collection<String>> query = new HashMap<>();
        query.put(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS, addresses);
        List<Address> selfAddress = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        List<TransactionRecord> selfTransactionRecord = new ArrayList<>();
        if (!CollectionUtils.isEmpty(selfAddress)){
            for (Address address : selfAddress){
                selfTransactionRecord.addAll(map.get(address.getAddress()));
            }
        }
        return selfTransactionRecord;
    }

    private List<TransactionRecord> getUnConfirmRecord(){
        Map<String, TransfConfig> configMap = contractConfig.getContractConfig();
        Query query = new Query();
        Criteria criteria = new Criteria();
        List<Criteria> list = new ArrayList();
        configMap.forEach((k, v) -> {
            Criteria cri = Criteria.where(MongoConstant.ETH_TRANSACTION_CLASS_COINTYPE).is(v.getCoinType())
                    .and(MongoConstant.ETH_TRANSACTION_CLASS_CONFIRM).lt(v.getConfirmed());
            list.add(cri);
        });
        if (list.size() > 0){
            Criteria[] criterias = new Criteria[list.size()];
            list.toArray(criterias);
            criteria.orOperator(criterias);
            query.addCriteria(criteria);
            return mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
        }
        return null;
    }



    private void updateTransaction(EthTransaction tx, TransactionRecord record, BigInteger nowHeight) {
        Optional<Transaction> optional = tx.getTransaction();
        if (optional == null){
            String uuid = UUIDUtils.getUuid();
            logger.error(uuid + "：=======检测未到达确认数的交易时，未从链上查到该交易");
            logger.error(uuid + "：=======" + JSONObject.toJSONString(record));
            return;
        }
        Map<String, TransfConfig> configMap = contractConfig.getCoinTypeConfig();
        TransfConfig config = configMap.get(record.getCoinType());
        Transaction transaction = null;
        try {
            transaction = optional.get();
        } catch (Exception e){
            logger.error("交易异常" + JSONObject.toJSONString(tx.getError()));
            logger.error(JSONObject.toJSONString(record));
            logger.error(JSONObject.toJSONString(tx.getError()));
            return;
        }
        Integer confirmed = config.getConfirmed();
        BigInteger height = record.getHeight();
        BigInteger blockNumber = null;
        try {
            blockNumber = transaction.getBlockNumber();
        } catch (Exception e){
            logger.error("正在广播中");
            logger.error("=========" + JSONObject.toJSONString(record));
            return;
        }
        //更新交易中的块号
        height = updateTransactionRecordHeight(height, blockNumber, record.getTxId());
        record.setHeight(blockNumber);
        BigInteger nowConfirmed = nowHeight.subtract(height).add(BigInteger.ONE);
        record.setConfirm(nowConfirmed.intValue());
        if (nowConfirmed.intValue() >= confirmed){
            //通知钱包
            createMessage(record);
        }
        //更新确认数
        updateTranscationInfo(record);
    }

    private BigInteger updateTransactionRecordHeight(BigInteger beforeHeight, BigInteger blockNumber, String txId){
        if (beforeHeight == null && blockNumber != null && blockNumber.compareTo(BigInteger.TEN) == 1){
            Query query = new Query();
            Criteria criteria = new Criteria();
            criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TXID).is(txId);
            query.addCriteria(criteria);
            Update update = new Update();
            update.set(MongoConstant.ETH_TRANSACTION_CLASS_HEIGHT, blockNumber);
            mongoUtil.update(query, MongoConstant.ETH_TRANSACTION_CLASS, update);
        }
        return blockNumber;
    }

    private void createMessage(TransactionRecord record){
        //通知钱包
        //to的加钱，from扣钱
        transfNoticeRecordMapper.saveNoticeRecord(createNoticeRecord(record, record.getToAddress(), 1));
        transfNoticeRecordMapper.saveNoticeRecord(createNoticeRecord(record, record.getFromAddress(), 0));
    }

    private void updateTranscationInfo(TransactionRecord record){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_TXID).is(record.getTxId());
        query.addCriteria(criteria);
        Update update = new Update();
        update.set(MongoConstant.ETH_TRANSACTION_CLASS_HEIGHT, record.getHeight());
        update.set(MongoConstant.ETH_TRANSACTION_CLASS_CONFIRM, record.getConfirm());
        mongoUtil.update(query, MongoConstant.ETH_TRANSACTION_CLASS, update);
    }

    private TransactionRecord hasBusinessTxId(String businessTxId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_RECORD_ID).is(businessTxId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.ETH_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }

    private Address getAddress(String address, String coinType){
        Map<String, Object> query = new HashMap();
        query.put(MongoConstant.ETH_ADDRESSES_CLASS_ADDRESS, address);
        List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }

    private TransfNoticeRecord createNoticeRecord(TransactionRecord record, String address, Integer style){
        BigDecimal serviceCharge = record.getGasLimit().multiply(record.getGasPrice().divide(new BigDecimal(10).pow(9)));
        TransfNoticeRecord transfNoticeRecord = new TransfNoticeRecord();
        transfNoticeRecord.setTransId(record.getRecordId());
        transfNoticeRecord.setCoinType(record.getCoinType());
        transfNoticeRecord.setCoinNetworkType("ETH");
        transfNoticeRecord.setAddress(address);
        transfNoticeRecord.setTxId(record.getTxId());
        transfNoticeRecord.setAmount(record.getAmount());
        transfNoticeRecord.setConfirm(record.getConfirm() + 0l);
        transfNoticeRecord.setRealFee(serviceCharge);
        transfNoticeRecord.setType(record.getType());
        transfNoticeRecord.setStyle(style);
        transfNoticeRecord.setIsUsed(0);
        transfNoticeRecord.setNoticeSuccess(0);
        transfNoticeRecord.setNoticeId(getNoticeId(record.getTxId(), address, record.getCoinType(), style));
        transfNoticeRecord.setCreateTime(new Date());
        transfNoticeRecord.setSystemType(contractConfig.type);
        return transfNoticeRecord;
    }

    private String getNoticeId(String txId, String address, String coinType, int style){
        return MD5Util.MD5(txId + address + coinType + style);
    }


    private void updateTxId(String txId, String recordId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ETH_TRANSACTION_CLASS_RECORD_ID).is(recordId);
        query.addCriteria(criteria);
        Update update = new Update();
        update.set(MongoConstant.ETH_TRANSACTION_CLASS_TXID, txId);
        mongoUtil.update(query, MongoConstant.ETH_TRANSACTION_CLASS, update);
        TransfBalanceChangeRecordExample example = new TransfBalanceChangeRecordExample();
        example.createCriteria().andTxIdEqualTo(recordId);
        TransfBalanceChangeRecord record = new TransfBalanceChangeRecord();
        record.setTxId(txId);
        transfBalanceChangeRecordMapper.updateByExampleSelective(record, example);
    }
}
