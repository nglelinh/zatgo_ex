package com.zatgo.zup.transaction.basic.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 46041 on 2018/7/24.
 */
public interface BaseMapper<T> {

    int countByExample(Object example);

    int deleteByExample(Object example);

    int deleteByPrimaryKey(String id);

    int insert(T record);

    int insertSelective(T record);

    List<T> selectByExample(Object example);

    T selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") T record, @Param("example") Object example);

    int updateByExample(@Param("record") T record, @Param("example") Object example);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);
}
