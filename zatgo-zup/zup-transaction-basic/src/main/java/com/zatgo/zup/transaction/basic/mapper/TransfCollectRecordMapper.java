package com.zatgo.zup.transaction.basic.mapper;


import com.zatgo.zup.transaction.basic.entity.collect.TransfCollectRecord;

public interface TransfCollectRecordMapper extends BaseMapper<TransfCollectRecord>{
}