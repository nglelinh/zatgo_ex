package com.zatgo.zup.transaction.basic.entity.eth;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 46041 on 2019/1/8.
 */
public enum UpdateBalanceTypeEnum {
    // 0未确认 1已确认
    UNCONFIRMED(0), CONFIRMED(1);

    private Integer code;

    private static final Map<Integer, UpdateBalanceTypeEnum> stringToEnum = new HashMap<Integer, UpdateBalanceTypeEnum>();
    static {
        for (UpdateBalanceTypeEnum enumType : values()) {
            stringToEnum.put(enumType.getCode(), enumType);
        }
    }

    UpdateBalanceTypeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
