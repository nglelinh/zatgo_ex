package com.zatgo.zup.transaction.basic.service;

import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.transaction.basic.entity.BaseTransactionRecord;

import java.util.Map;
import java.util.concurrent.Future;

/**
 * Created by 46041 on 2018/10/17.
 */
public interface ThreadPoolTaskService {


    Future<ResponseData<Object>> tx(String txId, Integer maxValue);

}
