package com.zatgo.zup.transaction.basic.service.impl;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.transaction.basic.entity.account.TransfAddressBalance;
import com.zatgo.zup.transaction.basic.entity.account.TransfAddressBalanceExample;
import com.zatgo.zup.transaction.basic.entity.account.TransfBalanceChangeRecord;
import com.zatgo.zup.transaction.basic.entity.eth.UpdateBalanceTypeEnum;
import com.zatgo.zup.transaction.basic.mapper.TransfAddressBalanceMapper;
import com.zatgo.zup.transaction.basic.mapper.TransfBalanceChangeRecordMapper;
import com.zatgo.zup.transaction.basic.mapper.TransfNoticeRecordMapper;
import com.zatgo.zup.transaction.basic.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 46041 on 2019/1/8.
 */


@Service("accountService")
public class AccountServiceImpl implements AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private TransfAddressBalanceMapper transfAddressBalanceMapper;
    @Autowired
    private TransfBalanceChangeRecordMapper transfBalanceChangeRecordMapper;
    @Autowired
    private TransfNoticeRecordMapper transfNoticeRecordMapper;


    @Override
    public List<TransfAddressBalance> getNeedCollectAccount(BigDecimal amount, String coinNetworkType, String coinType) {
        TransfAddressBalanceExample example = new TransfAddressBalanceExample();
        TransfAddressBalanceExample.Criteria criteria = example.createCriteria();
        criteria.andConfirmedGreaterThanOrEqualTo(amount);
        criteria.andCoinNetworkTypeEqualTo(coinNetworkType);
        criteria.andCoinTypeEqualTo(coinType);
        return transfAddressBalanceMapper.selectByExample(example);
    }

    @Override
    public TransfAddressBalance getAddressAccount(String coinNetworkType, String coinType, String address) {
        return getAccount(address, coinNetworkType, coinType);
    }

    @Override
    @Transactional
    public void updateAddressConfirmed(String coinNetworkType, String coinType, BigDecimal amount,
                                       String address, String txId) {
        //判断余额是否足够
        TransfAddressBalance account = getAccount(address, coinNetworkType, coinType);
        if (!hasBalance(amount, account))
            throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
        //修改余额
        transfAddressBalanceMapper.updateBalance(amount, null, account.getId());
        //保存修改记录
        TransfBalanceChangeRecord record = new TransfBalanceChangeRecord();
        record.setAddress(address);
        record.setId(UUIDUtils.getUuid());
        record.setCreateTime(new Date());
        record.setCoinNetworkType(coinNetworkType);
        record.setCoinType(coinType);
        record.setAmount(amount);
        record.setType(UpdateBalanceTypeEnum.CONFIRMED.getCode());
        record.setTxId(txId);
        insertUpdateBalanceRecord(record);
    }

    @Override
    @Transactional
    public void updateAddressUnconfirmed(String coinNetworkType, String coinType, BigDecimal amount,
                                         String address, String txId) {
        //获取账户余额
        TransfAddressBalance account = getAccount(address, coinNetworkType, coinType);
        //修改余额
        transfAddressBalanceMapper.updateBalance(null, amount, account.getId());
        //保存修改记录
        TransfBalanceChangeRecord record = new TransfBalanceChangeRecord();
        record.setAddress(address);
        record.setCoinNetworkType(coinNetworkType);
        record.setCoinType(coinType);
        record.setAmount(amount);
        record.setType(UpdateBalanceTypeEnum.UNCONFIRMED.getCode());
        record.setId(UUIDUtils.getUuid());
        record.setCreateTime(new Date());
        record.setTxId(txId);
        insertUpdateBalanceRecord(record);
    }

    @Override
    @Transactional
    public void updateBalanceExtract(String coinNetworkType, String coinType, BigDecimal amount, BigDecimal fee, String address, String txId) {
        updateAddressConfirmed(coinNetworkType.toUpperCase(), coinType.toUpperCase(), amount, address, txId);
        updateAddressConfirmed(coinNetworkType.toUpperCase(), coinNetworkType.toUpperCase(), fee, address, txId);
    }

    private void insertUpdateBalanceRecord(TransfBalanceChangeRecord record){
        transfBalanceChangeRecordMapper.insertSelective(record);
    }

    private boolean hasBalance(BigDecimal amount, TransfAddressBalance account){
        if (amount.compareTo(BigDecimal.ZERO) == -1){
            BigDecimal confirmed = account.getConfirmed();
            if (account == null)
                return false;
            if (confirmed.add(amount).compareTo(BigDecimal.ZERO) == -1)
                return false;
        }
        return true;
    }

    private TransfAddressBalance getAccount(String address, String coinNetworkType, String coinType){
        TransfAddressBalanceExample example = new TransfAddressBalanceExample();
        example.createCriteria().andAddressEqualTo(address)
                .andCoinTypeEqualTo(coinType)
                .andCoinNetworkTypeEqualTo(coinNetworkType);
        List<TransfAddressBalance> transfAddressBalances = transfAddressBalanceMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(transfAddressBalances)){
            return createAccount(address, coinNetworkType, coinType);
        }
        return transfAddressBalances.get(0);
    }

    private TransfAddressBalance createAccount(String address, String coinNetworkType, String coinType){
        String createBalanceLock = RedisKeyConstants.TRANSACTION_ADD_ADDRESS_LOCK + address + coinNetworkType + coinType;
        TransfAddressBalance account = null;
        redisLockUtils.lock(createBalanceLock);
        try {
            TransfAddressBalanceExample example = new TransfAddressBalanceExample();
            example.createCriteria().andAddressEqualTo(address)
                    .andCoinTypeEqualTo(coinType)
                    .andCoinNetworkTypeEqualTo(coinNetworkType);
            List<TransfAddressBalance> transfAddressBalances = transfAddressBalanceMapper.selectByExample(example);
            if (CollectionUtils.isEmpty(transfAddressBalances)){
                account = new TransfAddressBalance();
                account.setCoinNetworkType(coinNetworkType);
                account.setCoinType(coinType);
                account.setUnconfirmed(BigDecimal.ZERO);
                account.setConfirmed(BigDecimal.ZERO);
                account.setId(UUIDUtils.getUuid());
                account.setAddress(address);
                transfAddressBalanceMapper.insertSelective(account);
                return account;
            }
        } finally {
            redisLockUtils.releaseLock(createBalanceLock);
        }
        return account;
    }
}
