package com.zatgo.zup.transaction.basic.entity.usdt;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 46041 on 2018/10/10.
 */
public class Address implements Serializable{
    /**
     * 地址
     */
    private String address;
    /**
     * 0系统地址1用户地址
     */
    private Integer type;
    /**
     * 未确认金额
     */
    private Double confirmed;
    /**
     * 已确认金额
     */
    private Double unconfirmed;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date updateDate;



    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Double confirmed) {
        this.confirmed = confirmed;
    }

    public Double getUnconfirmed() {
        return unconfirmed;
    }

    public void setUnconfirmed(Double unconfirmed) {
        this.unconfirmed = unconfirmed;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
