package com.zatgo.zup.transaction.basic;

import com.zatgo.zup.transaction.basic.entity.constant.CoinConstant;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Scanner;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
@EnableSwagger2
@ComponentScan(basePackages = "com.zatgo.zup")
@MapperScan("com.zatgo.zup.transaction.basic.mapper")
@EnableDiscoveryClient
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
public class ZupTransactionBasicApplication {

	public static void main(String[] args) {
//		System.out.println("请输入密码");
//		Scanner scan = new Scanner(System.in);
//		if (scan.hasNext()){
//			String password = scan.next();
//			CoinConstant.SIGN_PASSWORD = password;
//		}
		SpringApplication.run(ZupTransactionBasicApplication.class, args);
	}

}

