package com.zatgo.zup.transaction.basic.entity.constant;

import java.util.HashMap;
import java.util.Map;

public class Constants {
    public static final String CODE_SUCCESS = "000";
    public static final String CODE_ARGS_ILLEGAL = "100";
    public static final String CODE_SIGN_FAIL = "200";
    public static final String CODE_RPOCESS_FAIL = "300";
    public static final String CODE_BUSSINESS_FAIL = "500";

    public static final String CODE_BUSSINESS_FAIL_INFO = "coin symbol not exist";

    public static final Integer START_UID = 10000;

    public static Map<String, String> msgMap = new HashMap<>();
    static {
        msgMap.put(CODE_SUCCESS, "Success");
        msgMap.put(CODE_ARGS_ILLEGAL, "params illegal");
        msgMap.put(CODE_SIGN_FAIL, "sign fail");
        msgMap.put(CODE_RPOCESS_FAIL, "process fail");
        msgMap.put(CODE_BUSSINESS_FAIL, CODE_BUSSINESS_FAIL_INFO);
    }

    public static String getMessage(String code){
        return msgMap.get(code);
    }
}
