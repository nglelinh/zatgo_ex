package com.zatgo.zup.transaction.basic.listener;

import com.zatgo.zup.transaction.basic.proxy.TimerProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 46041 on 2019/1/28.
 */

public class PushMessageListener implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(PushMessageListener.class);

    private TimerProxy timerProxy;



    @Override
    public void run() {
        while (true){
            timerProxy.transactionNoticeListen();
            try {
                Thread.sleep(10000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public PushMessageListener(TimerProxy timerProxy) {
        this.timerProxy = timerProxy;
    }

    public PushMessageListener() {
    }
}