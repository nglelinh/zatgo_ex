package com.zatgo.zup.transaction.basic.entity.qtum;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by 46041 on 2018/10/10.
 */
public class Vin implements Serializable {
    /**
     * 交易ID
     */
    private String txId;
    /**
     * 第几笔交易
     */
    private Integer vout;

    private ScriptPubkey scriptPubKey;
    /**
     * 金额
     */
    private BigDecimal value;
    /**
     * 地址（可能为空）
     */
    private String address;

    private String sequence;

    private String coinType;

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public Integer getVout() {
        return vout;
    }

    public void setVout(Integer vout) {
        this.vout = vout;
    }

    public ScriptPubkey getScriptPubKey() {
        return scriptPubKey;
    }

    public void setScriptPubKey(ScriptPubkey scriptPubKey) {
        this.scriptPubKey = scriptPubKey;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }
}
