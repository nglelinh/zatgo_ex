package com.zatgo.zup.transaction.basic.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.encrypt.MD5Util;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CreateAddressRequest;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SignUsdtTransactionParams;
import com.zatgo.zup.common.model.UnSpentUsdtData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.transaction.basic.conf.ContractConfig;
import com.zatgo.zup.transaction.basic.entity.SendRequest;
import com.zatgo.zup.transaction.basic.entity.account.TransfBalanceChangeRecord;
import com.zatgo.zup.transaction.basic.entity.account.TransfBalanceChangeRecordExample;
import com.zatgo.zup.transaction.basic.entity.account.TransfConfig;
import com.zatgo.zup.transaction.basic.entity.account.TransfNoticeRecord;
import com.zatgo.zup.transaction.basic.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.basic.entity.constant.Constants;
import com.zatgo.zup.transaction.basic.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.basic.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.basic.entity.params.InsertUsdtRecordParam;
import com.zatgo.zup.transaction.basic.entity.usdt.*;
import com.zatgo.zup.transaction.basic.mapper.TransfBalanceChangeRecordMapper;
import com.zatgo.zup.transaction.basic.mapper.TransfNoticeRecordMapper;
import com.zatgo.zup.transaction.basic.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.basic.remoteservice.WalletRemotService;
import com.zatgo.zup.transaction.basic.service.AccountService;
import com.zatgo.zup.transaction.basic.service.CoinService;
import com.zatgo.zup.transaction.basic.service.ThreadPoolTaskService;
import com.zatgo.zup.transaction.basic.util.MongoUtil;
import com.zatgo.zup.transaction.basic.util.usdt.UsdtUnSpentList;
import com.zatgo.zup.transaction.basic.util.usdt.UsdtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by 46041 on 2018/10/10.
 */
@Service("usdtService")
public class UsdtService implements CoinService {

    private static final Logger logger = LoggerFactory.getLogger(UsdtService.class);


    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private SignRemotService signRemotService;
    @Resource(name = "usdtThreadPoolTaskService")
    private ThreadPoolTaskService usdtThreadPoolTaskService;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private WalletRemotService walletRemotService;
    @Autowired
    private UsdtUtil usdtUtil;
    @Autowired
    private TransfNoticeRecordMapper transfNoticeRecordMapper;
    @Autowired
    private ContractConfig contractConfig;
    @Autowired
    private AccountService accountService;
    @Autowired
    private TransfBalanceChangeRecordMapper transfBalanceChangeRecordMapper;


    private BigDecimal usdtDust = new BigDecimal("0.00000546");


    /**
     * 1、先校验输入金额
     * 2、交易from地址是否正确，避免找零到别人的地址
     * 3、获取未在交易地址上的未被花费的utxo
     * 4、拼数据去签名
     * 5、保存交易记录
     *
     * @return
     */
    @Override
    public String sendTransaction(SendRequest sendRequest) {
        String txId = null;
        Map<String, TransfConfig> coinTypeConfig = contractConfig.getCoinTypeConfig();
        BigDecimal amount = sendRequest.getAmount();
        String recordId = sendRequest.getBusinessTxId();
        String fromAddress = sendRequest.getFromAddress();
        String toAddress = sendRequest.getToAddress();
        BigDecimal fee = sendRequest.getFee();
        String businessTxId = sendRequest.getBusinessTxId();
        TransfConfig usdt = coinTypeConfig.get("USDT");
        if (fee == null || BigDecimal.ZERO.compareTo(fee) == 0){
            fee = usdt.getFee();
        }
        if (amount.compareTo(BigDecimal.ZERO) == -1){
            throw new BusinessException(BusinessExceptionCode.EXTRACT_MONEY_ERROR);
        }
        String extractLock = RedisKeyConstants.TRANSACTION_EXTRACT_LOCK + fromAddress;
        redisLockUtils.lock(extractLock);
        try {
            if (StringUtils.isEmpty(recordId)){
                logger.error("转账失败的业务Id：" + recordId);
                throw new BusinessException(BusinessExceptionCode.BUSINESS_TXID_NOT_IS_EMPTY);
            }
            if (hasRecordId(recordId) == null) {
                Map<String, Object> query = new HashMap();
                query.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, fromAddress);
                List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
                if (CollectionUtils.isEmpty(data)) {
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException(BusinessExceptionCode.EXTRACT_FROM_ADDRESS_NOT_EXIST);
                }
                //在所有的未被花费的utxo中找合适的utxo
                UsdtUnSpentList usdtUnSpentList = new UsdtUnSpentList(mongoUtil, usdt.getConfirmed());
                BigDecimal count = usdtDust.add(fee);
                List<UTXO> utxos = usdtUnSpentList.get(count);
                if (CollectionUtils.isEmpty(utxos)) {
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
                }
                for (UTXO utxo : utxos) {
                    count = count.subtract(utxo.getAmount());
                }
                if (count.compareTo(BigDecimal.ZERO) == 1) {
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
                }
                accountService.updateBalanceExtract("BTC", "USDT", amount.negate(), fee, fromAddress, businessTxId);
                //获取了足够的utxo去签名
                ResponseData<String> responseData = signRemotService.usdtSign(getParams(utxos, toAddress, CoinConstant.SIGN_PASSWORD, fromAddress, amount, fee));
                if (!responseData.isSuccessful()) {
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException(BusinessExceptionCode.GO_TO_SIGN_ERROR);
                }
                String hex = responseData.getData();
                //保存记录
                InsertUsdtRecordParam record = new InsertUsdtRecordParam();
                record.setAmount(amount);
                record.setFromAddress(fromAddress);
                record.setHex(hex);
                record.setRecordId(recordId);
                record.setPayList(utxos);
                record.setToAddress(toAddress);
                record.setFee(fee);
                try {
                    insertTransactionRecord(record);
                } catch (ParseException e) {
                    logger.error("", e);
                    logger.error("转账失败的业务Id：" + recordId);
                    throw new BusinessException();
                }
                txId = usdtUtil.sendRawTransaction(hex);
                if (StringUtils.isEmpty(txId)){
                    throw new BusinessException(BusinessExceptionCode.ORDER_SEND_FAIL);
                }
                updateTxId(txId, businessTxId);
            }
        } catch (Throwable throwable){
            logger.error("" , throwable);
        } finally {
            redisLockUtils.releaseLock(extractLock);
        }
        return txId;
    }

    private void updateTxId(String txId, String recordId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.TRANSACTION_CLASS_RECORD_ID).is(recordId);
        query.addCriteria(criteria);
        Update update = new Update();
        update.set(MongoConstant.TRANSACTION_CLASS_TXID, txId);
        mongoUtil.update(query, MongoConstant.TRANSACTION_CLASS, update);
        TransfBalanceChangeRecordExample example = new TransfBalanceChangeRecordExample();
        example.createCriteria().andTxIdEqualTo(recordId);
        TransfBalanceChangeRecord record = new TransfBalanceChangeRecord();
        record.setTxId(txId);
        transfBalanceChangeRecordMapper.updateByExampleSelective(record, example);

    }


    /**
     * 1、获取配置文件中和mongodb的mainchain表的中的区块高度，取最大值
     * 2、扫描此区块，获取该区块中的所有交易
     * 3、通过交易获取交易记录
     * 4、通过交易记录获取所有未被消费的utxo
     * 5、更新mainchain中的区块高度
     * 6、如果该条交易不是提币，那新增一条交易记录
     */
    @Override
    public void scanBlock() {
        try{
            //获取数据库中区块高度
            Integer maxValue = mongoUtil.getMaxValue("height", MongoConstant.MAIN_CHAIN_CLASS_NAME) + 1;
//            maxValue = getMax(contractConfig.height, maxValue);
            maxValue = getMax(568952, maxValue);
            Integer blockCount = usdtUtil.getBlockCount();
            //如果已经扫描到最高的区块了，休息10秒继续扫描
            if (maxValue - 1 == blockCount){
                try {
                    logger.info("睡了10秒");
                    Thread.sleep(10 * 1000l);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return;
            }
            String blockHash = usdtUtil.getBlockHash(maxValue);
            List<String> blockTx = usdtUtil.getBlockTx(blockHash);
            List<Future> res = new ArrayList<>(blockTx.size());
            for (String txId : blockTx){
                Future<ResponseData<Object>> future = usdtThreadPoolTaskService.tx(txId, maxValue);
                res.add(future);
            }
            for (int i = 0; i < res.size() - 1; i++){
                try {
                    Future<ResponseData<Object>> future = res.get(i);
                    ResponseData<Object> result = future.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

            MainChain mainChain = new MainChain();
            mainChain.setHeight(maxValue);
            mainChain.setHash(blockHash);
            mongoUtil.insert(mainChain, MongoConstant.MAIN_CHAIN_CLASS_NAME);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void checkTransaction() {
        try{
            Map<String, TransfConfig> coinTypeConfig = contractConfig.getCoinTypeConfig();
            TransfConfig usdt = coinTypeConfig.get("USDT");
            List<TransactionRecord> unCheckTransction = UsdtUnSpentList.getUnCheckTransction(usdt.getConfirmed(), mongoUtil);
            if (!CollectionUtils.isEmpty(unCheckTransction)){
                Integer blockCount = usdtUtil.getBlockCount();
                if (blockCount == null)
                    return;
                for (TransactionRecord record : unCheckTransction){
                    String txId = record.getTxId();
                    if (StringUtils.isEmpty(txId))
                        continue;
                    TransactionRecord transaction = usdtUtil.getTransaction(txId, blockCount);
                    try {
                        Integer height = record.getHeight();
                        if (height == null){
                            record.setHeight(blockCount);
                        }
                        record.setVoutList(transaction.getVoutList());
                        record.setConfirmed(blockCount.intValue() - record.getHeight());
                    } catch (Throwable e){
                        logger.error("", e);
                    }
                }
                //修改确认数
                batchExecuteUpdateConfirmed(unCheckTransction);
            }
        } catch (Throwable e){
            logger.error("", e);
        }
    }

    @Override
    public void GetBtcExtractRecord() {
        Map<String, String> map = new HashMap();
        map.put("symbol", "USDT");
        Map<String, ?> stringMap = walletRemotService.withdrawConsume(map);
        if (stringMap != null){
            String code = (String) stringMap.get("errno");
            if (Constants.CODE_SUCCESS.equals(code)){
                List<Map> withdrawResults = (List<Map>) stringMap.get("data");
                if (!CollectionUtils.isEmpty(withdrawResults)){
                    for (Map m : withdrawResults){
                        SendRequest request = new SendRequest();
                        request.setFromAddress((String) m.get("from_address"));
                        request.setToAddress((String) m.get("address_to"));
                        request.setAmount(new BigDecimal(m.get("amount") + ""));
                        request.setFee(new BigDecimal(m.get("fee") + ""));
                        request.setBusinessTxId(m.get("trans_id") + "");
                        request.setCoinType("USDT");
                        sendTransaction(request);
                    }
                }
            }
        }
    }

    @Override
    @Async("scanExtractTxThreadPool")
    public void addAddress() {
        if (!CoinConstant.createAddressLockMap.containsKey(CoinConstant.USDT_ADDRESS_LOCK)){
            CoinConstant.createAddressLockMap.put(CoinConstant.USDT_ADDRESS_LOCK, "");
            try {
                CreateAddressRequest request = new CreateAddressRequest();
                List<String> data = new ArrayList<>();
                request.setPassword(CoinConstant.SIGN_PASSWORD);
                request.setNum(100);
                while (true){
                    try{
                        ResponseData<List<String>> newAddresses = signRemotService.getNewUsdtAddresses(request);
                        data.addAll(newAddresses.getData());
                        if (contractConfig.addressNum <= data.size()){
                            break;
                        }
                    } catch (Exception e){
                        logger.error("", e);
                    }
                }
                List<Address> addresses = new ArrayList<>(data.size());
                JSONArray array = new JSONArray();
                Date date = new Date();
                for (String a : data){
                    JSONObject object = new JSONObject();
                    object.put("address", a);
                    object.put("networkType", "USDT");
                    object.put("isUsed", 0);
                    object.put("create_time", date);
                    array.add(object);
                    Address address = new Address();
                    address.setAddress(a);
                    address.setCreateDate(date);
                    addresses.add(address);
                }
                ResponseData<String> responseData = walletRemotService.addAddresses(array);
                if (responseData.isSuccessful()){
                    mongoUtil.insert(addresses, MongoConstant.ADDRESS_CLASS_NAME);
                }
            } finally {
                CoinConstant.createAddressLockMap.remove(CoinConstant.USDT_ADDRESS_LOCK);
            }
        }
    }

    private Integer getMax(Integer v1, Integer v2){
        if (v1 == null){
            v1 = 1;
        }
        if (v2 == null){
            v2 = 1;
        }
        return v1 > v2 ? v1 : v2;
    }

    private SignUsdtTransactionParams getParams(List<UTXO> payList, String toAddress,
                                               String password, String changeAddress, BigDecimal amount, BigDecimal fee){
        SignUsdtTransactionParams params = new SignUsdtTransactionParams();
        BigDecimal unit = new BigDecimal("100000000");
        params.setAmount(amount.multiply(unit).longValue());
        params.setFee(fee.multiply(unit).longValue());
        params.setToAddress(toAddress);
        params.setChangeAddress(changeAddress);
        params.setPassword(password);
        List<UnSpentUsdtData> list = new ArrayList<>();
        for (UTXO utxo : payList){
            UnSpentUsdtData unSpentUsdtData = new UnSpentUsdtData();
            unSpentUsdtData.setTxId(utxo.getTxId());
            unSpentUsdtData.setHeight(utxo.getHeight());
            unSpentUsdtData.setSatoshis(utxo.getAmount().multiply(unit).longValue());
            unSpentUsdtData.setVout(Long.valueOf(utxo.getVout() + ""));
            unSpentUsdtData.setScriptPubKey(utxo.getScriptPubKey().getHex());
            list.add(unSpentUsdtData);
        }
        params.setUnSpentUsdts(list);
        return params;
    }

    private void insertTransactionRecord(InsertUsdtRecordParam param) throws ParseException {
        BigDecimal amount = param.getAmount();
        TransactionRecord record = new TransactionRecord();
        BigDecimal fee = param.getFee();
        record.setCreateDate(new Date());
        record.setFee(fee);
        record.setConfirmed(0);
        record.setHex(param.getHex());
        record.setRecordId(param.getRecordId());
        record.setTxId(param.getTxid());
        record.setType(0);
        List<Vin> vins = new ArrayList<>();
        List<Vout> vouts = new ArrayList<>();
        BigDecimal count = BigDecimal.ZERO;
        List<UTXO> payList = param.getPayList();
        Query query = null;
        Criteria criteria = null;
        Update update = null;
        for (UTXO utxo : payList){
            Vin vin = new Vin();
            String utxoAddress = utxo.getAddress();
            vin.setAddress(utxoAddress);
            vin.setVout(utxo.getVout());
            vin.setCoinType("BTC");
            vin.setTxId(utxo.getTxId());
            BigDecimal utxoAmount = utxo.getAmount();
            vin.setValue(utxoAmount);
            count = count.add(utxoAmount);
            vin.setScriptPubKey(utxo.getScriptPubKey());
            vins.add(vin);
            //修改utxo的状态
            query = new Query();
            criteria = new Criteria();
            criteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
            criteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
            query.addCriteria(criteria);
            update = new Update();
            update.set(MongoConstant.UTXO_CLASS_NAME_STATUS, 1);
            mongoUtil.update(query, MongoConstant.UTXO_CLASS_NAME, update);
        }
        //代币vin
        Vin token = new Vin();
        token.setCoinType("USDT");
        token.setAddress(param.getFromAddress());
        token.setVout(-1);
        token.setValue(amount);
        token.setTxId(param.getTxid());
        vins.add(token);
        //找零
        Vout toVout = new Vout();
        toVout.setAddress(param.getToAddress());
        toVout.setValue(amount);
        vouts.add(toVout);
        BigDecimal subtract = count.subtract(amount).subtract(fee);
        if (subtract.compareTo(BigDecimal.ZERO) == 1){
            Vout changeVout = new Vout();
            changeVout.setAddress(param.getFromAddress());
            changeVout.setValue(subtract);
            vouts.add(changeVout);
        }
        record.setVinList(vins);
        record.setVoutList(vouts);
        Query q = new Query();
        Criteria c = new Criteria();
        c.and(MongoConstant.USDT_TRANSACTION_CLASS_TXID).is(record.getTxId());
        q.addCriteria(c);
        List<TransactionRecord> data = mongoUtil.getData(q, TransactionRecord.class, MongoConstant.TRANSACTION_CLASS);
        if (CollectionUtils.isEmpty(data)) {
            mongoUtil.insert(record, MongoConstant.USDT_TRANSACTION_CLASS);
        }
    }

    private void batchExecuteUpdateConfirmed(List<TransactionRecord> list){
        Map<String, TransfConfig> coinTypeConfig = contractConfig.getCoinTypeConfig();
        TransfConfig usdt = coinTypeConfig.get("USDT");
        Map<Query, Update> queryUpdateMap = new HashMap();
        if (CollectionUtils.isEmpty(list))
            return;
        for (TransactionRecord record : list){
            try{
                Query query = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.USDT_TRANSACTION_CLASS_TXID).is(record.getTxId());
                query.addCriteria(criteria);
                Update update = new Update();
                update.set(MongoConstant.USDT_TRANSACTION_CLASS_CONFIRMED, record.getConfirmed());
                update.set(MongoConstant.TRANSACTION_CLASS_HEIGHT, record.getHeight());
                update.set(MongoConstant.USDT_TRANSACTION_CLASS_VOUTLIST, record.getVoutList());
                //到达确认数的，修改地址余额
                if (usdt.getConfirmed().intValue() <= record.getConfirmed().intValue()){
                    //通知钱包
                    Set<String> selfAddrSet = getSelfAddrSet(record);
                    createMessage(record, selfAddrSet);
                    updateUnconfirmedAmount(record);
                }
                queryUpdateMap.put(query, update);
            } catch (Exception e){
                logger.error("", e);
            }
        }
        if (!queryUpdateMap.isEmpty())
            mongoUtil.batchExecuteUpdate(queryUpdateMap, MongoConstant.USDT_TRANSACTION_CLASS);
    }

    private Set<String> getSelfAddrSet(TransactionRecord record){
        List<String> addressList = new ArrayList<>();
        List<Vin> vinList = record.getVinList();
        for (Vin vin : vinList){
            addressList.add(vin.getAddress());
        }
        List<Vout> vouts = record.getVoutList();
        for (Vout vout : vouts){
            addressList.add(vout.getAddress());
        }
        Map<String, Collection<String>> query = new HashMap<>();
        query.put(MongoConstant.USDT_ADDRESS_CLASS_NAME_ADDRESS, addressList);
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.USDT_ADDRESS_CLASS_NAME);
        Set<String> set = new HashSet<>();
        addresses.forEach(addr -> {set.add(addr.getAddress());});
        return set;
    }

    private void createMessage(TransactionRecord record, Set<String> selfAddrSet){
        //通知钱包
        //vin扣钱  vout加钱
        List<Vin> vinList = record.getVinList();
        List<Vout> voutList = record.getVoutList();
        Map<String, Vin> vinMap = new HashMap<>();
        Map<String, Vout> voutMap = new HashMap<>();
        setMap(vinList, voutList, vinMap, voutMap, selfAddrSet);
        vinMap.forEach((k,vin) -> {
            String coinType = vin.getCoinType();
            int vinType = 0;
            if (!"USDT".equals(coinType)){
                vinType = TransactionTypeEnum.FEE.getCode();
            }
            transfNoticeRecordMapper.saveNoticeRecord(createNoticeRecord(record, vin.getAddress(), 0, vin.getValue(), vin.getVout(), vin.getCoinType(), vinType));
        });
        voutMap.forEach((k,vout) -> {
            int voutType = 1;
            String coinType = vout.getCoinType();
            if (!"USDT".equals(coinType)){
                voutType = TransactionTypeEnum.FEE.getCode();
            }
            transfNoticeRecordMapper.saveNoticeRecord(createNoticeRecord(record, vout.getAddress(), 1, vout.getValue(), vout.getVout(), vout.getCoinType(), voutType));
        });
    }

    private TransfNoticeRecord createNoticeRecord(TransactionRecord record, String address, Integer style,
                                                  BigDecimal amount, int vout, String coinType, int type){
        TransfNoticeRecord transfNoticeRecord = new TransfNoticeRecord();
        transfNoticeRecord.setTransId(record.getRecordId());
        if ("usdt".equalsIgnoreCase(coinType)){
            transfNoticeRecord.setCoinType("USDT");
            transfNoticeRecord.setCoinNetworkType("BTC");
        } else {
            transfNoticeRecord.setCoinType("BTC");
            transfNoticeRecord.setCoinNetworkType("BTC");
        }
        transfNoticeRecord.setAddress(address);
        transfNoticeRecord.setTxId(record.getTxId());
        transfNoticeRecord.setAmount(amount);
        transfNoticeRecord.setConfirm(record.getConfirmed() + 0l);
        transfNoticeRecord.setRealFee(record.getFee());
        transfNoticeRecord.setType(type);
        transfNoticeRecord.setStyle(style);
        transfNoticeRecord.setIsUsed(0);
        transfNoticeRecord.setNoticeSuccess(0);
        transfNoticeRecord.setNoticeId(getNoticeId(record.getTxId(), address, "USDT", style, vout));
        transfNoticeRecord.setCreateTime(new Date());
        transfNoticeRecord.setSystemType(contractConfig.type);
        return transfNoticeRecord;
    }

    private String getNoticeId(String txId, String address, String coinType, int style, int vout){
        return MD5Util.MD5(txId + address + coinType + style + vout);
    }

    private void setMap(List<Vin> vinList, List<Vout> voutList, Map<String, Vin> vinMap,
                        Map<String, Vout> voutMap, Set<String> selfAddrSet){

        vinList.forEach(vin -> {
            String address = vin.getAddress();
            String coinType = vin.getCoinType();
            String key = address + "_" + coinType;
            if (selfAddrSet.contains(address)) {
                if (vinMap.containsKey(key)){
                    Vin in = vinMap.get(key);
                    in.setValue(in.getValue().add(vin.getValue()));
                } else {
                    vinMap.put(key, vin);
                }
            }
        });

        voutList.forEach(vout -> {
            String address = vout.getAddress();
            String coinType = vout.getCoinType();
            String key = address + "_" + coinType;
            if (selfAddrSet.contains(address)) {
                if (voutMap.containsKey(key)){
                    Vout out = voutMap.get(key);
                    out.setValue(out.getValue().add(vout.getValue()));
                } else {
                    voutMap.put(key, vout);
                }
            }
        });
    }

    private void updateUnconfirmedAmount(TransactionRecord record) {
        List<String> addressList = new ArrayList<>();
        List<Vin> vinList = record.getVinList();
        for (Vin vin : vinList){
            addressList.add(vin.getAddress());
        }
        List<Vout> vouts = record.getVoutList();
        for (Vout vout : vouts){
            addressList.add(vout.getAddress());
        }
        Map<String, Collection<String>> query = new HashMap<>();
        query.put(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS, addressList);
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        if (!CollectionUtils.isEmpty(addresses)){
            for (Vin vin : vinList){
                String address = vin.getAddress();
                for (Address a : addresses){
                    if (a.getAddress().equals(address)){
                        //修改utxo的状态
                        Query utxoQuery = new Query();
                        Criteria utxoCriteria = new Criteria();
                        utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(vin.getTxId());
                        utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(vin.getVout());
                        utxoQuery.addCriteria(utxoCriteria);
                        Update utxoUpdate = new Update();
                        utxoUpdate.set(MongoConstant.UTXO_CLASS_NAME_STATUS, 2);
                        mongoUtil.update(utxoQuery, MongoConstant.UTXO_CLASS_NAME, utxoUpdate);
                    }
                }
            }
            for (Vout vout : vouts){
                String address = vout.getAddress();
                for (Address a : addresses){
                    if (a.getAddress().equals(address)){
                        //修改utxo的状态
                        Query utxoQuery = new Query();
                        Criteria utxoCriteria = new Criteria();
                        utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_TXID).is(record.getTxId());
                        utxoCriteria.and(MongoConstant.UTXO_CLASS_NAME_VOUT).is(vout.getVout());
                        utxoQuery.addCriteria(utxoCriteria);
                        Update utxoUpdate = new Update();
                        utxoUpdate.set(MongoConstant.UTXO_CLASS_NAME_STATUS, 0);
                        mongoUtil.update(utxoQuery, MongoConstant.UTXO_CLASS_NAME, utxoUpdate);
                    }
                }
            }
        }
    }

    private TransactionRecord hasRecordId(String recordId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.TRANSACTION_CLASS_RECORD_ID).is(recordId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }

}
