package com.zatgo.zup.transaction.basic.util.btc;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.zatgo.zup.common.model.Commands;
import com.zatgo.zup.common.utils.ArgsUtils;
import com.zatgo.zup.transaction.basic.entity.btc.*;
import com.zatgo.zup.transaction.basic.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.basic.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.basic.util.MongoUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.bitcoinj.wallet.UnreadableWalletException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Mikhail Yevchenko m.ṥῥẚɱ.ѓѐḿởύḙ@azazar.com
 */

@Component
public class BitcoinUtil {

    private static final Logger logger = LoggerFactory.getLogger(BitcoinUtil.class);


    @Value("${transaction.user:}")
    private String user;
    @Value("${transaction.password:}")
    private String password;
    @Value("${transaction.host:}")
    private String host;
    @Value("${transaction.port:}")
    private String port;
    @Autowired
    private MongoUtil mongoUtil;


    private JsonRpcHttpClient client = null;

    private static Pattern P2PKH = Pattern.compile("^OP_DUP OP_HASH160 [a-zA-Z0-9]* OP_EQUALVERIFY OP_CHECKSIG$");

    private static Pattern P2SH = Pattern.compile("^OP_HASH160 [a-zA-Z0-9]* OP_EQUAL$");


    private JsonRpcHttpClient getClient(){
        try {
            if (client == null){
                client = new JsonRpcHttpClient(new URL("http://" + host + ":" + port));
                String cred = Base64.encodeBase64String((user + ":" + password).getBytes());
                Map<String, String> headers = new HashMap<String, String>(1);
                headers.put("Authorization", "Basic " + cred);
                client.setHeaders(headers);
            }
        } catch (Exception e){
            logger.error("", e);
        }
        return client;
    }


    /**
     * 获取块hash
     * @param height
     * @return
     */
    public String getBlockHash(Integer height) throws Throwable {
        return getClient().invoke(Commands.GET_BLOCK_HASH, ArgsUtils.asList(height), String.class);
    }

    /**
     * 获取当前区块高度
     * @return
     */
    public Integer getBlockCount() throws Throwable {
        return getClient().invoke(Commands.GET_BLOCK_COUNT, null, Integer.class);
    }

    /**
     * 获取区块中所有的交易ID list
     * @return
     */
    public List<String> getBlockTx(String blockHash) throws Throwable {
        JSONObject res = getClient().invoke(Commands.GET_BLOCK, ArgsUtils.asList(blockHash), JSONObject.class);
        if (res == null){
            return null;
        }
        return res.getJSONArray("tx").toJavaList(String.class);
    }

    /**
     * 根据交易Id获取交易信息
     * @param txId
     * @return
     */
    public TransactionRecord getTransaction(String txId, Integer height) throws Throwable {
        String hex = getClient().invoke(Commands.GET_RAW_TRANSACTION, ArgsUtils.asList(txId), String.class);
        JSONObject rawTransaction = getClient().invoke(Commands.DECODE_RAW_TRANSACTION, ArgsUtils.asList(hex), JSONObject.class);
        if (rawTransaction == null){
            return null;
        }
        TransactionRecord record = new TransactionRecord();
        record.setHeight(height);
        record.setCreateDate(new Date());
        record.setConfirmed(0);
        record.setHex(hex);
        record.setTxId(rawTransaction.getString("txid"));
        record.setType(TransactionTypeEnum.DEPOSIT.getCode());
        getVin(record, rawTransaction);
        getVout(record, rawTransaction);
        record.setCreateDate(new Date());
        return record;
    }

    public TransactionRecord getTransaction(String txId) throws Throwable {
        String hex = getClient().invoke(Commands.GET_RAW_TRANSACTION, ArgsUtils.asList(txId), String.class);
        JSONObject rawTransaction = getClient().invoke(Commands.DECODE_RAW_TRANSACTION, ArgsUtils.asList(hex), JSONObject.class);
        if (rawTransaction == null){
            return null;
        }
        TransactionRecord record = new TransactionRecord();
        record.setCreateDate(new Date());
        record.setConfirmed(0);
        record.setHex(rawTransaction.getString("hash"));
        record.setTxId(rawTransaction.getString("txid"));
        record.setType(TransactionTypeEnum.DEPOSIT.getCode());
        getVin(record, rawTransaction);
        getVout(record, rawTransaction);
        record.setCreateDate(new Date());
        return record;
    }

    /**
     * 根据交易记录获取未被消费的UTXO
     * @param record
     * @return
     */
    public List<UTXO> getUnSpentUTXO(TransactionRecord record) {
        List<UTXO> list = new ArrayList<>();
        List<Vout> voutList = record.getVoutList();
        for (Vout out : voutList){
            list.add(getUtxo(record, out));
        }
        return list;
    }

    /**
     * 根据交易记录获取已消费的UTXO
     * @param record
     * @return
     */
    public List<UTXO> getSpentUTXO(TransactionRecord record) {
        List<UTXO> list = new ArrayList<>();
        List<Vin> vinList = record.getVinList();
        Date date = new Date();
        for (Vin in : vinList){
            UTXO utxo = new UTXO();
            String address = in.getAddress();
//            if (StringUtils.isEmpty(address)){
//                if (!StringUtils.isEmpty(txId)){
//                    TransactionRecord transaction = getTransaction(txId);
//                    if (transaction != null){
//                        List<Vout> voutList = transaction.getVoutList();
//                        Vout vout = voutList.get(in.getVout());
//                        address = vout.getAddress();
//                        utxo.setAmount(vout.getValue());
//                    }
//                }
//            }
            utxo.setTxId(in.getTxId());
            utxo.setAddress(address);
            utxo.setScriptPubKey(in.getScriptPubKey());
            utxo.setVout(in.getVout());
            utxo.setHeight(record.getHeight());
            utxo.setBlockDate(record.getBlockDate());
            utxo.setCreateDate(date);
            list.add(utxo);
        }
        return list;
    }

    public UTXO getUtxo(TransactionRecord record, Vout out){
        Date date = new Date();
        UTXO utxo = new UTXO();
        utxo.setTxId(record.getTxId());
        utxo.setAddress(out.getAddress());
        utxo.setBlockDate(record.getBlockDate());
        utxo.setVout(out.getVout());
        utxo.setHeight(record.getHeight());
        utxo.setScriptPubKey(out.getScriptPubKey());
        utxo.setAmount(out.getValue());
        utxo.setCreateDate(date);
        utxo.setStatus(1);
        return utxo;
    }

    /**
     * 发送交易
     * @param hex
     * @return
     */
    public String sendTransaction(String hex) throws Throwable {
        return getClient().invoke(Commands.SEND_RAW_TRANSACTION, ArgsUtils.asList(hex), String.class);
    }

    private void getVin(TransactionRecord record, JSONObject rawTransaction){
        JSONArray ins = rawTransaction.getJSONArray("vin");
        List<Vin> list = new ArrayList<>();
        for (int i = 0; i < ins.size(); i++){
            JSONObject in = ins.getJSONObject(i);
            Vin vin = new Vin();
            vin.setTxId(in.getString("txid"));
            JSONObject scriptSig = in.getJSONObject("scriptSig");
            if (scriptSig != null){
                String hex = scriptSig.getString("hex");
                vin.setScriptPubKey(StringUtils.isEmpty(hex) ? "" : hex);
            }
            vin.setVout(in.getInteger("vout"));
            list.add(vin);
        }
        record.setVinList(list);
    }

    private void getVout(TransactionRecord record, JSONObject rawTransaction){
        JSONArray outs = rawTransaction.getJSONArray("vout");
        List<Vout> list = new ArrayList<>();
        for (int i = 0; i < outs.size(); i++){
            JSONObject out = outs.getJSONObject(i);
            Vout vout = new Vout();
            //addresses 可能为空的时候
            JSONObject scriptPubKey = out.getJSONObject("scriptPubKey");
            String asm = scriptPubKey.getString("asm");
            if (!P2PKH.matcher(asm).matches() || !P2SH.matcher(asm).matches())
                continue;
            JSONArray addresses = scriptPubKey.getJSONArray("addresses");
            if (addresses != null && !addresses.isEmpty()){
                vout.setAddress(addresses.getString(0));
            }
            vout.setScriptPubKey(scriptPubKey.getString("hex"));
            vout.setValue(new BigDecimal(out.getString("value")));
            vout.setVout(out.getInteger("n"));
            list.add(vout);
        }
        record.setVoutList(list);
    }

    public Address getAddress(String address){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.ADDRESS_CLASS_NAME_ADDRESS).is(address);
        query.addCriteria(criteria);
        List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        return data.get(0);
    }

    public static void main(String[] args) throws Throwable {

//        Matcher matcher = P2PKH.matcher("OP_DUP OP_HASH160 3d75d61e0c4fcd352e171ea31c586fd6da9ab0b0 OP_EQUALVERIFY OP_CHECKSIG");
//        System.out.println(matcher.matches());
//        matcher = P2SH.matcher("OP_HASH160 69f376f6d27898c2f0e05f1cd3cb19e58be1bfab OP_EQUAL");
//        System.out.println(matcher.matches());

        String txId = "9aeda5a2126456f7cd910098be5eeb3f05451e21c64455ba2310eff7ec2e3615";
        String hex = getClient(1).invoke(Commands.GET_RAW_TRANSACTION, ArgsUtils.asList(txId), String.class);
        JSONObject rawTransaction = getClient(1).invoke(Commands.DECODE_RAW_TRANSACTION, ArgsUtils.asList(hex), JSONObject.class);
        System.out.println(rawTransaction);

    }

    private static JsonRpcHttpClient getClient(int i){
        JsonRpcHttpClient client = null;
        try {
            if (client == null){
                client = new JsonRpcHttpClient(new URL("http://47.74.145.17:8332"));
                String cred = Base64.encodeBase64String(("111:222").getBytes());
                Map<String, String> headers = new HashMap<String, String>(1);
                headers.put("Authorization", "Basic " + cred);
                client.setHeaders(headers);
            }
        } catch (Exception e){
            logger.error("", e);
        }
        return client;
    }

}
