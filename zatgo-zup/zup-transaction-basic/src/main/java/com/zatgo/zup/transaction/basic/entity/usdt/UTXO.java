package com.zatgo.zup.transaction.basic.entity.usdt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 46041 on 2018/10/10.
 */
public class UTXO implements Serializable {
    private String txId;
    /**
     * 地址
     */
    private String address;
    /**
     * 金额
     */
    private BigDecimal amount;
    /**
     * vout中的第几笔
     */
    private Integer vout;

    private String redeemScript;

    private ScriptPubkey scriptPubKey;
    /**
     * 区块高度
     */
    private Integer height;
    /**
     * 区块上创建时间
     */
    private Date blockDate;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 状态 0未花销，1冻结，2已花销
     */
    private Integer status;

    private String coinType;



    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getVout() {
        return vout;
    }

    public void setVout(Integer vout) {
        this.vout = vout;
    }

    public String getRedeemScript() {
        return redeemScript;
    }

    public void setRedeemScript(String redeemScript) {
        this.redeemScript = redeemScript;
    }

    public ScriptPubkey getScriptPubKey() {
        return scriptPubKey;
    }

    public void setScriptPubKey(ScriptPubkey scriptPubKey) {
        this.scriptPubKey = scriptPubKey;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getBlockDate() {
        return blockDate;
    }

    public void setBlockDate(Date blockDate) {
        this.blockDate = blockDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }
}
