package com.zatgo.zup.transaction.basic.entity.qtum;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 46041 on 2018/10/29.
 */
public enum AddressTypeEnum {
    SYSTEM_ADDRESS(0), MEMBER_ADDRESS(1), SYSTEM_COLLECT(2);

    private Integer code;

    private static final Map<Integer, AddressTypeEnum> stringToEnum = new HashMap<Integer, AddressTypeEnum>();
    static {
        for (AddressTypeEnum enumType : values()) {
            stringToEnum.put(enumType.getCode(), enumType);
        }
    }

    AddressTypeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
