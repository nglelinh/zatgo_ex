package com.zatgo.zup.transaction.basic.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.encrypt.MD5Util;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.*;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.transaction.basic.conf.ContractConfig;
import com.zatgo.zup.transaction.basic.entity.SendRequest;
import com.zatgo.zup.transaction.basic.entity.account.TransfBalanceChangeRecord;
import com.zatgo.zup.transaction.basic.entity.account.TransfBalanceChangeRecordExample;
import com.zatgo.zup.transaction.basic.entity.account.TransfConfig;
import com.zatgo.zup.transaction.basic.entity.account.TransfNoticeRecord;
import com.zatgo.zup.transaction.basic.entity.constant.CoinConstant;
import com.zatgo.zup.transaction.basic.entity.constant.Constants;
import com.zatgo.zup.transaction.basic.entity.constant.MongoConstant;
import com.zatgo.zup.transaction.basic.entity.eth.TransactionTypeEnum;
import com.zatgo.zup.transaction.basic.entity.params.InsertQtumRecordParam;
import com.zatgo.zup.transaction.basic.entity.qtum.*;
import com.zatgo.zup.transaction.basic.mapper.TransfBalanceChangeRecordMapper;
import com.zatgo.zup.transaction.basic.mapper.TransfNoticeRecordMapper;
import com.zatgo.zup.transaction.basic.remoteservice.SignRemotService;
import com.zatgo.zup.transaction.basic.remoteservice.WalletRemotService;
import com.zatgo.zup.transaction.basic.service.AccountService;
import com.zatgo.zup.transaction.basic.service.CoinService;
import com.zatgo.zup.transaction.basic.util.MongoUtil;
import com.zatgo.zup.transaction.basic.util.qtum.QtumUnSpentList;
import com.zatgo.zup.transaction.basic.util.qtum.QtumUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by 46041 on 2018/11/22.
 */

@Service("qtumService")
public class QtumService implements CoinService {

    private static final Logger logger = LoggerFactory.getLogger(QtumService.class);

    @Autowired
    private QtumUtil qtumUtil;
    @Autowired
    private MongoUtil mongoUtil;
    @Autowired
    private RedisLockUtils redisLockUtils;
    @Autowired
    private SignRemotService signRemotService;
    @Autowired
    private WalletRemotService walletRemotService;
    @Autowired
    private TransfNoticeRecordMapper transfNoticeRecordMapper;
    @Autowired
    private ContractConfig contractConfig;
    @Autowired
    private AccountService accountService;
    @Autowired
    private TransfBalanceChangeRecordMapper transfBalanceChangeRecordMapper;


    @Override
    public String sendTransaction(SendRequest sendRequest) {
        String txId = null;
        BigDecimal amount = sendRequest.getAmount();
        String businessTxId = sendRequest.getBusinessTxId();
        String coinType = sendRequest.getCoinType();
        String fromAddress = sendRequest.getFromAddress();
        String toAddress = sendRequest.getToAddress();
        BigDecimal gasLimit = sendRequest.getGasLimit();
        Map<String, TransfConfig> coinTypeConfig = contractConfig.getCoinTypeConfig();
        TransfConfig transfConfig = coinTypeConfig.get(coinType);
        if (gasLimit == null){
            gasLimit = transfConfig.getGasLimit();
        }
        BigDecimal gasPrice = sendRequest.getGasPrice();
        if (gasPrice == null){
            gasPrice = transfConfig.getGasPrice();
        }
        BigDecimal fee = sendRequest.getFee();
        if (fee == null){
            fee = transfConfig.getFee();
        }
        if (amount.compareTo(BigDecimal.ZERO) == -1){
            throw new BusinessException(BusinessExceptionCode.EXTRACT_MONEY_ERROR);
        }
        String extractLock = RedisKeyConstants.TRANSACTION_EXTRACT_LOCK + fromAddress;
        redisLockUtils.lock(extractLock);
        try {
            if (StringUtils.isEmpty(businessTxId)){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BUSINESS_TXID_NOT_IS_EMPTY);
            }
            if (hasRecordId(businessTxId) == null) {
                Map<String, Object> query = new HashMap();
                query.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, fromAddress);
                List<Address> data = mongoUtil.getData(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
                if (CollectionUtils.isEmpty(data)) {
                    logger.error("转账失败的业务Id：" + businessTxId);
                    throw new BusinessException(BusinessExceptionCode.EXTRACT_FROM_ADDRESS_NOT_EXIST);
                }
                if ("QTUM".equalsIgnoreCase(coinType)){
                    txId = sendQtum(amount, businessTxId, toAddress, fromAddress, coinType,
                            TransactionTypeEnum.EXTRACT, fee, transfConfig.getConfirmed());
                } else {
                    txId =sendQtumContract(amount, businessTxId, toAddress, fromAddress, coinType,
                            TransactionTypeEnum.EXTRACT, fee, transfConfig, gasLimit, gasPrice);
                }
            }
        } catch (Throwable throwable) {
            logger.error("", throwable);
        } finally {
            redisLockUtils.releaseLock(extractLock);
        }
        return txId;
    }

    @Override
    public void scanBlock() throws IOException {
       try {
           //获取数据库中区块高度
           Integer maxValue = mongoUtil.getMaxValue("height", MongoConstant.QTUM_MAIN_CHAIN_CLASS) + 1;
           maxValue = getMax(contractConfig.height, maxValue);
           Integer blockCount = qtumUtil.getBlockCount();
           //如果已经扫描到最高的区块了，休息10秒继续扫描
           if (maxValue - 1 == blockCount){
               try {
                   logger.info("睡了10秒");
                   Thread.sleep(10 * 1000l);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               return;
           }
           String blockHash = qtumUtil.getBlockHash(maxValue);
           List<String> blockTx = qtumUtil.getBlockTx(blockHash);
            for (int i = 0; i < blockTx.size(); i++){
                String txId = blockTx.get(i);
                tx(txId, maxValue, i);
            }
           MainChain mainChain = new MainChain();
           mainChain.setHeight(maxValue);
           mainChain.setHash(blockHash);
           mongoUtil.insert(mainChain, MongoConstant.QTUM_MAIN_CHAIN_CLASS);
       } catch (Exception e){
           logger.error("", e);
       } catch (Throwable throwable) {
           throwable.printStackTrace();
       }
    }

    @Override
    public void checkTransaction() {
        Map<String, TransfConfig> coinTypeConfig = contractConfig.getCoinTypeConfig();
        TransfConfig qtum = coinTypeConfig.get("QTUM");
        List<TransactionRecord> unCheckTransction = QtumUnSpentList.getUnCheckTransction(qtum.getConfirmed(), mongoUtil);
        List<TransactionRecord> unCheckReturn = QtumUnSpentList.getUnCheckReturn(mongoUtil);
        if (!CollectionUtils.isEmpty(unCheckReturn)){
            unCheckTransction.addAll(unCheckReturn);
        }
        try{
            Integer blockCount = qtumUtil.getBlockCount();
            for (TransactionRecord record : unCheckTransction){
                String txId = record.getTxId();
                if (StringUtils.isEmpty(txId))
                    continue;
                TransactionRecord transaction = null;
                try {
                    transaction = qtumUtil.getTransaction(txId, blockCount, -1);
                    if (transaction == null)
                        continue;
                } catch (Throwable throwable) {
                    logger.error("", throwable);
                    continue;
                }
                record.setVoutList(transaction.getVoutList());
                if (record.getHeight() != null){
                    record.setConfirmed(transaction.getHeight() - record.getHeight() + 1);
                } else {
                    record.setHeight(blockCount);
                }
            }
            //修改确认数
            batchExecuteUpdateConfirmed(unCheckTransction);
        } catch (Throwable throwable) {
            logger.error("", throwable);
        }
    }

    @Override
    public void GetBtcExtractRecord() {
        Map<String, TransfConfig> configMap = contractConfig.getContractConfig();
        configMap.forEach((k, v) -> {
            Map<String, String> map = new HashMap();
            map.put("symbol", v.getCoinType());
            Map<String, ?> stringMap = walletRemotService.withdrawConsume(map);
            if (stringMap != null){
                String code = (String) stringMap.get("errno");
                if (Constants.CODE_SUCCESS.equals(code)){
                    List<Map> withdrawResults = (List<Map>) stringMap.get("data");
                    if (!CollectionUtils.isEmpty(withdrawResults)){
                        for (Map m : withdrawResults){
                            SendRequest request = new SendRequest();
                            request.setFromAddress((String) m.get("from_address"));
                            request.setToAddress((String) m.get("address_to"));
                            request.setAmount(new BigDecimal(m.get("amount") + ""));
                            Object fee = m.get("fee");
                            if (fee != null){
                                request.setFee(new BigDecimal( fee + ""));
                            }
                            request.setBusinessTxId(m.get("trans_id") + "");
                            request.setCoinType(v.getCoinType());
                            sendTransaction(request);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void addAddress() {
        if (!CoinConstant.createAddressLockMap.containsKey(CoinConstant.QTUM_ADDRESS_LOCK)){
            CoinConstant.createAddressLockMap.put(CoinConstant.QTUM_ADDRESS_LOCK, "");
            try {
                CreateAddressRequest request = new CreateAddressRequest();
                List<String> data = new ArrayList<>();
                request.setPassword(CoinConstant.SIGN_PASSWORD);
                request.setNum(100);
                while (true){
                    try{
                        ResponseData<List<String>> newAddresses = signRemotService.getNewQtumAddresses(request);
                        data.addAll(newAddresses.getData());
                        if (contractConfig.addressNum <= data.size()){
                            break;
                        }
                    } catch (Exception e){
                        logger.error("", e);
                    }
                }
                List<Address> addresses = new ArrayList<>(data.size());
                JSONArray array = new JSONArray();
                Date date = new Date();
                for (String a : data){
                    JSONObject object = new JSONObject();
                    object.put("address", a);
                    object.put("networkType", "QTUM");
                    object.put("isUsed", 0);
                    object.put("create_time", date);
                    array.add(object);
                    Address address = new Address();
                    address.setType(AddressTypeEnum.MEMBER_ADDRESS.getCode());
                    address.setAddress(a);
                    address.setCreateDate(date);
                    addresses.add(address);
                }
                ResponseData<String> responseData = walletRemotService.addAddresses(array);
                if (responseData.isSuccessful()){
                    mongoUtil.insert(addresses, MongoConstant.ADDRESS_CLASS_NAME);
                }
            } finally {
                CoinConstant.createAddressLockMap.remove(CoinConstant.QTUM_ADDRESS_LOCK);
            }
        }
    }

    private void batchExecuteUpdateConfirmed(List<TransactionRecord> list){
        Map<String, TransfConfig> coinTypeConfig = contractConfig.getCoinTypeConfig();
        TransfConfig qtum = coinTypeConfig.get("QTUM");
        Map<Query, Update> queryUpdateMap = new HashMap();
        if (CollectionUtils.isEmpty(list))
            return;
        for (TransactionRecord record : list){
            try{
                Query query = new Query();
                Criteria criteria = new Criteria();
                criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).is(record.getTxId());
                query.addCriteria(criteria);
                Update update = new Update();
                if (record.getConfirmed() == 0){
                    update.set(MongoConstant.QTUM_TRANSACTION_CLASS_HEIGHT, record.getHeight());
                }
                update.set(MongoConstant.QTUM_TRANSACTION_CLASS_CONFIRMED, record.getConfirmed());
                update.set(MongoConstant.QTUM_TRANSACTION_CLASS_VOUTLIST, record.getVoutList());
                //如果是旷工费退款，需要500个确认，如果不是，则需要到达当前币种指定的确认数
                if ((TransactionTypeEnum.RETURN.getCode().equals(record.getType()) && record.getConfirmed() >= 500)
                        || (!TransactionTypeEnum.RETURN.getCode().equals(record.getType()) && record.getConfirmed() >= qtum.getConfirmed())){
                    //通知钱包
                    Set<String> selfAddrSet = getSelfAddrSet(record);
                    createMessage(record, selfAddrSet);
                    updateUnconfirmedAmount(record);
                }
                queryUpdateMap.put(query, update);
            } catch (Exception e){
                logger.error("", e);
            }
        }
        if (!queryUpdateMap.isEmpty())
            mongoUtil.batchExecuteUpdate(queryUpdateMap, MongoConstant.TRANSACTION_CLASS);

    }

    private void createMessage(TransactionRecord record, Set<String> selfAddrSet){
        //通知钱包
        //vin扣钱  vout加钱
        List<Vin> vinList = record.getVinList();
        List<Vout> voutList = record.getVoutList();
        Map<String, Vin> vinMap = new HashMap<>();
        Map<String, Vout> voutMap = new HashMap<>();
        setMap(vinList, voutList, vinMap, voutMap, selfAddrSet);
        vinMap.forEach((k,vin) -> {
            transfNoticeRecordMapper.saveNoticeRecord(createNoticeRecord(record, vin.getAddress(), 0, vin.getValue(), vin.getVout(), vin.getCoinType()));
        });
        voutMap.forEach((k,vout) -> {
            transfNoticeRecordMapper.saveNoticeRecord(createNoticeRecord(record, vout.getAddress(), 1, vout.getValue(), vout.getVout(), vout.getCoinType()));
        });
    }

    private void setMap(List<Vin> vinList, List<Vout> voutList, Map<String, Vin> vinMap,
                        Map<String, Vout> voutMap, Set<String> selfAddrSet){

        vinList.forEach(vin -> {
            String address = vin.getAddress();
            String coinType = vin.getCoinType();
            String key = address + "_" + coinType;
            if (selfAddrSet.contains(address)) {
                if (vinMap.containsKey(key)){
                    Vin in = vinMap.get(key);
                    in.setValue(in.getValue().add(vin.getValue()));
                } else {
                    vinMap.put(key, vin);
                }
            }
        });

        voutList.forEach(vout -> {
            String address = vout.getAddress();
            String coinType = vout.getCoinType();
            String key = address + "_" + coinType;
            if (selfAddrSet.contains(address)) {
                if (voutMap.containsKey(key)){
                    Vout out = voutMap.get(key);
                    out.setValue(out.getValue().add(vout.getValue()));
                } else {
                    voutMap.put(key, vout);
                }
            }
        });
    }

    private TransfNoticeRecord createNoticeRecord(TransactionRecord record, String address, Integer style,
                                                  BigDecimal amount, int vout, String coinType){
        TransfNoticeRecord transfNoticeRecord = new TransfNoticeRecord();
        transfNoticeRecord.setTransId(record.getRecordId());
        transfNoticeRecord.setCoinType(coinType);
        transfNoticeRecord.setCoinNetworkType("QTUM");
        transfNoticeRecord.setAddress(address);
        transfNoticeRecord.setTxId(record.getTxId());
        transfNoticeRecord.setAmount(amount);
        transfNoticeRecord.setConfirm(record.getConfirmed() + 0l);
        transfNoticeRecord.setRealFee(record.getFee());
        transfNoticeRecord.setType(record.getType());
        transfNoticeRecord.setStyle(style);
        transfNoticeRecord.setIsUsed(0);
        transfNoticeRecord.setNoticeSuccess(0);
        transfNoticeRecord.setNoticeId(getNoticeId(record.getTxId(), address, "QTUM", style, vout));
        transfNoticeRecord.setCreateTime(new Date());
        transfNoticeRecord.setSystemType(contractConfig.type);
        return transfNoticeRecord;
    }

    private String getNoticeId(String txId, String address, String coinType, int style, int vout){
        return MD5Util.MD5(txId + address + coinType + style + vout);
    }

    private Set<String> getSelfAddrSet(TransactionRecord record){
        List<String> addressList = new ArrayList<>();
        List<Vin> vinList = record.getVinList();
        for (Vin vin : vinList){
            addressList.add(vin.getAddress());
        }
        List<Vout> vouts = record.getVoutList();
        for (Vout vout : vouts){
            addressList.add(vout.getAddress());
        }
        Map<String, Collection<String>> query = new HashMap<>();
        query.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, addressList);
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.QTUM_ADDRESS_CLASS_NAME);
        Set<String> set = new HashSet<>();
        addresses.forEach(addr -> {set.add(addr.getAddress());});
        return set;
    }


    private void updateUnconfirmedAmount(TransactionRecord record) {
        List<String> addressList = new ArrayList<>();
        List<Vin> vinList = record.getVinList();
        Integer type = record.getType();
        for (Vin vin : vinList){
            String address = vin.getAddress();
            if (!StringUtils.isEmpty(address))
                addressList.add(address);
        }
        List<Vout> vouts = record.getVoutList();
        for (Vout vout : vouts){
            String address = vout.getAddress();
            if (!StringUtils.isEmpty(address))
                addressList.add(address);
        }
        Map<String, Collection<String>> query = new HashMap<>();
        query.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, addressList);
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        if (!CollectionUtils.isEmpty(addresses)){
            for (Vin vin : vinList){
                String address = vin.getAddress();
                for (Address a : addresses){
                    if (a.getAddress().equals(address)){
                        //修改utxo的状态
                        Query utxoQuery = new Query();
                        Criteria utxoCriteria = new Criteria();
                        utxoCriteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(vin.getTxId());
                        utxoCriteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(vin.getVout());
                        utxoQuery.addCriteria(utxoCriteria);
                        Update utxoUpdate = new Update();
                        utxoUpdate.set(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS, 2);
                        mongoUtil.update(utxoQuery, MongoConstant.QTUM_UTXO_CLASS_NAME, utxoUpdate);
                    }
                }
            }
            for (Vout vout : vouts){
                String address = vout.getAddress();
                for (Address a : addresses){
                    if (a.getAddress().equals(address)){
                        Query utxoQuery = new Query();
                        Criteria utxoCriteria = new Criteria();
                        utxoCriteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(record.getTxId());
                        utxoCriteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(vout.getVout());
                        utxoQuery.addCriteria(utxoCriteria);
                        Update utxoUpdate = new Update();
                        utxoUpdate.set(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS, 0);
                        mongoUtil.update(utxoQuery, MongoConstant.QTUM_UTXO_CLASS_NAME, utxoUpdate);
                    }
                }
            }
        }
    }

    private void insertContractTransactionRecord(InsertQtumRecordParam param, Integer type) {
        String updateAddressBalanceLock = RedisKeyConstants.QTUM_UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try {
            BigDecimal amount = param.getAmount();
            BigDecimal gasLimit = param.getGasLimit();
            BigDecimal gasPrice = param.getGasPrice();
            BigDecimal fee = param.getFee();
            TransactionRecord record = new TransactionRecord();
            record.setCreateDate(new Date());
            record.setFee(fee);
            record.setConfirmed(0);
            record.setHex(param.getHex());
            record.setRecordId(param.getRecordId());
            record.setTxId(param.getTxid());
            record.setType(type);
            record.setGasLimit(gasLimit);
            record.setGasPrice(gasPrice);
            List<Vin> vins = new ArrayList<>();
            List<Vout> vouts = new ArrayList<>();
            BigDecimal count = BigDecimal.ZERO;
            List<UTXO> payList = param.getPayList();
            Query query = null;
            Criteria criteria = null;
            Update update = null;
            //修改utxo的金额
            for (UTXO utxo : payList){
                Vin vin = new Vin();
                String utxoAddress = utxo.getAddress();
                vin.setAddress(utxoAddress);
                vin.setVout(utxo.getVout());
                vin.setTxId(utxo.getTxId());
                vin.setCoinType("QTUM");
                BigDecimal utxoAmount = utxo.getAmount();
                vin.setValue(utxoAmount);
                count = count.add(utxoAmount);
                vin.setScriptPubKey(utxo.getScriptPubKey());
                vins.add(vin);
                //修改utxo的状态
                query = new Query();
                criteria = new Criteria();
                criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
                criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
                query.addCriteria(criteria);
                update = new Update();
                update.set(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS, 1);
                mongoUtil.update(query, MongoConstant.QTUM_UTXO_CLASS_NAME, update);
            }
            //代币vin
            Vin token = new Vin();
            token.setCoinType(param.getCoinType());
            token.setAddress(param.getFromAddress());
            token.setVout(-1);
            token.setValue(amount);
            token.setTxId(param.getTxid());
            vins.add(token);
            //找零
            Vout toVout = new Vout();
            toVout.setAddress(param.getToAddress());
            toVout.setValue(amount);
            vouts.add(toVout);
            BigDecimal subtract = count.subtract(fee).subtract(qtumUtil.qtumAmountFormate8(gasLimit.multiply(gasPrice)));
            if (subtract.compareTo(BigDecimal.ZERO) == 1){
                Vout changeVout = new Vout();
                changeVout.setAddress(param.getFromAddress());
                changeVout.setValue(subtract);
                vouts.add(changeVout);
            }
            record.setVinList(vins);
            record.setVoutList(vouts);
            Query q = new Query();
            Criteria c = new Criteria();
            c.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).is(record.getTxId());
            q.addCriteria(c);
            List<TransactionRecord> data = mongoUtil.getData(q, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
            if (CollectionUtils.isEmpty(data)) {
                mongoUtil.insert(record, MongoConstant.QTUM_TRANSACTION_CLASS);
            }
        }finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }


    private void insertTransactionRecord(InsertQtumRecordParam param, Integer type) {
        BigDecimal fee = param.getFee();
        TransactionRecord record = new TransactionRecord();
        record.setCreateDate(new Date());
        record.setFee(fee);
        record.setConfirmed(0);
        record.setHex(param.getHex());
        record.setRecordId(param.getRecordId());
        record.setTxId(param.getTxid());
        record.setFromAddress(param.getFromAddress());
        record.setToAddress(param.getToAddress());
        record.setType(type);
        List<Vin> vins = new ArrayList<>();
        List<Vout> vouts = new ArrayList<>();
        BigDecimal count = BigDecimal.ZERO;
        List<UTXO> payList = param.getPayList();
        Query query = null;
        Criteria criteria = null;
        Update update = null;
        for (UTXO utxo : payList){
            Vin vin = new Vin();
            String utxoAddress = utxo.getAddress();
            vin.setAddress(utxoAddress);
            vin.setVout(utxo.getVout());
            vin.setTxId(utxo.getTxId());
            BigDecimal utxoAmount = utxo.getAmount();
            vin.setValue(utxoAmount);
            count = count.add(utxoAmount);
            vin.setScriptPubKey(utxo.getScriptPubKey());
            vin.setCoinType(param.getCoinType());
            vins.add(vin);
            //修改utxo的状态
            query = new Query();
            criteria = new Criteria();
            criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
            criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
            query.addCriteria(criteria);
            update = new Update();
            update.set(MongoConstant.QTUM_UTXO_CLASS_NAME_STATUS, 1);
            mongoUtil.update(query, MongoConstant.QTUM_UTXO_CLASS_NAME, update);
        }
        BigDecimal amount = param.getAmount();
        Vout toVout = new Vout();
        toVout.setAddress(param.getToAddress());
        toVout.setValue(amount);
        vouts.add(toVout);
        BigDecimal subtract = count.subtract(amount).subtract(fee);
        if (subtract.compareTo(BigDecimal.ZERO) == 1){
            Vout changeVout = new Vout();
            changeVout.setAddress(param.getFromAddress());
            changeVout.setValue(subtract);
            vouts.add(changeVout);
        }
        record.setVinList(vins);
        record.setVoutList(vouts);
        Query q = new Query();
        Criteria c = new Criteria();
        c.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).is(record.getTxId());
        q.addCriteria(c);
        List<TransactionRecord> data = mongoUtil.getData(q, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
        if (CollectionUtils.isEmpty(data)) {
            mongoUtil.insert(record, MongoConstant.QTUM_TRANSACTION_CLASS);
        }
    }


    private SignQtumTransactionParams getParams(List<UTXO> payList, String toAddress,
                                               String password, String changeAddress, BigDecimal amount, BigDecimal fee){
        SignQtumTransactionParams params = new SignQtumTransactionParams();
        BigDecimal unit = new BigDecimal("100000000");
        params.setAmount(amount.multiply(unit).longValue());
        params.setFee(fee.multiply(unit).longValue());
        params.setToAddress(toAddress);
        params.setChangeAddress(changeAddress);
        params.setPassword(password);
        List<UnSpentQtumData> list = new ArrayList<>();
        for (UTXO utxo : payList){
            UnSpentQtumData unSpentQtumData = new UnSpentQtumData();
            unSpentQtumData.setTxId(utxo.getTxId());
            unSpentQtumData.setHeight(utxo.getHeight());
            unSpentQtumData.setSatoshis(utxo.getAmount().multiply(unit).longValue());
            unSpentQtumData.setVout(Long.valueOf(utxo.getVout() + ""));
            unSpentQtumData.setScriptPubKey(utxo.getScriptPubKey().getHex());
            list.add(unSpentQtumData);
        }
        params.setUnSpentBtcs(list);
        return params;
    }

    private QtumContractSignParams getContractParams(List<UTXO> payList,String fromAddress, String toAddress,
                                     String password, String changeAddress, BigDecimal amount, TransfConfig config,
                                                     BigDecimal fee, BigDecimal gasLimit, BigDecimal gasPrice) throws Throwable {
        QtumContractSignParams params = new QtumContractSignParams();
        BigInteger precision = new BigInteger(config.getPrecision() + "");
        params.setFromAddress(fromAddress);
        params.setToAddressHex(qtumUtil.getAddressHex(toAddress));
        params.setContractAddress(config.getContract());
        params.setDecimal(precision);
        params.setGasLimit(gasLimit.toBigInteger());
        params.setGasPrice(gasPrice.toBigInteger());
        params.setAmount(qtumUtil.qtumAmountFormateSumN(amount, new BigInteger(config.getPrecision() + "")));
        params.setToAddress(toAddress);
        params.setChangeAddress(changeAddress);
        params.setFee(qtumUtil.qtumAmountFormateSumN(fee, new BigInteger("8")).toBigInteger());
        params.setPassword(password);
        List<UnSpentQtumData> list = new ArrayList<>();
        for (UTXO utxo : payList){
            UnSpentQtumData unSpentQtumData = new UnSpentQtumData();
            unSpentQtumData.setTxId(utxo.getTxId());
            unSpentQtumData.setHeight(utxo.getHeight());
            unSpentQtumData.setSatoshis(qtumUtil.qtumAmountFormateSumN(utxo.getAmount(), precision).longValue());
            unSpentQtumData.setVout(Long.valueOf(utxo.getVout() + ""));
            unSpentQtumData.setScriptPubKey(utxo.getScriptPubKey().getHex());
            list.add(unSpentQtumData);
        }
        params.setUnSpentBtcs(list);
        return params;
    }

    private boolean isBalanceNotEnough(BigDecimal amount){
        Map<String, TransfConfig> coinTypeConfig = contractConfig.getCoinTypeConfig();
        TransfConfig qtum = coinTypeConfig.get("QTUM");
        QtumUnSpentList addressUsedBtcUnSpentList = new QtumUnSpentList(mongoUtil, qtum.getConfirmed(), 1);
        List<UTXO> usedUtxos = addressUsedBtcUnSpentList.get(amount);
        if (CollectionUtils.isEmpty(usedUtxos)){
            return true;
        }
        for (UTXO utxo : usedUtxos) {
            amount = amount.subtract(utxo.getAmount());
        }
        if (amount.compareTo(BigDecimal.ZERO) == 1) {
            return true;
        }
        return false;
    }

    private TransactionRecord hasRecordId(String recordId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_RECORD_ID).is(recordId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }


    private void tx(String txId, Integer height, int index){
        try{
            TransactionRecord transaction = qtumUtil.getTransaction(txId, height, index);
            //获取所有utxo
            List<UTXO> unSpentUTXO = qtumUtil.getUnSpentUTXO(transaction);
            //保存自己的utxo，返回是否存在自己的交易
            saveDepositUTXOInfo(unSpentUTXO, transaction);
        } catch (Exception e){
            logger.error("", e);
        } catch (Throwable throwable) {
            logger.error("", throwable);
        }
    }


    private void saveDepositUTXOInfo(List<UTXO> utxos, TransactionRecord transaction){
        if (CollectionUtils.isEmpty(utxos)){
            return;
        }
        Map<String, Collection<String>> query = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (UTXO utxo : utxos){
            String address = utxo.getAddress();
            if (address != null){
                list.add(address);
            }
        }
        query.put(MongoConstant.QTUM_ADDRESS_CLASS_NAME_ADDRESS, list);
        //查询是否为自己的地址
        List<Address> addresses = mongoUtil.getDataByListQuery(query, Address.class, MongoConstant.ADDRESS_CLASS_NAME);
        if (!CollectionUtils.isEmpty(addresses)){
            int i = utxos.size() - 1;
            for ( ; i >= 0; i--){
                UTXO utxo = utxos.get(i);
                String utxoAddress = utxo.getAddress();
                if (!hasAddress(addresses, utxoAddress)){
                    continue;
                }
                if ("call".equals(utxo.getScriptPubKey().getType())){
                    continue;
                }
                createUTXO(utxo);
            }
            if (getTransactionRecord(transaction.getTxId()) == null){
                mongoUtil.insert(transaction, MongoConstant.QTUM_TRANSACTION_CLASS);
            }
        }
    }

    private TransactionRecord getTransactionRecord(String txId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.QTUM_TRANSACTION_CLASS_TXID).is(txId);
        query.addCriteria(criteria);
        List<TransactionRecord> data = mongoUtil.getData(query, TransactionRecord.class, MongoConstant.QTUM_TRANSACTION_CLASS);
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }


    private String sendQtum(BigDecimal amount, String businessTxId, String toAddress, String fromAddress,
                            String coinType, TransactionTypeEnum type, BigDecimal fee, Integer confirmed) throws Throwable {
        //在所有的未被花费的utxo中找合适的utxo
        QtumUnSpentList qtumUnSpentList = null;
        //如果是搜集
        if (type.getCode() == TransactionTypeEnum.COLLECT.getCode()){
            List<String> addresses = new ArrayList<>();
            addresses.add(toAddress);
            qtumUnSpentList = new QtumUnSpentList(mongoUtil, confirmed, addresses);
        } else {
            qtumUnSpentList = new QtumUnSpentList(mongoUtil, confirmed);
        }
        BigDecimal count = amount.add(fee);
        List<UTXO> utxos = qtumUnSpentList.get(count);
        if (CollectionUtils.isEmpty(utxos)) {
            if (isBalanceNotEnough(amount.add(fee))){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
            }
        }
        for (UTXO utxo : utxos) {
            count = count.subtract(utxo.getAmount());
        }
        if (count.compareTo(BigDecimal.ZERO) == 1) {
            if (isBalanceNotEnough(amount.add(fee))){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
            }
        }
        accountService.updateBalanceExtract("QTUM", coinType, amount.negate(), fee, fromAddress, businessTxId);
        //获取了足够的utxo去签名
        ResponseData<String> responseData = signRemotService.qtumSign(getParams(utxos, toAddress, CoinConstant.SIGN_PASSWORD, fromAddress, amount, fee));
        if (!responseData.isSuccessful()) {
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException(BusinessExceptionCode.GO_TO_SIGN_ERROR);
        }
        String hex = responseData.getData();
        //保存记录
        InsertQtumRecordParam record = new InsertQtumRecordParam();
        record.setAmount(amount);
        record.setFromAddress(fromAddress);
        record.setHex(hex);
        record.setRecordId(businessTxId);
        record.setPayList(utxos);
        record.setToAddress(toAddress);
        record.setCoinType(coinType);
        record.setFee(fee);
        insertTransactionRecord(record, type.getCode());
        String txId = qtumUtil.sendRawTransaction(hex);
        if (StringUtils.isEmpty(txId)){
            throw new BusinessException(BusinessExceptionCode.ORDER_SEND_FAIL);
        }
        updateTxId(txId, businessTxId);
        return txId;
    }

    private String sendQtumContract(BigDecimal amount, String businessTxId, String toAddress, String fromAddress,
                                    String coinType, TransactionTypeEnum type, BigDecimal fee,
                                    TransfConfig transfConfig, BigDecimal gasLimit, BigDecimal gasPrice) throws Throwable {

        //在from地址中找手续费
        QtumUnSpentList qtumUnSpentList = new QtumUnSpentList(mongoUtil, transfConfig.getConfirmed(), fromAddress);
        BigDecimal count = fee;
        count = count.add(qtumUtil.qtumAmountFormateN(gasLimit.multiply(gasPrice), 8));
//        BigDecimal payFee = count;
        List<UTXO> utxos = qtumUnSpentList.get(count);
        if (CollectionUtils.isEmpty(utxos)) {
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
        }
        for (UTXO utxo : utxos) {
            count = count.subtract(utxo.getAmount());
        }
        if (count.compareTo(BigDecimal.ZERO) == 1) {
            if (isBalanceNotEnough(amount.add(fee))){
                logger.error("转账失败的业务Id：" + businessTxId);
                throw new BusinessException(BusinessExceptionCode.BALANCE_NOT_ENOUGH);
            }
        }
        accountService.updateBalanceExtract("QTUM", coinType, amount.negate(), fee, fromAddress, businessTxId);
        //获取了足够的utxo去签名
        ResponseData<String> responseData = signRemotService.qtumSign(getContractParams(
                utxos,fromAddress, toAddress, CoinConstant.SIGN_PASSWORD, fromAddress, amount, transfConfig, fee, gasLimit, gasPrice));
        if (!responseData.isSuccessful()) {
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException(BusinessExceptionCode.GO_TO_SIGN_ERROR);
        }
        String hex = responseData.getData();
        //保存记录
        InsertQtumRecordParam record = new InsertQtumRecordParam();
        record.setAmount(amount);
        record.setFromAddress(fromAddress);
        record.setHex(hex);
        record.setRecordId(businessTxId);
        record.setPayList(utxos);
        record.setToAddress(toAddress);
        record.setCoinType(coinType);
        record.setGasPrice(gasPrice);
        record.setGasLimit(gasLimit);
        record.setFee(fee);
        try {
            insertContractTransactionRecord(record, type.getCode());
        } catch (Exception e) {
            logger.error("", e);
            logger.error("转账失败的业务Id：" + businessTxId);
            throw new BusinessException();
        }
        String txId = qtumUtil.sendRawTransaction(hex);
        if (StringUtils.isEmpty(txId)){
            throw new BusinessException(BusinessExceptionCode.ORDER_SEND_FAIL);
        }
        updateTxId(txId, businessTxId);
        return txId;
    }

    private void updateTxId(String txId, String recordId){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and(MongoConstant.TRANSACTION_CLASS_RECORD_ID).is(recordId);
        query.addCriteria(criteria);
        Update update = new Update();
        update.set(MongoConstant.TRANSACTION_CLASS_TXID, txId);
        mongoUtil.update(query, MongoConstant.TRANSACTION_CLASS, update);
        TransfBalanceChangeRecordExample example = new TransfBalanceChangeRecordExample();
        example.createCriteria().andTxIdEqualTo(recordId);
        TransfBalanceChangeRecord record = new TransfBalanceChangeRecord();
        record.setTxId(txId);
        transfBalanceChangeRecordMapper.updateByExampleSelective(record, example);

    }

    private boolean hasAddress(List<Address> addresses, String find){
        for (Address address : addresses){
            if (address.getAddress().equals(find)){
                return true;
            }
        }
        return false;
    }

    private void createUTXO(UTXO utxo){
        String updateAddressBalanceLock = RedisKeyConstants.QTUM_UPDATE_ADDRESS_BALANCE_LOCK;
        redisLockUtils.lock(updateAddressBalanceLock);
        try{
            Query utxoQuery = new Query();
            Criteria criteria = new Criteria();
            criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_TXID).is(utxo.getTxId());
            criteria.and(MongoConstant.QTUM_UTXO_CLASS_NAME_VOUT).is(utxo.getVout());
            utxoQuery.addCriteria(criteria);
            List<UTXO> data = mongoUtil.getData(utxoQuery, UTXO.class, MongoConstant.UTXO_CLASS_NAME);
            //防止重复添加utxo
            if (CollectionUtils.isEmpty(data)){
                utxo.setStatus(0);
                //保存utxo
                mongoUtil.insert(utxo, MongoConstant.QTUM_UTXO_CLASS_NAME);
            }
        } finally {
            redisLockUtils.releaseLock(updateAddressBalanceLock);
        }
    }

    private Integer getMax(Integer v1, Integer v2){
        if (v1 == null){
            v1 = 1;
        }
        if (v2 == null){
            v2 = 1;
        }
        return v1 > v2 ? v1 : v2;
    }
}
