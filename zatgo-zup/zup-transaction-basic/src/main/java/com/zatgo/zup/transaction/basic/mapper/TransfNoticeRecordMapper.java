package com.zatgo.zup.transaction.basic.mapper;

import com.zatgo.zup.transaction.basic.entity.account.TransfNoticeRecord;
import org.springframework.stereotype.Repository;

@Repository
public interface TransfNoticeRecordMapper extends BaseMapper<TransfNoticeRecord>{

    void saveNoticeRecord(TransfNoticeRecord record);
}