package com.zatgo.zup.transaction.basic.service;

import com.zatgo.zup.transaction.basic.entity.SendRequest;

import java.io.IOException;
import java.util.List;

/**
 * Created by 46041 on 2018/10/10.
 */
public interface CoinService {


    /**
     * 发送交易
     * @param sendRequest
     * @return
     */
    String sendTransaction(SendRequest sendRequest);

    /**
     * 扫描区块,并更新相关充值信息
     */
    void scanBlock() throws IOException;

    /**
     * 确认交易是否达到确认数
     */
    void checkTransaction() throws IOException;

    /**
     *  获取待提币交易
     */
    void GetBtcExtractRecord();

    void addAddress();

}
