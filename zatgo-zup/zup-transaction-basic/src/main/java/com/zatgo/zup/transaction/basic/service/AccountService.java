package com.zatgo.zup.transaction.basic.service;

import com.zatgo.zup.transaction.basic.entity.account.TransfAddressBalance;
import com.zatgo.zup.transaction.basic.entity.eth.TransactionTypeEnum;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 46041 on 2019/1/8.
 */
public interface AccountService {
    /**
     * 获取需要搜集的账户
     * @param amount
     * @param coinNetworkType
     * @param coinType
     * @return
     */
    List<TransfAddressBalance> getNeedCollectAccount(BigDecimal amount, String coinNetworkType, String coinType);

    /**
     * 获取地址的对应账户
     * @param coinNetworkType
     * @param coinType
     * @param address
     * @return
     */
    TransfAddressBalance getAddressAccount(String coinNetworkType, String coinType, String address);

    /**
     * 修改地址已确认余额
     * @param coinNetworkType
     * @param coinType
     * @param amount
     * @param address
     */
    void updateAddressConfirmed(String coinNetworkType, String coinType, BigDecimal amount,
                                String address, String txId);

    /**
     * 修改地址未确认余额
     * @param coinNetworkType
     * @param coinType
     * @param amount
     * @param address
     */
    void updateAddressUnconfirmed(String coinNetworkType, String coinType, BigDecimal amount,
                                  String address, String txId);

    /**
     * 提币扣费专用
     * @param coinNetworkType
     * @param coinType
     * @param amount
     * @param address
     */
    void updateBalanceExtract(String coinNetworkType, String coinType, BigDecimal amount, BigDecimal fee,
                                  String address, String txId);
}
