package com.zatgo.zup.transaction.basic.conf;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Component
public class JsonRpcClient {
	@Value("${transaction.user:}")
	private String userName;
	@Value("${transaction.password:}")
	private String passWord;
	@Value("${transaction.host:}")
	private String address;
	@Value("${transaction.type}")
	private String type;
	@Value("${transaction.port:}")
	private String port;


	private JsonRpcHttpClient client;

	@PostConstruct
	public void init() throws MalformedURLException {
		if ("qtum".equalsIgnoreCase(type)) {
			client = new JsonRpcHttpClient(new URL("http://" + address + ":"));
			String cred = Base64
					.encodeBase64String((userName + ":" + passWord).getBytes());
			Map<String, String> headers = new HashMap<String, String>(1);
			headers.put("Authorization", "Basic " + cred);
			client.setHeaders(headers);
		}
	}

	public JsonRpcHttpClient getClient() {
		return client;
	}

	public void setClient(JsonRpcHttpClient client) {
		this.client = client;
	}

	public <T> T invoke(String methodName, Object argument, Class<T> clazz) throws Throwable {
		return client.invoke(methodName,argument,clazz);
	}

	public <T> T invoke(String methodName, Object argument, Class<T> clazz,Map<String, String> extraHeaders) throws Throwable {
		return client.invoke(methodName, argument, clazz, extraHeaders);
	}


}
