//package com.zatgo.zup.transaction.basic.conf;
//
//import com.zatgo.zup.transaction.basic.entity.constant.CoinConstant;
//import com.zatgo.zup.transaction.basic.util.MongoUtil;
//import com.zatgo.zup.transaction.basic.util.qtum.QtumUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.stereotype.Component;
//import org.springframework.util.CollectionUtils;
//
//import javax.annotation.PostConstruct;
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by 46041 on 2018/10/25.
// */
//
//@Component
//@ConfigurationProperties(prefix="transaction.qtum")
//public class QtumAndContractConfig {
//
//    private List<Map<String, String>> config = new ArrayList<>();
//
//    private static Map<String, ContractConfig> contractConfig = null;
//
//    private static Map<String, ContractConfig> coinTypeConfig = null;
//    @Autowired
//    private QtumUtil qtumUtil;
//    @Autowired
//    private MongoUtil mongoUtil;
//
//
//    @Value("${transaction.confirmed:}")
//    private Integer qtumConfirmed;
//    @Value("${transaction.fee:}")
//    private BigDecimal fee;
//    @Value("${transaction.type:}")
//    private String type;
//
//    public Map<String, ContractConfig> getContractConfig(){
//        if (contractConfig == null){
//            init();
//        }
//        return contractConfig;
//    }
//
//    public Map<String, ContractConfig> getCoinTypeConfig(){
//        if (coinTypeConfig == null){
//            init();
//        }
//        return coinTypeConfig;
//    }
//
//    @PostConstruct
//    private void init(){
//        if ("qtum".equalsIgnoreCase(type)){
//            if (contractConfig == null){
//                if (!CollectionUtils.isEmpty(config)){
//                    contractConfig = new HashMap<>();
//                    coinTypeConfig = new HashMap<>();
//                    for (Map<String, String> conf : config){
////                        ContractConfig config = JSONObject.parseObject(JSONObject.toJSONString(conf), ContractConfig.class) ;
////                        config.setName(config.getName().toUpperCase());
////                        config.setContract(config.getContract());
////                        Address systemAddress = qtumUtil.getSystemAddress(config.getName());
////                        if (systemAddress != null){
////                            config.setSystemAddress(systemAddress.getAddress());
////                        } else {
////                            systemAddress = createTokenAddress(config.getName());
////                            config.setSystemAddress(systemAddress.getAddress());
////                        }
////                        Address systemServiceChargeAddress = qtumUtil.getSystemCollectAddress();
////                        config.setSystemServiceChargeAddress(systemServiceChargeAddress.getAddress());
//////                        config.setGasprice(qtumUtil.qtumAmountFormateSumN(new BigDecimal(config.getGasprice()), new BigInteger("9")).toBigInteger());
////                        contractConfig.put(config.getContract(), config);
////                        coinTypeConfig.put(config.getName(), config);
//                    }
//                }
//            }
//            putQtumConfig();
//        }
//    }
//
//    private void putQtumConfig(){
//        ContractConfig config = new ContractConfig();
//        config.setName("QTUM");
//        config.setConfirmed(qtumConfirmed);
//        config.setPrecision(CoinConstant.ETH_PRECISION);
//        contractConfig.put(config.getContract(), config);
//        coinTypeConfig.put(config.getName(), config);
//    }
//
//    public List<Map<String, String>> getConfig() {
//        return config;
//    }
//
//    public void setConfig(List<Map<String, String>> config) {
//        this.config = config;
//    }
//}
