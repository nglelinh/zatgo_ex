package com.zatgo.zup.transaction.basic.entity.account;

import java.math.BigDecimal;

public class TransfAddressBalance {
    private String id;

    private String address;

    private String coinNetworkType;

    private String coinType;

    private BigDecimal confirmed;

    private BigDecimal unconfirmed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getCoinNetworkType() {
        return coinNetworkType;
    }

    public void setCoinNetworkType(String coinNetworkType) {
        this.coinNetworkType = coinNetworkType == null ? null : coinNetworkType.trim();
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType == null ? null : coinType.trim();
    }

    public BigDecimal getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(BigDecimal confirmed) {
        this.confirmed = confirmed;
    }

    public BigDecimal getUnconfirmed() {
        return unconfirmed;
    }

    public void setUnconfirmed(BigDecimal unconfirmed) {
        this.unconfirmed = unconfirmed;
    }
}