package com.zatgo.zup.transaction.basic.entity;

import com.zatgo.zup.transaction.basic.entity.eth.TransactionTypeEnum;

import java.math.BigDecimal;

/**
 * Created by 46041 on 2019/1/29.
 */
public class SendRequest {

    private String toAddress;

    private String fromAddress;

    private BigDecimal amount;

    private String businessTxId;

    private String coinType;

    private BigDecimal gasLimit;

    private BigDecimal gasPrice;

    private BigDecimal fee;

    private Integer transfType;


    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBusinessTxId() {
        return businessTxId;
    }

    public void setBusinessTxId(String businessTxId) {
        this.businessTxId = businessTxId;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public BigDecimal getGasLimit() {
        return gasLimit;
    }

    public void setGasLimit(BigDecimal gasLimit) {
        this.gasLimit = gasLimit;
    }

    public BigDecimal getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(BigDecimal gasPrice) {
        this.gasPrice = gasPrice;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public Integer getTransfType() {
        return transfType;
    }

    public void setTransfType(Integer transfType) {
        this.transfType = transfType;
    }
}
