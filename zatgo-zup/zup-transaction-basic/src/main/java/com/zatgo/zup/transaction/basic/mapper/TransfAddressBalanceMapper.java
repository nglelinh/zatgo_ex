package com.zatgo.zup.transaction.basic.mapper;


import com.zatgo.zup.transaction.basic.entity.account.TransfAddressBalance;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface TransfAddressBalanceMapper extends BaseMapper<TransfAddressBalance>{


    /**
     * 修改余额
     * @param confirmed
     * @param unconfirmed
     */
    void updateBalance(@Param("confirmed") BigDecimal confirmed ,@Param("unconfirmed") BigDecimal unconfirmed ,@Param("id") String id);
}