package com.zatgo.zup.transaction.basic.entity.account;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TransfConfigExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TransfConfigExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNull() {
            addCriterion("coin_network_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIsNotNull() {
            addCriterion("coin_network_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeEqualTo(String value) {
            addCriterion("coin_network_type =", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotEqualTo(String value) {
            addCriterion("coin_network_type <>", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThan(String value) {
            addCriterion("coin_network_type >", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_network_type >=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThan(String value) {
            addCriterion("coin_network_type <", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_network_type <=", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeLike(String value) {
            addCriterion("coin_network_type like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotLike(String value) {
            addCriterion("coin_network_type not like", value, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeIn(List<String> values) {
            addCriterion("coin_network_type in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotIn(List<String> values) {
            addCriterion("coin_network_type not in", values, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeBetween(String value1, String value2) {
            addCriterion("coin_network_type between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinNetworkTypeNotBetween(String value1, String value2) {
            addCriterion("coin_network_type not between", value1, value2, "coinNetworkType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNull() {
            addCriterion("coin_type is null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIsNotNull() {
            addCriterion("coin_type is not null");
            return (Criteria) this;
        }

        public Criteria andCoinTypeEqualTo(String value) {
            addCriterion("coin_type =", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotEqualTo(String value) {
            addCriterion("coin_type <>", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThan(String value) {
            addCriterion("coin_type >", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeGreaterThanOrEqualTo(String value) {
            addCriterion("coin_type >=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThan(String value) {
            addCriterion("coin_type <", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLessThanOrEqualTo(String value) {
            addCriterion("coin_type <=", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeLike(String value) {
            addCriterion("coin_type like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotLike(String value) {
            addCriterion("coin_type not like", value, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeIn(List<String> values) {
            addCriterion("coin_type in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotIn(List<String> values) {
            addCriterion("coin_type not in", values, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeBetween(String value1, String value2) {
            addCriterion("coin_type between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andCoinTypeNotBetween(String value1, String value2) {
            addCriterion("coin_type not between", value1, value2, "coinType");
            return (Criteria) this;
        }

        public Criteria andFeeIsNull() {
            addCriterion("fee is null");
            return (Criteria) this;
        }

        public Criteria andFeeIsNotNull() {
            addCriterion("fee is not null");
            return (Criteria) this;
        }

        public Criteria andFeeEqualTo(BigDecimal value) {
            addCriterion("fee =", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotEqualTo(BigDecimal value) {
            addCriterion("fee <>", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThan(BigDecimal value) {
            addCriterion("fee >", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fee >=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThan(BigDecimal value) {
            addCriterion("fee <", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fee <=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeIn(List<BigDecimal> values) {
            addCriterion("fee in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotIn(List<BigDecimal> values) {
            addCriterion("fee not in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee not between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andGasPriceIsNull() {
            addCriterion("gas_price is null");
            return (Criteria) this;
        }

        public Criteria andGasPriceIsNotNull() {
            addCriterion("gas_price is not null");
            return (Criteria) this;
        }

        public Criteria andGasPriceEqualTo(BigDecimal value) {
            addCriterion("gas_price =", value, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceNotEqualTo(BigDecimal value) {
            addCriterion("gas_price <>", value, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceGreaterThan(BigDecimal value) {
            addCriterion("gas_price >", value, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("gas_price >=", value, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceLessThan(BigDecimal value) {
            addCriterion("gas_price <", value, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("gas_price <=", value, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceIn(List<BigDecimal> values) {
            addCriterion("gas_price in", values, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceNotIn(List<BigDecimal> values) {
            addCriterion("gas_price not in", values, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gas_price between", value1, value2, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gas_price not between", value1, value2, "gasPrice");
            return (Criteria) this;
        }

        public Criteria andGasLimitIsNull() {
            addCriterion("gas_limit is null");
            return (Criteria) this;
        }

        public Criteria andGasLimitIsNotNull() {
            addCriterion("gas_limit is not null");
            return (Criteria) this;
        }

        public Criteria andGasLimitEqualTo(BigDecimal value) {
            addCriterion("gas_limit =", value, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitNotEqualTo(BigDecimal value) {
            addCriterion("gas_limit <>", value, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitGreaterThan(BigDecimal value) {
            addCriterion("gas_limit >", value, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("gas_limit >=", value, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitLessThan(BigDecimal value) {
            addCriterion("gas_limit <", value, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitLessThanOrEqualTo(BigDecimal value) {
            addCriterion("gas_limit <=", value, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitIn(List<BigDecimal> values) {
            addCriterion("gas_limit in", values, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitNotIn(List<BigDecimal> values) {
            addCriterion("gas_limit not in", values, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gas_limit between", value1, value2, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andGasLimitNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gas_limit not between", value1, value2, "gasLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitIsNull() {
            addCriterion("collect_limit is null");
            return (Criteria) this;
        }

        public Criteria andCollectLimitIsNotNull() {
            addCriterion("collect_limit is not null");
            return (Criteria) this;
        }

        public Criteria andCollectLimitEqualTo(BigDecimal value) {
            addCriterion("collect_limit =", value, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitNotEqualTo(BigDecimal value) {
            addCriterion("collect_limit <>", value, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitGreaterThan(BigDecimal value) {
            addCriterion("collect_limit >", value, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("collect_limit >=", value, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitLessThan(BigDecimal value) {
            addCriterion("collect_limit <", value, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitLessThanOrEqualTo(BigDecimal value) {
            addCriterion("collect_limit <=", value, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitIn(List<BigDecimal> values) {
            addCriterion("collect_limit in", values, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitNotIn(List<BigDecimal> values) {
            addCriterion("collect_limit not in", values, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("collect_limit between", value1, value2, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andCollectLimitNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("collect_limit not between", value1, value2, "collectLimit");
            return (Criteria) this;
        }

        public Criteria andIsCollectIsNull() {
            addCriterion("is_collect is null");
            return (Criteria) this;
        }

        public Criteria andIsCollectIsNotNull() {
            addCriterion("is_collect is not null");
            return (Criteria) this;
        }

        public Criteria andIsCollectEqualTo(Boolean value) {
            addCriterion("is_collect =", value, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectNotEqualTo(Boolean value) {
            addCriterion("is_collect <>", value, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectGreaterThan(Boolean value) {
            addCriterion("is_collect >", value, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_collect >=", value, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectLessThan(Boolean value) {
            addCriterion("is_collect <", value, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectLessThanOrEqualTo(Boolean value) {
            addCriterion("is_collect <=", value, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectIn(List<Boolean> values) {
            addCriterion("is_collect in", values, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectNotIn(List<Boolean> values) {
            addCriterion("is_collect not in", values, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectBetween(Boolean value1, Boolean value2) {
            addCriterion("is_collect between", value1, value2, "isCollect");
            return (Criteria) this;
        }

        public Criteria andIsCollectNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_collect not between", value1, value2, "isCollect");
            return (Criteria) this;
        }

        public Criteria andFeeAddressIsNull() {
            addCriterion("fee_address is null");
            return (Criteria) this;
        }

        public Criteria andFeeAddressIsNotNull() {
            addCriterion("fee_address is not null");
            return (Criteria) this;
        }

        public Criteria andFeeAddressEqualTo(String value) {
            addCriterion("fee_address =", value, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressNotEqualTo(String value) {
            addCriterion("fee_address <>", value, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressGreaterThan(String value) {
            addCriterion("fee_address >", value, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressGreaterThanOrEqualTo(String value) {
            addCriterion("fee_address >=", value, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressLessThan(String value) {
            addCriterion("fee_address <", value, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressLessThanOrEqualTo(String value) {
            addCriterion("fee_address <=", value, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressLike(String value) {
            addCriterion("fee_address like", value, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressNotLike(String value) {
            addCriterion("fee_address not like", value, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressIn(List<String> values) {
            addCriterion("fee_address in", values, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressNotIn(List<String> values) {
            addCriterion("fee_address not in", values, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressBetween(String value1, String value2) {
            addCriterion("fee_address between", value1, value2, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andFeeAddressNotBetween(String value1, String value2) {
            addCriterion("fee_address not between", value1, value2, "feeAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressIsNull() {
            addCriterion("collect_receive_address is null");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressIsNotNull() {
            addCriterion("collect_receive_address is not null");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressEqualTo(String value) {
            addCriterion("collect_receive_address =", value, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressNotEqualTo(String value) {
            addCriterion("collect_receive_address <>", value, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressGreaterThan(String value) {
            addCriterion("collect_receive_address >", value, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressGreaterThanOrEqualTo(String value) {
            addCriterion("collect_receive_address >=", value, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressLessThan(String value) {
            addCriterion("collect_receive_address <", value, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressLessThanOrEqualTo(String value) {
            addCriterion("collect_receive_address <=", value, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressLike(String value) {
            addCriterion("collect_receive_address like", value, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressNotLike(String value) {
            addCriterion("collect_receive_address not like", value, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressIn(List<String> values) {
            addCriterion("collect_receive_address in", values, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressNotIn(List<String> values) {
            addCriterion("collect_receive_address not in", values, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressBetween(String value1, String value2) {
            addCriterion("collect_receive_address between", value1, value2, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andCollectReceiveAddressNotBetween(String value1, String value2) {
            addCriterion("collect_receive_address not between", value1, value2, "collectReceiveAddress");
            return (Criteria) this;
        }

        public Criteria andPrecisionIsNull() {
            addCriterion("precision is null");
            return (Criteria) this;
        }

        public Criteria andPrecisionIsNotNull() {
            addCriterion("precision is not null");
            return (Criteria) this;
        }

        public Criteria andPrecisionEqualTo(Integer value) {
            addCriterion("precision =", value, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionNotEqualTo(Integer value) {
            addCriterion("precision <>", value, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionGreaterThan(Integer value) {
            addCriterion("precision >", value, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionGreaterThanOrEqualTo(Integer value) {
            addCriterion("precision >=", value, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionLessThan(Integer value) {
            addCriterion("precision <", value, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionLessThanOrEqualTo(Integer value) {
            addCriterion("precision <=", value, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionIn(List<Integer> values) {
            addCriterion("precision in", values, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionNotIn(List<Integer> values) {
            addCriterion("precision not in", values, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionBetween(Integer value1, Integer value2) {
            addCriterion("precision between", value1, value2, "precision");
            return (Criteria) this;
        }

        public Criteria andPrecisionNotBetween(Integer value1, Integer value2) {
            addCriterion("precision not between", value1, value2, "precision");
            return (Criteria) this;
        }

        public Criteria andConfirmedIsNull() {
            addCriterion("confirmed is null");
            return (Criteria) this;
        }

        public Criteria andConfirmedIsNotNull() {
            addCriterion("confirmed is not null");
            return (Criteria) this;
        }

        public Criteria andConfirmedEqualTo(Integer value) {
            addCriterion("confirmed =", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedNotEqualTo(Integer value) {
            addCriterion("confirmed <>", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedGreaterThan(Integer value) {
            addCriterion("confirmed >", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedGreaterThanOrEqualTo(Integer value) {
            addCriterion("confirmed >=", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedLessThan(Integer value) {
            addCriterion("confirmed <", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedLessThanOrEqualTo(Integer value) {
            addCriterion("confirmed <=", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedIn(List<Integer> values) {
            addCriterion("confirmed in", values, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedNotIn(List<Integer> values) {
            addCriterion("confirmed not in", values, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedBetween(Integer value1, Integer value2) {
            addCriterion("confirmed between", value1, value2, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedNotBetween(Integer value1, Integer value2) {
            addCriterion("confirmed not between", value1, value2, "confirmed");
            return (Criteria) this;
        }

        public Criteria andContractIsNull() {
            addCriterion("contract is null");
            return (Criteria) this;
        }

        public Criteria andContractIsNotNull() {
            addCriterion("contract is not null");
            return (Criteria) this;
        }

        public Criteria andContractEqualTo(String value) {
            addCriterion("contract =", value, "contract");
            return (Criteria) this;
        }

        public Criteria andContractNotEqualTo(String value) {
            addCriterion("contract <>", value, "contract");
            return (Criteria) this;
        }

        public Criteria andContractGreaterThan(String value) {
            addCriterion("contract >", value, "contract");
            return (Criteria) this;
        }

        public Criteria andContractGreaterThanOrEqualTo(String value) {
            addCriterion("contract >=", value, "contract");
            return (Criteria) this;
        }

        public Criteria andContractLessThan(String value) {
            addCriterion("contract <", value, "contract");
            return (Criteria) this;
        }

        public Criteria andContractLessThanOrEqualTo(String value) {
            addCriterion("contract <=", value, "contract");
            return (Criteria) this;
        }

        public Criteria andContractLike(String value) {
            addCriterion("contract like", value, "contract");
            return (Criteria) this;
        }

        public Criteria andContractNotLike(String value) {
            addCriterion("contract not like", value, "contract");
            return (Criteria) this;
        }

        public Criteria andContractIn(List<String> values) {
            addCriterion("contract in", values, "contract");
            return (Criteria) this;
        }

        public Criteria andContractNotIn(List<String> values) {
            addCriterion("contract not in", values, "contract");
            return (Criteria) this;
        }

        public Criteria andContractBetween(String value1, String value2) {
            addCriterion("contract between", value1, value2, "contract");
            return (Criteria) this;
        }

        public Criteria andContractNotBetween(String value1, String value2) {
            addCriterion("contract not between", value1, value2, "contract");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressIsNull() {
            addCriterion("system_send_address is null");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressIsNotNull() {
            addCriterion("system_send_address is not null");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressEqualTo(String value) {
            addCriterion("system_send_address =", value, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressNotEqualTo(String value) {
            addCriterion("system_send_address <>", value, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressGreaterThan(String value) {
            addCriterion("system_send_address >", value, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressGreaterThanOrEqualTo(String value) {
            addCriterion("system_send_address >=", value, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressLessThan(String value) {
            addCriterion("system_send_address <", value, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressLessThanOrEqualTo(String value) {
            addCriterion("system_send_address <=", value, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressLike(String value) {
            addCriterion("system_send_address like", value, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressNotLike(String value) {
            addCriterion("system_send_address not like", value, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressIn(List<String> values) {
            addCriterion("system_send_address in", values, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressNotIn(List<String> values) {
            addCriterion("system_send_address not in", values, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressBetween(String value1, String value2) {
            addCriterion("system_send_address between", value1, value2, "systemSendAddress");
            return (Criteria) this;
        }

        public Criteria andSystemSendAddressNotBetween(String value1, String value2) {
            addCriterion("system_send_address not between", value1, value2, "systemSendAddress");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}