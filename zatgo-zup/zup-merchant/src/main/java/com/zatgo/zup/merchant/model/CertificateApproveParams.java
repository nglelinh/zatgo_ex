package com.zatgo.zup.merchant.model;

import io.swagger.annotations.ApiModelProperty;

public class CertificateApproveParams {

	@ApiModelProperty(value="用户ID",required=true)
	private String userId;
	@ApiModelProperty(value="是否审批通过",required=true)
	private Boolean isApprovePass;
	@ApiModelProperty(value="审批失败原因",required=true)
	private String approveDemo;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Boolean getIsApprovePass() {
		return isApprovePass;
	}
	public void setIsApprovePass(Boolean isApprovePass) {
		this.isApprovePass = isApprovePass;
	}
	public String getApproveDemo() {
		return approveDemo;
	}
	public void setApproveDemo(String approveDemo) {
		this.approveDemo = approveDemo;
	}
	
}
