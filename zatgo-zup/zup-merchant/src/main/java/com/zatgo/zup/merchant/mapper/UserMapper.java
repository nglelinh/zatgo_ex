package com.zatgo.zup.merchant.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zatgo.zup.common.model.UserBasicData;
import com.zatgo.zup.merchant.entity.User;

@Mapper
public interface UserMapper  extends BaseMapper<User>{

//    @Select("select * from user where user_name = #{userName}")
    User selectByUserName(@Param("userName") String userName, @Param("cloudUserId") String cloudUserId);
    
    @Select("select * from user where mnemonic_word = #{mw}")
    User selectByMW(@Param("mw") String mw);
    
    @Update("update user set last_login_date = #{lastLoginDate} where user_id = #{userId}")
    void updateLastLoginTime(@Param("userId") String userId,@Param("lastLoginDate") Date lastLoginDate);
    
    @Update("update user set user_password = #{loginPassword} where user_id = #{userId}")
    void updateLoginPassword(@Param("userId") String userId,@Param("loginPassword") String loginPassword);
    
    @Update("update user set user_password = #{loginPassword} where user_name = #{userName}")
    void updateLoginPasswordByUserName(@Param("userName") String userName,@Param("loginPassword") String loginPassword);
    
    @Update("update user set user_pay_password = #{payPassword} where user_id = #{userId}")
    void updatePayPassword(@Param("userId") String userId,@Param("payPassword") String payPassword);

    /**
     * 批量修数据(邀请码)
     * @param list
     * @return
     */
    int updateInvitationCode(List<User> list);
    
    List<UserBasicData> selectInviteList(String inviteCode);
}