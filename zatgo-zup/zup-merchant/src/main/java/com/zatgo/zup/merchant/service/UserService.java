package com.zatgo.zup.merchant.service;

import java.util.List;

import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.UserBasicData;
import com.zatgo.zup.common.model.UserBasicParams;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.merchant.entity.Certificate;
import com.zatgo.zup.merchant.entity.User;
import com.zatgo.zup.merchant.entity.UserExt;
import com.zatgo.zup.merchant.model.CertificateApproveParams;
import com.zatgo.zup.merchant.model.CertificateParams;
import com.zatgo.zup.merchant.model.UserFollowParams;

public interface UserService {

	/**
	 * 根据用户名查询用户信息
	 * @param userName
	 * @return
	 */
	public User getUser(String userName, String cloudUserId);
	
	/**
	 * 根据用户ID更新登录时间
	 * @param userId
	 */
	public void updateLastLoginTime(String userId);
	
	/**
	 * 根据ID查询用户信息
	 * @param id
	 * @return
	 */
	public User getUserById(String userId);
	
	/**
	 * 创建用户
	 * @param userData
	 */
	public String createUser(UserData userData);

	/**
	 * 注册创建用户
	 * @param userData
	 */
	public String registerCreateUser(UserData userData, String deviceNo, String loginName);
	
	/**
	 * 修改登录密码
	 * @param userId
	 * @param loginPassword
	 */
	public void editLoginPassword(String userId,String loginPassword);
	
	/**
	 * 修改登录密码
	 * @param userName
	 * @param loginPassword
	 */
	public void editLoginPasswordByUserName(String userName,String loginPassword);
	
	/**
	 * 修改支付密码
	 * @param userId
	 * @param payPassword
	 */
	public void editPayPassword(String userId,String payPassword);
	
	/**
	 * 通过助记词查询用户
	 * @param mw
	 */
	public User getUserByMW(String mw);

	/**
	 * 根据邀请码查用户信息
	 * @param invitationCode
	 * @return
	 */
	List<User> getUserByInvitationCode(String invitationCode);

	/**
	 * 根据邀请码查用户ID
	 * @param invitationCode
	 * @return
	 */
	String getUserIdByInvitationCode(String invitationCode);

	/**
	 * 根据手机号码或者助记词或者电子邮箱查用户
	 * @param parameter
	 * @param type
	 * @return
	 */
	User getUserByMobileOrEmailOrMnemonicWord(String parameter, String type, String cloudUserId);

	/**
	 * 俢验证码数据专用接口
	 * @return
	 */
	String updateInvitationCode();
	
	/**
	 * 修改用户基础信息
	 * @param params
	 */
	void updateUserBasicsInfo(UserBasicParams params, String userId);
	
	/**
	 * 微信关注
	 * @param params
	 */
	void addUserWechatFollow(UserFollowParams params);
	
	/**
	 * 邀请好友列表
	 * @param inviteCode 邀请码
	 * @return
	 */
	List<UserBasicData> getInviteList(String inviteCode);
	
	/**
	 * 获取用户邀请码图片
	 * @param userId
	 * @param inviteCode
	 * @return
	 */
	String getInviteImg(String userId, String inviteCode, String imgName);
	
	/**
	 * 根据用户名与云ID查询用户扩展信息
	 * @param userName
	 * @param cloudUserId
	 * @return
	 */
	UserExt getUserExt(String userName, String cloudUserId);
	
	/**
	 * 根据用户id查询用户扩展信息
	 * @param userId
	 * @return
	 */
	UserExt getUserExtByUserId(String userId);
	
	/**
	 * 申请实名认证
	 * @param params
	 * @return
	 */
	Boolean updateCertificate(CertificateParams params,String userId);
	
	/**
	 * 根据状态分页查询所有实名认证申请，如果状态为null，则根据分页查询所有
	 * @param status
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PageInfo<Certificate> getAllCertificateByStatus(BusinessEnum.UserCertificateStatus status,String pageNo, String pageSize);

	/**
	 * 查询指定用户的实名认证信息
	 * @param userId
	 * @return
	 */
	Certificate getCertificateByUserId(String userId);
	
	/**
	 * 实名认证审批
	 */
	Boolean approveCertificate(AuthUserInfo adminUser,CertificateApproveParams params);

	User getExistUserByWechatOpenId(String wxOpenId, String cloudUserId);
}
