package com.zatgo.zup.merchant.model;
import io.swagger.annotations.ApiModelProperty;

public class RegisterParams {
	@ApiModelProperty(value="登录手机号或邮箱地址",required=false)
	private String loginName;

	@ApiModelProperty(value="国际区号",required=true)
	private String countryCode;

	@ApiModelProperty(value="注册类型",required=true)
	private String registType;
	
	@ApiModelProperty(value="登录密码",required=true)
	private String loginPassword;
	
	@ApiModelProperty(value="交易密码",required=true)
	private String payPassword;
	
	@ApiModelProperty(value="实名认证验证码（手机短信或邮件）",required=true)
	private String readNameAuthCode;
	
	@ApiModelProperty(value="助记词",required=false,hidden=true)
	private String mnemonicWord;

	@ApiModelProperty(value="云用户Id",required=true)
	private String cloudUserId;

	@ApiModelProperty(value="是否为云管理员 0否1是",required=false)
	private String isCloudManager;

	@ApiModelProperty(value="邀请码",required=false)
	private String inviteCode;

	public String getMnemonicWord() {
		return mnemonicWord;
	}
	public void setMnemonicWord(String mnemonicWord) {
		this.mnemonicWord = mnemonicWord;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginPassword() {
		return loginPassword;
	}
	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}
	public String getPayPassword() {
		return payPassword;
	}
	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}
	public String getReadNameAuthCode() {
		return readNameAuthCode;
	}
	public void setReadNameAuthCode(String readNameAuthCode) {
		this.readNameAuthCode = readNameAuthCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getRegistType() {
		return registType;
	}

	public void setRegistType(String registType) {
		this.registType = registType;
	}

	public String getCloudUserId() {
		return cloudUserId;
	}

	public void setCloudUserId(String cloudUserId) {
		this.cloudUserId = cloudUserId;
	}

	public String getIsCloudManager() {
		return isCloudManager;
	}

	public void setIsCloudManager(String isCloudManager) {
		this.isCloudManager = isCloudManager;
	}

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}
}