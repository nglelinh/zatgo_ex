//package com.zatgo.zup.merchant.service.impl;
//
//import com.ykb.mall.common.model.UmsMemberLevel;
//import com.ykb.mall.common.model.UmsMemberLevelExample;
//import com.zatgo.zup.merchant.mapper.UmsMemberLevelMapper;
//import com.zatgo.zup.merchant.service.UmsMemberLevelService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
///**
// * 会员等级管理Service实现类
// * Created by chen on 2018/4/26.
// */
//@Service
//public class UmsMemberLevelServiceImpl implements UmsMemberLevelService {
//    @Autowired
//    private UmsMemberLevelMapper memberLevelMapper;
//    @Override
//    public List<UmsMemberLevel> list(Integer defaultStatus) {
//        UmsMemberLevelExample example = new UmsMemberLevelExample();
//        example.createCriteria().andDefaultStatusEqualTo(defaultStatus);
//        return memberLevelMapper.selectByExample(example);
//    }
//}
