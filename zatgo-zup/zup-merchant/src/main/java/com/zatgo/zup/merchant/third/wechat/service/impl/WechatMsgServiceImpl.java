package com.zatgo.zup.merchant.third.wechat.service.impl;

import com.zatgo.zup.merchant.third.wechat.model.msg.MsgToken;
import com.zatgo.zup.merchant.third.wechat.model.msg.WechatMsg;
import com.zatgo.zup.merchant.third.wechat.service.IWechatMsgService;
import com.zatgo.zup.merchant.third.wechat.util.HashUtil;
import com.zatgo.zup.merchant.third.wechat.util.Xml2BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@SuppressWarnings("ALL")
@Service
public class WechatMsgServiceImpl implements IWechatMsgService {

	private Logger log = LoggerFactory.getLogger(getClass());

//	@Value("${wx.token}")
//	private String wxToken;

	@Override
	public MsgToken validateWxToken(MsgToken token) {

		String signature = token.getSignature();
		String timestamp = token.getTimestamp();
		String nonce = token.getNonce();
//		List<String> str = Arrays.asList(wxToken, timestamp, nonce);
		List<String> str = Arrays.asList(null, timestamp, nonce);
		Collections.sort(str);
		StringBuffer data = new StringBuffer();
		for (String string : str) {
			data.append(string);
		}

		String hashcode = HashUtil.hash(data.toString(), "SHA-1");
		if (!hashcode.equals(signature)) {
			token.setEchostr("");
		}

		return token;
	}

	@Override
	public String wxMsg(WechatMsg msg) {
		try {
			String msgType = msg.getMsgType();
			if (WechatMsg.MsgType.TEXT.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.IMAGE.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.VOICE.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.VIDEO.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.SHORTVIDEO.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.LOCATION.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.LINK.getType().equals(msgType)) {
				return "success";
			} else if (WechatMsg.MsgType.EVENT.getType().equals(msgType)) {
				return wechatEventMsg(msg);
			}

		} catch (Exception e) {
			log.error("微信消息处理失败", e);
		}
		return "success";
	}

	private String wechatEventMsg(WechatMsg msg) {
		WechatMsg res = null;
		String event = msg.getEvent();
		if (WechatMsg.Event.SUBSCRIBE.getEvent().equals(event)) {
			res = new WechatMsg();
			res.setToUserName(msg.getFromUserName());
			res.setFromUserName(msg.getToUserName());
			res.setCreateTime(String.valueOf(new Date().getTime()));
			res.setMsgType(WechatMsg.MsgType.TEXT.getType());
//			res.setContent("欢迎关注飞巴商旅。\r\n" + "飞巴，和你一起定义简单商旅。");
		} else if (WechatMsg.Event.UNSUBSCRIBE.getEvent().equals(event)) {
			return "success";
		} else if (WechatMsg.Event.SCAN.getEvent().equals(event)) {
			return "success";
		} else if (WechatMsg.Event.LOCATION.getEvent().equals(event)) {
			return "success";
		} else if (WechatMsg.Event.CLICK.getEvent().equals(event)) {
			return "success";
		} else if (WechatMsg.Event.VIEW.getEvent().equals(event)) {
			return "success";
		}

		String str = null;
		if (res == null) {
			str = "success";
		} else {
			str = Xml2BeanUtils.convertToXml(res, "UTF-8");
		}
		return str;
	}

}
