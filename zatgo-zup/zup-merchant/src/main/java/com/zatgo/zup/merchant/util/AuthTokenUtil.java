package com.zatgo.zup.merchant.util;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;


@Component
public class AuthTokenUtil {
	
	private Logger logger = LoggerFactory.getLogger(AuthTokenUtil.class);

    @Autowired
    private RedisTemplate redisTemplate;
    
    @Value("${auth.token.secret}")
	private String tokenSecret;

	@Value("${auth.token.expire}")
	private Integer tokenExpire;
    
    private JWTSigner signer;
    private JWTVerifier jwtVerifier;
    
    private static Lock lock = new ReentrantLock();
    
    public JWTSigner getSigner() {
    	
    	if(signer==null){
    		lock.lock();
    		try {
    			signer = new JWTSigner(tokenSecret);
    		}catch(Exception e) {
    			logger.error("",e);
    		}finally {
    			lock.unlock();
    		}
    		
    	}
		return signer;
	}

	public JWTVerifier getJwtVerifier() {
		if(jwtVerifier==null){
			lock.lock();
    		try {
    			jwtVerifier = new JWTVerifier(tokenSecret);
    		}catch(Exception e) {
    			logger.error("",e);
    		}finally {
    			lock.unlock();
    		}
			
		}
		return jwtVerifier;
	}



	/**
	 * 生成登录的token信息
	 * @param authUserInfo
	 * @return
	 */
	public String generateWechatLoginToken(AuthUserInfo authUserInfo) {

		Map<String, Object> tokenParams = new HashMap<String, Object>();
		String redisKey = authUserInfo.getCloudUserId() + authUserInfo.getWechatOpenId();
		tokenParams.put("userName", redisKey);
		tokenParams.put("ts", new Date());

		/**
		 *  产生Token
		 */
		java.security.Security.addProvider(new com.sun.crypto.provider.SunJCE());
		String userToken = getSigner().sign(tokenParams);

		/**
		 * 存入redis
		 */
		String key = RedisKeyConstants.AUTH_TOKEN_PRE+userToken;
		redisTemplate.opsForValue().set(key, authUserInfo);
		redisTemplate.expire(key, tokenExpire,TimeUnit.SECONDS);
		redisTemplate.opsForValue().set(redisKey, userToken);
		redisTemplate.expire(redisKey, tokenExpire,TimeUnit.SECONDS);
		return userToken;

	}

	/**
	 * 生成登录的token信息
	 * @param authUserInfo
	 * @return
	 */
    public String generateLoginToken(AuthUserInfo authUserInfo) {
    	
        Map<String, Object> tokenParams = new HashMap<String, Object>();

        tokenParams.put("userName", authUserInfo.getUserName());
        tokenParams.put("ts", new Date());

        /**
         *  产生Token
         */
        java.security.Security.addProvider(new com.sun.crypto.provider.SunJCE());
        String userToken = getSigner().sign(tokenParams);
        
        /**
         * 存入redis
         */
        String key = RedisKeyConstants.AUTH_TOKEN_PRE+userToken;
        redisTemplate.opsForValue().set(key, authUserInfo);
        redisTemplate.expire(key, tokenExpire,TimeUnit.SECONDS);
        return userToken;

    }
    
    /**
     * 失效登录的TOKEN
     * @param userToken
     */
    public void removeToken(String userToken) {
    	redisTemplate.delete(RedisKeyConstants.AUTH_TOKEN_PRE+userToken);
	}
    
    /**
     * 登录的token验证
     * @param token
     * @return
     */
    public boolean loginVerify(String userToken){
    	if (StringUtils.isEmpty(userToken))
            return false;
    	
    	try {
    		Map<String, Object> authUserMap = getJwtVerifier().verify(userToken);
    		
    		AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+userToken);
    		
    		if(authUserInfo == null) {
    			return false;
    		}
    		
    		String authUserInfoKey = new StringBuffer().append(authUserInfo.getUserName()).toString();
    		String authUserMapKey = new StringBuffer().append(authUserMap.get("userName")).toString();
    		
    		if(!authUserInfoKey.equals(authUserMapKey)){
    			return false;
    		}
    		
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| IllegalStateException | SignatureException | IOException
				| JWTVerifyException e) {
			logger.error(BusinessExceptionCode.TOKEN_VERIFY_FAIL,e);
			return false;
		}catch (Exception e) {
			logger.error(BusinessExceptionCode.TOKEN_VERIFY_FAIL,e);
			return false;
		}
    	
    	return true;
    }
    
    /**
     * 根据TOKEN获取用户信息
     * @param userToken
     * @return
     */
    public AuthUserInfo getAuthUserInfo(String userToken) {
    	if(StringUtils.isEmpty(userToken)){
    		return null;
    	}

    	AuthUserInfo authInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+userToken);
    	if(authInfo == null){
    		throw new BusinessException(BusinessExceptionCode.TOKEN_VERIFY_FAIL);
    	}
    	return authInfo;
    }

    /**
     * 刷新token时间
     * @param token
     */
	public void refreshLoginExpire(String userToken) {
		String key = RedisKeyConstants.AUTH_TOKEN_PRE+userToken;
		if(redisTemplate.hasKey(key)){
			redisTemplate.expire(key, tokenExpire,TimeUnit.SECONDS);
		}
	}
	
	
	/**
     * 检测设备是否异常(注册)
     * @param request
     * @return
     */
    public boolean checkRegistIsAbnormal(String clientIp,String deviceNo,String loginName) {

    	String key = null;
    	if(deviceNo != null) {
    		key = RedisKeyConstants.GATEWAY_LIMIT_DEVICE_NO_REGIST_PRE + deviceNo;
    		
    		Object registList = redisTemplate.opsForHash().keys(key);
        	if(registList != null) {
        		Set registHash = (Set)registList;
            	if(registHash.size() > 1) {
            		logger.error("registHash=" + JSON.toJSONString(registHash));
            		logger.error("regist: clientIp=" + clientIp + " deviceNo=" + deviceNo  + " userName=" + loginName);
            		return false;
            	}
        	}
        	
    		redisTemplate.opsForHash().put(key, loginName, null);
    		redisTemplate.expire(key, 1*24*60*1000,TimeUnit.SECONDS);
        	
    	}

    	return true;
    }
}
