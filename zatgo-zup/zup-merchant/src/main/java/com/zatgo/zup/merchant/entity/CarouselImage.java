package com.zatgo.zup.merchant.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "轮播图")
public class CarouselImage {
	
	@ApiModelProperty(value = "轮播图id", required = false, hidden = true)
    private String id;

	@ApiModelProperty(value = "云用户id", required = false, hidden = true)
    private String cloudUserId;

	@ApiModelProperty(value = "图片地址", required = true)
    private String imageUrl;

	@ApiModelProperty(value = "位置", required = false, hidden = true)
    private Byte siteType;

	@ApiModelProperty(value = "排序", required = false, hidden = true)
    private Integer sort;

	@ApiModelProperty(value = "点击跳转地址", required = true)
    private String clickEventAddress;

	@ApiModelProperty(value = "点击事件 0：app内跳转 1：h5跳转 2：无法点击", required = true)
    private Byte clickEventType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    public Byte getSiteType() {
        return siteType;
    }

    public void setSiteType(Byte siteType) {
        this.siteType = siteType;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getClickEventAddress() {
        return clickEventAddress;
    }

    public void setClickEventAddress(String clickEventAddress) {
        this.clickEventAddress = clickEventAddress == null ? null : clickEventAddress.trim();
    }

    public Byte getClickEventType() {
        return clickEventType;
    }

    public void setClickEventType(Byte clickEventType) {
        this.clickEventType = clickEventType;
    }
}