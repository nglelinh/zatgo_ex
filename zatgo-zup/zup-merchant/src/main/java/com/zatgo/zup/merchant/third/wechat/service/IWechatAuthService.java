package com.zatgo.zup.merchant.third.wechat.service;


import com.zatgo.zup.merchant.model.LoginData;
import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfig;
import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfigParams;

public interface IWechatAuthService {
	

	/**
	 * 
	 * @param code
	 * @param appid
	 * @return
	 */
	LoginData wxAuth(String code, String appid);
	
	WechatConfig getWechatConfig(WechatConfigParams params);

}
