package com.zatgo.zup.merchant.entity;


import java.util.List;

/**
 * APP版本信息返回参数
 * @author chenlei
 *
 */
public class AppVersionData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6070915886728767379L;
	
	/**
	 * 最新版本
	 */
	private String version;
	
	/**
	 * 是否必须更新
	 */
	private String force;
	
	/**
	 * 安卓版本下载地址
	 */
	private String androidUrl;
	
	/**
	 * IOS版本下载地址
	 */
	private String iosUrl;
	


	private Byte isCustom;

	private Long customId;

	public Byte getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(Byte isCustom) {
		this.isCustom = isCustom;
	}

	public Long getCustomId() {
		return customId;
	}

	public void setCustomId(Long customId) {
		this.customId = customId;
	}

	public String getAndroidUrl() {
		return androidUrl;
	}

	public void setAndroidUrl(String androidUrl) {
		this.androidUrl = androidUrl;
	}

	public String getIosUrl() {
		return iosUrl;
	}

	public void setIosUrl(String iosUrl) {
		this.iosUrl = iosUrl;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getForce() {
		return force;
	}

	public void setForce(String force) {
		this.force = force;
	}


	
}
