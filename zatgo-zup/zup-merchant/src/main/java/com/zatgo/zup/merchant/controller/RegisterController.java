package com.zatgo.zup.merchant.controller;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.merchant.remoteservice.TaskRemoteService;
import org.apache.commons.lang3.RandomStringUtils;
import org.bitcoinj.wallet.DeterministicSeed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.email.MailService;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AliyunValidateCode;
import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.common.model.Country;
import com.zatgo.zup.common.model.CreateAccountParams;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.SendEmailAuthCodeRequest;
import com.zatgo.zup.common.model.SendPhoneAuthCodeRequest;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.common.sms.santi.SantiService;
import com.zatgo.zup.merchant.conf.CountryConfig;
import com.zatgo.zup.merchant.entity.CloudUser;
import com.zatgo.zup.merchant.entity.User;
import com.zatgo.zup.merchant.model.RegisterParams;
import com.zatgo.zup.merchant.remoteservice.AccountRemoteService;
import com.zatgo.zup.merchant.service.CloudUserService;
import com.zatgo.zup.merchant.service.UserService;
import com.zatgo.zup.merchant.util.AliyunValidataCodeUtil;
import com.zatgo.zup.merchant.util.AuthTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/auth")
@RestController
public class RegisterController extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private SantiService santiService;

	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	protected UserService userService;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@Autowired
	protected CloudUserService cloudUserService;

	@Autowired
	private TaskRemoteService taskRemoteService;
	

	@Autowired
	private AccountRemoteService accountRemoteService;
	@Autowired
	private AliyunValidataCodeUtil aliyunValidataCodeUtil;

	@Value("${notice.register.realnameauth.template.id:}")
	private String registerRealNameAuthTemplateId;

	@Value("${notice.register.realnameauth.template.content:}")
	private String registerRealNameAuthTemplateContent;

	@Value("${notice.register.realnameauth.template.timeout:}")
	private String registerRealNameAuthTemplateTimeout;
	
	@Value("${system.isSliderVerification:true}")
	private Boolean isSliderVerification;
	
	@Autowired
	private AuthTokenUtil authTokenUtil;
 

	//实名验证码 5分钟
	private static final Long REAL_NAME_AUTH_CODE_TIME_OUT = 300000L;
	//实名验证码短信发送间隔
	private static final Long SEND_SMS_INTERVAL = 60000L;
	//实名验证码短信一天限制次数
	private static final Long SEND_SMS_DAY_NUM_LIMIT = 5L;

	@ApiOperation(value="注册确认接口")
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Boolean> register(@RequestBody RegisterParams params){

		//支付密码校验
		String regExPay = "^\\d{6}$";
		Pattern patternPay = Pattern.compile(regExPay);
		Matcher matcherPayPass = patternPay.matcher(params.getPayPassword());
		if (!matcherPayPass.matches()){
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REQUEST_PARAM_ERROR,"支付密码应为6位数字");
		}
		String registType = params.getRegistType();
		String cloudUserId = params.getCloudUserId();

		//兼容老版本
		if (StringUtils.isEmpty(cloudUserId)){
			cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
		}
		if (StringUtils.isEmpty(registType)) {
			if(!StringUtils.isEmpty(params.getLoginName())) {
				registType = "0";
			}else if(!StringUtils.isEmpty(params.getMnemonicWord())) {
				registType = "2";
			}
		}
		String userId = null;
		if("0".equals(registType) || "1".equals(registType)) {
			//校验密码的合法性
			String regEx = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
			Pattern pattern = Pattern.compile(regEx);
			Matcher matcherLoginPass = pattern.matcher(params.getLoginPassword());
			if (!matcherLoginPass.matches()){
				return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REQUEST_PARAM_ERROR,"登陆密码应为6-20位字母数字组合,区分大小写");
			}

			//实名认证码（手机或邮件确认码）
			String realNameAuthCodekey = params.getLoginName();
			String realNameAuthCode = (String)redisTemplate.opsForValue().get(realNameAuthCodekey);
			if(realNameAuthCode == null || !realNameAuthCode.equals(params.getReadNameAuthCode())) {
				return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REAL_NAME_AUTH_FIAL);
			}
			UserData userData = new UserData();
			userData.setUserName(params.getLoginName());
			userData.setUserPassword(params.getLoginPassword());
			userData.setUserPayPassword(params.getPayPassword());
			userData.setRegistType(new Byte(registType));
			userData.setCloudUserId(cloudUserId);
			userData.setIsCloudManager(new Byte("0"));
			userData.setInvitedCode(params.getInviteCode());
			String countryCode = params.getCountryCode();
			if ("0".equals(registType)){
				userData.setMobile(params.getLoginName());
				userData.setMobileCountryCode(countryCode == null ? "86" : countryCode);
			} else {
				userData.setEmail(params.getLoginName());
				userData.setEmailCountryCode(countryCode == null ? "86" : countryCode);
			}
			
			String key = RedisKeyConstants.USER_INVITATION_CODE;
			redisLockUtils.lock(key);
			try {
				User existUser = userService.getUserByMobileOrEmailOrMnemonicWord(userData.getUserName(), registType, cloudUserId);
				if(existUser != null) {
					throw new BusinessException(BusinessExceptionCode.USER_IS_EXIST);
				}
				userId = userService.registerCreateUser(userData, getDeviceNo(), params.getLoginName());
			}finally {
				redisLockUtils.releaseLock(key);
			}

		}else {
			params.setLoginName(UUID.randomUUID().toString().replace("-", ""));
			params.setLoginPassword(UUID.randomUUID().toString().replace("-", ""));
			if(params.getMnemonicWord() == null || params.getMnemonicWord().trim().equals("")) {
				logger.error("助记词为空");
				return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REQUEST_PARAM_ERROR,"生成钱包失败");
			}

			String key = RedisKeyConstants.BUSI_USER + params.getMnemonicWord();
			redisLockUtils.lock(key);
			try {
				User existUser = userService.getUserByMW(params.getMnemonicWord());
				if(existUser != null) {
					throw new BusinessException(BusinessExceptionCode.USER_IS_EXIST);
				}

				UserData userData = new UserData();
				userData.setUserName(params.getLoginName());
				userData.setUserPassword(params.getLoginPassword());
				userData.setUserPayPassword(params.getPayPassword());
				userData.setMnemonicWord(params.getMnemonicWord());
				userData.setRegistType(new Byte(registType));
				userData.setCloudUserId(cloudUserId);
				userId = userService.registerCreateUser(userData, getDeviceNo(), params.getLoginName());
				
			}finally {
				redisLockUtils.releaseLock(key);
			}

		}
		
		//创建默认账户
		//设置默认账户可以失败
		if (CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID.equals(cloudUserId)) {
			try {
				CreateAccountParams cap = new CreateAccountParams();
				cap.setCurrencyType("ZAT");
				cap.setName("ZAT");
				cap.setNetworkType("ETH");
				cap.setUserId(userId);
				accountRemoteService.innerCreateAccount(cap);
			} catch (Exception e) {
				logger.error("", e);
			}
		}

		//注册时送币
		try {
			if (!StringUtils.isEmpty(userId)){
				taskRemoteService.registerTaskDone(userId, cloudUserId, UUIDUtils.getUuid());
			}
		} catch (Exception e){
			logger.error("注册送币失败", e);
		}

		return BusinessResponseFactory.createSuccess(true);
	}

	@ApiOperation(value="获取新的助记词")
	@RequestMapping(value = "/getNewMnemonicWord", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<String> getNewMnemonicWord(){
		String passphrase = "";
		SecureRandom secureRandom = new SecureRandom();
		long creationTimeSeconds = System.currentTimeMillis() / 1000;
		DeterministicSeed deterministicSeed = new DeterministicSeed(secureRandom, 128, passphrase, creationTimeSeconds);
		List<String> mnemonicCodes = deterministicSeed.getMnemonicCode();
		String mnemonicCodeStr = String.join(" ", mnemonicCodes);

		return BusinessResponseFactory.createSuccess(mnemonicCodeStr);
	}

	@ApiOperation(value="发送手机验证码接口")
	@RequestMapping(value = "/sendPhoneAuthCode", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Boolean> sendPhoneAuthCode(@RequestBody SendPhoneAuthCodeRequest request) {
		String mobilePhone = request.getMobilePhone();
		String cloudUserId = request.getCloudUserId();
		String host = getClientIp();
		
		if(isSliderVerification == true) {
			AliyunValidateCode aliyunValidateCode = request.getAliyunValidateCode();
			if (aliyunValidateCode == null){
				return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
			}
			if (!aliyunValidataCodeUtil.checkAliyunH5Validate(host, aliyunValidateCode)){
				return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
			}
		}
		

		String REGEX_MOBILE = "^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";

		if(mobilePhone == null || mobilePhone == null || mobilePhone.trim().equals("")) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		String sendIntervalSms = (String)redisTemplate.opsForValue().get(mobilePhone+"_SEND_SMS_INTERVAL");
		if(sendIntervalSms != null) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.VISIT_TOO_FREQUENTLY);
		}

		Object sendSmsDayNumLimitObj = redisTemplate.opsForValue().get(mobilePhone+"_SEND_SMS_DAY_NUM_LIMIT");
		Long sendSmsDayNumLimit = null;
		if(sendSmsDayNumLimitObj != null && sendSmsDayNumLimitObj instanceof Integer) {
			sendSmsDayNumLimit = ((Integer)sendSmsDayNumLimitObj).longValue();
		}else {
			sendSmsDayNumLimit = sendSmsDayNumLimitObj == null ? 0L : (Long)sendSmsDayNumLimitObj;
		}
		if(sendSmsDayNumLimit != null && sendSmsDayNumLimit.compareTo(SEND_SMS_DAY_NUM_LIMIT) > 0) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.VISIT_TOO_FREQUENTLY);
		}


		CloudUser cloudUser = cloudUserService.selectCloudUserByCloudUserId(cloudUserId);

		String realNameAuthCodekey = mobilePhone;
		String authCode = RandomStringUtils.randomNumeric(6);
		//验证内容
		redisTemplate.opsForValue().set(realNameAuthCodekey,authCode);
		redisTemplate.expire(realNameAuthCodekey, REAL_NAME_AUTH_CODE_TIME_OUT, TimeUnit.MILLISECONDS);

		//限制短信发送的频率
		redisTemplate.opsForValue().set(realNameAuthCodekey+"_SEND_SMS_INTERVAL",authCode);
		redisTemplate.expire(realNameAuthCodekey+"_SEND_SMS_INTERVAL", SEND_SMS_INTERVAL, TimeUnit.MILLISECONDS);

		//限制一天相同手机号码短信发送的次数
		sendSmsDayNumLimit = sendSmsDayNumLimit + 1;
		redisTemplate.opsForValue().set(realNameAuthCodekey+"_SEND_SMS_DAY_NUM_LIMIT",sendSmsDayNumLimit == null?Long.valueOf("1"):sendSmsDayNumLimit);
		redisTemplate.expire(realNameAuthCodekey+"_SEND_SMS_DAY_NUM_LIMIT", 1000*60*60*24, TimeUnit.MILLISECONDS);

		String content = authCode + "|" + registerRealNameAuthTemplateTimeout;

		String[] phonePamas = realNameAuthCodekey.split("-");

		String smsSign = cloudUser.getSmsSign();
		if(phonePamas.length == 1) {
			if(realNameAuthCodekey.trim().isEmpty()) {
				logger.error("手机号码不能为空");
				new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
			}
			if(!Pattern.matches(REGEX_MOBILE, realNameAuthCodekey)) {
				logger.error("错误的手机号码" + realNameAuthCodekey);
				new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
			}
			santiService.sendSms(smsSign, content, realNameAuthCodekey, registerRealNameAuthTemplateId);
		} else if(phonePamas.length == 2 && phonePamas[0].replace("+", "").equals("86")) {
			if(phonePamas[1].trim().isEmpty()) {
				logger.error("手机号码不能为空");
				new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
			}
			if(!Pattern.matches(REGEX_MOBILE, phonePamas[1])) {
				logger.error("错误的手机号码" + phonePamas[1]);
				new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"phone number format error");
			}
			santiService.sendSms(smsSign, content, phonePamas[1], registerRealNameAuthTemplateId);
		} else if(phonePamas.length == 2) {
			if(phonePamas[1].trim().isEmpty() || phonePamas[0].trim().isEmpty()) {
				logger.error("手机号码或国籍不能为空");
				new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"phone number format error");
			}
			if(!Pattern.matches(REGEX_MOBILE, phonePamas[1])) {
				logger.error("错误的手机号码" + phonePamas[1]);
				new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"phone number format error");
			}
			santiService.sendInternationalSMS(smsSign, content, phonePamas[0], phonePamas[1], registerRealNameAuthTemplateId);
		} else {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		return BusinessResponseFactory.createSuccess(true);
	}

	@ApiOperation(value="发送邮箱验证码接口")
	@RequestMapping(value = "/sendEmailAuthCode", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Boolean> sendEmailAuthCode(ServerHttpRequest req, @RequestBody SendEmailAuthCodeRequest request){
		String email = request.getEmail();
		String host = getClientIp();
		if(StringUtils.isEmpty(email)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		if(isSliderVerification == true) {
			AliyunValidateCode aliyunValidateCode = request.getAliyunValidateCode();
			if (aliyunValidateCode == null){
				throw new BusinessException(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
			}
			if (!aliyunValidataCodeUtil.checkAliyunH5Validate(host, request.getAliyunValidateCode())){
				return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
			}
		}
		

		String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

		String realNameAuthCodekey = email;

		if(!Pattern.matches(REGEX_EMAIL, realNameAuthCodekey)) {
			logger.error("错误的邮箱格式" + realNameAuthCodekey);
			new BusinessException(BusinessExceptionCode.SYSTEM_CUSTOM_ERROR,"email format error");
		}

		String authCode = RandomStringUtils.randomNumeric(6);
		redisTemplate.opsForValue().set(realNameAuthCodekey,authCode);
		redisTemplate.expire(realNameAuthCodekey, REAL_NAME_AUTH_CODE_TIME_OUT, TimeUnit.MILLISECONDS);

		HashMap<String,String> data = new HashMap<>();
		data.put("authCode", authCode);

		//mailService.sendMailByTemplate("auth code:", email, data, registerRealNameAuthTemplateContent);
		return BusinessResponseFactory.createSuccess(true);
	}

	@ApiOperation(value="校验验证码接口")
	@RequestMapping(value = "/verifyAuthCode/{registName}/{authCode}",name="校验验证码接口", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Boolean> verifyAuthCode(@PathVariable("registName") String registName,@PathVariable("authCode") String authCode) {
		//实名认证码（手机或邮件确认码）
		String realNameAuthCodekey = registName;
		String realNameAuthCode = (String)redisTemplate.opsForValue().get(realNameAuthCodekey);
		if(realNameAuthCode == null || !realNameAuthCode.equals(authCode)) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.AUTH_CODE_ERROR);
		}

		return BusinessResponseFactory.createSuccess(true);
	}

	@ApiOperation(value="获取国家信息")
	@RequestMapping(value = "/country/info",name="获取国家信息", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<List<Country>> selectCountryInfo() {
		List<Country> list = null;

		if (redisUtils.hasKey(RedisKeyConstants.SYSTEM_COUNTRY_INFO)){
			list = redisUtils.getList(RedisKeyConstants.SYSTEM_COUNTRY_INFO, ArrayList.class);
		} else {
			list = CountryConfig.getCountryList();
			redisUtils.put(RedisKeyConstants.SYSTEM_COUNTRY_INFO, list);
		}
		return BusinessResponseFactory.createSuccess(list);
	}
}
