package com.zatgo.zup.merchant.third;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.merchant.controller.AuthController;
import com.zatgo.zup.merchant.controller.BaseController;
import com.zatgo.zup.merchant.third.wechat.service.H5AuthService;
import com.zatgo.zup.merchant.util.AuthTokenUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 登陆认证
 * @author chenlei
 *
 */
@Api(value = "/user/wx/auth", description = "登陆鉴权", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/user/wx/auth")
@Controller
public class H5AuthController extends BaseController{
	@Autowired
	private AuthController controller;

	@Resource
	private OkHttpService okHttpService;

//	@Value("${wx.appid}")
//	private String appid;
//
//	@Value("${wx.appsecret}")
//	private String appsecret;
//
//	@Value("${redirectUrl}")
//	private String redirectUrl;

	@Autowired
	private H5AuthService h5AuthService;
	@Autowired
	private AuthTokenUtil authTokenUtil;
	@Autowired
	private RedisUtils redisUtils;

    
	@RequestMapping(value = "/user/getUserOpenid", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<String> getUserOpenid(@RequestParam(required = false) String code, @RequestParam(required = false) String state) throws Exception {
		String openId = h5AuthService.getUserOpenid(code, state);
		String token = initToken(openId, state);
		return BusinessResponseFactory.createSuccess(token);
	}


//	@RequestMapping(value = "/login", method = RequestMethod.GET)
//	@ResponseBody
//	public ResponseData wechatLogin(@RequestParam("openid") String openid, @RequestParam("cloudUserId") String cloudUserId) {
//		String key = cloudUserId + openid;
//		String token = redisUtils.get(key, String.class);
//		if (StringUtils.isEmpty(token)){
//			throw new BusinessException(BusinessExceptionCode.YOUR_OPENID_NEED_WECHAT_AUTHORIZATION);
//		}
//		return BusinessResponseFactory.createSuccess(token);
//	}


	private String initToken(String openid, String cloudUserId){
		String key = cloudUserId + openid;
		String token = redisUtils.get(key, String.class);
		if (StringUtils.isEmpty(token)){
			AuthUserInfo data = new AuthUserInfo();
			data.setUserId(key);
			data.setCloudUserId(cloudUserId);
			data.setWechatOpenId(openid);
			token = authTokenUtil.generateWechatLoginToken(data);
		}
		return token;
	}

}
