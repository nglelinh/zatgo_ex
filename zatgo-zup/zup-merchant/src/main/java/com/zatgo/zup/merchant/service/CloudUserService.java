package com.zatgo.zup.merchant.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.model.CloudUserApi;
import com.zatgo.zup.common.model.CloudUserApiParams;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.merchant.entity.CloudAdminUser;
import com.zatgo.zup.merchant.entity.CloudUser;

/**
 * Created by 46041 on 2018/9/11.
 */
public interface CloudUserService {

    /**
     * 创建云用户API
     * @param cloudUserApi
     */
    void createUserApi(CloudUserApi cloudUserApi);

    /**
     * 查询云用户API列表
     * @param cloudUserApi
     * @return
     */
    PageInfo<CloudUserApi> selectUserApiList(CloudUserApiParams cloudUserApi);

    /**
     * 修改云用户API
     * @param cloudUserApi
     */
    void updateUserApi(CloudUserApi cloudUserApi);

    /**
     * 根据用户ID获取云用户的管理员用户ID
     * @param userId
     * @param type
     * @return
     */
    UserData getManageUserId(String userId,BusinessEnum.CloudManageUserType type);
    
    /**
     * 根据cloudUserID获取云用户的管理员用户
     * @param userId
     * @param type
     * @return
     */
    UserData getManageUserByCloudUserId(String cloudUserId,BusinessEnum.CloudManageUserType type);

    /**
     * 查询单个云用户API
     * @param publicKey
     * @return
     */
    CloudUserApi selectUserApiDetail(String publicKey);

    /**
     * 创建云用户
     * @param cloudUserName
     */
    void createCloudUser(String cloudUserName);

    /**
     * 查询云用户列表
     * @param cloudUserName
     */
    PageInfo<CloudUser> selectCloudUserList(String cloudUserName, Integer pageNo, Integer pageSize);
    
    /**
     * 查询云用户信息
     * @return
     */
    CloudUser selectCloudUserByCloudUserId(String cloudUserId);
    
    /**
     * 查询云管理员
     * @param cloudAdminName
     * @return
     */
    CloudAdminUser selectCloudAdminByName(String cloudAdminName,String cloudUserId);
}
