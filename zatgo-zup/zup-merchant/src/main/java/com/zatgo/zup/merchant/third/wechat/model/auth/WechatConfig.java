package com.zatgo.zup.merchant.third.wechat.model.auth;

import io.swagger.annotations.ApiModel;

import java.util.UUID;


@ApiModel("微信JSSDK配置参数")
public class WechatConfig {
	
	private String appid;
	
	private String timestamp;
	
	private String nonceStr;
	
	private String signature;
	
	public WechatConfig() {
		super();
	}
	
	public WechatConfig(String appid) {
		this.appid = appid;
		timestamp = String.valueOf(System.currentTimeMillis() / 1000);
		nonceStr = String.valueOf(UUID.randomUUID().toString());
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

}
