package com.zatgo.zup.merchant.remoteservice;

import com.zatgo.zup.common.model.RequestData;
import com.zatgo.zup.common.model.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("zup-wallet")
public interface PresentRemoteService {

	@RequestMapping(value = "/wallet/present/{loginUserName}/{cloudUserId}")
	public ResponseData<Object> present(@RequestParam("loginUserName") String loginUserName,
                                        @RequestParam("cloudUserId") String cloudUserId,
                                        @RequestBody RequestData<String> verifyCode);
}
