package com.zatgo.zup.merchant.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户关注参数")
public class UserFollowParams {
	
	@ApiModelProperty(value = "userid", required = false, hidden = true)
	private String userId;
	
	@ApiModelProperty(value = "openid", required = true)
	private String openid;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

}
