package com.zatgo.zup.merchant.third.wechat.model;

import com.alibaba.fastjson.annotation.JSONField;

public class WeChatAuth {
	
	@JSONField(name = "errcode")
	private String errcode;
	
	@JSONField(name = "errmsg")
	private String errmsg;
	
	@JSONField(name = "access_token")
	private String accessToken;
	
	@JSONField(name = "expires_in")
	private Integer expiresIn;
	
	@JSONField(name = "refreshToken")
	private String refresh_token;
	
	@JSONField(name = "openid")
	private String openid;
	
	@JSONField(name = "scope")
	private String scope;

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

}
