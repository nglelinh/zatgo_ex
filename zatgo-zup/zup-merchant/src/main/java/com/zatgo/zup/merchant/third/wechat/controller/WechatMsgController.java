package com.zatgo.zup.merchant.third.wechat.controller;

import com.zatgo.zup.merchant.third.wechat.model.msg.MsgToken;
import com.zatgo.zup.merchant.third.wechat.model.msg.WechatMsg;
import com.zatgo.zup.merchant.third.wechat.service.IWechatMenuService;
import com.zatgo.zup.merchant.third.wechat.service.IWechatMsgService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 暂不使用
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/user/wx/msg")
public class WechatMsgController{
	
	@Resource
	private IWechatMsgService wechatMsgService;
	
	@Resource
	private IWechatMenuService wechatMenuService;
	
	@RequestMapping(value = "/token", method = RequestMethod.GET)
	@ResponseBody
	public MsgToken msgToken(MsgToken token) {
		
		if (StringUtils.isEmpty(token.getSignature()) || StringUtils.isEmpty(token.getTimestamp())
				|| StringUtils.isEmpty(token.getNonce()) || StringUtils.isEmpty(token.getEchostr())) {
			token.setEchostr("");
			return token;
		}
		
		return wechatMsgService.validateWxToken(token);
	}
	
	@RequestMapping(value = "/{cloudUserId}", method = RequestMethod.POST)
	@ResponseBody
	public String msgToken(@PathVariable("cloudUserId") String cloudUserId, @RequestBody WechatMsg msg) {
		return wechatMsgService.wxMsg(msg);
	}
	
	@RequestMapping(value = "/menu", method = RequestMethod.GET)
	@ResponseBody
	public void menu() {
		wechatMenuService.generateWechatMenu();
	}

}
