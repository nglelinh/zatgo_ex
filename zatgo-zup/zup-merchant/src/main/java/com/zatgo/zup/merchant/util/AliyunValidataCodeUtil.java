package com.zatgo.zup.merchant.util;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.afs.model.v20180112.AuthenticateSigRequest;
import com.aliyuncs.afs.model.v20180112.AuthenticateSigResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.zatgo.zup.common.model.AliyunValidateCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2018/12/28.
 */

@Component
public class AliyunValidataCodeUtil {

    private static Logger logger = LoggerFactory.getLogger(AliyunValidataCodeUtil.class);

    @Value("${aliyun.accesskeyId}")
    private String accesskeyId;
    @Value("${aliyun.accessKeySecret}")
    private String accessKeySecret;
    @Value("${aliyun.ncAppkey}")
    private String ncAppkey;

    public boolean checkAliyunH5Validate(String ip, AliyunValidateCode aliyunValidateCode) {

        try {
            AuthenticateSigRequest authenticateSigRequest = new AuthenticateSigRequest();
            authenticateSigRequest.setSessionId(aliyunValidateCode.getCsessionid());// 必填参数，从前端获取，不可更改，android和ios只传这个参数即可
            authenticateSigRequest.setSig(aliyunValidateCode.getSig());// 必填参数，从前端获取，不可更改
            authenticateSigRequest.setToken(aliyunValidateCode.getToken());// 必填参数，从前端获取，不可更改
            authenticateSigRequest.setScene("nc_login_h5");// 必填参数，从前端获取，不可更改
//            String nc_appkey = PropertyUtil.getProperties("aliyun_nc_appkey");
            authenticateSigRequest.setAppKey(ncAppkey);// 必填参数，后端填写
            authenticateSigRequest.setRemoteIp(ip);// 必填参数，后端填写
//            String accessKeyId = PropertyUtil.getProperties("aliyun_access_key_id");
//            String accessKeySecret = PropertyUtil.getProperties("aliyun_access_key_secret");
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accesskeyId, accessKeySecret);
            IAcsClient client = new DefaultAcsClient(profile);

            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", "afs", "afs.aliyuncs.com");
            AuthenticateSigResponse response = client.getAcsResponse(authenticateSigRequest);
            if (response.getCode() == 100) {
                System.out.println("验签通过");
                return true;
            } else {
                logger.error("h5验签失败：" + response.getMsg());
                System.out.println("验签失败");
                return false;
            }
            // TODO
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
