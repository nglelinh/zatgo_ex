package com.zatgo.zup.merchant.remoteservice;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("zup-wallet")
public interface HelloRemoteService {

	@RequestMapping("/wallet/hello/{fallback}")
    public String hello(@PathVariable("fallback") String fallback);
}
