package com.zatgo.zup.merchant.service.impl;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.model.CloudUserApiParams;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.merchant.entity.CloudAdminUser;
import com.zatgo.zup.merchant.mapper.CloudAdminUserMapper;
import com.zatgo.zup.merchant.service.CloudAdminUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created by 46041 on 2018/9/13.
 */

@Service("cloudAdminUserService")
public class CloudAdminUserServiceImpl implements CloudAdminUserService {

    @Autowired
    private CloudAdminUserMapper cloudAdminUserMapper;

    @Override
    public void createCloudAdminUser(CloudAdminUser cloudAdminUser) {
        if (StringUtils.isEmpty(cloudAdminUser.getId()))
            cloudAdminUser.setId(UUIDUtils.getUuid());
        cloudAdminUserMapper.insertSelective(cloudAdminUser);
    }

    @Override
    public PageInfo<CloudAdminUser> selectCloudAdminUserList(CloudUserApiParams cloudUserApi) {
        return null;
    }

    @Override
    public void updateCloudAdminUser(CloudAdminUser cloudAdminUser) {

    }
}
