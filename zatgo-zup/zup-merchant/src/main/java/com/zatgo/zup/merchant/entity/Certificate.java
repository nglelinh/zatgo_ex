package com.zatgo.zup.merchant.entity;

import java.util.Date;

public class Certificate {
    private String userId;

    private String countryCode;

    private String lastName;

    private String firstName;

    private Byte certificateType;

    private String certificateId;

    private String firstPhotoUrl;

    private String secondPhotoUrl;

    private String holdPhotoUrl;

    private Date createDate;

    private Date updateDate;

    private Byte approveStatus;

    private String approveOperator;

    private String approveOperatorId;

    private Date approveDate;

    private String approveDemo;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode == null ? null : countryCode.trim();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName == null ? null : lastName.trim();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName == null ? null : firstName.trim();
    }

    public Byte getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(Byte certificateType) {
        this.certificateType = certificateType;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId == null ? null : certificateId.trim();
    }

    public String getFirstPhotoUrl() {
        return firstPhotoUrl;
    }

    public void setFirstPhotoUrl(String firstPhotoUrl) {
        this.firstPhotoUrl = firstPhotoUrl == null ? null : firstPhotoUrl.trim();
    }

    public String getSecondPhotoUrl() {
        return secondPhotoUrl;
    }

    public void setSecondPhotoUrl(String secondPhotoUrl) {
        this.secondPhotoUrl = secondPhotoUrl == null ? null : secondPhotoUrl.trim();
    }

    public String getHoldPhotoUrl() {
        return holdPhotoUrl;
    }

    public void setHoldPhotoUrl(String holdPhotoUrl) {
        this.holdPhotoUrl = holdPhotoUrl == null ? null : holdPhotoUrl.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Byte getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(Byte approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getApproveOperator() {
        return approveOperator;
    }

    public void setApproveOperator(String approveOperator) {
        this.approveOperator = approveOperator == null ? null : approveOperator.trim();
    }

    public String getApproveOperatorId() {
        return approveOperatorId;
    }

    public void setApproveOperatorId(String approveOperatorId) {
        this.approveOperatorId = approveOperatorId == null ? null : approveOperatorId.trim();
    }

    public Date getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(Date approveDate) {
        this.approveDate = approveDate;
    }

    public String getApproveDemo() {
        return approveDemo;
    }

    public void setApproveDemo(String approveDemo) {
        this.approveDemo = approveDemo == null ? null : approveDemo.trim();
    }
}