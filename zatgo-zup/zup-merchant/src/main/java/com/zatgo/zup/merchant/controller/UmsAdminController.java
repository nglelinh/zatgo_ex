package com.zatgo.zup.merchant.controller;

import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.merchant.entity.CloudAdminUser;
import com.zatgo.zup.merchant.model.CommonResult;
import com.zatgo.zup.merchant.model.LoginData;
import com.zatgo.zup.merchant.model.LoginParams;
import com.zatgo.zup.merchant.model.UmsAdminLoginParam;
import com.zatgo.zup.merchant.service.AuthService;
import com.zatgo.zup.merchant.service.UmsAdminService;
import com.zatgo.zup.merchant.util.AuthTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 后台用户管理
 * Created by chen on 2018/4/26.
 */
@Controller
@Api(tags = "UmsAdminController", description = "后台用户管理")
@RequestMapping("/admin")
public class UmsAdminController extends BaseController{
    @Autowired
    private UmsAdminService adminService;
    @Autowired
    private AuthService authService;
    @Autowired
    private AuthTokenUtil authTokenUtil;
//    @Value("${jwt.tokenHeader}")
//    private String tokenHeader;
//    @Value("${jwt.tokenHead}")
//    private String tokenHead;

    @ApiOperation(value = "用户注册")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public Object register(@RequestBody CloudAdminUser cloudAdminUser) {
        CloudAdminUser umsAdmin = adminService.register(cloudAdminUser);
        if (umsAdmin == null) {
            new CommonResult().failed();
        }
        return new CommonResult().success(umsAdmin);
    }

    @ApiOperation(value = "登录以后返回token")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Object login(@RequestBody UmsAdminLoginParam umsAdminLoginParam) {
        LoginParams loginParams = new LoginParams();
        loginParams.setLoginName(umsAdminLoginParam.getUsername());
        loginParams.setLoginPassword(umsAdminLoginParam.getPassword());
        LoginData loginData = authService.adminLogin(loginParams, umsAdminLoginParam.getCloudUserId());
        if (loginData == null || loginData.getToken() == null) {
            return new CommonResult().validateFailed("用户名或密码错误");
        }
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", loginData.getToken());
        return new CommonResult().success(tokenMap);
    }

    @ApiOperation(value = "刷新token")
    @RequestMapping(value = "/token/refresh", method = RequestMethod.GET)
    @ResponseBody
    public Object refreshToken(HttpServletRequest request) {
        AuthUserInfo userInfo = getUserInfo();
        Map<String, String> tokenMap = new HashMap<>();
        String token = authTokenUtil.generateLoginToken(userInfo);
        tokenMap.put("token", token);
        return new CommonResult().success(tokenMap);
    }

    @ApiOperation(value = "获取当前登录用户信息")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ResponseBody
    public Object getAdminInfo(Principal principal) {
        AuthUserInfo userInfo = getUserInfo();
        Map<String, Object> data = new HashMap<>();
        data.put("username", userInfo.getUserName());
        data.put("roles", new String[]{"TEST"});
        data.put("icon", null);
        return new CommonResult().success(data);
    }

    @ApiOperation(value = "登出功能")
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseBody
    public Object logout() {
        return new CommonResult().success(null);
    }

    @ApiOperation("根据用户名或姓名分页获取用户列表")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ResponseBody
    public Object list(@RequestParam(value = "name",required = false) String name,
                       @RequestParam(value = "cloudUserId") String cloudUserId,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum){
        List<CloudAdminUser> adminList = adminService.list(name,pageSize,pageNum, cloudUserId);
        return new CommonResult().pageSuccess(adminList);
    }

    @ApiOperation("获取指定用户信息")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Object getItem(@PathVariable String id){
        AuthUserInfo userInfo = getUserInfo();
        CloudAdminUser admin = adminService.getItem(id, userInfo.getCloudUserId());
        return new CommonResult().success(admin);
    }

    @ApiOperation("修改指定用户信息")
    @RequestMapping(value = "/update/{id}",method = RequestMethod.POST)
    @ResponseBody
    public Object update(@PathVariable String id, @RequestBody CloudAdminUser admin){
        int count = adminService.update(id,admin);
        if(count>0){
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("删除指定用户信息")
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.POST)
    @ResponseBody
    public Object delete(@PathVariable String id){
        int count = adminService.delete(id);
        if(count>0){
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

//    @ApiOperation("给用户分配角色")
//    @RequestMapping(value = "/role/update",method = RequestMethod.POST)
//    @ResponseBody
//    public Object updateRole(@RequestParam("adminId") Long adminId,
//                             @RequestParam("roleIds") List<Long> roleIds){
//        int count = adminService.updateRole(adminId,roleIds);
//        if(count>=0){
//            return new CommonResult().success(count);
//        }
//        return new CommonResult().failed();
//    }
//
//    @ApiOperation("获取指定用户的角色")
//    @RequestMapping(value = "/role/{adminId}",method = RequestMethod.GET)
//    @ResponseBody
//    public Object getRoleList(@PathVariable Long adminId){
//        List<UmsRole> roleList = adminService.getRoleList(adminId);
//        return new CommonResult().success(roleList);
//    }
//
//    @ApiOperation("给用户分配+-权限")
//    @RequestMapping(value = "/permission/update",method = RequestMethod.POST)
//    @ResponseBody
//    public Object updatePermission(@RequestParam Long adminId,
//                                   @RequestParam("permissionIds") List<Long> permissionIds){
//        int count = adminService.updatePermission(adminId,permissionIds);
//        if(count>0){
//            return new CommonResult().success(count);
//        }
//        return new CommonResult().failed();
//    }
//
//    @ApiOperation("获取用户所有权限（包括+-权限）")
//    @RequestMapping(value = "/permission/{adminId}",method = RequestMethod.GET)
//    @ResponseBody
//    public Object getPermissionList(@PathVariable Long adminId){
//        List<UmsPermission> permissionList = adminService.getPermissionList(adminId);
//        return new CommonResult().success(permissionList);
//    }
}
