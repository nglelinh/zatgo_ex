package com.zatgo.zup.merchant.third.wechat.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zatgo.zup.common.http.OkHttpService;
import com.zatgo.zup.merchant.entity.CloudUser;
import com.zatgo.zup.merchant.mapper.CloudUserMapper;
import com.zatgo.zup.merchant.third.wechat.service.H5AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by 46041 on 2019/3/19.
 */

@Service("h5AuthService")
public class H5AuthServiceImpl implements H5AuthService {


    @Autowired
    private CloudUserMapper cloudUserMapper;
    @Autowired
    private OkHttpService okHttpService;


    @Override
    public String getUserOpenid(String code, String data) {
        CloudUser cloudUser = cloudUserMapper.selectByPrimaryKey(data);
        JSONObject userOpenId = getUserOpenId(cloudUser.getWeixinOfficialAcctKey(), cloudUser.getWeixinOfficialAcctAppId(), code);
        return userOpenId.getString("openid");
    }














    private JSONObject getUserOpenId(String secret, String appid, String code){
        String[] str = null;
        HashMap<String, String> params = new HashMap<>();
        params.put("secret", secret);
        params.put("appid", appid);
        params.put("grant_type", "authorization_code");
        params.put("code", code);
        return okHttpService.get("https://api.weixin.qq.com/sns/oauth2/access_token", params, JSONObject.class, str);
    }
}
