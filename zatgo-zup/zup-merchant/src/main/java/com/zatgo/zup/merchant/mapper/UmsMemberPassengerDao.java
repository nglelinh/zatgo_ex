package com.zatgo.zup.merchant.mapper;

import com.ykb.mall.common.model.UmsMemberPassengerData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsMemberPassengerDao {
	
	List<UmsMemberPassengerData> passengerList(@Param("memberId") String memberId, @Param("name") String name);

}
