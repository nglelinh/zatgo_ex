package com.zatgo.zup.merchant.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class UserExtExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserExtExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginIsNull() {
            addCriterion("is_lock_login is null");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginIsNotNull() {
            addCriterion("is_lock_login is not null");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginEqualTo(Boolean value) {
            addCriterion("is_lock_login =", value, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginNotEqualTo(Boolean value) {
            addCriterion("is_lock_login <>", value, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginGreaterThan(Boolean value) {
            addCriterion("is_lock_login >", value, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_lock_login >=", value, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginLessThan(Boolean value) {
            addCriterion("is_lock_login <", value, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginLessThanOrEqualTo(Boolean value) {
            addCriterion("is_lock_login <=", value, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginIn(List<Boolean> values) {
            addCriterion("is_lock_login in", values, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginNotIn(List<Boolean> values) {
            addCriterion("is_lock_login not in", values, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginBetween(Boolean value1, Boolean value2) {
            addCriterion("is_lock_login between", value1, value2, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockLoginNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_lock_login not between", value1, value2, "isLockLogin");
            return (Criteria) this;
        }

        public Criteria andIsLockPayIsNull() {
            addCriterion("is_lock_pay is null");
            return (Criteria) this;
        }

        public Criteria andIsLockPayIsNotNull() {
            addCriterion("is_lock_pay is not null");
            return (Criteria) this;
        }

        public Criteria andIsLockPayEqualTo(Boolean value) {
            addCriterion("is_lock_pay =", value, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayNotEqualTo(Boolean value) {
            addCriterion("is_lock_pay <>", value, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayGreaterThan(Boolean value) {
            addCriterion("is_lock_pay >", value, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_lock_pay >=", value, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayLessThan(Boolean value) {
            addCriterion("is_lock_pay <", value, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayLessThanOrEqualTo(Boolean value) {
            addCriterion("is_lock_pay <=", value, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayIn(List<Boolean> values) {
            addCriterion("is_lock_pay in", values, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayNotIn(List<Boolean> values) {
            addCriterion("is_lock_pay not in", values, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayBetween(Boolean value1, Boolean value2) {
            addCriterion("is_lock_pay between", value1, value2, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockPayNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_lock_pay not between", value1, value2, "isLockPay");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawIsNull() {
            addCriterion("is_lock_withdraw is null");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawIsNotNull() {
            addCriterion("is_lock_withdraw is not null");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawEqualTo(Boolean value) {
            addCriterion("is_lock_withdraw =", value, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawNotEqualTo(Boolean value) {
            addCriterion("is_lock_withdraw <>", value, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawGreaterThan(Boolean value) {
            addCriterion("is_lock_withdraw >", value, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_lock_withdraw >=", value, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawLessThan(Boolean value) {
            addCriterion("is_lock_withdraw <", value, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawLessThanOrEqualTo(Boolean value) {
            addCriterion("is_lock_withdraw <=", value, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawIn(List<Boolean> values) {
            addCriterion("is_lock_withdraw in", values, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawNotIn(List<Boolean> values) {
            addCriterion("is_lock_withdraw not in", values, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawBetween(Boolean value1, Boolean value2) {
            addCriterion("is_lock_withdraw between", value1, value2, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andIsLockWithdrawNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_lock_withdraw not between", value1, value2, "isLockWithdraw");
            return (Criteria) this;
        }

        public Criteria andUserGradeIsNull() {
            addCriterion("user_grade is null");
            return (Criteria) this;
        }

        public Criteria andUserGradeIsNotNull() {
            addCriterion("user_grade is not null");
            return (Criteria) this;
        }

        public Criteria andUserGradeEqualTo(Byte value) {
            addCriterion("user_grade =", value, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeNotEqualTo(Byte value) {
            addCriterion("user_grade <>", value, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeGreaterThan(Byte value) {
            addCriterion("user_grade >", value, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeGreaterThanOrEqualTo(Byte value) {
            addCriterion("user_grade >=", value, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeLessThan(Byte value) {
            addCriterion("user_grade <", value, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeLessThanOrEqualTo(Byte value) {
            addCriterion("user_grade <=", value, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeIn(List<Byte> values) {
            addCriterion("user_grade in", values, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeNotIn(List<Byte> values) {
            addCriterion("user_grade not in", values, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeBetween(Byte value1, Byte value2) {
            addCriterion("user_grade between", value1, value2, "userGrade");
            return (Criteria) this;
        }

        public Criteria andUserGradeNotBetween(Byte value1, Byte value2) {
            addCriterion("user_grade not between", value1, value2, "userGrade");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidIsNull() {
            addCriterion("wechat_openid is null");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidIsNotNull() {
            addCriterion("wechat_openid is not null");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidEqualTo(String value) {
            addCriterion("wechat_openid =", value, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidNotEqualTo(String value) {
            addCriterion("wechat_openid <>", value, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidGreaterThan(String value) {
            addCriterion("wechat_openid >", value, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidGreaterThanOrEqualTo(String value) {
            addCriterion("wechat_openid >=", value, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidLessThan(String value) {
            addCriterion("wechat_openid <", value, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidLessThanOrEqualTo(String value) {
            addCriterion("wechat_openid <=", value, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidLike(String value) {
            addCriterion("wechat_openid like", value, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidNotLike(String value) {
            addCriterion("wechat_openid not like", value, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidIn(List<String> values) {
            addCriterion("wechat_openid in", values, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidNotIn(List<String> values) {
            addCriterion("wechat_openid not in", values, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidBetween(String value1, String value2) {
            addCriterion("wechat_openid between", value1, value2, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andWechatOpenidNotBetween(String value1, String value2) {
            addCriterion("wechat_openid not between", value1, value2, "wechatOpenid");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorIsNull() {
            addCriterion("prize_rate_factor is null");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorIsNotNull() {
            addCriterion("prize_rate_factor is not null");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorEqualTo(BigDecimal value) {
            addCriterion("prize_rate_factor =", value, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorNotEqualTo(BigDecimal value) {
            addCriterion("prize_rate_factor <>", value, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorGreaterThan(BigDecimal value) {
            addCriterion("prize_rate_factor >", value, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("prize_rate_factor >=", value, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorLessThan(BigDecimal value) {
            addCriterion("prize_rate_factor <", value, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorLessThanOrEqualTo(BigDecimal value) {
            addCriterion("prize_rate_factor <=", value, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorIn(List<BigDecimal> values) {
            addCriterion("prize_rate_factor in", values, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorNotIn(List<BigDecimal> values) {
            addCriterion("prize_rate_factor not in", values, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("prize_rate_factor between", value1, value2, "prizeRateFactor");
            return (Criteria) this;
        }

        public Criteria andPrizeRateFactorNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("prize_rate_factor not between", value1, value2, "prizeRateFactor");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}