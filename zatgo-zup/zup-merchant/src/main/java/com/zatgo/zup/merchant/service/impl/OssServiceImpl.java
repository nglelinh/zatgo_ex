package com.zatgo.zup.merchant.service.impl;

import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.ObjectMetadata;
import com.zatgo.zup.common.enumtype.BusinessEnum.OssBucket;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.merchant.service.OssService;

@Service
public class OssServiceImpl implements OssService {

	private static final Logger log = LoggerFactory.getLogger(OssServiceImpl.class);

	// #阿里云OSS
	@Value(value = "${oss.endpoint:}")
	private String endpoint;

	@Value(value = "${oss.accessKeyId:}")
	private String accessKeyId;

	@Value(value = "${oss.accessKeySecret:}")
	private String accessKeySecret;

	@Value(value = "${oss.bucketName:}")
	private String ossBucketName;

	@Value(value = "${oss.env:}")
	private String ossEnv;

	@Override
	public String getOssFilePath(OssBucket bucket, String key) {

		// 创建OSSClient实例。
		OSS client = new OSSClientBuilder().build("http://" + endpoint, accessKeyId, accessKeySecret);

		String fileName = bucket.getName() + "-" + ossEnv + "/" + key;
		// 判断文件是否存在。
		boolean found = client.doesObjectExist(ossBucketName, fileName);
		// 关闭OSSClient。
		client.shutdown();
		if (found) {
			return new StringBuffer().append("http://").append(ossBucketName).append(".").append(endpoint).append("/")
					.append(fileName).toString();
		}

		return null;
	}

	@Override
	public String upFileToOss(OssBucket bucket, InputStream input, String fileName) {
		OSS client = new OSSClientBuilder().build("http://" + endpoint, accessKeyId, accessKeySecret);
		try {
			// 创建上传Object的Metadata
			ObjectMetadata meta = new ObjectMetadata();
			// 必须设置ContentLength
			meta.setCacheControl("no-cache");
			meta.setObjectAcl(CannedAccessControlList.PublicRead);

			meta.setContentLength(input.available());

			String key = bucket.getName() + "-" + ossEnv + "/" + fileName;
			client.putObject(ossBucketName, key, input, meta);

			StringBuffer fileUrl = new StringBuffer().append("http://").append(ossBucketName).append(".")
					.append(endpoint).append("/").append(key);

			return fileUrl.toString();
		} catch (Exception e) {
			log.error("上传oss失败：", e);
			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR);
		} finally {
			client.shutdown();
		}
	}

}
