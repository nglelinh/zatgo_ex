package com.zatgo.zup.merchant.third.wechat.model.auth;

public class WechatConfigParams {
	
	private String url;

	private Long corpId;

	/**
	 * 1-飞巴商旅 2-飞鸿商旅
	 */
	private Byte appType;

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}

	public Byte getAppType() {
		return appType;
	}

	public void setAppType(Byte appType) {
		this.appType = appType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
