package com.zatgo.zup.merchant.third.wechat.service.impl;

import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.redis.RedisUtils;
import com.zatgo.zup.common.utils.ThreadPoolUtils;
import com.zatgo.zup.merchant.mapper.CloudUserMapper;
import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfig;
import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfigParams;
import com.zatgo.zup.merchant.third.wechat.service.IWechatJSSDKConfigService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
public class WechatJSSDKConfigServiceImpl implements IWechatJSSDKConfigService {

	private static final String URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=#ACCESS_TOKEN#&type=jsapi";

	private final Logger log = LoggerFactory.getLogger(getClass());

//	@Value("${wx.appid}")
//	private String appid;
//
//	@Value("${jssdk_url:}")
//	private String jssdk_url;

	@Resource
	private RedisUtils redisUtils;

	@Resource
	private RedisLockUtils redisLockUtils;

	@Autowired
	private CloudUserMapper cloudUserMapper;



	@Override
	public WechatConfig getWechatJSSDKConfig(WechatConfigParams params) {

		String ticket = getWechatJsapiTicket(params);
		WechatConfig config = null;
//		log.info(ticket);
//		if(null!=params.getAppType() && 2==params.getAppType().byteValue()){
//			config = new WechatConfig("wx3e255d09abb2d7e4");
//		}else {
////			config = new WechatConfig(appid);
//		}
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("noncestr", config.getNonceStr());
//		map.put("jsapi_ticket", ticket);
//		map.put("timestamp", config.getTimestamp());
//		map.put("url", params.getUrl());
//
//		List<String> keys = new ArrayList<>(map.keySet());
//		keys.sort((fir, sed) -> fir.compareTo(sed));
//		StringBuffer sign = new StringBuffer();
//		keys.forEach(value -> sign.append("&").append(value).append("=").append(map.get(value)));
//
//		String signStr = SignUtils.sha1(sign.substring(1));
//		config.setSignature(signStr);
		return config;
	}

	private String getWechatJsapiTicket(WechatConfigParams params) {

		String ticket = redisUtils.get(RedisKeyConstants.WECHAT_JSAPI_TICKET, String.class);

		if (StringUtils.isNotEmpty(ticket)) {
			return ticket;
		}

		ticket = redisUtils.get(RedisKeyConstants.WECHAT_JSAPI_TICKET, String.class);
		if (StringUtils.isNotEmpty(ticket)) {

			ThreadPoolUtils.getThreadPool().execute(new Runnable() {

				@Override
				public void run() {
					// 获取jsapi_ticket
					cacheWechatJsapiTicket(0L,params);
				}

			});

			return ticket;
		}

		ticket = cacheWechatJsapiTicket(-1L,params);

		return ticket;
	}

	private String cacheWechatJsapiTicket(Long timeOut,WechatConfigParams params) {

		String ticket = "";

		String redisKey = "";

		if (redisLockUtils.lock(redisKey)) {
			try {

				ticket = redisUtils.get(RedisKeyConstants.WECHAT_JSAPI_TICKET, String.class);
				if (StringUtils.isNotEmpty(ticket)) {
					return ticket;
				}
//				CorpWeChatConfig corpWeChatConfig = cloudUserMapper.selectByCorpId(params.getCorpId());
//				if(corpWeChatConfig != null || params.getCorpId() == null){
//					ticket = reqWechatJsapiTicket(corpWeChatConfig.getAppId(),corpWeChatConfig.getAppSecret());
//				}else{
//					if(null!=params.getAppType() && 2==params.getAppType().byteValue()){
//						ticket = reqWechatJsapiTicket("wx3e255d09abb2d7e4","5be40e3344c1745d3aa3995c846aecef");
//					}else {
//						ticket = reqWechatJsapiTicket(appid,"78bfc23476cb34322afa8740335c046f");
//					}
//				}
			} catch (Exception e) {
				log.error("获取微信jsapi_ticket失败：", e);
				throw e;
			} finally {
				redisLockUtils.releaseLock(redisKey);
			}
		}
		return ticket;
	}

//	private String reqWechatJsapiTicket(String appId,String appSecret) {
//		String token = wechatAccessTokenService.getAccessToken(appId,appSecret);
//		String url = URL.replace("#ACCESS_TOKEN#", token);
//		WechatJsapiTicketData data = okHttpService.postObj(url, null, WechatJsapiTicketData.class);
//		if (data.getErrcode() != 0) {
//			log.error("获取微信jsapi_ticket失败：" + data.getErrmsg());
//			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR, "获取微信公众号配置失败");
//		}
//
//		String ticket = data.getTicket();
//		if (StringUtils.isEmpty(ticket)) {
//			log.error("获取微信jsapi_ticket失败，jsapi_ticketticket未返回");
//			throw new BusinessException(BusinessExceptionCode.SYSTEM_ERROR, "获取微信公众号配置失败");
//		}
//
//		cacheWechatJsapiTicket(data.getExpiresIn(), ticket);
//
//		return ticket;
//	}

	private void cacheWechatJsapiTicket(int sec, String ticket) {

		redisUtils.put(RedisKeyConstants.WECHAT_JSAPI_TICKET, ticket, sec - 10 * 60l, TimeUnit.SECONDS);

		/*redisClientTemplate.set(RedisKeyConstants.WECHAT_JSAPI_TICKET_BACKUP, ticket);
		redisClientTemplate.expire(RedisKeyConstants.WECHAT_JSAPI_TICKET_BACKUP, sec - 5 * 60);*/
	}

}
