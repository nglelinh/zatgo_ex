package com.zatgo.zup.merchant.mapper;

import com.zatgo.zup.merchant.entity.CarouselImage;

public interface CarouselImageMapper extends BaseMapper<CarouselImage> {
}