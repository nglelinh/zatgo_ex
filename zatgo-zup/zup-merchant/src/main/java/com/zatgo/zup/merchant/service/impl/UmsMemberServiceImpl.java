package com.zatgo.zup.merchant.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ykb.mall.common.enumType.BusinessEnum.IdcardType;
import com.ykb.mall.common.enumType.BusinessEnum.YesOrNo;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.ykb.mall.common.model.*;
import com.ykb.mall.common.utils.IdcardUtils;
import com.ykb.mall.common.utils.PatternUtils;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.merchant.mapper.*;
import com.zatgo.zup.merchant.model.CommonResult;
import com.zatgo.zup.merchant.model.PassengerIdcardParams;
import com.zatgo.zup.merchant.model.PassengerParams;
import com.zatgo.zup.merchant.model.UmsMemberParam;
import com.zatgo.zup.merchant.remoteservice.WechatRemoteService;
import com.zatgo.zup.merchant.service.RedisService;
import com.zatgo.zup.merchant.service.UmsMemberService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;

/**
 * 会员管理Service实现类
 * Created by chen on 2018/8/3.
 */
@Service
public class UmsMemberServiceImpl implements UmsMemberService {

    private static final Logger logger = LoggerFactory.getLogger(UmsMemberServiceImpl.class);

//    @Value("${redis.key.prefix.authCode}")
    private String REDIS_KEY_PREFIX_AUTH_CODE = "portal:authCode:";

    @Value("${authCode.expire.seconds}")
    private Long AUTH_CODE_EXPIRE_SECONDS;

//    @Value("${wx.account.default.password}")
//    private String DEFAULT_PASSWORD;

//    @Value("${jwt.tokenHead}")
//    private String tokenHead;

    @Value("${system.isDev:false}")
    private boolean isDev;

    @Autowired
    private UmsMemberMapper memberMapper;

    @Autowired
    private UmsMemberLevelMapper memberLevelMapper;

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    @Autowired
    private RedisService redisService;

    @Autowired
    private UmsMemberPassengerMapper umsMemberPassengerMapper;

    @Autowired
    private UmsMemberPassengerIdcardMapper umsMemberPassengerIdcardMapper;

    @Autowired
    private UmsMemberPassengerDao umsMemberPassengerDao;

    @Autowired
    private WechatRemoteService wechatRemoteService;

    @Override
    public UmsMember getByUsername(String username) {
        UmsMemberExample example = new UmsMemberExample();
        example.createCriteria().andUsernameEqualTo(username);
        List<UmsMember> memberList = memberMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(memberList)) {
            return memberList.get(0);
        }
        return null;
    }

    /**
     * 登录
     * @param param
     * @return
     */
    @Override
    public CommonResult login(UmsMemberParam param) {
//        if (StringUtils.isNotEmpty(param.getWxCode())) {
//            WxAccessTokenData data = null;
//            try {
//                // 获取微信openId
//                data = weiXinService.getOpenId(param.getWxCode());
//            } catch (Exception e) {
//                return new CommonResult().failed("获取微信openId失败");
//            }
//            if (StringUtils.isNotEmpty(data.getOpenId())) {
//                param.setWxOpenId(data.getOpenId());
//                List<UmsMemberAccount> members = umsMemberDao.selectMemberAccountInfo(param);
//                // 当前微信用户没有绑定过系统账户，需要先注册
//                if (CollectionUtils.isEmpty(members)) {
//                    logger.info("当前微信用户没有绑定过系统账户，需微信授权账户信息");
//                    return new CommonResult().refused("当前微信用户没有绑定过系统账户，需微信授权账户信息");
//                } else {    // 登录
//                    UmsMemberAccount umsMember = members.get(0);
//                    return loginSecurity(umsMember, param);
//                }
//            } else {
//                logger.error("未获取到微信openId，param:{}", JSON.toJSONString(param));
//                return new CommonResult().failed("获取微信用户信息失败");
//            }
//        }
        return new CommonResult().failed("登录失败");
    }

    /**
     * 微信注册
     * @param param
     * @return
     */
    @Override
    public CommonResult wxRigester(UmsMemberParam param) {
        if (StringUtils.isNotEmpty(param.getWxCode())) {
            WxAccessTokenData data = null;
            try {
                // 获取微信openId
                ResponseData<WxAccessTokenData> wechatOpenId = wechatRemoteService.getWechatOpenId(param.getWxCode());
                if (wechatOpenId != null && wechatOpenId.isSuccessful()){
                    data = wechatOpenId.getData();
                } else {
                    logger.error("", JSONObject.toJSONString(wechatOpenId));
                    return new CommonResult().failed("获取微信openId失败");
                }
            } catch (Exception e) {
                return new CommonResult().failed("获取微信openId失败");
            }
            if (StringUtils.isNotEmpty(data.getOpenId())) {




            } else {
                logger.error("未获取到微信openId，param:{}", JSON.toJSONString(param));
                return new CommonResult().failed("获取微信用户信息失败");
            }
        }
        return new CommonResult().failed("微信账户注册失败");
    }

    @Override
    public UmsMember getById(Long id) {
        return memberMapper.selectByPrimaryKey(id);
    }

//    @Override
//    public CommonResult register(String username, String password, String telephone, String authCode) {
//        //验证验证码
//        if(!verifyAuthCode(authCode,telephone)){
//            return new CommonResult().failed("验证码错误");
//        }
//        //查询是否已有该用户
//        UmsMemberExample example = new UmsMemberExample();
//        example.createCriteria().andUsernameEqualTo(username);
//        example.or(example.createCriteria().andPhoneEqualTo(telephone));
//        List<UmsMember> umsMembers = memberMapper.selectByExample(example);
//        if (!CollectionUtils.isEmpty(umsMembers)) {
//            return new CommonResult().failed("该用户已经存在");
//        }
//        //没有该用户进行添加操作
//        UmsMember umsMember = new UmsMember();
//        umsMember.setUsername(username);
//        umsMember.setPhone(telephone);
//        umsMember.setPassword(passwordEncoder.encode(password));
//        umsMember.setCreateTime(new Date());
//        umsMember.setStatus(1);
//        //获取默认会员等级并设置
//        UmsMemberLevelExample levelExample = new UmsMemberLevelExample();
//        levelExample.createCriteria().andDefaultStatusEqualTo(1);
//        List<UmsMemberLevel> memberLevelList = memberLevelMapper.selectByExample(levelExample);
//        if (!CollectionUtils.isEmpty(memberLevelList)) {
//            umsMember.setMemberLevelId(memberLevelList.get(0).getId());
//        }
//        memberMapper.insert(umsMember);
//        umsMember.setPassword(null);
//        return new CommonResult().success("注册成功",null);
//    }

    @Override
    public CommonResult generateAuthCode(String telephone) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for(int i=0;i<6;i++){
            sb.append(random.nextInt(10));
        }
        //验证码绑定手机号并存储到redis
        redisService.set(REDIS_KEY_PREFIX_AUTH_CODE+telephone,sb.toString());
        redisService.expire(REDIS_KEY_PREFIX_AUTH_CODE+telephone,AUTH_CODE_EXPIRE_SECONDS);
        return new CommonResult().success("获取验证码成功",sb.toString());
    }

//    @Override
//    public CommonResult updatePassword(String telephone, String password, String authCode) {
//        UmsMemberExample example = new UmsMemberExample();
//        example.createCriteria().andPhoneEqualTo(telephone);
//        List<UmsMember> memberList = memberMapper.selectByExample(example);
//        if(CollectionUtils.isEmpty(memberList)){
//            return new CommonResult().failed("该账号不存在");
//        }
//        //验证验证码
//        if(!verifyAuthCode(authCode,telephone)){
//            return new CommonResult().failed("验证码错误");
//        }
//        UmsMember umsMember = memberList.get(0);
//        umsMember.setPassword(passwordEncoder.encode(password));
//        memberMapper.updateByPrimaryKeySelective(umsMember);
//        return new CommonResult().success("密码修改成功",null);
//    }

    @Override
    public void updateIntegration(Long id, Integer integration) {
        UmsMember record=new UmsMember();
        record.setId(id);
        record.setIntegration(integration);
        memberMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 登录账户
     * @param umsMember
     * @return
     */
//    private CommonResult loginSecurity(UmsMemberAccount umsMember, UmsMemberParam param){
//        if (StringUtils.isEmpty(param.getUsername())) {
//            param.setUsername(umsMember.getUsername());
//        }
//        // 不通过账号密码登录的密码为初始默认的
//        if (StringUtils.isEmpty(param.getPassword())) {
//            param.setPassword(DEFAULT_PASSWORD);
//        }
//        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(param.getUsername(), param.getPassword());
//        try {
//            Authentication authentication = authenticationManager.authenticate(authenticationToken);
//            SecurityContextHolder.getContext().setAuthentication(authentication);
//            UserDetails userDetails = userDetailsService.loadUserByUsername(param.getUsername());
//            String token = jwtTokenUtil.generateToken(userDetails);
//            if (StringUtils.isEmpty(token)) {
//                throw new BusinessException(BusinessExceptionCode.WEIXIN_LOGIN_ERROR, "生成token失败");
//            }
//            Map<String, String> tokenMap = new HashMap<>();
//            tokenMap.put("token", token);
//            tokenMap.put("tokenHead", tokenHead);
//            umsMember.setTokenMap(tokenMap);
//        } catch (AuthenticationException e) {
//            logger.error("登录失败", e);
//            return new CommonResult().failed("登录失败");
//        }
//        umsMember.setPassword(null);
//        return new CommonResult().success(umsMember);
//    }

    /**
     * 对输入的验证码进行校验
     * @param authCode
     * @param telephone
     * @return
     */
    private boolean verifyAuthCode(String authCode, String telephone){
        if(StringUtils.isEmpty(authCode)){
            return false;
        }
        String realAuthCode = redisService.get(REDIS_KEY_PREFIX_AUTH_CODE + telephone);
        return authCode.equals(realAuthCode);
    }

    @Transactional
	@Override
	public CommonResult addPassenger(PassengerParams params, AuthUserInfo userInfo) {

		if(!checkPhone(params.getPhone())) {
			return new CommonResult().failed("手机号码格式不正确");
		}

		UmsMemberPassenger passenger = new UmsMemberPassenger();
		passenger.setBirthday(params.getBirthday());
		passenger.setMemberId(userInfo.getUserId());
		passenger.setName(params.getName());
		passenger.setPhone(params.getPhone());
		passenger.setSex(params.getSex());
		passenger.setIsAdult(params.getIsAdult());
		umsMemberPassengerMapper.insertSelective(passenger);

		if(!StringUtils.isEmpty(params.getIdcardCode()) && params.getIdcardType() != null) {

			if (!checkIdentityCard(params.getIdcardType(), params.getIdcardCode(), params.getIsAdult())) {
				return new CommonResult().failed("身份证件格式不正确");
			}

			UmsMemberPassengerIdcard idcard = new UmsMemberPassengerIdcard();
			idcard.setIdcardCode(params.getIdcardCode());
			idcard.setIdcardType(params.getIdcardType());
			idcard.setPassengerId(passenger.getId());
			umsMemberPassengerIdcardMapper.insertSelective(idcard);
		}

		return new CommonResult().success(passenger);
	}

	@Override
	public CommonResult updatePassengerIdCard(Long idCardId, PassengerIdcardParams params, String userId) {

		UmsMemberPassengerIdcard idcard = umsMemberPassengerIdcardMapper.selectByPrimaryKey(idCardId);
		if(idcard == null) {
			return new CommonResult().failed("乘客证件信息不存在");
		} else if(!idcard.getIdcardType().equals(params.getIdcardType())) {
			UmsMemberPassengerIdcardExample example = new UmsMemberPassengerIdcardExample();
			example.createCriteria().andPassengerIdEqualTo(idcard.getPassengerId()).andIdcardTypeEqualTo(params.getIdcardType());
			List<UmsMemberPassengerIdcard> idcards = umsMemberPassengerIdcardMapper.selectByExample(example);
			if(!CollectionUtils.isEmpty(idcards)) {
				return new CommonResult().failed("乘客同类证件已存在");
			}
		}

		List<UmsMemberPassenger> passengers = selectPassenger(idcard.getPassengerId(), userId);
		if (!checkIdentityCard(params.getIdcardType(), params.getIdcardCode(), passengers.get(0).getIsAdult())) {
			return new CommonResult().failed("身份证件格式不正确");
		}

		idcard.setIdcardCode(params.getIdcardCode());
		idcard.setIdcardType(params.getIdcardType());

		umsMemberPassengerIdcardMapper.updateByPrimaryKeySelective(idcard);
		return new CommonResult().success(idcard);
	}

	@Override
	public CommonResult addPassengerIdCard(Long passengerId, PassengerIdcardParams params, String userId) {

		List<UmsMemberPassenger> passengers = selectPassenger(passengerId, userId);
		if(CollectionUtils.isEmpty(passengers)) {
			return new CommonResult().failed("乘客信息不存在");
		}

		UmsMemberPassenger passenger = passengers.get(0);

		UmsMemberPassengerIdcardExample example = new UmsMemberPassengerIdcardExample();
		example.createCriteria().andPassengerIdEqualTo(passengerId).andIdcardTypeEqualTo(params.getIdcardType());
		List<UmsMemberPassengerIdcard> idcards = umsMemberPassengerIdcardMapper.selectByExample(example);

		if(CollectionUtils.isEmpty(idcards)) {

			if (!checkIdentityCard(params.getIdcardType(), params.getIdcardCode(), passenger.getIsAdult())) {
				return new CommonResult().failed("身份证件格式不正确");
			}

			UmsMemberPassengerIdcard idcard = new UmsMemberPassengerIdcard();
			idcard.setIdcardCode(params.getIdcardCode());
			idcard.setIdcardType(params.getIdcardType());
			idcard.setPassengerId(passenger.getId());
			umsMemberPassengerIdcardMapper.insertSelective(idcard);
			return new CommonResult().success(idcard);
		} else {
			return new CommonResult().failed("乘客同类证件已存在");
		}

	}

	@Override
	public CommonResult updatePassenger(Long passengerId, PassengerParams params, String userId) {

		List<UmsMemberPassenger> passengers = selectPassenger(passengerId, userId);
		if(CollectionUtils.isEmpty(passengers)) {
			return new CommonResult().failed("乘客信息不存在");
		}

		UmsMemberPassenger passenger = passengers.get(0);

		if(!StringUtils.isEmpty(params.getPhone()) && !checkPhone(params.getPhone())) {
			return new CommonResult().failed("手机号码错误");
		}

		if (!StringUtils.isEmpty(params.getIdcardCode())
				&& !checkIdentityCard(params.getIdcardType(), params.getIdcardCode(), params.getIsAdult())) {
			return new CommonResult().failed("身份证件格式不正确");
		}

		passenger.setBirthday(params.getBirthday());
		passenger.setName(params.getName());
		passenger.setPhone(params.getPhone());
		passenger.setSex(params.getSex());
		passenger.setIsAdult(params.getIsAdult());
		umsMemberPassengerMapper.updateByPrimaryKey(passenger);

		return new CommonResult().success(passenger);
	}

	@Override
	public CommonResult passengerList(String name, AuthUserInfo userInfo) {
		return new CommonResult().success(umsMemberPassengerDao.passengerList(userInfo.getUserId(), name));
	}

	@Transactional
	@Override
	public CommonResult deletePassenger(Long passengerId, AuthUserInfo userInfo) {

		UmsMemberPassengerExample passengerExample = new UmsMemberPassengerExample();
		passengerExample.createCriteria().andIdEqualTo(passengerId).andMemberIdEqualTo(userInfo.getUserId());
		umsMemberPassengerMapper.deleteByExample(passengerExample);

		UmsMemberPassengerIdcardExample idcardExample = new UmsMemberPassengerIdcardExample();
		idcardExample.createCriteria().andPassengerIdEqualTo(passengerId);
		umsMemberPassengerIdcardMapper.deleteByExample(idcardExample);
		return new CommonResult().success("");
	}

	private List<UmsMemberPassenger> selectPassenger(Long passengerId, String userId) {
		UmsMemberPassengerExample passengerExample = new UmsMemberPassengerExample();
		passengerExample.createCriteria().andIdEqualTo(passengerId).andMemberIdEqualTo(userId);

		List<UmsMemberPassenger> passengers = umsMemberPassengerMapper.selectByExample(passengerExample);
		return passengers;
	}

	private boolean checkIdentityCard(Byte idcardType, String idcardCode, Byte isAdult) {
		if (IdcardType.IDENTITY_CARD.getCode().equals(idcardType) ) {

			if(!IdcardUtils.isValidatedAllIdcard(idcardCode)) {
				return false;
			}

			if(YesOrNo.NO.getCode().equals(isAdult) && IdcardUtils.getAgeByIdCard(idcardCode) > 12) {
				return false;
			}
		}

		return true;
	}

	private boolean checkPhone(String phone) {
		if (!PatternUtils.isMobile(phone)) {
			return false;
		}

		return true;
	}

}
