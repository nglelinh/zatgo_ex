package com.zatgo.zup.merchant.service;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.model.CloudUserApiParams;
import com.zatgo.zup.merchant.entity.CloudAdminUser;

/**
 * Created by 46041 on 2018/9/13.
 */
public interface CloudAdminUserService {

    /**
     * 创建云管理用户
     * @param cloudAdminUser
     */
    void createCloudAdminUser(CloudAdminUser cloudAdminUser);

    /**
     * 查询云管理用户列表
     * @param cloudUserApi
     * @return
     */
    PageInfo<CloudAdminUser> selectCloudAdminUserList(CloudUserApiParams cloudUserApi);

    /**
     * 修改云管理用户
     * @param cloudAdminUser
     */
    void updateCloudAdminUser(CloudAdminUser cloudAdminUser);
}
