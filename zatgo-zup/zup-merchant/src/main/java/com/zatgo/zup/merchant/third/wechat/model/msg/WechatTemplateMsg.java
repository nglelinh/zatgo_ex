package com.zatgo.zup.merchant.third.wechat.model.msg;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Map;

/**
 * Created by 1 on 2018/8/29.
 */
public class WechatTemplateMsg{
    /**
     * 接收者openid
     */
    private String touser;
    /**
     * 模板ID
     */
    private String templateId;
    /**
     * 模板跳转链接
     */
    private String url;
    /**
     * 跳小程序所需数据，不需跳小程序可不用传该数据
     */
    private WechatMiniProgram miniProgram;
    /**
     * 	模板数据
     */
    private Map<String,TemplateData> data;

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    @JSONField(name = "template_id")
    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public WechatMiniProgram getMiniProgram() {
        return miniProgram;
    }

    public void setMiniProgram(WechatMiniProgram miniProgram) {
        this.miniProgram = miniProgram;
    }

    public Map<String, TemplateData> getData() {
        return data;
    }

    public void setData(Map<String, TemplateData> data) {
        this.data = data;
    }
}
