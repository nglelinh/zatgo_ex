package com.zatgo.zup.merchant.third.wechat.service.impl;

import com.zatgo.zup.merchant.third.wechat.service.IWechatMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class WechatMenuServiceImpl implements IWechatMenuService {

	private Logger log = LoggerFactory.getLogger(getClass());

	private static String URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";

//	@Value("${wx.appid}")
//	private String appid;
//
//	@Value("${wx.appsecret}")
//	private String appsecret;


	@Override
	public void generateWechatMenu() {

	}

	private enum MenuLevel {
		FIRST_MENU(1), SECOND_MENU(2);

		private Byte level;

		MenuLevel(Integer level) {
			this.level = level.byteValue();
		}

		public Byte getLevel() {
			return level;
		}
	}

//	@Override
//	public void generateWechatMenu() {
//		List<WechatMenu> menus = wechatMenuMapper.selectWechatMenuByAppId(appid);
//		if (CollectionUtils.isEmpty(menus)) {
//			log.info("微信菜单未配置");
//			return;
//		}
//
//		Iterator<WechatMenu> all = menus.iterator();
//
//		List<WechatMenu> firMenu = new ArrayList<WechatMenu>();
//		while (all.hasNext()) {
//			WechatMenu menu = all.next();
//			if (menu.getMenuLevel().equals(MenuLevel.FIRST_MENU.getLevel())) {
//				firMenu.add(menu);
//				all.remove();
//			}
//		}
//
//		if (CollectionUtils.isEmpty(firMenu)) {
//			log.error("微信一级菜单未配置");
//			return;
//		}
//
//		Map<Long, List<WechatMenu>> secMenu = new HashMap<Long, List<WechatMenu>>();
//		for (WechatMenu _firMenu : firMenu) {
//			List<WechatMenu> _secMenu = new ArrayList<WechatMenu>();
//			Iterator<WechatMenu> part = menus.iterator();
//			while (part.hasNext()) {
//				WechatMenu menu = part.next();
//				if (_firMenu.getMenuId().equals(menu.getParentId())) {
//					_secMenu.add(menu);
//					part.remove();
//				}
//			}
//
//			secMenu.put(_firMenu.getMenuId(), _secMenu);
//		}
//
//		firMenu.sort((min, max) -> min.getSort().compareTo(max.getSort()));
//
//		List<WechatMenuButton> menuButtons = new ArrayList<WechatMenuButton>();
//		for (WechatMenu menu : firMenu) {
//			WechatMenuButton button = new WechatMenuButton();
//			button.setAppid(menu.getAppid());
//			button.setKey(menu.getKey());
//			button.setMediaId(menu.getMediaId());
//			button.setName(menu.getName());
//			button.setPagepath(menu.getPagepath());
//			button.setType(menu.getType());
//			button.setUrl(menu.getUrl());
//			List<WechatMenu> _secMenu = secMenu.get(menu.getMenuId());
//			if (CollectionUtils.isNotEmpty(_secMenu)) {
//				List<WechatMenuButton> buttons = new ArrayList<WechatMenuButton>();
//				for (WechatMenu _menu : _secMenu) {
//					WechatMenuButton _button = new WechatMenuButton();
//					_button.setAppid(_menu.getAppid());
//					_button.setKey(_menu.getKey());
//					_button.setMediaId(_menu.getMediaId());
//					_button.setName(_menu.getName());
//					_button.setPagepath(_menu.getPagepath());
//					_button.setType(_menu.getType());
//					_button.setUrl(_menu.getUrl());
//					buttons.add(_button);
//				}
//
//				button.setSub_button(buttons);
//			}
//			menuButtons.add(button);
//		}
//
//		WechatMenuData menu = new WechatMenuData();
//		menu.setButton(menuButtons);
//
//		String url = new StringBuffer(URL).append(wechatAccessTokenService.getAccessToken(appid,appsecret)).toString();
//		String res = okHttpService.postStr(url, menu);
//		log.info("微信创建菜单返回结果：" + res);
//	}

}
