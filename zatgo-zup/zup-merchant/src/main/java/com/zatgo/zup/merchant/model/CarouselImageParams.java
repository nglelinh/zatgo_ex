package com.zatgo.zup.merchant.model;

import org.apache.commons.lang3.StringUtils;

import com.zatgo.zup.common.enumtype.BusinessEnum.CarouselImageSiteType;
import com.zatgo.zup.common.model.CloudSystemConstant;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "轮播图查询参数")
public class CarouselImageParams {

	@ApiModelProperty(value = "位置 0：启动页 1：首页banner", required = true)
	private CarouselImageSiteType siteType;

	@ApiModelProperty(value = "云用户id", required = true)
	private String cloudUserId;

	public CarouselImageSiteType getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = CarouselImageSiteType.getEnumByType(siteType);
	}

	public String getCloudUserId() {
		return StringUtils.isEmpty(cloudUserId) ? CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID : cloudUserId;
	}

	public void setCloudUserId(String cloudUserId) {
		this.cloudUserId = cloudUserId;
	}

}
