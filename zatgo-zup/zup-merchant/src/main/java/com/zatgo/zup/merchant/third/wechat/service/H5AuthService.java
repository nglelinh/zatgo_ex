package com.zatgo.zup.merchant.third.wechat.service;

/**
 * Created by 46041 on 2019/3/19.
 */
public interface H5AuthService {
    String getUserOpenid(String code, String data);
}
