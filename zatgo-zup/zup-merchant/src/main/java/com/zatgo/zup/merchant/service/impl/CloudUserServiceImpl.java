package com.zatgo.zup.merchant.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.BusinessEnum.CloudManageUserType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.CloudUserApi;
import com.zatgo.zup.common.model.CloudUserApiParams;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.merchant.entity.*;
import com.zatgo.zup.merchant.mapper.CloudAdminUserMapper;
import com.zatgo.zup.merchant.mapper.CloudUserApiMapper;
import com.zatgo.zup.merchant.mapper.CloudUserMapper;
import com.zatgo.zup.merchant.mapper.UserMapper;
import com.zatgo.zup.merchant.service.CloudUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by 46041 on 2018/9/11.
 */

@Service("cloudUserService")
public class CloudUserServiceImpl implements CloudUserService {

    @Autowired
    private CloudUserApiMapper cloudUserApiMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CloudUserMapper cloudUserMapper;
    
    @Autowired
    private CloudAdminUserMapper cloudAdminUserMapper;

    @Override
    public void createUserApi(CloudUserApi cloudUserApi) {
        cloudUserApi.setId(UUIDUtils.getUuid());
        cloudUserApiMapper.insertSelective(cloudUserApi);
    }

    @Override
    public PageInfo<CloudUserApi> selectUserApiList(CloudUserApiParams cloudUserApi) {
        CloudUserApiExample example = new CloudUserApiExample();
        CloudUserApiExample.Criteria criteria = example.createCriteria();
        String bindIps = cloudUserApi.getBindIps();
        if (!StringUtils.isEmpty(bindIps))
            criteria.andBindIpsEqualTo(bindIps);
        String publicKey = cloudUserApi.getPublicKey();
        if (!StringUtils.isEmpty(publicKey))
            criteria.andPublicKeyEqualTo(publicKey);
        PageHelper.startPage(cloudUserApi.getPageNo(), cloudUserApi.getPageSize());
        List<CloudUserApi> cloudUserApis = cloudUserApiMapper.selectByExample(example);
        return new PageInfo<CloudUserApi>(cloudUserApis);
    }

    @Override
    public void updateUserApi(CloudUserApi cloudUserApi) {
        cloudUserApiMapper.updateByPrimaryKeySelective(cloudUserApi);
    }

    @Override
    public UserData getManageUserId(String userId,BusinessEnum.CloudManageUserType type) {
        User user = userMapper.selectByPrimaryKey(userId);
        if (user != null){
            String cloudUserId = user.getCloudUserId();
            UserExample userExample = new UserExample();
            userExample.createCriteria().andCloudUserIdEqualTo(cloudUserId)
                    .andIsCloudManagerEqualTo(new Byte("1"))
                    .andCloudManagerTypeEqualTo(type.getType().byteValue());
            List<User> users = userMapper.selectByExample(userExample);
            if (!CollectionUtils.isEmpty(users)){
                user = users.get(0);
                UserData res = new UserData();
                BeanUtils.copyProperties(user, res);
                return res;
            }
        }
        return null;
    }

    @Override
    public CloudUserApi selectUserApiDetail(String cloudUserId) {
        CloudUserApiExample example = new CloudUserApiExample();
        example.createCriteria().andCloudUserIdEqualTo(cloudUserId);
        List<CloudUserApi> cloudUserApis = cloudUserApiMapper.selectByExample(example);
        return CollectionUtils.isEmpty(cloudUserApis) ? null : cloudUserApis.get(0);
    }

    @Override
    public void createCloudUser(String cloudUserName) {
        int i = cloudUserMapper.countByExample(cloudUserName);
        if (i > 0)
            throw new BusinessException(BusinessExceptionCode.TITLE_IS_EXIST);
        CloudUser cloudUser = new CloudUser();
        cloudUser.setCloudUserId(UUIDUtils.getUuid());
        cloudUser.setCloudUserName(cloudUserName);
        cloudUserMapper.insertSelective(cloudUser);
    }

    @Override
    public PageInfo<CloudUser> selectCloudUserList(String cloudUserName, Integer pageNo, Integer pageSize) {
        CloudUserExample example = new CloudUserExample();
        if (!StringUtils.isEmpty(cloudUserName)){
            example.createCriteria().andCloudUserNameLike("%" + cloudUserName + "%");
        }
        PageHelper.startPage(pageNo, pageSize);
        return new PageInfo<>(cloudUserMapper.selectByExample(example));
    }

	@Override
	public CloudUser selectCloudUserByCloudUserId(String cloudUserId) {
		if(StringUtils.isEmpty(cloudUserId)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}
		CloudUser user = cloudUserMapper.selectByPrimaryKey(cloudUserId);
		return user;
	}

	@Override
	public CloudAdminUser selectCloudAdminByName(String cloudAdminName,String cloudUserId) {
		
		CloudAdminUserExample example = new CloudAdminUserExample();
		example.createCriteria().andNameEqualTo(cloudAdminName).andCloudUserIdEqualTo(cloudUserId);
		List<CloudAdminUser> users = cloudAdminUserMapper.selectByExample(example);
		if(org.apache.commons.collections4.CollectionUtils.isEmpty(users)) {
			return null;
		}
		
		CloudAdminUser user = users.get(0);
		return user;
	}

	@Override
	public UserData getManageUserByCloudUserId(String cloudUserId, CloudManageUserType type) {

		UserExample userExample = new UserExample();
		userExample.createCriteria().andCloudUserIdEqualTo(cloudUserId).andIsCloudManagerEqualTo(new Byte("1"))
				.andCloudManagerTypeEqualTo(type.getType().byteValue());
		List<User> users = userMapper.selectByExample(userExample);
		if (!CollectionUtils.isEmpty(users)) {
			User user = users.get(0);
			UserData res = new UserData();
			BeanUtils.copyProperties(user, res);
			return res;
		}else {
			throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}


	}
}
