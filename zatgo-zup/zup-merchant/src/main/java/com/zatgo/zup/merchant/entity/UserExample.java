package com.zatgo.zup.merchant.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIsNull() {
            addCriterion("user_password is null");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIsNotNull() {
            addCriterion("user_password is not null");
            return (Criteria) this;
        }

        public Criteria andUserPasswordEqualTo(String value) {
            addCriterion("user_password =", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotEqualTo(String value) {
            addCriterion("user_password <>", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordGreaterThan(String value) {
            addCriterion("user_password >", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("user_password >=", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLessThan(String value) {
            addCriterion("user_password <", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLessThanOrEqualTo(String value) {
            addCriterion("user_password <=", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLike(String value) {
            addCriterion("user_password like", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotLike(String value) {
            addCriterion("user_password not like", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIn(List<String> values) {
            addCriterion("user_password in", values, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotIn(List<String> values) {
            addCriterion("user_password not in", values, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordBetween(String value1, String value2) {
            addCriterion("user_password between", value1, value2, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotBetween(String value1, String value2) {
            addCriterion("user_password not between", value1, value2, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordIsNull() {
            addCriterion("user_pay_password is null");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordIsNotNull() {
            addCriterion("user_pay_password is not null");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordEqualTo(String value) {
            addCriterion("user_pay_password =", value, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordNotEqualTo(String value) {
            addCriterion("user_pay_password <>", value, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordGreaterThan(String value) {
            addCriterion("user_pay_password >", value, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("user_pay_password >=", value, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordLessThan(String value) {
            addCriterion("user_pay_password <", value, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordLessThanOrEqualTo(String value) {
            addCriterion("user_pay_password <=", value, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordLike(String value) {
            addCriterion("user_pay_password like", value, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordNotLike(String value) {
            addCriterion("user_pay_password not like", value, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordIn(List<String> values) {
            addCriterion("user_pay_password in", values, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordNotIn(List<String> values) {
            addCriterion("user_pay_password not in", values, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordBetween(String value1, String value2) {
            addCriterion("user_pay_password between", value1, value2, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andUserPayPasswordNotBetween(String value1, String value2) {
            addCriterion("user_pay_password not between", value1, value2, "userPayPassword");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateIsNull() {
            addCriterion("last_login_date is null");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateIsNotNull() {
            addCriterion("last_login_date is not null");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateEqualTo(Date value) {
            addCriterion("last_login_date =", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateNotEqualTo(Date value) {
            addCriterion("last_login_date <>", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateGreaterThan(Date value) {
            addCriterion("last_login_date >", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateGreaterThanOrEqualTo(Date value) {
            addCriterion("last_login_date >=", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateLessThan(Date value) {
            addCriterion("last_login_date <", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateLessThanOrEqualTo(Date value) {
            addCriterion("last_login_date <=", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateIn(List<Date> values) {
            addCriterion("last_login_date in", values, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateNotIn(List<Date> values) {
            addCriterion("last_login_date not in", values, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateBetween(Date value1, Date value2) {
            addCriterion("last_login_date between", value1, value2, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateNotBetween(Date value1, Date value2) {
            addCriterion("last_login_date not between", value1, value2, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordIsNull() {
            addCriterion("mnemonic_word is null");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordIsNotNull() {
            addCriterion("mnemonic_word is not null");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordEqualTo(String value) {
            addCriterion("mnemonic_word =", value, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordNotEqualTo(String value) {
            addCriterion("mnemonic_word <>", value, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordGreaterThan(String value) {
            addCriterion("mnemonic_word >", value, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordGreaterThanOrEqualTo(String value) {
            addCriterion("mnemonic_word >=", value, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordLessThan(String value) {
            addCriterion("mnemonic_word <", value, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordLessThanOrEqualTo(String value) {
            addCriterion("mnemonic_word <=", value, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordLike(String value) {
            addCriterion("mnemonic_word like", value, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordNotLike(String value) {
            addCriterion("mnemonic_word not like", value, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordIn(List<String> values) {
            addCriterion("mnemonic_word in", values, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordNotIn(List<String> values) {
            addCriterion("mnemonic_word not in", values, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordBetween(String value1, String value2) {
            addCriterion("mnemonic_word between", value1, value2, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andMnemonicWordNotBetween(String value1, String value2) {
            addCriterion("mnemonic_word not between", value1, value2, "mnemonicWord");
            return (Criteria) this;
        }

        public Criteria andInviteCodeIsNull() {
            addCriterion("invite_code is null");
            return (Criteria) this;
        }

        public Criteria andInviteCodeIsNotNull() {
            addCriterion("invite_code is not null");
            return (Criteria) this;
        }

        public Criteria andInviteCodeEqualTo(String value) {
            addCriterion("invite_code =", value, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeNotEqualTo(String value) {
            addCriterion("invite_code <>", value, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeGreaterThan(String value) {
            addCriterion("invite_code >", value, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeGreaterThanOrEqualTo(String value) {
            addCriterion("invite_code >=", value, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeLessThan(String value) {
            addCriterion("invite_code <", value, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeLessThanOrEqualTo(String value) {
            addCriterion("invite_code <=", value, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeLike(String value) {
            addCriterion("invite_code like", value, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeNotLike(String value) {
            addCriterion("invite_code not like", value, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeIn(List<String> values) {
            addCriterion("invite_code in", values, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeNotIn(List<String> values) {
            addCriterion("invite_code not in", values, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeBetween(String value1, String value2) {
            addCriterion("invite_code between", value1, value2, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInviteCodeNotBetween(String value1, String value2) {
            addCriterion("invite_code not between", value1, value2, "inviteCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeIsNull() {
            addCriterion("invited_code is null");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeIsNotNull() {
            addCriterion("invited_code is not null");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeEqualTo(String value) {
            addCriterion("invited_code =", value, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeNotEqualTo(String value) {
            addCriterion("invited_code <>", value, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeGreaterThan(String value) {
            addCriterion("invited_code >", value, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeGreaterThanOrEqualTo(String value) {
            addCriterion("invited_code >=", value, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeLessThan(String value) {
            addCriterion("invited_code <", value, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeLessThanOrEqualTo(String value) {
            addCriterion("invited_code <=", value, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeLike(String value) {
            addCriterion("invited_code like", value, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeNotLike(String value) {
            addCriterion("invited_code not like", value, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeIn(List<String> values) {
            addCriterion("invited_code in", values, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeNotIn(List<String> values) {
            addCriterion("invited_code not in", values, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeBetween(String value1, String value2) {
            addCriterion("invited_code between", value1, value2, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andInvitedCodeNotBetween(String value1, String value2) {
            addCriterion("invited_code not between", value1, value2, "invitedCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeIsNull() {
            addCriterion("email_country_code is null");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeIsNotNull() {
            addCriterion("email_country_code is not null");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeEqualTo(String value) {
            addCriterion("email_country_code =", value, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeNotEqualTo(String value) {
            addCriterion("email_country_code <>", value, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeGreaterThan(String value) {
            addCriterion("email_country_code >", value, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("email_country_code >=", value, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeLessThan(String value) {
            addCriterion("email_country_code <", value, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeLessThanOrEqualTo(String value) {
            addCriterion("email_country_code <=", value, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeLike(String value) {
            addCriterion("email_country_code like", value, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeNotLike(String value) {
            addCriterion("email_country_code not like", value, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeIn(List<String> values) {
            addCriterion("email_country_code in", values, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeNotIn(List<String> values) {
            addCriterion("email_country_code not in", values, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeBetween(String value1, String value2) {
            addCriterion("email_country_code between", value1, value2, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailCountryCodeNotBetween(String value1, String value2) {
            addCriterion("email_country_code not between", value1, value2, "emailCountryCode");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeIsNull() {
            addCriterion("mobile_country_code is null");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeIsNotNull() {
            addCriterion("mobile_country_code is not null");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeEqualTo(String value) {
            addCriterion("mobile_country_code =", value, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeNotEqualTo(String value) {
            addCriterion("mobile_country_code <>", value, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeGreaterThan(String value) {
            addCriterion("mobile_country_code >", value, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("mobile_country_code >=", value, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeLessThan(String value) {
            addCriterion("mobile_country_code <", value, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeLessThanOrEqualTo(String value) {
            addCriterion("mobile_country_code <=", value, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeLike(String value) {
            addCriterion("mobile_country_code like", value, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeNotLike(String value) {
            addCriterion("mobile_country_code not like", value, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeIn(List<String> values) {
            addCriterion("mobile_country_code in", values, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeNotIn(List<String> values) {
            addCriterion("mobile_country_code not in", values, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeBetween(String value1, String value2) {
            addCriterion("mobile_country_code between", value1, value2, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andMobileCountryCodeNotBetween(String value1, String value2) {
            addCriterion("mobile_country_code not between", value1, value2, "mobileCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegistTypeIsNull() {
            addCriterion("regist_type is null");
            return (Criteria) this;
        }

        public Criteria andRegistTypeIsNotNull() {
            addCriterion("regist_type is not null");
            return (Criteria) this;
        }

        public Criteria andRegistTypeEqualTo(Byte value) {
            addCriterion("regist_type =", value, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeNotEqualTo(Byte value) {
            addCriterion("regist_type <>", value, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeGreaterThan(Byte value) {
            addCriterion("regist_type >", value, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("regist_type >=", value, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeLessThan(Byte value) {
            addCriterion("regist_type <", value, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeLessThanOrEqualTo(Byte value) {
            addCriterion("regist_type <=", value, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeIn(List<Byte> values) {
            addCriterion("regist_type in", values, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeNotIn(List<Byte> values) {
            addCriterion("regist_type not in", values, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeBetween(Byte value1, Byte value2) {
            addCriterion("regist_type between", value1, value2, "registType");
            return (Criteria) this;
        }

        public Criteria andRegistTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("regist_type not between", value1, value2, "registType");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerIsNull() {
            addCriterion("is_cloud_manager is null");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerIsNotNull() {
            addCriterion("is_cloud_manager is not null");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerEqualTo(Byte value) {
            addCriterion("is_cloud_manager =", value, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andCloudManagerTypeEqualTo(Byte value) {
            addCriterion("cloud_manage_type =", value, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerNotEqualTo(Byte value) {
            addCriterion("is_cloud_manager <>", value, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerGreaterThan(Byte value) {
            addCriterion("is_cloud_manager >", value, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_cloud_manager >=", value, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerLessThan(Byte value) {
            addCriterion("is_cloud_manager <", value, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerLessThanOrEqualTo(Byte value) {
            addCriterion("is_cloud_manager <=", value, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerIn(List<Byte> values) {
            addCriterion("is_cloud_manager in", values, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerNotIn(List<Byte> values) {
            addCriterion("is_cloud_manager not in", values, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerBetween(Byte value1, Byte value2) {
            addCriterion("is_cloud_manager between", value1, value2, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIsCloudManagerNotBetween(Byte value1, Byte value2) {
            addCriterion("is_cloud_manager not between", value1, value2, "isCloudManager");
            return (Criteria) this;
        }

        public Criteria andIconUrlIsNull() {
            addCriterion("icon_url is null");
            return (Criteria) this;
        }

        public Criteria andIconUrlIsNotNull() {
            addCriterion("icon_url is not null");
            return (Criteria) this;
        }

        public Criteria andIconUrlEqualTo(String value) {
            addCriterion("icon_url =", value, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlNotEqualTo(String value) {
            addCriterion("icon_url <>", value, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlGreaterThan(String value) {
            addCriterion("icon_url >", value, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlGreaterThanOrEqualTo(String value) {
            addCriterion("icon_url >=", value, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlLessThan(String value) {
            addCriterion("icon_url <", value, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlLessThanOrEqualTo(String value) {
            addCriterion("icon_url <=", value, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlLike(String value) {
            addCriterion("icon_url like", value, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlNotLike(String value) {
            addCriterion("icon_url not like", value, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlIn(List<String> values) {
            addCriterion("icon_url in", values, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlNotIn(List<String> values) {
            addCriterion("icon_url not in", values, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlBetween(String value1, String value2) {
            addCriterion("icon_url between", value1, value2, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andIconUrlNotBetween(String value1, String value2) {
            addCriterion("icon_url not between", value1, value2, "iconUrl");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNull() {
            addCriterion("nickname is null");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNotNull() {
            addCriterion("nickname is not null");
            return (Criteria) this;
        }

        public Criteria andNicknameEqualTo(String value) {
            addCriterion("nickname =", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotEqualTo(String value) {
            addCriterion("nickname <>", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThan(String value) {
            addCriterion("nickname >", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThanOrEqualTo(String value) {
            addCriterion("nickname >=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThan(String value) {
            addCriterion("nickname <", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThanOrEqualTo(String value) {
            addCriterion("nickname <=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLike(String value) {
            addCriterion("nickname like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotLike(String value) {
            addCriterion("nickname not like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameIn(List<String> values) {
            addCriterion("nickname in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotIn(List<String> values) {
            addCriterion("nickname not in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameBetween(String value1, String value2) {
            addCriterion("nickname between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotBetween(String value1, String value2) {
            addCriterion("nickname not between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeIsNull() {
            addCriterion("cloud_manage_type is null");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeIsNotNull() {
            addCriterion("cloud_manage_type is not null");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeEqualTo(Byte value) {
            addCriterion("cloud_manage_type =", value, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeNotEqualTo(Byte value) {
            addCriterion("cloud_manage_type <>", value, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeGreaterThan(Byte value) {
            addCriterion("cloud_manage_type >", value, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("cloud_manage_type >=", value, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeLessThan(Byte value) {
            addCriterion("cloud_manage_type <", value, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeLessThanOrEqualTo(Byte value) {
            addCriterion("cloud_manage_type <=", value, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeIn(List<Byte> values) {
            addCriterion("cloud_manage_type in", values, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeNotIn(List<Byte> values) {
            addCriterion("cloud_manage_type not in", values, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeBetween(Byte value1, Byte value2) {
            addCriterion("cloud_manage_type between", value1, value2, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andCloudManageTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("cloud_manage_type not between", value1, value2, "cloudManageType");
            return (Criteria) this;
        }

        public Criteria andAuthLevelIsNull() {
            addCriterion("auth_level is null");
            return (Criteria) this;
        }

        public Criteria andAuthLevelIsNotNull() {
            addCriterion("auth_level is not null");
            return (Criteria) this;
        }

        public Criteria andAuthLevelEqualTo(Byte value) {
            addCriterion("auth_level =", value, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelNotEqualTo(Byte value) {
            addCriterion("auth_level <>", value, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelGreaterThan(Byte value) {
            addCriterion("auth_level >", value, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelGreaterThanOrEqualTo(Byte value) {
            addCriterion("auth_level >=", value, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelLessThan(Byte value) {
            addCriterion("auth_level <", value, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelLessThanOrEqualTo(Byte value) {
            addCriterion("auth_level <=", value, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelIn(List<Byte> values) {
            addCriterion("auth_level in", values, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelNotIn(List<Byte> values) {
            addCriterion("auth_level not in", values, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelBetween(Byte value1, Byte value2) {
            addCriterion("auth_level between", value1, value2, "authLevel");
            return (Criteria) this;
        }

        public Criteria andAuthLevelNotBetween(Byte value1, Byte value2) {
            addCriterion("auth_level not between", value1, value2, "authLevel");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdIsNull() {
            addCriterion("wechat_open_id is null");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdIsNotNull() {
            addCriterion("wechat_open_id is not null");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdEqualTo(String value) {
            addCriterion("wechat_open_id =", value, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdNotEqualTo(String value) {
            addCriterion("wechat_open_id <>", value, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdGreaterThan(String value) {
            addCriterion("wechat_open_id >", value, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdGreaterThanOrEqualTo(String value) {
            addCriterion("wechat_open_id >=", value, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdLessThan(String value) {
            addCriterion("wechat_open_id <", value, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdLessThanOrEqualTo(String value) {
            addCriterion("wechat_open_id <=", value, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdLike(String value) {
            addCriterion("wechat_open_id like", value, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdNotLike(String value) {
            addCriterion("wechat_open_id not like", value, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdIn(List<String> values) {
            addCriterion("wechat_open_id in", values, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdNotIn(List<String> values) {
            addCriterion("wechat_open_id not in", values, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdBetween(String value1, String value2) {
            addCriterion("wechat_open_id between", value1, value2, "wechatOpenId");
            return (Criteria) this;
        }

        public Criteria andWechatOpenIdNotBetween(String value1, String value2) {
            addCriterion("wechat_open_id not between", value1, value2, "wechatOpenId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}