package com.zatgo.zup.merchant.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("出行人信息")
public class PassengerParams extends PassengerIdcardParams {
	
	@ApiModelProperty("出行人姓名")
	private String name;
	
	@ApiModelProperty("联系电话")
	private String phone;
	
	@ApiModelProperty(value = "性别：1-男；2-女", required = false)
	private Byte sex;
	
	@ApiModelProperty(value = "生日", required = false)
	private Date birthday;
	
	@ApiModelProperty(value = "是否成人：0-否；1-是", required = true, allowableValues = "0,1")
	private Byte isAdult;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Byte getSex() {
		return sex;
	}

	public void setSex(Byte sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Byte getIsAdult() {
		return isAdult;
	}

	public void setIsAdult(Byte isAdult) {
		this.isAdult = isAdult;
	}

}
