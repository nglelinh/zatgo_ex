package com.zatgo.zup.merchant.service.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.imageio.ImageIO;

import com.zatgo.zup.common.model.wx.ThirdLoginInfo;
import com.zatgo.zup.common.utils.UUIDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.encrypt.BCryptUtil;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.enumtype.BusinessEnum.OssBucket;
import com.zatgo.zup.common.enumtype.BusinessEnum.UserCertificateStatus;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.UserBasicData;
import com.zatgo.zup.common.model.UserBasicParams;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.merchant.entity.Certificate;
import com.zatgo.zup.merchant.entity.CloudAdminUser;
import com.zatgo.zup.merchant.entity.User;
import com.zatgo.zup.merchant.entity.UserExample;
import com.zatgo.zup.merchant.entity.UserExt;
import com.zatgo.zup.merchant.mapper.CertificateMapper;
import com.zatgo.zup.merchant.mapper.UserExtMapper;
import com.zatgo.zup.merchant.mapper.UserMapper;
import com.zatgo.zup.merchant.model.CertificateApproveParams;
import com.zatgo.zup.merchant.model.CertificateParams;
import com.zatgo.zup.merchant.model.UserFollowParams;
import com.zatgo.zup.merchant.remoteservice.TaskRemoteService;
import com.zatgo.zup.merchant.service.CloudAdminUserService;
import com.zatgo.zup.merchant.service.OssService;
import com.zatgo.zup.merchant.service.UserService;
import com.zatgo.zup.merchant.util.AuthTokenUtil;
import com.zatgo.zup.merchant.util.RandomUtil;

import sun.font.FontDesignMetrics;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RedisLockUtils redisLockUtils;
	@Autowired
	private TaskRemoteService taskRemoteService;
	@Autowired
	private CloudAdminUserService cloudAdminUserService;
	@Autowired
	private UserExtMapper userExtMapper;
	@Autowired
	private OssService ossService;
	@Autowired
	private AuthTokenUtil authTokenUtil;
	@Value("${user.inviteImg.name:invite.png}")
	private String inviteImg;
	
	@Value("${user.inviteImg.color:73,118,194}")
	private String inviteColor;
	
	@Autowired
	private CertificateMapper certificateMapper;

	@Override
	public User getUser(String userName, String cloudUserId) {
		if(userName == null) {
			throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		User user = userMapper.selectByUserName(userName, cloudUserId);
		return user;
	}

	@Override
	@Transactional
	public void updateLastLoginTime(String userId) {
		userMapper.updateLastLoginTime(userId, new Date());
	}

	@Override
	public User getUserById(String userId) {
		User user = userMapper.selectByPrimaryKey(userId);
		if(user == null){
		    throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		return user;
	}

	@Transactional
	@Override
	public String registerCreateUser(UserData userData, String deviceNo,String loginName) {
		//检查恶意注册
		if(!authTokenUtil.checkRegistIsAbnormal(null, deviceNo, loginName)) {
			throw new BusinessException(BusinessExceptionCode.VISIT_TOO_FREQUENTLY);
		}
		String invitedCode = userData.getInvitedCode();

		//验证邀请码是否正常
		if(invitedCode != null && !invitedCode.trim().equals("")) {
			UserExample invitedCodeExample = new UserExample();
			invitedCodeExample.createCriteria().andInviteCodeEqualTo(invitedCode.toUpperCase()).andCloudUserIdEqualTo(userData.getCloudUserId());
			List<User> invitedCodeUsers = userMapper.selectByExample(invitedCodeExample);
			if(invitedCodeUsers == null || invitedCodeUsers.size() == 0) {
				throw new BusinessException(BusinessExceptionCode.MERCHANT_EXCEPTION_INVITED_CODE);
			}
		}
		//这里是完成邀请码的任务,报错忽略,不影响注册
		if (!StringUtils.isEmpty(invitedCode)){
			try {
				invitationTaskDone(invitedCode, userData.getCloudUserId());
			} catch (Exception e){
				logger.error("", e);
			}
		}
		return createUser(userData);
	}

	@Override
	@Transactional
	public String createUser(UserData userData) {

		String userId = UUID.randomUUID().toString();
		String cloudUserId = userData.getCloudUserId();

		UserExample userExample = new UserExample();
		//获取行号
		Integer rowNumber = userMapper.countByExample(userExample);
		String code = RandomUtil.toSerialCode(rowNumber + 1);

		User user = new User();
		user.setInviteCode(code);
		user.setCreateDate(new Date());
		user.setUpdateDate(new Date());
		user.setUserId(userId);
		user.setUserPassword(BCryptUtil.encrypt("", userData.getUserPassword()));
		user.setUserPayPassword(BCryptUtil.encrypt("", userData.getUserPayPassword()));
		user.setMnemonicWord(userData.getMnemonicWord());
		user.setInvitedCode(userData.getInvitedCode());
		user.setCloudUserId(cloudUserId);
		user.setIsCloudManager(userData.getIsCloudManager());
		user.setEmail(userData.getEmail());
		user.setEmailCountryCode(userData.getEmailCountryCode());
		user.setMobile(userData.getMobile());
		user.setMobileCountryCode(userData.getMobileCountryCode());
		user.setIconUrl(userData.getIconUrl());
		user.setNickname(userData.getNickname());
		user.setUserName(userData.getUserName());
		user.setRegistType(userData.getRegistType());
		user.setWechatOpenId(userData.getWechatOpenid());
		//如果设置成管理员,校验前云用户管理员是否存在
		createCloudAdminUser(user);
		userMapper.insertSelective(user);

		return userId;
	}

	@Override
	public void editLoginPassword(String userId, String loginPassword) {
		String encryptPass = BCryptUtil.encrypt("", loginPassword);
		userMapper.updateLoginPassword(userId, encryptPass);
	}

	@Override
	public void editPayPassword(String userId, String payPassword) {
		String encryptPass = BCryptUtil.encrypt("", payPassword);
		userMapper.updatePayPassword(userId, encryptPass);
		
	}

	@Override
	public void editLoginPasswordByUserName(String userName, String loginPassword) {
		String encryptPass = BCryptUtil.encrypt("", loginPassword);
		userMapper.updateLoginPasswordByUserName(userName, encryptPass);
	}

	@Override
	public User getUserByMW(String mw) {
		User user = userMapper.selectByMW(mw);
		return user;
		
	}

	@Override
	public List<User> getUserByInvitationCode(String invitationCode) {
		UserExample example = new UserExample();
		example.createCriteria().andInvitedCodeEqualTo(invitationCode);
		return userMapper.selectByExample(example);
	}

	@Override
	public String getUserIdByInvitationCode(String invitationCode) {
		UserExample example = new UserExample();
		example.createCriteria().andInviteCodeEqualTo(invitationCode);
		List<User> users = userMapper.selectByExample(example);
		return CollectionUtils.isEmpty(users) ? null : users.get(0).getUserId();
	}

	@Override
	public User getUserByMobileOrEmailOrMnemonicWord(String parameter, String type, String cloudUserId) {
		UserExample example = new UserExample();
		UserExample.Criteria criteria = example.createCriteria();
		criteria.andRegistTypeEqualTo(new Byte(type));
		criteria.andUserNameEqualTo(parameter);
		criteria.andCloudUserIdEqualTo(cloudUserId);
		List<User> users = userMapper.selectByExample(example);
		return CollectionUtils.isEmpty(users) ? null : users.get(0);
	}

	@Override
	public String updateInvitationCode() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int pageNo = 0;
		int pageSize = 200;
		int total = userMapper.countByExample(new UserExample());
		logger.error("开始升级数据");
		for (; pageNo <= total; pageNo += pageSize){
			logger.error("pageNo:" + pageNo);
			logger.error("pageSize:" + pageSize);
			logger.error("总数:" + total);
			logger.error("时间:" + sdf.format(new Date()));
			executeUpdate(pageNo, pageSize);
			logger.error("==============================================================");
		}
		logger.error("数据升级完成");
		return null;
	}


	private int executeUpdate(int pageNo, int pageSize){
		UserExample example = new UserExample();
		example.setOrderByClause("create_date asc");
		PageHelper.offsetPage(pageNo, pageSize);
		List<User> users = userMapper.selectByExample(example);
		List<User> list = users;
		if (!CollectionUtils.isEmpty(list)){
			for (int i = list.size() - 1; i >= 0 ; i--){
				User user = list.get(i);
				if (StringUtils.isEmpty(user.getInviteCode())){
					user.setInviteCode(RandomUtil.toSerialCode(pageNo + i));
					continue;
				}
				list.remove(i);
			}
			if (!CollectionUtils.isEmpty(list)) {
				userMapper.updateInvitationCode(list);
			}
		}
		return Integer.valueOf(list.size() + "");
	}


	private void invitationTaskDone(String invitationCode, String cloudUserId){
		//看看这个邀请码是否存在
		String userIdByInvitationCode = getUserIdByInvitationCode(invitationCode);
		if (StringUtils.isEmpty(userIdByInvitationCode))
			return;
		taskRemoteService.invitationTaskDone(0l, userIdByInvitationCode, cloudUserId, UUIDUtils.getUuid());
	}

	/**
	 * 创建云用户
	 * @param user
	 */
	private void createCloudAdminUser(User user){
		if ("1".equals(user.getIsCloudManager().toString())){
			UserExample userExample = new UserExample();
			userExample.createCriteria().andCloudUserIdEqualTo(user.getCloudUserId()).andIsCloudManagerEqualTo(new Byte("1"));
			int count = userMapper.countByExample(userExample);
			if (count > 0)
				throw new BusinessException(BusinessExceptionCode.CLOUD_USER_MANAGE_IS_EXIST);
			CloudAdminUser cloudAdminUser = new CloudAdminUser();
			cloudAdminUser.setId(user.getUserId());
			cloudAdminUser.setCloudUserId(user.getCloudUserId());
			cloudAdminUser.setName(user.getUserName());
			cloudAdminUser.setPassword(user.getUserPassword());
			cloudAdminUserService.createCloudAdminUser(cloudAdminUser);
		}
	}

	@Override
	public void updateUserBasicsInfo(UserBasicParams params, String userId) {
		User record = new User();
		record.setUserId(userId);
		record.setIconUrl(params.getIconUrl());
		record.setNickname(params.getNickname());
		userMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public void addUserWechatFollow(UserFollowParams params) {
		String openid = params.getOpenid();
		if(org.apache.commons.lang3.StringUtils.isEmpty(openid)) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "openid miss");
		}
		
		UserExt record = new UserExt();
		record.setUserId(params.getUserId());
		record.setWechatOpenid(openid);
		
		userExtMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<UserBasicData> getInviteList(String inviteCode) {
		return userMapper.selectInviteList(inviteCode);
	}

	@Override
	public String getInviteImg(String userId, String inviteCode, String imgName) {
		String fileName = StringUtils.isEmpty(imgName) ? inviteImg : imgName;
		String ossFileName = new StringBuffer().append(userId).append("_").append(inviteCode).append("_").append(fileName).toString();
		String filePath = ossService.getOssFilePath(OssBucket.USER, ossFileName);
		if (org.apache.commons.lang3.StringUtils.isNotEmpty(filePath)) {
			return filePath;
		}

		BufferedImage bimg;
		try {
			ClassPathResource resource = new ClassPathResource("image/" + fileName);
			bimg = ImageIO.read(resource.getInputStream());

			// 得到Graphics2D 对象
			Graphics2D g2d = (Graphics2D) bimg.getGraphics();
			// 设置颜色和画笔粗细
			String[] color = inviteColor.split(",");
			g2d.setColor(new Color(Integer.valueOf(color[0]), Integer.valueOf(color[1]), Integer.valueOf(color[2])));
			g2d.setStroke(new BasicStroke(1));
			g2d.setFont(new Font("方正楷体", Font.PLAIN, 90));

			// 计算邀请码像素
			FontMetrics fm = FontDesignMetrics.getMetrics(new Font("方正楷体", java.awt.Font.PLAIN, 90));
			int w = fm.stringWidth(inviteCode);

			// 计算邀请码间距
			int gap = 350 - w;

			// 绘制图案或文字
			g2d.drawString(inviteCode, 204  + gap / 2, 1050);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(bimg, "png", os);
			InputStream is = new ByteArrayInputStream(os.toByteArray());
			filePath = ossService.upFileToOss(OssBucket.USER, is, ossFileName);
			return filePath;
		} catch (Exception e) {
			logger.error("", e);
			throw new BusinessException();
		}
	}

	@Override
	public UserExt getUserExt(String userName, String cloudUserId) {
		User user = getUser(userName, cloudUserId);
		if(user == null) {
			throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		UserExt userExt = getUserExtByUserId(user.getUserId());
		return userExt;
	}

	@Override
	public UserExt getUserExtByUserId(String userId) {
		UserExt userExt = userExtMapper.selectByPrimaryKey(userId);
		if(userExt == null) {
			userExt = new UserExt();
		}
		return userExt;
	}
	
	public static void main(String[] args) {
		System.out.println(BCryptUtil.encrypt("", "123456a"));
	}

	@Override
	public Boolean updateCertificate(CertificateParams params,String userId) {
		Certificate certificate = certificateMapper.selectByPrimaryKey(userId);
		if(certificate != null && !certificate.getApproveStatus().equals(Byte.valueOf(BusinessEnum.UserCertificateStatus.approveFail.getCode()))) {
			throw new BusinessException(BusinessExceptionCode.REPETITIVE_DATA_PROCESSING);
		}
		
		Boolean isUpdate = true;
		if(certificate == null) {
			certificate = new Certificate();
			certificate.setCreateDate(new Date());
			isUpdate = false;
		}
		
		certificate.setApproveStatus(Byte.valueOf(BusinessEnum.UserCertificateStatus.applying.getCode()));
		certificate.setCertificateId(params.getCertificateId());
		certificate.setCertificateType(params.getCertificateType());
		certificate.setCountryCode(params.getCountryCode());
		certificate.setFirstName(params.getFirstName());
		certificate.setFirstPhotoUrl(params.getFirstPhotoUrl());
		certificate.setHoldPhotoUrl(params.getHoldPhotoUrl());
		certificate.setLastName(params.getLastName());
		certificate.setSecondPhotoUrl(params.getSecondPhotoUrl());
		certificate.setUpdateDate(new Date());
		certificate.setUserId(userId);
		
		if(isUpdate == true) {
			certificateMapper.updateByPrimaryKeySelective(certificate);
		}else {
			certificateMapper.insert(certificate);
		}
		
		return true;
	}

	@Override
	public PageInfo<Certificate> getAllCertificateByStatus(UserCertificateStatus status,
			String pageNo, String pageSize) {
		PageHelper.startPage(Integer.valueOf(pageNo), Integer.valueOf(pageSize));
		Byte statusByte = null;
		if(status != null) {
			statusByte = Byte.valueOf(status.getCode());
		}
		Page<Certificate> pageData = certificateMapper.selectByStatus(statusByte);
		
		PageInfo<Certificate> pageInfoOrder =
				new PageInfo<Certificate>(pageData.getPageNum(),pageData.getPageSize(),pageData.getTotal(),pageData.getResult());

		PageHelper.clearPage();
		return pageInfoOrder;
	}

	@Override
	public Certificate getCertificateByUserId(String userId) {
		return certificateMapper.selectByPrimaryKey(userId);
	}

	@Override
	public Boolean approveCertificate(AuthUserInfo adminUser, CertificateApproveParams params) {
		Certificate certificate = certificateMapper.selectByPrimaryKey(params.getUserId());

		Certificate updateCertificate = new Certificate();
		updateCertificate.setUserId(params.getUserId());
		updateCertificate.setApproveDate(new Date());
		updateCertificate.setApproveOperator(adminUser.getAdminName());
		updateCertificate.setApproveOperatorId(adminUser.getAdminId());
		if(params.getIsApprovePass()) {
			updateCertificate.setApproveStatus(Byte.valueOf(BusinessEnum.UserCertificateStatus.approve.getCode()));
			updateCertificate.setApproveDemo("");
			
			User record = new User();
			record.setUserId(params.getUserId());
			record.setAuthLevel(Byte.valueOf(BusinessEnum.UserAuthLevel.certificateAuth.getCode()));
			userMapper.updateByPrimaryKeySelective(record);
		}else {
			updateCertificate.setApproveStatus(Byte.valueOf(BusinessEnum.UserCertificateStatus.approveFail.getCode()));
			updateCertificate.setApproveDemo(params.getApproveDemo());
			
			User record = new User();
			record.setUserId(params.getUserId());
			record.setAuthLevel(Byte.valueOf(BusinessEnum.UserAuthLevel.unverified.getCode()));
			userMapper.updateByPrimaryKeySelective(record);
		}
		
		certificateMapper.updateByPrimaryKeySelective(updateCertificate);
		return true;
		
	}

	@Override
	public User getExistUserByWechatOpenId(String wxOpenId, String cloudUserId) {
		UserExample example = new UserExample();
		example.createCriteria().andWechatOpenIdEqualTo(wxOpenId).andCloudUserIdEqualTo(cloudUserId);
		List<User> users = userMapper.selectByExample(example);
		return CollectionUtils.isEmpty(users) ? null : users.get(0);
	}

}
