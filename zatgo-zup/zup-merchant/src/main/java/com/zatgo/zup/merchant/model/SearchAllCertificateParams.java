package com.zatgo.zup.merchant.model;

import com.zatgo.zup.common.enumtype.BusinessEnum;

import io.swagger.annotations.ApiModelProperty;

public class SearchAllCertificateParams {

	@ApiModelProperty(value="页码",required=true)
	private String pageNo;
	@ApiModelProperty(value="每页条数",required=true)
	private String pageSize;
	@ApiModelProperty(value="查询指定状态的申请单，如果不填为所有",required=true)
	private BusinessEnum.UserCertificateStatus status;
	
	public String getPageNo() {
		return pageNo;
	}
	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public BusinessEnum.UserCertificateStatus getStatus() {
		return status;
	}
	public void setStatus(BusinessEnum.UserCertificateStatus status) {
		this.status = status;
	}

	
	
}
