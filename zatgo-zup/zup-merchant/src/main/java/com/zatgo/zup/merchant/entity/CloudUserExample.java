package com.zatgo.zup.merchant.entity;

import java.util.ArrayList;
import java.util.List;

public class CloudUserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CloudUserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCloudUserIdIsNull() {
            addCriterion("cloud_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIsNotNull() {
            addCriterion("cloud_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdEqualTo(String value) {
            addCriterion("cloud_user_id =", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotEqualTo(String value) {
            addCriterion("cloud_user_id <>", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThan(String value) {
            addCriterion("cloud_user_id >", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_id >=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThan(String value) {
            addCriterion("cloud_user_id <", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_id <=", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdLike(String value) {
            addCriterion("cloud_user_id like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotLike(String value) {
            addCriterion("cloud_user_id not like", value, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdIn(List<String> values) {
            addCriterion("cloud_user_id in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotIn(List<String> values) {
            addCriterion("cloud_user_id not in", values, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdBetween(String value1, String value2) {
            addCriterion("cloud_user_id between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserIdNotBetween(String value1, String value2) {
            addCriterion("cloud_user_id not between", value1, value2, "cloudUserId");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameIsNull() {
            addCriterion("cloud_user_name is null");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameIsNotNull() {
            addCriterion("cloud_user_name is not null");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameEqualTo(String value) {
            addCriterion("cloud_user_name =", value, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameNotEqualTo(String value) {
            addCriterion("cloud_user_name <>", value, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameGreaterThan(String value) {
            addCriterion("cloud_user_name >", value, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("cloud_user_name >=", value, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameLessThan(String value) {
            addCriterion("cloud_user_name <", value, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameLessThanOrEqualTo(String value) {
            addCriterion("cloud_user_name <=", value, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameLike(String value) {
            addCriterion("cloud_user_name like", value, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameNotLike(String value) {
            addCriterion("cloud_user_name not like", value, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameIn(List<String> values) {
            addCriterion("cloud_user_name in", values, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameNotIn(List<String> values) {
            addCriterion("cloud_user_name not in", values, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameBetween(String value1, String value2) {
            addCriterion("cloud_user_name between", value1, value2, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andCloudUserNameNotBetween(String value1, String value2) {
            addCriterion("cloud_user_name not between", value1, value2, "cloudUserName");
            return (Criteria) this;
        }

        public Criteria andSmsSignIsNull() {
            addCriterion("sms_sign is null");
            return (Criteria) this;
        }

        public Criteria andSmsSignIsNotNull() {
            addCriterion("sms_sign is not null");
            return (Criteria) this;
        }

        public Criteria andSmsSignEqualTo(String value) {
            addCriterion("sms_sign =", value, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignNotEqualTo(String value) {
            addCriterion("sms_sign <>", value, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignGreaterThan(String value) {
            addCriterion("sms_sign >", value, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignGreaterThanOrEqualTo(String value) {
            addCriterion("sms_sign >=", value, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignLessThan(String value) {
            addCriterion("sms_sign <", value, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignLessThanOrEqualTo(String value) {
            addCriterion("sms_sign <=", value, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignLike(String value) {
            addCriterion("sms_sign like", value, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignNotLike(String value) {
            addCriterion("sms_sign not like", value, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignIn(List<String> values) {
            addCriterion("sms_sign in", values, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignNotIn(List<String> values) {
            addCriterion("sms_sign not in", values, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignBetween(String value1, String value2) {
            addCriterion("sms_sign between", value1, value2, "smsSign");
            return (Criteria) this;
        }

        public Criteria andSmsSignNotBetween(String value1, String value2) {
            addCriterion("sms_sign not between", value1, value2, "smsSign");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdIsNull() {
            addCriterion("weixin_official_acct_app_id is null");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdIsNotNull() {
            addCriterion("weixin_official_acct_app_id is not null");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdEqualTo(String value) {
            addCriterion("weixin_official_acct_app_id =", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdNotEqualTo(String value) {
            addCriterion("weixin_official_acct_app_id <>", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdGreaterThan(String value) {
            addCriterion("weixin_official_acct_app_id >", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdGreaterThanOrEqualTo(String value) {
            addCriterion("weixin_official_acct_app_id >=", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdLessThan(String value) {
            addCriterion("weixin_official_acct_app_id <", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdLessThanOrEqualTo(String value) {
            addCriterion("weixin_official_acct_app_id <=", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdLike(String value) {
            addCriterion("weixin_official_acct_app_id like", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdNotLike(String value) {
            addCriterion("weixin_official_acct_app_id not like", value, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdIn(List<String> values) {
            addCriterion("weixin_official_acct_app_id in", values, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdNotIn(List<String> values) {
            addCriterion("weixin_official_acct_app_id not in", values, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdBetween(String value1, String value2) {
            addCriterion("weixin_official_acct_app_id between", value1, value2, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctAppIdNotBetween(String value1, String value2) {
            addCriterion("weixin_official_acct_app_id not between", value1, value2, "weixinOfficialAcctAppId");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyIsNull() {
            addCriterion("weixin_official_acct_key is null");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyIsNotNull() {
            addCriterion("weixin_official_acct_key is not null");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyEqualTo(String value) {
            addCriterion("weixin_official_acct_key =", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyNotEqualTo(String value) {
            addCriterion("weixin_official_acct_key <>", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyGreaterThan(String value) {
            addCriterion("weixin_official_acct_key >", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyGreaterThanOrEqualTo(String value) {
            addCriterion("weixin_official_acct_key >=", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyLessThan(String value) {
            addCriterion("weixin_official_acct_key <", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyLessThanOrEqualTo(String value) {
            addCriterion("weixin_official_acct_key <=", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyLike(String value) {
            addCriterion("weixin_official_acct_key like", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyNotLike(String value) {
            addCriterion("weixin_official_acct_key not like", value, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyIn(List<String> values) {
            addCriterion("weixin_official_acct_key in", values, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyNotIn(List<String> values) {
            addCriterion("weixin_official_acct_key not in", values, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyBetween(String value1, String value2) {
            addCriterion("weixin_official_acct_key between", value1, value2, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andWeixinOfficialAcctKeyNotBetween(String value1, String value2) {
            addCriterion("weixin_official_acct_key not between", value1, value2, "weixinOfficialAcctKey");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlIsNull() {
            addCriterion("third_authentication_url is null");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlIsNotNull() {
            addCriterion("third_authentication_url is not null");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlEqualTo(String value) {
            addCriterion("third_authentication_url =", value, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlNotEqualTo(String value) {
            addCriterion("third_authentication_url <>", value, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlGreaterThan(String value) {
            addCriterion("third_authentication_url >", value, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlGreaterThanOrEqualTo(String value) {
            addCriterion("third_authentication_url >=", value, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlLessThan(String value) {
            addCriterion("third_authentication_url <", value, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlLessThanOrEqualTo(String value) {
            addCriterion("third_authentication_url <=", value, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlLike(String value) {
            addCriterion("third_authentication_url like", value, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlNotLike(String value) {
            addCriterion("third_authentication_url not like", value, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlIn(List<String> values) {
            addCriterion("third_authentication_url in", values, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlNotIn(List<String> values) {
            addCriterion("third_authentication_url not in", values, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlBetween(String value1, String value2) {
            addCriterion("third_authentication_url between", value1, value2, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationUrlNotBetween(String value1, String value2) {
            addCriterion("third_authentication_url not between", value1, value2, "thirdAuthenticationUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlIsNull() {
            addCriterion("third_authentication_admin_url is null");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlIsNotNull() {
            addCriterion("third_authentication_admin_url is not null");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlEqualTo(String value) {
            addCriterion("third_authentication_admin_url =", value, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlNotEqualTo(String value) {
            addCriterion("third_authentication_admin_url <>", value, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlGreaterThan(String value) {
            addCriterion("third_authentication_admin_url >", value, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlGreaterThanOrEqualTo(String value) {
            addCriterion("third_authentication_admin_url >=", value, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlLessThan(String value) {
            addCriterion("third_authentication_admin_url <", value, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlLessThanOrEqualTo(String value) {
            addCriterion("third_authentication_admin_url <=", value, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlLike(String value) {
            addCriterion("third_authentication_admin_url like", value, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlNotLike(String value) {
            addCriterion("third_authentication_admin_url not like", value, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlIn(List<String> values) {
            addCriterion("third_authentication_admin_url in", values, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlNotIn(List<String> values) {
            addCriterion("third_authentication_admin_url not in", values, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlBetween(String value1, String value2) {
            addCriterion("third_authentication_admin_url between", value1, value2, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationAdminUrlNotBetween(String value1, String value2) {
            addCriterion("third_authentication_admin_url not between", value1, value2, "thirdAuthenticationAdminUrl");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenIsNull() {
            addCriterion("third_authentication_token is null");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenIsNotNull() {
            addCriterion("third_authentication_token is not null");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenEqualTo(String value) {
            addCriterion("third_authentication_token =", value, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenNotEqualTo(String value) {
            addCriterion("third_authentication_token <>", value, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenGreaterThan(String value) {
            addCriterion("third_authentication_token >", value, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenGreaterThanOrEqualTo(String value) {
            addCriterion("third_authentication_token >=", value, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenLessThan(String value) {
            addCriterion("third_authentication_token <", value, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenLessThanOrEqualTo(String value) {
            addCriterion("third_authentication_token <=", value, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenLike(String value) {
            addCriterion("third_authentication_token like", value, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenNotLike(String value) {
            addCriterion("third_authentication_token not like", value, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenIn(List<String> values) {
            addCriterion("third_authentication_token in", values, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenNotIn(List<String> values) {
            addCriterion("third_authentication_token not in", values, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenBetween(String value1, String value2) {
            addCriterion("third_authentication_token between", value1, value2, "thirdAuthenticationToken");
            return (Criteria) this;
        }

        public Criteria andThirdAuthenticationTokenNotBetween(String value1, String value2) {
            addCriterion("third_authentication_token not between", value1, value2, "thirdAuthenticationToken");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}