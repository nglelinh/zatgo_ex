package com.zatgo.zup.merchant.mapper;

import com.zatgo.zup.merchant.entity.CloudUser;

public interface CloudUserMapper extends BaseMapper<CloudUser>{
}