package com.zatgo.zup.merchant.entity;

public class CloudAdminUser {
    private String id;

    private String cloudUserId;

    private String name;

    private String password;

    private Byte superAdmin;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId == null ? null : cloudUserId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public Byte getSuperAdmin() {
        return superAdmin;
    }

    public void setSuperAdmin(Byte superAdmin) {
        this.superAdmin = superAdmin;
    }
}