package com.zatgo.zup.merchant.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("乘客证件类型")
public class UmsMemberPassengerIdCardData {
	
	@ApiModelProperty("证件编号")
	private Long id;
	
	@ApiModelProperty("证件类型，1-身份证；2-护照；3-军官证；4-港澳通行证；5-其他证件")
	private Byte idcardType;
	
	@ApiModelProperty("证件号码")
	private String idcardCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Byte getIdcardType() {
		return idcardType;
	}

	public void setIdcardType(Byte idcardType) {
		this.idcardType = idcardType;
	}

	public String getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(String idcardCode) {
		this.idcardCode = idcardCode;
	}

}
