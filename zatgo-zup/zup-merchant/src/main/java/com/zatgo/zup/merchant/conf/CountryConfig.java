package com.zatgo.zup.merchant.conf;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.zatgo.zup.common.model.Country;

public class CountryConfig {

    public static List<Country> getCountryList(){
    	URL url = CountryConfig.class.getClassLoader().getResource("CountryList.xml");
    	List<Country> list = getCountryListFromXml(url,"Country");
    	return list;
    }
    private static List<Country> getCountryListFromXml(URL xmlFile, String nodeName) {
        StringBuffer str = new StringBuffer("");
        Document doc = null;
        List<Country> list = new ArrayList<Country>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(xmlFile.openStream(),"utf-8"));
            String s;
            while ((s = reader.readLine()) != null) {
                str.append(s);
            }
            reader.close();

            doc = DocumentHelper.parseText(str.toString()); // 将字符串转为XML
            Element rootElt = doc.getRootElement(); // 获取根节点

            Iterator<Element> it = rootElt.elementIterator(nodeName);// 获取根节点下所有content
            while (it.hasNext()) {
                Element elementGroupService = (Element) it.next();
                Country baseBean = fromXmlToCountryBean(elementGroupService);
                list.add(baseBean);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("数据解析错误");
        }
        return list;
    }
    
    @SuppressWarnings({ "rawtypes", "deprecation" })
    private static Country fromXmlToCountryBean(Element rootElt) throws Exception {
        Country country = new Country();
        if (rootElt.elementTextTrim("enName") != null && !"".equals(rootElt.elementTextTrim("enName"))) {
            country.setEnName(rootElt.elementTextTrim("enName"));
        }
        if (rootElt.elementTextTrim("cnName") != null && !"".equals(rootElt.elementTextTrim("cnName"))) {
            country.setCnName(rootElt.elementTextTrim("cnName"));
        }
        if (rootElt.elementTextTrim("dialingCode") != null && !"".equals(rootElt.elementTextTrim("dialingCode"))) {
            country.setDialingCode(rootElt.elementTextTrim("dialingCode"));
        }
        if (rootElt.elementTextTrim("NumberCode") != null && !"".equals(rootElt.elementTextTrim("NumberCode"))) {
            country.setNumberCode(rootElt.elementTextTrim("NumberCode"));
        }
        return country;
    }

}
