package com.zatgo.zup.merchant.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Author wangyucong
 * @Date 2019/2/19 11:57
 */
@ApiModel("会员账户信息")
public class UmsMemberParam {

    private String username;

    private String password;

    @ApiModelProperty("微信授权code")
    private String wxCode;

    @ApiModelProperty("微信openId")
    private String wxOpenId;

    @ApiModelProperty(value = "访问类型（1 - 登录，2 - 注册）", required = true)
    private Byte visitType;

    private String cloudUserId;

    private String deviceNo;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getWxCode() {
        return wxCode;
    }

    public void setWxCode(String wxCode) {
        this.wxCode = wxCode;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

    public Byte getVisitType() {
        return visitType;
    }

    public void setVisitType(Byte visitType) {
        this.visitType = visitType;
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }
}