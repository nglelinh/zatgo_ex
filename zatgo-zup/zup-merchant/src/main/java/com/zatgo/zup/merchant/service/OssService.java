package com.zatgo.zup.merchant.service;

import java.io.InputStream;

import com.zatgo.zup.common.enumtype.BusinessEnum.OssBucket;

public interface OssService {
	
	/**
	 * 获取oss访问地址
	 * @param bucket
	 * @param key
	 * @return
	 */
	String getOssFilePath(OssBucket bucket, String key);
	
	String upFileToOss(OssBucket bucket, InputStream input, String fileName);

}
