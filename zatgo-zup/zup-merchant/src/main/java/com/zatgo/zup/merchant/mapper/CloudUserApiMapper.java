package com.zatgo.zup.merchant.mapper;

import com.zatgo.zup.common.model.CloudUserApi;

public interface CloudUserApiMapper extends BaseMapper<CloudUserApi>{
}