package com.zatgo.zup.merchant.third.wechat.service;


import com.zatgo.zup.merchant.third.wechat.model.msg.MsgToken;
import com.zatgo.zup.merchant.third.wechat.model.msg.WechatMsg;

public interface IWechatMsgService {

	MsgToken validateWxToken(MsgToken token);
	
	String wxMsg(WechatMsg msg);
	
}
