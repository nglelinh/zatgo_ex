package com.zatgo.zup.merchant.controller;

import com.zatgo.zup.common.model.Country;
import com.zatgo.zup.merchant.conf.CountryConfig;
import com.zatgo.zup.merchant.entity.CarouselImage;
import com.zatgo.zup.merchant.model.CarouselImageParams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.merchant.service.SystemService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

@RestController
@Api(value="/user/system", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/user/system")
public class SystemController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(SystemController.class);
	
	@Value("${appVersion.ios:}")
	private String iosVersion;
	
	@Autowired
	private SystemService systemService;
	
	@ApiOperation(value = "app版本号相关")
	@RequestMapping(value = "/app/version",name="app版本号相关", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Boolean> findVersionByType(){

		try {
			String appType = getAppType();
			if("0".equals(appType)) {
				String version = getVersion();
				Integer appVersionInt = Integer.valueOf(version.replace(".", ""));
				Integer iosVersionInt = Integer.valueOf(iosVersion.replace(".", ""));
				
				Boolean b = iosVersionInt > appVersionInt;
				return BusinessResponseFactory.createSuccess(b);
			}else {
				return BusinessResponseFactory.createSuccess(false);
			}
			
		}catch(Exception e) {
			logger.error("",e);
			return BusinessResponseFactory.createSuccess(false);
		}
		
	}

	@ApiOperation(value = "获取国家信息")
	@RequestMapping(value = "/country/info",name="获取国家信息", method = RequestMethod.GET)
	@ResponseBody
	public List<Country> getCountryInfo(){
		return CountryConfig.getCountryList();
	}

	@ApiOperation(value = "获取轮播图列表")
	@RequestMapping(value = "/carouselImage/list", name = "获取轮播图列表", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<List<CarouselImage>> getCarouselImageList(@ModelAttribute CarouselImageParams params) {
		List<CarouselImage> list = systemService.getCarouselImageList(params);
		return BusinessResponseFactory.createSuccess(list);
	}

}
