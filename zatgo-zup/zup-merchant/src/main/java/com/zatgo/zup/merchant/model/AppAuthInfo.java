package com.zatgo.zup.merchant.model;

import io.swagger.annotations.ApiModelProperty;

public class AppAuthInfo {

	@ApiModelProperty(value="应用编号",required=true)
	private String appId;
	
	@ApiModelProperty(value="应用版本",required=true)
	private String appVersion;
	
	@ApiModelProperty(value="应用类型",required=true,allowableValues="1-安卓；2-ios")
	private String appType;
	
	@ApiModelProperty(value="时间戳",required=true)
	private String ts;
	
	@ApiModelProperty(value="鉴权令牌",required=true)
	private String token;
	
	@ApiModelProperty(value="签名",required=true)
	private String sign;

	public AppAuthInfo(String appId, String appType, String appVersion,
			String ts, String token, String sign) {
		this.appId = appId;
		this.appType = appType;
		this.appVersion = appVersion;
		this.ts = ts;
		this.token = token;
		this.sign = sign;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
}
