package com.zatgo.zup.merchant.service;

import java.util.List;

import com.zatgo.zup.merchant.entity.AppVersion;
import com.zatgo.zup.merchant.entity.CarouselImage;
import com.zatgo.zup.merchant.model.CarouselImageParams;

public interface SystemService {

	AppVersion findVersionByType(Byte type);
	
	List<CarouselImage> getCarouselImageList(CarouselImageParams params);

}
