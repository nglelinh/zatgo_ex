package com.zatgo.zup.merchant.controller;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AliyunValidateCode;
import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.common.model.RequestData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.merchant.model.LoginData;
import com.zatgo.zup.merchant.model.LoginParams;
import com.zatgo.zup.merchant.model.UmsMemberParam;
import com.zatgo.zup.merchant.model.WechatLoginParam;
import com.zatgo.zup.merchant.remoteservice.PresentRemoteService;
import com.zatgo.zup.merchant.service.AuthService;
import com.zatgo.zup.merchant.util.AliyunValidataCodeUtil;
import com.zatgo.zup.merchant.util.AuthTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@Api(value="/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/auth")
@RestController
public class AuthController extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	private AuthService authService;

	@Autowired
	protected AuthTokenUtil authTokenUtil;

	@Autowired
	private PresentRemoteService presentRemoteService;

	@Autowired
	private AliyunValidataCodeUtil aliyunValidataCodeUtil;
	
	@Value("${system.isSliderVerification:true}")
	private Boolean isSliderVerification;

	@ApiOperation(value = "登陆接口")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<LoginData> login(@RequestBody LoginParams loginParams, @RequestParam(value = "cloudUserId", required=false) String cloudUserId) {
		if (StringUtils.isEmpty(cloudUserId)){
			cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
		}
		String host = getClientIp();
		if(isSliderVerification == true) {
			AliyunValidateCode aliyunValidateCode = loginParams.getAliyunValidateCode();
			if (aliyunValidateCode == null){
				throw new BusinessException(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
			}
			if (!aliyunValidataCodeUtil.checkAliyunH5Validate(host, loginParams.getAliyunValidateCode())){
				return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.APP_AUTH_VALIDATE_SIGN_ERROR);
			}
		}
		
		//参数校验
		if(StringUtils.isEmpty(loginParams.getLoginName())
				|| StringUtils.isEmpty(loginParams.getLoginPassword())) {
			throw new BusinessException(
					BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		LoginData loginData = authService.login(loginParams, cloudUserId);

		//登入触发赠送
		try {
			ResponseData<Object> returnObj = presentRemoteService.present(loginData.getUserName(), cloudUserId, new RequestData<>());
			if(!returnObj.isSuccessful()) {
				logger.error(loginData.getUserName() + "-赠送失败：" + returnObj.getMessage());
			}
		}catch(Exception e) {
			logger.error(loginData.getUserName() + "-赠送失败:",e);
		}

		return BusinessResponseFactory.createSuccess(loginData);
	}
	
	@ApiOperation(value = "微信授权登陆接口")
	@RequestMapping(value = "/weixinLogin", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<LoginData> weixinLogin(@RequestBody WechatLoginParam param) {
		return authService.weixinLogin(param);
	}

	@ApiOperation(value = "admin登陆接口")
	@RequestMapping(value = "/login/admin", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<LoginData> adminLogin(@RequestBody LoginParams loginParams, @RequestParam(value = "cloudUserId", required=false) String cloudUserId) {
		//参数校验
		if(StringUtils.isEmpty(loginParams.getLoginName())
				|| StringUtils.isEmpty(loginParams.getLoginPassword())) {
			throw new BusinessException(
					BusinessExceptionCode.REQUEST_PARAM_ERROR);
		}

		LoginData loginData = authService.adminLogin(loginParams, cloudUserId);
		loginData.setRoles(Arrays.asList("TEST"));
		return BusinessResponseFactory.createSuccess(loginData);
	}

	@ApiOperation(value = "退出登陆")
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Object> logout(@RequestHeader("token") String token){
		try {
			/**
			 * 参数校验
			 */
			if(StringUtils.isEmpty(token)){
				throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR);
			}

			authTokenUtil.removeToken(token);
		}catch(Exception e) {
			logger.error("",e);
		}

		ResponseData<Object> returnData = BusinessResponseFactory.createSuccess(null);
		return returnData;
	}
}
