package com.zatgo.zup.merchant.service.impl;

import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ykb.mall.common.model.WxAccessTokenData;
import com.ykb.mall.common.utils.UUIDUtil;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.model.wx.WxUserInfoData;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.merchant.model.*;
import com.zatgo.zup.merchant.remoteservice.WechatRemoteService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zatgo.zup.common.encrypt.BCryptUtil;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.merchant.entity.CloudAdminUser;
import com.zatgo.zup.merchant.entity.User;
import com.zatgo.zup.merchant.entity.UserExt;
import com.zatgo.zup.merchant.service.AuthService;
import com.zatgo.zup.merchant.service.CloudUserService;
import com.zatgo.zup.merchant.service.UserService;
import com.zatgo.zup.merchant.util.AuthTokenUtil;

@Service
public class AuthServiceImpl implements AuthService {

	private static final Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);

	@Autowired
	private RedisTemplate redisTemplate;
	
	@Autowired
    protected AuthTokenUtil authTokenUtil;
	
	@Autowired
	protected UserService userService;
	
	@Autowired
	protected CloudUserService cloudUserService;

	@Autowired
	private WechatRemoteService wechatRemoteService;

	@Autowired
	private RedisLockUtils redisLockUtils;
	

	/**
	 * 账号锁定判断次数
	 */
	@Value("${user.login.lock.times}")
	private int lockTimes;
	/**
	 * 账号锁定时长
	 */
	@Value("${user.login.lock.expire}")
	private int lockExpire;
	
	@Override
	@Transactional
	public LoginData login(LoginParams loginParams, String cloudUserId) {
		
		User user = userService.getUser(loginParams.getLoginName(), cloudUserId);
		if(user == null) {
			throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		UserExt userExt = userService.getUserExtByUserId(user.getUserId());
		
		//云用户的统计账户不允许登入
		if(user.getIsCloudManager() != null && user.getIsCloudManager().equals(1)) {
			throw new BusinessException(BusinessExceptionCode.USER_AUTH_ERROR);
		}
		//用户状态判断
		if(userExt != null && userExt.getIsLockLogin() != null &&
				userExt.getIsLockLogin() == true) {
			throw new BusinessException(BusinessExceptionCode.MERCHANT_EXCEPTION_USER);
		}
		
		//用户是否被自动上锁
		String lockKey = RedisKeyConstants.USER_LOGIN_LOCK_PRE+loginParams.getLoginName();
		if(isLockTime(lockKey)){
			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_LOCK);
		}else{
			//已经超过锁定时间，解锁
			releaseLock(lockKey);
		}
		
		//RSA加密参数中的密码
//		String password = "";
//		try {
//			password = RSAServerUtil.decrypt(loginParams.getLoginPassword(),ClientTypeEnum.PHONE);
//		} catch (Exception e) {
//			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_PASSWORD_ERROR);
//		}
		
		//密码校验
        Boolean flag=BCryptUtil.check("" , loginParams.getLoginPassword(), user.getUserPassword());
        if(!flag){
        	//密码校验失败，需要记录失败次数，超过N次则锁定账号N小时（配置文件可配）
        	long residueNum = incLoginFailNumber(lockKey);
        	if(residueNum == 0) {
    			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_LOCK);
        	}else {
    			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_PASSWORD_ERROR);
        	}
        }
        
        //密码校验通过，则解锁redis中的Key
		releaseLock(lockKey);
		//生成登录TOKEN
		AuthUserInfo data = new AuthUserInfo();
		data.setUserName(user.getUserName());
		data.setUserId(user.getUserId());
		data.setInviteCode(user.getInviteCode());
		data.setCloudUserId(user.getCloudUserId());
		data.setWechatOpenId(user.getWechatOpenId());
		data.setIcon(user.getIconUrl());
		data.setNickname(user.getNickname());
		String token = authTokenUtil.generateLoginToken(data);
		
		//生成返回参数
		LoginData loginData = new LoginData();
		loginData.setToken(token);
		loginData.setUserName(user.getUserName());
		loginData.setUserId(user.getUserId());
		loginData.setNickname(user.getNickname());
		loginData.setIcon(user.getIconUrl());
		
		//修改登陆时间
		userService.updateLastLoginTime(user.getUserId());
		
		return loginData;
	}
	
	@Override
	@Transactional
	public LoginData adminLogin(LoginParams loginParams, String cloudUserId) {
		CloudAdminUser user = cloudUserService.selectCloudAdminByName(loginParams.getLoginName(),cloudUserId);
		
		if(user == null) {
			throw new BusinessException(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		
		// 用户是否被自动上锁
		String lockKey = RedisKeyConstants.USER_LOGIN_LOCK_PRE + loginParams.getLoginName();
		if (isLockTime(lockKey)) {
			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_LOCK);
		} else {
			// 已经超过锁定时间，解锁
			releaseLock(lockKey);
		}
		
		//密码校验
        Boolean flag=BCryptUtil.check("" , loginParams.getLoginPassword(), user.getPassword());
        if(!flag){
        	//密码校验失败，需要记录失败次数，超过N次则锁定账号N小时（配置文件可配）
        	long residueNum = incLoginFailNumber(lockKey);
        	if(residueNum == 0) {
    			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_LOCK);
        	}else {
    			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_PASSWORD_ERROR);
        	}
        }
        
        //密码校验通过，则解锁redis中的Key
		releaseLock(lockKey);
		//生成登录TOKEN
		AuthUserInfo data = new AuthUserInfo();
		data.setCloudUserId(cloudUserId);
		data.setAdminId(user.getId());
		data.setAdminName(user.getName());
		data.setIsAdmin(true);
		if((new Byte("1")).equals(user.getSuperAdmin())) {
			data.setIsSuperAdmin(true);
		} else {
			data.setIsSuperAdmin(false);
		}
		String token = authTokenUtil.generateLoginToken(data);
		
		//生成返回参数
		LoginData loginData = new LoginData();
		loginData.setToken(token);
		loginData.setUserName(user.getName());
		loginData.setUserId(user.getId());
		
		return loginData;
	}

	@Override
	public ResponseData<LoginData> weixinLogin(WechatLoginParam param) {
		if (StringUtils.isNotEmpty(param.getWxCode())) {
			WxAccessTokenData data = null;
			try {
				// 获取微信openId
				ResponseData<WxAccessTokenData> wxAccessRes = wechatRemoteService.getWechatOpenId(param.getWxCode());
				if (wxAccessRes == null || !wxAccessRes.isSuccessful()){
					logger.error("", JSONObject.toJSONString(wxAccessRes));
					return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.GET_WECHAT_OPENID_FAIL, "获取微信openId失败");
				}
				data = wxAccessRes.getData();
				String wxOpenId = data.getOpenId();
				if (StringUtils.isNotEmpty(wxOpenId)) {
					String key = RedisKeyConstants.USER_INVITATION_CODE;
					redisLockUtils.lock(key);
					try {
						User existUser = userService.getExistUserByWechatOpenId(wxOpenId, param.getCloudUserId());
						String cloudUserId = param.getCloudUserId();
						String userName = null;
						String password = "123456";
						if(existUser == null) {
							ResponseData<WxUserInfoData> wxUserInfo = wechatRemoteService.getRegisterWxUserInfo(wxOpenId, data.getAccessToken());
							if (wxUserInfo == null || !wxUserInfo.isSuccessful()){
								if("401".equals(wxUserInfo.getCode())){
									logger.error("", wxUserInfo);
									ResponseData businessError = BusinessResponseFactory.createBusinessError("401", "未获取微信授权，请先获取授权");
									ResponseData res = BusinessResponseFactory.createBusinessError(BusinessExceptionCode.GET_WECHAT_OPENID_FAIL, "获取微信openId失败");
									res.setData(businessError);
									return res;
								}
								throw new BusinessException(BusinessExceptionCode.GET_USER_INFO_FAIL);
							}
							WxUserInfoData wxUserInfoData = wxUserInfo.getData();
							UserData userData = new UserData();
							userData.setCloudUserId(cloudUserId);
							userData.setUserId(UUIDUtils.getUuid());
							userData.setUserName(UUIDUtils.getUuid());
							userName = userData.getUserName();
							userData.setUserPassword("123456");
							userData.setUserPayPassword("123456");
							userData.setWechatOpenid(wxOpenId);
							userData.setNickname(wxUserInfoData.getNickName());
							userData.setIconUrl(wxUserInfoData.getHeadimgUrl());
							userData.setIsCloudManager(new Byte("0"));
							userData.setRegistType(new Byte("3"));
							userData.setWechatOpenid(wxOpenId);
							userService.registerCreateUser(userData,userName, userName);
						} else {
							userName = existUser.getUserName();
						}
						LoginParams params = new LoginParams();
						params.setLoginName(userName);
						params.setLoginPassword(password);
						LoginData loginData = login(params, cloudUserId);
						return BusinessResponseFactory.createSuccess(loginData);
					}finally {
						redisLockUtils.releaseLock(key);
					}



				} else {
					logger.error("未获取到微信openId，param:{}", JSON.toJSONString(param));
					return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.GET_WECHAT_OPENID_FAIL, "获取微信openId失败");

				}
			} catch (Exception e) {
				logger.error("", e);
				return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.GET_WECHAT_OPENID_FAIL, "获取微信openId失败");
			}

		}
		return null;
	}


	/**
	 * 记录用户登录失败次数
	 * @param lockKey
	 * @return 返回剩余次数
	 */
	private long incLoginFailNumber(String lockKey) {
		
		long times = 1;
		
		if(!redisTemplate.hasKey(lockKey)){
			redisTemplate.opsForValue().set(lockKey,String.valueOf(times));
		}else{
			times = redisTemplate.opsForValue().increment(lockKey, 1);
		}
		//设置失效时间
		redisTemplate.expire(lockKey, lockExpire,TimeUnit.SECONDS);

		return lockTimes - times;
	}
	
	/**
	 * 判断账号是否在锁定期内
	 * @param lockKey
	 * @return
	 */
	private boolean isLockTime(String lockKey) {
		if(redisTemplate.hasKey(lockKey)){
			int times = Integer.parseInt((String)redisTemplate.opsForValue().get(lockKey));
			if(times>=lockTimes){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 释放锁定的计数
	 * @param lockKey
	 */
	private void releaseLock(String lockKey) {
		redisTemplate.delete(lockKey);
	}


}
