package com.zatgo.zup.merchant.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zatgo.zup.common.encrypt.BCryptUtil;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.model.CloudSystemConstant;
import com.zatgo.zup.common.model.EditPasswordParams;
import com.zatgo.zup.common.model.PageInfo;
import com.zatgo.zup.common.model.RequestData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.UserBasicData;
import com.zatgo.zup.common.model.UserBasicParams;
import com.zatgo.zup.common.model.UserData;
import com.zatgo.zup.common.model.UserExtModel;
import com.zatgo.zup.common.redis.RedisKeyConstants;
import com.zatgo.zup.common.redis.RedisLockUtils;
import com.zatgo.zup.merchant.entity.Certificate;
import com.zatgo.zup.merchant.entity.User;
import com.zatgo.zup.merchant.entity.UserExt;
import com.zatgo.zup.merchant.model.CertificateApproveParams;
import com.zatgo.zup.merchant.model.CertificateParams;
import com.zatgo.zup.merchant.model.ForgetPasswordParams;
import com.zatgo.zup.merchant.model.SearchAllCertificateParams;
import com.zatgo.zup.merchant.model.UserFollowParams;
import com.zatgo.zup.merchant.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@Api(value="/user", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/user")
public class UserController extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
	@ApiOperation(value = "根据用户名查询用户信息")
	@RequestMapping(value = "/byname/{userName}",name="根据用户名查询用户信息", method = RequestMethod.GET)
	@ResponseBody
    public ResponseData<UserData> getUser(@PathVariable("userName") String userName, @RequestParam(value = "cloudUserId", required=false) String cloudUserId){
		if (StringUtils.isEmpty(cloudUserId)){
			cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
		}
		User user = userService.getUser(userName, cloudUserId);
		if(user == null) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		
		UserExt userExt = userService.getUserExtByUserId(user.getUserId());
		
		UserData userData = new UserData();
		try {
			BeanUtils.copyProperties(userData, userExt);
			BeanUtils.copyProperties(userData, user);
		} catch (IllegalAccessException e) {
			logger.error("",e);
		} catch (InvocationTargetException e) {
			logger.error("",e);
		}
		return BusinessResponseFactory.createSuccess(userData);
    }
	
	@ApiOperation(value = "根据用户名查询用户基本信息")
	@RequestMapping(value = "/basicbyname/{userName}",name="根据用户名查询用户信息", method = RequestMethod.GET)
	@ResponseBody
    public ResponseData<UserBasicData> getUserBasicInfo(@PathVariable("userName") String userName, @RequestParam(value = "cloudUserId", required = false) String cloudUserId){
		if (StringUtils.isEmpty(cloudUserId)){
			cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
		}
		User user = userService.getUser(userName, cloudUserId);
		if(user == null) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.USER_IS_NOT_EXIST);
		}
		
		
		UserBasicData userBasicData = new UserBasicData();
		try {
			BeanUtils.copyProperties(userBasicData, user);
		} catch (IllegalAccessException e) {
			logger.error("",e);
		} catch (InvocationTargetException e) {
			logger.error("",e);
		}
		return BusinessResponseFactory.createSuccess(userBasicData);
    }
	
	@ApiOperation(value = "查询当前用户基本信息")
	@RequestMapping(value = "/basicInfo",name="查询当前用户基本信息", method = RequestMethod.GET)
	@ResponseBody
    public ResponseData<UserBasicData> getUserBasicInfo(){
		AuthUserInfo userInfo = getUserInfo();
		User user = userService.getUserById(userInfo.getUserId());
		
		UserBasicData userBasicData = new UserBasicData();
		try {
			BeanUtils.copyProperties(userBasicData, user);
		} catch (IllegalAccessException e) {
			logger.error("",e);
		} catch (InvocationTargetException e) {
			logger.error("",e);
		}
		
		String url = userService.getInviteImg(userInfo.getUserId(), user.getInviteCode(), null);
		userBasicData.setInviteImg(url);
		return BusinessResponseFactory.createSuccess(userBasicData);
    }

	@ApiOperation(value = "查询当前用户邀请码图片")
	@RequestMapping(value = "/inviteCodeImage/{imgName}",name="查询当前用户邀请码图片", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<String> inviteCodeImage(@PathVariable("imgName") String imgName){
		AuthUserInfo userInfo = getUserInfo();
		User user = userService.getUserById(userInfo.getUserId());
		String url = userService.getInviteImg(userInfo.getUserId(), user.getInviteCode(), imgName);
		return BusinessResponseFactory.createSuccess(url);
	}
	
	@ApiOperation(value = "根据用户ID查询用户基础信息")
	@RequestMapping(value = "/basicbyid/{userId}",name="根据用户ID查询用户基础信息", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<UserBasicData> getUserbasicInfoById(@PathVariable("userId") String userId) {
		User user = userService.getUserById(userId);
		
		UserBasicData userBasicData = new UserBasicData();
		try {
			BeanUtils.copyProperties(userBasicData, user);
		} catch (IllegalAccessException e) {
			logger.error("",e);
		} catch (InvocationTargetException e) {
			logger.error("",e);
		}
		
		String url = userService.getInviteImg(userId, user.getInviteCode(), null);
		userBasicData.setInviteImg(url);
		return BusinessResponseFactory.createSuccess(userBasicData);
	}
	
	@ApiOperation(value = "更新用户登录时间")
	@RequestMapping(value = "/updatelogintime",name="更新用户登录时间", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Object> updateLastLoginTime(@RequestBody String token) {
		AuthUserInfo userInfo = getUserInfo(token);
		userService.updateLastLoginTime(userInfo.getUserId());
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "根据用户ID查询用户信息")
	@RequestMapping(value = "/byid/{userId}",name="根据用户ID查询用户信息", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<UserData> getUserById(@PathVariable("userId") String userId) {
		UserData userData = new UserData();
		User user = userService.getUserById(userId);
		UserExt userExt = userService.getUserExtByUserId(user.getUserId());
		org.springframework.beans.BeanUtils.copyProperties(userExt, userData);
		org.springframework.beans.BeanUtils.copyProperties(user, userData);
		userData.setWechatOpenid(user.getWechatOpenId());
		return BusinessResponseFactory.createSuccess(userData);
	}
	
	@ApiOperation(value = "创建用户")
	@RequestMapping(value = "/create",name="创建用户", method = RequestMethod.POST)
	public ResponseData<Object> createUser(@RequestBody UserData userData) {
		String registType = userData.getRegistType() + "";
		String key = userData.getUserName();
		String cloudUserId = userData.getCloudUserId();
		String userId = null;
		redisLockUtils.lock(key);
		try {
			User existUser = userService.getUserByMobileOrEmailOrMnemonicWord(userData.getUserName(), registType, cloudUserId);
			if(existUser != null) {
				throw new BusinessException(BusinessExceptionCode.USER_IS_EXIST);
			}
			userId = userService.createUser(userData);
			return BusinessResponseFactory.createSuccess(userId);
		} catch(BusinessException e){
			logger.error("",e);
			return BusinessResponseFactory.createBusinessError(e.getCode());
		} catch(Exception e) {
			logger.error("",e);
			throw e;
		}finally {
			redisLockUtils.releaseLock(key);
		}
	}
	
	@ApiOperation(value = "修改登录密码")
	@RequestMapping(value = "/editLoginPassword",name="修改登录密码", method = RequestMethod.POST)
	public ResponseData<Object> editLoginPassword(@RequestBody EditPasswordParams params){
		AuthUserInfo authUserInfo = getUserInfo();
		User user = userService.getUserById(authUserInfo.getUserId());
		//密码验证
		Boolean flag=BCryptUtil.check("" , params.getOldPassword(), user.getUserPassword());
		if(!flag) {
			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_PASSWORD_ERROR);
		}
		
		userService.editLoginPassword(authUserInfo.getUserId(), params.getNewPassword());
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "修改支付密码")
	@RequestMapping(value = "/editPayPassword", name="修改支付密码", method = RequestMethod.POST)
	public ResponseData<Object> editPayPassword(@RequestBody EditPasswordParams params){
		AuthUserInfo authUserInfo = getUserInfo();
		
		User user = userService.getUserById(authUserInfo.getUserId());
		//密码验证
		Boolean flag=BCryptUtil.check("" , params.getOldPassword(), user.getUserPayPassword());
		if(!flag) {
			throw new BusinessException(BusinessExceptionCode.LOGIN_FAIL_PASSWORD_ERROR);
		}
				
		userService.editPayPassword(authUserInfo.getUserId(), params.getNewPassword());
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "通过助记词修改支付密码")
	@RequestMapping(value = "/editPayPasswordByMW", name="通过助记词修改支付密码", method = RequestMethod.POST)
	public ResponseData<Object> editPayPasswordByMW(@RequestBody EditPasswordParams params){

		User user = userService.getUserByMW(params.getMnemonicWord());
				
		userService.editPayPassword(user.getUserId(), params.getNewPassword());
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "通过助记词查询用户")
	@RequestMapping(value = "/getUserByMW", name="通过助记词查询用户", method = RequestMethod.POST)
	public ResponseData<UserData> getUserByMW(@RequestBody RequestData<String> params){

		User user = userService.getUserByMW(params.getData());
		UserData userData = null;
		if(user != null) {
			userData = new UserData();
			try {
				BeanUtils.copyProperties(userData, user);
			} catch (IllegalAccessException e) {
				logger.error("",e);
			} catch (InvocationTargetException e) {
				logger.error("",e);
			}
		}
		
		return BusinessResponseFactory.createSuccess(userData);

	}
	
	@ApiOperation(value = "忘记登录密码")
	@RequestMapping(value = "/forgetLoginPassword",name="忘记登录密码", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Object> forgetLoginPassword(@RequestBody ForgetPasswordParams params){
		//实名认证码（手机或邮件确认码）
		String realNameAuthCode = (String)redisTemplate.opsForValue().get(params.getUserName());
		if(!realNameAuthCode.equals(params.getVerifyCode())) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REAL_NAME_AUTH_FIAL);
		}
		
		userService.editLoginPasswordByUserName(params.getUserName(), params.getNewPassword());
		
		//清除账户密码错误次数太多锁定
		try {
			String lockKey = RedisKeyConstants.USER_LOGIN_LOCK_PRE+params.getUserName();
			redisTemplate.delete(lockKey);
		}catch(Exception e) {
			logger.error("",e);
		}
		
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "忘记支付密码")
	@RequestMapping(value = "/forgetPayPassword",name="忘记支付密码", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Object> forgetPayPassword(@RequestBody ForgetPasswordParams params){
		AuthUserInfo authUserInfo = getUserInfo();
		
		//实名认证码（手机或邮件确认码）
		String realNameAuthCode = (String)redisTemplate.opsForValue().get(params.getUserName());
		if(!realNameAuthCode.equals(params.getVerifyCode())) {
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.REAL_NAME_AUTH_FIAL);
		}
		
		userService.editPayPassword(authUserInfo.getUserId(), params.getNewPassword());
		return BusinessResponseFactory.createSuccess(null);
	}


	@ApiOperation(value = "查询用户名是否存在")
	@RequestMapping(value = "/exist",name="查询用户名是否存在", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<UserBasicData> userExist(@RequestParam("userName") String userName, @RequestParam(value = "cloudUserId", required = false) String cloudUserId){
		if (StringUtils.isEmpty(cloudUserId)){
			cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
		}
		User user = userService.getUser(userName, cloudUserId);
		if(user != null){
			return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.USER_IS_EXIST);
		}
		return BusinessResponseFactory.createSuccess(null);
	}

	@ApiOperation(value = "根据用户邀请码查用户信息(内部调用)")
	@RequestMapping(value = "/userId/invitationCode/{invitationCode}", method = RequestMethod.GET)
	@ResponseBody
	public String getUserIdByInvitationCode(@PathVariable("invitationCode") String invitationCode){
		return userService.getUserIdByInvitationCode(invitationCode);
	}

	@ApiOperation(value = "修邀请码数据专用接口(内部调用)")
	@RequestMapping(value = "/update/invitationCode", method = RequestMethod.GET)
	@ResponseBody
	public String updateInvitationCode(){
		return userService.updateInvitationCode();
	}
	
	@ApiOperation(value = "修改用户基础信息")
	@RequestMapping(value = "/update/basicInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Object> updateUserBasicsInfo(@RequestBody UserBasicParams params){
		String userId = getUserInfo().getUserId();
		userService.updateUserBasicsInfo(params, userId);
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "添加微信关注")
	@RequestMapping(value = "/follow/wechat", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Object> addUserWechatFollow(@RequestBody UserFollowParams params){
		
		String userId = getUserInfo().getUserId();
		params.setUserId(userId);
		userService.addUserWechatFollow(params);
		
		return BusinessResponseFactory.createSuccess(null);
	}
	
	@ApiOperation(value = "邀请好友列表")
	@RequestMapping(value = "/invite/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<List<UserBasicData>> inviteList(){
		
		List<UserBasicData> datas = userService.getInviteList(getUserInfo().getInviteCode());
		return BusinessResponseFactory.createSuccess(datas);
	}

	@ApiOperation(value = "用户黑名单(内部调用)")
	@RequestMapping(value = "/black/list/{userId}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<UserExtModel> getBlackList(@PathVariable(value = "userId") String userId){
		UserExt userExtByUserId = userService.getUserExtByUserId(userId);
		UserExtModel res = new UserExtModel();
		try {
			BeanUtils.copyProperties(res, userExtByUserId);
		} catch (IllegalAccessException e) {
			logger.error("", e);
		} catch (InvocationTargetException e) {
			logger.error("", e);
		}
		return BusinessResponseFactory.createSuccess(res);
	}
	
	@ApiOperation(value = "用户实名认证申请")
	@RequestMapping(value = "/certificate/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Boolean> updateCertificate(@RequestBody CertificateParams params){
		String userId = getUserInfo().getUserId();
		userService.updateCertificate(params, userId);
		return BusinessResponseFactory.createSuccess(true);
	}
	
	@ApiOperation(value = "管理后台查询实名认证申请单")
	@RequestMapping(value = "/certificate/admin/pageInfo/{pageNo}/{pageSize}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<PageInfo<Certificate>> getAllCertificate(@RequestBody SearchAllCertificateParams params){
		AuthUserInfo authUserInfo = getAdminInfo();
		if(authUserInfo.getIsAdmin() == null || !authUserInfo.getIsAdmin()) {
			throw new BusinessException(BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
		}
		PageInfo<Certificate> pageInfo = userService.getAllCertificateByStatus(params.getStatus(), params.getPageNo(), params.getPageSize());
		return BusinessResponseFactory.createSuccess(pageInfo);
	}
	
	
	@ApiOperation(value = "查询用户实名认证信息")
	@RequestMapping(value = "/certificate/basicInfo", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<Certificate> getCertificateByUser(){
		String userId = getUserInfo().getUserId();
		Certificate  certificate  = userService.getCertificateByUserId(userId);
		return BusinessResponseFactory.createSuccess(certificate);
	}
	
	@ApiOperation(value = "管理后台实名认证审批")
	@RequestMapping(value = "/certificate/admin/approve", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData<Boolean> approveCertificate(@RequestBody CertificateApproveParams params){
		AuthUserInfo authUserInfo = getAdminInfo();
		if(authUserInfo.getIsAdmin() == null || !authUserInfo.getIsAdmin()) {
			throw new BusinessException(BusinessExceptionCode.NOT_AUTH_ACCESS_INTERFACE);
		}
		
		userService.approveCertificate(authUserInfo, params);
		return BusinessResponseFactory.createSuccess(true);
	}

	@ApiOperation(value = "根据微信openId查用户")
	@RequestMapping(value = "/userInfo/wechatOpenId/{wechatOpenId}/{cloudUserId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData<String> getUserInfoByWechatOpenId(@PathVariable("wechatOpenId") String wechatOpenId,
															   @PathVariable("cloudUserId") String cloudUserId){
		User existUserByWechatOpenId = userService.getExistUserByWechatOpenId(wechatOpenId, cloudUserId);
		return BusinessResponseFactory.createSuccess(existUserByWechatOpenId.getUserId());
	}
}
