package com.zatgo.zup.merchant.remoteservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2018/7/25.
 */
@FeignClient("zup-mining")
public interface TaskRemoteService {

    @GetMapping(value = "/mining/task/done/invitationTask")
    @ResponseBody
    void invitationTaskDone(@RequestParam("count") Long count,
                                 @RequestParam("invitedUserId") String invitedUserId,
                            @RequestParam("cloudUserId") String cloudUserId,
                            @RequestParam("businessId") String businessId);

    @GetMapping(value = "/mining/task/done/registerTaskDone")
    @ResponseBody
    void registerTaskDone(@RequestParam("userId") String userId, @RequestParam("cloudUserId") String cloudUserId,
                          @RequestParam("businessId") String businessId);
}
