package com.zatgo.zup.merchant.model;

import com.zatgo.zup.common.enumtype.BusinessEnum.UserCertificateStatus;

import io.swagger.annotations.ApiModelProperty;

public class CertificateParams {

	@ApiModelProperty(value="国籍",required=true)
	private String countryCode;

	@ApiModelProperty(value="姓",required=true)
    private String lastName;

	@ApiModelProperty(value="名",required=true)
    private String firstName;

	@ApiModelProperty(value="证件类型，0：身份证，1、护照",required=true)
    private Byte certificateType;

	@ApiModelProperty(value="证件编号",required=true)
    private String certificateId;

	@ApiModelProperty(value="证件正面照url",required=false)
    private String firstPhotoUrl;

	@ApiModelProperty(value="证件反面照url",required=false)
    private String secondPhotoUrl;

	@ApiModelProperty(value="手持证件照url",required=false)
    private String holdPhotoUrl;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Byte getCertificateType() {
		return certificateType;
	}

	public void setCertificateType(Byte certificateType) {
		this.certificateType = certificateType;
	}

	public String getCertificateId() {
		return certificateId;
	}

	public void setCertificateId(String certificateId) {
		this.certificateId = certificateId;
	}

	public String getFirstPhotoUrl() {
		return firstPhotoUrl;
	}

	public void setFirstPhotoUrl(String firstPhotoUrl) {
		this.firstPhotoUrl = firstPhotoUrl;
	}

	public String getSecondPhotoUrl() {
		return secondPhotoUrl;
	}

	public void setSecondPhotoUrl(String secondPhotoUrl) {
		this.secondPhotoUrl = secondPhotoUrl;
	}

	public String getHoldPhotoUrl() {
		return holdPhotoUrl;
	}

	public void setHoldPhotoUrl(String holdPhotoUrl) {
		this.holdPhotoUrl = holdPhotoUrl;
	}
	
	
}
