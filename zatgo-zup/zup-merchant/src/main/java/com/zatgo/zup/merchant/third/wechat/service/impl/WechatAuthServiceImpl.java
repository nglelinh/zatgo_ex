package com.zatgo.zup.merchant.third.wechat.service.impl;

import com.zatgo.zup.merchant.model.LoginData;
import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfig;
import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfigParams;
import com.zatgo.zup.merchant.third.wechat.service.IWechatAuthService;
import com.zatgo.zup.merchant.third.wechat.service.IWechatJSSDKConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class WechatAuthServiceImpl implements IWechatAuthService {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	private static final String AUTH_URL = "https://api.weixin.qq.com/sns/oauth2/component/access_token?appid=#APPID#&code=#CODE#&grant_type=authorization_code&component_appid=#COMPONENT_APPID#&component_access_token=#COMPONENT_ACCESS_TOKEN#";
	
//	@Value("${wx.auth}")
//	private Byte auth;
//
//	@Value("${wx.appid}")
//	private String appid;
//
//	@Value("${wx.appsecret}")
//	private String secret;


	@Resource
	private IWechatJSSDKConfigService wechatJSSDKConfigService;



	@Override
	public LoginData wxAuth(String code, String appid) {
//		String url = AUTH_URL.replace("#APPID#", appid).replace("#COMPONENT_APPID#", this.appid).replace("#CODE#", code).replace("#COMPONENT_ACCESS_TOKEN#", secret);
//		WeChatAuth auth = okHttpService.get(url, WeChatAuth.class);
//		if(StringUtils.isNotEmpty(auth.getErrcode())) {
//			log.error("微信授权失败"+JSON.toJSONString(auth));
////			throw new BusinessException(BusinessExceptionCode.WECHAT_AUTH_ERROR);
//		}
		
		return null;
	}

	@Override
	public WechatConfig getWechatConfig(WechatConfigParams params) {
		return wechatJSSDKConfigService.getWechatJSSDKConfig(params);
	}

}
