package com.zatgo.zup.merchant.service;


import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.merchant.model.LoginData;
import com.zatgo.zup.merchant.model.LoginParams;
import com.zatgo.zup.merchant.model.UmsMemberParam;
import com.zatgo.zup.merchant.model.WechatLoginParam;

public interface AuthService {

	/**
	 * 用户登录
	 * @param loginParams
	 * @return
	 */
	public LoginData login(LoginParams loginParams, String cloudUserId);
	
	/**
	 * 用户登录
	 * @param loginParams
	 * @return
	 */
	public LoginData adminLogin(LoginParams loginParams, String cloudUserId);

	ResponseData<LoginData> weixinLogin(WechatLoginParam param);

}
