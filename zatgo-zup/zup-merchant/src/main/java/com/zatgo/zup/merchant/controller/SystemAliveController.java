package com.zatgo.zup.merchant.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sys")
public class SystemAliveController {
    @RequestMapping(value = "/alive")
    public String alive() {
        return "OK";
    }
}
