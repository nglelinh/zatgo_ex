package com.zatgo.zup.merchant.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.model.AuthUserInfo;
import com.zatgo.zup.common.redis.RedisKeyConstants;

/**
 * Created by 46041 on 2018/9/4.
 */
public class BaseController {

    @Resource
    protected HttpServletRequest request;

    @Resource
    protected HttpServletResponse response;

    @Resource
    protected HttpSession session;

    @Autowired
    protected RedisTemplate redisTemplate;

    protected AuthUserInfo getUserInfo(){
        String token = request.getHeader("token");
        return getUserInfo(token);
    }
    
    protected AuthUserInfo getAdminInfo(){
        String token = request.getHeader("token");
        return getAdminInfo(token);
    }


    protected AuthUserInfo getUserInfo(String token){
        AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
        if(authUserInfo == null) {
            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
        }
        
        if(authUserInfo.getIsAdmin()) {
        	throw new BusinessException(BusinessExceptionCode.USER_AUTH_ERROR);
        }
        
        return authUserInfo;
    }
    
    protected AuthUserInfo getAdminInfo(String token){
        AuthUserInfo authUserInfo = (AuthUserInfo)redisTemplate.opsForValue().get(RedisKeyConstants.AUTH_TOKEN_PRE+token);
        if(authUserInfo == null) {
            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
        }
        
        if(!authUserInfo.getIsAdmin()) {
        	throw new BusinessException(BusinessExceptionCode.USER_AUTH_ERROR);
        }
        
        return authUserInfo;
    }

    /**
     * 校验操作用户数据的权限
     * @param userId 被操作的用户ID
     * @return
     */
    protected void verifyUserInterface(String userId) {

        //是否内部接口调用
        String innerAccessId = request.getHeader("innerAccessId");
        if(innerAccessId != null && !innerAccessId.trim().equals("")) {
            String isInnerAccessTrue = (String)redisTemplate.opsForValue().get(innerAccessId);
            if(isInnerAccessTrue != null) {
                return;
            }
        }

        //如果不是内部接口调用，则只能操作自己的数据
        AuthUserInfo userInfo = getUserInfo();
        if(userInfo.getUserId().equals(userId)) {
            return;
        }else {
            throw new BusinessException(BusinessExceptionCode.USER_NOT_LOGIN);
        }
    }

    protected String getClientIp(){
        return request.getHeader("client-ip");
    }

    protected String getAppType() {
        return request.getHeader("appType");
    }

    protected String getVersion() {
        return request.getHeader("appVersion");
    }
    
    protected String getDeviceNo() {
        return request.getHeader("deviceNo");
    }
}
