package com.zatgo.zup.merchant.mapper;

import com.zatgo.zup.merchant.entity.CloudAdminUser;

public interface CloudAdminUserMapper extends BaseMapper<CloudAdminUser>{

}