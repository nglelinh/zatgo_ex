package com.zatgo.zup.merchant.entity;

import java.util.Date;

public class User {
    private String userId;

    private String userName;

    private String userPassword;

    private String userPayPassword;

    private Date createDate;

    private Date updateDate;

    private Date lastLoginDate;
    
    private String mnemonicWord;

    private String inviteCode;

    private String invitedCode;

    private String emailCountryCode;

    private String email;

    private String mobile;

    private String mobileCountryCode;

    private Byte registType;

    private String cloudUserId;

    private Byte isCloudManager;

    private String iconUrl;

    private String nickname;
    
    private Byte authLevel;

    private String wechatOpenId;

    public Byte getAuthLevel() {
		return authLevel;
	}

	public void setAuthLevel(Byte authLevel) {
		this.authLevel = authLevel;
	}

	public String getMnemonicWord() {
		return mnemonicWord;
	}

	public void setMnemonicWord(String mnemonicWord) {
		this.mnemonicWord = mnemonicWord;
	}

	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    public String getUserPayPassword() {
        return userPayPassword;
    }

    public void setUserPayPassword(String userPayPassword) {
        this.userPayPassword = userPayPassword == null ? null : userPayPassword.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getInvitedCode() {
        return invitedCode;
    }

    public void setInvitedCode(String invitedCode) {
        this.invitedCode = invitedCode;
    }

    public String getEmailCountryCode() {
        return emailCountryCode;
    }

    public void setEmailCountryCode(String emailCountryCode) {
        this.emailCountryCode = emailCountryCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(String mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public Byte getRegistType() {
        return registType;
    }

    public void setRegistType(Byte registType) {
        this.registType = registType;
    }

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }

    public Byte getIsCloudManager() {
        return isCloudManager;
    }

    public void setIsCloudManager(Byte isCloudManager) {
        this.isCloudManager = isCloudManager;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getWechatOpenId() {
        return wechatOpenId;
    }

    public void setWechatOpenId(String wechatOpenId) {
        this.wechatOpenId = wechatOpenId;
    }
}