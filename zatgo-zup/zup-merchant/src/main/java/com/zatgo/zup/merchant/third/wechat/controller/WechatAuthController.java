package com.zatgo.zup.merchant.third.wechat.controller;

import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfig;
import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfigParams;
import com.zatgo.zup.merchant.third.wechat.service.IWechatAuthService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 暂时不使用
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/user/wx/auth")
public class WechatAuthController{
	
	@Resource
	private IWechatAuthService wechatAuthService;

	@RequestMapping(value = "/config", method = RequestMethod.POST)
	@ResponseBody
	public WechatConfig getWechatConfig(@RequestBody WechatConfigParams params) {
		return wechatAuthService.getWechatConfig(params);
	}



}
