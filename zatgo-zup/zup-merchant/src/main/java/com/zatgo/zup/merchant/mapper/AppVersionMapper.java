package com.zatgo.zup.merchant.mapper;

import com.zatgo.zup.merchant.entity.AppVersion;
import org.apache.ibatis.annotations.Param;

public interface AppVersionMapper {
    int deleteByPrimaryKey(Long versionId);

    int insert(AppVersion record);

    int insertSelective(AppVersion record);

    AppVersion selectByPrimaryKey(Long versionId);

    int updateByPrimaryKeySelective(AppVersion record);

    int updateByPrimaryKey(AppVersion record);

	AppVersion findVersionByType(@Param("clientType") Byte clientType);
}