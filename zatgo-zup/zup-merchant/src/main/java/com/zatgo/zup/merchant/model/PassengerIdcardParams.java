package com.zatgo.zup.merchant.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("出行人证件信息")
public class PassengerIdcardParams {
	
	@ApiModelProperty("证件类型，1-身份证；2-护照；3-军官证；4-港澳通行证；5-其他证件")
	private Byte idcardType;
	
	@ApiModelProperty("证件号码")
	private String idcardCode;

	public Byte getIdcardType() {
		return idcardType;
	}

	public void setIdcardType(Byte idcardType) {
		this.idcardType = idcardType;
	}

	public String getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(String idcardCode) {
		this.idcardCode = idcardCode;
	}

}
