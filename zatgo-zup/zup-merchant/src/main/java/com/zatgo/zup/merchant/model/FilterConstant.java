package com.zatgo.zup.merchant.model;

import com.zatgo.zup.common.model.CloudSystemConstant;
import org.springframework.stereotype.Component;

/**
 * Created by 46041 on 2018/9/18.
 */

@Component
public class FilterConstant {

    public String systemCloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;

    public final String CLOUD_USER_ID_KEY = "cloudUserId";
}
