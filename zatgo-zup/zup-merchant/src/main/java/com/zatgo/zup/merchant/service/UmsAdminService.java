package com.zatgo.zup.merchant.service;

import com.zatgo.zup.merchant.entity.CloudAdminUser;

import java.util.List;

/**
 * 后台管理员Service
 * Created by chen on 2018/4/26.
 */
public interface UmsAdminService {
    /**
     * 根据用户名获取后台管理员
     */
//    UmsAdmin getAdminByUsername(String username);

    /**
     * 注册功能
     */
    CloudAdminUser register(CloudAdminUser cloudAdminUser);

    /**
     * 根据用户id获取用户
     */
    CloudAdminUser getItem(String id, String cloudUserId);

    /**
     * 根据用户名或昵称分页查询用户
     */
    List<CloudAdminUser> list(String name, Integer pageSize, Integer pageNum, String cloudUserId);

    /**
     * 修改指定用户信息
     */
    int update(String id, CloudAdminUser admin);

    /**
     * 删除指定用户
     */
    int delete(String id);

    /**
     * 修改用户角色关系
     */
//    @Transactional
//    int updateRole(Long adminId, List<Long> roleIds);
//
//    /**
//     * 获取用户对于角色
//     */
//    List<UmsRole> getRoleList(Long adminId);
//
//    /**
//     * 修改用户的+-权限
//     */
//    @Transactional
//    int updatePermission(Long adminId, List<Long> permissionIds);
//
//    /**
//     * 获取用户所有权限（包括角色权限和+-权限）
//     */
//    List<UmsPermission> getPermissionList(Long adminId);
}
