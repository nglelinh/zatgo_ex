package com.zatgo.zup.merchant.third.wechat.model;

import com.alibaba.fastjson.annotation.JSONField;

public class WechatJsapiTicketData {
	
	private int errcode;
	
	private String errmsg;
	
	private String ticket;
	
	@JSONField(name = "expires_in")
	private int expiresIn;

	public int getErrcode() {
		return errcode;
	}

	public void setErrcode(int errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

}
