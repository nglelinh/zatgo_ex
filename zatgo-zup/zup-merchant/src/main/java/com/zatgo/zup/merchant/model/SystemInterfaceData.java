package com.zatgo.zup.merchant.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "system.interface")
public class SystemInterfaceData {
	
	private List<String> credibleips;

	public List<String> getCredibleips() {
		return credibleips;
	}

	public void setCredibleips(List<String> credibleips) {
		this.credibleips = credibleips;
	}

}
