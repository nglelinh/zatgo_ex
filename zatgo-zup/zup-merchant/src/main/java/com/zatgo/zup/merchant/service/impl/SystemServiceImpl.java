package com.zatgo.zup.merchant.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zatgo.zup.common.enumtype.BusinessEnum.CarouselImageSiteType;
import com.zatgo.zup.common.exception.BusinessException;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.merchant.entity.AppVersion;
import com.zatgo.zup.merchant.entity.CarouselImage;
import com.zatgo.zup.merchant.entity.CarouselImageExample;
import com.zatgo.zup.merchant.mapper.AppVersionMapper;
import com.zatgo.zup.merchant.mapper.CarouselImageMapper;
import com.zatgo.zup.merchant.model.CarouselImageParams;
import com.zatgo.zup.merchant.service.SystemService;

@Component
public class SystemServiceImpl implements SystemService {
	@Autowired
	private AppVersionMapper appVersionMapper;
	
	@Autowired
	private CarouselImageMapper carouselImageMapper;

	@Override
	public AppVersion findVersionByType(Byte type) {
		return appVersionMapper.findVersionByType(type);
	}

	@Override
	public List<CarouselImage> getCarouselImageList(CarouselImageParams params) {
		
		CarouselImageSiteType siteType = params.getSiteType();
		if(siteType == null) {
			throw new BusinessException(BusinessExceptionCode.REQUEST_PARAM_ERROR, "site type miss");
		}
		
		CarouselImageExample example = new CarouselImageExample();
		example.createCriteria().andSiteTypeEqualTo(siteType.getType()).andCloudUserIdEqualTo(params.getCloudUserId());
		example.setOrderByClause("sort");
		
		List<CarouselImage> images = carouselImageMapper.selectByExample(example);
		
		return images;
	}
}
