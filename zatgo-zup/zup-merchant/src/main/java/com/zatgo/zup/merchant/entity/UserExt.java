package com.zatgo.zup.merchant.entity;

import java.math.BigDecimal;

public class UserExt {
    private String userId;

    private Boolean isLockLogin;

    private Boolean isLockPay;

    private Boolean isLockWithdraw;

    private Byte userGrade;

    private String wechatOpenid;

    private BigDecimal prizeRateFactor;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Boolean getIsLockLogin() {
        return isLockLogin;
    }

    public void setIsLockLogin(Boolean isLockLogin) {
        this.isLockLogin = isLockLogin;
    }

    public Boolean getIsLockPay() {
        return isLockPay;
    }

    public void setIsLockPay(Boolean isLockPay) {
        this.isLockPay = isLockPay;
    }

    public Boolean getIsLockWithdraw() {
        return isLockWithdraw;
    }

    public void setIsLockWithdraw(Boolean isLockWithdraw) {
        this.isLockWithdraw = isLockWithdraw;
    }

    public Byte getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(Byte userGrade) {
        this.userGrade = userGrade;
    }

    public String getWechatOpenid() {
        return wechatOpenid;
    }

    public void setWechatOpenid(String wechatOpenid) {
        this.wechatOpenid = wechatOpenid == null ? null : wechatOpenid.trim();
    }

    public BigDecimal getPrizeRateFactor() {
        return prizeRateFactor;
    }

    public void setPrizeRateFactor(BigDecimal prizeRateFactor) {
        this.prizeRateFactor = prizeRateFactor;
    }
}