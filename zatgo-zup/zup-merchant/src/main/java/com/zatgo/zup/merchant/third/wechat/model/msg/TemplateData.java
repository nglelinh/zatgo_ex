package com.zatgo.zup.merchant.third.wechat.model.msg;


/**
 * Created by 1 on 2018/8/29.
 */
public class TemplateData {
    /**
     * 对应模板中变量
     */
    private String value;
    /**
     * 模板内容字体颜色，不填默认为黑色
     */
    private String color;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
