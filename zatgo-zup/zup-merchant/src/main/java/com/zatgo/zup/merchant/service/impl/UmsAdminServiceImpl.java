package com.zatgo.zup.merchant.service.impl;

import com.github.pagehelper.PageHelper;
import com.zatgo.zup.common.exception.BusinessException;
import com.ykb.mall.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.encrypt.BCryptUtil;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.merchant.entity.CloudAdminUser;
import com.zatgo.zup.merchant.entity.CloudAdminUserExample;
import com.zatgo.zup.merchant.entity.UserExample;
import com.zatgo.zup.merchant.mapper.CloudAdminUserMapper;
import com.zatgo.zup.merchant.service.UmsAdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * UmsAdminService实现类
 * Created by chen on 2018/4/26.
 */
@Service
public class UmsAdminServiceImpl implements UmsAdminService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UmsAdminServiceImpl.class);

    @Autowired
    private CloudAdminUserMapper cloudAdminUserMapper;

//    @Override
//    public UmsAdmin getAdminByUsername(String username) {
//        CloudAdminUserExample example = new CloudAdminUserExample();
//        List<CloudAdminUser> cloudAdminUsers = cloudAdminUserMapper.selectByExample(example);
//        UmsAdmin umsAdmin = null;
//        if (!CollectionUtils.isEmpty(cloudAdminUsers)){
//            umsAdmin = new UmsAdmin();
//            CloudAdminUser cloudAdminUser = cloudAdminUsers.get(0);
//            String name = cloudAdminUser.getName();
//            umsAdmin.setUsername(name);
//            umsAdmin.setId();
//        }
//        return umsAdmin;
//    }

    @Override
    public CloudAdminUser register(CloudAdminUser cloudAdminUser) {
        String name = cloudAdminUser.getName();
        CloudAdminUserExample example = new CloudAdminUserExample();
        example.createCriteria().andNameEqualTo(name);
        List<CloudAdminUser> cloudAdminUsers = cloudAdminUserMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(cloudAdminUsers))
            throw new BusinessException(BusinessExceptionCode.USER_IS_EXIST);
        cloudAdminUser.setId(UUIDUtils.getUuid());
        cloudAdminUser.setCloudUserId(UUIDUtils.getUuid());
        cloudAdminUser.setPassword(BCryptUtil.encrypt("", cloudAdminUser.getPassword()));
        cloudAdminUserMapper.insertSelective(cloudAdminUser);
        return cloudAdminUser;
    }

//    /**
//     * 添加登录记录
//     * @param username 用户名
//     */
//    private void insertLoginLog(String username) {
//        UmsAdmin admin = getAdminByUsername(username);
//        UmsAdminLoginLog loginLog = new UmsAdminLoginLog();
//        loginLog.setAdminId(admin.getId());
//        loginLog.setCreateTime(new Date());
//        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        HttpServletRequest request = attributes.getRequest();
//        loginLog.setIp(request.getRemoteAddr());
//        loginLogMapper.insert(loginLog);
//    }

    @Override
    public CloudAdminUser getItem(String id, String cloudUserId) {
        UserExample example = new UserExample();
        example.createCriteria().andCloudUserIdEqualTo(cloudUserId).andUserIdEqualTo(id);
        List<CloudAdminUser> users = cloudAdminUserMapper.selectByExample(example);
        return CollectionUtils.isEmpty(users) ? null : users.get(0);
    }

    @Override
    public List<CloudAdminUser> list(String name, Integer pageSize, Integer pageNum, String cloudUserId) {
        PageHelper.startPage(pageNum, pageSize);
        CloudAdminUserExample example = new CloudAdminUserExample();
        CloudAdminUserExample.Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        criteria.andCloudUserIdEqualTo(cloudUserId);
        return cloudAdminUserMapper.selectByExample(example);
    }

    @Override
    public int update(String id, CloudAdminUser admin) {
        admin.setId(id);
        return cloudAdminUserMapper.updateByPrimaryKey(admin);
    }

    @Override
    public int delete(String id) {
        return cloudAdminUserMapper.deleteByPrimaryKey(id);
    }
//
//    @Override
//    public int updateRole(Long adminId, List<Long> roleIds) {
//        int count = roleIds == null ? 0 : roleIds.size();
//        //先删除原来的关系
//        UmsAdminRoleRelationExample adminRoleRelationExample = new UmsAdminRoleRelationExample();
//        adminRoleRelationExample.createCriteria().andAdminIdEqualTo(adminId);
//        adminRoleRelationMapper.deleteByExample(adminRoleRelationExample);
//        //建立新关系
//        if (!CollectionUtils.isEmpty(roleIds)) {
//            List<UmsAdminRoleRelation> list = new ArrayList<>();
//            for (Long roleId : roleIds) {
//                UmsAdminRoleRelation roleRelation = new UmsAdminRoleRelation();
//                roleRelation.setAdminId(adminId);
//                roleRelation.setRoleId(roleId);
//                list.add(roleRelation);
//            }
//            adminRoleRelationDao.insertList(list);
//        }
//        return count;
//    }
//
//    @Override
//    public List<UmsRole> getRoleList(Long adminId) {
//        return adminRoleRelationDao.getRoleList(adminId);
//    }
//
//    @Override
//    public int updatePermission(Long adminId, List<Long> permissionIds) {
//        //删除原所有权限关系
//        UmsAdminPermissionRelationExample relationExample = new UmsAdminPermissionRelationExample();
//        relationExample.createCriteria().andAdminIdEqualTo(adminId);
//        adminPermissionRelationMapper.deleteByExample(relationExample);
//        //获取用户所有角色权限
//        List<UmsPermission> permissionList = adminRoleRelationDao.getRolePermissionList(adminId);
//        List<Long> rolePermissionList = permissionList.stream().map(UmsPermission::getId).collect(Collectors.toList());
//        if (!CollectionUtils.isEmpty(permissionIds)) {
//            List<UmsAdminPermissionRelation> relationList = new ArrayList<>();
//            //筛选出+权限
//            List<Long> addPermissionIdList = permissionIds.stream().filter(permissionId -> !rolePermissionList.contains(permissionId)).collect(Collectors.toList());
//            //筛选出-权限
//            List<Long> subPermissionIdList = rolePermissionList.stream().filter(permissionId -> !permissionIds.contains(permissionId)).collect(Collectors.toList());
//            //插入+-权限关系
//            relationList.addAll(convert(adminId,1,addPermissionIdList));
//            relationList.addAll(convert(adminId,-1,subPermissionIdList));
//            return adminPermissionRelationDao.insertList(relationList);
//        }
//        return 0;
//    }
//
//    /**
//     * 将+-权限关系转化为对象
//     */
//    private List<UmsAdminPermissionRelation> convert(Long adminId,Integer type,List<Long> permissionIdList) {
//        List<UmsAdminPermissionRelation> relationList = permissionIdList.stream().map(permissionId -> {
//            UmsAdminPermissionRelation relation = new UmsAdminPermissionRelation();
//            relation.setAdminId(adminId);
//            relation.setType(type);
//            relation.setPermissionId(permissionId);
//            return relation;
//        }).collect(Collectors.toList());
//        return relationList;
//    }
//
//    @Override
//    public List<UmsPermission> getPermissionList(Long adminId) {
//        return adminRoleRelationDao.getPermissionList(adminId);
//    }
}
