package com.zatgo.zup.merchant.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class LoginData {

	@ApiModelProperty(value="登录token",required=true)
	private String token;
	
	@ApiModelProperty(value="登录用户名",required=true)
	private String userName;
	
	@ApiModelProperty(value="登录用户ID",required=true)
	private String userId;

	@ApiModelProperty(value="用户昵称",required=true)
	private String nickname;

	@ApiModelProperty(value="用户头像",required=true)
	private String icon;

	@ApiModelProperty(value="权限",required=false)
	private List<String> roles;

	
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
}
