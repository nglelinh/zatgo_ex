package com.zatgo.zup.merchant.model;

/**
 * Created by 46041 on 2019/6/12.
 */
public class WechatLoginParam {

    private String cloudUserId;

    private String visitType;

    private String wxCode;

    public String getCloudUserId() {
        return cloudUserId;
    }

    public void setCloudUserId(String cloudUserId) {
        this.cloudUserId = cloudUserId;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getWxCode() {
        return wxCode;
    }

    public void setWxCode(String wxCode) {
        this.wxCode = wxCode;
    }
}
