package com.zatgo.zup.merchant.remoteservice;

import com.ykb.mall.common.model.WxAccessTokenData;
import com.zatgo.zup.common.model.ResponseData;
import com.zatgo.zup.common.model.wx.WxUserInfoData;
import com.zatgo.zup.merchant.model.CommonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by 46041 on 2019/6/
 */

@FeignClient("zup-wechat")
public interface WechatRemoteService {

    @GetMapping(value = "/wechat/getWechatOpenId")
    @ResponseBody
    ResponseData<WxAccessTokenData> getWechatOpenId(@RequestParam("wxCode") String wxCode);

    @GetMapping(value = "/wechat/getWxUserInfo")
    @ApiOperation("获取微信用户信息")
    ResponseData<WxUserInfoData> getWxUserInfo(@RequestParam("openId") String openId, @RequestParam("accessToken") String accessToken);

    @GetMapping(value = "/wechat/register/getWxUserInfo")
    @ApiOperation("注册获取微信用户信息")
    ResponseData<WxUserInfoData> getRegisterWxUserInfo(@RequestParam("openId") String openId, @RequestParam("accessToken") String accessToken);
}
