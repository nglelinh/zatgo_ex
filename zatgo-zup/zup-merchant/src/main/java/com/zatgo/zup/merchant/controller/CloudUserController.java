package com.zatgo.zup.merchant.controller;

import com.github.pagehelper.PageInfo;
import com.zatgo.zup.common.enumtype.BusinessEnum;
import com.zatgo.zup.common.exception.BusinessExceptionCode;
import com.zatgo.zup.common.exception.BusinessResponseFactory;
import com.zatgo.zup.common.model.*;
import com.zatgo.zup.common.utils.UUIDUtils;
import com.zatgo.zup.merchant.entity.CloudAdminUser;
import com.zatgo.zup.merchant.entity.CloudUser;
import com.zatgo.zup.merchant.service.CloudUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 46041 on 2018/9/11.
 */


@RestController
@Api(value="/user/cloud", produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/user/cloud")
public class CloudUserController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(CloudUserController.class);


    @Autowired
    private CloudUserService cloudUserService;


    @ApiOperation(value = "创建云用户API")
    @PostMapping(value = "/userApi/create")
    @ResponseBody
    public ResponseData createUserApi(@RequestBody CloudUserApi cloudUserApi){
        cloudUserService.createUserApi(cloudUserApi);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "查询云用户API列表")
    @PostMapping(value = "/userApi/list")
    @ResponseBody
    public ResponseData<PageInfo<CloudUserApi>> selectUserApiList(@RequestBody CloudUserApiParams cloudUserApi){
        return BusinessResponseFactory.createSuccess(cloudUserService.selectUserApiList(cloudUserApi));
    }

    @ApiOperation(value = "查询单个云用户API")
    @PostMapping(value = "/userApi/detail")
    @ResponseBody
    public ResponseData<CloudUserApi> selectUserApiDetail(@RequestParam(value = "cloudUserId", required = false) String cloudUserId){
        return BusinessResponseFactory.createSuccess(cloudUserService.selectUserApiDetail(cloudUserId));
    }

    @ApiOperation(value = "修改云用户API")
    @PostMapping(value = "/userApi/edit")
    @ResponseBody
    public ResponseData updateUserApi(@RequestBody CloudUserApi cloudUserApi){
        cloudUserService.updateUserApi(cloudUserApi);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "根据用户ID获取云用户的管理员用户ID(内部调用)")
    @PostMapping(value = "/get/manageUserId/{userId}/{cloudManageType}")
    @ResponseBody
    public ResponseData<UserData> getManageUserId(@PathVariable("userId") String userId,@PathVariable("cloudManageType") String cloudManageType){
        UserData userData = null;
        try {
        	BusinessEnum.CloudManageUserType typeEnum = BusinessEnum.CloudManageUserType.getEnumByType(cloudManageType);
            userData = cloudUserService.getManageUserId(userId,typeEnum);
        } catch (Exception e){
            logger.error("", e);
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
        return BusinessResponseFactory.createSuccess(userData);
    }
    
    
    @ApiOperation(value = "根据用户ID获取云用户的管理员用户ID(内部调用)")
    @PostMapping(value = "/get/manageUserByCloudUserId/{cloudUserId}/{cloudManageType}")
    @ResponseBody
    public ResponseData<UserData> getManageUserByCloudUserId(@PathVariable("cloudUserId") String cloudUserId,@PathVariable("cloudManageType") String cloudManageType){
        UserData userData = null;
        try {
        	BusinessEnum.CloudManageUserType typeEnum = BusinessEnum.CloudManageUserType.getEnumByType(cloudManageType);
            userData = cloudUserService.getManageUserByCloudUserId(cloudUserId,typeEnum);
        } catch (Exception e){
            logger.error("", e);
            return BusinessResponseFactory.createBusinessError(BusinessExceptionCode.SYSTEM_ERROR);
        }
        return BusinessResponseFactory.createSuccess(userData);
    }

    @ApiOperation(value = "添加云用户")
    @PostMapping(value = "/account/create")
    @ResponseBody
    public ResponseData createCloudUser(@RequestParam("cloudUserName") String cloudUserName){
        cloudUserService.createCloudUser(cloudUserName);
        return BusinessResponseFactory.createSuccess(null);
    }

    @ApiOperation(value = "查询云用户列表")
    @PostMapping(value = "/account/list")
    @ResponseBody
    public ResponseData selectCloudUserList(@RequestParam("cloudUserName") String cloudUserName,
                                            @RequestParam("pageNo") Integer pageNo,
                                            @RequestParam("pageSize") Integer pageSize){
        PageInfo<CloudUser> info = cloudUserService.selectCloudUserList(cloudUserName, pageNo, pageSize);
        return BusinessResponseFactory.createSuccess(info);
    }
    
    @ApiOperation(value = "查询云用户信息")
    @GetMapping(value = "/account/detail/{cloudUserId}")
    @ResponseBody
    public ResponseData<CloudUser> selectCloudUserDetail(@PathVariable(value = "cloudUserId", required = false) String cloudUserId){
        if (StringUtils.isEmpty(cloudUserId)) {
            cloudUserId = CloudSystemConstant.DEFAULT_SYSTEM_CLOUD_USER_ID;
        }
    	CloudUser user = cloudUserService.selectCloudUserByCloudUserId(cloudUserId);
    	return BusinessResponseFactory.createSuccess(user);
    }
    
    @ApiOperation(value = "查询云管理员信息")
    @GetMapping(value = "/admin/byName/{cloudUserId}/{cloudAdminName}")
    @ResponseBody
    public ResponseData<CloudAdminUserData> selectCloudAdminByName(@PathVariable("cloudUserId") String cloudUserId,@PathVariable("cloudAdminName") String cloudAdminName){
    	CloudAdminUser user = cloudUserService.selectCloudAdminByName(cloudAdminName,cloudUserId);
    	CloudAdminUserData userData = null;
    	if(user != null) {
    		userData = new CloudAdminUserData();
    		BeanUtils.copyProperties(user, userData);
    	}
    	return BusinessResponseFactory.createSuccess(userData);
    }

}
