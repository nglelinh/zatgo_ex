package com.zatgo.zup.merchant.model;

import com.zatgo.zup.common.model.AliyunValidateCode;
import io.swagger.annotations.ApiModelProperty;

public class LoginParams {

	@ApiModelProperty(value="登录手机号或邮箱地址",required=true)
	private String loginName;

	@ApiModelProperty(value="登录密码",required=true)
	private String loginPassword;

	@ApiModelProperty(value="阿里云返回数据",required=true)
	private AliyunValidateCode aliyunValidateCode;

	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginPassword() {
		return loginPassword;
	}
	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public AliyunValidateCode getAliyunValidateCode() {
		return aliyunValidateCode;
	}

	public void setAliyunValidateCode(AliyunValidateCode aliyunValidateCode) {
		this.aliyunValidateCode = aliyunValidateCode;
	}
}
