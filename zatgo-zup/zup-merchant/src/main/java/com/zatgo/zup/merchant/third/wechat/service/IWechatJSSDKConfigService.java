package com.zatgo.zup.merchant.third.wechat.service;


import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfig;
import com.zatgo.zup.merchant.third.wechat.model.auth.WechatConfigParams;

public interface IWechatJSSDKConfigService {
	
	WechatConfig getWechatJSSDKConfig(WechatConfigParams params);

}
