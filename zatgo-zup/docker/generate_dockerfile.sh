#!/bin/bash
echo "generate Dockerfile"

PROFILE=$1

array_profile=("dev", "prod")
if [[ ! " ${array_profile[@]} " =~ "$PROFILE" ]]; then
    # whatever you want to do when arr doesn't contain value
    echo "PROFILE is allowed only [${array_profile[@]}]\n\
    dev = It does not setting APMs\n\
    prod = It does setting APMs"
    exit
fi

array=("zup-suanli" "zup-blockexplorer" "zup-config" "zup-exchange-appapi" "zup-manage" "zup-matching" "zup-merchant" "zup-mining" "zup-payment-web" "zup-register" "zup-sign" "zup-task" "zup-transaction" "zup-wallet" "zup-gateway" "zup-games" "zup-coupons" "zup-activity" "zup-trade-admin" "zup-trade-api" "zup-wechat")

for var in ${array[@]}
do
	echo "generate $var"
	sed "s/\$server_name/$var/g;s/\$profile/prod/g" Dockerfile >  "Dockerfile_$var"_bk

	line=$(grep -n 'Insert setting APM if environment is production.' "Dockerfile_$var"_bk | cut -d: -f1)

	if [ "$PROFILE" == "prod" ]; then
	    sed '/Insert setting APM if environment is production./ r Dockerfile_APM' "Dockerfile_$var"_bk > "Dockerfile_$var"
	else
	    cp  "Dockerfile_$var"_bk "Dockerfile_$var"
	fi
	rm "Dockerfile_$var"_bk
done

echo "
------------------------------------------------------------------------------

docker build . -t zup-suanli            -f ./docker/Dockerfile_zup-suanli
docker build . -t zup-blockexplorer     -f ./docker/Dockerfile_zup-blockexplorer
docker build . -t zup-config            -f ./docker/Dockerfile_zup-config
docker build . -t zup-coupons           -f ./docker/Dockerfile_zup-coupons
docker build . -t zup-exchange-appapi   -f ./docker/Dockerfile_zup-exchange-appapi
docker build . -t zup-manage            -f ./docker/Dockerfile_zup-manage
docker build . -t zup-matching          -f ./docker/Dockerfile_zup-matching
docker build . -t zup-merchantchant     -f ./docker/Dockerfile_zup-merchant
docker build . -t zup-mining            -f ./docker/Dockerfile_zup-mining
docker build . -t zup-payment-web       -f ./docker/Dockerfile_zup-payment-web
docker build . -t zup-register          -f ./docker/Dockerfile_zup-register
docker build . -t zup-sign              -f ./docker/Dockerfile_zup-sign
docker build . -t zup-task              -f ./docker/Dockerfile_zup-task
docker build . -t zup-transaction       -f ./docker/Dockerfile_zup-transaction
docker build . -t zup-wallet            -f ./docker/Dockerfile_zup-wallet
docker build . -t zup-gateway           -f ./docker/Dockerfile_zup-gateway
docker build . -t zup-games             -f ./docker/Dockerfile_zup-games
docker build . -t zup-activity             -f ./docker/Dockerfile_zup-activity

-------------------------------------------------------------------------------
"
