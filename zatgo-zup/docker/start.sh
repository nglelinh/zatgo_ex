#!/bin/bash
server_name=`cat /tmp/server_name.txt`
java -jar -Djava.security.egd=file:/dev/.urandom -Xmx512M -Xms512M -Xmn192M -XX:MaxMetaspaceSize=128M -XX:MetaspaceSize=128M -XX:+UseSerialGC -Duser.timezone=GMT+08 /data/webapps/${server_name}/${server_name}.jar --eureka.client.serviceUrl.defaultZone=http://zup-register.zatgo-test.svc.flybycs.com/eureka/ --spring.cloud.config.server.native.searchLocations=./config  --spring.resources.static-locations=file:./web,classpath:/META-INF/resources/  --eureka.instance.prefer-ip-address=true
