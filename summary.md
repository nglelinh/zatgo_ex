# Summary

- The architecture is built under a *Micro service* pattern
    - Core modules such as, transaction, mining, payment..., for most logical workflows
    - Non-core modules such as, games, events..., supplements the ad hoc / holiday-event based services
    - The components are communicating mainly through HTTP
- It integrates with 4 blockchain inmplementations
    - Blockchain layer: BTC, ETH, USDT, QTUM
    - Simple operations, e.g., create transaction and retrieving balances, are wired to the customer-facing parts of the system
- Plenty marketing/holiday event-based/gaming features are also designed as micro services
- Deployments scripts are provided

The core feature of the system should be creating online transactions with crypto-currency. There's no aviation or booking system related features observed in the codebase whatsoever.

## Technology stack

- Deployment: 
    - Jenkins CI (pipeline)
    - Docker (container)
    - Kubernetes (container orchestration)
- Backend: 
    - Java (programming language)
    - Maven (build tool)
    - Spring Boot (web framework)
    - Spring Cloud (web framework)
    - Redis (database)
    - Mongo (database)
    - MySQL (database)
    - RocketMQ (message queue)
    - HTTP (transfer protocol)
- Frontend:
    - Javascript (programming language)
    - React Native (web frontend, mobile frontend)

## Project Structure
```
zup
|-- zatgo-zup			// Backend service with multiple components. SaaS section in the architecture diagram.
|   |-- zup-activity
|   |-- zup-blockexplorer
|   |-- zup-common
|   |-- zup-coupons
|   |-- zup-crawler
|   |-- zup-exchange-appapi
|   |-- zup-mining
|   |-- zup-payment-web
|   |-- zup-sign
|   |-- zup-trade-admin
|   |-- zup-trade-api
|   |-- zup-transaction
|   |-- zup-wallet
|   |-- zup-wechat
|   |-- ...
|
|-- zatgo-zup-app		// Mobile app. Mobile section in the architecture diagram.
|-- zatgo-zup-exchange-app	// Web app. Looks like it displays the exchange rates only, and it's not found on zatgo's official site. 
`-- zatgo-zup-web		// Not clear. Looks like static files for wechat 小程序.
```

The mobile app and backend service should be the core of their business.

## Core Backend Modules
This section highlights the modules that I think are fundamental to the transactions. Section 3.2.1 in the design specification attached 
has a good overview of the system architecture, and most of the codes are written under *zatgo-zup* are for the SaaS end in the diagram.

### zup-gateway
A central portal for frontends to access different components, working similarly to a reverse proxy. In particular, it uses 'spring-cloud-gateway'.
###### Functionality
- routing downstream HTTP requests to destination micro services
- user authentication through `zup-merchant` component 

### zup-merchant
User management.
###### Functionality
- login credential management
- user info manaagement

### zup-payment-web
Provides payment endpoints when customers makes an order.

###### Functionality
- integrates with major payment systems
    - including AliPay, WeChat Pay (using a 3rd party library, WxPay), AiCoin

### zup-transaction
Provides blockchain transaction execution and monitoring.

###### Functionality
- connects to existing blockchain implementations
    - including BTC, ETH, USDT and QTUM
    - worth reviewing, especially it's in java

### zup-wallet
Provides an off-chain token balance store.

###### Functionality
- query on wallet balance
- updates wallet balance upon ledger updates (as described in a sequence diagram in the design specification)
    - in fact, other components would trigger the updates here

### zup-exchange-appapi
Provides connection to crypto currency exchanges for trading ETH for BTC.

It connects to 
- Bytex("bytex")
- Huobi("huobi")
- Huobi_Hadax("huobi_hadax")
- Bitshop("bitshop")
- Zatgo("zatgo")
- Gsboms("gsboms")

I believe this is the entry point to the sequence diagram in the design specification.

###### Terminologies
- Order: a currency exchange order made to the exchanges

###### Functionality
- place orders to crypto exchanges 
- display tick data from crypto exchanges

### zup-trade-(admin/api)
Provides flight / hotel booking functionalities.

Admin has to right to create products
User (api) has to right to create orders.

###### Terminologies
- Cms: 
- Pms: Product management system.
- Oms: Order mananagement system.
- Sms: 
- Order: A order placed to book a flight or hotel.
- Product: A slot for flight or hotel.

### Other modules
- `zup-blockexplorer`: Looks like it's for debug purpose, containing duplicating functionalities to `zup-transcation`.
- `zup-mining`: Not actual BTC mining, but a feature to increase the time customer spend on the app.
- `zup-games`: In-app games and rewarding system.
- `zup-activity`: Controlling in-app marketing/sales campaigns.
- `zup-coupons`: Managing discounts on in-app purchases.

## !Not implemented!
- No proper unit/integration tests in place, with most modules having close to none.
- No access control in place. 
    - It's hard to distinguish APIs used by users and between components.
- No fault tolerance on inter-component communication, whilst most are through HTTP.

# Code reuse

Code reuse can be high, but there would be a learning curve to adopt the pattern of micro services (or spring cloud to be precise).

# Code quality

## Readability
- Backend codes are quite concise and consistent
- Swagger is provided for HTTP endpoint documentation
- In-code comments in Chinese explaining complex logics, e.g., functions with > 30 lines

## Maintainability 
### Pro
- Micro serivce pattern makes it easy to put the componenents back together if needed
- The codes are pretty well structured
### Con
- Lack of documentation on inter-component relationships.
- Hosting multiple micro services is not common in our company.
    - Normally, we strictly enforce HTTPS, so *cert management* can be complex for inter-component communication
- Intensive use of String, int, ... primitive types when defining API parameters and DB schemas.
    - Some sematics are stripped of when the type infromation is down casted to primitive.
    - Type casting (Serializing / deserializing) can be everywhere.
    - In our company, we write serializers/deserializers to centralize such conversions.
- Business logic is scattered everywhere in the code.
    - I would expect much knowledge transfer from the zatgo team before we actually take over the project.

# Final Notes
Several items for consideration
- kotlin vs. java: not worried about the conversion to kotlin, there are tools for this purpose
- micro service architecture: can be a poin to get everything running
- unit testing: has to be further developed