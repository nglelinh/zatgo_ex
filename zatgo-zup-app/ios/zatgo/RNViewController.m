//
//  RNViewController.m
//  zatgo
//
//  Created by 周建新 on 2018/6/25.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "RNViewController.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import "AppDelegate.h"
#import "WKWebViewController.h"

@interface RNViewController ()

@end

@implementation RNViewController

-(void)viewDidAppear:(BOOL)animated{
  if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;    //让rootView禁止滑动
  }
}

-(void)viewWillAppear:(BOOL)animated{
  self.navigationController.navigationBarHidden = YES;
  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  appDelegate.allowRotation = NO;

}

-(void)viewWillDisappear:(BOOL)animated{
  
  self.navigationController.navigationBarHidden = NO;

}

- (void)viewDidLoad {
    [super viewDidLoad];
  
  NSURL *jsCodeLocation;
  
#ifdef DEBUG
//#ifdef DEBUG
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
//  jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];

//#else
//  jsCodeLocation = [CodePush bundleURL];
//#endif
#else
  
  jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
  
  
  RCTRootView* rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"zatgo"
                                               initialProperties:self.props
                                                   launchOptions:nil];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
  [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushView:) name:@"pushView" object:nil];
  self.view = rootView;
}
-(void)pushView:(NSNotification *)noti{
  NSLog(@"%@",noti.object);
  WKWebViewController* webview = [[WKWebViewController alloc]init];
  webview.data = noti.object;
  webview.serverUrl = self.props;
  [self.navigationController pushViewController:webview animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
