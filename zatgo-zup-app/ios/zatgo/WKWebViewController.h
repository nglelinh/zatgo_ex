//
//  ViewController.h
//  WKWebViewDemo
//
//  Created by hujunhua on 2016/11/17.
//  Copyright © 2016年 hujunhua. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WKWebViewController : UIViewController
@property(nonatomic,strong)NSDictionary* data;
@property(nonatomic,strong)NSDictionary * serverUrl;

@end

