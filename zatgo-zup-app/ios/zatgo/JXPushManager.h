//
//  JXPushManager.h
//  zatgo
//
//  Created by 周建新 on 2018/8/7.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
@interface JXPushManager : RCTEventEmitter <RCTBridgeModule>

+(void)notificationEvent:(NSString *)emitName obj:(NSString*)obj;
@end

