//
//  RNViewController.h
//  zatgo
//
//  Created by 周建新 on 2018/6/25.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RNViewController : UIViewController<UIGestureRecognizerDelegate>

@property(nonatomic,strong)NSDictionary * props;

@end
