//
//  JXLoadingView.m
//  zatgo
//
//  Created by 周建新 on 2019/3/4.
//  Copyright © 2019年 Facebook. All rights reserved.
//

#import "JXLoadingView.h"


@interface  JXLoadingView ()

@property (nonatomic,weak) UIImageView *imageView;
@property (nonatomic,strong) NSMutableArray *imageArray;
@property (nonatomic,strong) UILabel *label;

@end


@implementation JXLoadingView
  
  - (instancetype)init {
    self = [super init];
    if (self) {
      self.backgroundColor = [UIColor colorWithRed:0.94f green:0.94f  blue:0.94f  alpha:1.00f];
    }
    
    return  self;
  }
  
  - (void)showInView:(UIView *)view {
    if (view == nil) {
      view = [UIApplication sharedApplication].keyWindow;
    }
    
    [view addSubview:self];
    self.frame = view.bounds;
    self.imageView.frame  = CGRectMake(0, 0, 70, 100);
    self.imageView.center = self.center;
    
    [self.imageView startAnimating];
  }
  

- (void)showErrorInView:(UIView *)view{
  [_imageArray removeAllObjects];
  [_imageView stopAnimating];
  [_imageView removeFromSuperview];
  [self removeFromSuperview];
  
  if (view == nil) {
    view = [UIApplication sharedApplication].keyWindow;
  }
  
  [view addSubview:self];
  self.frame = view.bounds;
  self.label.center = self.center;
}


  - (void)dismiss {
    [_imageArray removeAllObjects];
    [_imageView stopAnimating];
    [_imageView removeFromSuperview];
    [self removeFromSuperview];
  }
  
  - (NSMutableArray *)imageArray {
    
    if (!_imageArray) {
      _imageArray = [NSMutableArray array];
    }
    return  _imageArray;
  }
  
  - (UIImageView *)imageView {
    if (!_imageView) {
      
      UIImageView *img = [[UIImageView alloc]init];
      [self addSubview:img];
      _imageView = img;
      for (NSInteger i = 1; i < 26; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"loading%ld.png", i]];
        [self.imageArray addObject:image];
      }
      self.imageView.animationDuration = 1.0;
      self.imageView.animationRepeatCount = 0;
      self.imageView.animationImages = self.imageArray;
    }
    return _imageView;
    
  }

-(UILabel*)label{
  if(!_label){
    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 280, 80)];
    [self addSubview:_label];
  }
  _label.text = @"页面加载失败，请退出重试";//默认为空
  _label.font = [UIFont systemFontOfSize:17];//默认使用系统的17
  _label.textColor = [UIColor blackColor];//默认使用文本黑色
  _label.textAlignment = NSTextAlignmentCenter;
  return _label;
}

  @end
