//
//  JXLoadingView.h
//  zatgo
//
//  Created by 周建新 on 2019/3/4.
//  Copyright © 2019年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JXLoadingView : UIView

//展示动画
- (void)showInView:(UIView *)view;

//展示动画
- (void)showErrorInView:(UIView *)view;

//取消动画
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
