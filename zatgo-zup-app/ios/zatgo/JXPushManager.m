//
//  JXPushManager.m
//  zatgo
//
//  Created by 周建新 on 2018/8/7.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "JXPushManager.h"

@implementation JXPushManager

RCT_EXPORT_MODULE()

#pragma mark-重写父类方法
- (NSArray<NSString *> *)supportedEvents {
  return @[@"WebViewCloseEvent"];
}

-(void)startObserving{
  for(NSString* emit in self.supportedEvents){
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(postNotificationEvent:) name:emit object:nil];
  }
}

-(void)stopObserving{
  [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark-自定义方法

-(void)postNotificationEvent:(NSNotification *)notification
{
  NSString* emitName = notification.userInfo[@"name"];
  [self sendEventWithName:emitName body:emitName];
}

+(void)notificationEvent:(NSString *)emitName obj:(NSString *)obj{
  [[NSNotificationCenter defaultCenter]postNotificationName:emitName object:self userInfo: @{@"name":emitName}];
}

@end
