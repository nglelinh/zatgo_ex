/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"
#import "AFNetworking.h"
#import <PgySDK/PgyManager.h>
#import <PgyUpdate/PgyUpdateManager.h>
#import <CodePush/CodePush.h>
#import <React/RCTBundleURLProvider.h>
#import "SplashScreen.h"
#import <React/RCTLinkingManager.h>
#import "RNViewController.h"
#import <UMSocialCore/UMSocialCore.h>


@implementation NSURLRequest (AllowAnyHTTPSCertificate)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
  return YES;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

  [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
  NSURL *url = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
  //app 通过urlscheme启动
  NSString *str1 = @"";
  if(url){
    str1 = [url absoluteString];
  }
  
  NSURL *jsCodeLocation;
  #ifdef DEBUG
    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  #else
  NSDictionary *props;
#ifdef TEST
  props = @{@"serverUrl":@"http://work.flybycloud.com:6674",
            @"payUrl":@"http://work.flybycloud.com:6674",
            @"feibaH5":@"http://work.flybycloud.com:1013",
            @"exchangeUrl":@"http://192.168.1.37:7085/app.html",
            @"sliderH5":@"http://192.168.1.37:6674/secure-slider/NCH5.html",
            @"devEnv":@YES,
            @"tradeParams":str1,
            @"downloadUrl":@"https://work.flybycloud.com:6673",
            };
//  [[PgyUpdateManager sharedPgyManager] startManagerWithAppId:@"a1e050a1a70200101bcdad45fc8c776d"];   //37测试

  //https://work.flybycloud.com:1022",

//  props = @{@"serverUrl": @"https://work.flybycloud.com:1022",
//            @"payUrl":@"https://work.flybycloud.com:1022",
//            @"feibaH5":@"http://work.flybycloud.com:1018",
//            @"exchangeUrl":@"https://work.flybycloud.com:1022/exchange-web/app.html",
//            @"sliderH5":@"https://work.flybycloud.com:1022/secure-slider/NCH5.html",
//            @"devEnv":@NO,
//            @"tradeParams":str1,
//            @"downloadUrl":@"https://work.flybycloud.com:1022",
//            };
//
//
//      [[PgyUpdateManager sharedPgyManager] startManagerWithAppId:@"7b24ba5075d3a8958c41a65c4f8e7e4f"];   // release1.0.25

#endif

#ifdef RELEASE

  props = @{@"serverUrl":@"https://work.flybycloud.com:1022",
            @"payUrl":@"https://work.flybycloud.com:1022",
            @"feibaH5":@"https://work.flybycloud.com:1018",
            @"exchangeUrl":@"https://work.flybycloud.com:1022/exchange-web/app.html",
            @"sliderH5":@"https://work.flybycloud.com:1022/secure-slider/NCH5.html",
            @"devEnv":@NO,
            @"tradeParams":str1,
            @"downloadUrl":@"http://work.flybycloud.com:1022",
            };
   jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
  #endif
#endif
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"zatgo"
                                               initialProperties:nil
                                               initialProperties:props
                                                   launchOptions:launchOptions];
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  
  RNViewController *rootViewController = [RNViewController new];
  rootViewController.props = props;
  _nav = [[UINavigationController alloc]initWithRootViewController:rootViewController];
  self.window.rootViewController = _nav;
  
  [self.window makeKeyAndVisible];
  [[PgyUpdateManager sharedPgyManager] checkUpdateWithDelegete:self selector:@selector(updateMethod:)];

  [SplashScreen show];
  UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
  center.delegate = self;
  [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;

  
  /* 打开调试日志 */
  [[UMSocialManager defaultManager] openLog:YES];
  
  /* 设置友盟appkey */
  [[UMSocialManager defaultManager] setUmSocialAppkey:@"5bea93dff1f55686dc00005e"];
  
  /*
   * 关闭强制验证https，可允许http图片分享，但需要在info.plist设置安全域名
   */
  [UMSocialGlobal shareInstance].isUsingHttpsWhenShareContent = NO;
  
  /*
   设置微信的appKey和appSecret
   [微信平台从U-Share 4/5升级说明]http://dev.umeng.com/social/ios/%E8%BF%9B%E9%98%B6%E6%96%87%E6%A1%A3#1_1
   */
  [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wx5196d3e4518da194" appSecret:@"e14c75bd3a3ce0dc320512facbed53b2" redirectURL:nil];
  
  return YES;
}

//监听系统横屏竖屏
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
  if (self.allowRotation) {
    return UIInterfaceOrientationMaskAll;
  }
  return UIInterfaceOrientationMaskPortrait;
  
}

// 在AppDelete实现该方法
- (void)applicationWillEnterForeground:(UIApplication *)application {
  // app从后台进入前台都会调用这个方法
 [[PgyUpdateManager sharedPgyManager] checkUpdateWithDelegete:self selector:@selector(updateMethod:)];
}

// Required to register for notifications
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
  [RCTPushNotificationManager didRegisterUserNotificationSettings:notificationSettings];
}
// Required for the register event.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  [RCTPushNotificationManager didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}
// Required for the notification event. You must call the completion handler after handling the remote notification.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
  [RCTPushNotificationManager didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}
// Required for the registrationError event.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
  [RCTPushNotificationManager didFailToRegisterForRemoteNotificationsWithError:error];
}
// Required for the localNotification event.
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
  [RCTPushNotificationManager didReceiveLocalNotification:notification];
}

//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
  completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//9.0前的方法，为了适配低版本 保留
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
  return [RCTLinkingManager application:application openURL:url options:nil];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
  return [RCTLinkingManager application:application openURL:url options:nil];
}

//9.0后的方法
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
  
  return [RCTLinkingManager application:app openURL:url options:options];

}
#pragma mark ------提示用户版本更新------蒲公英判断

-(void)updateMethod:(NSDictionary*)response{
  if(response!=nil){
  __weak __typeof(self) weakself= self;
  dispatch_async(dispatch_queue_create(0, 0), ^{
    // 子线程执行任务（比如获取较大数据）
       UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"发现新版本，请立即更新" message:response[@"releaseNote"] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:response[@"appUrl"]]];
    }]];
    [weakself.window.rootViewController presentViewController:alert animated:YES completion:nil];
  });
  }else{
      [self VersonUpdate];
  }
}

#pragma mark ------提示用户版本更新------

-(void)VersonUpdate{
  NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
  NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil];
  [manager.requestSerializer setValue:app_Version forHTTPHeaderField:@"appVersion"];
  [manager.requestSerializer setValue:@"0" forHTTPHeaderField:@"appType"];
    __weak __typeof(self) weakself= self;
  // 2.设置非校验证书模式
  manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.securityPolicy setValidatesDomainName:NO];
  
  NSString* urlString = [NSString stringWithFormat:@"%@/user/system/app/version",self.urlStr[@"serverUrl"]];
  [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    NSString* update = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"] boolValue] ? @"YES":@"NO"];
    if ([update isEqualToString:@"YES"]) {
      dispatch_async(dispatch_queue_create(0, 0), ^{
               UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"发现新版本，请立即更新" message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
              
              NSString* downloadUrl = [NSString stringWithFormat:@"%@/download/release/index.html",self.urlStr[@"downloadUrl"]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downloadUrl]];
              
            }]];
      
            [weakself.window.rootViewController presentViewController:alert animated:YES completion:nil];
      });
    }
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    
  }];
}

@end
