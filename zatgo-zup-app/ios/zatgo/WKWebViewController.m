//
//  ViewController.m
//  WKWebViewDemo
//
//  Created by hujunhua on 2016/11/17.
//  Copyright © 2016年 hujunhua. All rights reserved.
//

#import "WKWebViewController.h"
#import <WebKit/WebKit.h>
#import "AFNetworking.h"
#import "JXPushManager.h"
#import "NativeManager.h"
#import "DeviceUID.h"
#import "AppDelegate.h"
#import "JXLoadingView.h"

#define DeviceUID [DeviceUID uid]
#define ScreenWidth   [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight  [[UIScreen mainScreen] bounds].size.height
#define SafeAreaTopHeight (ScreenHeight == 812.0 ?88 : 64)
#define SafeAreaBottomHeight (ScreenHeight == 812.0 ?20 : 0)
#define IsiPoneX (ScreenHeight == 812.0 ?44 :20 )
#define ProgressHeight (ScreenHeight == 812.0 ?44 :1 )

#ifdef DEBUG
#   define feibaH5 @"work.flybycloud.com:1018"
#else
#   define feibaH5 @"wx.flybytrip.com"
#endif

static CGFloat addViewHeight = 500;   // 添加自定义 View 的高度


@interface WKWebViewController ()
<
WKNavigationDelegate,
WKUIDelegate,
UIScrollViewDelegate,
WKScriptMessageHandler,
UINavigationControllerDelegate
>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, assign) CGFloat delayTime;
@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, assign) CGFloat progressH;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, strong) NSString*  currentURL;
@property (nonatomic, strong) NSTimer*  timer;
@property (nonatomic,strong) UIBarButtonItem *leftBarButton;
@property (nonatomic,strong) UIBarButtonItem *leftBarButtonSecond;
@property (nonatomic,strong)JXLoadingView *loadingView;

//@property (nonatomic, strong) NSURLConnection *httpsUrlConnection;
//@property (nonatomic, assign) BOOL httpsAuth;
//@property (nonatomic, strong) NSURLRequest *originRequest;

@end

@implementation WKWebViewController

#pragma mark - Life Cycle

// 用来测试的一些url链接
- (NSString *)testurl {
  NSString * url;
  if (self.data[@"prepayId"]&&![self.data[@"prepayId"] isEqual:@""]) {
    url =[NSString stringWithFormat:@"%@/checkout/h5/login.html?token=%@&prepayId=%@",self.data[@"payUrl"],self.data[@"zatgoToken"],self.data[@"prepayId"]];
  }else{
    url = [NSString stringWithFormat: @"%@",self.data[@"url"]];
  }
  
  //  url = @"http://work.flybycloud.com:1013?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0bWNJZCI6MTExMTEyMjkxLCJjb3JwSWQiOjExMTExNjYxMywidXNlcklkIjoyMTExMjAyNjQsInRzIjoxNTMwMDA5NTQ4MTI2fQ.jJxd6exR4WZwhb42fYw3G86cQx4lKzj-TQDGD14LnIU&zatgoToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6IjE4NzYxNjAwOTI4IiwidHMiOjE1MzAwMDg3ODc2OTd9.PamCBdwurYYekjK1Zuk8w7Q01ZmfL5MsXZZoGyT0bo0";
  
  return url;
}


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  appDelegate.allowRotation = YES;
  if(self.data[@"timer"]){
    NSInteger time = [self.data[@"timer"] integerValue];
    self.timer = [NSTimer scheduledTimerWithTimeInterval: time*60 target:self selector:@selector(dealTimer) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
  }
  [self setBarButtonItem];
}


- (void)viewDidLoad {

  [super viewDidLoad];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
  
  self.height = 0.0;

  self.view.backgroundColor = [UIColor whiteColor];
  self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
  self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
  [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
  
  WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
  config.userContentController = [[WKUserContentController alloc] init];
  // 交互对象设置
  [config.userContentController addScriptMessageHandler:self name:@"closeWeb"];
  // 支持内嵌视频播放，不然网页中的视频无法播放
  config.allowsInlineMediaPlayback = YES;
  self.webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight-self.height, ScreenWidth, ScreenHeight-SafeAreaBottomHeight-SafeAreaTopHeight+self.height) configuration:config];
  [self.view addSubview:self.webView];
  
  self.webView.scrollView.delegate = self;
  self.webView.navigationDelegate = self;
  self.webView.UIDelegate = self;
  
  self.webView.scrollView.bounces = NO;
  // 开始右滑返回手势
  self.webView.allowsBackForwardNavigationGestures = YES;
  NSString *url = [self testurl];
  [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
  
  //    self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, self.progressH, ScreenWidth, 2)];
  //    [self.view addSubview:self.progressView];
  //    self.progressView.progressTintColor = [UIColor greenColor];
  //    self.progressView.trackTintColor = [UIColor clearColor];
  
  NSKeyValueObservingOptions observingOptions = NSKeyValueObservingOptionNew;
  // KVO 监听属性，除了下面列举的两个，还有其他的一些属性，具体参考 WKWebView 的头文件
  [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:observingOptions context:nil];
  [self.webView addObserver:self forKeyPath:@"title" options:observingOptions context:nil];
  [self.webView addObserver:self forKeyPath:@"canGoBack" options:NSKeyValueObservingOptionNew context:NULL];
  
  
  // 监听 self.webView.scrollView 的 contentSize 属性改变，从而对底部添加的自定义 View 进行位置调整
  [self.webView.scrollView addObserver:self forKeyPath:@"contentSize" options:observingOptions context:nil];
  self.loadingView = [[JXLoadingView alloc]init];
  [self.loadingView showInView:self.view];
  
}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
  if ([keyPath isEqualToString:@"estimatedProgress"]) {
    //        [self.progressView setProgress:self.webView.estimatedProgress animated:YES];
    //        if (self.webView.estimatedProgress < 1.0) {
    //            self.delayTime = 1 - self.webView.estimatedProgress;
    //            return;
    //        }
    //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //            self.progressView.progress = 0;
    //        });
  } else if ([keyPath isEqualToString:@"title"]) {
    if([[self.webView.title lowercaseString] isEqualToString:@"title"]||[[self.webView.title lowercaseString] isEqualToString:@"weixin"]
       ||[self.data[@"NavgationBar"] isEqualToString:@"yes"]){
      self.title = @"";
    }else{
      self.title = self.webView.title;
    }
    
  } else if ([keyPath isEqualToString:@"contentSize"]) {
    if (self.contentHeight != self.webView.scrollView.contentSize.height) {
      self.contentHeight = self.webView.scrollView.contentSize.height;
      NSLog(@"----------%@", NSStringFromCGSize(self.webView.scrollView.contentSize));
    }
  }
}

#pragma mark 设置BarButtonItem
- (void)setBarButtonItem
{
  //通过imageInset调整item的位置和item之间的位置
  //设置返回按钮
  self.leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:[self addItemWithImage:@"back_item"
                                                                              imageInset:UIEdgeInsetsMake(0, -5, 0, 5)
                                                                                    size:CGSizeMake(28, 28)
                                                                                  action:@selector(selectedToBack)]];
  
  //设置关闭按钮
  self.leftBarButtonSecond = [[UIBarButtonItem alloc]initWithCustomView:[self addItemWithImage:@"close_item"
                                                                                    imageInset:UIEdgeInsetsMake(0, -5, 0, -5)
                                                                                          size:CGSizeMake(28, 28)
                                                                                        action:@selector(selectedToClose)]];
  
  if(![self.data[@"NavgationBar"] isEqualToString:@"yes"]){
    self.navigationItem.leftBarButtonItems = @[self.leftBarButton];
  }
  
  if (@available(iOS 11,*)) {
    NSLog(@"** iOS 11 **");
  }else{
    NSLog(@"** Not iOS 11 **");
  }
  
  //设置刷新按妞
  UIBarButtonItem *reloadItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"reload_item"]
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(selectedToReloadData)];
  
  self.navigationItem.rightBarButtonItems = @[self.leftBarButtonSecond];
  
}
#pragma mark 关闭并上一界面 并通知React native刷新
- (void)selectedToClose
{
  [JXPushManager notificationEvent:@"WebViewCloseEvent" obj:@"shuaxin"];
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 返回上一个网页还是上一个Controller，对机票特殊处理
- (void)selectedToBack
{
  NSInteger step = self.webView.backForwardList.backList.count;
  if (self.webView.canGoBack == YES){
    if(step==1&&[[self testurl] containsString:feibaH5]){
      [self selectedToClose];
    }else{
      [self.webView goBack];
    }
  }else{
    [self selectedToClose];
  }
}

#pragma mark reload
- (void)selectedToReloadData
{
  [_webView reload];
}
#pragma mark 添加导航栏按钮item
- (UIButton *)addItemWithImage:(NSString *)imageName imageInset:(UIEdgeInsets)inset size:(CGSize)itemSize action:(SEL)action {
  
  UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
  UIImage *image = [UIImage imageNamed:imageName];
  button.frame = CGRectMake(0, 0, itemSize.width, itemSize.height);
  [button setImageEdgeInsets:inset];
  [button setImage:image forState:UIControlStateNormal];
  [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
  button.titleLabel.font = [UIFont systemFontOfSize:16];
  
  UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
  self.navigationItem.leftBarButtonItem = rightItem;
  
  return button;
}

#pragma mark - WKNavigationDelegate

// 开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
  NSLog(@"didStartProvisionalNavigation   ====    %@", navigation);
  self.currentURL = webView.URL.absoluteString;
}

// 页面加载完调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
  [self.loadingView dismiss];
  if([self.data[@"hiddenNavBar"] isEqualToString:@"yes"]){
    self.height = SafeAreaTopHeight+IsiPoneX;
    self.webView.frame = CGRectMake(0, SafeAreaTopHeight-self.height, ScreenWidth, ScreenHeight-SafeAreaBottomHeight-SafeAreaTopHeight+self.height);
    [self.navigationController setNavigationBarHidden:YES animated:YES];
  }else{
    self.height = 0.0;
    self.webView.frame = CGRectMake(0, SafeAreaTopHeight-self.height, ScreenWidth, ScreenHeight-SafeAreaBottomHeight-SafeAreaTopHeight+self.height);
    [self.navigationController setNavigationBarHidden: NO animated:YES];
  }
  
  NSLog(@"didFinishNavigation   ====    %@", navigation);
  [self.webView evaluateJavaScript:@"isZatGo()" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
    //此处可以打印error.
    if (error) {
      NSLog(@"error = %@", error);
    }else {
      NSLog(@"object = %@", result);
    }
  }];
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
  
  [self.loadingView showErrorInView:self.view];
  NSLog(@"didFailProvisionalNavigation   ====    %@\nerror   ====   %@", navigation, error);
}

// 内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(null_unspecified WKNavigation *)navigation {
  NSLog(@"didCommitNavigation   ====    %@", navigation);
}

#pragma mark - webview请求时跳转处理
// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
  
  // 获取完整url并进行UTF-8转码
  NSString *strRequest = [navigationAction.request.URL.absoluteString stringByRemovingPercentEncoding];
  
  NSURL *url = navigationAction.request.URL;
  NSString *scheme = [url scheme];
  UIApplication *app = [UIApplication sharedApplication];
  WKNavigationActionPolicy actionPolicy = WKNavigationActionPolicyAllow;
  
  if ([scheme isEqualToString:@"tel"]) {
    if ([app canOpenURL:url]) {
      CGFloat version = [[[UIDevice currentDevice]systemVersion]floatValue];
      if (version >= 10.0) {
        /// 大于等于10.0系统使用此openURL方法
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
      } else {
        [[UIApplication sharedApplication] openURL:url];
      }
    }
  }
  
  if (([strRequest hasPrefix:@"http://"]||[strRequest hasPrefix:@"https://"])&&![strRequest containsString:@"/h5/router.html"]) {
    // 允许跳转
    //如果是跳转一个新页面
    if (navigationAction.targetFrame == nil) {
      [webView loadRequest:navigationAction.request];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
  }
  //拦截收银台路由页
  if([strRequest containsString:@"/h5/router.html"]&&![strRequest containsString:@"timestamp="]){
    NSString* urlString =[NSString stringWithFormat:@"%@&timestamp=%@",strRequest,[self currentTimeStr]];
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    decisionHandler(WKNavigationActionPolicyCancel);
    return;
  }else if([strRequest containsString:@"/h5/router.html"]&&[strRequest containsString:@"timestamp="]){
    decisionHandler(WKNavigationActionPolicyAllow);
  }else{
    // 拦截点击链接
    if([strRequest hasPrefix:@"zatgo://"]){
      NSArray * arr = [strRequest componentsSeparatedByString:@"?"];
      NSString* prepayIdStr = [arr[0] stringByReplacingOccurrencesOfString:@"zatgo://" withString:@""];
      
      NSMutableArray *array = [[NSMutableArray alloc]init];
      
      for (int i=1; i<arr.count;i++){
        [array addObject:arr[i]];
      }
      
      NSString* subString = [array componentsJoinedByString:@"?"];
      NSString* urlString =[NSString stringWithFormat:@"%@/checkout/h5/login.html?token=%@&prepayId=%@&%@",self.data[@"payUrl"],self.data[@"zatgoToken"],prepayIdStr,subString];
      [self checkPrepayId:prepayIdStr url:urlString];
      decisionHandler(WKNavigationActionPolicyCancel);
      return;
    }
    
    if([strRequest hasPrefix:@"weixin://"]||[strRequest hasPrefix:@"wx://"]){
      if ([[UIApplication sharedApplication] canOpenURL:navigationAction.request.URL]){
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]){
          [[UIApplication sharedApplication] openURL:navigationAction.request.URL options:@{UIApplicationOpenURLOptionUniversalLinksOnly: @NO} completionHandler:^(BOOL success) {
            
          }];
        }else{
          [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
        }
      }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请确认是否已安装微信" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
      }
    }
    decisionHandler(WKNavigationActionPolicyCancel);
  }
}

// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
  NSLog(@"decidePolicyForNavigationResponse   ====    %@==%@", navigationResponse,webView.URL);
  decisionHandler(WKNavigationResponsePolicyAllow);
}

// 加载 HTTPS 的链接，需要权限认证时调用  \  如果 HTTPS 是用的证书在信任列表中这不要此代理方法
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
  if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
    if ([challenge previousFailureCount] == 0) {
      NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
      completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
    } else {
      completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    }
  } else {
    completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
  }
}

#pragma mark - WKUIDelegate

// 提示框
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
  UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示框" message:message preferredStyle:UIAlertControllerStyleAlert];
  [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    completionHandler();
  }]];
  [self presentViewController:alert animated:YES completion:NULL];
}

// 确认框
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler {
  UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确认框" message:message preferredStyle:UIAlertControllerStyleAlert];
  [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    completionHandler(YES);
  }]];
  [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    completionHandler(NO);
  }]];
  [self presentViewController:alert animated:YES completion:NULL];
}

// 输入框
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * __nullable result))completionHandler {
  
  UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"输入框" message:prompt preferredStyle:UIAlertControllerStyleAlert];
  [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
    textField.textColor = [UIColor blackColor];
    textField.placeholder = defaultText;
  }];
  [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    completionHandler([[alert.textFields lastObject] text]);
  }]];
  [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    completionHandler(nil);
  }]];
  [self presentViewController:alert animated:YES completion:NULL];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  [self.webView evaluateJavaScript:@"parseFloat(document.getElementById(\"AppAppendDIV\").style.width);" completionHandler:^(id value, NSError * _Nullable error) {
    NSLog(@"======= %@", value);
  }];
}


#pragma mark 设置wkwebview的WKScriptMessageHandler代理方法
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message{
  if ([message.name isEqualToString:@"closeWeb"]) {
    // 打印所传过来的参数，只支持NSNumber, NSString, NSDate, NSArray,
    // NSDictionary, and NSNull类型
    [self selectedToClose];
  }
}

#pragma mark - 监听移除

- (void)dealloc {
  [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
  [self.webView removeObserver:self forKeyPath:@"title"];
  [self.webView removeObserver:self forKeyPath:@"canGoBack"];
  [self.webView.scrollView removeObserver:self forKeyPath:@"contentSize"];
  [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"closeWeb"];
}

#pragma mark - 机票支付时，prepayId验证
//获取当前时间戳
- (NSString *)currentTimeStr{
  NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
  NSTimeInterval time=[date timeIntervalSince1970]*1000;// *1000 是精确到毫秒，不乘就是精确到秒
  NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
  return timeString;
}

-(void)checkPrepayId:(NSString*)prepayId url:(NSString*)url{
  NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
  NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil];
  [manager.requestSerializer setValue:app_Version forHTTPHeaderField:@"appVersion"];
  [manager.requestSerializer setValue:@"0" forHTTPHeaderField:@"appType"];
  [manager.requestSerializer setValue:self.data[@"zatgoToken"] forHTTPHeaderField:@"token"];
  [manager.requestSerializer setValue:@"7c195566331c4cbd86ef38d5e3a845a1" forHTTPHeaderField:@"appSign"];
  [manager.requestSerializer setValue:DeviceUID forHTTPHeaderField:@"deviceNo"];

  // 2.设置非校验证书模式
  manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.securityPolicy setValidatesDomainName:NO];
  
  NSString* urlString = [NSString stringWithFormat:@"%@/pay/orders/check/prepay/%@",self.serverUrl[@"serverUrl"],prepayId];
  [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    NSLog(@"%@",responseObject);
    NSString* update = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"] boolValue] ? @"YES":@"NO"];
    if ([update isEqualToString:@"YES"]) {
      [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    }else{
      [self selectedToClose];
      
    }
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    [self selectedToClose];
  }];
}


#pragma mark -Timer 定时功能
-(void)dealTimer{
  if(self.data[@"timer"]){
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil];
    [manager.requestSerializer setValue:app_Version forHTTPHeaderField:@"appVersion"];
    [manager.requestSerializer setValue:@"0" forHTTPHeaderField:@"appType"];
    [manager.requestSerializer setValue:self.data[@"zatgoToken"] forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"7c195566331c4cbd86ef38d5e3a845a1" forHTTPHeaderField:@"appSign"];
    [manager.requestSerializer setValue:DeviceUID forHTTPHeaderField:@"deviceNo"];

    // 2.设置非校验证书模式
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.securityPolicy setValidatesDomainName:NO];
    
    NSString* urlString = [NSString stringWithFormat:@"%@/mining/task/done/%@",self.serverUrl[@"serverUrl"],self.data[@"taskId"]];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
      
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
      
    }];
  }
}
#pragma mark - 监听app进入后台停止计时，进入前台重新计时
- (void)enterActive:(NSNotification*)noti{
  // 程序进入前台，开启定时器
  NSInteger time = [self.data[@"timer"] integerValue];
  [self.timer setFireDate:[NSDate dateWithTimeInterval: 60 * time sinceDate:[NSDate date]]];
  
}

- (void)enterBackground:(NSNotification *)noti {
  // 程序进入后台，关闭定时器
  [self.timer  setFireDate:[NSDate distantFuture]];
}

#pragma mark - 退出webview，屏幕强制竖屏
- (void)viewDidDisappear:(BOOL)animated {
  [self.timer invalidate];
  self.timer = nil;
  [self.navigationController setNavigationBarHidden:NO animated:animated];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.allowRotation = NO;
    //强制归正：
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
      SEL selector = NSSelectorFromString(@"setOrientation:");
      NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
      [invocation setSelector:selector];
      [invocation setTarget:[UIDevice currentDevice]];
      int val =UIInterfaceOrientationPortrait;
      [invocation setArgument:&val atIndex:2];
      [invocation invoke];
    }
    [self.timer invalidate];
    self.timer = nil;
}

//#pragma mark - UINavigationControllerDelegate
//// 将要显示控制器
//- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//  // 判断要显示的控制器是否是自己
//  BOOL isShowHomePage = [viewController isKindOfClass:[self class]];
//
//  [self.navigationController setNavigationBarHidden:isShowHomePage animated:YES];
//}
#pragma mark - 监听屏幕旋转
//监听屏幕旋转，更改webview的frame
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
  [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
  if (size.width > size.height) {
    //横屏设置，为防止遮挡键盘,调整输入视图的高度
    self.webView.frame=CGRectMake(0, 0, size.width,size.height) ;
  }else{
    //竖屏设置
    self.webView.frame=CGRectMake(0, 0, size.width, size.height) ;
  }
}


@end

