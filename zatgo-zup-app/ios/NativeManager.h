//
//  QRCodeManager.h
//  zatgo
//
//  Created by 周建新 on 2018/6/19.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface NativeManager : NSObject<RCTBridgeModule>

@end
