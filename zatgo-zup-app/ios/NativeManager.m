//
//  QRCodeManager.m
//  zatgo
//
//  Created by 周建新 on 2018/6/19.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "NativeManager.h"
#import <UIKit/UIKit.h>
#import<AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
#import<AssetsLibrary/AssetsLibrary.h>
#import<CoreLocation/CoreLocation.h>

@implementation NativeManager
RCT_EXPORT_MODULE();

//  对外提供调用方法,演示Callback
RCT_EXPORT_METHOD(ImageSelectCallbackEventOne:(NSString *)name callback:(RCTResponseSenderBlock)callback)
{
  //获取选中的照片
  NSURL *url = [NSURL URLWithString:name];
  UIImage *image=[[UIImage alloc]initWithData:[NSData dataWithContentsOfURL:url]];
  //初始化  将类型设置为二维码
  CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:nil];
  
    //设置数组，放置识别完之后的数据
    NSArray *features = [detector featuresInImage:[CIImage imageWithData:UIImagePNGRepresentation(image)]];
    //判断是否有数据（即是否是二维码）
    if (features.count >= 1) {
      //取第一个元素就是二维码所存放的文本信息
      CIQRCodeFeature *feature = features[0];
      NSString *scannedResult = feature.messageString;
      //通过对话框的形式呈现
      callback(@[[NSNull null],scannedResult]);

    }else{
      callback(@[[NSNull null],[NSNull null]]);

    }
}

RCT_EXPORT_METHOD(RNOpenOneVC:(NSDictionary *)msg){
  
  NSLog(@"RN传入原生界面的数据为:%@",msg);
  //主要这里必须使用主线程发送,不然有可能失效
  dispatch_async(dispatch_get_main_queue(), ^{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"pushView" object:msg];
  });
}

RCT_EXPORT_METHOD(authStatus:(NSInteger)index callback:(RCTResponseSenderBlock)callback){
  
  switch (index) {
    case 1:
    {
      //相机权限
      AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
      if (authStatus ==AVAuthorizationStatusRestricted ||//此应用程序没有被授权访问的照片数据。
          authStatus ==AVAuthorizationStatusDenied)  //用户已经明确否认了这一照片数据的应用程序访问
      {
        
//        // 无权限 引导去开启
//        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//        if ([[UIApplication sharedApplication]canOpenURL:url]) {
//          [[UIApplication sharedApplication]openURL:url];
//        }
        callback(@[[NSNull null],UIApplicationOpenSettingsURLString]);

      }else{
        callback(@[[NSNull null],@"yes"]);
      }
      
    }
      break;
    case 2:
    {
      //相册权限
      ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
      if (author ==ALAuthorizationStatusRestricted || author ==ALAuthorizationStatusDenied){
//        //无权限 引导去开启
//        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//        if ([[UIApplication sharedApplication] canOpenURL:url]) {
//          [[UIApplication sharedApplication] openURL:url];
//        }
        callback(@[[NSNull null],UIApplicationOpenSettingsURLString]);

      }else{
        callback(@[[NSNull null],@"yes"]);

      }
      
      
    }
      break;
      
    default:
      break;
  }
}


@end
