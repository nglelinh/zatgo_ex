package com.zatgo;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.zatgo.utils.HttpUtil;
import com.zatgo.utils.Logger;
import com.zatgo.utils.SharedPreferencesUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


/**
 * 收银台（外部唤醒）
 */
public class ZatGoPayActivity extends AppCompatActivity {
    private static final String TAG = "ZatGoPayActivity";
    private WebView mWebView;
    private LinearLayout linearLayout;
    private LinearLayout.LayoutParams params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h5_pay);
        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        String action = intent.getAction();
        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {
                String dataString = intent.getDataString();
                Logger.d(TAG, "dataString: " + dataString);
                String host = uri.getHost();
                Logger.d(TAG, "host: " + host);
                if (TextUtils.isEmpty(host)) {
                    finish();
                } else {
                    String checkUrl = String.format("%s%s%s", BuildConfig.API_HOST, Constants.CHECK_PREPAY_ID, host);
                    checkPrepayId(checkUrl, host);
                }
            }
        }
    }

    private void initView(String host) {
        String url = String.format("%s%s", BuildConfig.PAY_HOST, Constants.API_PAY);
        String token = (String) SharedPreferencesUtil.get(this, Constants.NATIVE_PERSIST_KEY, "");
        if (!TextUtils.isEmpty(token)) {
            url = String.format("%s?token=%s&prepayId=%s", url, token, host);
            Logger.d(TAG, "Token非空：" + url);
        } else {
            // 未登录
            Intent it = new Intent(this, MainActivity.class);
            it.putExtra(Constants.EXTRA_PAY_UNLOGIN, host);
            startActivity(it);
            finish();
        }

        params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        linearLayout = (LinearLayout) findViewById(R.id.webview_pay);
        mWebView = new WebView(getApplicationContext());
        mWebView.setLayoutParams(params);
        setDefaultWebSettings(mWebView);

        mWebView.loadUrl(url);
        linearLayout.addView(mWebView);
    }

    /**
     * PrepayId校验
     *
     * @param checkUrl
     * @param host
     */
    private void checkPrepayId(String checkUrl, final String host) {
        Logger.d(TAG, "checkPrepayId: " + checkUrl);
        HttpUtil httpUtil = MainApplication.getInstance().getHttpUtil();
        httpUtil.get(checkUrl, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Logger.d(TAG, "onFailure: " + e.getMessage());
                finish();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String res = response.body().string();
                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        boolean isValid = jsonObject.optBoolean("data");
//                        boolean isValid = JSONObjectUtils.getBoolean(JSONObjectUtils.parse(res), "data");
                        if (isValid) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    initView(host);
                                }
                            });
                        } else {
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        finish();
                    }
                    Logger.d(TAG, "onResponse: " + res);
                }

            }
        });
    }

    /**
     * webview属性设置
     *
     * @param webView
     */
    private void setDefaultWebSettings(WebView webView) {
        WebSettings settings = webView.getSettings();
        //5.0以上开启混合模式加载
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setJavaScriptEnabled(true);//允许js代码
        settings.setDisplayZoomControls(false);//禁用放缩
        settings.setBuiltInZoomControls(false);
        settings.setSavePassword(false);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);//不使用网络缓存
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setAllowFileAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setLoadsImagesAutomatically(true);//自动加载图片
        settings.setBlockNetworkImage(false);// 不阻塞图片
        settings.setBlockNetworkLoads(false);
        webView.setVerticalScrollbarOverlay(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.addJavascriptInterface(this, "zatgo");
        //移除部分系统JavaScript接口
        ZatGoPayActivity.removeJavascriptInterfaces(webView);
    }

    // 防止远程执行漏洞
    @TargetApi(11)
    private static final void removeJavascriptInterfaces(WebView webView) {
        try {
            if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT < 17) {
                webView.removeJavascriptInterface("searchBoxJavaBridge_");
                webView.removeJavascriptInterface("accessibility");
                webView.removeJavascriptInterface("accessibilityTraversal");
            }
        } catch (Throwable tr) {
            tr.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && mWebView != null) {
            mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && mWebView != null) {
            mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, String url) {
            Logger.d(TAG, "shouldOverrideUrlLoading: " + url);
            if (!(url.startsWith("http") || url.startsWith("https"))) {
                return true;
            }
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWebView != null) {
            mWebView.removeAllViews();
            try {
                mWebView.destroy();
            } catch (Throwable t) {
            }
            mWebView = null;
        }
    }


    @android.webkit.JavascriptInterface
    public void closePayPage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ZatGoPayActivity.this.finish();
            }
        });
    }


    @android.webkit.JavascriptInterface
    public boolean isZatGo() {
        return true;
    }

}
