package com.zatgo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zatgo.modules.ExtensionModule;
import com.zatgo.utils.HttpUtil;
import com.zatgo.utils.Logger;
import com.zatgo.utils.SharedPreferencesUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * zatgo内部加载网页
 */
public class WebViewAndroidActivity extends AppCompatActivity {

    private static final String TAG = "TAGAndroidActivity";
    private WebView mWebView;
    private ProgressBar progressBar;
    private LinearLayout rootView;
    private LinearLayout.LayoutParams params;
    private TextView textTitle;
    private LinearLayout btnBack;
    private LinearLayout btnClose;
    private boolean titleInvisible;
    private boolean hiddenNavBar;
    private boolean showBackButton = false;
    private List<String> titles = new ArrayList<>();
    private ValueCallback<Uri> uploadMessage;
    private ValueCallback<Uri[]> uploadMessageAboveL;
    private final static int FILE_CHOOSER_RESULT_CODE = 10000;
    private static final int MSG_PAGE_TIMEOUT = 29990;
    private static final long TIME_OUT = 15000;
    private int elapsedTime;// 消耗时间
    private String taskId;
    private String taskUrl;
    private HttpUtil httpUtil;
    private ScheduledExecutorService scheduler;
    private RelativeLayout navBar;
    private Button btnRefresh;
    private LinearLayout errorView;
    private String webUrl;

    private Timer mTimer;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_PAGE_TIMEOUT:
                    //这里对已经显示出页面且加载超时的情况不做处理
                    Log.d(TAG, "handleMessage: 超时");
                    if (mWebView != null && mWebView.getProgress() < 100)
                        loadErrorPage();
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_android);
        initView();
    }


    private void initView() {
        navBar = (RelativeLayout) findViewById(R.id.layout_titlebar);
        textTitle = (TextView) findViewById(R.id.text_title);
        btnBack = (LinearLayout) findViewById(R.id.btn_backward);
        btnClose = (LinearLayout) findViewById(R.id.button_close);
        rootView = (LinearLayout) findViewById(R.id.webview_android);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btnRefresh = (Button) findViewById(R.id.btn_refresh);
        errorView = (LinearLayout) findViewById(R.id.layout_error);
        btnRefresh.setOnClickListener(mOnClickListener);
        btnClose.setOnClickListener(mOnClickListener);
        btnBack.setOnClickListener(mOnClickListener);


        webUrl = BuildConfig.FEIBA_H5;
        Intent intent = getIntent();
        // rn调用webview
        if (intent.hasExtra(Constants.WB_EXTRA_PARAMS)) {
            webUrl = intent.getStringExtra(Constants.WB_EXTRA_PARAMS);
            Logger.d(TAG, "initView js传参:" + webUrl);

            Uri uri = Uri.parse(webUrl);
            if (webUrl.contains("hiddenNavBar")) {
                hiddenNavBar = uri.getBooleanQueryParameter("hiddenNavBar", false);
            }

            if (webUrl.contains("showBackButton")) {
                showBackButton = uri.getBooleanQueryParameter("showBackButton", false);
            }

            if (webUrl.contains(Constants.WEBVIEW_TITLE_INVISIBLE)) {
                titleInvisible = uri.getBooleanQueryParameter(Constants.WEBVIEW_TITLE_INVISIBLE, false);
            }

            taskId = uri.getQueryParameter("taskId");
            String timer = uri.getQueryParameter("timer");
            if (!TextUtils.isEmpty(taskId) && !TextUtils.isEmpty(timer)) {
                elapsedTime = Integer.parseInt(timer);
                taskUrl = String.format("%s%s%s", BuildConfig.API_HOST, "/mining/task/done/", taskId);
            }
            Logger.d(TAG, "initView taskId参:" + taskId);
            Logger.d(TAG, "initView timer参:" + elapsedTime);

        }

        handleTitle(titleInvisible);
        handleBack(showBackButton);

        params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mWebView = new WebView(getApplicationContext());
        mWebView.setLayoutParams(params);
        setDefaultWebSettings(mWebView);


        mWebView.loadUrl(webUrl);
        rootView.addView(mWebView);

        httpUtil = MainApplication.getInstance().getHttpUtil();
    }


    final TimerTask task = new TimerTask() {
        @Override
        public void run() {
            //execute task
            Logger.d(TAG, "请求路径：" + taskUrl);
            httpUtil.get(taskUrl, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        final String res = response.body().string();
                        Logger.d(TAG, "请求返回: " + res);
                    }
                }
            });
        }
    };

    private void startTask() {
        if (!TextUtils.isEmpty(taskUrl) && elapsedTime > 0) {
            Logger.d(TAG, "开始任务: ");
            scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleAtFixedRate(task, elapsedTime, elapsedTime, TimeUnit.MINUTES);
        }
    }

    private void stopTask(ScheduledExecutorService scheduler) {
        if (scheduler != null && !scheduler.isShutdown()) {
            Logger.d(TAG, "结束任务: ");
            scheduler.shutdown();
        }
    }


    // 控制标题栏显示
    private void handleTitle(boolean invisible) {
        if (invisible) {
            hideTitle();
        } else {
            showTitle();
        }
    }

    // 控制返回键显示
    private void handleBack(boolean showBackButton) {
        btnBack.setVisibility(showBackButton ? View.VISIBLE : View.INVISIBLE);
    }

    private void showTitle() {
        if (textTitle != null && textTitle.getVisibility() == View.INVISIBLE) {
            textTitle.setVisibility(View.VISIBLE);
        }
    }

    private void hideTitle() {
        if (textTitle != null && textTitle.getVisibility() == View.VISIBLE) {
            textTitle.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * webview
     *
     * @param webView
     */
    private void setDefaultWebSettings(WebView webView) {
        WebSettings settings = webView.getSettings();
        //5.0以上开启混合模式加载
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setJavaScriptEnabled(true);//允许js代码
        settings.setDisplayZoomControls(false);//禁用放缩
        settings.setBuiltInZoomControls(false);
        settings.setSavePassword(false);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);//不使用网络缓存
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setAllowFileAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setLoadsImagesAutomatically(true);//自动加载图片
        settings.setBlockNetworkImage(false);// 不阻塞图片
        settings.setBlockNetworkLoads(false);

        webView.setVerticalScrollbarOverlay(true);
        webView.setWebViewClient(mWebViewClient);
        webView.setWebChromeClient(mWebChromeClient);
        webView.addJavascriptInterface(this, "zatgo");
        //移除部分系统JavaScript接口
        WebViewAndroidActivity.removeJavascriptInterfaces(webView);
    }

    // 防止远程执行漏洞
    private static final void removeJavascriptInterfaces(WebView webView) {
        try {
            if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT < 17) {
                webView.removeJavascriptInterface("searchBoxJavaBridge_");
                webView.removeJavascriptInterface("accessibility");
                webView.removeJavascriptInterface("accessibilityTraversal");
            }
        } catch (Throwable tr) {
            tr.printStackTrace();
        }
    }


    /**
     * 自定义WebChromeClient
     */
    private WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            // android 6.0 以下通过title获取
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                if (title.contains("404") || title.contains("500") || title.contains("Error")) {
                    loadErrorPage();
                }
            }
            titles.add(title);
            if (!TextUtils.isEmpty(title)) {
                Logger.d(TAG, "onReceivedTitle: " + title);
                if ("Title".equalsIgnoreCase(title) || title.contains("/h5/router.html")) {
                    textTitle.setText("");
                } else {
                    textTitle.setText(title);
                }

            }
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            progressBar.setProgress(newProgress);
            super.onProgressChanged(view, newProgress);
        }

        // For Android < 3.0
        public void openFileChooser(ValueCallback<Uri> valueCallback) {
            uploadMessage = valueCallback;
            openImageChooserActivity();
        }

        // For Android  >= 3.0
        public void openFileChooser(ValueCallback valueCallback, String acceptType) {
            uploadMessage = valueCallback;
            openImageChooserActivity();
        }

        //For Android  >= 4.1
        public void openFileChooser(ValueCallback<Uri> valueCallback, String acceptType, String capture) {
            uploadMessage = valueCallback;
            openImageChooserActivity();
        }

        // For Android >= 5.0
        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
            uploadMessageAboveL = filePathCallback;
            openImageChooserActivity();
            return true;
        }


    };


    /**
     * 自定义WebViewClient
     */
    private WebViewClient mWebViewClient = new WebViewClient() {

        @Override
        public void onPageStarted(final WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mTimer = new Timer();
            TimerTask tt = new TimerTask() {
                @Override
                public void run() {
                    Message m = new Message();
                    m.what = MSG_PAGE_TIMEOUT;
                    mHandler.sendMessage(m);
                    mTimer.cancel();
                    mTimer.purge();
                }
            };
            mTimer.schedule(tt, TIME_OUT);
        }

        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, String url) {
            Logger.d(TAG, "shouldOverrideUrlLoading: " + url);

            if ((url.startsWith("http:") || url.startsWith("https:")) && !url.contains("/h5/router.html")) {
                return false;
            } else if (url.contains("/h5/router.html")) {
                String routerUrl = String.format("%s&timestamp=%d", url, new Date().getTime());
                Logger.d(TAG, "路由页：" + routerUrl);
                view.loadUrl(routerUrl);
                return true;
            } else if (url.startsWith("zatgo://")) {
                Uri uri = Uri.parse(url);
                String prepayId = uri.getHost();
                String query = uri.getQuery();
                Logger.d(TAG, "query：" + query);
                String checkUrl = String.format("%s%s%s", BuildConfig.API_HOST, Constants.CHECK_PREPAY_ID, prepayId);
                checkPrepayId(view, checkUrl, prepayId, query);

                return true;
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                if (intent.resolveActivity(view.getContext().getPackageManager()) != null) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    view.getContext().startActivity(intent);
                } else if (url.startsWith("wx:") || url.startsWith("weixin:")) {
                    new AlertDialog.Builder(WebViewAndroidActivity.this).setTitle(getResources().getString(R.string.dialog_wx_uninstall_title))
                            .setMessage(getResources().getString(R.string.dialog_wx_uninstall_msg))
                            .setPositiveButton(getResources().getString(R.string.dialog_wx_uninstall_ok), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                }
                            }).show();
                }
                return true;
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.d(TAG, "onPageFinished: ");
            mTimer.cancel();
            mTimer.purge();
            navBar.setVisibility(hiddenNavBar ? View.GONE : View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            if (rootView.getVisibility() != View.VISIBLE) {
                rootView.setVisibility(View.VISIBLE);
            }
            if (!mWebView.getSettings().getLoadsImagesAutomatically()) {
                mWebView.getSettings().setLoadsImagesAutomatically(true);
            }
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.d(TAG, "onReceivedError: " + errorCode + description + ":" + failingUrl);
            mWebView.loadUrl("about:blank");
            loadErrorPage();
        }


    };


    /**
     * PrepayId校验
     *
     * @param checkUrl
     * @param query
     */
    private void checkPrepayId(final WebView view, String checkUrl, final String prepayId, final String query) {
        Logger.d(TAG, "checkPrepayId: " + checkUrl);
        HttpUtil httpUtil = MainApplication.getInstance().getHttpUtil();
        httpUtil.get(checkUrl, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Logger.d(TAG, "onFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String res = response.body().string();
                    Logger.d(TAG, "onResponse: " + res);
                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        boolean isValid = jsonObject.optBoolean("data");
                        if (isValid) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            openPayPage(view, prepayId, query);
                                        }
                                    });
                                }
                            });
                        } else {
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        finish();
                    }

                }

            }
        });
    }


    /**
     * 跳转收银台页面
     *
     * @param view
     * @param query
     */
    private void openPayPage(WebView view, String prepayId, String query) {
        String payUrl = String.format("%s%s", BuildConfig.PAY_HOST, Constants.API_PAY);
        String token = (String) SharedPreferencesUtil.get(WebViewAndroidActivity.this, Constants.NATIVE_PERSIST_KEY, "");
        if (!TextUtils.isEmpty(token)) {
            payUrl = String.format("%s?token=%s", payUrl, token);
            if (!TextUtils.isEmpty(prepayId)) {
                payUrl = String.format("%s&prepayId=%s", payUrl, prepayId);
            }
            if (!TextUtils.isEmpty(query)) {
                payUrl = String.format("%s&%s", payUrl, query);
            }
            Logger.d(TAG, "Token非空：" + payUrl);
        }
        payUrl = payUrl + "?webviewTitleInvisible=true";
        view.loadUrl(payUrl);
    }


    /**
     * 图片选择
     */
    private void openImageChooserActivity() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        startActivityForResult(Intent.createChooser(i, "Image Chooser"), FILE_CHOOSER_RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_CHOOSER_RESULT_CODE) {
            if (null == uploadMessage && null == uploadMessageAboveL) {
                return;
            }
            Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
            if (uploadMessageAboveL != null) {
                onActivityResultAboveL(requestCode, resultCode, data);
            } else if (uploadMessage != null) {
                uploadMessage.onReceiveValue(result);
                uploadMessage = null;
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void onActivityResultAboveL(int requestCode, int resultCode, Intent intent) {
        if (requestCode != FILE_CHOOSER_RESULT_CODE || uploadMessageAboveL == null) {
            return;
        }
        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK) {
            if (intent != null) {
                String dataString = intent.getDataString();
                ClipData clipData = intent.getClipData();
                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null) {
                    results = new Uri[]{Uri.parse(dataString)};
                }
            }
        }
        uploadMessageAboveL.onReceiveValue(results);
        uploadMessageAboveL = null;
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    // 页面回退
    private void goBack() {
        if (mWebView.canGoBack()) {
            WebBackForwardList webBackForwardList = mWebView.copyBackForwardList();
            String h5 = String.format("%s/", BuildConfig.FEIBA_H5);
            if (webBackForwardList.getCurrentIndex() == 1 && h5.equalsIgnoreCase(mWebView.getUrl())) {
                finish();
            }
            if (titles.size() > 1) {
                titles.remove(titles.size() - 1);
                String title = titles.get(titles.size() - 1);
                if ("Title".equalsIgnoreCase(title) || title.contains("/h5/router.html")) {
                    textTitle.setText("");
                } else {
                    textTitle.setText(title);
                }
            }
            mWebView.goBack();
        } else {
            WebViewAndroidActivity.this.finish();
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_close:
                    WebViewAndroidActivity.this.finish();
                    break;
                case R.id.button_backward:
                    goBack();
                    break;
                case R.id.btn_refresh:
                    if (mWebView != null) {
                        errorView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                        rootView.setVisibility(View.INVISIBLE);
                        mWebView.loadUrl(webUrl);

                        mWebView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (mWebView != null) {
                                    mWebView.clearHistory();
                                }

                            }
                        }, 1200);

                    }
                    break;
                default:
                    break;
            }
        }

    };


    @Override
    protected void onPause() {
        if (mWebView != null) {
            mWebView.onPause();
        }
        stopTask(scheduler);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWebView != null) {
            mWebView.onResume();
        }
        startTask();
    }

    @Override
    protected void onDestroy() {
        if (mWebView != null) {
            mWebView.removeAllViews();
            mWebView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            mWebView.clearHistory();
            mWebView.clearCache(true);
            ((ViewGroup) mWebView.getParent()).removeView(mWebView);
            try {
                mWebView.destroy();
            } catch (Throwable t) {
            }
            mWebView = null;
        }
        stopTask(scheduler);
        ExtensionModule.sendEvent("WebViewCloseEvent", "webviewClose");
        super.onDestroy();
    }

    // 关闭当前页面
    @android.webkit.JavascriptInterface
    public void closePayPage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }

    // 关闭当前页面
    @android.webkit.JavascriptInterface
    public void closeWebView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }

    // 是否ZatGo内加载
    @android.webkit.JavascriptInterface
    public boolean isZatGo() {
        return true;
    }

    /**
     * 加载错误页面
     */
    private void loadErrorPage() {
        // 自定义错误提示页面
        if (mWebView != null) {
            mWebView.stopLoading();
        }

        rootView.setVisibility(View.INVISIBLE);
        errorView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

    }
}
