package com.zatgo;


import android.content.Intent;
import android.os.Bundle;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.pgyersdk.update.PgyUpdateManager;
import com.umeng.socialize.UMShareAPI;
import com.zatgo.modules.ShareModule;
import com.zatgo.utils.Logger;

import org.devio.rn.splashscreen.SplashScreen;

public class MainActivity extends ReactActivity {
    private static final String TAG = "MainActivity";
    private static final String SERVER_URL = "serverUrl";
    private static final String FEIBA_H5 = "feibaH5";
    private static final String SLIDER_H5 = "sliderH5";
    private static final String FEIBA_AUTH = "feibaAuth";
    private static final String EXCHANGE_H5 = "exchangeUrl";
    private static final String BDD_AUTH = "bddAuth";
    private static final String DEV_ENV = "devEnv";
    private static final String FLAVOR_TEST = "t";
    private static final String PAY_ORDER_ID = "payOrderId";
    private static final String EVENT_NAME = "pullLogin";
    private String orderId;

    /**
     * Returns the name of the main components registered from JavaScript.
     * This is used to schedule rendering of the components.
     */
    @Override
    protected String getMainComponentName() {
        return "zatgo";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this, true);
        super.onCreate(savedInstanceState);
        ShareModule.initActivity(this);
        Intent intent = getIntent();
        if (intent.hasExtra(Constants.EXTRA_PAY_UNLOGIN)) {
            orderId = intent.getStringExtra(Constants.EXTRA_PAY_UNLOGIN);
            WritableMap params = new WritableNativeMap();
            params.putString(PAY_ORDER_ID, orderId);
            Logger.d(TAG, "onCreate: " + orderId);
        } else {
            PgyUpdateManager.setIsForced(true); //设置是否强制更新。true为强制更新；false为不强制更新（默认值）。
            PgyUpdateManager.register(this);
        }
    }


    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        Bundle bundle = new Bundle();
        bundle.putString(SERVER_URL, BuildConfig.API_HOST);
        bundle.putString(FEIBA_H5, BuildConfig.FEIBA_H5);
        bundle.putString(EXCHANGE_H5, BuildConfig.EXCHANGE_H5);
        bundle.putString(SLIDER_H5, BuildConfig.SLIDER_H5);
        bundle.putBoolean(DEV_ENV, FLAVOR_TEST.equalsIgnoreCase(BuildConfig.FLAVOR));

        ReactActivityDelegateWrap delegate = new ReactActivityDelegateWrap(this, getMainComponentName());
        delegate.setLaunchOptions(bundle);
        return delegate;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PgyUpdateManager.unregister();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }
}
