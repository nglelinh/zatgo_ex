package com.zatgo.modules;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.zatgo.utils.QRCodeDecoder;
import com.zatgo.utils.Logger;
import com.zatgo.utils.PathUtil;

import java.lang.ref.WeakReference;

public class ImageParseModule extends ReactContextBaseJavaModule implements ActivityEventListener {
    private static final String CLS_NAME = "ImageParseManager";

    private static final int CODE_GALLERY_REQUEST = 1;
    private final ReactApplicationContext reactContext;
    protected Callback callback;

    public ImageParseModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        this.reactContext.addActivityEventListener(this);
    }

    @ReactMethod
    public void parseImage(String imagePath, Callback callback) {
        this.callback = callback;
        new ParseTask(reactContext.getCurrentActivity()).execute(imagePath);
    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_GALLERY_REQUEST) {
            String photoPath = getRealPathFromURI(data.getData());
            // 回调到rn
            Logger.d(CLS_NAME, "onActivityResult: " + photoPath);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
    }

    @Override
    public String getName() {
        return CLS_NAME;
    }

    private String getRealPathFromURI(@NonNull final Uri uri) {
        return PathUtil.getRealPathFromUri(reactContext, uri);
    }


    private class ParseTask extends AsyncTask<String, Integer, String> {
        // 弱引用是允许被gc回收的;
        private final WeakReference<Activity> weakActivity;

        ParseTask(Activity activity) {
            this.weakActivity = new WeakReference<>(activity);
        }

        @Override
        protected String doInBackground(String... params) {
            // 解析二维码/条码
            return QRCodeDecoder.syncDecodeQRCode(params[0]);
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        protected void onPostExecute(String s) {
            Activity activity = weakActivity.get();
            reactContext.getCurrentActivity();
            if (activity == null || activity.isFinishing() || activity.isDestroyed()) {
                return;
            }

            callback.invoke(s);
        }
    }
}
