package com.zatgo.utils;

import android.content.Context;

import com.zatgo.Constants;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class HttpUtil {
    private final MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient mClient;
    private Context mContext;
    private String deviceId = "";


    public HttpUtil(Context context, OkHttpClient okHttpClient) {
        this.mContext = context;
        deviceId = GetDeviceId.getDeviceId(context);
        if (mClient == null) {
            this.mClient = okHttpClient;
        }
    }

    public void post(String url, String params, Callback callback) {
        //通过RequestBody.create 创建requestBody对象
        RequestBody requestBody = RequestBody.create(mediaType, params);
        //创建Request对象，设置URL地址，将RequestBody作为post方法的参数传入
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        //创建一个call对象,参数就是Request请求对象
        Call call = mClient.newCall(request);
        //请求加入调度,重写回调方法
        call.enqueue(callback);
    }

    public void get(String url, Callback callback) {
        String token = (String) SharedPreferencesUtil.get(mContext, Constants.NATIVE_PERSIST_KEY, "");
        //创建Request对象
        Request request = new Request.Builder()
                .url(url)
                .addHeader("token", token)
                .addHeader("appSign", "7c195566331c4cbd86ef38d5e3a845a1")
                .addHeader("deviceNo", deviceId)
                .method("GET", null)
                .build();
        //创建一个call对象,参数就是Request请求对象
        Call call = mClient.newCall(request);
        //请求加入调度，重写回调方法
        call.enqueue(callback);
    }
}
