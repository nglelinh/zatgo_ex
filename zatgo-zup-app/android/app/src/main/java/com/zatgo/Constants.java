package com.zatgo;

public class Constants {
    public static final String API_PAY = "/checkout/h5/login.html";
    public static final String NATIVE_PERSIST_KEY = "native_persist_key";
    public static final String EXTRA_PAY_UNLOGIN = "extra_pay_unlogin";
    public static final String WB_EXTRA_PARAMS = "params";
    public static final String CHECK_PREPAY_ID = "/pay/orders/check/prepay/";
    public static final String WEBVIEW_TITLE_INVISIBLE = "webviewTitleInvisible";
}
