package com.zatgo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.facebook.react.ReactActivityDelegate;

public class ReactActivityDelegateWrap extends ReactActivityDelegate {
    private Bundle mProps;
    public ReactActivityDelegateWrap(Activity activity, @Nullable String mainComponentName) {
        super(activity, mainComponentName);
    }

    public ReactActivityDelegateWrap(FragmentActivity fragmentActivity, @Nullable String mainComponentName) {
        super(fragmentActivity, mainComponentName);
    }

    @Nullable
    @Override protected Bundle getLaunchOptions() {
        return mProps;
    }

    //设置传入props
    protected void setLaunchOptions(Bundle props) {
        mProps = props;
    }
}

