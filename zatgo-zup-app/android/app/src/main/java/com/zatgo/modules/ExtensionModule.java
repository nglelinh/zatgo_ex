package com.zatgo.modules;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.zatgo.Constants;
import com.zatgo.utils.Logger;
import com.zatgo.utils.SharedPreferencesUtil;

public class ExtensionModule extends ReactContextBaseJavaModule {
    private static final String TAG = "ExtensionModule";

    public static ReactApplicationContext reactContext;

    public ExtensionModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    public static ReactApplicationContext getReactContext() {
        return reactContext;
    }

    @Override
    public String getName() {
        return "ExtensionModule";
    }

    @ReactMethod
    public void saveData(String key, String data) {
        Logger.d("测试", "保存数据Key：" + key + "," + data);
        SharedPreferencesUtil.put(getReactApplicationContext(), key, data);
    }

    @ReactMethod
    public void removeData(String key) {
        Logger.d("测试", "删除数据Key：" + key);
        SharedPreferencesUtil.remove(getReactApplicationContext(), key);
    }

    /**
     * 通过Activity名称启动
     * @param name
     * @param params
     */
    @ReactMethod
    public void startActivityFromJS(String name, String params) {
        try {
            Activity currentActivity = getCurrentActivity();
            if (null != currentActivity) {
                Class toActivity = Class.forName(name);
                Intent intent = new Intent(currentActivity, toActivity);
                intent.putExtra(Constants.WB_EXTRA_PARAMS, params);
                currentActivity.startActivity(intent);
            }
        } catch (Exception e) {
            throw new JSApplicationIllegalArgumentException(
                    "Could not open the activity :" + e.getMessage());
        }
    }

    /**
     * uri启动收银台页面
     * @param payInfo
     */
    @ReactMethod
    public void startZatGoPayPage(String payInfo) {
        try {
            Activity currentActivity = getCurrentActivity();
            if (null != currentActivity) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(String.format("zatgo://%s", payInfo)));
                currentActivity.startActivity(intent);
                currentActivity.finish();
            }
        } catch (Exception e) {
            throw new JSApplicationIllegalArgumentException(
                    "Could not open the activity :" + e.getMessage());
        }
    }


    /**
     * 获取Activity跳转传参
     * @param success
     * @param error
     */
    @ReactMethod
    public void getDataFromIntent(Callback success, Callback error) {
        try {
            Activity currentActivity = getCurrentActivity();
            Intent intent = currentActivity.getIntent();
            String result = null;
            if (intent.hasExtra(Constants.EXTRA_PAY_UNLOGIN)) {
                result = intent.getStringExtra(Constants.EXTRA_PAY_UNLOGIN);//会有对应数据放入
            }
            Logger.d(TAG, "getDataFromIntent: " + result);
            success.invoke(result);
        } catch (Exception e) {
            error.invoke(e.getMessage());
        }
    }

    /**
     * 给rn发送event
     *
     * @param eventName
     * @param body
     */
    public static void sendEvent(String eventName, WritableMap body) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, body);
    }

    /**
     * 给rn发送event
     *
     * @param eventName
     * @param body
     */
    public static void sendEvent(String eventName, String body) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, body);
    }


}
