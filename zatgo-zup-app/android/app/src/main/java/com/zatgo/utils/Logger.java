package com.zatgo.utils;

import android.util.Log;

import com.zatgo.BuildConfig;

public class Logger {
    /**
     * log tag
     */
    private static final String tag = "Logger";//application name
    /**
     * debug or not
     */
    private static boolean debug = BuildConfig.DEBUG;


    /**
     * log.i
     */
    public static void i(String tag, String msg) {
        if (debug) {
            Log.i(tag, msg);
        }
    }

    /**
     * log.v
     */
    public static void v(String tag, String msg) {
        if (debug) {
            Log.v(tag, msg);
        }
    }

    /**
     * log.d
     */
    public static void d(String tag, String msg) {
        if (debug) {
            Log.d(tag, msg);
        }
    }

    /**
     * log.e
     */
    public static void e(String tag, String msg) {
        if (debug) {
            Log.e(tag, msg);
        }
    }

    /**
     * log.error
     */
    public static void error(String tag, Exception e) {
        if (debug) {
            Log.e(tag, e.getMessage());
        }
    }

    /**
     * log.d
     */
    public void w(String tag, String msg) {
        if (debug) {
            Log.w(tag, msg);
        }
    }

    /**
     * set debug
     */
    public static void setDebug(boolean d) {
        debug = d;
    }
}
