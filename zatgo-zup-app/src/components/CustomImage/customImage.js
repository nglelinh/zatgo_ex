import React, {Component} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import PropTypes from 'prop-types';

/**
 * 自定义图片
 */

const MineAvatar = require('@zatgo/assets/common/defaultImage.png')

export default class CustomImage extends Component {
  static propTypes = {
    uri: PropTypes.string.isRequired, // 图片路径，必填
    errImage: PropTypes.number, // 加载错误图片，可不传
    defaultImage: PropTypes.number, // 预加载图片
    resizeMode: PropTypes.string
  };

  static defaultProps = {
    defaultImage: MineAvatar,
    errImage: MineAvatar, // 默认加载错误图片，可在此统一设置
    resizeMode:'stretch'
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoadComplete: true,
      isLoad: true,
      type: 0, // 0,正常加载，1加载错误，
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.uri !== nextProps.uri) {
      this.setState({
        isLoadComplete: true,
        isLoad: true,
        type: 0, // 0,正常加载，1加载错误，
      });
    }
  }

  render() {
    const {uri, defaultImage, errImage, style, defaultStyle,resizeMode} = this.props;
    let source = {uri};
    if (this.state.type === 1) {
      source = errImage;
    }
    return (
        <View style={[styles.imgDefault, style]}>
          {this.state.isLoad ? (
              <Image
                  resizeMode={resizeMode}
                  source={source}
                  style={[
                    styles.imgDefault,
                    {overflow: 'hidden', position: 'absolute'},
                    style,
                  ]}
                  onError={() => {
                    this.setState({
                      type: 1,
                      isLoad: false,
                      isLoadComplete: false
                    });
                  }}
                  onLoadEnd={() => {
                    this.setState({
                      isLoadComplete: true,
                    });
                  }}
              />
          ) : null}
          {this.state.isLoadComplete ? null : (
              <Image
                  style={[defaultStyle,{backgroundColor:'#f0f2f5'}]}
                  source={defaultImage}
                  resizeMode="contain"
              />
          )}
        </View>
    );
  }
}
const styles = StyleSheet.create({
  imgDefault: {
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
});
