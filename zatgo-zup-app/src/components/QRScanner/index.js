import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { RNCamera } from 'react-native-camera';
import { StyleSheet, View, Text } from 'react-native';
import QRScannerRectView from './QRScannerRectView';
import I18n from 'I18n';

/**
 * 扫描界面
 */
export default class QRScannerView extends PureComponent {
  static propTypes = {
    maskColor: PropTypes.string,
    borderColor: PropTypes.string,
    cornerColor: PropTypes.string,
    borderWidth: PropTypes.number,
    cornerBorderWidth: PropTypes.number,
    cornerBorderLength: PropTypes.number,
    rectHeight: PropTypes.number,
    rectWidth: PropTypes.number,
    isLoading: PropTypes.bool,
    isCornerOffset: PropTypes.bool,//边角是否偏移
    cornerOffsetSize: PropTypes.number,
    bottomMenuHeight: PropTypes.number,
    scanBarAnimateTime: PropTypes.number,
    scanBarColor: PropTypes.string,
    scanBarImage: PropTypes.any,
    scanBarHeight: PropTypes.number,
    scanBarMargin: PropTypes.number,
    hintText: PropTypes.string,
    hintTextStyle: PropTypes.object,
    hintTextPosition: PropTypes.number,
    renderTopBarView: PropTypes.func,
    renderBottomMenuView: PropTypes.func,
    isShowScanBar: PropTypes.bool,
    bottomMenuStyle: PropTypes.object,
    onScanResultReceived: PropTypes.func,
  };

  constructor(props) {
    super(props);
    //通过这句代码屏蔽 YellowBox
    console.disableYellowBox = true;
  }

  render() {
    const { renderTopBarView, renderBottomMenuView } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <RNCamera
          type={RNCamera.Constants.Type.back}
          onBarCodeRead={this.props.onScanResultReceived}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your camera phone'}
          style={{ flex: 1 }}
          notAuthorizedView={
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
              <Text>{I18n.t('scanner_no_authorized')}</Text>
            </View>
          }
        >
          {/*绘制顶部标题栏组件*/}
          {renderTopBarView && renderTopBarView()}

          {/*绘制扫描遮罩*/}
          <QRScannerRectView
            maskColor={this.props.maskColor}
            cornerColor={this.props.cornerColor}
            borderColor={this.props.borderColor}
            rectHeight={this.props.rectHeight}
            rectWidth={this.props.rectWidth}
            borderWidth={this.props.borderWidth}
            cornerBorderWidth={this.props.cornerBorderWidth}
            cornerBorderLength={this.props.cornerBorderLength}
            isLoading={this.props.isLoading}
            cornerOffsetSize={this.props.cornerOffsetSize}
            isCornerOffset={this.props.isCornerOffset}
            bottomMenuHeight={this.props.bottomMenuHeight}
            scanBarAnimateTime={this.props.scanBarAnimateTime}
            scanBarColor={this.props.scanBarColor}
            scanBarHeight={this.props.scanBarHeight}
            scanBarMargin={this.props.scanBarMargin}
            hintText={this.props.hintText}
            hintTextStyle={this.props.hintTextStyle}
            scanBarImage={this.props.scanBarImage}
            hintTextPosition={this.props.hintTextPosition}
            isShowScanBar={this.props.isShowScanBar}
          />

          {/*绘制底部操作栏*/}
          <View style={[styles.buttonsContainer, this.props.bottomMenuStyle]}>
            {renderBottomMenuView && renderBottomMenuView()}
          </View>

        </RNCamera>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  buttonsContainer: {
    position: 'absolute',
    height: 100,
    bottom: 0,
    left: 0,
    right: 0,
  },
});