/*
 *  @File   : index
 *  @Author : lsl
 *  @Date   : 2018-4-5 10:53:50
 *  @Last Modified   : 2018-4-5 10:53:50
 *  @Desc 数字输入框
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types'
import { View, Text, TextInput, StyleSheet } from 'react-native';
import I18n from 'I18n';
import { window } from '@zatgo/constants';

class NumberInput extends PureComponent {

  static propTypes = {
    editable: PropTypes.bool.isRequired,// 是否可编辑
  }

  state = {
    amount: '',
  }

  chkPrice(obj) {
    obj = obj.replace(/[^\d.]/g, "");
    //必须保证第一位为数字而不是.   
    obj = obj.replace(/^\./g, "");
    //保证只有出现一个.而没有多个.   
    obj = obj.replace(/\.{2,}/g, ".");
    //保证.只出现一次，而不能出现两次以上   
    obj = obj.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    return obj;
  }

  onChangeAmount = (value) => {
    let amount = this.chkPrice(value.replace(/\s+/g, ""));
    this.setState({ amount });

    const { onChangeText } = this.props;
    onChangeText && onChangeText(amount)
  }


  render() {
    return (
      <TextInput
        {...this.props}
        multiline={false}
        keyboardType="numeric"
        underlineColorAndroid="transparent"
        style={[styles.txtInput,
        {
          fontSize: this.props.fontSize ? this.props.fontSize : 14 * window.fontScale
        }]
        }
        value={this.props.editable ? this.state.amount : this.props.defaultValue}
        onChangeText={this.onChangeAmount}
      />
    );
  }
}

export default NumberInput;

const styles = StyleSheet.create({
  txtInput: {
    flex: 1,
    padding: 0,
    color: '#323B43',
    fontSize: 14 * window.fontScale
  },
})