import React from 'react';
import {
  Text,
  View,
  Image,
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Dimensions,
  ViewPropTypes,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import { window } from '../constants';
import { Actions } from 'react-native-router-flux';

const NAV_BAR_HEIGHT_ANDROID = 50;
const NAV_BAR_HEIGHT_IOS = 44;
const deviceW = Dimensions.get('window').width;
// 设置基准分辨率
const BASE_PIXEL_RATIO = 375;

const px2dp = (px) => {
  return px * deviceW / BASE_PIXEL_RATIO;
}

const StatusBarShape = {
  barStyle: PropTypes.oneOf(['light-content', 'default', 'dark-content']),
  hidden: PropTypes.bool,
  backgroundColor: PropTypes.string,
};

export default class NavigationBar extends React.PureComponent {

  static propTypes = {
    statusBar: PropTypes.shape(StatusBarShape),

    style: ViewPropTypes.style,
    title: PropTypes.string,
    titleView: PropTypes.element,

    showLeft: PropTypes.bool,
    leftText: PropTypes.string,
    leftTextStyle: ViewPropTypes.style,
    showleftImg: PropTypes.bool,
    leftButton: PropTypes.element,
    onLeftClick: PropTypes.func,

    showRight: PropTypes.bool,
    rightText: PropTypes.string,
    rightTextStyle: ViewPropTypes.style,
    rightButton: PropTypes.element,
    onRightClick: PropTypes.func,
    leftImg: PropTypes.number,
    rightImg: PropTypes.number
  };

  static defaultProps = {
    statusBar: {
      barStyle: 'default',
      hidden: false,
      backgroundColor: '#00000000',
    },
    showLeft: true,
    showleftImg: true,  //是否显示返回箭头
    leftText: '',   //返回键位置的文字
    showRight: false,
    rightText: '更多',
  };

  _renderLeft() {
    let { leftButton, leftTextStyle, showLeft, onLeftClick, leftText, showleftImg, leftImg } = this.props;
    if (!showLeft) {
      return null;
    }
    if (leftButton == null) {
      return (
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            if (onLeftClick) {
              onLeftClick();
            } else {
              Actions.pop();
            }
          }}>
          <View style={styles.leftContainer}>
            {
              showleftImg ?
                <Image source={leftImg}
                  style={{
                    width: px2dp(25),
                    height: px2dp(25)
                  }} />
                : null
            }
            <Text style={[styles.leftRightStyle, leftTextStyle]}>{leftText}</Text>
          </View>
        </TouchableOpacity>)
    }
    return leftButton;
  }

  _renderRight() {
    let { rightButton, rightTextStyle, showRight, onRightClick, rightText, rightImg } = this.props;
    if (!showRight) {
      return null;
    }
    if (rightButton == null) {
      return (
        <TouchableOpacity
            ref={(e)=>{this.ref=e}}
          activeOpacity={0.7}
          onPress={() => {
            if (onRightClick) {
              this.ref.measure((ox, oy, width, height, px, py) => {
                onRightClick({x: px, y: py, width: width, height: height})
              })
            }
          }}>
          <View style={styles.rightContainer}>
            {
              rightImg ?
                <Image source={rightImg}
                  style={{
                    width: px2dp(25),
                    height: px2dp(25)
                  }} />
                : null
            }
            {rightText?<Text style={[styles.leftRightStyle, rightTextStyle]}>{rightText}</Text>:null}
          </View>
        </TouchableOpacity>)
    }
    return rightButton;
  }

  render() {
    let leftButton = this._renderLeft();
    let rightButton = this._renderRight();
    let statusBar = <StatusBar {...this.props.statusBar} translucent={true} />;
    let titleView = this.props.titleView ? this.props.titleView :
      <Text style={[styles.titleStyle, this.props.titleLayoutStyle]}>{this.props.title}</Text>;
    let content =
      <View style={styles.content}>
        {leftButton}
        <View style={styles.titleView}>{titleView}</View>
        {rightButton}
      </View>;
    return (
      <View style={[styles.container, this.props.style]}>
        {statusBar}
        {content}
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    width: deviceW,
    justifyContent: 'center',
    paddingTop: window.statusBarHeight,
  },
  content: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    height: Platform.OS === 'ios' ? 44 : 50,
  },
  titleView: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 40,
    right: 40,
    top: 0,
    bottom: 0,
  },
  titleStyle: {
    fontSize: 18,
    fontWeight:'bold',
    // color: 'white'
  },
  leftRightStyle: {
    color: 'white',
    fontSize: 14
  },
  leftContainer: {
    marginLeft: px2dp(15),
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightContainer: {
    marginRight: px2dp(15),
    flexDirection: 'row',
    alignItems: 'center'
  },
});
