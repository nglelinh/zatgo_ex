/**
 * Created by zhoujianxin on 2018/10/29.
 * @Desc
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Modal, Image, TouchableOpacity} from 'react-native';
import {Grid} from 'antd-mobile';
import {window} from '@zatgo/constants';
import I18n from 'I18n';
import { UShare} from "../../constants";

const getSales = () => [
  {
    icon: require('@zatgo/assets/common/WeChat.png'),
    text: I18n.t('home_train'),
    shareType: '微信',
    shareName: 2
  },
  {
    icon: require('@zatgo/assets/common/Moments.png'),
    text: I18n.t('home_travel'),
    shareType: '朋友圈',
    shareName: 3
  }
]


const GridHeader = ({title}) => (
    <View style={styles.gridHeader}>
      <View style={styles.labeler}/>
      <Text style={styles.headerTxt}>
        {title}
      </Text>
    </View>
)

export default class index extends Component {
  state = {}

  onClose = () => {
    this.props.onClose && this.props.onClose();
  };

  onItemClick = (item) => {
    /**
     * 第三方分享
     * 参数：标题、分享内容、分享链接、图片、平台、分享结果回调
     */
    if (this.props.data) {
      UShare.shareImage(this.props.data, item.shareName,
          (message) => {
            // message: 分享成功、分享失败、取消分享
            // TODO ...
            console.log(message)
          });
    }
    this.props.onClose && this.props.onClose();
  }

  render() {
    // console.log(this.props.data)
    return (
        <View style={styles.container}>
          <Modal
              animationType="slide"
              transparent
              onClose={this.onClose}
              onShow={this.props.onOpen}
              maskClosable
              onRequestClose={this.onClose}
              visible={this.props.visible ? this.props.visible : false}

          >
            <TouchableOpacity activeOpacity={1}
                              style={{flex: 1}}
                              onPress={() => this.onClose()}>
            {/*<Image source={{uri: this.props.data ? this.props.data : ''}}*/}
                   {/*resizeMode="stretch"*/}
                   {/*style={{flex: 1, width: window.width, backgroundColor: 'white'}}/>*/}
            <View style={styles.actContainer}>
              <GridHeader title={I18n.t('dig_share')}/>
              <Grid data={getSales()}
                    columnNum={5}
                    hasLine={false}
                    renderItem={(item, index) =>
                        <View style={{alignItems: 'center'}}>
                          <Image source={item.icon} style={{width: 30, height: 30}}/>
                          <Text>{item.shareType}</Text>
                        </View>
                    }
                    onClick={this.onItemClick}
                    itemStyle={styles.gridItemStyle}
              />
              <TouchableOpacity onPress={() => this.onClose()}>
                <View style={{
                  borderTopWidth: 0.5,
                  borderTopColor: '#d4d4d4',
                  width: window.width,
                  height: 44,
                  alignItems: 'center',
                  justifyContent: 'center',
                  paddingBottom: window.isIphoneX ? 20 : 0
                }}><Text>{I18n.t('alert_cancel')}</Text></View>
              </TouchableOpacity>
            </View>
            </TouchableOpacity>
          </Modal>
        </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  actContainer: {
    borderRadius: 8,
    width: window.width,
    padding: 0,
    backgroundColor: 'white',
    // marginBottom: 40,
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
  },
  gridItemStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  gridHeader: {
    height: 40,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderBottomWidth: 0.5,
    borderBottomColor: '#d4d4d4',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  labeler: {
    width: 3,
    height: 15,
    marginLeft: 15,
    backgroundColor: '#4DCC7B',
    borderRadius: 2,
  },
  headerTxt: {
    fontSize: 14 * window.fontScale,
    color: '#323B43',
    marginLeft: 10
  }
});
