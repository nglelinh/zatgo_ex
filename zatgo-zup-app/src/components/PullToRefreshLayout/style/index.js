'use strict';

import {
  StyleSheet
} from 'react-native';
import window from '../../../constants/window';

export default StyleSheet.create({
    wrap: {
        flex: 1,
        flexGrow: 1,
        flexDirection: 'column',
        zIndex:-999,
    },
    hide: {
        position: 'absolute',
        left: 10000,
    },
    show: {
        position: 'relative',
        left: 0,
        paddingTop: window.isIphoneX?40:20
    }
});
