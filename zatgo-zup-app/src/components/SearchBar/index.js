/*
 *  @File   : index
 *  @Author : lsl
 *  @Date   : 2018-4-3 17:20:8
 *  @Last Modified   : 2018-4-3 17:20:8
 *  @Desc 搜索框
 */
import React from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
} from 'react-native';
import window from '@zatgo/constants/window';

const SearchBar = (props) => (
  <View style={styles.container}>
    <Image
      source={require('@zatgo/assets/common/com_inquire.png')}
      style={styles.searchIcon} />
    <TextInput
      {...props}
      placeholderTextColor='#A0A4A8'
      underlineColorAndroid="transparent"
      returnKeyType='done'
      style={styles.textInput}
    />
  </View>
)

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#F0F2F5',
    borderWidth: 1,
    backgroundColor: '#F0F2F5'
  },
  searchIcon: {
    width: 16,
    height: 16,
    marginLeft: 10,
    marginRight: 10
  },
  textInput: {
    flex: 1,
    padding: 0,
    height: 42,
    fontSize: 14*window.fontScale,
  }
});
