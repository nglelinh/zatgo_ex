import { NativeModules, Platform, Alert, Linking } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import I18n from 'I18n';
import { Actions } from 'react-native-router-flux';
import { Toast } from  'antd-mobile'

//图片选择器参数设置
const options = {
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};


// 图片选择器参数设置
const options2 = {
  title: '选择图片',
  cancelButtonTitle: '取消',
  takePhotoButtonTitle: '拍照',
  chooseFromLibraryButtonTitle: '选择照片',
  cameraType: 'back',
  mediaType: 'photo',
  videoQuality: 'high',
  durationLimit: 10,
  maxWidth: 300,
  maxHeight: 300,
  quality: 0.8,
  angle: 0,
  allowsEditing: false,
  noData: false,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

// 相册图片选择、识别
const onParseGalleryImage = (callBack = () => {}) => {
  ImagePicker.showImagePicker(options2, response => {
    if (response.didCancel) {
      // console.log('用户取消了选择！');
    } else if (response.error) {
      // alert("ImagePicker发生错误：" + response.error);
    } else if (response.customButton) {
      // alert("自定义按钮点击：" + response.customButton);
    } else {
      // console.log(JSON.stringify(response));
      let file;
      if (Platform.OS === 'android') {
        file = response.uri;
      } else {
        file = response.uri.replace('file://', '');
      }
      callBack(file);
      // request.uploadImage(response.uri',)
    }
  });
};

const handelResult = (events) => {
  Toast.hide();
  if (events == null) {
    Alert.alert('',
        I18n.t('alert_qrcode_error'),[{
        text: I18n.t('alert_ok'),}]);

  } else if (events.indexOf('channel') > -1 && events.indexOf('ZatGoPay') > -1 && events.indexOf('device') > -1) {
    const json = JSON.parse(events)
    if (json.device === 1) {
      // qrFrom 1pc收银台 2 收款码 3 提取
      Actions.replace('payment', { qrFrom: 1, qrData: json });
    } else if (json.device === 2 || json.device === 3) {
      Actions.replace('payment', { qrFrom: 2, qrData: json });
    }
  } else {
    Actions.replace('scanResult', { qrData: events })
  }
}

// 图片识别
const parseImage = (response) => {
  if (Platform.OS === "android") {
    NativeModules.ImageParseManager.parseImage(response.path, (data) => {
      handelResult(data);
    });
  } else {
    NativeModules.NativeManager.ImageSelectCallbackEventOne((response.uri), (error, data) => {
      handelResult(data);
    })
  }
};

// 相册图片选择、识别
const parseGalleryImage = () => {
  if(Platform.OS === "ios"){
    NativeModules.NativeManager.authStatus(2, (error, data) => {
      if(data!=='yes'){
        // console.log(data)
        Alert.alert('',
            I18n.t('alert_qrcode_permissions_photo'),[
              {
                text: I18n.t('alert_cancel')
              },{
              text: I18n.t('alert_ok'),onPress:()=>{Linking.openURL(data)}}
              ]);

        return;
      }
    })
  }
  ImagePicker.launchImageLibrary(options, (response) => {
    if (response.didCancel) {
      // console.log('用户取消了选择！');
    }
    else if (response.error) {
      // alert("ImagePicker发生错误：" + response.error);
    }
    else if (response.customButton) {
      // alert("自定义按钮点击：" + response.customButton);
    }
    else {
      Toast.loading(I18n.t('toast_processing'));
      parseImage(response);
    }
  })
};


export default {
  parseImage,
  parseGalleryImage,
  onParseGalleryImage
}