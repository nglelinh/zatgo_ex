/**
 * Created by zhoujianxin on 2018/12/28.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  // Modal,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from 'react-native';
import {Modal} from 'antd-mobile'
import WebView from '../WebView'
import window from "../../constants/window";
import I18n from 'I18n';

export default class index extends Component {
  state = {
    loading:false
  }

  onClose = () => {
    this.setState({
      loading:false
    })
    this.props.onClose && this.props.onClose();
  };

  render() {
    const {sliderStyle} = this.props;
    return (
          <Modal
              transparent
              onClose={this.onClose}
              maskClosable
              visible={this.props.visible ? this.props.visible : false}
              style={[{backgroundColor: 'transparent', width:window.width-80},sliderStyle]}
          >
            <View style={styles.actContainer}>
              <View style={{width:window.width - 120,height: 50,borderRadius: 10}}>
                <WebView
                    originWhitelist={['*']}
                    ref={(webview) => {
                      this.web = webview
                    }}
                    style={{width: window.width - 120, height: 44, borderRadius: 10}}
                    onMessage={(event) => {
                      if (event && event.nativeEvent && event.nativeEvent.data) {
                        console.log(JSON.parse(event.nativeEvent.data))
                        this.props.onSubmit && this.props.onSubmit(JSON.parse(event.nativeEvent.data))
                        this.onClose()
                      }
                    }}
                    startInLoadingState
                    source={{uri: global.sliderH5}}
                    // source={require('./NCH5.html')}
                    bounces={false}
                    javaScriptEnabled={true}
                    androiddomStorageEnabled={false}
                    renderError={() => {
                      // console.log('renderError')
                      return <View><Text>出现错误</Text></View>
                    }}
                    onLoadEnd={() => {
                      console.log('end'), this.setState({loading: true})
                    }}
                />
              </View>
            </View>
        </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems:'center',
    justifyContent:'center'
  },
  actContainer: {
    minWidth: window.width - 110,
    height:54,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'white',
    borderRadius: 10
  },
});