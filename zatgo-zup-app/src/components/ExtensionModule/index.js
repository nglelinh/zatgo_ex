import { NativeModules, Platform } from 'react-native';
import { constDefines } from '../../constants';

const GetQueryString = (urlString,paraName)=> {
  var url = urlString;
  var arrObj = url.split("?");

  if (arrObj.length > 1) {
    var arrPara = arrObj[1].split("&");
    var arr;

    for (var i = 0; i < arrPara.length; i++) {
      arr = arrPara[i].split("=");

      if (arr != null && arr[0] == paraName) {
        return arr[1]==='true'?'yes':'no';
      }
    }
    return "";
  }
  else {
    return "";
  }
}

const queryString = (url, params) => {
  if (params) {
    let paramsArray = [];
    Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
    if (url.search(/\?/) === -1) {
      url += '?' + paramsArray.join('&')
    } else {
      url += '&' + paramsArray.join('&')
    }
  }
  return url;
}

const saveTokenNative = (data) => {
  if (Platform.OS === "android") {
    NativeModules.ExtensionModule.saveData(constDefines.nativePersistKey, data);
  }
}

const removeTokenNative = () => {
  if (Platform.OS === "android") {
    NativeModules.ExtensionModule.removeData(constDefines.nativePersistKey);
  }
}

const startWebViewPage = (params) => {
  if (Platform.OS === "android") {
    NativeModules.ExtensionModule.startActivityFromJS("com.zatgo.WebViewAndroidActivity", params);
  }
}

// 唤起收银台页面
const startZatGoPayPage = (params) => {
  if (Platform.OS === "android") {
    NativeModules.ExtensionModule.startZatGoPayPage(params);
  }
}

const getDataFromIntent = (success, err) => {
  if (Platform.OS === "android") {
    NativeModules.ExtensionModule.getDataFromIntent(success, err);
  }
}

export default {
  saveTokenNative,
  removeTokenNative,
  startWebViewPage,
  getDataFromIntent,
  startZatGoPayPage,
  GetQueryString,
  queryString
}