// export const baseUrl = 'http://work.flybycloud.com:6671'//199
// export const baseUrl = 'http://work.flybycloud.com:6674'// 线下37
// export const feiba_h5 = 'http://wx.flybytrip.com'// 线上
// export const feiba_signlogin = 'http://mapi.flybycloud.com:8091/h5/auth/signLogin'// 线上
// export const feiba_h5 = 'http://work.flybycloud.com:1013'// 线下
// export const feiba_signlogin = 'http://work.flybycloud.com:8023/h5/auth/signLogin'// 线下

export const productionUrl = 'https://zup.zatgo.net'// 线上,仅区分环境用

// auth
export const auth_login = '/auth/login'
export const auth_logout = '/auth/logout'
export const auth_usernameExist = '/user/exist'
export const auth_sendPhoneAuthCode = '/auth/sendPhoneAuthCode'
export const auth_sendEmailAuthCode = '/auth/sendEmailAuthCode'
export const auth_verifyAuthCode = '/auth/verifyAuthCode'
export const auth_register = '/auth/register'

// user 
export const user_exist = '/user/exist'
export const user_forgetLoginPassword = '/user/forgetLoginPassword'
export const user_byname = '/user/basicbyname'
export const user_byid = '/user/basicbyid'
export const user_basicInfo = '/user/basicInfo'
export const user_certificateInfo = '/user/certificate/basicInfo'
export const user_editLoginPassword = '/user/editLoginPassword'
export const user_editPayPassword = '/user/editPayPassword'
export const user_forgetPayPassword = '/user/forgetPayPassword'
export const user_uploadImage = '/suanli/oss/upload/file'
export const user_updateBasicInfo = '/user/update/basicInfo'
export const user_updateCertificateInfo = '/user/certificate/update'


// wallet
export const wallet_account_list = '/wallet/account/list'
export const wallet_payment_account = '/wallet/payment/account/pageinfo/'
export const wallet_payment_list = `/wallet/payment/list/pageinfo/`
export const wallet_extract_record_list = `/wallet/extract/record/list`
export const wallet_deposit_record_list = `/wallet/deposit/record/list`
export const wallet_order_record_list = '/pay/orders/pageinfo'
export const wallet_system_cointypes = `/wallet/system/cointypes`
export const wallet_pay = '/wallet/pay'
export const wallet_transfer = '/wallet/transfer'
export const wallet_extract = '/wallet/extract/add'
export const wallet_address_deposit = '/wallet/address/deposit'
export const wallet_account_create = '/wallet/account/create'
export const wallet_account_primary = '/wallet/account/primary'

// pay
export const pay_orderpay = '/pay/orderpay'
export const pay_check_PrepayId = '/pay/orders/check/prepay/'
// sys
export const sys_audit_version = '/user/system/app/version'

//dig
export const dig_premininginfo = '/mining/preMiningInfo'
export const dig_miningrecord = '/mining/miningRecord'
export const dig_getcomputepowerrecord = '/mining/getComputePowerRecord'
export const dig_usercomputepowertop = '/mining/userComputePowerTop'
export const dig_mining = '/mining/mining'
export const dig_task = '/mining/task/list/taskData'
export const dig_login_task = '/mining/task/loginTaskDone'
export const dig_invite_task = '/user/invite/list'

//application
export const application_list = '/pay/merchant/app/list'//(获取app应用表)
export const banner_list = '/user/system/carouselImage/list'//(获取banner)
// 优惠券
export const coupon_list = '/coupons/user/getUserCouponsList'