import { Dimensions, PixelRatio } from 'react-native';

const fontScale = 1 / PixelRatio.getFontScale()
// color
const color = {
  text: '#323B43',
  lightText: 'white',
  bgColor: '#f0f2f5'
};

// font sizes
const fontSize = {
  xsmall: 12 / fontScale,
  small: 14 / fontScale,
  default: 17 / fontScale,
  large: 24 / fontScale,
  xlarge: 32 / fontScale,
};

export default {
  color,
  fontSize,
}