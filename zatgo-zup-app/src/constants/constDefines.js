import { Platform } from 'react-native';
/**
 *  app类型
 */
const appType = Platform.OS === 'ios' ? 2 : 1;

const storeAuthInfoKey = 'store_auth_info_key';
const nativePersistKey = 'native_persist_key';

const defaultLanguageKey = "default_language_key"

const deployStagingKey = Platform.OS === 'ios' ? '6E_3q0a9S4xK4qxO1_PxFuMgII1mc8e0a77e-2ef1-426f-85f8-fe97c0c795b2' : "dc024sxWjlMc0iDc6oxIEQXj5CK3f5918f45-0e04-4b6d-a076-dfba6b288407"
const deployProductionKey = Platform.OS === 'ios' ? 'WWNq6IGCDw1UZXEXUsdSwXojXkdjc8e0a77e-2ef1-426f-85f8-fe97c0c795b2' : "iOEPAWzAwq9rIyS5efA8gKjJ9Maof5918f45-0e04-4b6d-a076-dfba6b288407"

const defaultTimeout = 30000

export default {
  appType,
  storeAuthInfoKey,
  defaultLanguageKey,
  deployStagingKey,
  deployProductionKey,
  defaultTimeout,
  nativePersistKey,
}