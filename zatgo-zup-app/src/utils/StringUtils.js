/*科学计数法转换数值*/
function formatE(num) {
  var str = num.toString();
  // var reg = /^[-+]?(\d+)(e)([\-]?\d+)$/;
  var reg = /^[-+]?(\d+)(e)([\-+]?\d+)$/;
  var arr, len, zero = '';

  /*6e7或6e+7 都会自动转换数值*/
  if (!reg.test(str)) {
    return num;
  } else {
    /*6e-7 需要手动转换*/
    arr = reg.exec(str);
    // console.log('测试', arr)
    if (Math.abs(Number(num)) < 1) {
      len = Math.abs(arr[3]) - 1;
      for (var i = 0; i < len; i++) {
        zero += '0';
      }

      return num > 0 ? '0.' + zero + arr[1] : '-0.' + zero + arr[1];
    } else {
      len = Math.abs(arr[3]);
      for (var i = 0; i < len; i++) {
        zero += '0';
      }
      return num > 0 ? arr[1] + zero : '-' + arr[1] + zero;
    }
  }
}

export default {
  formatE
}