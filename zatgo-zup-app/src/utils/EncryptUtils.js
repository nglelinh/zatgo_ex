import MD5 from 'md5'
import JSEncrypt from 'jsencrypt'

const publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCcYh1x2qXbimdx"
  + "cu/S4ClyjMksf9bxGYMHgXVfpxC+7AyeGTB+mHq1eZr+ps8kBVH7t9+ZROSm8QoU0d0aIm5cs+SY5"
  + "PhjwqmSRw1LpQFVP41Gb8nvErXXpfs29aWNDID8B5fMglNd39wBWvX07qyVPzDYQs6I7LESK/ggIY"
  + "bgnQIDAQAB"

const zatPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCLV0cwstLPRldLKn4k"
  + "s3eUuNmz4ybvNuwKfUM1o9ACpR+YyULXqtA4BV1TsmM8KaK9HJDmOEj8U"
  + "jgAUVCfu5+ZFhR81K+8Q6YuiFzdcWe3mGQP9rJk/hIExs5dSXW1g+o2rXj"
  + "zw+qFO8OUCzsbRGVP+wLqLwnW4hKbLNOdT3tr6wIDAQAB";

const sign = '7c195566331c4cbd86ef38d5e3a845a1'
const getTs = ()=>{
  return (new Date()).valueOf();
}

export default class EncryptUtils {

  static rsa(str) {//rsa加密
    const jsencrypt = new JSEncrypt()
    jsencrypt.setKey(global.devEnv ? publicKey : zatPublicKey)
    return jsencrypt.encrypt(str)
  }

  static md5(str) {
    return MD5(str)
  }

  static webMd5(){
    let str = getTs()+'_'+MD5(getTs()+sign);
    return str
  }
}