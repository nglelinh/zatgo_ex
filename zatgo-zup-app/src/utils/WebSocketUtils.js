/**
 *
 * Created by zhoujianxin on 2018/4/24.
 */
import PushNotification from 'react-native-push-notification';
import I18n from 'I18n';
import { Actions } from 'react-native-router-flux';

var lockReconnect = false;  //避免ws重复连接
var closeSign = false;
var ws = null;

PushNotification.configure({

  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function (token) {
    console.log('TOKEN:', token);
  },

  // (required) Called when a remote or local notification is opened or received
  onNotification: function (notification) {
    console.log('NOTIFICATION:', notification);
    Actions.paymentRecords()
  },

  // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
  senderID: "YOUR GCM SENDER ID",

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   */
  requestPermissions: true,
});

//心跳检测
var heartCheck = {
  timeout: 60 * 1000,        //60秒发一次心跳
  timeoutObj: null,
  serverTimeoutObj: null,
  reset: function () {
    clearTimeout(this.timeoutObj);
    clearTimeout(this.serverTimeoutObj);
    return this;
  },
  start: function () {
    var self = this;
    this.timeoutObj = setTimeout(function () {
      //这里发送一个心跳，后端收到后，返回一个心跳消息，
      //onmessage拿到返回的心跳就说明连接正常
      try {
        ws.send("ping");
      } catch (err) {
        console.log('Network anomalies')
      }
      console.log("ping!")
      self.serverTimeoutObj = setTimeout(function () {//如果超过一定时间还没重置，说明后端主动断开了
        // closeSign = true
        ws.close();     //如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
      }, self.timeout)
    }, this.timeout)
  }
}

export default class WebSocketUtils {
  static notifWebSocket = (token) => {
    try {
      let temp = "";
      let url = "";
      if(global.serverUrl.indexOf("https://")!=-1){
        temp = "wss";
        url = global.serverUrl.replace('https://', '');
      }else {
        temp = "ws";
        url = global.serverUrl.replace('http://', '');
      }
      ws = new WebSocket(`${temp}://${url}/ws/wallet/appwebsocket/${token}`)
      initEventHandle(token);
    } catch (e) {
      reconnect(token);
      console.log(e);
    }
  }

  static closeWebSocket = () => {
    try {
      closeSign = true
      console.log('关闭')
      ws.close()
    } catch (err) {

    }
  }
}
function initEventHandle(wsUrl) {
  ws.onclose = function () {
    if (!closeSign) {
      reconnect(wsUrl);
      console.log("连接关闭!" + new Date().toUTCString());
    } else {
      console.log("连接关闭2!" + new Date().toUTCString());
      closeSign = false
    }
  };
  ws.onerror = function () {
    reconnect(wsUrl);
    console.log("连接错误!");
  };
  ws.onopen = function () {
    heartCheck.reset().start();      //心跳检测重置
    console.log("连接成功!" + new Date().toUTCString());
  };
  ws.onmessage = function (event) {    //如果获取到消息，心跳检测重置
    heartCheck.reset().start();      //拿到任何消息都说明当前连接是正常的
    console.log(event.data)
    if (isJSON(event.data)) {
      var received_msg = JSON.parse(event.data);
      console.log(received_msg)
      if (received_msg != '') {
        PushNotification.localNotificationSchedule({
          message: `${I18n.t('home_collection')}:${received_msg.amount}  ${received_msg.coinType?received_msg.coinType:''}`,// (required)
          date: new Date(Date.now() + (1 * 1000)), // in 60 secs
        });
      }
    }
  }
}
function isJSON(str) {
  if (typeof str == 'string') {
    try {
      JSON.parse(str);
      return true;
    } catch (e) {
      return false;
    }
  }
  console.log('It is not a string!')
}

function reconnect(url) {
  if (lockReconnect) return;
  lockReconnect = true;
  setTimeout(function () {     //没连接上会一直重连，设置延迟避免请求过多
    WebSocketUtils.notifWebSocket(url);
    lockReconnect = false;
  }, 3000);
}

