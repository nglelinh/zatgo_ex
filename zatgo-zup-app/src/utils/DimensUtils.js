import {
  PixelRatio,
  Dimensions
} from 'react-native';
//  mdpi for android
const PIXEL_RATIO_160_1 = 1;
// hdp from android
const PIXEL_RATIO_240_1d5 = 1.5;
// iphone4, 4s, 5, 5c, 5s, 6, 7; xhdpi from android
const PIXEL_RATIO_320_2 = 320;
// iphone6p, 7p; xxhdpi for android,1080p
const PIXEL_RATIO_480_3 = 480;
// larger from android
const PIXEL_RATIO_560_3x5 = 3.5;

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

// 设置基准分辨率
const BASE_PIXEL_RATIO = 375;

// 根据密度适配不同的分辨率,参数为dp
const getDpScale = (length) => {
  // 获取密度
  let ratio = PixelRatio.get();
  if (length == null) {
    length = 0;
  }

  return SCREEN_WIDTH < BASE_PIXEL_RATIO ? parseInt(length / (BASE_PIXEL_RATIO / SCREEN_WIDTH)) : length;
}

export default {
  getDpScale,
}