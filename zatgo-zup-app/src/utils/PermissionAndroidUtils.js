import { PermissionsAndroid, Platform } from 'react-native';

export default class CheckPermissionsAndroid {

  static async requestCarmeraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("你已获取了相机权限")
      } else {
        console.log("获取相机失败")
      }
    } catch (err) {
      console.log(err.toString())
    }
  }

  static checkPermission() {
    try {
      //返回Promise类型
      const granted = PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
      )
      granted.then((data) => {
        console.log(data)
      }).catch((err) => {
        console.log(err.toString())
      })
    } catch (err) {
      console.log(err.toString())
    }
  }

  static async requestMultiPermission() {
    if (Platform.OS === 'android') {
      try {
        const permissions = [
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.CAMERA,
        ]

        const grantWrite = await PermissionsAndroid.check(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
        )
        const grantCamera = await PermissionsAndroid.check(
          PermissionsAndroid.PERMISSIONS.CAMERA
        )

        if (grantWrite && grantCamera) {
          return true;
        } else {
          //返回得是对象类型
          const result = await PermissionsAndroid.requestMultiple(permissions);
          const granteds = Object.values(result);

          const isGrant = granteds.every((granted) => {
            return granted === PermissionsAndroid.RESULTS.GRANTED || granted === true;
          })
          return isGrant;
        }
      } catch (err) {
        console.log(err.toString())
      }
    } else {
      return true;
    }
  }
}
