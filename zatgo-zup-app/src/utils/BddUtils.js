import moment from 'moment';
import EncryptUtils from './EncryptUtils';
import { constDefines } from '../constants';

const reqHeaders = {
  'Content-Type': 'application/json',
  'appId': '111',
  'appVersion': '1.0.1',
  'appType': '0',
}


/**
 * 时间戳
 */
const getTs = () => {
  return moment().format('YYYY-MM-DD HH:mm:ss')
}

/**
 * 签名
 * @param {接口名} api 
 * @param {请求参数} params 
 */
const getSign = (api, params) => {
  let sign = api
  let allParams = params ? Object.assign(params, reqHeaders) : reqHeaders
  for (let key in allParams) {
    if ((allParams[key] instanceof Object) || (allParams[key] instanceof Array)) {
      allParams[key] = ''
    }
    let value = key + '=' + allParams[key]
    sign += value
  }
  return EncryptUtils.md5(sign)
}


// 模拟abort
const abortablePromise = (requestPromise, timeout = 30000) => {
  let timeoutAction = null;
  const timerPromise = new Promise((resolve, reject) => {
    timeoutAction = () => {
      reject({ code: '9999', message: 'Request timeout' });
    }
  })
  setTimeout(() => { timeoutAction() }, timeout)
  return Promise.race([requestPromise, timerPromise]);
}

// 带超时效果的fetch
const fetchTimeout = (request, timeout = constDefines.defaultTimeout) => {
  return new Promise((resolve, reject) => {
    abortablePromise(request, timeout)
      .then(response => response.json())
      .then(json => {
        console.log('币多多返回', JSON.stringify(json))
        if (json.message !== null && json.message === "success") {//正常返回
          resolve(json)
        } else {
          reject(json)
        }
      })
      .catch(error => {
        console.log('币多多错误', JSON.stringify(error))
        reject({ code: error.code, message: error.message });
      });
  });
}

export default class BddUtils {

  /**
  *
  * @param api 接口名称
  * @param params 提交参数
  * @returns {Promise.<U>|Promise.<T>}
  */
  static post = (api, params) => {
    console.log('post路径：' + api);
    console.log('参数：' + JSON.stringify(params));

    const httpPost = fetch(api, {
      method: 'POST',
      headers: reqHeaders,
      body: JSON.stringify(params),
    });
    return fetchTimeout(httpPost);
  }
}

