import moment from 'moment';
import {NetInfo, Platform} from 'react-native'
import {Actions} from 'react-native-router-flux';
import DeviceInfo from 'react-native-device-info';
import StorageUtils from "./StorageUtils";
import WebSocketUtils from './WebSocketUtils';
import EncryptUtils from './EncryptUtils';
import {constDefines, api} from '../constants';
import I18n from 'I18n';

// const baseUrl = api.baseUrl// 线上
const cloudUserId = '?cloudUserId=65c2c3a074774f8090a1e4544a56632a';

const reqHeaders = {
  'Content-Type': 'application/json',
  'appId': '111',
  'appVersion': DeviceInfo.getVersion(),
  'appType': (Platform.OS === 'ios') ? '0' : '1',// 0 IOS, 1 Android
  'appSign': '7c195566331c4cbd86ef38d5e3a845a1',
  'deviceNo': DeviceInfo.getUniqueID()
}

/**
 * 时间戳
 */
const getTs = () => {
  return moment().format('YYYY-MM-DD HH:mm:ss')
}

/**
 * 签名
 * @param {接口名} api
 * @param {请求参数} params
 */
const getSign = (api, params) => {
  let sign = api
  let allParams = params ? Object.assign(params, reqHeaders) : reqHeaders
  for (let key in allParams) {
    if ((allParams[key] instanceof Object) || (allParams[key] instanceof Array)) {
      allParams[key] = ''
    }
    let value = key + '=' + allParams[key]
    sign += value
  }
  return EncryptUtils.rsa(EncryptUtils.md5(sign));
}


let token;
let language = I18n.getLanguages === 'en' ? 1 : 0;

export const configToken = (param) => {
  console.log('设置token', param)
  token = param;
}


export const configLanguage = (param) => {
  console.log('设置token', param)
  language = param === 'en' ? 1 : 0;
}

// query 参数拼接
const queryString = (url, params) => {
  if (params) {
    let paramsArray = [];
    Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
    if (url.search(/\?/) === -1) {
      url += '?' + paramsArray.join('&')
    } else {
      url += '&' + paramsArray.join('&')
    }
  }
  return url;
}


// 模拟abort
const abortablePromise = (requestPromise, timeout = 30000) => {
  let timeoutAction = null;
  const timerPromise = new Promise((resolve, reject) => {
    timeoutAction = () => {
      reject({code: '99999', message: I18n.t('request_timeout')});
    }
  })
  setTimeout(() => {
    timeoutAction()
  }, timeout)
  return Promise.race([requestPromise, timerPromise]);
}

// 带超时效果的fetch
const fetchTimeout = (request, timeout = constDefines.defaultTimeout) => {
  return new Promise((resolve, reject) => {
    abortablePromise(request, timeout)
      .then(response => {
        // console.log('错误lll:', JSON.stringify(response))
        return response.json();
      })
      .then(json => {
        if (json.code !== null && json.code === '0000') {//正常返回
          resolve(json)
        } else {
          if (json.code && I18n.t('system_error')[json.code]) {
            reject({code: json.code, message: I18n.t('system_error')[json.code]});
            if (json.code !== null && (json.code === '10015' || json.code === '1004')) {
              StorageUtils.clear();
              WebSocketUtils.closeWebSocket()
              Actions.reset('login');
            }
          }else {
            reject({code: '9999', message: null});
          }
          }
        })
        .catch(error => {
          console.log('错误', JSON.stringify(error))
          if (error.code && I18n.t('system_error')[error.code]) {
            reject({code: error.code, message: I18n.t('system_error')[error.code]});
          } else {
            NetInfo.isConnected.fetch().then(() => {
              try {
                NetInfo.isConnected.fetch().then(isConnected => {
                  if (isConnected) {
                    reject({code: '9999', message: null});
                  } else {
                    reject({code: '9999', message: I18n.t('network_error')});
                  }
                });
              } catch (error) {

              }
            })
          }
        });
  });
}

export default class HttpUtils {


  /**
   *
   * @param api 接口名称
   * @param params 提交参数
   * @returns {Promise.<U>|Promise.<T>}
   */
  static get = (api, params) => {
    if(token){
      reqHeaders.token = token;
    }else {
      delete reqHeaders['token']
    }
    reqHeaders.language = language;

    // reqHeaders.sign = getSign(api, params);
    // console.log('reqHeaders：', JSON.stringify(reqHeaders));
    let url = queryString(`${global.serverUrl}${api}${cloudUserId}`, params)
    console.log('get路径：' + url);
    reqHeaders['Content-Type'] = 'application/json';

    const httpGet = fetch(url, {
      method: 'GET',
      headers: reqHeaders,
    });

    return fetchTimeout(httpGet)
  }

  /**
   *
   * @param api 接口名称
   * @param params 提交参数
   * @returns {Promise.<U>|Promise.<T>}
   */
  static post = (api, params) => {
    if(token){
      reqHeaders.token = token;
    }else {
      delete reqHeaders['token']
    }
    reqHeaders.language = language;
    // reqHeaders.sign = getSign(api, params);
    // console.log('postreqHeaders：', JSON.stringify(reqHeaders));
    console.log('post路径：' + `${global.serverUrl}${api}`);
    // console.log('参数：' + JSON.stringify(params));
    reqHeaders['Content-Type'] = 'application/json';

    const httpPost = fetch(`${global.serverUrl}${api}${cloudUserId}`, {
      method: 'POST',
      headers: reqHeaders,
      body: JSON.stringify(params),
    });

    return fetchTimeout(httpPost);
  }

  /**
   *
   * @param api 接口名称
   * @param params 提交参数
   * @returns {Promise.<U>|Promise.<T>}
   */
  static put = (api, params) => {
    reqHeaders.token = token;
    reqHeaders.language = language;
    // reqHeaders.sign = getSign(api, params);
    let url = queryString(`${global.serverUrl}${api}${cloudUserId}`, params)
    console.log('putHeaders：', JSON.stringify(reqHeaders));
    console.log('put路径：' + url);
    reqHeaders['Content-Type'] = 'application/json';

    const httpPut = fetch(url, {
      method: 'PUT',
      headers: reqHeaders,
    });
    return fetchTimeout(httpPut);
  }

  /**
   *
   * @param api 接口名称
   * @param params 提交参数
   * @returns {Promise.<U>|Promise.<T>}
   */
  static delete = (api, params) => {
    reqHeaders.token = token;
    reqHeaders.language = language;
    // reqHeaders.sign = getSign(api, params);
    let url = queryString(`${global.serverUrl}${api}${cloudUserId}`, params)
    console.log('deleteHeaders：', JSON.stringify(reqHeaders));
    console.log('delete路径：' + url);
    reqHeaders['Content-Type'] = 'application/json';
    const httpDelete = fetch(url, {
      method: 'DELETE',
      headers: reqHeaders,
    });
    return fetchTimeout(httpDelete);
  }


  static uploadImage = (apiName, params) => {
    reqHeaders.token = token;
    reqHeaders.language = language;
    console.log('reqHeaders' + JSON.stringify(reqHeaders));
    // console.log('params'+JSON.stringify(params));
    // console.log(`URL:${global.serverUrl}${apiName}${cloudUserId}`);
    const formData = new FormData();
    const file = {
      uri: params.path,
      type: 'application/octet-stream',
      name: 'icon.jpg',
    };
    formData.append('files', file);
    formData.append('ossBucket', 'user');
    reqHeaders['Content-Type'] = 'multipart/form-data;charset=utf-8';
    const uploadRequest = fetch(`${global.serverUrl}${apiName}${cloudUserId}`, {
      method: 'POST',
      headers: reqHeaders,
      body: formData,
    });
    return fetchTimeout(uploadRequest);
  };

}

