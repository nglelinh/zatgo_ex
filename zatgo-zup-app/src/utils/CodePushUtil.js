import CodePush from "react-native-code-push";
import constDefines from '../constants/constDefines';


export default class CodePushUtil {
  static checkForUpdate = (deployKey) => {
    CodePush.checkForUpdate(deployKey).then((update) => {
      if (update) {
        CodePush.sync({
          deploymentKey: deployKey,
          updateDialog: false,
          installMode: CodePush.InstallMode.IMMEDIATE
        });
      }
    });
  }
}
