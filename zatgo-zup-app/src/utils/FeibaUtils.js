import moment from 'moment';
import EncryptUtils from './EncryptUtils';
import DeviceInfo from 'react-native-device-info';
import { constDefines } from '../constants';

const reqHeaders = {
  'Content-Type': 'application/json',
  'appId': '111',
  'appVersion': '1.0.1',
  'appType': '0',
  'appSign': '7c195566331c4cbd86ef38d5e3a845a1',
  'deviceNo': DeviceInfo.getUniqueID()


}


/**
 * 时间戳
 */
const getTs = () => {
  return moment().format('YYYY-MM-DD HH:mm:ss')
}

/**
 * 签名
 * @param {接口名} api 
 * @param {请求参数} params 
 */
const getSign = (api, params) => {
  let sign = api
  let allParams = params ? Object.assign(params, reqHeaders) : reqHeaders
  for (let key in allParams) {
    if ((allParams[key] instanceof Object) || (allParams[key] instanceof Array)) {
      allParams[key] = ''
    }
    let value = key + '=' + allParams[key]
    sign += value
  }
  return EncryptUtils.md5(sign)
}


// 模拟abort
const abortablePromise = (requestPromise, timeout = 30000) => {
  let timeoutAction = null;
  const timerPromise = new Promise((resolve, reject) => {
    timeoutAction = () => {
      reject({ code: '9999', message: 'Request timeout' });
    }
  })
  setTimeout(() => { timeoutAction() }, timeout)
  return Promise.race([requestPromise, timerPromise]);
}

// 带超时效果的fetch
const fetchTimeout = (request, timeout = constDefines.defaultTimeout) => {
  return new Promise((resolve, reject) => {
    abortablePromise(request, timeout)
      .then(response => response.json())
      .then(json => {
        console.log('飞巴返回', JSON.stringify(json))
        if (json.errorCode !== null && Number(json.errorCode) === 0) {//正常返回
          resolve(json)
        } else {
          reject(json)
        }
      })
      .catch(error => {
        console.log('飞巴错误', JSON.stringify(error))
        reject({ code: error.code, message: error.message });
      });
  });
}

export default class FeibaUtils {

  /**
  *
  * @param api 接口名称
  * @param params 提交参数
  * @returns {Promise.<U>|Promise.<T>}
  */
  static post = (api, params) => {
    reqHeaders.ts = getTs()
    reqHeaders.sign = getSign();
    console.log('post路径：' + api);
    console.log('参数：' + JSON.stringify(params));

    const httpPost = fetch(api, {
      method: 'POST',
      headers: reqHeaders,
      body: JSON.stringify(params),
    });
    return fetchTimeout(httpPost);
  }
}

