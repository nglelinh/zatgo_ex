import { call, put, take, takeLatest, fork } from 'redux-saga/effects'
import { Actions } from 'react-native-router-flux'
import { Toast } from 'antd-mobile'
import { HttpUtils } from "../../utils"
import { walletActions } from '../actions'
import { api } from '../../constants'
import I18n from 'I18n'

function* getAccountList() {
  try {
    const response = yield call(HttpUtils.get, api.wallet_account_list)
    yield put(walletActions.getAccountsSuccess(response.data))
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
    yield put(walletActions.getAccountsFail())
  }
}

/**
 * 账户交易记录
 * @param {*} action 
 */
function* getAccountPayment(action) {
  try {
    if (action.payload.pageNo < 2) {
      yield put(walletActions.getAccountPaymentSuccess([]));
      Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    }
    const url = `${api.wallet_payment_account}${action.payload.id}/${action.payload.pageNo}/${20}`
    const response = yield call(HttpUtils.get, url)
    console.log(response)
    if (action.payload.pageNo < 2) {
      yield put(walletActions.getAccountPaymentSuccess(response.data.list))
    } else if (response.data.list.length > 0 && (action.payload.pageNo * 20) < response.data.total) {
      yield put(walletActions.getAccountPaymentLoadMore(response.data.list))
    } else {
      yield put(walletActions.getAccountPaymentNoMore(response.data.list))
    }
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

/**
 * 用户交易记录
 * @param {*} action 
 */
function* getPaymentList(action) {
  try {
    if (action.payload.pageNo < 2) {
      yield put(walletActions.getPaymentListSuccess([]));
      Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    }
    const url = `${api.wallet_payment_list}${action.payload.pageNo}/${20}`
    const response = yield call(HttpUtils.get, url)
    if (action.payload.pageNo < 2) {
      yield put(walletActions.getPaymentListSuccess(response.data.list))
    } else if (response.data.list.length > 0 && (action.payload.pageNo * 20) < response.data.total) {
      yield put(walletActions.getPaymentListLoadMore(response.data.list))
    } else {
      yield put(walletActions.getPaymentListNoMore(response.data.list))
    }
    Toast.hide();
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

/**
 * 提取记录
 */
function* getExtractRecord() {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const url = api.wallet_extract_record_list
    const response = yield call(HttpUtils.get, url)
    yield put(walletActions.getExtractRecordSuccess(response.data))
    Toast.hide();
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

/**
 * 充值记录
 */
function* getDepositRecord() {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const url = api.wallet_deposit_record_list
    const response = yield call(HttpUtils.get, url)
    yield put(walletActions.getDepositRecordSuccess(response.data))
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

/**
 * 订单记录
 */
function* getOrderRecord(action) {
  try {

    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const url = `${api.wallet_order_record_list}/${action.payload.pageNo}/${20}`
    const response = yield call(HttpUtils.get, url)
    if (action.payload.pageNo < 2) {
      yield put(walletActions.getOrderRecordSuccess(response.data.list))
    } else if (response.data.list.length > 0 && (action.payload.pageNo * 20) < response.data.total) {
      yield put(walletActions.getOrderRecordLoadMore(response.data.list))
    } else {
      yield put(walletActions.getOrderRecordNoMore(response.data.list))
    }
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
    yield put(walletActions.getOrderRecordFail())

  }
}

/**
 * 支持币种
 */
function* getCoinTypes() {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const url = api.wallet_system_cointypes
    const response = yield call(HttpUtils.get, url)
    yield put(walletActions.getCointypesSuccess(response.data))
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

/**
 * 钱包支付
 * @param {*} action 
 */
function* walletPay(action) {
  try {
    Toast.loading(`${I18n.t('toast_pay')}...`, 0);
    const response = yield call(HttpUtils.post, api.wallet_pay, action.payload)
    yield put(walletActions.walletPaySuccess(response.data))
    Toast.hide()
    Actions.replace('paymentSuccess', { result: action.payload });
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 3);
    yield put(walletActions.walletPayFail())
  }
}

/**
 * 钱包转账
 * @param {*} action 
 */
function* walletTransfer(action) {
  try {
    Toast.loading(`${I18n.t('toast_transfer')}...`, 0);
    const response = yield call(HttpUtils.post, api.wallet_transfer, action.payload)
    yield put(walletActions.walletTransferSuccess(response.data))
    Toast.hide()
    Actions.replace('paymentSuccess', { result: action.payload });
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
    yield put(walletActions.walletTransferFail())
  }
}



/**
 * 收银台订单支付
 * @param {*} action 
 */
function* orderPay(action) {
  try {
    Toast.loading(`${I18n.t('toast_pay')}...`, 0);
    const response = (yield call(HttpUtils.post, api.pay_orderpay, action.payload)).data
    yield put(walletActions.orderPaySuccess(response))
    Toast.hide()
    if (response.resultCode === "FAIL") {
      Toast.fail('支付失败', 3);
    } else {
      Actions.replace('paymentSuccess', { resultCode: response.resultCode, result: action.payload });
    }
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 3);
    yield put(walletActions.orderPayFail())
  }
}

/**
 * 提币
 * @param {*} action 
 */
function* walletExtract(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.post, api.wallet_extract, action.payload)
    yield put(walletActions.walletExtractSuccess(response.data))
    Toast.hide()
    Toast.success(I18n.t('toast_extract'), 1)
    Actions.popTo('_mine');
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
    yield put(walletActions.walletExtractFail())
  }
}

/**
 * 获取充值地址
 * @param {*} action 
 */
function* getDepositAddress(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.post, api.wallet_address_deposit, action.payload)
    yield put(walletActions.getDepositAddressSuccess(response.data))
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
    yield put(walletActions.getDepositAddressFail())
  }
}

/**
 * 获取主账户
 */
function* getPrimaryAccount() {
  try {
    // Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.get, api.wallet_account_primary)
    yield put(walletActions.getPrimaryAccountSuccess(response.data))
    Toast.hide();
  } catch (err) {
    console.log('主账户', err.message)
    Toast.hide();
    yield put(walletActions.getPrimaryAccountFail())
  }
}

/**
 * 创建账户
 * @param {*} action 
 */
function* createAccount(action) {
  try {
    const response = yield call(HttpUtils.post, api.wallet_account_create, action.payload)
    yield put(walletActions.createAccountSuccess(response.data))
    Toast.success(I18n.t('toast_createAccount_success'), 2)
    Actions.pop();
  } catch (err) {
    err.message && Toast.fail(err.message, 3);
    yield put(walletActions.createAccountFail())
  }
}

/**
 * 删除账户
 * @param {*} action
 */
function* deleteAccount(action) {
  try {
    const response = yield call(HttpUtils.delete, `/wallet/account/${action.payload}`)
    yield put(walletActions.deleteAccountSuccess(response.data))
    Toast.success(I18n.t('toast_deleteAccount_success'), 2)
    Actions.pop();
  } catch (err) {
    err.message && Toast.fail(err.message, 3);
    yield put(walletActions.deleteAccountFail())
  }
}

/**
 * 设置主账户
 */
function* setPrimaryAccount(action) {
  try {
    const response = yield call(HttpUtils.put, api.wallet_account_primary, action.payload)
    yield put(walletActions.setPrimaryAccountSuccess())
    Actions.pop();
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
    yield put(walletActions.setPrimaryAccountFail())
  }
}

/**
 * 根据用户ID和币种类型查询账户信息
 */
function* getAccountByCoinType(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const url = `${api.wallet_account_primary}/${action.payload}`
    const response = yield call(HttpUtils.get, url)
    yield put(walletActions.getAccountByCoinTypeSuccess(response.data))
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
    yield put(walletActions.getAccountByCoinTypeFail())
  }
}


export function* watchAccountList() {
  yield takeLatest(walletActions.GET_ACCOUNTS, getAccountList)
}

export function* watchAccountPayment() {
  yield takeLatest(walletActions.GET_ACCOUNT_PAYMENT, getAccountPayment)
}

export function* watchPaymentList() {
  yield takeLatest(walletActions.GET_PAYMENTLIST, getPaymentList)
}

export function* watchExtractRecord() {
  yield takeLatest(walletActions.EXTRACT_RECORD, getExtractRecord)
}

export function* watchCoinTypes() {
  yield takeLatest(walletActions.GET_COINTYPES, getCoinTypes)
}

export function* watchDepositRecord() {
  yield takeLatest(walletActions.DEPOSIT_RECORD, getDepositRecord)
}

export function* watchOrderRecord() {
  yield takeLatest(walletActions.ORDER_RECORD, getOrderRecord)
}

export function* watchOrderRecordLoad() {
  yield takeLatest(walletActions.ORDER_RECORD_LOAD, getOrderRecord)
}

export function* watchWalletPay() {
  yield takeLatest(walletActions.WALLET_PAY, walletPay)
}

export function* watchWalletTransfer() {
  yield takeLatest(walletActions.WALLET_TRANSFER, walletTransfer)
}

export function* watchOrderPay() {
  yield takeLatest(walletActions.ORDER_PAY, orderPay)
}

export function* watchWalletExtract() {
  yield takeLatest(walletActions.WALLET_EXTRACT, walletExtract)
}

export function* watchDepositAddress() {
  yield takeLatest(walletActions.GET_DEPOSIT_ADDRESS, getDepositAddress)
}

export function* watchPrimaryAccount() {
  yield takeLatest(walletActions.GET_PRIMARY_ACCOUNT, getPrimaryAccount)
}

export function* watchCreateAccount() {
  yield takeLatest(walletActions.CREATE_ACCOUNT, createAccount)
}

export function* watchCreateAccountSuccess() {
  yield takeLatest(walletActions.CREATE_ACCOUNT_SUCCESS, getAccountList)
}

export function* watchDeleteAccount() {
  yield takeLatest(walletActions.DELETE_ACCOUNT, deleteAccount)
}

export function* watchDeleteAccountSuccess() {
  yield takeLatest(walletActions.DELETE_ACCOUNT_SUCCESS, getAccountList)
}

export function* watchSetPrimaryAccount() {
  yield takeLatest(walletActions.SET_PRIMARY_ACCOUNT, setPrimaryAccount)
}

export function* watchUpdatePrimaryAccount() {
  yield takeLatest(walletActions.SET_PRIMARY_ACCOUNT_SUCCESS, getPrimaryAccount)
}

export function* watchGetAccountByCoinType() {
  yield takeLatest(walletActions.GET_ACCOUNT_BYCOINTYPE, getAccountByCoinType)
}


const sagas = [
  watchAccountList,
  watchAccountPayment,
  watchPaymentList,
  watchExtractRecord,
  watchCoinTypes,
  watchDepositRecord,
  watchWalletPay,
  watchWalletTransfer,
  watchOrderPay,
  watchWalletExtract,
  watchDepositAddress,
  watchPrimaryAccount,
  watchCreateAccount,
  watchSetPrimaryAccount,
  watchUpdatePrimaryAccount,
  watchCreateAccountSuccess,
  watchDeleteAccount,
  watchDeleteAccountSuccess,
  watchGetAccountByCoinType,
  watchOrderRecord,
  watchOrderRecordLoad
]

export default sagas;