import { call, put, take, takeLatest, fork } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import { Toast } from 'antd-mobile';
import { Platform, Alert, Linking, NativeModules } from 'react-native';
import { HttpUtils, StorageUtils, configToken, FeibaUtils, BddUtils, EncryptUtils } from "../../utils";
import { constDefines, api } from '../../constants';
import { authActions, userActions } from '../actions';
import I18n from 'I18n';
import ExtensionModule from '../../components/ExtensionModule';

function* authLogin(action) {
  try {
    Toast.loading(`${I18n.t('toast_login')}...`, 0);
    const response = yield call(HttpUtils.post, api.auth_login, action.payload)
    yield call(StorageUtils.set, constDefines.storeAuthInfoKey, response.data)
    yield put(authActions.loginSuccess(response.data))
    yield call(ExtensionModule.saveTokenNative, response.data.token)
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
    yield put(authActions.loginFail())
  }
}

function* loginSuccess(action) {
  const payload = action.payload;
  configToken(payload.token)
  if (Platform.OS === "android") {
    ExtensionModule.getDataFromIntent(
      (result) => {
        if (result) {
          ExtensionModule.startZatGoPayPage(result)
        } else {
          Actions.reset('tabbar')
        }
      },
      (err) => {
        console.log('失败', err)
      }
    );
  } else {
    if (global.trade != '') {
      prepayId = global.trade;
      try {
        const response = yield call(HttpUtils.get, `${api.pay_check_PrepayId}${prepayId}`)
        if (response.data) {
          Actions.reset('web', { 'zatgoToken': payload.token, 'prepayId': prepayId });
        } else {
          Actions.reset('tabbar');
          Alert.alert(I18n.t('alert_web_title'), I18n.t('alert_prepayid_fail'), 1);
        }
      } catch (err) {
        Actions.reset('tabbar');
        Alert.alert(I18n.t('alert_web_title'), err.message)
      }
      trade = '';
    } else {
      Actions.reset('tabbar');
    }
  }
}

function* authLogout() {
  try {
    const response = yield call(HttpUtils.get, api.auth_logout)
    yield call(StorageUtils.clear)
    yield call(ExtensionModule.removeTokenNative)
    yield put(authActions.logoutSuccess())
    Toast.success(I18n.t('toast_logout_success'), 1)
    Actions.reset('login');
  } catch (err) {
    yield call(StorageUtils.clear)
    yield call(ExtensionModule.removeTokenNative)
    Toast.success(I18n.t('toast_logout_success'), 1)
    Actions.reset('login');
    yield put(authActions.logoutFail())
  }
}

//注册用户判断重复
function* usernameExist(action) {
  const username = action.payload.loginName
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.get, `${api.auth_usernameExist}`, { userName: username })
    Toast.hide();
    if (!action.payload.forgotPsw) {
      yield put(authActions.sendAuthCodeSuccess(action.payload))
    }
    if (action.payload.forgotPsw) {
      Toast.fail(I18n.t('toast_username_exist'))
    }
  } catch (err) {
    Toast.hide()
    if (err.code == '1006' && action.payload.forgotPsw) {
      yield put(authActions.sendAuthCodeSuccess(action.payload))
    } else {
      err.message && Toast.fail(err.message, 2);
    }
  }
}

function* sendAuthCodeSuccess(action) {
  const loginName = action.payload.loginName
  const aliyunValidateCode = action.payload.aliyunValidateCode
  try {
    const response = yield call(HttpUtils.post, api.auth_sendPhoneAuthCode, { aliyunValidateCode, 'mobilePhone': loginName })
    Toast.success(I18n.t('toast_sendPhone_success'), 1)
    Actions.captcha(action.payload);
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
  }
}

//邮箱发送请求
function* sendEmailAuthCode(action) {
  try {
    const reqParams = action.payload || {};
    const response = yield call(HttpUtils.post, api.auth_sendEmailAuthCode, action.payload)
    Toast.success(I18n.t('toast_sendEmail_success'), 1)
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
  }
}

//短息发送请求
function* sendPhoneAuthCode(action) {
  try {
    const response = yield call(HttpUtils.post, api.auth_sendPhoneAuthCode, action.payload)
    Toast.hide()
    Toast.success(I18n.t('toast_sendPhone_success'), 1)
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
  }
}

//短息验证
function* verifyAuthCode(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const url = `${api.auth_verifyAuthCode}/${action.payload.AuthCode.registName}/${action.payload.AuthCode.authCode}`
    const response = yield call(HttpUtils.get, url)
    Toast.hide()
    Toast.success(I18n.t('toast_verificationCode_success'), 1)
    if (action.payload.type === 'forgotPsw') {
      Actions.forgotPasswordPage(action.payload.params)
    } else {
      Actions.password(action.payload.params);
    }
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

function* authRegister(action) {
  try {
    Toast.loading(`${I18n.t('toast_registering')}...`, 0);
    const reqParams = action.payload || {};
    const response = yield call(HttpUtils.post, api.auth_register, action.payload)
    Toast.hide();
    Toast.success(I18n.t('toast_registered_succsee'), 1)
    Actions.login();
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

function* retrievePassword(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const reqParams = action.payload || {};
    const response = yield call(HttpUtils.post, api.user_forgetLoginPassword, action.payload)
    Toast.hide()
    Toast.success(I18n.t('toast_modify_success'), 1)
    Actions.login();
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }

}

function* getUserByName(action) {
  try {
    const url = `${api.user_byname}/${action.payload}`;
    const response = yield call(HttpUtils.get, url)
    yield put(userActions.getUserSuccess(response.data))
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
    yield put(userActions.getUserFail())
  }
}

function* getUserById(action) {
  try {
    const url = `${api.user_byid}/${action.payload}`;
    const response = yield call(HttpUtils.get, url)
    yield put(userActions.getUserSuccess(response.data))
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
    yield put(userActions.getUserFail())
  }
}

function* getAuthById(action) {
  try {
    const url = `${api.user_byid}/${action.payload}`;
    const response = yield call(HttpUtils.get, url)
    yield put(userActions.getAuthSuccess(response.data))
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
    yield put(userActions.getAuthFail())
  }
}


//获取用户认证信息
function* getCertificateInfo(action) {
  try {
    const url = api.user_certificateInfo
    const response = yield call(HttpUtils.get, url)
    yield put(userActions.getCertificateSuccess(response.data))
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
    yield put(userActions.getCertificateFail())
  }
}

function* uploadImage(action) {
  try {
    Toast.loading('上传...');
    const response = yield call(HttpUtils.uploadImage, api.user_uploadImage, action.payload);
    // console.log(response)
    const iconUrl = Object.values(response.data);
    yield put(userActions.updateBasicInfo({
      iconUrl: iconUrl[0],
      nickname: action.payload.nickName,
      userId: action.payload.userId,
    }))
  } catch (err) {
    Toast.hide();
    err.message && Toast.fail(err.message, 2);
  }
}

function* upDateBasicInfo(action) {
  try {
    // console.log(action)
    const params = { iconUrl: action.payload.iconUrl, nickname: action.payload.nickName, }
    const response = yield call(HttpUtils.post, api.user_updateBasicInfo, params)
    // console.log(action.payload.userId)
    yield put(userActions.getAuthById(action.payload.userId))
    Toast.hide();
  } catch (err) {
    Toast.hide();
    err.message && Toast.fail(err.message, 2);
  }
}

//上传用户认证照片
function* uploadCertificateImage(action) {
  try {
    Toast.loading('上传...');
    const response = yield call(HttpUtils.uploadImage, api.user_uploadImage, action.payload);
    // console.log(response)
    const iconUrl = Object.values(response.data);
    console.log(iconUrl)
    yield put(userActions.uploadCertificateInfo({
      certificateId: action.payload.cardNum,
      certificateType: action.payload.cardType,
      countryCode: action.payload.country,
      firstName: action.payload.name,
      holdPhotoUrl: iconUrl[0],
    }))
  } catch (err) {
    Toast.hide();
    err.message && Toast.fail(err.message, 2);
  }
}

//上传用户认证信息
function* upDateCertificateInfo(action) {
  try {
    console.log(action.payload)
    const response = yield call(HttpUtils.post, api.user_updateCertificateInfo, action.payload)
    console.log('response', response)
    yield put(userActions.getCertificateStatus())
    Toast.hide();
    Actions.pop();
  } catch (err) {
    Toast.hide();
    console.log('responseError')
    err.message && Toast.fail(err.message, 2);
  }
}


function* signLoginFeiba(action) {
  try {
    const { weburl, zatgoToken, params, appAuth } = action.payload || {}
    if (appAuth) {
      Toast.loading(`${I18n.t('toast_loading')}...`, 0);
      const response = yield call(FeibaUtils.post, appAuth, params)
      yield put(authActions.signLoginFeibaSuccess(response.data))
      Toast.hide()
      if (Platform.OS === "android") {
        const feibaUrl = `${weburl}&token=${response.data.token}&zatgoToken=${zatgoToken}&webSign=${EncryptUtils.webMd5()}`
        ExtensionModule.startWebViewPage(feibaUrl);
      } else {

        const feibaUrl = `${weburl}&token=${response.data.token}&zatgoToken=${zatgoToken}&webSign=${EncryptUtils.webMd5()}`
        NativeModules.NativeManager.RNOpenOneVC({
          url: feibaUrl,
          zatgoToken: zatgoToken,
          payUrl: payUrl,
          NavgationBar: ExtensionModule.GetQueryString(feibaUrl, 'webviewTitleInvisible')
        })//NavgationBar隐藏标导航栏按钮
      }

    } else {
      if (Platform.OS === "android") {
        const feibaUrl = `${weburl}&zatgoToken=${zatgoToken}&webSign=${EncryptUtils.webMd5()}`
        ExtensionModule.startWebViewPage(feibaUrl);
      } else {

        const feibaUrl = `${weburl}&zatgoToken=${zatgoToken}&webSign=${EncryptUtils.webMd5()}`
        NativeModules.NativeManager.RNOpenOneVC({
          url: feibaUrl,
          zatgoToken: zatgoToken,
          payUrl: payUrl,
          NavgationBar: ExtensionModule.GetQueryString(feibaUrl, 'webviewTitleInvisible'),//NavgationBar隐藏标导航栏按钮
          // hiddenNavBar: ExtensionModule.GetQueryString(feibaUrl, 'hiddenNavBar')',//隐藏webview导航栏
        })
      }
    }
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.errorMsg, 2);
    yield put(authActions.signLoginFeibaFail())
  }
}

function* signLoginBdd(action) {
  try {
    const { weburl, params, zatgoToken } = action.payload || {}
    Toast.loading(`${I18n.t('toast_loading')}...`, 0);
    const response = yield call(BddUtils.post, global.bddAuth, params)
    yield put(authActions.signLoginBddSuccess(response.token))
    Toast.hide()
    const bddUrl = `${weburl}?bdd_platform=Zat&token=${response.token}`
    console.log('h5地址 ' + bddUrl);
    if (Platform.OS === "android") {
      ExtensionModule.startWebViewPage(bddUrl);
    } else {
      // Actions.reset('web',{ url: bddUrl, zatgoToken: zatgoToken })
      NativeModules.NativeManager.RNOpenOneVC({ url: bddUrl, zatgoToken: zatgoToken, payUrl: payUrl, })
    }
  } catch (err) {
    Toast.fail(err.errorMsg, 2);
    yield put(authActions.signLoginBddFail())
  }
}

function* checkAuditVersion() {
  try {
    const response = yield call(HttpUtils.get, api.sys_audit_version);
    yield put(userActions.checkAuditVersionSuccess(response.data))
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
  }
}

function* getApplicationList(action) {
  try {
    const response = yield call(HttpUtils.get, api.application_list);
    yield put(authActions.getApplicationListSuccess(response.data))
    // console.log(response)
  } catch (err) {
    yield put(authActions.getApplicationListFail());
    err.message && Toast.fail(err.message, 2);
  }
}

function* getBannerList(action) {
  try {
    const { siteType } = action.payload
    const response = yield call(HttpUtils.get, api.banner_list, { siteType: siteType });
    if (siteType === 1) {
      yield put(authActions.getBannerListSuccess(response.data))
    } else {
      yield put(authActions.getAdvertListSuccess(response.data))
    }
  } catch (err) {
    if (action.payload.siteType === 1) {
      yield put(authActions.getBannerListFail());
    } else {
      yield put(authActions.getAdvertListFail())
    }
    err.message && Toast.fail(err.message, 2);
  }
}


export function* watchCheckAuditVersion() {
  yield takeLatest(userActions.CHECK_AUDIT_VERSION, checkAuditVersion)
}

export function* watchLogin() {
  yield takeLatest(authActions.LOGIN, authLogin)
}

export function* watchLoginSuccess() {
  yield takeLatest(authActions.LOGIN_SUCCESS, loginSuccess)
}

export function* watchLogout() {
  yield takeLatest(authActions.LOGOUT, authLogout)
}


export function* watchEmailAuthCode() {
  yield takeLatest(authActions.SEND_EMAIL_CODE, sendEmailAuthCode)
}

export function* watchPhoneAuthCode() {
  yield takeLatest(authActions.SEND_PHONE_CODE, sendPhoneAuthCode)
}

export function* watchGetUserByName() {
  yield takeLatest(userActions.GET_USER_BYNAME, getUserByName)
}

export function* watchGetUserById() {
  yield takeLatest(userActions.GET_USER_BYID, getUserById)
}

export function* watchGetAuthById() {
  yield takeLatest(userActions.GET_AUTH_BYID, getAuthById)
}

export function* watchUploadImage() {
  yield takeLatest(userActions.UPLOAD_IMAGE, uploadImage)
}

export function* watchUpDateBasicInfo() {
  yield takeLatest(userActions.UPDATE_BASICINFO, upDateBasicInfo)
}

export function* watchUesrnameExist() {
  yield takeLatest(authActions.USERNAME_EXIST, usernameExist)
}

export function* watchSendAuthCodeSuccess() {
  yield takeLatest(authActions.SEND_AUTH_CODE_SUCCESS, sendAuthCodeSuccess)
}

export function* watchVerifyAuthCode() {
  yield takeLatest(authActions.VERIFYAUTHCODE, verifyAuthCode)
}

export function* watchAuthRegister() {
  yield takeLatest(authActions.REGISTER_AUTH, authRegister)
}

export function* watchRetrievePassword() {
  yield takeLatest(authActions.RETRIEVE_PASSWORD, retrievePassword);
}

export function* watchSignLoginFeiba() {
  yield takeLatest(authActions.SIGN_LOGIN_FEIBA, signLoginFeiba)
}

export function* watchSignLoginBdd() {
  yield takeLatest(authActions.SIGN_LOGIN_BDD, signLoginBdd)
}

export function* watchGetApplicationList() {
  yield takeLatest(authActions.GET_APPLICTATION_LIST, getApplicationList)
}

export function* watchGetBannerList() {
  yield takeLatest(authActions.GET_BANNER_LIST, getBannerList)
}
export function* watchUploadCertificateImage() {
  yield takeLatest(userActions.UPLOAD_CERTIFICATE_IMAGE, uploadCertificateImage)
}
export function* watchUpDateCertificateInfo() {
  yield takeLatest(userActions.UPLOAD_CERTIFICATE_INFO, upDateCertificateInfo)
}

export function* watchGetCertificateInfo() {
  yield takeLatest(userActions.GET_CERTIFICATE_STATUS, getCertificateInfo)
}

const sagas = [
  watchLogin,
  watchLoginSuccess,
  watchLogout,
  watchEmailAuthCode,
  watchPhoneAuthCode,
  watchGetUserById,
  watchGetAuthById,
  watchUpDateBasicInfo,
  watchUploadImage,
  watchGetUserByName,
  watchAuthRegister,
  watchRetrievePassword,
  watchVerifyAuthCode,
  watchUesrnameExist,
  watchSignLoginFeiba,
  watchSendAuthCodeSuccess,
  watchSignLoginBdd,
  watchCheckAuditVersion,
  watchGetApplicationList,
  watchGetBannerList,
  watchUploadCertificateImage,
  watchUpDateCertificateInfo,
  watchGetCertificateInfo
]

export default sagas;