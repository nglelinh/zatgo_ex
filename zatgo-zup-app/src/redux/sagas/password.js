import { call, takeLatest } from 'redux-saga/effects'
import { Actions } from 'react-native-router-flux'
import { Toast } from 'antd-mobile'
import { HttpUtils, StorageUtils } from "../../utils"
import { pswChangeActions } from '../actions'
import { api} from '../../constants'
import I18n from 'I18n'

function* loginPswChange(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.post, api.user_editLoginPassword, action.payload)
    Toast.hide()
    Toast.success(I18n.t('toast_modify_success'), 1)
    Actions.login();
  } catch (err) {
    Toast.hide()
    err.message&&Toast.fail(err.message, 2);
  }
}

function* payPswChange(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.post, api.user_editPayPassword, action.payload)
    Toast.hide()
    Toast.success(I18n.t('toast_modify_success'), 1)
    Actions.pop()
  } catch (err) {
    Toast.hide()
    err.message&&Toast.fail(err.message, 2);
  }
}

function* resetPayPassword(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.post, api.user_forgetPayPassword, action.payload)
    Toast.hide()
    Toast.success(I18n.t('toast_reset_success'), 1)
    Actions.pop()
  } catch (err) {
    Toast.hide()
    err.message&&Toast.fail(err.message, 2);
  }
}

export function* watchLoginPswChange() {
  yield takeLatest(pswChangeActions.LOGINPWSCHANGE, loginPswChange)

}

export function* watchPayPwdChange() {
  yield takeLatest(pswChangeActions.EDITPAYPWD, payPswChange)

}

export function* watchResetPayPassword() {
  yield takeLatest(pswChangeActions.RESETPAYPWD, resetPayPassword)

}

const sagas = [
  watchLoginPswChange,
  watchPayPwdChange,
  watchResetPayPassword
]

export default sagas;