import { select, call, put, take, takeLatest, fork } from 'redux-saga/effects'
import { Toast } from 'antd-mobile'
import { api } from '../../constants'
import { HttpUtils, } from "../../utils"
import { couponActions } from '../actions'
import I18n from 'I18n'

function* getCoupons(action) {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.post, api.coupon_list, action.payload)
    const { status } = action.payload;
    yield put(couponActions.getCouponsSuccess({ data: response.data, type: status }));
    Toast.hide();
  } catch (err) {
    Toast.hide();
    yield put(couponActions.getCouponsFail());
    err.message && Toast.fail(err.message, 2);
  }
}

function* loadMoreCoupon(action) {
  try {
    const response = (yield call(HttpUtils.post, api.coupon_list, action.payload)).data;
    const { list, pageNum, pageSize, size, total, pages } = response;
    const { status } = action.payload;
    if (status === 0) {
      const effective = yield select(state => state.coupon.effective.list);
      this.newlist = effective.concat(list);
    } else if (status === 1) {
      const used = yield select(state => state.coupon.used.list);
      this.newlist = used.concat(list);
    } else if (status === 2) {
      const expired = yield select(state => state.coupon.expired.list);
      this.newlist = expired.concat(list);
    }
    const newObject = { list: this.newlist, pageNum, pageSize, size, total, pages };
    yield put(couponActions.getCouponsSuccess({ data: newObject, type: status }));
  } catch (err) {
    yield put(couponActions.getCouponsFail());
    err.message && Toast.fail(err.message, 2);
  }
}

export function* watchGetCoupons() {
  yield takeLatest(couponActions.GET_COUPONS, getCoupons)
}

export function* watchLoadMoreCoupon() {
  yield takeLatest(couponActions.GET_MORECOUPON, loadMoreCoupon)
}

const sagas = [
  watchGetCoupons,
  watchLoadMoreCoupon,
]

export default sagas;