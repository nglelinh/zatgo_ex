import { call, put, take, takeLatest, fork } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import { Toast } from 'antd-mobile';
import { HttpUtils, } from "../../utils";
import { constDefines, api } from '../../constants';
import { digActions, } from '../actions';
import I18n from 'I18n';


//登录任务
function* loginTask() {
  try {
    const response = yield call(HttpUtils.get, api.dig_login_task);
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
  }
}

//获取用户待挖矿信息
function* getUserMiningData() {
  try {
    yield put(digActions.getUserMiningDataSuccess([]));
    const response = yield call(HttpUtils.get, api.dig_premininginfo);
    yield put(digActions.getUserMiningDataSuccess(response.data));
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
  }
}

//挖矿记录列表
function* getUserMiningRecord(action) {
  try {
    let pageNo = action.payload.pageNo;
    pageNo != 1 ? Toast.loading(`${I18n.t('toast_requesting')}...`, 0) : null;
    const response = yield call(HttpUtils.get, `${api.dig_miningrecord}`, { page: pageNo, size: 15 });
    if (pageNo < 2) {
      yield put(digActions.getMiningRecordSuccess(response.data.list))
    } else if (response.data.list.length > 0 && (pageNo * 15) < response.data.total) {
      yield put(digActions.getMiningRecordLoadMore(response.data.list))
    } else {
      yield put(digActions.getMiningRecordNoMore(response.data.list))
    }
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

//获得算力历史记录
function* getComputePowerRecord(action) {
  try {
    pageNo === 1 && Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    let pageNo = action.payload.pageNo;
    const response = yield call(HttpUtils.get, `${api.dig_getcomputepowerrecord}`, { page: pageNo, size: 15 });
    if (pageNo < 2) {
      yield put(digActions.getComputePowerRecordSuccess(response.data.list))
    } else if (response.data.list.length > 0 && (pageNo * 15) < response.data.total) {
      yield put(digActions.getComputePowerRecordLoadMore(response.data.list))
    } else {
      yield put(digActions.getComputePowerRecordNoMore(response.data.list))
    }
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

//获得算力排行
function* getComputePowerTop() {
  try {
    // Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.get, api.dig_usercomputepowertop);
    yield put(digActions.getComputePowerTopSuccess(response.data));
    Toast.hide()
  } catch (err) {
    err.message && Toast.fail(err.message, 2);
  }
}

//算力任务
function* getHashRaseTask() {
  try {
    // Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.get, api.dig_task);
    yield put(digActions.getHashRaseTaskSuccess(response.data));
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

//算力邀请任务
function* getInvitationTask() {
  try {
    Toast.loading(`${I18n.t('toast_requesting')}...`, 0);
    const response = yield call(HttpUtils.get, api.dig_invite_task);
    yield put(digActions.getInvitationTaskSuccess(response.data));
    Toast.hide()
  } catch (err) {
    Toast.hide()
    err.message && Toast.fail(err.message, 2);
  }
}

export function* watchgLoginTask() {
  yield takeLatest(digActions.LOGINTASK, loginTask);
}


export function* watchgGetUserMiningData() {
  yield takeLatest(digActions.GET_USER_MINING_INFO, getUserMiningData);
}

export function* watchgGetUserMiningRecord() {
  yield takeLatest(digActions.GET_MINING_RECORD, getUserMiningRecord);
}

export function* watchgGetComputePowerRecord() {
  yield takeLatest(digActions.GET_COMPUTE_POWER_RECORD, getComputePowerRecord);
}

export function* watchGetComputePowerTop() {
  yield takeLatest(digActions.GET_COMPUTE_POWER_TOP, getComputePowerTop);
}

export function* watchGetComputePowerTask() {
  yield takeLatest(digActions.GET_HASHRATE_TASK, getHashRaseTask);
}
export function* watchGetInvitationTask() {
  yield takeLatest(digActions.GET_INVITATION_TASK, getInvitationTask)
}

const sagas = [
  watchgLoginTask,
  watchgGetUserMiningData,
  watchgGetUserMiningRecord,
  watchgGetComputePowerRecord,
  watchGetComputePowerTop,
  watchGetComputePowerTask,
  watchGetInvitationTask
]

export default sagas;