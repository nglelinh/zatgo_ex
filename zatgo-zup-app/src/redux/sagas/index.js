import { fork } from 'redux-saga/effects'

import authSaga from './auth'
import walletSaga from './wallet'
import passwordSaga from './password'
import digSaga from './dig'
import couponSaga from './coupon'

const sagas = [
  ...couponSaga,
  ...authSaga,
  ...walletSaga,
  ...passwordSaga,
  ...digSaga
]

export default function* rootSaga() {
  yield sagas.map(saga => fork(saga));
}