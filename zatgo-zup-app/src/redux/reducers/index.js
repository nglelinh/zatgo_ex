import { combineReducers } from 'redux'
import user from './user'
import wallet from './wallet'
import global from './global'
import dig from './dig'
import coupon from './coupon'

export default combineReducers({
    dig,
    user,
    wallet,
    global,
    coupon,
})