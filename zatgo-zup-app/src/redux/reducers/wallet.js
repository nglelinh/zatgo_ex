import * as Types from '../actions/wallet'

const initialState = {
  accounts: null,
  payments: [],// 账户交易记录
  paymentsStatus: true,
  paymentList: [],// 用户交易记录
  paymentListStatus: true,
  extractRecord: null,// 提取记录
  depositRecord: null,// 充值记录
  orderRecord: [],//订单记录
  coinTypes: null,
  primaryAccount: null,
  depositAddress: null,
  currentPayAccount: null,// 当前扣款账户
  orderStatus: true
}

const walletReducer = (state = initialState, action) => {
  const payload = action.payload;
  switch (action.type) {
    case Types.GET_ACCOUNTS:
      return {
        ...state,
        accounts: null,
      };
    case Types.GET_ACCOUNTS_SUCCESS:
      return {
        ...state,
        accounts: payload,
      };
    case Types.GET_PRIMARY_ACCOUNT_SUCCESS:
      return {
        ...state,
        primaryAccount: payload
      };
    case Types.GET_ACCOUNT_PAYMENT:
      return {
        ...state,
        // payments: null
      };
    case Types.GET_ACCOUNT_PAYMENT_SUCCESS:
      return {
        ...state,
        payments: payload,
        paymentsStatus: payload.length >= 20 ? true : false
      };
    case Types.GET_ACCOUNT_PAYMENT_LOAD_MORE:
      return {
        ...state,
        payments: state.payments.concat(payload),
        paymentsStatus: true
      };
    case Types.GET_ACCOUNT_PAYMENT_NO_MORE:
      return {
        ...state,
        payments: state.payments.concat(payload),
        paymentsStatus: false
      };
    case Types.GET_PAYMENTLIST:
      return {
        ...state,
        // paymentList: []
      };
    case Types.GET_PAYMENTLIST_SUCCESS:
      return {
        ...state,
        paymentList: payload,
        paymentListStatus: payload.length >= 20 ? true : false

      };
    case Types.GET_PAYMENTLIST_LOAD_MORE:
      return {
        ...state,
        paymentList: state.paymentList.concat(payload),
        paymentListStatus: true
      };
    case Types.GET_PAYMENTLIST_NO_MORE:
      return {
        ...state,
        paymentList: state.paymentList.concat(payload),
        paymentListStatus: false
      };
    case Types.EXTRACT_RECORD:
      return {
        ...state,
        extractRecord: null
      };
    case Types.EXTRACT_RECORD_SUCCESS:
      return {
        ...state,
        extractRecord: payload
      };
    case Types.GET_COINTYPES_SUCCESS:
      return {
        ...state,
        coinTypes: payload
      };
    case Types.ORDER_RECORD:
      return {
        ...state,
        orderRecord: [],
      };
    case Types.ORDER_RECORD_SUCCESS:
      return {
        ...state,
        orderRecord: payload,
        orderStatus: payload.length >= 20 ? true : false
      };
    case Types.ORDER_RECORD_LOAD_MORE:
      return {
        ...state,
        orderRecord: state.orderRecord.concat(payload),
        orderStatus: true
      };
    case Types.ORDER_RECORD_NO_MORE:
      return {
        ...state,
        orderRecord: state.orderRecord.concat(payload),
        orderStatus: false,
      };
    case Types.ORDER_RECORD_FAIL:
      return {
        ...state,
        orderRecord: [],
        orderStatus: false
      };
      case Types.DEPOSIT_RECORD:
      return {
        ...state,
        depositRecord: null
      };
    case Types.DEPOSIT_RECORD_SUCCESS:
      return {
        ...state,
        depositRecord: payload
      };
    case Types.GET_DEPOSIT_ADDRESS:
      return {
        ...state,
        depositAddress: null
      };
    case Types.GET_DEPOSIT_ADDRESS_SUCCESS:
      return {
        ...state,
        depositAddress: payload
      };
    case Types.GET_ACCOUNT_BYCOINTYPE_SUCCESS:
      return {
        ...state,
        currentPayAccount: payload
      };
    default:
      return state;
  }
}

export default walletReducer;