import * as digTypes from '../actions/dig'

const initialState = {
  miningData: [],
  userInfo: {},
  computePowerTop: [],
  computePowerRecord: [],
  miningRecord: [],
  taskData:{},
  invitationTaskData:[],
  computePowerStatus:true,
  miningRecordStatuse:true
}

const digReducer = (state = initialState, action) => {
  const payload = action.payload;
  switch (action.type) {
    case digTypes.GET_USER_MINING_INFO_SUCCESS:
      return {
        ...state,
        miningData: payload
      };

    case digTypes.GET_COMPUTE_POWER_TOP_SUCCESS:
      return {
        ...state,
        computePowerTop: payload
      };
    case digTypes.GET_MINING_RECORD_SUCCESS:
      return {
        ...state,
        miningRecord: payload,
        miningRecordStatuse:payload.length>=15?true:false,
      };
    case digTypes.GET_MINING_RECORD_LOADMORE:
      return {
        ...state,
        miningRecord: state.miningRecord.concat(payload),
        miningRecordStatuse:true
      };
    case digTypes.GET_MINING_RECORD_NOMORE:
      return {
        ...state,
        miningRecord: state.miningRecord.concat(payload),
        miningRecordStatuse:false
      };
    case digTypes.GET_MINING_RECORD_FAIL:
      return {
        ...state,
        miningRecord: [],
        miningRecordStatuse:true,
      };
    case digTypes.GET_COMPUTE_POWER_RECORD_SUCCESS:
      return {
        ...state,
        computePowerRecord: payload,
        computePowerStatus:payload.length>=15?true:false,
      };
    case digTypes.GET_COMPUTE_POWER_RECORD_LOADMORE:
      return {
        ...state,
        computePowerRecord: state.computePowerRecord.concat(payload),
        computePowerStatus: true,
      };
    case digTypes.GET_COMPUTE_POWER_RECORD_NOMORE:
      return {
        ...state,
        computePowerRecord: state.computePowerRecord.concat(payload),
        computePowerStatus: false,
      };
    case digTypes.GET_COMPUTE_POWER_RECORD_FAIL:
      return {
        ...state,
        computePowerRecord: [],
        computePowerStatus: true,
      };
    case digTypes.GET_HASHRATE_TASK_SUCCESS:
      return {
        ...state,
        taskData: payload,
      };
    case digTypes.GET_INVITATION_TASK_SUCCESS:
      return {
        ...state,
        invitationTaskData: payload,
      };
    default:
      return state;
  }
}

export default digReducer;