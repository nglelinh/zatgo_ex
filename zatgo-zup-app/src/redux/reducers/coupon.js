import * as Types from '../actions/coupon'

const initialState = {
  used: {},
  expired: {},
  effective: {},
  pending: false,
}

const fetchSuccess = (state, payload) => {
  if (payload.type === 0) {
    return {
      ...state,
      pending: false,
      effective: payload.data,
    };
  } else if (payload.type === 1) {
    return {
      ...state,
      pending: false,
      used: payload.data,
    };
  } else if (payload.type === 2) {
    return {
      ...state,
      pending: false,
      expired: payload.data,
    };
  }
}


const couponReducer = (state = initialState, action) => {
  const payload = action.payload;
  switch (action.type) {
    case Types.GET_COUPONS:
      return {
        ...state,
        used: {},
        expired: {},
        effective: {},
        pending: true,
      };
    case Types.GET_COUPONS_FAIL:
      return {
        ...state,
        pending: false,
      };
    case Types.GET_COUPONS_SUCCESS: return fetchSuccess(state, payload);
    default:
      return state;
  }
}

export default couponReducer;