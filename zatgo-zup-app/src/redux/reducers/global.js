import * as Types from '../actions/global'
import {configLanguage} from '../../utils/HttpUtils'

const initialState = {
  language: null,
  redEnvelope:true,
}

const globalReducer = (state = initialState, action) => {
  const payload = action.payload;
  switch (action.type) {
    case Types.SET_LANGUAGE:
      configLanguage(payload);
      return {
        ...state,
        language: payload
      };
    case Types.SET_REDENVELOPE:
      return {
        ...state,
        redEnvelope: payload
      };
    default:
      return state;
  }
}

export default globalReducer;