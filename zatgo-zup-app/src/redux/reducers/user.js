import * as AuthTypes from '../actions/auth'
import * as UserTypes from '../actions/user'
import {GET_ADVERT_LIST_SUCCESS} from "../actions/auth";

const initialState = {
  userInfo: {},
  authInfo: {},
  feibaAct: null,
  bddToken: null,
  isAudit: false,
  applicationList: [],
  bannerList: [],
  advertList: [],
  mineInfo:{},
  certificateInfo:{}
}

const userReducer = (state = initialState, action) => {
  const payload = action.payload;
  switch (action.type) {
    case AuthTypes.LOGIN_SUCCESS:
      return {
        ...state,
        authInfo: payload
      };
    case AuthTypes.LOGIN_SETAUTHINFO:
      return {
        ...state,
        authInfo: payload
      };
    case AuthTypes.LOGOUT:
      return {
        ...state,
        userInfo: {},
        authInfo: {},
        feibaAct: null,
        bddAct: null,
      };
    case AuthTypes.LOGOUT_SUCCESS:
      return {
        ...state,
        userInfo: {},
        authInfo: {},
        feibaAct: null,
        bddAct: null,
      };
    case AuthTypes.LOGOUT_FAIL:
      return {
        ...state,
        userInfo: {},
        authInfo: {},
        feibaAct: null,
        bddAct: null,
      };
    case UserTypes.GET_USER_SUCCESS:
      return {
        ...state,
        userInfo: payload
      };
    case UserTypes.GET_USER_BYID:
      return {
        ...state,
        userInfo: {}
      };
    case UserTypes.GET_AUTH_SUCCESS:
      return {
        ...state,
        mineInfo: payload
      };
    case UserTypes.GET_AUTH_BYID:
      return {
        ...state,
        mineInfo: {}
      };
    case AuthTypes.SIGN_LOGIN_FEIBA:
      return {
        ...state,
        feibaAct: null
      };
    case AuthTypes.SIGN_LOGIN_FEIBA_SUCCESS:
      return {
        ...state,
        feibaAct: payload
      };
    case AuthTypes.SIGN_LOGIN_BDD:
      return {
        ...state,
        bddAct: null
      };
    case AuthTypes.SIGN_LOGIN_BDD_SUCCESS:
      return {
        ...state,
        bddAct: payload
      };
    case AuthTypes.GET_APPLICTATION_LIST_SUCCSEE:
      return {
        ...state,
        applicationList: payload
      };
    case AuthTypes.GET_APPLICTATION_LIST_FAIL:
      return {
        ...state,
        applicationList: []
      };
    case AuthTypes.GET_BANNER_LIST_SUCCESS:
      return {
        ...state,
        bannerList: payload
      };
    case AuthTypes.GET_BANNER_LIST_FAIL:
      return {
        ...state,
        bannerList: []
      };
    case AuthTypes.GET_ADVERT_LIST_SUCCESS:
      return {
        ...state,
        advertList: payload
      };
    case AuthTypes.GET_ADVERT_LIST_FAIL:
      return {
        ...state,
        advertList: []
      };
    case UserTypes.CHECK_AUDIT_VERSION:
      return {
        ...state,
        isAudit: false
      };
    case UserTypes.CHECK_AUDIT_VERSION_SUCCESS:
      return {
        ...state,
        isAudit: payload
      };
    case UserTypes.GET_CERTIFICATE_STATUS:
      return {
        ...state,
        certificateInfo: {}
      };
    case UserTypes.GET_CERTIFICATE_SUCCESS:
      return {
        ...state,
        certificateInfo: payload
      };
    case UserTypes.GET_CERTIFICATE_FAIL:
      return {
        ...state,
        certificateInfo: {}
      };
    default:
      return state;
  }
}

export default userReducer;