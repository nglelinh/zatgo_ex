import { createAction } from '../../helpers/createAction'

export const GET_COUPONS = 'GET_COUPONS'
export const GET_COUPONS_FAIL = 'GET_COUPONS_FAIL'
export const GET_COUPONS_SUCCESS = 'GET_COUPONS_SUCCESS'

export const GET_MORECOUPON = 'GET_MORECOUPON'

export const getCoupons = createAction(GET_COUPONS)
export const getCouponsFail = createAction(GET_COUPONS_FAIL)
export const getCouponsSuccess = createAction(GET_COUPONS_SUCCESS)

export const getMoreCoupon = createAction(GET_MORECOUPON)
