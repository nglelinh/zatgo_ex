import { createAction } from '../../helpers/createAction'

export const LOGINPWSCHANGE = 'ChangeLoginPassword'
export const EDITPAYPWD = 'EDITPAYPWD'
export const RESETPAYPWD = 'RESETPAYPWD'

export const LoginPwsChange = createAction(LOGINPWSCHANGE)
export const EditPayPwd = createAction(EDITPAYPWD)
export const ResetPayPwd = createAction(RESETPAYPWD)