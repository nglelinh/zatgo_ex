import * as authActions from "./auth"
import * as digActions from "./dig"
import * as walletActions from "./wallet"
import * as userActions from "./user"
import * as pswChangeActions from './password'
import * as globalActions from './global'
import * as couponActions from './coupon'

export {
  couponActions,
  authActions,
  userActions,
  walletActions,
  globalActions,
  pswChangeActions,
  digActions
}