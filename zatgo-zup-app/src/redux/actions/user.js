import { createAction } from '../../helpers/createAction'

export const GET_USER_BYID = 'GET_USER_BYID'
export const GET_USER_BYNAME = 'GET_USER_BYNAME'
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS'
export const GET_USER_FAIL = 'GET_USER_FAIL'
export const CHECK_AUDIT_VERSION = 'CHECK_AUDIT_VERSION'
export const CHECK_AUDIT_VERSION_SUCCESS = 'CHECK_AUDIT_VERSION_SUCCESS'
export const UPLOAD_IMAGE = 'UPLOAD_IMAGE'
export const UPDATE_BASICINFO = 'UPDATE_BASICINFO'

export const GET_AUTH_BYID = 'GET_AUTH_BYID'
export const GET_AUTH_SUCCESS = 'GET_AUTH_SUCCESS'
export const GET_AUTH_FAIL = 'GET_AUTH_FAIL'

export const UPLOAD_CERTIFICATE_IMAGE =  'UPLOAD_CERTIFICATE_IMAGE'
export const UPLOAD_CERTIFICATE_INFO =  'UPLOAD_CERTIFICATE_INFO'

export const GET_CERTIFICATE_STATUS = 'GET_CERTIFICATE_STATUS'
export const GET_CERTIFICATE_SUCCESS = 'GET_CERTIFICATE_SUCCESS'
export const GET_CERTIFICATE_FAIL = 'GET_CERTIFICATE_FAIL'

export const getUserById = createAction(GET_USER_BYID)
export const getUserByName = createAction(GET_USER_BYNAME)
export const getUserSuccess = createAction(GET_USER_SUCCESS)
export const getUserFail = createAction(GET_USER_FAIL)

export const getAuthById = createAction(GET_AUTH_BYID)
export const getAuthSuccess = createAction(GET_AUTH_SUCCESS)
export const getAuthFail = createAction(GET_AUTH_FAIL)

export const uploadImage = createAction(UPLOAD_IMAGE)
export const updateBasicInfo = createAction(UPDATE_BASICINFO)

export const uploadCertificateImage = createAction(UPLOAD_CERTIFICATE_IMAGE)
export const uploadCertificateInfo = createAction(UPLOAD_CERTIFICATE_INFO)

export const getCertificateStatus = createAction(GET_CERTIFICATE_STATUS)
export const getCertificateSuccess = createAction(GET_CERTIFICATE_SUCCESS)
export const getCertificateFail = createAction(GET_CERTIFICATE_FAIL)

export const checkAuditVersion = createAction(CHECK_AUDIT_VERSION)
export const checkAuditVersionSuccess = createAction(CHECK_AUDIT_VERSION_SUCCESS)
