/**
 * Created by zhoujianxin on 2018/7/26.
 * @Desc
 */
import { createAction } from '../../helpers/createAction'
import {LOGIN, LOGIN_FAIL, LOGIN_SUCCESS} from "./auth";


//登录任务
export const LOGINTASK = 'LOGINTASK'
export const LOGINTASK_SUCCESS = 'LOGINTASK_SUCCESS'
export const LOGINTASK_FAIL = 'LOGINTASK_FAIL'

//挖矿信息
export const GET_USER_MINING_INFO = 'GET_USER_MINING_INFO'
export const GET_USER_MINING_INFO_SUCCESS = 'GET_USER_MINING_INFO_SUCCESS'
export const GET_USER_MINING_INFO_FAIL = 'GET_USER_MINING_INFO_FAIL'

//挖矿记录列表
export const GET_MINING_RECORD = 'GET_MINING_RECORD'
export const GET_MINING_RECORD_SUCCESS = 'GET_MINING_RECORD_SUCCESS'
export const GET_MINING_RECORD_FAIL = 'GET_MINING_RECORD_FAIL'
export const GET_MINING_RECORD_LOADMORE = 'GET_MINING_RECORD_LOADMORE'
export const GET_MINING_RECORD_NOMORE = 'GET_MINING_RECORD_NOMORE'

//算力记录列表
export const GET_COMPUTE_POWER_RECORD = 'GET_COMPUTE_POWER_RECORD'
export const GET_COMPUTE_POWER_RECORD_SUCCESS = 'GET_COMPUTE_POWER_RECORD_SUCCESS'
export const GET_COMPUTE_POWER_RECORD_FAIL = 'GET_COMPUTE_POWER_RECORD_FAIL'
export const GET_COMPUTE_POWER_RECORD_LOADMORE = 'GET_COMPUTE_POWER_RECORD_LOADMORE'
export const GET_COMPUTE_POWER_RECORD_NOMORE = 'GET_COMPUTE_POWER_RECORD_NOMORE'

//算力排行
export const GET_COMPUTE_POWER_TOP = 'GET_COMPUTE_POWER_TOP'
export const GET_COMPUTE_POWER_TOP_SUCCESS = 'GET_COMPUTE_POWER_TOP_SUCCESS'

//算力任务
export const GET_HASHRATE_TASK = 'GET_HASHRATE_TASK'
export const GET_HASHRATE_TASK_SUCCESS = 'GET_HASHRATE_TASK_SUCCESS'

//邀请任务列表
export const GET_INVITATION_TASK = 'GET_INVITATION_TASK'
export const GET_INVITATION_TASK_SUCCESS = 'GET_INVITATION_TASK_SUCCESS'

export const loginTask = createAction(LOGINTASK)
export const loginTaskSuccess = createAction(LOGINTASK_SUCCESS)
export const loginTaskFail = createAction(LOGINTASK_FAIL)
//挖矿信息
export const getUserMiningData = createAction(GET_USER_MINING_INFO)
export const getUserMiningDataSuccess = createAction(GET_USER_MINING_INFO_SUCCESS)

//挖矿记录列表
export const getMiningRecord = createAction(GET_MINING_RECORD)
export const getMiningRecordSuccess = createAction(GET_MINING_RECORD_SUCCESS)
export const getMiningRecordFail = createAction(GET_MINING_RECORD_FAIL)
export const getMiningRecordLoadMore = createAction(GET_MINING_RECORD_LOADMORE)
export const getMiningRecordNoMore = createAction(GET_MINING_RECORD_NOMORE)


//算力记录列表
export const getComputePowerRecord = createAction(GET_COMPUTE_POWER_RECORD)
export const getComputePowerRecordSuccess = createAction(GET_COMPUTE_POWER_RECORD_SUCCESS)
export const getComputePowerRecordFail = createAction(GET_COMPUTE_POWER_RECORD_FAIL)
export const getComputePowerRecordLoadMore = createAction(GET_COMPUTE_POWER_RECORD_LOADMORE)
export const getComputePowerRecordNoMore = createAction(GET_COMPUTE_POWER_RECORD_NOMORE)


//算力排行
export const getComputePowerTop = createAction(GET_COMPUTE_POWER_TOP)
export const getComputePowerTopSuccess = createAction(GET_COMPUTE_POWER_TOP_SUCCESS)

//算力任务
export const getHashRaseTask = createAction(GET_HASHRATE_TASK)
export const getHashRaseTaskSuccess = createAction(GET_HASHRATE_TASK_SUCCESS)
//邀请任务列表
export const getInvitationTask = createAction(GET_INVITATION_TASK)
export const getInvitationTaskSuccess = createAction(GET_INVITATION_TASK_SUCCESS)







