import { createAction } from '../../helpers/createAction'


export const GET_ACCOUNTS = 'GET_ACCOUNTS'
export const GET_ACCOUNTS_SUCCESS = 'GET_ACCOUNTS_SUCCESS'
export const GET_ACCOUNTS_FAIL = 'GET_ACCOUNTS_FAIL'

export const GET_ACCOUNT_PAYMENT = 'GET_ACCOUNT_PAYMENT'
export const GET_ACCOUNT_PAYMENT_SUCCESS = 'GET_ACCOUNT_PAYMENT_SUCCESS'
export const GET_ACCOUNT_PAYMENT_LOAD_MORE = 'GET_ACCOUNT_PAYMENT_LOAD_MORE'
export const GET_ACCOUNT_PAYMENT_NO_MORE = 'GET_ACCOUNT_PAYMENT_NO_MORE'
export const GET_ACCOUNT_PAYMENT_FAIL = 'GET_ACCOUNT_PAYMENT_FAIL'

export const GET_PAYMENTLIST = 'GET_PAYMENTLIST'
export const GET_PAYMENTLIST_SUCCESS = 'GET_PAYMENTLIST_SUCCESS'
export const GET_PAYMENTLIST_LOAD_MORE = 'GET_PAYMENTLIST_LOAD_MORE'
export const GET_PAYMENTLIST_NO_MORE = 'GET_PAYMENTLIST_NO_MORE'
export const GET_PAYMENTLIST_FAIL = 'GET_PAYMENTLIST_FAIL'

export const DEPOSIT_RECORD = 'DEPOSIT_RECORD'
export const DEPOSIT_RECORD_FAIL = 'DEPOSIT_RECORD_FAIL'
export const DEPOSIT_RECORD_SUCCESS = 'DEPOSIT_RECORD_SUCCESS'

export const EXTRACT_RECORD = 'EXTRACT_RECORD'
export const EXTRACT_RECORD_FAIL = 'EXTRACT_RECORD_FAIL'
export const EXTRACT_RECORD_SUCCESS = 'EXTRACT_RECORD_SUCCESS'

export const ORDER_RECORD = 'ORDER_RECORD'
export const ORDER_RECORD_LOAD = 'ORDER_RECORD_LOAD'
export const ORDER_RECORD_FAIL = 'ORDER_RECORD_FAIL'
export const ORDER_RECORD_SUCCESS = 'ORDER_RECORD_SUCCESS'
export const ORDER_RECORD_LOAD_MORE = 'ORDER_RECORD_LOAD_MORE'
export const ORDER_RECORD_NO_MORE = 'ORDER_RECORD_NO_MORE'

export const GET_COINTYPES = 'GET_COINTYPES'
export const GET_COINTYPES_FAIL = 'GET_COINTYPES_FAIL'
export const GET_COINTYPES_SUCCESS = 'GET_COINTYPES_SUCCESS'

export const WALLET_PAY = 'WALLET_PAY'
export const WALLET_PAY_FAIL = 'WALLET_PAY_FAIL'
export const WALLET_PAY_SUCCESS = 'WALLET_PAY_SUCCESS'

export const WALLET_TRANSFER = 'WALLET_TRANSFER'
export const WALLET_TRANSFER_FAIL = 'WALLET_TRANSFER_FAIL'
export const WALLET_TRANSFER_SUCCESS = 'WALLET_TRANSFER_SUCCESS'

export const WALLET_EXTRACT = 'WALLET_EXTRACT'
export const WALLET_EXTRACT_FAIL = 'WALLET_EXTRACT_FAIL'
export const WALLET_EXTRACT_SUCCESS = 'WALLET_EXTRACT_SUCCESS'

export const ORDER_PAY = 'ORDER_PAY'
export const ORDER_PAY_FAIL = 'ORDER_PAY_FAIL'
export const ORDER_PAY_SUCCESS = 'ORDER_PAY_SUCCESS'

export const GET_DEPOSIT_ADDRESS = 'GET_DEPOSIT_ADDRESS'
export const GET_DEPOSIT_ADDRESS_FAIL = 'GET_DEPOSIT_ADDRESS_FAIL'
export const GET_DEPOSIT_ADDRESS_SUCCESS = 'GET_DEPOSIT_ADDRESS_SUCCESS'

export const GET_PRIMARY_ACCOUNT = 'GET_PRIMARY_ACCOUNT'
export const GET_PRIMARY_ACCOUNT_FAIL = 'GET_PRIMARY_ACCOUNT_FAIL'
export const GET_PRIMARY_ACCOUNT_SUCCESS = 'GET_PRIMARY_ACCOUNT_SUCCESS'

export const SET_PRIMARY_ACCOUNT = 'SET_PRIMARY_ACCOUNT'
export const SET_PRIMARY_ACCOUNT_FAIL = 'SET_PRIMARY_ACCOUNT_FAIL'
export const SET_PRIMARY_ACCOUNT_SUCCESS = 'SET_PRIMARY_ACCOUNT_SUCCESS'

export const CREATE_ACCOUNT = 'CREATE_ACCOUNT'
export const CREATE_ACCOUNT_FAIL = 'CREATE_ACCOUNT_FAIL'
export const CREATE_ACCOUNT_SUCCESS = 'CREATE_ACCOUNT_SUCCESS'

export const DELETE_ACCOUNT= 'DELETE_ACCOUNT'
export const DELETE_ACCOUNT_FAIL = 'DELETE_ACCOUNT_FAIL'
export const DELETE_ACCOUNT_SUCCESS = 'DELETE_ACCOUNT_SUCCESS'

export const GET_ACCOUNT_BYCOINTYPE = 'GET_ACCOUNT_BYCOINTYPE'
export const GET_ACCOUNT_BYCOINTYPE_FAIL = 'GET_ACCOUNT_BYCOINTYPE_FAIL'
export const GET_ACCOUNT_BYCOINTYPE_SUCCESS = 'GET_ACCOUNT_BYCOINTYPE_SUCCESS'

export const getAccounts = createAction(GET_ACCOUNTS)
export const getAccountsSuccess = createAction(GET_ACCOUNTS_SUCCESS)
export const getAccountsFail = createAction(GET_ACCOUNTS_FAIL)

export const getAccountPayment = createAction(GET_ACCOUNT_PAYMENT)
export const getAccountPaymentLoadMore = createAction(GET_ACCOUNT_PAYMENT_LOAD_MORE)
export const getAccountPaymentNoMore = createAction(GET_ACCOUNT_PAYMENT_NO_MORE)
export const getAccountPaymentSuccess = createAction(GET_ACCOUNT_PAYMENT_SUCCESS)

export const getPaymentList = createAction(GET_PAYMENTLIST)
export const getPaymentListSuccess = createAction(GET_PAYMENTLIST_SUCCESS)
export const getPaymentListLoadMore = createAction(GET_PAYMENTLIST_LOAD_MORE)
export const getPaymentListNoMore = createAction(GET_PAYMENTLIST_NO_MORE)

export const getPaymentListFail = createAction(GET_PAYMENTLIST_FAIL)

export const getExtractRecord = createAction(EXTRACT_RECORD)
export const getExtractRecordSuccess = createAction(EXTRACT_RECORD_SUCCESS)
export const getExtractRecordFail = createAction(EXTRACT_RECORD_FAIL)

export const getDepositRecord = createAction(DEPOSIT_RECORD)
export const getDepositRecordFail = createAction(DEPOSIT_RECORD_FAIL)
export const getDepositRecordSuccess = createAction(DEPOSIT_RECORD_SUCCESS)

export const getOrderRecord = createAction(ORDER_RECORD)
export const getOrderRecordLoad = createAction(ORDER_RECORD_LOAD)
export const getOrderRecordFail = createAction(ORDER_RECORD_FAIL)
export const getOrderRecordSuccess = createAction(ORDER_RECORD_SUCCESS)
export const getOrderRecordLoadMore = createAction(ORDER_RECORD_LOAD_MORE)
export const getOrderRecordNoMore = createAction(ORDER_RECORD_NO_MORE)


export const getCointypes = createAction(GET_COINTYPES)
export const getCointypesSuccess = createAction(GET_COINTYPES_SUCCESS)
export const getCointypesFail = createAction(GET_COINTYPES_FAIL)

export const walletPay = createAction(WALLET_PAY)
export const walletPayFail = createAction(WALLET_PAY_FAIL)
export const walletPaySuccess = createAction(WALLET_PAY_SUCCESS)

export const walletTransfer = createAction(WALLET_TRANSFER)
export const walletTransferFail = createAction(WALLET_TRANSFER_FAIL)
export const walletTransferSuccess = createAction(WALLET_TRANSFER_SUCCESS)

export const walletExtract = createAction(WALLET_EXTRACT)
export const walletExtractFail = createAction(WALLET_EXTRACT_FAIL)
export const walletExtractSuccess = createAction(WALLET_EXTRACT_SUCCESS)

export const orderPay = createAction(ORDER_PAY)
export const orderPayFail = createAction(ORDER_PAY_FAIL)
export const orderPaySuccess = createAction(ORDER_PAY_SUCCESS)

export const getDepositAddress = createAction(GET_DEPOSIT_ADDRESS)
export const getDepositAddressFail = createAction(GET_DEPOSIT_ADDRESS_FAIL)
export const getDepositAddressSuccess = createAction(GET_DEPOSIT_ADDRESS_SUCCESS)

export const getPrimaryAccount = createAction(GET_PRIMARY_ACCOUNT)
export const getPrimaryAccountFail = createAction(GET_PRIMARY_ACCOUNT_FAIL)
export const getPrimaryAccountSuccess = createAction(GET_PRIMARY_ACCOUNT_SUCCESS)

export const createAccount = createAction(CREATE_ACCOUNT)
export const createAccountFail = createAction(CREATE_ACCOUNT_FAIL)
export const createAccountSuccess = createAction(CREATE_ACCOUNT_SUCCESS)

export const deleteAccount = createAction(DELETE_ACCOUNT)
export const deleteAccountFail = createAction(DELETE_ACCOUNT_FAIL)
export const deleteAccountSuccess = createAction(DELETE_ACCOUNT_SUCCESS)

export const setPrimaryAccount = createAction(SET_PRIMARY_ACCOUNT)
export const setPrimaryAccountFail = createAction(SET_PRIMARY_ACCOUNT_FAIL)
export const setPrimaryAccountSuccess = createAction(SET_PRIMARY_ACCOUNT_SUCCESS)

export const getAccountByCoinType = createAction(GET_ACCOUNT_BYCOINTYPE)
export const getAccountByCoinTypeFail = createAction(GET_ACCOUNT_BYCOINTYPE_FAIL)
export const getAccountByCoinTypeSuccess = createAction(GET_ACCOUNT_BYCOINTYPE_SUCCESS)