import { createAction } from '../../helpers/createAction'

export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGIN_SETAUTHINFO = 'LOGIN_SETAUTHINFO'

export const LOGOUT = 'LOGOUT'
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const LOGOUT_FAIL = 'LOGOUT_FAIL'

export const SEND_EMAIL_CODE = 'SEND_EMAIL_CODE'
export const SEND_PHONE_CODE = 'SEND_PHONE_CODE'
export const SEND_AUTH_CODE_SUCCESS = 'SEND_AUTH_CODE_SUCCESS'

export const REGISTER_AUTH = 'REGISTER_AUTH'
export const VERIFYAUTHCODE = 'VERIFYAUTHCODE'
export const RETRIEVE_PASSWORD = 'RETRIEVE_PASSWORD'
export const USERNAME_EXIST = 'USERNAME_EXIST'

export const SIGN_LOGIN_FEIBA = 'SIGN_LOGIN_FEIBA'
export const SIGN_LOGIN_FEIBA_FAIL = 'SIGN_LOGIN_FEIBA_FAIL'
export const SIGN_LOGIN_FEIBA_SUCCESS = 'SIGN_LOGIN_FEIBA_SUCCESS'

export const SIGN_LOGIN_BDD = 'SIGN_LOGIN_BDD'
export const SIGN_LOGIN_BDD_FAIL = 'SIGN_LOGIN_BDD_FAIL'
export const SIGN_LOGIN_BDD_SUCCESS = 'SIGN_LOGIN_BDD_SUCCESS'

export const GET_APPLICTATION_LIST = 'GET_APPLICTATION_LIST'
export const GET_APPLICTATION_LIST_SUCCSEE = 'GET_APPLICTATION_LIST_SUCCSEE'
export const GET_APPLICTATION_LIST_FAIL = 'GET_APPLICTATION_LIST_FAIL'

export const GET_BANNER_LIST = 'GET_BANNER_LIST'
export const GET_BANNER_LIST_SUCCESS = 'GET_BANNER_LIST_SUCCESS'
export const GET_ADVERT_LIST_SUCCESS = 'GET_ADVERT_LIST_SUCCESS'
export const GET_BANNER_LIST_FAIL = 'GET_BANNER_LIST_FAIL'
export const GET_ADVERT_LIST_FAIL = 'GET_ADVERT_LIST_FAIL'


export const login = createAction(LOGIN)
export const loginSuccess = createAction(LOGIN_SUCCESS)
export const loginFail = createAction(LOGIN_FAIL)
export const loginSetAuthInfo = createAction(LOGIN_SETAUTHINFO)

export const logout = createAction(LOGOUT)
export const logoutSuccess = createAction(LOGOUT_SUCCESS)
export const logoutFail = createAction(LOGOUT_FAIL)

export const sendEmailAuthCode = createAction(SEND_EMAIL_CODE)
export const sendPhoneAuthCode = createAction(SEND_PHONE_CODE)
export const sendAuthCodeSuccess = createAction(SEND_AUTH_CODE_SUCCESS)
export const usernameExist = createAction(USERNAME_EXIST)

export const registerAuth = createAction(REGISTER_AUTH)
export const verifyAuthCode = createAction(VERIFYAUTHCODE)
export const retrievePassword = createAction(RETRIEVE_PASSWORD)

export const signLoginFeiba = createAction(SIGN_LOGIN_FEIBA)
export const signLoginFeibaSuccess = createAction(SIGN_LOGIN_FEIBA_SUCCESS)
export const signLoginFeibaFail = createAction(SIGN_LOGIN_FEIBA_FAIL)

export const signLoginBdd = createAction(SIGN_LOGIN_BDD)
export const signLoginBddSuccess = createAction(SIGN_LOGIN_BDD_SUCCESS)
export const signLoginBddFail = createAction(SIGN_LOGIN_BDD_FAIL)

export const getApplicationList = createAction(GET_APPLICTATION_LIST)
export const getApplicationListSuccess = createAction(GET_APPLICTATION_LIST_SUCCSEE)
export const getApplicationListFail = createAction(GET_APPLICTATION_LIST_FAIL)

export const getBannerList = createAction(GET_BANNER_LIST)
export const getBannerListSuccess = createAction(GET_BANNER_LIST_SUCCESS)
export const getBannerListFail = createAction(GET_BANNER_LIST_FAIL)
export const getAdvertListSuccess = createAction(GET_ADVERT_LIST_SUCCESS)
export const getAdvertListFail = createAction(GET_ADVERT_LIST_FAIL)
