import {createAction} from '../../helpers/createAction'

export const SET_LANGUAGE = 'SET_LANGUAGE'
export const SET_REDENVELOPE = 'SET_REDENVELOPE'

export const setLanguage = createAction(SET_LANGUAGE)
export const setRedEnvelope =createAction(SET_REDENVELOPE)