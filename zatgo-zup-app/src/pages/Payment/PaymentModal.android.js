import React, { Component } from 'react';
import {
  View,
  Text,
  Modal,
  Image,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';

import I18n from 'I18n';
import { window } from '@zatgo/constants';
import PinInput from '@zatgo/components/PinInput';

const PaymentModal = (props) => (
  <Modal
    animationType='slide'
    transparent={true}
    visible={props.visible}
    onRequestClose={props.onClose}
  >
    <View style={styles.container}>
      <View style={styles.mainContainer}>

        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={props.onClose}>
            <Image
              style={styles.closeIcon}
              source={require('@zatgo/assets/common/com_close.png')}
            />
          </TouchableOpacity>

          <Text style={styles.font14}>
            {I18n.t('pay_password')}
          </Text>
          <View />
        </View>

        <Text style={styles.amountTxt}>
          {props.amount ? props.amount : ''}
          <Text style={styles.currencyTxt}>
            {props.coinType ? props.coinType : ''}
          </Text>
        </Text>
        <KeyboardAvoidingView behavior='padding' style={styles.inputContainer}>
          <PinInput
            size={50}
            space={0}
            autoFocus={false}
            borderType='square'
            inputPosition='center'
            activeColor='rgba(211, 211, 211, 1)'
            codeInputStyle={styles.codeInputStyle}
            onFulfill={props.onInputCompleted}
            secureTextEntry
          />
        </KeyboardAvoidingView>
      </View>
    </View>
  </Modal>
)

export default PaymentModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  mainContainer: {
    backgroundColor: 'white',
    alignItems: 'center'
  },
  headerContainer: {
    height: 44,
    width: window.width,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: '#d4d4d4',
  },
  font14: {
    color: '#323B43',
    fontSize: 15 * window.fontScale
  },
  inputContainer: {
    marginTop: 20, marginBottom: 35
  },
  codeInputStyle: {
    fontSize: 30 * window.fontScale,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderColor: 'rgba(211, 211, 211, 1)'
  },
  amountTxt: {
    fontSize: 34 * window.fontScale,
    color: '#323B43',
    marginTop: 20
  },
  currencyTxt: {
    fontSize: 20 * window.fontScale,
    color: '#323B43'
  },
  closeIcon: {
    width: 15,
    height: 15,
    marginLeft: 15
  }
});
