import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import I18n from 'I18n';
import { window } from '@zatgo/constants';

class PaymentSuccessPage extends Component {
  onPressViewLog = () => {
    Actions.replace('paymentRecords');
  }

  render() {
    const { result, resultCode } = this.props;
    return (
      <View style={styles.container}>
        <Image
          style={styles.logoImage}
          source={require('../../assets/tabbar/ZAT.png')} />
        <Text style={styles.successTxt}>
          {resultCode && resultCode === "PROCESSING" ? I18n.t('pay_process') : I18n.t('pay_success')}
        </Text>
        <Text style={styles.amountTxt}>
          {`${result.amount}`}<Text style={styles.currencyTxt}>{`${result.coinType}`}</Text>
        </Text>
        <TouchableOpacity activeOpacity={0.6} style={styles.viewButton} onPress={this.onPressViewLog}>
          <Text style={styles.buttonTxt}> {I18n.t('pay_viewlog')}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default PaymentSuccessPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  logoImage: {
    width: 100,
    height: 100,
    marginTop: 20,
  },
  viewButton: {
    borderWidth: 1,
    height: 40,
    marginTop: 78,
    alignItems: 'center',
    justifyContent: 'center'
  },
  successTxt: {
    color: '#323B43',
    fontSize: 18 * window.fontScale,
    marginTop: 10,
    marginBottom: 10
  },
  amountTxt: {
    color: '#323B43',
    fontSize: 36 * window.fontScale
  },
  currencyTxt: {
    color: '#323B43',
    fontSize: 16 * window.fontScale
  },
  buttonTxt: {
    marginLeft: 30, marginRight: 30
  }
});
