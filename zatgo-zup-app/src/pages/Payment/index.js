import PaymentPage from './PaymentPage'
import PaymentSuccessPage from './PaymentSuccessPage'

export {
  PaymentPage,
  PaymentSuccessPage
}