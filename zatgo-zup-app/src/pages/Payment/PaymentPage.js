import React, { Component } from 'react';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Toast } from 'antd-mobile';

import I18n from 'I18n';
import { window } from '@zatgo/constants';
import PinInput from '@zatgo/components/PinInput';
import NumberInput from '@zatgo/components/NumberInput';
import PaymentModal from './PaymentModal';
import { walletActions, userActions } from '../../redux/actions';

const UserIcon = require('../../assets/tabbar/User.png');

const Avatar = (props) => (
  <View style={styles.avatar} >
    <Image resizeMode='stretch' source={props.source} />
  </View>
)

const Button = (props) => (
  <TouchableOpacity
    activeOpacity={0.6}
    onPress={props.onPress}
    style={{ flexDirection: 'row' }}>
    <LinearGradient
      style={styles.payButton}
      colors={['#4DCC7B', '#12A0D2']}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
    >
      <Text style={styles.payTxt}>{props.text}</Text>
    </LinearGradient>
  </TouchableOpacity>
)

class PaymentPage extends Component {
  state = {
    modalVisible: false,
    payPassword: null,
    amount: null,
  }

  componentDidMount() {
    // qrFrom 1pc收银台 2 收款码 3 充值（转入）
    const { qrData, getUserById, getAccountByCoinType } = this.props;
    getAccountByCoinType(qrData.coinType);
    getUserById(qrData.receiptUserId)
  }

  onChangeAmount = (value) => {
    let amount = value.replace(/\s+/g, "");
    this.setState({ amount });
  }

  payForOrder = (payPassword) => {
    const { qrData, orderPay, userInfo } = this.props;
    const params = {
      "amount": qrData.amount,
      "coinType": qrData.coinType,
      "networkType": qrData.networkType,
      "orderId": qrData.orderId,
      "receiptUserId": qrData.receiptUserId,
      "receiptUserName": userInfo.userName,
      "sign": qrData.sign,
      "payPassword": payPassword
    }
    orderPay(params);
  }

  payForTransfer = (payPassword) => {
    const { amount } = this.state;
    const { qrData, walletTransfer, userInfo } = this.props;

    const params = {
      "amount": amount,
      "coinType": qrData.coinType,
      "networkType": qrData.networkType,
      "receiptUserId": qrData.receiptUserId,
      "receiptUserName": userInfo.userName,
      "sign": qrData.sign,
      "payPassword": payPassword
    }
    walletTransfer(params);
  }

  onPressPay = () => {
    const { qrFrom, authInfo, qrData, userInfo } = this.props;
    if (authInfo.userId === qrData.receiptUserId) {
      Toast.fail(I18n.t('toast_transfer_self_err'))
      return;
    }
    if (!userInfo || !userInfo.userName) {
      Toast.fail(I18n.t('toast_account_not_exist'))
      return;
    }
    const { amount } = this.state;
    if (qrFrom === 1 || amount) {
      this.setState({ modalVisible: true })
    } else {
      Toast.info(I18n.t('toast_please_enter_amount'))
    }
  }

  setModalClose = () => {
    this.setState({ modalVisible: false })
  }

  onInputCompleted = (payPassword) => {
    const { qrFrom } = this.props;
    if (qrFrom === 1) {
      this.payForOrder(payPassword)
    } else if (qrFrom === 2) {
      this.payForTransfer(payPassword)
    } else if (qrFrom === 3) { }

    this.setState({ modalVisible: false })
  }

  render() {
    const { qrFrom, qrData, userInfo, currentAccount } = this.props;
    return (
      <View style={styles.container}>
        <Avatar source={UserIcon} />
        <Text style={[styles.font15, { marginTop: 15 }]}>
          {I18n.t('pay_to_account')}
        </Text>
        <Text style={[styles.font15, { marginTop: 5 }]}>
          {userInfo ? userInfo.userName : ''}
        </Text>

        <View style={styles.mainLayout}>
          <Text style={[styles.font14, { marginBottom: 12, marginTop: 20 }]}>
            {I18n.t('pay_amount')}
          </Text>
          <View style={styles.txtInputContainer}>
            <Image
              resizeMode='stretch'
              source={{ uri: currentAccount.coinImage }}
              style={styles.currencyIcon}
            />
            <NumberInput
              defaultValue={qrFrom === 1 ? qrData.amount : null}
              onChangeText={this.onChangeAmount}
              editable={qrFrom !== 1}
              style={styles.textInput}
              fontSize={36 * window.fontScale}
            />
          </View>
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <Text style={styles.font14}>
              {I18n.t('pay_banlance')}
            </Text>
            <Text style={[styles.font14, { marginLeft: 5 }]}>
              {currentAccount.balance ? currentAccount.balance : 0}
              {currentAccount.coinType ? ` ${currentAccount.coinType}` : ` ${qrData.coinType}`}
            </Text>
          </View>

        </View>

        <Button
          onPress={this.onPressPay}
          text={I18n.t('pay_button_txt')}
        />
        <PaymentModal
          visible={this.state.modalVisible}
          onClose={this.setModalClose}
          amount={this.state.amount}
          coinType={qrData.coinType}
          onInputCompleted={this.onInputCompleted}
        />
      </View>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  getUserById: (id) => {
    return dispatch(userActions.getUserById(id));
  },

  orderPay: (params) => {
    return dispatch(walletActions.orderPay(params));
  },

  walletTransfer: (params) => {
    return dispatch(walletActions.walletTransfer(params));
  },

  getAccountByCoinType: (coinType) => {
    return dispatch(walletActions.getAccountByCoinType(coinType));
  }
})


const mapStateToProps = (state, ownProps) => ({
  userInfo: state.user.userInfo,
  authInfo: state.user.authInfo,
  currentAccount: state.wallet.currentPayAccount ? state.wallet.currentPayAccount : {},
})

export default connect(mapStateToProps, mapDispatchToProps)(PaymentPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  avatar: {
    width: 60,
    height: 60,
    marginTop: 20,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#D9D9D9',
    overflow: 'hidden',
  },
  mainLayout: {
    alignItems: 'flex-start',
    marginLeft: 15,
    marginRight: 15
  },
  txtInputContainer: {
    height: 62,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 1
  },
  textInput: {
    flex: 1,
    padding: 0,
    height: 62,
    fontSize: 36 * window.fontScale
  },
  currencyIcon: {
    width: 30,
    height: 30,
    marginRight: 10
  },
  payButton: {
    flex: 1,
    height: 45,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 28,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  payTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  },
  font14: {
    color: '#323B43',
    fontSize: 15 * window.fontScale
  },
  font15: {
    color: '#323B43',
    fontSize: 15 * window.fontScale
  },
});
