/*
 *  @File   : RichScanPage
 *  @Author : lsl
 *  @Date   : 2018-4-3 15:12:47
 *  @Last Modified   : 2018-4-3 15:12:47
 *  @Desc 二维码扫码
 */
import React, { PureComponent } from 'react';
import {Platform, NativeModules, Alert, Linking} from 'react-native'
import { Actions } from 'react-native-router-flux';
import QRScannerView from '@zatgo/components/QRScanner';
import I18n from 'I18n';

class RichScanPage extends PureComponent {
  state = {
    transCode: null
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      NativeModules.NativeManager.authStatus(1, (error, data) => {
        if (data !== 'yes') {
          Alert.alert('',
              I18n.t('alert_qrcode_permissions_camera'), [
                {
                  text: I18n.t('alert_cancel')
                }, {
                  text: I18n.t('alert_ok'), onPress: () => {
                    Linking.openURL(data)
                  }
                }
              ]);
        }
      })
    }
  }

  render() {
    return (
      <QRScannerView
        hintText={I18n.t('scan_prompt')}
        onScanResultReceived={this.barcodeReceived} />
    );
  }

  barcodeReceived = (e) => {
    let qrData = e.data;
    if (qrData !== this.transCode) {
      this.transCode = e.data; // 放在this上，防止触发多次
      const { qrFrom } = this.props;
      if ((qrFrom && qrFrom === 3)) {
        Actions.pop({ refresh: ({ qrData }) })
      } else if (qrData.indexOf('channel') > -1 && qrData.indexOf('ZatGoPay') > -1 && qrData.indexOf('device') > -1) {
        const json = JSON.parse(qrData)
        if (json.device === 1) {
          // qrFrom 1pc收银台 2 收款码 3 提取
          Actions.replace('payment', { qrFrom: 1, qrData: json });
        } else if (json.device === 2 || json.device === 3) {
          Actions.replace('payment', { qrFrom: 2, qrData: json });
        }
      } else {
        Actions.replace('scanResult', { qrData })
      }
    }
  }
}

export default RichScanPage;
