/*
 *  @File   : RichScanResultPage
 *  @Author : lsl
 *  @Date   : 2018-4-4 10:25:43
 *  @Last Modified   : 2018-4-4 10:25:43
 *  @Desc 展示普通扫码结果
 */
import React, { Component } from 'react';
import { View, Text, } from 'react-native';

class RichScanResultPage extends Component {
  render() {
    return (
      <View style={{ flex: 1,margin:10 }}>
        <Text selectable={true}> {this.props.qrData} </Text>
      </View>
    );
  }
}

export default RichScanResultPage;
