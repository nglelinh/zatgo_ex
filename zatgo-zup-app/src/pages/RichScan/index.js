import RichScanPage from './RichScanPage'
import RichScanResultPage from './RichScanResultPage'

export {
  RichScanPage,
  RichScanResultPage
}