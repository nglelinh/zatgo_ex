import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import I18n from 'I18n';
import window from '@zatgo/constants/window';
import CusTomsImage from '../../../components/CustomImage/customImage'

const ImageIcon = ({ icon, text, type = 'md', isOpen = false, onClick }) => {
  return onClick ?
      <TouchableOpacity
          style={styles.container}
          onPress={() => onClick()}
      >
        <CusTomsImage
            style={type == 'xs' ? styles.icon20 : styles.icon30}
            defaultStyle={type == 'xs' ? styles.icon20 : styles.icon30}
            uri={icon ? icon : ''}/>
        <Text style={[type == 'xs' ? styles.font12 : styles.font13, {textAlign: 'center'}]}
              numberOfLines={1}
        >
          {text}
        </Text>
      </TouchableOpacity>
      :
      <View style={styles.itemWithOpenState}>
        {
          isOpen ? null : <Text style={styles.openTxt}> </Text>
        }
        <View style={styles.middleLayout}>
          <CusTomsImage
              style={type == 'xs' ? styles.icon20 : styles.icon30}
              defaultStyle={type == 'xs' ? styles.icon20 : styles.icon30}
              uri={icon ? icon : ''}/>
          <Text style={[type == 'xs' ? styles.font12 : styles.font13, {textAlign: 'center'}]}
                numberOfLines={1}
          >
            {text}
          </Text>
        </View>
        {
          isOpen ? null :
              <Text style={styles.openTxt}>
                {I18n.t('home_open_soon')}
              </Text>
        }
      </View>
}

export default ImageIcon;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  font12: {
    color: '#7E95A8',
    marginTop: 8,
    fontSize: 12 * window.fontScale,
  },
  font13: {
    fontSize: 13 * window.fontScale,
    color: '#323B43',
    marginTop: 10,
  },
  icon20: {
    width: 26,
    height: 26,
    borderRadius: 13
  },
  icon30: {
    width: 30,
    height: 30
  },
  openTxt: {
    color: '#A0A4A8',
    fontSize: 10 * window.fontScale,
    marginTop: 5,
  },
  itemWithOpenState: {
    flex: 1,
    padding: 5,
    marginTop:10,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  middleLayout: {
    alignItems: 'center',
    justifyContent: 'center'
  }
})