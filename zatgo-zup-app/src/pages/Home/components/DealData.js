/**
 * Created by zhoujianxin on 2019/3/4.
 * @Desc
 */
import {FeibaUtils} from '../../../utils'


const dealRequestData = (authInfo, item, sort,index) => {
  if(item&&item.appAuth){
    const params = {
      "tmcCode": "ZATGO",
      "corpCode": "ZAT",
      "sign": "123456",
      "relationUserId": authInfo.userId,
      "contact": authInfo.userName,
    }
    FeibaUtils.post(item.appAuth, params).then(res => {
      console.log(`${item.appGotoAddress}&token=${res.data.token}`)
      global.tokenData[sort][index]= `${item.appGotoAddress}&token=${res.data.token}`
    }).catch(error => {
      global.tokenData[sort][index] = null;
    })
  }
  global.tokenData[sort][index] = null
}

const queryString = (url, params) => {
  if (params && url) {
    let paramsArray = [];
    Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
    if (url.search(/\?/) === -1) {
      url += '?' + paramsArray.join('&')
    } else {
      url += '&' + paramsArray.join('&')
    }
  }
  return url;
}

export default {
  dealRequestData
}