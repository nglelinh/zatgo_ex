/**
 * Created by zhoujianxin on 2018/12/28.
 * @Desc
 */

import React, { Component } from 'react';
import { connect } from "react-redux";
import {
  Text,
  View,
  Image,
  Modal,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  DeviceEventEmitter
} from 'react-native';
import I18n from 'I18n';
import { EncryptUtils, } from "@zatgo/utils";
import window from "@zatgo/constants/window";
import WebView from '@zatgo/components/WebView';
import ExtensionModule from "@zatgo/components/ExtensionModule";
import LoadingView from './LoadingView';

class ActiveModel extends Component {
  state = {
    loading: false,
    loadError: false,
  }

  onClose = () => {
    DeviceEventEmitter.emit('RefreshTask'); //发监听
    this.setState({ loadError: false })
    global.closeModel();
  };

  getQueryString(url, token) {
    return ExtensionModule.queryString(url, {
      'webSign': EncryptUtils.webMd5(),
      zatgoToken: token,
      cloudUserId: '65c2c3a074774f8090a1e4544a56632a'
    });
  }

  render() {
    const { dataURL, authInfo, visible } = this.props;
    const token = authInfo.token ? authInfo.token : ''
    let sourceURL = dataURL && dataURL.localtionUrl ? this.getQueryString(dataURL.localtionUrl, token) : '';

    let NavView = ExtensionModule.GetQueryString(sourceURL, 'webviewTitleInvisible');
    console.log('activeState', visible)
    return (
      <View>
        <Modal
          transparent
          visible={visible ? visible : false}
          onRequestClose={this.onClose}
        >
          <View style={styles.actContainer}>
            <View style={{ width: window.width, height: window.height, borderRadius: 10, }}>
              {!this.state.loadError ?
                <WebView
                  originWhitelist={['*']}
                  ref={(webview) => { this.web = webview }}
                  style={{
                    width: window.width,
                    borderRadius: 10,
                    backgroundColor: 'rgba(0,0,0,0.2)',
                  }}
                  onMessage={(event) => {
                    if (event && event.nativeEvent && event.nativeEvent.data) {
                      if (event.nativeEvent.data === 'closeRedEnvelope') {
                        this.onClose()
                      }
                    }
                  }}
                  mixedContentMode={'always'}
                  startInLoadingState
                  source={{ uri: sourceURL }}
                  bounces={false}
                  javaScriptEnabled={true}
                  androiddomStorageEnabled={false}
                  renderError={() => {
                    console.log('renderError')
                    return (
                      <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'rgba(0,0,0,0.4)',
                      }}>
                        <Text style={{ color: 'white' }}>{I18n.t('web_error')}</Text>
                      </View>
                    )
                  }}
                  renderLoading={() => {
                    return (
                      <View style={styles.loadingView}>
                        <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.onClose}>
                          <LoadingView />
                          <Text style={{ margin: 5 }}>点击动画关闭</Text>
                        </TouchableOpacity>
                      </View>
                    )
                  }}
                  onLoadEnd={() => { console.log('页面加载结束') }}
                  onError={() => {
                    console.log('页面加载错误')
                    this.setState({ loadError: true })
                  }}
                /> : null}
            </View>
            {!NavView ?
              <TouchableOpacity style={styles.closeTouchStyle} onPress={this.onClose}>
                <Image source={require('@zatgo/assets/home/close.png')}
                  style={styles.closeImageStyle} />
              </TouchableOpacity>
              : null
            }

            {this.state.loadError ?
              <View style={styles.loadStartView}>
                <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.onClose}>
                  <LoadingView />
                </TouchableOpacity>
                {this.state.loadError ? <Text style={{ margin: 5 }}>网页加载错误,点击动画关闭</Text> : null}
              </View>
              : null
            }
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  authInfo: state.user.authInfo,
})

export default connect(mapStateToProps)(ActiveModel)

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  actContainer: {
    minWidth: window.width,
    height: window.height,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  closeTouchStyle: {
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
    top: window.isIphoneX ? 50 : 30,
    bottom: 0,
    right: 10,
    position: 'absolute',
    overflow: 'hidden',
    borderRadius: 18,
    backgroundColor: 'rgba(0,0,0,0.4)',

  },
  closeImageStyle: {
    backgroundColor: 'transparent',
    width: 30,
    height: 30,
    borderRadius: 15,
  },
  loadingView: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  loadStartView: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  }

});