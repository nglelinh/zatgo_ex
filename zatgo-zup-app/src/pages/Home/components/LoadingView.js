/**
 * Created by zhoujianxin on 2019/3/7.
 * @Desc
 */

import React, { Component } from 'react';
import {
  Animated,
  Easing,
  Image,
  StyleSheet
} from 'react-native';

const image = [
  require('@zatgo/assets/loading/loading1.png'),
  require('@zatgo/assets/loading/loading2.png'),
  require('@zatgo/assets/loading/loading3.png'),
  require('@zatgo/assets/loading/loading4.png'),
  require('@zatgo/assets/loading/loading5.png'),
  require('@zatgo/assets/loading/loading6.png'),
  require('@zatgo/assets/loading/loading7.png'),
  require('@zatgo/assets/loading/loading8.png'),
  require('@zatgo/assets/loading/loading9.png'),
  require('@zatgo/assets/loading/loading10.png'),
  require('@zatgo/assets/loading/loading11.png'),
  require('@zatgo/assets/loading/loading12.png'),
  require('@zatgo/assets/loading/loading13.png'),
  require('@zatgo/assets/loading/loading14.png'),
  require('@zatgo/assets/loading/loading15.png'),
  require('@zatgo/assets/loading/loading16.png'),
  require('@zatgo/assets/loading/loading17.png'),
  require('@zatgo/assets/loading/loading18.png'),
  require('@zatgo/assets/loading/loading19.png'),
  require('@zatgo/assets/loading/loading20.png'),
  require('@zatgo/assets/loading/loading21.png'),
  require('@zatgo/assets/loading/loading22.png'),
  require('@zatgo/assets/loading/loading23.png'),
  require('@zatgo/assets/loading/loading24.png'),
  require('@zatgo/assets/loading/loading25.png'),
  require('@zatgo/assets/loading/loading26.png'),
  require('@zatgo/assets/loading/loading27.png'),
  require('@zatgo/assets/loading/loading28.png'),
  require('@zatgo/assets/loading/loading29.png'),
  require('@zatgo/assets/loading/loading30.png'),
  require('@zatgo/assets/loading/loading31.png'),
  require('@zatgo/assets/loading/loading32.png'),
  require('@zatgo/assets/loading/loading33.png'),
  require('@zatgo/assets/loading/loading34.png'),
  require('@zatgo/assets/loading/loading35.png'),
  require('@zatgo/assets/loading/loading36.png'),
  require('@zatgo/assets/loading/loading37.png'),
  require('@zatgo/assets/loading/loading38.png'),
  require('@zatgo/assets/loading/loading39.png'),
  require('@zatgo/assets/loading/loading40.png'),
  require('@zatgo/assets/loading/loading41.png'),
  require('@zatgo/assets/loading/loading42.png'),
  require('@zatgo/assets/loading/loading43.png'),
  require('@zatgo/assets/loading/loading44.png'),
  require('@zatgo/assets/loading/loading45.png'),
  require('@zatgo/assets/loading/loading46.png'),
  require('@zatgo/assets/loading/loading47.png'),
  require('@zatgo/assets/loading/loading48.png'),
  require('@zatgo/assets/loading/loading49.png'),
  require('@zatgo/assets/loading/loading50.png'),
]

export default class FrameAnimation extends Component {

  constructor(props) {
    super(props);
    this.state = {
      frameValue: new Animated.Value(1),
      imageSource: 1,
    }
    this.isGoing = false; //为真旋转
    this.myAnimate = Animated.timing(this.state.frameValue, {
      toValue: 25,
      duration: 25 * 60,
      easing: Easing.inOut(Easing.linear),
    });
  }

  componentDidMount() {
    this.startAnimation(25, 25 * 60);
    this.state.frameValue.addListener((event) => {
      console.log(event)
      let nextImage = parseInt(event.value);
      if (nextImage !== 0 && nextImage !== this.state.imageSource) {
        this.setState({
          imageSource: parseInt(event.value),
        });
      }
    });

  }

  componentWillUnmount() {
    this.state.frameValue && this.state.frameValue.removeAllListeners();
    this.isGoing = false; //为真旋转
    this.state.frameValue.stopAnimation((oneTimeRotate) => {
      //计算角度比例
      this.myAnimate = Animated.timing(this.state.frameValue, {
        toValue: 25,
        duration: 25 * 60,
        easing: Easing.inOut(Easing.linear),
      });
    });
  }


  startAnimation(toValue, duration) {
    this.isGoing = !this.isGoing;
    this.myAnimate.start(() => {
      console.log('start');
      this.myAnimate = Animated.timing(this.state.frameValue, {
        toValue: 25,
        duration: 25 * 60,
        easing: Easing.inOut(Easing.linear),
      });
      this.imgMoving()
    })
  }

  imgMoving = () => {
    if (this.isGoing) {
      this.state.frameValue.setValue(1);
      this.myAnimate.start(() => {
        console.log('start');
        this.imgMoving()
      })
    }
  }

  render() {
    let source = image[this.state.imageSource];
    return (
      <Image
        source={source}
        style={styles.img}
      />
    );
  }
}

const styles = StyleSheet.create({
  img: {
    height: 64,
    width: 64,
  },
});