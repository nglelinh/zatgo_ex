/*
 *  @File   : index
 *  @Author : lsl
 *  @Date   : 2018-3-5 17:26:33
 *  @Last Modified   : 2018-3-5 17:26:33
 *  @Desc 首页
 */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Grid } from 'antd-mobile';
import Carousel from 'react-native-banner-carousel';
import I18n from 'I18n';
import ImageIcon from './ImageIcon';
import CustomsImageIcon from './CustomsImageIcon';
import { window, theme } from '@zatgo/constants';
import CustomImage from '../../../components/CustomImage/customImage'
import { PullView } from '../../../components/PullToRefreshLayout'
import NavBar from '../../../components/NavBar'

const BannerHeight = 150;

const images = () => [
  {
    image: require('@zatgo/assets/home/Home_Banner.png'),
    url: global.feibaH5,
    key: 0
  }
];

const HomeCarousel = ({ onItemClick, data }) => {
  return (
    <View style={{ height: 200, width: window.width, justifyContent: 'center' }}>
      <Carousel
        loop
        index={0}
        autoplay
        autoplayTimeout={3000}
        activePageIndicatorStyle={{ backgroundColor: '#4DCC7B', width: 10 }}
        pageSize={window.width}>
        {data.length > 0 ? data.map((item, index) => (
          <TouchableOpacity
            key={index + 'Banner'}
            activeOpacity={1} onPress={() => {
              onItemClick(item, item)
            }}>
            <View style={styles.carouselImage}>
              <CustomImage
                style={{ width: window.width - 40, height: BannerHeight, borderRadius: 10 }}
                defaultStyle={{ width: window.width - 40, height: BannerHeight, borderRadius: 10 }}
                uri={item.imageUrl}
              />
            </View>
          </TouchableOpacity>
        )
        ) :
          <View style={styles.carouselImage}>
            <Image
              style={{ width: window.width - 40, height: BannerHeight, borderRadius: 10 }}
              source={require('@zatgo/assets/home/Home_Banner.png')}
            />
          </View>
        }
      </Carousel>
    </View>
  )
}

const GridHeader = ({ title }) => (
  <View style={styles.gridHeader}>
    <View style={styles.labeler} />
    <Text style={styles.headerTxt}>
      {title}
    </Text>
  </View>
)

const CoreFuncView = (props) => (
  <View style={styles.coreFuncContainer}>
    <ImageIcon
      type="xs"
      icon={require('@zatgo/assets/home/QRCode.png')}
      text={I18n.t('home_rich_scan')}
      onClick={props.onClickRichScan}
    />
    <ImageIcon
      type="xs"
      icon={require('@zatgo/assets/home/Collection.png')}
      text={I18n.t('home_collection')}
      onClick={props.onClickCollection}
    />
    <ImageIcon
      type="xs"
      icon={require('@zatgo/assets/home/Wallet.png')}
      text={I18n.t('home_assethub')}
      onClick={props.onClickAssetHub}
    />
  </View>
)

const GridView = ({authInfo, sort, onItemClick, title, data, type}) => (
  <Grid data={data}
    columnNum={4}
    hasLine={false}
    renderItem={(item, index) => {
      return (
          <CustomsImageIcon
              icon={item.appImage}
              text={item.appName}
              isOpen={item.status === 2}
              type={type}
          />
      )
    }
    }
    square={false}
    onClick={(item, index) => {
      console.log(global.tokenData)
      if(global.tokenData[sort][index]){
        let params = item;
        params['appAuth'] = null;
        params['appGotoAddress'] = global.tokenData[sort][index];
        onItemClick(params);
      }else {
        onItemClick(item);
      }
    }}
    itemStyle={styles.gridItemStyle}
  />
)

const ActivityView = ({ onItemClick, data }) => (
  <View style={styles.actContainer}>
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ paddingLeft: 13, }}>
      {data.length > 0 && data.map((item, index) => (
        <TouchableOpacity
          key={index + 'Banner'}
          activeOpacity={1}
          onPress={() => {
            onItemClick(item, item)
          }}>
          <View style={styles.activityStyle}>
            <View style={{ backgroundColor: 'transparent', borderRadius: 10, height: 140, }}>
              <CustomImage
                style={{ width: 220, height: 90, borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
                defaultStyle={{ width: 220, height: 90, borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
                uri={item.appImage}
              />
              <View style={{ marginLeft: 8, marginRight: 8, height: 42, justifyContent: 'center' }}>
                <Text numberOfLines={1}
                  style={{ marginLeft: 10, color: '#323B43', fontSize: 14, marginTop: 10, textAlign: 'left', }}>
                  {item.appName ? item.appName : ''}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      ))}
      <View style={{ backgroundColor: 'transparent', width: 20 }} />
    </ScrollView>
  </View>
)

const NewsView = ({ onItemClick, data }) => (
  <View style={styles.actContainer}>
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
      {data.length > 0 && data.map((item, index) => (
        <TouchableOpacity
          key={index + 'Banner'}
          activeOpacity={1}
          onPress={() => {
            onItemClick(item, item)
          }}>
          <View style={[styles.newsStyle, { marginRight: index === data.length - 1 ? 20 : 0 }]}>
            <CustomImage
              style={{ width: 40, height: 40, borderRadius: 20 }}
              defaultStyle={{ width: 40, height: 40, borderRadius: 20 }}
              uri={item.appImage}
            />
            <View style={{ margin: 5, marginTop: 13, justifyContent: 'center' }}>
              <Text numberOfLines={1}
                style={{ color: '#323B43', fontSize: 12 * window.fontScale, textAlign: 'center', }}>
                {item.appName ? item.appName : ''}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  </View>
)

const SalesView = ({authInfo, onItemClick, isAudit, title, data, sort}) => {
  if (sort === 1) {
    return (
      <View style={[styles.salesContainer, { marginBottom: 20 }]}>
        <GridView sort={sort} data={data} authInfo={authInfo} onItemClick={onItemClick} title={title}/>
      </View>
    )
  }
  if (sort === 2) {
    return (
      <View style={[styles.salesContainer, { marginBottom: 20 }]}>
        <GridHeader title={title ? title : I18n.t('home_sales')} />
        <ActivityView sort={sort} onItemClick={onItemClick} title={title} data={data} />
      </View>
    )
  }

  if (sort === 4) {
    return (
      <View style={styles.salesContainer}>
        <GridHeader title={title ? title : I18n.t('home_sales')} />
        <NewsView sort={sort} sort={sort}onItemClick={onItemClick} title={title} data={data} />
      </View>
    )
  }
  return (
    <View style={styles.salesContainer}>
      <GridHeader title={title ? title : I18n.t('home_sales')} />
      <GridView sort={sort} data={data} onItemClick={onItemClick} title={title} type={'xs'} />
    </View>
  )
}

const HomeView = (props) => {
  const { onBannerItemClick, onItemClick, isAudit, onPullRelease, applicationList, bannerList, getApplicationList, showSpinner,authInfo } = props;
  let arr = []
  return (
    <View style={styles.container}>
      <NavBar
        style={{ paddingTop: window.isIphoneX ? 46 : 26, backgroundColor: 'white' }}
        titleLayoutStyle={{ fontSize: 21, color: '#333333' }}
        showRight
        rightText={null}
        title={I18n.t('title_home')}
        rightImg={require('@zatgo/assets/home/list.png')}
        onRightClick={(data) => {
          showSpinner(data)
        }}
        statusBar={{
          barStyle: 'dark-content',
          hidden: false,
          backgroundColor: '#00000000',
        }}
      />
      <PullView style={{ backgroundColor: 'white', flex: 1, width: window.width }}
        onPullRelease={(resolve) => {
          setTimeout(() => {
            resolve();
          }, 2000);
          onPullRelease()
        }}
      >
        <HomeCarousel onItemClick={onBannerItemClick} data={bannerList} />
        {/*<CoreFuncView {...props} />*/}
        {applicationList.length > 0 ?
          <View>
            {applicationList && applicationList.map((item, index) => (
              <SalesView key={index + 'app'}
                onItemClick={onItemClick}
                isAudit={isAudit}
                sort={index + 1}
                title={item.appCategoryName}
                data={item.appDatas}
                authInfo={authInfo}
              />
            ))}
          </View>
          :
          <View style={{ height: 200, alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity
              style={{ backgroundColor: 'transparent' }}
              onPress={() => {
                getApplicationList && getApplicationList()
              }}>
              <Text style={{ color: '#797F85', fontSize: 16 }}>{I18n.t('home_refresh')}</Text>
            </TouchableOpacity>
          </View>
        }
      </PullView>
    </View>
  )
}

export default HomeView;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  coreFuncContainer: {
    height: 75,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0.5,
  },
  carouselImage: {
    width: window.width - 40,
    height: BannerHeight,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 30,
    backgroundColor: 'white',
    borderRadius: 10,
    //以下是阴影属性：
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowColor: 'black',
    //注意：这一句是可以让安卓拥有灰色阴影
    elevation: 3,
    // zIndex: Global.isIOS() ? 1 : 0
  },
  activityStyle: {
    backgroundColor: 'white',
    width: 220,
    height: 140,
    marginLeft: 7,
    marginRight: 7,
    marginBottom: 5,
    borderRadius: 10,
    //以下是阴影属性：
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    shadowColor: '#808a92',
    //注意：这一句是可以让安卓拥有灰色阴影
    elevation: 2,
    // zIndex: Global.isIOS() ? 1 : 0
  },
  newsStyle: {
    width: 90,
    height: 90,
    marginLeft: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 10,
    //以下是阴影属性：
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    shadowColor: '#808a92',
    //注意：这一句是可以让安卓拥有灰色阴影
    elevation: 2,
    // zIndex: Global.isIOS() ? 1 : 0
  },
  gridItemStyle: {
    height: undefined,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  actContainer: {
    marginTop: 3,
    backgroundColor: 'white',
  },
  salesContainer: {
    backgroundColor: 'white',
  },
  gridHeader: {
    height: 37,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  labeler: {
    width: 3,
    height: 15,
    marginLeft: 20,
    backgroundColor: '#4DCC7B',
    borderRadius: 2,
  },
  headerTxt: {
    fontSize: 18 * window.fontScale,
    fontWeight: 'bold',
    color: '#323B43',
    marginLeft: 6
  }
});