/*
 *  @File   : index
 *  @Author : lsl
 *  @Date   : 2018-3-5 17:26:33
 *  @Last Modified   : 2018-3-5 17:26:33
 *  @Desc 首页
 */
import React, { PureComponent } from 'react';
import { Text, TouchableHighlight, View, Image, StatusBar } from 'react-native'
import { connect } from 'react-redux';
import { Toast } from 'antd-mobile';
import { Actions } from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen';
import I18n from 'I18n';
import constDefines from '@zatgo/constants/constDefines';
import { WebSocketUtils, CodePushUtil } from "../../utils";
import { authActions, digActions, globalActions } from '../../redux/actions';
import ExtensionModule from '../../components/ExtensionModule';
import HomeView from './components/HomeView';
import Popover from './components/Popover'
import App from "../../../App";
import DealData from './components/DealData';

global.tokenData = {
  '1': [],
  '2': [],
  '3': [],
  '4': []
}

const cloudUserId = { 'cloudUserId': '65c2c3a074774f8090a1e4544a56632a' }
const spinnerTextArray = () => [
  {
    icon: require('@zatgo/assets/mine/mine_scan.png'),
    text: I18n.t('home_rich_scan'),
  },
  {
    icon: require('@zatgo/assets/mine/mine_collection.png'),
    text: I18n.t('home_collection'),
  },
  {
    icon: require('@zatgo/assets/mine/mine_wallet.png'),
    text: I18n.t('home_assethub'),
  },
];

class HomePage extends PureComponent {

  state = {
    isVisible: false,
    spinnerRect: {},
  }

  componentDidMount() {
    SplashScreen.hide();
    const { token } = this.props.authInfo;
    WebSocketUtils.notifWebSocket(token);
    CodePushUtil.checkForUpdate(constDefines.deployStagingKey);
    this.props.getApplicationList();
    this.props.getBannerList({ siteType: 1 });
    this.props.getHashRaseTask();
  }

  onRefreshData = () => {
    this.props.getApplicationList();
    this.props.getBannerList({ siteType: 1 });
  }

  onSignLogin = (appAuth, weburl) => {
    const { authInfo } = this.props;
    const zatgoToken = authInfo.token ? authInfo.token : ''
    const params = {
      "tmcCode": "ZATGO",
      "corpCode": "ZAT",
      "sign": "123456",
      "relationUserId": authInfo.userId,
      "contact": authInfo.userName,
    }
    this.props.signLoginFeiba({ appAuth, weburl, zatgoToken, params })

  }

  onItemClick = (item, index) => {
    console.log(JSON.stringify(item))
    if (item.status !== 1) {
      if (item.appGotoType && Number(item.appGotoType) === 1) {
        this.onSignLogin(item.appAuth, this.queryString(item.appGotoAddress, cloudUserId))
      }
      if (Number(item.appGotoType) === 0) {
        Actions.push(item.appGotoAddress)
      }
      if (item.appGotoType && Number(item.appGotoType) === 3) {
        let items = item;
        items['localtionUrl'] = item.appGotoAddress;
        global.showModel(items, true, true);
      }
    } else {
      Toast.info(I18n.t('home_funnot_available'), 1)
    }
  }

  onBannerItemClick = (item, index) => {
    console.log(item)
    if (Number(item.clickEventType) === 1) {
      this.onSignLogin(null, this.queryString(item.clickEventAddress, cloudUserId))
    }
    if (Number(item.clickEventType) === 0) {
      Actions.push(item.clickEventAddress)
    }

    if (Number(item.clickEventType) === 3) {
      let items = item;
      items['localtionUrl'] = item.clickEventAddress;
      global.showModel(items, true, true);
    }
  }

  queryString = (url, params) => {
    if (params && url) {
      let paramsArray = [];
      Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
      if (url.search(/\?/) === -1) {
        url += '?' + paramsArray.join('&')
      } else {
        url += '&' + paramsArray.join('&')
      }
    }
    return url;
  }

  onActivity = (item, index) => {
    if (index === 0) {
      Actions.digTask();
    } else {
      Toast.info(I18n.t('home_funnot_available'), 1)
    }
  }

  onClickRichScan = () => {
    Actions.richScan();
  }

  onClickCollection = () => {
    Actions.collection();
  }

  onClickAssetHub = () => {
    Actions.assetHub();
  }

  //显示下拉列表
  showSpinner = (data) => {
    this.setState({
      isVisible: true,
      spinnerRect: data
    });
  }

  //隐藏下拉列表
  closeSpinner = () => {
    this.setState({
      isVisible: false
    });
  }

  //下拉列表每一行点击事件
  onBtnClick = (i) => {
    if (i === 1) { this.onClickRichScan() };
    if (i === 2) { this.onClickCollection() };
    if (i === 3) { this.onClickAssetHub() };
    this.closeSpinner();
  }

  onShow = () => {
    this.setState({
      activeState: true
    })
  }



  render() {
    const { applicationList, bannerList, getApplicationList, taskData, redEnvelope, authInfo } = this.props;
    if (JSON.stringify(taskData) !== '{}') {
      global.showModel(taskData, false, redEnvelope);
      this.props.setRedEnvelope(false);
    }
    if (applicationList.length > 0) {
      applicationList && applicationList.map((items, indexs) => (
        items && items.appDatas.map((item, index) => {
          DealData.dealRequestData(authInfo, item, JSON.stringify(indexs + 1), index);
        })
      ))
    }
    return (
      <View style={{ flex: 1 }}>
        <HomeView
          applicationList={applicationList}
          bannerList={bannerList}
          isAudit={this.props.isAudit}
          onBannerItemClick={this.onBannerItemClick}
          onItemClick={this.onItemClick}
          onClickRichScan={this.onClickRichScan}
          onClickCollection={this.onClickCollection}
          onClickAssetHub={this.onClickAssetHub}
          onActivity={this.onActivity}
          onPullRelease={this.onRefreshData}
          getApplicationList={getApplicationList}
          showSpinner={this.showSpinner}
          authInfo={authInfo}
        />
        <Popover
          //设置可见性
          isVisible={this.state.isVisible}
          //设置下拉位置
          fromRect={this.state.spinnerRect}
          placement="bottom"
          //点击下拉框外范围关闭下拉框
          onClose={() => this.closeSpinner()}
          //设置内容样式
          contentStyle={{ opacity: 1, backgroundColor: 'white' }}
          style={{}}>
          <View style={{ alignItems: 'flex-start' }}>
            {spinnerTextArray().map((result, i, arr) => {
              return <TouchableHighlight key={i} onPress={() => this.onBtnClick(i + 1)}
                underlayColor='transparent'>
                <View style={{ flexDirection: 'row', alignItems: 'center', margin: 5 }}>
                  <Image
                    style={{ width: 16, height: 16, }}
                    source={arr[i].icon} />
                  <Text
                    style={{ fontSize: 14, color: 'black', padding: 8, fontWeight: '400' }}>
                    {arr[i].text}
                  </Text>
                </View>
              </TouchableHighlight>
            })
            }
          </View>
        </Popover>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  signLoginFeiba: (sign) => {
    dispatch(authActions.signLoginFeiba(sign))
  },
  getApplicationList: (sign) => {
    dispatch(authActions.getApplicationList())
  },
  getBannerList: (sign) => {
    dispatch(authActions.getBannerList(sign))
  },
  getHashRaseTask: () => {
    dispatch(digActions.getHashRaseTask())
  },
  setRedEnvelope: (params) => {
    dispatch(globalActions.setRedEnvelope(params))
  },
})

const mapStateToProps = (state) => ({
  authInfo: state.user.authInfo,
  feibaAct: state.user.feibaAct,
  isAudit: state.user.isAudit,
  applicationList: state.user.applicationList,
  bannerList: state.user.bannerList,
  taskData: state.dig.taskData,
  redEnvelope: state.global.redEnvelope
})

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
