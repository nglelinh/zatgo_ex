/*
 *  @File   : index
 *  @Author : lsl
 *  @Date   : 2018-4-1 11:35:27
 *  @Last Modified   : 2018-4-1 11:35:27
 *  @Desc 个人中心
 */
import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { List } from 'antd-mobile'
import I18n from 'I18n';
import CustomImage from '../../components/CustomImage/customImage'
import { theme, window } from '@zatgo/constants';
import { userActions, walletActions } from '../../redux/actions';
import { DimensUtils, StorageUtils } from '../../utils';
import constDefines from '@zatgo/constants/constDefines';
import NavBar from '../../components/NavBar';
import ListItem from './ListItem'

const MineCollection = require('../../assets/mine/mine_collection.png')
const MineWallet = require('../../assets/mine/mine_wallet.png')
const MineScan = require('../../assets/mine/mine_scan.png')
const MineWave = require('../../assets/common/com_wave.png')
const MineDeposit = require('../../assets/mine/mine_deposit_1.png')
const MineExtract = require('../../assets/mine/mine_extract_1.png')
const MineOrderQuery = require('../../assets/mine/mine_orderquery.png')
const MineDefaultCurrency = require('../../assets/mine/mine_default.png')
const MineContact = require('../../assets/mine/mine_about.png')
const MineInviteCode = require('../../assets/mine/mine_inviteCode.png')
const MineOrder = require('../../assets/mine/mine_order.png')

class User extends PureComponent {

  componentDidMount() {
    StorageUtils.get(constDefines.storeAuthInfoKey).then((res) => {
      if (res && res.userId) {
        this.props.getAuthById(res.userId);
        this.props.getPrimaryAccount();

      } else {
        Actions.replace('login')
      }
    });
  }

  selectName = data => {
    if (data.nickname && data.nickname.length > 0) {
      return data.nickname;
    }
    if (data.userName && data.userName.length > 8) {
      return data.userName.substr(0, 3) + '****' + data.userName.substr(7);
    }
    return '';
  };

  renderUserInfo() {
    const { userInfo } = this.props;
    return (
      <View style={styles.topLayout}>
        <View style={styles.userContainer}>
          <TouchableOpacity onPress={() => { Actions.userInfo({ userInfoData: userInfo }) }}>
            <View style={styles.userFlexRow}>
              <View style={styles.avatar}>
                <CustomImage
                  defaultImage={require('../../assets/mine/default_avatar.png')}
                  errImage={require('../../assets/mine/default_avatar.png')}
                  uri={userInfo.iconUrl ? userInfo.iconUrl : 'xxxxx.png'}
                  style={{ width: 60, height: 60, borderRadius: 30 }}
                />
              </View>
              <View>
                <Text style={styles.txtAct} numberOfLines={1}>
                  {this.selectName(userInfo)}
                </Text>
                <Text
                  style={{ fontSize: 13, color: '#858585', marginTop: 11 }}
                >
                  {I18n.t('usr_showInfo')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderTradeActionView() {
    return (
      <View style={{ height: 114 }}>
        <View style={styles.actionView}>
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => Actions.richScan()}
            style={styles.tradeContainer}>
            <Image
              style={styles.actionIcon}
              source={MineScan}
            />
            <Text style={styles.font16}>
              {I18n.t('home_rich_scan')}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => Actions.collection()}
            style={styles.tradeContainer}>
            <Image
              style={styles.actionIcon}
              source={MineCollection} />
            <Text style={styles.font16}>
              {I18n.t('home_collection')}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => Actions.assetHub()}
            style={styles.tradeContainer}>
            <Image
              style={styles.actionIcon}
              source={MineWallet} />
            <Text style={styles.font16}>
              {I18n.t('home_assethub')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderMain() {
    const { primaryAccount, userInfo } = this.props;
    return (
      <View style={{ margin: 10 }}>
        <ListItem title={I18n.t('usr_deposit')}
          icon={MineDeposit}
          onClick={() => Actions.selectCoin({ toRouter: 1 })}
        />
        <ListItem title={I18n.t('usr_withdrawal')}
          icon={MineExtract}
          onClick={() => Actions.selectCoin({ toRouter: 2 })}
        />
        <ListItem title={I18n.t('usr_history')}
          icon={MineOrder}
          onClick={() => Actions.OrderList()}
        />
        <ListItem title={I18n.t('usr_coupons')}
          icon={MineOrderQuery}
          onClick={() => Actions.Coupons()}
        />
        <ListItem title={I18n.t('usr_order_query')}
          icon={MineOrderQuery}
          onClick={() => Actions.orderquery()}
        />
        <ListItem title={I18n.t('usr_default_currency')}
          primaryAccount={primaryAccount}
          icon={MineDefaultCurrency}
          onClick={() => Actions.defaultAccount()}
        />
        <ListItem title={I18n.t('usr_invitation')}
          icon={MineInviteCode}
          onClick={() => Actions.InviteCode()}
        />
        <ListItem title={I18n.t('usr_about')}
          icon={MineContact}
          onClick={() => Actions.servicePage()}
        />
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <NavBar
          style={{ paddingTop: window.isIphoneX ? 40 : 20 }}
          titleLayoutStyle={{ fontSize: 21 }}
          onLeftClick={() => { }}
          showRight
          rightText={null}
          rightImg={require('@zatgo/assets/mine/setting.png')}
          onRightClick={() => {
            Actions.setting()
          }}
        // statusBar={{
        //   barStyle: 'dark-content',
        //   hidden: false,
        //   backgroundColor: '#00000000',
        // }}
        />
        {this.renderUserInfo()}
        {this.renderTradeActionView()}
        <ScrollView>
          <View style={styles.mainStyle}>
            {this.renderMain()}
          </View>
        </ScrollView>
      </View>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  getAuthById: (id) => {
    return dispatch(userActions.getAuthById(id));
  },
  getPrimaryAccount: () => {
    dispatch(walletActions.getPrimaryAccount())
  }
})

const mapStateToProps = (state) => ({
  userInfo: state.user.mineInfo,
  primaryAccount: state.wallet.primaryAccount,
})

export default connect(mapStateToProps, mapDispatchToProps)(User);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  topLayout: {
    height: 80,
    backgroundColor: 'white',
    justifyContent: 'center',
    marginLeft: 30,
    marginRight: 30,
  },
  waveImg: {
    height: 40, width: window.width
  },
  walletImg: {
    width: 30,
    height: 30
  },
  mainStyle: {
    backgroundColor: 'white',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 10,
    //以下是阴影属性：
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    shadowColor: '#808a92',
    //注意：这一句是可以让安卓拥有灰色阴影
    elevation: 2,
  },
  userContainer: {
    flex: 1,
    // justifyContent: 'flex-end',
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  userFlexRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  actionView: {
    flexDirection: 'row',
    backgroundColor: 'white',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 10,
    //以下是阴影属性：
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    shadowColor: '#808a92',
    //注意：这一句是可以让安卓拥有灰色阴影
    elevation: 2,
  },
  avatar: {
    width: DimensUtils.getDpScale(60),
    height: DimensUtils.getDpScale(60),
    marginRight: 10,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    overflow: 'hidden',
  },
  tradeContainer: {
    flex: 1,
    height: 90,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemIcon: {
    width: 25,
    height: 25,
    marginRight: 10
  },
  actionIcon: {
    width: 30,
    height: 30,
  },
  font14: {
    color: '#323B43',
    fontSize: 14 * window.fontScale
  },
  font16: {
    color: '#323B43',
    // fontWeight: 'bold',
    fontSize: 16 * window.fontScale,
    marginTop: 10
  },
  verticalLine: {
    width: 0.5,
    height: 36,
    backgroundColor: '#D9D9D9',
  },
  txtAct: {
    width: DimensUtils.getDpScale(180),
    color: '#1A1A1A',
    fontSize: 24 * window.fontScale,
    fontWeight: 'bold'
  }
});