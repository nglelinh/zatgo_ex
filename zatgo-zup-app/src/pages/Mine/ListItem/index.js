/**
 * Created by zhoujianxin on 2019/1/2.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import {Icon} from 'antd-mobile';


export default class index extends Component {
  render() {
    const {icon,title,onClick,primaryAccount}=this.props;
    return (
        <View style={styles.container}>
          <TouchableOpacity style={{flex: 1}} onPress={() => {
            onClick && onClick()
          }}>
            <View style={styles.listStyle}>
              <Image
                  style={styles.itemIcon}
                  source={icon}/>
              <View style={{flex: 1}}>
                <Text style={styles.font14}>{title}
                  <Text style={styles.txtGreen}>
                    {primaryAccount ? primaryAccount.coinType : ''}
                  </Text>
                </Text>
              </View>
              <Image
                  style={styles.rightStyle}
                  source={require('@zatgo/assets/mine/right.png')}/>
            </View>
          </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height:44,
  },
  listStyle:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    paddingLeft:5,
    backgroundColor:'white',
  },

  itemIcon: {
    width: 25,
    height: 25,
    marginRight: 9
  },
  rightStyle: {
    width: 16,
    height: 16,
    marginRight: 4
  },
  font14: {
    color: '#323B43',
    fontSize: 14 * window.fontScale
  },
  txtGreen: {
    color: '#4DCC7B',
    fontSize: 14 * window.fontScale,
  },
});