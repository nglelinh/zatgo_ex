/**
 * Created by zhoujianxin on 2018/10/19.
 * @Desc
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Toast, Modal, InputItem, List} from 'antd-mobile';
import {window} from '@zatgo/constants';
import I18n from 'I18n';

export default class InputBtn extends Component {
  state = {
    nickName: '',
  };

  componentDidMount() {
  this.setState({
    nickName:this.props.nickname
  })
  }

  componentWillReceiveProps(nextProps) {
    if(this.state.nickName!==nextProps.nickname){
      this.setState({
        nickName:this.props.nickname
      })
    }
  }

  onClose = () => {
    this.props.onClose && this.props.onClose();
  }

  render() {
    return (
        <View style={styles.container}>
          <Modal
              transparent
              onClose={this.onClose}
              maskClosable
              visible={this.props.visible ? this.props.visible : false}
              style={{backgroundColor: 'transparent'}}
          >
            <View style={{
              backgroundColor: '#fff',
              minWidth: window.width - 116,
              height: 121,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 8
            }}>
              <View
                  style={{
                    borderWidth: 0.6,
                    borderColor: '#D9D9D9',
                    width: window.width - 140,
                    margin: (17, 12, 17, 12),
                  }}
              >
                <List>
                  <InputItem
                      style={[{}, styles.inputStyle]}
                      value={this.state.nickName}
                      placeholder="请输入昵称"
                      onChangeText={text => {
                        let value = text.replace(/\s+/g, "");
                        this.setState({nickName:value })}
                      }
                      maxLength={10}
                  />
                </List>
              </View>
              <View
                  style={{
                    width: window.width - 116,
                    height: 0.5,
                    backgroundColor: '#D9D9D9',
                  }}
              />
              <View
                  style={{
                    width: window.width - 126,
                    flexDirection: 'row',
                    height: 44,
                    alignItems: 'center',
                    backgroundColor: 'white',
                    marginLeft: 4,
                    marginRight: 4
                  }}
              >
                <TouchableOpacity
                    onPress={() => {
                      if (this.state.nickName.length > 0) {
                        this.props.onClick && this.props.onClick(this.state.nickName);
                      }
                    }}
                >
                  <View
                      style={{
                        width: (window.width - 116) / 2-10,
                        height: 44,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                  >
                    <Text style={{fontSize: 18, color: this.state.nickName.length>0? '#F0BC1B':'#D9D9D9'}}>{I18n.t('alert_ok')}</Text>
                  </View>
                </TouchableOpacity>
                <View style={{width: 0.5, height: 46, backgroundColor: '#D9D9D9'}}/>
                <TouchableOpacity
                    onPress={() => {
                      this.props.onClose && this.props.onClose();
                    }}
                >
                  <View
                      style={{
                        width: (window.width - 116) / 2-10,
                        height: 44,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                  >
                    <Text style={{fontSize: 18, color: '#B2B2B2'}}>{I18n.t('alert_cancel')}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputStyle: {
    borderColor: 'white',
    height: 45,
  },
});
