/**
 * Created by zhoujianxin on 2018/9/21.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Text,
  StatusBar
} from 'react-native';
import {Actions} from 'react-native-router-flux'
import ImageParse from '../../../components/ImageParse';
import CustomImage from '../../../components/CustomImage/customImage';
import {createAction, StorageUtils} from '../../../utils';
import {List} from "antd-mobile";
import I18n from 'I18n';
import {window} from '@zatgo/constants';
import {userActions} from '../../../redux/actions';
import {connect} from "react-redux";
import constDefines from '@zatgo/constants/constDefines';
import InputBtn from './InputBtn';

const Item = List.Item;

let textArr= () => ({
  '0': I18n.t('certifi_authentication'),
  '1': I18n.t('certifi_authenticated'),
  '2': I18n.t('certifi_authentication_failed')
});


class index extends Component {

  state = {
    userId: '',
    visible: false,
  };

  componentDidMount() {
    this.props.getCertificateInfo()
    StorageUtils.get(constDefines.storeAuthInfoKey).then((res) => {
      if (res && res.userId) {
        this.setState({
          userId: res.userId
        })
      }
    });
  }

  onClose = () => {
    this.setState({
      visible: false,
    });
  }

  onClick = (userInfoData, nickName, userId) => {
    this.props.updateBasicInfo({
      iconUrl: userInfoData.iconUrl ? userInfoData.iconUrl : '',
      nickName,
      userId,
    })
    this.onClose();
  };

  separatorCom = () => (
      <View style={{height: 1, width: windows.width, backgroundColor: 'white'}}>
        <View
            style={{
              borderBottomWidth: 0.5,
              borderBottomColor: '#D9D9D9',
              marginLeft: 15,
            }}
        />
      </View>
  );


  render() {
    const {userInfo, certificateInfo} = this.props;
    const {userId} = this.state;
    return (
        <View style={styles.container}>
          <ScrollView style={{flex: 1, backgroundColor: '#F7F7F7'}}>
            <List style={{marginTop: 10, marginBottom: 10}}>
              <Item
                  onClick={() => {
                    ImageParse.onParseGalleryImage(data => {
                      this.props.uploadImage({
                        path: data,
                        nickName: userInfo.nickname
                            ? userInfo.nickname
                            : '',
                        userId,
                      })
                    });
                  }}
                  arrow="horizontal"
                  extra={<View
                      style={{
                        width: 32,
                        height: 32,
                        borderRadius: 16,
                        backgroundColor: '#F5F5F5',
                      }}
                  >
                    <CustomImage
                        defaultImage={require('../../../assets/mine/default_avatar.png')}
                        errImage={require('../../../assets/mine/default_avatar.png')}
                        style={{
                          width: 32,
                          height: 32,
                          borderRadius: 16,
                        }}
                        defaultStyle={{
                          width: 32,
                          height: 32,
                          borderRadius: 16,
                        }}
                        uri={
                          userInfo.iconUrl ? userInfo.iconUrl : 'xxxxx.png'
                        }
                    />
                  </View>}
              >
                <Text style={styles.font14}>
                  {I18n.t('usr_picture')}
                </Text>
              </Item>

              <Item
                  onClick={() => {
                    this.setState({
                      visible: true
                    })
                  }}
                  arrow="horizontal"
                  extra={
                    <View>
                      <Text>
                        {userInfo.nickname ? userInfo.nickname : ''}
                      </Text>
                    </View>
                  }
              >
                <Text style={styles.font14}>
                  {I18n.t('usr_nickname')}
                </Text>
              </Item>

              <Item
                  onClick={() => {
                    if(!certificateInfo||certificateInfo.approveStatus!==1) {
                      Actions.RealName({userId: userId});
                    }
                  }}
                  arrow="horizontal"
                  extra={
                    <View>
                      <Text
                          style={{color: certificateInfo && certificateInfo.approveStatus === 1 ? '#35BAA0' : '#797F85'}}>
                        {certificateInfo ? textArr()[`${certificateInfo.approveStatus}`] : I18n.t('certifi_not_authenticated')}
                      </Text>
                    </View>
                  }
              >
                <Text style={styles.font14}>{I18n.t('certifi_Real_name')}</Text>
              </Item>

            </List>

            <InputBtn
                visible={this.state.visible}
                nickname={userInfo.nickname ? userInfo.nickname : ''}
                onClose={() => {
                  this.onClose();
                }}
                onClick={(nickName) => {
                  this.onClick(userInfo, nickName, userId)
                }}
            />
          </ScrollView>
        </View>
    )
        ;
  }
}


const mapDispatchToProps = (dispatch) => ({
  uploadImage: (data) => {
    return dispatch(userActions.uploadImage(data));
  },
  updateBasicInfo: (params) => {
    dispatch(userActions.updateBasicInfo(params))
  },
  getCertificateInfo: (params) => {
    dispatch(userActions.getCertificateStatus())
  },
})

const mapStateToProps = (state) => ({
  userInfo: state.user.mineInfo,
  certificateInfo: state.user.certificateInfo,
})

export default connect(mapStateToProps, mapDispatchToProps)(index);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon: {
    width: 24,
    height: 24,
    // tintColor: 'gray',
  },
  fontSize18: {
    color: '#1A1A1A',
    fontSize: 18,
  },
  fontSize14: {
    color: '#858585',
    fontSize: 14,
    marginTop: 12,
  },
});
