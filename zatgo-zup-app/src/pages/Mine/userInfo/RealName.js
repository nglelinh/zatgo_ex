import React, {Component} from 'react';
import {View, StyleSheet, Image, TouchableOpacity, DeviceEventEmitter, ScrollView, Text, ImageBackground, Platform} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {List, InputItem, Picker, Toast} from "antd-mobile";
import {connect} from 'react-redux'
import I18n from 'I18n';
import {window} from '@zatgo/constants';
import ImageParse from "../../../components/ImageParse";
import {userActions} from "../../../redux/actions";

class RealName extends Component {
  state = {
    name: '',
    cardNum: '',
    positiveFile: [],
    cardType: [],
    country: [],
  }

  componentDidMount() {
    const {certificateInfo} = this.props;
    this.renderNav(certificateInfo)
    if (certificateInfo&&JSON.stringify(certificateInfo) !== '{}') {
      console.log('componentDidMount')
      this.setState({
        name: certificateInfo.firstName,
        cardNum: certificateInfo.certificateId,
        positiveFile: [{url: certificateInfo.holdPhotoUrl}],
        cardType: [`${certificateInfo.certificateType}`],
        country: [certificateInfo.countryCode],
      })
    }
  }

  renderNav = (certificateInfo) => {
    if(!certificateInfo||certificateInfo&&JSON.stringify(certificateInfo) !== '{}'&&certificateInfo.approveStatus ===2) {
      Actions.refresh({
        rightTitle: I18n.t('reg_submit'),
        rightButtonTextStyle:{color:'black'},
        onRight: () => {
          const {name, cardNum, positiveFile, cardType, country} = this.state;
          if (name !== '' && cardNum !== '' && positiveFile.length > 0 && cardType.length > 0 && country.length > 0) {
            this.props.uploadCertificateImage({
              path: positiveFile[0],
              name: name,
              cardNum: cardNum,
              cardType: cardType[0],
              country: country[0]
            })
          }else {
            Toast.info('请完善认证信息')
          }
        }
      });
    } else {
      Actions.refresh({
        rightTitle: '',
        onRight: () => {}
      });

    }
  }

  onChange = (value,key) => {
    console.log(value)
    this.setState({ [key]:value });
  }

  onAddImageClick = (key) => {
    ImageParse.onParseGalleryImage((res) => {
      this.setState({ [key]: this.state[key].concat({ url: res }) });
      console.log('选择图片：', this.state[key])
    })
  }

  onChangeImage = (key) => {
    this.setState({ [key]:[] });
  }

  renderAddImageView = (key) => {
    //pages 变量，用来存储，我们遍历出来的路径，生成的ImageBackground显示节点。
    let pages = [];
    let data = this.state[key][0];
    if (this.state[key].length > 0&&data&&data['url']) {
      pages.push(
          <View style={styles.imageBg} key={key+'1'}>
            <ImageBackground source={{uri:data['url']}} style={styles.uploadImage} imageStyle={{ borderRadius:10 }}/>
            <TouchableOpacity style={styles.rightDelButton} onPress={() => {this.onChangeImage(key)}}>
              <Image style={{width: 25, height: 25}}
                     source={require('../../../assets/common/ic_ext_close.png')} />
            </TouchableOpacity>
          </View>
      )
    } else {
      pages.push(
          <View style={styles.imageBg} key={key+'1'}>
            <TouchableOpacity onPress={()=>{this.onAddImageClick(key)}}>
              <Image style={{width: 60, height: 60}} source={require('../../../assets/common/com_add.png')}/>
            </TouchableOpacity>
            <Text style={styles.normalTitle}>上传照片</Text>
          </View>
      )
    }
    return (pages)
  }

  render() {
    const {certificateInfo} = this.props;
    return (
        <View style={{flex:1}}>
          <ScrollView style={{flex: 1, backgroundColor: '#F7F7F7'}} bounces={false}>
            <List style={{marginTop: 10, marginBottom: 10}}>
              <InputItem
                  textAlign={'center'}
                  value={this.state.name}
                  clear
                  placeholder={I18n.t('certificateInfo_nameInput')}
                  onChange={value => {
                    console.log('value1', value)
                    this.setState({
                      name: value.toLowerCase()
                    })
                  }}
              >
                <Text style={{fontSize:17,color:'#000000'}}>
                  {I18n.t('certificateInfo_name')}
                </Text>
              </InputItem>

              <Picker
                  data={I18n.t('countries')['data']}
                  cols={1}
                  value={this.state.country}
                  onChange={(v) => this.onChange(v, 'country')}
                  dismissText={'取消'}
                  okText={'确认'}
              >
                <List.Item arrow="horizontal" onPress={this.onPress}>
                  {I18n.t('certificateInfo_nationality')}
                </List.Item>
              </Picker>

              <Picker
                  data={[
                    {label: I18n.t('certificateInfo_idCard'), value: '0',},
                    {label: I18n.t('certificateInfo_passport'), value: '1',},]}
                  cols={1}
                  value={this.state.cardType}
                  onChange={(v) => this.onChange(v, 'cardType')}
                  dismissText={'取消'}
                  okText={'确认'}
              >
                <List.Item arrow="horizontal" onPress={this.onPress}>
                  {I18n.t('certificateInfo_cardType')}
                </List.Item>
              </Picker>

              <InputItem
                  textAlign={'center'}
                  value={this.state.cardNum}
                  clear
                  placeholder={I18n.t('certificateInfo_cardInput')}
                  onChange={value => {
                    console.log('value2', value)
                    this.setState({
                      cardNum: value.toLowerCase()
                    })
                  }}
              >
                <Text style={{fontSize:17,color:'#000000'}}>
                  {I18n.t('certificateInfo_cardNum')}
                </Text>
              </InputItem>
            </List>

            <View style={{flex:1,alignItems: 'center'}}>
              <Text style={[styles.normalTitle,{color:'#218afe', marginBottom:5}]}>拍摄/上传手持身份证人像面</Text>
              {this.renderAddImageView('positiveFile')}
            </View>

            {certificateInfo && JSON.stringify(certificateInfo) !== '{}' && certificateInfo.approveStatus === 2 ?
                <View style={{flex: 1, alignItems: 'center', marginTop: 20, marginLeft: 40, marginRight: 40}}>
                  <Text style={{color: 'red'}}>审批失败：{certificateInfo.approveDemo}</Text>
                </View> : null
            }

          </ScrollView>
        </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  uploadCertificateImage: (data) => {
    return dispatch(userActions.uploadCertificateImage(data));
  },
})


const mapStateToProps = (state) => ({
  certificateInfo: state.user.certificateInfo,
})

export default connect(mapStateToProps, mapDispatchToProps)(RealName);

const styles = StyleSheet.create({
  normalTitle: {
    textAlign: "center",
    marginTop: 20,
  },
  normalText: {
    textAlign: "center"
  },
  imageBg: {
    backgroundColor: 'white',
    alignItems: "center",
    justifyContent: "center",
    width: window.width / 2 - 20,
    height: window.width / 2 - 20,
    borderRadius: 10
  },
  uploadImage: {
    alignItems: "center",
    justifyContent: "center",
    width: window.width / 2 - 25,
    height: window.width / 2 - 25,
    borderRadius: 10
  },
  rightDelButton: {
    position: 'absolute',
    top: 10,
    right: 10,
  }
});
