/**
 * @Desc 半圆
 */

import React from 'react';
import { View, StyleSheet, } from 'react-native';

const Semicircle = ({ left }) => <View style={left ? styles.semicircle_left : styles.semicircle_right} />

export default Semicircle;

const styles = StyleSheet.create({
  semicircle_left: {
    width: 8,
    height: 16,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    backgroundColor: '#F0F2F5',
  },
  semicircle_right: {
    width: 8,
    height: 16,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: '#F0F2F5',
  },
});