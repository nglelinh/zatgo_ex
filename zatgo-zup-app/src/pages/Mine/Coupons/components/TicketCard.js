/**
 * @Desc 优惠券
 */

import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  findNodeHandle,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';

import Semicircle from './Semicircle';

const TicketValue = ({ active, data }) => (
  <View style={styles.view_left}>
    {
      data.discountType === 0 ?
        <Text style={[styles.txt_ticket_value, { color: active ? '#4DCC7B' : 'rgba(77,204,123,0.4)', }]}>
          ￥<Text style={{ fontSize: 30, }}>{data.couponsPrice}</Text>
        </Text> :
        <Text style={[styles.txt_ticket_value, { fontSize: 30, color: active ? '#4DCC7B' : 'rgba(77,204,123,0.4)', }]}>
          {data.couponsPrice}<Text style={{ fontSize: 20, }}>折</Text>
        </Text>
    }
    {
      data.howManyFull ?
        <Text style={[styles.txt_using_condition, { color: active ? '#333333' : 'rgba(51,51,51,0.4)', }]}>
          {`满${data.howManyFull}可用`}
        </Text> : null
    }
  </View>
)

const TicketInfo = ({ active, data }) => (
  <View style={styles.view_right}>
    <Text style={[styles.txt_ticket_type, { color: active ? '#333333' : 'rgba(51,51,51,0.4)', }]}>
      {data.couponsName}
    </Text>
    <Text style={[styles.txt_ticket_indate, { color: active ? '#999' : 'rgba(153,153,153,0.4)', }]}>
      {`${moment(data.validStartDate).format('YYYY.M.D')} - ${moment(data.validEndDate).format('YYYY.M.D')}`}
    </Text>
    {
      data.usageExplain ?
        <Text style={[styles.txt_using_range, { color: active ? '#999' : 'rgba(153,153,153,0.4)', }]}>
          {data.usageExplain}
        </Text> : null
    }
  </View>
)

const TicketCard = ({ item }) => {
  const { userCouponsStatus } = item;
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => { }}
      style={styles.coupons_container}
    >
      <View style={styles.view_coupons}>
        <Semicircle left />
        <TicketValue active={userCouponsStatus === 0} data={item} />
        <TicketInfo active={userCouponsStatus === 0} data={item} />
        <Semicircle left={false} />
      </View >
    </TouchableOpacity>
  )
}
export default TicketCard;

const styles = StyleSheet.create({
  absolute: {
    position: "absolute",
    top: 0, left: 0, bottom: 0, right: 0,
  },
  coupons_container: {
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  view_coupons: {
    display: 'flex',
    height: 120,
    borderRadius: 5,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
  },
  view_flexColumn: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  view_left: {
    flex: 1.5,
    marginLeft: 2,
    marginTop: 30,
    marginBottom: 30,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  view_right: {
    flex: 2,
    marginTop: 30,
    marginBottom: 30,
    marginRight: 2,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  txt_ticket_value: {
    fontSize: 20,
    textAlign: 'center',
  },
  txt_using_condition: {
    fontSize: 11,
    textAlign: 'center',
  },
  txt_ticket_type: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  txt_ticket_indate: {
    fontSize: 11,
    marginTop: 10,
  },
  txt_using_range: {
    fontSize: 11,
    marginTop: 5,
  },
});