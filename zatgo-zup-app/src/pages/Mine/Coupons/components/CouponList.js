/**
 * @Desc 优惠券列表
 */

import React, { PureComponent } from 'react';
import {
  Text,
  View,
  StyleSheet,
} from 'react-native';
import I18n from 'I18n';
import JXFlatList from '@zatgo/components/JXFlatList';
import TicketCard from './TicketCard';

const ListFooter = () => <View style={{ height: 25, width: 200, }} />

const CouponList = ({ data, onRefresh, onEndReached, pageSize }) => {
  return (
    <JXFlatList
      data={data}
      refreshing={0}
      pageSize={pageSize}
      renderItem={TicketCard}
      onRefresh={onRefresh}
      onEndReached={onEndReached}
      ItemSeparatorComponent={() => null}
      ListFooterComponent={ListFooter}
    />
  );
}

export default CouponList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0F2F5',
  },
});