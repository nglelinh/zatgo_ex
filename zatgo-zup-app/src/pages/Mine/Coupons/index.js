/**
 * @Desc 优惠券列表
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, } from 'react-native';
import { connect } from 'react-redux';
import { Tabs } from 'antd-mobile';
import I18n from 'I18n';
import CouponList from './components/CouponList';
import { couponActions } from '@zatgo/redux/actions';

const tabs = [
  { title: '未使用' },
  { title: '已使用' },
  { title: '已过期' },
]

let tabSelect = 0;
const defaultPageNo = 1;
const defaultPageSize = 10;

class Coupon extends PureComponent {

  componentDidMount() {
    this.reqData(defaultPageNo, 0);
  }

  reqData = (pageNo, status) => {
    const { getCoupons, pending } = this.props;
    if (!pending) {
      getCoupons({ pageNo, pageSize: defaultPageSize, status });
    }
  }

  // Tab切换
  onTabChange = (tab, index) => {
    console.log('onTabChange', index);
    tabSelect = index;
    this.reqData(defaultPageNo, index);
  }

  onRefresh = () => {
    console.log('刷新优惠券');
    this.reqData(defaultPageNo, tabSelect);
  }

  onEndReached = () => {
    console.log('加载更多优惠券');
    const { getMoreCoupon, used, expired, effective, pending } = this.props;
    if (tabSelect === 0) {
      const { pages, pageNum } = effective;
      this.hasMore = (pageNum < pages);
      this.pageNum = pageNum + 1;
    } else if (tabSelect === 1) {
      const { pages, pageNum } = used;
      this.hasMore = (pageNum < pages);
      this.pageNum = pageNum + 1;
    } else if (tabSelect === 2) {
      const { pages, pageNum } = expired;
      this.hasMore = (pageNum < pages);
      this.pageNum = pageNum + 1;
    }
    if (this.hasMore && !pending) {
      getMoreCoupon({ pageNo: this.pageNum, pageSize: defaultPageSize, status: tabSelect });
    }
  }

  render() {
    const { used, expired, effective } = this.props;
    return (
      <View style={styles.container}>
        <Tabs
          tabs={tabs}
          initialPage={0}
          swipeable={false}
          onChange={this.onTabChange}
          tabBarActiveTextColor='#4DCC7B'
          tabBarInactiveTextColor='#797F85'
          tabBarUnderlineStyle={{ backgroundColor: '#4DCC7B' }}
        >
          <CouponList
            data={effective.list ? effective.list : []}
            onRefresh={this.onRefresh}
            onEndReached={this.onEndReached}
            pageSize={defaultPageSize}
          />
          <CouponList
            data={used.list ? used.list : []}
            onRefresh={this.onRefresh}
            onEndReached={this.onEndReached}
            pageSize={defaultPageSize}
          />
          <CouponList
            data={expired.list ? expired.list : []}
            onRefresh={this.onRefresh}
            onEndReached={this.onEndReached}
            pageSize={defaultPageSize}
          />
        </Tabs>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCoupons: (payload) => {
    return dispatch(couponActions.getCoupons(payload));
  },
  getMoreCoupon: (payload) => {
    return dispatch(couponActions.getMoreCoupon(payload));
  },
})

const mapStateToProps = (state) => ({
  used: state.coupon.used,
  expired: state.coupon.expired,
  effective: state.coupon.effective,
  pending: state.coupon.pending,
})

export default connect(mapStateToProps, mapDispatchToProps)(Coupon);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0F2F5',
  },
});