/*
 *  @File   : LoginIndexPage
 *  @Author : lsl
 *  @Date   : 2018-4-3 17:19:47
 *  @Last Modified   : 2018-4-3 17:19:47
 *  @Desc 登录
 */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  Dimensions,
  TouchableOpacity,
  Platform
} from 'react-native';
import I18n from 'I18n';
import { Toast } from 'antd-mobile'
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { authActions } from '../../redux/actions';
import { window } from '@zatgo/constants';
import { EncryptUtils, DimensUtils, configToken } from '@zatgo/utils';
import H5 from '../../components/SliderView'

class LoginIndexPage extends Component {

  state = {
    visible:false,
    loginParams: {
      "loginName": null,
      "loginPassword": null
    }
  }

  componentDidMount() {
    SplashScreen.hide();
    configToken(null);
  }

  onChangeAccount = (value) => {
    const { loginParams } = this.state;
    loginParams['loginName'] = value.replace(/\s+/g, "");
    this.setState({ loginParams });
  }

  onChangePassword = (value) => {
    const { loginParams } = this.state;
    // 密码对称加密
    const rsa = EncryptUtils.rsa(value.replace(/\s+/g, ""));
    loginParams['loginPassword'] = value.replace(/\s+/g, "");
    this.setState({ loginParams });
  }

  onPressRegister = () => {
    Actions.register({});
  }

  onPressForgotPsw = () => {
    Actions.register({ forgotPsw: 'forgotPsw' });

  }

  onPressLogin = () => {
    const {loginName, loginPassword} = this.state.loginParams;
    if (loginName && loginPassword) {
      this.setState({
        visible:true
      })
    } else {
      Toast.info(I18n.t('login_request_err'), 1)
    }
  }

  onClose = () => {
    this.setState({
      visible:false
    })
  }

  onSubmit = (data) => {
    const {loginName, loginPassword} = this.state.loginParams;
    let params = data?{'aliyunValidateCode':data}:{};
    params['loginName'] = loginName;
    params['loginPassword'] = loginPassword;
    console.log(params)
    this.props.login(params)
  }

  render() {
    let topSize = (window.height/2) >= 420 ? 390 - (window.height/2) : 390 - (window.height/2)
    return (
      <View style={{ flex: 1, backgroundColor: 'white', }}>
        <KeyboardAwareScrollView
          style={{ width: window.width, height: window.height, }}
          ref='scroll'
          keyboardShouldPersistTaps="handled"
          onKeyboardWillShow={(frames) => { }}
        >
          <View style={styles.container}>
            <Image
              style={styles.logoImage}
              source={require('../../assets/tabbar/ZAT.png')} />

            <TextInput
              placeholder={I18n.t('login_act_hint')}
              placeholderTextColor='#A0A4A8'
              underlineColorAndroid='transparent'
              returnKeyType='next'
              onChangeText={this.onChangeAccount}
              style={[styles.textInput, { marginTop: DimensUtils.getDpScale(50) }]} />

            <TextInput
              placeholder={I18n.t('login_psw_hint')}
              placeholderTextColor='#A0A4A8'
              underlineColorAndroid={'transparent'}
              secureTextEntry={true}
              returnKeyType='done'
              onChangeText={this.onChangePassword}
              style={styles.textInput} />

            <TouchableOpacity
              activeOpacity={0.6}
              onPress={()=>{this.onPressLogin()}}
              disabled={false}>
              <LinearGradient
                colors={['#4DCC7B', '#12A0D2']}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={styles.loginButton}>
                <Text style={styles.loginTxt}>
                  {I18n.t('login')}
                </Text>
              </LinearGradient>
            </TouchableOpacity>

            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: 220,
            }}
            >
              {
                this.props.isAudit ? null :
                  <TouchableOpacity
                    activeOpacity={0.6}
                    onPress={this.onPressRegister}
                  >
                    <Text style={[styles.signInTxt, { marginRight: 54 }]}>
                      {I18n.t('login_sign_in')}
                    </Text>
                  </TouchableOpacity>
              }


              <TouchableOpacity
                activeOpacity={0.6}
                onPress={this.onPressForgotPsw}>
                <Text style={styles.signInTxt}>
                  {I18n.t('login_forgot_psw')}?
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Image
          resizeMode="stretch"
          style={styles.bottomBg}
          source={require('../../assets/tabbar/bottombg.png')} />

        <H5 visible={this.state.visible}
            sliderStyle={{top:topSize}}
            onSubmit={(data)=>{this.onSubmit(data)}}
            onClose={() => {this.onClose()}}/>
      </View>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  login: (loginParams) => {
    dispatch(authActions.login(loginParams))
  }
})

const mapStateToProps = (state) => ({
  authInfo: state.user.authInfo,
  isAudit: state.user.isAudit
})


export default connect(mapStateToProps, mapDispatchToProps)(LoginIndexPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  logoImage: {
    width: DimensUtils.getDpScale(100),
    height: DimensUtils.getDpScale(100),
    marginTop: DimensUtils.getDpScale(90),
  },
  bottomBg: {
    position: 'absolute',
    height: 41,
    bottom: 0,
    top: window.height - 41 - window.statusBarHeight,
    width: window.width
  },
  textInput: {
    padding: 0,
    width: 220,
    height: 50,
    fontSize: 18 * window.fontScale,
    borderBottomWidth: 0.5,
    borderBottomColor: '#d9d9d9'
  },
  loginButton: {
    width: 220,
    height: 50,
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#4DCC7B',
    borderRadius: 5,
  },
  loginTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  },
  signInTxt: {
    marginTop: 15,
    color: '#A0A4A8',
    fontSize: 14 * window.fontScale,
    textAlign: 'center'
  }
});
