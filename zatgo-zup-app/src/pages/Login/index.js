import LoginIndexPage from './LoginIndexPage'
import LoginRegisterPage from './LoginRegisterPage'
import LoginRegisterSetPwdPage from './LoginRegisterSetPwdPage'
import LoginRegisterCaptchaPage from './LoginRegisterCaptchaPage'
import LoginResetPasswordPage from './LoginResetPasswordPage'

export {
  LoginIndexPage,
  LoginRegisterPage,
  LoginRegisterSetPwdPage,
  LoginRegisterCaptchaPage,
  LoginResetPasswordPage
}