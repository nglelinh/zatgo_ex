import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {connect} from 'react-redux'
import {Toast} from 'antd-mobile';
import I18n from 'I18n';
import {window} from '@zatgo/constants';
import {authActions} from '../../redux/actions';

const eye_close = require('../../assets/common/com_eyes_close.png')
const eye_open = require('../../assets/common/com_eyes_open.png')

class LoginRegisterSetPwdPage extends Component {

  state = {
    loginPwd: true,
    loginPwdConfirm: true,
    payPwd: true,
    payPwdConfirm: true,
    "formData": {
      "loginPassword": '',
      "payPassword": '',
      "passwordConfirm": '',
      "payPasswordConfirm": '',
      'inviteCode': ''
    }
  }

  onChangeTxt = (field, value) => {
    const {formData} = this.state;
    formData[field] = value.replace(/\s+/g, "");
    this.setState({formData});
  }

  onRegister = () => {
    const {loginName, readNameAuthCode} = this.props;
    const {loginPassword, payPassword, passwordConfirm, payPasswordConfirm, inviteCode} = this.state.formData;

    if (loginPassword == '' || payPassword == '' || passwordConfirm == '' || payPasswordConfirm == '') {
      Toast.info(I18n.t('toast_inputText_null'))
      return;
    }
    if (loginPassword != passwordConfirm) {
      Toast.info(I18n.t('toast_inputText_inconsistent_login'))
      return;
    }
    if (payPassword != payPasswordConfirm) {
      Toast.info(I18n.t('toast_inputText_inconsistent_pay'))
      return;
    }
    if (payPassword.length != 6 || payPasswordConfirm.length != 6) {
      Toast.info(I18n.t('toast_inputText_length'))
      return;
    }
    var pattern = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
    var strpwd = pattern.test(loginPassword);
    if (!strpwd) {
      //密码格式不正确
      Toast.fail(I18n.t('toast_inputText_pwderror'))
      return
    }
    if (!/^[0-9]*$/.test(payPassword)) {
      //支付密码格式不正确
      Toast.fail(I18n.t('toast_inputText_length'))
      return
    }

    const params = {loginName, loginPassword, payPassword, readNameAuthCode, inviteCode}
    this.props.authRegister(params)

  }

  render() {
    return (
        <View style={styles.container}>

          <View style={[styles.textInputView, {marginTop: 20}]}>
            <TextInput
                placeholder={I18n.t('reg_psw_hint')}
                placeholderTextColor='#A0A4A8'
                underlineColorAndroid={'transparent'}
                secureTextEntry={this.state.loginPwd}
                returnKeyType='next'
                onChangeText={(txt) => this.onChangeTxt('loginPassword', txt)}
                style={styles.textInput}
            />
            <TouchableOpacity activeOpacity={0.6} onPress={() => {
              this.clickEvent('loginPassword')
            }}>
              <Image source={this.state.loginPwd ? eye_close : eye_open} style={{marginRight: 10}}/>
            </TouchableOpacity>
          </View>
          <View style={[styles.textInputView, {marginTop: 20}]}>

            <TextInput
                placeholder={I18n.t('reg_repsw_hint')}
                placeholderTextColor='#A0A4A8'
                underlineColorAndroid={'transparent'}
                secureTextEntry={this.state.loginPwdConfirm}
                returnKeyType='next'
                onChangeText={(txt) => this.onChangeTxt('passwordConfirm', txt)}
                style={styles.textInput}
            />
            <TouchableOpacity activeOpacity={0.6} onPress={() => {
              this.clickEvent('passwordConfirm')
            }}>
              <Image source={this.state.loginPwdConfirm ? eye_close : eye_open} style={{marginRight: 10}}/>
            </TouchableOpacity>
          </View>

          <View style={[styles.textInputView, {marginTop: 20}]}>
            <TextInput
                placeholder={I18n.t('reg_tradepsw_hint')}
                placeholderTextColor='#A0A4A8'
                underlineColorAndroid={'transparent'}
                secureTextEntry={this.state.payPwd}
                keyboardType="numeric"
                returnKeyType='next'
                maxLength={6}
                onChangeText={(txt) => this.onChangeTxt('payPassword', txt)}
                style={styles.textInput}
            />
            <TouchableOpacity activeOpacity={0.6} onPress={() => {
              this.clickEvent('payPassword')
            }}>
              <Image source={this.state.payPwd ? eye_close : eye_open} style={{marginRight: 10}}/>
            </TouchableOpacity>
          </View>

          <View style={[styles.textInputView, {marginTop: 20}]}>
            <TextInput
                placeholder={I18n.t('reg_retradepsw_hint')}
                placeholderTextColor='#A0A4A8'
                underlineColorAndroid={'transparent'}
                secureTextEntry={this.state.payPwdConfirm}
                keyboardType="numeric"
                returnKeyType='next'
                maxLength={6}
                onChangeText={(txt) => this.onChangeTxt('payPasswordConfirm', txt)}
                style={styles.textInput}
            />
            <TouchableOpacity activeOpacity={0.6} onPress={() => {
              this.clickEvent('payPwdConfirm')
            }}>
              <Image source={this.state.payPwdConfirm ? eye_close : eye_open} style={{marginRight: 10}}/>
            </TouchableOpacity>
          </View>

          <View style={[styles.textInputView, {marginTop: 20}]}>
            <TextInput
                placeholder={I18n.t('reg_inviteCod')}
                placeholderTextColor='#A0A4A8'
                underlineColorAndroid={'transparent'}
                returnKeyType='done'
                onChangeText={(txt) => this.onChangeTxt('inviteCode', txt)}
                style={styles.textInput}
            />
          </View>

          <TouchableOpacity
              activeOpacity={0.6}
              onPress={this.onRegister}>
            <LinearGradient colors={['#4DCC7B', '#12A0D2']} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                            style={styles.registerButton}>
              <Text style={styles.finishTxt}>{I18n.t('reg_registered')}</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
    );
  }

  clickEvent = (type) => {
    if (type === 'loginPassword') {
      let pwdState = !this.state.loginPwd
      this.setState({
        loginPwd: pwdState
      })

    } else if (type === 'passwordConfirm') {
      let pwdState = !this.state.loginPwdConfirm
      this.setState({
        loginPwdConfirm: pwdState
      })
    } else if (type === 'payPassword') {
      let pwdState = !this.state.payPwd
      this.setState({
        payPwd: pwdState
      })
    } else if (type === 'payPwdConfirm') {
      let pwdState = !this.state.payPwdConfirm
      this.setState({
        payPwdConfirm: pwdState
      })
    }
  }
}


const mapDispatchToProps = (dispatch) => ({
  authRegister: (params) => {
    dispatch(authActions.registerAuth(params))
  },
})

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(LoginRegisterSetPwdPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  registerButton: {
    width: 335,
    height: 50,
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#12A0D2',
    borderRadius: 5,
  },
  textInputView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 6,
    borderBottomColor: '#d9d9d9',
    borderBottomWidth: 0.5,
  },
  textInput: {
    flex: 1,
    height: 44,
  },
  finishTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  }
});
