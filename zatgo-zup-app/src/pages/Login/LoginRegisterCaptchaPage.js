import React, { Component } from 'react';
import { authActions } from '../../redux/actions'
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import I18n from 'I18n';
import { Toast } from 'antd-mobile'
import PinInput from '@zatgo/components/PinInput';
import { window } from '@zatgo/constants';
import TimerButton from '../../components/TimeButton/TimerButton'
import H5 from '../../components/SliderView';

class LoginRegisterCaptchaPage extends Component {

  state = {
    visible: false,
    "captcha": '',
    state: '这里显示状态',
  }

  onInputCompleted = (captcha) => {
    this.setState({ captcha })
  }

  onPressNextStep = () => {
    const { captcha } = this.state;
    const { loginName, forgotPsw } = this.props;
    if (captcha.length != 6) {
      Toast.info(I18n.t('toast_inputText_codelength'))
      return;
    }

    let params = {}
    if (forgotPsw === 'forgotPsw') {
      params = {
        'type': 'forgotPsw',
        'AuthCode': { 'registName': loginName, 'authCode': captcha },
        'params': { 'userName': loginName, 'verifyCode': captcha }
      }
      // Actions.forgotPasswordPage(params)
    } else {
      params = {
        'type': 'registerPsw',
        'AuthCode': { 'registName': loginName, 'authCode': captcha },
        'params': { loginName, 'readNameAuthCode': captcha }
      }
      // Actions.password(params);
    }

    this.props.verifyAuthCode(params)

  }

  onClose = () => {
    this.setState({
      visible: false
    })
  }

  onSubmit = (data) => {
    const { loginName } = this.props
    let params = data ? {'aliyunValidateCode': data} : {};
    params['mobilePhone'] = loginName;
    this.TimerBtn._shouldStartCountting(true);
    this.props.sendPhoneAuthCode(params)
  }

  onPressResend = (shouldStartCounting) => {
    this.TimerBtn._setEnbleState()
    const { loginName } = this.props
    this.setState({
      state: '正在请求验证码',
      visible: true
    })
    if (loginName.indexOf('@') > -1) {
      //TODO email
      // this.props.sendEmailAuthCode({ 'data': loginName })
    } else {
      //TODO mobilePhone
      // this.props.sendPhoneAuthCode({ 'data': loginName })
    }
    // shouldStartCounting && shouldStartCounting(true)
  }

  render() {
    const { captcha } = this.state;
    const { loginName } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.promptTxt}>
          {`${I18n.t('page_content_Code_send')} ${loginName}`}
        </Text>
        <PinInput
          ref='codeInputRef'
          borderType='underline'
          size={50}
          inputPosition='center'
          codeInputStyle={styles.codeInputStyle}
          onFulfill={this.onInputCompleted}
        />
        <TouchableOpacity
          activeOpacity={0.6}
          onPress={this.onPressNextStep}
        >
          <LinearGradient colors={['#4DCC7B', '#12A0D2']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
            style={styles.registerButton}>
            <Text style={styles.nextStepTxt}>{I18n.t('reg_next_step')}</Text>
          </LinearGradient>
        </TouchableOpacity>

        <TimerButton
            ref={(e)=>{this.TimerBtn = e}}
            timerTitle={I18n.t('reg_resend')}
          enable={loginName.length}
          first={true}
          onClick={(shouldStartCounting) => { this.onPressResend(shouldStartCounting) }}
          timerEnd={() => {
            this.setState({
              state: '倒计时结束'
            })
          }} />
        <H5 visible={this.state.visible}
            onSubmit={(data)=>{this.onSubmit(data)}}
            onClose={() => {this.onClose()}}/>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  verifyAuthCode: (params) => {
    dispatch(authActions.verifyAuthCode(params))
  },
  sendPhoneAuthCode: (params) => {
    dispatch(authActions.sendPhoneAuthCode(params))
  },
  sendEmailAuthCode: (params) => {
    dispatch(authActions.sendEmailAuthCode(params))
  }
})

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(LoginRegisterCaptchaPage);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  promptTxt: {
    marginTop: 10,
    color: '#797F85',
    fontSize: 14 * window.fontScale
  },
  codeInputStyle: {
    fontSize: 30 * window.fontScale,
    marginTop: 10
  },
  registerButton: {
    width: 335,
    height: 50,
    marginTop: 55,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#12A0D2',
    borderRadius: 5,
  },
  nextStepTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  },
  resendTxt: {
    color: '#4DCC7B',
    fontSize: 16 * window.fontScale,
    marginTop: 20
  }
});
