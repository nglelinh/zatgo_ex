import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Toast } from 'antd-mobile'
import I18n from 'I18n';
import { window } from '@zatgo/constants';
import { authActions } from '../../redux/actions';
import H5 from '../../components/SliderView';


class LoginRegisterPage extends Component {

  state = {
    visible:false,
    "loginName": '',
  }

  componentWillMount() {
    const title = this.props.forgotPsw === 'forgotPsw'
      ? I18n.t('title_current_Account')
      : I18n.t('title_register')

    Actions.refresh({ title: title })
  }

  onClose = () => {
    this.setState({
      visible:false
    })
  }

  onSubmit = (data) => {
    const {loginName} = this.state;
    let params = data ? {'aliyunValidateCode': data} : {};
    params['loginName'] = loginName;
    if (this.props.forgotPsw) {
      params['forgotPsw'] = 'forgotPsw'
      this.props.usernameExist(params)
    } else {
      this.props.usernameExist(params)
    }
  }

  onRegister = () => {
    const { loginName } = this.state
    if (loginName == '') {
      Toast.fail(I18n.t('toast_inputText_null'))
      return
    }
    // if (loginName.indexOf('@') > -1) {
    //   var patternEamil = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    //   var strEmail = patternEamil.test(loginName);
    //   if (strEmail) {
    //     //TODO email
    //     // this.props.sendEmailAuthCode({'data': loginName})
    //   } else {
    //     //邮箱格式不正确
    //     Toast.fail(I18n.t('toast_inputText_usernameerror'))
    //     return
    //   }
    // } else {
    var patternPhone = /^[1][3,4,5,7,8,9][0-9]{9}$/
    var strPhone = patternPhone.test(loginName);
    if (strPhone) {
      // this.props.sendPhoneAuthCode({'data': loginName})
    } else {
      //手机格式不正确
      Toast.fail(I18n.t('toast_inputText_usernameerror'))
      return
    }
    this.setState({
      visible:true
    })
  }

  onChangeText = (value) => {
    let loginName = value.replace(/\s+/g, "");
    this.setState({ loginName });
  }
  render() {
    const { loginName } = this.state;
    return (
      <View style={styles.container}>
        <TextInput
          placeholder={I18n.t('login_act_hint')}
          placeholderTextColor='#A0A4A8'
          underlineColorAndroid={'transparent'}
          returnKeyType='done'
          onChangeText={this.onChangeText}
          style={styles.textInput}
        />
        <TouchableOpacity
          activeOpacity={0.6}
          onPress={this.onRegister}>
          <LinearGradient
            colors={['#4DCC7B', '#12A0D2']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.registerButton}
          >
            <Text style={styles.nextStepTxt}>
              {I18n.t('reg_next_step')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
        <H5 visible={this.state.visible}
            onSubmit={(data)=>{this.onSubmit(data)}}
            onClose={() => {this.onClose()}}/>

      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  sendPhoneAuthCode: (params) => {
    dispatch(authActions.sendPhoneAuthCode(params))
  },
  sendEmailAuthCode: (params) => {
    dispatch(authActions.sendEmailAuthCode(params))
  },
  usernameExist: (params) => {
    dispatch(authActions.usernameExist(params))
  },
})

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(LoginRegisterPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  registerButton: {
    width: 335,
    height: 50,
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#12A0D2',
    borderRadius: 5,
  },
  textInput: {
    padding: 0,
    width: 335,
    height: 50,
    marginTop: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: '#d9d9d9'
  },
  nextStepTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  }
});
