import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { Toast } from 'antd-mobile';
import I18n from 'I18n';
import { authActions } from '../../redux/actions'
import { window } from '@zatgo/constants';

const eye_close = require('../../assets/common/com_eyes_close.png')
const eye_open = require('../../assets/common/com_eyes_open.png')

class ForgotPwdSetPwdPage extends Component {

  state = {
    'loginPwd': true,
    'loginPwdConfirm': true,
    "formData": {
      "newPassword": '',
      "passwordConfirm": '',
    }
  }

  onChangeTxt = (field, value) => {
    const { formData } = this.state;
    formData[field] = value.replace(/\s+/g, "");
    this.setState({ formData });
  }

  onSubmit = () => {
    const { userName, verifyCode } = this.props;
    const { newPassword, passwordConfirm } = this.state.formData;
    if (newPassword != passwordConfirm) {
      Toast.info(I18n.t('toast_inputText_inconsistent_login'))
      return;
    }
    if (newPassword === '' || passwordConfirm === '') {
      Toast.info(I18n.t('toast_inputText_null'))
      return;
    }
    const params = {
      "newPassword": newPassword,
      "userName": userName,
      "verifyCode": verifyCode
    }

    var pattern = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
    var strpwd = pattern.test(newPassword);
    if (!strpwd) {
      //密码格式不正确
      Toast.fail(I18n.t('toast_inputText_pwderror'))
      return
    }
    this.props.retrievePassword(params)

  }

  render() {
    return (
      <View style={styles.container}>

        <View style={[styles.textInputView, { marginTop: 20 }]}>
          <TextInput
            placeholder={I18n.t('reg_psw_hint')}
            placeholderTextColor='#A0A4A8'
            underlineColorAndroid='transparent'
            secureTextEntry={this.state.loginPwd}
            returnKeyType='next'
            onChangeText={(txt) => this.onChangeTxt('newPassword', txt)}
            style={styles.textInput}
          />
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => { this.clickEvent('newPassword') }}>
            <Image
              source={this.state.loginPwd ? eye_close : eye_open}
              style={{ marginRight: 10 }}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.textInputView, { marginTop: 20 }]}>

          <TextInput
            placeholder={I18n.t('reg_repsw_hint')}
            placeholderTextColor='#A0A4A8'
            underlineColorAndroid='transparent'
            secureTextEntry={this.state.loginPwdConfirm}
            returnKeyType='next'
            onChangeText={(txt) => this.onChangeTxt('passwordConfirm', txt)}
            style={styles.textInput}
          />
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => { this.clickEvent('passwordConfirm') }}
          >
            <Image
              source={this.state.loginPwdConfirm ? eye_close : eye_open}
              style={{ marginRight: 10 }}
            />
          </TouchableOpacity>
        </View>


        <TouchableOpacity
          activeOpacity={0.6}
          onPress={this.onSubmit}
        >
          <LinearGradient
            colors={['#4DCC7B', '#12A0D2']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.registerButton}>
            <Text style={styles.finishTxt}>
              {I18n.t('reg_submit')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }

  clickEvent = (type) => {
    if (type === 'newPassword') {
      let pwdState = !this.state.loginPwd
      this.setState({
        loginPwd: pwdState
      })

    } else if (type === 'passwordConfirm') {
      let pwdState = !this.state.loginPwdConfirm
      this.setState({
        loginPwdConfirm: pwdState
      })
    }
  }
}


const mapDispatchToProps = (dispatch) => ({
  retrievePassword: (params) => {
    dispatch(authActions.retrievePassword(params))
  },
})

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPwdSetPwdPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  registerButton: {
    width: 335,
    height: 50,
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#12A0D2',
    borderRadius: 5,
  },
  textInputView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 6,
    borderBottomColor: '#d9d9d9',
    borderBottomWidth: 0.5,
  },
  textInput: {
    flex: 1,
    height: 44,
  },
  finishTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  }
});
