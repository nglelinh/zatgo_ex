/*
 *  @File   : CollectionView
 *  @Author : lsl
 *  @Date   : 2018-4-1 15:4:47
 *  @Last Modified   : 2018-4-1 15:4:47
 *  @Desc 收款
 */
import React, { PureComponent } from 'react';
import QRCode from 'react-native-qrcode';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  Image,
  Linking,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Platform
} from 'react-native';
import I18n from 'I18n';
import window from '@zatgo/constants/window';

const ZatGoLogo = require('@zatgo/assets/tabbar/Home.png');

class CollectionView extends PureComponent {

  componentDidMount() {
    this.props.getPrimaryAccount();
    this.props.getCointypes();
  }

  onChangeCurrency = () => {
    Actions.selectCoin();
  }

  openUrl = () => {
    Linking.openURL('http://www.zatgo.net')
      .catch(err => console.error('An error occurred', err));
  }

  getQrString = () => {
    const { authInfo, coinInfo: { coinType, networkType } } = this.props;
    return (authInfo&&authInfo.userId)?
    JSON.stringify({
      'receiptUserId': authInfo.userId,
      "coinType": coinType,
      "networkType": networkType,
      "channel": "ZatGoPay",
      "device": 2,
      "sign": "112233",
    }):null;
  }

  render() {
    const {  coinInfo: { coinType, coinImage } } = this.props;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.cardContainer}>
          <Image
            style={styles.logo}
            source={coinImage ? { uri: coinImage } : ZatGoLogo} />
          <Text style={styles.currencyTxt}>
            {coinType ? coinType : ''}
          </Text>

          <View style={{ marginTop: 20 }}>
            {this.getQrString()? <QRCode value={this.getQrString()} size={200} />:
                <View style={{width:200,height:200,alignItems:'center',justifyContent:'center',borderWidth:0.5,borderColor:'#bec4c4'}}>
                  <Text>{I18n.t('colection_error')}</Text>
                </View>}
          </View>

          <TouchableOpacity
            activeOpacity={0.6}
            onPress={this.onChangeCurrency}>
            <LinearGradient
              colors={['#4DCC7B', '#12A0D2']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.changeButton}>
              <Text style={styles.changeTxt}>
                {I18n.t('colection_change_currency')}
              </Text>
            </LinearGradient>
          </TouchableOpacity>

          <Text style={styles.promptTxt}>
            {I18n.t('page_content_collection')}
          </Text>

          <TouchableOpacity activeOpacity={0.6} onPress={this.openUrl}>
            <Text style={styles.website}>www.zatgo.net</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

export default CollectionView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardContainer: {
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    marginBottom: 60,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  logo: {
    width: 40,
    height: 40,
    marginTop: 20
  },
  currencyTxt: {
    color: '#323B43',
    fontSize: 18 * window.fontScale,
    marginTop: 10
  },
  changeButton: {
    width: 170,
    height: 50,
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  changeTxt: {
    color: 'white',
    fontSize: 14 * window.fontScale,
  },
  promptTxt: {
    marginTop: 30,
    marginLeft: 10,
    marginRight: 10,
    textAlign: 'center',
    fontSize: 14 * window.fontScale,
  },
  website: {
    fontSize: 12 * window.fontScale,
    color: '#4DCC7B',
    marginBottom: 35,
    textDecorationLine: "underline"
  }
})
