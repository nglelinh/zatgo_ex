import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import moment from 'moment';
import { window } from '@zatgo/constants';
import { StringUtils } from '@zatgo/utils';

const RecordListItem = ({ item }) => (
    <View>
      <View style={styles.recordContainer}>
        <View>
          <Text style={styles.actTxt}>
            {item.amount < 0 ? item.receiptUserName : item.payUserName}
          </Text>
          <Text style={styles.dateTxt}>
            {moment(Number(item.paymentDate)).format('MM-DD HH:mm')}
          </Text>
        </View>
        <View style={styles.amountContainer}>
          <Text
              style={[styles.amountTxt,{color: item.amount < 0 ? "#323B43":"#4DCC7B"}]}
              numberOfLines={1}
              ellipsizeMode="tail"
          >
            {item.amount < 0 ? StringUtils.formatE(item.amount) : `+${StringUtils.formatE(item.amount)}`}
            <Text style={[styles.typeTxt,{color: item.amount < 0 ? "#323B43":"#4DCC7B"}]}>
              {item.coinType ? ` ${item.coinType}` : ''}
            </Text>
          </Text>
        </View>
      </View>
      <View style={styles.separator}/>
    </View>
)

export default RecordListItem;

const styles = StyleSheet.create({
  recordContainer: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0,
    backgroundColor: 'white',
  },
  actTxt: {
    color: '#323B43',
    fontSize: 15 * window.fontScale,
  },
  dateTxt: {
    color: '#797F85',
    fontSize: 11 * window.fontScale,
    marginTop: 7
  },
  amountContainer: {
    flex:1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    // width: 150,
  },
  amountTxt: {
    flex: 1,
    textAlign: 'right',
    color: '#4DCC7B',
    fontSize: 20 * window.fontScale,
  },
  typeTxt: {
    color: '#4DCC7B',
    fontSize: 12 * window.fontScale,
    marginLeft: 5,
  },
  separator: {
    height: 0.5,
    backgroundColor: '#d4d4d4'
  },
});
