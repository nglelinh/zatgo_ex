/*
 *  @File   : CollectionPayRecordPage
 *  @Author : lsl
 *  @Date   : 2018-5-2 16:28:52
 *  @Last Modified   : 2018-5-2 16:28:52
 *  @Desc 交易记录
 */
import React, { PureComponent } from 'react';
import { View, ActivityIndicator, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';
import JXFlatList from '../../components/JXFlatList';
import I18n from 'I18n';
import { walletActions } from '../../redux/actions';
import { theme } from '@zatgo/constants';
import RecordListItem from './components/RecordListItem';

var pageNo = 1;

class CollectionPayRecordPage extends PureComponent {

  componentDidMount() {
    this.props.getPaymentList({ 'pageNo': pageNo })
    pageNo++;
  }

  componentWillUnmount() {
    pageNo = 1;
  }

  _refreshing = () => {
    pageNo = 1
    this.props.getPaymentList({ 'pageNo': pageNo })
    pageNo++
  }

  _onload = () => {
    if (this.props.paymentListStatus) {
      this.props.getPaymentList({ 'pageNo': pageNo })
      pageNo++
    }
  }

  _footer = () => {
    if (this.props.paymentListStatus) {
      return (
        <View style={styles.footerStyle}>
          <ActivityIndicator size="small" color="#888888" />
        </View>
      )
    } else {
      return (
        <View style={styles.footerStyle}>
          <Text>{I18n.t('order_no_more')}</Text>
        </View>
      )
    }
  }

  renderSeparator = () => (
    <View style={styles.separator} />
  )

  render() {
    return (
      <View style={styles.container}>
        <JXFlatList
          data={this.props.paymentList}
          refreshing={0}
          pageSize={20}
          onRefresh={this._refreshing}
          renderItem={RecordListItem}
          ItemSeparatorComponent={this.renderSeparator}
          ListFooterComponent={this._footer}
          onEndReached={this._onload}
        />
      </View>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  getPaymentList: (params) => {
    dispatch(walletActions.getPaymentList(params))
  }
})

const mapStateToProps = (state) => ({
  paymentList: state.wallet.paymentList,
  paymentListStatus: state.wallet.paymentListStatus
})


export default connect(mapStateToProps, mapDispatchToProps)(CollectionPayRecordPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  separator: {
    height: 0.5,
    backgroundColor: '#d4d4d4'
  },
  footerStyle: {
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2
  }
});
