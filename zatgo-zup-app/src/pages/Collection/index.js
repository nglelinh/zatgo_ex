import CollectionPage from './CollectionPage'
import CollectionPayRecordPage from './CollectionPayRecordPage'

export {
  CollectionPage,
  CollectionPayRecordPage,
}