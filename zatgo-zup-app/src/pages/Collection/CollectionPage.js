/*
 *  @File   : CollectionPage
 *  @Author : lsl
 *  @Date   : 2018-4-1 15:4:47
 *  @Last Modified   : 2018-4-1 15:4:47
 *  @Desc 收款
 */
import { connect } from 'react-redux';
import { walletActions } from '../../redux/actions';
import CollectionView from './components/CollectionView';

const mapDispatchToProps = (dispatch) => ({
  getCointypes: () => {
    dispatch(walletActions.getCointypes())
  },
  getPrimaryAccount: () => {
    dispatch(walletActions.getPrimaryAccount())
  }
})

const select = (primaryAccount, coinTypes, ownProps) => {
  const coinObj = {}
  if (ownProps.currency) {
    const { coinType, coinNetworkType, coinImage } = ownProps.currency;
    coinObj.coinType = coinType;
    coinObj.networkType = coinNetworkType;
    coinObj.coinImage = coinImage;
  } else if (primaryAccount) {
    const { coinNetworkType, coinType, coinImage } = primaryAccount
    coinObj.coinType = coinType;
    coinObj.networkType = coinNetworkType;
    coinObj.coinImage = coinImage;
  } else if (coinTypes && coinTypes[0]) {
    const { coinType, coinNetworkType, coinImage } = coinTypes[0]
    coinObj.coinType = coinType;
    coinObj.networkType = coinNetworkType;
    coinObj.coinImage = coinImage;
  }
  return coinObj;
}

const mapStateToProps = (state, ownProps) => ({
  authInfo: state.user.authInfo,
  coinInfo: select(state.wallet.primaryAccount, state.wallet.coinTypes, ownProps),
})

export default connect(mapStateToProps, mapDispatchToProps)(CollectionView);
