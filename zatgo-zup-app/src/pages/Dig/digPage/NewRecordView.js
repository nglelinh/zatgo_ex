/**
 * Created by zhoujianxin on 2018/7/31.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  DeviceEventEmitter
} from 'react-native';
import window from "../../../constants/window";
import helpFun from "../components/helpFun";
import {digActions} from "../../../redux/actions";
import {connect} from "react-redux";
import moment from 'moment';
import I18n from 'I18n';

moment.locale('en', {
  relativeTime : {
    future: "%s",
    past:   "%s",
    s:  "Just now",
    m:  "Just now",
    mm: "%d minutes",
    h:  "1 hour",
    hh: "%d hours",
    d:  "1 day",
    dd: "%d days",
    M:  "1 month",
    MM: "%d months",
    y:  "1 year",
    yy: "%d years"
  }
});


class NewRecordView extends Component {

  componentDidMount() {
    this.props.getUserMiningRecord({pageNo:1});
    //收到监听
    this.listener = DeviceEventEmitter.addListener('refresh',(e)=>{this.props.getUserMiningRecord({pageNo:1});});
  }
  componentWillUnmount(){
    // 移除监听
    this.listener.remove();
  }

  render() {
    return (
        <View style={styles.container}>
          {this.latestRecord()}
        </View>
    );
  }

  latestRecord = () => {
    const {miningRecord} = this.props;
    var itemViews = []
    let items = miningRecord.length>5?miningRecord.slice(0,5):miningRecord;
    for (let i = 0; i < items.length; i++) {
      itemViews.push(
          <View key={i} style={[{flexDirection:'row',height:44}]}>
            <View style={[styles.itemView,{}]}>
              <Image style={{width:24,height:24}} source={{uri:items[i].coinImage}}/>
              <Text style={[styles.titleTextStyle,{fontWeight:'bold'}]}>{I18n.t('dig_obtain')}</Text>
            </View>

            <View style={[styles.itemView,{flex:1}]}>
              <Text style={{color: '#4DCC7B', fontSize: window.fontScale * 15}}>{items[i].coinNum}</Text>
              <View style={{marginBottom:0}}>
                <Text style={{fontSize: window.fontScale * 11, color: '#140000', marginLeft: 2,marginTop:2}}>{items[i].coinType}</Text>
              </View>
            </View>

            <View style={[styles.itemView,{}]}>
              <Text style={[styles.titleTextStyle,{color:'#A0A4A8'}]}>{helpFun.getTime(items[i].createDate)}</Text>
            </View>
          </View>
      )
    }
    return itemViews
  }
}

const mapDispatchToProps = (dispatch) => ({
  getUserMiningRecord:(params)=>{dispatch(digActions.getMiningRecord(params))}
})

const mapStateToProps = (state) => ({
  miningRecord:state.dig.miningRecord
})

export default connect(mapStateToProps, mapDispatchToProps)(NewRecordView);

const styles = StyleSheet.create({
  container: {
      flex: 1,
      marginLeft: 15,
      marginRight: 15,
    },
  titleTextStyle: {
    marginLeft: 15,
    // marginRight: 15,
    color: '#323B43',
    fontSize: 13 * window.fontScale,
  },
  itemView:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
  }
});