/**
 * Created by zhoujianxin on 2018/5/21.
 * @Desc 挖矿页
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, DeviceEventEmitter, StatusBar } from 'react-native';
import DigHeadView from './DigHeadView';
import { digActions } from "../../../redux/actions";
import { connect } from 'react-redux';

class DigPage extends Component {

  componentDidMount() {
    this.props.loginTask();
    this.listener = DeviceEventEmitter.addListener('HashRaseRefresh', (e) => {
      this.props.loginTask();
      this.props.getComputePowerTop();
    });
    this.listenerStatusBar = DeviceEventEmitter.addListener('StatusBar', (e) => {
      StatusBar.setBarStyle('light-content', true);
      this.setState({ a: 1 })
    });

    this.request();
  }

  componentWillUnmount() {
    // 移除监听
    this.listener.remove();
    this.listenerStatusBar.remove();
  }

  onPullRelease = () => {
    this.props.getUserMiningData();
    this.props.loginTask();
    this.props.getComputePowerTop();
    DeviceEventEmitter.emit('refresh'); //发监听
  }

  request = () => {
    this.props.getUserMiningData();
    this.props.getComputePowerTop();
  }

  render() {
    const { computePowerTop } = this.props;
    let computeNum = computePowerTop.self;
    StatusBar.setBackgroundColor('#00000000');
    return (
      <View style={styles.container}>
        <DigHeadView miningData={this.props.miningData}
          computeNum={computeNum && computeNum.computePower}
          refreshData={() => {
            DeviceEventEmitter.emit('refresh'); //发监听
          }}
          rankData={this.props.computePowerTop}
          onTimeEnd={this.request}
          onPullRelease={this.onPullRelease}

        />
      </View>
    );
  }

}


const mapDispatchToProps = (dispatch) => ({
  getUserMiningData: () => {
    dispatch(digActions.getUserMiningData())
  },
  getComputePowerTop: () => {
    dispatch(digActions.getComputePowerTop())
  },
  loginTask: () => {
    dispatch(digActions.loginTask())
  }
})

const mapStateToProps = (state) => ({
  miningData: state.dig.miningData,
  computePowerTop: state.dig.computePowerTop,
})


export default connect(mapStateToProps, mapDispatchToProps)(DigPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },



});