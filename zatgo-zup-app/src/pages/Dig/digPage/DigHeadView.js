/**
 * Created by zhoujianxin on 2018/5/21.
 * @Desc 挖矿游戏
 */
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  DeviceEventEmitter
} from 'react-native';
import {Grid,Flex} from 'antd-mobile';
import moment from 'moment';
import {Actions} from 'react-native-router-flux';
import I18n from 'I18n';
import helpFun from '../components/helpFun';
import window from '../../../constants/window';
import AnimatedPage from '../components/AnimationPage';
import CustomImage from '../../../components/CustomImage/customImage';
import RankingList from '../components/RankingList';
import NewRecordView from './NewRecordView';
import {PullView} from '../../../components/PullToRefreshLayout'

let num = 0;
let data;
let arrView;
let isNoData;
let noView;

const getActivities = () => [
  {
    icon: require('@zatgo/assets/dig/MyAssets.png'),
    text: I18n.t('dig_my_assets'),
  },
  {
    icon: require('@zatgo/assets/dig/GetCalculate.png'),
    text: I18n.t('dig_obtain_compute'),
  },
  {
    icon: require('@zatgo/assets/dig/travel.png'),
    text: I18n.t('dig_secret'),
  }
]
const ActivityView = ({onItemClick}) => (
    <View style={styles.actContainer}>
      <Grid data={getActivities()}
            style={{alignItems:'center'}}
            columnNum={3}
            hasLine={false}
            itemStyle={{height:undefined,}}
            renderItem={(item,index) =>
                <TouchableOpacity
                    style={{alignItems: 'center', justifyContent: 'center'}}
                    onPress={() => {
                      onItemClick(index)
                    }}>
                  <Image
                      style={styles.icon}
                      source={item.icon}/>
                  <Text style={[styles.font12,{textAlign: 'center',marginTop:10}]}>{item.text}</Text>
                </TouchableOpacity>}
      />
    </View>
)

const UserImage = ({data}) => (
    <View style={{alignItems: 'center', justifyContent: 'center',position: 'absolute', top: 0,left:0,right:0,zIndex:9999,elevation: 99999}}>
      <View style={styles.imageStyle}>
        <CustomImage
            defaultImage={require('../../../assets/dig/wakagi.png')}
            errImage={require('../../../assets/dig/wakagi.png')}
            uri={data.iconUrl ? data.iconUrl : 'xxxx.png'}
            style={{width: 50, height: 50, borderRadius: 25}}
            defaultStyle={{width: 50, height: 50, borderRadius: 25}}
        />
      </View>
      <View style={{width: 1, height: 15, backgroundColor: '#E6E6E6'}}/>
    </View>
)

export default class DigHeadView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      /*
       初始化动画值
       * */
      coinNum: 120,
      temp:[],
    }
  }
  componentDidMount(){
    this.intervalTime(this.getTime());

  }

   dealMiningData = (miningData)=> {
    let arr = [];
    let x,y;
      miningData&&miningData.length>0&&miningData.forEach((item,index)=>{
      if(moment().isAfter(moment(item.startTime))){
        let sign =index>0?index%10:index
        if(sign<5) {
          x = Math.floor(Math.random() * (window.width/5-50)+(window.width/5)*sign);
          y = Math.floor(Math.random() * (90) + 5);
        }else {
          x = Math.floor(Math.random() * (window.width/5-50)+(window.width/5)*(sign-5));
          y = Math.floor(Math.random() * (90) + 120);
        }
        item['x'] = x;
        item['y'] = y;
        arr.push(item);
      }else {

      }
    })
      num=0;
      data= arr;
      arrView= arr.slice(0, 10);
      isNoData= 0;
      noView= arr.slice(0, 10).length > 0 ? false : true;
  }

  AnimationView = () => {
    let items = arrView;
    if(items&&items.length===0)return;
    return items.map((item,index)=> {
      return(
      <View style={{position: 'absolute', left: item.x, top: item.y}} key={index+10*isNoData}>
        <AnimatedPage data={item} callBack={this.display}/>
      </View>)
    })
  }

  render() {
    console.log('1111');
    const { rankData, onPullRelease} = this.props;
    let data = rankData && rankData.self ? rankData.self : {};
    this.dealMiningData(this.props.miningData);
    return (
        <View style={styles.container}>
          <PullView style={{backgroundColor: 'white', flex: 1, width: window.width}}
                    onPullRelease={(resolve) => {
                      setTimeout(() => {
                        resolve();
                      }, 2000);
                      onPullRelease()
                    }}
          >
            <View>
              <ImageBackground source={require('@zatgo/assets/dig/Digbg.png')}
                               style={{height: 410, width: window.width}}>
                <View style={{
                  width: window.width,
                  height: window.isIphoneX ? 68 : 44,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop:20
                }}>
                  <Text style={{color: '#FFFFFF', fontSize: 21, fontWeight: 'bold'}}>{I18n.t('title_dig')}</Text>
                </View>
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                  {noView ?
                      <View style={{marginBottom: 60}}>
                        <AnimatedPage producing={true}
                                      onEnd={() => {
                                        this.props.onTimeEnd && this.props.onTimeEnd()
                                      }}
                        />
                      </View> :
                      this.AnimationView()
                  }
                </View>
              </ImageBackground>
            </View>
            <View style={{height: 140}}>
              <View style={[styles.userViewStyle, {backgroundColor: 'transparent'}]}>
                <UserImage data={data}/>
                <View style={styles.userInfoStyle}>
                  <View style={styles.textStyle}>
                    <Text style={{fontSize: 21, color: '#323B43'}}>{data.computePower}</Text>
                    <Text style={{fontSize: 14, color: '#8299AD', marginTop: 6}}>算力</Text>
                  </View>
                  <View style={styles.textStyle}>
                    <Text style={{fontSize: 21, color: '#323B43'}}>{data.sort}</Text>
                    <Text style={{fontSize: 14, color: '#8299AD', marginTop: 6}}>排名</Text>
                  </View>
                </View>
              </View>
              <ActivityView onItemClick={(data) => {
                this.onItemClick(data)
              }}/>
            </View>

            <View style={styles.lineTwo}/>

            <View>
              <View style={{flexDirection: 'row'}}>
                <View style={[styles.titleViewStyle, {flex: 1}]}>
                  <Text style={styles.titleTextStyle}>{I18n.t('dig_view_latest_records')}</Text>
                </View>
                <TouchableOpacity onPress={() => {
                  Actions.push('digRecords')
                }}>
                  <View style={styles.titleViewStyle}>
                    <Text style={styles.moreTextStyle}>{I18n.t('dig_view_more_records')}</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <NewRecordView/>
            </View>

            <View style={styles.lineTwo}/>
            <RankingList rankData={rankData}/>
          </PullView>
        </View>
    );
  }

  display = (data) =>{
    num +=1;
      if(num===arrView.length){
        num=0;
        let dataState = Math.ceil(data.length/10)-1>= isNoData+1?false:true;
        if(dataState){
          noView= dataState ? true : false,
          this.props.onTimeEnd&&this.props.onTimeEnd();
        }else {
            isNoData= isNoData + 1;
            arrView=dataState ? data.slice(10 * isNoData) : data.slice(10 * (isNoData), 10 + 10 * (isNoData));
            this.setState({temp:arrView});
        }

      }
  }

  intervalTime= (times)=> {
    this.interval = setInterval(() => {
      const date = helpFun.getDateData(times);
      if (date) {
        // this.setState(date);
      } else {
        this.stop();
        this.intervalTime(this.getTime());
        isNoData=0;
        this.props.onTimeEnd&&this.props.onTimeEnd();
      }
    }, 1000);
  }

  onItemClick = (data)=>{
    if(data===0){
      Actions.assetHub();
    }else if(data===1) {
      DeviceEventEmitter.emit('HashRaseRefresh',); //发监听
      Actions.digTask();
    }else if(data===2){
      Actions.largeTravel();
    }
  }

  getTime = ()=>{
    let now = Number(moment(moment().format('YYYY-MM-DD HH')).format('x'))+ 1 * 60 * 60 * 1000;
    let  date = new Date(now);
    return date.toISOString(); //2018-07-30 23:36:09
  }

  componentWillUnmount() {
    this.stop();
  }

  stop = ()=> {
    clearInterval(this.interval);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    // maxHeight: 500,
  },
  calculateText: {
    marginTop: 40,
    marginLeft: 20,
    borderWidth: 0.5,
    borderColor: "white",
    justifyContent: 'center',
    borderRadius: 16,
    flexDirection:'row'
  },
  coinText: {
    marginTop: 3,
    marginLeft: 20,
    borderWidth: 0.5,
    borderColor: "white",
    justifyContent: 'center',
    borderRadius: 16,
    minHeight: 32,
    maxWidth: 160
  },
  actContainer: {
    backgroundColor: 'white',
    height:60,
    marginTop:5,
    top: -40
  },
  gridHeader: {
    height: 40,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderBottomWidth: 0.5,
    borderBottomColor: '#d4d4d4',
  },
  textStyle: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
    margin: 5
  },
  headerTxt: {
    fontSize: 14 * window.fontScale,
    color: '#323B43',
    marginLeft: 10
  },
  font14: {
    color: '#323B43',
    marginTop: 8,
    fontSize: 14 * window.fontScale,
  },
  font13: {
    fontSize: 13 * window.fontScale,
    color: '#323B43',
    marginTop: 6,
  },
  openTxt: {
    color: '#A0A4A8',
    fontSize: 10 * window.fontScale,
    marginTop: 5,
  },
  icon: {
    width: 24,
    height: 24
  },
  imageStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    width: 60,
    height: 60,
    borderRadius: 30
  },
  userViewStyle:{
    height: 110,
    marginLeft: 20,
    marginRight: 20,
    justifyContent:'flex-end',
    backgroundColor: 'white',
    position: 'relative',
    top: -60,
  },
  userInfoStyle: {
    flexDirection: 'row',
    height: 80,
    margin:5,
    backgroundColor: 'white',
    borderRadius: 10,
    //以下是阴影属性：
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 5,
    shadowColor: '#808a92',
    //注意：这一句是可以让安卓拥有灰色阴影
    elevation: 2,
  },
  titleViewStyle: {
    height: 44,
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  titleTextStyle: {
    marginLeft: 15,
    marginRight: 15,
    color: '#323B43',
    fontSize: 16 * window.fontScale,
    fontWeight:'bold'
  },
  moreTextStyle: {
    marginLeft: 15,
    marginRight: 15,
    color: '#8299AD',
    fontSize: 14 * window.fontScale,
    fontWeight:'bold'
  },
  line: {
    flex: 1,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#D9D9D9',
    height: 0.5
  },
  lineTwo: {
    flex: 1,
    backgroundColor: '#F0F2F5',
    height: 10
  },
  itemView:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
  }
});