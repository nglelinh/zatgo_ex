import DigPage from './digPage/DigPage'
import DigRecordspage from './digHistoryPage/DigRecordspage'
import ComputePowerRecord from './digHistoryPage/ComputePowerRecord'
import LargeTravelPage from './largeTravel/LargeTravelPage'
import DigTaskPage from './digTask/DigTaskPage'


export {
  DigPage,
  DigRecordspage,
  ComputePowerRecord,
  LargeTravelPage,
  DigTaskPage
}