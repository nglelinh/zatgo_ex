/**
 * Created by zhoujianxin on 2018/8/1.
 * @Desc
 */

import React, {Component} from 'react';
import {StyleSheet, ImageBackground, Text, View, Image,ScrollView,StatusBar} from 'react-native';
import window from "../../../constants/window";

export default class LargeTravelPage extends Component {
  render() {
    StatusBar.setBarStyle('light-content',true);
    return (
        <View style={styles.container}>
          <ScrollView style={{flex: 1}}
                      showsVerticalScrollIndicator={false}
                      bounces={false}
          >
            <ImageBackground source={require('@zatgo/assets/dig/LTbg.png')}
                             resizeMode={'stretch'}
                             style={[styles.imageStyles, {height: 400, width: window.width}]}>
             <Image source={require('@zatgo/assets/dig/titleImg.png')} style={{width:200,height:61,marginTop:88}}/>
              <Image source={require('@zatgo/assets/dig/titleImg2.png')} style={{marginTop:86}}/>
              <Text style={[{fontSize: window.fontScale * 17},styles.textStyle]}>
                算力是基于个人在ZatGo上活动的证明，可以直接影响挖矿的获取数量。{'\n'}
                算力越大，挖矿的速度也越快，得到的矿也越多。
              </Text>
            </ImageBackground>
            <View style={{alignItems: 'center'}}>

              <Image source={require('@zatgo/assets/dig/titleImg3.png')} style={{marginTop: 29}}/>
              <Text style={[{fontSize: window.fontScale * 17},styles.textStyle]}>
                算力是影响用户挖矿的影响因子，同一段时间内，算力越高，获取的币越多。
                算力分永久算力和临时算力两种类型，完成不同的任务，将获取不同类型的算力，永久算力长期生效，能够持续参与挖矿活动；{'\n'}
                临时算力一般数值较大， 但有固定生效期，仅在生效期内可参与挖矿活动，超过期限将会自动失效。
              </Text>
              <Text style={[{fontSize: window.fontScale * 12},styles.textStyle]}>
                举个例子：每日发放币的总数C{'\n'}
                用户每日领取到的币=Cx该用户当前生效的算力/所有用户生效算力的总和
              </Text>

              <Image source={require('@zatgo/assets/dig/titleImg4.png')} style={{marginTop: 67}}/>
              <Text style={[{fontSize: window.fontScale * 17},styles.textStyle]}>
                在ZatGo平台上进行浏览，学习，社交等所有活动，可以增加算力，
                由于ZatGo平台正在前期搭建中，未来将支持更多获取算力的方式。
              </Text>

            </View>

          </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'black',
    marginTop:window.isIphoneX?-84:-64,
    paddingBottom:window.isIphoneX?40:40,
  },
  imageStyles:{
    // justifyContent:'center',
    alignItems:"center",
  },
  textStyle:{
    color:'white',
    marginTop:15,
    marginLeft:37.5,
    marginRight:37.5
  }
});