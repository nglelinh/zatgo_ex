/**
 * Created by zhoujianxin on 2018/8/3.
 * @Desc
 */

'use strict';
import React, { Component } from 'react';
import { StyleSheet, Text, NativeModules, Image, Platform, TouchableOpacity, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { Grid } from 'antd-mobile';
import I18n from 'I18n';
import window from "../../../constants/window";
import theme from "../../../constants/theme";
import { StorageUtils,EncryptUtils } from '../../../utils';
import { constDefines } from '../../../constants';
import RuleModel from './components/RuleModel';
import ExtensionModule from '../../../components/ExtensionModule';

const GridHeader = ({ title }) => (
  <View style={styles.gridHeader}>
    <View style={styles.labeler} />
    <Text style={styles.headerTxt}>
      {title}
    </Text>
  </View>
)

const SalesView = ({ data, onItemClick, modelClick }) => (
  <View style={styles.salesContainer}>
    <GridHeader title={I18n.t('dig_Daily_tasks')}/>
    <Grid data={data}
      columnNum={3}
      hasLine={false}
      renderItem={(item, index) =>
          <View style={{flex: 1,borderWidth:0.5,borderColor:'#d4d4d4',borderRadius:5, margin:5}}>
          {item.description.length>0 ?
            <TouchableOpacity
              style={styles.ruleStyle}
              onPress={() => {
                modelClick(item, index)
              }}>
              <Image
                style={{ width: 15, height: 15, marginRight: 5 }}
                source={require('@zatgo/assets/dig/rule.png')} />
            </TouchableOpacity> : null
          }
          <TouchableOpacity
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
            onPress={() => {
              onItemClick(item, index)
            }}>
            <Image
              style={styles.icon}
              source={{ uri: item.img }} />
            <Text style={{
              fontWeight: 'bold',
              fontSize: 14 * window.fontScale,
              marginBottom: 5
            }}>{item.title}</Text>
            <Text numberOfLines={1}
                style={{
              color: '#797F85',
              fontSize: 12 * window.fontScale,
              marginBottom: 10,
              textAlign: 'center',
            }}>{item.taskAbstract}</Text>

            {item.isDone === 0 ?
              <LinearGradient
                colors={['#4DCC7B', '#12A0D2']}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={{
                  width: 80,
                  marginBottom: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: '#4DCC7B',
                }}>
                <Text style={{ margin: 5, color: 'white', fontSize: window.fontScale * 11 }}>
                  {item.isOpen=== '0'? I18n.t('dig_wait') :item.rewardDescription}
                </Text>
              </LinearGradient> :
              <View
                style={{ margin: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                <Image
                  style={{ width: 13, height: 13, marginRight: 5 }}
                  source={require('@zatgo/assets/dig/select.png')} />
                <Text>{I18n.t('dig_completed')}</Text>
              </View>
            }

          </TouchableOpacity>
        </View>
      }
      itemStyle={styles.gridItemStyle}
    />
  </View>
)

class DailyTaskView extends Component {
  state = {
    visible: false,
    taskData: [],
    description: '',
  }

  componentDidMount() { }

  render() {
    return (
      <View style={{ backgroundColor: '#ffffff' }}>
        <SalesView data={this.props.data}
          onItemClick={this.onItemClick}
          modelClick={this.modelClick}
        />
        <RuleModel visible={this.state.visible} description={this.state.description} onClose={() => { this.onClose() }} />
      </View>
    );
  }

  onClose = () => {
    this.setState({
      visible: false
    })
  }

  modelClick = (item) => {
    this.setState({
      visible: true,
      description: item.description
    })
  }

  onItemClick = (item, index) => {
    console.log(item)
    if (item.isOpen !== '0' && item.localtionUrl !== null && item.clickEventType === 0) {
      this.props.onOpen && this.props.onOpen(item.localtionUrl);
      return;
    }
    if (item.isOpen !== '0' && item.localtionUrl !== null && item.clickEventType === 1) {
      this.onOpenWeb(item);
      return;
    }
    if (item.isOpen !== '0' && item.localtionUrl !== null && item.clickEventType === 3) {
      global.showModel(item,true,true);
      return;
    }
  }

  onOpenWeb = (item) => {
    StorageUtils.get(constDefines.storeAuthInfoKey).then(ret => {
      if (ret) {
        const targetUrl = ExtensionModule.queryString(item.localtionUrl,{'webSign':EncryptUtils.webMd5(),'zatgoToken':ret.token});
        if (Platform.OS === "android") {
          const webUrl = targetUrl.indexOf("taskId") !== -1? targetUrl:ExtensionModule.queryString(targetUrl,{taskId:item.id});
          const param = Number(item.executeInterval) > 0&& item.isDone === 0 ? `${webUrl}&timer=${item.executeInterval}` : targetUrl;
          ExtensionModule.startWebViewPage(param);
        } else {
          const webUrl = targetUrl.indexOf("taskId") !== -1? targetUrl:ExtensionModule.queryString(targetUrl,{taskId:item.id});
          let params = {};
          if (Number(item.executeInterval) > 0&& item.isDone === 0) {
            params = {
              url: webUrl,
              zatgoToken: ret.token,
              timer: Number(item.executeInterval),
              taskId: item.id,
              // hiddenNavBar:'yes'
            }
          } else {
            params = {url: webUrl,}//hiddenNavBar隐藏原生VC导航栏
          }
          params['NavgationBar']= ExtensionModule.GetQueryString(webUrl,'webviewTitleInvisible');
          console.log(params);
          NativeModules.NativeManager.RNOpenOneVC(params);
        }
      } else {
        Actions.reset('login')
      }
    });
  }
}

export default DailyTaskView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor
  },
  coreFuncContainer: {
    height: 75,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0.5,
  },
  gridItemStyle: {
    height: undefined,
    justifyContent: 'center',
    backgroundColor: 'white',
    // borderBottomWidth: 0.5,
    // borderRightWidth: 0.5
  },
  actContainer: {
    marginTop: 15,
    marginBottom: 20,
    backgroundColor: 'white',
  },
  salesContainer: {
    marginTop: 15,
    backgroundColor: 'white',
  },
  icon: {
    width: 30,
    height: 30,
    marginBottom: 12,
    marginTop: 12
  },
  gridHeader: {
    height: 40,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',

    // borderBottomWidth: 0.5,
    // borderBottomColor: '#d4d4d4',
  },
  labeler: {
    width: 3,
    height: 15,
    marginLeft: 15,
    backgroundColor: '#4DCC7B',
    borderRadius: 2,
  },
  headerTxt: {
    fontSize: 17 * window.fontScale,
    color: '#323B43',
    marginLeft: 10,
    fontWeight: 'bold'
  },
  ruleStyle: {
    position: 'absolute',
    zIndex: 3,
    marginLeft: window.width / 3 - 30,
    marginTop: 5
  }
});

