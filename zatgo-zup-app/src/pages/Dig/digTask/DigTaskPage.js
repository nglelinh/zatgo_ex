/**
 * Created by zhoujianxin on 2018/8/3.
 * @Desc
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  ScrollView,
  NativeModules,
  DeviceEventEmitter,
  NativeEventEmitter,
  StatusBar
} from 'react-native';
import window from "../../../constants/window";
import BaseTaskView from './BaseTaskView';
import DailyTaskView from './DailyTaskView';
import ActiveTaskView from './ActiveTaskView';
import { connect } from 'react-redux';
import { digActions } from "../../../redux/actions";
import { Actions } from 'react-native-router-flux';
import {PullView} from '../../../components/PullToRefreshLayout'

const myNativeEvt = new NativeEventEmitter(NativeModules.JXPushManager);  //创建自定义事件接口


class DigTaskPage extends Component {

  state = {
    visible: false
  }

  componentDidMount() {
    this.props.getHashRaseTask();
    this.RefreshTask = DeviceEventEmitter.addListener('RefreshTask', (e) => {
      console.log('RefreshTask')
      this.props.getHashRaseTask();

    });
  }

  //在组件中使用
  componentWillMount() {
    this.listener = myNativeEvt.addListener('WebViewCloseEvent', (name) => {
      this.props.getHashRaseTask();
      DeviceEventEmitter.emit('HashRaseRefresh'); //发监听
    });  //对应了原生端的名字
  }

  componentWillUnmount() {
    this.listener && this.listener.remove();  //记得remove哦
    this.RefreshTask && this.RefreshTask.remove();  //记得remove哦
  }

  onOpen = (data) => {
    Actions.push(data);
  };

  render() {
    const { taskData, invitationTask } = this.props;
    StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor('#fff');
    return (
      <View style={styles.container}>
        <PullView style={{backgroundColor: 'white', flex: 1, width: window.width}}
                  onPullRelease={(resolve) => {
                    setTimeout(() => {
                      resolve();
                    }, 2000);
                    this.props.getHashRaseTask();
                  }}
        >
          <ImageBackground source={require('@zatgo/assets/dig/TaskBg.png')}
                           resizeMode='stretch'
                           style={[styles.imageStyles, {height: 200, width: window.width}]}>
            <View style={{flexDirection: 'row', alignItems: 'center', margin: 15,}}>
              <View style={{flex: 1}}>
                <Text style={{fontSize: window.fontScale * 20, color: 'white', marginLeft: 15, fontWeight: 'bold'}}>
                  算力任务
                </Text>
                <View style={{height: 0.5, backgroundColor: "white", margin: 15}}/>
                <Text style={{fontSize: window.fontScale * 12, color: 'white', marginLeft: 15}}>
                  贡献越多，算力越多，{'\n'}
                  矿种生长越快
                </Text>
              </View>
              <View style={{flex: 1}}>
                <Image source={require('@zatgo/assets/dig/TaskImage.png')}/>
              </View>
            </View>
          </ImageBackground>

          {taskData['3'] && taskData['3'].length > 0 ?
              <View style={{marginTop: 10, marginBottom: 10}}>
                <ActiveTaskView data={taskData['3']} onOpen={(item) => {
                  this.onOpen(item)
                }}/>
              </View> : null
          }

          {taskData['0'] && taskData['0'].length > 0 ?
              <View style={{marginTop: 0}}>
                <BaseTaskView data={taskData['0']} invitationTask={invitationTask} onOpen={(item) => {
                  this.onOpen(item)
                }}/>
              </View> : null
          }

          {taskData['1'] && taskData['1'].length > 0 ?
              <View style={{marginTop: 10, marginBottom: 10}}>
                <DailyTaskView data={taskData['1']} onOpen={(item) => {
                  this.onOpen(item)
                }}/>
              </View> : null
          }
        </PullView>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  getHashRaseTask: () => {
    dispatch(digActions.getHashRaseTask())
  },
  getInvitationTask: () => {
    dispatch(digActions.getInvitationTask())
  }
})

const mapStateToProps = (state) => ({
  taskData: state.dig.taskData,
  invitationTask: state.dig.invitationTaskData,
})


export default connect(mapStateToProps, mapDispatchToProps)(DigTaskPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: window.isIphoneX ? 20 : 0,
  },
  imageStyles: {
    justifyContent: 'center',
    alignItems: "center",
  },
});