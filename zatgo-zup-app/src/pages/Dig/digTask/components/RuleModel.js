/**
 * Created by zhoujianxin on 2018/8/7.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import {Modal} from 'antd-mobile';
import window from "../../../../constants/window";
import LinearGradient from 'react-native-linear-gradient';
import I18n from 'I18n';

export default class RuleModel extends Component {

  render() {
    return (
        <View style={styles.container}>
          <Modal
              transparent
              onClose={this.onClose}
              maskClosable
              visible={this.props.visible ? this.props.visible : false}
              style={{backgroundColor: 'transparent'}}
          >
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <LinearGradient
                  colors={['#4DCC7B', '#12A0D2']}
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 0}}
                  style={{
                    width: window.width - 100,
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderTopRightRadius: 10,
                    borderTopLeftRadius: 10,
                    overflow: 'hidden'
                  }}>
                <Text style={{margin: 5, color: 'white', fontSize: window.fontScale * 20}}>
                  {I18n.t('dig_rules')}
                </Text>
              </LinearGradient>
              <View style={{
                width: window.width - 100,
                backgroundColor: 'white',
                padding: 15,
                alignItems: 'center',
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10,
                justifyContent: 'center'
              }}>
                <Text>
                  {this.props.description}
                </Text>
                <TouchableOpacity style={{marginTop: 30}} onPress={this.onClose}>
                  <LinearGradient
                      colors={['#4DCC7B', '#12A0D2']}
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 0}}
                      style={{
                        width: window.width - 130,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#4DCC7B',
                        borderRadius: 15
                      }}>
                    <Text style={{margin: 10, color: 'white', fontSize: window.fontScale * 11}}>
                      {I18n.t('alert_know')}
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>
    );
  }

  onClose = () => {
    this.props.onClose && this.props.onClose();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});