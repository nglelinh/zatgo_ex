/**
 * Created by zhoujianxin on 2018/5/21.
 * @Desc 排行榜
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import JXFlatList from '../../../components/JXFlatList'
import window from '../../../constants/window';
import I18n from 'I18n';
import CustomImage from '../../../components/CustomImage/customImage';

const getRankNum = [
  {
    icon: require('@zatgo/assets/dig/1.png'),
    color:'#E5B158'
  },
  {
    icon: require('@zatgo/assets/dig/2.png'),
    color:'#B6C8D8'
  },
  {
    icon: require('@zatgo/assets/dig/3.png'),
    color:'#CEAD76'
  }
]

export default class RankingList extends Component {


  selectName = data => {
    if (data.userName) {
      if (data.nickname && data.nickname.length > 0) {
        return data.nickname;
      }
      return data.userName.length > 10 ? data.userName.substr(0, 3) + '****' + data.userName.substr(7) : data.userName;
    }
    return '';
  };

  renderItem = (item) => {
    return (
      <View style={styles.itemView}>
        <View style={{ flex: 1, alignItems: 'flex-start', marginLeft: 15 }}>
          {item.index == 0 || item.index == 1 || item.index == 2 ?
            <Image style={{ width: 24, height: 24 }}
              source={getRankNum[item.index]["icon"]} />
            : <Text style={{ marginLeft: 6 }}>{item.index + 1}</Text>
          }
        </View>

        <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{}}>{this.selectName(item.item)}</Text>
        </View>

        <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 15 }}>
          <Text style={{color:item.index == 0 || item.index == 1 || item.index == 2 ?getRankNum[item.index]["color"]:'#323B43'}}>
            {item.item.computePower}
            </Text>
        </View>
      </View>
    )
  }

  renderListHeader = () => {
    return (
      <View style={[{ flexDirection: 'row', height: 34, alignItems: 'center', backgroundColor: 'white' }]}>
        <View style={{ flex: 1, alignItems: 'flex-start', marginLeft: 15 }}>
          <Text style={styles.headerTextStyle}>{I18n.t('dig_ranking')}</Text>
        </View>

        <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={styles.headerTextStyle}>{I18n.t('dig_user')}</Text>
        </View>

        <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 15 }}>
          <Text style={styles.headerTextStyle}>{I18n.t('dig_compute_num')}</Text>
        </View>
      </View>
    )
  }

  render() {
    const { rankData } = this.props;
    return (
      <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <View style={[styles.titleViewStyle,{}]}>
            <Text style={styles.titleTextStyle}>{I18n.t('dig_ranking_list')}</Text>
          </View>
        </View>
        {this.renderListHeader()}
        <JXFlatList
          data={rankData.top}
          refreshing={false}
          renderItem={this.renderItem}
          ItemSeparatorComponent={this.Separator}
          ListFooterComponent={this.footer}
          empHeight={true}
        />
      </View>
    );
  }
  footer = () => {
    return (
      <View style={{ borderTopWidth: 0.5, borderColor: '#d4d4d4' }} />
    )
  }
  Separator = () => (
    <View style={styles.separator} />
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    minHeight: 520,
  },
  userInfoStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  line: {
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#D9D9D9',
    height: 0.5
  },
  itemView: {
    flex: 1,
    flexDirection: 'row',
    minHeight: 44,
    alignItems: 'center'
  },
  nameText: {
    color: '#323B43',
    fontSize: 18 * window.fontScale,
  },
  infoTitle: {
    color: '#797F85',
    fontSize: 15 * window.fontScale,
  },
  infoNum: {
    color: '#4DCC7B',
    fontSize: 15 * window.fontScale,
  },
  separator: {
    // height: 0.5,
    // backgroundColor: '#d4d4d4'
  },
  titleViewStyle: {
    height: 44,
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  titleTextStyle: {
    marginLeft: 15,
    marginRight: 15,
    color: '#323B43',
    fontSize: 16 * window.fontScale,
    fontWeight:'bold'
  },
  headerTextStyle: {
    color: '#8299AD',
    fontSize: 14 * window.fontScale,
  }
});