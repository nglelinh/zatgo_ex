import { NativeModules, Platform, Alert, Linking } from 'react-native';
import I18n from 'I18n';
import moment from "moment/moment";

const getTime = (data)=>{
  let time = moment(data).fromNow();
  if(time==='Just now'){
    return I18n.t('timeText')[time];
  }else {
    let arr = time.split(' ');
    // console.log(arr)
    // console.log(arr[0]+I18n.t('timeText')[arr[1]])
    switch (arr[1]) {
      case 'minutes':
        if(Number(arr[0])>30) {
          return arr[0] + I18n.t('timeText')['minutes'];
        }
        return I18n.t('timeText')['Just now'];
      case 'hour':
        return arr[0]+I18n.t('timeText')['hour'];
      case 'hours':
        return arr[0]+I18n.t('timeText')['hours'];
      case 'day':
        return arr[0]+I18n.t('timeText')['day'];
      case 'days':
        return arr[0]+I18n.t('timeText')['days'];
      case 'month':
        return arr[0]+I18n.t('timeText')['month'];
      case 'months':
        return arr[0]+I18n.t('timeText')['months'];
      case 'year':
        return arr[0]+I18n.t('timeText')['year'];
      case 'years':
        return arr[0]+I18n.t('timeText')['years'];
    }
  }
}


const getDateData = (endDate)=> {
  let diff = (Date.parse(new Date(endDate)) - Date.parse(new Date)) / 1000;

  if (diff <= 0) {
    return false;
  }

  const timeLeft = {
    years: 0,
    days: 0,
    hours: 0,
    min: 0,
    sec: 0,
    millisec: 0,
  };

  if (diff >= (365.25 * 86400)) {
    timeLeft.years = Math.floor(diff / (365.25 * 86400));
    diff -= timeLeft.years * 365.25 * 86400;
  }
  if (diff >= 86400) {
    timeLeft.days = Math.floor(diff / 86400);
    diff -= timeLeft.days * 86400;
  }
  if (diff >= 3600) {
    timeLeft.hours = Math.floor(diff / 3600);
    diff -= timeLeft.hours * 3600;
  }
  if (diff >= 60) {
    timeLeft.min = Math.floor(diff / 60);
    diff -= timeLeft.min * 60;
  }
  timeLeft.sec = diff;
  return timeLeft;
}

export default {
  getTime,
  getDateData
}