/**
 * Created by zhoujianxin on 2018/5/23.
 * @Desc
 */

import React, { Component } from 'react';
import { StyleSheet, Image, Text, TouchableOpacity,DeviceEventEmitter } from 'react-native';
import { View } from 'react-native-animatable';
import { HttpUtils } from '../../../utils'
import { api } from '../../../constants';
import window from "../../../constants/window";
import I18n from 'I18n';


export default class AnimationPage extends Component {
  state = {
    disabled: false,
    isMove: false,
  }

  componentDidMount() {
    this.setState({
      disabled: this.props.disabled ? this.props.disabled : false
    })

  }

  bounce = (item) => {
    let params = item;
    this.setState({ disabled: true });
    this.view.fadeOutUp().then(endState => {
      this.setState({ isMove: true });
    });
    HttpUtils.post(api.dig_mining, params)
      .then((response) => {
        DeviceEventEmitter.emit('refresh',); //发监听
      })
      .catch(err => {
        console.log(err.message)
      });
    this.props.callBack && this.props.callBack(item);
  }


  render() {
    if (!this.state.isMove) {
      let imageUrl = this.props.data && this.props.data.coinImage;
      return (
        <View
          ref={this.handleViewRef}
          style={{ height: 63 }}
        >
          <View animation={{
            0: { translateY: 5, },
            0.5: { translateY: 0, },
            1: { translateY: 5, }
          }}
            duration={1500}
            iterationCount='infinite'
            direction="alternate"
            easing='linear'
            style={{ backfaceVisibility: 'hidden', flex: 1 }}>
            {
              this.props.data ?
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ color: 'white', textAlign: 'center', fontSize: 10, }}>
                    +{this.props.data.coinNum}
                  </Text>
                  <TouchableOpacity onPress={() => this.bounce(this.props.data)}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                    disabled={this.state.disabled}>
                    <Image
                      resizeMode='stretch'
                      style={{ width: 36, height: 36, borderRadius: 18 }}
                      source={{ uri: imageUrl }} />
                  </TouchableOpacity>
                </View> : null
            }
            {
              this.props.producing ?
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ color: 'white', textAlign: 'center', fontSize: 10, }}>
                    {I18n.t('dig_producing')}
                  </Text>
                  <Image
                    resizeMode='stretch'
                    style={{ width: 36, height: 36, borderRadius: 18 }}
                    source={require('@zatgo/assets/dig/ZAT.png')} />
                </View> : null
            }

          </View>
        </View>
      )
    } else {
      return null;
    }
  }

  handleViewRef = ref => this.view = ref;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    fontSize: 10 * window.fontScale,
    color: '#FFF',
    marginLeft: 7,
  },
});