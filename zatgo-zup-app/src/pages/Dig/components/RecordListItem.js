import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import moment from 'moment';
import { window } from '@zatgo/constants';
import { StringUtils } from '@zatgo/utils';
import I18n from 'I18n';

const RecordListItem = ({ item }) => {
  return(
      <View>
        <View style={styles.recordContainer}>
          <Image style={{width:40,height:40,marginRight:15}} source={{uri:item.coinImage}}/>
          <View>
            <Text style={styles.actTxt}>{I18n.t('dig_mining_income')}</Text>
            <Text style={styles.dateTxt}>
              {moment(item.createDate).format('YYYY-MM-DD HH:mm')}
            </Text>
          </View>
          <View style={styles.amountContainer}>
            <Text
                style={[styles.amountTxt,{flex:1}]}
                numberOfLines={1}
                ellipsizeMode="tail">
              +{item.coinNum}
              <Text style={styles.typeTxt}>  {item.coinType}</Text>
            </Text>
          </View>
        </View>
        <View style={styles.separator}/>
      </View>
  )
}

export default RecordListItem;

const styles = StyleSheet.create({
  recordContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0,
    backgroundColor: 'white',
    paddingTop:10,
    paddingBottom:10,
    paddingLeft:15,
    paddingRight:15
  },
  actTxt: {
    color: '#323B43',
    fontSize: 15 * window.fontScale,
  },
  dateTxt: {
    color: '#797F85',
    fontSize: 11 * window.fontScale,
    marginTop: 7
  },
  amountContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flex:1,
  },
  amountTxt: {
    color: '#4DCC7B',
    fontSize: 20 * window.fontScale,
    textAlign:'right'
  },
  typeTxt: {
    color: '#140000',
    fontSize: 12 * window.fontScale,
    marginLeft: 5,
  },
  separator: {
    height: 0.5,
    backgroundColor: '#d4d4d4'
  },
});
