/**
 * Created by zhoujianxin on 2018/7/9.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator
} from 'react-native';
import JXFlatList from '../../../components/JXFlatList'
import {theme} from '@zatgo/constants';
import moment from "moment/moment";
import {connect} from "react-redux";
import I18n from 'I18n';
import {digActions} from "../../../redux/actions";
import window from '../../../constants/window';


let pageNo = 1;
let sourceArr = () => ({
  '0': I18n.t('dig_source_Login'),
  '1': I18n.t('dig_source_read'),
  '2': I18n.t('dig_source_shop'),
  '3': I18n.t('dig_source_focus'),
  '4': I18n.t('dig_source_invitation'),
})

class ComputePowerRecord extends Component {

  componentDidMount() {
    this.props.getComputePowerRecord({pageNo: pageNo});
    pageNo++;
  }

  RecordListItem = (item) => {
    let source = item.item.sourceName ? item.item.sourceName : '';
    let computeType = item.item.computeType === 0 ? I18n.t('dig_computeType_permanent') : I18n.t('dig_computeType_time');
    return (
        <View>
          <View style={styles.recordContainer}>
            {/*<Image style={{width:40,height:40,marginRight:15}} source={require('@zatgo/assets/mine/ZAT.png')}/>*/}
            <View>
              <Text style={styles.actTxt}>{source}</Text>
              <Text style={styles.dateTxt}>
                {moment(item.item.validStartDate).format('YYYY-MM-DD HH:mm')}
                {' ' + I18n.t('dig_compute_effect')}
              </Text>
              {item.item.computeType === 0 ? null :
                  <Text style={styles.dateTxt}>
                    {moment(item.item.validEndDate).format('YYYY-MM-DD HH:mm')}
                    {' ' + I18n.t('dig_compute_failure')}
                  </Text>
              }
            </View>
            <View style={styles.amountContainer}>
              <Text style={[styles.amountTxt, {flex: 1}]}
                    numberOfLines={1}
                    ellipsizeMode="tail">
                <Text style={styles.computeType}>{computeType} </Text>
                +{item.item.computeNum}
              </Text>
            </View>
          </View>
          <View style={styles.separator}/>
        </View>
    );
  }

  render() {
    return (
        <View style={styles.container}>
          <JXFlatList
              data={this.props.computePowerRecord}
              refreshing={0}
              pageSize={15}
              onRefresh={this.refreshing}
              renderItem={this.RecordListItem}
              ItemSeparatorComponent={this.Separator}
              ListFooterComponent={this.footer}
              onEndReached={this.onload}
          />
        </View>
    );
  }

  Separator = () => (
      <View style={styles.separator}/>
  )

  refreshing = () => {
    pageNo = 1;
    this.props.getComputePowerRecord({pageNo: pageNo});
    pageNo++;
  }

  onload = () => {
    if (this.props.computePowerStatus) {
      this.props.getComputePowerRecord({pageNo: pageNo});
      pageNo++;
    }
  }

  footer = () => {
    if (this.props.computePowerStatus) {
      return (
          <View style={styles.footerStyle}>
            <ActivityIndicator size="small" color="#888888"/>
          </View>
      )
    } else {
      return (
          <View style={styles.footerStyle}>
            <Text>{I18n.t('order_no_more')}</Text>
          </View>
      )
    }
  }

  componentWillUnmount() {
    pageNo = 1;
    this.props.defaultcomputePowerRecord()
  }
}

const mapDispatchToProps = (dispatch) => ({
  getComputePowerRecord: (params) => dispatch(digActions.getComputePowerRecord(params)),
  defaultcomputePowerRecord: () => {
    dispatch(digActions.getComputePowerRecordFail())
  }
})

const mapStateToProps = (state) => ({
  computePowerRecord: state.dig.computePowerRecord,
  computePowerStatus: state.dig.computePowerStatus
})

export default connect(mapStateToProps, mapDispatchToProps)(ComputePowerRecord);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginBottom: window.isIphoneX ? 20 : 0
  },
  recordContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0,
    backgroundColor: 'white',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15
  },
  actTxt: {
    color: '#323B43',
    fontSize: 15 * window.fontScale,
  },
  dateTxt: {
    color: '#797F85',
    fontSize: 11 * window.fontScale,
    marginTop: 7
  },
  amountContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flex: 1,
  },
  amountTxt: {
    color: '#4DCC7B',
    fontSize: 20 * window.fontScale,
    textAlign: 'right'
  },
  typeTxt: {
    color: '#140000',
    fontSize: 12 * window.fontScale,
    marginLeft: 5,
  },
  computeType: {
    color: 'red',
    fontSize: 15 * window.fontScale,
    marginLeft: 5,
  },
  separator: {
    height: 0.5,
    backgroundColor: '#d4d4d4'
  },
  footerStyle: {
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2
  }
});