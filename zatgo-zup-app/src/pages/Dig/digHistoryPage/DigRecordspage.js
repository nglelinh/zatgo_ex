/**
 * Created by zhoujianxin on 2018/7/9.
 * @Desc 挖矿记录
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ActivityIndicator
} from 'react-native';
import JXFlatList from '../../../components/JXFlatList'
import { theme } from '@zatgo/constants';
import RecordListItem from '../components/RecordListItem';
import { digActions } from "../../../redux/actions";
import { connect } from "react-redux";
import window from '../../../constants/window';
import I18n from 'I18n';

let pageNo = 1;

const Separator = () => (<View style={styles.separator} />)

class DigRecordspage extends Component {

  componentDidMount() {
    this.props.getUserMiningRecord({ pageNo })
    pageNo++;
  }

  componentWillUnmount() {
    pageNo = 1;
  }


  refreshing = () => {
    pageNo = 1;
    this.props.getUserMiningRecord({ pageNo: pageNo });
    pageNo++;
  }

  onload = () => {
    if (this.props.miningRecordStatuse) {
      this.props.getUserMiningRecord({ pageNo: pageNo });
      pageNo++;
    }
  }

  footer = () => {
    if (this.props.miningRecordStatuse) {
      return (
        <View style={styles.footerStyle}>
          <ActivityIndicator size="small" color="#888888" />
        </View>
      )
    } else {
      return (
        <View style={styles.footerStyle}>
          <Text>{I18n.t('order_no_more')}</Text>
        </View>
      )
    }
  }

  render() {
    StatusBar.setBarStyle('dark-content',true);
    return (
      <View style={styles.container}>
        <JXFlatList
          data={this.props.miningRecord}
          refreshing={0}
          pageSize={15}
          onRefresh={this.refreshing}
          renderItem={RecordListItem}
          ItemSeparatorComponent={Separator}
          ListFooterComponent={this.footer}
          onEndReached={this.onload}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  getUserMiningRecord: (params) => { dispatch(digActions.getMiningRecord(params)) },
  defaultMiningRecord: () => { dispatch(digActions.getMiningRecordFail()) }
})

const mapStateToProps = (state) => ({
  miningRecord: state.dig.miningRecord,
  miningRecordStatuse: state.dig.miningRecordStatuse
})


export default connect(mapStateToProps, mapDispatchToProps)(DigRecordspage)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginBottom: window.isIphoneX ? 20 : 0
  },
  separator: {
    height: 0.5, backgroundColor: '#d4d4d4'
  },
  footerStyle: {
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2
  }
});