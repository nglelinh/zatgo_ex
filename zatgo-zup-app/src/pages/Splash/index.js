import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  StatusBar,
  StyleSheet,
  BackHandler,
  DeviceEventEmitter,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen';
import I18n from 'I18n';
import { authActions, userActions } from '../../redux/actions';
import { StorageUtils, PermissionAndroidUtils, configToken } from '../../utils';
import constDefines from '@zatgo/constants/constDefines';
import { window, theme } from '@zatgo/constants';
import Carousel from 'react-native-banner-carousel';
import CustomImage from '@zatgo/components/CustomImage/customImage'

const cloudUserId = { 'cloudUserId': '65c2c3a074774f8090a1e4544a56632a' };

class SplashPage extends PureComponent {

  state = {
    times: 6
  }

  componentDidMount() {
    this.setTimeout = setTimeout(
      () => {
        SplashScreen.hide();
        if (this.props.advertList.length <= 0) { this.checkPermission(); }
      }, 1000);
    this.animate();
  }

  componentWillUnmount() {
    this.setTimeout && clearTimeout(this.setTimeout);
    this.timer && clearTimeout(this.timer);
  }

  animate = data => {
    let seconds = 6;
    this.timer = setInterval(() => {
      if (seconds <= 0) {
        this.checkPermission();
        clearInterval(this.timer);
      } else {
        seconds -= 1;
        if (seconds <= 0) {
          seconds = 0;
        }
        this.setState({ times: seconds });
      }
    }, 1000);
  };


  async checkPermission() {
    const granted = await PermissionAndroidUtils.requestMultiPermission();
    if (granted) {
      this.checkLogin();
    } else {
      Alert.alert(null, I18n.t('alert_permission'),
        [{
          text: I18n.t('alert_ok'),
          onPress: this.exitApp,
        }]
      )
    }
  }

  exitApp = () => {
    BackHandler.exitApp();
  }

  checkLogin = () => {
    StorageUtils.get(constDefines.storeAuthInfoKey).then((res) => {
      if (res) {
        this.props.loginSetAuthInfo(res);
        this.props.loginSuccess(res);
      } else {
        Actions.replace('login')
      }
    });
  }

  onItemClick = (item) => {
    console.log(item)
    if (Number(item.clickEventType) === 1 || Number(item.clickEventType) === 0) {
      StorageUtils.get(constDefines.storeAuthInfoKey).then((res) => {
        if (res) {
          this.props.loginSetAuthInfo(res);
          configToken(res.token);
          Actions.reset('tabbar')
          if (Number(item.clickEventType) === 1) {
            this.onSignLogin(null, this.queryString(item.clickEventAddress, cloudUserId))
          }
          if (Number(item.clickEventType) === 0) {
            Actions.push(item.clickEventAddress);
          }
        } else {
          Actions.replace('login')
          if (Number(item.clickEventType) === 1) {
            this.onSignLogin(null, this.queryString(item.clickEventAddress, cloudUserId))
          }
        }
      });
    }
  }


  onSignLogin = (appAuth, weburl) => {
    const { authInfo } = this.props;
    const zatgoToken = authInfo.token ? authInfo.token : ''
    const params = {
      "tmcCode": "ZATGO",
      "corpCode": "ZAT",
      "sign": "123456",
      "relationUserId": authInfo.userId,
      "contact": authInfo.userName,
    }
    this.props.signLoginFeiba({ appAuth, weburl, zatgoToken, params })

  }

  queryString = (url, params) => {
    if (params && url) {
      let paramsArray = [];
      Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
      if (url && url.search(/\?/) === -1) {
        url += '?' + paramsArray.join('&')
      } else {
        url += '&' + paramsArray.join('&')
      }
    }
    return url;
  }

  render() {
    const { advertList } = this.props;

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <StatusBar hidden={true} barStyle='dark-content' />
        {advertList.length > 0 ?
          <Carousel
            loop
            index={0}
            autoplay
            autoplayTimeout={3000}
            pageSize={window.width}>
            {advertList.map((item, index) => (
              <TouchableOpacity key={index + 'Banner'} activeOpacity={1} onPress={() => {
                this.onItemClick(item)
              }}>
                <CustomImage
                  style={{ width: window.width, height: window.isIphoneX ? window.height - 20 : window.height, }}
                  defaultStyle={{ width: window.width, height: window.isIphoneX ? window.height - 20 : window.height, }}
                  uri={item.imageUrl}
                />

              </TouchableOpacity>
            ))}
          </Carousel>
          :
          <View style={{
            width: window.width,
            height: window.height,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white'
          }}>
            <Image
              resizeMode="stretch"
              source={require('../../assets/common/launch_logo.png')}
            />
          </View>
        }
        {
          advertList.length > 0 ?
            <TouchableOpacity
              style={{ position: 'absolute', top: window.isIphoneX ? 64 : 44, right: 15, zIndex: 10, }}
              onPress={this.checkLogin}>
              <View style={styles.pushBtn}>
                <Text>{I18n.t("ad_finish")}</Text>
                <Text>{this.state.times}</Text>
              </View>
            </TouchableOpacity> : null
        }
      </View>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  loginSuccess: (params) => {
    dispatch(authActions.loginSuccess(params))
  },
  loginSetAuthInfo: (params) => {
    dispatch(authActions.loginSetAuthInfo(params))
  },
  signLoginFeiba: (sign) => {
    dispatch(authActions.signLoginFeiba(sign))
  },
  checkAuditVersion: () => {
    dispatch(userActions.checkAuditVersion())
  },
})

const mapStateToProps = (state) => ({
  isAudit: state.user.isAudit,
  advertList: state.user.advertList,
  authInfo: state.user.authInfo,
})


export default connect(mapStateToProps, mapDispatchToProps)(SplashPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  pushBtn: {
    padding: 5,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderRadius: 5,
  }
})
