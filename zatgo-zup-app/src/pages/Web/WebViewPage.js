/*
 *  @File   : WebViewPage
 *  @Author : lsl
 *  @Date   : 2018-3-5 17:26:51
 *  @Last Modified   : 2018-3-5 17:26:51
 *  @Desc webview
 */

import React, { PureComponent } from 'react';
import { View, Platform, requireNativeComponent,StatusBar } from 'react-native';
import { connect } from 'react-redux';
import window from "../../constants/window";
import WebView from '../../components/WebView';
import {EncryptUtils} from "../../utils";

// const RNCWKWebView = requireNativeComponent('RNCWKWebView', RNCWKWebView);
// const RNCWebView = requireNativeComponent('RNCWebView', RNCWebView);
const weburl = 'http://118.25.179.93:8085/app.html';
// const weburl = 'http://118.25.179.93:8085/app.html';
// const weburl = 'http://zup.zatgo.net/exchange-web/app.html';

class WebViewPage extends PureComponent {
  state = {
    loading: true,
  }

  shouldComponentUpdate(nextProps, nextState){
    console.log('shouldComponentUpdate');
    StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor('#fff');
  }

  render() {
    const { authInfo } = this.props;
    const token = authInfo.token ? authInfo.token : ''
    const url = `${global.exchangeUrl}?token=${token}&cloudUserId=65c2c3a074774f8090a1e4544a56632a&webSign=${EncryptUtils.webMd5()}`;
    console.log('页面url:', url);
    return (
      <View style={{ flex: 1 ,backgroundColor:'white',paddingTop: window.isIphoneX ? 44 : Platform.OS === 'ios' ? 20 : 20}}>
        <WebView source={{ uri: url }}
          style={{ flex: 1,  }}
          mixedContentMode={'always'}
          bounces={false} />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  authInfo: state.user.authInfo,
})

export default connect(mapStateToProps)(WebViewPage);
