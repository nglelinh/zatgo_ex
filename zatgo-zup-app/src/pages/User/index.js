import SettingPage from './Setting/SettingPage'
import CoinSelectPage from './CoinSelect/CoinSelectPage'
import DefaultAccountPage from './Default/DefaultAccountPage'
import LanguageSetPage from './Langue/LanguageSetPage'
import ServicePage from './Service/ServicePage'
import OrderQueryPage from './OrderQuery/OrderQueryPage'
import OrderQueryDetail from './OrderQuery/OrderQueryDetail'
import InviteCodePage from './InviteCode'
import OrderList from './OrderList'

export {
  SettingPage,
  CoinSelectPage,
  DefaultAccountPage,
  LanguageSetPage,
  ServicePage,
  OrderQueryPage,
  OrderQueryDetail,
  InviteCodePage,
  OrderList
}