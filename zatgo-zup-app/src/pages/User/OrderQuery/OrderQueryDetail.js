/**
 *
 * Created by zhoujianxin on 2018/5/10.
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, } from 'react-native';
import { window, theme } from '@zatgo/constants';
import moment from 'moment';
import I18n from 'I18n';

let status = () => ({
  'PAID': { 'display': I18n.t('order_list_pay_success'), 'textColor': '#4DCC7B' },
  'PAYING': { 'display': I18n.t('order_list_pay_paying'), 'textColor': '#4DCC7B' },
  'FAIL': { 'display': I18n.t('order_list_pay_fail'), 'textColor': '#A0A4A8' },
  'PAY_UNKOWN_ERROR': { 'display': I18n.t('order_list_pay_err'), 'textColor': '#A0A4A8' },
  'REFUND': { 'display': I18n.t('order_list_refund_success'), 'textColor': '#4DCC7B' },
  'REFUND_FAIL': { 'display': I18n.t('order_list_refund_fail'), 'textColor': '#A0A4A8' },
  'REFUND_UNKOWN_ERROR': { 'display': I18n.t('order_list_refund_err'), 'textColor': '#A0A4A8' }

});

var TextState=1;

export default class OrderQueryDetail extends Component {


  renderHeaderView = (orderkey, thirdPaykey, orderMoney, cashAmount) => {
    var temp = Number(orderMoney) != 0 && Number(cashAmount) != 0 ? true : false
    var orderSize = orderMoney&&String(orderMoney).length>7?36-5*(String(orderMoney).length/4):36;
    var cashSize = cashAmount&&String(cashAmount).length>7?36-5*(String(cashAmount).length/4):36;

    return (
      <View style={styles.headerViewStyle}>
        <View style={{ flexDirection: 'row', marginBottom: 15 }}>
          {
            Number(orderMoney) != 0 ?
              <Text style={styles.payTextState}>
                {status()[orderkey] && status()[orderkey].display}
              </Text> : null
          }
          {temp ? <View style={{ width: 30 }} /> : null}
          {
            Number(cashAmount) != 0 ?
              <Text style={styles.payTextState}>
                {status()[thirdPaykey] && status()[thirdPaykey].display}
              </Text> : null
          }
        </View>

        <View style={{ flexDirection: 'row' }}>
          {
            Number(orderMoney) != 0 ?
              <View style={{ flex: 1, alignItems: temp ? 'flex-end' : 'center' }}>
                <Text style={styles.payType}>{this.props.data.isMultiPay===1?this.props.data.mixCoinTypeEnum:this.props.data.coinTypeEnum}</Text>
                <Text style={[styles.moneyNumStyle,{fontSize: orderSize * window.fontScale,}]}>{orderMoney}</Text>
              </View> : null
          }

          {
            temp ?
              <View style={{ width: 23, marginLeft: 10, marginRight: 10 }}>
                <Image style={{ flex: 1, resizeMode: 'contain', }}
                  source={require('../../../assets/mine/payadd.png')} />
              </View> : null
          }

          {
            Number(cashAmount) != 0 ?
              <View style={{ flex: 1, alignItems: temp ? 'flex-start' : 'center' }}>
                <Text style={styles.payType}>CNY</Text>
                <Text style={[styles.moneyNumStyle,{fontSize: cashSize * window.fontScale,}]}>{cashAmount}</Text>
              </View> : null
          }
        </View>

        <View style={{ flexDirection: 'row', marginBottom: 15 }}>
          {
            Number(orderMoney) != 0 ?
              <Image
                style={{ width: 25, height: 25 }}
                source={require('../../../assets/mine/ZAT.png')}
              /> : null
          }
          {temp ? <View style={{ width: 30 }} /> : null}
          {
            Number(cashAmount) != 0 ?
              <Image
                style={{ width: 25, height: 25 }}
                source={require('../../../assets/mine/weixin.png')}
              /> : null
          }
        </View>

      </View>
    )
  }

  render() {
    const { data } = this.props
    var orderkey = data.orderStatusEnum
    var thirdPaykey = data.thirdPayStatusEnum
    return (
      <View style={styles.container}>

        {this.renderHeaderView(orderkey, thirdPaykey, data.orderMoney, data.cashAmount)}
        <View style={[styles.separator, { marginRight: 10, marginLeft: 10, marginTop: 15 }]} />

        <View style={styles.itemStyle}>
          <View style={styles.titlestyle}>
            <Text style={[{color:'#797F85' }]}>{I18n.t('order_detail_creattime')}</Text>
          </View>
          <View style={styles.containerStyle}>
            <Text style={[{ marginLeft: 12, textAlign: 'right' }]}>
              {moment(data.createDate).format('YYYY-MM-DD HH:mm')}
            </Text>
          </View>
        </View>

        <View style={styles.itemStyle}>
          <View style={styles.titlestyle}>
            <Text style={[{color:'#797F85' }]}>{I18n.t('order_detail_merchants')}</Text>
          </View>
          <View style={styles.containerStyle}>
            <Text style={[{ marginLeft: 12, }]} selectable={true}>{data.receiptUserName}</Text>
          </View>
        </View>

        <View style={styles.itemStyle}>
          <View style={styles.titlestyle}>
            <Text style={[{color:'#797F85' }]}>{I18n.t('order_detail_orderNum')}</Text>
          </View>
          <View style={styles.containerStyle}>
            <Text style={[{ marginLeft: 12, }]} selectable={true}>{data.orderId}</Text>
          </View>
        </View>


        <View style={styles.itemStyle}>
          <View style={styles.titlestyle}>
            <Text style={[{color:'#797F85' }]}>{I18n.t('order_detail_merchantsNum')}</Text>
          </View>
          <View style={styles.containerStyle}>
            <Text style={[{ marginLeft: 12, }]} selectable={true}>{data.orderFlowCode}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  containerStyle: {
    flex: 2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    padding: 8,

  },
  headerViewStyle: {
    backgroundColor: 'white',
    alignItems: "center",
    justifyContent: 'center',
    minHeight: 200
  },
  payTextState: {
    fontSize: 14 * window.fontScale,
    color: '#797F85',
  },
  payType: {
    fontSize: 12 * window.fontScale,
    color: '#797F85',
    marginBottom: 12
  },
  moneyNumStyle: {
    color: '#323B43',
    marginBottom: 15,
  },
  txtBlack: {
    fontSize: 12 * window.fontScale,
    color: '#797F85',
  },
  separator: {
    height: 0.8,
    backgroundColor: '#d4d4d4'
  },
  titlestyle: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 12
  },
  itemStyle: {
    backgroundColor: '#FFFFFF',
    minHeight: 36,
    flexDirection: 'row',
  }
});