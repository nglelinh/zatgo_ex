/*
 * * Created by zhoujianxin on 2018/5/9.
 */
import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, FlatList, StyleSheet, Dimensions, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import I18n from 'I18n';
import moment from 'moment';
import JXFlatList from '../../../components/JXFlatList'
import window from '../../../constants/window';
import {walletActions} from '../../../redux/actions';


var pageNo = 1;

let statusSign = () => ({
  'PAID': {'display': I18n.t('order_list_pay_success'), 'textColor': '#4DCC7B'},
  'PAYING': {'display': I18n.t('order_list_pay_paying'), 'textColor': '#4DCC7B'},
  'FAIL': {'display': I18n.t('order_list_pay_fail'), 'textColor': '#A0A4A8'},
  'PAY_UNKOWN_ERROR': {'display': I18n.t('order_list_pay_err'), 'textColor': '#A0A4A8'},
  'REFUND': {'display': I18n.t('order_list_refund_success'), 'textColor': '#4DCC7B'},
  'REFUND_FAIL': {'display': I18n.t('order_list_refund_fail'), 'textColor': '#A0A4A8'},
  'REFUND_UNKOWN_ERROR': {'display': I18n.t('order_list_refund_err'), 'textColor': '#A0A4A8'}

})

class OrderQueryPage extends Component {
  state = {
    dataSign: true,
    refreshing: 0
  }

  componentDidMount() {
    this.props.getOrderRecord({'pageNo': pageNo})
    pageNo++;
  }

  refreshing = () => {
    pageNo = 1
    this.props.getOrderRecord({'pageNo': pageNo})
    pageNo++
  }

  _onload = () => {
    if (this.props.orderStatus) {
      this.props.getOrderRecordLoad({'pageNo': pageNo})
      pageNo++
    }
  }

  render() {
    const {orderRecord} = this.props
    return (
        <View style={styles.container}>
          <JXFlatList
              ListFooterComponent={this._footer}
              onEndReached={this._onload}
              ItemSeparatorComponent={this._Separator}
              renderItem={this._renderItem}
              onRefresh={this.refreshing}
              refreshing={this.state.refreshing}
              pageSize={20}
              data={orderRecord}>
          </JXFlatList>
        </View>
    );
  }

  _renderItem = (item) => {
    var orderkey = item.item.orderStatusEnum;
    var thirdPaykey = item.item.thirdPayStatusEnum;
    var payStatus = ''
    var image = require('../../../assets/mine/merchants2.png')
    if (Number(item.item.orderMoney) != 0 && Number(item.item.cashAmount) != 0) {
      if (orderkey == 'PAID' || orderkey == 'PAYING' || orderkey == 'REFUND') {
        if ((thirdPaykey == 'PAID'||thirdPaykey == 'REFUND')&&orderkey=='PAID') {
          payStatus = statusSign()['PAID']
        } else if (thirdPaykey == 'REFUND'&&orderkey == 'REFUND') {
          payStatus = statusSign()['REFUND']
        } else {
          payStatus = statusSign()['PAYING']
        }
        image = require('../../../assets/mine/merchants.png')
      } else {
        payStatus = statusSign()[orderkey]
        image = require('../../../assets/mine/merchants2.png')
      }
    } else {
      if (Number(item.item.orderMoney) != 0) {
        if (orderkey == 'PAID' || orderkey == 'PAYING' || orderkey == 'REFUND') {
          image = require('../../../assets/mine/merchants.png')
        }
        payStatus = statusSign()[orderkey]
      } else {
        if (thirdPaykey == 'PAID' || thirdPaykey == 'PAYING' || thirdPaykey == 'REFUND') {
          image = require('../../../assets/mine/merchants.png')
        }
        payStatus = statusSign()[thirdPaykey]
      }
    }
    if(!payStatus){
        payStatus = {'display': '', 'textColor': '#A0A4A8'};

    }

    return (
        <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => { Actions.orderquerydetail({ data: item.item }) }}>
          <View
              style={{ backgroundColor: '#FFFFFF', minHeight: 60, flexDirection: "row", paddingTop: 13, paddingBottom: 13 }}>

            <View style={{ flex: 1, flexDirection: "row", alignItems: 'center' }}>
              <Image style={{ marginLeft: 10 }}
                     source={image}/>
              <View style={{ padding: 4, flex: 1 }}>
                <Text style={styles.userNameText}>{item.item.receiptUserName}</Text>
                <Text style={styles.dateText}>{moment(Number(item.item.createDate)).format('YYYY-MM-DD')}</Text>
              </View>
            </View>

            <View style={{ flex: 1, alignItems: 'flex-end', paddingLeft: 10 }}>
              <Text
                  style={[styles.userNameText, { color: payStatus.textColor, marginRight: 15 }]}>{payStatus.display}</Text>
              {Number(item.item.cashAmount) != 0 ?
                  <View style={{ flexDirection: 'row', marginRight: 15,alignItems:'center'}}>
                    <Text style={[styles.moneyStyle,]} numberOfLines={1}>¥{Number(item.item.cashAmount).toFixed(2)}</Text>
                    <Text
                        style={[styles.dateText, { marginLeft: 10 }]}>{item.item.thirdPayTypeEnum?I18n.t(item.item.thirdPayTypeEnum):''} {I18n.t('pay_button_txt')}</Text>
                  </View> : null
              }

              {Number(item.item.orderMoney) != 0 ?
                  <View style={{ flexDirection: 'row', marginRight: 15,alignItems:'center' }}>
                    <Text style={[styles.moneyStyle]} numberOfLines={1}>{item.item.orderMoney}</Text>
                    <Text
                        style={[styles.dateText, { marginLeft: 10 }]}>{item.item.isMultiPay===1?item.item.mixCoinTypeEnum:item.item.coinTypeEnum} {I18n.t('pay_button_txt')}</Text>
                  </View> : null
              }
            </View>

          </View>
        </TouchableOpacity>
    )
  }

  _footer = () => {
    if (this.props.orderStatus) {
      return (
          <View style={styles.footerStyle}>
            <ActivityIndicator size="small" color="#888888"/>
          </View>
      )
    } else {
      return (
          <View style={styles.footerStyle}>
            <Text>{I18n.t('order_no_more')}</Text>
          </View>
      )
    }
  }

  _Separator = () => (
      <View style={styles.separator}/>
  )

  componentWillUnmount() {
    pageNo = 1
  }

}

const mapDispatchToProps = (dispatch) => ({
  getOrderRecord: (params) => dispatch(walletActions.getOrderRecord(params)),
  getOrderRecordLoad:(params) => dispatch(walletActions.getOrderRecordLoad(params)),
})

const mapStateToProps = (state) => ({
  orderRecord: state.wallet.orderRecord,
  orderStatus: state.wallet.orderStatus
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderQueryPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: window.isIphoneX ? 20 : 0,
  },
  userNameText: {
    fontSize: 15 * window.fontScale,
    color: '#323B43',
  },
  moneyStyle: {
    flex: 1,
    textAlign: 'right',
    fontSize: 15 * window.fontScale,
    color: '#323B43',
  },
  dateText: {
    fontSize: 12 * window.fontScale,
    color: '#A0A4A8',
  },
  txt: {
    textAlign: 'center',
    textAlignVertical: 'center',
    color: 'white',
    fontSize: 30,
  },
  separator: {
    height: 0.5,
    backgroundColor: '#d4d4d4'
  },
  finishTxt: {
    fontSize: 20 * window.fontScale,
    color: '#ffffff',
  },
  footerStyle: {
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2
  }

})