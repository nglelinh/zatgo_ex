/*
 *  @File   : CoinSelectPage
 *  @Author : lsl
 *  @Date   : 2018-4-2 10:5:42
 *  @Last Modified   : 2018-4-2 10:5:42
 *  @Desc 选择币种-转入、转出前选择
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, Toast } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { walletActions } from '../../../redux/actions';
import SearchBar from '@zatgo/components/SearchBar';
import I18n from 'I18n';
import theme from '@zatgo/constants/theme';
import window from '@zatgo/constants/window';


const screenW = window.width;

// 一些常量设置
const cols = 3;
const cellW = (screenW - 12) / 3;
const cellH = 44;
const vMargin = (screenW - cellW * cols) / (cols + 1);
const hMargin = 20;

class CoinSelectPage extends Component {

  state = {
    coinTypes: null,
  }

  componentDidMount() {
    this.props.getCointypes()
  }

  onItemClick(item) {
    const { toRouter } = this.props;
    if (toRouter && toRouter === 1) {
      if (item.is_close_deposit) {
        Toast.info(I18n.t('toast_nosupport_deposit'));
      } else {
        Actions.deposit({ currency: item })
      }
    } else if (toRouter && toRouter === 2) {
      if (item.is_close_withdraw) {
        Toast.info(I18n.t('toast_nosupport_extract'));
      } else {
        Actions.rollout({ currency: item })
      }
    } else {
      Actions.pop({ refresh: ({ currency: item }) })
    }
  }

  onChangeText = (txt) => {
    let coinTypes = this.props.coinTypes.filter((item) => {
      return item.coinType.indexOf(txt) !== -1 || item.coinType.indexOf(txt.toUpperCase()) !== -1;
    })
    this.setState({ coinTypes })
  }

  renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => this.onItemClick(item)}
      activeOpacity={0.6}
      style={styles.itemStyle}>
      {
        item.coinImage ?
          <Image
            source={{ uri: item.coinImage }}
            style={styles.iconStyle}
          />
          : null
      }
      <Text style={{ color: '#323B43', fontSize: 14 * window.fontScale, }}>
        {item.coinType}
      </Text>
    </TouchableOpacity>
  )

  keyExtractor = (item, index) => index + '_';

  render() {
    const { coinTypes } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.searchContainer}>
          <SearchBar
            maxLength={20}
            placeholder={I18n.t('asset_search_hint')}
            onChangeText={this.onChangeText} />
        </View>
        <FlatList
          data={coinTypes ? coinTypes : this.props.coinTypes}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          numColumns={3}
          ListEmptyComponent={() => this.renderNodata()}
        />
      </View>
    );
  }
  renderNodata = () => {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
        <Text style={styles.searchText}>{I18n.t('order_not_search')}</Text>
      </View>
    )
  }
}


const mapDispatchToProps = (dispatch) => ({
  getCointypes: () => dispatch(walletActions.getCointypes()),
})

const mapStateToProps = (state) => ({
  coinTypes: state.wallet.coinTypes,
})

export default connect(mapStateToProps, mapDispatchToProps)(CoinSelectPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'white',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0.5
  },
  separator: {
    height: 1,
    backgroundColor: '#d4d4d4'
  },
  listViewStyle: {
    // flexDirection:'row',
    // alignItems:'center', // 必须设置,否则换行不起作用
  },
  itemStyle: {
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    width: cellW,
    height: cellH,
    marginLeft: vMargin,
    marginTop: hMargin,
    // 文字内容居中对齐
    alignItems: 'center'
  },
  searchText: {
    fontSize: 17,
    color: '#666666',
  },
  iconStyle: {
    height: 25,
    width: 25,
    marginRight: 10,
    backgroundColor: 'transparent',
  }
});
