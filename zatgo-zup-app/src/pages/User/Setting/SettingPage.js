import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { List, Toast } from 'antd-mobile';
import { connect } from 'react-redux'
import I18n from 'I18n';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import { authActions } from '../../../redux/actions';
import { window, theme } from '@zatgo/constants';
import { WebSocketUtils } from "../../../utils";

const Item = List.Item;

class SettingPage extends PureComponent {

  onExitConfirm = () => {
    WebSocketUtils.closeWebSocket()
    this.props.logout()
  }

  onPressLogout = () => {
    Alert.alert(I18n.t('alert_title'), null,
      [
        {
          text: I18n.t('alert_cancel')
        },
        {
          text: I18n.t('alert_ok'),
          onPress: this.onExitConfirm,
        }
      ]
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 1, backgroundColor: theme.color.bgColor, }}>
          <List style={{borderTopColor: theme.color.bgColor, borderTopWidth: 10}}>
            <Item arrow="horizontal" style={{ display: 'none' }}>
              <Text style={styles.itemTxt}> {I18n.t('set_auth')}</Text>
            </Item>
            <Item arrow="horizontal"
              onClick={() => Actions.editLoginPassword()}>
              <Text style={styles.itemTxt}> {I18n.t('set_change_login_psw')}</Text>
            </Item>
            <Item arrow="horizontal"
              onClick={() => Actions.editPayPassword()}>
              <Text style={styles.itemTxt}> {I18n.t('set_change_trade_psw')}</Text>
            </Item>
            <Item arrow="horizontal"
              onClick={() => Actions.resetTradingPassword()}>
              <Text style={styles.itemTxt}> {I18n.t('set_reset_trade_psw')}</Text>
            </Item>
            <Item arrow="horizontal"
              onClick={() => Actions.languageSet()}>
              <Text style={styles.itemTxt}> {I18n.t('set_language')}</Text>
            </Item>
          </List>
        </View>

        <TouchableOpacity
          activeOpacity={0.6}
          onPress={this.onPressLogout}>
          <LinearGradient
            colors={['#323B43', '#323B43']}
            style={styles.registerButton}
          >
            <Text style={styles.finishTxt}>
              {I18n.t('set_logout')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(authActions.logout())
})

export default connect((state) => ({}), mapDispatchToProps)(SettingPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor,
  },
  itemTxt: {
    color: '#323B43',
    fontSize: 14 * window.fontScale,
  },
  registerButton: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#12A0D2',
    paddingBottom:window.isIphoneX?20:0,

  },
  finishTxt: {
    fontSize: 20 * window.fontScale,
    color: '#ffffff'
  }
});
