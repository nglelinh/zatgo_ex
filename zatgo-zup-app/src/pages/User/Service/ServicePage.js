import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, Image, ScrollView } from 'react-native';
import { List, Toast } from 'antd-mobile';
import DeviceInfo from 'react-native-device-info';
import { window, theme } from '@zatgo/constants';
import I18n from 'I18n';

const Item = List.Item;

class ServicePage extends Component {

  openLinkingURL = (url) => {
    if (url) {
      Linking.canOpenURL(url)
        .then(supported => {
          if (supported) {
            return Linking.openURL(url);
          } else {
            return false;
          }
        }).catch(err => {
          return false;
        })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 36,
            marginTop: 50
          }}
          >
            <Image
              style={{
                width: 94,
                height: 94,
                marginBottom: 22
              }}
              source={require('../../../assets/mine/aboutUs.png')}
            />
            <Text style={styles.versionText}>
              {I18n.t('ser_app_version')} V{DeviceInfo.getVersion()}
            </Text>
          </View>
          <List style={{ marginBottom: 20 }}>
            <Item
              style={{ height: 50 }}
              extra={<Text style={styles.itemTxt}>www.zatgo.net</Text>}
              arrow="horizontal"
              onClick={() => { this.openLinkingURL('http://www.zatgo.net') }}>
              {I18n.t('ser_website')}
            </Item>
            <Item
              style={{ height: 50, }}
              extra={<Text style={styles.itemTxt}>info@zatgo.net</Text>}
              arrow="horizontal"
              onClick={() => { this.openLinkingURL('http://info@zatgo.net') }}>
              {I18n.t('ser_mail')}
            </Item>

            <Item
              style={{ height: 50 }}
              extra={<Text style={styles.itemTxt}>ZatGo承云</Text>}
              arrow="horizontal"
              onClick={() => { this.openLinkingURL('https://t.me/gogozenair') }}>
              {I18n.t('ser_telegram')}
            </Item>

            <Item
              style={{ height: 50 }}
              extra={<Text style={styles.itemTxt}>ZatGo官方群</Text>}
              arrow="horizontal"
              onClick={() => { this.openLinkingURL('https://beechat.io/join?g=7a68825cb83d44cc892b9dc64b08563c&lang=en') }}>
              {I18n.t('ser_beechat')}
            </Item>

            <Item
              extra={<Text style={styles.itemTxt}>ZatGo_official</Text>}
              arrow="horizontal"
              onClick={() => { this.openLinkingURL('https://twitter.com/ZatGo_official') }}>
              {I18n.t('ser_twittwe')}
            </Item>
          </List>
        </ScrollView>
      </View>
    );
  }
}

export default ServicePage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor
  },
  itemTxt: {
    color: '#797F85',
    fontSize: 14 * window.fontScale,
    marginRight: 12
    // fontFamily:'',
  },
  versionText: {
    color: '#323B43',
    fontSize: 14 * window.fontScale,
  }
});
