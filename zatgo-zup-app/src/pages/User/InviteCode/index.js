/**
 * Created by zhoujianxin on 2018/11/6.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  Clipboard,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import {Toast} from 'antd-mobile'
import {window} from '@zatgo/constants';
import LinearGradient from 'react-native-linear-gradient';
import I18n from 'I18n';
import ShareModel from '../../../components/ShareModel'
import {userActions, walletActions} from "../../../redux/actions";
import {connect} from "react-redux";

class index extends Component {

  state = {
    visible: false,
    loading: true,
  }


  onClose = () => {
    this.setState({
      visible: false,
    });
  }

  onOpen = () => {
    this.setState({
      visible: true,
    });
  };

  async copy(data) {
    if (data) {
      Clipboard.setString(`${data.inviteCode}`);
      let str = await Clipboard.getString();
      console.log(str);
      Toast.success(I18n.t('invite_copySuccess'))//我是文本
    }
  }

  render() {
    const {userInfo} = this.props;
    return (
        <View style={styles.container}>
          <ScrollView style={{flex: 1}}
                      showsVerticalScrollIndicator={false}
                      bounces={false}>
            <Image style={{flex: 1, width: window.width, height: window.width * 2.4}}
                             resizeMode={'stretch'}
                             onLoadEnd={() => {
                               this.setState({loading: false})
                             }}
                             source={{uri: userInfo.inviteImg}}
            />
          </ScrollView>
          <View style={[styles.btnStyle, {paddingBottom: window.isIphoneX ? 20 : 0,}]}>
                <TouchableOpacity style={{flex: 1, marginLeft: 10, marginRight: 5}} onPress={() => {
                  this.copy(userInfo)
                }}>
                  <LinearGradient
                      colors={['#4DCC7B', '#12A0D2']}
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 0}}
                      style={{
                        height: 44,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#4DCC7B',
                        borderRadius: 4
                      }}>
                    <Text style={{margin: 10, color: 'white', fontSize: window.fontScale * 14}}>
                      {I18n.t('invite_copy')}
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1, marginLeft: 5, marginRight: 10}} onPress={() => {
                  this.onOpen()
                }}>
                  <LinearGradient
                      colors={['#4DCC7B', '#12A0D2']}
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 0}}
                      style={{
                        height: 44,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#4DCC7B',
                        borderRadius: 4
                      }}>
                    <Text style={{margin: 10, color: 'white', fontSize: window.fontScale * 14}}>
                      {I18n.t('invite_share')}
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            <ShareModel
                data={userInfo.inviteImg}
                visible={this.state.visible}
                onClose={() => {
                  this.onClose();
                }}
                onOpen={(nickName) => {
                  this.onOpen()
                }}
            />
            {this.state.loading ?
                <View style={styles.loadingStyle}>
                  <ActivityIndicator size="large" color="#709dd6"/>
                </View>
                : null}
        </View>
    );
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.user.mineInfo,
})

export default connect(mapStateToProps)(index)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnStyle: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    zIndex:11
  },
  loadingStyle: {
    position: 'absolute',
    left: 0,
    top: window.height / 2 - 70,
    right: 0,
    flex: 1,
  }
});