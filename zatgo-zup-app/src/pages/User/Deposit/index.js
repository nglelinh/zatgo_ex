import DepositPage from './DepositPage'
import DepositHistoryPage from './DepositHistoryPage'
import DepositHistoryDetailPage from './DepositHistoryDetailPage'

export {
  DepositPage,
  DepositHistoryPage,
  DepositHistoryDetailPage
}
