/*
 *  @File   : DepositHistoryDetailPage
 *  @Author : lsl
 *  @Date   : 2018-4-1 9:50:49
 *  @Last Modified   : 2018-4-1 9:50:49
 *  @Desc 转入历史详情
 */
import React, { Component } from 'react';
import {
  View,
  Text,
  Clipboard,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import { Toast } from 'antd-mobile';
import I18n from 'I18n';
import { window, theme } from '@zatgo/constants';

const getStatus = () => [
  I18n.t('deposit_going'),
  I18n.t('deposit_success'),
  I18n.t('deposit_fail')
]

const Item = (props) => (
  <View style={{ backgroundColor: 'white' }}>
    <View style={styles.itemContainer}>
      <Text
        style={[{ width: window.width - 80 }, styles.txtBlack]}
        numberOfLines={1}
      >
        {props.children}
      </Text>
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={props.onPressCopy}
      >
        <Text style={styles.txtGreen}>
          {props.extra}
        </Text>
      </TouchableOpacity>
    </View>
  </View>
)


class DepositHistoryDetailPage extends Component {

  onCopyTxHash = () => {
    const { detail } = this.props
    Clipboard.setString(detail.txHash)
    Toast.info(I18n.t('toast_copy_clipboard'), 1)
  }

  onCopyFromAddress = () => {
    const { detail } = this.props
    Clipboard.setString(detail.fromAddresss)
    Toast.info(I18n.t('toast_copy_clipboard'), 1)
  }

  onCopyToAddress = () => {
    const { detail } = this.props
    Clipboard.setString(detail.toAddress)
    Toast.info(I18n.t('toast_copy_clipboard'), 1)
  }

  render() {
    const { detail } = this.props
    return (
      <View style={styles.container}>
        <Item extra={I18n.t('deposit_copy')} onPressCopy={this.onCopyTxHash}>
          {`TxHash: ${detail.txHash ? detail.txHash : ''}`}
        </Item>
        <Item>
          <Text style={styles.txtBlack}>
            TxReceipt Status：
            <Text style={styles.txtGreen}>
              {getStatus()[Number(detail.depositStatus)]}
            </Text>
          </Text>
        </Item>
        <Item>
          {`${I18n.t('extract_block_hash')}: ${detail.blockHash ? detail.blockHash : ''}`}
        </Item>
        <Item>
          {`${I18n.t('extract_timestamp')}: ${moment(Number(detail.depositDate)).format('YYYY-MM-DD')}`}
        </Item>
        <Item extra={I18n.t('deposit_copy')} onPressCopy={this.onCopyFromAddress}>
          {`${I18n.t('deposit_from')}: ${detail.fromAddresss ? detail.fromAddresss : ''}`}
        </Item>
        <Item extra={I18n.t('deposit_copy')} onPressCopy={this.onCopyToAddress}>
          {`${I18n.t('deposit_to')}: ${detail.toAddress ? detail.toAddress : ''}`}
        </Item>
      </View>
    );
  }
}

export default DepositHistoryDetailPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor,
  },
  itemContainer: {
    height: 48,
    marginLeft: 10,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0.5,
    justifyContent: 'space-between',
  },
  txtBlack: {
    fontSize: 12 * window.fontScale,
    color: '#797F85',
  },
  txtGreen: {
    fontSize: 12 * window.fontScale,
    color: '#4DCC7B',
    marginRight: 10,
    marginLeft: 25
  }
});
