/*
 *  @File   : DepositPage
 *  @Author : lsl
 *  @Date   : 2018-4-1 15:4:47
 *  @Last Modified   : 2018-4-1 15:4:47
 *  @Desc 转入
 */
import React, { PureComponent } from 'react';
import QRCode from 'react-native-qrcode';
import LinearGradient from 'react-native-linear-gradient';
import {
  Alert,
  View,
  Text,
  Image,
  TextInput,
  Clipboard,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Toast, Modal } from 'antd-mobile';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { walletActions } from '../../../redux/actions';
import I18n from 'I18n';
import { window, theme, api } from '@zatgo/constants';

const ZatGoLogo = require('@zatgo/assets/tabbar/Home.png');

class DepositPage extends PureComponent {

  componentDidMount() {
    const { authInfo, currency } = this.props;
    const params = {
      "coinType": currency.coinType ? currency.coinType : "",
      "networkType": currency.coinNetworkType ? currency.coinNetworkType : "",
      "userId": authInfo.userId
    }
    this.props.getDepositAddress(params);
    // this.onWarnAlert();
  }

  onCopyAddress = () => {
    const { depositAddress } = this.props;
    Clipboard.setString(depositAddress)
    Toast.info(I18n.t('toast_copy_clipboard'), 1)
  }

  // 测试环境充值提示
  onWarnAlert = () => (
    global.serverUrl !== api.productionUrl &&
    Alert.alert(null, I18n.t('alert_transfer'),
      [{
        text: I18n.t('alert_ok'),
      }]
    )
  )

  render() {
    const { depositAddress, currency } = this.props;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.cardContainer}>
          <Image
            style={styles.logo}
            source={currency.coinImage ? { uri: currency.coinImage } : ZatGoLogo} />
          <Text style={styles.currencyTxt}>{currency.coinType}</Text>

          <View style={{marginTop: 20}}>
            {depositAddress ? <QRCode value={depositAddress ? depositAddress : ''} size={200}/> :
                <View style={{
                  width: 200,
                  height: 200,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 0.5,
                  borderColor: '#bec4c4'
                }}>
                  <Text>{I18n.t('colection_error')}</Text>
                </View>}
          </View>

          <Text style={styles.promptTxt}>
            {I18n.t('deposit_current_addr')}
          </Text>

          <View style={{ flexDirection: 'row' }}>
            <View style={styles.addressLayout}>
              <Text
                numberOfLines={1}
                style={styles.addressTxt}>
                {depositAddress ? depositAddress : ''}
              </Text>
            </View>
          </View>
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={this.onCopyAddress}
          >
            <LinearGradient
              colors={['#4DCC7B', '#12A0D2']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.copyButton}
            >
              <Text style={styles.copyTxt}>
                {I18n.t('deposit_copy_button')}
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  getDepositAddress: (params) => dispatch(walletActions.getDepositAddress(params)),
})

const mapStateToProps = (state) => ({
  authInfo: state.user.authInfo,
  depositAddress: state.wallet.depositAddress,
})

export default connect(mapStateToProps, mapDispatchToProps)(DepositPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor
  },
  cardContainer: {
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    marginBottom: 60,
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
  logo: {
    width: 40,
    height: 40,
    marginTop: 20
  },
  currencyTxt: {
    fontSize: 18 * window.fontScale,
    color: '#323B43',
    marginTop: 10
  },
  copyButton: {
    width: 170,
    height: 50,
    marginTop: 25,
    marginBottom: 25,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  copyTxt: {
    color: 'white',
    fontSize: 14 * window.fontScale
  },
  promptTxt: {
    marginTop: 30,
    marginLeft: 10,
    marginRight: 10,
    textAlign: 'center',
    fontSize: 14 * window.fontScale,
    color: '#797F85'
  },
  addressLayout: {
    flex: 1,
    height: 35,
    marginTop: 8,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0.5,
    borderColor: '#d4d4d4',
  },
  addressTxt: {
    color: '#323B43',
    fontSize: 16 * window.fontScale,
    padding: 0,
  }
})
