/*
 *  @File   : DepositHistoryPage
 *  @Author : lsl
 *  @Date   : 2018-4-3 9:38:26
 *  @Last Modified   : 2018-4-3 9:38:26
 *  @Desc 转入历史
 */
import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { walletActions } from '../../../redux/actions';
import I18n from 'I18n';
import { window } from '@zatgo/constants';
import JXFlatList from '../../../components/JXFlatList';
import { StringUtils } from '../../../utils';

const getStatus = () => [
  I18n.t('deposit_going'),
  I18n.t('deposit_success'),
  I18n.t('deposit_fail')
]

const Separator = () => (
  <View style={styles.separator} />
)

class DepositHistoryPage extends Component {

  componentDidMount() {
    this.props.getDepositRecord()
  }

  onItemPress(item) {
    Actions.depositHisDetail({ detail: item });
  }

  renderItem = ({ item }) => (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={() => this.onItemPress(item)}
      style={styles.container}>
      <Text style={styles.hashTxt}>
        {`TxHash: ${item.txHash ? item.txHash : ''}`}
      </Text>

      <View style={styles.bottomContainer}>
        <View style={styles.midContainer}>
          <Text style={styles.stateTxt}>
            {getStatus()[Number(item.depositStatus)]}
          </Text>
          <Text style={styles.amountTxt} ellipsizeMode="tail" numberOfLines={1}>
            {`+${StringUtils.formatE(item.depositNumber)} ${item.coinType}`}
          </Text>
        </View>
        <Text style={styles.feeTxt}>
          {moment(Number(item.depositDate)).format('MM-DD')}
        </Text>
      </View>
    </TouchableOpacity>
  )

  render() {
    return (
      <JXFlatList
        data={this.props.depositRecord}
        refreshing={false}
        onRefresh={this._refreshing}
        renderItem={this.renderItem}
        ItemSeparatorComponent={Separator} />
    );
  }
  _refreshing = () => {
    this.props.getDepositRecord()
  }

}

const mapDispatchToProps = (dispatch) => ({
  getDepositRecord: () => dispatch(walletActions.getDepositRecord()),
})

const mapStateToProps = (state) => ({
  depositRecord: state.wallet.depositRecord,
})

export default connect(mapStateToProps, mapDispatchToProps)(DepositHistoryPage);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    padding: 15
  },
  hashTxt: {
    color: '#797F85',
    fontSize: 12 * window.fontScale
  },
  midContainer: {
    flexDirection: 'row',

  },
  stateTxt: {
    fontSize: 12 * window.fontScale,
    color: '#4DCC7B',
    marginRight: 25
  },
  amountTxt: {
    width: 150,
    fontSize: 12 * window.fontScale,
    color: '#323B43'
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  feeTxt: {
    color: '#797F85',
    fontSize: 12 * window.fontScale
  },
  separator: {
    height: 0.5, backgroundColor: '#d4d4d4'
  }
})
