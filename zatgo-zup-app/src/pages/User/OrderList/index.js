/**
 * Created by zhoujianxin on 2019/1/2.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import {Tabs} from 'antd-mobile'
import I18n from 'I18n';
import DepositHistoryPage from '../Deposit/DepositHistoryPage'
import ExtractHistoryPage from '../Extract/ExtractHistoryPage'

export default class index extends Component {
  render() {
    const tabs = [
      { title: I18n.t('title_deposit_history') },
      { title: I18n.t('title_extract_history') },
    ];

    return (
        <View style={styles.container}>
          <Tabs tabs={tabs}
                tabBarActiveTextColor={'#35BAA0'}
                tabBarInactiveTextColor={'#797F85'}
                tabBarUnderlineStyle={{backgroundColor:'#35BAA0'}}
          >
            <View style={styles.container}>
              <DepositHistoryPage/>
            </View>
            <View style={styles.container}>
              <ExtractHistoryPage/>
            </View>
          </Tabs>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});