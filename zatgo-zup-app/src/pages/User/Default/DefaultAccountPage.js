import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import I18n from 'I18n';
import DefaultItemView from './DefaultItemView';
import { walletActions } from '../../../redux/actions';
import { theme, window } from '@zatgo/constants';
import { StringUtils } from '@zatgo/utils';
import SearchBar from '@zatgo/components/SearchBar';


// 一些常量设置
const cols = 3;
const cellW = (window.width-12)/3;
const cellH = 44;
const vMargin = (window.width - cellW * cols) / (cols + 1);
const hMargin = 20;

class DefaultAccountPage extends Component {

  state = {
    accounts: null,
  }

  componentDidMount() {
    this.props.getAccounts();
  }

  // 设置主账户
  onItemPress(item) {
    this.props.setPrimaryAccount({ accountId: item.accountId });
  }

  onChangeText = (txt) => {
    let accounts = this.props.accounts.filter((item) => {
      return item.coinType.indexOf(txt) !== -1 || item.coinType.indexOf(txt.toUpperCase()) !== -1;
    })
    this.setState({ accounts })
  }

  _renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      <DefaultItemView
        title={item.coinType}
        thumb={item.coinImage}
        extra={StringUtils.formatE(item.balance)}
        onItemPress={() => this.onItemPress(item)} />
    </View>
  )

  renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => this.onItemPress(item)}
      activeOpacity={0.6}
      style={styles.itemStyle}>
      {
        item.coinImage ?
          <Image
            style={styles.thumb}
            source={{ uri: item.coinImage }}
          /> : null
      }
      <Text style={{ color: '#323B43', fontSize: 14 * window.fontScale, }}>
        {item.coinType}
      </Text>
    </TouchableOpacity>
  )

  keyExtractor = (item, index) => index + '_';

  render() {
    const { accounts } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.searchContainer}>
          <SearchBar
            maxLength={20}
            placeholder={I18n.t('asset_search_hint')}
            onChangeText={this.onChangeText} />
        </View>
        <FlatList
          data={accounts ? accounts : this.props.accounts}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          numColumns={3}
          ListEmptyComponent={() => this.renderNodata()}
        />
      </View>
    );
  }
  
  renderNodata = () => {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
        <Text style={styles.searchText}>{I18n.t('order_not_search')}</Text>
      </View>
    )
  }
}


const mapDispatchToProps = (dispatch) => ({
  getAccounts: () => dispatch(walletActions.getAccounts()),
  setPrimaryAccount: (accountId) => {
    dispatch(walletActions.setPrimaryAccount(accountId))
  }
})

const mapStateToProps = (state) => ({
  accounts: state.wallet.accounts,
})

export default connect(mapStateToProps, mapDispatchToProps)(DefaultAccountPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'white',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0.5
  },
  separator: {
    height: 1,
    backgroundColor: '#d4d4d4'
  },
  itemContainer: {
    width: window.width - 20,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  itemStyle: {
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    width: cellW,
    height: cellH,
    marginLeft: vMargin,
    marginTop: hMargin,
    // 文字内容居中对齐
    alignItems: 'center'
  },
  thumb: {
    width: 25,
    height: 25,
    marginRight: 10,
    backgroundColor: 'transparent'
  },
  searchText: {
    fontSize: 17,
    color: '#666666',
  },
})
