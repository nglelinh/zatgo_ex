/*
 *  @File   : DefaultItemView
 *  @Author : lsl
 *  @Date   : 2018-4-3 17:16:31
 *  @Last Modified   : 2018-4-3 17:16:31
 *  @Desc 钱包子项
 */
import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { window } from '@zatgo/constants';

const DefaultItemView = (props) => (
  <TouchableOpacity
    onPress={props.onItemPress}
    activeOpacity={0.6}
    style={styles.container}
  >
    <View style={styles.rowLayout}>
      <Image
        style={styles.thumb}
        source={{ uri: props.thumb ? props.thumb : '' }} />
      <Text style={styles.txtStyle}>{props.title}</Text>
    </View>
    <View style={styles.extraContainer}>
      <Text
        style={styles.txtStyle}
        numberOfLines={1}
        ellipsizeMode="tail"
      >
        {props.extra}
        </Text>
    </View>
  </TouchableOpacity>
)

export default DefaultItemView;

const styles = StyleSheet.create({
  container: {
    height: 70,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  thumb: {
    width: 25,
    height: 25,
    marginRight: 10,
    backgroundColor: 'transparent',
  },
  txtStyle: {
    color: '#323B43',
    fontSize: 18 * window.fontScale
  },
  rowLayout: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  extraContainer: {
    flex: 1,
    marginLeft: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});