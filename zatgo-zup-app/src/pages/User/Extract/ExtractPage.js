/*
 *  @File   : ExtractPage
 *  @Author : lsl
 *  @Date   : 2018-4-3 11:7:54
 *  @Last Modified   : 2018-4-3 11:7:54
 *  @Desc 提币页面
 */
import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { Toast } from 'antd-mobile';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import I18n from 'I18n';
import ListItem from './ListItem';
import { theme, window } from '@zatgo/constants';
import NumberInput from '@zatgo/components/NumberInput';
import { walletActions } from '../../../redux/actions';
import { StringUtils } from '@zatgo/utils';

class ExtractPage extends PureComponent {
  state = {
    amount: null,
    passWord: null,
    toAddress: '',
    type: null,
  }

  componentDidMount() {
    const { currency } = this.props;
    this.props.getAccountByCoinType(currency.coinType);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.qrData) {
      this.state.toAddress = nextProps.qrData;
      this.forceUpdate();
    }
  }

  onChangeAddress = (value) => {
    let toAddress = value.replace(/\s+/g, "");
    this.setState({ toAddress });
  }

  onChangeAmount = (value) => {
    let amount = value.replace(/\s+/g, "");
    this.setState({ amount });
  }

  onChangePassword = (value) => {
    let passWord = value.replace(/\s+/g, "");
    this.setState({ passWord });
  }


  onConfirmTransfer = () => {
    const { authInfo, currency } = this.props;
    const { amount, passWord, toAddress } = this.state;
    if (parseFloat(amount) < currency.lowestExtractNumber) {
      Toast.info(I18n.t('toast_rollout_min'))
      return;
    }
    if (amount && passWord && toAddress) {
      const params = {
        "amount": amount,
        "passWord": passWord,
        "toAddress": toAddress,
        "type": currency.coinType,
      }
      this.props.walletExtract(params)
    } else {
      Toast.info(I18n.t('toast_extract_incomplete_info'))
    }
  }

  render() {
    const { currentAccount, currency } = this.props;
    return (
      <View style={styles.container}>
        <ListItem
          title={I18n.t('rollout_assets')}
          extra={
            <Text
              style={styles.assetsTxt}
              numberOfLines={1}
              ellipsizeMode="tail">
              {currentAccount.balance ? StringUtils.formatE(currentAccount.balance) : 0}
              {` ${currentAccount.coinType ? currentAccount.coinType : currency.coinType}`}
            </Text>
          }
        />
        <ListItem
          style={{ marginTop: 20 }}
          title={I18n.t('rollout_address')}
          extra={
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => Actions.otherRichScan({ qrFrom: 3 })}
            >
              <Image
                style={{ width: 25, height: 25 }}
                source={require('@zatgo/assets/common/com_scan.png')}
              />
            </TouchableOpacity>
          }
          content={
            <TextInput
              placeholderTextColor="#A0A4A8"
              placeholder={I18n.t('rollout_address_hint')}
              underlineColorAndroid="transparent"
              style={styles.txtInput}
              defaultValue={this.state.toAddress}
              onChangeText={this.onChangeAddress} />
          }
        />
        <ListItem
          title={I18n.t('rollout_assets')}
          content={
            <NumberInput
              editable
              placeholderTextColor="#A0A4A8"
              placeholder={`${I18n.t('rollout_assets_least')} ${currency.lowestExtractNumber}${currency.coinType}`}
              onChangeText={this.onChangeAmount}
            />
          }
        />
        <ListItem
          title={I18n.t('rollout_pin')}
          content={
            <TextInput
              maxLength={6}
              keyboardType="numeric"
              secureTextEntry={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#A0A4A8"
              placeholder={I18n.t('rollout_pin_hint')}
              style={styles.txtInput}
              onChangeText={this.onChangePassword} />
          }
        />

        <TouchableOpacity
          activeOpacity={0.6}
          disabled={false}
          onPress={this.onConfirmTransfer}>
          <LinearGradient
            colors={['#4DCC7B', '#12A0D2']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.confirmButton}
          >
            <Text style={styles.confirmTxt}>
              {I18n.t('rollout_confirm_button')}
            </Text>
          </LinearGradient>
        </TouchableOpacity>

        <View style={styles.instrLayout}>
          <View style={styles.instrContainer}>
            <Text style={styles.instrTxt}>
              {I18n.t('rollout_instructions')}
            </Text>
            <Text style={styles.instrContentTxt}>
              {I18n.t('page_content_extract')}
            </Text>
            <Text style={styles.instrContentTxt}>
              {
                `${I18n.t('rollout_service_fee')}${currency.sysTradingFee}${currency.sysTradingFeeType === 1 ? currency.coinType : "%"}`
              }
            </Text>
          </View>
        </View>
      </View>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  walletExtract: (params) => dispatch(walletActions.walletExtract(params)),
  getAccountByCoinType: (coinType) => dispatch(walletActions.getAccountByCoinType(coinType)),
})


const mapStateToProps = (state, ownProps) => ({
  authInfo: state.user.authInfo,
  currentAccount: state.wallet.currentPayAccount ? state.wallet.currentPayAccount : {},
})

export default connect(mapStateToProps, mapDispatchToProps)(ExtractPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.color.bgColor
  },
  txtInput: {
    flex: 1,
    padding: 0,
    color: '#323B43',
    fontSize: 14 * window.fontScale
  },
  assetsTxt: {
    color: '#323B43',
    fontSize: 14 * window.fontScale,
    width: window.width - 100,
    textAlign: 'right'
  },
  confirmButton: {
    width: window.width-40,
    height: 44,
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    marginLeft: 20,
    marginRight: 20,
  },
  confirmTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  },
  instrContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10
  },
  instrLayout: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  instrTxt: {
    color: '#4DCC7B',
    fontSize: 14 * window.fontScale,
  },
  instrContentTxt: {
    fontSize: 12 * window.fontScale,
    color: '#797F85'
  }
})
