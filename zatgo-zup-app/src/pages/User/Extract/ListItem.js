import React from 'react';
import { View, Text, StyleSheet, } from 'react-native';
import window from '@zatgo/constants/window';

const ListItem = (props) => (
  <View style={[styles.layout, { ...props.style }]}>
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.titleTxt}>{props.title}</Text>
        {props.content}
      </View>
      <View style={styles.extraContainer}>
        {props.extra}
      </View>
    </View>
  </View>
)

export default ListItem;

const styles = StyleSheet.create({
  layout: {
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  container: {
    flex: 1,
    height: 50,
    marginLeft: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderBottomColor: '#d4d4d4'
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  extraContainer: {
    marginRight: 10,
    marginLeft: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  titleTxt: {
    marginRight: 10,
    width: 60,
    color: '#323B43',
    fontSize: 14 * window.fontScale
  }
})