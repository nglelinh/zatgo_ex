/*
 *  @File   : ExtractHistoryPage
 *  @Author : lsl
 *  @Date   : 2018-4-3 9:38:26
 *  @Last Modified   : 2018-4-3 9:38:26
 *  @Desc 提币历史
 */
import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { walletActions } from '../../../redux/actions';
import I18n from 'I18n';
import window from '@zatgo/constants/window';
import { StringUtils } from '../../../utils';
import JXFlatList from '../../../components/JXFlatList'

const getStatus = () => [
  I18n.t('extract_going'),
  I18n.t('extract_success'),
  I18n.t('extract_proceed_audit'),
  I18n.t('extract_pass_audit'),
  I18n.t('extract_abnormal'),
  I18n.t('extract_failure_audit'),
]

const Separator = () => (
  <View style={styles.separator} />
)

class ExtractHistoryPage extends Component {
  componentDidMount() {
    this.props.getExtractRecord()
  }

  onItemPress(item) {
    Actions.extractHisDetail({ detail: item });
  }

  renderItem = ({ item }) => (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={() => this.onItemPress(item)}
      style={styles.container}>
      <Text style={styles.hashTxt}>
        {`TxHash: ${item.txHash ? item.txHash : ''}`}
      </Text>

      <View style={styles.bottomContainer}>
        <View style={styles.midContainer}>
          <Text style={styles.stateTxt}>
            {getStatus()[Number(item.extractStatus)]}
          </Text>
          <Text style={styles.amountTxt} ellipsizeMode="tail" numberOfLines={1}>
            {`-${StringUtils.formatE(item.extractNumber)} ${item.coinType}`}
          </Text>
        </View>
        <Text style={styles.feeTxt}>
          {moment(Number(item.extractDate)).format('MM-DD')}
        </Text>
      </View>
    </TouchableOpacity>
  )

  onRefreshing = () => {
    this.props.getExtractRecord()
  }

  _footer = () => {
    return (
        <View style={styles.footerStyle}>
          <Text>{I18n.t('order_no_more')}</Text>
        </View>
    )
  }

  render() {
    return (
      <JXFlatList
        ListFooterComponent={this._footer}
        data={this.props.extractRecord}
        refreshing={false}
        onRefresh={this.onRefreshing}
        renderItem={this.renderItem}
        ItemSeparatorComponent={Separator}
      />
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  getExtractRecord: () => dispatch(walletActions.getExtractRecord()),
})

const mapStateToProps = (state) => ({
  extractRecord: state.wallet.extractRecord,
})

export default connect(mapStateToProps, mapDispatchToProps)(ExtractHistoryPage);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    padding: 15
  },
  hashTxt: {
    color: '#797F85',
    fontSize: 12 * window.fontScale,
  },
  midContainer: {
    flex:1,
    flexDirection: 'row',
  },
  stateTxt: {
    flex:1,
    fontSize: 12 * window.fontScale,
    color: '#4DCC7B',
    // marginRight: 25
  },
  amountTxt: {
    flex:2,
    fontSize: 12 * window.fontScale,
    color: '#323B43'
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  feeTxt: {
    color: '#797F85',
    fontSize: 12 * window.fontScale,
  },
  separator: {
    height: 0.5, backgroundColor: '#d4d4d4'
  },
  footerStyle: {
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2
  }
})
