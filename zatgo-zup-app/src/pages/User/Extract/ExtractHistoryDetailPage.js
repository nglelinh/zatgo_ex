/*
 *  @File   : ExtractHistoryDetailPage
 *  @Author : lsl
 *  @Date   : 2018-6-3 16:50:56
 *  @Last Modified   : 2018-6-3 16:50:56
 *  @Desc 提币详情
 */
import React, { Component } from 'react';
import {
  View,
  Text,
  Clipboard,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Toast } from 'antd-mobile';
import moment from 'moment';
import I18n from 'I18n';
import { window, theme } from '@zatgo/constants';

const getStatus = () => [
  I18n.t('extract_going'),
  I18n.t('extract_success'),
  I18n.t('extract_proceed_audit'),
  I18n.t('extract_pass_audit'),
  I18n.t('extract_abnormal'),
  I18n.t('extract_failure_audit'),
]

const Item = (props) => (
  <View style={{ backgroundColor: 'white' }}>
    <View style={styles.itemContainer}>
      <Text style={[{ width: window.width - 80 }, styles.txtBlack]} numberOfLines={1}>
        {props.children}
      </Text>
      <TouchableOpacity activeOpacity={0.6} onPress={props.onPressCopy}>
        <Text style={styles.txtGreen}>
          {props.extra}
        </Text>
      </TouchableOpacity>
    </View>
  </View>
)

class ExtractHistoryDetailPage extends Component {

  onCopyTxHash = () => {
    const { detail } = this.props;
    Clipboard.setString(detail.txHash);
    Toast.info(I18n.t('toast_copy_clipboard'), 1)
  }

  onCopyAddress = () => {
    const { detail } = this.props;
    Clipboard.setString(detail.extractAddress);
    Toast.info(I18n.t('toast_copy_clipboard'), 1)
  }

  render() {
    const { detail } = this.props
    console.log(detail.approveResult)
    return (
      <View style={styles.container}>
        <Item extra={I18n.t('extract_copy')} onPressCopy={this.onCopyTxHash}>
          {`TxHash: ${detail.txHash && Number(detail.extractStatus) === 1 ? detail.txHash : ''}`}
        </Item>
        <Item>
          <Text style={styles.txtBlack}>
            {`${I18n.t('extract_receipt_status')}：`}
            <Text style={styles.txtGreen}>
              {getStatus()[Number(detail.extractStatus)]}
            </Text>
          </Text>
        </Item>
        <Item >
          {`${I18n.t('extract_approveResult')}: ${detail.approveResult ? detail.approveResult : ''}`}
        </Item>
        <Item>
          {`${I18n.t('extract_block_hash')}: ${detail.blockHash && Number(detail.extractStatus) === 1 ? detail.blockHash : ''}`}
        </Item>
        <Item>
          {`${I18n.t('extract_timestamp')}: ${moment(Number(detail.extractDate)).format('YYYY-MM-DD')}`}
        </Item>
        <Item >
          {`${I18n.t('extract_poundage')}: ${detail.charge ? detail.charge : '0'} ${detail.coinType}`}
        </Item>
        <Item extra={I18n.t('extract_copy')} onPressCopy={this.onCopyAddress}>
          {`${I18n.t('extract_address')}: ${detail.extractAddress ? detail.extractAddress : ''}`}
        </Item>
      </View>
    );
  }
}

export default ExtractHistoryDetailPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor,
  },
  itemContainer: {
    flexDirection: 'row',
    height: 48,
    backgroundColor: 'white',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0.5,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 10
  },
  txtBlack: {
    fontSize: 12 * window.fontScale,
    color: '#797F85',
  },
  txtGreen: {
    fontSize: 12 * window.fontScale,
    color: '#4DCC7B',
    marginRight: 10,
    marginLeft: 25
  }
});
