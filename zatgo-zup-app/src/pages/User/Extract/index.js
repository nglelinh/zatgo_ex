import ExtractPage from './ExtractPage'
import ExtractHistoryPage from './ExtractHistoryPage'
import ExtractHistoryDetailPage from './ExtractHistoryDetailPage'
export  {
  ExtractPage,
  ExtractHistoryPage,
  ExtractHistoryDetailPage,
}