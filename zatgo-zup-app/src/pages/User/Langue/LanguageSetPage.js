import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  BackHandler
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { List, Toast } from 'antd-mobile';
import { connect } from 'react-redux';
import I18n from 'I18n';
import theme from '@zatgo/constants/theme';
import { StorageUtils } from "../../../utils";
import { globalActions } from '../../../redux/actions';
import constDefines from '../../../constants/constDefines';

const Item = List.Item;

class LanguageSetPage extends Component {

  onChangeZh = () => {
    this.props.setLanguage('zh')
    StorageUtils.set(constDefines.defaultLanguageKey, 'zh')
    Actions.reset('tabbar');
    // BackHandler.exitApp();
  }

  onChangeEn = () => {
    this.props.setLanguage('en')
    StorageUtils.set(constDefines.defaultLanguageKey, 'en')
    Actions.reset('tabbar');
    // BackHandler.exitApp();
  }

  render() {
    return (
      <View style={styles.container}>
        <List>
          <Item
            arrow="horizontal"
            onClick={this.onChangeZh}>
            <Text style={styles.itemTxt}>{I18n.t('language_zh')}</Text>
          </Item>
          <Item
            arrow="horizontal"
            onClick={this.onChangeEn} >
            <Text style={styles.itemTxt}>{I18n.t('language_en')}</Text>
          </Item>
        </List>
      </View>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  setLanguage: (language) => {
    dispatch(globalActions.setLanguage(language))
  }
})

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSetPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor
  },
})
