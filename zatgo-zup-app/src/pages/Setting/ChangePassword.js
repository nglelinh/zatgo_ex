/**
 *  登录密码修改
 * Created by zhoujianxin on 2018/4/12.
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { Toast } from 'antd-mobile';
import I18n from 'I18n';
import { window } from '@zatgo/constants';
import { pswChangeActions } from '../../redux/actions';

const eye_close = require('../../assets/common/com_eyes_close.png')
const eye_open = require('../../assets/common/com_eyes_open.png')

class ChangePassword extends Component {
  state = {
    oldPsw: true,
    newPsw: true,
    againPsw: true,
    params: {
      oldText: '',
      newText: '',
      againtext: ''
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.textInputView, { marginTop: 20 }]}>
          <TextInput
            placeholder={I18n.t('change_oldpassword')}
            placeholderTextColor='#A0A4A8'
            underlineColorAndroid='transparent'
            secureTextEntry={this.state.oldPsw}
            returnKeyType='next'
            onChangeText={(txt) => this.onChangeTxt('oldText', txt)}
            style={styles.textInput}
          />
          <TouchableOpacity activeOpacity={0.6} onPress={() => { this.clickEvent('old') }}>
            <Image source={this.state.oldPsw ? eye_close : eye_open} style={{ marginRight: 10 }} />
          </TouchableOpacity>
        </View>

        <View style={[styles.textInputView,]}>
          <TextInput
            placeholder={I18n.t('change_newpassword')}
            placeholderTextColor='#A0A4A8'
            underlineColorAndroid='transparent'
            secureTextEntry={this.state.newPsw}
            returnKeyType='next'
            onChangeText={(txt) => this.onChangeTxt('newText', txt)}
            style={styles.textInput}
          />
          <TouchableOpacity activeOpacity={0.6} onPress={() => { this.clickEvent('new') }}>
            <Image source={this.state.newPsw ? eye_close : eye_open} style={{ marginRight: 10 }} />
          </TouchableOpacity>
        </View>

        <View style={[styles.textInputView,]}>
          <TextInput
            placeholder={I18n.t('change_passwordagain')}
            placeholderTextColor='#A0A4A8'
            underlineColorAndroid='transparent'
            secureTextEntry={this.state.againPsw}
            returnKeyType='done'
            onChangeText={(txt) => this.onChangeTxt('againtext', txt)}
            style={styles.textInput}
          />
          <TouchableOpacity activeOpacity={0.6} onPress={() => { this.clickEvent('again') }}>
            <Image source={this.state.againPsw ? eye_close : eye_open} style={{ marginRight: 10 }} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity activeOpacity={0.6} onPress={this.onRegister}>
          <LinearGradient colors={['#4DCC7B', '#12A0D2']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
            style={styles.finishButton}>
            <Text style={styles.finishTxt}>{I18n.t('reg_submit')}</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }

  onRegister = () => {
    const { oldText, newText, againtext } = this.state.params;
    if (newText !== againtext) {
      Toast.show(I18n.t('toast_inputText_inconsistent_login'))
      return
    }
    if (oldText == '' || newText === '' || againtext === '') {
      Toast.show(I18n.t('toast_inputText_null'))
      return
    }
    if (oldText === newText) {
      Toast.show(I18n.t('toast_inputText_same'))
      return
    }

    var pattern = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
    var strOldpwd = pattern.test(oldText);
    var strNewPwd = pattern.test(newText);
    if (!strNewPwd || !strOldpwd) {
      //密码格式不正确
      Toast.fail(I18n.t('toast_inputText_pwderror'))
      return
    }

    this.props.editPsw({ 'newPassword': newText, 'oldPassword': oldText })
  }

  onChangeTxt = (type, text) => {
    this.state.params[type] = text
    this.setState({ params: this.state.params });
  }

  clickEvent = (type) => {
    if (type === 'old') {
      let pwdState = !this.state.oldPsw
      this.setState({
        oldPsw: pwdState
      })

    } else if (type === 'new') {
      let pwdState = !this.state.newPsw
      this.setState({
        newPsw: pwdState
      })
    } else if (type === 'again') {
      let pwdState = !this.state.againPsw
      this.setState({
        againPsw: pwdState
      })
    }
  }
}

const mapDispatchToProps = (dispatch) => ({
  editPsw: (Params) => {
    dispatch(pswChangeActions.LoginPwsChange(Params))
  },
})

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
  textInputView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 6,
    borderBottomColor: '#d9d9d9',
    borderBottomWidth: 0.5,
  },
  textInput: {
    flex: 1,
    height: 44,
    fontSize: 16 * window.fontScale,
  },
  finishButton: {
    height: 40,
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#12A0D2',
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 5,

  },
  finishTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  },
});