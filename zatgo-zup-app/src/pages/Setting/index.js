import EditLoginPassword from './ChangePassword'
import EditPayPassword from './EditPayPassword'
import ResetTradingPassword from './ResetTradingPassword'

export {
  EditLoginPassword,
  EditPayPassword,
  ResetTradingPassword
}