/**
 *  支付密码修改
 * Created by zhoujianxin on 2018/4/13.
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { Toast } from 'antd-mobile';
import I18n from 'I18n';
import { window } from '@zatgo/constants';
import { pswChangeActions } from '../../redux/actions';

const eye_close = require('../../assets/common/com_eyes_close.png')
const eye_open = require('../../assets/common/com_eyes_open.png')

class EditPayPassword extends Component {
  state = {
    oldPayPwd: true,
    newPayPwd: true,
    againPayPwd: true,
    params: {
      oldText: '',
      newText: '',
      againtext: ''
    }
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={[styles.textInputView, { marginTop: 20 }]}>
          <TextInput
            placeholder={I18n.t('edit_old_paypwd')}
            placeholderTextColor='#A0A4A8'
            underlineColorAndroid='transparent'
            secureTextEntry={this.state.oldPayPwd}
            keyboardType="numeric"
            returnKeyType='next'
            maxLength={6}
            onChangeText={(txt) => this.onChangeTxt('oldText', txt)}
            style={styles.textInput}
          />
          <TouchableOpacity activeOpacity={0.6} onPress={() => { this.clickEvent('old') }}>
            <Image source={this.state.oldPayPwd ? eye_close : eye_open} style={{ marginRight: 10 }} />
          </TouchableOpacity>
        </View>

        <View style={[styles.textInputView,]}>
          <TextInput
            placeholder={I18n.t('edit_new_paypwd')}
            placeholderTextColor='#A0A4A8'
            underlineColorAndroid='transparent'
            secureTextEntry={this.state.newPayPwd}
            keyboardType="numeric"
            returnKeyType='next'
            maxLength={6}
            onChangeText={(txt) => this.onChangeTxt('newText', txt)}
            style={styles.textInput}
          />
          <TouchableOpacity activeOpacity={0.6} onPress={() => { this.clickEvent('new') }}>
            <Image source={this.state.newPayPwd ? eye_close : eye_open} style={{ marginRight: 10 }} />
          </TouchableOpacity>
        </View>

        <View style={[styles.textInputView,]}>
          <TextInput
            placeholder={I18n.t('edit_paypwd_again')}
            placeholderTextColor='#A0A4A8'
            underlineColorAndroid='transparent'
            secureTextEntry={this.state.againPayPwd}
            keyboardType="numeric"
            returnKeyType='done'
            maxLength={6}
            onChangeText={(txt) => this.onChangeTxt('againtext', txt)}
            style={styles.textInput}
          />
          <TouchableOpacity activeOpacity={0.6} onPress={() => { this.clickEvent('again') }}>
            <Image source={this.state.againPayPwd ? eye_close : eye_open} style={{ marginRight: 10 }} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity activeOpacity={0.6} onPress={this.onRegister}>
          <LinearGradient colors={['#4DCC7B', '#12A0D2']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
            style={styles.finishButton}>
            <Text style={styles.finishTxt}>{I18n.t('reg_submit')}</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }

  onRegister = () => {
    const { oldText, newText, againtext } = this.state.params;
    if (newText !== againtext) {
      Toast.show(I18n.t('toast_inputText_inconsistent_pay'))
      return
    }
    if (oldText == '' || newText === '' || againtext === '') {
      Toast.show(I18n.t('toast_inputText_null'))
      return
    }
    if (oldText === newText) {
      Toast.show(I18n.t('toast_inputText_same'))
      return
    }
    if (newText.length != 6 || againtext.length != 6) {
      Toast.show(I18n.t('toast_inputText_length'))
      return
    }
    if(!/^[0-9]*$/.test(newText)){
      //支付密码格式不正确
      Toast.fail(I18n.t('toast_inputText_length'))
      return
    }

    this.props.editPayPwd({ 'newPassword': newText, 'oldPassword': oldText })
  }

  onChangeTxt = (type, text) => {
    this.state.params[type] = text.replace(/\s+/g, "")
    this.setState({ params: this.state.params });
  }

  clickEvent = (type) => {
    if (type === 'old') {
      let pwdState = !this.state.oldPayPwd
      this.setState({
        oldPayPwd: pwdState
      })
    } else if (type === 'new') {
      let pwdState = !this.state.newPayPwd
      this.setState({
        newPayPwd: pwdState
      })
    } else if (type === 'again') {
      let pwdState = !this.state.againPayPwd
      this.setState({
        againPayPwd: pwdState
      })
    }
  }
}

const mapDispatchToProps = (dispatch) => ({
  editPayPwd: (Params) => {
    dispatch(pswChangeActions.EditPayPwd(Params))
  },

})

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(EditPayPassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
  textInputView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 6,
    borderBottomColor: '#d9d9d9',
    borderBottomWidth: 0.5,
  },
  textInput: {
    flex: 1,
    height: 44,
    fontSize: 16 * window.fontScale,
  },
  finishButton: {
    height: 40,
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#12A0D2',
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 5,

  },
  finishTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  },
});