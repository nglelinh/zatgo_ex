/**
 * 支付密码重置
 * Created by zhoujianxin on 2018/4/16.
 */

import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { Toast } from 'antd-mobile';
import I18n from 'I18n';
import { window } from '@zatgo/constants';
import TimerButton from '../../components/TimeButton/TimerButton';
import { authActions, pswChangeActions } from '../../redux/actions';
import H5 from '../../components/SliderView';


const eye_close = require('../../assets/common/com_eyes_close.png');
const eye_open = require('../../assets/common/com_eyes_open.png');

const TIME_NORMAL = 0;//默认
const TIME_START = 1;// 倒计时开始
const TIME_END = 2;// 结束

class ResetTradingPassword extends Component {

  state = {
    visible:false,
    payPwd: true,
    payPwdConfirm: true,
    userName: '',
    codeState: TIME_NORMAL,
    formData: {
      'payPassword': '',
      'payPasswordConfirm': '',
      'verifyCode': ''
    },
  }

  //2 秒后随机模拟获取验证码成功或失败的情况
  _requestAPI = (shouldStartCounting) => {
    this.TimerBtn._setEnbleState()
    this.setState({
      codeState: TIME_START,
      visible: true
    })
  }

  onClose = () => {
    this.setState({
      visible: false
    })
  }

  onSubmit = (data) => {
    const {userName} = this.props.authInfo
    let params = data ? {'aliyunValidateCode': data} : {};
    params['mobilePhone'] = userName;
    this.TimerBtn._shouldStartCountting(true);
    this.props.sendPhoneAuthCode(params)
  }

    onReset = () => {
    const { userName } = this.props.authInfo
    const { payPassword, verifyCode, payPasswordConfirm } = this.state.formData
    const param = { userName, 'newPassword': payPassword, verifyCode }

    if (this.state.codeState < 1) {
      Toast.show(I18n.t('toast_get_verifycode_first'))
      return
    }

    if (payPassword !== payPasswordConfirm) {
      Toast.show(I18n.t('toast_inputText_inconsistent_pay'))
      return
    }
    if (verifyCode === '') {
      Toast.show(I18n.t('toast_enter_verify_code'))
      return
    }
    if (userName === '' || payPassword === '' || payPasswordConfirm === '') {
      Toast.show(I18n.t('toast_inputText_null'))
      return
    }
    if (payPassword.length != 6 || payPasswordConfirm.length != 6) {
      Toast.show(I18n.t('toast_inputText_length'))
      return
    }

    if(!/^[0-9]*$/.test(payPassword)){
      //支付密码格式不正确
      Toast.fail(I18n.t('toast_inputText_length'))
      return
    }

    this.props.resetPayPassword(param)
  }


  render() {
    const { userName } = this.props.authInfo
    return (
      <View style={styles.container}>

        <View style={styles.promptTxtView}>
          <Text style={styles.promptTxt}>
            {`${I18n.t('page_content_Code_send')} ${userName}`}
          </Text>
        </View>

        <View style={{ alignItems: 'center' }}>
          <View style={styles.textInputView}>
            <TextInput
              placeholder={I18n.t('reset_pay_MSG')}
              placeholderTextColor='#A0A4A8'
              underlineColorAndroid='transparent'
              keyboardType="numeric"
              returnKeyType='done'
              maxLength={6}
              onChangeText={(txt) => { this.onChangeTxt('verifyCode', txt) }}
              style={styles.textInput}
            />
            <TimerButton
                ref={(e)=>{this.TimerBtn = e}}
              timerTitle={I18n.t('page_content_code')}
              enable={userName.length}
              onClick={(shouldStartCounting) => { this._requestAPI(shouldStartCounting) }}
              timerEnd={() => {
                this.setState({
                  codeState: TIME_END
                })
              }} />
          </View>


          <View style={[styles.textInputView, { marginTop: 20 }]}>
            <TextInput
              placeholder={I18n.t('reg_tradepsw_hint')}
              placeholderTextColor='#A0A4A8'
              underlineColorAndroid='transparent'
              secureTextEntry={this.state.payPwd}
              keyboardType="numeric"
              returnKeyType='next'
              maxLength={6}
              onChangeText={(txt) => this.onChangeTxt('payPassword', txt)}
              style={styles.textInput}
            />
            <TouchableOpacity activeOpacity={0.6} onPress={() => { this.clickEvent('payPassword') }}>
              <Image source={this.state.payPwd ? eye_close : eye_open} style={{ marginRight: 10 }} />
            </TouchableOpacity>
          </View>

          <View style={[styles.textInputView, { marginTop: 20 }]}>
            <TextInput
              placeholder={I18n.t('reg_retradepsw_hint')}
              placeholderTextColor='#A0A4A8'
              underlineColorAndroid='transparent'
              secureTextEntry={this.state.payPwdConfirm}
              keyboardType="numeric"
              returnKeyType='done'
              maxLength={6}
              onChangeText={(txt) => this.onChangeTxt('payPasswordConfirm', txt)}
              style={styles.textInput}
            />
            <TouchableOpacity activeOpacity={0.6} onPress={() => { this.clickEvent('payPwdConfirm') }}>
              <Image source={this.state.payPwdConfirm ? eye_close : eye_open} style={{ marginRight: 10 }} />
            </TouchableOpacity>
          </View>
        </View>
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={this.onReset}>
            <LinearGradient colors={['#4DCC7B', '#12A0D2']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
              style={styles.registerButton}>
              <Text style={styles.finishTxt}>{I18n.t('reg_submit')}</Text>
            </LinearGradient>
          </TouchableOpacity>
        <H5 visible={this.state.visible}
            onSubmit={(data)=>{this.onSubmit(data)}}
            onClose={() => {this.onClose()}}/>
      </View>
    );
  }

  clickEvent = (type) => {
    if (type === 'payPassword') {
      let pwdState = !this.state.payPwd
      this.setState({
        payPwd: pwdState
      })
    } else if (type === 'payPwdConfirm') {
      let pwdState = !this.state.payPwdConfirm
      this.setState({
        payPwdConfirm: pwdState
      })
    }
  }

  onChangeTxt = (field, value) => {
    const { formData } = this.state;
    formData[field] = value.replace(/\s+/g, "");
    this.setState({ formData });
  }
}
const mapDispatchToProps = (dispatch) => ({
  sendPhoneAuthCode: (params) => {
    dispatch(authActions.sendPhoneAuthCode(params))
  },
  sendEmailAuthCode: (params) => {
    dispatch(authActions.sendEmailAuthCode(params))
  },
  resetPayPassword: (params) => {
    dispatch(pswChangeActions.ResetPayPwd(params))
  }
})

const mapStateToProps = (state) => ({
  authInfo: state.user.authInfo,
  sendMsgState: state.sendMsgState
})

export default connect(mapStateToProps, mapDispatchToProps)(ResetTradingPassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  promptTxtView: {
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 6,
    borderBottomColor: '#d9d9d9',
    borderBottomWidth: 0.5,
    justifyContent: 'center',
    minHeight: 44,
  },
  promptTxt: {
    color: '#797F85',
    fontSize: 14 * window.fontScale
  },
  textInputView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 6,
    borderBottomColor: '#d9d9d9',
    borderBottomWidth: 0.5,
  },
  textInput: {
    flex: 1,
    height: 44,
    fontSize: 16 * window.fontScale,
  },
  registerButton: {
    height: 40,
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#12A0D2',
    borderRadius: 5,
    marginLeft: 20,
    marginRight: 20,
  },
  finishTxt: {
    fontSize: 18 * window.fontScale,
    color: 'white'
  },
});