/**
 * Created by zhoujianxin on 2019/1/15.
 * @Desc
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import I18n from 'I18n';

export default class CoinTypeDetail extends Component {
  render() {
    const {detail} = this.props;
    console.log(detail)
    return (
        <View style={styles.container}>
          <Text style={{fontSize: 18, fontWeight: 'bold'}}>{I18n.t('asset_coinName')}{detail.coinType}</Text>
          <Text style={{fontSize: 18, fontWeight: 'bold', marginTop: 10}}>{I18n.t('asset_Introduction')}</Text>
          <Text style={{marginLeft: 45, marginTop: 5}}>{detail.introduction}</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
    paddingHorizontal: 15
  },
});