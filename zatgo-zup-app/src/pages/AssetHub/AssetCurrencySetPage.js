/*
 *  @File   : AssetCurrencySetPage
 *  @Author : lsl
 *  @Date   : 2018-4-1 10:1:39
 *  @Last Modified   : 2018-4-1 10:1:39
 *  @Desc 币种设置
 */
import React, { Component } from 'react'
import { View, Text, StyleSheet, FlatList } from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { walletActions } from '../../redux/actions';
import SearchBar from '@zatgo/components/SearchBar';
import I18n from 'I18n';
import AssetCurrencyItem from './AssetCurrencyItem';


ItemSeparator = () => (
  <View style={styles.separator} />
)

class AssetCurrencySetPage extends Component {

  state = {
    coinTypes: null,
  }

  componentDidMount() {
    this.props.getCointypes();
  }

  onPressAdd(item) {
    const { authInfo } = this.props;
    const params = {
      "name": item.coinType,
      "userId": authInfo.userId,
      "currencyType": item.coinType,
      "networkType": item.coinNetworkType
    }
    this.props.createAccount(params)
  }

  onPressDelete(item) {
    // const {accounts} = this.props
    // var accountId = ''
    // for (var i = 0; i < accounts.length; i++) {
    //   if (accounts[i]['coinNetworkType'] ==item.networkType && accounts[i].coinType === item.coinType  ) {
    //     accountId = accounts[i].accountId
    //   }
    // }
    // this.props.deleteAccount(accountId)
  }

  onChangeText = (txt) => {
    let coinTypes = this.props.coinTypes.filter((item) => {
      return item.coinType.indexOf(txt) !== -1 || item.coinType.indexOf(txt.toUpperCase()) !== -1;
    })
    this.setState({ coinTypes })
  }

  keyExtractor = (item, index) => index + '_';

  renderItem = ({ item }) => (
    <AssetCurrencyItem
      title={item.coinType}
      thumb={item.coinImage}
      check={this.checkState(item)}
      onAddAccount={(checked) => checked ? this.onPressAdd(item) : this.onPressDelete(item)}
    />
  )
  
  checkState = (item) => {
    const { accounts } = this.props
    var checkstate = false
    if(accounts&&accounts.length>0) {
      for (var i = 0; i < accounts.length; i++) {
        if (accounts[i]['coinNetworkType'] === item.coinNetworkType && accounts[i].coinType === item.coinType) {
          checkstate = true
          break
        }
      }
    }
    return checkstate
  }


  render() {
    const { coinTypes } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.container}>
          <SearchBar
            maxLength={20}
            placeholder={I18n.t('asset_search_hint')}
            onChangeText={this.onChangeText} />
        </View>
        <FlatList
          data={coinTypes ? coinTypes : this.props.coinTypes}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          ItemSeparatorComponent={ItemSeparator}
          ListEmptyComponent={() => this.renderNodata()}
        />
      </View>
    );
  }
  renderNodata = () => {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
        <Text style={styles.searchText}>{I18n.t('order_not_search')}</Text>
      </View>
    )
  }
}


const mapDispatchToProps = (dispatch) => ({
  getCointypes: () => dispatch(walletActions.getCointypes()),
  createAccount: (params) => dispatch(walletActions.createAccount(params)),
  deleteAccount: (params) => dispatch(walletActions.deleteAccount(params)),
})

const mapStateToProps = (state) => ({
  coinTypes: state.wallet.coinTypes,
  authInfo: state.user.authInfo,
  accounts: state.wallet.accounts,
})

export default connect(mapStateToProps, mapDispatchToProps)(AssetCurrencySetPage);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'white',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0.5
  },
  separator: {
    flexDirection: 'row',
    height: 0.5,
    backgroundColor: '#d4d4d4'
  },
  searchText: {
    fontSize: 17,
    color: '#666666',
  },
});
