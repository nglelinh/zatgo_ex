/*
 *  @File   : AssetHubPage
 *  @Author : lsl
 *  @Date   : 2018-4-1 15:5:6
 *  @Last Modified   : 2018-4-1 15:5:6
 *  @Desc 钱包
 */
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AssetHubToolBar from './AssetHubToolBar';
import AssetHubItemView from './AssetHubItemView';
import { walletActions } from '../../redux/actions';
import theme from '@zatgo/constants/theme';
import { StringUtils } from '../../utils';
import window from '@zatgo/constants/window';
import I18n from 'I18n';

class AssetHubPage extends PureComponent {
  state = {
    queryParam: null,
    accounts: null,
  }

  componentDidMount() {
    this.props.getAccounts();
  }

  onChangeText = (txt) => {
    let accounts = this.props.accounts.filter((item) => {
      return item.coinType.indexOf(txt) !== -1 || item.coinType.indexOf(txt.toUpperCase()) !== -1;
    })
    this.setState({ accounts })
  }

  onAddCurrency = () => {
    Actions.currencySet();
  }

  onItemPress = (item) => {
    Actions.assetPayRecord({ account: item });
  }

  renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      <AssetHubItemView
        title={item.coinType}
        thumb={item.coinImage ? item.coinImage : ''}
        extra={StringUtils.formatE(item.balance)}
        onItemPress={() => this.onItemPress(item)} />
    </View>
  )

  keyExtractor = (item, index) => index + '_';

  renderNodata = () => {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
        <Text style={styles.searchText}>{I18n.t('order_not_search')}</Text>
      </View>
    )
  }

  render() {
    const { accounts } = this.state;
    StatusBar.setBarStyle('dark-content',true);
    StatusBar.setBackgroundColor('#fff');
    return (
      <View style={styles.container}>
        <AssetHubToolBar
          onChangeText={this.onChangeText}
          onAddCurrency={this.onAddCurrency} />
        {this.props.accounts && this.props.accounts.length > 0 ?
          <FlatList
            data={accounts ? accounts : this.props.accounts}
            renderItem={this.renderItem}
            keyExtractor={this.keyExtractor}
            ListEmptyComponent={() => this.renderNodata()}
            ListFooterComponent={() => <View style={{ height: 15 }} />}
          /> :
          <Text style={styles.emptyText}>{I18n.t('order_add_currency')}</Text>
        }
      </View>
    );
  }

}


const mapDispatchToProps = (dispatch) => ({
  getAccounts: () => {
    dispatch(walletActions.getAccounts())
  },
})

const mapStateToProps = (state) => ({
  accounts: state.wallet.accounts,
  authInfo: state.user.authInfo
})

export default connect(mapStateToProps, mapDispatchToProps)(AssetHubPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.bgColor,
    alignItems: 'center',
  },
  itemContainer: {
    width: window.width - 20,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  searchText: {
    fontSize: 17,
    color: '#666666',
  },
  emptyText: {
    fontSize: 17,
    color: '#666666',
    marginTop: window.height / 2 - 100
  },

})
