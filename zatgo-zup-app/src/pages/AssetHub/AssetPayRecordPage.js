/*
 *  @File   : AssetPayRecordPage
 *  @Author : lsl
 *  @Date   : 2018-5-3 10:45:19
 *  @Last Modified   : 2018-5-3 10:45:19
 *  @Desc 某币种交易记录
 */
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux';
import window from '@zatgo/constants/window';
import { walletActions } from '../../redux/actions';
import JXFlatList from '../../components/JXFlatList'
import I18n from 'I18n';
import { StringUtils } from '../../utils';
import { Actions } from 'react-native-router-flux';

const WaveImg = require('../../assets/common/com_wave.png');
const Separator = () => (
  <View style={styles.separator} />
)
const PayRecordItem = ({item}) => {
  let type = I18n.t('coin_source')[item.paymentType]?I18n.t('coin_source')[item.paymentType]:I18n.t('coin_source_other');
  let  typeStr ='';
  if(item.paymentType===1){
  typeStr = `${type+'\n'}(${item.amount < 0? item.receiptUserName:item.payUserName})`;
  }else {
  typeStr = type;
  }
  return (
      <View style={styles.recordContainer}>
        <View>
          <Text style={styles.accountTxt}>
            {typeStr}
          </Text>
          <Text style={styles.dateTxt}>
            {moment(Number(item.paymentDate)).format('MM-DD HH:mm')}
          </Text>
        </View>
        <View style={styles.amountContainer}>
          <Text style={[styles.amountTxt, {color: item.amount < 0 ? "#323B43" : "#4DCC7B"}]}
                numberOfLines={1}
                ellipsizeMode="tail"
          >
            {item.amount < 0 ? StringUtils.formatE(item.amount) : `+${StringUtils.formatE(item.amount)}`}
            <Text style={[styles.typeTxt, {color: item.amount < 0 ? "#323B43" : "#4DCC7B"}]}>
              {item.coinType ? ` ${item.coinType}` : ''}
            </Text>
          </Text>
        </View>
      </View>
  )
}

var pageNo = 1;

class AssetPayRecordPage extends PureComponent {

  componentDidMount() {
    const { account, getPayments } = this.props;
    getPayments({id:account.accountId,'pageNo': pageNo})
    pageNo++;
  }

  componentWillUnmount(){
    pageNo=1;
  }

  _refreshing = () => {
    const { account, getPayments } = this.props;
    pageNo = 1
    getPayments({id:account.accountId,'pageNo': pageNo})
    pageNo++
  }

  _onload = () => {
    const { account, getPayments } = this.props;
    if (this.props.paymentsStatus) {
      getPayments({id:account.accountId,'pageNo': pageNo})
      pageNo++
    }
  }

  _footer = () => {
    if (this.props.paymentsStatus) {
      return (
          <View style={styles.footerStyle}>
            <ActivityIndicator size="small" color="#888888"/>
          </View>
      )
    } else {
      return (
          <View style={styles.footerStyle}>
            <Text>{I18n.t('order_no_more')}</Text>
          </View>
      )
    }
  }

  render() {
    const { account } = this.props;
    console.log(account)
    const balance = account.balance ? StringUtils.formatE(account.balance) : 0;
    return (
      <View style={styles.container}>
        <View style={styles.balanceContainer}>
          <View style={styles.balanceLayout}>
            <Text
                style={styles.balanceTxt}
                allowFontScaling={false}
                numberOfLines={1}
                ellipsizeMode="tail"
            >
              {`${I18n.t('asset_balance')}: ${balance} ${account.coinType}`}
            </Text>
            {account.introduction?
            <View style={{flex: 1, marginTop: 10,marginRight:15,alignItems: 'flex-end', justifyContent: 'flex-end'}}>
              <TouchableOpacity onPress={() => {
                console.log(account.introduction)
                Actions.coinTypeDetail({detail: account});
              }}>
                <Text style={{color: 'white'}}>简介</Text>
              </TouchableOpacity>
            </View>:null}
          </View>
          <Image
            source={WaveImg}
            resizeMode="stretch"
            style={styles.waveImg}
          />
        </View>
        <JXFlatList
          data={this.props.payments}
          refreshing={0}
          pageSize={20}
          onRefresh={this._refreshing}
          renderItem={PayRecordItem}
          ItemSeparatorComponent={Separator}
          ListFooterComponent={this._footer}
          onEndReached={this._onload}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  getPayments: (accountId) => {
    dispatch(walletActions.getAccountPayment(accountId))
  },
})

const mapStateToProps = (state) => ({
  payments: state.wallet.payments,
  paymentsStatus: state.wallet.paymentsStatus,

})

export default connect(mapStateToProps, mapDispatchToProps)(AssetPayRecordPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white'
  },
  balanceContainer: {
    height: 95,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    backgroundColor: '#323B43',
  },
  balanceLayout: {
    flexDirection: 'row',
    width: window.width,
  },
  waveImg: {
    height: 40, width: window.width
  },
  balanceTxt: {
    color: '#fff',
    fontSize: 20 * window.fontScale,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 22,
    marginBottom: 10,
  },
  recordContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'space-between',
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 0.5,
    padding: 15,
  },
  amountContainer: {
    flex:1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    // width: 150,
  },
  amountTxt: {
    flex: 1,
    textAlign: 'right',
    color: '#4DCC7B',
    fontSize: 20 * window.fontScale,
  },
  dateTxt: {
    color: '#797F85',
    fontSize: 11 * window.fontScale,
    marginTop: 7
  },
  accountTxt: {
    color: '#323B43',
    fontSize: 15 * window.fontScale,
  },
  typeTxt: {
    color: '#4DCC7B',
    fontSize: 12 * window.fontScale,
    marginLeft: 5,
  },
  separator: {
    height: 0.5,
    backgroundColor: '#d4d4d4'
  },
  footerStyle: {
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2
  }
});
