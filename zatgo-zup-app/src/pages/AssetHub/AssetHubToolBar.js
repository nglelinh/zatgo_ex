/*
 *  @File   : AssetHubToolBar
 *  @Author : lsl
 *  @Date   : 2018-4-3 17:16:56
 *  @Last Modified   : 2018-4-3 17:16:56
 *  @Desc 钱包-搜索&添加
 */
import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import SearchBar from '@zatgo/components/SearchBar';
import I18n from 'I18n';

const AssetHubToolBar = (props) => (
  <View style={styles.container}>
    <SearchBar
      maxLength={20}
      placeholder={I18n.t('asset_search_hint')}
      onChangeText={props.onChangeText} />
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={props.onAddCurrency}>
      <Image
        style={styles.iconAdd}
        source={require('@zatgo/assets/common/com_add.png')} />
    </TouchableOpacity>
  </View>
)

export default AssetHubToolBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'white',
  },
  iconAdd: {
    width: 35,
    height: 35,
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: 'transparent',
  }
})