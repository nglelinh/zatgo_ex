import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import { Switch } from 'antd-mobile';
import window from '@zatgo/constants/window';

const AssetCurrencyItem = (props) => (
  <View style={styles.container}>
    <View style={styles.rowLayout}>
      {props.thumb ? <Image
        style={styles.thumb}
        source={{ uri: props.thumb }} /> : null}
      <Text style={styles.txtStyle}>{props.title}</Text>
    </View>

    <Switch checked={props.check}
      onChange={(checked) => { props.onAddAccount(checked) }}
      disabled={props.check}
    />

  </View>
)

export default AssetCurrencyItem;

const styles = StyleSheet.create({
  container: {
    height: 70,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 0,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  thumb: {
    width: 40,
    height: 40,
    marginRight: 10,
    backgroundColor: 'transparent',
  },
  txtStyle: {
    color: '#323B43',
    fontSize: 18 * window.fontScale,
  },
  rowLayout: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  iconAdd: {
    width: 35,
    height: 35,
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: 'transparent',
  }
});
