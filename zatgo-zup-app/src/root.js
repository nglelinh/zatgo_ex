'use strict';

import React, { Component } from 'react';
import { View, StatusBar, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Provider } from 'react-redux';
import App from './router';
import store from './redux/store';
import ActiveModel from './pages/Home/components/ActiveModel';


if (!__DEV__) {
  global.console = {
    info: () => { },
    log: () => { },
    warn: () => { },
    error: () => { },
  }
}

// global.showModel = () => {}; // 所有子页面均可直接调用global.showLoading()来展示Loading
// global.closeModel = () => {}; // 所有子页面均可直接调用global.closeLoading()来关闭Loading

export default class Root extends Component {

  state = {
    activeState: false,
    taskList: []
  }

  componentWillMount() {
    Linking.addEventListener('url', this.handleOpenURL);
    const { serverUrl, feibaH5, devEnv, tradeParams, payUrl, downloadUrl, exchangeUrl, sliderH5 } = this.props;
    global.serverUrl = serverUrl;
    global.feibaH5 = feibaH5;
    global.devEnv = devEnv;
    global.trade = tradeParams ? tradeParams.replace('zatgo://', '') : '';
    global.payUrl = payUrl ? payUrl : '';
    global.downloadUrl = downloadUrl ? downloadUrl : '';
    global.exchangeUrl = exchangeUrl;
    global.sliderH5 = sliderH5;

    global.showModel = (data, type, state) => {
      this.setActiveModel(data, type, state);
    }

    global.closeModel = () => {
      this.onClose();
    }
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this.handleOpenURL);
  }

  handleOpenURL = (event) => {
    console.log(event.url)
    if (event.url.replace('zatgo://', '') !== '') {
      global.trade = event.url.indexOf("zatgo://") !== -1 ? event.url.replace('zatgo://', '') : '';
      if (global.trade) {
        Actions.reset('splash')
      }
    }
  }

  onClose = () => {
    let temp = this.state.taskList.length > 1 ? this.state.taskList.slice(1) : [];
    StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor('white');
    this.setState({
      activeState: false
    }, () => {
      this.timer = setTimeout(
        () => {
          if (temp.length > 0) {
            StatusBar.setBarStyle('light-content', true);
            StatusBar.setBackgroundColor('gray');
            this.setState({ activeState: true, taskList: temp })
          }
        }, 1000);

    })
  }

  setActiveModel = (data, type, state) => {
    let temp = [];
    if (data !== {} && state) {
      if (type) {
        temp.push(data);
      } else {
        for (let key in data) {
          for (let i = 0; i < data[key].length; i++) {
            let items = data[key];
            let currentTimestamp = new Date().getTime();
            if (items[i].isDone === 0 && items[i].isOpen !== '0' && items[i].clickEventType === 3 && !items[i].isDone && parseFloat(currentTimestamp) >= parseFloat(items[i].startDate)
              && parseFloat(currentTimestamp) < parseFloat(items[i].endDate)) {
              temp.push(items[i]);
            }
          }
        }
      }
    }
    if (temp.length > 0 && state) {
      StatusBar.setBarStyle('light-content', true);
      StatusBar.setBackgroundColor('gray');
      this.setState({ activeState: true, taskList: temp })
    }
  }

  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <ActiveModel
            visible={this.state.activeState}
            dataURL={this.state.taskList[0]}
            onClose={this.onClose}
          />
          <App {...this.props} />
        </View>
      </Provider>
    )
  }
}

