import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, BackHandler, DeviceEventEmitter, StatusBar, Platform } from 'react-native';
import {
    Scene,
    Router,
    Actions,
    Tabs,
    Stack,
} from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Toast } from 'antd-mobile';
import I18n from 'I18n';
import { getLanguages } from 'react-native-i18n';
import { StorageUtils } from "./utils";
import { authActions, globalActions } from './redux/actions';
import ImageParse from '@zatgo/components/ImageParse'
import { window, constDefines } from '@zatgo/constants';
import UserPage from './pages/Mine';
import HomePage from './pages/Home';
import { DigPage, DigRecordspage, ComputePowerRecord, LargeTravelPage, DigTaskPage } from './pages/Dig'
import UserInfoPage from './pages/Mine/userInfo';
import RealName from './pages/Mine/userInfo/RealName';
import Coupons from './pages/Mine/Coupons';
import SplashPage from './pages/Splash';
import WebViewPage from './pages/Web/WebViewPage';
import { PaymentPage, PaymentSuccessPage } from './pages/Payment';
import { RichScanPage, RichScanResultPage } from './pages/RichScan';
import { AssetHubPage, AssetPayRecordPage, AssetCurrencySetPage } from './pages/AssetHub';
import { EditPayPassword, EditLoginPassword, ResetTradingPassword } from './pages/Setting';
import { DepositPage, DepositHistoryPage, DepositHistoryDetailPage } from './pages/User/Deposit';
import { ExtractPage, ExtractHistoryPage, ExtractHistoryDetailPage } from './pages/User/Extract';
import { CollectionPage, CollectionPayRecordPage } from './pages/Collection';
import {
    LoginIndexPage,
    LoginRegisterPage,
    LoginRegisterCaptchaPage,
    LoginRegisterSetPwdPage,
    LoginResetPasswordPage
} from './pages/Login';
import {
    SettingPage,
    CoinSelectPage,
    DefaultAccountPage,
    LanguageSetPage,
    ServicePage,
    OrderQueryPage,
    OrderQueryDetail,
    InviteCodePage,
    OrderList
} from './pages/User';
import CoinTypeDetail from './pages/AssetHub/CoinTypeDetail'

const ArrowLeft = require('./assets/common/left.png');
const ArrowCommon = require('./assets/common/com_arrow.png');
const CloseWhite = require('./assets/common/com_close_white.png');

const TabDig = require('./assets/tabbar/Dig2.png');
const TabDigActive = require('./assets/tabbar/Dig.png');
const TabPrice = require('./assets/tabbar/Price2.png');
const TabPriceActive = require('./assets/tabbar/Price.png');
const TabHome = require('./assets/tabbar/Home2.png');
const TabHomeActive = require('./assets/tabbar/Home.png');
const TabUser = require('./assets/tabbar/User2.png');
const TabUserActive = require('./assets/tabbar/User.png');

const DigIcon = (props) => (
    <Image
        style={styles.tabItem}
        source={props.focused ? TabDigActive : TabDig}
    />
)

const PriceIcon = (props) => (
    <Image
        style={styles.tabItem}
        source={props.focused ? TabPriceActive : TabPrice}
    />
)

const HomeIcon = (props) => (
    <Image
        style={styles.tabItem}
        source={props.focused ? TabHomeActive : TabHome}
    />
)

const UserIcon = (props) => (
    <Image
        style={styles.tabItem}
        source={props.focused ? TabUserActive : TabUser}
    />
)

const LeftRecordButton = () => (
    <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => Actions.paymentRecords()}
    >
        <Text style={styles.titleRightLight}>
            {I18n.t('colection_record')}
        </Text>
    </TouchableOpacity>
)

const RightDigHashRateButton = () => (
    <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => Actions.push('computePowerRecord')}
    >
        <Text style={styles.titleRightLight}>
            {I18n.t('dig_viewlog')}
        </Text>
    </TouchableOpacity>
)

const LeftFinishButton = () => (
    <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => { Actions.reset('tabbar'); Actions.home() }}
    >
        <Text style={styles.titleRightDark}>
            {I18n.t('pay_finish')}
        </Text>
    </TouchableOpacity>
)

const RightCloseButton = () => (
    <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => Actions.reset('tabbar')}
    >
        <Image
            resizeMode="cover"
            source={CloseWhite}
            style={{ marginRight: 10 }} />
    </TouchableOpacity>
)

const QRCodeButton = () => (
    <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => {
            ImageParse.parseGalleryImage()
        }}
    >
        <Text style={styles.titleRightWhite}>
            {I18n.t('pay_photos')}
        </Text>
    </TouchableOpacity>
)

class RootRouter extends Component {

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
    }

    componentDidMount() {
        this.props.getBannerList({ siteType: 0 });
        StorageUtils.get(constDefines.defaultLanguageKey)
            .then((language) => {
                if (language) {
                    this.props.setLanguage(language);
                } else {
                    getLanguages().then(languages => {
                        // console.log(languages);
                        let param = languages[0].indexOf("zh") != -1 ? 'zh' : 'en';
                        this.props.setLanguage(param);
                    });
                }
            });
    }

    onBackAndroid = () => {
        if (Actions.currentScene === '_home') {
            if (this.lastBackPressed && this.lastBackPressed + 2000 >= Date.now()) {
                BackHandler.exitApp();
            }
            this.lastBackPressed = Date.now();
            Toast.info(I18n.t('toast_exit_again'));
            return true;
        } else {
            Actions.pop();
        }
    }

    render() {
        const { language } = this.props;
        language && (I18n.locale = language);
        return (
            <Router>
                <Stack
                    key="root"
                    titleStyle={styles.titleLight}
                    navigationBarStyle={{ backgroundColor: "white", }}
                    backButtonImage={ArrowLeft}
                    renderRightButton={() => <View />}
                >

                    <Scene key="splash" hideNavBar component={SplashPage} />
                    <Scene key="login" hideNavBar component={LoginIndexPage} />

                    <Scene
                        key="register"
                        component={LoginRegisterPage}
                        titleStyle={styles.titleDark}
                        navigationBarStyle={styles.navBarLight}
                        backButtonImage={ArrowCommon}
                    />
                    <Scene
                        key="captcha"
                        component={LoginRegisterCaptchaPage}
                        title={() => I18n.t('title_reg_captcha')}
                        titleStyle={styles.titleDark}
                        navigationBarStyle={styles.navBarLight}
                        backButtonImage={ArrowCommon}
                    />
                    <Scene
                        key="password"
                        component={LoginRegisterSetPwdPage}
                        title={() => I18n.t('title_recovery_pwd')}
                        titleStyle={styles.titleDark}
                        navigationBarStyle={styles.navBarLight}
                        backButtonImage={ArrowCommon}
                    />
                    <Scene
                        key="forgotPasswordPage"
                        component={LoginResetPasswordPage}
                        title={() => I18n.t('title_reg_psw')}
                        titleStyle={styles.titleDark}
                        navigationBarStyle={styles.navBarLight}
                        backButtonImage={ArrowCommon}
                    />

                    <Tabs
                        key="tabbar"
                        tabBarPosition="bottom"
                        tabBarStyle={styles.tabbarContainer}
                        activeTintColor="#323B43"
                        inactiveTintColor="#797F85"
                        swipeEnabled={false}
                    >
                        <Scene
                            key="home"
                            hideNavBar
                            icon={HomeIcon}
                            component={HomePage}
                            title={I18n.t('title_home')}
                            tabBarOnPress={() => {
                                StatusBar.setBarStyle('dark-content', true);
                                StatusBar.setBackgroundColor('#fff');
                                return Actions.home();
                            }}
                            initial
                        />

                        <Scene
                            key="market"
                            hideNavBar
                            icon={PriceIcon}
                            component={WebViewPage}
                            title={I18n.t('title_market')}
                            tabBarOnPress={() => {
                                return Actions.market();
                            }}
                        />


                        <Scene
                            key="dig"
                            hideNavBar
                            icon={DigIcon}
                            component={DigPage}
                            title={I18n.t('title_dig')}
                            onEnter={() => {
                                DeviceEventEmitter.emit('StatusBar'); //发监听
                            }}
                            tabBarOnPress={() => {
                                DeviceEventEmitter.emit('HashRaseRefresh'); //发监听
                                StatusBar.setBarStyle('light-content', true);
                                return Actions.dig();
                            }}

                        />
                        <Scene
                            key="mine"
                            hideNavBar
                            icon={UserIcon}
                            component={UserPage}
                            title={I18n.t('title_user')}
                            tabBarOnPress={() => {
                                StatusBar.setBarStyle('dark-content', true);
                                StatusBar.setBackgroundColor('#fff');
                                return Actions.mine();
                            }}
                        />
                    </Tabs>

                    <Scene
                        key="assetHub"
                        component={AssetHubPage}
                        title={() => I18n.t('title_assethub')}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="currencySet"
                        component={AssetCurrencySetPage}
                        title={() => I18n.t('title_currency_set')}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="collection"
                        title={() => I18n.t('title_collection')}
                        component={CollectionPage}
                        renderRightButton={LeftRecordButton}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="paymentRecords"
                        title={() => I18n.t('title_payment_records')}
                        component={CollectionPayRecordPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="assetPayRecord"
                        title={() => I18n.t('title_transaction_record')}
                        component={AssetPayRecordPage}
                        navigationBarStyle={[styles.navBarDark, { borderBottomWidth: 0 }]}
                    />
                    <Scene
                        key="coinTypeDetail"
                        title={() => I18n.t('title_coinTypeDetail')}
                        component={CoinTypeDetail}
                        navigationBarStyle={[styles.navBarDark, { borderBottomWidth: 0 }]}
                    />
                    <Scene
                        key="payment"
                        title={() => I18n.t('title_payment')}
                        component={PaymentPage}
                        navigationBarStyle={styles.navBarDark} />
                    <Scene
                        key="paymentSuccess"
                        onBack={() => {
                        }}
                        hideBackImage
                        component={PaymentSuccessPage}
                        renderRightButton={LeftFinishButton}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="editLoginPassword"
                        title={() => I18n.t('title_change_Login_password')}
                        component={EditLoginPassword}
                        navigationBarStyle={styles.navBarDark} />
                    <Scene
                        key="defaultAccount"
                        title={() => I18n.t('title_default_account')}
                        component={DefaultAccountPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="editPayPassword"
                        title={() => I18n.t('title_change_trading_password')}
                        component={EditPayPassword}
                        navigationBarStyle={styles.navBarDark} />
                    <Scene
                        key="resetTradingPassword"
                        title={() => I18n.t('title_reset_trading_password')}
                        component={ResetTradingPassword}
                        navigationBarStyle={styles.navBarDark} />
                    <Scene
                        key="deposit"
                        title={() => I18n.t('title_deposit')}
                        component={DepositPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="rollout"
                        title={() => I18n.t('title_rollout')}
                        component={ExtractPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="selectCoin"
                        title={() => I18n.t('title_select_coin')}
                        component={CoinSelectPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene key="depositHistory"
                        title={() => I18n.t('title_deposit_history')}
                        component={DepositHistoryPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="depositHisDetail"
                        title={() => I18n.t('title_detail')}
                        component={DepositHistoryDetailPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="extractHistory"
                        title={() => I18n.t('title_extract_history')}
                        component={ExtractHistoryPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="extractHisDetail"
                        title={() => I18n.t('title_detail')}
                        component={ExtractHistoryDetailPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="OrderList"
                        title={() => I18n.t('title_detail')}
                        component={OrderList}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="servicePage"
                        title={() => I18n.t('title_contact_us')}
                        component={ServicePage}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene
                        key="languageSet"
                        title={() => I18n.t('set_language')}
                        component={LanguageSetPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="setting"
                        title={() => I18n.t('title_setting')}
                        component={SettingPage}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene
                        key="orderquery"
                        title={() => I18n.t('title_order_query')}
                        component={OrderQueryPage}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene
                        key="orderquerydetail"
                        title={() => I18n.t('title_order_detail')}
                        component={OrderQueryDetail}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene
                        key="richScan"
                        component={RichScanPage}
                        renderRightButton={QRCodeButton}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene key="otherRichScan"
                        component={RichScanPage}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene key="scanResult"
                        component={RichScanResultPage}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene
                        key="digRecords"
                        title={() => I18n.t('title_dig_records')}
                        component={DigRecordspage}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene
                        key="InviteCode"
                        title={() => I18n.t('title_invite_Code')}
                        component={InviteCodePage}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene
                        key="computePowerRecord"
                        title={() => I18n.t('title_computepower_records')}
                        component={ComputePowerRecord}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="largeTravel"
                        navigationBarStyle={{
                            backgroundColor: 'transparent',
                            elevation: 0,
                            borderBottomWidth: 0,
                            borderBottomColor: 'white',
                            paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
                        }}
                        backButtonImage={require('./assets/dig/Arrow.png')}
                        component={LargeTravelPage}
                    />
                    <Scene
                        key="digTask"
                        title={() => I18n.t('title_digtask')}
                        component={DigTaskPage}
                        renderRightButton={RightDigHashRateButton}
                        navigationBarStyle={styles.navBarLight} />

                    <Scene
                        key="userInfo"
                        title={() => I18n.t('title_userinfo')}
                        component={UserInfoPage}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="RealName"
                        title={() => I18n.t('title_authentication')}
                        component={RealName}
                        navigationBarStyle={styles.navBarLight} />
                    <Scene
                        key="Coupons"
                        title={() => I18n.t('title_coupons')}
                        component={Coupons}
                        navigationBarStyle={styles.navBarLight} />
                </Stack>
            </Router>
        );
    }
}

const styles = StyleSheet.create({
    tabbarContainer: {
        backgroundColor: "white",
        borderTopWidth: 0,
        borderTopColor: 'white'
    },
    tabItem: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
    },
    navBarDark: {
        backgroundColor: "white",
        elevation: 0,
        height: 44,
        borderBottomWidth: 0,
        borderBottomColor: 'white',
        marginTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
    },
    navBarLight: {
        backgroundColor: "white",
        elevation: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'white',
        height: 44,
        marginTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
    },
    titleLight: {
        alignSelf: 'center',
        color: '#333333',
        fontSize: 21 * window.fontScale,
        fontWeight: 'bold'
    },
    titleDark: {
        fontSize: 18 * window.fontScale,
        alignSelf: 'center',
        color: 'black'
    },
    titleRightLight: {
        color: '#333333',
        fontSize: 16 * window.fontScale,
        marginRight: 10
    },
    titleRightDark: {
        color: '#323B43',
        fontSize: 16 * window.fontScale,
        marginRight: 10
    },
    titleRightWhite: {
        color: '#333333',
        fontSize: 16 * window.fontScale,
        marginRight: 10
    }
});


const mapDispatchToProps = (dispatch) => ({
    setLanguage: (language) => {
        dispatch(globalActions.setLanguage(language))
    },
    getBannerList: (sign) => {
        dispatch(authActions.getBannerList(sign))
    },
})

const mapStateToProps = (state) => ({
    language: state.global.language,
})

export default connect(mapStateToProps, mapDispatchToProps)(RootRouter);