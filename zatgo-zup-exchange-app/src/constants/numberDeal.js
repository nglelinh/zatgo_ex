
function getFullNum(num) {
  // 处理非数字
  if (isNaN(num)) { return num }
  // 处理不需要转换的数字
  let str = String(num);
  if (!/e/i.test(str)) { return num }
  return (num).toFixed(18).replace(/\.?0+$/, '');
}

function numSub(num1, num2) {
  let baseNum; let baseNum1; let baseNum2;
  let precision;// 精度
  try {
    baseNum1 = num1.toString().split('.')[1].length;
  } catch (e) {
    baseNum1 = 0;
  }
  try {
    baseNum2 = num2.toString().split('.')[1].length;
  } catch (e) {
    baseNum2 = 0;
  }
  baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
  precision = (baseNum1 >= baseNum2) ? baseNum1 : baseNum2;
  return ((num1 * baseNum - num2 * baseNum) / baseNum).toFixed(precision);
}


export default {
  getFullNum,
  numSub
};
