export default {
  login: '/api/login',
  coinPairs: '/exchange/appapi/coinPairs',
  publicRate: '/exchange/appapi/rate',
  getEntryOrderData: '/api/getEntryOrderData',
  getBalanceData: '/wallet/account/list',
  getOrderData: '/exchange/appapi/order/pageSearch',
  cancelOrder: '/exchange/appapi/order/cancel',
  submitOrder: '/exchange/appapi/order/create',

  // mine
  mine_info: `/user/basicInfo`,
  mine_assets_coin: `/wallet/account`,
  mine_assets_lever: `/wallet/withfunding/account`,

  // 币币
  coin_withdrawList: `/wallet/extract/record/list`, // 币币提币
  coin_depositList: `/wallet/deposit/record/list`, // 币币充值

  // 杠杆
  lever_borrow: `/exchange/appapi/withFundingApply`, // 借贷申请
  lever_borrowList: `/exchange/appapi/withFundingList`, // 借贷列表
  lever_borrowRepayment: '/exchange/appapi/withFundingRepayment', // 还款

  lever_transferRecharge: `/exchange/appapi/withFundingRecharge`, // 划转充值
  lever_transferWithdrawal: `/exchange/appapi/withFundingWithdrawal`, // 划转提币
  lever_transferList: `/exchange/appapi/withFundingTransferRecord`, // 划转列表

  // 订单管理
  orderManager: '/exchange/appapi/order/pageSearch',

  // login
  phoneLogin: '/auth/login', // 手机登录
  emailLogin: '/auth/loginEmail', // 邮箱登录
  phoneFogotPwd: '/auth/forgetLoginPassword', // 忘记密码-手机
  emailFogotPwd: '/auth/forgetLoginPasswordEmail', // 忘记密码-邮箱
  phoneRegister: '/auth/register', // 手机注册
  emailRegister: '/auth/regEmail', // 邮箱注册
  sendPhoneCode: '/auth/sendPhoneAuthCode', // 手机短信发送
  sendEmailCode: '/auth/sendEmailAuthCode', // 邮箱短信发送
  logout: '/auth/logout', // 退出
};
