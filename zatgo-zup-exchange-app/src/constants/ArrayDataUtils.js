/* webSocket数据处理 */
export function dealWithData(dataArr, Array, key, type = false) {
  let data = dataArr;
  for (let i = 0; i < Array.length; i++) {
    let index = 0;
    for (let j = 0; j < dataArr.length; j++) {
      index++;
      if (Array[i][key] === dataArr[j][key]) {
        data.splice(j, 1, Array[i]);
        index--;

      }
    }
    if (index >= dataArr.length) {
      if (!type) {
        data.push(Array[i]);
      } else {
        let temp = data;
        data = [Array[i]].concat(temp);
      }
    }
  }
  return data;
}
