import React, { Component } from 'react';
import intl from 'react-intl-universal';
import { DocumentTitle } from 'components';
import { connect } from 'dva';
import styles from './MinePage.css';
import MineTop from './MineTop';
import MineListView from './MineListView';

class MinePage extends Component {

  render() {
    return (
      <DocumentTitle title={intl.get('title.user')}>
        <div className={styles.app}>
          <div className={styles.mainContent}>
            <MineTop userInfo={this.props.userInfo} />
            <MineListView userInfo={this.props.userInfo} />
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.mine.userInfo,
});

export default connect(mapStateToProps)(MinePage);

