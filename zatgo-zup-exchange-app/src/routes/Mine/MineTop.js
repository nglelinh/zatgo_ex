import  { Component } from 'react';
import { Flex } from 'antd-mobile';
import styles from './MinePage.css';
import touxiang from '../../assets/mine/mine_touxiang.svg';

class MineTop extends Component {

  hiddenName = (name) => {
    if (name && name.length > 10) {
      return name.substr(0, 3) + '****' + name.substr(7);
    }
    return '';
  }

  render() {
    const {userInfo} = this.props;
    console.log('userInfo', userInfo);
    return (
      <div className={styles.mineTop}>
        <Flex style={{
          textAlign: 'left',
          justifyContent: 'center',
          alignItems: 'center',
          // paddingLeft: '20px',
          height: 108,
          backgroundColor: '#dddddd',
        }}>
          <Flex.Item style={{
            flexGrow: '0.2',
            textAlign: 'center'
          }}>
            <img src={touxiang} style={{width: 40, height: 40}} />
          </Flex.Item>
          <Flex.Item>
            <div className={styles.userName} style={{}}>
              {`Hi,${this.hiddenName(userInfo.userName)}`}
            </div>
            <div className={styles.userId}>
              {`UID:${userInfo.userId}`}
            </div>
          </Flex.Item>
        </Flex>
      </div>
    );
  }
}

export default MineTop;
