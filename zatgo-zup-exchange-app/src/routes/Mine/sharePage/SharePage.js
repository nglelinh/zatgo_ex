/**
 * Created by zhoujianxin on 2018/11/28.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import {Flex, NavBar, Icon, Button} from 'antd-mobile';
import {DocumentTitle} from 'components';
import intl from 'react-intl-universal';
import CopyToClipboard from 'react-copy-to-clipboard';
import DisplayView from './DisplayView';
import secret from '../../../constants/secret';

class SharePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copied: false
    };
  }

  componentDidMount() {

  }

  render() {
    const {goBack, userInfo} = this.props;
    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div style={{ display: 'flex', flex: 1, height: '100%', flexDirection: 'column', }}>
          <div style={{overflow: 'auto', height: '100%', }}>
            <NavBar
              mode="light"
              icon={
                <Flex direction="row" align="center">
                  <Icon type="left" color={'black'} />
                </Flex>
              }
              onLeftClick={goBack}
            />
            <div style={{display: 'flex', flexDirection: 'column', margin: 15}}>
              <div style={{fontSize: 24, fontWeight: 'bold', color: '#3d81cf'}}>分享邀请</div>
            </div>

            <div style={{ width: '300'}}>
              <DisplayView />
              <div style={{ display: 'flex', flexDirection: 'column', flex: 1, alignItems: 'center', justifyContent: 'center', height: 40}} />
            </div>
          </div>

          <div style={{width: '100%', height: 5, backgroundColor: '#d3d3d3'}} />
          <div style={styles.button}>
            <div style={{display: 'flex', flex: 1, flexDirection: 'row', margin: 10, alignItems: 'center'}}>
              <div style={{display: 'flex', flex: 1, alignItems: 'center', }}>
                <div style={{ fontSize: 18}}>邀请码：
                  <font style={{color: '#3d81cf', fontWeight: 'bold'}}>
                    {userInfo.inviteCode ? userInfo.inviteCode : ''}
                  </font>
                </div>
              </div>
              <CopyToClipboard text={'234566'}
                onCopy={() => this.setState({copied: true})}>
                <Button onClick={() => {}} size="small" style={{fontSize: 18}}>复制</Button>
              </CopyToClipboard>
            </div>
            <Button style={{color: 'white', backgroundColor: '#3d81cf'}}onClick={() => console.log(secret.Encrypt('你好'))}>马上邀请好友</Button>
          </div>

        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.mine.userInfo,
});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SharePage);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    height: 30,
    justifyContent: 'space-between',
    paddingTop: 5,
    paddingBottom: 5,
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    height: 200,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 20
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
