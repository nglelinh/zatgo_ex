/**
 * Created by zhoujianxin on 2018/11/28.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import {WingBlank} from 'antd-mobile';


class DisplayView extends Component {
  state = {
    data: ['1'],
    imgHeight: 800,
  }

  componentDidMount() {
    // simulate img loading
    setTimeout(() => {
      this.setState({
        data: ['AiyWuByWklrrUDlFignR'],
      });
    }, 100);
  }

  render() {
    return (
      <WingBlank>
        <div style={{...styles.container, height: this.state.imgHeight }}>
          <img
            src={require('../../../assets/11.png')}
            alt=""
            style={{
              width: '100%',
              height: this.state.imgHeight,
              borderStyle: 'solid',
              borderRadius: 8,
              borderWidth: 0
            }}
          />
        </div>
      </WingBlank>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(DisplayView);

const styles = {
  container: {
    display: 'flex',
    width: '100%',

    borderStyle: 'solid',
    borderRadius: 8,
    borderWidth: 0
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#E26A6A',
    height: 30,
    width: 65,
    justifyContent: 'center',
    alignItems: 'center',
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
