/**
 * Created by zhoujianxin on 2018/11/27.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';


class SwitchView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from: '币币账户',
      to: '',
      type: true, // true:币币账户到杠杆 fale：杠杆到币币
    };
  }

  componentDidMount() {
    this.setState({
      to: `${this.props.title ? this.props.title : ''}杠杆账户`
    });
  }

  render() {
    return (
      <div style={styles.container}>
        <div style={{display: 'flex', marginRight: 10, marginLeft: 10, alignItems: 'center', flexDirection: 'column'}}>
          <div style={{flex: 1, width: 0.5}} />
          <div style={{backgroundColor: '#3d81cf', width: 10, height: 10, borderStyle: 'solid', borderRadius: 5, borderWidth: 0}} />
          <div style={{flex: 1, width: 0.5, backgroundColor: '#d0d0d0'}} />
          <div style={{backgroundColor: '#cf7016', width: 10, height: 10, borderStyle: 'solid', borderRadius: 5, borderWidth: 0}} />
          <div style={{flex: 1, width: 0.5}} />
        </div>
        <div style={{display: 'flex', flex: 3, flexDirection: 'column'}}>
          <div style={{ margin: 10, marginLeft: 0, color: '#36366e', fontSize: 14}}>
            <font style={{ color: '#d0d0d0', marginRight: 10}}>从 </font>{this.state.from}
          </div>
          <div style={{borderStyle: 'solid', borderColor: '#d0d0d0', borderWidth: 0.5}} />
          <div style={{ margin: 10, marginLeft: 0, color: '#36366e', fontSize: 14}}>
            <font style={{ color: '#d0d0d0', marginRight: 10}}>到 </font>{this.state.to}
          </div>
        </div>
        <div style={{display: 'flex', flex: 1, width: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: '#f4f4f4'}}
          onClick={() => {
            let item = this.state.from;
            this.setState({
              from: this.state.to,
              to: item,
              type: !this.state.type
            });
            this.props.onSwitch && this.props.onSwitch(!this.state.type);
          }}
        >
          <img style={{width: 20, height: 20}} src={require('../../../assets/mine/mine_change.svg')} alt="" />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(SwitchView);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#d0d0d0'
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#E26A6A',
    height: 30,
    width: 65,
    justifyContent: 'center',
    alignItems: 'center',
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
