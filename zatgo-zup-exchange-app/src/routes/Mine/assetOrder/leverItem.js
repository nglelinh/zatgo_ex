/**
 * Created by zhoujianxin on 2018/12/5.
 * @Desc
 */
// import intl from 'react-intl-universal';
import {Flex} from 'antd-mobile';
import dayjs from 'dayjs';

const data = {
  '0': '未审批', '1': '已通过', '2': '已拒绝', '3': '已借贷', '4': '已还款', '5': '爆仓还款', '6': '还款失败'
};
const leverItem   = (props) => {
  const {itemInfo, onItemClick} = props;
  // console.log(itemInfo);
  return (
    <div style={styles.container}>
      <Flex style={{marginTop: 5}}>
        <Flex.Item style={{color: '#C4C8CB'}}>创建时间</Flex.Item>
        <Flex.Item style={{color: '#C4C8CB'}}>币种</Flex.Item>
        <Flex.Item style={{color: '#C4C8CB'}}>价格</Flex.Item>
        <Flex.Item style={{textAlign: 'right', color: '#C4C8CB'}}>状态</Flex.Item>
      </Flex>

      <Flex style={{marginTop: 5, marginBottom: 5, alignItems: 'center'}}>
        <Flex.Item style={{color: '#C4C8CB'}}>{itemInfo.ctime ? dayjs(new Date(itemInfo.ctime)).format('YYYY-MM-DD HH:mm:ss') : ''}</Flex.Item>
        <Flex.Item style={{color: '#C4C8CB'}}>{itemInfo.coin.toUpperCase()}</Flex.Item>
        <Flex.Item style={{color: '#C4C8CB'}}>{itemInfo.amount}</Flex.Item>
        <Flex.Item style={{textAlign: 'right', color: '#C4C8CB'}}>
          {itemInfo.type ? itemInfo.type === 1 ? '转入' : '转出' : data[itemInfo.status] ? data[itemInfo.status] : ''}
        </Flex.Item>
      </Flex>
      {itemInfo.type ?
        null :
        <Flex>
          <Flex.Item style={{color: '#C4C8CB'}}>操作：</Flex.Item>
          {itemInfo.status === 3 ?
            <div>
              <div style={styles.inputContainer} onClick={() => { onItemClick && onItemClick(itemInfo.id) }}>
                <div style={styles.input}>还款</div>
              </div>
            </div>
            : null
          }
        </Flex>
      }
    </div>
  );
};

export default leverItem;

const styles = {
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: '5px 10px 5px 10px',
    // alignItems: 'center',
    backgroundColor: '#e8f1f7',
    marginBottom: 5,
    margin: 10
  },
  input: {
    display: 'flex',
    flex: 1,
    fontSize: 14,
    marginLeft: 8,
    marginRight: 8,
    border: 'none',
    outline: 'medium',
    autoComplete: 'off',
    backgroundColor: 'white',
  },
  inputContainer: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'start',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 20,
    position: 'relative',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderRadius: 6,
  },
  font11: {
    color: '#797F85', fontSize: 11,
  },
  font16: {
    color: '#323B43',
    fontSize: 16,
  }
};
