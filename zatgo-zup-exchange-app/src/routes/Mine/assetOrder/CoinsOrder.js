/**
 * Created by zhoujianxin on 2018/11/19.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import intl from 'react-intl-universal';
import {Flex, NavBar, Icon, Picker} from 'antd-mobile';
import {DocumentTitle, ListView} from 'components';
import CoinsItem from './CoinsItem';

const defaultData = [
  {
    icon: require('../../../assets/mine/mine_order.svg'),
    text: '充币',
    isOpen: false,
  },
  {
    icon: require('../../../assets/mine/mine_order.svg'),
    text: `提币`,
    isOpen: false,
  },
  {
    icon: require('../../../assets/mine/mine_jiaoyi.svg'),
    text: `交易`,
    isOpen: true,
  }
];
let pickData = [
  {label: '提币', value: '19'},
  {label: '提币未确认', value: '10' },
  {label: '提币已完成', value: '11'},
  {label: '提币异常', value: '12'},
  {label: '充值', value: '29' },
  {label: '充值未确认', value: '20' },
  {label: '充值已完成', value: '21' },
  {label: '充值异常', value: '22' }
];

let paramsData = {
  pageNum: 1,
  pageSize: 10,
  coinSymbol: null,
  status: null
};

class CoinsOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pickerValue: [],
      value: '',
      listState: true,
      coinsData: {},
      data: []
    };
  }

  componentDidMount() {
    this.setState({
      pickerValue: [pickData[0].value],
      value: pickData[0].label,
      index: '10'
    });
    this.getOrderList();
  }

  componentWillUnmount() {
    paramsData = {pageNum: 1, pageSize: 10, coinSymbol: null, status: null };
  }

  /**/
  getOrderList = () => {
    console.log(paramsData);
    // 提币
    if (this.state.listState) {
      console.log('提币');
      this.props.getCoinWithdrawList(paramsData);
    } else {
      // 充值
      console.log('充值');
      this.props.getCoinDepositList(paramsData);
    }
  }

  // 加载更多
  getData=() => {
    paramsData['pageNum'] = paramsData.pageNum + 1;
    // 提币
    if (this.state.listState) {
      console.log('提币');
      this.props.getCoinWithdrawList(paramsData);
    } else {
      // 充值
      console.log('充值');
      this.props.getCoinDepositList(paramsData);
    }
  }

  onClick = (index) => {
    console.log('23');
    const {changeUrl} = this.props;
    changeUrl({ pathname: '/deal', state: { quoteCoin: 'usdt', baseCoin: paramsData['coinSymbol'] } });

  }

  onChange = (e) => {
    console.log(e);
    let that = this;
    for (let i = 0; i < pickData.length; i++) {
      if (pickData[i].value === e[0]) {
        this.setState({
          pickerValue: e,
          value: [pickData[i].label],
          listState: Number(e[0]) < 20
        },
          // eslint-disable-next-line
        () => {
          paramsData['pageNum'] = 1;
          if (this.state.listState) {
            paramsData['status'] = Number(e[0]) % 10 !== 9 ? Number(e[0]) % 10 : null;
          } else {
            paramsData['status'] = Number(e[0]) % 10 !== 9 ? Number(e[0]) % 10 : null;
          }
          that.getOrderList();
        });
      }
    }
  }

  onItemClick= (info) => {
    // alert(JSON.stringify(info));
  }

  render() {
    const {goBack, coinsData, coinWithdrawList, coinDepositList, withdrawLoadMore, depositLoadMore } = this.props;
    paramsData['coinSymbol'] = coinsData.coin;
    let arrData = this.state.listState ? coinWithdrawList : coinDepositList;
    let noMore = this.state.listState ? withdrawLoadMore : depositLoadMore;
    return (
      <DocumentTitle title={intl.get('title.coins')}>
        <div style={{display: 'flex', flexDirection: 'column', height: '100%' }}>
          <div>
            <NavBar
              mode="light"
              icon={
                <Flex direction="row" align="center">
                  <Icon type="left" color={'black'} />
                </Flex>
              }
              onLeftClick={goBack}
            />
          </div>

          <div style={{display: 'flex', flexDirection: 'column', margin: 15}}>
            <div style={{fontSize: 24, fontWeight: 'bold', color: '#3d81cf'}}>
              {JSON.stringify(coinsData) !== '{}' ? (coinsData.quoteCoin).toUpperCase() : ''}
            </div>

            <Flex style={{marginTop: 18}}>
              <Flex.Item style={{color: '#C4C8CB'}}>可用</Flex.Item>
              <Flex.Item style={{color: '#C4C8CB'}}>冻结</Flex.Item>
              <Flex.Item style={{textAlign: 'right', color: '#C4C8CB'}}>折合(CNY)</Flex.Item>
            </Flex>

            <Flex style={{marginTop: 10}}>
              <Flex.Item>{coinsData && coinsData.normal ? coinsData.normal : '0.000000'}</Flex.Item>
              <Flex.Item>{coinsData && coinsData.locked ? coinsData.locked : '0.000000'}</Flex.Item>
              <Flex.Item style={{textAlign: 'right'}}>{coinsData && coinsData.CNY ? coinsData.CNY : '0.00'}</Flex.Item>
            </Flex>
          </div>
          <div style={{width: '100%', height: 15, backgroundColor: '#f7f6fa'}} />

          <Flex style={{margin: 15}}>
            <div style={{color: '#273f57', fontSize: 24, fontWeight: 'bold', flex: 1}}>财务记录</div>
            <Picker
              data={pickData}
              value={this.state.pickerValue}
              cols={1}
              onChange={this.onChange}
            >
              <div style={{display: 'flex', flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                <img
                  src={require('../../../assets/mine/mine_xuanze.svg')}
                  style={{ width: '20px', height: '20px', marginRight: 5 }}
                />
                <div>{this.state.value}</div>
              </div>
            </Picker>
          </Flex>
          <div style={{display: 'flex', flex: 1}}>
            {arrData.length > 0 ? <div style={{ position: 'relative', backgroundColor: 'white', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
              <ListView
                data={arrData}
                ListItem={CoinsItem}
                onItemClick={this.onItemClick}
                offsetHeight={100}
                getData={this.getData}
                disableLoadMore={!noMore}
              />
            </div> :
            <div style={{display: 'flex', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <img
                src={require('../../../assets/mine/mine_order.svg')}
                style={{ width: '60px', height: '60px' }}
              />
            </div>
            }
            {coinsData.coin && coinsData.coin.toUpperCase() !== 'USDT' ? <div style={styles.button}>
              <div
                onClick={() => {
                  this.onClick();
                }}
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <img src={defaultData[2].icon} style={{width: '30px', height: '30px' }} alt="" />
                <div style={{
                  color: '#3d81cf',
                  fontSize: '14px',
                  textAlign: 'center'
                }}>{defaultData[2].text}</div>
              </div>
            </div> : null}
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({
  coinsData: state.native.coinsData,
  coinWithdrawList: state.asset.coinWithdrawList,
  withdrawLoadMore: state.asset.withdrawLoadMore,

  coinDepositList: state.asset.coinDepositList,
  depositLoadMore: state.asset.depositLoadMore,
});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
  getCoinWithdrawList: (payload) => {
    dispatch({type: 'asset/getCoinWithdrawList', payload: payload});
  },
  getCoinDepositList: (payload) => {
    dispatch({type: 'asset/getCoinDepositList', payload: payload});
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CoinsOrder);

const styles = {
  button: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',
    position: 'fixed',
    right: 20,
    bottom: 20,

  },
};
