/**
 * Created by zhoujianxin on 2018/11/26.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import {NavBar, Flex, Icon, Button} from 'antd-mobile';
import {DocumentTitle} from 'components';
import intl from 'react-intl-universal';
import numberDeal from 'constants/numberDeal';

class LeverBorrow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectBtn: true,
      leftBtnText: '',
      leftData: {},
      rightBtnText: '',
      rightData: {},
      leftInput: '',
      rightInput: '',
    };
  }

  componentDidMount() {
    const { state: {leverageData, baseCoin, quoteCoin } } = this.props.location;
    console.log(leverageData);
    this.setState({
      leftBtnText: quoteCoin ? quoteCoin.toUpperCase() : '',
      leftData: leverageData && leverageData.coinData && leverageData.coinData.quoteCoin ? leverageData.coinData.quoteCoin : {'loan': '', 'normal': ''},
      rightBtnText: baseCoin ? baseCoin.toUpperCase() : '',
      rightData: leverageData && leverageData.coinData && leverageData.coinData.baseCoin ? leverageData.coinData.baseCoin : {'loan': '', 'normal': ''},
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    const {leverAssetList } = nextProps;
    const {leverageData} = this.props;
    if (this.props.leverAssetList !== leverAssetList) {
      leverAssetList.map((item, index) => {
        console.log(item);
        if (item.symbol === leverageData.symbol) {
          this.props.setLeverageData(item);
          this.setState({
            leftData: item.coinData && item.coinData['quoteCoin'] ? item.coinData['quoteCoin'] : {'loan': '', 'normal': ''},
            rightData: item.coinData && item.coinData['baseCoin'] ? item.coinData['baseCoin'] : {'loan': '', 'normal': ''},
          });
        }
        return null;
      });

    }
  }

  onSelectBtn=(item) => {
    this.setState({
      selectBtn: item,
      leftInput: '',
      rightInput: ''
    });
  }

  handleChange=(e) => {
    let regex = new RegExp('(^[0-9]{1,8}$)|(^[0-9]{1,8}[\.]{1}[0-9]{1,8}$)');
    if (regex.test(Number(e.target.value))) {
      if (this.state.selectBtn) {
        this.setState({
          leftInput: e.target.value
        });
      } else {
        this.setState({
          rightInput: e.target.value
        });
      }
    }
  }

  selseAll=() => {
    if (this.state.selectBtn) {
      this.setState({
        leftInput: '203'
      });
    } else {
      this.setState({
        rightInput: '9340'
      });
    }
  }

  submit=(symbol) => {
    let coin = this.state.selectBtn ? this.state.leftBtnText : this.state.rightBtnText;
    let amount = this.state.selectBtn ? this.state.leftInput : this.state.rightInput;
    console.log(symbol);
    console.log(coin);
    console.log(amount);
    this.props.appleLeverBorrow({
      'symbol': symbol ? symbol.toLowerCase() : null,
      'coin': coin ? coin.toLowerCase() : null,
      'amount': amount
    });
  }

  render() {
    const {goBack} = this.props;
    let symbol = this.state.rightBtnText + this.state.leftBtnText;

    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div>
          <div style={{ flexDirection: 'column', overflow: 'auto', flex: 'auto' }}>
            <div>
              <NavBar
                mode="light"
                icon={
                  <Flex direction="row" align="center">
                    <Icon type="left" color={'black'} />
                  </Flex>
                }
                onLeftClick={goBack}
              />
            </div>
            <div style={{display: 'flex', flexDirection: 'column', margin: 15}}>
              <div style={{fontSize: 24, fontWeight: 'bold', color: '#3d81cf'}}>借贷</div>
            </div>

            <div style={{display: 'flex', flexDirection: 'column', margin: 15}}>
              <Flex style={{flex: 1, justifyContent: 'center', }}>
                <Button style={{
                  flex: 1,
                  color: this.state.selectBtn ? 'white' : '#d0d0d0',
                  backgroundColor: this.state.selectBtn ? '#3d81cf' : 'white'
                }} onClick={() => { this.onSelectBtn(true) }}
                >{this.state.leftBtnText}</Button>
                <div style={{width: '5%'}} />
                <Button style={{
                  flex: 1,
                  color: !this.state.selectBtn ? 'white' : '#d0d0d0',
                  backgroundColor: !this.state.selectBtn ? '#3d81cf' : 'white'
                }} onClick={() => { this.onSelectBtn(false) }}
                >{this.state.rightBtnText}</Button>
              </Flex>

              <div style={{backgroundColor: '#f2f2f2', padding: 15, marginTop: 15}}>
                <Flex style={{}}>
                  <div style={{color: '#C4C8CB'}}>已借</div>
                  <Flex.Item style={{textAlign: 'right'}}>
                    {this.state.selectBtn ? this.state.leftData.loan : this.state.rightData.loan}
                    {this.state.selectBtn ? ' ' + this.state.leftBtnText : ' ' + this.state.rightBtnText}
                  </Flex.Item>
                </Flex>

                <Flex style={{marginTop: 15}}>
                  <div style={{color: '#aeb2b5'}}>最大额度</div>
                  <Flex.Item style={{textAlign: 'right'}}>
                    {this.state.selectBtn ? numberDeal.numSub(Number(this.state.leftData.normal), Number(this.state.leftData.loan))
                    : numberDeal.numSub(Number(this.state.rightData.normal), Number(this.state.rightData.loan))}
                    {this.state.selectBtn ? ' ' + this.state.leftBtnText : ' ' + this.state.rightBtnText}
                  </Flex.Item>
                </Flex>

                <Flex style={{marginTop: 15}}>
                  <div style={{color: '#aeb2b5'}}>利率</div>
                  <Flex.Item style={{textAlign: 'right'}}>
                    {this.state.selectBtn ? this.state.leftData.rate || 0 : this.state.rightData.rate || 0}%
                  </Flex.Item>
                </Flex>
              </div>

              <div style={{display: 'flex', flexDirection: 'column', marginTop: 15}}>
                <div style={{marginBottom: 15}}>数量</div>
                <Flex style={{borderBottomStyle: 'solid', borderBottomWidth: 0.1, borderBottomColor: '#aeb2b5', paddingBottom: 10}}>
                  <input
                    value={this.state.selectBtn ? this.state.leftInput : this.state.rightInput}
                    onChange={this.handleChange}
                    style={{flex: 1, borderStyle: 'solid', borderWidth: 0}}
                    placeholder="最少0.150"
                    pointerEvents={'none'}
                  />
                  <div style={{color: '#aeb2b5'}}>{this.state.selectBtn ? this.state.leftBtnText : this.state.rightBtnText}</div>
                  <div style={{width: 1, height: 15, backgroundColor: '#aeb2b5', marginLeft: 10, marginRight: 10}} />
                  <div style={{}} onClick={() => { this.selseAll() }}>全部</div>
                </Flex>
                <div style={{marginTop: 15, color: '#aeb2b5'}}>可借
                  {this.state.selectBtn ? numberDeal.numSub(Number(this.state.leftData.normal), 2 * Number(this.state.leftData.loan))
                  : numberDeal.numSub(Number(this.state.rightData.normal), 2 * Number(this.state.rightData.loan))}
                  {this.state.selectBtn ? this.state.leftBtnText : this.state.rightBtnText}</div>
              </div>
            </div>
          </div>
          <div style={styles.button}>
            <Button
              style={{
                width: '100%',
                backgroundColor: this.state.leftInput.length > 0 || this.state.rightInput.length > 0 ? '#3d81cf' : '#aeb2b5',
                color: 'white'
              }}
              onClick={() => {
                if (this.state.leftInput.length > 0 || this.state.rightInput.length > 0) {
                  this.submit(symbol);
                }
              }}>
              借贷</Button>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({
  leverageData: state.native.leverageData,
  leverAssetList: state.mine.leverAssetList,

});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
  appleLeverBorrow: (payload) => {
    dispatch({type: 'asset/appleLeverBorrow', payload: payload});
  },
  setLeverageData: (payload) => {
    dispatch({type: 'native/setLeverageData', payload: payload});
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LeverBorrow);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    height: 30,
    justifyContent: 'space-between',
    paddingTop: 5,
    paddingBottom: 5,
  },
  button: {
    position: 'fixed',
    left: 0,
    right: 0,
    bottom: 5,
    marginLeft: 15,
    marginRight: 15
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
