/**
 * Created by zhoujianxin on 2018/11/19.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import intl from 'react-intl-universal';
import {Flex, NavBar, Icon, Picker, Progress} from 'antd-mobile';
import {DocumentTitle, ListView} from 'components';
import leverItem from './leverItem';

const data = [
  {
    icon: require('../../../assets/mine/mine_jiedai.svg'),
    text: '借贷',
  },
  {
    icon: require('../../../assets/mine/mine_huazhuan.svg'),
    text: `划转`,
  },
  {
    icon: require('../../../assets/mine/mine_jiaoyi.svg'),
    text: `交易`,
  }
];
const pickData = [
  {label: '划转-全部', value: '19', },
  {label: '划转-转入', value: '11', },
  {label: '划转-转出', value: '12', },
  {label: '借贷-全部', value: '109', },
  {label: '借贷-未审批', value: '20', },
  {label: '借贷-已通过', value: '21', },
  {label: '借贷-已拒绝', value: '22', },
  {label: '借贷-已借贷', value: '23', },
  {label: '借贷-已还款', value: '24', },
  {label: '借贷-爆仓还款', value: '25', },
  {label: '借贷-还款失败', value: '26', },
];

let paramsData = {
  pageNum: 1,
  symbol: null,
  coin: null,
  type: null,
  status: null

};

class CoinsOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pickerValue: [],
      value: '',
      btnState: 2,
      listState: true,
    };
  }

  componentDidMount() {
    this.setState({
      pickerValue: [pickData[0].value],
      value: pickData[0].label,
      listState: true
    });
    this.getOrderList();
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    const {leverAssetList } = nextProps;
    const {leverageData} = this.props;
    if (this.props.leverAssetList !== leverAssetList) {
      leverAssetList.map((item, index) => {
        console.log(item);
        if (item.symbol === leverageData.symbol) {
          this.props.setLeverageData(item);
        }
        return null;
      });

    }
  }

  componentWillUnmount() {
    paramsData = {pageNum: 1, symbol: null, coin: null, type: null, status: null, };
  }

  getOrderList = (index) => {
    if (this.state.listState) {
      let params = {
        pageNum: paramsData.pageNum,
        pageSize: 10,
        symbol: paramsData.symbol,
        coin: paramsData.coin,
        type: paramsData.type
      };
      console.log('转出');
      this.props.getLeverTransferList(params);
    } else {
      let params = {
        pageNum: paramsData.pageNum,
        pageSize: 10,
        symbol: paramsData.symbol,
        coin: paramsData.coin,
        status: paramsData.status
      };
      console.log('转入');
      this.props.getLeverBorrowList(params);
    }
  }

  // 加载更多
  getData=() => {
    paramsData['pageNum'] = paramsData.pageNum + 1;
    if (this.state.listState) {
      let params = {
        pageNum: paramsData.pageNum,
        pageSize: 10,
        symbol: paramsData.symbol,
        coin: paramsData.coin,
        type: paramsData.type
      };
      console.log('转出');
      this.props.getLeverTransferList(params);
    } else {
      let params = {
        pageNum: paramsData.pageNum,
        pageSize: 10,
        symbol: paramsData.symbol,
        coin: paramsData.coin,
        status: paramsData.status
      };
      console.log('转入');
      this.props.getLeverBorrowList(params);
    }
  }

  onChange=(e) => {
    let that = this;
    for (let i = 0; i < pickData.length; i++) {
      if (pickData[i].value === e[0]) {
        this.setState({
          pickerValue: e,
          value: [pickData[i].label],
          listState: Number(e[0]) < 20
        },
          // eslint-disable-next-line
          () => {
          paramsData['pageNum'] = 1;
          if (that.state.listState) {
            paramsData['type'] = Number(e[0]) % 10 === 9 ? null : Number(e[0]) % 10;
          } else {
            paramsData['status'] = Number(e[0]) % 10 === 9 ? null : Number(e[0]) % 10;
          }
          that.getOrderList();
        });
        console.log('22222');
      }
    }
  }

  pushPage=(index) => {
    const { changeUrl, leverageData} = this.props;
    let baseCoin = leverageData.coinData && leverageData.coinData['baseCoin'] ? leverageData.coinData['baseCoin'] : {'coin': '--'};
    let quoteCoin = leverageData.coinData && leverageData.coinData['quoteCoin'] ? leverageData.coinData['quoteCoin'] : {'coin': '--'};
    if (index === 0) {
      const path = {
        pathname: '/leverBorrow',
        state: {leverageData, baseCoin: baseCoin.coin, quoteCoin: quoteCoin.coin}
      };
      changeUrl(path);
    } else if (index === 1) {
      const path = {
        pathname: '/leverTransfer',
        state: {coinNum: {baseCoinNum: baseCoin.normal, quoteCoinNum: quoteCoin.normal}, baseCoin: baseCoin.coin, quoteCoin: quoteCoin.coin}
      };
      changeUrl(path);
    } else if (index === 2) {
      changeUrl({ pathname: '/deal', state: { quoteCoin: quoteCoin.coin, baseCoin: baseCoin.coin } });
    }
  }

  onItemClick= (id) => {
    alert('', intl.get('mine.mag'), [
      { text: intl.get('mine.cancel'), onPress: () => { } },
      {
        text: intl.get('mine.ok'), onPress: () => { this.props.leverBorrowRepayment({loanId: id});
        }
      },
    ]);
  }

  render() {
    const {goBack, leverageData, leverBorrowList, borrowListLoadMore, leverTransferList, transferListLoadMore } = this.props;
    paramsData['symbol'] = leverageData['symbol'];
    let baseCoin = leverageData.coinData && leverageData.coinData['baseCoin'] ? leverageData.coinData['baseCoin'] : {'coin': '--'};
    let quoteCoin = leverageData.coinData && leverageData.coinData['quoteCoin'] ? leverageData.coinData['quoteCoin'] : {'coin': '--'};
    let arrData = this.state.listState ? leverTransferList : leverBorrowList;
    let noMore = this.state.listState ? transferListLoadMore : borrowListLoadMore;
    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div style={{display: 'flex', flexDirection: 'column', height: '100%' }}>
          <div>
            <NavBar
              mode="light"
              icon={
                <Flex direction="row" align="center">
                  <Icon type="left" color={'black'} />
                </Flex>
              }
              onLeftClick={goBack}
            />
          </div>

          <div style={{display: 'flex', flexDirection: 'column', margin: 15}}>
            <div style={{fontSize: 24, fontWeight: 'bold', color: '#373d4f', marginBottom: 15}}>
              {(`${baseCoin.coin}/${quoteCoin.coin}`).toUpperCase()}
            </div>

            <div style={{display: 'flex', flexDirection: 'column', marginTop: 15}}>
              <div style={{color: '#C4C8CB', fontSize: 14}}>风险率 20%</div>
              <Progress style={{height: 10, marginTop: 10}} barStyle={{borderTopWidth: 5, borderBottomWidth: 5}} percent={20} position="normal" />
            </div>

            <Flex style={{marginTop: 18}}>
              <Flex.Item style={{color: '#C4C8CB'}}>类型</Flex.Item>
              <Flex.Item style={{color: '#C4C8CB'}}>{baseCoin.coin.toUpperCase()}</Flex.Item>
              <Flex.Item style={{textAlign: 'right', color: '#C4C8CB'}}>{quoteCoin.coin.toUpperCase()}</Flex.Item>
            </Flex>

            <Flex style={{marginTop: 10}}>
              <Flex.Item style={{color: '#aeb2b5'}}>可用</Flex.Item>
              <Flex.Item>{baseCoin.normal ? baseCoin.normal : '0.000000'}</Flex.Item>
              <Flex.Item style={{textAlign: 'right'}}>{quoteCoin.normal ? quoteCoin.normal : '0.000000'}</Flex.Item>
            </Flex>

            <Flex style={{marginTop: 10}}>
              <Flex.Item style={{color: '#aeb2b5'}}>冻结</Flex.Item>
              <Flex.Item>{baseCoin.locked ? baseCoin.locked : '0.00000'}</Flex.Item>
              <Flex.Item style={{textAlign: 'right'}}>{quoteCoin.locked ? quoteCoin.locked : '0.000000'}</Flex.Item>
            </Flex>

            <Flex style={{marginTop: 10}}>
              <Flex.Item style={{color: '#aeb2b5'}}>已借</Flex.Item>
              <Flex.Item>{baseCoin.loan ? baseCoin.loan : '0.00000'}</Flex.Item>
              <Flex.Item style={{textAlign: 'right'}}>{quoteCoin.loan ? quoteCoin.loan : '0.000000'}</Flex.Item>
            </Flex>
            <div style={{color: '#aeb2b5', fontSize: 14, marginTop: 20}}>折合{leverageData.CNY}CNY</div>
          </div>

          <div style={{width: '100%', height: 15, backgroundColor: '#f7f6fa'}} />

          <Flex style={{margin: 15}}>
            <div style={{color: '#273f57', fontSize: 24, fontWeight: 'bold', flex: 1}}>财务记录</div>
            <Picker
              data={pickData}
              value={this.state.pickerValue}
              cols={1}
              onChange={this.onChange}
            >
              <div style={{display: 'flex', flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                <img
                  src={require('../../../assets/mine/mine_xuanze.svg')}
                  style={{ width: '20px', height: '20px', marginRight: 5 }}
                />
                <div>{this.state.value}</div>
              </div>
            </Picker>
          </Flex>
          <div style={{display: 'flex', flex: 1}}>
            {arrData.length > 0 ?
              <div style={{flex: 1, backgroundColor: 'white', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                <ListView
                  data={arrData}
                  ListItem={leverItem}
                  onItemClick={this.onItemClick}
                  offsetHeight={100}
                  getData={this.getData}
                  disableLoadMore={!noMore}
                />
              </div> :
              <div style={{display: 'flex', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <img
                  src={require('../../../assets/mine/mine_order.svg')}
                  style={{ width: '60px', height: '60px' }}
                />
              </div>
            }
            <div style={styles.button}>
              {data.map((item, index) =>
                (<div key={index}
                  onClick={() => {
                    this.setState({btnState: index});
                    this.pushPage(index);
                  }}
                  style={{
                    marginLeft: index === 1 ? '20%' : 0,
                    marginRight: index === 1 ? '20%' : 0,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <img src={item.icon} style={{width: '30px', height: '30px' }} alt="" />
                  <div style={{ color: index === this.state.btnState ? '#3d81cf' : '#888', fontSize: '14px', textAlign: 'center' }}>{item.text}</div>
                </div>)
              )}
            </div>
          </div>
        </div>

      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({
  leverageData: state.native.leverageData,
  leverAssetList: state.mine.leverAssetList,

  leverBorrowList: state.asset.leverBorrowList,
  borrowListLoadMore: state.asset.borrowListLoadMore,

  leverTransferList: state.asset.leverTransferList,
  transferListLoadMore: state.asset.transferListLoadMore,

});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
  getLeverBorrowList: (payload) => {
    dispatch({type: 'asset/getLeverBorrowList', payload: payload});
  },
  getLeverTransferList: (payload) => {
    dispatch({type: 'asset/getLeverTransferList', payload: payload});
  },
  leverBorrowRepayment: (payload) => {
    dispatch({type: 'asset/leverBorrowRepayment', payload: payload});
  },
  setLeverageData: (payload) => {
    dispatch({type: 'native/setLeverageData', payload: payload});
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CoinsOrder);

const styles = {
//   container: {
//     display: 'flex',
//     flexDirection: 'row',
//     height: 30,
//     justifyContent: 'space-between',
//     paddingTop: 5,
//     paddingBottom: 5,
//   },
  button: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',
    position: 'fixed',
    left: 0,
    right: 0,
    bottom: 10,

  },
//   font11: {
//     color: '#797F85',
//     fontSize: 11,
//     textAlign: 'center'
//   },
//   font16: {
//     fontSize: 16
//   }
};
