/**
 * Created by zhoujianxin on 2018/12/5.
 * @Desc
 */
// import intl from 'react-intl-universal';
import {Flex} from 'antd-mobile';
import dayjs from 'dayjs';

const data = {
  '0': '未确认', '1': '已完成', '2': '异常'
};

const CoinsItem   = (props) => {
  const {itemInfo, onItemClick} = props;
  return (
    <div style={styles.container} onClick={() => onItemClick()}>

      <Flex style={{marginTop: 5}}>
        <Flex.Item style={{color: '#C4C8CB'}}>创建时间</Flex.Item>
        <Flex.Item style={{color: '#C4C8CB'}}>数量</Flex.Item>
        <Flex.Item style={{color: '#C4C8CB'}}>价格</Flex.Item>
        <Flex.Item style={{textAlign: 'right', color: '#C4C8CB'}}>状态</Flex.Item>
      </Flex>

      <Flex style={{marginTop: 5, alignItems: 'flex-end'}}>
        <Flex.Item style={{color: '#C4C8CB'}}>{dayjs(new Date(itemInfo.extractDate)).format('YYYY-MM-DD HH:mm:ss')}</Flex.Item>
        <Flex.Item style={{color: '#C4C8CB'}}>{itemInfo.extractNumber}</Flex.Item>
        <Flex.Item style={{color: '#C4C8CB'}}>{itemInfo.fee}</Flex.Item>
        <Flex.Item style={{textAlign: 'right', color: '#C4C8CB'}}>{data[itemInfo.extractStatus] ? data[itemInfo.extractStatus] : ''}</Flex.Item>
      </Flex>

    </div>
  );
};

export default CoinsItem;

const styles = {
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: '5px 10px 5px 10px',
    // alignItems: 'center',
    backgroundColor: '#e8f1f7',
    marginBottom: 5,
    margin: 10
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    height: 30,
    width: 70,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  font11: {
    color: '#797F85', fontSize: 11,
  },
  font16: {
    color: '#323B43',
    fontSize: 16,
  }
};
