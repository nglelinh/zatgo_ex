/**
 * Created by zhoujianxin on 2018/11/26.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import {NavBar, Flex, Icon, Picker, Button} from 'antd-mobile';
import {DocumentTitle} from 'components';
import intl from 'react-intl-universal';
import SwitchView from '../component/SwitchView';

let data = [];
let coinNum = '';
class LeverTransfer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coinsName: [],
      leverageNum: '',
      quoteCoinNum: '',
      baseCoinNum: '',
      type: true,
    };
  }

  componentDidMount() {
    const { state: { baseCoin, quoteCoin, coinNum } } = this.props.location;
    data = [
      {
        label: quoteCoin.toUpperCase(),
        value: quoteCoin.toUpperCase(),
      }, {
        label: baseCoin.toUpperCase(),
        value: baseCoin.toUpperCase(),
      }
    ];
    this.setState({
      coinsName: quoteCoin ? [quoteCoin.toUpperCase()] : '',
      quoteCoinNum: coinNum.quoteCoinNum ? coinNum.quoteCoinNum : '0.00',
      baseCoinNum: coinNum.baseCoinNum ? coinNum.baseCoinNum : '0.00',
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    const {leverAssetList } = nextProps;
    const {leverageData} = this.props;
    if (this.props.leverAssetList !== leverAssetList) {
      leverAssetList.map((item, index) => {
        if (item.symbol === leverageData.symbol) {
          let baseCoin = item.coinData && item.coinData['baseCoin'] ? item.coinData['baseCoin'] : {'coin': '--'};
          let quoteCoin = item.coinData && item.coinData['quoteCoin'] ? item.coinData['quoteCoin'] : {'coin': '--'};
          this.props.setLeverageData(item);
          this.setState({
            quoteCoinNum: quoteCoin.normal ? quoteCoin.normal : '0.00',
            baseCoinNum: baseCoin.normal ? baseCoin.normal : '0.00',
          });
        }
        return null;
      });

    }
  }

  onChange=(e) => {
    console.log(e);
    coinNum = '';
    this.setState({
      coinsName: e,
      leverageNum: ''
    });
  }

  handleChange=(e) => {
    let regex = new RegExp('(^[0-9]{1,8}$)|(^[0-9]{1,8}[\.]{1}[0-9]{1,8}$)');
    if (regex.test(Number(e.target.value))) {
      this.setState({
        leverageNum: e.target.value
      });
    }

  }

  selseAll=() => {
    this.setState({
      leverageNum: coinNum
    });
  }

  changeCoinNum = (assetList) => {
    let data = {};
    for (let i = 0; i < assetList.length; i++) {
      if (assetList[i].coin.toUpperCase() === this.state.coinsName[0]) {
        data = assetList[i];
      }
    }
    console.log(data);
    return data.normal ? data.normal : 0.00;
  }

  changeCoinsNum=() => {

  }

  changeAvailable=(assetList, leverAssetList, quoteCoinName) => {
    console.log(quoteCoinName);
    console.log(leverAssetList);
    let data = this.state.type ? this.changeCoinNum(assetList) :
      this.state.coinsName[0] === quoteCoinName ? this.state.quoteCoinNum : this.state.baseCoinNum;
    coinNum = data;
    return data;
  }

  submit=(type, symbol) => {
    let coin = this.state.coinsName[0] ? this.state.coinsName[0].toLowerCase() : null;
    if (type) {
      this.props.leverTransferRecharge({
        'symbol': symbol ? symbol : null,
        'coin': coin,
        'amount': this.state.leverageNum
      });
    } else {
      this.props.leverTransferWithdrawal({
        'symbol': symbol ? symbol : null,
        'coin': coin,
        'amount': this.state.leverageNum
      });
    }
  }
  render() {
    const {goBack, assetList, leverAssetList} = this.props;
    const { state: { baseCoin, quoteCoin } } = this.props.location;
    let baseCoinName = baseCoin.toUpperCase();
    let quoteCoinName = quoteCoin.toUpperCase();
    let symbol = baseCoin + quoteCoin;
    console.log(leverAssetList);
    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div>
          <div style={{ flexDirection: 'column', overflow: 'auto', flex: 'auto' }}>
            <div>
              <NavBar
                mode="light"
                icon={
                  <Flex direction="row" align="center">
                    <Icon type="left" color={'black'} />
                  </Flex>
                }
                onLeftClick={goBack}
              />
            </div>
            <div style={{display: 'flex', flexDirection: 'column', margin: 15}}>
              <div style={{fontSize: 24, fontWeight: 'bold', color: '#3d81cf'}}>划转</div>
            </div>

            <div style={{display: 'flex', flexDirection: 'column', margin: 15}}>
              <SwitchView
                title={baseCoin.length > 0 && quoteCoin.length > 0 ? `${baseCoinName}/${quoteCoinName}` : ''}
                onSwitch={(item) => {
                  this.setState({
                    type: item
                  });
                }}
              />

              <div style={{display: 'flex', flexDirection: 'column', marginTop: 15}}>
                <div style={{marginBottom: 15, color: '#36366e'}}>币种</div>
                <Picker
                  data={data}
                  value={this.state.coinsName}
                  cols={1}
                  onChange={this.onChange}
                  onOk={this.onChange}
                >
                  <Flex style={{borderBottomStyle: 'solid', borderBottomWidth: 0.1, borderBottomColor: '#aeb2b5', paddingBottom: 10}}>
                    <div style={{flex: 1, color: '#36366e'}}>{this.state.coinsName}</div>
                    <Icon type="right" color={'black'} />
                  </Flex>
                </Picker>

              </div>

              <div style={{display: 'flex', flexDirection: 'column', marginTop: 30}}>
                <div style={{marginBottom: 15, color: '#36366e'}}>划转数量</div>
                <Flex style={{borderBottomStyle: 'solid', borderBottomWidth: 0.1, borderBottomColor: '#aeb2b5', paddingBottom: 10}}>
                  <input
                    value={this.state.leverageNum}
                    onChange={this.handleChange}
                    style={{flex: 1, borderStyle: 'solid', borderWidth: 0}}
                    placeholder="请输入划转数量"
                    pointerEvents={'none'}
                  />
                  <div style={{color: '#aeb2b5'}}>{this.state.coinsName}</div>
                  <div style={{width: 1, height: 15, backgroundColor: '#aeb2b5', marginLeft: 10, marginRight: 10}} />
                  <div style={{color: '#36366e', fontWeight: 'bold'}} onClick={() => { this.selseAll() }}>全部</div>
                </Flex>
                <div style={{marginTop: 15, color: '#aeb2b5'}}>可用
                  {this.changeAvailable(assetList, leverAssetList, quoteCoinName)}
                  {this.state.coinsName}</div>
              </div>
            </div>

            <div style={{backgroundColor: '#f4f4f4', padding: 15, margin: 15}}>
              <div style={{color: '#b3b7ba'}}>进行账户间划转时，资产即时到账且不收取任何手续费</div>
            </div>
          </div>

          <div style={styles.button}>
            <Button
              style={{
                width: '100%',
                backgroundColor: this.state.leverageNum.length > 0 ? '#3d81cf' : '#aeb2b5',
                color: 'white'
              }}
              onClick={() => {
                if (this.state.leverageNum.length > 0) {
                  this.submit(this.state.type, symbol);
                }
              }}>划转</Button>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({
  assetList: state.mine.assetList,
  leverAssetList: state.mine.leverAssetList,
  leverageData: state.native.leverageData,
});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
  leverTransferRecharge: (payload) => {
    dispatch({type: 'asset/leverTransferRecharge', payload: payload});
  },
  leverTransferWithdrawal: (payload) => {
    dispatch({type: 'asset/leverTransferWithdrawal', payload: payload});
  },
  setLeverageData: (payload) => {
    dispatch({type: 'native/setLeverageData', payload: payload});
  },

});

export default connect(mapStateToProps, mapDispatchToProps)(LeverTransfer);

const styles = {
  button: {
    marginLeft: 15,
    marginRight: 15,
    position: 'fixed',
    left: 0,
    right: 0,
    bottom: 5,
  },
};
