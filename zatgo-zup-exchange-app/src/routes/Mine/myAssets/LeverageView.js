/**
 * Created by zhoujianxin on 2018/11/19.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import {Flex, Icon} from 'antd-mobile';

const close = require('../../../assets/mine/mine_close.svg');
const open = require('../../../assets/mine/mine_open.svg');

class LeverageView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      closeOrOpen: true
    };
  }

  componentDidMount() {

  }

  render() {
    const {hiddenAsset, assetInfo, rates} = this.props;
    let CNY = rates['btc'] && assetInfo.totalAsset ? Number(rates['btc']) * Number(assetInfo.totalAsset) : 0.00;
    return (
      <div style={styles.container}>
        <div style={{backgroundColor: '#373d4f', paddingLeft: 15, paddingRight: 15}}>
          <Flex style={{height: 40}}>
            <div style={{fontSize: 14, color: 'white', marginRight: 10}}>杠杆账户</div>
            <div style={{fontSize: 12, color: '#9ba1b1', flex: 1}}>总资产折合（BTC）</div>
            <img
              onClick={() => {
                this.props.onHiddenAsset && this.props.onHiddenAsset();
              }}
              src={this.state.closeOrOpen ? open : close}
              style={{ width: 20, height: 20, marginRight: 12, marginLeft: 10 }} alt=""
            />
          </Flex>
          <div style={{fontSize: 18, color: 'white', marginBottom: 15}}>
            {hiddenAsset ? assetInfo.totalAsset ? assetInfo.totalAsset : '0.00' : '*****'}</div>
          <div style={{fontSize: 14, color: 'white', marginBottom: 15}}>
            {hiddenAsset ? `≈${CNY.toFixed(2)}CNY` : '*****'}
          </div>
          <div style={styles.inputStyle}>
            <div style={{fontSize: 14, color: '#9ba1b1', marginTop: 15}}
              onClick={() => {}}
            >隐藏小额币种</div>
            <div style={styles.inputContainer}>
              <Icon type="search" size="xs" />
              <input
                style={styles.input}
                maxLength={20}
                placeholder={'搜索币种'}
                onChange={(e) => {
                  this.props.searchData && this.props.searchData(e.target.value);
                }}
              />
            </div>
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  hiddenAsset: state.native.hiddenAsset,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(LeverageView);

const styles = {
  container: {
    display: 'block',
  },
  inputStyle: {
    display: 'flex',
    borderTopStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: '#9ba1b1',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  inputContainer: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'start',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: '8px 1px',
    height: 30,
    position: 'relative',
    top: 20,
    borderStyle: 'solid',
    borderRadius: 6,
    borderWidth: 0
  },
  input: {
    display: 'flex',
    flex: 1,
    fontSize: 14,
    marginLeft: 8,
    border: 'none',
    outline: 'medium',
    autoComplete: 'off',
    backgroundColor: 'white',
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
