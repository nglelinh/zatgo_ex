/**
 * Created by zhoujianxin on 2018/11/19.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import {Flex, Icon} from 'antd-mobile';


class LeverageItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    const {hiddenAsset, data, rates} = this.props;
    let baseCoin = data.coinData['baseCoin'] ? data.coinData['baseCoin'] : {'coin': '--'};
    let quoteCoin = data.coinData['quoteCoin'] ? data.coinData['quoteCoin'] : {'coin': '--'};
    let baseCoinCNY = baseCoin.coin !== '--' && rates[baseCoin.coin.toLocaleLowerCase()] ?
      Number(rates[baseCoin.coin.toLocaleLowerCase()]) * Number(baseCoin.normal + baseCoin.locked) : 0.00;
    let quoteCoinCNY =  quoteCoin.coin !== '--' && rates[quoteCoin.coin.toLocaleLowerCase()] ?
      Number(rates[quoteCoin.coin.toLocaleLowerCase()]) * Number(quoteCoin.normal + quoteCoin.locked) : 0.00;

    return (
      <div style={styles.container}>
        <div style={{margin: 10}} onClick={() => {
          let params = data;
          params['CNY'] = (baseCoinCNY + quoteCoinCNY).toFixed(2);
          this.props.onClick && this.props.onClick(params);
        }}>
          <Flex>
            <div style={{flex: 1, color: '#3d81cf', fontWeight: 'bold', fontSize: 16}}>
              {(`${baseCoin.coin}/${quoteCoin.coin}`).toUpperCase()}
            </div>
            <Icon type="right" size="xs" />
          </Flex>

          <Flex style={{marginTop: 18}}>
            <Flex.Item style={{color: '#C4C8CB'}}>币种</Flex.Item>
            <Flex.Item style={{color: '#C4C8CB'}}>可用</Flex.Item>
            <Flex.Item style={{textAlign: 'right', color: '#C4C8CB'}}>已借</Flex.Item>
          </Flex>

          <Flex style={{marginTop: 10}}>
            <Flex.Item style={{color: '#C4C8CB'}}>{baseCoin.coin.toUpperCase()}</Flex.Item>
            <Flex.Item>{hiddenAsset ? baseCoin.normal : '*****'}</Flex.Item>
            <Flex.Item style={{textAlign: 'right'}}>{hiddenAsset ? baseCoin.loan : '*****'}</Flex.Item>
          </Flex>

          <Flex style={{marginTop: 10}}>
            <Flex.Item style={{color: '#C4C8CB'}}>{quoteCoin.coin.toUpperCase()}</Flex.Item>
            <Flex.Item>{hiddenAsset ? quoteCoin.normal : '*****'}</Flex.Item>
            <Flex.Item style={{textAlign: 'right'}}>{hiddenAsset ? quoteCoin.loan : '*****'}</Flex.Item>
          </Flex>

          <div style={{color: '#C4C8CB', marginTop: 10}}>{hiddenAsset ? `折合${(baseCoinCNY + quoteCoinCNY).toFixed(2)}CNY` : '*****'}</div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  hiddenAsset: state.native.hiddenAsset,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(LeverageItem);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    margin: 15,
    backgroundColor: 'white'
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#E26A6A',
    height: 30,
    width: 65,
    justifyContent: 'center',
    alignItems: 'center',
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
