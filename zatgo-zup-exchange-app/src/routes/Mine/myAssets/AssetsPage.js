/**
 * Created by zhoujianxin on 2018/11/16.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import intl from 'react-intl-universal';
import {Flex, NavBar, Icon} from 'antd-mobile';
import {DocumentTitle} from 'components';
import CoinsView from './CoinsView';
import CoinsListItem from './CoinsListItem';
import LeverageView from './LeverageView';
import LeverageItem from './LeverageItem';


class AssetsPage extends Component {
  state = {
    tab: 0,
    dataArr: [],
    searchData: null
  }

  componentDidMount() {
    this.props.getAsset();
    this.props.getLeverAsset();

    if (this.props.tabState) {
      this.setState({
        tab: this.props.tabState
      });
    }
  }

  componentWillUnmount() {
  }

  tabClick=(index) => {
    this.props.setTabState(index);
    this.setState({tab: index, searchData: null});
  }

  filterMarket = (txt) => {
    if (txt.length > 0) {
      console.log(txt);
      const input = txt.toLowerCase();
      const {assetList, leverAssetList} = this.props;
      const searchData = this.state.tab === 0 ? assetList.filter((item) => {
        const {coin} = item;
        return coin.toLowerCase().indexOf(input) !== -1;
      }) :
        leverAssetList.filter((item) => {
          console.log(item);
          const {coinData} = item;
          return coinData.baseCoin && coinData.baseCoin.coin.toLowerCase().indexOf(input) !== -1;
        });
      this.setState({searchData});
    } else {
      this.setState({searchData: null});

    }
  }

  hiddenAsset = () => {
    let item = this.props.hiddenAsset;
    this.props.setHiddenAsset(!item);
  }

  render() {
    const {goBack, changeUrl, rate, setCoinsData, setLeverageData, assetInfo, assetList, leverAssetInfo, leverAssetList} = this.props;
    let arrData = this.state.searchData ? this.state.searchData : assetList;
    let leverArrData = this.state.searchData ? this.state.searchData : leverAssetList;
    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div style={styles.container}>
          <div>
            <NavBar
              mode="light"
              icon={
                <Flex direction="row" align="center">
                  <Icon type="left" color={'black'} />
                </Flex>
              }
              onLeftClick={goBack}
            >资产管理</NavBar>
          </div>

          <Flex style={{display: 'flex', justifyContent: 'center', backgroundColor: this.state.tab === 0 ? '#3d81cf' : '#373d4f'}}>
            <div style={{margin: 10, fontSize: 18, color: this.state.tab === 0 ? 'white' : '#c3c3c3'}} onClick={() => { this.tabClick(0) }}>币币</div>
            <div style={{margin: 10, fontSize: 18, color: this.state.tab === 1 ? 'white' : '#c3c3c3'}} onClick={() => { this.tabClick(1) }}>杠杆</div>
          </Flex>

          <div style={{width: '100%', height: 0.5, backgroundColor: '#c3c3c3'}} />
          {this.state.tab === 0 ?
            <div style={{display: 'flex', height: '100%', flexDirection: 'column', overflow: 'auto', flex: 'auto'}}>
              <CoinsView
                assetInfo={assetInfo}
                rates={rate}
                onHiddenAsset={() => {
                  this.hiddenAsset();
                }}
                searchData={(text) => {
                  this.filterMarket(text);
                }}
              />
              <div style={{
                flexDirection: 'column',
                paddingTop: 20,
                // backgroundColor: 'green'
              }}>{arrData.map((item, index) => {
                  return (
                    <CoinsListItem key={index}
                      data={item}
                      rates={rate}
                      onClick={(item) => {
                        setCoinsData(item);
                        changeUrl('/coinsOrder');
                      }}
                    />);
                })}
              </div>
            </div> :
            <div style={{display: 'flex', height: '100%', flexDirection: 'column', overflow: 'auto', flex: 'auto'}}>
              <LeverageView
                assetInfo={leverAssetInfo}
                rates={rate}
                onHiddenAsset={() => {
                  this.hiddenAsset();
                }}
                searchData={(text) => {
                  this.filterMarket(text); }
                }
              />
              <div style={{
                flexDirection: 'column',
                paddingTop: 20,
                // backgroundColor: 'green'
              }}>
                {leverArrData.map((item, index) => {
                  return (
                    <LeverageItem key={index}
                      data={item}
                      rates={rate}
                      onClick={(item) => {
                        setLeverageData(item);
                        changeUrl('/leverageOrder');
                      }}
                    />);
                })
                }
              </div>
            </div>
          }


        </div>
      </DocumentTitle>
    );
  }
}

const selectZh = (rates) => (rates['cny'] ? rates['cny'] : {});

const mapStateToProps = (state) => ({
  tabState: state.native.tabState,
  rate: selectZh(state.app.rates),
  hiddenAsset: state.native.hiddenAsset,
  assetInfo: state.mine.assetInfo,
  assetList: state.mine.assetList,
  leverAssetInfo: state.mine.leverAssetInfo,
  leverAssetList: state.mine.leverAssetList,
});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch({type: 'native/setTabState', payload: null});
    dispatch(routerRedux.goBack());
  },
  setTabState: (payload) => {
    dispatch({type: 'native/setTabState', payload: payload});
  },
  setCoinsData: (payload) => {
    dispatch({type: 'native/setCoinsData', payload: payload});
  },
  setLeverageData: (payload) => {
    dispatch({type: 'native/setLeverageData', payload: payload});
  },
  setHiddenAsset: (payload) => {
    dispatch({type: 'native/setHiddenAsset', payload: payload});
  },
  getAsset: (payload) => {
    dispatch({type: 'mine/getAsset', payload: payload});
  },
  getLeverAsset: (payload) => {
    dispatch({type: 'mine/getLeverAsset', payload: payload});
  },

});

export default connect(mapStateToProps, mapDispatchToProps)(AssetsPage);

const styles = {
  container: {
    display: 'flex',
    flex: 1,
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#f7f6fa'
  },
};
