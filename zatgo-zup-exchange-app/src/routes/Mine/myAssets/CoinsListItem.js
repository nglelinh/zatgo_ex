/**
 * Created by zhoujianxin on 2018/11/19.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import {Flex, Icon} from 'antd-mobile';


class CoinsListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

  }


  render() {
    const {data, hiddenAsset, rates} = this.props;
    let locked = data.normal ? Number(data.normal) : 0;
    let normal = data.lockeds ? Number(data.locked) : 0;
    let CNY = data.coin && rates[data.coin.toLocaleLowerCase()] ? Number(rates[data.coin.toLocaleLowerCase()]) * (locked + normal) : 0.00;
    return (
      <div style={styles.container}>
        <div style={{margin: 10}} onClick={() => {
          let params = data;
          params['baseCoin'] = 'BTC';
          params['quoteCoin'] = data.coin;
          params['CNY'] = CNY.toFixed(2);
          this.props.onClick && this.props.onClick(data);
        }}>
          <Flex>
            <div style={{flex: 1, color: '#3d81cf', fontWeight: 'bold', fontSize: 16}}>{data.coin.toUpperCase()}</div>
            <Icon type="right" size="xs" />
          </Flex>

          <Flex style={{marginTop: 18}}>
            <Flex.Item style={{color: '#C4C8CB'}}>可用</Flex.Item>
            <Flex.Item style={{color: '#C4C8CB'}}>冻结</Flex.Item>
            <Flex.Item style={{textAlign: 'right', color: '#C4C8CB'}}>折合(CNY)</Flex.Item>
          </Flex>

          <Flex style={{marginTop: 10}}>
            <Flex.Item>{hiddenAsset ? data.normal : '*****'}</Flex.Item>
            <Flex.Item>{hiddenAsset ? data.locked : '*****'}</Flex.Item>
            <Flex.Item style={{textAlign: 'right'}}>{CNY > 0 ? CNY.toFixed(2) : '0.00'}</Flex.Item>
          </Flex>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  hiddenAsset: state.native.hiddenAsset,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CoinsListItem);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    margin: 15,
    backgroundColor: 'white'
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#E26A6A',
    height: 30,
    width: 65,
    justifyContent: 'center',
    alignItems: 'center',
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
