/**
 * Created by zhoujianxin on 2018/11/15.
 * @Desc
 */

import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import {DocumentTitle} from 'components';
import intl from 'react-intl-universal';
import {Flex, NavBar, Icon} from 'antd-mobile';


class About extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    const {goBack} = this.props;

    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div>
          <NavBar
            mode="dark"
            icon={
              <Flex direction="row" align="center">
                <Icon type="left" />
              </Flex>
            }
            onLeftClick={goBack}
          >分享邀请
          </NavBar>
        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(About);

// const styles = {
//   container: {
//     display: 'flex',
//     flexDirection: 'row',
//     height: 100,
//     justifyContent: 'space-between',
//     paddingTop: 5,
//     paddingBottom: 5,
//     backgroundColor: '#dddddd'
//   },
//   button: {
//     display: 'flex',
//     flexDirection: 'column',
//     backgroundColor: '#E26A6A',
//     height: 30,
//     width: 65,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   font11: {
//     color: '#797F85',
//     fontSize: 11,
//     textAlign: 'center'
//   },
//   font16: {
//     fontSize: 16
//   }
// };
