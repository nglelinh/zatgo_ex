
import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { List, WhiteSpace, Button } from 'antd-mobile';
import styles from './MinePage.css';

import orderImg from '../../assets/mine/mine_ordermanger.svg';
import about from '../../assets/mine/mine_about.svg';
import qianbao from '../../assets/mine/mine_qianbao.svg';
import yaoqing from '../../assets/mine/mine_yaoqing.svg';


const Item = List.Item;

class MineListView extends React.Component {

  onLogout=() => {
    const {onLogout} = this.props;
    onLogout && onLogout();
  }

  render() {
    const {changeUrl} = this.props;
    return (
      <div className={styles.MineList}>
        <WhiteSpace />
        <List>
          <Item
            thumb={qianbao}
            arrow="horizontal"
            // extra={<img src={orderImg} />}
            onClick={() => { changeUrl('/myAssets') }}
          >
            我的资产
          </Item>
          <Item
            thumb={orderImg}
            arrow="horizontal"
            onClick={() => { changeUrl('/MyOrder') }}
          >
            订单管理
          </Item>

          <Item
            thumb={yaoqing}
            arrow="horizontal"
            onClick={() => { changeUrl('/sharePage') }}// changeUrl('/invite') }}
          >
            分享邀请
          </Item>

          <Item
            thumb={about}
            arrow="horizontal"
            onClick={() => { changeUrl('/about') }}
          >
            关于我们
          </Item>

        </List>
        <WhiteSpace />
        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
          <div style={{flex: 1, width: '100%', height: 100}} />
          <Button type="primary" onClick={() => this.onLogout()}>退出</Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  tab: state.app.tab,
});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  onLogout: (symbol) => {
    dispatch({ type: 'login/onLogout', payload: symbol });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(MineListView);
