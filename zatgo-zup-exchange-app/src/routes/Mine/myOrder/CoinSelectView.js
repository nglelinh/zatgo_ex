/**
 * Created by zhoujianxin on 2018/12/12.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import {Modal, Flex, Button, Menu} from 'antd-mobile';

let data = [];

class CoinSelectView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMeun: false,
      quoteCoins: '',
      baseCoin: '',
      meunValue: [],
      selectBtn1: false,
      selectBtn2: false
    };
  }

  componentDidMount() {
    const { quoteCoins } = this.props;
    if (quoteCoins && quoteCoins.length > 0) {
      for (let i = 0; i < quoteCoins.length; i++) {
        data[i] = {value: i, label: quoteCoins[i].toUpperCase()};
      }
      console.log(data[0]);
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    const { tab, quoteCoins } = nextProps;
    if (this.props.tab !== tab) {
      this.onReset();
    }
    if (this.props.quoteCoins !== quoteCoins) {
      for (let i = 0; i < quoteCoins.length; i++) {
        data[i] = {value: i, label: quoteCoins[i]};
      }
    }
  }

  componentWillUnmount() {
    data = [];
  }

  onSelectBtn=(item) => {
    this.setState({
      selectBtn1: item ? !this.state.selectBtn1 : false,
      selectBtn2: !item ? !this.state.selectBtn2 : false,

    });
  }

  onChange=(txt) => {
    this.setState({
      baseCoin: txt
    });
  }

  onChangeMeun=(value) => {
    let num =  value[0];
    console.log(num + '****' + data[num] + '*****' + data[0]);
    this.setState({
      quoteCoins: data[num].label,
      meunValue: value,
      showMeun: false,
    });
  }

  onShow=() => {
    this.props.onShow && this.props.onShow();
  }

  onClose=() => {
    this.props.onClose && this.props.onClose();
  }

  onReset = () => {
    this.setState({
      showMeun: false,
      quoteCoins: '',
      baseCoin: '',
      meunValue: [],
      selectBtn1: false,
      selectBtn2: false
    });
  };

  onSubmit = () => {
    const {tab} = this.props;
    const {selectBtn1, selectBtn2, baseCoin, quoteCoins} = this.state;
    let params = {
      baseCoin: baseCoin !== '' ? baseCoin.toLowerCase() : '',
      quoteCoins: quoteCoins !== '' ? quoteCoins.toLowerCase() : '',
    };
    if (tab === 0) {
      let side =  selectBtn1 ? 'BUY' : selectBtn2 ? 'SELL' : null;
      if (side) {
        params['side'] = side;
      }
      this.props.submit && this.props.submit(params);
    } else {
      let orderStuts = !selectBtn1 && !selectBtn2 ? '2,4' : selectBtn1 ? '4' : selectBtn2 ? '2' : null;
      if (orderStuts) {
        params['orderStatuss'] = orderStuts;
      }
      this.props.submit && this.props.submit(params);
    }
  };

  render() {
    const {visible, tab} = this.props;
    const menuEl = (
      <Menu
        style={{
          position: 'relative',
          zIndex: 100
        }}
        value={this.state.meunValue}
        data={data}
        level={1}
        onChange={this.onChangeMeun}
        height={document.documentElement.clientHeight * 0.3}
      />
    );
    return (
      <Modal
        popup
        visible={visible}
        onClose={() => this.onClose()}
        animationType="slide-down"
      >
        <div>
          <div style={{display: 'flex', margin: 10, flexDirection: 'column'}}>
            <div style={{fontSize: 16, textAlign: 'left'}}>设置币对</div>
            <Flex>
              <div style={styles.inputContainer}>
                <input
                  style={styles.input}
                  value={this.state.baseCoin}
                  placeholder={'币种'}
                  onChange={(e) => { this.onChange(e.target.value) }}
                />
              </div>
              <div style={{height: 20, margin: 10}}>/</div>
              <div style={styles.inputContainer} onClick={() => { this.setState({showMeun: !this.state.showMeun}) }}>
                <div style={styles.input} onChange={(e) => { this.onChange(e.target.value) }}>
                  {this.state.quoteCoins !== '' ? this.state.quoteCoins : '选择价单位'}
                </div>
              </div>
            </Flex>
            {this.state.showMeun ? menuEl : null}
          </div>

          <div style={{display: 'flex', margin: 10, flexDirection: 'column'}}>
            <div style={{textAlign: 'left'}}>订单状态</div>
            <Flex style={{flex: 1, justifyContent: 'center', }}>
              <div style={{
                ...styles.btn,
                color: this.state.selectBtn1 ? 'white' : '#d0d0d0',
                backgroundColor: this.state.selectBtn1 ? '#3d81cf' : 'white'
              }} onClick={() => { this.onSelectBtn(true) }}
              ><div style={{margin: 5}}>{tab === 0 ? '买入' : '已撤单'}</div></div>
              <div style={{width: '5%'}} />
              <div style={{
                ...styles.btn,
                color: this.state.selectBtn2 ? 'white' : '#d0d0d0',
                backgroundColor: this.state.selectBtn2 ? '#3d81cf' : 'white'
              }} onClick={() => { this.onSelectBtn(false) }}
              ><div style={{margin: 5}}>{tab === 0 ? '卖出' : '已完成'}</div></div>
            </Flex>
          </div>

          <Flex style={{marginRight: 5, marginLeft: 5}}>
            <Flex.Item>
              <Button type="primary" onClick={() => this.onReset()}>重置</Button>
            </Flex.Item>
            <Flex.Item>
              <Button type="primary" onClick={() => this.onSubmit()}>确定</Button>
            </Flex.Item>
          </Flex>
        </div>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CoinSelectView);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#d0d0d0'
  },
  input: {
    display: 'flex',
    flex: 1,
    fontSize: 14,
    marginLeft: 8,
    marginRight: 8,
    border: 'none',
    outline: 'medium',
    autoComplete: 'off',
    backgroundColor: 'white',
  },
  inputContainer: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'start',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: '8px 1px',
    height: 20,
    position: 'relative',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderRadius: 6,
  },
  btn: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderRadius: 6,
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
