/**
 * Created by zhoujianxin on 2018/11/15.
 * @Desc
 */

import React, {Component} from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {Flex, NavBar, Icon, List} from 'antd-mobile';
import {DocumentTitle} from 'components';
import intl from 'react-intl-universal';

const Item = List.Item;

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    const {goBack} = this.props;
    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div style={{display: 'flex', flexDirection: 'column', height: '100%' }}>
          <div>
            <NavBar
              mode="light"
              icon={
                <Flex direction="row" align="center">
                  <Icon type="left" color={'black'} />
                </Flex>
              }
              onLeftClick={goBack}
            />
          </div>
          <div style={{display: 'flex', flexDirection: 'column', margin: 15}}>
            <div style={{fontSize: 24, fontWeight: 'bold', color: '#3d81cf'}}>关于我们</div>
          </div>

          <List className="my-list">
            <Item>联系我们</Item>
            <Item>隐私政策</Item>
            <Item extra="0.0.1" arrow="horizontal" onClick={() => {}}>版本</Item>

          </List>

        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(About);

// const styles = {
//   container: {
//     display: 'flex',
//     flexDirection: 'row',
//     height: 30,
//     justifyContent: 'space-between',
//     paddingTop: 5,
//     paddingBottom: 5,
//     backgroundColor: '#dddddd'
//   },
//   button: {
//     display: 'flex',
//     flexDirection: 'column',
//     backgroundColor: '#E26A6A',
//     height: 30,
//     width: 65,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   font11: {
//     color: '#797F85',
//     fontSize: 11,
//     textAlign: 'center'
//   },
//   font16: {
//     fontSize: 16
//   }
// };
