/**
 * Created by zhoujianxin on 2018/8/13.
 * @Desc
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import { Flex } from 'antd-mobile';
import Stepper from './components/Stepper';
import { EntryOrder } from 'components';

const getFullNum = (num) => {
  // 处理非数字
  if (isNaN(num)) { return num }

  // 处理不需要转换的数字
  let str = String(num);
  if (!/e/i.test(str)) { return num }

  return (num).toFixed(18).replace(/\.?0+$/, '');
};

class Market extends Component {

  constructor(props) {
    super(props);
    this.state = {
      val: 1,
      data: ''
    };
  }

  componentDidMount() {

  }

  onChange = (val) => {
    this.setState({ val });
    this.props.deepClick && this.props.deepClick(val);
  }

  onClick = (data) => {
    this.props.onClick && this.props.onClick(data);
  }

  currentPrice = (tickers, arr) => {
    if (arr.length > 1 && tickers && tickers['baseCoin'].toUpperCase() === arr[0] && tickers['quoteCoin'].toUpperCase() === arr[1]) {
      return {price: tickers.last, color: '#35BAA0'};
    }
    return { price: 0, color: '#35BAA0' };
  }

  render() {
    const { entryOrderInfo, rates, data, tickers } = this.props;
    let arr = data && data.name.split('/');
    let zh = arr.length > 0 && rates && arr[1] ? rates[arr[1].toLowerCase()] : 0;
    let currentPrice = this.currentPrice(tickers, arr);
    return (
      <div>
        <Flex style={{ display: 'flex', marginBottom: 15.0 }}>
          <div style={{ fontSize: 20, marginLeft: 10, color: currentPrice.color }}>{currentPrice.price ? getFullNum(currentPrice.price) : 0}</div>
          <div style={styles.price}>
            {`≈${currentPrice.price && zh ? (Number(zh) * currentPrice.price).toFixed(2) : 0} CNY`}
          </div>
          <Stepper maxNum={3}
            onClick={this.onChange}
            defaultVal={this.state.val}
          />
        </Flex>
        <EntryOrder
          maxNum={5}
          onItemClick={(data) => this.onClick(data)}
          buyData={entryOrderInfo['bids'] ? entryOrderInfo['bids'] : []}
          sellData={entryOrderInfo['asks'] ? entryOrderInfo['asks'] : []}
        />
      </div>
    );
  }
}


const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Market);

const styles = {
  price: {
    flex: 1,
    color: '#A0A4A8',
    fontSize: 11,
    marginTop: 7
  }
};
