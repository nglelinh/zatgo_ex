/**
 * Created by zhoujianxin on 2018/8/24.
 * @Desc
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import { Flex, Modal } from 'antd-mobile';
import { routerRedux } from 'dva/router';
import Circle from 'react-circle';
import intl from 'react-intl-universal';
import DealCss from './DealPage.css';
import dayjs from 'dayjs';
import Big from 'big.js';

const alert = Modal.alert;

class OrderList extends Component {

  state = {

  };

  getFullNum = (num) => {
    // 处理非数字
    if (isNaN(num)) { return num }

    // 处理不需要转换的数字
    let str = String(num);
    if (!/e/i.test(str)) { return num }

    return (num).toFixed(18).replace(/\.?0+$/, '');
  }

  dealItem = (item, index) => {
    let remainVolume = item.remainVolume ? Number(item.remainVolume) : 0;
    let volume = item.volume ? new Big(item.volume) : 0;
    let progress = parseFloat((volume.minus(remainVolume) / volume) * 100).toFixed(1);
    return (
      <div key={index}>
        <div style={styles.container} >
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            width: '45%'
          }}>
            <Circle
              animate={true}
              animationDuration="1s"
              responsive={false} // Boolean: Make SVG adapt to parent size
              size={50} // Number: Defines the size of the circle.
              lineWidth={40} // Number: Defines the thickness of the circle's stroke.
              progress={Number(progress) > 0 ? progress : 0} // Number: Update to change the progress and percentage.
              progressColor="#1D74A8"  // String: Color of "progress" portion of circle.
              bgColor="whitesmoke" // String: Color of "empty" portion of circle.
              textColor="red" // String: Color of percentage text color.
              textStyle={{ font: 'bold 5rem Helvetica, Arial, sans-serif' }}
              percentSpacing={20} // Number: Adjust spacing of "%" symbol and number.
              roundedStroke={true} // Boolean: Rounded/Flat line ends
              showPercentage={true} // Boolean: Show/hide percentage.
              showPercentageSymbol={true} // Boolean: Show/hide only the "%" symbol.
            />
            <div>
              <div style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'left',
              }}>
                <div style={styles.font14}>{`${item.baseCoin.toLocaleUpperCase()}/${item.quoteCoin.toLocaleUpperCase()}`}</div>
                <div style={{ fontSize: 11, marginTop: 3, color: item.side === 'SELL' ? '#E26A6A' : '#35BAA0' }}>
                  ({item.side === 'BUY' ? intl.get('deal.buy2') : intl.get('deal.sell2')})
                </div>
              </div>
              <div style={{
                paddingTop: '15px',
                color: '#a3a3a3',
                fontSize: '12px',
              }}>{item.createDate && dayjs(new Date(item.createDate)).format('MM-DD HH:mm:ss')}</div>
            </div>
          </div>


          <div style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'left',
            marginRight: 5,
            flex: 1,
            width: '35%'
          }}>
            <div className={DealCss.title} style={{ ...styles.font14, width: '100%', color: '#797F85', textAlign: 'left' }}>
              {this.getFullNum(Number(item.price))}
            </div>
          </div>

          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <div style={{ ...styles.font14, color: '#797F85', textAlign: 'center' }}> {item.volume}</div>
            {item.status !== 'CANCELED' && item.status !== 'FILLED' && item.status !== 'EXPIRED' ?
              <div className={DealCss.btn}
                style={{ ...styles.button, marginTop: 10, fontSize: 11 }}
                onClick={() => {
                  let params = {
                    'baseCoin': item.baseCoin,
                    'orderId': item.orderId,
                    'quoteCoin': item.quoteCoin,
                    'symbol': `${item.baseCoin}${item.quoteCoin}`
                  };
                  alert('', intl.get('deal.msg'), [
                    { text: intl.get('deal.cancel'), onPress: () => { } },
                    { text: intl.get('deal.ok'), onPress: () => { this.props.onCancel && this.props.onCancel(params) } },
                  ]);
                }}>
                {intl.get('deal.cancel')}
              </div> :
              <div style={{ width: 44, marginTop: 26 }} />
            }
          </div>
        </div>
      </div >
    );
  };

  render() {
    const { orderList, params } = this.props;
    return (
      <div style={{ display: 'flex', flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
        <div>
          <Flex style={{ marginTop: 10, marginBottom: 10 }}>
            <div style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 10, flex: 1, color: '#797F85' }}>
              {intl.get('deal.currentorder')}
            </div>

            <div style={{ display: 'flex', flexDirection: 'row', marginRight: 10 }}
              onClick={() => this.props.changeUrl({pathname: '/order', state: {params}})}
            >
              <img
                src={require('../../assets/deal/order.svg')}
                style={{ width: 16, height: 16, marginLeft: 10 }} alt=""
              />
              <div style={{ color: '#797F85' }}>{intl.get('deal.all')}</div>
            </div>
          </Flex>
        </div>
        <div style={{
          display: 'flex',
          background: 'white',
          color: '#6e889e',
          padding: '10 10 10',
          fontSize: '12px'
        }}>
          <div style={{ width: '44%', textAlign: 'center', fontSize: 12 }}>{intl.get('order.market')}</div>
          <div style={{ width: '45%', fontSize: 12 }}>{intl.get('deal.price')}</div>
          <div style={{ width: '11%', textAlign: 'left', fontSize: 12}}>{intl.get('deal.number')}</div>
        </div>
        <div style={{
          width: '100%',
          height: 0.5,
          backgroundColor: '#D9D9D9'
        }}
        />

        <div style={{ marginBottom: 20, marginTop: 10 }}>
          {orderList && orderList.slice(0, 10).map((item, index) => (
            this.dealItem(item, index)
          ))
          }
        </div>
      </div>
    );
  }


}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderBottom: '1px solid #d9d9d9',
    padding: '10px 0px'
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: 44,
    color: '#7d96ac',
    padding: '2px 0',
    border: '1px solid #7d96ac'
  },
  font11: {
    color: '#797F85', fontSize: 11, marginTop: 8
  },
  font14: {
    fontSize: 14
  },
  font14Padding: {
    fontSize: 14, paddingTop: '10px'
  },
  font16: {
    fontSize: 16,
    textAlign: 'center'
  },

};
