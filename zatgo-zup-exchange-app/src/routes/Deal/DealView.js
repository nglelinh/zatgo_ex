/**
 * Created by zhoujianxin on 2018/8/13.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import {Flex, Button, Modal, List, Toast} from 'antd-mobile';
import {routerRedux} from 'dva/router';
import DealCss from './DealPage.css';
import intl from 'react-intl-universal';
import Big from 'big.js';

const styleArr = [
  {borderWidth: 1, borderColor: '#35BAA0', borderStyle: 'solid', marginRight: 10, width: 50, },
  {borderWidth: 1, borderColor: '#D9D9D9', borderStyle: 'solid', marginRight: 10, width: 50, },
  {borderWidth: 1, borderColor: '#CC4D4D', borderStyle: 'solid', marginRight: 10, width: 50, },
];
const textStyleArr = [
  {margin: 5, color: '#35BAA0', textAlign: 'center'},
  {margin: 5, color: '#D9D9D9', textAlign: 'center'},
  {margin: 5, color: '#CC4D4D', textAlign: 'center'}
];

let precision = [];

class DealView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      val: '',
      coinNumName: '',
      quoteCoin: '',
      buyOrSell: 0, // 0 是买入 1是卖出
      baseCoin: '',
      priceName: '',
      available: 0,
      sub: false,
      add: false,
      type: true,
    };
  }

  componentDidMount() {
    const {selectPrice, data, balanceData} = this.props;
    let arr = data && data.name.split('/');
    let type = data.type === 0 ? 1 : 0;
    this.setState({
      priceName: arr[1],
      quoteCoin: selectPrice > 0 ? selectPrice : '', // 计价货币
      coinNumName: arr[0],
      baseCoin: '', // 基础货币
      buyOrSell: data.type ? data.type : 0,
      val: intl.get('deal.limit'),
      sub: this.state.baseCoin > 0 ? true : false,
      available: this.getBalanceOfCoin(arr[type], balanceData)
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    let arr = nextProps.data && nextProps.data.name.split('/');
    this.getPrecision();
    let balanceData = nextProps.balanceInfo;
    let type = nextProps.data.type === 0 ? 1 : 0;
    let arrType = this.state.buyOrSell === 0 ? 1 : 0;
    if ((nextProps.selectPrice !== 0 && nextProps.selectPrice !== this.state.quoteCoin)) {
      balanceData['coinInfoName'] = nextProps.data && nextProps.data.name;
      this.setState({
        quoteCoin: nextProps.selectPrice > 0 ? nextProps.selectPrice : '',
        priceName: arr[1],
        coinNumName: arr[0],
        buyOrSell: this.state.buyOrSell,
        available: this.state.type ? this.getBalanceOfCoin(arr[type], balanceData) : this.getBalanceOfCoin(arr[arrType], balanceData)

      });
    } else {
      this.setState({
        priceName: arr[1],
        coinNumName: arr[0],
        buyOrSell: nextProps.data.type && this.state.type ? nextProps.data.type : this.state.buyOrSell,
        available: this.state.type ? this.getBalanceOfCoin(arr[type], balanceData) : this.getBalanceOfCoin(arr[arrType], balanceData),
      });
    }
  }

  getPrecision = () => {
    const {coinPairs} = this.props;
    const {priceName, coinNumName} = this.state;
    let quoteCoin = priceName;
    let baseCoin = coinNumName;
    let pricePrecision = 8;
    let amountPrecision = 0;
    if (coinPairs && coinPairs[quoteCoin]) {
      coinPairs[quoteCoin].forEach((item, index) => {
        if (item['baseCoin'] === baseCoin) {
          pricePrecision = item['pricePrecision'];
          amountPrecision = item['amountPrecision'];
          return;
        }
      });
    }
    precision = [pricePrecision, amountPrecision];
  }

  showModal = key => (e) => {
    e.preventDefault(); // 修复 Android 上点击穿透
    this.setState({
      [key]: true,
    });
  };

  onClose = key => () => {
    this.setState({
      [key]: false,
    });
  };

  onChance = (item, key) => {
    this.setState({
      [key]: false,
      val: item,
      quoteCoin: '',
      baseCoin: '',
    });
  };

  buyOrSellStatus = (key) => {
    // this.state.buyOrSell === 0 ? priceName : coinNumName
    const {balanceInfo, buyPrice, sellPrice } = this.props;
    const {priceName, coinNumName} = this.state;
    if (key === 0) {
      this.setState({
        buyOrSell: 0,
        quoteCoin: buyPrice && buyPrice['price'] ? buyPrice['price'] : '',
        baseCoin: '',
        available: this.getBalanceOfCoin(priceName, balanceInfo),
        type: false
      });
    } else {
      this.setState({
        buyOrSell: 1,
        quoteCoin: sellPrice && sellPrice['price'] ? sellPrice['price'] : '',
        baseCoin: '',
        available: this.getBalanceOfCoin(coinNumName, balanceInfo),
        type: false
      });
    }
  }

  getBalanceOfCoin = (coinName, balanceInfo) => {
    if (balanceInfo && balanceInfo.length > 0) {
      let temp = balanceInfo.filter((item, index) => (item.coinType === coinName))[0];
      return temp && temp.balance ? temp.balance : 0;
    }
    return 0;
  }

  renderInputBtn = () => {
    const {rates} = this.props;
    const {quoteCoin, baseCoin, priceName, coinNumName, available} = this.state;
    let price = baseCoin > 0 && quoteCoin > 0 ? (new Big(baseCoin).times(quoteCoin)) : 0;
    let zh = rates;
    return (
      <div>
        {this.state.val === '限价' || this.state.val === 'Limit' ?
          <div style={styles.moneyInput}>
            <input type="text" value={quoteCoin}
              style={{width: '50%', border: 'none', fontSize: 16, marginLeft: 10}}
              onChange={this.coinPriceChange}
              placeholder={intl.get('deal.price')}
            />
            <div className={DealCss.title}
              style={{width: 80, fontSize: 12, color: '#A0A4A8', marginTop: 4}}>
              {`≈${priceName && zh && zh[priceName] ? (Number(zh[priceName]) * quoteCoin).toFixed(2) : 0} CNY`}
            </div>
            <div style={{marginRight: 5, fontSize: 16, color: '#A0A4A8'}}>{priceName}</div>

            <div style={styles.btnsStyle}>
              <button className={DealCss.btn} type="button"
                disabled={this.state.sub}
                style={styles.btnStyle}
                onClick={() => {
                  this.addOrSub('-');
                }}>-
              </button>
              <div style={{height: 16, width: 1, backgroundColor: '#A0A4A8'}} />
              <button className={DealCss.btn} type="button"
                disabled={this.state.add}
                style={styles.btnStyle}
                onClick={() => {
                  this.addOrSub('+');
                }}>+
              </button>
            </div>
          </div>
          :
          <Flex style={styles.moneyInput2}>
            <div style={{
              display: 'flex',
              flex: 1, flexDirection: 'row', color: '#A0A4A8',
              marginLeft: 10,
              alignItems: 'center',
            }}>
              {intl.get('deal.markertmsg')}
            </div>
          </Flex>
        }

        <Flex style={styles.numberInput}>
          <input type="text" value={baseCoin}
            onChange={this.coinNumChange}
            style={{ width: '50%', border: 'none', fontSize: 16, marginLeft: 10}}
            placeholder={(this.state.val === '市价' || this.state.val === 'Market') && this.state.buyOrSell === 0 ?
              intl.get('deal.volumeoftrade') : intl.get('deal.number')}
          />
          <div style={{display: 'flex', marginRight: 10, alignItems: 'center'}}>
            <div style={{fontSize: 14, color: '#A0A4A8', }}>
              {(this.state.val === '市价' || this.state.val === 'Market') && this.state.buyOrSell === 0 ?
                priceName : coinNumName}
            </div>
          </div>
          <div style={styles.btnsStyle2}>
            <button className={DealCss.btn} type="button" style={styles.btnStyle1} onClick={() => {
              this.coinNumClick('1/4');
            }}>1/4
            </button>
            <div style={{height: 16, width: 1, backgroundColor: '#A0A4A8'}} />
            <button className={DealCss.btn} type="button" style={styles.btnStyle1} onClick={() => {
              this.coinNumClick('1/2');
            }}>1/2
            </button>
            <div style={{height: 16, width: 1, backgroundColor: '#A0A4A8'}} />
            <button className={DealCss.btn} type="button" style={styles.btnStyle1} onClick={() => {
              this.coinNumClick('1');
            }}>
              {intl.get('deal.all')}
            </button>
          </div>
        </Flex>

        <div style={{marginLeft: 10, color: '#A0A4A8', }}>
          {intl.get('deal.avail')} {available}
          {this.state.buyOrSell === 0 ? priceName : coinNumName}
        </div>
        {this.state.val === '限价' || this.state.val === 'Limit' ?
          <Flex style={{marginTop: 15}}>
            <div style={{flex: 1, marginLeft: 10, color: '#797F85', fontSize: 18, fontWeight: 'bold'}}>
              {intl.get('deal.volumeoftrade')}
            </div>
            <div style={{marginRight: 10, color: '#A0A4A8'}}>{this.getFullNum(parseFloat(price))}</div>
          </Flex> :
          <div style={{height: 20, marginTop: 15}} />
        }

      </div>
    );
  }

  // 设置inputValue
  coinPriceChange = (e) => {
    let pricePrecision = precision[0] ? precision[0] : 5;
    let regex = new RegExp('(^[0-9]{1,8}$)|(^[0-9]{1,8}[\.]{1}[0-9]{1,' + pricePrecision + '}$)');
    if (regex.test(Number(e.target.value))) {
      this.setState({
        quoteCoin: e.target.value
      });
    }
  }
  // 设置textareaValue
  coinNumChange = (e) => {
    let amountPrecision =  precision[1] ? precision[1] : 5;
    if ((this.state.val === '市价' || this.state.val === 'Market') && this.state.buyOrSell === 0) {
      amountPrecision = precision[0] ? precision[0] : 5;
    }

    let regex = new RegExp('(^[0-9]{1,8}$)|(^[0-9]{1,8}[\.]{1}[0-9]{1,' + amountPrecision + '}$)');
    if (regex.test(Number(e.target.value)) && Number(e.target.value) <= Number(this.state.available)) {
      this.setState({
        baseCoin: e.target.value
      });
    }
  }

  addOrSub = (type) => {
    if (type === '-') {
      let num = this.pricePrecision(this.state.quoteCoin, '-');
      this.setState({
        sub: this.state.quoteCoin > 0 ? false : true,
        quoteCoin: Number(num) > 0 ? num : 0,
        add: false,
      });
    } else if (type === '+') {
      let num = this.pricePrecision(this.state.quoteCoin, '+');
      this.setState({
        sub: false,
        quoteCoin: num
      });
    }
  }

  getFullNum = (num) => {
    // 处理非数字
    if (isNaN(num)) { return num }

    // 处理不需要转换的数字
    let str = String(num);
    if (!/e/i.test(str)) { return num }

    return (num).toFixed(18).replace(/\.?0+$/, '');
  }

  pricePrecision= (num, type) => {
    const { coinPairs } = this.props;
    const {priceName, coinNumName} = this.state;
    let quoteCoin = priceName;
    let baseCoin = coinNumName;
    let pricePrecision = 5;
    if (coinPairs && coinPairs[quoteCoin]) {
      coinPairs[quoteCoin].forEach((item, index) => {
        if (item['baseCoin'] === baseCoin) {
          pricePrecision = item['pricePrecision'];
          return;
        }
      });
    }
    let m = Math.pow(10, pricePrecision);
    let a = 1 / m;
    return type === '-' ? (Math.round(num * m - a * m) / m).toFixed(pricePrecision)
      : (Math.round(num * m + a * m) / m).toFixed(pricePrecision);
  }

  coinNumClick = (type) => {
    if (type === '1/4') {
      this.setState({
        baseCoin: this.cutXiaoNum((this.state.available / 4), precision[1])
      });
    } else if (type === '1/2') {
      this.setState({
        baseCoin: this.cutXiaoNum((this.state.available / 2), precision[1])
      });
    } else if (type === '1') {
      this.setState({
        baseCoin: this.cutXiaoNum((this.state.available), precision[1])
      });
    }
  }

  cutXiaoNum = (num, len) => {
    let numStr = num.toString();
    if (len == null) {
      len = numStr.length;
    }
    let index = numStr.indexOf('.');
    if (index === -1) {
      index = numStr.length;
      numStr += '.0000000000000';
    } else {
      numStr += '0000000000000';
    }
    let newNum = numStr.substring(0, index + len + 1);
    return newNum;
  }

  onSubmit = () => {
    const {priceName, coinNumName, baseCoin, quoteCoin, available} = this.state;
    let x = String(baseCoin).indexOf('.') + 1;   // 小数点的位置
    let y = String(baseCoin).length - x;  // 小数的位数
    let type = this.state.val === '限价' || this.state.val === 'Limit' ? 1 : 2;
    if (type === 1 && !(Number(quoteCoin) > 0)) {
      Toast.show(intl.get('deal.enterprice'));
      return;
    }
    if (type === 1 && !(Number(baseCoin) > 0)) {
      Toast.show(intl.get('deal.enteramount'));
      return;
    }
    if (type === 2 && !(Number(baseCoin) > 0) && this.state.buyOrSell === 0) {
      Toast.show(intl.get('deal.entertotal'));
      return;
    }

    if (type === 2 && !(Number(baseCoin) > 0) && this.state.buyOrSell === 1) {
      Toast.show(intl.get('deal.enteramount'));
      return;
    }
    if (!(available > 0)) {
      Toast.show(intl.get('deal.noenough'));
      return;
    }

    if ((this.state.val === '市价' || this.state.val === 'Market') && this.state.buyOrSell === 0 && precision[0] < y) {
      Toast.show(intl.get('deal.maximumlimit'));
      return;
    }

    let params = {
      quoteCoin: priceName,
      baseCoin: coinNumName,
      val: this.state.val,
      type: type,
      volume: baseCoin,
      price: type === 1 ? quoteCoin : 0
    };
    this.props.onSubmit && this.props.onSubmit(params, this.state.buyOrSell);
    this.setState({
      quoteCoin: '',
      baseCoin: '',
    });
  }

  render() {
    const {coinNumName} = this.state;
    return (
      <div style={{display: 'flex', width: '100%', flexDirection: 'column'}}>
        <div>
          <Flex style={{marginLeft: 10, height: 60}}>
            <div style={{display: 'flex', flex: 1, flexDirection: 'row', marginBottom: 10}}>
              <div style={this.state.buyOrSell === 0 ? styleArr[0] : styleArr[1]}
                onClick={() => this.buyOrSellStatus(0)}>
                <div style={this.state.buyOrSell === 0 ? textStyleArr[0] : textStyleArr[1]}>
                  {intl.get('deal.buy')}
                </div>
              </div>

              <div style={this.state.buyOrSell === 1 ? styleArr[2] : styleArr[1]}
                onClick={() => this.buyOrSellStatus(1)}>
                <div style={this.state.buyOrSell === 1 ? textStyleArr[2] : textStyleArr[1]}>
                  {intl.get('deal.sell')}
                </div>
              </div>
            </div>
            <div style={{textAlign: 'right', marginTop: 6}}>
              <div style={{marginRight: 10, alignItems: 'center', color: '#797F85'}} onClick={this.showModal('modal')}>
                {this.state.val}
                <img
                  src={this.state.modal ? require('../../assets/deal/arrowUp.svg') : require('../../assets/deal/arrowDown.svg')}
                  style={{width: 12, height: 12, marginRight: 12, marginLeft: 10}} alt=""
                />
              </div>
            </div>
          </Flex>
        </div>

        {this.renderInputBtn()}

        <div>
          <div className={this.state.buyOrSell === 0 ? DealCss.btn1 : DealCss.btn2}
            style={{
              display: 'flex',
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              margin: 10,
            }}
            onClick={() => {
              this.onSubmit();
            }}>
            <div style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
              {this.state.buyOrSell === 0 ? intl.get('deal.buy') : intl.get('deal.sell')} {coinNumName}
            </div>
          </div>
        </div>

        <div style={{width: '100%', height: 8, backgroundColor: '#F0F0F0', marginTop: 5}} />

        <Modal
          popup
          visible={this.state.modal}
          onClose={this.onClose('modal')}
          animationType="slide-up"
        >
          <List>
            {[intl.get('deal.market'), intl.get('deal.limit')].map((i, index) => (
              <List.Item key={index} onClick={() => this.onChance(i, 'modal')}>{i}</List.Item>
            ))}
            <List.Item>
              <Button style={{backgroundColor: '#4DCC7B'}} type="primary" onClick={this.onClose('modal')}>取消</Button>
            </List.Item>
          </List>
        </Modal>
      </div>
    );
  }

}


const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(DealView);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5,
    paddingBottom: 5,
    margin: 10,
    alignItems: 'center'
  },
  button: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 44,
    // backgroundColor: '#E26A6A',
    borderColor: '#817c77',
    borderStyle: 'solid',
    borderWidth: 1
  },
  font11: {
    color: '#797F85', fontSize: 11, marginTop: 8
  },
  font16: {
    color: '#323B43', fontSize: 16
  },
  triangle: {
    height: 0,
    width: 100,
    bordertop: '100px solid red',
    borderright: '37px solid transparent',
  },
  submitBtn: {
    display: 'flex',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  numberInput: {
    flex: 3,
    borderColor: '#D9D9D9',
    borderWidth: 1,
    borderStyle: 'solid',
    height: 44,
    margin: 10
  },
  moneyInput: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#D9D9D9',
    borderWidth: 1,
    borderStyle: 'solid',
    height: 44,
    marginLeft: 10,
    marginRight: 10,
  },
  moneyInput2: {
    borderColor: '#D9D9D9',
    borderWidth: 1,
    borderStyle: 'solid',
    height: 44,
    backgroundColor: '#F0F0F0',
    marginLeft: 10,
    marginRight: 10,
  },
  btnsStyle: {
    display: 'flex',
    flexDirection: 'row',
    width: 88,
    height: 44,
    borderLeftColor: '#D9D9D9',
    borderLeftWidth: 1,
    borderLeftStyle: 'solid',
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  btnsStyle2: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
    height: 44,
    borderLeftColor: '#D9D9D9',
    borderLeftWidth: 1,
    borderLeftStyle: 'solid',
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  btnStyle: {
    flex: 1,
    height: 44,
    color: '#A0A4A8',
    fontSize: 14
  },
  btnStyle1: {
    flex: 1,
    height: 40,
    color: '#A0A4A8',
    fontSize: 14
  },
};
