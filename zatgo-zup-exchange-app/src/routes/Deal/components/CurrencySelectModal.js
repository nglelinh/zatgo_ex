/*
 * @Author: lsl
 * @Date: 2018-08-15 16:50:14
 * @Last Modified by: lsl
 * @Last Modified time: 2018-09-06 17:49:11
 * @Desc 币种选择
 */
import React, { Component } from 'react';
import { Modal } from 'antd-mobile';
import { PriceList } from 'components';


class CurrencySelectModal extends Component {

  render() {
    const { market, loading, visible, onClose, onItemClick, quoteCoins, rates } = this.props;
    return (
      <Modal
        popup
        visible={visible}
        onClose={onClose}
        maskClosable={false}
        style={{ height: '100vh', width: '100vw' }}
        animationType="slide-down"
      >
        <PriceList
          onCancel={() => { onClose() }}
          showCancelButton
          market={market}
          rates={rates}
          quoteCoins={quoteCoins}
          loading={loading}
          onItemClick={(item) => { onClose(); onItemClick(item) }}
        />
      </Modal>
    );
  }
}

export default CurrencySelectModal;