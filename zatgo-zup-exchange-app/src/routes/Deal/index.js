import React, { Component } from 'react';
import { connect } from 'dva';
import ReactDOM from 'react-dom';
import dayjs from 'dayjs';
import intl from 'react-intl-universal';
import { routerRedux } from 'dva/router';
import { Flex, PullToRefresh } from 'antd-mobile';
import MarketView from './Market';
import DealView from './DealView';
import { DocumentTitle } from 'components';
import CurrencySelectModal from './components/CurrencySelectModal';
import OrderList from './OrderList';



const Header = ({ data, onSwitch = () => { }, goBack = () => { } }) => {
  return (
    <Flex style={{ height: 44, marginLeft: 10, marginRight: 10, marginTop: 5 }}>
      {/* <div style={{ fontSize: 16, fontWeight: 'bold' }} */}
      {/* onClick={goBack} */}
      {/* > */}
      {/* <Icon type="left" color="#000" /> */}
      {/* </div> */}
      <div style={{ fontSize: 16, fontWeight: 'bold', flex: 1, textAlign: 'center' }}>{data && data['name']}</div>
      <img
        onClick={onSwitch}
        src={require('../../assets/deal/change.svg')}
        style={{ width: 16, height: 16, marginRight: 12, marginLeft: 10 }} alt=""
      />
    </Flex>
  );
};

const getData = (item) => {
  const { baseCoin, quoteCoin } = item;
  const coinPairName = `${baseCoin}/${quoteCoin}`.toUpperCase();
  return { name: coinPairName, key: `${baseCoin}${quoteCoin}`, type: item.type };

};

const param = {
  'baseCoin': '',
  'depthStep': 0,
  'event': 'SUB',
  'klineTime': '5',
  'quoteCoin': '',
  'symbol': '',
  'type': 'DEPTH_STEP'
};

const listParam =
{
  'baseCoin': '',
  'isReturnTradeDetail': false,
  'orderStatuss': ['NEW', 'INIT', 'PART_FILLED', 'PENDING_CANCEL'],
  'pageNo': 1,
  'pageSize': 10,
  'quoteCoin': '',
  'symbol': ''
};


class DealPage extends Component {

  state = {
    switchVisible: false,
    refreshing: false,
    down: true,
    height: document.documentElement.clientHeight / 2,
    selectPrice: 0,
    day: dayjs().format('MM-DD HH:mm:ss'),
    data: { 'name': '', 'key': '' },
    entryOrderInfo: {},
  };

  componentDidMount() {
    const data = this.props.location.state ? this.props.location.state : this.getdefauleData();
    console.log(data);
    const height = document.documentElement.clientHeight - ReactDOM.findDOMNode(this.ptr).parentNode.offsetTop;
    this.setState({
      height: height,
      data: getData(data),
    });
    param['depthStep'] = 0;
    this.subDepthStep(data);
    this.getOrderList(data);
    this.subscribeTicker(data);
    this.props.getBalanceData();
  }

  getdefauleData = () => {
    const { coinPairs } = this.props;
    let coinPairsArr = Object.values(coinPairs);
    if (coinPairsArr) {
      if (coinPairsArr[0]) {
        let temp = coinPairsArr[0][0];
        console.log({'baseCoin': temp.baseCoin.toLocaleLowerCase(), 'quoteCoin': temp.quoteCoin.toLocaleLowerCase()});
        return {'baseCoin': temp.baseCoin.toLocaleLowerCase(), 'quoteCoin': temp.quoteCoin.toLocaleLowerCase()};
      }
    }
    return {'baseCoin': '', 'quoteCoin': ''};
  }

  getOrderList = (item) => {
    const { baseCoin, quoteCoin } = item;
    listParam['baseCoin'] = baseCoin;
    listParam['quoteCoin'] = quoteCoin;
    listParam['symbol'] = `${baseCoin}${quoteCoin}`;
    this.props.getOrderList(listParam);
  }

  subDepthStep = (item) => {
    const { baseCoin, quoteCoin } = item;
    param['baseCoin'] = baseCoin;
    param['quoteCoin'] = quoteCoin;
    param['event'] = 'SUB';
    param['symbol'] = `${baseCoin}${quoteCoin}`;
    this.props.getEntryOrderData(param);
  }

  subscribeTicker = (item) => {
    const { baseCoin, quoteCoin } = item;
    const ticker = {
      'quoteCoin': quoteCoin,
      'baseCoin': baseCoin,
      'depthStep': '1',
      'event': 'SUB',
      'klineTime': '5',
      'symbol': `${baseCoin}${quoteCoin}`,
    };
    ticker['type'] = 'TICKER';
    this.props.getTicker(ticker);
  }

  showModal = key => (e) => {
    e.preventDefault(); // 修复 Android 上点击穿透
    this.setState({
      [key]: true,
    });
  }

  onClose = key => () => {
    this.setState({
      [key]: false,
    });
  }

  onItemClick = (item) => {
    const { baseCoin, quoteCoin } = item;
    const coinPairName = `${baseCoin}/${quoteCoin}`.toUpperCase();
    this.setState({
      data: { 'name': coinPairName, 'key': `${baseCoin}${quoteCoin}` },
    });
    this.subDepthStep(item);
    this.getOrderList(item);
    this.subscribeTicker(item);
    this.props.getBalanceData();


  }

  onSelectPrice = (data) => {
    this.setState({
      selectPrice: data,
    }, () => {
      this.setState({
        selectPrice: 0,

      });
    });
  }

  deepClick = (value) => {
    param['depthStep'] = value - 1;
    this.subDepthStep(param);

  }

  onSubmit = (item, type) => {
    if ((item.type === 2 || item.baseCoin !== '') && item.quoteCoin !== '') {
      this.setState({
        selectPrice: 0,
      });
      let params = {
        'baseCoin': item.baseCoin.toLocaleLowerCase(),
        'type': item.type,
        'side': type === 0 ? 'buy' : 'sell',
        'volume': item.volume,
        'symbol': `${item.baseCoin}${item.quoteCoin}`.toLocaleLowerCase(),
        'quoteCoin': item.quoteCoin.toLocaleLowerCase(),
        'feeIsUserPlatformCoin': false,
        'price': item.price,
      };

      console.log('新增：', JSON.stringify(params));
      this.props.submitOrder(params, listParam);
    }
  }

  onCancel = (item) => {
    console.log('取消：', JSON.stringify(item));
    this.props.cancelOrder(item, listParam);
  }

  render() {
    const { loading, market, entryOrderInfo, balanceInfo, orderList, rates, quoteCoins, ticker, coinPairs } = this.props;
    return (
      <DocumentTitle title={intl.get('title.deal')}>
        <div >
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Header onSwitch={this.showModal('switchVisible')}
              goBack={this.props.goBack}
              data={this.state.data}
            />
            <MarketView onClick={this.onSelectPrice}
              deepClick={this.deepClick}
              entryOrderInfo={entryOrderInfo ? entryOrderInfo : {}}
              data={this.state.data}
              rates={rates}
              tickers={ticker}
            />
          </div>

          <div style={{ display: 'flex', flexDirection: 'column', marginTop: 10, marginBottom: global.exchangeType ? 44 : 20}}>
            <PullToRefresh
              damping={100}
              ref={el => { this.ptr = el }}
              style={{
                height: this.state.height,
                overflow: 'auto',
              }}
              indicator={{
                activate: `${intl.get('deal.pulldown')}${this.state.day}`,
                // release: `刷新中，刷新时间:${this.state.day}...`,
                finish: `${intl.get('deal.update')}${this.state.day}`
              }}
              direction={'down'}
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({
                  refreshing: true,
                });
                setTimeout(() => {
                  this.getOrderList(listParam);
                  this.setState({
                    refreshing: false,
                    day: dayjs().format('MM-DD HH:mm:ss')
                  });
                }, 1000);
              }}
            >
              <DealView selectPrice={this.state.selectPrice}
                onSubmit={this.onSubmit}
                data={this.state.data}
                orderList={orderList}
                balanceInfo={balanceInfo}
                rates={rates}
                coinPairs={coinPairs ? coinPairs : 3}
                buyPrice={entryOrderInfo && entryOrderInfo['bids'] ? entryOrderInfo['bids'][0] : []}
                sellPrice={entryOrderInfo && entryOrderInfo['asks'] ? entryOrderInfo['asks'][0] : []}
              />
              <OrderList
                data={this.state.data}
                orderList={orderList}
                onCancel={this.onCancel}
                params={{'baseCoin': listParam['baseCoin'], 'quoteCoin': listParam['quoteCoin'], 'symbol': listParam['symbol']}}
              />
            </PullToRefresh>
          </div>

          <CurrencySelectModal
            rates={rates}
            quoteCoins={quoteCoins}
            market={market}
            loading={loading}
            visible={this.state.switchVisible}
            onItemClick={this.onItemClick}
            onClose={this.onClose('switchVisible')}
          />
        </div>
      </DocumentTitle>
    );
  }
}

const selectZh = (rates) => (rates['cny'] ? rates['cny'] : {});

const mapStateToProps = (state) => ({
  market: state.price.market,
  entryOrderInfo: state.deal.entryOrderInfo,
  balanceInfo: state.deal.balanceInfo,
  orderList: state.deal.orderList,
  rates: selectZh(state.app.rates),
  quoteCoins: state.app.quoteCoins,
  coinPairs: state.app.coinPairs,
  ticker: state.price.ticker
});

const mapDispatchToProps = (dispatch) => ({
  getEntryOrderData: (payload) => {
    dispatch({ type: 'deal/getEntryOrderData', payload: payload });
  },
  getBalanceData: (payload) => {
    dispatch({ type: 'deal/getBalanceData', payload: payload });
  },
  getOrderList: (payload) => {
    dispatch({ type: 'deal/getOrderList', payload: payload });
  },
  getTicker: (symbol) => {
    dispatch({ type: 'price/getTicker', payload: symbol });
  },
  submitOrder: (payload, listParams) => {
    dispatch({ type: 'deal/submitOrder', payload: payload, listParams: listParams });
  },
  cancelOrder: (payload, listParams) => {
    dispatch({ type: 'deal/cancelOrder', payload: payload, listParams: listParams });
  },
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(DealPage);
