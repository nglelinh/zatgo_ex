const routes = [
  {
    path: '/',
    name: 'IndexPage',
    getComponent(nextState, cb) {
      import('./Price/PricePage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/deal',
    getComponent(nextState, cb) {
      import('./Deal').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/priceDetail',
    getComponent(nextState, cb) {
      import('./Price/PriceDetailPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/optional',
    getComponent(nextState, cb) {
      import('./Optional/OptionalAddPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/order',
    getComponent(nextState, cb) {
      import('./Order/OrderPage').then(module => {
        cb(null, module.default);
      });
    },
  }
];

export default routes;
