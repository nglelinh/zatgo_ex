/*
 * @Author: lsl
 * @Date: 2018-08-16 09:30:43
 * @Last Modified by: lsl
 * @Last Modified time: 2018-11-09 17:33:54
 */
import React, { Component } from 'react';
import { Tabs } from 'antd-mobile';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import intl from 'react-intl-universal';
import styles from './Price.css';
import { asyncComponent } from 'utils';
import { EntryOrder, DocumentTitle } from 'components';
import ChartTools from 'components/Charts/chartTool';
import { Header, BottomActionBar} from './components/PriceDetail';
import TradeList from './components/TradeList';
import TickerView from './components/TickerView';

const Candlestick = asyncComponent(() => import('components/Charts/Candlestick'));
const Depthmap = asyncComponent(() => import('components/Charts/Depthmap'));

const getTabs = () => (
  [{ title: intl.get('detail.depth') }, { title: intl.get('detail.trades') }]
);


const TabLayout = (props) => {
  const { depthData, tradeTickers, onTabChange, tabs, isLoading, quoteCoin, baseCoin} = props;
  let depthmapOption = ChartTools.depthmapOption();
  if (depthData && depthData.bids) {
    depthmapOption = ChartTools.depthmapOption(depthData);
  }
  return (
    <Tabs
      tabs={tabs}
      tabBarActiveTextColor="#35BAA0"
      tabBarInactiveTextColor="#797F85"
      onChange={onTabChange}
      swipeable={false}
    >
      <div className={styles.mainContent}>
        <Depthmap option={depthmapOption} isLoading={isLoading} />
        <EntryOrder
          buyData={depthData && depthData['bids']}
          sellData={depthData && depthData['asks']}
          style={{ marginTop: 20, marginBottom: 20 }}
        />
      </div>
      <TradeList tradeTickers={tradeTickers} quoteCoin={quoteCoin} baseCoin={baseCoin} />
    </Tabs>
  );
};

class PriceDetailPage extends Component {

  state={
    favorites: true
  }

  componentDidMount() {
    this.selectState();
    this.subscribeTicker();
    this.subscribeTradeTicker();
    this.subscribeKLine();
    this.subscribeDepthStep();
  }

  componentWillUnmount() {
    this.setState({
      favorites: true
    });
    this.props.onCloseWebSocket();
    this.props.closeDepthStep();
  }
  subscribeTicker = () => {
    const { state: { baseCoin, quoteCoin } } = this.props.location;
    const ticker = {
      'quoteCoin': quoteCoin,
      'baseCoin': baseCoin,
      'depthStep': '1',
      'event': 'SUB',
      'klineTime': '5',
      'symbol': `${baseCoin}${quoteCoin}`,
    };
    ticker['type'] = 'TICKER';
    this.props.getTicker(ticker);
  }

  subscribeTradeTicker = () => {
    const { state: { baseCoin, quoteCoin } } = this.props.location;
    const param = {
      'quoteCoin': quoteCoin,
      'baseCoin': baseCoin,
      'depthStep': '1',
      'event': 'SUB',
      'klineTime': '5',
      'symbol': `${baseCoin}${quoteCoin}`,
    };
    param['type'] = 'TRADE_TICKER';
    this.props.getTradeTicker(param);
  }

  subscribeKLine = () => {
    const { state: { baseCoin, quoteCoin } } = this.props.location;
    const kline = {
      'quoteCoin': quoteCoin,
      'baseCoin': baseCoin,
      'depthStep': '1',
      'event': 'SUB',
      'klineTime': '60',
      'symbol': `${baseCoin}${quoteCoin}`,
    };
    kline['type'] = 'KLINE';
    this.props.getKLine(kline);
  }

  subscribeDepthStep = () => {
    const { state: { baseCoin, quoteCoin } } = this.props.location;
    const depth = {
      'quoteCoin': quoteCoin,
      'baseCoin': baseCoin,
      'depthStep': '1',
      'event': 'SUB',
      'klineTime': '5',
      'symbol': `${baseCoin}${quoteCoin}`,
    };
    depth['type'] = 'DEPTH_STEP';
    this.props.getEntryOrderData(depth);
  }

  onTabChange = (tab, index) => { }

  /**
   * 0:买入、1:卖出
   */
  onTrade(type) {
    const { changeUrl, location } = this.props;
    const { state: { baseCoin, quoteCoin } } = location;
    changeUrl({ pathname: '/deal', state: { quoteCoin, baseCoin, type } });
  }

  /**
   * 0:买入、1:卖出
   */
  addOptional(type) {
    const {state: {baseCoin, quoteCoin}} = this.props.location;
    if (this.state.favorites) {
      this.props.addOptional({baseCoin: baseCoin, quoteCoin: quoteCoin});

    } else {
      this.props.deleteOptional({baseCoin: baseCoin, quoteCoin: quoteCoin});
    }
    this.selectState();
  }

  selectState = () => {
    const {state: {baseCoin, quoteCoin}} = this.props.location;
    let dataSource = JSON.parse(localStorage.getItem('symbols')) ? JSON.parse(localStorage.getItem('symbols')) : {};
    let key = `${baseCoin}${quoteCoin}`;
    if (key in dataSource) {
      this.setState({
        favorites: false
      });
    } else {
      this.setState({
        favorites: true
      });
    }
  }



  render() {
    const { goBack, entryOrderInfo, tradeTickers, location, ticker, kline, rate, klineLoading, depthLoading } = this.props;
    const { baseCoin, quoteCoin } = location.state;
    const coinPairName = `${baseCoin.toUpperCase()}/${quoteCoin.toUpperCase()}`;
    const candlestickOption = ChartTools.candlestickOption(kline);
    return (
      <DocumentTitle title={coinPairName}>
        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
          <Header
            onLeftClick={goBack}
            leftText={coinPairName}
            rightContent={
              <BottomActionBar selectState={this.state.favorites}
                onSelect={() => { this.selectState() }}
                addOptional={() => { this.addOptional() }}
                onBuy={() => this.onTrade(0)}
                onSell={() => this.onTrade(1)}
              />}
          />
          <div style={{ flex: 1, overflow: 'auto' }}>
            <TickerView ticker={ticker ? ticker : location.state} rate={rate} />
            <Candlestick option={candlestickOption} isLoading={klineLoading} />
            <TabLayout
              isLoading={depthLoading}
              depthData={entryOrderInfo}
              tradeTickers={tradeTickers}
              tabs={getTabs()}
              baseCoin={baseCoin && baseCoin.toUpperCase()}
              quoteCoin={quoteCoin && quoteCoin.toUpperCase()}
            />
          </div>
          {/* <BottomActionBar */}
          {/* selectState={this.state.favorites} */}
          {/* onSelect={() => { this.selectState() }} */}
          {/* addOptional={() => { this.addOptional() }} */}
          {/* onBuy={() => this.onTrade(0)} */}
          {/* onSell={() => this.onTrade(1)} */}
          {/* /> */}
        </div>
      </DocumentTitle>
    );
  }
}

const selectZh = (rates) => (rates['cny'] ? rates['cny'] : {});

const mapStateToProps = (state) => ({
  rate: selectZh(state.app.rates),
  entryOrderInfo: state.deal.entryOrderInfo,
  tradeTickers: state.price.tradeTickers,
  ticker: state.price.ticker,
  kline: state.price.kline,
  klineLoading: state.price.klineLoading,
  depthLoading: state.deal.depthLoading,
});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
  getEntryOrderData: (payload) => {
    dispatch({ type: 'deal/getEntryOrderData', payload: payload });
  },
  getTradeTicker: (symbol) => {
    dispatch({ type: 'price/getTradeTicker', payload: symbol });
  },
  getTicker: (symbol) => {
    dispatch({ type: 'price/getTicker', payload: symbol });
  },
  getKLine: (symbol) => {
    dispatch({ type: 'price/getKLine', payload: symbol });
  },
  addOptional: (symbol) => {
    dispatch({ type: 'price/addOptional', payload: symbol });
  },
  getOptional: (symbol) => {
    dispatch({ type: 'price/getOptional', payload: symbol });
  },
  deleteOptional: (symbol) => {
    dispatch({ type: 'price/deleteOptional', payload: symbol });
  },
  onCloseWebSocket: (symbol) => {
    dispatch({ type: 'price/onCloseWebSocket', payload: symbol });
  },
  closeDepthStep: (symbol) => {
    dispatch({ type: 'deal/closeDepthStep', payload: symbol });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PriceDetailPage);
