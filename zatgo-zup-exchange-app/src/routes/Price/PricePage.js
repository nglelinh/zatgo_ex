/*
 * @Author: lsl
 * @Date: 2018-08-16 09:30:36
 * @Last Modified by: lsl
 * @Last Modified time: 2018-09-07 15:49:36
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Tabs } from 'antd-mobile';
import intl from 'react-intl-universal';
import { ListView, SearchBar, DocumentTitle } from 'components';
import PriceItem from './components/PriceItem';
import OptionalEmptyView from './components/OptionalEmptyView';

const getTabs = (quoteCoins) => {
  const favorites = [{ key: 'favorites', title: intl.get('price.favorites') }];
  // const favorites = [];
  const tabs = quoteCoins.map((v) => ({ title: v.toUpperCase(), key: v }));
  return favorites.concat(tabs);
};

let selectTab;

class PricePage extends Component {

  state = {
    curTickers: null,
    selectOptionalEmpty: false,
  }

  componentDidMount() {
    this.subMarket();
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    const { quoteCoins } = nextProps;
    const {maketTab} = this.props;
    if (this.props.quoteCoins !== quoteCoins && quoteCoins[0]) {
      selectTab = maketTab !== 1 ? maketTab ? quoteCoins[maketTab - 2] : quoteCoins[0] : 'favorites';
      if (maketTab === 1) {
        let dataSource = JSON.parse(localStorage.getItem('symbols'));
        this.setState({selectOptionalEmpty: !dataSource || JSON.stringify(dataSource) === '{}'});
      }
      this.subMarket();
    }
  }

  subMarket = () => {
    const param = {
      'depthStep': '1',
      'event': 'SUB',
      'type': 'MARKET'
    };
    console.log('订阅', JSON.stringify(param));
    this.props.getMarket(param);
  }

  unsubMarket = (quoteCoin) => {
    const param = {
      'quoteCoin': quoteCoin,
      'depthStep': '1',
      'event': 'UNSUB',
      'type': 'MARKET'
    };
    console.log('退订', JSON.stringify(param));
    this.props.getMarket(param);
  }

  filterMarket = (txt) => {
    if (txt.length > 0) {
      const input = txt.toLowerCase();
      const {market} = this.props;
      const allMarket = Object.values(market).reduce((pre, cur) => pre.concat(cur));
      const curTickers = allMarket.filter((item) => {
        const {baseCoin} = item;
        return baseCoin.toLowerCase().indexOf(input) !== -1;
      });
      this.setState({curTickers});
    } else {
      this.setState({curTickers: null});
    }
  }

  onTabChange = (tab, index) => {
    // this.unsubMarket(selectTab.toLowerCase());
    this.props.setMarketTabState(index + 1);
    switch (tab.key) {
      // eslint-disable-next-line
      case 'favorites':
        // 自选
        selectTab = tab.key;
        let dataSource = JSON.parse(localStorage.getItem('symbols'));
        this.setState({ selectOptionalEmpty: !dataSource || JSON.stringify(dataSource) === '{}'});
        break;
      default:
        selectTab = tab.key;
        this.subMarket(tab.key);
        this.setState({ selectOptionalEmpty: false});
        break;
    }
  }

  favoritesMarket = () => {
    const {market} = this.props;
    let dataSource = JSON.parse(localStorage.getItem('symbols'));
    if (JSON.stringify(market) !== '{}' && JSON.stringify(dataSource) !== '{}') {
      const allMarket = Object.values(market).reduce((pre, cur) => pre.concat(cur));
      let favoritesMarket = [];
      // eslint-disable-next-line
      for (let key in dataSource) {
        let curTickers = allMarket.filter((item) => {
          return item['symbol'] === key;
        });
        if (curTickers.length > 0) {
          favoritesMarket = favoritesMarket.concat(curTickers);
        }
      }
      return favoritesMarket;
    }
  }

  onItemClick = (item) => {
    const path = {
      pathname: '/priceDetail',
      state: item
    };
    console.log(item);
    this.props.changeUrl(path);
  }

  OrderListTitle = () => {
    return (
      <div style={{
        display: 'flex',
        justifyContent: 'space-between',
        background: '#fff',
        color: '#6e889e',
        padding: '10px',
        fontSize: '12px'
      }}>
        {
          this.state.tab === 0 ?
            <div style={{flex: '0 0 15%'}}>{null}</div> : null
        }
        <div style={{width: '35%', textAlign: 'center'}}>名称</div>
        <div style={{flex: '1 1 auto', width: '30%', textAlign: 'center'}}>全球价格</div>
        <div style={{width: '20%', textAlign: 'right'}}>24H涨跌</div>
      </div>
    );
  }


  render() {
    const {selectOptionalEmpty, curTickers} = this.state;
    const {market, loading, changeUrl, quoteCoins, rate, coinPairs, marketLoading, maketTab} = this.props;
    const key = maketTab && market !== 1 ? quoteCoins[maketTab - 2] : null;
    const curMarket = key ? market[key.toLowerCase()] : selectTab && market[selectTab.toLowerCase()] ? market[selectTab.toLowerCase()] :
      market[quoteCoins[0]] ? market[quoteCoins[0]] : [];
    const allCoinPairs = JSON.stringify(coinPairs) !== '{}' ? Object.values(coinPairs).reduce((pre, cur) => pre.concat(cur)) : [];
    return (
      <DocumentTitle title={intl.get('title.price')}>
        <div>
          <SearchBar
            placeholder={intl.get('price.search')}
            maxLength={20}
            onChange={(text) => {
              this.filterMarket(text);
            }}
          />
          <Tabs
            tabs={getTabs(quoteCoins)}

            initialPage={1}
            page={maketTab ? maketTab - 1 : 1}

            tabBarActiveTextColor="#35BAA0"
            tabBarInactiveTextColor="#797F85"
            onChange={this.onTabChange}
          >

            {
              selectOptionalEmpty ?
                <OptionalEmptyView
                  buttonText={intl.get('price.addFavorites')}
                  addOptional={() => changeUrl('/optional')}
                /> :
                <div>
                  {this.OrderListTitle()}
                  <div style={{
                    width: '100%',
                    borderTopWidth: 0.5,
                    borderTopColor: '#D9D9D9',
                    borderTopStyle: 'solid',
                  }}
                  />
                  <ListView
                    data={curTickers ? curTickers : selectTab !== 'favorites' ? curMarket : this.favoritesMarket()}
                    ListItem={PriceItem}
                    loading={loading}
                    onItemClick={this.onItemClick}
                    offsetHeight={100}
                    marketLoading={marketLoading}
                    rate={rate}
                    coinPairs={selectTab && allCoinPairs}
                    disableRefresh={false}
                    onRefresh={() => { this.subMarket() }}
                  />
                </div>

            }
          </Tabs>
        </div>
      </DocumentTitle>
    );
  }
}

const selectZh = (rates) => (rates['cny'] ? rates['cny'] : {});

const mapStateToProps = (state) => ({
  maketTab: state.native.maketTab,
  rate: selectZh(state.app.rates),
  quoteCoins: state.app.quoteCoins,
  coinPairs: state.app.coinPairs,
  market: state.price.market,
  marketLoading: state.price.marketLoading,
  optionals: state.app.optionals,
  loading: state.loading.effects['app/getBasicData']
});

const mapDispatchToProps = (dispatch) => ({
  getMarket: (symbol) => {
    dispatch({ type: 'price/getMarket', payload: symbol });
  },
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  setMarketTabState: (payload) => {
    dispatch({type: 'native/setMarketTabState', payload: payload});
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PricePage);
