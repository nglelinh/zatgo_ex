import intl from 'react-intl-universal';


const getFullNum = (num) => {
  // 处理非数字
  if (isNaN(num)) { return num }

  // 处理不需要转换的数字
  let str = String(num);
  if (!/e/i.test(str)) { return num }

  return (num).toFixed(18).replace(/\.?0+$/, '');
};

const PriceItem = (props) => {
  const {itemInfo, onItemClick, rate, coinPairs} = props;
  const quoteCoin = itemInfo.quoteCoin.toUpperCase();
  const last = itemInfo.last ? getFullNum(itemInfo.last) : 0;
  const vol = itemInfo.vol ? itemInfo.vol : 0;
  let iconImage = [];
  coinPairs && coinPairs.map(function (item) {
    if (item.baseCoin === itemInfo.baseCoin) iconImage.push(item.coinImage);
    return null;
  });

  return (
    <div style={styles.container} onClick={() => onItemClick(itemInfo)}>
      {iconImage.length > 0 && iconImage[0] ? <div>
        <img style={{width: 36, height: 36, borderRadius: 18}}
          src={iconImage[0]}
        />
      </div> : null
      }

      <div style={{
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 4,
      }}>
        <div style={styles.font16}>
          {itemInfo.baseCoin.toUpperCase()}
          <font style={{...styles.font11, marginLeft: 7}}>
            {`/${quoteCoin}`}
          </font>
        </div>
        <div style={{...styles.font11, marginTop: 8}}>
          {`24H${intl.get('detail.vol')} ${vol.toFixed(0)}`}
        </div>
      </div>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
        flex: 1,
        marginRight: 15
      }}>
        <div style={styles.font16}>{` ${last}`}</div>
        <div style={{...styles.font11, marginTop: 8}}>
          {`￥${(last * rate[itemInfo.quoteCoin]).toFixed(2)}`}
        </div>
      </div>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        height: 30,
        width: 65,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        backgroundColor: itemInfo.chg ? Number(itemInfo.chg) > 0 ? '#35BAA0' : '#E26A6A' : '#A0A4A8',
        fontSize: 13,
      }}>
        {itemInfo.chg ? `${(itemInfo.chg) > 0 ? '+' : ''}${(itemInfo.chg).toFixed(2)}%` : '0.00%'}
      </div>
    </div>
  );
};

export default PriceItem;

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '20px 10px 5px 10px',
    alignItems: 'center'
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    height: 30,
    width: 70,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  font11: {
    color: '#797F85', fontSize: 11,
  },
  font16: {
    color: '#323B43',
    fontSize: 16,
  }
};
