import React, { Component } from 'react';
import intl from 'react-intl-universal';

const getFullNum = (num) => {
  // 处理非数字
  if (isNaN(num)) { return num }

  // 处理不需要转换的数字
  let str = String(num);
  if (!/e/i.test(str)) { return num }

  return (num).toFixed(18).replace(/\.?0+$/, '');
};


class TickerView extends Component {

  render() {
    const {ticker, rate = {}} = this.props;

    const quoteCoin = ticker.quoteCoin && ticker.quoteCoin;
    const last = ticker.last ? ticker.last : 0;
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: '10px 22px',
      }}>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
        }}>
          <div style={{ fontSize: 18, color: '#35BAA0' }}>{getFullNum(last)}</div>
          <div style={{ color: '#797F85', marginTop: 8 }}>{`≈${last ? (last * rate[quoteCoin]).toFixed(2) : 0}CNY`}
            <font style={{ color: '#35BAA0', marginLeft: 5 }}>{``}</font>
          </div>
        </div>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
          width: 135
        }}>
          <div style={{
            display: 'flex',
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
          }}>
            <div>{ticker.high ? getFullNum(ticker.high) : 0}</div>
            <div style={{ color: '#797F85' }}>{intl.get('detail.high')}</div>
          </div>
          <div style={{
            display: 'flex',
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
            marginTop: 8,
            marginBottom: 8
          }}>
            <div>{ticker.low ? getFullNum(ticker.low) : 0}</div>
            <div style={{ color: '#797F85' }}>{intl.get('detail.low')}</div>
          </div>
          <div style={{
            display: 'flex',
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
          }}>
            <div style={{
              display: 'block',
              textAlign: 'right',
              width: 90,
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              color: 'black',
              textOverflow: 'clip'
            }}>{ticker.vol ? getFullNum(ticker.vol).toFixed(0) : 0}</div>
            <div style={{color: '#797F85'}}>{`24H${intl.get('detail.vol')}`}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default TickerView;

