/*
 * @Author: lsl
 * @Date: 2018-08-30 14:58:46
 * @Last Modified by: lsl
 * @Last Modified time: 2018-11-12 17:27:36
 * @Desc 详情页组件
 */

import { NavBar, Icon, Flex } from 'antd-mobile';
import dayjs from 'dayjs';
import intl from 'react-intl-universal';
import styles from './common.css';

/**
 * 标题栏
 */
export const Header = ({ onLeftClick, leftText, rightContent }) => {
  return (
    <NavBar
      mode="light"
      style={{ backgroundColor: '#fff' }}
      icon={
        <Flex direction="row" align="center">
          <Icon type="left" color="#000" />
          <span style={{ fontSize: 14, color: 'black', fontWeight: 'bold' }}>
            {leftText}
          </span>
        </Flex>
      }
      rightContent={rightContent}
      onLeftClick={onLeftClick}
    />
  );
};

/**
 * 24小时行情
 */
export const TickerView = ({ ticker = {}, rate }) => {
  const quoteCoin = ticker.quoteCoin && ticker.quoteCoin.toUpperCase();
  const last = ticker.last ? ticker.last : 0;
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: '10px 22px',
    }}>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
      }}>
        <div style={{ fontSize: 18, color: '#35BAA0' }}>{getFullNum(last)}</div>
        <div style={{ color: '#797F85', marginTop: 8 }}>{`≈${(last * rate[quoteCoin]).toFixed(2)}CNY`}
          <font style={{ color: '#35BAA0', marginLeft: 5 }}>{``}</font>
        </div>
      </div>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: 135
      }}>
        <div style={{
          display: 'flex',
          flexDirection: 'row-reverse',
          justifyContent: 'space-between',
        }}>
          <div>{ticker.high ? getFullNum(ticker.high) : 0}</div>
          <div style={{ color: '#797F85' }}>{intl.get('detail.high')}</div>
        </div>
        <div style={{
          display: 'flex',
          flexDirection: 'row-reverse',
          justifyContent: 'space-between',
          marginTop: 8,
          marginBottom: 8
        }}>
          <div>{ticker.low ? getFullNum(ticker.low) : 0}</div>
          <div style={{ color: '#797F85' }}>{intl.get('detail.low')}</div>
        </div>
        <div style={{
          display: 'flex',
          flexDirection: 'row-reverse',
          justifyContent: 'space-between',
        }}>
          <div style={{
            display: 'block',
            textAlign: 'right',
            width: 90,
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'clip'
          }}>{ticker.vol ? getFullNum(ticker.vol) : 0}</div>
          <div style={{color: '#797F85'}}>{`24H${intl.get('detail.vol')}`}</div>
        </div>
      </div>
    </div>
  );
};

/**
 * 成交记录子项
 */
export const TradeItem = ({ item }) => (
  <Flex
    direction="row"
    justify="between"
    align="center"
    style={{ height: 45, paddingLeft: 15, paddingRight: 15 }}
  >
    <Flex.Item>{item.ts && dayjs(Number(item.ts)).format('HH:mm:ss')}</Flex.Item>
    <Flex.Item style={{ textAlign: 'center', color: item.side === 'BUY' ? '#4cc453' : '#e94c4c'}}>
      {item.side === 'BUY' ? intl.get('deal.buy') : intl.get('deal.sell')}
    </Flex.Item>
    <Flex.Item style={{flex: 2, textAlign: 'right' }}>{getFullNum(item.price)}</Flex.Item>
    <Flex.Item style={{flex: 2, textAlign: 'right' }}>{getFullNum(item.vol)}</Flex.Item>
  </Flex>
);

export const getFullNum = (num) => {
  // 处理非数字
  if (isNaN(num)) { return num }

  // 处理不需要转换的数字
  let str = String(num);
  if (!/e/i.test(str)) { return num }

  return (num).toFixed(18).replace(/\.?0+$/, '');
};

/**
 * 成交-表头
 */
export const TradeHeader = ({baseCoin, quoteCoin}) => (
  <Flex
    direction="row"
    justify="between"
    align="center"
    style={{ paddingLeft: 15, paddingRight: 15, height: 40 }}
  >
    <Flex.Item style={{ fontSize: 12}}>{intl.get('detail.time')}</Flex.Item>
    <Flex.Item style={{ marginLeft: 8, textAlign: 'center', fontSize: 12 }}>{intl.get('detail.direction')}</Flex.Item>
    <Flex.Item style={{flex: 2, textAlign: 'right', fontSize: 12 }}>{intl.get('detail.price') + `(${quoteCoin})`}</Flex.Item>
    <Flex.Item style={{flex: 2, textAlign: 'right', fontSize: 12 }}>{intl.get('detail.amount') + `(${baseCoin})`}</Flex.Item>
  </Flex>
);

/**
 * 成交-列表
 */
export const TradeList = ({ tradeTickers, quoteCoin, baseCoin }) => (
  <div style={{ marginBottom: 30 }}>
    <TradeHeader quoteCoin={quoteCoin} baseCoin={baseCoin} />
    {
      tradeTickers && tradeTickers.map((item, index) => {
        return index < 20 ? (<TradeItem key={index} item={item} />) : null; })
    }
  </div>
);

// 详情页-自定义按钮
export const ActionButton = ({ text, onClick, color }) => (
  <div
    onClick={onClick}
    style={{
      display: 'flex',
      flex: 1,
      backgroundColor: color,
      justifyContent: 'center',
      alignItems: 'center',
      margin: '5px 0px 5px 10px'
    }}>
    {text}
  </div>
);

/**
 * 底部操作栏
 */
export const BottomActionBar = ({ onBuy, onSell, addOptional, selectState = true }) => {
  return (
    <div className={styles.tabbar}>
      {/* <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}> */}
      {/* <ActionButton */}
      {/* text={intl.get('detail.buy')} */}
      {/* color="#35BAA0" */}
      {/* onClick={onBuy} */}
      {/* /> */}
      {/* <ActionButton */}
      {/* text={intl.get('detail.sell')} */}
      {/* color="#EE5C42" */}
      {/* onClick={onSell} */}
      {/* /> */}
      {/* </div> */}
      <div className={styles.optionalTxt} onClick={() => { addOptional() }}>
        {selectState ? <img
          src={require('../../../assets/deal/shoucang.svg')}
          style={{width: 20, height: 20, marginRight: 12, marginLeft: 10}} alt=""
        /> :
        <img
          src={require('../../../assets/deal/shoucang2.svg')}
          style={{width: 20, height: 20, marginRight: 12, marginLeft: 10}} alt=""
        />}
      </div>
    </div>
  );
};
