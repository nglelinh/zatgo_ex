import { Button } from 'antd-mobile';
/**
 * 无自选数据展示
 * @param {func} addOptional 添加自选
 */
const OptionalEmptyView = ({ addOptional, buttonText }) => (
  <div style={{
    display: 'flex',
    height: '70vh',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  }}>
    <Button
      onClick={addOptional}
      style={{
        fontSize: 14,
        color: '#35BAA0',
        height: 44,
        width: 115
      }}
    >
      {buttonText}
    </Button>
    {/* <div>暂无自选</div> */}
  </div>
);

export default OptionalEmptyView;
