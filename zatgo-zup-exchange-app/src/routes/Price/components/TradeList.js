import React, { Component } from 'react';
import intl from 'react-intl-universal';
import {Flex} from 'antd-mobile';
import dayjs from 'dayjs';

const getFullNum = (num) => {
  // 处理非数字
  if (isNaN(num)) { return num }

  // 处理不需要转换的数字
  let str = String(num);
  if (!/e/i.test(str)) { return num }

  return (num).toFixed(18).replace(/\.?0+$/, '');
};



class TradeList extends Component {

  /**
   * 成交-表头
   */
  TradeHeader = (baseCoin, quoteCoin) => (
    <Flex
      direction="row"
      justify="between"
      align="center"
      style={{ paddingLeft: 15, paddingRight: 15, height: 40 }}
    >
      <Flex.Item style={{ fontSize: 12}}>{intl.get('detail.time')}</Flex.Item>
      <Flex.Item style={{ marginLeft: 8, textAlign: 'center', fontSize: 12 }}>{intl.get('detail.direction')}</Flex.Item>
      <Flex.Item style={{flex: 2, textAlign: 'right', fontSize: 12 }}>{intl.get('detail.price') + `(${baseCoin})`}</Flex.Item>
      <Flex.Item style={{flex: 2, textAlign: 'right', fontSize: 12 }}>{intl.get('detail.amount') + `(${quoteCoin})`}</Flex.Item>
    </Flex>
  );

  /**
   * 成交记录子项
   */
  TradeItem = (index, item) => (
    <Flex
      key={index}
      direction="row"
      justify="between"
      align="center"
      style={{ height: 45, paddingLeft: 15, paddingRight: 15 }}
    >
      <Flex.Item>{item.ts && dayjs(Number(item.ts)).format('HH:mm:ss')}</Flex.Item>
      <Flex.Item style={{ textAlign: 'center', color: item.side === 'BUY' ? '#4cc453' : '#e94c4c'}}>
        {item.side === 'BUY' ? intl.get('deal.buy') : intl.get('deal.sell')}
      </Flex.Item>
      <Flex.Item style={{
        flex: 2,
        textAlign: 'right',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'clip'
      }}>{getFullNum(item.price)}</Flex.Item>
      <Flex.Item style={{
        flex: 2,
        textAlign: 'right',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'clip'
      }}>{getFullNum(item.vol)}</Flex.Item>
    </Flex>
  );

  /**
   * 成交-列表
   */
  TradeList = (tradeTickers, quoteCoin, baseCoin) => (
    <div style={{ marginBottom: 30 }}>
      {this.TradeHeader(quoteCoin, baseCoin)}
      {
        tradeTickers && tradeTickers.map((item, index) => {
          return index < 20 ? this.TradeItem(index, item) : null;
        })
      }
    </div>
  );
  render() {
    const {tradeTickers, quoteCoin, baseCoin} = this.props;
    return (
      <div>
        {this.TradeList(tradeTickers, quoteCoin, baseCoin)}
      </div>
    );
  }
}

export default TradeList;

