/**
 * Created by zhoujianxin on 2018/12/17.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import {Flex, WingBlank, List, InputItem, WhiteSpace, Toast} from 'antd-mobile';
import {DocumentTitle} from 'components';
import intl from 'react-intl-universal';
import NoCaptcha from 'components/NoCaptcha';
import PhoneInput from 'components/PhoneInput';



class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      userPassword: '',
      isRemember: false,
      tab: 0,
      visible: false,
      sign: null,
      authCode: '',
      countryCode: '',
    };
  }

  componentDidMount() {
    // this.props.phoneLogin();
  }

  tabClick=(index) => {
    this.NoCaptcha.reset();
    this.setState({
      tab: index,
      userName: '',
      userPassword: '',
      isRemember: false,
      visible: false,
      sign: null,
      authCode: '',
      countryCode: '',
    });
  }

  onRef = (e) => {
    this.NoCaptcha = e;
  }

  changeUsername = (value) => {
    this.setState({
      userName: value
    });
  }

  changePassword = (value) => {
    this.setState({
      userPassword: value
    });
  }


  onChange=(txt) => {
    this.setState({
      authCode: txt
    });
  }

  handleClick=() => {
    console.log('000000');
    if (this.state.authCode === '' || this.state.authCode === null) {
      Toast.show('验证码不能为空');
      return;
    }
    if (this.state.sign) {
      if (this.state.tab === 0) {
        let params =  {
          'csessionid': this.state.sign.csessionid,
          'email': this.state.userName,
          'password': this.state.userPassword,
          'scene': this.state.sign.scene,
          'sig': this.state.sign.sig,
          'token': this.state.sign.token,
          'verifycode': this.state.authCode,
        };
        console.log(params);
        this.props.emailLogin(params);
      } else {
        let params = {
          'country': this.state.countryCode,
          'csessionid': this.state.sign.csessionid,
          'isAdmin': false,
          'loginName': this.state.userName,
          'loginPassword': this.state.userPassword,
          'authCode': this.state.authCode,
          'scene': this.state.sign.scene,
          'sig': this.state.sign.sig,
          'token': this.state.sign.token
        };
        this.props.phoneLogin(params);
      }
    }
    this.NoCaptcha.reset();
  }

  onChangeUrl=(path) => {
    const { changeUrl} = this.props;
    changeUrl({ pathname: path });

  }

  onShowOrClose=(type) => {
    this.setState({
      visible: type,
    });
  }

  sendCode=() => {
    if (this.state.tab === 0) {
      let params = {
        'email': this.state.userName,
        'otype': 1,
      };
      this.props.sendEmailCode(params);
    } else {
      let params =  {
        'mobile': this.state.userName,
        'country': this.state.countryCode,
        'otype': 1,
      };
      this.props.sendPhoneCode(params);
    }
  }

  render() {
    console.log(this.state.sign);
    return (
      <DocumentTitle title={intl.get('title.login')}>
        <div style={styles.container}>
          <Flex style={{display: 'flex', marginBottom: 5}}>
            <div>
              <div style={{margin: 10, fontSize: 18, color: this.state.tab === 0 ? 'black' : '#c3c3c3'}} onClick={() => { this.tabClick(0) }}>邮箱登录</div>
              {this.state.tab === 0 ? <div style={{width: '100%', height: 1, backgroundColor: 'red'}} /> : null}
            </div>
            <div style={{width: 20}} />
            <div>
              <div style={{margin: 10, fontSize: 18, color: this.state.tab === 1 ? 'black' : '#c3c3c3'}} onClick={() => { this.tabClick(1) }}>手机登录</div>
              {this.state.tab === 1 ? <div style={{width: '100%', height: 1, backgroundColor: 'red'}} /> : null}
            </div>
          </Flex>
          <WingBlank>
            <List>
              {this.state.tab === 0 ?
                <InputItem placeholder="输入账户"
                  value={this.state.userName}
                  onChange={(value) => { this.changeUsername(value) }}>
                  用户名:
                </InputItem>
                :
                <PhoneInput callBack={(data) => { this.setState({userName: data.value, countryCode: data.dialCode}) }} />
              }
              <InputItem placeholder="输入密码"
                type="password"
                value={this.state.userPassword}
                onChange={(value) => { this.changePassword(value) }}>
                密码:
              </InputItem>
            </List>
            <div style={{width: '100%', flexDirection: 'column', alignItems: 'center', marginTop: 30}}>
              <NoCaptcha onRef={this.onRef}
                sendCode={() => { this.sendCode() }}
                visible={this.state.visible}
                onClose={() => { this.onShowOrClose(false) }}
                callBack={(data) => { this.setState({sign: data}) }}
                submit={(data) => { this.setState({
                  authCode: data
                }, () => { this.onShowOrClose(false); this.handleClick() });
                }}
              />
            </div>
            <WhiteSpace />
            <div style={styles.button}
              onClick={() => {
                if (this.state.sign) {
                  if (this.state.userName === '' || this.state.userName === null) {
                    Toast.show('账户不能为空');
                    return;
                  }
                  if (this.state.userPassword === '' || this.state.userPassword === null || this.state.passwordAgain === '' || this.state.passwordAgain === null) {
                    Toast.show('密码不能为空');
                    return;
                  }
                  this.onShowOrClose(true);
                } else {
                  Toast.show('请进行滑动验证');
                }
              }}>
              <div>登录</div>
            </div>
            <WhiteSpace />
            <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
              <div style={{textAlign: 'right'}} onClick={() => { this.onChangeUrl('/registerPage') }}>注册</div>
              <div style={{width: 1, height: 10, backgroundColor: 'black', margin: 5}} />
              <div style={{textAlign: 'right'}} onClick={() => { this.onChangeUrl('/forgoPwdPage') }}>忘记密码</div>
            </div>
          </WingBlank>

        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  phoneLogin: (symbol) => {
    dispatch({ type: 'login/phoneLogin', payload: symbol });
  },
  emailLogin: (symbol) => {
    dispatch({ type: 'login/emailLogin', payload: symbol });
  },
  sendPhoneCode: (symbol) => {
    dispatch({ type: 'login/sendPhoneCode', payload: symbol });
  },
  sendEmailCode: (symbol) => {
    dispatch({ type: 'login/sendEmailCode', payload: symbol });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = {
  container: {
    display: 'flex',
    flex: 1,
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 15,
    marginRight: 15
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#35BAA0',
    height: 44,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderType: 'solid',
    borderRadius: 4
  },
  country: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderType: 'sliod'
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
