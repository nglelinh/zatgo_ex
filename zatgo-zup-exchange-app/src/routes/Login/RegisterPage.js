/**
 * Created by zhoujianxin on 2018/12/17.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import {Flex, WingBlank, List, InputItem, WhiteSpace, Button, NavBar, Icon} from 'antd-mobile';
import NoCaptcha from 'components/NoCaptcha';
import { routerRedux } from 'dva/router';
import {DocumentTitle} from 'components';
import Validation from 'components/Validation';
import intl from 'react-intl-universal';
import PhoneInput from 'components/PhoneInput';
import {Toast} from 'antd-mobile/lib/index';


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      userPassword: '',
      passwordAgain: '',
      authCode: '',
      countryCode: '',
      isRemember: false,
      tab: 0,
      sign: null
    };
  }

  componentDidMount() {

  }

  tabClick=(index) => {
    this.NoCaptcha.reset();
    this.setState({
      tab: index,
      userName: '',
      userPassword: '',
      passwordAgain: '',
      authCode: '',
      countryCode: '',
      sign: null
    });
  }

  changeUsername = (value) => {
    this.setState({
      userName: value
    });
  }

  changePassword = (value) => {
    this.setState({
      userPassword: value
    });
  }

  changePasswordAgain=(value) => {
    console.log(value);
    this.setState({
      passwordAgain: value
    });
  }

  onChange=(txt) => {
    this.setState({
      authCode: txt
    });
  }

  handleClick=() => {
    console.log('000000');
    if (this.state.userName === '' || this.state.userName === null) {
      Toast.show('账户不能为空');
      return;
    }
    if (this.state.userPassword === '' || this.state.userPassword === null || this.state.passwordAgain === '' || this.state.passwordAgain === null) {
      Toast.show('密码不能为空');
      return;
    }
    if (this.state.authCode === '' || this.state.authCode === null) {
      Toast.show('验证码不能为空');
      return;
    }
    if (this.state.tab === 0) {
      let params =  {
        'csessionid': this.state.sign.csessionid,
        'email': this.state.userName,
        'password': this.state.userPassword,
        'scene': this.state.sign.scene,
        'sig': this.state.sign.sig,
        'token': this.state.sign.token,
        'verifycode': this.state.authCode,
      };
      this.props.emailRegister(params);
    } else {
      let params = {
        'countryCode': this.state.countryCode,
        'csessionid': this.state.sign.csessionid,
        'loginName': this.state.userName,
        'loginPassword': this.state.userPassword,
        'readNameAuthCode': this.state.authCode,
        'scene': this.state.sign.scene,
        'sig': this.state.sign.sig,
        'token': this.state.sign.token
      };
      this.props.phoneRegister(params);
    }
    this.NoCaptcha.reset();
  }

  onRef = (e) => {
    this.NoCaptcha = e;
  }

  sendCode=() => {
    if (this.state.tab === 0) {
      let params = {
        'email': this.state.userName,
        'otype': 1,
      };
      this.props.sendEmailCode(params);
    } else {
      let params =  {
        'mobile': this.state.userName,
        'country': this.state.countryCode,
        'otype': 1,
      };
      this.props.sendPhoneCode(params);
    }
  }

  render() {
    const {goBack} = this.props;
    console.log(this.state.sign);
    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div style={{display: 'flex', flexDirection: 'column', height: '100%' }}>
          <div>
            <NavBar
              mode="light"
              icon={
                <Flex direction="row" align="center">
                  <Icon type="left" color={'black'} />
                </Flex>
              }
              onLeftClick={goBack}
            />
          </div>
          <div style={styles.container}>

            <Flex style={{display: 'flex', marginBottom: 5}}>
              <div>
                <div style={{margin: 10, fontSize: 18, color: this.state.tab === 0 ? 'black' : '#c3c3c3'}} onClick={() => { this.tabClick(0) }}>邮箱注册</div>
                {this.state.tab === 0 ? <div style={{width: '100%', height: 1, backgroundColor: 'red'}} /> : null}
              </div>
              <div style={{width: 20}} />
              <div>
                <div style={{margin: 10, fontSize: 18, color: this.state.tab === 1 ? 'black' : '#c3c3c3'}} onClick={() => { this.tabClick(1) }}>手机注册</div>
                {this.state.tab === 1 ? <div style={{width: '100%', height: 1, backgroundColor: 'red'}} /> : null}
              </div>
            </Flex>

            <WingBlank>
              <List>
                {this.state.tab === 0 ?
                  <InputItem placeholder="输入账户"
                    value={this.state.userName}
                    onChange={(value) => { this.changeUsername(value) }}>
                    账户:
                  </InputItem>
                  :
                  <PhoneInput callBack={(data) => {
                    this.setState({userName: data.value, countryCode: data.dialCode}); }}
                  />
                }
                <div style={{width: '100%', flexDirection: 'column', alignItems: 'center', marginTop: 15}}>
                  <NoCaptcha onRef={this.onRef}
                    callBack={(data) => { this.setState({sign: data}) }}
                  />
                </div>
                <div style={styles.button}>
                  <input style={{flex: 2}}
                    value={this.state.authCode}
                    placeholder={this.state.tab === 0 ? '输入邮箱验证码' : '输入短信验证码'}
                    onChange={(e) => { this.onChange(e.target.value) }}

                  />
                  <div style={styles.line} />
                  <Validation
                    sendCode={() => { this.sendCode() }}
                  />
                </div>
                <InputItem placeholder="输入密码" type="password"
                  value={this.state.userPassword}
                  onChange={(value) => { this.changePassword(value) }}>
                  密码:
                </InputItem>
                <InputItem placeholder="确认密码" type="password"
                  value={this.state.passwordAgain}
                  onChange={(value) => { this.changePasswordAgain(value) }}>
                  确认密码:
                </InputItem>
              </List>
              <WhiteSpace style={{marginTop: 50}} />
              <Button type="primary" onClick={() => {
                if (this.state.sign) {
                  this.handleClick();
                } else {
                  Toast.show('请进行滑动验证');
                }
              }}>注册</Button>
              <WhiteSpace />
            </WingBlank>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
  phoneRegister: (symbol) => {
    dispatch({ type: 'login/phoneRegister', payload: symbol });
  },
  emailRegister: (symbol) => {
    dispatch({ type: 'login/phoneRegister', payload: symbol });
  },
  sendPhoneCode: (symbol) => {
    dispatch({ type: 'login/sendPhoneCode', payload: symbol });
  },
  sendEmailCode: (symbol) => {
    dispatch({ type: 'login/sendEmailCode', payload: symbol });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    overflow: 'auto',
    flex: 'auto',
    alignItems: 'center',
  },
  button: {
    display: 'flex',
    flexDirection: 'row',
    height: 44,
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 15,
    paddingRight: 15,
    borderBottom: '0.2px solid #EEEEEE'

  },
  line: {
    display: 'flex',
    width: 1,
    height: 20,
    backgroundColor: '#aeb2b5'
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
