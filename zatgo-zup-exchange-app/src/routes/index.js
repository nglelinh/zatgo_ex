import { AuthCheckUtil } from 'utils';

const routes = [
  {
    path: '/',
    onEnter: (nextState, replace) => {
      AuthCheckUtil.authCheck(nextState, replace);
    },
    getComponent(nextState, cb) {
      import('../layouts/AppLayout').then(module => {
        cb(null, module.default);
      });
    },
    indexRoute: {
      getComponent(location, cb) {
        import('./Price/PricePage').then(module => {
          cb(null, module.default);
        });
      }
    },
    childRoutes: [
      {
        path: '/deal',
        getComponent(nextState, cb) {
          import('./Deal').then(module => {
            cb(null, module.default);
          });
        },
      },
      // {
      //   path: '/myAssets',
      //   getComponent(nextState, cb) {
      //     import('./Mine/myAssets/AssetsPage').then(module => {
      //       cb(null, module.default);
      //     });
      //   },
      // },
      {
        path: '/mine',
        getComponent(nextState, cb) {
          import('./Mine').then(module => {
            cb(null, module.default);
          });
        },
      },
    ]
  },
  {
    path: '/auth',
    name: 'IndexPage',
    getComponent(nextState, cb) {
      import('./Auth').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/login',
    getComponent(nextState, cb) {
      import('./Login/LoginPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/registerPage',
    getComponent(nextState, cb) {
      import('./Login/RegisterPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/forgoPwdPage',
    getComponent(nextState, cb) {
      import('./Login/ForgoPwdPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/deal',
    getComponent(nextState, cb) {
      import('./Deal').then(module => {
        cb(null, module.default);
      });
    }
  },
  {
    path: '/priceDetail',
    getComponent(nextState, cb) {
      import('./Price/PriceDetailPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/optional',
    getComponent(nextState, cb) {
      import('./Optional/OptionalAddPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/order',
    getComponent(nextState, cb) {
      import('./Order/OrderPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/about',
    getComponent(nextState, cb) {
      import('./Mine/about/AboutPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/invite',
    getComponent(nextState, cb) {
      import('./Mine/invite/InvitePage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/myAssets',
    getComponent(nextState, cb) {
      import('./Mine/myAssets/AssetsPage').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/coinsOrder',
    getComponent(nextState, cb) {
      import('./Mine/assetOrder/CoinsOrder').then(module => {
        cb(null, module.default);
      });
    },
  },
  {
    path: '/leverageOrder',
    getComponent(nextState, cb) {
      import('./Mine/assetOrder/LeverageOrder').then(module => {
        cb(null, module.default);
      });
    },
  }, {
    path: '/leverBorrow',
    getComponent(nextState, cb) {
      import('./Mine/assetOrder/LeverBorrow').then(module => {
        cb(null, module.default);
      });
    },
  }, {
    path: '/MyOrder',
    getComponent(nextState, cb) {
      import('./Mine/myOrder/MyOrder').then(module => {
        cb(null, module.default);
      });
    },
  }, {
    path: '/leverTransfer',
    getComponent(nextState, cb) {
      import('./Mine/assetOrder/LeverTransfer').then(module => {
        cb(null, module.default);
      });
    },
  }, {
    path: '/sharePage',
    getComponent(nextState, cb) {
      import('./Mine/sharePage/SharePage').then(module => {
        cb(null, module.default);
      });
    },
  }
];
export default routes;
