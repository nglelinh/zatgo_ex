import React, { Component } from 'react';
import { DocumentTitle, ListView } from 'components';
import { NavBar, Icon, Flex, Tabs } from 'antd-mobile';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import Circle from 'react-circle';
import intl from 'react-intl-universal';
import dayjs from 'dayjs';
import Big from 'big.js';

const tabs = () => (
  [
    { title: intl.get('order.allorder'), key: '1' },
    { title: intl.get('order.currentorder'), key: '2' },
  ]);
const orderTitle = [
  ['order.market', 'order.price', 'order.number'],
  ['order.market', 'order.average', 'order.amont'],
  ['order.market', 'order.price', 'order.number'],
];

const statusText = {
  'CANCELED': 'order.cenceled',
  'FILLED': 'order.filled',
  'EXCEPTION': 'order.exception',
};

const listParam =
{
  'isReturnTradeDetail': false,
  'orderStatuss': [
    'INIT', 'NEW', 'FILLED', 'PART_FILLED', 'CANCELED', 'PENDING_CANCEL', 'EXCEPTION'
  ],
  'pageNo': 1,
  'pageSize': 10,
};

let data = {baseCoin: 'but', quoteCoin: 'btc', symbol: 'butbtc'};
let pageNo = 1;
class OrderPage extends Component {
  state = {
    tab: 0,
    dataArr: []
  }

  componentDidMount() {
    // data = this.props.location.state.params;
    this.getOrderList();
  }

  /**/
  getOrderList = () => {
    listParam['pageNo'] = pageNo;
    listParam['baseCoin'] = data.baseCoin;
    listParam['quoteCoin'] = data.quoteCoin;
    listParam['symbol'] = data.symbol;

    this.props.getOrderList(listParam);
  }

  changeState = (tab, index) => {
    if (index === 0) {
      listParam['orderStatuss'] = ['INIT', 'NEW', 'FILLED', 'PART_FILLED', 'CANCELED', 'PENDING_CANCEL', 'EXCEPTION'];
    } else if (index === 1) {
      listParam['orderStatuss'] = ['INIT', 'NEW', 'PART_FILLED', 'PENDING_CANCEL'];
    }
    listParam['pageNo'] = 1;
    pageNo = 1;
    this.getOrderList();
    this.setState({
      tab: index,
    });
  }

  /* 获取更多 */
  getData = () => {
    pageNo = pageNo + 1;
    this.getOrderList();
  }

  getFullNum = (num) => {
    // 处理非数字
    if (isNaN(num)) { return num }

    // 处理不需要转换的数字
    let str = String(num);
    if (!/e/i.test(str)) { return num }

    return (num).toFixed(18).replace(/\.?0+$/, '');
  }

  OrderItem = (data) => {
    let item = data.itemInfo;
    let remainVolume = item.remainVolume ? Number(item.remainVolume) : 0;
    let volume = item.volume ? new Big(item.volume) : 0;
    let progress = parseFloat((volume.minus(remainVolume) / volume) * 100).toFixed(1);
    console.log(progress);
    return (
      <div style={styles.container}>
        {
          this.state.tab === 0 ?
            <div style={{
              width: '15%'
            }}>
              <Circle
                animate={true}
                animationDuration="1s"
                responsive={false} // Boolean: Make SVG adapt to parent size
                size={50} // Number: Defines the size of the circle.
                lineWidth={40} // Number: Defines the thickness of the circle's stroke.
                progress={Number(progress) > 0 ? progress : 0} // Number: Update to change the progress and percentage.
                progressColor="#1D74A8"  // String: Color of "progress" portion of circle.
                bgColor="whitesmoke" // String: Color of "empty" portion of circle.
                textColor="red" // String: Color of percentage text color.
                textStyle={{ font: 'bold 5rem Helvetica, Arial, sans-serif' }}
                percentSpacing={20} // Number: Adjust spacing of "%" symbol and number.
                roundedStroke={true} // Boolean: Rounded/Flat line ends
                showPercentage={true} // Boolean: Show/hide percentage.
                showPercentageSymbol={true} // Boolean: Show/hide only the "%" symbol.
              />
            </div> : null
        }
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'left',
          width: '35%'
        }}>
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'left',
          }}>
            <div style={styles.font16}>{`${item.baseCoin.toUpperCase()}/${item.quoteCoin.toUpperCase()}`}</div>
            <div style={{ fontSize: 11, marginTop: 3, color: item.side === 'SELL' ? '#E26A6A' : '#35BAA0' }}>
              ({item.side === 'BUY' ? intl.get('deal.buy2') : intl.get('deal.sell2')})
            </div>
          </div>
          <div style={{
            paddingTop: '10px',
            color: '#a3a3a3',
            fontSize: '12px'
          }}>{item.createDate && dayjs(new Date(item.createDate)).format('MM-DD HH:mm:ss')}</div>
        </div>

        <div style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'left',
          width: '30%',
          flex: '1 1 auto'
        }}>
          <div style={styles.font14}>
            {this.getFullNum(Number(item.price))}
          </div>
          {
            this.state.tab === 1 ?
              <div style={styles.font14Padding}>
                {this.getFullNum(Number(item.avgPrice))}
              </div> : null
          }

        </div>
        <div style={{ width: '20%' }}>
          <div style={{ ...styles.font14, textAlign: 'right' }}> {item.volume}</div>
          {item.status !== 'CANCELED' && item.status !== 'FILLED' && item.status !== 'EXPIRED' ?
            <div style={{ ...styles.button, marginTop: 10, }}
              onClick={() => {
                let params = {
                  'baseCoin': item.baseCoin,
                  'orderId': item.orderId,
                  'quoteCoin': item.quoteCoin,
                  'symbol': `${item.baseCoin}${item.quoteCoin}`
                };
                this.props.cancelOrder(params, listParam);
              }}>
              {intl.get('deal.cancel')}
            </div> :
            <div style={{ marginTop: 10, textAlign: 'right', color: '#797F85'}}>
              {statusText[item.status] ? intl.get(statusText[item.status]) : ''}
            </div>
          }
        </div>
      </div>

    );
  }


  OrderListTitle = () => {
    return (
      <div style={{
        display: 'flex',
        justifyContent: 'space-between',
        background: '#fff',
        color: '#6e889e',
        padding: '10px',
        fontSize: '12px'
      }}>
        {
          this.state.tab === 0 ?
            <div style={{ flex: '0 0 15%' }}>{null}</div> : null
        }
        <div style={{ width: '35%', textAlign: 'center' }}>{intl.get(orderTitle[this.state.tab][0])}</div>
        <div style={{ flex: '1 1 auto', width: '30%', textAlign: 'left' }}>{intl.get(orderTitle[this.state.tab][1])}</div>
        <div style={{ width: '20%', textAlign: 'right' }}>{intl.get(orderTitle[this.state.tab][2])}</div>
      </div>
    );
  }

  render() {
    const { goBack, orderList, listLoadMore } = this.props;
    return (
      <DocumentTitle title={intl.get('title.order')}>
        <div>
          {global.exchangeType ?
            null :
            <NavBar
              mode="dark"
              icon={
                <Flex direction="row" align="center">
                  <Icon type="left" />
                </Flex>
              }
              onLeftClick={goBack}
            >{intl.get('order.manage')}</NavBar>

          }
          <Tabs
            tabs={tabs(1)}
            initialPage={0}
            useOnPan={false}
            tabBarActiveTextColor="#35BAA0"
            tabBarInactiveTextColor="#797F85"
            onChange={this.changeState}
          >
            <div style={{ background: '#fff' }}>
              <this.OrderListTitle />
              <ListView
                data={orderList}
                ListItem={this.OrderItem}
                getData={this.getData}
                offsetHeight={global.exchangeType ? 70 : 150}
                reachEnd={listLoadMore}
                disableLoadMore={!listLoadMore}
              />
            </div>
          </Tabs>
        </div>
      </DocumentTitle>
    );
  }
}
const mapStateToProps = (state) => ({
  orderList: state.deal.orderList,
  listLoadMore: state.deal.listLoadMore,
  loading: state.loading.effects['price/getTicker']
});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
  getOrderList: (payload) => {
    dispatch({ type: 'deal/getOrderList', payload: payload });
  },
  cancelOrder: (payload) => {
    dispatch({ type: 'deal/cancelOrder', payload: payload });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderPage);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: '15px 10px 10px',
    borderBottom: '1px solid rgba(255,255,255,0.5)'
  },
  button: {
    display: 'block',
    float: 'right',
    width: 44,
    textAlign: 'center',
    color: '#7d96ac',
    padding: '2px 0',
    border: '1px solid #D9D9D9'
  },
  font11: {
    color: '#797F85', fontSize: 11, marginTop: 8
  },
  font14: {
    fontSize: 14
  },
  font14Padding: {
    fontSize: 14, paddingTop: '10px'
  },
  font16: {
    fontSize: 16
  },
};
