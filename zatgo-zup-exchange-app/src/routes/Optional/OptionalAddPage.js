/*
 * @Author: lsl
 * @Date: 2018-08-16 09:30:43
 * @Last Modified by: lsl
 * @Last Modified time: 2018-09-05 13:36:32
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import intl from 'react-intl-universal';
import { Flex, Toast } from 'antd-mobile';
import { SearchBar, ListView } from 'components';

const selectState = (quoteCoin, baseCoin) => {
  let dataSource = JSON.parse(localStorage.getItem('symbols')) ? JSON.parse(localStorage.getItem('symbols')) : {};
  console.log(dataSource);
  let key = `${baseCoin}${quoteCoin}`;
  if (key in dataSource) {
    return false;
  } else {
    return true;
  }
};

const SymbolItem = (props) => {
  const { itemInfo, onItemClick, addFavorites, deleteFavorites} = props;
  let state = selectState(itemInfo.quoteCoin, itemInfo.baseCoin);
  return (
    <Flex align="center" justify="between">
      <div
        onClick={() => onItemClick(itemInfo)}
        style={{
          flex: 1,
          margin: 10,
          fontSize: 16,
          color: '#323B43',
        }}>
        {itemInfo.baseCoin.toUpperCase()}
        <font style={{ color: '#797F85', fontSize: 11, marginLeft: 7 }}>
          {`/${itemInfo.quoteCoin.toUpperCase()}`}
        </font>
      </div>
      {state ?
        <div style={{
          display: 'flex',
          alignitems: 'center',
          justifycontent: 'center',
          margin: 10
        }} onClick={() => { addFavorites(itemInfo.baseCoin, itemInfo.quoteCoin) }}>
          <img src={require('../../assets/deal/shoucang.svg')}
            style={{width: 20, height: 20, marginRight: 12, marginLeft: 10}} alt=""
          />
        </div>
        :
        <div style={{
          display: 'flex',
          alignitems: 'center',
          justifycontent: 'center',
          margin: 10
        }} onClick={() => { deleteFavorites(itemInfo.baseCoin, itemInfo.quoteCoin) }}>
          <img
            src={require('../../assets/deal/shoucang2.svg')}
            style={{width: 20, height: 20, marginRight: 12, marginLeft: 10}} alt=""
          />
        </div>}
    </Flex>
  );
};

class OptionalAddPage extends Component {

  state = { searchSymbols: [] }

  onSearch = (txt) => {
    if (txt.length > 0) {
      const input = txt.toLowerCase();
      const {market} = this.props;
      const temp = Object.values(market).reduce((pre, cur) => {
        return pre.concat(cur);
      });
      const searchSymbols = temp.filter((item) => {
        const {baseCoin} = item;
        return baseCoin.toLowerCase().indexOf(input) !== -1;
      });
      this.setState({searchSymbols});
    } else {
      this.setState({searchSymbols: []});
    }
  }

  // 点击子项
  onItemClick = (item) => {
    const path = {
      pathname: '/priceDetail',
      state: item
    };
    console.log(item);
    this.props.changeUrl(path);
  }

  // 添加到自选
  addFavorites = (baseCoin, quoteCoin) => {
    console.log(baseCoin, quoteCoin);
    this.props.addOptional({baseCoin: baseCoin, quoteCoin: quoteCoin});
    this.setState({a: 1});
    Toast.info('添加自选成功', 1);
  }

  // 删除自选
  deleteFavorites = (baseCoin, quoteCoin) => {
    this.props.deleteOptional({baseCoin: baseCoin, quoteCoin: quoteCoin});
    this.setState({a: 2});
    Toast.info('删除自选成功', 1);
  }

  render() {
    const { goBack } = this.props;
    return (
      <div>
        <SearchBar
          cancelText={intl.get('com.cancel')}
          placeholder={intl.get('price.search')}
          maxLength={20}
          onChange={this.onSearch}
          showCancelButton
          onCancel={() => { goBack() }}
        />
        <ListView
          ref={listview => { this.lv = listview }}
          data={this.state.searchSymbols}
          ListItem={SymbolItem}
          onItemClick={this.onItemClick}
          addFavorites={this.addFavorites}
          deleteFavorites={this.deleteFavorites}
          offsetHeight={60}
          noData={intl.get('com.noDataSearch')}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  coinPairs: state.app.coinPairs,
  market: state.price.market,

});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  },
  goBack: () => {
    dispatch(routerRedux.goBack());
  },
  addOptional: (symbol) => {
    dispatch({ type: 'price/addOptional', payload: symbol });
  },
  deleteOptional: (symbol) => {
    dispatch({ type: 'price/deleteOptional', payload: symbol });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(OptionalAddPage);
