/**
 * Created by zhoujianxin on 2018/9/7.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
// import {Flex} from 'antd-mobile';
// import intl from 'react-intl-universal';
import DealCss from '../Deal/DealPage.css';

let storage = window.localStorage;

class AuthPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiKey: '6f12d25f-0378ee13-4af02aef-4c12a',
      secretKey: '6c902510-ece567fc-c016503d-88ed4',
    };
  }

  componentDidMount() {
    // storage.clear();
    if (!window.localStorage) {
      alert('浏览器不支持localstorage');
      return false;
    } else {
      // 主逻辑业务
      let apiKey = storage['apiKey'];
      let secretKey = storage['secretKey'];
      if (apiKey && secretKey) {
        this.props.changeUrl({ pathname: '/' });
      }
    }
  }


  // 设置inputValue
  apiKeyChange = (e) => {
    this.setState({
      apiKey: e.target.value
    });
  }
  // 设置textareaValue
  secretKeyChange = (e) => {
    this.setState({
      secretKey: e.target.value
    });
  }

  onAuthorization = () => {
    if (this.state.apiKey.length > 0 && this.state.secretKey.length > 0) {
      storage['apiKey'] = this.state.apiKey;
      storage['secretKey'] = this.state.secretKey;
      this.props.changeUrl({ pathname: '/' });
    }
  }

  render() {
    return (
      <div style={styles.container}>

        <div style={{ marginLeft: 20, marginRight: 20, width: '80%', marginTop: '20%' }}>
          <div>Bytex提供的apiKey和secretKey非常重要，请小心保管，不要外泄，确保资金安全！建议您通过QQ，微信等较为安全的工具发送Key，防止泄漏。</div>
        </div>

        <div style={{ margin: 20, width: '80%', }}>
          <div>请输入您的apiKey和secretKey:</div>
        </div>

        <div style={{...styles.apiKeyInput, backgroundColor: 'white'}}>
          <img
            src={require('../../assets/deal/apiKey.svg')}
            style={{width: 20, height: 20, marginRight: 12, marginLeft: 10}} alt=""
          />
          <input type="text" value={this.state.apiKey}
            style={{ width: '80%', border: 'none', fontSize: 16 }}
            onChange={this.apiKeyChange}
            placeholder={'apiKey'}
          />
        </div>

        <div style={{...styles.apiKeyInput, marginTop: 15, backgroundColor: 'white'}}>
          <img
            src={require('../../assets/deal/secretKey.svg')}
            style={{width: 20, height: 20, marginRight: 12, marginLeft: 10}} alt=""
          />
          <input type="text" value={this.state.secretKey}
            onChange={this.secretKeyChange}
            style={{ width: '80%', border: 'none', fontSize: 16 }}
            placeholder={'secretKey'}
          />
        </div>

        <div className={DealCss.btn3}
          style={{...styles.apiKeyInput, marginTop: 15, justifyContent: 'center'}}
          onClick={this.onAuthorization}
        >
          <div style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
            授权交易
          </div>
        </div>

        <div style={{ margin: 10, width: '70%', }}>
          <div style={{color: '#ababab', fontSize: 12}}>用户密钥经过多重加密且仅存储在本设备内，请放心使用！</div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  changeUrl: (url) => {
    dispatch(routerRedux.push(url));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);

const styles = {
  container: {
    display: 'flex',
    height: '100vh',
    flexDirection: 'column',
    paddingTop: 5,
    paddingBottom: 5,
    alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: '#f2f3f5'
  },
  apiKeyInput: {
    display: 'flex',
    width: '80%',
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#D9D9D9',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: '22px',
    height: 44,
    marginLeft: 10,
    marginRight: 10,
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
