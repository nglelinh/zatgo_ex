import 'core-js/es6/promise';
import 'core-js/es6/set';
import 'core-js/es6/map';
import 'raf/polyfill';
import dva from 'dva';
import ReactDOM from 'react-dom';
import FastClick from 'fastclick';
import createLoading from 'dva-loading';
import { browserHistory, Router } from 'dva/router';
import './index.css';
import { LocaleProvider, messages } from 'locales';
import routes from '../routes/index';

const RouterConfig = ({ history, app }) => {
  return (
    <Router history={history} routes={routes} />
  );
};

// 1. Initialize
const app = dva({
  history: browserHistory,
  initialState: {},
});

// 2. Plugins
app.use(createLoading());

// 3. Model
app.model(require('../models/login').default);
app.model(require('../models/price').default);
app.model(require('../models/app').default);
app.model(require('../models/deal').default);
app.model(require('../models/nativeState').default);
app.model(require('../models/mine').default);
app.model(require('../models/asset').default);

// 4. Router
app.router(RouterConfig);

// 5. Start
const App = app.start();

ReactDOM.render(
  <LocaleProvider
    locale="zh-CN"
    messages={messages}
  >
    <App />
  </LocaleProvider>,
  document.getElementById('root')
);

FastClick.attach(document.body, {});
