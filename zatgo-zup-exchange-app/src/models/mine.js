// import tradeTicker from '../../mock/tradeTicker.json';
// import mockMarket from '../../mock/market.json';
// import mockTicker from '../../mock/ticker.json';

import {getUserInfo, getCoinData, getLeverData, getOrderMangerList} from '../services/mine';
import {Toast} from 'antd-mobile';

export default {
  namespace: 'mine',
  state: {
    orderMangerData: [],
    orderMangerMore: true,
    orderLoading: true,
    userInfo: {},
    assetInfo: {},
    assetList: [],
    leverAssetInfo: {},
    leverAssetList: [],
  },
  reducers: {
    saveOrderData(state, { payload }) {
      return { ...state, orderData: payload };
    },
    saveLoading(state, { payload }) {
      return { ...state, ...payload };
    },
    saveOrderManagerData(state, { payload }) {
      return { ...state, ...payload };
    },
    saveAssetInfo(state, { payload }) {
      return { ...state, assetInfo: payload };
    },
    saveAssetList(state, { payload }) {
      return { ...state, assetList: payload};
    },
    saveLeverAssetInfo(state, { payload }) {
      return { ...state, leverAssetInfo: payload };
    },
    saveLeverAssetList(state, { payload }) {
      return { ...state, leverAssetList: payload};
    },
    saveUserInfo(state, { payload }) {
      return { ...state, userInfo: payload};
    }
  },
  effects: {
    * getUserInfo({ payload }, { call, put}) {
      try {
        const response = (yield call(getUserInfo)).data;
        yield put({type: 'saveUserInfo', payload: response.user});
      } catch (err) {
        console.log(err);
      }
    },
    * getAsset({ payload }, { call, put}) {
      try {
        const response = (yield call(getCoinData)).data;
        console.log(response);
        let data = {};
        let dataList = response.list;
        for (let key in response) {
          if (key !== 'list') {
            data[key] = response[key];
          }
        }
        yield put({type: 'saveAssetInfo', payload: data});
        yield put({type: 'saveAssetList', payload: dataList});
      } catch (err) {
        console.log(err);
      }
    },
    * getLeverAsset({ payload }, { call, put}) {
      try {
        const response = (yield call(getLeverData)).data;
        console.log(response);
        let data = {};
        let dataList = response.accountBalanceData;
        for (let key in response) {
          if (key !== 'accountBalanceData') {
            data[key] = response[key];
          }
        }

        yield put({type: 'saveLeverAssetInfo', payload: data});
        yield put({type: 'saveLeverAssetList', payload: dataList});
      } catch (err) {
        console.log(err);
      }
    },
    * getOrderMangerList({ payload }, { call, put, select}) {
      try {
        console.log(payload);
        if (payload['pageNum'] < 2)Toast.loading('loading...');
        const response = yield call(getOrderMangerList, payload);
        const arr = response.data ? response.data : { list: [] };
        if (payload['pageNum'] < 2) {
          yield put({ type: 'saveOrderManagerData', payload: { orderMangerData: arr.list, orderMangerMore: arr.list.length >= 10 }});
        } else {
          const orderListArr = JSON.parse(JSON.stringify(yield select(state => state.mine.orderMangerData)));
          if (arr.list.length < 10) {
            yield put({ type: 'saveOrderManagerData', payload: {orderMangerData: orderListArr.concat(arr.list), orderMangerMore: false }});
          } else {
            yield put({ type: 'saveOrderManagerData', payload: {orderMangerData: orderListArr.concat(arr.list), orderMangerMore: true }});
          }
        }
        Toast.hide();
        yield put({ type: 'saveLoading', payload: {orderLoading: false} });
      } catch (err) {
        Toast.hide();
        yield put({ type: 'saveLoading', payload: {orderLoading: false} });
        console.log(err);
      }
    },
    * setOrderMangerList({ payload }, { call, put}) {
      try {
        yield put({ type: 'saveOrderManagerData', payload: {orderMangerData: [], orderMangerMore: true }});

      } catch (err) {
        console.log(err);
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      // subscriptions
    }
  }
};
