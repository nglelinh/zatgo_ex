import queryString from 'query-string';
import {getCoinPairs, queryRate} from '../services/app';

const mockCloudUserId = '65c2c3a074774f8090a1e4544a56632a';

export default {
  namespace: 'app',
  state: {
    tab: 'home',
    rates: [],
    coinPairs: {},
    quoteCoins: [],
  },
  effects: {
    * getBasicData({payload = {}}, {call, put, all}) {
      try {
        const [coinPair, rate] = yield all([call(getCoinPairs), call(queryRate)]);
        // console.log('汇率：', JSON.stringify(rate));
        const {quoteCoinList, pairs} = coinPair.data;
        const quoteCoins = quoteCoinList ? quoteCoinList : [];
        const coinPairs = pairs ? pairs : {};
        const {rates} = rate.data;
        const coinrates = rates ? rates : [];
        yield put({type: 'saveBasicData', payload: {coinPairs, rates: coinrates, quoteCoins}});
      } catch (err) {
        console.log('异常', err);
      }
    },
    * getCoinPairs({payload = {}}, {call, put}) {
      try {
        const response = (yield call(getCoinPairs)).data;
        yield put({type: 'saveCoinPairs', payload: {coinPairs: response.rates}});
      } catch (err) {
        console.log('异常', err);
      }
    },
    * queryRate({payload = {}}, {call, put}) {
      try {
        const response = (yield call(queryRate)).data;
        yield put({type: 'saveRates', payload: response.rates});
      } catch (err) {
        console.log('异常', err);
      }
    },
  },

  reducers: {
    changeTab(state, action) {
      return {...state, tab: action.payload};
    },
    saveBasicData(state, action) {
      return {...state, ...action.payload};
    },
    saveRates(state, action) {
      return {...state, rates: action.payload};
    },
    saveCoinPairs(state, action) {
      return {...state, coinPairs: action.payload};
    },
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        console.log('路径：', pathname);
        const parsed = queryString.parse(window.location.search);
        const {exchangeType, token, cloudUserId} = parsed;
        if (pathname === '/' || pathname === '/price') {
          console.log(token);
          global.token = token ? token : '';
          global.cloudUserId = cloudUserId ? cloudUserId : mockCloudUserId;
        }
        if (pathname === '/' && exchangeType) {
          global.exchangeType = exchangeType;
        }
        // global.exchangeType = 'huobi_hadax';
        switch (pathname) {
          case '/':
            dispatch({type: 'getBasicData'});
            // dispatch({type: 'mine/getUserInfo'});
            dispatch({type: 'changeTab', payload: 'price'});
            break;
          case '/price':
            dispatch({type: 'getBasicData'});
            dispatch({type: 'changeTab', payload: 'price'});
            break;
          case '/deal':
            dispatch({type: 'changeTab', payload: 'deal'});
            break;
          case '/mine':
            dispatch({type: 'queryRate'});
            dispatch({type: 'mine/getUserInfo'});
            dispatch({type: 'changeTab', payload: 'mine'});
            break;
          default:
            break;
        }
      });
    },
  }
};
