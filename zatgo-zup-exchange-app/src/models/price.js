import { getMarket, getTradeTicker, getTicker, getKLine, onCloseWebSocket, onCloseAllWebSocket} from '../services/app';
import {dealWithData} from '../constants/ArrayDataUtils';

// import tradeTicker from '../../mock/tradeTicker.json';
// import mockMarket from '../../mock/market.json';
// import mockTicker from '../../mock/ticker.json';
// import mockKLine from '../../mock/kline.json';

const isEqual = (p1, p2) => {
  return p1.toUpperCase() === p2.toUpperCase();
};

export default {
  namespace: 'price',
  state: {
    market: {},
    ticker: null,
    tradeTickers: [],
    kline: [],
    optionals: [],
    klineLoading: false,
    marketLoading: true,
  },
  reducers: {
    saveMarket(state, { payload }) {
      return { ...state, market: payload };
    },
    saveTicker(state, { payload }) {
      return { ...state, ticker: payload };
    },
    saveTradeTicker(state, { payload }) {
      return { ...state, tradeTickers: payload };
    },
    saveKLine(state, { payload }) {
      return { ...state, kline: payload };
    },
    saveOptional(state, { payload }) {
      return { ...state, optionals: payload };
    },
    updateKLineLoading(state, { payload }) {
      return { ...state, klineLoading: payload };
    },
    updateMarketLoading(state, { payload }) {
      return { ...state, marketLoading: payload };
    },
  },
  effects: {
    * onCloseWebSocket({payload}, {call}) {
      try {
        // console.log('onCloseWebSocket')
        yield call(onCloseWebSocket);
      } catch (err) {
        console.log(err);
      }
    },
    * onCloseAllWebSocket({payload}, {call}) {
      try {
        // console.log('onCloseWebSocket')
        yield call(onCloseAllWebSocket);
      } catch (err) {
        console.log(err);
      }
    },
    * getMarket({ payload }, { call, put, take, select }) {
      // yield put({ type: 'saveMarket', payload: mockMarket.data });
      try {
        const lastData = JSON.parse(JSON.stringify(yield select(state => state.app.coinPairs)));
        yield put({type: 'updateMarketLoading', payload: true});
        let params = payload;
        params['event'] = 'REQ';
        const channel = yield call(getMarket, payload);
        const res = (yield take(channel)).data;
        // console.log('24小时:', JSON.stringify(res));
        const {data, type} = res;
        if (isEqual(payload.type, type)) {
          if (JSON.stringify(lastData) === '{}') {
            yield put({type: 'saveMarket', payload: data});
          } else {
            let dealData = {};
            // eslint-disable-next-line
            for (let key in lastData) {
              let obj = key.toLocaleLowerCase();
              dealData[obj] = data[obj] && data[obj].length >= 0 ? dealWithData(lastData[key], data[obj], 'symbol') : lastData[key];
            }
            yield put({type: 'saveMarket', payload: dealData});
          }
          yield put({type: 'updateMarketLoading', payload: false});
        }

        params['event'] = 'SUB';
        const channelWS = yield call(getMarket, payload);
        while (true) {
          const res = (yield take(channelWS)).data;
          // console.log('24小时:', JSON.stringify(res));
          const {data, type, operateType} = res;
          const lastData = JSON.parse(JSON.stringify(yield select(state => state.price.market)));
          if (isEqual(payload.type, type)) {
            if (operateType === 'increment' || operateType === 'INCREMENT') {
              let dealData = {};
              // eslint-disable-next-line
              for (let obj in lastData) {
                dealData[obj] = data[obj] && data[obj].length >= 0 ? dealWithData(lastData[obj], data[obj], 'symbol', true) : lastData[obj];
              }
              yield put({type: 'saveMarket', payload: dealData});
            } else {
              console.log('FULL');
              yield put({type: 'saveMarket', payload: data});
            }
          }
        }
      } catch (err) {
        console.log(err);
      }
    },
    * getTicker({ payload }, { call, put, take, select }) {
      // yield put({ type: 'saveTicker', payload: mockTicker.data });
      try {
        yield put({type: 'saveTicker', payload: null});
        let params = payload;
        params['event'] = 'REQ';
        const channel = yield call(getTicker, params);
        const res = (yield take(channel)).data;
        // console.log('24小时:', JSON.stringify(res));
        const {data, symbol, type} = res;
        if (isEqual(payload.symbol, symbol) && isEqual(payload.type, type)) {
          yield put({type: 'saveTicker', payload: data});
        }
        params['event'] = 'SUB';
        const channelWS = yield call(getTicker, params);
        while (true) {
          const res = (yield take(channelWS)).data;
          // console.log('24小时:', JSON.stringify(res));
          const {data, symbol, type} = res;
          if (isEqual(payload.symbol, symbol) && isEqual(payload.type, type)) {
            yield put({type: 'saveTicker', payload: data});
          }
        }
      } catch (err) {
        console.log(err);
      }
    },
    * getTradeTicker({ payload }, { call, put, take, select }) {
      // yield put({ type: 'saveTradeTicker', payload: tradeTicker.data });
      try {
        let params = payload;
        params['event'] = 'REQ';
        const channel = yield call(getTradeTicker, params);
        const res = (yield take(channel)).data;
        // console.log('交易:', JSON.stringify(res));
        const {data, symbol, type} = res;
        if (isEqual(payload.symbol, symbol) && isEqual(payload.type, type)) {
          yield put({type: 'saveTradeTicker', payload: data});
        }
        params['event'] = 'SUB';
        const channelWS = yield call(getTradeTicker, params);
        while (true) {
          const res = (yield take(channelWS)).data;
          // console.log('交易:', JSON.stringify(res));
          const {data, symbol, type, operateType} = res;
          const lastData = JSON.parse(JSON.stringify(yield select(state => state.price.tradeTickers)));
          if (isEqual(payload.symbol, symbol) && isEqual(payload.type, type)) {
            if (operateType === 'increment' || operateType === 'INCREMENT') {
              console.log('increment');
              let dealData = dealWithData(lastData, data, 'id', true);
              yield put({type: 'saveTradeTicker', payload: dealData});
            } else {
              console.log('FULL');
              yield put({type: 'saveTradeTicker', payload: data});
            }
          }
        }
      } catch (err) {
        console.log(err);
      }
    },
    * getKLine({ payload }, { call, put, take, select }) {
      // yield put({ type: 'saveKLine', payload: mockKLine.data });
      try {
        let params = payload;
        params['event'] = 'REQ';
        yield put({type: 'updateKLineLoading', payload: true});
        const channel = yield call(getKLine, params);
        const res = (yield take(channel)).data;
        // console.log('KLINE:', JSON.stringify(res));
        const {data, symbol, type} = res;
        if (isEqual(payload.symbol, symbol) && isEqual(payload.type, type)) {
          yield put({type: 'saveKLine', payload: data});
          yield put({type: 'updateKLineLoading', payload: false});
        }

        params['event'] = 'SUB';
        const channelWS = yield call(getKLine, params);
        while (true) {
          const res = (yield take(channelWS)).data;
          // console.log('KLINE:', JSON.stringify(res));
          const {data, symbol, type, operateType} = res;
          const lastData = JSON.parse(JSON.stringify(yield select(state => state.price.kline)));
          if (isEqual(payload.symbol, symbol) && isEqual(payload.type, type)) {
            yield put({type: 'updateKLineLoading', payload: false});
            if (operateType === 'increment' || operateType === 'INCREMENT') {
              console.log('increment');
              let dealData = dealWithData(lastData, data, 'timePoint');
              yield put({type: 'saveKLine', payload: dealData});
            } else {
              console.log('FULL');
              yield put({type: 'saveKLine', payload: data});
            }
          }
        }
      } catch (err) {
        console.log(err);

      }
    },
    * addOptional({ payload }, { call, put, take, select }) {
      try {
        if (!window.localStorage) {
          alert('浏览器不支持localstorage');
          return false;
        } else {
          // 主逻辑业务
          let data = {};
          let symbol = `${payload.baseCoin}${payload.quoteCoin}`;
          if (JSON.parse(localStorage.getItem('symbols')) !== null) {
            data = JSON.parse(localStorage.getItem('symbols'));
            data[symbol] = payload;
          } else {
            data[symbol] = payload;
          }
          console.log(data);
          localStorage.setItem('symbols', JSON.stringify(data));
          yield put({type: 'saveOptional', payload: [12]});
        }
      } catch (err) {
        console.log(err);
      }
    },
    * deleteOptional({ payload }, { call, put, take, select }) {
      try {
        if (!window.localStorage) {
          alert('浏览器不支持localstorage');
          return false;
        } else {
          // 主逻辑业务
          let symbol = `${payload.baseCoin}${payload.quoteCoin}`;
          let data = JSON.parse(localStorage.getItem('symbols'));
          delete data[symbol];
          localStorage.setItem('symbols', JSON.stringify(data));
          yield put({type: 'saveOptional', payload: [12]});

        }
      } catch (err) {
        console.log(err);
      }
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      // subscriptions
    }
  }
};
