// import tradeTicker from '../../mock/tradeTicker.json';
// import mockMarket from '../../mock/market.json';
// import mockTicker from '../../mock/ticker.json';
// import mockKLine from '../../mock/kline.json';


export default {
  namespace: 'native',
  state: {
    tabState: null,
    maketTab: null,
    coinsData: {},
    leverageData: {},
    hiddenAsset: true,
  },
  reducers: {
    saveTabState(state, { payload }) {
      return { ...state, tabState: payload };
    },
    saveMarketTabState(state, { payload }) {
      return { ...state, maketTab: payload };
    },
    saveCoinsData(state, { payload }) {
      return { ...state, coinsData: payload };
    },
    saveLeverageData(state, { payload }) {
      return { ...state, leverageData: payload };
    },
    saveHiddenAsset(state, { payload }) {
      return { ...state, hiddenAsset: payload };
    },

  },
  effects: {
    * setTabState({ payload }, { put}) {
      try {
        console.log(payload);
        yield put({type: 'saveTabState', payload: payload});
      } catch (err) {
        console.log(err);
      }
    },
    * setMarketTabState({ payload }, { put}) {
      try {
        console.log(payload);
        yield put({type: 'saveMarketTabState', payload: payload});
      } catch (err) {
        console.log(err);
      }
    },
    * setCoinsData({ payload }, { put}) {
      try {
        console.log(payload);
        yield put({type: 'saveCoinsData', payload: payload});
      } catch (err) {
        console.log(err);
      }
    },
    * setLeverageData({ payload }, { put}) {
      try {
        console.log(payload);
        yield put({type: 'saveLeverageData', payload: payload});
      } catch (err) {
        console.log(err);
      }
    },
    * setHiddenAsset({ payload }, { put}) {
      try {
        yield put({type: 'saveHiddenAsset', payload: payload});
      } catch (err) {
        console.log(err);
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      // subscriptions
    }
  }
};
