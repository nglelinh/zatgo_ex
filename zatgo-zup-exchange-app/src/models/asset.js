// import tradeTicker from '../../mock/tradeTicker.json';
// import mockMarket from '../../mock/market.json';
// import mockTicker from '../../mock/ticker.json';
import {
  getWithdrawList,
  getDepositList,
  leverBorrow,
  getLeverBorrowList,
  leverTransferRecharget,
  leverTransferWithdrawal,
  getLeverTransferList,
  leverBorrowRepayment
} from '../services/mine';
import {Toast} from 'antd-mobile/lib/index';

export default {
  namespace: 'asset',
  state: {
    coinWithdrawList: [],
    withdrawLoading: true,
    withdrawLoadMore: true,

    coinDepositList: [],
    depositLoading: true,
    depositLoadMore: true,

    leverBorrowList: [],
    leverBorrowLoading: true,
    borrowListLoadMore: true,

    leverTransferList: [],
    leverTransferLoading: true,
    transferListLoadMore: true,
  },
  reducers: {
    saveCoinWithdrawList(state, { payload }) {
      return { ...state, ...payload };
    },
    saveCoinDepositList(state, { payload }) {
      return { ...state, ...payload };
    },
    saveLoading(state, { payload }) {
      return { ...state, ...payload };
    },
    saveLeverBorrowList(state, { payload }) {
      return { ...state, ...payload };
    },
    saveLeverTransferList(state, { payload }) {
      return { ...state, ...payload };
    },
  },
  effects: {
    * getCoinWithdrawList({ payload }, { call, put, select}) {
      try {
        console.log(payload);
        const response = yield call(getWithdrawList, payload);
        const arr = response.data && response.data ? response.data : { list: [] };
        if (payload['pageNum'] < 2) {
          yield put({ type: 'saveCoinWithdrawList', payload: {coinWithdrawList: arr.list, withdrawLoadMore: arr.list.length >= 10}});
        } else {
          const orderListArr = JSON.parse(JSON.stringify(yield select(state => state.asset.coinWithdrawList)));
          if (arr.list.length < 10) {
            yield put({ type: 'saveCoinWithdrawList', payload: {coinWithdrawList: orderListArr.concat(arr.list), withdrawLoadMore: false }});
          } else {
            yield put({ type: 'saveCoinWithdrawList', payload: {coinWithdrawList: orderListArr.concat(arr.list), withdrawLoadMore: true} });
          }
        }
        yield put({ type: 'saveLoading', payload: {withdrawLoading: false} });

      } catch (err) {
        yield put({ type: 'saveLoading', payload: {withdrawLoading: false} });
        console.log(err);
      }
    },
    * getCoinDepositList({ payload }, { call, put, select}) {
      try {
        console.log(payload);
        const response = yield call(getDepositList, payload);
        const arr = response.data ? response.data : { list: [] };
        if (payload['pageNum'] < 2) {
          yield put({ type: 'saveCoinDepositList', payload: {coinDepositList: arr.list, depositLoadMore: arr.list.length >= 10} });
        } else {
          const orderListArr = JSON.parse(JSON.stringify(yield select(state => state.asset.coinDepositList)));
          if (arr.list.length < 10) {
            yield put({ type: 'saveCoinDepositList', payload: {coinDepositList: orderListArr.concat(arr.list), depositLoadMore: false }});
          } else {
            yield put({ type: 'saveCoinDepositList', payload: {coinDepositList: orderListArr.concat(arr.list), depositLoadMore: true} });
          }
        }
        yield put({ type: 'saveLoading', payload: {depositLoading: false} });
      } catch (err) {
        yield put({ type: 'saveLoading', payload: {depositLoading: false} });
        console.log(err);
      }
    },

    * appleLeverBorrow({ payload }, { call, put, select}) {
      try {
        const response = yield call(leverBorrow, payload);
        console.log(response);
        yield put({ type: 'mine/getLeverAsset' });
        Toast.show('申请成功');

      } catch (err) {
        Toast.show('申请失败');
        console.log(err);
      }
    },
    * getLeverBorrowList({ payload }, { call, put, select}) {
      try {
        const response = yield call(getLeverBorrowList, payload);
        console.log(response);
        const arr = response.data ? response.data : { list: [] };
        if (payload['pageNum'] < 2) {
          yield put({ type: 'saveLeverBorrowList', payload: {leverBorrowList: arr.list, borrowListLoadMore: arr.list.length >= 10}});
        } else {
          const orderListArr = JSON.parse(JSON.stringify(yield select(state => state.asset.leverBorrowList)));
          if (arr.list.length < 10) {
            yield put({ type: 'saveLeverBorrowList', payload: {leverBorrowList: orderListArr.concat(arr.list), borrowListLoadMore: false }});
          } else {
            yield put({ type: 'saveLeverBorrowList', payload: {leverBorrowList: orderListArr.concat(arr.list), borrowListLoadMore: true} });
          }
        }
        yield put({ type: 'saveLoading', payload: {leverBorrowLoading: false} });

      } catch (err) {
        yield put({ type: 'saveLoading', payload: {leverBorrowLoading: false} });
        console.log(err);
      }
    },
    * leverBorrowRepayment({ payload }, { call, put, select}) {
      try {
        const response = yield call(leverBorrowRepayment, payload);
        console.log(response);
        yield put({ type: 'mine/getLeverAsset' });
        Toast.show('还款成功');
      } catch (err) {
        Toast.show('还款失败');
        console.log(err);
      }
    },
    * leverTransferRecharge({ payload }, { call, put, select}) {
      try {
        const response = yield call(leverTransferRecharget, payload);
        console.log(response);
        yield put({ type: 'mine/getAsset' });
        yield put({ type: 'mine/getLeverAsset' });
        Toast.show('提交成功');
      } catch (err) {
        if (err.message) {
          Toast.show(err.message);
        } else {
          Toast.show('提交失败');
        }
        console.log(err);
      }
    },
    * leverTransferWithdrawal({ payload }, { call, put, select}) {
      try {
        const response = yield call(leverTransferWithdrawal, payload);
        console.log(response);
        yield put({ type: 'mine/getAsset' });
        yield put({ type: 'mine/getLeverAsset' });
        Toast.show('提交成功');
      } catch (err) {
        if (err.message) {
          Toast.show(err.message);
        } else {
          Toast.show('提交失败');
        }
        console.log(err);
      }
    },

    * getLeverTransferList({ payload }, { call, put, select}) {
      try {
        const response = yield call(getLeverTransferList, payload);
        console.log(response);
        const arr = response.data ? response.data : { list: [] };
        if (payload['pageNum'] < 2) {
          yield put({ type: 'saveLeverTransferList', payload: {leverTransferList: arr.list, transferListLoadMore: arr.list.length >= 10}});
        } else {
          const orderListArr = JSON.parse(JSON.stringify(yield select(state => state.asset.leverTransferList)));
          if (arr.list.length < 10) {
            yield put({ type: 'saveLeverTransferList', payload: {leverTransferList: orderListArr.concat(arr.list), transferListLoadMore: false }});
          } else {
            yield put({ type: 'saveLeverTransferList', payload: {leverTransferList: orderListArr.concat(arr.list), transferListLoadMore: true} });
          }
        }
        yield put({ type: 'saveLoading', payload: {leverTransferLoading: false} });

      } catch (err) {
        yield put({ type: 'saveLoading', payload: {leverTransferLoading: false} });
        console.log(err);
      }
    },


  },
  subscriptions: {
    setup({ dispatch, history }) {
      // subscriptions
    }
  }
};
