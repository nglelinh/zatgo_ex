import { getBalanceData, getOrderData, submitOrder, cancelOrder } from '../services/app';
import { eventChannel } from 'redux-saga';
import * as SocketUtil from 'utils/SocketUtil';
import intl from 'react-intl-universal';
import { Toast } from 'antd-mobile';
import {dealWithData} from '../constants/ArrayDataUtils';
// import entryOrderData from '../../mock/entryOrderData.json';

let depthStepSocket = null;
const storage = window.localStorage;

const createEventChannel = (param) => {
  console.log(param);
  return eventChannel(emit => {
    if (depthStepSocket !== null) {
      SocketUtil.closeWebSocket(depthStepSocket);
    }
    depthStepSocket = SocketUtil.createWebSocket();
    SocketUtil.sendMessage(depthStepSocket, param, (data) => emit({ data: JSON.parse(data) }));
    return () => { depthStepSocket.close() };
  });
};

const isEqual = (p1, p2) => {
  return p1.toUpperCase() === p2.toUpperCase();
};

export default {
  namespace: 'deal',
  state: {
    entryOrderInfo: [],
    balanceInfo: [],
    orderList: [],
    depthLoading: false,
    listLoadMore: true
  },
  reducers: {
    saveEntryOrderData(state, { payload }) {
      return { ...state, entryOrderInfo: payload };
    },
    saveBalance(state, { payload }) {
      return { ...state, balanceInfo: payload };
    },
    saveOrder(state, { payload, listLoadMore }) {
      return { ...state, orderList: payload, listLoadMore: listLoadMore };
    },
    updateDepthLoading(state, { payload }) {
      return { ...state, depthLoading: payload };
    },

  },
  effects: {
    * closeDepthStep({payload}, {call}) {
      try {
        // console.log('closeDepthStep');
        if (depthStepSocket) yield call(SocketUtil.closeWebSocket, depthStepSocket);
      } catch (err) {
        console.log(err);
      }
    },
    * getEntryOrderData({ payload }, { call, put, take, select }) {
      // yield put({type: 'saveEntryOrderData', payload: entryOrderData.data});
      try {
        yield put({type: 'saveEntryOrderData', payload: []});
        let params = payload;
        params['event'] = 'REQ';
        yield put({type: 'updateDepthLoading', payload: true});
        const channel = yield call(createEventChannel, params);
        const response = (yield take(channel)).data;
        // console.log('深度：', JSON.stringify(response));
        const {type, symbol} = response;
        if (isEqual(payload.symbol, symbol) && isEqual(payload.type, type)) {
          yield put({type: 'saveEntryOrderData', payload: response.data});
          yield put({type: 'updateDepthLoading', payload: false});
        }
        params['event'] = 'SUB';
        const channelWS = yield call(createEventChannel, params);
        while (true) {
          const response = (yield take(channelWS)).data;
          // console.log('深度：', JSON.stringify(response));
          const {type, symbol, operateType} = response;
          const lastData = JSON.parse(JSON.stringify(yield select(state => state.deal.entryOrderInfo)));
          if (isEqual(payload.symbol, symbol) && isEqual(payload.type, type)) {
            yield put({type: 'updateDepthLoading', payload: false});
            if (operateType === 'increment' || operateType === 'INCREMENT') {
              // console.log('increment');
              let dealData = dealWithData(lastData, response.data, 'id');
              yield put({type: 'saveEntryOrderData', payload: dealData});
            } else {
              // console.log('FULL');
              yield put({type: 'saveEntryOrderData', payload: response.data});
            }
          }
        }
      } catch (err) {
        yield put({type: 'saveEntryOrderData', payload: []});
        console.log(err);
      }
    },
    * getBalanceData({ payload }, { call, put }) {
      try {
        const response = yield call(getBalanceData, payload);
        yield put({ type: 'saveBalance', payload: response.data });
      } catch (err) {
        console.log(err);
      }
    },
    * getOrderList({ payload }, { call, put, select }) {
      try {
        if (global.exchangeType) {
          payload['exchangeType'] = global.exchangeType;
          payload['apiKey'] = storage['apiKey'];
          payload['secretKey'] = storage['secretKey'];
        }
        const response = yield call(getOrderData, payload);
        const arr = response.data ? response.data : { list: [] };
        if (payload['pageNo'] < 2) {
          yield put({ type: 'saveOrder', payload: arr.list, listLoadMore: arr.list.length >= 10 ? true : false });
        } else {
          const orderListArr = JSON.parse(JSON.stringify(yield select(state => state.deal.orderList)));
          if (arr.list.length < 10) {
            yield put({ type: 'saveOrder', payload: orderListArr.concat(arr.list), listLoadMore: false });
          } else {
            yield put({ type: 'saveOrder', payload: orderListArr.concat(arr.list), listLoadMore: true });
          }

        }
      } catch (err) {
        console.log(err);
      }
    },
    * submitOrder({ payload, listParams }, { call, put, }) {
      try {
        Toast.loading(intl.get('deal.create'));
        if (global.exchangeType) {
          payload['exchangeType'] = global.exchangeType;
          payload['apiKey'] = storage['apiKey'];
          payload['secretKey'] = storage['secretKey'];
        }
        const response = yield call(submitOrder, payload);
        console.log(JSON.stringify(response));
        yield put({ type: 'getOrderList', payload: listParams });
        yield put({ type: 'getBalanceData' });
        Toast.hide();
        Toast.success(intl.get('deal.creatsuc'));
      } catch (err) {
        Toast.fail(intl.get('deal.creatfail'));
        console.log(err);
      }
    },
    * cancelOrder({ payload, listParams }, { call, put }) {
      try {
        Toast.loading(intl.get('deal.canceling'));
        if (global.exchangeType) {
          payload['exchangeType'] = global.exchangeType;
          payload['apiKey'] = storage['apiKey'];
          payload['secretKey'] = storage['secretKey'];
        }
        const response = yield call(cancelOrder, payload);
        console.log(JSON.stringify(response));
        yield put({ type: 'getOrderList', payload: listParams });
        yield put({ type: 'getBalanceData' });
        Toast.hide();
        Toast.success(intl.get('deal.cancelsuc'));
      } catch (err) {
        Toast.fail(intl.get('deal.cancelfail'));
        console.log(err);
      }
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {

    }
  }
};
