import {Toast} from 'antd-mobile';
import { routerRedux } from 'dva/router';
import intl from 'react-intl-universal';
import { phoneLogin, phoneRegister, phoneFogotPwd, sendPhoneCode, emailLogin, emailRegister, emailFogotPwd, sendEmailCode, onLogout} from '../services/login';

export default {

  namespace: 'login',
  state: {
    name: '测试',
    id: 1
  },
  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *phoneLogin({ payload }, { call, put }) {  // eslint-disable-line
      try {
        const response = yield call(phoneLogin, payload);
        console.log(response);
        global.exchangeToken = response && response.data && response.data.token ? response.data.token : '';
        console.log(response && response.data && response.data.token ? response.data.token : '');
        console.log('exchangeToken' + global.exchangeToken);
        yield put(routerRedux.push('/'));
        Toast.success(intl.get('toast.loginSuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },
    *phoneFogotPwd({ payload }, { call, put }) {  // eslint-disable-line
      try {
        const response = yield call(phoneFogotPwd, payload);
        console.log(response);
        Toast.success(intl.get('toast.modifySuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },
    *phoneRegister({ payload }, { call, put }) {  // eslint-disable-line
      try {
        console.log(payload);
        const response = yield call(phoneRegister, payload);
        console.log(response);
        Toast.success(intl.get('toast.regSuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },
    *sendPhoneCode({ payload }, { call, put }) {  // eslint-disable-line
      try {
        const response = yield call(sendPhoneCode, payload);
        console.log(response);
        Toast.success(intl.get('toast.sendSuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },

    *emailLogin({ payload }, { call, put }) {  // eslint-disable-line
      try {
        const response = yield call(emailLogin, payload);
        console.log(response);
        Toast.success(intl.get('toast.loginSuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },
    *emailFogotPwd({ payload }, { call, put }) {  // eslint-disable-line
      try {
        const response = yield call(emailFogotPwd, payload);
        console.log(response);
        Toast.success(intl.get('toast.modifySuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },
    *emailRegister({ payload }, { call, put }) {  // eslint-disable-line
      try {
        const response = yield call(emailRegister, payload);
        console.log(response);

        Toast.success(intl.get('toast.regSuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },
    *sendEmailCode({ payload }, { call, put }) {  // eslint-disable-line
      try {
        const response = yield call(sendEmailCode, payload);
        console.log(response);
        Toast.success(intl.get('toast.sendSuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },
    *onLogout({ payload }, { call, put }){  // eslint-disable-line
      try {
        const response = yield call(onLogout, payload);
        console.log(response);
        global.exchangeToken = null;
        yield put(routerRedux.push('/login'));
        yield put({type: 'price/onCloseAllWebSocket'});
        yield put({type: 'deal/closeDepthStep'});
        Toast.success(intl.get('toast.logoutSuc'));
      } catch (err) {
        Toast.fail(err.message);
        console.log(err);
      }
    },


  },

  reducers: {
    save(state, { payload }) {
      console.log(payload);
      return { ...state, ...payload };
    },
  },

};
