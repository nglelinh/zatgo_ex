import api from 'constants/api';
import newRequest from 'utils/newRequest';


/**
 * 用户手机登录
 * @param {*} payload
 */
export function phoneLogin(payload) {
  return newRequest(api.phoneLogin, payload, 'post');
}

/**
 * 用户邮箱登录
 * @param {*} payload
 */
export function emailLogin(payload) {
  return newRequest(api.emailLogin, payload, 'post');
}

/**
 * 忘记用户手机密码
 * @param {*} payload
 */
export function phoneFogotPwd(payload) {
  return newRequest(api.phoneFogotPwd, payload, 'post');
}

/**
 * 用户忘记邮箱密码
 * @param {*} payload
 */
export function emailFogotPwd(payload) {
  return newRequest(api.emailFogotPwd, payload, 'post');
}

/**
 * 用户手机注册
 * @param {*} payload
 */
export function phoneRegister(payload) {
  return newRequest(api.phoneRegister, payload, 'post');
}

/**
 * 用户邮箱注册
 * @param {*} payload
 */
export function emailRegister(payload) {
  return newRequest(api.emailRegister, payload, 'post');
}

/**
 * 用户手机验证
 * @param {*} payload
 */
export function sendPhoneCode(payload) {
  return newRequest(api.sendPhoneCode, payload);
}

/**
 * 用户邮箱验证
 * @param {*} payload
 */
export function sendEmailCode(payload) {
  return newRequest(api.sendEmailCode, payload);
}

/**
 * 用户退出
 * @param {*} payload
 */
export function onLogout(payload) {
  return newRequest(api.logout, payload);
}
