import { eventChannel } from 'redux-saga';
import api from 'constants/api';
import request from 'utils/request';
import * as SocketUtil from 'utils/SocketUtil';
let marketSocket = null;
let tickerSocket = null;
let tradeTickerSocket = null;
let klineSocket = null;

const createMarketChannel = (param) => {
  console.log(param);
  return eventChannel(emit => {
    if (marketSocket !== null) {
      SocketUtil.closeWebSocket(marketSocket);
    }
    marketSocket = SocketUtil.createWebSocket();
    SocketUtil.sendMessage(marketSocket, param, (data) => emit({ data: JSON.parse(data) }));
    return () => { marketSocket.close() };
  });
};

const createTickerChannel = (param) => {
  console.log(param);
  return eventChannel(emit => {
    if (tickerSocket != null) {
      SocketUtil.closeWebSocket(tickerSocket);
    }
    tickerSocket = SocketUtil.createWebSocket();
    SocketUtil.sendMessage(tickerSocket, param, (data) => emit({ data: JSON.parse(data) }));
    return () => { tickerSocket.close() };
  });
};

const createTradeTickerChannel = (param) => {
  console.log(param);
  return eventChannel(emit => {
    if (tradeTickerSocket != null) {
      SocketUtil.closeWebSocket(tradeTickerSocket);
    }
    tradeTickerSocket = SocketUtil.createWebSocket();
    SocketUtil.sendMessage(tradeTickerSocket, param, (data) => emit({ data: JSON.parse(data) }));
    return () => { tradeTickerSocket.close() };
  });
};

const createKLineChannel = (param) => {
  console.log(param);
  return eventChannel(emit => {
    if (marketSocket != null) {
      SocketUtil.closeWebSocket(marketSocket);
    }

    if (klineSocket != null) {
      SocketUtil.closeWebSocket(klineSocket);
    }
    klineSocket = SocketUtil.createWebSocket();
    SocketUtil.sendMessage(klineSocket, param, (data) => emit({ data: JSON.parse(data) }));
    return () => { klineSocket.close() };
  });
};

const createEventChannel = (param) => {
  return eventChannel(emit => {
    const ws = SocketUtil.createWebSocket();
    SocketUtil.sendMessage(ws, param, (data) => emit({ data: JSON.parse(data) }));
    return () => { ws.close() };
  });
};

/**
 * 登录
 * @param {*} params
 */
export function login(params) {
  return request(api.login, params, 'post');
}

/**
 * 登出
 */
export function logout() {
  return request(api.logout);
}

/**
 * 查询系统支持的所有交易对及精度
 */
export function getCoinPairs() {
  if (global.exchangeType) {
    return request(api.coinPairs, { exchangeType: global.exchangeType });
  }
  return request(api.coinPairs);
}

/**
 * 查询汇率
 */
export function queryRate() {
  if (global.exchangeType) {
    return request(api.publicRate, { exchangeType: global.exchangeType });
  }
  return request(api.publicRate);
}

/**
 * 订阅-行情
 * @param {*} quoteCoin 计价币
 */
export function getMarket(quoteCoin) {
  if (global.exchangeType) {
    quoteCoin['exchangeType'] = global.exchangeType;
  }
  return createMarketChannel(quoteCoin);
}

/**
 * 订阅-24h行情
 * @param {*} symbol 币对
 */
export function getTicker(symbol) {
  if (global.exchangeType) {
    symbol['exchangeType'] = global.exchangeType;
  }
  return createTickerChannel(symbol);
}

/**
 * 订阅-实时成交信息
 * @param {*} symbol
 */
export function getTradeTicker(symbol) {
  if (global.exchangeType) {
    symbol['exchangeType'] = global.exchangeType;
  }
  return createTradeTickerChannel(symbol);
}

/**
 * 订阅-kline
 * @param {*} symbol
 */
export function getKLine(symbol) {
  if (global.exchangeType) {
    symbol['exchangeType'] = global.exchangeType;
  }
  return createKLineChannel(symbol);
}

/**
 * 订阅-深度
 * @param {*} symbol
 */
export function getDepthStep(symbol) {
  if (global.exchangeType) {
    symbol['exchangeType'] = global.exchangeType;
  }
  return createEventChannel(symbol);
}


export function onCloseWebSocket() {
  SocketUtil.closeWebSocket(tickerSocket);
  SocketUtil.closeWebSocket(tradeTickerSocket);
  SocketUtil.closeWebSocket(klineSocket);
}

export function onCloseAllWebSocket() {
  if (marketSocket !== null) SocketUtil.closeWebSocket(marketSocket);
  if (tickerSocket !== null)SocketUtil.closeWebSocket(tickerSocket);
  if (tradeTickerSocket !== null)SocketUtil.closeWebSocket(tradeTickerSocket);
  if (klineSocket !== null)SocketUtil.closeWebSocket(klineSocket);
}

/**
 * 获取当前挂单
 * @param {*} payload
 */
export function getEntryOrderData(payload) {
  return request(api.getEntryOrderData, payload);
}


/**
 * 获取当前用户余额
 * @param {*} payload
 */
export function getBalanceData(payload) {
  return request(api.getBalanceData, payload);
}

/**
 * 获取最新订单
 * @param {*} payload
 */
export function getOrderData(payload) {
  return request(api.getOrderData, payload, 'post');
}

/**
 * 提交订单
 * @param {*} payload
 */
export function submitOrder(payload) {
  return request(api.submitOrder, payload, 'post');
}

/**
 * 取消订单
 * @param {*} payload
 */
export function cancelOrder(payload) {
  return request(api.cancelOrder, payload, 'post');
}
