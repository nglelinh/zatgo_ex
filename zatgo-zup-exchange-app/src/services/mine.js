import api from 'constants/api';
import newRequest from 'utils/newRequest';


/**
 * 获取用户信息
 * @param {*} payload
 */
export function getUserInfo(payload) {
  return newRequest(api.mine_info, payload);
}

/**
 * 获取币币交易账户信息
 * @param {*} payload
 */
export function getCoinData(payload) {
  return newRequest(api.mine_assets_coin, payload);
}

/**
 * 获取杠杆交易账户信息
 * @param {*} payload
 */
export function getLeverData(payload) {
  return newRequest(api.mine_assets_lever, payload);
}


// 币币交易请求

/**
 * 币币提币列表
 * @param {*} payload
 */
export function getWithdrawList(payload) {
  return newRequest(api.coin_withdrawList, payload);
}


/**
 * 币币充值列表
 * @param {*} payload
 */
export function getDepositList(payload) {
  return newRequest(api.coin_depositList, payload);
}

// 杠杆交易请求

/**
 * 杠杆借贷申请
 * @param {*} payload
 */
export function leverBorrow(payload) {
  return newRequest(api.lever_borrow, payload, 'post');
}


/**
 * 杠杆借贷申请列表
 * @param {*} payload
 */
export function getLeverBorrowList(payload) {
  return newRequest(api.lever_borrowList, payload);
}

/**
 * 杠杆借贷还款
 * @param {*} payload
 */
export function leverBorrowRepayment(payload) {
  return newRequest(api.lever_borrowRepayment, payload, 'post');
}


/**
 *杠杆划转充值
 * @param {*} payload
 */
export function leverTransferRecharget(payload) {
  return newRequest(api.lever_transferRecharge, payload, 'post');
}

/**
 * 杠杆划转提币
 * @param {*} payload
 */
export function leverTransferWithdrawal(payload) {
  return newRequest(api.lever_transferWithdrawal, payload, 'post');
}

/**
 * 杠杆划转列表
 * @param {*} payload
 */
export function getLeverTransferList(payload) {
  return newRequest(api.lever_transferList, payload);
}

/**
 * 订单管理
 * @param {*} payload
 */
export function getOrderMangerList(payload) {
  return newRequest(api.orderManager, payload);
}
