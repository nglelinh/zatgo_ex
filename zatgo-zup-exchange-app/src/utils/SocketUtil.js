let lockReconnect = false;  // 避免ws重复连接
// let closeSign = false;
let WS;
let WSparams;
let WScallBack = () => {};
const getWsUrl = () => {
  // return `ws://work.flybycloud.com:6674/ws/exchange/appapi?token=${global.token}&cloudUserId=${global.cloudUserId}`; // Dev
  return `${window.WebSocketURL}/ws/exchange/appapi?token=${global.token}&cloudUserId=${global.cloudUserId}`; // release

};

// const wsUrl = `ws://192.168.1.37:8082/ws/exchange/appapi`;

export const createWebSocket = () => {
  try {
    return new WebSocket(getWsUrl());
  } catch (err) {
    console.error('socket创建失败', err);
  }
};

export const closeWebSocket = (socket) => {
  try {
    // closeSign = true;
    console.log('主动关闭');
    socket.close();
  } catch (err) {
    console.error(err);
  }
};

export const sendMessage = (ws, params, onCallback) => {
  WS = ws;
  WSparams = params;
  WScallBack = onCallback;
  ws.onopen = () => {
    console.log('连接成功' + new Date().toUTCString());
    ws.send(JSON.stringify(params));
  };

  ws.onmessage = (event) => {
    // 拿到任何消息都说明当前连接是正常的
    let item = event.data && JSON.parse(event.data);
    if (item.event === 'PING') {
      console.log(item);
      ws.send(JSON.stringify({'data': item.data, 'event': 'PONG'}));
    } else {
      onCallback(event.data);
    }
  };

  ws.onclose = () => {
    console.log('连接关闭' + new Date().toUTCString());
    WS = null;
    WSparams = null;
    WScallBack = () => {};
    // if (closeSign) {
    //   closeSign = false;
    // } else {
    //   reconnect(getWsUrl());
    // }
  };
  ws.onerror = () => {
    closeWebSocket(ws);
    reconnect(getWsUrl());
    console.error('连接错误');
  };
};

const reconnect = (url) => {
  if (lockReconnect) return;
  lockReconnect = true;

  // 没连接上会一直重连，设置延迟避免请求过多
  setTimeout(() => {
    try {
      createWebSocket();
      sendMessage(WS, WSparams, WScallBack);
      lockReconnect = false;
    } catch (err) {
      console.error('重连失败', err);
    }
  }, 3000);
};

