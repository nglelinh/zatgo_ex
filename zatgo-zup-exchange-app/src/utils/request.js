import axios from 'axios';
import { Toast } from 'antd-mobile';

// axios 配置
axios.defaults.timeout = 20000;  // 设置超时时间
// axios.defaults.baseURL = 'http://work.flybycloud.com:6674';// dev
const baseURL = window.baseURL || 'http://118.25.180.37:6674';

const reqHeaders = {
  'Content-Type': 'application/json',
};

const getAuthToken = () => {
  let header;
  if (global.token) {
    header = global.token;
  }
  return header ? header : '';
};

const checkStatus = (res) => {
  if (res.status >= 200 && res.status < 300) {
    return res.data;
  }
  const error = new Error(res.statusText);
  error.res = res;
  throw error;
};

const checkCode = (res) => {
  if (res.code !== null && res.code === '0000') { // 正常返回
    return res;
  } else {
    if (res.code !== null && (res.code === '10015' || res.code === '1004')) {
      Toast.fail('TOKEN失效');
    }
    return Promise.reject(res);
  }
};

const request = (url, params, method = 'get') => {
  console.log('url:', url);
  let baseUrl = baseURL + url;
  reqHeaders.token = getAuthToken();
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      headers: reqHeaders,
      url: baseUrl + `?cloudUserId=${global.cloudUserId}`,
      data: method === 'post' ? params : {},
      params: method === 'get' ? params : {}
    })
      .then(checkStatus)
      .then(checkCode)
      .then(data => resolve(data))
      .catch(error => {
        console.log('返回异常:', error);
        reject(error);
      });
  });
};
export default request;
