import axios from 'axios';
import { Toast } from 'antd-mobile';
import secret from 'constants/secret';

// axios 配置
axios.defaults.timeout = 20000;  // 设置超时时间
// axios.defaults.baseURL = 'http://work.flybycloud.com:6674';// dev
const baseURL = window.NewBaseURL || 'http://118.25.180.37:6674';

const reqHeaders = {
  'Content-Type': 'application/json',
  'appId': '111',
  'appVersion': '1.0.1',
  'appType': '0',
};

const checkStatus = (res) => {
  if (res.status >= 200 && res.status < 300) {
    return res.data;
  }
  const error = new Error(res.statusText);
  error.res = res;
  throw error;
};

/**
 * 时间戳
 */
const getTs = () => {
  return (new Date()).valueOf();
};

/**
 * 签名
 * @param {接口名} api
 * @param {请求参数} params
 */
const getSign = (data) => {
  let params =  data ? JSON.parse(JSON.stringify(data)) : null;
  let temp = {};
  if (params) {
    for (let key in params) {
      if (key === 'csessionid' || key === 'scene' || key === 'sig' || key === 'token' || JSON.stringify(params[key]) === 'null') continue;
      temp[key] = params[key];
    }
  }
  let sign = '';
  let allParams = JSON.stringify(temp) !== '{}' ? Object.assign(temp, reqHeaders) : reqHeaders;
  console.log('allParams', allParams);
  // eslint-disable-next-line
  for (let key in allParams) {
    if ((allParams[key] instanceof Object) || (allParams[key] instanceof Array)) {
      allParams[key] = '';
    }
    if (key === 'sign' || JSON.stringify(allParams[key]) === 'null') continue;
    let value = key + '=' + allParams[key] + '&';
    sign += value;
  }
  return secret.Encrypt(sign.substring(0, sign.length - 1));
};

const getAuthToken = () => {
  let header;
  if (global.exchangeToken) {
    header = global.exchangeToken;
  }
  return header ? header : null;
};

const checkCode = (res) => {
  if (res.code !== null && res.code === '0000') { // 正常返回
    return res;
  } else {
    if (res.code !== null && (res.code === '10015' || res.code === '1004')) {
      Toast.fail('TOKEN失效');
    }
    return Promise.reject(res);
  }
};

const newRequest = (url, params, method = 'get') => {
  console.log('newUrl:', url);
  let baseUrl = baseURL + url;
  console.log(baseUrl);
  reqHeaders.ts = getTs();
  if (getAuthToken())reqHeaders.token = getAuthToken();
  reqHeaders.sign = getSign(params);

  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: baseUrl,
      headers: reqHeaders,
      data: method === 'post' ? params : {},
      params: method === 'get' ? params : {},
    })
      .then(checkStatus)
      .then(checkCode)
      .then(data => resolve(data))
      .catch(error => {
        console.log('返回异常:', error);
        reject(error);
      });
  });
};
export default newRequest;

