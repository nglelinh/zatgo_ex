import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';

export default class LocaleProvider extends PureComponent {
  static propTypes = {
    children: PropTypes.any,
    locale: PropTypes.string,
    messages: PropTypes.object,
  };

  state = { initDone: false }

  componentDidMount() {
    this.loadLocales();
  }

  loadLocales() {
    const { messages, locale } = this.props;
    intl.init({
      currentLocale: locale,
      locales: messages,
    }).then(() => {
      this.setState({ initDone: true });
    });
  }

  render() {
    return this.state.initDone && React.Children.only(this.props.children);
  }
}
