export default {
  // common
  'com.cancel': '取消',
  'com.loading': '加载中',
  'com.noMore': '没有更多了',
  'com.noData': '很抱歉，没有相关数据',
  'com.searchData': '数据加载中',
  'com.noDataSearch': '通过搜索币种可添加自选币对',

  // chart
  'chart.buy': '买单',
  'chart.sell': '卖单',
  // title
  'title.price': '行情',
  'title.deal': '交易',
  'title.user': '我的',
  'title.order': '订单管理',
  'title.assets': '资产管理',
  'title.about': '关于',
  'title.coins': '币种详情',
  'title.lever': '币对详情',
  'title.borrow': '借贷',
  'title.transfer': '划转',
  'title.myOrder': '订单管理',
  'title.login': '登录',
  'title.loginRegister': '账号注册',
  'title.forgotPwd': '忘记密码',

  // tab
  'tab.price': '行情',
  'tab.deal': '交易',
  'tab.mine': '我的',
  // price
  'price.search': '搜索币种',
  'price.favorites': '自选',
  'price.addFavorites': '添加自选',
  // price detail
  'detail.depth': '深度',
  'detail.trades': '交易',
  'detail.direction': '方向',
  'detail.time': '时间',
  'detail.price': '价格',
  'detail.amount': '数量',
  'detail.buy': '买入',
  'detail.sell': '卖出',
  'detail.high': '高',
  'detail.low': '低',
  'detail.vol': '量',
  // deal
  'deal.buy': '买入',
  'deal.sell': '卖出',
  'deal.buy2': '买',
  'deal.sell2': '卖',
  'deal.price': '价格',
  'deal.number': '数量',
  'deal.all': '全部',
  'deal.market': '市价',
  'deal.limit': '限价',
  'deal.volumeoftrade': '交易额',
  'deal.currentorder': '当前订单',
  'deal.avail': '可用',
  'deal.pulldown': '松开刷新,最新时间:',
  'deal.update': '更新完成，最新时间:',
  'deal.markertmsg': '以当前最优价格交易',
  'deal.creatsuc': '订单创建成功',
  'deal.creatfail': '订单创建失败',
  'deal.create': '创建中...',
  'deal.cancel': '取消',
  'deal.msg': '是否取消当前订单？',
  'deal.ok': '确定',
  'deal.canceling': '取消中...',
  'deal.cancelsuc': '订单取消成功',
  'deal.cancelfail': '订单取消失败',
  'deal.noenough': '可用余额不足',
  'deal.enterprice': '请输入价格',
  'deal.enteramount': '请输入数量',
  'deal.entertotal': '请输入交易额',
  'deal.maximumlimit': '数量或价格精度超过最大限制',
  'order.market': '市场',
  'order.price': '价格',
  'order.number': '数量',
  'order.average': '价格/成交均价',
  'order.amont': '数量/成交数量',
  'order.cenceled': '已取消',
  'order.filled': '已完成',
  'order.exception': '异常',
  'order.allorder': '全部订单',
  'order.currentorder': '当前订单',
  'order.finshorder': '成交订单',
  'order.manage': '订单管理',
  'order.historyOrder': '历史订单',

  // mine
  'mine.mag': '确认还款',
  'mine.ok': '确定',
  'mine.cancel': '取消',
  // Toast
  'toast.regSuc': '注册成功',
  'toast.regFail': '注册失败',
  'toast.sendSuc': '发送成功',
  'toast.sendFail': '发送失败',
  'toast.loginSuc': '登录成功',
  'toast.loginFail': '登录失败',
  'toast.modifySuc': '修改失败',
  'toast.modifyFail': '修改失败',
  'toast.logoutSuc': '登出成功',
  'toast.logoutFail': '登出失败',


};
