import enUS from './en-US.js';
import zhCN from './zh-CN.js';

export { default as LocaleProvider } from './components/provider';

export const messages = { 'en-US': enUS, 'zh-CN': zhCN };