/**
 * Created by zhoujianxin on 2018/12/19.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import style from './phoneInputStyle.css';
import IntlTelInput from 'react-intl-tel-input';
import 'react-intl-tel-input/dist/main.css';



class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      dialCode: '+86'
    };
  }

  componentDidMount() {

  }

  onChange=(value) => {
    console.log(value);
    this.setState({
      value: value
    });
  }

  onCountryChange=(dialCode) => {
    console.log(dialCode);
    this.setState({
      dialCode: dialCode
    });
  }

  render() {
    const {value } = this.state;
    console.log(value);
    return (
      <div style={styles.container}>
        <IntlTelInput style={{height: 44, width: '100%', alignItems: 'center', justifyContent: 'center', paddingBottom: 2 }}
          css={['intl-tel-input', style.input]}
          fieldName={'phone'}
          placeholder={''}
          value={this.state.value || ''}
          defaultCountry={'cn'}
          separateDialCode
          onSelectFlag={(num, country) => {
            this.onCountryChange(country.dialCode);

          }}
          onPhoneNumberChange={(status, value, countryData, number, id) => {
            this.onChange(value);
          }}
          onPhoneNumberBlur={(status, value, countryData, number, id) => {
            let params = {
              dialCode: this.state.dialCode,
              value: this.state.value,
              number: number
            };
            this.props.callBack && this.props.callBack(params);
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(index);

const styles = {
  container: {
    display: 'flex',
    flex: 1,
    borderBottom: '0.2px solid #EEEEEE'
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#E26A6A',
    height: 30,
    width: 65,
    justifyContent: 'center',
    alignItems: 'center',
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
