import React from 'react';


export default class index extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      count: 60, // 秒数初始化为60秒
      liked: true // 文案默认为‘获取验证码‘
    };
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  // 获取短信验证码
  handleClick = () => {
    // liked is false 的时候，不允许再点击
    if (!this.state.liked) {
      return;
    }
    this.props.sendCode && this.props.sendCode();
    let count = this.state.count;
    console.log(count);
    this.timer = setInterval(() => {
      this.setState({count: (count--), liked: false},
        () => {
          if (count === 0) {
            clearInterval(this.timer);
            this.setState({
              liked: true,
              count: 60
            });
          }
        });
    }, 1000);
  }

  render() {
    return (
      <div style={styles.container} onClick={this.handleClick}>
        {
          this.state.liked ?
            <div>获取验证码</div>
            :
            <div style={{}}>{this.state.count + 's'}</div>
        }
      </div>
    );
  }
}

const styles = {
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
};
