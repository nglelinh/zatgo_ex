/**
 * Created by zhoujianxin on 2018/12/17.
 * @Desc
 */
import React, {Component} from 'react';
import {connect} from 'dva';
import CodeModal from './CodeModal';

const NoCaptcha = require('NoCaptcha');
let nc;
let nc_token = ['FFFF00000000017A7076', (new Date()).getTime(), Math.random()].join(':');
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      userPassword: '',
      unameHelp: '',
      upwdHelp: ''
    };
    this.props.onRef && this.props.onRef(this);
  }

  componentDidMount() {
    this.onNoCaptcha();
  }

 onNoCaptcha =() => {
   let that = this;
   console.log(nc_token);
   nc = NoCaptcha.init({
     renderTo: '#nc',
     appkey: 'FFFF00000000017A7076',
     scene: 'nc_login_h5',
     token: nc_token,
     trans: {'key1': 'code0'},
     elementID: ['usernameID'],
     is_Opt: 0,
     language: 'cn',
     timeout: 10000,
     retryTimes: 5,
     errorTimes: 5,
     inline: false,
     apimap: {
       // 'analyze': '//a.com/nocaptcha/analyze.jsonp',
       // 'uab_Url': '//aeu.alicdn.com/js/uac/909.js',
     },
     bannerHidden: false,
     initHidden: false,
     callback: function (data) {
       console.log(data);
       that.props.callBack && that.props.callBack({
         csessionid: data.csessionid,
         scene: 'nc_login_h5',
         sig: data.sig,
         token: nc_token,
       });
     },
     error: function (s) {
     }
   });
   NoCaptcha.setEnabled(true);
   nc.reset();// 请务必确保这里调用一次reset()方法
   NoCaptcha.upLang('cn', {
     'LOADING': '加载中...', // 加载
     'SLIDER_LABEL': '请向右滑动验证', // 等待滑动
     'CHECK_Y': '验证通过', // 通过
     'ERROR_TITLE': '非常抱歉，这出错了...', // 拦截
     'CHECK_N': '验证未通过', // 准备唤醒二次验证
     'OVERLAY_INFORM': '经检测你当前操作环境存在风险，请输入验证码', // 二次验证
     'TIPS_TITLE': '验证码错误，请重新输入'// 验证码输错时的提示
   });
 }

  reset=() => {
    nc_token = ['FFFF00000000017A7076', (new Date()).getTime(), Math.random()].join(':');
    nc.reset();// reset()方法
    this.onNoCaptcha();

  }

  onCancel=() => {
    nc.reset();// 请务必确保这里调用一次reset()方法
    this.props.onClose && this.props.onClose();
  }

  onSendCode=() => {
    this.props.sendCode && this.props.sendCode();
  }

  submit=(code) => {
    this.props.submit && this.props.submit(code);
  }

  render() {
    const { visible, onClose} = this.props;
    return (
      <div style={styles.container}>
        <div id="__nc" style={{width: '100%', height: '80%'}}>
          <div id="nc"></div>
        </div>
        <CodeModal visible={visible}
          onClose={() => { onClose() }}
          onCancel={() => { this.onCancel() }}
          submit={(code) => { this.submit(code) }}
          onSendCode={() => { this.onSendCode() }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(index);

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  button: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#E26A6A',
    height: 30,
    width: 65,
    justifyContent: 'center',
    alignItems: 'center',
  },
  font11: {
    color: '#797F85',
    fontSize: 11,
    textAlign: 'center'
  },
  font16: {
    fontSize: 16
  }
};
