/**
 * Created by zhoujianxin on 2018/12/17.
 * @Desc
 */
import React, { Component } from 'react';
import { Modal} from 'antd-mobile';
import Validation from '../Validation';
export default class codeModal extends Component {

  state={
    code: ''
  };

  handleCancel = () => {
    this.props.onCancel && this.props.onCancel();
  }

  handleCreate = () => {
    if (this.state.code !== '') {
      this.props.submit && this.props.submit(this.state.code);
      this.setState({
        code: ''
      });
    }
  }

  sendCode=() => {
    this.props.onSendCode && this.props.onSendCode();
  }

  render() {
    const { visible } = this.props;
    return (
      <Modal
        visible={visible}
        transparent
        maskClosable={false}
        onClose={() => { this.handleCancel() }}
        title="短信验证"
        footer={[{ text: '取消', onPress: () => { this.handleCancel() } },
          { text: '确定', onPress: () => { this.handleCreate() } }]}
      >
        <div style={styles.container} >
          <input
            style={{width: '65%'}}
            value={this.state.code}
            placeholder="请输入验证码"
            onChange={(e) => { this.setState({code: e.target.value}) }}
          />
          <div style={styles.line} />
          <Validation
            sendCode={() => { this.sendCode() }}
          />
        </div>
      </Modal>
    );
  }
}

const styles = {
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  line: {
    display: 'flex',
    width: 1,
    height: 20,
    backgroundColor: '#aeb2b5'
  },
};
