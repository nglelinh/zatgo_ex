import React, { Component } from 'react';
import { Flex } from 'antd-mobile';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';
import btnCss from './EntryOrder.css';


class EntryOrder extends Component {

  onClick = (item) => {
    const { onItemClick } = this.props;
    onItemClick && onItemClick(item);
  }

  getFullNum = (num) => {
    // 处理非数字
    if (isNaN(num)) { return num }

    // 处理不需要转换的数字
    let str = String(num);
    if (!/e/i.test(str)) { return num }

    return (num).toFixed(18).replace(/\.?0+$/, '');
  }

  renderItem = (data, type = false, maxWth) => {
    const { buyTextColor, sellTextColor, buyBgColor, sellBgColor } = this.props;
    const item = data;
    let arr = [];
    for (let i = 0; i < item.length; i++) {
      let width = 100;
      if (item[i]['amount'] < maxWth) {
        width = item[i]['amount'] / maxWth * 100;
      }
      let left = type ? '0' : `${100 - width}%`;
      arr.push(
        <div style={{ display: 'flex', flexDirection: 'row', position: 'relative' }} key={type + i}>
          <div style={{
            position: 'absolute', left: left, backgroundColor: type ? sellBgColor : buyBgColor,
            zIndex: 1, width: `${width}%`, height: '100%', opacity: 0.15
          }}
          />

          <div className={type ? btnCss.btn2 : btnCss.btn1} style={styles.container} onClick={() => {
            this.onClick(item[i].price);
          }}>
            {type ?
              <div style={{
                display: 'flex',
                flexDirection: 'column',
                textAlign: 'left',
                flex: 4,
                marginLeft: 4
              }}>
                <div style={{ color: sellTextColor, fontSize: 11 }}>
                  {this.getFullNum(item[i].price)}
                </div>
              </div> :
              <div style={{
                display: 'flex',
                flexDirection: 'column',
                flex: 1,
                marginRight: 11
              }}>
                <div style={{...styles.font11, paddingLeft: 5}}>{i + 1}</div>
              </div>

            }

            <div style={{
              display: 'flex',
              flexDirection: 'column',
            }}>
              <div className={btnCss.lineLimit} style={{ textAlign: type ? 'right' : 'left', color: '#A0A4A8', fontSize: 11 }}>
                {this.getFullNum(item[i].amount)}
              </div>
            </div>

            {type ?
              <div style={{
                display: 'flex',
                flexDirection: 'column',
                flex: 1,
                marginLeft: 11
              }}>
                <div style={{...styles.font11, paddingRight: 5}}>{i + 1}</div>
              </div> :
              <div style={{
                display: 'flex',
                flexDirection: 'column',
                textAlign: 'right',
                flex: 4,
                marginRight: 4
              }}>
                <div style={{ color: buyTextColor, fontSize: 11 }}>
                  {this.getFullNum(item[i].price)}
                </div>
              </div>
            }
          </div>
        </div>
      );
    }
    return arr;
  }

  render() {
    const { titleColor, style, buyData, sellData, maxNum } = this.props;
    let arrAll = buyData.concat(sellData);
    let maxWth = 0;
    for (let i = 0; i < arrAll.length; i++) {
      arrAll[i]['amount'] > maxWth && (maxWth = arrAll[i]['amount']);
    }
    return (
      <div style={{ ...style }}>
        <Flex style={{ alignItems: 'flex-start' }}>
          <div style={{ flex: 1 }}>
            <Flex>
              <div style={{ flex: 1, textAlign: 'center', color: titleColor, paddingLeft: 4, marginRight: 9}}>
                {intl.get('deal.buy2')}
              </div>
              <div style={{ width: '79px', textAlign: 'left', color: titleColor }}>
                {intl.get('deal.number')}
              </div>
              <div style={{ flex: 4, textAlign: 'right', color: titleColor, marginRight: 4 }}>
                {intl.get('deal.price')}
              </div>
            </Flex>
            {this.renderItem(buyData && buyData.slice(0, maxNum), false, maxWth)}
          </div>

          <div style={{ flex: 1 }}>
            <Flex>
              <div style={{ flex: 4, textAlign: 'left', color: titleColor, marginLeft: 4 }}>
                {intl.get('deal.price')}
              </div>
              <div style={{width: '79px', textAlign: 'right', color: titleColor }}>
                {intl.get('deal.number')}
              </div>
              <div style={{ flex: 1, textAlign: 'center', color: titleColor, paddingRight: 4, marginLeft: 9 }}>
                {intl.get('deal.sell2')}
              </div>
            </Flex>
            {this.renderItem(sellData && sellData.slice(0, maxNum), true, maxWth)}
          </div>
        </Flex>
      </div>
    );
  }
}

EntryOrder.propTypes = {
  buyData: PropTypes.array,
  sellData: PropTypes.array,
  onClick: PropTypes.func,
  style: PropTypes.object,
  titleColor: PropTypes.string,
  viewColor: PropTypes.string,
  buyTextColor: PropTypes.string,
  buyBgColor: PropTypes.string,
  sellTextColor: PropTypes.string,
  sellBgColor: PropTypes.string,
  maxNum: PropTypes.number
};

EntryOrder.defaultProps = {
  buyData: [],
  sellData: [],
  titleColor: '#A0A4A8',
  buyTextColor: '#35BAA0',
  buyBgColor: '#35BAA0',
  sellTextColor: '#E26A6A',
  sellBgColor: '#E26A6A',
  maxNum: 10
};

export default EntryOrder;

const styles = {
  container: {
    zIndex: 2,
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    height: 25,
    justifyContent: 'space-between',
    cursor: 'pointer',
    alignItems: 'center'
  },
  font11: {
    color: '#A0A4A8',
    fontSize: 11,
    textAlign: 'center'
  }
};
