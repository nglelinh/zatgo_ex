
import dayjs from 'dayjs';
import intl from 'react-intl-universal';

const klineData = require('./kline.json').data;
const depthData = require('./depth.json').data;

function splitData(rawData) {
  let categoryData = [];
  let values = [];
  let volumes = [];
  for (let i = 0; i < rawData.length; i++) {
    categoryData.push(rawData[i].splice(0, 1)[0]);
    values.push(rawData[i]);
    volumes.push([i, rawData[i][4], rawData[i][0] > rawData[i][1] ? 1 : -1]);
  }
  return {
    categoryData: categoryData,
    values: values,
    volumes: volumes
  };
}


function calculateMA(dayCount, data0) {
  // const klineArray = handleCandlestickData(klineData);
  // const data0 = splitData(klineArray);
  let result = [];
  for (let i = 0, len = data0.values.length; i < len; i++) {
    if (i < dayCount) {
      result.push('-');
      continue;
    }
    let sum = 0;
    for (let j = 0; j < dayCount; j++) {
      sum += data0.values[i - j][1];
    }
    result.push(sum / dayCount);
  }
  return result;
}

function handleCandlestickData(data) {
  let klineArray = [];
  if (data) {
    data.forEach(item => {
      const time = dayjs(Number(item.timePoint) * 1000).format('MM-DD HH:mm');
      klineArray.push([time, item.open, item.close, item.low, item.high, item.vol]);
    });
  }
  return klineArray;
}

// function getMaxMinPrice(arrs) {
//   if (arrs && arrs.length > 0) {
//     let min = arrs[0].low;
//     let max = arrs[0].high;
//     arrs.forEach(item => {
//       if (item.high > max) {
//         max = item.high;
//       }
//       if (item.low < min) {
//         min = item.low;
//       }
//     });
//     max = max > 1 ? Math.ceil(max) : max;
//     min = min > 1 ? Math.floor(min) : min;
//     return { max, min };
//   }
//   return { max: 0, min: 0 };
// }

function getArraySum(array) {
  return array.reduce((pre, cur, index, arr) => {
    (index === 0) ? pre.push(cur) : pre.push(cur + pre[index - 1]);
    return pre;
  }, []);
}

function handleDepthData(data) {

  let asksPrices = data.asks.map(item => item.price);
  let bidsPrices = data.bids.map(item => item.price);
  let asksValues = data.asks.map(item => item.amount);
  let bidsValues = data.bids.map(item => item.amount);
  bidsPrices.reverse();
  let categoryData = bidsPrices.concat(asksPrices);

  let spaceArr = [];
  bidsValues.forEach(() => {
    spaceArr.push('');
  });

  const tempBids = getArraySum(bidsValues);
  tempBids.reverse();
  bidsValues = tempBids.concat(spaceArr);

  const tempAsks = getArraySum(asksValues);
  asksValues = spaceArr.concat(tempAsks);
  return {
    categoryData,
    bidsValues,
    asksValues,
  };
}

const depthmapOption = (data = depthData) => {
  let data0 = handleDepthData(data);
  // console.log('处理数据：', JSON.stringify(data0));
  return {
    grid: {
      left: 0,
      top: 30,
      right: 0,
      bottom: 0
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: { type: 'line', lineStyle: { color: 'rgba(0, 0, 0, 0)' } },
      backgroundColor: '#355475',
      textStyle: { color: '#fff', fontSize: '14px' },
    },
    axisPointer: {
      show: false,
      triggerOn: 'none'
    },
    legend: {
      data: [
        { name: intl.get('chart.buy'), icon: 'rect', },
        { name: intl.get('chart.sell'), icon: 'rect', }
      ],
      itemWidth: 10,
      itemHeight: 10,
      selectedMode: false,
      textStyle: { color: '#000' },
    },
    xAxis: {
      type: 'category',
      axisLine: { show: false },
      axisTick: { show: false },
      axisLabel: { show: false },
      data: data0.categoryData,
      splitNumber: 3,
      boundaryGap: false,
      min: 'dataMin',
      max: 'dataMax',
    },
    yAxis: [{
      scale: true,
      type: 'value',
      position: 'right',
      axisLine: { show: false },
      splitLine: { show: false },
      axisTick: { inside: true },
      boundaryGap: false,
      axisLabel: {
        show: true,
        verticalAlign: 'top',
        inside: true,
        formatter: function (value) {
          let txt = value;
          if (value > 1000) {
            txt = `${Math.ceil((value / 1000))}k`;
          }
          return txt;
        }
      },
      min: 'dataMin',
      max: 'dataMax'
    }],
    series: [
      {
        name: intl.get('chart.buy'),
        type: 'line',
        smooth: true,
        symbol: 'circle',
        showSymbol: false,
        symbolSize: 3,
        sampling: 'average',
        itemStyle: { normal: { color: '#4cc453' } },
        areaStyle: { normal: { color: 'rgb(108, 191, 134)' } },
        data: data0.bidsValues
      },
      {
        name: intl.get('chart.sell'),
        type: 'line',
        smooth: true,
        symbol: 'circle',
        showSymbol: false,
        symbolSize: 3,
        sampling: 'average',
        itemStyle: { normal: { color: '#e94c4c' } },
        areaStyle: { normal: { color: 'rgb(203, 111, 71)' } },
        data: data0.asksValues
      }]
  };
};

const candlestickOption = (data = klineData) => {
  // console.log('默认数据：', JSON.stringify(data));
  const klineArray = handleCandlestickData(data);
  const data0 = splitData(klineArray);
  // console.log('处理数据：', JSON.stringify(data0));
  return {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross'
      },
      position: function (pos, params, el, elRect, size) {
        let obj = { top: 60 };
        obj[['left', 'right'][Number(pos[0] < size.viewSize[0] / 2)]] = 5;
        return obj;
      },
      // 自定义展示数据
      formatter: function (params) {

      }
    },
    axisPointer: {
      link: { xAxisIndex: 'all' },
      label: {
        backgroundColor: '#777'
      },
      triggerOn: 'click'
    },
    visualMap: {
      show: false,
      seriesIndex: 1,
      dimension: 2,
      pieces: [{
        value: 1,
        color: 'rgb(236, 111, 71)'
      }, {
        value: -1,
        color: 'rgb(108, 191, 134)'
      }]
    },
    grid: [{
      top: 20,
      left: 5,
      right: 5,
      height: 320
    }, {
      left: 5,
      right: 5,
      height: 50,
      top: 330
    }],
    xAxis: [{
      axisLabel: { show: false },
      axisTick: { show: false },
      axisPointer: { label: { show: false } },
      type: 'category',
      data: data0.categoryData,
      scale: true,
      boundaryGap: false,
      axisLine: {
        onZero: false,
        show: false
      },
      splitLine: {
        lineStyle: { color: 'rgb(43, 50, 69)' }
      },
      splitNumber: 5,
      min: 'dataMin',
      max: 'dataMax'
    }, {
      type: 'category',
      gridIndex: 1,
      data: data0.categoryData,
      scale: true,
      boundaryGap: false,
      axisLine: { onZero: false },
      splitNumber: 5,
      min: 'dataMin',
      max: 'dataMax',
      axisLabel: {
        fontSize: 8,
        color: 'rgb(102, 112, 133)'
      },
    }],
    yAxis: [{
      position: 'right',
      scale: true,
      splitLine: {
        lineStyle: { color: 'rgba(102, 112, 133, 0.2)', width: 0.5 },
        show: true
      },
      axisTick: { inside: true },
      axisLine: { show: false },
      axisLabel: {
        inside: true,
        formatter: function (value, index) {
          return value.toFixed(11);
        },
        fontSize: 8,
        color: 'rgba(102, 112, 133, 0.8)'
      },
      splitArea: { show: false }
    }, {
      scale: true,
      gridIndex: 1,
      splitNumber: 2,
      axisLabel: { show: true },
      axisLine: { show: false },
      axisTick: { show: false },
      splitLine: { show: false }
    }],
    dataZoom: {
      type: 'inside',
      start: 90,
      end: 100,
      minValueSpan: 0,
      xAxisIndex: [0, 1],
      preventDefaultMouseMove: false
    },
    series: [
      {
        name: '日K',
        type: 'candlestick',
        data: data0.values,
        itemStyle: {
          normal: {
            color: 'rgb(108, 191, 134)',
            color0: 'rgb(236, 111, 71)',
            borderColor: null,
            borderColor0: null
          }
        }
      },
      {
        name: 'Volume',
        type: 'bar',
        data: data0.volumes,
        xAxisIndex: 1,
        yAxisIndex: 1,
      },
      {
        name: 'MA5',
        type: 'line',
        data: calculateMA(5, data0),
        smooth: true,
        showSymbol: false,
        lineStyle: {
          normal: {
            width: 1
          }
        }
      },
      {
        name: 'MA10',
        type: 'line',
        data: calculateMA(10, data0),
        smooth: true,
        showSymbol: false,
        lineStyle: {
          normal: {
            width: 1
          }
        }
      },
      {
        name: 'MA20',
        type: 'line',
        data: calculateMA(20, data0),
        smooth: true,
        showSymbol: false,
        lineStyle: {
          normal: {
            width: 1
          }
        }
      }
    ]
  };
};

export default {
  candlestickOption,
  depthmapOption
};
