
import React from 'react';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/candlestick';
import 'echarts/lib/chart/line';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/grid';
import 'echarts/lib/component/dataZoom';
import 'echarts/lib/component/visualMap';

let dataRefresh = true;
let myChart;
let startPercent = 90;
let endPercent = 100;

export default class Candlestick extends React.PureComponent {

  constructor(props) {
    super(props);
    this.initChart = this.initChart.bind(this);
  }

  componentDidMount() {
    myChart = echarts.init(this.ID);
    myChart.clear();
    this.initChart();
  }

  UNSAFE_componentWillUpdate() {
    this.initChart();
  }

  componentWillUnmount() {
    dataRefresh = true;
    startPercent = 90;
    endPercent = 100;
  }

  initChart() {
    const { option = {}, isLoading = false} = this.props;
    if (isLoading) {
      myChart.showLoading({
        text: '',
        color: '#c23531',
        textStyle: {color: '#444', fontSize: 20 },
        maskColor: 'rgba(233, 234, 235, 0.5)',
      });
    } else {
      myChart.hideLoading();
      myChart.on('datazoom', function (params) {
        // params里面有什么，可以打印出来看一下就明白
        // 可以通过params获取缩放的起止百分比，但是鼠标滚轮和伸缩条拖动触发的params格式不同，所以用另一种方法
        // 获得起止位置百分比
        startPercent = myChart.getModel().option.dataZoom[0].start;
        endPercent = myChart.getModel().option.dataZoom[0].end;
        console.log('startPercent=' + startPercent);
        console.log('endPercent=' + endPercent);

        if (Number(endPercent) >= 98) {
          dataRefresh = true;
        } else {
          dataRefresh = false;
        }
      });
      option.dataZoom.start = startPercent;
      option.dataZoom.end = endPercent;
      if (dataRefresh) {
      // 设置options
        myChart.setOption(option, true);
        window.onresize = function () {
          myChart.resize();
        };
      }
    }
  }

  render() {
    const { width = '100%', height = '400px' } = this.props;
    return <div ref={ID => { this.ID = ID }} style={{ width, height }}></div>;
  }
}

