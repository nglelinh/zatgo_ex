/*
 * @Author: lsl
 * @Date: 2018-08-16 09:31:49
 * @Last Modified by: lsl
 * @Last Modified time: 2018-09-06 18:24:24
 */
import React, { Component } from 'react';
import { Tabs } from 'antd-mobile';
import intl from 'react-intl-universal';
import { ListView, SearchBar } from 'components';

const getTabs = (quoteCoins) => {
  const favorites = [{ key: 'favorites', title: intl.get('price.favorites') }];
  const tabs = quoteCoins.map((v) => ({ title: v.toUpperCase(), key: v }));
  return favorites.concat(tabs);
};

const getFullNum = (num) => {
  // 处理非数字
  if (isNaN(num)) { return num }

  // 处理不需要转换的数字
  let str = String(num);
  if (!/e/i.test(str)) { return num }

  return (num).toFixed(18).replace(/\.?0+$/, '');
};


const PriceItem = (props) => {
  const { itemInfo, onItemClick, rate } = props;
  const quoteCoin = itemInfo.quoteCoin.toLowerCase();
  const last = itemInfo.last ? itemInfo.last : 0;
  const vol = itemInfo.vol ? itemInfo.vol : 0;
  return (
    <div
      style={styles.container}
      onClick={() => onItemClick(itemInfo)}
    >
      <div style={styles.itemLeft}>
        <div style={styles.font16}>
          {itemInfo.baseCoin.toUpperCase()}
          <font style={{ ...styles.font11, marginLeft: 7 }}>
            {`/${quoteCoin.toUpperCase()}`}
          </font>
        </div>
        <div style={{ ...styles.font11, marginTop: 8 }}>
          {`24H${intl.get('detail.vol')} ${Math.round(vol)}`}
        </div>
      </div>
      <div style={styles.itemRight}>
        <div style={styles.font16}>{` ${getFullNum(last)}`}</div>
        <div style={{ ...styles.font11, marginTop: 8 }}>
          {`￥${(last * rate[quoteCoin]).toFixed(2)}`}
        </div>
      </div>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        height: 30,
        width: 65,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        backgroundColor: itemInfo.chg ? Number(itemInfo.chg) > 0 ? '#35BAA0' : '#E26A6A' : '#A0A4A8',
        fontSize: 13,
      }}>
        {itemInfo.chg ? `${(itemInfo.chg) > 0 ? '+' : ''}${(itemInfo.chg).toFixed(2)}%` : '0.00%'}
      </div>
    </div>
  );
};


class PriceList extends Component {

  state = {
    curTickers: null,
    selectTab: '',
  }

  selectTicker = (quoteCoin, market) => {
    return quoteCoin ? market[quoteCoin.toLowerCase()] : [];
  }

  // tab切换
  onTabChange = (tab, index) => {
    if (tab.key === 'favorites') {
      this.setState({
        selectTab: tab.key
      });
    } else {
      this.setState({
        selectTab: ''
      });
      this.onSearchTickers(tab.key, false);
    }
  }

  onItemClick = (item) => {
    const { onItemClick } = this.props;
    onItemClick && onItemClick(item);
  }

  onSearchTickers = (txt, type) => {
    const input = txt.toLowerCase();
    const { market } = this.props;
    if (JSON.stringify(market) !== '{}') {
      const allMarket = Object.values(market).reduce((pre, cur) => pre.concat(cur));
      const curTickers = allMarket.filter((item) => {
        const baseCoin = item.baseCoin.toLowerCase();
        const quoteCoin = item.quoteCoin.toLowerCase();
        if (type) {
          return baseCoin.indexOf(input) !== -1;
        } else {
          return quoteCoin.indexOf(input) !== -1;
        }
      });
      this.setState({curTickers});
    }
  }

  favoritesMarket = () => {
    const {market} = this.props;
    let dataSource = JSON.parse(localStorage.getItem('symbols'));
    if (JSON.stringify(market) !== '{}' && JSON.stringify(dataSource) !== '{}') {
      const allMarket = Object.values(market).reduce((pre, cur) => pre.concat(cur));
      let favoritesMarket = [];
      // eslint-disable-next-line
      for (let key in dataSource) {
        let curTickers = allMarket.filter((item) => {
          return item['symbol'] === key;
        });
        if (curTickers.length > 0) {
          favoritesMarket = favoritesMarket.concat(curTickers);
        }
      }
      console.log(JSON.stringify(favoritesMarket));
      return favoritesMarket;
    }
  }

  render() {
    const { curTickers } = this.state;
    const { market, loading, onCancel, showCancelButton = false, quoteCoins, rates } = this.props;
    const filterTickers = this.selectTicker(quoteCoins[0], market);
    return (
      <div>
        <SearchBar
          cancelText={intl.get('com.cancel')}
          placeholder={intl.get('price.search')}
          maxLength={20}
          showCancelButton={showCancelButton}
          onCancel={() => { onCancel() }}
          onChange={(text) => {
            this.onSearchTickers(text, true);
          }}
        />
        <Tabs
          tabs={getTabs(quoteCoins)}
          initialPage={1}
          tabBarActiveTextColor="#35BAA0"
          tabBarInactiveTextColor="#797F85"
          onChange={this.onTabChange}
        >
          <ListView
            data={this.state.selectTab === 'favorites' ? this.favoritesMarket() : curTickers ? curTickers : filterTickers}
            ListItem={PriceItem}
            loading={loading}
            onItemClick={this.onItemClick}
            offsetHeight={100}
            rate={rates}
          />
        </Tabs>
      </div>
    );
  }
}

export default PriceList;

const styles = {
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '20px 10px 5px 10px'
  },
  button: {
    display: 'none',
    flexDirection: 'column',
    backgroundColor: '#E26A6A',
    height: 30,
    width: 65,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  font11: {
    color: '#797F85', fontSize: 11,
  },
  font16: {
    color: '#323B43',
    fontSize: 16,
  },
  itemLeft: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  itemRight: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  }
};
