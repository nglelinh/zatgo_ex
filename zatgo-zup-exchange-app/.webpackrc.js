import { resolve } from 'path';
export default {
  publicPath: "./",
  entry: "./src/pages/*.js",
  theme: "./theme-config.js",
  env: {
    development: {
      extraBabelPlugins: [
        'dva-hmr',
        ["module-resolver", { "alias": { "dva": "dva-react-router-3" } }],
        ["import", { "libraryName": "antd-mobile", "libraryDirectory": "lib", "style": true }]
      ]
    },
    production: {
      extraBabelPlugins: [
        ["transform-remove-console", { "exclude": ["error", "warn"] }],
        ["module-resolver", { "alias": { "dva": "dva-react-router-3" } }],
        ["import", { "libraryName": "antd-mobile", "libraryDirectory": "lib", "style": true }]
      ]
    }
  },
  copy: [{from:'./src/ipconfig.js', to: 'ipconfig.js'}],
  es5ImcompatibleVersions: true,
  extraBabelIncludes: [],
  externals:{
    "NoCaptcha": "window.NoCaptcha"
  },
  alias: {
    components: resolve(__dirname, "./src/components"),
    utils: resolve(__dirname, "./src/utils"),
    services: resolve(__dirname, "./src/services"),
    models: resolve(__dirname, "./src/models"),
    routes: resolve(__dirname, "./src/routes"),
    locales: resolve(__dirname, "./src/locales"),
    assets: resolve(__dirname, "./src/assets"),
    constants: resolve(__dirname, "./src/constants")
  }
}
